//
//  SafetyStepThreeResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>

#define SistemaDevuelvelListaDeCuentasVacia @""\
@"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
@"<MSG-S>"\
@"<INFORMACIONADICIONAL>"\
@"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
@"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
@"<MONEDA>SOLES</MONEDA>"\
@"<IMPORTE>456.90</IMPORTE>"\
@"<DIVIMPORTE>S/.</DIVIMPORTE>"\
@"<CUENTA>"\
@"<MONEDA>SOLES</MONEDA>"\
@"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
@"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
@"</CUENTA>"\
@"<EMPRESA></EMPRESA>"\
@"<SERVICIO></SERVICIO>"\
@"<COORDENADA></COORDENADA>"\
@"<SELLO></SELLO>"\
@"</INFORMACIONADICIONAL>"\
@"</MSG-S>"

@interface SafetyStepThreeResponseTest : XCTestCase

@end

@implementation SafetyStepThreeResponseTest

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

#pragma mark El sistema no devuelven cuentas para ese cliente

- (void) testSistemaDevuelveListaDeCuentasVacia_CountDebeSerIgualACero {
    
}

@end
