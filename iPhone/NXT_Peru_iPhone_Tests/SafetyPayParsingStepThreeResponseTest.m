//
//  SafetyPayParsingStepThreeResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 22/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SafetyPayDetailsResponse.h"


@interface SafetyPayParsingStepThreeResponseTest : XCTestCase
{
@private
    SafetyPayDetailsResponse *safetyPayDetailsResponse_;
}
@end

@implementation SafetyPayParsingStepThreeResponseTest

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

#pragma mark Numero de transacción

- (void) testParsingRespuestaNumeroTransaccionTieneDatos {

    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO></SERVICIO>"\
    @"<COORDENADA></COORDENADA>"\
    @"<SELLOSEG></SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];

    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.transactionNumber isEqualToString: @"4263" ]);
    
}

- (void) testParsingRespuestaNumeroTransaccionEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    /*@"<IDENTIFICADOR>4263</IDENTIFICADOR>"\*/
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO></SERVICIO>"\
    @"<COORDENADA></COORDENADA>"\
    @"<SELLOSEG></SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayDetailsResponse_.additionalInformation transactionNumber]);
}

- (void) testParsingRespuestaNumeroTransaccionEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR></IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO></SERVICIO>"\
    @"<COORDENADA></COORDENADA>"\
    @"<SELLOSEG></SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.transactionNumber length] == 0);
}


- (void) testParsingRespuestaNumeroTransaccionTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>   </IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO></SERVICIO>"\
    @"<COORDENADA></COORDENADA>"\
    @"<SELLOSEG></SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.transactionNumber length] == 0);
    
}

- (void) testParsingRespuestaNumeroTransaccionTieneEspaciosEnBlancoIzquierdayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>   </IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO></SERVICIO>"\
    @"<COORDENADA></COORDENADA>"\
    @"<SELLOSEG></SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.transactionNumber length] == 0);
    
}

#pragma mark Establecimiento


- (void) testParsingRespuestaEstableciemientoTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.establishment isEqualToString: @"Rosatel" ]);
    
}

- (void) testParsingRespuestaEstableciemientoEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    /*@"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\*/
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayDetailsResponse_.additionalInformation establishment]);
    
}

- (void) testParsingRespuestaEstableciemientoEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO></ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.establishment length] == 0);
    
}


- (void) testParsingRespuestaEstableciemientoTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>     </ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.establishment length] == 0);
    
}

- (void) testParsingRespuestaEstableciemientoTieneEspaciosEnBlancoIzquierdayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>   Rosatel  </ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.establishment isEqualToString: @"Rosatel"]);
    
}

#pragma mark Moneda

- (void) testParsingRespuestaMonedaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.currency isEqualToString: @"SOLES" ]);
    
}

- (void) testParsingRespuestaMonedaEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    /*@"<MONEDA>SOLES</MONEDA>"\*/
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayDetailsResponse_.additionalInformation currency]);
    
}

- (void) testParsingRespuestaMonedaEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA></MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.currency length] == 0);
    
}

- (void) testParsingRespuestaMonedaTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>      </MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.currency length] == 0);
    
}

- (void) testParsingRespuestaMonedaTieneEspaciosEnBlancoIzquierdayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA> SOLES </MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.currency isEqualToString: @"SOLES"]);
    
}

#pragma mark Importe

- (void) testParsingRespuestaImporteTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.amount doubleValue] == 456.90);
    
}

- (void) testParsingRespuestaImporteEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    /*@"<IMPORTE>456.90</IMPORTE>"\*/
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayDetailsResponse_.additionalInformation amount]);
    
}

- (void) testParsingRespuestaImporteEsVacioDebeSerCero {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayDetailsResponse_.additionalInformation.amount decimalValue];
    NSDecimal dec2 = [@(456.90) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1,&dec2)==NSOrderedSame);
    
}

- (void) testParsingRespuestaImporteTieneLetrasDebeSerCero {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>abc</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayDetailsResponse_.additionalInformation.amount decimalValue];
    NSDecimal dec2 = [@(0) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1, &dec2) == NSOrderedSame);
    
}

- (void) testParsingRespuestaImporteTieneEspaciosEnBlancoDebeSerCero {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE></IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayDetailsResponse_.additionalInformation.amount decimalValue];
    NSDecimal dec2 = [@(0) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1, &dec2) == NSOrderedSame);

    
}

- (void) testParsingRespuestaImporteTieneEspaciosEnBlancoIzquierdayDerechaDebeSerCero {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE> 450.56 </IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];

    NSDecimal dec1 = [safetyPayDetailsResponse_.additionalInformation.amount decimalValue];
    NSDecimal dec2 = [@(450.56) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1, &dec2) == NSOrderedSame);
}

#pragma mark Divisa importe

- (void) testParsingRespuestaDivisaImporteTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([[safetyPayDetailsResponse_.additionalInformation.badge lowercaseString] isEqualToString: @"s/."]);
    
}

- (void) testParsingRespuestaDivisaImporteEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    /*@"<DIVIMPORTE>S/.</DIVIMPORTE>"\*/
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayDetailsResponse_.additionalInformation badge]);
    
}

- (void) testParsingRespuestaDivisaImporteEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE></DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.badge length] == 0);
    
}

- (void) testParsingRespuestaDivisaImporteTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>  </DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.badge length] == 0);
    
}

- (void) testParsingRespuestaDivisaImporteTieneEspaciosEnBlancoIzquierdayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>  S/.   </DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([[safetyPayDetailsResponse_.additionalInformation.badge lowercaseString] isEqualToString: @"s/."]);
}

#pragma mark Cuenta

- (void) testParsingRespuestaTipoDeMonedaDeCuentaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.account.currency isEqualToString: @"SOLES"]);
}

- (void) testParsingRespuestaTipoDeCuentaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.account.accountType isEqualToString: @"CUENTA CORRIENTE"]);
}

- (void) testParsingRespuestaAsuntoDeCuentaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.account.subject isEqualToString: @"0011-0130-0119414501"]);
}

#pragma mark Empresa

- (void) testParsingRespuestaEmpresaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.company isEqualToString: @"SafetyPay"]);
}

- (void) testParsingRespuestaEmpresaTieneEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    /*@"<EMPRESA>SafetyPay</EMPRESA>"\*/
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayDetailsResponse_.additionalInformation company]);
}

- (void) testParsingRespuestaEmpresaTieneEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.company length] == 0);
}

- (void) testParsingRespuestaEmpresaTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>     </EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.company length] == 0);
}

- (void) testParsingRespuestaEmpresaTieneEspaciosEnBlancoIzquierdayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>    SafetyPay     </EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.company isEqualToString: @"SafetyPay"]);
}

#pragma mark Servicio


- (void) testParsingRespuestaServiceTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.service isEqualToString: @"Compras por Internet"]);
}

- (void) testParsingRespuestaServiceEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    /*@"<SERVICIO>Compras por Internet</SERVICIO>"\*/
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayDetailsResponse_.additionalInformation service]);
}

- (void) testParsingRespuestaServiceEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO></SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.service length] == 0);
}

- (void) testParsingRespuestaServiceEsTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>    </SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.service length] == 0);
}

- (void) testParsingRespuestaServiceEsTieneEspaciosEnBlancoIzquiedayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>  Compras por Internet  </SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.service isEqualToString: @"Compras por Internet"]);
}

#pragma mark Coordenada

- (void) testParsingRespuestaCoordenadaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.coordinate isEqualToString: @"A-1"]);
}

- (void) testParsingRespuestaCoordenadaEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    /*@"<COORDENADA>A-1</COORDENADA>"\*/
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayDetailsResponse_.additionalInformation coordinate]);
}

- (void) testParsingRespuestaCoordenadaEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA></COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.coordinate length] == 0);
}

- (void) testParsingRespuestaCoordenadaTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA></COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.coordinate length] == 0);
}

- (void) testParsingRespuestaCoordenadaTieneEspaciosEnBlancoIzquiedayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA></COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.coordinate length] == 0);
}

#pragma mark SELLOSEG

- (void) testParsingRespuestaSELLOSEGTieneData {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.seal isEqualToString: @"1010011010|-|--||-|-xoxooxxoxonynnyynyn"]);
}

- (void) testParsingRespuestaSELLOSEGEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    /*@"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\*/
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayDetailsResponse_.additionalInformation seal]);
}

- (void) testParsingRespuestaSELLOSEGEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG></SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.seal length] == 0);
}

- (void) testParsingRespuestaSELLOSEGTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>   </SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.seal length] == 0);
}

- (void) testParsingRespuestaSELLOSEGTieneEspaciosEnBlancoIzquierdayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>456.90</IMPORTE>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<COORDENADA>A-1</COORDENADA>"\
    @"<SELLOSEG>   1010011010|-|--||-|-xoxooxxoxonynnyynyn  </SELLOSEG>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepThreeResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayDetailsResponse_.additionalInformation.seal isEqualToString:@"1010011010|-|--||-|-xoxooxxoxonynnyynyn"]);
}

#pragma mark TEST METHOD

- (void) doParsingSafetyPayStepThreeResponseWithXMLString : (NSString *) xmlString {

    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    safetyPayDetailsResponse_ = [SafetyPayDetailsResponse alloc];
    safetyPayDetailsResponse_.openingTag = @"informacionadicional";
    
    [parser setDelegate: safetyPayDetailsResponse_];
    
    [safetyPayDetailsResponse_ parserDidStartDocument: parser];
    
    [parser parse];
}


@end
