//
//  SafetyStepTwoResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 23/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SafetyPayStepTwoResponse.h"
#import "Updater.h"

#define ClienteIngresaImporteyNumeroDeTransaccionCorrectamente @""\
@"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
@"<MSG-S>"\
@"<INFORMACIONADICIONAL>"\
@"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
@"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
@"<MONEDA>SOLES</MONEDA>"\
@"<IMPORTE>450.65</IMPORTE>"\
@"<EMPRESA>SafetyPay</EMPRESA>"\
@"<SERVICIO>Compras por Internet</SERVICIO>"\
@"<CUENTAS>"\
@"  <E>"\
@"      <MONEDA>SOLES</MONEDA>"\
@"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
@"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
@"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
@"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
@"  </E>"\
@"</CUENTAS>"\
@"<OPERADORAS>"\
@"  <E>"\
@"      <CODIGO>CLARO</CODIGO>"\
@"      <DESCRIPCION>Claro</DESCRIPCION>"\
@"  </E>"\
@"</OPERADORAS>"\
@"</INFORMACIONADICIONAL>"\
@"</MSG-S>"

#define ClienteIngresaNumeroTransaccionIncorrectamente @""\
@"<MSG-S>"\
@"<ERROR>"\
@"<CODIGOERROR>null</CODIGOERROR>"\
@"<MENSAJE>Estimado cliente, por el momento no podemos atender su operación. Intente nuevamente en unos instantes.</MENSAJE>"\
@"</ERROR>"\
@"</MSG-S>"

#define ClienteIngresaImporteIncorrectamente @""\
@"<MSG-S>"\
@"<ERROR>"\
@"<CODIGOERROR>null</CODIGOERROR>"\
@"<MENSAJE>Por favor, verifique si ha ingresado el importe correctamente.</MENSAJE>"\
@"</ERROR>"\
@"</MSG-S>"

#define ClienteIngresaImporteVacio @""\
@"<MSG-S>"\
@"<ERROR>"\
@"<CODIGOERROR>null</CODIGOERROR>"\
@"<MENSAJE>Por favor, verifique si ha ingresado el importe correctamente.</MENSAJE>"\
@"</ERROR>"\
@"</MSG-S>"

#define ClienteIngresaNumeroTransaccionVacio @""\
@"<MSG-S>"\
@"<ERROR>"\
@"<CODIGOERROR>null</CODIGOERROR>"\
@"<MENSAJE>Por favor, verifique si ha ingresado el número de transacción correctamente.</MENSAJE>"\
@"</ERROR>"\
@"</MSG-S>"

@interface SafetyStepTwoResponseTest : XCTestCase
{
@private
    NSString *transactionNumber_;
    NSString *amount_;
    SafetyPayStepTwoResponse *response_;
}
@end

@implementation SafetyStepTwoResponseTest

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

#pragma mark El cliente ingresa una transaccion correcta

- (void) testClienteIngresaCorrectamenteImporteyNumeroTransaccionNoDebeDevolverError {
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: ClienteIngresaImporteyNumeroDeTransaccionCorrectamente];
    
    XCTAssertTrue([response_ errorCode] == nil || [response_ errorMessage] == nil);
}

#pragma mark El cliente ingresa una transaccion incorrecta

- (void) testClienteIngresaIncorrectamenteNumeroTransaccionDebeDevolverError {
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: ClienteIngresaNumeroTransaccionIncorrectamente];
    
    //NSLog(@"\n\n%@\n\n",[response_ errorMessage]);
    
    XCTAssertTrue([response_ errorCode] != nil || [response_ errorMessage] != nil);
}

- (void) testClienteIngresaIncorrectamenteImporteDebeDevolverError {
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: ClienteIngresaImporteIncorrectamente];
    
    //NSLog(@"\n\n%@\n\n",[response_ errorMessage]);
    
    XCTAssertTrue([response_ errorCode] != nil || [response_ errorMessage] != nil);
}

- (void) testClienteIngresaNumeroTransaccionVacioDebeDevolverError {
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: ClienteIngresaNumeroTransaccionVacio];
    
    //NSLog(@"\n\n%@\n\n",[response_ errorMessage]);
    
    XCTAssertTrue([response_ errorCode] != nil || [response_ errorMessage] != nil);
}

- (void) testClienteIngresaImporteVacioDebeDevolverError {
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: ClienteIngresaImporteVacio];
    
    //NSLog(@"\n\n%@\n\n",[response_ errorMessage]);
    
    XCTAssertTrue([response_ errorCode] != nil || [response_ errorMessage] != nil);
}

#pragma mark Envio correcto de Parametros al sistema

- (void) testUrlContieneValoresDeImporteyNumeroDeTransaccion {
    
    transactionNumber_ = @"4263";
    amount_ = @"450.65";
    
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayTransactionInfoWithTransactionNumber : transactionNumber_ AndAmount: amount_];
    
    NSString *transactionNumberValue = [self valueFromURL :url forKeyParam : @"Identificador"];
    NSString *amountValue = [self valueFromURL:url forKeyParam: @"Importe"];
    
    XCTAssertTrue([transactionNumberValue isEqualToString: transactionNumber_] && [amountValue isEqualToString: amount_]);
}

#pragma mark -

- (void) doParsingSafetyPayStepTwoResponseWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    response_ = [SafetyPayStepTwoResponse alloc];
    response_.openingTag = @"informacionadicional";
    
    [parser setDelegate: response_];
    
    [response_ parserDidStartDocument: parser];
    
    [parser parse];
}

- (NSString *) valueFromURL : (NSString *) urlString forKeyParam :(NSString *) keyParam {
    
    NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
    NSArray *urlComponents = [urlString componentsSeparatedByString:@"&"];
    for (NSString *keyValuePair in urlComponents)
    {
        NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
        NSString *key = [pairComponents objectAtIndex:0];
        NSString *value = [pairComponents objectAtIndex:1];
        
        [queryStringDictionary setObject:value forKey:key];
    }
    return [queryStringDictionary objectForKey: keyParam];
}

@end
