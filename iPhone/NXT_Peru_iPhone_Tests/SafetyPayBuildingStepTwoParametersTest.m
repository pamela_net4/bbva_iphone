//
//  SafetyPayBuildingStepTwoParametersTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 20/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+URLAndHTMLUtils.h"
#import "Updater.h"

@interface SafetyPayBuildingStepTwoParametersTest : XCTestCase
{
@private
    NSString *numeroTransaccion_;
    NSString *importe_;
}
@property (nonatomic, readwrite) NSString *numeroTransaccion;
@property (nonatomic, readwrite) NSString *importe;
@end

@implementation SafetyPayBuildingStepTwoParametersTest

@synthesize numeroTransaccion = numeroTransaccion_;
@synthesize importe = importe_;

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void) testDevuelveURLConNumeroTransaccionIgualCienyImporteCientoVeinte {
    numeroTransaccion_ = @"100";
    importe_ = @"120.0";
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayTransactionInfoWithTransactionNumber:numeroTransaccion_ AndAmount :importe_];
    XCTAssertTrue([url isEqualToString:@"Identificador=100&Importe=120.0"]);
}

- (void) testDevuelveURLParametrosVaciosConNumeroTransaccionNuloyImporteNulo {
    numeroTransaccion_ = nil;
    importe_ = nil;
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayTransactionInfoWithTransactionNumber:numeroTransaccion_ AndAmount:importe_];
    XCTAssertTrue([url isEqualToString: @"Identificador=&Importe="]);
}

- (void) testDevuelveURLParametrosVaciosConNumeroTransaccionVacioyImporteVacio {
    numeroTransaccion_ = @"";
    importe_ = @"";
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayTransactionInfoWithTransactionNumber:numeroTransaccion_ AndAmount:importe_];
    XCTAssertTrue([url isEqualToString: @"Identificador=&Importe="]);
}

- (void) testDevuelveURLParametrosVaciosConNumeroTransaccionyImporteConEspaciosEnBlanco {
    numeroTransaccion_ = @"  ";
    importe_ = @"  ";
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayTransactionInfoWithTransactionNumber:numeroTransaccion_ AndAmount :importe_];
    XCTAssertTrue([url isEqualToString:@"Identificador=&Importe="]);
}

- (void) testDevuelveURLParametrosRecortadosConNumeroTransaccionyImporteConEspaciosEnBlancoIzquierdayDerecha {
    numeroTransaccion_ = @" 4566 ";
    importe_ = @" 4.556 ";
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayTransactionInfoWithTransactionNumber:numeroTransaccion_ AndAmount :importe_];
    XCTAssertTrue([url isEqualToString:@"Identificador=4566&Importe=4.556"]);
}


@end
