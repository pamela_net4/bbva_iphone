//
//  SafetyPayBuildingStepFourParametersTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 20/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+URLAndHTMLUtils.h"
#import "Updater.h"

@interface SafetyPayBuildingStepFourParametersTest : XCTestCase
{
@private
    NSString *segundoFactor_;
}

@property (nonatomic, readwrite) NSString *segundoFactor;

@end

@implementation SafetyPayBuildingStepFourParametersTest

@synthesize segundoFactor = segundoFactor_;

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void) testDevuelveURLConSegundoFactorIgualCientoUno {
    segundoFactor_ = @"101";
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayPaymentConfirmationWithSecondFactor:segundoFactor_];
    XCTAssertTrue([url isEqualToString:@"SegundoFactor=101"]);
}

- (void) testDevuelveParametrosVaciosConSegundoFactorNulo {
    segundoFactor_ = nil;
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayPaymentConfirmationWithSecondFactor:segundoFactor_];
    XCTAssertTrue([url isEqualToString: @"SegundoFactor="]);
}

- (void) testDevuelveParametrosVaciosConSegundoFactorVacio {
    segundoFactor_ = @"";
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayPaymentConfirmationWithSecondFactor:segundoFactor_];
    XCTAssertTrue([url isEqualToString: @"SegundoFactor="]);
}

- (void) testDevuelveUrlParametrosVaciosConSegundoFactorEspaciosEnBlanco {
    segundoFactor_ = @"  ";
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayPaymentConfirmationWithSecondFactor:segundoFactor_];
    XCTAssertTrue([url isEqualToString:@"SegundoFactor="]);
}

- (void) testDevuelveUrlParametrosRecortadosConSegundoFactorEspaciosEnBlancoIzquierdayDerecha {
    segundoFactor_ = @" 101 ";
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayPaymentConfirmationWithSecondFactor:segundoFactor_];
    XCTAssertTrue([url isEqualToString:@"SegundoFactor=101"]);
}

@end
