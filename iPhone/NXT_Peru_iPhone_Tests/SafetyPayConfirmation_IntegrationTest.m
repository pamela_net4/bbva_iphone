//
//  SafetyPayConfirmation_IntegrationTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "STUB_Updater.h"
#import "Session.h"
#import "XCTestCase+AsyncTesting.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "StringKeys.h"
#import "SafetyPayConfirmationAdditionalInformation.h"

@interface SafetyPayConfirmation_IntegrationTest : XCTestCase {
@private SafetyPayConfirmationAdditionalInformation *additionalInformation_;
}

@property (nonatomic, readwrite, retain) SafetyPayConfirmationAdditionalInformation *additionalInformation;
@end

@implementation SafetyPayConfirmation_IntegrationTest

@synthesize additionalInformation = additionalInformation_;

- (void)setUp
{
    [super setUp];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginResponseReceived_STUB:) name:kNotificationLoginEndsTest object:nil];
    
    [[STUB_Updater getInstance] loginWithId: @"4919149020771371" andPassword: @"gestion99"];
    
    [self waitForStatus:XCTAsyncTestCaseStatusSucceeded timeout:80];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(safetyPayConfirmationResponseReceived_STUB:) name:kNotificationSafetyPayConfirmationResponseReceivedTest object:nil];
    
    [[STUB_Updater getInstance] obtainSafetyPaySuccessFromConfirmPaymentWithSecondFactor:@"779"];
    
    [self waitForStatus:XCTAsyncTestCaseStatusSucceeded timeout:80];
    
    additionalInformation_ = [[Session getInstance] safetyPayConfirmationAdditionalInformation];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testSafetyPayConfirmation_ParsingNoContieneErrores
{
    XCTAssertNil([additionalInformation_ parseError]);
}

- (void)loginResponseReceived_STUB:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationLoginEndsTest object:nil];
    [self notify:XCTAsyncTestCaseStatusSucceeded];
    
}

- (void) safetyPayConfirmationResponseReceived_STUB: (NSNotificationCenter *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationSafetyPayDetailsResponseReceivedTest object:nil];
    [self notify:XCTAsyncTestCaseStatusSucceeded];
    
}

@end
