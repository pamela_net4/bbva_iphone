//
//  SafetyPayParsingStepFourResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 23/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SafetyPayConfirmationResponse.h"

@interface SafetyPayParsingStepFourResponseTest : XCTestCase
{
@private SafetyPayConfirmationResponse *safetyPayConfirmationResponse_;
}
@end

@implementation SafetyPayParsingStepFourResponseTest

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}



#pragma mark Numero de transacción

- (void) testParsingRespuestaNumeroTransaccionTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO></SERVICIO>"\
    @"<FECHAHORA>30/12/2012   15:30</FECHAHORA>"\
    @"<NUMOPERACION></NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.transactionNumber isEqualToString: @"4263" ]);
    
}

- (void) testParsingRespuestaNumeroTransaccionEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    /*@"<IDENTIFICADOR>4263</IDENTIFICADOR>"\*/
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO></SERVICIO>"\
    @"<FECHAHORA>30/12/2012   15:30</FECHAHORA>"\
    @"<NUMOPERACION></NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayConfirmationResponse_.additionalInformation transactionNumber]);
}

- (void) testParsingRespuestaNumeroTransaccionEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR></IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO></SERVICIO>"\
    @"<FECHAHORA>30/12/2012   15:30</FECHAHORA>"\
    @"<NUMOPERACION></NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.transactionNumber length] == 0);
}


- (void) testParsingRespuestaNumeroTransaccionTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>   </IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO></SERVICIO>"\
    @"<FECHAHORA>30/12/2012   15:30</FECHAHORA>"\
    @"<NUMOPERACION></NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.transactionNumber length] == 0);
    
}

- (void) testParsingRespuestaNumeroTransaccionTieneEspaciosEnBlancoIzquierdayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>  4563  </IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO></SERVICIO>"\
    @"<FECHAHORA>30/12/2012   15:30</FECHAHORA>"\
    @"<NUMOPERACION></NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.transactionNumber isEqualToString: @"4563"]);
    
}

#pragma mark Establecimiento


- (void) testParsingRespuestaEstableciemientoTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.establishment isEqualToString: @"Rosatel" ]);
    
}

- (void) testParsingRespuestaEstableciemientoEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    /*@"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\*/
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayConfirmationResponse_.additionalInformation establishment]);
    
}

- (void) testParsingRespuestaEstableciemientoEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO></ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.establishment length] == 0);
    
}


- (void) testParsingRespuestaEstableciemientoTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>     </ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.establishment length] == 0);
    
}

- (void) testParsingRespuestaEstableciemientoTieneEspaciosEnBlancoIzquierdayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>   Rosatel  </ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.establishment isEqualToString: @"Rosatel"]);
    
}

#pragma mark Moneda

- (void) testParsingRespuestaMonedaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.currency isEqualToString: @"SOLES" ]);
    
}

- (void) testParsingRespuestaMonedaEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    /*@"<MONEDA>SOLES</MONEDA>"\*/
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayConfirmationResponse_.additionalInformation currency]);
    
}

- (void) testParsingRespuestaMonedaEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA></MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.currency length] == 0);
    
}

- (void) testParsingRespuestaMonedaTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>      </MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.currency length] == 0);
    
}

- (void) testParsingRespuestaMonedaTieneEspaciosEnBlancoIzquierdayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA> SOLES </MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.currency isEqualToString: @"SOLES"]);
    
}

#pragma mark Importe

- (void) testParsingRespuestaImporteTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.amount doubleValue] == 456.90);
    
}

- (void) testParsingRespuestaImporteEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    /*@"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\*/
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayConfirmationResponse_.additionalInformation amount]);
    
}

- (void) testParsingRespuestaImporteEsVacioDebeSerCero {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO></IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayConfirmationResponse_.additionalInformation.amount decimalValue];
    NSDecimal dec2 = [@(0) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1,&dec2)==NSOrderedSame);
    
}

- (void) testParsingRespuestaImporteTieneLetrasDebeSerCero {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>abc</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayConfirmationResponse_.additionalInformation.amount decimalValue];
    NSDecimal dec2 = [@(0) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1, &dec2) == NSOrderedSame);
    
}

- (void) testParsingRespuestaImporteTieneEspaciosEnBlancoDebeSerCero {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO></IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayConfirmationResponse_.additionalInformation.amount decimalValue];
    NSDecimal dec2 = [@(0) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1, &dec2) == NSOrderedSame);
    
    
}

- (void) testParsingRespuestaImporteTieneEspaciosEnBlancoIzquierdayDerechaDebeSerCero {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO> 450.56 </IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayConfirmationResponse_.additionalInformation.amount decimalValue];
    NSDecimal dec2 = [@(450.56) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1, &dec2) == NSOrderedSame);
}


#pragma mark Importe pagado

- (void) testParsingRespuestaImportePagadoTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<IMPORTEPAGADO>456.90</IMPORTEPAGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.paidAmount doubleValue] == 456.90);
    
}

- (void) testParsingRespuestaImportePagadoEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    /*@"<IMPORTEPAGADO>456.90</IMPORTEPAGADO>"\*/
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayConfirmationResponse_.additionalInformation paidAmount]);
    
}

- (void) testParsingRespuestaImportePagadoEsVacioDebeSerCero {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<IMPORTEPAGADO></IMPORTEPAGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayConfirmationResponse_.additionalInformation.paidAmount decimalValue];
    NSDecimal dec2 = [@(0) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1,&dec2)==NSOrderedSame);
    
}

- (void) testParsingRespuestaImportePagadoTieneLetrasDebeSerCero {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<IMPORTEPAGADO>abc</IMPORTEPAGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayConfirmationResponse_.additionalInformation.paidAmount decimalValue];
    NSDecimal dec2 = [@(0) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1, &dec2) == NSOrderedSame);
    
}

- (void) testParsingRespuestaImportePagadoTieneEspaciosEnBlancoDebeSerCero {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>450.90</IMPORTECARGADO>"\
    @"<IMPORTEPAGADO></IMPORTEPAGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayConfirmationResponse_.additionalInformation.paidAmount decimalValue];
    NSDecimal dec2 = [@(0) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1, &dec2) == NSOrderedSame);
    
    
}

- (void) testParsingRespuestaImportePagadoTieneEspaciosEnBlancoIzquierdayDerechaDebeSerCero {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>450.56</IMPORTECARGADO>"\
    @"<IMPORTEPAGADO>   456.56  </IMPORTEPAGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayConfirmationResponse_.additionalInformation.paidAmount decimalValue];
    NSDecimal dec2 = [@(456.56) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1, &dec2) == NSOrderedSame);
}

#pragma mark Divisa importe

- (void) testParsingRespuestaDivisaImporteTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([[safetyPayConfirmationResponse_.additionalInformation.badge lowercaseString] isEqualToString: @"s/."]);
    
}

- (void) testParsingRespuestaDivisaImporteEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    /*@"<DIVIMPORTE>S/.</DIVIMPORTE>"\*/
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayConfirmationResponse_.additionalInformation badge]);
    
}

- (void) testParsingRespuestaDivisaImporteEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE></DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.badge length] == 0);
    
}

- (void) testParsingRespuestaDivisaImporteTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>  </DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.badge length] == 0);
    
}

- (void) testParsingRespuestaDivisaImporteTieneEspaciosEnBlancoIzquierdayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>  S/.   </DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([[safetyPayConfirmationResponse_.additionalInformation.badge lowercaseString] isEqualToString: @"s/."]);
}

#pragma mark Cuenta

- (void) testParsingRespuestaTipoDeMonedaDeCuentaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.account.currency isEqualToString: @"SOLES"]);
}

- (void) testParsingRespuestaTipoDeCuentaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.account.accountType isEqualToString: @"CUENTA CORRIENTE"]);
}

- (void) testParsingRespuestaAsuntoDeCuentaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.account.subject isEqualToString: @"0011-0130-0119414501"]);
}

#pragma mark Empresa

- (void) testParsingRespuestaEmpresaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([[safetyPayConfirmationResponse_.additionalInformation.company lowercaseString] isEqualToString: @"safetypay"]);
}

- (void) testParsingRespuestaEmpresaTieneEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    /*@"<EMPRESA>SafetyPay</EMPRESA>"\*/
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayConfirmationResponse_.additionalInformation company]);
}

- (void) testParsingRespuestaEmpresaTieneEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.company length] == 0);
}

- (void) testParsingRespuestaEmpresaTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>     </EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.company length] == 0);
}

- (void) testParsingRespuestaEmpresaTieneEspaciosEnBlancoIzquierdayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>    SafetyPay     </EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([[safetyPayConfirmationResponse_.additionalInformation.company lowercaseString] isEqualToString: @"safetypay"]);
}

#pragma mark Servicio


- (void) testParsingRespuestaServiceTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.service isEqualToString: @"Compras por Internet"]);
}

- (void) testParsingRespuestaServiceEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    /*@"<SERVICIO>Compras por Internet</SERVICIO>"\*/
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayConfirmationResponse_.additionalInformation service]);
}

- (void) testParsingRespuestaServiceEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO></SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.service length] == 0);
}

- (void) testParsingRespuestaServiceEsTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>    </SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.service length] == 0);
}

- (void) testParsingRespuestaServiceEsTieneEspaciosEnBlancoIzquiedayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>  Compras por Internet  </SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.service isEqualToString: @"Compras por Internet"]);
}

#pragma mark Fecha y hora

- (void) testParsingRespuestaFechaHoraTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.operationDateString isEqualToString: @"30/12/2012  | 15:30"]);
}

- (void) testParsingRespuestaFechaHoraEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    /*@"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\*/
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayConfirmationResponse_.additionalInformation operationDateString]);
}

- (void) testParsingRespuestaFechaHoraEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA></FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.operationDateString length] == 0);
}

- (void) testParsingRespuestaFechaHoraTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>       </FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.operationDateString length] == 0);
}

- (void) testParsingRespuestaFechaHoraTieneEspaciosEnBlancoIzquiedayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.operationDateString isEqualToString:@"30/12/2012  | 15:30"]);
}

#pragma mark Numero de operación

- (void) testParsingRespuestaNumeroOperacionTieneData {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>123451234566</NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.operationNumber isEqualToString: @"123451234566"]);
}

- (void) testParsingRespuestaNumeroOperacionEsNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    /*@"<NUMOPERACION>123451234566</NUMOPERACION>"\*/
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertNil([safetyPayConfirmationResponse_.additionalInformation operationNumber]);
}

- (void) testParsingRespuestaNumeroOperacionEsVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION></NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.operationNumber length] == 0);
}

- (void) testParsingRespuestaNumeroOperacionTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>   </NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.operationNumber length] == 0);
}

- (void) testParsingRespuestaNumeroOperacionTieneEspaciosEnBlancoIzquierdayDerecha {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
    @"<DIVIMPORTE>S/.</DIVIMPORTE>"\
    @"<CUENTA>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"</CUENTA>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<FECHAHORA>30/12/2012  | 15:30</FECHAHORA>"\
    @"<NUMOPERACION>   123451234566  </NUMOPERACION>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepFourResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayConfirmationResponse_.additionalInformation.operationNumber isEqualToString:@"123451234566"]);
}

#pragma mark TEST METHOD

- (void) doParsingSafetyPayStepFourResponseWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    safetyPayConfirmationResponse_ = [SafetyPayConfirmationResponse alloc];
    safetyPayConfirmationResponse_.additionalInformation.openingTag = @"informacionadicional";
    
    [parser setDelegate: safetyPayConfirmationResponse_];
    
    [safetyPayConfirmationResponse_ parserDidStartDocument: parser];
    
    [parser parse];
}


@end
