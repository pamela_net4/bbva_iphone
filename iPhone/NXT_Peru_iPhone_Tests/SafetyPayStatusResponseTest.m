//
//  SafetyPayStatusResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <CoreFoundation/CoreFoundation.h>
#import <Foundation/Foundation.h>
#import "Updater.h"
#import "HTTPInvoker.h"
#import "Session.h"
#import "XCTestCase+AsyncTesting.h"

@interface SafetyPayStatusResponseTest : XCTestCase <NSURLConnectionDelegate, NSXMLParserDelegate>
{
@private
    NSMutableData *_responseData;
}
@end

@implementation SafetyPayStatusResponseTest


- (void)setUp
{
    [super setUp];
    
    [self login];
}

- (void)tearDown
{
    [super tearDown];
}


- (void) testStatusInfoResponse {
    
    NSURL *url = [NSURL URLWithString: @"https://200.107.164.147:30443/bdmv_pe_web/bdmv_pe_web/OperacionCBTFServlet?"];
    NSString *aContentType = @"application/x-www-form-urlencoded; charset=UTF-8";
    NSString *bodyDataStrig = @"proceso=trans_stp_pr&operacion=stp_ingresa_datos_op&accion=continuar";
    NSData *aBodyData = [bodyDataStrig dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest* URLRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120];
    
    [URLRequest setValue: aContentType forHTTPHeaderField: @"Content-Type"];
    [URLRequest setHTTPBody:aBodyData];
    [URLRequest setHTTPMethod: @"POST"];
    
    NSString *result = nil;
    
    if ((URLRequest != nil) ){
        
		result = @"1989445841";
        
        if ([result length] > 0) {
            
			@try {
                
                [URLRequest setTimeoutInterval:HTTP_REQUEST_TIMEOUT];
                NSURLConnection *URLConnection = [NSURLConnection connectionWithRequest:URLRequest delegate:self];
                [URLConnection start];
                
                [self waitForStatus:XCTAsyncTestCaseStatusSucceeded timeout:60.0];
                
			}
			@catch (id exception) {
                
				result = nil;
                
			}
		}
        
	}
    
}

#pragma mark NSURLConnection Delegates

- (BOOL) connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void) connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"%@", error.description);
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
	if ([response isKindOfClass:[NSHTTPURLResponse class]] == YES) {
        
		NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
		
		NSInteger statusCode = httpResponse.statusCode;
		
		if (statusCode == OK_STATUS_CODE) {
            
			//NSLog(@"Status Code = %i", statusCode);
            
		} else {
            NSString *message = [NSString stringWithFormat:@"Error en la conexión, Status : %i", statusCode];
            //NSLog(@"%@", message);
            XCTAssertTrue(OK_STATUS_CODE != statusCode);
        }
	}
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",dataString);
    [self notify: XCTAsyncTestCaseStatusSucceeded];
    
}

#pragma mark -

#pragma mark NSXMLParser Delegates

- (void) parserDidStartDocument:(NSXMLParser *)parser {

}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
   
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {

}

#pragma mark -


- (void) login
{
    bool clearCookies = true;
    
    NSURL *url = [NSURL URLWithString: @"https://200.107.164.147:30443/slod_pe_web/slod/DFServlet"];
    NSString *aContentType = @"application/x-www-form-urlencoded; charset=UTF-8";
    NSString *bodyDataStrig = @"eai_user=4919149020771371&eai_password=gestion99&origen=contimovil&eai_tipoCP=up&eai_URLDestino=%2Fbdmv_pe_web%2Fbdmv_pe_web%2FLogonOperacionServlet%3Fproceso%3Doperaciones_generales_pr%26operacion%3Dinicio_op%26accion%3Dinicio%26tipo_smartphone%3DP%26indicador%3D2";
    NSData *aBodyData = [bodyDataStrig dataUsingEncoding:NSUTF8StringEncoding];
    
    if (clearCookies) {
        
        NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray *cookies = [storage cookiesForURL:url];
        for (NSHTTPCookie *cookie in cookies) {
            [storage deleteCookie:cookie];
        }
    }
    
    NSMutableURLRequest* URLRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120];
    
    [URLRequest setValue: aContentType forHTTPHeaderField: @"Content-Type"];
    [URLRequest setHTTPBody:aBodyData];
    [URLRequest setHTTPMethod: @"POST"];
    
    NSString *result = nil;
    
    if ((URLRequest != nil) ){
        
		result = @"1989445840";
        
        if ([result length] > 0) {
            
			@try {
                
                [URLRequest setTimeoutInterval:HTTP_REQUEST_TIMEOUT];
                NSURLConnection *URLConnection = [NSURLConnection connectionWithRequest:URLRequest delegate:self];
                [URLConnection start];
                
                [self waitForStatus:XCTAsyncTestCaseStatusSucceeded timeout:60.0];
                
			}
			@catch (id exception) {
                
				result = nil;
                
			}
		}
        
	}
}


@end
