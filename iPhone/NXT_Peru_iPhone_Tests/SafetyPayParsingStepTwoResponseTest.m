//
//  SafetyPayParsingStepTwoResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 20/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SafetyPayTransactionInfoResponse.h"

@interface SafetyPayParsingStepTwoResponseTest : XCTestCase
{
@private
    SafetyPayTransactionInfoResponse *safetyPayTransactionInfoResponse_;
}
@property (nonatomic, readonly, assign) SafetyPayTransactionInfoResponse *safetyPayTransactionInfoResponse;
@end

@implementation SafetyPayParsingStepTwoResponseTest

@synthesize safetyPayTransactionInfoResponse = safetyPayTransactionInfoResponse_;

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}


#pragma mark Numero de Transaccion

- (void) testParsingRespuestaNumeroTransaccionTieneDatos {
    NSString *xmlString =
   
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.transactionNumber isEqualToString:@"4263"]);

}

- (void) testParsingRespuestaNumeroTransaccionEsNulo {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    /*@"<IDENTIFICADOR>4263</IDENTIFICADOR>"\*/
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertNil([safetyPayTransactionInfoResponse_.additionalInformation transactionNumber]);
    
}

- (void) testParsingRespuestaNumeroTransaccionEsVacio {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR></IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([[safetyPayTransactionInfoResponse_.additionalInformation transactionNumber] length] == 0);
    
}

- (void) testParsingRespuestaNumeroTransaccionTieneEspaciosEnBlanco {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>   </IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([[safetyPayTransactionInfoResponse_.additionalInformation transactionNumber] length] == 0);
    
}


- (void) testParsingRespuestaNumeroTransaccionTieneEspaciosEnBlancoIzquierdayDerecha {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>  4263 </IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.transactionNumber isEqualToString:@"4263"]);
    
}

#pragma mark Establecimiento

- (void) testParsingRespuestaEstablecimientoTieneDatos {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.establishment isEqualToString:@"Rosatel"]);
    
}


- (void) testParsingRespuestaEstablecimientoEsNulo {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    /*@"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\*/
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertNil([safetyPayTransactionInfoResponse_.additionalInformation establishment]);
    
}

- (void) testParsingRespuestaEstablecimientoEsVacio {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO></ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([[safetyPayTransactionInfoResponse_.additionalInformation establishment] length] == 0);
    
}

- (void) testParsingRespuestaEstablecimientoTieneEspaciosEnBlanco {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>     </ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([[safetyPayTransactionInfoResponse_.additionalInformation establishment] length] == 0);
    
}

- (void) testParsingRespuestaEstablecimientoTieneEspaciosEnBlancoIzquierdayDerecha {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>     Rosatel     </ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.establishment isEqualToString: @"Rosatel"]);
    
}

#pragma mark Moneda

- (void) testParsingRespuestaMonedaTieneDatos {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.currency isEqualToString: @"SOLES"]);
    
}


- (void) testParsingRespuestaMonedaEsNulo {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    /*@"<MONEDA>SOLES</MONEDA>"\*/
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertNil([safetyPayTransactionInfoResponse_.additionalInformation currency]);
    
}

- (void) testParsingRespuestaMonedaEsVacio {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA></MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([[safetyPayTransactionInfoResponse_.additionalInformation currency] length] == 0);
    
}

- (void) testParsingRespuestaMonedaTieneEspaciosEnBlanco {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>  </MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([[safetyPayTransactionInfoResponse_.additionalInformation currency] length] == 0);
    
}

- (void) testParsingRespuestaMonedaTieneEspaciosEnBlancoIzquierdayDerecha {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA> SOLES </MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.currency isEqualToString: @"SOLES"]);
    
}

#pragma mark Importe

- (void) testParsingRespuestaImporteTieneDatos {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.amount doubleValue] == 450.65);
    
}


- (void) testParsingRespuestaImporteEsNulo {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    /*@"<IMPORTE>450.65</IMPORTE>"\*/
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertNil([safetyPayTransactionInfoResponse_.additionalInformation amount]);
    
}

- (void) testParsingRespuestaImporteEsVacioDebeSerCero {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE></IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayTransactionInfoResponse_.additionalInformation.amount decimalValue];
    NSDecimal dec2 = [@(0) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1, &dec2)==NSOrderedSame);
    
}

- (void) testParsingRespuestaImporteTieneLetrasDebeSerCero {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>abc</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayTransactionInfoResponse_.additionalInformation.amount decimalValue];
    NSDecimal dec2 = [@(0) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1, &dec2)==NSOrderedSame);
    
}

- (void) testParsingRespuestaImporteTieneEspaciosEnBlancoDebeSerCero {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>     </IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayTransactionInfoResponse_.additionalInformation.amount decimalValue];
    NSDecimal dec2 = [@(0) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1, &dec2) == NSOrderedSame);
    
}

- (void) testParsingRespuestaImporteTieneEspaciosEnBlancoIzquierdayDerechaDebeSerCero {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>  450.65   </IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    
    NSDecimal dec1 = [safetyPayTransactionInfoResponse_.additionalInformation.amount decimalValue];
    NSDecimal dec2 = [@(450.65) decimalValue];
    
    XCTAssertTrue(NSDecimalCompare(&dec1, &dec2) == NSOrderedSame);
    
}

#pragma mark Empresa

- (void) testParsingRespuestaEmpresaTieneDatos {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.company isEqualToString:@"SafetyPay"]);
    
}


- (void) testParsingRespuestaEmpresaEsNulo {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    /*@"<EMPRESA>SafetyPay</EMPRESA>"\*/
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertNil([safetyPayTransactionInfoResponse_.additionalInformation company]);
    
}

- (void) testParsingRespuestaEmpresaEsVacio {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([[safetyPayTransactionInfoResponse_.additionalInformation company] length] == 0);
    
}

- (void) testParsingRespuestaEmpresaTieneEspaciosEnBlanco {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>     </EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([[safetyPayTransactionInfoResponse_.additionalInformation company] length] == 0);
    
}

- (void) testParsingRespuestaEmpresaTieneEspaciosEnBlancoIzquierdayDerecha {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>  Rosatel   </EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.company isEqualToString: @"Rosatel"]);
    
}

#pragma mark Servicio

- (void) testParsingRespuestaServicioTieneDatos {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.service isEqualToString:@"Compras por Internet"]);
    
}


- (void) testParsingRespuestaServicioEsNulo {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    /*@"<SERVICIO>Compras por Internet</SERVICIO>"\*/
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertNil([safetyPayTransactionInfoResponse_.additionalInformation service]);
    
}

- (void) testParsingRespuestaServicioEsVacio {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA></EMPRESA>"\
    @"<SERVICIO></SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([[safetyPayTransactionInfoResponse_.additionalInformation service] length] == 0);
    
}

- (void) testParsingRespuestaServicioTieneEspaciosEnBlanco {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>Rosatel</EMPRESA>"\
    @"<SERVICIO>    </SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([[safetyPayTransactionInfoResponse_.additionalInformation service] length] == 0);
    
}

- (void) testParsingRespuestaServicioTieneEspaciosEnBlancoIzquierdayDerecha {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>Rosatel</EMPRESA>"\
    @"<SERVICIO>  Compras por Internet  </SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.service isEqualToString: @"Compras por Internet"]);
    
}

#pragma mark Lista de cuentas

- (void) testParsingRespuestaListaCuentasTieneDatos {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.accountList accountCount] > 0);
    
}

- (void) testParsingRespuestaListaCuentasEsNulo {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    /*@"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\*/
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertNil([safetyPayTransactionInfoResponse_.additionalInformation accountList]);
    
}

- (void) testParsingRespuestaListaCuentasEstaVacia {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    /*@"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\*/
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    XCTAssertTrue([[safetyPayTransactionInfoResponse_.additionalInformation accountList] accountCount] == 0);
    
}

#pragma mark Operadoras

- (void) testParsingRespuestaListaOperadorasVacia {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.alterCarrierList.alterCarrierList count] > 0);
    
}

- (void) testParsingRespuestaListaOperadorasEsNulo {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    /*@"<OPERADORAS>"\
    @"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\
    @"</OPERADORAS>"\*/
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    
    XCTAssertNil(safetyPayTransactionInfoResponse_.additionalInformation.alterCarrierList);
    
}

- (void) testParsingRespuestaListaOperadorasEstaVacia {
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
    @"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
    @"<MONEDA>SOLES</MONEDA>"\
    @"<IMPORTE>450.65</IMPORTE>"\
    @"<EMPRESA>SafetyPay</EMPRESA>"\
    @"<SERVICIO>Compras por Internet</SERVICIO>"\
    @"<CUENTAS>"\
    @"  <E>"\
    @"      <MONEDA>SOLES</MONEDA>"\
    @"      <TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
    @"      <ASUNTO>0011-0130-0119414501</ASUNTO>"\
    @"      <SALDODISPONIBLE>52.06</SALDODISPONIBLE>"\
    @"      <DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"  </E>"\
    @"</CUENTAS>"\
    @"<OPERADORAS>"\
    /*@"  <E>"\
    @"      <CODIGO>CLARO</CODIGO>"\
    @"      <DESCRIPCION>Claro</DESCRIPCION>"\
    @"  </E>"\*/
    @"</OPERADORAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepTwoResponseWithXMLString: xmlString];
    
    XCTAssertTrue([safetyPayTransactionInfoResponse_.additionalInformation.alterCarrierList.alterCarrierList count] == 0);
    
}

#pragma mark TEST METHOD

- (void) doParsingSafetyPayStepTwoResponseWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    safetyPayTransactionInfoResponse_ = [SafetyPayTransactionInfoResponse alloc];
    safetyPayTransactionInfoResponse_.additionalInformation.openingTag = @"MSG-S";
    
    [parser setDelegate: safetyPayTransactionInfoResponse_];
    
    [safetyPayTransactionInfoResponse_ parserDidStartDocument:parser];
    
    [parser parse];
}

@end
