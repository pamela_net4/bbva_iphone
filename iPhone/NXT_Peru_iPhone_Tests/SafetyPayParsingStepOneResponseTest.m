//
//  SafetyPayParsingStepOneResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 20/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SafetyPayStatusResponse.h"


@interface SafetyPayParsingStepOneResponseTest : XCTestCase
{
@private
    SafetyPayStatusResponse *safetyPayStatusResponse_;
}
@property (nonatomic, readwrite, retain) SafetyPayStatusResponse *safetyPayStatusResponse;

@end


@implementation SafetyPayParsingStepOneResponseTest

@synthesize safetyPayStatusResponse = safetyPayStatusResponse_;

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

#pragma mark Mensaje informativo

- (void) testParsingRespuestaMensajeInformativoTieneDatos {
    
    NSString *xmlString =
    @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"\
    @"<MSG-S>"\
    @"<informacionadicional>"\
    @"<estadoactivo>N</estadoactivo>"\
    @"<mensajeinformativo>No se encuentra habilitado SafetyPay en éste momento.</mensajeinformativo>"\
    @"</informacionadicional>"
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepOneResponseWithXMLString:xmlString];
    
    XCTAssertNotNil([safetyPayStatusResponse_.additionalInformation message]);
}

- (void) testParsingRespuestaMensajeInformativoEsNulo {
    
    NSString *xmlString =
    @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"\
    @"<MSG-S>"\
    @"<informacionadicional>"\
    @"<estadoactivo>N</estadoactivo>"\
    /*@"<mensajeinformativo></mensajeinformativo>"\*/
    @"</informacionadicional>"
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepOneResponseWithXMLString:xmlString];
    
    XCTAssertNil([safetyPayStatusResponse_.additionalInformation message]);
}

- (void) testParsingRespuestaMensajeInformativoEsVacio {
    
    NSString *xmlString =
    @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"\
    @"<MSG-S>"\
    @"<informacionadicional>"\
    @"<estadoactivo>N</estadoactivo>"\
    @"<mensajeinformativo></mensajeinformativo>"\
    @"</informacionadicional>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepOneResponseWithXMLString:xmlString];
    
    XCTAssertTrue([[safetyPayStatusResponse_.additionalInformation message] length] == 0);
}


- (void) testParsingRespuestaMensajeInformativoTieneEspaciosEnBlanco {
    
    NSString *xmlString =
    @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"\
    @"<MSG-S>"\
    @"<informacionadicional>"\
    @"<estadoactivo>N</estadoactivo>"\
    @"<mensajeinformativo>      </mensajeinformativo>"\
    @"</informacionadicional>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepOneResponseWithXMLString:xmlString];
    
    XCTAssertTrue([[safetyPayStatusResponse_.additionalInformation message] length] == 0);
}

- (void) testParsingRespuestaMensajeInformativoTieneEspaciosEnBlancoIzquierdayDerecha {
    
    NSString *xmlString =
    @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"\
    @"<MSG-S>"\
    @"<informacionadicional>"\
    @"<estadoactivo>N</estadoactivo>"\
    @"<mensajeinformativo> mensaje de prueba </mensajeinformativo>"\
    @"</informacionadicional>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepOneResponseWithXMLString:xmlString];
    
    XCTAssertTrue([safetyPayStatusResponse_.additionalInformation.message isEqualToString: @"mensaje de prueba" ]);
}

#pragma mark Estado

- (void) testParsingRespuestaEstadoTieneDatos {
    NSString *xmlString =
    @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"\
    @"<MSG-S>"\
    @"<informacionadicional>"\
    @"<estadoactivo>N</estadoactivo>"\
    @"<mensajeinformativo>No se encuentra habilitado SafetyPay en éste momento.</mensajeinformativo>"\
    @"</informacionadicional>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepOneResponseWithXMLString:xmlString];
    
    XCTAssertNotNil([safetyPayStatusResponse_.additionalInformation activeStatus]);
}

- (void) testParsingRespuestaEstadoEsNulo {
    NSString *xmlString =
    @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"\
    @"<MSG-S>"\
    @"<informacionadicional>"\
    /*@"<estadoactivo>N</estadoactivo>"*/
    @"<mensajeinformativo>No se encuentra habilitado SafetyPay en éste momento.</mensajeinformativo>"\
    @"</informacionadicional>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepOneResponseWithXMLString:xmlString];
    
    XCTAssertNil([safetyPayStatusResponse_.additionalInformation activeStatus]);
}

- (void) testParsingRespuestaEstadoVacio {
    NSString *xmlString =
    @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"\
    @"<MSG-S>"\
    @"<informacionadicional>"\
    @"<estadoactivo></estadoactivo>"\
    @"<mensajeinformativo>No se encuentra habilitado SafetyPay en éste momento.</mensajeinformativo>"\
    @"</informacionadicional>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepOneResponseWithXMLString:xmlString];
    
    XCTAssertTrue([[safetyPayStatusResponse_.additionalInformation activeStatus] length] == 0);
}

- (void) testParsingRespuestaEstadoTieneEspaciosEnBlanco {
    NSString *xmlString =
    @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"\
    @"<MSG-S>"\
    @"<informacionadicional>"\
    @"<estadoactivo>    </estadoactivo>"\
    @"<mensajeinformativo>No se encuentra habilitado SafetyPay en éste momento.</mensajeinformativo>"\
    @"</informacionadicional>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepOneResponseWithXMLString:xmlString];
    
    XCTAssertTrue([[safetyPayStatusResponse_.additionalInformation activeStatus] length] == 0);
}

- (void) testParsingRespuestaEstadoTieneEspaciosEnBlancoIzquierdayDerecha {
    NSString *xmlString =
    @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"\
    @"<MSG-S>"\
    @"<informacionadicional>"\
    @"<estadoactivo>  S  </estadoactivo>"\
    @"<mensajeinformativo>No se encuentra habilitado SafetyPay en éste momento.</mensajeinformativo>"\
    @"</informacionadicional>"\
    @"</MSG-S>";
    
    [self doParsingSafetyPayStepOneResponseWithXMLString:xmlString];
    
    XCTAssertTrue([safetyPayStatusResponse_.additionalInformation.activeStatus isEqualToString:@"S"]);
}

- (void) doParsingSafetyPayStepOneResponseWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
        
    safetyPayStatusResponse_ = [SafetyPayStatusResponse alloc];
    safetyPayStatusResponse_.openingTag = @"MSG-S";
        
    [parser setDelegate: safetyPayStatusResponse_];
        
    [safetyPayStatusResponse_ parserDidStartDocument:parser];
        
    [parser parse];
        
    
    
}


@end
