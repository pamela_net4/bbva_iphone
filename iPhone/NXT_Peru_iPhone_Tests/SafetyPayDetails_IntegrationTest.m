//
//  SafetyPayDetails_IntegrationTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "STUB_Updater.h"
#import "Session.h"
#import "XCTestCase+AsyncTesting.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "StringKeys.h"
#import "SafetyPayDetailsAdditionalInformation.h"

@interface SafetyPayDetails_IntegrationTest : XCTestCase {
@private SafetyPayDetailsAdditionalInformation *additionalInformation_;
}

@property (nonatomic, readwrite, retain) SafetyPayDetailsAdditionalInformation *additionalInformation;
@end

@implementation SafetyPayDetails_IntegrationTest

@synthesize additionalInformation = additionalInformation_;

- (void)setUp
{
    [super setUp];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginResponseReceived_STUB:) name:kNotificationLoginEndsTest object:nil];
    
    [[STUB_Updater getInstance] loginWithId: @"4919149020771371" andPassword: @"gestion99"];
    
    [self waitForStatus:XCTAsyncTestCaseStatusSucceeded timeout:80];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(safetyPayDetailsResponseReceived_STUB:) name:kNotificationSafetyPayDetailsResponseReceivedTest object:nil];
    
    [[STUB_Updater getInstance] obtainSafetyPayPaymentDetailsByAccountNumber:@"0011-0123-0200234567" cellphoneNumber1:@"980447066" cellphoneNumber2:@"987654321" carrier1:@"CLARO" carrier2:@"MOVISTAR" email1:@"nombre1.apellido@mail.com.pe" email2:@"nombre2.apellido@mail.com.pe" message:@"SafetyPay Transaction"];
    
    [self waitForStatus:XCTAsyncTestCaseStatusSucceeded timeout:80];
    
    additionalInformation_ = [[Session getInstance] safetyPayDetailsAdditionalInformation];
    
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testSafetyPayDetails_ParsingNoContieneErrores
{
    XCTAssertNil([additionalInformation_ parseError]);
}

- (void)loginResponseReceived_STUB:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationLoginEndsTest object:nil];
    [self notify:XCTAsyncTestCaseStatusSucceeded];
    
}

- (void) safetyPayDetailsResponseReceived_STUB: (NSNotificationCenter *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationSafetyPayDetailsResponseReceivedTest object:nil];
    [self notify:XCTAsyncTestCaseStatusSucceeded];
    
}

@end
