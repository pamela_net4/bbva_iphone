//
//  SafetyPayBuildingStepThreeParametersTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 20/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+URLAndHTMLUtils.h"
#import "Updater.h"

@interface SafetyPayBuildingStepThreeParametersTest : XCTestCase
{
@private
    NSString *cuenta_;
    NSString *celular_;
    NSString *operadora_;
    NSString *mensajeEmail_;
}

@property (nonatomic, readwrite) NSString *cuenta;
@property (nonatomic, readwrite) NSString *celular;
@property (nonatomic, readwrite) NSString *operadora;
@property (nonatomic, readwrite) NSString *mensajeEmail;

@end

@implementation SafetyPayBuildingStepThreeParametersTest

@synthesize cuenta = cuenta_;
@synthesize celular = celular_;
@synthesize operadora = operadora_;
@synthesize mensajeEmail = mensajeEmail_;

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void) testDevuelveUrlConValoresNoNulos {
    cuenta_ = @"0011-00570209584345";
    celular_ = @"980447066";
    operadora_ = @"CLARO";
    mensajeEmail_ = @"Operacion ejecutada.";
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayPaymentInfoWithAccountNumber:cuenta_ cellphone :celular_ carrier:operadora_ emailMessage:mensajeEmail_];
    XCTAssertTrue([url isEqualToString:@"Cuenta=0011-00570209584345&Celular=980447066&Operadora=CLARO&MensajeEmail=Operacion%20ejecutada."]);
}


- (void) testDevuelveUrlParametrosVaciosConValoresNulos {
    cuenta_ = nil;
    celular_ = nil;
    operadora_ = nil;
    mensajeEmail_ = nil;
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayPaymentInfoWithAccountNumber:cuenta_ cellphone:celular_ carrier:operadora_ emailMessage:mensajeEmail_];
    XCTAssertTrue([url isEqualToString:@"Cuenta=&Celular=&Operadora=&MensajeEmail="]);
}

- (void) testDevuelveUrlParametrosVaciosConValoresVacios {
    cuenta_ = @"";
    celular_ = @"";
    operadora_ = @"";
    mensajeEmail_ = @"";
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayPaymentInfoWithAccountNumber:cuenta_ cellphone:celular_ carrier:operadora_ emailMessage:mensajeEmail_];
    XCTAssertTrue([url isEqualToString: @"Cuenta=&Celular=&Operadora=&MensajeEmail="]);
}

- (void) testDevuelveUrlParametrosVaciosConValoresEspaciosEnBlanco {
    cuenta_ = @"    ";
    celular_ = @"   ";
    operadora_ = @"    ";
    mensajeEmail_ = @"  ";
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayPaymentInfoWithAccountNumber:cuenta_ cellphone :celular_ carrier:operadora_ emailMessage:mensajeEmail_];
    XCTAssertTrue([url isEqualToString:@"Cuenta=&Celular=&Operadora=&MensajeEmail="]);
}

- (void) testDevuelveUrlParametrosRecortadosConValoresEspaciosEnBlancoIzquierdayDerecha {
    cuenta_ = @" 0011-00570209584345 ";
    celular_ = @" 980447066 ";
    operadora_ = @" CLARO ";
    mensajeEmail_ = @" Operacion ejecutada. ";
    NSString *url = [[Updater getInstance] returnEscapedParameterSequenceForSafetyPayPaymentInfoWithAccountNumber:cuenta_ cellphone :celular_ carrier:operadora_ emailMessage:mensajeEmail_];
    XCTAssertTrue([url isEqualToString:@"Cuenta=0011-00570209584345&Celular=980447066&Operadora=CLARO&MensajeEmail=Operacion%20ejecutada."]);
}
@end
