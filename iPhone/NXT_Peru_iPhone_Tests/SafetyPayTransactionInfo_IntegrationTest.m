//
//  SafetyPayTransactionInfo_IntegrationTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "STUB_Updater.h"
#import "Session.h"
#import "XCTestCase+AsyncTesting.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "StringKeys.h"
#import "SafetyPayTransactionAdditionalInformation.h"

@interface SafetyPayTransactionInfo_IntegrationTest : XCTestCase {
@private SafetyPayTransactionAdditionalInformation *additionalInformation_;
}

@property (nonatomic, readwrite, retain) SafetyPayTransactionAdditionalInformation *additionalInformation;
@end

@implementation SafetyPayTransactionInfo_IntegrationTest

@synthesize additionalInformation = additionalInformation_;

- (void)setUp
{
    [super setUp];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginResponseReceived_STUB:) name:kNotificationLoginEndsTest object:nil];
    
    [[STUB_Updater getInstance] loginWithId: @"4919149020771371" andPassword: @"gestion99"];
    
    [self waitForStatus:XCTAsyncTestCaseStatusSucceeded timeout:80];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(safetyPayTransaccionInfoResponseReceived_STUB:) name:kNotificationSafetyPayTransaccionInfoResponseReceivedTest object:nil];
    
    [[STUB_Updater getInstance] obtainSafetyPayTransactionInfoByTransactionNumber:@"4263" AndAmount: @"100.90"];
    
    [self waitForStatus:XCTAsyncTestCaseStatusSucceeded timeout:80];
    
    additionalInformation_ = [[Session getInstance] safetyPayTransactionAdditionalInformation];
    
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testSafetyPayTransactionInfoResponse_ParsingNoContieneErrores
{
    //NSLog(@"Parsing error : %@",[additionalInformation_ parseError]);
    XCTAssertNil([additionalInformation_ parseError]);
}

- (void)loginResponseReceived_STUB:(NSNotification *)notification {

    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationLoginEndsTest object:nil];
    [self notify:XCTAsyncTestCaseStatusSucceeded];

    
}

- (void)safetyPayTransaccionInfoResponseReceived_STUB:(NSNotificationCenter *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationSafetyPayTransaccionInfoResponseReceivedTest object:nil];
    [self notify:XCTAsyncTestCaseStatusSucceeded];
    
    
}
@end
