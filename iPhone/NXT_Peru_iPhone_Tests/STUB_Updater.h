/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>
#import "SingletonBase.h"
#import "DownloadListener.h"
#import "Constants.h"

/**
 * Operation types
 */
#define LOGIN                                               1
#define LOGIN_COORDINATE                                    2
#define RETRIEVE_GLOBAL_POSITION                            10
#define RETRIEVE_ACCOUNT_TRANSACTION_LIST                   20
#define RETRIEVE_ACCOUNT_TRANSACTION_DETAIL                 21
#define RETRIEVE_CARD_TRANSACTION_LIST                      30
#define TRANSFER_BETWEEN_ACCOUNTS_STARTUP                   40
#define TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION              41
#define TRANSFER_BETWEEN_ACCOUNTS_RESULT                    42
#define RETRIEVE_RETAINS                                    50
#define SEND_TRANSACTION                                    60
#define SEND_TRANSACTION_CONFIRMATION                       61
#define LOGOUT                                              100
#define CLOSE                                               101
#define TRANSFER_TO_THIRD_ACCOUNTS_STARTUP                  110
#define TRANSFER_TO_THIRD_ACCOUNTS_CONFIRMATION             111
#define TRANSFER_TO_THIRD_ACCOUNTS_RESULT                   112
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_STARTUP		120
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION	121
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_RESULT			122
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_STARTUP        150
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_CONFIRMATION   151
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_RESULT         152
#define OBTAIN_CASH_MOBILE_DETAIL_STARTUP                   190
#define OBTAIN_CASH_MOBILE_DETAIL                           191
#define OBTAIN_CASH_MOBILE_DETAIL_RESEND                    192

#define PAYMENT_SERVICES_LIST_OPERATION_RESULT									130
#define PAYMENT_PUBLIC_SERVICE_INITIAL                                          140
#define PAYMENT_PUBLIC_SERVICE_INITIAL_ELECT_LUZ_DEL_SUR                        141
#define PAYMENT_PUBLIC_SERVICE_INITIAL_ELECT_EDELNOR                            142
#define PAYMENT_PUBLIC_SERVICE_INITIAL_ELECT_EDELNOR                            142
#define PAYMENT_PUBLIC_SERVICE_INITIAL_WATER_SEDAPAL                            143
#define PAYMENT_PUBLIC_SERVICE_INITIAL_PHONE_TELEFONICA                         144
#define PAYMENT_PUBLIC_SERVICE_INITIAL_CELL_MOVISTAR                            145
#define PAYMENT_PUBLIC_SERVICE_INITIAL_CELL_CLARO                               146
#define PAYMENT_PUBLIC_SERVICE_INITIAL_GAS_REPSOL                               147

#define PAYMENT_PUBLIC_SERVICE_DATA                                             160
#define PAYMENT_PUBLIC_SERVICE_DATA_ELECT_LUZ_DEL_SUR                           161
#define PAYMENT_PUBLIC_SERVICE_DATA_ELECT_EDELNOR                               162
#define PAYMENT_PUBLIC_SERVICE_DATA_WATER_SEDAPAL                               163
#define PAYMENT_PUBLIC_SERVICE_DATA_PHONE_TELEFONICA                            164
#define PAYMENT_PUBLIC_SERVICE_DATA_CELL_MOVISTAR                               165
#define PAYMENT_PUBLIC_SERVICE_DATA_CELL_CLARO                                  166

#define PAYMENT_PUBLIC_SERVICE_CONF                                             180
#define PAYMENT_PUBLIC_SERVICE_CONF_ELECT_LUZ_DEL_SUR                           181
#define PAYMENT_PUBLIC_SERVICE_CONF_ELECT_EDELNOR                               182
#define PAYMENT_PUBLIC_SERVICE_CONF_WATER_SEDAPAL                               183
#define PAYMENT_PUBLIC_SERVICE_CONF_PHONE_TELEFONICA                            184
#define PAYMENT_PUBLIC_SERVICE_CONF_CELL_MOVISTAR                               185
#define PAYMENT_PUBLIC_SERVICE_CONF_CELL_CLARO                                  186

#define PAYMENT_PUBLIC_SERVICE_SUCCESS                                          200
#define PAYMENT_PUBLIC_SERVICE_SUCCESS_ELECT_LUZ_DEL_SUR                        201
#define PAYMENT_PUBLIC_SERVICE_SUCCESS_ELECT_EDELNOR                            202
#define PAYMENT_PUBLIC_SERVICE_SUCCESS_WATER_SEDAPAL                            203
#define PAYMENT_PUBLIC_SERVICE_SUCCESS_PHONE_TELEFONICA                         204
#define PAYMENT_PUBLIC_SERVICE_SUCCESS_CELL_MOVISTAR                            205
#define PAYMENT_PUBLIC_SERVICE_SUCCESS_CELL_CLARO                               206

#define PAYMENT_CARDS_CONT_INITIAL                                              220
#define PAYMENT_CARDS_CONT_OWN_DATA                                             221
#define PAYMENT_CARDS_CONT_OWN_CONF                                             222
#define PAYMENT_CARDS_CONT_OWN_SUCCESS                                          223
#define PAYMENT_CARDS_CONT_THIRD_DATA                                           224
#define PAYMENT_CARDS_CONT_THIRD_CONF                                           225
#define PAYMENT_CARDS_CONT_THIRD_SUCCESS                                        226

#define PAYMENT_CARDS_OTHER_BANK_INITIAL                                        230
#define PAYMENT_CARDS_OTHER_BANK_DATA                                           231
#define PAYMENT_CARDS_OTHER_BANK_CONF                                           232
#define PAYMENT_CARDS_OTHER_BANK_SUCCESS                                        233

#define PAYMENT_RECHARGE_DATA                                                   240
#define PAYMENT_RECHARGE_CONF                                                   241
#define PAYMENT_RECHARGE_MOVISTAR_SUCCESS                                       242
#define PAYMENT_RECHARGE_CLARO_SUCCESS                                          243

#define PAYMENT_INSTITUTIONS_LIST_OPERATION_RESULT								250
#define PAYMENT_INSTITUTIONS_SEARCH_LIST_RESULT                                 251
#define PAYMENT_INSTITUTIONS_DETAIL                                             252
#define PAYMENT_INSTITUTIONS_PENDING_PAYS_DETAIL                                253
#define PAYMENT_INSTITUTIONS_CONFIRMATION_PAY_DETAIL                            254
#define PAYMENT_INSTITUTIONS_SUCCESS_PAYS_DETAIL                                255

#define RETRIEVE_SAFETYPAY_STATUS_INFO                                          256
#define RETRIEVE_SAFETYPAY_TRANSACTION_INFO                                     257
#define RETRIEVE_SAFETYPAY_PAYMENT_DETAILS                                      258
#define RETRIEVE_SAFETYPAY_PAYMENT_CONFIRM                                      259

//Forward declarations
@class Stock;


/**
 * This class is responsible of the updating of the model
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface STUB_Updater : SingletonBase <DownloadListener> {
    
@private
	
	/**
	 * Dictionary of active operations
	 */
	NSMutableDictionary* activeOperations_;
	
}

/**
 * Returns the singleton only instance
 *
 * @result The singleton only instance
 */
+ (STUB_Updater *)getInstance;

/**
 * Cancels a download defined by its download file
 *
 * @param aDownloadId The download identification
 */
- (void)cancelDownload: (NSString*) aDownloadId;

/**
 * Cancels all active downloads
 */
- (void)cancelAllDownloads;

/**
 * Returns the number of active downloads
 */
- (NSInteger)numberOfActiveDownloads;

/**
 * Login
 *
 * @param identification the user identification
 * @param password the password
 */
- (void)loginWithId:(NSString *)identification andPassword:(NSString *)password; 

/**
 * Login coordinate
 *
 * @param coordinate the coordinate
 */
- (void)loginWithCoordinate:(NSString *)coordinate; 

/**
 * Obtains the global position
 */
- (void)obtainGlobalPosition;

/**
 * Obtains the account transactions without filters
 *
 * @param anAccountNumber The account number to obtain the transactions from
 */
- (void)obtainAccountTransactionsForAccountNumber:(NSString *)anAccountNumber;

/*
 * Obtains the account transactions detail for a transaction number
 *
 * @param anAccountNumber The account number to obtain the transactions from
 * @param number The transaction number
 */
- (void)obtainAccountTransactionDetailForAccountNumber:(NSString *)anAccountNumber 
                                     transactionNumber:(NSString *)number;

/**
 * Obtains the card transactions
 *
 * @param aCardNumber The card number identifying it inside the list
 */
- (void)obtainCardTransactionsForCardNumber:(NSString *)aCardNumber;

/**
 * Invoke transfer between accounts startup
 */
- (void)transferBetweenAccountsStartup;

/**
 * Invoke transfer between accounts confirmation
 *
 * @param fromAccountNumber from account number
 * @param fromAccountType from account type
 * @param fromCurrency from currency
 * @param fromType from type
 * @param fromIndex from index
 * @param toAccountNumber to account number
 * @param toAccountType to account type
 * @param toCurrency to currency
 * @param toType to type
 * @param toIndex to index
 * @param amount the amount
 * @param currency the currency
 * @param subject the subject
 */
- (void)transferBetweenAccountsConfirmationFromAccountNumber:(NSString *)fromAccountNumber 
                                             fromAccountType:(NSString *)fromAccountType 
                                                fromCurrency:(NSString *)fromCurrency
                                                    fromType:(NSString *)fromType
                                                   fromIndex:(NSString *)fromIndex
                                             toAccountNumber:(NSString *)toAccountNumber 
                                               toAccountType:(NSString *)toAccountType 
                                                  toCurrency:(NSString *)toCurrency
                                                      toType:(NSString *)toType
                                                     toIndex:(NSString *)toIndex
                                                      amount:(NSString *)amount
                                                    currency:(NSString *)currency
                                                  andSubject:(NSString *)subject;
	
/**
 * Invoke transfer between accounts result
 */
- (void)transferBetweenAccountsResult;
	
/**
 * Invoke transfer to third accounts startup
 */
- (void)transferToThirdAccountsStartup;


/**
 * Invoke transfer to third accounts confirmation
 *
 * @param fromAccountType from account type
 * @param fromCurrency from currency
 * @param fromIndex from index
 * @param toBranch to branch number
 * @param toAccount to account number
 * @param amount the amount
 * @param currency the currency
 * @param reference reference
 * @param anEmail1 to send
 * @param anEmail2 to send
 * @param aPhone1 to send 
 * @param aCarrier1 carrier of the first phone
 * @param aPhone2 to send
 * @param aCarrier2 carrier of the second phone
 * @param message the message
 */
- (void)transferToThirdAccountsConfirmationFromAccountType:(NSString *)fromAccountType 
                                              fromCurrency:(NSString *)fromCurrency
                                                 fromIndex:(NSString *)fromIndex
                                                  toBranch:(NSString *)toBranch 
                                                 toAccount:(NSString *)toAccount 
                                                    amount:(NSString *)amount 
                                                  currency:(NSString *)currency 
                                                 reference:(NSString *)reference
                                                    email1:(NSString *)anEmail1 
                                                    email2:(NSString *)anEmail2
                                                    phone1:(NSString *)aPhone1 
                                                  carrier1:(NSString *)aCarrier1 
                                                    phone2:(NSString *)aPhone2
                                                  carrier2:(NSString *)aCarrier2
                                                andMessage:(NSString *)message;



/**
 * Invoke transfer to third accounts result
 *
 * @param secondFactor The second factor key
 */
- (void)transferToThirdAccountsResultFromSecondFactorKey:(NSString *)secondFactor;

/**
 * Invoke transfer to accounts from other bank startup
 */
- (void)transferToAccountsFromOtherBanksStartup;	
	
	
/**
 * Invoke transfer to accounts from other banks confirmation
 *
 * @param fromAccountType from account type
 * @param fromCurrency from currency
 * @param fromIndex from index
 * @param toBank to bank number
 * @param toBranch to branch number
 * @param toAccount to account number
 * @param toCc to bank number
 * @param amount the amount
 * @param currency the currency
 * @param itf the itf
 * @param reference reference
 * @param anEmail1 to send
 * @param anEmail2 to send
 * @param aPhone1 to send 
 * @param aCarrier1 carrier of the first phone
 * @param aPhone2 to send
 * @param aCarrier2 carrier of the second phone
 * @param message the message
 * @param beneficiary the beneficiary
 * @param documentType the document type
 * @param documentNumber the document number
 */
- (void)transferToAccountsFromOtherBanksConfirmationFromAccountType:(NSString *)fromAccountType 
													   fromCurrency:(NSString *)fromCurrency
														  fromIndex:(NSString *)fromIndex
															 toBank:(NSString *)toBank
														   toBranch:(NSString *)toBranch 
														  toAccount:(NSString *)toAccount
															   toCc:(NSString *)toCc
															 amount:(NSString *)amount
														   currency:(NSString *)currency
														  reference:(NSString *)reference
																itf:(BOOL)itf
															 email1:(NSString *)anEmail1 
															 email2:(NSString *)anEmail2
															 phone1:(NSString *)aPhone1 
														   carrier1:(NSString *)aCarrier1 
															 phone2:(NSString *)aPhone2
														   carrier2:(NSString *)aCarrier2
															message:(NSString *)message
														beneficiary:(NSString *)beneficiary
													   documentType:(NSString *)documentType
												  andDocumentNumber:(NSString *)documentNumber;

/**
 * Invoke transfer between accounts result
 *
 * @param secondFactor The second factor key
 */
- (void)transferToAccountsFromOtherBanksResultFromSecondFactorKey:(NSString *)secondFactor;

/*
 * Invoke transfer to accounts With Cash Mobile startup
 */
- (void)transferToAccountsWithCashMobileStartup;

/**
 * Invoke transfer to third accounts confirmation
 *
 * @param fromAccountType from account type
 * @param fromCurrency from currency
 * @param fromIndex from index
 * @param toPhone to Phone number
 * @param amount the amount
 * @param currency the currency
 */
- (void)transferWithCashMobileConfirmationFromAccountType:(NSString *)fromAccountType
                                             fromCurrency:(NSString *)fromCurrency
                                                fromIndex:(NSString *)fromIndex
                                                  toPhone:(NSString *)toPhone
                                                   amount:(NSString *)amount
                                                 currency:(NSString *)currency;

/**
 * Invoke transfer with cash mobile result
 *
 * @param secondKeyFactor The second key factor.
 */
- (void)transferWithCashMobileResultFromSecondKeyFactor:(NSString *)secondKeyFactor;

/*
 * Invoke obtain cash mobile transactions information
 */
- (void)obtainCashMobileTransactionDetail;

/**
 * Invoke transfer with cash mobile result
 *
 * @param operationCode Code of the cash mobile transaction to view.
 */
- (void)obtainCashMobileTransactionDetailWithOperationCode:(NSString *)operationCode;

/*
 * Invoke obtain cash mobile transactions resend
 *
 * @param operationCode Code of the cash mobile transaction to view.
 */
- (void)obtainCashMobileTransactionResend:(NSString *)operationCode;

/**
 * Invoke retains retrieval
 *
 * @param forAccountNumber the account number
 * @param accountType the account type
 * @param balance the balance
 * @param availableBalance the available balance
 * @param currency the currency
 * @param bank the bank number
 * @param office the office number
 * @param controlDigit the control digit
 * @param account the account number
 * @param type the type
 * @param index the index
 */
- (void)retrieveRetainsForAccountNumber:(NSString *)forAccountNumber 
							accountType:(NSString *)accountType 
								balance:(NSString *)balance
					   availableBalance:(NSString *)availableBalance
							   currency:(NSString *)currency
								   bank:(NSString *)bank
								 office:(NSString *)office
						   controlDigit:(NSString *)controlDigit
								account:(NSString *)account
								   type:(NSString *)type
							   andIndex:(NSString *)index;
	

/**
 * Invoke transaction send confirmation
 *
 * @param email1 the first email
 * @param email2 the second email
 * @param carrier1 the first carrier
 * @param carrier2 the second carrier
 * @param phonenumber1 the first phonenumber
 * @param phonenumber2 the second phonenumber
 * @param comments the user comments
 */
- (void)sendTransactionConfirmationForEmail:(NSString *)email1 
									 email2:(NSString *)email2
								   carrier1:(NSString *)carrier1
								   carrier2:(NSString *)carrier2
							   phonenumber1:(NSString *)phonenumber1
							   phonenumber2:(NSString *)phonenumber2
								andComments:(NSString *)comments;


/*
 * Obtains the institutions and copmanies services list
 */
- (void)obtainPaymentInstitutionsAndCompanies;

/*
 * Search a institution by some arguments
 * @param entity the entity code
 * @param searchType the type of the search
 * @param nextMovement the nex movement
 * @param indPag the current page
 * @param searchArg the search argument
 * @param lastDescription the las description recieved
 * @param button the user button
 */
- (void)obtainInstitutionsAndCompaniesForEntity:(NSString *)entity
                                     searchType:(NSString *)searchType
                                   nextMovement:(NSString *)nextMovement
                                         indPag:(NSString *)indPag
                                      searchArg:(NSString *)searchArg
                                      LastDescr:(NSString *)lastDescription
                                         button:(NSString *)button
                                         action:(NSString *)action;

/*
 * Obtains the institution detail
 * @param institutionCode the institution code
 * @param optionType the type operation
 */
- (void)obtainPaymentInstitutionsDetailForCod:(NSString *)institutionCode
                                   optionType:(NSString *)optionType;

/*
 * Obtain the information of the pending payments dor the institution
 * @param code the code of the institution
 * @param arrayLong the longitud of the array
 * @param array the array
 * @param titlesArray collection of titles
 * @param valData Validation Data
 * @param flagModule Flag to know if is a module or not
 */
- (void)obtainInstitutionsAndCompaniesPendingPaysForCode:(NSString *)code
                                               arrayLong:(NSString *)arrayLong
                                                   array:(NSString *)array
                                             titlesArray:(NSString *)titlesArray
                                          validationData:(NSString *)valData
                                              flagModule:(NSString *)flagModule;

/*
 * Obtain the confirmation of the payments for the institution
 * @param code the code of the institution
 * @param arrayLong the longitud of the array
 * @param array the array
 * @param titlesArray collection of titles
 * @param valData Validation Data
 * @param flagModule Flag to know if is a module or not
 */
- (void)obtainInstitutionsAndCompaniesPayConfirmationWithForm:(NSString *)payForm
                                            ownSubjectAccount:(NSString *)ownSubjectAccount
                                               ownSubjectCard:(NSString *)card
                                                    payImport:(NSString *)payImport
                                                 phonenumber1:(NSString *)phonenumber1
                                                 phonenumber2:(NSString *)phonenumber2
                                                       email1:(NSString *)email1
                                                       email2:(NSString *)email2
                                                     carrier1:(NSString *)carrier1
                                                     carrier2:(NSString *)carrier2
                                                  mailMessage:(NSString *)mailMessage
                                                        brand:(NSString *)brand
                                                         type:(int)type;


/*
 * Invoke confirmation payment institutions
 */
- (void)institutionsAndcompaniesConfirmResultFromSecondKeyFactor:(NSString *)secondKeyFactor;

/**
 * Obtains the payments public services list
 */
- (void)obtainPaymentPublicServices;


/**
 * Obtains the public service - electric service - payment inicialization
 *
 * @param company the company
 */
- (void)obtainPaymentPSElectricServicesInitializationForCompany:(NSString *)company;


/**
 * Obtains the public service - electric service - payment data
 *
 * @param company the company
 * @param supplies the supplies
 */
- (void)obtainPaymentPSElectricServicesDataForCompany:(NSString *)company
                                             supplies:(NSString *)supplies;

/**
 * Obtains the public service - electric service - payment confirmation
 *
 * @param company the company
 * @param issue the issue
 * @param idPayment the payment identification
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentPSElectricServicesConfirmationForCompany:(NSString *)company
                                                        issue:(NSString *)issue 
                                                    idPayment:(NSString *)idPayment 
                                                       email1:(NSString *)email1 
                                                       email2:(NSString *)email2 
                                                 phoneNumber1:(NSString *)phoneNumber1 
                                                 phoneNumber2:(NSString *)phoneNumber2 
                                                     carrier1:(NSString *)carrier1 
                                                     carrier2:(NSString *)carrier2 
                                                      message:(NSString *)message;

/**
 * Obtains the public service - electric service - payment success
 *
 * @param company the company
 * @param secondFactorKey The second factor key: sms key or coordinates key
 */
- (void)obtainPaymentPSElectricServicesSuccessForCompany:(NSString *)company
                                         secondFactorKey:(NSString *)secondFactorKey;


/**
 * Obtains the public service - water service - payment inicialization
 *
 * @param company the company
 */
- (void)obtainPaymentPSWaterServicesInitializationForCompany:(NSString *)company;


/**
 * Obtains the public service - water service - payment data
 *
 * @param company the company
 * @param supplies the supplies
 */
- (void)obtainPaymentPSWaterServicesDataForCompany:(NSString *)company
                                          supplies:(NSString *)supplies;

/**
 * Obtains the public service - water service - payment confirmation
 *
 * @param company the company
 * @param issue the issue
 * @param payments the payment identification
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentPSWaterServicesConfirmationForCompany:(NSString *)company
                                                     issue:(NSString *)issue
												idPayments:(NSString *)payments
                                                    email1:(NSString *)email1 
                                                    email2:(NSString *)email2 
                                              phoneNumber1:(NSString *)phoneNumber1 
                                              phoneNumber2:(NSString *)phoneNumber2 
                                                  carrier1:(NSString *)carrier1 
                                                  carrier2:(NSString *)carrier2 
                                                   message:(NSString *)message;

/**
 * Obtains the public service - water service - payment success
 *
 * @param company the company
 * @param secondFactorKey The second factor key: sms key or coordinates key
 */
- (void)obtainPaymentPSWaterServicesSuccessForCompany:(NSString *)company
                                      secondFactorKey:(NSString *)secondFactorKey;




/**
 * Obtains the public service - phone - payment inicialization
 *
 * @param company the company
 */
- (void)obtainPaymentPSPhoneInitializationForCompany:(NSString *)company;

/**
 * Obtains the public service - phone - payment data
 *
 * @param company The company.
 * @param phoneNumber The phone number.
 */
- (void)obtainPaymentPSPhoneServicesDataForCompany:(NSString *)company
                                       phoneNumber:(NSString *)phoneNumber;

/**
 * Obtains the public service - phone - payment confirmation
 *
 * @param company the company
 * @param issue the issue
 * @param idPayment the payment identification
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentPSPhoneConfirmationForCompany:(NSString *)company
                                             issue:(NSString *)issue 
                                         idPayment:(NSString *)idPayment 
                                            email1:(NSString *)email1 
                                            email2:(NSString *)email2 
                                      phoneNumber1:(NSString *)phoneNumber1 
                                      phoneNumber2:(NSString *)phoneNumber2 
                                          carrier1:(NSString *)carrier1 
                                          carrier2:(NSString *)carrier2 
                                           message:(NSString *)message;

/**
 * Obtains the public service - phone - payment success
 *
 * @param company the company
 * @param secondFactorKey The second factor key: sms key or coordinates key
 */
- (void)obtainPaymentPSPhoneSuccessForCompany:(NSString *)company
                              secondFactorKey:(NSString *)secondFactorKey;


/**
 * Obtains the public service - cellular - payment inicialization
 *
 * @param company the company
 */
- (void)obtainPaymentPSCellularInitializationForCompany:(NSString *)company;

/**
 * Obtains the public service - phone - payment data
 *
 * @param company The company
 * @param phoneNumber The phone number
 */
- (void)obtainPaymentPSCellularServicesDataForCompany:(NSString *)company
                                          phoneNumber:(NSString *)phoneNumber;

/**
 * Obtains the public service - Cellular - payment confirmation
 *
 * @param company the company
 * @param issue the issue
 * @param idPayment the payment identification
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentPSCellularConfirmationForCompany:(NSString *)company
                                                issue:(NSString *)issue 
                                            idPayment:(NSString *)idPayment 
                                               email1:(NSString *)email1 
                                               email2:(NSString *)email2 
                                         phoneNumber1:(NSString *)phoneNumber1 
                                         phoneNumber2:(NSString *)phoneNumber2 
                                             carrier1:(NSString *)carrier1 
                                             carrier2:(NSString *)carrier2 
                                              message:(NSString *)message;

/**
 * Obtains the public service - Cellular - payment success
 *
 * @param company the company
 * @param secondFactorKey The second factor key: sms key or coordinates key
 */
- (void)obtainPaymentPSCellularSuccessForCompany:(NSString *)company
                                 secondFactorKey:(NSString *)secondFactorKey;

/**
 * Obtains the public service - cellular - payment inicialization
 *
 * @param company the company
 */
- (void)obtainPaymentPSGasInitializationForCompany:(NSString *)company;

/**
 * Obtains the Continental card payment inicialization
 */
- (void)obtainPaymentCardContinentalInitialization;

/**
 * Obtains the Continental card payment own account data
 *
 * @param issue the issue
 */
- (void)obtainPaymentCardContinentalOwnAccountDataForIssue:(NSString *)issue;

/**
 * Obtains the Continental card payment own account confirmation
 *
 * @param issue the issue
 * @param currency the currency
 * @param amount the amount
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentCardContinentalOwnAccountConfirmationForIssue:(NSString *)issue 
                                                          currency:(NSString *)currency 
                                                            amount:(NSString *)amount 
                                                            email1:(NSString *)email1 
                                                            email2:(NSString *)email2 
                                                      phoneNumber1:(NSString *)phoneNumber1 
                                                      phoneNumber2:(NSString *)phoneNumber2 
                                                          carrier1:(NSString *)carrier1 
                                                          carrier2:(NSString *)carrier2 
                                                           message:(NSString *)message;

/**
 * Obtains the Continental card payment own account success
 *
 */
- (void)obtainPaymentCardContinentalOwnAccountSuccess;

/**
 * Obtains the Continental card payment third account data
 *
 * @param issue the issue
 */
- (void)obtainPaymentCardContinentalThirdAccountDataForIssue:(NSString *)issue;

/**
 * Obtains the Continental card payment third account confirmation
 *
 * @param issue the issue
 * @param currency the currency
 * @param amount the amount
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentCardContinentalThirdAccountConfirmationForIssue:(NSString *)issue 
                                                            currency:(NSString *)currency 
                                                              amount:(NSString *)amount 
                                                              email1:(NSString *)email1 
                                                              email2:(NSString *)email2 
                                                        phoneNumber1:(NSString *)phoneNumber1 
                                                        phoneNumber2:(NSString *)phoneNumber2 
                                                            carrier1:(NSString *)carrier1 
                                                            carrier2:(NSString *)carrier2 
                                                             message:(NSString *)message;

/**
 * Obtains the Continental card payment third account success
 *
 * @param secondFactorKey The second factor key
 */
- (void)obtainPaymentCardContinentalThirdAccountSuccessForSecondFactorKey:(NSString *)secondFactorKey;

/**
 * Obtains the Other banks card payment inicialization
 */
- (void)obtainPaymentCardOtherBanksInitialization;

/**
 * Obtains the other banks card payment data
 *
 * @param aClass A class
 * @param destBank The destination bank
 * @param accountNumber The account number
 */
- (void)obtainPaymentCardOtherBankDataForClass:(NSString *)aClass 
                                      destBank:(NSString *)destBank 
                                 accountNumber:(NSString *)accountNumber;

/**
 * Obtains the other banks card payment confirmation
 *
 * @param locality the locality
 * @param issue the issue
 * @param currency the currency
 * @param amount the amount
 * @param beneficiary the beneficiary
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentCardCardOtherBankConfirmationForLocality:(NSString *)locality
                                                        issue:(NSString *)issue 
                                                     currency:(NSString *)currency 
                                                       amount:(NSString *)amount 
                                                  beneficiary:(NSString *)beneficiary 
                                                       email1:(NSString *)email1 
                                                       email2:(NSString *)email2 
                                                 phoneNumber1:(NSString *)phoneNumber1 
                                                 phoneNumber2:(NSString *)phoneNumber2 
                                                     carrier1:(NSString *)carrier1 
                                                     carrier2:(NSString *)carrier2 
                                                      message:(NSString *)message;

/**
 * Obtains the other banks card payment success
 *
 * @param secondFactorKey The second factor key: sms key or coordinates key
 */
- (void)obtainPaymentCardOtherBankSuccessForSecondFactorKey:(NSString *)secondFactorKey;

/**
 * Obtains the recharge payment data
 *
 * @param company the company
 */
- (void)obtainPaymentRechargeDataForCompany:(NSString *)company;

/**
 * Obtains the recharge payment data
 *
 * @param phoneNumber the phoneNumber
 * @param issue the issue
 * @param amount the amount
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentRechargeConfirmationForPhoneNumber:(NSString *)phoneNumber
                                                  issue:(NSString *)issue 
                                                 amount:(NSString *)amount 
                                                 email1:(NSString *)email1 
                                                 email2:(NSString *)email2 
                                           phoneNumber1:(NSString *)phoneNumber1 
                                           phoneNumber2:(NSString *)phoneNumber2 
                                               carrier1:(NSString *)carrier1 
                                               carrier2:(NSString *)carrier2 
                                                message:(NSString *)message;


/**
 * Obtains the recharge payment success of Movistar or Claro
 *
 * @param secondFactorKey The second factor key: sms key or coordinates key
 * @param carrier the carrier to get information.
 */
- (void)obtainPaymentRechargeSuccessForSecondFactorKey:(NSString *)secondFactorKey
                                               carrier:(NSString *)carrier;

#pragma mark -
#pragma mark SafetyPay methods

- (void) obtainSafetyPayStatusInfo ;

- (void) obtainSafetyPayTransactionInfoByTransactionNumber: (NSString *) transactionNumber AndAmount :(NSString *) amount;

- (void) obtainSafetyPayPaymentDetailsByAccountNumber: (NSString *) accountNumber
                                    cellphoneNumber1 : (NSString *) cellphoneNumber1
                                    cellphoneNumber2 : (NSString *) cellphoneNumber2
                                            carrier1 : (NSString *) carrier1
                                            carrier2 : (NSString *) carrier2
                                              email1 : (NSString *) email1
                                              email2 : (NSString *) email2
                                             message : (NSString *) message;

- (void) obtainSafetyPaySuccessFromConfirmPaymentWithSecondFactor : (NSString *) secondFactor;

#pragma mark -

/**
 * Close
 */
- (void)close;

/**
 * Logout
 */
- (void)logout;


- (NSString *) returnEscapedParameterSequenceForSafetyPayTransactionInfoWithTransactionNumber : (NSString *) transactionNumber AndAmount : (NSString *) amount;

- (NSString *) returnEscapedParameterSequenceForSafetyPayPaymentInfoWithAccountNumber:(NSString *) accountNumber
                                                                             email1:(NSString *)email1
                                                                             email2:(NSString *)email2
                                                                       phoneNumber1:(NSString *)phoneNumber1
                                                                       phoneNumber2:(NSString *)phoneNumber2
                                                                           carrier1:(NSString *)carrier1
                                                                           carrier2:(NSString *)carrier2
                                                                            message:(NSString *)message;

- (NSString *) returnEscapedParameterSequenceForSafetyPayPaymentConfirmationWithSecondFactor :(NSString *) secondFactor;
@end
