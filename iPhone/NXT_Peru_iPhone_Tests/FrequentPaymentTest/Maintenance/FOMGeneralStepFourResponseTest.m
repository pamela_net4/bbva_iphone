//
//  FOMGeneralStepFourResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FOMGeneralStepFourResponse.h"

@interface FOMGeneralStepFourResponseTest : XCTestCase
{
    @private
    FOMGeneralStepFourResponse *fomGeneralStepFourResponse_;
}
@end

@implementation FOMGeneralStepFourResponseTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark Test Operacion

- (void) testParsingRespuestaOperacionTieneDatos {
    
    NSString *xmlString =

    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACION>Actualizacion de Pago Frecuente</OPERACION>"\
    @"<ALIAS>Pepe</ALIAS>"\
    @"<DIAAVISO>1</DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>1</CODIGO>"\
    @"<DESCRIPCION>Canal 1</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<FECHAHORA>10-10-2010</FECHAHORA>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOMGeneralStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fomGeneralStepFourResponse_.operation isEqualToString: @"Actualizacion de Pago Frecuente" ]);
    
}

- (void) testParsingRespuestaOperacionNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    /*@"<OPERACION>Actualizaciónn de Pago Frecuente</OPERACION>"\*/
    @"<FECHAHORA>23/12/2013  13:10:00</FECHAHORA>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOMGeneralStepOneResponseWithXMLString: xmlString];
    XCTAssertNil(fomGeneralStepFourResponse_.operation);
    
}

- (void) testParsingRespuestaOperacionVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACION></OPERACION>"\
    @"<FECHAHORA>23/12/2013  13:10:00</FECHAHORA>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOMGeneralStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fomGeneralStepFourResponse_.operation isEqualToString: @"" ]);
    
}


#pragma mark TEST METHOD

- (void) doParsingFOMGeneralStepOneResponseWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    fomGeneralStepFourResponse_ = [FOMGeneralStepFourResponse alloc];
    fomGeneralStepFourResponse_.openingTag = @"informacionadicional";
    
    [parser setDelegate: fomGeneralStepFourResponse_];
    
    [fomGeneralStepFourResponse_ parserDidStartDocument: parser];
    
    [parser parse];
}





@end
