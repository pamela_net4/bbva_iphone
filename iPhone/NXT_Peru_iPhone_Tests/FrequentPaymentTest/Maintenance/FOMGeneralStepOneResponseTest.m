//
//  FOMGeneralStepOneResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FOMGeneralStepOneResponse.h"

@interface FOMGeneralStepOneResponseTest : XCTestCase
{
    @private
    FOMGeneralStepOneResponse *fomGeneralStepOneResponse_;
}
@end

@implementation FOMGeneralStepOneResponseTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}
#pragma mark Test Coordenadas

- (void) testParsingRespuestaCoordenadasTieneDatos {
    
    NSString *xmlString =
    

    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<COORDENADA>A-10</COORDENADA>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOMGeneralStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fomGeneralStepOneResponse_.coordinate isEqualToString: @"A-10" ]);
    
}

- (void) testParsingRespuestaCoordenadasNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    /*@"<COORDENADA></COORDENADA>"\*/
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOMGeneralStepOneResponseWithXMLString: xmlString];
    XCTAssertNil(fomGeneralStepOneResponse_.coordinate);
    
}

- (void) testParsingRespuestaCoordenadasVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<COORDENADA></COORDENADA>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOMGeneralStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fomGeneralStepOneResponse_.coordinate isEqualToString: @"" ]);
    
}


#pragma mark TEST METHOD

- (void) doParsingFOMGeneralStepOneResponseWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    fomGeneralStepOneResponse_ = [FOMGeneralStepOneResponse alloc];
    fomGeneralStepOneResponse_.openingTag = @"informacionadicional";
    
    [parser setDelegate: fomGeneralStepOneResponse_];
    
    [fomGeneralStepOneResponse_ parserDidStartDocument: parser];
    
    [parser parse];
}


@end
