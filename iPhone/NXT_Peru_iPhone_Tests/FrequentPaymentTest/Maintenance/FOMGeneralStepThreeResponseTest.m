//
//  FOMGeneralStepThreeResponseTest.m
//  NXT_Peru_iPhThree
//
//  Created by usuario on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FOMGeneralStepThreeResponse.h"

@interface FOMGeneralStepThreeResponseTest : XCTestCase
{
@private
    FOMGeneralStepThreeResponse *fomGeneralStepThreeResponse_;
}
@end

@implementation FOMGeneralStepThreeResponseTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark Test Operacion

- (void) testParsingRespuestaOperacionTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACION>Test Operacion</OPERACION>"\
    @"<ALIAS></ALIAS>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOMGeneralStepThreeResponseWithXMLString: xmlString];
    XCTAssertTrue([fomGeneralStepThreeResponse_.operation isEqualToString: @"Test Operacion" ]);
    
}

- (void) testParsingRespuestaOperacionNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    /*@"<OPERACION></OPERACION>"\*/
    @"<ALIAS></ALIAS>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOMGeneralStepThreeResponseWithXMLString: xmlString];
    XCTAssertNil(fomGeneralStepThreeResponse_.operation);
    
}

- (void) testParsingRespuestaOperacionVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACION></OPERACION>"\
    @"<ALIAS></ALIAS>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";

    
    [self doParsingFOMGeneralStepThreeResponseWithXMLString: xmlString];
    XCTAssertTrue([fomGeneralStepThreeResponse_.operation isEqualToString: @"" ]);
    
}




#pragma mark TEST METHOD

- (void) doParsingFOMGeneralStepThreeResponseWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    fomGeneralStepThreeResponse_ = [[FOMGeneralStepThreeResponse alloc] init ];
    fomGeneralStepThreeResponse_.openingTag = @"informacionadicional";
    
    [parser setDelegate: fomGeneralStepThreeResponse_];
    
    [fomGeneralStepThreeResponse_ parserDidStartDocument: parser];
    
    [parser parse];
}
@end
