//
//  FORInitialResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/30/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FORInitialResponse.h"
#import "ChannelList.h"
#import "FOPhoneList.h"
#import "FOEmailList.h"

@interface FORInitialResponseTest : XCTestCase
{
@private
    FORInitialResponse *forInitialResponse_;
}
@end

@implementation FORInitialResponseTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}
#pragma mark Test Servicio

- (void) testParsingRespuestaServicioTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO>Pago de tarjetas de otros bancos.</SERVICIO>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>chan1</CODIGO>"\
    @"<DESCRIPCION>channel 1</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<LISTACELULAR>"\
    @"<E>"\
    @"<CELULAR>"\
    @"<NROCELULAR>99999999</NROCELULAR>"\
    @"<OPERADORA>Oscuro</OPERADORA>"\
    @"<CODIGO>phone1</CODIGO>"\
    @"</CELULAR>"\
    @"</E>"\
    @"</LISTACELULAR>"\
    @"<LISTACORREO>"\
    @"<E>"\
    @"<CORREO>"\
    @"<EMAIL>correo@mail.com</EMAIL>"\
    @"<CODIGO>email1</CODIGO>"\
    @"</CORREO>"\
    @"</E>"\
    @"</LISTACORREO>"\
    @"<DISCLAIMERCELULARES>esto no es un celular</DISCLAIMERCELULARES>"\
    @"<DISCLAIMERCORREOS>esto no es un correo</DISCLAIMERCORREOS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFORInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([forInitialResponse_.service isEqualToString: @"Pago de tarjetas de otros bancos." ]);
    
}

- (void) testParsingRespuestaServicioNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<LISTACELULAR>"\
    @"<E>"\
    @"<CELULAR>"\
    @"<NROCELULAR></NROCELULAR>"\
    @"<OPERADORA></OPERADORA>"\
    @"<CODIGO></CODIGO>"\
    @"</CELULAR>"\
    @"</E>"\
    @"</LISTACELULAR>"\
    @"<LISTACORREO>"\
    @"<E>"\
    @"<CORREO>"\
    @"<EMAIL></EMAIL>"\
    @"<CODIGO></CODIGO>"\
    @"</CORREO>"\
    @"</E>"\
    @"</LISTACORREO>"\
    @"<DISCLAIMERCELULARES></DISCLAIMERCELULARES>"\
    @"<DISCLAIMERCORREOS></DISCLAIMERCORREOS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFORInitialResponseWithXMLString: xmlString];
    XCTAssertNil(forInitialResponse_.service);
    
}

- (void) testParsingRespuestaServicioVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>chan1</CODIGO>"\
    @"<DESCRIPCION>channel 1</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<LISTACELULAR>"\
    @"<E>"\
    @"<CELULAR>"\
    @"<NROCELULAR>99999999</NROCELULAR>"\
    @"<OPERADORA>Oscuro</OPERADORA>"\
    @"<CODIGO>phone1</CODIGO>"\
    @"</CELULAR>"\
    @"</E>"\
    @"</LISTACELULAR>"\
    @"<LISTACORREO>"\
    @"<E>"\
    @"<CORREO>"\
    @"<EMAIL>correo@mail.com</EMAIL>"\
    @"<CODIGO>email1</CODIGO>"\
    @"</CORREO>"\
    @"</E>"\
    @"</LISTACORREO>"\
    @"<DISCLAIMERCELULARES>esto no es un celular</DISCLAIMERCELULARES>"\
    @"<DISCLAIMERCORREOS>esto no es un correo</DISCLAIMERCORREOS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFORInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([forInitialResponse_.service isEqualToString: @"" ]);
    
}




#pragma mark TEST METHOD

- (void) doParsingFORInitialResponseWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    forInitialResponse_ = [FORInitialResponse alloc];
    forInitialResponse_.openingTag = @"informacionadicional";
    
    [parser setDelegate: forInitialResponse_];
    
    [forInitialResponse_ parserDidStartDocument: parser];
    
    [parser parse];
}

@end
