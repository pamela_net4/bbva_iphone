//
//  FOInitialResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 29/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FOInitialResponse.h"
#import "ChannelList.h"
#import "Channel.h"

@interface FOInitialResponseTest : XCTestCase
{
@private
    FOInitialResponse *foInitialResponse_;
}
@end

@implementation FOInitialResponseTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark Test Alias

- (void) testParsingRespuestaAliasTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS>Power</ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.nickname isEqualToString: @"Power" ]);
    
}

- (void) testParsingRespuestaAliasNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    /*@"<ALIAS></ALIAS>"\*/
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertNil(foInitialResponse_.nickname);
    
}

- (void) testParsingRespuestaAliasVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.nickname isEqualToString: @"" ]);
    
}

#pragma mark Test Alias Corto

- (void) testParsingRespuestaAliasCortoTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO>Power</ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.shortNickname isEqualToString: @"Power" ]);
    
}

- (void) testParsingRespuestaAliasCortoNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    /*@"<ALIASCORTO></ALIASCORTO>"\*/
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertNil(foInitialResponse_.shortNickname);
    
}

- (void) testParsingRespuestaAliasCortoVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.shortNickname isEqualToString: @"" ]);
    
}

#pragma mark Test Servicio

- (void) testParsingRespuestaServicioTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO>Pago de tarjetas de otros bancos.</SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.service isEqualToString: @"Pago de tarjetas de otros bancos." ]);
    
}

- (void) testParsingRespuestaServicioNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    /*@"<SERVICIO></SERVICIO>"\*/
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertNil(foInitialResponse_.service);
    
}

- (void) testParsingRespuestaServicioVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.service isEqualToString: @"" ]);
    
}


#pragma mark Test Dia aviso

- (void) testParsingRespuestaDiaAvisoTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO>23</DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.dayNotice isEqualToString: @"23" ]);
    
}

- (void) testParsingRespuestaDiaAvisoNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    /*@"<DIAAVISO></DIAAVISO>"\*/
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertNil(foInitialResponse_.dayNotice);
    
}

- (void) testParsingRespuestaDiaAvisoVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.dayNotice isEqualToString: @"" ]);
    
}


#pragma mark Test SMS Móvil

- (void) testParsingRespuestaSMSMovilTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL>949126743</SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.smsMobile isEqualToString: @"949126743" ]);
    
}

- (void) testParsingRespuestaSMSMovilNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    /*@"<SMSMOVIL></SMSMOVIL>"\*/
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertNil(foInitialResponse_.smsMobile);
    
}

- (void) testParsingRespuestaSMSMovilVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.smsMobile isEqualToString: @"" ]);
    
}


#pragma mark Test SMS Email

- (void) testParsingRespuestaSMSEmailTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL>Prueba_123@Test.com</SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.smsEmail isEqualToString: @"Prueba_123@Test.com" ]);
    
}

- (void) testParsingRespuestaSMSEmailNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    /*@"<SMSEMAIL></SMSEMAIL>"\*/
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertNil(foInitialResponse_.smsEmail);
    
}

- (void) testParsingRespuestaSMSEmailVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.smsEmail isEqualToString: @"" ]);
    
}


#pragma mark Test Canales

- (void) testParsingRespuestaCanalesTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL>@</SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.channelList channelCount] > 0 );
    
}

- (void) testParsingRespuestaCanalesNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    /*@"<CANALES>"\
     @"<E>"\
     @"<CANAL>"\
     @"<CODIGO></CODIGO>"\
     @"<DESCRIPCION></DESCRIPCION>"\
     @"</CANAL>"\
     @"</E>"\
     @"</CANALES>"\*/
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertNil(foInitialResponse_.channelList);
    
}

- (void) testParsingRespuestaCanalesVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    /*@"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\*/
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([foInitialResponse_.channelList channelCount]==0);
    
}


#pragma mark Test Canal

- (void) testParsingRespuestaCanalTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL>@</SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([((Channel *)[foInitialResponse_.channelList chanelAtPosition:0]).code isEqualToString:@"001"] );
    
}

- (void) testParsingRespuestaCanalNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    /* @"<E>"\
     @"<CANAL>"\
     @"<CODIGO></CODIGO>"\
     @"<DESCRIPCION></DESCRIPCION>"\
     @"</CANAL>"\
     @"</E>"\*/
     @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertNil(((Channel *)[foInitialResponse_.channelList chanelAtPosition:0]));
    
}

- (void) testParsingRespuestaCanalVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<SERVICIO></SERVICIO>"\
    @"<ALIAS></ALIAS>"\
    @"<ALIASCORTO></ALIASCORTO>"\
    @"<DIAAVISO></DIAAVISO>"\
    @"<SMSMOVIL></SMSMOVIL>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"<SMSEMAIL></SMSEMAIL>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOInitialResponseWithXMLString: xmlString];
    XCTAssertTrue([((Channel *)[foInitialResponse_.channelList chanelAtPosition:0]).code isEqualToString:@""]);
    
}




#pragma mark TEST METHOD

- (void) doParsingFOInitialResponseWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    foInitialResponse_ = [FOInitialResponse alloc];
    foInitialResponse_.openingTag = @"informacionadicional";
    
    [parser setDelegate: foInitialResponse_];
    
    [foInitialResponse_ parserDidStartDocument: parser];
    
    [parser parse];
}

@end
