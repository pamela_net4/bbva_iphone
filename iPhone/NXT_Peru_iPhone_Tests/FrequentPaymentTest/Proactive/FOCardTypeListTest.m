//
//  CardTypeListTest.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FOCardTypeList.h"
#import "FOCardType.h"

@interface FOCardTypeListTest : XCTestCase
{
    @private
    FOCardTypeList *cardTypeList_;
    
}
@end

@implementation FOCardTypeListTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}
#pragma mark Test TipoTarjetas

- (void) testParsingRespuestaTipoTarjetasTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<TIPOSTARJETA>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</TIPOSTARJETA>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingCardTypeListWithXMLString: xmlString];
    XCTAssertTrue([cardTypeList_ foCardTypeCount] > 0 );
    
}

- (void) testParsingRespuestaTipoTarjetasNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    /*@"<TIPOSTARJETA>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</TIPOSTARJETA>"\*/
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingCardTypeListWithXMLString: xmlString];
    XCTAssertNotNil(cardTypeList_);
    
}

- (void) testParsingRespuestaTipoTarjetasVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<TIPOSTARJETA>"\
    /* @"<E>"\
     @"<CODIGO>001</CODIGO>"\
     @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
     @"</E>"\*/
     @"</TIPOSTARJETA>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingCardTypeListWithXMLString: xmlString];
    XCTAssertTrue([cardTypeList_ foCardTypeCount]==0);
    
}


#pragma mark Test TipoTarjeta

- (void) testParsingRespuestaTipoTarjetaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<TIPOSTARJETA>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</TIPOSTARJETA>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingCardTypeListWithXMLString: xmlString];
    XCTAssertTrue([((FOCardType *)[cardTypeList_ foCardTypeAtPosition:0]).code isEqualToString:@"001"] );
    
}

- (void) testParsingRespuestaTipoTarjetaNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<TIPOSTARJETA>"\
    /*@"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\*/
    @"</TIPOSTARJETA>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingCardTypeListWithXMLString: xmlString];
    XCTAssertNil(((FOCardType *)[cardTypeList_ foCardTypeAtPosition:0]));
    
}

- (void) testParsingRespuestaTipoTarjetaVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<TIPOSTARJETA>"\
    @"<E>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</E>"\
    @"</TIPOSTARJETA>"\
    @"</MSG-S>";
    
    [self doParsingCardTypeListWithXMLString: xmlString];
    XCTAssertTrue([((FOCardType *)[cardTypeList_ foCardTypeAtPosition:0]).code isEqualToString:@""]);
    
}




#pragma mark TEST METHOD

- (void) doParsingCardTypeListWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    cardTypeList_ = [FOCardTypeList alloc];
    cardTypeList_.openingTag = @"tipostarjeta";
    
    [parser setDelegate: cardTypeList_];

    [cardTypeList_ parserDidStartDocument: parser];
    
    [parser parse];
}

@end
