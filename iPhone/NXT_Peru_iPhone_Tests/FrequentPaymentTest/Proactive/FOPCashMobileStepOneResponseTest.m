//
//  FOPCashMobileStepOneResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 29/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FOPCashMobileStepOneResponse.h"
#import "ChannelList.h"
#import "Channel.h"
#import "FOAccountList.h"
#import "Account.h"
#import "AlterCarrierList.h"
#import "AlterCarrier.h"

@interface FOPCashMobileStepOneResponseTest : XCTestCase
{
    @private
    FOPCashMobileStepOneResponse *fopCashMobileStepOneResponse_;
}
@end

@implementation FOPCashMobileStepOneResponseTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark Test Cuentas

- (void) testParsingRespuestaCuentasTieneDatos {

    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    @"<E>"\
    @"<SALDODISPONIBLE>10000</SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"</E>"\
    @"</CUENTAS>"\
    @"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>002</CODIGO>"\
    @"<DESCRIPCION>Operador</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fopCashMobileStepOneResponse_.accountList accountCount] > 0 );
    
}

- (void) testParsingRespuestaCuentasNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    /*@"<CUENTAS>"\
    @"<E>"\
    @"<SALDODISPONIBLE>10000</SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"</E>"\
    @"</CUENTAS>"\*/
    @"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertNil(fopCashMobileStepOneResponse_.accountList);
    
}

- (void) testParsingRespuestaCuentasVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    /*@"<E>"\
    @"<SALDODISPONIBLE>10000</SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"</E>"\*/
    @"</CUENTAS>"\
    @"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\
    @"<CANALES>"\
    @"<E>"\
     @"<CANAL>"\
     @"<CODIGO>001</CODIGO>"\
     @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
     @"</CANAL>"\
     @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fopCashMobileStepOneResponse_.accountList accountCount]==0);
    
}


#pragma mark Test Cuenta

- (void) testParsingRespuestaCuentaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    @"<E>"\
    @"<SALDODISPONIBLE>10000</SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"</E>"\
    @"</CUENTAS>"\
    @"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([((Account *)[fopCashMobileStepOneResponse_.accountList accountAtPosition:0]).balanceAvailable isEqualToString:@"10000"] );
    
}

- (void) testParsingRespuestaCuentaNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    /*@"<E>"\
    @"<SALDODISPONIBLE>10000</SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"</E>"\*/
    @"</CUENTAS>"\
    @"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\
    @"<CANALES>"\
    @"<E>"\
     @"<CANAL>"\
     @"<CODIGO>001</CODIGO>"\
     @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
     @"</CANAL>"\
     @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertNil(((Account *)[fopCashMobileStepOneResponse_.accountList accountAtPosition:0]));
    
}

- (void) testParsingRespuestaCuentaVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    @"<E>"\
    @"<SALDODISPONIBLE></SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE></DIVSALDODISPONIBLE>"\
    @"</E>"\
    @"</CUENTAS>"\
    @"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([((Account *)[fopCashMobileStepOneResponse_.accountList accountAtPosition:0]).balanceAvailable isEqualToString:@""]);
    
}

#pragma mark Test Operadores

- (void) testParsingRespuestaOperadoresTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    @"<E>"\
    @"<SALDODISPONIBLE>10000</SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
    @"</E>"\
    @"</CUENTAS>"\
    @"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>002</CODIGO>"\
    @"<DESCRIPCION>Operador</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fopCashMobileStepOneResponse_.carrierList alterCarrierCount] > 0);
}

- (void) testParsingRespuestaOperadoresNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
     @"<E>"\
     @"<SALDODISPONIBLE>10000</SALDODISPONIBLE>"\
     @"<DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
     @"</E>"\
     @"</CUENTAS>"\
    /*@"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\*/
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertNil(fopCashMobileStepOneResponse_.carrierList);
    
}

- (void) testParsingRespuestaOperadoresVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    @"<E>"\
     @"<SALDODISPONIBLE>10000</SALDODISPONIBLE>"\
     @"<DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
     @"</E>"\
    @"</CUENTAS>"\
    @"<OPERADORES>"\
    /*@"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"*/
    @"</OPERADORES>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    NSLog(@"%@",[fopCashMobileStepOneResponse_.carrierList carrierAtPosition:0]);
    XCTAssertTrue([fopCashMobileStepOneResponse_.carrierList alterCarrierCount]==0);
    
}



#pragma mark Test Canales

- (void) testParsingRespuestaCanalesTieneDatos {
    
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    @"<E>"\
    @"<SALDODISPONIBLE>001</SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE>Banca iPad</DIVSALDODISPONIBLE>"\
    @"</E>"\
    @"</CUENTAS>"\
    @"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fopCashMobileStepOneResponse_.channelList channelCount] > 0 );
    
}

- (void) testParsingRespuestaCanalesNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    @"<E>"\
    @"<SALDODISPONIBLE>001</SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE>Banca iPad</DIVSALDODISPONIBLE>"\
    @"</E>"\
    @"</CUENTAS>"\
    @"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\
    /*@"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\*/
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertNil(fopCashMobileStepOneResponse_.channelList);
    
}

- (void) testParsingRespuestaCanalesVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    @"<E>"\
    @"<SALDODISPONIBLE>001</SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE>Banca iPad</DIVSALDODISPONIBLE>"\
    @"</E>"\
    @"</CUENTAS>"\
    @"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\
    @"<CANALES>"\
    /*@"<E>"\
    @"<CANAL>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\*/
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fopCashMobileStepOneResponse_.channelList channelCount]==0);
    
}


#pragma mark Test Canal

- (void) testParsingRespuestaCanalTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    @"<E>"\
    @"<SALDODISPONIBLE>001</SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE>Banca iPad</DIVSALDODISPONIBLE>"\
    @"</E>"\
    @"</CUENTAS>"\
    @"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([((Channel *)[fopCashMobileStepOneResponse_.channelList chanelAtPosition:0]).code isEqualToString:@"001"] );
    
}

- (void) testParsingRespuestaCanalNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    @"<E>"\
    @"<SALDODISPONIBLE>001</SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE>Banca iPad</DIVSALDODISPONIBLE>"\
    @"</E>"\
    @"</CUENTAS>"\
    @"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\
    @"<CANALES>"\
    /*@"<E>"\
    @"<CANAL>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\*/
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertNil(((Channel *)[fopCashMobileStepOneResponse_.channelList chanelAtPosition:0]));
    
}

- (void) testParsingRespuestaCanalVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    @"<E>"\
    @"<SALDODISPONIBLE>001</SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE>Banca iPad</DIVSALDODISPONIBLE>"\
    @"</E>"\
    @"</CUENTAS>"\
    @"<OPERADORES>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</OPERADORES>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPCashMobileStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([((Channel *)[fopCashMobileStepOneResponse_.channelList chanelAtPosition:0]).code isEqualToString:@""]);
    
}




#pragma mark TEST METHOD

- (void) doParsingFOPCashMobileStepOneResponseWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    fopCashMobileStepOneResponse_ = [FOPCashMobileStepOneResponse alloc];
    fopCashMobileStepOneResponse_.openingTag = @"informacionadicional";
    
    [parser setDelegate: fopCashMobileStepOneResponse_];
    
    [fopCashMobileStepOneResponse_ parserDidStartDocument: parser];
    
    [parser parse];
}


@end
