//
//  FOCardListTest.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FOCardList.h"
#import "FOPRechargeGiftCardStepOneResponse.h"

@interface FOCardListTest : XCTestCase
{
@private
    FOCardList *foCardList_;
    FOPRechargeGiftCardStepOneResponse *fopRechargeGiftCardStepOneResponse_;
}
@end

@implementation FOCardListTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark Test Tarjetas

- (void) testParsingRespuestaTarjetasTieneDatos {
    
    NSString *xmlString =
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<CUENTAS>"\
    @"<E>"\
    @"<SALDODISPONIBLE>Saldo</SALDODISPONIBLE>"\
    @"<DIVSALDODISPONIBLE>Divisa</DIVSALDODISPONIBLE>"\
    @"</E>"\
    @"</CUENTAS>"\
    @"<TARJETAS>"\
     @"<E>"\
     @"<NUMEROTARJETA>Test 1</NUMEROTARJETA>"\
     @"<DESCRIPCION>Descrip 1</DESCRIPCION>"\
     @"</E>"\
    @"</TARJETAS>"\
    @"<CANALES>"\
    @"<E>"\
    @"<CANAL>"\
    @"<CODIGO>Test Canal</CODIGO>"\
    @"<DESCRIPCION>Test Descrip Canal</DESCRIPCION>"\
    @"</CANAL>"\
    @"</E>"\
    @"</CANALES>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";

    /*
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<TARJETAS>"\
    @"<E>"\
    @"<NUMEROTARJETA>123456</NUMEROTARJETA>"\
    @"<DESCRIPCION>Test Parse Tarjeta</DESCRIPCION>"\
    @"</E>"\
    @"</TARJETAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";*/
    
    [self doParsingFOCardListResponseWithXMLString: xmlString];
    XCTAssertTrue([fopRechargeGiftCardStepOneResponse_.foCardList foCardCount] > 0 );
    
}

- (void) testParsingRespuestaTarjetasNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    /*@"<TARJETAS>"\
     @"<E>"\
     @"<NUMEROTAREJTA></NUMEROTARJETA>"\
     @"<DESCRIPCION></DESCRIPCION>"\
     @"</E>"\
     @"</TARJETAS>"\*/
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOCardListResponseWithXMLString: xmlString];
    XCTAssertNotNil(foCardList_);
    
}

- (void) testParsingRespuestaTarjetasVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<TARJETAS>"\
   /* @"<E>"\
     @"<NUMEROTARJETA>Test 1</NUMEROTARJETA>"\
     @"<DESCRIPCION>Descrip 1</DESCRIPCION>"\
     @"</E>"\*/
    @"</TARJETAS>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOCardListResponseWithXMLString: xmlString];
    XCTAssertTrue([foCardList_ foCardCount]==0);
    
}


#pragma mark TEST METHOD

- (void) doParsingFOCardListResponseWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    fopRechargeGiftCardStepOneResponse_ = [FOPRechargeGiftCardStepOneResponse alloc];
    fopRechargeGiftCardStepOneResponse_.openingTag = @"informacionadicional";
    [parser setDelegate: fopRechargeGiftCardStepOneResponse_];
    [fopRechargeGiftCardStepOneResponse_ parserDidStartDocument:parser];
    /*
    foCardList_ = [[FOCardList alloc] init];
    foCardList_.openingTag = @"informacionadicional";
    
    [parser setDelegate: foCardList_];
    
    [foCardList_ parserDidStartDocument: parser];
    */
    [parser parse];
}

@end
