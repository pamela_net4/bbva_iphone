//
//  FODestinationBankListTest.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FODestinationBankList.h"
#import "FODestinationBank.h"

@interface FODestinationBankListTest : XCTestCase
{
    @private
    FODestinationBankList *foDestinationBankList_;
}
@end

@implementation FODestinationBankListTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

#pragma mark Test TipoTarjetas

- (void) testParsingRespuestaTipoTarjetasTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<BANCOSDESTINO>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</BANCOSDESTINO>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingCardTypeListWithXMLString: xmlString];
    XCTAssertTrue([foDestinationBankList_ foDestinationBankCount] > 0 );
    
}

- (void) testParsingRespuestaTipoTarjetasNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    /*@"<BANCOSDESTINO>"\
     @"<E>"\
     @"<CODIGO>001</CODIGO>"\
     @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
     @"</E>"\
     @"</BANCOSDESTINO>"\*/
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingCardTypeListWithXMLString: xmlString];
    XCTAssertNotNil(foDestinationBankList_);
    
}

- (void) testParsingRespuestaTipoTarjetasVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<BANCOSDESTINO>"\
    /* @"<E>"\
     @"<CODIGO>001</CODIGO>"\
     @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
     @"</E>"\*/
    @"</BANCOSDESTINO>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingCardTypeListWithXMLString: xmlString];
    XCTAssertTrue([foDestinationBankList_ foDestinationBankCount]==0);
    
}


#pragma mark Test TipoTarjeta

- (void) testParsingRespuestaTipoTarjetaTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<BANCOSDESTINO>"\
    @"<E>"\
    @"<CODIGO>001</CODIGO>"\
    @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
    @"</E>"\
    @"</BANCOSDESTINO>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingCardTypeListWithXMLString: xmlString];
    XCTAssertTrue([((FODestinationBank *)[foDestinationBankList_ foDestinationBankAtPosition:0]).code isEqualToString:@"001"] );
    
}

- (void) testParsingRespuestaTipoTarjetaNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<BANCOSDESTINO>"\
    /*@"<E>"\
     @"<CODIGO>001</CODIGO>"\
     @"<DESCRIPCION>Banca iPad</DESCRIPCION>"\
     @"</E>"\*/
    @"</BANCOSDESTINO>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingCardTypeListWithXMLString: xmlString];
    XCTAssertNil(((FODestinationBank *)[foDestinationBankList_ foDestinationBankAtPosition:0]));
    
}

- (void) testParsingRespuestaTipoTarjetaVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<BANCOSDESTINO>"\
    @"<E>"\
    @"<CODIGO></CODIGO>"\
    @"<DESCRIPCION></DESCRIPCION>"\
    @"</E>"\
    @"</BANCOSDESTINO>"\
    @"</MSG-S>";
    
    [self doParsingCardTypeListWithXMLString: xmlString];
    XCTAssertTrue([((FODestinationBank *)[foDestinationBankList_ foDestinationBankAtPosition:0]).code isEqualToString:@""]);
    
}




#pragma mark TEST METHOD

- (void) doParsingCardTypeListWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    foDestinationBankList_ = [[FODestinationBankList alloc] init];
    foDestinationBankList_.openingTag = @"bancosdestino";
    
    [parser setDelegate: foDestinationBankList_];
    
    [foDestinationBankList_ parserDidStartDocument: parser];
    
    [parser parse];
}





@end
