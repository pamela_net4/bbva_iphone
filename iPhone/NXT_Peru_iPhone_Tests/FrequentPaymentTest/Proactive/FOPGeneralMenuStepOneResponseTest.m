//
//  FOPGeneralMenuStepOneResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 29/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FOPGeneralMenuStepOneResponse.h"
#import "FrequentOperationList.h"
#import "FrequentOperation.h"


@interface FOPGeneralMenuStepOneResponseTest : XCTestCase
{
@private
    FOPGeneralMenuStepOneResponse *fopGeneralMenuStepOneResponse_;
}

@end

@implementation FOPGeneralMenuStepOneResponseTest

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}


#pragma mark Test mas datos

- (void) testParsingRespuestaMasDatosTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
	@"<E>"\
	@"<OPFRECUENTE>"\
	@"<CODOPERACION></CODOPERACION>"\
	@"<SERVICIO></SERVICIO>"\
	@"<ALIAS></ALIAS>"\
	@"<ALIASCORTO></ALIASCORTO>"\
	@"<CELULAR></CELULAR>"\
	@"<DIAMES></DIAMES>"\
	@"<EMAIL></EMAIL>"\
	@"<NUMAFIL></NUMAFIL>"\
	@"<FORMAFIL></FORMAFIL>"\
	@"</OPFRECUENTE>"\
	@"</E>"\
    @"</OPERACIONES>"\
    @"<MASDATOS>Test Mas Datos</MASDATOS>"\
    @"<NEXTPAGE></NEXTPAGE>"\
    @"<BACKPAGE></BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fopGeneralMenuStepOneResponse_.moreData isEqualToString: @"Test Mas Datos" ]);
    
}

- (void) testParsingRespuestaMasDatosNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
	@"<E>"\
	@"<OPFRECUENTE>"\
	@"<CODOPERACION></CODOPERACION>"\
	@"<SERVICIO></SERVICIO>"\
	@"<ALIAS></ALIAS>"\
	@"<ALIASCORTO></ALIASCORTO>"\
	@"<CELULAR></CELULAR>"\
	@"<DIAMES></DIAMES>"\
	@"<EMAIL></EMAIL>"\
	@"<NUMAFIL></NUMAFIL>"\
	@"<FORMAFIL></FORMAFIL>"\
	@"</OPFRECUENTE>"\
	@"</E>"\
    @"</OPERACIONES>"\
    /*@"<MASDATOS></MASDATOS>"\*/
    @"<NEXTPAGE></NEXTPAGE>"\
    @"<BACKPAGE></BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertNil(fopGeneralMenuStepOneResponse_.moreData);
    
}

- (void) testParsingRespuestaMasDatosVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
	@"<E>"\
	@"<OPFRECUENTE>"\
	@"<CODOPERACION></CODOPERACION>"\
	@"<SERVICIO></SERVICIO>"\
	@"<ALIAS></ALIAS>"\
	@"<ALIASCORTO></ALIASCORTO>"\
	@"<CELULAR></CELULAR>"\
	@"<DIAMES></DIAMES>"\
	@"<EMAIL></EMAIL>"\
	@"<NUMAFIL></NUMAFIL>"\
	@"<FORMAFIL></FORMAFIL>"\
	@"</OPFRECUENTE>"\
	@"</E>"\
    @"</OPERACIONES>"\
    @"<MASDATOS></MASDATOS>"\
    @"<NEXTPAGE></NEXTPAGE>"\
    @"<BACKPAGE></BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fopGeneralMenuStepOneResponse_.moreData isEqualToString: @"" ]);
    
}

#pragma mark Test Next Page

- (void) testParsingRespuestaNextPageTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
	@"<E>"\
	@"<OPFRECUENTE>"\
	@"<CODOPERACION></CODOPERACION>"\
	@"<SERVICIO></SERVICIO>"\
	@"<ALIAS></ALIAS>"\
	@"<ALIASCORTO></ALIASCORTO>"\
	@"<CELULAR></CELULAR>"\
	@"<DIAMES></DIAMES>"\
	@"<EMAIL></EMAIL>"\
	@"<NUMAFIL></NUMAFIL>"\
	@"<FORMAFIL></FORMAFIL>"\
	@"</OPFRECUENTE>"\
	@"</E>"\
    @"</OPERACIONES>"\
    @"<MASDATOS></MASDATOS>"\
    @"<NEXTPAGE>Test Next Page</NEXTPAGE>"\
    @"<BACKPAGE></BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fopGeneralMenuStepOneResponse_.nextPage isEqualToString: @"Test Next Page" ]);
    
}

- (void) testParsingRespuestaNextPageNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
	@"<E>"\
	@"<OPFRECUENTE>"\
	@"<CODOPERACION></CODOPERACION>"\
	@"<SERVICIO></SERVICIO>"\
	@"<ALIAS></ALIAS>"\
	@"<ALIASCORTO></ALIASCORTO>"\
	@"<CELULAR></CELULAR>"\
	@"<DIAMES></DIAMES>"\
	@"<EMAIL></EMAIL>"\
	@"<NUMAFIL></NUMAFIL>"\
	@"<FORMAFIL></FORMAFIL>"\
	@"</OPFRECUENTE>"\
	@"</E>"\
    @"</OPERACIONES>"\
    @"<MASDATOS></MASDATOS>"\
    /*@"<NEXTPAGE></NEXTPAGE>"\*/
    @"<BACKPAGE></BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertNil(fopGeneralMenuStepOneResponse_.nextPage);
    
}

- (void) testParsingRespuestaNextPageVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
	@"<E>"\
	@"<OPFRECUENTE>"\
	@"<CODOPERACION></CODOPERACION>"\
	@"<SERVICIO></SERVICIO>"\
	@"<ALIAS></ALIAS>"\
	@"<ALIASCORTO></ALIASCORTO>"\
	@"<CELULAR></CELULAR>"\
	@"<DIAMES></DIAMES>"\
	@"<EMAIL></EMAIL>"\
	@"<NUMAFIL></NUMAFIL>"\
	@"<FORMAFIL></FORMAFIL>"\
	@"</OPFRECUENTE>"\
	@"</E>"\
    @"</OPERACIONES>"\
    @"<MASDATOS></MASDATOS>"\
    @"<NEXTPAGE></NEXTPAGE>"\
    @"<BACKPAGE></BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fopGeneralMenuStepOneResponse_.nextPage isEqualToString: @"" ]);
    
}


#pragma mark Test Back Page

- (void) testParsingRespuestaBackPageTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
	@"<E>"\
	@"<OPFRECUENTE>"\
	@"<CODOPERACION></CODOPERACION>"\
	@"<SERVICIO></SERVICIO>"\
	@"<ALIAS></ALIAS>"\
	@"<ALIASCORTO></ALIASCORTO>"\
	@"<CELULAR></CELULAR>"\
	@"<DIAMES></DIAMES>"\
	@"<EMAIL></EMAIL>"\
	@"<NUMAFIL></NUMAFIL>"\
	@"<FORMAFIL></FORMAFIL>"\
	@"</OPFRECUENTE>"\
	@"</E>"\
    @"</OPERACIONES>"\
    @"<MASDATOS></MASDATOS>"\
    @"<NEXTPAGE></NEXTPAGE>"\
    @"<BACKPAGE>Test Back Page</BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fopGeneralMenuStepOneResponse_.backPage isEqualToString: @"Test Back Page" ]);
    
}

- (void) testParsingRespuestaBackPageNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
	@"<E>"\
	@"<OPFRECUENTE>"\
	@"<CODOPERACION></CODOPERACION>"\
	@"<SERVICIO></SERVICIO>"\
	@"<ALIAS></ALIAS>"\
	@"<ALIASCORTO></ALIASCORTO>"\
	@"<CELULAR></CELULAR>"\
	@"<DIAMES></DIAMES>"\
	@"<EMAIL></EMAIL>"\
	@"<NUMAFIL></NUMAFIL>"\
	@"<FORMAFIL></FORMAFIL>"\
	@"</OPFRECUENTE>"\
	@"</E>"\
    @"</OPERACIONES>"\
    @"<MASDATOS></MASDATOS>"\
    @"<NEXTPAGE></NEXTPAGE>"\
    /*@"<BACKPAGE></BACKPAGE>"\*/
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertNil(fopGeneralMenuStepOneResponse_.backPage);
    
}

- (void) testParsingRespuestaBackPageVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
	@"<E>"\
	@"<OPFRECUENTE>"\
	@"<CODOPERACION></CODOPERACION>"\
	@"<SERVICIO></SERVICIO>"\
	@"<ALIAS></ALIAS>"\
	@"<ALIASCORTO></ALIASCORTO>"\
	@"<CELULAR></CELULAR>"\
	@"<DIAMES></DIAMES>"\
	@"<EMAIL></EMAIL>"\
	@"<NUMAFIL></NUMAFIL>"\
	@"<FORMAFIL></FORMAFIL>"\
	@"</OPFRECUENTE>"\
	@"</E>"\
    @"</OPERACIONES>"\
    @"<MASDATOS></MASDATOS>"\
    @"<NEXTPAGE></NEXTPAGE>"\
    @"<BACKPAGE></BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fopGeneralMenuStepOneResponse_.backPage isEqualToString: @"" ]);
    
}


#pragma mark Test Operaciones

- (void) testParsingRespuestaOperacionesTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
	@"<E>"\
	@"<OPFRECUENTE>"\
	@"<CODOPERACION>005</CODOPERACION>"\
	@"<SERVICIO>Test Servicio</SERVICIO>"\
	@"<ALIAS>Test Alias</ALIAS>"\
	@"<ALIASCORTO>Test Alias Corto</ALIASCORTO>"\
	@"<CELULAR>946533217</CELULAR>"\
	@"<DIAMES></DIAMES>"\
	@"<EMAIL></EMAIL>"\
	@"<NUMAFIL></NUMAFIL>"\
	@"<FORMAFIL></FORMAFIL>"\
	@"</OPFRECUENTE>"\
	@"</E>"\
    @"</OPERACIONES>"\
    @"<MASDATOS></MASDATOS>"\
    @"<NEXTPAGE></NEXTPAGE>"\
    @"<BACKPAGE>Test Back Page</BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fopGeneralMenuStepOneResponse_.frequentOperationList foPlaceCount] > 0 );
    
}

- (void) testParsingRespuestaOperacionesNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    /*@"<OPERACIONES>"\
	@"<E>"\
	@"<OPFRECUENTE>"\
	@"<CODOPERACION></CODOPERACION>"\
	@"<SERVICIO></SERVICIO>"\
	@"<ALIAS></ALIAS>"\
	@"<ALIASCORTO></ALIASCORTO>"\
	@"<CELULAR></CELULAR>"\
	@"<DIAMES></DIAMES>"\
	@"<EMAIL></EMAIL>"\
	@"<NUMAFIL></NUMAFIL>"\
	@"<FORMAFIL></FORMAFIL>"\
	@"</OPFRECUENTE>"\
	@"</E>"\
    @"</OPERACIONES>"\*/
    @"<MASDATOS></MASDATOS>"\
    @"<NEXTPAGE></NEXTPAGE>"\
    @"<BACKPAGE></BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertNil(fopGeneralMenuStepOneResponse_.frequentOperationList);
    
}

- (void) testParsingRespuestaOperacionesVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
	/*@"<E>"\
	@"<OPFRECUENTE>"\
	@"<CODOPERACION></CODOPERACION>"\
	@"<SERVICIO></SERVICIO>"\
	@"<ALIAS></ALIAS>"\
	@"<ALIASCORTO></ALIASCORTO>"\
	@"<CELULAR></CELULAR>"\
	@"<DIAMES></DIAMES>"\
	@"<EMAIL></EMAIL>"\
	@"<NUMAFIL></NUMAFIL>"\
	@"<FORMAFIL></FORMAFIL>"\
	@"</OPFRECUENTE>"\
	@"</E>"\*/
    @"</OPERACIONES>"\
    @"<MASDATOS></MASDATOS>"\
    @"<NEXTPAGE></NEXTPAGE>"\
    @"<BACKPAGE></BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([fopGeneralMenuStepOneResponse_.frequentOperationList foPlaceCount] == 0);
    
}


#pragma mark Test Operaciones

- (void) testParsingRespuestaOperacionFrecuenteTieneDatos {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
	@"<E>"\
	@"<OPFRECUENTE>"\
	@"<CODOPERACION>005</CODOPERACION>"\
	@"<SERVICIO>Test Servicio</SERVICIO>"\
	@"<ALIAS>Test Alias</ALIAS>"\
	@"<ALIASCORTO>Test Alias Corto</ALIASCORTO>"\
	@"<CELULAR>946533217</CELULAR>"\
	@"<DIAMES></DIAMES>"\
	@"<EMAIL></EMAIL>"\
	@"<NUMAFIL></NUMAFIL>"\
	@"<FORMAFIL></FORMAFIL>"\
	@"</OPFRECUENTE>"\
	@"</E>"\
    @"</OPERACIONES>"\
    @"<MASDATOS></MASDATOS>"\
    @"<NEXTPAGE></NEXTPAGE>"\
    @"<BACKPAGE>Test Back Page</BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([((FrequentOperation *)[fopGeneralMenuStepOneResponse_.frequentOperationList frequentOperationAtPosition:0]).service isEqualToString:@"Test Servicio"]  );
    
}

- (void) testParsingRespuestaOperacionFrecuenteNulo {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
     /*@"<E>"\
     @"<OPFRECUENTE>"\
     @"<CODOPERACION></CODOPERACION>"\
     @"<SERVICIO></SERVICIO>"\
     @"<ALIAS></ALIAS>"\
     @"<ALIASCORTO></ALIASCORTO>"\
     @"<CELULAR></CELULAR>"\
     @"<DIAMES></DIAMES>"\
     @"<EMAIL></EMAIL>"\
     @"<NUMAFIL></NUMAFIL>"\
     @"<FORMAFIL></FORMAFIL>"\
     @"</OPFRECUENTE>"\
     @"</E>"\*/
     @"</OPERACIONES>"\
    @"<MASDATOS></MASDATOS>"\
    @"<NEXTPAGE></NEXTPAGE>"\
    @"<BACKPAGE></BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertNil([fopGeneralMenuStepOneResponse_.frequentOperationList frequentOperationAtPosition:0]);
    
}

- (void) testParsingRespuestaOperacionFrecuenteVacio {
    
    NSString *xmlString =
    
    @"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
    @"<MSG-S>"\
    @"<INFORMACIONADICIONAL>"\
    @"<OPERACIONES>"\
	@"<E>"\
     @"<OPFRECUENTE>"\
     @"<CODOPERACION></CODOPERACION>"\
     @"<SERVICIO></SERVICIO>"\
     @"<ALIAS></ALIAS>"\
     @"<ALIASCORTO></ALIASCORTO>"\
     @"<CELULAR></CELULAR>"\
     @"<DIAMES></DIAMES>"\
     @"<EMAIL></EMAIL>"\
     @"<NUMAFIL></NUMAFIL>"\
     @"<FORMAFIL></FORMAFIL>"\
     @"</OPFRECUENTE>"\
    @"</E>"\
    @"</OPERACIONES>"\
    @"<MASDATOS></MASDATOS>"\
    @"<NEXTPAGE></NEXTPAGE>"\
    @"<BACKPAGE></BACKPAGE>"\
    @"</INFORMACIONADICIONAL>"\
    @"</MSG-S>";
    
    [self doParsingFOPGeneralMenuStepOneResponseWithXMLString: xmlString];
    XCTAssertTrue([((FrequentOperation *)[fopGeneralMenuStepOneResponse_.frequentOperationList frequentOperationAtPosition:0]).service isEqualToString:@""] );
    
}

#pragma mark TEST METHOD

- (void) doParsingFOPGeneralMenuStepOneResponseWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    fopGeneralMenuStepOneResponse_ = [FOPGeneralMenuStepOneResponse alloc];
    fopGeneralMenuStepOneResponse_.openingTag = @"informacionadicional";
    
    [parser setDelegate: fopGeneralMenuStepOneResponse_];
    
    [fopGeneralMenuStepOneResponse_ parserDidStartDocument: parser];
    
    [parser parse];
}


@end
