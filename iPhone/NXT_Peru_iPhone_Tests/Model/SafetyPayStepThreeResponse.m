//
//  SafetyPayStepThreeResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 22/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "SafetyPayStepThreeResponse.h"
#import "BankAccount.h"

@implementation SafetyPayStepThreeResponse

@synthesize transactionNumber = transactionNumber_;
@synthesize establishment = establishment_;
@synthesize currency = currency_;
@synthesize badge = badge_;
@synthesize amount = amount_;
@synthesize company = company_;
@synthesize service = service_;
@synthesize coordinate = coordinate_;
@synthesize seal = seal_;
@synthesize account = account_;

- (void) parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearSatefyPayStepThreeResponseData];
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing) {
    
        NSString *lname = [elementName lowercaseString];
        
        if([lname isEqualToString: @"informacionadicional"]){
            
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingSafetyPayStepThreeResponseDetail;
            
        } else if([lname isEqualToString: @"identificador"]){
        
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingTransactionNumber;
            
        } else if([lname isEqualToString: @"establecimiento"]){
        
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingEstablishment;
            
        } else if([lname isEqualToString: @"moneda"]){
            
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingCurrency;
            
        } else if([lname isEqualToString: @"divimporte"]){
        
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingBadge;
            
        } else if([lname isEqualToString: @"importe"]){
            
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingAmount;
        
        } else if([lname isEqualToString: @"cuenta"]){
        
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingAccount;
            account_ = nil;
            account_ = [[BankAccount alloc] init];
            [account_ setParentParseableObject:self];
            account_.openingTag = lname;
            [parser setDelegate:account_];
            [account_ parserDidStartDocument:parser];
            
        } else if([lname isEqualToString: @"empresa"]){
            
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingCompany;
        
        } else if([lname isEqualToString: @"servicio"]){
            
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingService;
        
        } else if([lname isEqualToString: @"coordenada"]){
            
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingCoordinate;
        
        } else if([lname isEqualToString: @"sello"]){
            
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingSeal;
        
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString *elementString = [self.elementString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if(xmlAnalysisCurrentValue_ != serxeas_Nothing){
    
        if(xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingTransactionNumber){
        
            transactionNumber_ = nil;
            transactionNumber_ = elementString;
            
        } else if (xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingEstablishment) {
            
            establishment_ = nil;
            establishment_ = elementString;
        
        } else if (xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingCurrency){
            
            currency_ = nil;
            currency_ = elementString;
            
        } else if(xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingBadge){
        
            badge_ = nil;
            badge_ = elementString;
            
        } else if (xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingAmount){
        
            amount_ = nil;
            amount_ = [[NSDecimalNumber alloc] initWithDouble:elementString.doubleValue];
            
        } else if(xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingCompany){
        
            company_ = nil;
            company_ = elementString;
            
        } else if(xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingService){
            
            service_ = nil;
            service_ = elementString;
            
        } else if(xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingCoordinate){
            
            coordinate_ = nil;
            coordinate_ = elementString;
            
        } else if(xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingSeal){
        
            seal_ = nil;
            
            seal_ = [elementString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
            seal_ = [seal_ stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
        }
        
    }
    
}

- (void) clearSatefyPayStepThreeResponseData {
    transactionNumber_ = nil;
    establishment_ = nil;
    currency_ = nil;
    badge_ = nil;
    amount_ = nil;
    company_ = nil;
    service_ = nil;
    coordinate_ = nil;
    seal_ = nil;
}

@end
