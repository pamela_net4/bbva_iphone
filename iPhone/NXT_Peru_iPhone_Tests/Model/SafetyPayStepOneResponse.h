//
//  SafetyPayStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 21/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"

typedef enum {
    atrdxeas_AnalyzingSafetyPayStepOneResponseDetail,
    atrdxeas_AnalyzingStatus,
    atrdxeas_AnalyzingMessage
}atrdxeas_AnalyzingSafetyPayStepOneResponseXMLElementAnalyzerState;



@interface SafetyPayStepOneResponse : StatusEnabledResponse
{
@private
    NSString *estadoActivo_;
    NSString *mensajeInformativo_;
}
@property (nonatomic, readonly, retain) NSString *estadoActivo;
@property (nonatomic, readonly, retain) NSString *mensajeInformativo;
- (void) clearSafetyPayStepOneResponseData;
@end
