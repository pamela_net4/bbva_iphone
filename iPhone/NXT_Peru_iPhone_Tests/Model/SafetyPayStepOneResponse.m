//
//  SafetyPayStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 21/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "SafetyPayStepOneResponse.h"

@implementation SafetyPayStepOneResponse

@synthesize estadoActivo = estadoActivo_;
@synthesize mensajeInformativo = mensajeInformativo_;


#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc
{
    [self clearSafetyPayStepOneResponseData];
    [super dealloc];
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a GlobalPositionResponse instance providing the associated notification
 *
 * @return The initialized GlobalPositionResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationSafetyPayStatusResponseReceived;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
-(void) parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearSafetyPayStepOneResponseData];
}

-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if(xmlAnalysisCurrentValue_ != serxeas_Nothing){
        
        NSString *lname = [elementName lowercaseString];
        
        if([lname isEqualToString: @"informacionadicional"]){
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingSafetyPayStepOneResponseDetail;
        }
        if([lname isEqualToString: @"estadoactivo"]){
            
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingStatus;
            
            
        }
        if([lname isEqualToString: @"mensajeinformativo"]){
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingMessage;
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
-(void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing){
        
        NSString *lname = [elementName lowercaseString];
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if([lname isEqualToString: @"estadoactivo"]){
            
            [estadoActivo_ release];
            estadoActivo_ = nil;
            estadoActivo_ = elementString;
            
        } else if([lname isEqualToString: @"mensajeinformativo"]){
            
            [mensajeInformativo_ release];
            mensajeInformativo_ = nil;
            mensajeInformativo_ = elementString;
        
        } else {
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
    }
    
}

-(void) clearSafetyPayStepOneResponseData {
    [estadoActivo_ release];
    estadoActivo_ = nil;
}

@end
