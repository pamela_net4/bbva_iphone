//
//  SafetyPayStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 21/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"
#import "FOAccountList.h"
#import "AlterCarrierList.h"

typedef enum {
    atrdxeas_AnalyzingSafetyPayStepTwoResponseDetail,
    atrdxeas_AnalyzingTransactionNumber,
    atrdxeas_AnalyzingEstablishment,
    atrdxeas_AnalyzingCurrency,
    atrdxeas_AnalyzingAmount,
    atrdxeas_AnalyzingAccountList,
    atrdxeas_AnalyzingCompany,
    atrdxeas_AnalyzingService,
    atrdxeas_AnalyzingCarrierList
} atrdxeas_AnalyzingSafetyPayStepTwoResponseXMLElementAnalyzerSate;

@interface SafetyPayStepTwoResponse : StatusEnabledResponse
{
@private
    NSString *transactionNumber_;
    NSString *establishment_;
    NSString *currency_;
    NSDecimalNumber *amount_;
    FOAccountList *accountList_;
    NSString *company_;
    NSString *service_;
    AlterCarrierList *alterCarrierList_;
}
@property (nonatomic, readonly, copy) NSString *transactionNumber;
@property (nonatomic, readonly, copy) NSString *establishment;
@property (nonatomic, readonly, copy) NSString *currency;
@property (nonatomic, readonly, copy) NSDecimalNumber *amount;
@property (nonatomic, readonly, copy) FOAccountList *accountList;
@property (nonatomic, readonly, copy) NSString *company;
@property (nonatomic, readonly, copy) NSString *service;
@property (nonatomic, readonly, copy) AlterCarrierList *alterCarrierList;
- (void) clearSafetyPayStepTwoResponseData;
@end
