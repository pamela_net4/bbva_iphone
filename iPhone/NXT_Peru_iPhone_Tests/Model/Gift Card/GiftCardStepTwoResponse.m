//
//  GiftCardStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "GiftCardStepTwoResponse.h"
#import "Account.h"
#import "FOCard.h"
/**
 * Enumerates the analysis state
 */
typedef enum {
    gcstwrxeas_activeState  = serxeas_ElementsCount,//!<Analyzing the active state
    
    gcstwrxeas_AnalyzingAccount, //!<Analyzing the account
    gcstwrxeas_AnalyzingCard, //!<Analyzing the card
    gcstwrxeas_AnalyzingGiftCard,//!<Analyzing the gift card
    gcstwrxeas_AnalyzingAmount,//!<Analyzing the Amount
    gcstwrxeas_AnalyzingCurrency,//!<Analyzing the currency
    gcstwrxeas_AnalyzingBadgeAmount,//!<Analyzing the badge amount
    gcstwrxeas_AnalyzingCoordinate,//!<Analyzing the Coordinate
    gcstwrxeas_AnalyzingSecuritySeal//!<Analyzing the security seal
    
}
GiftCardStepTwoResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * GiftCardStepTwoResponse private category
 */
@interface GiftCardStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearGiftCardStepTwoResponseData;

@end


#pragma mark -




@implementation GiftCardStepTwoResponse

#pragma mark -
#pragma mark Properties


@synthesize account=account_;
@synthesize foCard=foCard_;
@synthesize giftCard=giftCard_;
@synthesize amount=amount_;
@synthesize currency=currency_;
@synthesize badgeAmount=badgeAmount_;
@synthesize coordinate=coordinate_;
@synthesize securitySeal=securitySeal_;

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a GiftCardStepTwoResponse instance providing the associated notification
 *
 * @return The initialized GiftCardStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationGiftCardStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearGiftCardStepTwoResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"cuenta"]) {
            
            xmlAnalysisCurrentValue_ = gcstwrxeas_AnalyzingAccount;
			[account_ release];
            account_ = nil;
            account_ = [[Account alloc] init];
            account_.openingTag = lname;
            [account_ setParentParseableObject:self];
            [parser setDelegate:account_];
            [account_ parserDidStartDocument:parser];
            
            
        } else if ([lname isEqualToString: @"tarjeta"]) {
            
            xmlAnalysisCurrentValue_ = gcstwrxeas_AnalyzingCard;
			[foCard_ release];
            foCard_ = nil;
            foCard_ = [[FOCard alloc] init];
            foCard_.openingTag = lname;
            [foCard_ setParentParseableObject:self];
            [parser setDelegate:foCard_];
            [foCard_ parserDidStartDocument:parser];
            
        }else if ([lname isEqualToString: @"tarjetaregalo"]) {
            
            xmlAnalysisCurrentValue_ = gcstwrxeas_AnalyzingGiftCard ;
            
        }else if ([lname isEqualToString: @"importe"]) {
            
            xmlAnalysisCurrentValue_ = gcstwrxeas_AnalyzingAmount;
            
        }   else if ([lname isEqualToString: @"moneda"]) {
            
            xmlAnalysisCurrentValue_ = gcstwrxeas_AnalyzingCurrency ;
            
        }  else if ([lname isEqualToString: @"divimporte"]) {
            
            xmlAnalysisCurrentValue_ = gcstwrxeas_AnalyzingBadgeAmount ;
            
        }  else if ([lname isEqualToString: @"coordenada"]) {
            
            xmlAnalysisCurrentValue_ = gcstwrxeas_AnalyzingCoordinate ;
            
        }  else if ([lname isEqualToString: @"sello"]) {
            
            xmlAnalysisCurrentValue_ = gcstwrxeas_AnalyzingSecuritySeal ;
            
        }   else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}


/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == gcstwrxeas_AnalyzingGiftCard ) {
            
            [giftCard_ release];
            giftCard_ = nil;
            giftCard_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == gcstwrxeas_AnalyzingAmount) {
            
            [amount_ release];
            amount_ = nil;
            amount_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gcstwrxeas_AnalyzingCurrency ) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == gcstwrxeas_AnalyzingBadgeAmount ) {
            
            [badgeAmount_ release];
            badgeAmount_ = nil;
            badgeAmount_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == gcstwrxeas_AnalyzingCoordinate) {
            
            [coordinate_ release];
            coordinate_ = nil;
            coordinate_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == gcstwrxeas_AnalyzingSecuritySeal) {
            
            [securitySeal_ release];
            securitySeal_ = nil;
            securitySeal_ = [elementString copyWithZone:self.zone];
            
        }

        
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end


#pragma mark -

@implementation GiftCardStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearGiftCardStepTwoResponseData {
    
    [account_ release];
    account_ = nil;
    
    [foCard_ release];
    foCard_ = nil;
    
    [giftCard_ release];
    giftCard_ = nil;
    
    [amount_ release];
    amount_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [coordinate_ release];
    coordinate_ = nil;
    
    [badgeAmount_ release];
    badgeAmount_ = nil;
    
    [currency_ release];
    currency_ = nil;
    

    
}




@end
