//
//  GiftCardStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
#import "Account.h"
#import "FOCard.h"


@interface GiftCardStepThreeResponse : StatusEnabledResponse
{
@private
    
    /**
     * Account
     */
    Account *account_;
    /**
     * Card
     */
    FOCard *foCard_;
    /**
     * Amount Paid
     */
    NSString *amountPaid_;
    /**
     * Amount Charge
     */
    NSString *amountCharge_;
    /**
     * Currency
     */
    NSString *currency_;
    /**
     * badge
     */
    NSString *badge_;
    /**
     * service
     */
    NSString *service_;
    
    NSString *exchangeRate_;
    /**
     * date hour
     */
    NSString *dateHour_;
    /**
     * operation number
     */
    NSString *operationNumber_;
    
}

/**
 * Provides read-only access to the account
 */
@property (nonatomic, readonly, retain) Account *account;
/**
 * Provides read-only access to the foCard
 */
@property (nonatomic, readonly, retain) FOCard *foCard;
/**
 * Provides read-only access to the amount Paid
 */
@property (nonatomic, readonly, retain) NSString *amountPaid;
/**
 * Provides read-only access to the amount Charge
 */
@property (nonatomic, readonly, retain) NSString *amountCharge;
/**
 * Provides read-only access to the short currency
 */
@property (nonatomic, readonly, retain) NSString *currency;
/**
 * Provides read-only access to the day badge
 */
@property (nonatomic, readonly, retain) NSString *badge;
/**
 * Provides read-only access to the sms service
 */
@property (nonatomic, readonly, retain) NSString *service;
/**
 * Provides read-only access to the sms date Hour
 */
@property (nonatomic, readonly, retain) NSString *dateHour;
/**
 * Provides read-only access to the operation number
 */
@property (nonatomic, readonly, retain) NSString *operationNumber;


@property (nonatomic, readonly, retain) NSString *exchangeRate;

@end
