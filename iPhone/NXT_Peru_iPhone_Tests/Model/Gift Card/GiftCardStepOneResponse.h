//
//  GiftCardStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@class FOAccountList;
@class FOCardList;
@class AlterCarrierList;

@interface GiftCardStepOneResponse : StatusEnabledResponse
{
    @private
    NSString *activeState_;
    FOAccountList *foAccountList_;
    FOCardList *foCardList_;
    AlterCarrierList *alterCarrierList_;
}

/**
 * Provides read-only access to the active State
 */
@property (nonatomic, readonly, retain) NSString *activeState;

/**
 * Provides read-only access to the account list
 */
@property (nonatomic, readonly, retain) FOAccountList *foAccountList;

/**
 * Provides read-only access to the card list
 */
@property (nonatomic, readonly, retain) FOCardList *foCardList;

/**
 * Provides read-only access to the carrier list
 */
@property (nonatomic, readonly, retain) AlterCarrierList *alterCarrierList;

@end
