//
//  GiftCardStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@class Account;
@class FOCard;


@interface GiftCardStepTwoResponse : StatusEnabledResponse
{
    @private
    Account *account_;
    FOCard *foCard_;
    NSString *giftCard_;
    NSString *amount_;
    NSString *currency_;
    NSString *badgeAmount_;
    NSString *coordinate_;
    NSString *securitySeal_;
}

/**
 * Provides read-only access to the account
 */
@property (nonatomic, readonly, retain) Account *account;

/**
 * Provides read-only access to the FOCard
 */
@property (nonatomic, readonly, retain) FOCard *foCard;

/**
 * Provides read-only access to the gift card
 */
@property (nonatomic, readonly, retain) NSString *giftCard;

/**
 * Provides read-only access to the amount
 */
@property (nonatomic, readonly, retain) NSString *amount;

/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, retain) NSString *currency;

/**
 * Provides read-only access to the badgeAmount
 */
@property (nonatomic, readonly, retain) NSString *badgeAmount;

/**
 * Provides read-only access to the coordinate
 */
@property (nonatomic, readonly, retain) NSString *coordinate;

/**
 * Provides read-only access to the security seal
 */
@property (nonatomic, readonly, retain) NSString *securitySeal;

@end
