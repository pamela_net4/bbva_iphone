//
//  GiftCardStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "GiftCardStepOneResponse.h"
#import "FOAccountList.h"
#import "FOCardList.h"
#import "AlterCarrierList.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    gcsorxeas_activeState  = serxeas_ElementsCount,//!<Analyzing the active state

    gcsorxeas_AnalyzingFOAccountList, //!<Analyzing the account list
    gcsorxeas_AnalyzingFOCardList, //!<Analyzing the card list
    gcsorxeas_AnalyzingAlterCarrierList//!<Analyzing the alter carrier list

    
}
GiftCardStepOneResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * GiftCardStepOneResponse private category
 */
@interface GiftCardStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearGiftCardStepOneResponseData;

@end


#pragma mark -



@implementation GiftCardStepOneResponse

#pragma mark -
#pragma mark Properties

@synthesize activeState =activeState_;
@synthesize foAccountList=foAccountList_;
@synthesize foCardList=foCardList_;
@synthesize alterCarrierList=alterCarrierList_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearGiftCardStepOneResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a GiftCardStepOneResponse instance providing the associated notification
 *
 * @return The initialized GiftCardStepOneResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationGiftCardStepOneResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearGiftCardStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        if ([lname isEqualToString: @"estadoactivo"])
        {
            xmlAnalysisCurrentValue_ = gcsorxeas_activeState;
            
        }else if ([lname isEqualToString: @"cuentas"]) {
            
            xmlAnalysisCurrentValue_ = gcsorxeas_AnalyzingFOAccountList;
			[foAccountList_ release];
            foAccountList_ = nil;
            foAccountList_ = [[FOAccountList alloc] init];
            foAccountList_.openingTag = lname;
            [foAccountList_ setParentParseableObject:self];
            [parser setDelegate:foAccountList_];
            [foAccountList_ parserDidStartDocument:parser];
            
        }else if ([lname isEqualToString: @"tarjetas"]) {
            
            xmlAnalysisCurrentValue_ = gcsorxeas_AnalyzingFOCardList;
			[foCardList_ release];
            foCardList_ = nil;
            foCardList_ = [[FOCardList alloc] init];
            foCardList_.openingTag = lname;
            [foCardList_ setParentParseableObject:self];
            [parser setDelegate:foCardList_];
            [foCardList_ parserDidStartDocument:parser];
            
            
        } else if ([lname isEqualToString: @"operadoras"]) {
            
            xmlAnalysisCurrentValue_ = gcsorxeas_AnalyzingAlterCarrierList;
			[alterCarrierList_ release];
            alterCarrierList_ = nil;
            alterCarrierList_ = [[AlterCarrierList alloc] init];
            alterCarrierList_.openingTag = lname;
            [alterCarrierList_ setParentParseableObject:self];
            [parser setDelegate:alterCarrierList_];
            [alterCarrierList_ parserDidStartDocument:parser];
            
            
        }   else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}


/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    //NSString* lname = [elementName lowercaseString];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == gcsorxeas_activeState ) {
            
            [activeState_ release];
            activeState_ = nil;
            activeState_ = [elementString copyWithZone:self.zone];
            
        }
        
        
        /*if ([lname isEqualToString: @"msg-s"]) {
            
            informationUpdated_ = soie_InfoAvailable;
            [parser setDelegate:parentParseableObject_];
            parentParseableObject_ = nil;
            
        }*/
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
    }
    
}

@end


#pragma mark -

@implementation GiftCardStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearGiftCardStepOneResponseData {
    
    [activeState_ release];
    activeState_ = nil;
    
    [foAccountList_ release];
    foAccountList_ = nil;
    
    [foCardList_ release];
    foCardList_ = nil;
    
    [alterCarrierList_ release];
    alterCarrierList_ = nil;
    
    
}




@end

