//
//  GiftCardStepThreeResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "GiftCardStepThreeResponse.h"



/**
 * Enumerates the analysis state
 */
typedef enum {
    
    gcstrxeas_AnalyzingAccount = serxeas_ElementsCount, //!<Analyzing the account list
    gcstrxeas_AnalyzingFOCard, //!<Analyzing the channel list
    gcstrxeas_AnalyzingAmountPaid,
    gcstrxeas_AnalyzingAmountCharge,
    gcstrxeas_AnalyzingCurrency,
    gcstrxeas_AnalyzingBadge,
    gcstrxeas_AnalyzingService,
    gcstrxeas_AnalyzingDateHour,
    gcstrxeas_AnalyzingOperationNumber,
    gcstrxeas_AnalyzingExchangeRate
    
} GiftCardStepThreeResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * GiftCardStepThreeResponse private category
 */
@interface GiftCardStepThreeResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearGiftCardStepThreeResponseData;

@end


#pragma mark -



@implementation GiftCardStepThreeResponse
#pragma mark -
#pragma mark Properties

@synthesize account = account_;
@synthesize foCard = foCard_;
@synthesize amountPaid = amountPaid_;
@synthesize amountCharge = amountCharge_;
@synthesize currency = currency_;
@synthesize badge = badge_;
@synthesize service = service_;
@synthesize dateHour = dateHour_;
@synthesize operationNumber = operationNumber_;
@synthesize exchangeRate = exchangeRate_;



/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearGiftCardStepThreeResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a GiftCardStepThreeResponse instance providing the associated notification
 *
 * @return The initialized GiftCardStepThreeResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationGiftCardStepThreeResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearGiftCardStepThreeResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"cuenta"]) {
            
            xmlAnalysisCurrentValue_ = gcstrxeas_AnalyzingAccount;
			[account_ release];
            account_ = nil;
            account_ = [[Account alloc] init];
            account_.openingTag = lname;
            [account_ setParentParseableObject:self];
            [parser setDelegate:account_];
            [account_ parserDidStartDocument:parser];
            
            
        } else if ([lname isEqualToString: @"tarjeta"]) {
            
            xmlAnalysisCurrentValue_ = gcstrxeas_AnalyzingFOCard;
			[foCard_ release];
            foCard_ = nil;
            foCard_ = [[FOCard alloc] init];
            foCard_.openingTag = lname;
            [foCard_ setParentParseableObject:self];
            [parser setDelegate:foCard_];
            [foCard_ parserDidStartDocument:parser];
            
        }else if ([lname isEqualToString: @"servicio"]) {
            
            xmlAnalysisCurrentValue_ = gcstrxeas_AnalyzingService ;
            
        }else if ([lname isEqualToString: @"importecargado"]) {
            
            xmlAnalysisCurrentValue_ = gcstrxeas_AnalyzingAmountCharge;
            
        }  else if ([lname isEqualToString: @"importeabonado"]) {
            
            xmlAnalysisCurrentValue_ = gcstrxeas_AnalyzingAmountPaid ;
            
        }  else if ([lname isEqualToString: @"moneda"]) {
            
            xmlAnalysisCurrentValue_ = gcstrxeas_AnalyzingCurrency ;
            
        }  else if ([lname isEqualToString: @"divimporte"]) {
            
            xmlAnalysisCurrentValue_ = gcstrxeas_AnalyzingBadge ;
            
        }  else if ([lname isEqualToString: @"fechahora"]) {
            
            xmlAnalysisCurrentValue_ = gcstrxeas_AnalyzingDateHour ;
            
        }  else if ([lname isEqualToString: @"numoperacion"]) {
            
            xmlAnalysisCurrentValue_ = gcstrxeas_AnalyzingOperationNumber ;
        
        } else if ([lname isEqualToString: @"tipocambio"]) {
            
            xmlAnalysisCurrentValue_ = gcstrxeas_AnalyzingExchangeRate ;
            
        }  else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}


/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == gcstrxeas_AnalyzingService ) {
            
            [service_ release];
            service_ = nil;
            service_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == gcstrxeas_AnalyzingAmountCharge ) {
            
            [amountCharge_ release];
            amountCharge_ = nil;
            amountCharge_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gcstrxeas_AnalyzingAmountPaid ) {
            
            [amountPaid_ release];
            amountPaid_ = nil;
            amountPaid_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == gcstrxeas_AnalyzingCurrency ) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == gcstrxeas_AnalyzingBadge ) {
            
            [badge_ release];
            badge_ = nil;
            badge_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == gcstrxeas_AnalyzingDateHour ) {
            
            [dateHour_ release];
            dateHour_ = nil;
            dateHour_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == gcstrxeas_AnalyzingOperationNumber ) {
            
            [operationNumber_ release];
            operationNumber_ = nil;
            operationNumber_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == gcstrxeas_AnalyzingExchangeRate ) {
            
            [exchangeRate_ release];
            exchangeRate_ = nil;
            exchangeRate_ = [elementString copyWithZone:self.zone];
            
        }
        
        
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end


#pragma mark -

@implementation GiftCardStepThreeResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearGiftCardStepThreeResponseData {
    
    [account_ release];
    account_ = nil;
    
    [foCard_ release];
    foCard_ = nil;
    
    [service_ release];
    service_ = nil;
    
    [amountPaid_ release];
    amountPaid_ = nil;
    
    [amountCharge_ release];
    amountCharge_ = nil;
    
    [badge_ release];
    badge_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [dateHour_ release];
    dateHour_ = nil;
    
    [operationNumber_ release];
    operationNumber_ = nil;
    
    [exchangeRate_ release];
    exchangeRate_ = nil;
}




@end
