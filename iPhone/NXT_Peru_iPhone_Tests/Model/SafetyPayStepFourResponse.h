//
//  SafetyPayStepFourResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 23/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BankAccount.h"

typedef enum {
    
    atrdxeas_AnalyzingSafetyPayStepFourResponseDetail,
    atrdxeas_AnalyzingTransactionNumber,
    atrdxeas_AnalyzingEstablishment,
    atrdxeas_AnalyzingCurrency,
    atrdxeas_AnalyzingBadge,
    atrdxeas_AnalyzingAmount,
    atrdxeas_AnalyzingPaidAmount,
    atrdxeas_AnalyzingAccount,
    atrdxeas_AnalyzingCompany,
    atrdxeas_AnalyzingService,
    atrdxeas_AnalyzingOperationDate,
    atrdxeas_AnalyzingOperationNumber
} atrd_AnalyzingSafetyPayStepFourResponseXMLElementAnalyzerState;

@interface SafetyPayStepFourResponse : StatusEnabledResponse
{
@private
    NSString *transactionNumber_;
    NSString *establishment_;
    NSString *currency_;
    NSString *badge_;
    NSDecimalNumber *amount_;
    NSDecimalNumber *paidAmount_;
    BankAccount *account_;
    NSString *company_;
    NSString *service_;
    NSString *operationDateString_;
    NSDate *operationDate_;
    NSString *operationNumber_;
}
@property (nonatomic, readonly, copy) NSString *transactionNumber;
@property (nonatomic, readonly, copy) NSString *establishment;
@property (nonatomic, readonly, copy) NSString *currency;
@property (nonatomic, readonly, copy) NSString *badge;
@property (nonatomic, readonly, copy) NSDecimalNumber *amount;
@property (nonatomic, readonly, copy) NSDecimalNumber *paidAmount;
@property (nonatomic, readonly, copy) BankAccount *account;
@property (nonatomic, readonly, copy) NSString *company;
@property (nonatomic, readonly, copy) NSString *service;
@property (nonatomic, readonly, copy) NSString *operationDateString;
@property (nonatomic, readonly, copy) NSDate *operationDate;
@property (nonatomic, readonly, copy) NSString *operationNumber;
- (void) clearSatefyPayStepFourResponseData;
@end
