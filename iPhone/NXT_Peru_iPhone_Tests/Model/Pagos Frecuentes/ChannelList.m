//
//  ChannelList.m
//  NXT_Peru_iPad
//
//  Created by usuario on 23/01/14.
//
//

#import "ChannelList.h"
#import "Channel.h"


#pragma mark -

/**
 * ChannelList private category
 */
@interface ChannelList(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearChannelListData;

@end


#pragma mark -

@implementation ChannelList


#pragma mark -
#pragma mark Properties

@dynamic channelsList;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearChannelListData];
    
    [channelList_ release];
    channelList_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a ChannelList instance creating the associated array
 *
 * @return The initialized ChannelList instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        channelList_ = [[NSMutableArray alloc] init];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates the Channels list from another Channels list
 */
- (void)updateFrom:(ChannelList *)aChannelList {
    
    NSArray *otherChannelListArray = aChannelList.channelsList;
    
    [self clearChannelListData];
    
    for (Channel *Channel in otherChannelListArray) {
        
        [channelList_ addObject:Channel];
        
    }
    
    [self updateFromStatusEnabledResponse:aChannelList];
    
    self.informationUpdated = aChannelList.informationUpdated;
    
}

/*
 * Remove the contained data
 */
- (void)removeData {
    
    [self clearChannelListData];
    
}

#pragma mark -
#pragma mark Getters and setters

/*
 * Returns the Channel located at the given position, or nil if position is not valid
 */
- (Channel *)chanelAtPosition:(NSUInteger)aPosition{
    
	Channel *result = nil;
	
	if (aPosition < [channelList_ count]) {
        
		result = [channelList_ objectAtIndex:aPosition];
        
	}
	
	return result;
    
}

/**
 * Provides read access to the number of Channels stored
 *
 * @return The count
 */
- (NSUInteger) channelCount {
	return [channelList_ count];
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearChannelListData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (auxChannel_ != nil) {
        
        [channelList_ addObject:auxChannel_];
        [auxChannel_ release];
        auxChannel_ = nil;
        
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
            auxChannel_ = [[Channel alloc] init];
            [auxChannel_ setParentParseableObject:self];
            auxChannel_.openingTag = lname;
            [parser setDelegate:auxChannel_];
            [auxChannel_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (auxChannel_ != nil) {
        
        [channelList_ addObject:auxChannel_];
        [auxChannel_ release];
        auxChannel_ = nil;
        
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the Channels list array
 *
 * @return The Channels list array
 */
- (NSArray *)channelsList {
    
    return [NSArray arrayWithArray:channelList_];
    
}

@end


#pragma mark -

@implementation ChannelList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearChannelListData {
    
    [channelList_ removeAllObjects];
    
    [auxChannel_ release];
    auxChannel_ = nil;
    
}


@end
