//
//  FOPlace.m
//  NXT_Peru_iPad
//
//  Created by Jose Luis on 23/01/14.
//
//

#import "FOPhone.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    fopxeas_AnalyzingCode = serxeas_ElementsCount, //!<Analyzing the code
    fopxeas_AnalyzingPhoneNumber, //!<Analyzing the phone number
    fopxeas_AnalyzingCarrier //!<Analyzing the carrier
    
    
} FOPlaceXMLElementAnalyzerState;


@interface FOPhone(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPhoneData;

@end


@implementation FOPhone

#pragma mark-
#pragma mark Properties

@synthesize code = code_;
@synthesize phoneNumber = phoneNumber_;
@synthesize carrier = carrier_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPhoneData];
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPhoneData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"codigo"]) {
            
            xmlAnalysisCurrentValue_ = fopxeas_AnalyzingCode;
            
        } else if ([lname isEqualToString:@"nrocelular"]){
            
            xmlAnalysisCurrentValue_ = fopxeas_AnalyzingPhoneNumber;
            
        }  else if ([lname isEqualToString:@"operadora"]){
            
            xmlAnalysisCurrentValue_ = fopxeas_AnalyzingCarrier;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}



/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fopxeas_AnalyzingCode) {
            code_ = nil;
            code_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == fopxeas_AnalyzingPhoneNumber)
        {
            phoneNumber_ = nil;
            phoneNumber_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == fopxeas_AnalyzingCarrier)
        {
            carrier_ = nil;
            carrier_ = [elementString copyWithZone:self.zone];
        }
    }
    else
    {
        informationUpdated_ = soie_InfoAvailable;
    }
}


@end

#pragma mark -

@implementation FOPhone(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPhoneData {
    
    [code_ release];
    code_ = nil;
    
    [phoneNumber_ release];
    phoneNumber_ = nil;
    
    [carrier_ release];
    carrier_ = nil;
	
}

@end
