//
//  FODestinationBankList.m
//  NXT_Peru_iPad
//
//  Created by Jose Luis on 23/01/14.
//
//

#import "FODestinationBankList.h"
#import "FODestinationBank.h"

#pragma mark -
/**
 * TransactionDetailResponse private category
 */
@interface FODestinationBankList(private)
/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFODestinationBankListData;

@end

#pragma mark -

@implementation FODestinationBankList



#pragma mark -
#pragma mark Properties

@dynamic foDestinationBankList;

#pragma mark -
#pragma mark Memory management


/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFODestinationBankListData];
    
    auxFODestinationBank_ = nil;
    foDestinationBankList_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Initialization

/**
 * Object initializer
 *
 * @return An initialized instance
 */
- (id) init {
    
	if (self = [super init]) {
        
        foDestinationBankList_ = [[NSMutableArray alloc] init];
        
	}
	
	return self;
    
}


#pragma mark -
#pragma mark Information management

/*
 * Updates the account transaction list from another account transaction list
 */
- (void)updateFrom:(FODestinationBankList *)aFODestinationBankList {
	
    NSArray *destinationBanks = aFODestinationBankList.foDestinationBankList;
	
	[self clearFODestinationBankListData];
	
	if (destinationBanks != nil) {
        
		for (FODestinationBank *destinationBank in destinationBanks) {
            
            [foDestinationBankList_ addObject:destinationBank];
            
		}
        
	}
	
    [self updateFromStatusEnabledResponse:aFODestinationBankList];
	
	self.informationUpdated = aFODestinationBankList.informationUpdated;
	
}

/*
 * Remove the contained data
 */
- (void)removeData {
    
	[self clearFODestinationBankListData];
    
}


/*
 * Returns the AccountTransaction located at the given position, or nil if position is not valid
 */
- (FODestinationBank *)foDestinationBankAtPosition:(NSUInteger)aPosition {
    
	FODestinationBank *result = nil;
	
	if (aPosition < [foDestinationBankList_ count]) {
        
		result = [foDestinationBankList_ objectAtIndex:aPosition];
        
	}
	
	return result;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFODestinationBankListData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (auxFODestinationBank_ != nil) {
        
        [foDestinationBankList_ addObject:auxFODestinationBank_];
		[auxFODestinationBank_ release];
		auxFODestinationBank_ = nil;
        
	}
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
			[auxFODestinationBank_ release];
            auxFODestinationBank_ = [[FODestinationBank alloc] init];
            auxFODestinationBank_.openingTag = lname;
            [auxFODestinationBank_ setParentParseableObject:self];
            [parser setDelegate:auxFODestinationBank_];
            [auxFODestinationBank_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (auxFODestinationBank_ != nil) {
        
        [foDestinationBankList_ addObject:auxFODestinationBank_];
        [auxFODestinationBank_ release];
        auxFODestinationBank_ = nil;
        
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the account transaction list
 *
 * @return The account transaction list
 */
- (NSArray *)foDestinationBankList {
    
    return [NSArray arrayWithArray:foDestinationBankList_];
    
}

/*
 * Returns the account transactions count
 *
 * @return The account transactions count
 */
- (NSUInteger)foDestinationBankCount {
    
    return [foDestinationBankList_ count];
    
}

@end



#pragma mark -

@implementation FODestinationBankList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFODestinationBankListData {
    
    [foDestinationBankList_ removeAllObjects];
    
    auxFODestinationBank_ = nil;

    
}
@end