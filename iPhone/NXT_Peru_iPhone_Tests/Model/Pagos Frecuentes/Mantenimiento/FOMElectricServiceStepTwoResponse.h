//
//  FOMElectricServiceStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOMElectricServiceStepTwoResponse : FOInitialResponse
{
    @private
    /**
     * Company Name
     */
    NSString *companyName_;
    /**
     * Agreement Name
     */
    NSString *agreementNumber_;
}
/**
 * Provides read-only access to the company name
 */
@property (nonatomic, readonly, retain) NSString *companyName;

/**
 * Provides read-only access to the agreement number
 */
@property (nonatomic, readonly, retain) NSString *agreementNumber;


@end
