//
//  FOMCOtherBankPaymentStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOMCOtherBankPaymentStepTwoResponse.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fomcobpstwrxeas_AnalyzingCardType =  foirxeas_ElementsCount,
    fomcobpstwrxeas_AnalyzingCardNumber,
    fomcobpstwrxeas_AnalyzingHolderName,
    fomcobpstwrxeas_AnalyzingEmisionPlace,
    fomcobpstwrxeas_AnalyzingDestinationBank
    
    
} FOMCOtherBankPaymentStepTwoResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * FOMCOtherBankPaymentStepTwoResponse private category
 */
@interface FOMCOtherBankPaymentStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOMCOtherBankPaymentStepTwoResponseData;

@end


#pragma mark -
@implementation FOMCOtherBankPaymentStepTwoResponse

@synthesize cardType=cardType_;
@synthesize cardNumber= cardNumber_;
@synthesize holderName=holderName_;
@synthesize emisionPlace=emisionPlace_;
@synthesize destinationBank=destinationBank_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOMCOtherBankPaymentStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOMCOtherBankPaymentStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOMCOtherBankPaymentStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOMCOtherBankPaymentStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOMCOtherBankPaymentStepTwoResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"tipotarjeta"]) {
            
            xmlAnalysisCurrentValue_ = fomcobpstwrxeas_AnalyzingCardType;
            
        }else if ([lname isEqualToString:@"numerotarjeta"])
        {
            xmlAnalysisCurrentValue_ = fomcobpstwrxeas_AnalyzingCardNumber;
            
        }
        else if ([lname isEqualToString:@"nombretitular"])
        {
            xmlAnalysisCurrentValue_ = fomcobpstwrxeas_AnalyzingHolderName;
            
        }
        else if ([lname isEqualToString:@"lugaremision"])
        {
            xmlAnalysisCurrentValue_ = fomcobpstwrxeas_AnalyzingEmisionPlace;
            
        }
        else if ([lname isEqualToString:@"bancodestino"])
        {
            xmlAnalysisCurrentValue_ = fomcobpstwrxeas_AnalyzingDestinationBank;
            
        }
        else{
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fomcobpstwrxeas_AnalyzingCardType ) {
            
            [cardType_ release];
            cardType_ = nil;
            cardType_ = [elementString copyWithZone:self.zone];
            
        }
        else if (xmlAnalysisCurrentValue_ == fomcobpstwrxeas_AnalyzingCardNumber) {
            
            [cardNumber_ release];
            cardNumber_ = nil;
            cardNumber_ = [elementString  copyWithZone:self.zone];
            
        }
        else if (xmlAnalysisCurrentValue_ == fomcobpstwrxeas_AnalyzingHolderName) {
            
            [holderName_ release];
            holderName_ = nil;
            holderName_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == fomcobpstwrxeas_AnalyzingEmisionPlace) {
            
            [emisionPlace_ release];
            emisionPlace_ = nil;
            emisionPlace_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fomcobpstwrxeas_AnalyzingDestinationBank) {
            
            [destinationBank_ release];
            destinationBank_ = nil;
            destinationBank_ = [elementString  copyWithZone:self.zone];
            
        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOMCOtherBankPaymentStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOMCOtherBankPaymentStepTwoResponseData {
    
    [cardType_ release];
    cardType_ = nil;
    
    [cardNumber_ release];
    cardNumber_ = nil;
    
    [holderName_ release];
    holderName_ = nil;
    
    [emisionPlace_ release];
    emisionPlace_ = nil;
    
    [destinationBank_ release];
    destinationBank_ = nil;
    
}



@end