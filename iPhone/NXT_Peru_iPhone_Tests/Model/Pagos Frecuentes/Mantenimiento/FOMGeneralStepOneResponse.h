//
//  FOMGeneralStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@interface FOMGeneralStepOneResponse : StatusEnabledResponse
{
@private
    /**
     * coordinate
     */
    NSString *coordinate_;
}
/**
 * Provides read-only access to the coordinate
 */
@property (nonatomic, readonly, retain) NSString *coordinate;
@end
