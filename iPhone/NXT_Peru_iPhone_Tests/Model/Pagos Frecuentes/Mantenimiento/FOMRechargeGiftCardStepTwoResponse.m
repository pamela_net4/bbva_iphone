//
//  FOMRechargeGiftCardStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOMRechargeGiftCardStepTwoResponse.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fomrgcstwrxeas_AnalyzingGiftCard =  foirxeas_ElementsCount,

    
    
} FOMRechargeGiftCardStepTwoResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * FOMRechargeGiftCardStepTwoResponse private category
 */
@interface FOMRechargeGiftCardStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOMRechargeGiftCardStepTwoResponseData;

@end


#pragma mark -


@implementation FOMRechargeGiftCardStepTwoResponse

@synthesize giftCard =giftCard_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOMRechargeGiftCardStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOMRechargeGiftCardStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOMRechargeGiftCardStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOMRechargeGiftCardStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOMRechargeGiftCardStepTwoResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"tarjetaregalo"]) {
            
            xmlAnalysisCurrentValue_ = fomrgcstwrxeas_AnalyzingGiftCard;
            
        }
        else{
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fomrgcstwrxeas_AnalyzingGiftCard ) {
            
            [giftCard_ release];
            giftCard_ = nil;
            giftCard_ = [elementString copyWithZone:self.zone];
            
        }
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOMRechargeGiftCardStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOMRechargeGiftCardStepTwoResponseData {
    
    [giftCard_ release];
    giftCard_ = nil;
    
}



@end
