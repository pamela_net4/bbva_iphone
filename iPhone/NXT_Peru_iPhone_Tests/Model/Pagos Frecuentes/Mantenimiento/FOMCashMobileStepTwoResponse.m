//
//  FOMCashMobileStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOMCashMobileStepTwoResponse.h"


/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fomcmstwrxeas_AnalyzingChargeAccount =  foirxeas_ElementsCount,
    fomcmstwrxeas_AnalyzingRecipentCell,
    fomcmstwrxeas_AnalyzingAmount
    
    
} FOMCashMobileStepTwoResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOMCashMobileStepTwoResponse private category
 */
@interface FOMCashMobileStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOMCashMobileStepTwoResponseData;

@end


#pragma mark -

@implementation FOMCashMobileStepTwoResponse

@synthesize chargeAccount = chargeAccount_;
@synthesize amount = amount_;
@synthesize recipentCell = recipentCell_;



#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOMCashMobileStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOMCashMobileStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOMCashMobileStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOMCashMobileStepTwoResponse;
        
    }
	
	return self;
    
}


#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOMCashMobileStepTwoResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"cuentacargo"])
        {
            xmlAnalysisCurrentValue_ = fomcmstwrxeas_AnalyzingChargeAccount;
            
        } else if ([lname isEqualToString:@"monto"])
        {
            xmlAnalysisCurrentValue_ = fomcmstwrxeas_AnalyzingAmount;
            
        } else if ([lname isEqualToString:@"celularbeneficiario"])
        {
            xmlAnalysisCurrentValue_ = fomcmstwrxeas_AnalyzingRecipentCell;
            
        }
        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
     if (xmlAnalysisCurrentValue_ == fomcmstwrxeas_AnalyzingChargeAccount) {
            
            [chargeAccount_ release];
            chargeAccount_ = nil;
            chargeAccount_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fomcmstwrxeas_AnalyzingAmount) {
            
            [amount_ release];
            amount_ = nil;
            amount_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fomcmstwrxeas_AnalyzingRecipentCell) {
            
            [recipentCell_ release];
            recipentCell_ = nil;
            recipentCell_ = [elementString  copyWithZone:self.zone];
            
        }
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOMCashMobileStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOMCashMobileStepTwoResponseData {
    

    [chargeAccount_ release];
    chargeAccount_ = nil;
    [amount_ release];
    amount_ = nil;
    [recipentCell_ release];
    recipentCell_ = nil;
    
    
}











@end
