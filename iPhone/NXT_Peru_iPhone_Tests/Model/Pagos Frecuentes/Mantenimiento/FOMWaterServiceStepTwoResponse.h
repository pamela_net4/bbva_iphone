//
//  FOMWaterServiceStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOMWaterServiceStepTwoResponse : FOInitialResponse
{
    @private
    /*
     * Supply
     */
    NSString *supply_;
    /*
     * Company
     */
    NSString *company_;
    /*
     * Service Type
     */
    NSString *serviceType_;
}
/**
 * Provides read-only access to the supply
 */
@property (nonatomic, readonly, retain) NSString *supply;
/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, retain) NSString *company;
/**
 * Provides read-only access to the service type
 */
@property (nonatomic, readonly, retain) NSString *serviceType;

@end
