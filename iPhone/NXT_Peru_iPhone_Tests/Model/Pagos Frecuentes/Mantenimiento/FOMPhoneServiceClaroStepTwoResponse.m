//
//  FOMPhoneServiceClaroStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOMPhoneServiceClaroStepTwoResponse.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    
    fompscstwrxeas_AnalyzingBusiness = foirxeas_ElementsCount, //!<Analyzing the channels list
    fompscstwrxeas_AnalyzingTelephone
    
    
    
    
} FOMPhoneServiceClaroStepTwoResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOMPhoneServiceClaroStepTwoResponse private category
 */
@interface FOMPhoneServiceClaroStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOMPhoneServiceClaroStepTwoResponseData;

@end

@implementation FOMPhoneServiceClaroStepTwoResponse



#pragma mark-
#pragma mark Properties

@synthesize businessName = businessName_;
@synthesize telephone = telephone_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOMPhoneServiceClaroStepTwoResponseData];
    [super dealloc];
    
}
#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOMPhoneServiceClaroStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOMPhoneServiceClaroStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationFOMPhoneServiceClaroStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOMPhoneServiceClaroStepTwoResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"empresa"])
        {
            xmlAnalysisCurrentValue_ = fompscstwrxeas_AnalyzingBusiness;
        }
        else if ([lname isEqualToString: @"telefono"])
        {
            xmlAnalysisCurrentValue_ = fompscstwrxeas_AnalyzingTelephone;
        }
        else
        {
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fompscstwrxeas_AnalyzingBusiness)
        {
            [businessName_ release];
            businessName_ = nil;
            businessName_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == fompscstwrxeas_AnalyzingTelephone)
        {
            telephone_ = nil;
            telephone_ = [elementString copyWithZone:self.zone];;
        }
        
    }
    else
    {
        informationUpdated_ = soie_InfoAvailable;
    }
    
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}




@end

#pragma mark -

@implementation FOMPhoneServiceClaroStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOMPhoneServiceClaroStepTwoResponseData {
    
    [businessName_ release];
    businessName_ = nil;
    
    [telephone_ release];
    telephone_ = nil;

    
    
    
}


@end
