//
//  FOMPhoneServiceMovistarStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOMPhoneServiceMovistarStepTwoResponse.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fompsmstwrxeas_AnalyzingBusinessName =  foirxeas_ElementsCount,
    fompsmstwrxeas_AnalyzingTypeService,
    fompsmstwrxeas_AnalyzingTelephone,
    fompsmstwrxeas_AnalyzingNameHolder
    
    
} FOMPhoneServiceMovistarStepTwoResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * FOMPhoneServiceMovistarStepTwoResponse private category
 */
@interface FOMPhoneServiceMovistarStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOMPhoneServiceMovistarStepTwoResponseData;

@end


#pragma mark -


@implementation FOMPhoneServiceMovistarStepTwoResponse

@synthesize businessName = businessName_;
@synthesize typeService = typeService_;
@synthesize telephone = telephone_;
@synthesize nameHolder = nameHolder_;
#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOMPhoneServiceMovistarStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOMPhoneServiceMovistarStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOMPhoneServiceMovistarStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOMPhoneServiceMovistarStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOMPhoneServiceMovistarStepTwoResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"nombreempresa"]) {
            
            xmlAnalysisCurrentValue_ = fompsmstwrxeas_AnalyzingBusinessName;
            
        }else if ([lname isEqualToString:@"tiposervicio"])
        {
            xmlAnalysisCurrentValue_ = fompsmstwrxeas_AnalyzingTypeService;
            
        }
        else if ([lname isEqualToString:@"telefonocliente"])
        {
            xmlAnalysisCurrentValue_ = fompsmstwrxeas_AnalyzingTelephone;
            
        }
        else if ([lname isEqualToString:@"titular"])
        {
            xmlAnalysisCurrentValue_ = fompsmstwrxeas_AnalyzingNameHolder;
            
        }
        else{
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fompsmstwrxeas_AnalyzingBusinessName ) {
            
            [businessName_ release];
            businessName_ = nil;
            businessName_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fompsmstwrxeas_AnalyzingTelephone) {
            
            [telephone_ release];
            telephone_ = nil;
            telephone_ = [elementString  copyWithZone:self.zone];
            
        }
        else if (xmlAnalysisCurrentValue_ == fompsmstwrxeas_AnalyzingTypeService) {
            
            [typeService_ release];
            typeService_ = nil;
            typeService_ = [elementString  copyWithZone:self.zone];
            
        }
        else if (xmlAnalysisCurrentValue_ == fompsmstwrxeas_AnalyzingNameHolder) {
            
            [nameHolder_ release];
            nameHolder_ = nil;
            nameHolder_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOMPhoneServiceMovistarStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOMPhoneServiceMovistarStepTwoResponseData {
    
    [businessName_ release];
    businessName_ = nil;
    
    [telephone_ release];
    telephone_ = nil;
    
    [typeService_ release];
    typeService_ = nil;
    
    [nameHolder_ release];
    nameHolder_ = nil;
    
}


@end
