//
//  FOMThirdAccountTransferStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOMThirdAccountTransferStepTwoResponse : FOInitialResponse
{
@private
    /**
     * account Payment
     */
    NSString *accountPayment_;
    /**
     * destination bank account
     */
    NSString *destinationBankAccount_;
    /**
     * name holder
     */
    NSString *nameHolder_;
    
    
}

/**
 * Provides read-only access to the account payment
 */
@property (nonatomic, readonly, retain) NSString *accountPayment;
/**
 * Provides read-only access to the destination Bank Account
 */
@property (nonatomic, readonly, retain) NSString *destinationBankAccount;
/**
 * Provides read-only access to the name holder
 */
@property (nonatomic, readonly, retain) NSString *nameHolder;

@end
