//
//  FOMRechargeGiftCardStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@class ChannelList;

@interface FOMRechargeGiftCardStepTwoResponse : FOInitialResponse
{
    @private
    /**
     * giftCard
     */
    NSString *giftCard_;

}

/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, retain) NSString *giftCard;



@end
