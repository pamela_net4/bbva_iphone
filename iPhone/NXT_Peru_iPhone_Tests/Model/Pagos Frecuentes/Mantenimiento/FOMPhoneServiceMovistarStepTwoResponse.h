//
//  FOMPhoneServiceMovistarStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOMPhoneServiceMovistarStepTwoResponse : FOInitialResponse
{
@private
    /**
     * business Name
     */
    NSString *businessName_;
    /**
     * name Holder
     */
    NSString *typeService_;
    /**
     * telephone
     */
    NSString *telephone_;
    /**
     * name Holder
     */
    NSString *nameHolder_;
}


/**
 * Provides read-only access to the business name
 */
@property (nonatomic, readonly, retain) NSString *businessName;
/**
 * Provides read-only access to the telephone
 */
@property (nonatomic, readonly, retain) NSString *telephone;
/**
 * Provides read-only access to the name holder
 */
@property (nonatomic, readonly, retain) NSString *nameHolder;
/**
 * Provides read-only access to the type service
 */
@property (nonatomic, readonly, retain) NSString *typeService;

@end
