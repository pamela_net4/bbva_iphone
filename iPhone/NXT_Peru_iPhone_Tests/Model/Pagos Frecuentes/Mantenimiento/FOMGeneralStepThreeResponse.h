//
//  FOMCashMobileStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOMGeneralStepThreeResponse : FOInitialResponse
{
@private
    /**
     * operation
     */
    NSString *operation_;
}
/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, retain) NSString *operation;

@end



