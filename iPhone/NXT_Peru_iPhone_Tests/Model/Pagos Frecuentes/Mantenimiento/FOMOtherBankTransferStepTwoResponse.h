//
//  FOMOtherBankTransferStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOMOtherBankTransferStepTwoResponse : FOInitialResponse
{
@private
    /**
     * account Payment
     */
    NSString *accountPayment_;
    /**
     * own account
     */
    NSString *ownAccount_;
    /**
     * destination bank
     */
    NSString *destinationBank_;
    /**
     * beneficiary Name
     */
    NSString *beneficiaryName_;
    /**
     * description document type
     */
    NSString *documentType_;
    /**
     * document number
     */
    NSString *documentNumber_;
}
/**
 * Provides read-only access to the own account
 */
@property (nonatomic, readonly, retain) NSString *ownAccount;
/**
 * Provides read-only access to the account Payment
 */
@property (nonatomic, readonly, retain) NSString *accountPayment;
/**
 * Provides read-only access to the beneficiary name
 */
@property (nonatomic, readonly, retain) NSString *beneficiaryName;
/**
 * Provides read-only access to the destination bank
 */
@property (nonatomic, readonly, retain) NSString *destinationBank;
/**
 * Provides read-only access to the docuent type
 */
@property (nonatomic, readonly, retain) NSString *documentType;
/**
 * Provides read-only access to the document number
 */
@property (nonatomic, readonly, retain) NSString *documentNumber;



@end
