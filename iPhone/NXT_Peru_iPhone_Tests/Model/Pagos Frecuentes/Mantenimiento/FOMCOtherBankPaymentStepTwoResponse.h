//
//  FOMCOtherBankPaymentStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOMCOtherBankPaymentStepTwoResponse : FOInitialResponse
{
    @private
    NSString *cardType_;
    NSString *cardNumber_;
    NSString *holderName_;
    NSString *emisionPlace_;
    NSString *destinationBank_;
}
/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, retain) NSString *cardType;
/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, retain) NSString *cardNumber;
/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, retain) NSString *holderName;
/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, retain) NSString *emisionPlace;
/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, retain) NSString *destinationBank;

@end
