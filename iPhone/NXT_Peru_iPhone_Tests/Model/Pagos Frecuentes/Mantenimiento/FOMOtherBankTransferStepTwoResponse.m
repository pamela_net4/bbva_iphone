//
//  FOMOtherBankTransferStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOMOtherBankTransferStepTwoResponse.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fomobtstwrxeas_AnalyzingownAccount =  foirxeas_ElementsCount,
    fomobtstwrxeas_AnalyzingAccountPayment,
    fomobtstwrxeas_AnalyzingDestinationBank,
    fomobtstwrxeas_AnalyzingBeneficiaryName,
    fomobtstwrxeas_AnalyzingDocumentType,
    fomobtstwrxeas_AnalyzingDocumentNumber
    
    
} FOMOtherBankTransferStepTwoResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOMOtherBankTransferStepTwoResponse private category
 */
@interface FOMOtherBankTransferStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOMOtherBankTransferStepTwoResponseData;

@end


#pragma mark -


@implementation FOMOtherBankTransferStepTwoResponse



@synthesize ownAccount = ownAccount_;
@synthesize destinationBank = destinationBank_;
@synthesize beneficiaryName = beneficiaryName_;
@synthesize accountPayment = accountPayment_;
@synthesize documentNumber = documentNumber_;
@synthesize documentType = documentType_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOMOtherBankTransferStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOMOtherBankTransferStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOMOtherBankTransferStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOMOtherBankTransferStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOMOtherBankTransferStepTwoResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"operacion"]) {
            
            xmlAnalysisCurrentValue_ = fomobtstwrxeas_AnalyzingownAccount;
            
            
        }  else if ([lname isEqualToString:@"ctabancodestino"])
        {
            xmlAnalysisCurrentValue_ = fomobtstwrxeas_AnalyzingDestinationBank;
            
        } else if ([lname isEqualToString:@"cuentaabono"])
        {
            xmlAnalysisCurrentValue_ = fomobtstwrxeas_AnalyzingAccountPayment;
            
        } else if ([lname isEqualToString:@"nombrebeneficiario"])
        {
            xmlAnalysisCurrentValue_ = fomobtstwrxeas_AnalyzingBeneficiaryName;
            
        }
        else if ([lname isEqualToString:@"nrodoc"])
        {
            xmlAnalysisCurrentValue_ = fomobtstwrxeas_AnalyzingDocumentNumber;
            
        } else if ([lname isEqualToString:@"desctipodoc"])
        {
            xmlAnalysisCurrentValue_ = fomobtstwrxeas_AnalyzingDocumentType;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}




/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fomobtstwrxeas_AnalyzingownAccount ) {
            
            [ownAccount_ release];
            ownAccount_ = nil;
            ownAccount_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == fomobtstwrxeas_AnalyzingDestinationBank) {
            
            [destinationBank_ release];
            destinationBank_ = nil;
            destinationBank_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fomobtstwrxeas_AnalyzingBeneficiaryName) {
            
            [beneficiaryName_ release];
            beneficiaryName_ = nil;
            beneficiaryName_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fomobtstwrxeas_AnalyzingAccountPayment) {
            
            [accountPayment_ release];
            accountPayment_ = nil;
            accountPayment_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fomobtstwrxeas_AnalyzingDocumentNumber) {
            
            [documentNumber_ release];
            documentNumber_ = nil;
            documentNumber_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fomobtstwrxeas_AnalyzingDocumentType) {
            
            [documentType_ release];
            documentType_ = nil;
            documentType_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOMOtherBankTransferStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOMOtherBankTransferStepTwoResponseData {
    
    [ownAccount_ release];
    ownAccount_ = nil;
    [destinationBank_ release];
    destinationBank_ = nil;
    [beneficiaryName_ release];
    beneficiaryName_ = nil;
    [accountPayment_ release];
    accountPayment_ = nil;
    [documentNumber_ release];
    documentNumber_ = nil;
    [documentType_ release];
    documentType_ = nil;
    
    
}


@end
