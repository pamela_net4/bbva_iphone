//
//  FOMPhoneServiceClaroStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOMPhoneServiceClaroStepTwoResponse : FOInitialResponse
{
@private
    /**
     * business Name
     */
    NSString *businessName_;
    /**
     * telephone
     */
    NSString *telephone_;
}


/**
 * Provides read-only access to the business name
 */
@property (nonatomic, readonly, retain) NSString *businessName;
/**
 * Provides read-only access to the telephone
 */
@property (nonatomic, readonly, retain) NSString *telephone;

@end
