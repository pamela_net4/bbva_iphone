//
//  FOMInstitutionPaymentStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOMInstitutionPaymentStepTwoResponse : FOInitialResponse
{
@private
    /**
     * Company
     */
    NSString *company_;
    
    /**
     * SDS
     */
    NSString *data_;
}
/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, retain) NSString *company;
/**
 * Provides read-only access to the sds
 */
@property (nonatomic, readonly, retain) NSString *data;

@end
