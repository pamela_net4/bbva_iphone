//
//  FOMThirdCardPaymentStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOMThirdCardPaymentStepTwoResponse : FOInitialResponse
{
    @private
    NSString *cardType_;
    NSString *cardNumber_;
    NSString *cardCurrency_;
    NSString *holderName_;
}
/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, retain) NSString *cardType;

/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, retain) NSString *cardNumber;

/**
 * Provides read-only access to the card currency
 */
@property (nonatomic, readonly, retain) NSString *cardCurrency;

/**
 * Provides read-only access to the holder name
 */
@property (nonatomic, readonly, retain) NSString *holderName;

@end
