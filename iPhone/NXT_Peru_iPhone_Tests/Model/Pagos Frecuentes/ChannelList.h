//
//  ChannelList.h
//  NXT_Peru_iPad
//
//  Created by usuario on 23/01/14.
//
//

#import "StatusEnabledResponse.h"

//Forward declarations
@class Channel;


@interface ChannelList : StatusEnabledResponse
{
@private
    
    /**
	 * Channel array list
	 */
	NSMutableArray *channelList_;
    
	/**
	 * Auxiliar channel instance for parsing
	 */
	Channel *auxChannel_;
    
}

/**
 * Provides read-only access to the channel count
 */
@property (nonatomic, readonly, assign) NSUInteger channelCount;

/**
 * Provides read-only access to the channel array list
 */
@property (nonatomic, readonly, retain) NSArray *channelsList;


/**
 * Updates the Channel list from another Channel list
 *
 * @param aChannelList The ChannelList instance to update from
 */
- (void)updateFrom:(ChannelList *)aChannelList;

/**
 * Remove the contained data
 */
- (void)removeData;

/**
 * Returns the Channel located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the Channel is located at
 * @return Channel located at the given position, or nil if position is not valid
 */
- (Channel *)chanelAtPosition:(NSUInteger)aPosition;

@end
