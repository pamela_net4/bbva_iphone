//
//  FOData.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/30/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@interface FOData : StatusEnabledResponse
{
@private
    NSString *field_;
    NSString *value_;
}
@property (nonatomic, readonly, copy) NSString* field;
@property (nonatomic, readonly, copy) NSString* value;
@end