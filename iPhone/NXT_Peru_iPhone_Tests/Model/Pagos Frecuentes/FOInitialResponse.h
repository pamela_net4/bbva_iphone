//
//  FOInitialResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/23/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    foirxeas_AnalyzingChannelList  =  serxeas_ElementsCount, //!<Analyzing the channel list
    foirxeas_AnalyzingNickname,
    foirxeas_AnalyzingShortNickname,
    foirxeas_AnalyzingDayNotice,
    foirxeas_AnalyzingSMSMobile,
    foirxeas_AnalyzingSMSEmail,
    foirxeas_AnalyzingService,
    foirxeas_ElementsCount //!<last element
    
} FOInitialResponseXMLElementAnalyzerState;

@class ChannelList;

@interface FOInitialResponse : StatusEnabledResponse
{
@private
    
    /**
     * Channel list
     */
    ChannelList *channelList_;
    /**
     * service
     */
    NSString *service_;
    /**
     * nickname
     */
    NSString *nickname_;
    /**
     * short nickname
     */
    NSString *shortNickname_;
    /**
     * day Notice
     */
    NSString *dayNotice_;
    /**
     * sms Mobile
     */
    NSString *smsMobile_;
    /**
     * sms Email
     */
    NSString *smsEmail_;

    
    
}


/**
 * Provides read-only access to the Channel list
 */
@property (nonatomic, readonly, retain) ChannelList *channelList;
/**
 * Provides read-only access to the service
 */
@property (nonatomic, readonly, retain) NSString *service;
/**
 * Provides read-only access to the nickname
 */
@property (nonatomic, readonly, retain) NSString *nickname;
/**
 * Provides read-only access to the short nickname
 */
@property (nonatomic, readonly, retain) NSString *shortNickname;
/**
 * Provides read-only access to the day notice
 */
@property (nonatomic, readonly, retain) NSString *dayNotice;
/**
 * Provides read-only access to the sms Mobile
 */
@property (nonatomic, readonly, retain) NSString *smsMobile;
/**
 * Provides read-only access to the sms Email
 */
@property (nonatomic, readonly, retain) NSString *smsEmail;


@end
