//
//  Acount.m
//  NXT_Peru_iPad
//
//  Created by usuario on 23/01/14.
//
//

#import "Account.h"
#import "Tools.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    axeas_AnalyzingCurrency = serxeas_ElementsCount,
    axeas_AnalyzingAccountType,
    axeas_AnalyzingSubject,
    axeas_AnalyzingBalanceAvailable,
    axeas_AnalyzingBadge,
    axeas_AnalyzingParameter
    
} AccountXMLElementAnalyzerState;

#pragma mark -

/**
 * Account private category
 */
@interface Account(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearAccountData;

@end

@implementation Account

#pragma mark -
#pragma mark Properties


@synthesize currency = currency_;
@synthesize accountType = accountType_;
@synthesize subject = subject_;
@synthesize balanceAvailable = balanceAvailable_;
@synthesize availableBalanceString = availableBalanceString_;
@synthesize badge = badge_;
@synthesize param = param_;
@dynamic accountIdAndDescription;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearAccountData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearAccountData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"moneda"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingCurrency;
            
        } else if ([lname isEqualToString:@"tipocta"]){
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingAccountType;
            
        } else if ([lname isEqualToString:@"asunto"]){
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingSubject;
            
        }  else if ([lname isEqualToString:@"saldodisponible"]){
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingBalanceAvailable;
            
        }  else if ([lname isEqualToString:@"divsaldodisponible"]){
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingBadge;
            
        }  else if ([lname isEqualToString:@"paramcuenta"]){
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingParameter;
            
        }  else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}


/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == axeas_AnalyzingCurrency) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];
            currency_=elementString;
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingAccountType) {
            
            [accountType_ release];
            accountType_ = nil;
            accountType_ = [elementString copyWithZone:self.zone];

        }  else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingSubject) {
            
            [subject_ release];
            subject_ = nil;
            subject_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingBalanceAvailable) {
            
            [balanceAvailable_ release];
            balanceAvailable_ = nil;
            balanceAvailable_ = [elementString copyWithZone:self.zone];
            
            [availableBalanceString_ release];
            availableBalanceString_ = nil;
            availableBalanceString_ = [elementString copyWithZone:self.zone];

        }  else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingBadge) {
            
            [badge_ release];
            badge_ = nil;
            badge_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingParameter) {
            
            [param_ release];
            param_ = nil;
            param_ = [elementString copyWithZone:self.zone];
            
        }
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark - propertie selector
/*
 * Returns the account identification plus the description
 *
 * @return The account identification plus the description
 */
- (NSString *)accountIdAndDescription {
    
    NSString *result = @"";
    
    NSString *accountNumber = subject_;
    
    if (accountNumber == nil) {
        accountNumber = subject_;
    }
    
    result = [NSString stringWithFormat:@"%@ %@ - Disp. %@ %@", accountType_, [Tools obfuscateAccountNumber:accountNumber], [Tools getCurrencySimbol:currency_], balanceAvailable_];
    
    return result;
    
}

@end


#pragma mark -

@implementation Account(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearAccountData {
    
    [currency_ release];
    currency_ = nil;
    
    [accountType_ release];
    accountType_ = nil;
	
    [param_ release];
    param_ = nil;
    
    [availableBalanceString_ release];
    availableBalanceString_ = nil;
}


@end
