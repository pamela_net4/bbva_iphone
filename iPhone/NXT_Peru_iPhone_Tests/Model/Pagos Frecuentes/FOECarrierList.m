//
//  FOECarrierList.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 2/05/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOECarrierList.h"

@interface FOECarrierList(Private)

-(void) clearCarrierListData;

@end

@implementation FOECarrierList

@dynamic carrierList;

-(void) dealloc{

    [self clearCarrierListData];
    
    [super dealloc];

}

-(id) init {
    
    if(self == [super init]){
        
        carrierList_ = [[NSMutableArray alloc] init];
        
    }
    
    return self;
}

-(NSArray *) carrierList{
    
    return [NSArray arrayWithArray: carrierList_];
}

-(NSUInteger *) carrierListCount{
    
    return [carrierList_ count];
}


-(void) removeData{
    
    [self clearCarrierListData];

}

-(void) clearCarrierListData{

    [carrierList_ removeAllObjects];
    [carrierAux_ release];
    carrierAux_ = nil;
}

-(void) parserDidStartDocument:(NSXMLParser *)parser{

    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearCarrierListData];
}

-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if (carrierAux_ != nil) {
        [carrierList_ addObject: carrierAux_];
        [carrierAux_ release];
        carrierAux_ = nil;
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing){
    
        NSString *lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
            [carrierAux_ release];
            carrierAux_ = [[FOECarrier alloc] init];
            [carrierAux_ setParentParseableObject:self];
            carrierAux_.openingTag = lname;
            [parser setDelegate:carrierAux_];
            [carrierAux_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
    }
}

-(void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if(carrierAux_ != nil){
        [carrierList_ addObject:carrierAux_];
        [carrierAux_ release];
        carrierAux_ = nil;
    }
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        informationUpdated_ = soie_InfoAvailable;
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
}

@end
