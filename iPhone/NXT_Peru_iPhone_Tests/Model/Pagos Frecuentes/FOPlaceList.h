//
//  FOPlaceList.h
//  NXT_Peru_iPad
//
//  Created by Jose Luis on 23/01/14.
//
//

#import "StatusEnabledResponse.h"
@class FOPlace;

@interface FOPlaceList : StatusEnabledResponse
{
    @private
    FOPlace *auxFOPlace_;
    NSMutableArray *foPlaceArray_;
}

@property(nonatomic,readonly,retain) NSMutableArray *foPlaceArray;

/**
 * Provides read-only access to the Places count
 */
@property(nonatomic,readonly,assign) NSUInteger foPlaceCount;

/**
 * Returns the FOPlace located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the AccountTransaction is located at
 * @return FOPlace located at the given position, or nil if position is not valid
 */
- (FOPlace *)foPlaceAtPosition:(NSUInteger)aPosition;

/**
 * Remove the contained data
 */
- (void)removeData;

@end