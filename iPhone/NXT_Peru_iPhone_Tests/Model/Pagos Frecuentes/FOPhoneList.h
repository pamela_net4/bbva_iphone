//
//  FOPlaceList.h
//  NXT_Peru_iPad
//
//  Created by Jose Luis on 23/01/14.
//
//

#import "StatusEnabledResponse.h"
@class FOPhone;

@interface FOPhoneList : StatusEnabledResponse
{
    @private
    FOPhone *auxFOPhone_;
    NSMutableArray *foPhoneArray_;
}

@property(nonatomic,readonly,retain) NSMutableArray *foPhoneArray;

/**
 * Provides read-only access to the Places count
 */
@property(nonatomic,readonly,assign) NSUInteger foPhoneCount;

/**
 * Returns the FOPhone located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the FOPhone is located at
 * @return FOPhone located at the given position, or nil if position is not valid
 */
- (FOPhone *)foPhoneAtPosition:(NSUInteger)aPosition;

/**
 * Remove the contained data
 */
- (void)removeData;

@end