//
//  Channel.m
//  NXT_Peru_iPad
//
//  Created by usuario on 23/01/14.
//
//

#import "Channel.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    cxeas_AnalyzingCode = serxeas_ElementsCount,
    cxeas_AnalyzingDescription
    
} ChannelXMLElementAnalyzerState;

#pragma mark -

/**
 * Channel private category
 */
@interface Channel(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearChannelData;

@end


@implementation Channel



#pragma mark -
#pragma mark Properties
@synthesize code = code_;
@synthesize description = description_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearChannelData];
    
    [super dealloc];
    
}
#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearChannelData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"codigo"]) {
            
            xmlAnalysisCurrentValue_ = cxeas_AnalyzingCode;
            
        } else if ([lname isEqualToString:@"descripcion"]){
            
            xmlAnalysisCurrentValue_ = cxeas_AnalyzingDescription;
            
        }  else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}


/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == cxeas_AnalyzingCode) {
            
            [code_ release];
            code_ = nil;
            code_ = [elementString copyWithZone:self.zone];

        } else if (xmlAnalysisCurrentValue_ == cxeas_AnalyzingDescription) {
            
            [description_ release];
            description_ = nil;
            description_ = [elementString copyWithZone:self.zone];

        }
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}



@end


#pragma mark -

@implementation Channel(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearChannelData {
    
    [code_ release];
    code_ = nil;
    
    [description_ release];
    description_ = nil;
	
}

@end


















