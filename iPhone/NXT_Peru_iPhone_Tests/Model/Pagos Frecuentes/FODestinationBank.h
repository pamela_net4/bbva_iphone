//
//  FODestinationBank.h
//  NXT_Peru_iPad
//
//  Created by Jose Luis on 23/01/14.
//
//

#import "StatusEnabledResponse.h"

@interface FODestinationBank : StatusEnabledResponse
{
@private
    NSString *code_;
    NSString *description_;
}
@property (nonatomic, readonly, copy) NSString* code;
@property (nonatomic, readonly, copy) NSString* description;

@end
