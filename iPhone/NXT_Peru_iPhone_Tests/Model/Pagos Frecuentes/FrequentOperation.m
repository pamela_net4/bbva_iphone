//
//  FrequentOperation.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperation.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    foxeas_AnalyzingOperationCode = serxeas_ElementsCount, //!<Analyzing the operation code
    foxeas_AnalyzingService, //!<Analyzing the service
    foxeas_AnalyzingNickname,
    foxeas_AnalyzingShortNickname,
    foxeas_AnalyzingCellphone,
    foxeas_AnalyzingEmail,
    foxeas_AnalyzingDayMonth,
    foxeas_AnalyzingMemberNumber,
    foxeas_AnalyzingAfilitationForm,
    foxeas_AnalyzingServiceType
    
} FrequentOperationXMLElementAnalyzerState;


@interface FrequentOperation(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFrequentOperationData;

@end

@implementation FrequentOperation

#pragma mark-
#pragma mark Properties

@synthesize operationCode=operationCode_;
@synthesize service=service_;
@synthesize nickname=nickname_;
@synthesize shortNickname=shortNickname_;
@synthesize cellphone=cellphone_;
@synthesize dayMonth=dayMonth_;
@synthesize email=email_;
@synthesize memberNumber=memberNumber_;
@synthesize afilitationForm = afilitationForm_;
@synthesize serviceType = serviceType_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFrequentOperationData];
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFrequentOperationData];
    
}
/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"codoperacion"]) {
            
            xmlAnalysisCurrentValue_ = foxeas_AnalyzingOperationCode;
            
        } else if ([lname isEqualToString:@"servicio"]){
            
            xmlAnalysisCurrentValue_ = foxeas_AnalyzingService;
            
        } else if ([lname isEqualToString:@"alias"]){
            
            xmlAnalysisCurrentValue_ = foxeas_AnalyzingNickname;
            
        } else if ([lname isEqualToString:@"aliascorto"]){
            
            xmlAnalysisCurrentValue_ = foxeas_AnalyzingShortNickname;
            
        } else if ([lname isEqualToString:@"celular"]){
            
            xmlAnalysisCurrentValue_ = foxeas_AnalyzingCellphone;
            
        } else if ([lname isEqualToString:@"diames"]){
            
            xmlAnalysisCurrentValue_ = foxeas_AnalyzingDayMonth;
            
        } else if ([lname isEqualToString:@"email"]){
            
            xmlAnalysisCurrentValue_ = foxeas_AnalyzingEmail;
            
        } else if ([lname isEqualToString:@"numafil"]){
            
            xmlAnalysisCurrentValue_ = foxeas_AnalyzingMemberNumber;
            
        } else if ([lname isEqualToString:@"formafil"]){
            
            xmlAnalysisCurrentValue_ = foxeas_AnalyzingAfilitationForm;
            
        } else if ([lname isEqualToString:@"tiposervicio"]){
            
            xmlAnalysisCurrentValue_ = foxeas_AnalyzingServiceType;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == foxeas_AnalyzingOperationCode) {
            [operationCode_ release];
            operationCode_ = nil;
            operationCode_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == foxeas_AnalyzingService)
        {
            [service_ release];
            service_ = nil;
            service_ =[elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == foxeas_AnalyzingNickname)
        {
            [nickname_ release];
            nickname_ = nil;
            nickname_ =[elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == foxeas_AnalyzingShortNickname)
        {
            [shortNickname_ release];
            shortNickname_ = nil;
            shortNickname_ =[elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == foxeas_AnalyzingCellphone)
        {
            [cellphone_ release];
            cellphone_ = nil;
            cellphone_ =[elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == foxeas_AnalyzingEmail)
        {
            [email_ release];
            email_ = nil;
            email_ =[elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == foxeas_AnalyzingDayMonth)
        {
            [dayMonth_ release];
            dayMonth_ = nil;
            dayMonth_ =[elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == foxeas_AnalyzingMemberNumber)
        {
            [memberNumber_ release];
            memberNumber_ = nil;
            memberNumber_ =[elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == foxeas_AnalyzingAfilitationForm)
        {
            [afilitationForm_ release];
            afilitationForm_ = nil;
            afilitationForm_ =[elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == foxeas_AnalyzingServiceType)
        {
            [serviceType_ release];
            serviceType_ = nil;
            serviceType_ =[elementString copyWithZone:self.zone];
        }
    }
    else
    {
        informationUpdated_ = soie_InfoAvailable;
    }
}


@end

#pragma mark -

@implementation FrequentOperation(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFrequentOperationData {
    
    [operationCode_ release];
    operationCode_ = nil;
    
    [service_ release];
    service_ = nil;
    
    [nickname_ release];
    nickname_ = nil;
    
    [shortNickname_ release];
    shortNickname_ = nil;
    
    [cellphone_ release];
    cellphone_ = nil;
    
    [email_ release];
    email_ = nil;
    
    [dayMonth_ release];
    dayMonth_ = nil;
    
    [memberNumber_ release];
    memberNumber_ = nil;
	
    [afilitationForm_ release];
    afilitationForm_ = nil;
}

@end
