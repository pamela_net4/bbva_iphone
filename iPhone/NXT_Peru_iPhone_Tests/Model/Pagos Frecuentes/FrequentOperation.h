//
//  FrequentOperation.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@interface FrequentOperation : StatusEnabledResponse
{
    @private
    NSString *operationCode_;
    NSString *service_;
    NSString *nickname_;
    NSString *shortNickname_;
    NSString *cellphone_;
    NSString *dayMonth_;
    NSString *email_;
    NSString *memberNumber_;
    NSString *afilitationForm_;
    NSString *serviceType_;
}
/**
 * Provides read-only access to the operation Code
 */
@property (nonatomic, readonly, retain) NSString *operationCode;
/**
 * Provides read-only access to the Service
 */
@property (nonatomic, readonly, retain) NSString *service;
/**
 * Provides read-only access to the nickname
 */
@property (nonatomic, readonly, retain) NSString *nickname;
/**
 * Provides read-only access to the short nickname
 */
@property (nonatomic, readonly, retain) NSString *shortNickname;
/**
 * Provides read-only access to the cellphone
 */
@property (nonatomic, readonly, retain) NSString *cellphone;
/**
 * Provides read-only access to the day Month
 */
@property (nonatomic, readonly, retain) NSString *dayMonth;
/**
 * Provides read-only access to the email
 */
@property (nonatomic, readonly, retain) NSString *email;
/**
 * Provides read-only access to the memberNumber
 */
@property (nonatomic, readonly, retain) NSString *memberNumber;
/**
 * Provides read-only access to the afilitationForm
 */
@property (nonatomic, readonly, retain) NSString *afilitationForm;
/**
 * Provides read-only access to the service type
 */
@property (nonatomic, readonly, retain) NSString *serviceType;
@end
