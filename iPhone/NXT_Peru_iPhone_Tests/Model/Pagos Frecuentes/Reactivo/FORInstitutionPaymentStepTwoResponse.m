//
//  FORInstitutionPaymentStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORInstitutionPaymentStepTwoResponse.h"
/**
 * Enumerates the analysis states
 */
typedef enum {
    
    forspstrxeas_AnalyzingOperation = foirxeas_ElementsCount, //!<Analyzing operation elment
    forspstrxeas_AnalyzingAgreement, //!<Analyzing the agreement elment
    forspstrxeas_AnalyzingAccessCode, //!<Analyzing the access code element
    forspstrxeas_AnalyzingDescription, //!<Analyzing the description element
    forspstrxeas_AnalyzingCoordinate, //!<Analyzing the coordinate element
    forspstrxeas_AnalyzingSeal //!<Analyzing the seal element
	
} FORInstitutionPaymentStepTwoResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FORInstitutionPaymentStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFORInstitutionPaymentStepTwoResponseData;

@end


#pragma mark -
@implementation FORInstitutionPaymentStepTwoResponse

#pragma mark -
#pragma mark Properties

@synthesize operation = operation_;
@synthesize agreement = agreement_;
@synthesize accessCode = accessCode_;
@synthesize description = description_;
@synthesize coordinate = coordinate_;
@synthesize seal = seal_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFORInstitutionPaymentStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFORInstitutionPaymentStepTwoResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"operacion"]) {
            
            xmlAnalysisCurrentValue_ = forspstrxeas_AnalyzingOperation;
            
        }else if ([lname isEqualToString:@"convenio"]) {
            
            xmlAnalysisCurrentValue_ = forspstrxeas_AnalyzingAgreement;
            
        } else if ([lname isEqualToString:@"codigoacceso"]) {
            
            xmlAnalysisCurrentValue_ = forspstrxeas_AnalyzingAccessCode;
            
        } else if ([lname isEqualToString:@"descripcion"]) {
            
            xmlAnalysisCurrentValue_ = forspstrxeas_AnalyzingDescription;
            
        } else if ([lname isEqualToString:@"coordenada"]) {
            
            xmlAnalysisCurrentValue_ = forspstrxeas_AnalyzingCoordinate;
            
        } else if ([lname isEqualToString:@"sello"]) {
            
            xmlAnalysisCurrentValue_ = forspstrxeas_AnalyzingSeal;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == forspstrxeas_AnalyzingOperation) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forspstrxeas_AnalyzingAgreement) {
            
            [agreement_ release];
            agreement_ = nil;
            agreement_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forspstrxeas_AnalyzingAccessCode) {
            
            [accessCode_ release];
            accessCode_ = nil;
            accessCode_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forspstrxeas_AnalyzingDescription) {
            
            [description_ release];
            description_ = nil;
            description_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forspstrxeas_AnalyzingCoordinate) {
            
            [coordinate_ release];
            coordinate_ = nil;
            coordinate_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forspstrxeas_AnalyzingSeal) {
            
            [seal_ release];
            seal_ = nil;
            seal_ = [elementString  copyWithZone:self.zone];
            
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FORInstitutionPaymentStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFORInstitutionPaymentStepTwoResponseData {
    
    [operation_ release];
    operation_ = nil;
    
    [agreement_ release];
    agreement_ = nil;
    
    [accessCode_ release];
    accessCode_ = nil;
    
    [description_ release];
    description_ = nil;
    
    [coordinate_ release];
    coordinate_ = nil;
    
    [seal_ release];
    seal_ = nil;
    
}

@end
