//
//  FORCashMobileStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/24/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORCashMobileStepTwoResponse.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    forcmstrxeas_AnalyzingOperation = foirxeas_ElementsCount, //!<Analyzing the market place commision
    forcmstrxeas_AnalyzingChargeAccount, //!<Analyzing Charged account elment
    forcmstrxeas_AnalyzingBeneficiaryNumber, //!<Analyzing the Phone number
    forcmstrxeas_AnalyzingAmount, //!<Analyzing the amount
    forcmstrxeas_AnalyzingCoordinate, //!<Analyzing the amount
    forcmstrxeas_AnalyzingSeal //!<Analyzing the amount
    
	
} FORCashMobileStepTwoResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FORCashMobileStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFORCashMobileStepTwoResponseData;

@end


#pragma mark -
@implementation FORCashMobileStepTwoResponse

#pragma mark -
#pragma mark Properties
@synthesize operation = operation_;
@synthesize chargeAccount = chargeAccount_;
@synthesize beneficiaryNumber = beneficiaryNumber_;
@synthesize amount = amount_;
@synthesize coordinate = coordinate_;
@synthesize seal = seal_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFORCashMobileStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFORCashMobileStepTwoResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"operacion"]) {
            
            xmlAnalysisCurrentValue_ = forcmstrxeas_AnalyzingOperation;
            
        } else if ([lname isEqualToString:@"cuentacargo"]) {
            
            xmlAnalysisCurrentValue_ = forcmstrxeas_AnalyzingChargeAccount;
            
        } else if ([lname isEqualToString:@"celularbeneficiario"]) {
            
            xmlAnalysisCurrentValue_ = forcmstrxeas_AnalyzingBeneficiaryNumber;
            
        } else if ([lname isEqualToString:@"monto"]) {
            
            xmlAnalysisCurrentValue_ = forcmstrxeas_AnalyzingAmount;
            
        }  else if ([lname isEqualToString:@"coordenada"]) {
            
            xmlAnalysisCurrentValue_ = forcmstrxeas_AnalyzingCoordinate;
            
        }  else if ([lname isEqualToString:@"sello"]) {
            
            xmlAnalysisCurrentValue_ = forcmstrxeas_AnalyzingSeal;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == forcmstrxeas_AnalyzingOperation ) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forcmstrxeas_AnalyzingChargeAccount) {
            
            [chargeAccount_ release];
            chargeAccount_ = nil;
            chargeAccount_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forcmstrxeas_AnalyzingBeneficiaryNumber) {
            
            [beneficiaryNumber_ release];
            beneficiaryNumber_ = nil;
            beneficiaryNumber_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forcmstrxeas_AnalyzingAmount) {
            
            [amount_ release];
            amount_ = nil;
            amount_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forcmstrxeas_AnalyzingCoordinate) {
            
            [coordinate_ release];
            coordinate_ = nil;
            coordinate_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forcmstrxeas_AnalyzingSeal) {
            
            [seal_ release];
            seal_ = nil;
            seal_ = [elementString  copyWithZone:self.zone];
            
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FORCashMobileStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFORCashMobileStepTwoResponseData {
    
    [operation_ release];
    operation_ = nil;
    
    [chargeAccount_ release];
    chargeAccount_ = nil;
    
    [amount_ release];
    amount_ = nil;
    
    [beneficiaryNumber_ release];
    beneficiaryNumber_ = nil;
    
    [coordinate_ release];
    coordinate_ = nil;
    
    [seal_ release];
    seal_ = nil;
    
}
@end