//
//  FORRechargeGiftCardStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORRechargeGiftCardStepTwoResponse : FOInitialResponse
{
    /**
	 * operation
	 */
    NSString *operation_;
    
    /**
	 * card number for the operation
	 */
    NSString *cardNumber_;
    
    /**
	 * Charge account for the operation
	 */
    NSString *chargeAccount_;
    
    /**
	 * amount of the operation
	 */
    NSString *coordinate_;
    
    /**
	 * amount of the operation
	 */
    NSString *seal_;
    
}

/**
 * Provides read-only access to operation
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to the chargeAccount
 */
@property (nonatomic, readonly, copy) NSString *chargeAccount;

/**
 * Provides read-only access to the card number
 */
@property (nonatomic, readonly, copy) NSString *cardNumber;

/**
 * Provides read-only access to the coordinate
 */
@property (nonatomic, readonly, copy) NSString *coordinate;

/**
 * Provides read-only access to the seal
 */
@property (nonatomic, readonly, copy) NSString *seal;

@end
