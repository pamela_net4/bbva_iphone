//
//  FORRechargeGiftCardStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORInitialResponse.h"


@interface FORRechargeGiftCardStepOneResponse : FORInitialResponse
{
    /**
	 * card number for the operation
	 */
    NSString *cardNumber_;
    
    /**
	 * Charge account for the operation
	 */
    NSString *chargeAccount_;
    
}

/**
 * Provides read-only access to the chargeAccount
 */
@property (nonatomic, readonly, copy) NSString *chargeAccount;

/**
 * Provides read-only access to the card number
 */
@property (nonatomic, readonly, copy) NSString *cardNumber;

@end
