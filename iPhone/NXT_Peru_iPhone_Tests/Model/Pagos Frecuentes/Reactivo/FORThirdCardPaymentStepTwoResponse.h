//
//  FORThirdCardPaymentStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORThirdCardPaymentStepTwoResponse : FOInitialResponse
{    
    /**
	 * operation
	 */
    NSString *operation_;
    
    /**
     * Third card number
     */
    NSString *thirdCard_;
    
    /**
     * Card's holder name
     */
    NSString *holderName_;
    
    /**
	 * Coordinates to confirm
	 */
    NSString *coordinate_;
    /**
	 * Security Seal
	 */
    NSString *seal_;
}
/**
 * Provides read-only access to operation
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to third card number
 */
@property (nonatomic, readonly, copy) NSString *thirdCard;

/**
 * Provides read-only access to card's holder name type
 */
@property (nonatomic, readonly, copy) NSString *holderName;

/**
 * Provides read-only access to the coordinate
 */
@property (nonatomic, readonly, copy) NSString *coordinate;

/**
 * Provides read-only access to the seal
 */
@property (nonatomic, readonly, copy) NSString *seal;
@end
