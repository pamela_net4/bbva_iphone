//
//  FORThirdAccountTransferStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/29/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORInitialResponse.h"

@interface FORThirdAccountTransferStepOneResponse : FORInitialResponse
{
    /**
	 * Cuenta abono
	 */
    NSString *paymentAccount_;
    /**
	 * Name of the beneficiary
	 */
    NSString *holderName_;

}

/**
 * Provides read-only access to the paymentAccount
 */
@property (nonatomic, readonly, copy) NSString *paymentAccount;
/**
 * Provides read-only access to beneficiaryName
 */
@property (nonatomic, readonly, copy) NSString *holderName;
@end