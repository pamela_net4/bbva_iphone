//
//  FORServicePaymentStepThreeResponse.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORServicePaymentStepThreeResponse.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    forspstrxeas_AnalyzingOperation = foirxeas_ElementsCount, //!<Analyzing Copmany elment
    forspstrxeas_AnalyzingCompany, //!<Analyzing Copmany elment
    forspstrxeas_AnalyzingServiceType, //!<Analyzing the service type element
    forspstrxeas_AnalyzingCode, //!<Analyzing the code element
    forspstrxeas_AnalyzingHolder, //!<Analyzing the holder element
    forspstrxeas_AnalyzingInformativeText, //!<Analyzing the supply element
    forspstrxeas_AnalyzingDateHour //!<Analyzing the supply element
	
} FORServicePaymentStepThreeResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FORServicePaymentStepThreeResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFORServicePaymentStepThreeResponseData;

@end


#pragma mark -
@implementation FORServicePaymentStepThreeResponse

#pragma mark -
#pragma mark Properties

@synthesize operation = operation_;
@synthesize company = company_;
@synthesize serviceType = serviceType_;
@synthesize code = code_;
@synthesize holder = holder_;
@synthesize informativeText = informativeText_;
@synthesize dateHour = dateHour_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFORServicePaymentStepThreeResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFORServicePaymentStepThreeResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"operacion"]) {
            
            xmlAnalysisCurrentValue_ = forspstrxeas_AnalyzingOperation;
            
        } else if ([lname isEqualToString:@"empresa"]) {
            
            xmlAnalysisCurrentValue_ = forspstrxeas_AnalyzingCompany;
            
        } else if ([lname isEqualToString:@"tiposervicio"]) {
            
            xmlAnalysisCurrentValue_ = forspstrxeas_AnalyzingServiceType;
            
        } else if ([lname isEqualToString:@"suministro"] ||
                   [lname isEqualToString:@"codigopago"] ||
                   [lname isEqualToString:@"telefono"] ||
                   [lname isEqualToString:@"sds"]
                   ) {
            
            xmlAnalysisCurrentValue_ = forspstrxeas_AnalyzingCode;
            
        } else if ([lname isEqualToString:@"titular"]) {
            
            xmlAnalysisCurrentValue_ = forspstrxeas_AnalyzingHolder;
            
        } else if ([lname isEqualToString:@"textoinformativo"]) {
            
            xmlAnalysisCurrentValue_ = forspstrxeas_AnalyzingInformativeText;
            
        } else if ([lname isEqualToString:@"fechahora"]) {
            
            xmlAnalysisCurrentValue_ = forspstrxeas_AnalyzingDateHour;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == forspstrxeas_AnalyzingOperation) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forspstrxeas_AnalyzingCompany) {
            
            [company_ release];
            company_ = nil;
            company_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forspstrxeas_AnalyzingServiceType) {
            
            [serviceType_ release];
            serviceType_ = nil;
            serviceType_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forspstrxeas_AnalyzingCode) {
            
            [code_ release];
            code_ = nil;
            code_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forspstrxeas_AnalyzingHolder) {
            
            [holder_ release];
            holder_ = nil;
            holder_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forspstrxeas_AnalyzingInformativeText) {
            
            [informativeText_ release];
            informativeText_ = nil;
            informativeText_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forspstrxeas_AnalyzingDateHour) {
            
            [dateHour_ release];
            dateHour_ = nil;
            dateHour_ = [elementString  copyWithZone:self.zone];
            
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FORServicePaymentStepThreeResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFORServicePaymentStepThreeResponseData {
    
    [company_ release];
    company_ = nil;
    
    [serviceType_ release];
    serviceType_ = nil;
    
    [code_ release];
    code_ = nil;

    [holder_ release];
    holder_ = nil;

    [operation_ release];
    operation_ = nil;
    
    [informativeText_ release];
    informativeText_ = nil;
    
    [dateHour_ release];
    dateHour_ = nil;
    
}

@end
