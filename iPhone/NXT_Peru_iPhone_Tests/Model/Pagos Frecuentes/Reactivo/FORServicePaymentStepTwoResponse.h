//
//  FORServicePaymentStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORServicePaymentStepTwoResponse : FOInitialResponse
{
    /**
	 * company to pay
	 */
    NSString *operation_;
    /**
	 * company to pay
	 */
    NSString *company_;
    /**
	 * type of service
	 */
    NSString *serviceType_;
    /**
	 * code to enter
	 */
    NSString *code_;
    /**
	 * holder
	 */
    NSString *holder_;
    /**
	 * supply to enter
	 */
    NSString *coordinate_;
    /**
	 * supply to enter
	 */
    NSString *seal_;
    
}

/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, copy) NSString *operation;
/**
 * Provides read-only access to the company name
 */
@property (nonatomic, readonly, copy) NSString *company;
/**
 * Provides read-only access to service type
 */
@property (nonatomic, readonly, copy) NSString *serviceType;
/**
 * Provides read-only access to the code
 */
@property (nonatomic, readonly, copy) NSString *code;
/**
 * Provides read-only access to the holder
 */
@property (nonatomic, readonly, copy) NSString *holder;
/**
 * Provides read-only access to the coordinates to enter
 */
@property (nonatomic, readonly, copy) NSString *coordinate;
/**
 * Provides read-only access to oepration seal of the user
 */
@property (nonatomic, readonly, copy) NSString *seal;

@end
