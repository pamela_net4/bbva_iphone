//
//  FORInstitutionPaymentStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORInstitutionPaymentStepOneResponse.h"
/**
 * Enumerates the analysis states
 */
typedef enum {
    
    foripsorxeas_AnalyzingAgreement = forirxeas_ElementsCount, //!<Analyzing agreement elment
    foripsorxeas_AnalyzingAccessCode, //!<Analyzing the Access code element
    foripsorxeas_AnalyzingAccessDescription //!<Analyzing the Description element
    
	
} FORInstitutionPaymentStepOneResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FORInstitutionPaymentStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFORInstitutionPaymentStepOneResponseData;

@end


#pragma mark -
@implementation FORInstitutionPaymentStepOneResponse

#pragma mark -
#pragma mark Properties

@synthesize agreement = agreement_;
@synthesize accessCode = accessCode_;
@synthesize description = description_;
@synthesize companyCode = companyCode_;
@synthesize companyName = companyName_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFORInstitutionPaymentStepOneResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFORInstitutionPaymentStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"convenio"]) {
            
            xmlAnalysisCurrentValue_ = foripsorxeas_AnalyzingAgreement;
            
        } else if ([lname isEqualToString:@"codigoacceso"]) {
            
            xmlAnalysisCurrentValue_ = foripsorxeas_AnalyzingAccessCode;
            
        } else if ([lname isEqualToString:@"descripcion"]) {
            
            xmlAnalysisCurrentValue_ = foripsorxeas_AnalyzingAccessDescription;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == foripsorxeas_AnalyzingAgreement) {
            
            [agreement_ release];
            agreement_ = nil;
            agreement_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foripsorxeas_AnalyzingAccessCode) {
            
            [accessCode_ release];
            accessCode_ = nil;
            accessCode_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foripsorxeas_AnalyzingAccessDescription) {
            
            [description_ release];
            description_ = nil;
            description_ = [elementString  copyWithZone:self.zone];
            
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FORInstitutionPaymentStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFORInstitutionPaymentStepOneResponseData {
    
    [agreement_ release];
    agreement_ = nil;
    
    [accessCode_ release];
    accessCode_ = nil;
    
    [description_ release];
    description_ = nil;
    
}
@end
