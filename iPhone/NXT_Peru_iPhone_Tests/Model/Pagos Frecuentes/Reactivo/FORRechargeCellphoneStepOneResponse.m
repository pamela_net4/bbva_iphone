//
//  FORRechargeCellphoneStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORRechargeCellphoneStepOneResponse.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    forrcsorxeas_AnalyzingCarrier = forirxeas_ElementsCount, //!<Analyzing carrier account elment
    forrcsorxeas_AnalyzingPhoneNumber, //!<Analyzing the Phone number
    forrcsorxeas_AnalyzingHolderService //!<Analyzing the holder service
    
	
} FORRechargeCellphoneStepOneResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FORRechargeCellphoneStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFORRechargeCellphoneStepOneResponseData;

@end

#pragma mark -
@implementation FORRechargeCellphoneStepOneResponse
#pragma mark -
#pragma mark Properties

@synthesize carrier = carrier_;
@synthesize phoneNumber = phoneNumber_;
@synthesize holderService = holderService_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFORRechargeCellphoneStepOneResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFORRechargeCellphoneStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"empresa"]) {
            
            xmlAnalysisCurrentValue_ = forrcsorxeas_AnalyzingCarrier;
            
        } else if ([lname isEqualToString:@"telefono"]) {
            
            xmlAnalysisCurrentValue_ = forrcsorxeas_AnalyzingPhoneNumber;
            
        } else if ([lname isEqualToString:@"titular"]) {
            
            xmlAnalysisCurrentValue_ = forrcsorxeas_AnalyzingHolderService;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == forrcsorxeas_AnalyzingCarrier) {
            
            [carrier_ release];
            carrier_ = nil;
            carrier_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forrcsorxeas_AnalyzingPhoneNumber) {
            
            [phoneNumber_ release];
            phoneNumber_ = nil;
            phoneNumber_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forrcsorxeas_AnalyzingHolderService) {
            
            [holderService_ release];
            holderService_ = nil;
            holderService_ = [elementString  copyWithZone:self.zone];
            
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FORRechargeCellphoneStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFORRechargeCellphoneStepOneResponseData {
    
    [carrier_ release];
    carrier_ = nil;
    
    [phoneNumber_ release];
    phoneNumber_ = nil;
    
    [holderService_ release];
    holderService_ = nil;
    
}

@end
