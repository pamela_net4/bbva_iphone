//
//  FOCashMobileEnrollResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/23/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORInitialResponse.h"

@interface FORCashMobileStepOneResponse : FORInitialResponse
{
    /**
	 * Charge account for the operation
	 */
    NSString *chargeAccount_;
    /**
	 * phone for the operation
	 */
    NSString *phone_;
    /**
	 * amount of the operation
	 */
    NSString *amount_;
    
}

/**
 * Provides read-only access to the chargeAccount
 */
@property (nonatomic, readonly, copy) NSString *chargeAccount;

/**
 * Provides read-only access to the phone
 */
@property (nonatomic, readonly, copy) NSString *phone;

/**
 * Provides read-only access to the amount
 */
@property (nonatomic, readonly, copy) NSString *amount;


@end
