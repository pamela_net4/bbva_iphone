//
//  FORThirdCardPaymentStepThreeResponde.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORThirdCardPaymentStepThreeResponde : FOInitialResponse
{
    
    /**
	 * operation
	 */
    NSString *operation_;
    
    /**
     * Third card number
     */
    NSString *thirdCard_;
    
    /**
     * Card's holder name
     */
    NSString *holderName_;
    
    /**
	 * disclaimer for the operation
	 */
    
    NSString *disclaimer_;
    
    /**
	 * date and hour for the operation
	 */
    NSString *dateAndHourOperation_;
}

/**
 * Provides read-only access to operation
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to third card number
 */
@property (nonatomic, readonly, copy) NSString *thirdCard;

/**
 * Provides read-only access to card's holder name type
 */
@property (nonatomic, readonly, copy) NSString *holderName;

/**
 * Provides read-only access to the mailDisclaimer
 */
@property (nonatomic, readonly, copy) NSString *disclaimer;

/**
 * Provides read-only access to the date and hour
 */
@property (nonatomic, readonly, copy) NSString *dateAndHourOperation;


@end
