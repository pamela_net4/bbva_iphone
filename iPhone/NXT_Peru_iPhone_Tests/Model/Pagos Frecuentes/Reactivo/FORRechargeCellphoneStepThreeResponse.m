//
//  FORRechargeCellphoneStepThreeResponse.m
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORRechargeCellphoneStepThreeResponse.h"
/**
 * Enumerates the analysis states
 */
typedef enum {
    forrcstrxeas_AnalyzingOperation = foirxeas_ElementsCount,
    forrcstrxeas_AnalyzingCarrier, //!<Analyzing carrier account elment
    forrcstrxeas_AnalyzingPhoneNumber, //!<Analyzing the Phone number
    forrcstrxeas_AnalyzingHolderService, //!<Analyzing the holder service
    forrcstrxeas_AnalyzingDisclaimer, //!<Analyzing the disclaimer
    forrcstrxeas_AnalyzingDateAndHourOperation
	
} FORRechargeCellphoneStepThreeResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FORRechargeCellphoneStepThreeResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFORRechargeCellphoneStepThreeResponseData;

@end

#pragma mark -


@implementation FORRechargeCellphoneStepThreeResponse
#pragma mark -
#pragma mark Properties
@synthesize operation=operation_;
@synthesize carrier = carrier_;
@synthesize phoneNumber = phoneNumber_;
@synthesize holderService = holderService_;
@synthesize disclaimer=disclaimer_;
@synthesize dateAndHourOperation=dateAndHourOperation_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFORRechargeCellphoneStepThreeResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFORRechargeCellphoneStepThreeResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        if ([lname isEqualToString:@"operacion"]) {
            
            xmlAnalysisCurrentValue_ = forrcstrxeas_AnalyzingOperation;
            
        }else if ([lname isEqualToString:@"empresa"]) {
            
            xmlAnalysisCurrentValue_ = forrcstrxeas_AnalyzingCarrier;
            
        } else if ([lname isEqualToString:@"telefono"]) {
            
            xmlAnalysisCurrentValue_ = forrcstrxeas_AnalyzingPhoneNumber;
            
        } else if ([lname isEqualToString:@"titular"]) {
            
            xmlAnalysisCurrentValue_ = forrcstrxeas_AnalyzingHolderService;
            
        } else if ([lname isEqualToString:@"textoinformativo"]) {
            
            xmlAnalysisCurrentValue_ = forrcstrxeas_AnalyzingDisclaimer;
            
        } else if ([lname isEqualToString:@"fechahora"]) {
            
            xmlAnalysisCurrentValue_ = forrcstrxeas_AnalyzingDateAndHourOperation;
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == forrcstrxeas_AnalyzingOperation) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forrcstrxeas_AnalyzingCarrier) {
            
            [carrier_ release];
            carrier_ = nil;
            carrier_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forrcstrxeas_AnalyzingPhoneNumber) {
            
            [phoneNumber_ release];
            phoneNumber_ = nil;
            phoneNumber_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forrcstrxeas_AnalyzingHolderService) {
            
            [holderService_ release];
            holderService_ = nil;
            holderService_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == forrcstrxeas_AnalyzingDisclaimer) {
            
            [disclaimer_ release];
            disclaimer_ = nil;
            disclaimer_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == forrcstrxeas_AnalyzingDateAndHourOperation) {
            
            [dateAndHourOperation_ release];
            dateAndHourOperation_ = nil;
            dateAndHourOperation_ = [elementString  copyWithZone:self.zone];
   
                 
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FORRechargeCellphoneStepThreeResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFORRechargeCellphoneStepThreeResponseData {
    
    [operation_ release];
    operation_=nil;
    
    [carrier_ release];
    carrier_ = nil;
    
    [phoneNumber_ release];
    phoneNumber_ = nil;
    
    [holderService_ release];
    holderService_ = nil;
    
    [disclaimer_ release];
    disclaimer_ = nil;
    
    [dateAndHourOperation_ release];
    dateAndHourOperation_ = nil;
    
}

@end

