//
//  FORThirdAccountTransferStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/29/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORThirdAccountTransferStepThreeResponse : FOInitialResponse
{
    /**
	 * company to pay
	 */
    NSString *operation_;
    /**
	 * Cuenta abono
	 */
    NSString *paymentAccount_;
    /**
	 * Name of the holder
	 */
    NSString *holderName_;
    /**
	 * supply to enter
	 */
    NSString *informativeText_;
    /**
	 * supply to enter
	 */
    NSString *dateHour_;
    
}
/**
 * Provides read-only access to the paymentAccount
 */
@property (nonatomic, readonly, copy) NSString *operation;
/**
 * Provides read-only access to the paymentAccount
 */
@property (nonatomic, readonly, copy) NSString *paymentAccount;
/**
 * Provides read-only access to beneficiaryName
 */
@property (nonatomic, readonly, copy) NSString *holderName;
/**
 * Provides read-only access to the informative text to display
 */
@property (nonatomic, readonly, copy) NSString *informativeText;
/**
 * Provides read-only access to the date hour
 */
@property (nonatomic, readonly, copy) NSString *dateHour;
@end
