//
//  FORThirdCardPaymentStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORInitialResponse.h"

@interface FORThirdCardPaymentStepOneResponse : FORInitialResponse{
    
    /**
	 * card for the operation
	 */
    NSString *card_;
    /**
	 * card's holder name
	 */
    NSString *holderName_;
    
}
/**
 * Provides read-only access to the card number
 */
@property (nonatomic, readonly, copy) NSString *card;
/**
 * Provides read-only access to card class
 */
@property (nonatomic, readonly, copy) NSString *holderName;

@end
