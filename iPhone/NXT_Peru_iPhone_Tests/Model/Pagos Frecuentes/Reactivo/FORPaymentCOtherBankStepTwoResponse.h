//
//  FORPaymentCOtherBankStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORPaymentCOtherBankStepTwoResponse : FOInitialResponse
{
    
    /**
	 * operation
	 */
    NSString *operation_;

    /**
     * Card number
     */
    NSString *cardNumber_;
    
    /**
     * Card type
     */
    NSString *cardType_;
    
    /**
     * Destination Bank
     */
    
    NSString *destinationBank;
    
    /**
     * Place Emission
     */
    
    NSString *placeEmission_;
    
    /**
     * Beneficiary Name
     */
    
    NSString *beneficiaryName_;
    
    /**
	 * Coordinates to confirm
	 */
    NSString *coordinate_;
    /**
	 * Security Seal
	 */
    NSString *seal_;
}
/**
 * Provides read-only access to operation
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to card number
 */
@property (nonatomic, readonly, copy) NSString *cardNumber;

/**
 * Provides read-only access to card type
 */
@property (nonatomic, readonly, copy) NSString *cardType;

/**
 * Provides read-only access to destination bank
 */
@property (nonatomic, readonly, copy) NSString *destinationBank;

/**
 * Provides read-only access to place emission
 */
@property (nonatomic, readonly, copy) NSString *placeEmission;

/**
 * Provides read-only access to beneficiary name
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryName;

/**
 * Provides read-only access to the coordinate
 */
@property (nonatomic, readonly, copy) NSString *coordinate;

/**
 * Provides read-only access to the seal
 */
@property (nonatomic, readonly, copy) NSString *seal;

@end
