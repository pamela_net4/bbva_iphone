//
//  FORInstitutionPaymentStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORInstitutionPaymentStepTwoResponse : FOInitialResponse
{
    /**
	 * company to pay
	 */
    NSString *operation_;
    /**
	 * company to pay
	 */
    NSString *agreement_;
    /**
	 * access Code
	 */
    NSString *accessCode_;
    /**
	 * description of the institution
	 */
    NSString *description_;
    /**
	 * supply to enter
	 */
    NSString *coordinate_;
    /**
	 * supply to enter
	 */
    NSString *seal_;
    
}
/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, copy) NSString *operation;
/**
 * Provides read-only access to the agreement
 */
@property (nonatomic, readonly, copy) NSString *agreement;
/**
 * Provides read-only access to the accessCode
 */
@property (nonatomic, readonly, copy) NSString *accessCode;
/**
 * Provides read-only access to the description
 */
@property (nonatomic, readonly, copy) NSString *description;
/**
 * Provides read-only access to the coordinates to enter
 */
@property (nonatomic, readonly, copy) NSString *coordinate;
/**
 * Provides read-only access to oepration seal of the user
 */
@property (nonatomic, readonly, copy) NSString *seal;
@end
