//
//  FORInstitutionPaymentStepThreeResponse.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORInstitutionPaymentStepThreeResponse.h"
/**
 * Enumerates the analysis states
 */
typedef enum {
    
    foripstrxeas_AnalyzingOperation = foirxeas_ElementsCount, //!<Analyzing operation elment
    foripstrxeas_AnalyzingAgreement, //!<Analyzing the agreement elment
    foripstrxeas_AnalyzingAccessCode, //!<Analyzing the access code element
    foripstrxeas_AnalyzingDescription, //!<Analyzing the description element
    foripstrxeas_AnalyzingDateAndHourOperation, //!<Analyzing the date of hour element
	foripstrxeas_AnalyzingInformativeText //!<Analyzing the informative text element
    
} FORInstitutionPaymentStepThreeResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FORInstitutionPaymentStepThreeResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFORInstitutionPaymentStepThreeResponseData;

@end


#pragma mark -
@implementation FORInstitutionPaymentStepThreeResponse

#pragma mark -
#pragma mark Properties

@synthesize operation = operation_;
@synthesize agreement = agreement_;
@synthesize accessCode = accessCode_;
@synthesize description = description_;
@synthesize dateAndHourOperation = dateAndHourOperation_;
@synthesize informativeText = informativeText_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFORInstitutionPaymentStepThreeResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFORInstitutionPaymentStepThreeResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"operacion"]) {
            
            xmlAnalysisCurrentValue_ = foripstrxeas_AnalyzingOperation;
            
        }else if ([lname isEqualToString:@"convenio"]) {
            
            xmlAnalysisCurrentValue_ = foripstrxeas_AnalyzingAgreement;
            
        } else if ([lname isEqualToString:@"codigoacceso"]) {
            
            xmlAnalysisCurrentValue_ = foripstrxeas_AnalyzingAccessCode;
            
        } else if ([lname isEqualToString:@"descripcion"]) {
            
            xmlAnalysisCurrentValue_ = foripstrxeas_AnalyzingDescription;
            
        } else if ([lname isEqualToString: @"fechahora"]){
            
            xmlAnalysisCurrentValue_ = foripstrxeas_AnalyzingDateAndHourOperation;
        
        } else if ([lname isEqualToString: @"textoinformativo"]){
            
            xmlAnalysisCurrentValue_ = foripstrxeas_AnalyzingInformativeText;
        
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == foripstrxeas_AnalyzingOperation) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foripstrxeas_AnalyzingAgreement) {
            
            [agreement_ release];
            agreement_ = nil;
            agreement_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foripstrxeas_AnalyzingAccessCode) {
            
            [accessCode_ release];
            accessCode_ = nil;
            accessCode_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foripstrxeas_AnalyzingDescription) {
            
            [description_ release];
            description_ = nil;
            description_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foripstrxeas_AnalyzingDateAndHourOperation) {
        
            [dateAndHourOperation_ release];
            dateAndHourOperation_ = nil;
            dateAndHourOperation_= [elementString copyWithZone: self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foripstrxeas_AnalyzingInformativeText) {
        
            [informativeText_ release];
            informativeText_ = nil;
            informativeText_ = [elementString copyWithZone: self.zone];
            
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FORInstitutionPaymentStepThreeResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFORInstitutionPaymentStepThreeResponseData {
    
    [operation_ release];
    operation_ = nil;
    
    [agreement_ release];
    agreement_ = nil;
    
    [accessCode_ release];
    accessCode_ = nil;
    
    [description_ release];
    description_ = nil;
    
    [dateAndHourOperation_ release];
    dateAndHourOperation_ = nil;
    
    [informativeText_ release];
    informativeText_ = nil;
    
}

@end
