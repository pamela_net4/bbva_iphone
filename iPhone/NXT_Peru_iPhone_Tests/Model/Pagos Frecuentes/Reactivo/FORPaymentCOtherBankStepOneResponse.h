//
//  FORPaymentCOtherBankStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORInitialResponse.h"

@interface FORPaymentCOtherBankStepOneResponse : FORInitialResponse{
    
    /**
	 * card number for the operation
	 */
    NSString *cardNumber_;
    /**
	 * card type to do the pay
	 */
    NSString *cardClass_;
    /**
	 * the destination bank of the card
	 */
    NSString *destinationBank_;
    /**
	 * where the card was emited
	 */
    NSString *emissionPlace_;
    /**
	 * the beneficiary name
	 */
    NSString *beneficiaryName_;
    
}
/**
 * Provides read-only access to the beneficiary name
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryName;
/**
 * Provides read-only access to the card number
 */
@property (nonatomic, readonly, copy) NSString *cardNumber;
/**
 * Provides read-only access to card class
 */
@property (nonatomic, readonly, copy) NSString *cardClass;
/**
 * Provides read-only access to the destination bank
 */
@property (nonatomic, readonly, copy) NSString *destinationBank;
/**
 * Provides read-only access to the emision place
 */
@property (nonatomic, readonly, copy) NSString *emissionPlace;

@end
