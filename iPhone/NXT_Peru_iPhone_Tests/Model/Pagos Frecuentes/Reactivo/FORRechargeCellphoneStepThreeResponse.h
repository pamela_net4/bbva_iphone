//
//  FORRechargeCellphoneStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORRechargeCellphoneStepThreeResponse : FOInitialResponse
{

    /**
	 * operation
	 */
    NSString *operation_;
    
    /**
     * carrier for the operation
     */
    NSString *carrier_;
    
    /**
     * phone number for the operation
     */
    NSString *phoneNumber_;
    
    /**
     * holder service for the operation
     */
    NSString *holderService_;
    
    /**
	 * disclaimer for the operation
	 */
    NSString *disclaimer_;
    /**
	 * date and hour for the operation
	 */
    NSString *dateAndHourOperation_;
    
}

/**
 * Provides read-only access to operation
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to the carrier
 */
@property (nonatomic, readonly, copy) NSString *carrier;

/**
 * Provides read-only access to the phoneNumber
 */
@property (nonatomic, readonly, copy) NSString *phoneNumber;

/**
 * Provides read-only access to the holderService
 */
@property (nonatomic, readonly, copy) NSString *holderService;

/**
 * Provides read-only access to the mailDisclaimer
 */
@property (nonatomic, readonly, copy) NSString *disclaimer;

/**
 * Provides read-only access to the date and hour
 */
@property (nonatomic, readonly, copy) NSString *dateAndHourOperation;

@end
