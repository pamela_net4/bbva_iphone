//
//  FORServicePaymentStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORInitialResponse.h"

@interface FORServicePaymentStepOneResponse : FORInitialResponse
{
    /**
	 * company to pay
	 */
    NSString *company_;
    /**
	 * type of service
	 */
    NSString *serviceType_;
    /**
	 * supply to enter
	 */
    NSString *code_;
    /**
	 * card's holder name
	 */
    NSString *holder_;
    
}

/**
 * Provides read-only access to the company name
 */
@property (nonatomic, readonly, copy) NSString *company;
/**
 * Provides read-only access to service type
 */
@property (nonatomic, readonly, copy) NSString *serviceType;
/**
 * Provides read-only access to supply
 */
@property (nonatomic, readonly, copy) NSString *code;
/**
 * Provides read-only access to holder
 */
@property (nonatomic, readonly, copy) NSString *holder;
@end
