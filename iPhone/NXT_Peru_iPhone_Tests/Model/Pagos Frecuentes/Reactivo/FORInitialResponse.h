//
//  FOInitialResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/23/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    forcmsorxeas_AnalyzingService = serxeas_ElementsCount, //!<Analyzing the service
    forirxeas_AnalyzingPhoneList, //!<Analyzing the list of phones
    forirxeas_AnalyzingEmailList, //!<Analyzing the list of emails
    forirxeas_AnalyzingChannelList, //!<Analyzing the list of channels
    forirxeas_AnalyzingPhoneDisclaimer, //!<Analyzing the Phone disclaimer
    forirxeas_AnalyzingEmailDisclaimer, //!<Analyzing the email disclaimer
    forirxeas_AnalyzingSuggestion,
    forirxeas_ElementsCount //!<last Element
    
    
} FORInitialResponseXMLElementAnalyzerState;

@class ChannelList;
@class FOPhoneList;
@class FOEmailList;
@interface FORInitialResponse : StatusEnabledResponse
{
@private
    
    /**
	 * Service name
	 */
    NSString *service_;
    /**
	 * list of phones for the operation
	 */
    FOPhoneList *phoneList_;
    /**
	 * list of mails for the operation
	 */
    FOEmailList *emailList_;
    /**
	 * list of channels for the operation
	 */
    ChannelList *channelList_;
    /**
	 * disclaimer for the phone
	 */
    NSString *phoneDisclaimer_;
    /**
	 * disclaimer for the mail
	 */
    NSString *emailDisclaimer_;
    
    /**
     * suggestion
     */
    NSString *suggestion_;
    
}
/**
 * Provides read-only access to Suggestion
 */
@property (nonatomic, readonly, retain) NSString *suggestion;

/**
 * Provides read-only access to service name
 */
@property (nonatomic, readonly, copy) NSString *service;
/**
 * Provides read-only access to the phoneList
 */
@property (nonatomic, readonly, copy) FOPhoneList *phoneList;

/**
 * Provides read-only access to the mailList
 */
@property (nonatomic, readonly, copy) FOEmailList *emailList;

/**
 * Provides read-only access to the mailList
 */
@property(nonatomic,readonly,retain) ChannelList *channelList;

/**
 * Provides read-only access to the phoneDisclaimer
 */
@property (nonatomic, readonly, copy) NSString *phoneDisclaimer;

/**
 * Provides read-only access to the mailDisclaimer
 */
@property (nonatomic, readonly, copy) NSString *emailDisclaimer;

@end
