//
//  FORRechargeCellphoneStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORRechargeCellphoneStepTwoResponse : FOInitialResponse
{
    /**
	 * operation
	 */
    NSString *operation_;
    
    /**
     * carrier for the operation
     */
    NSString *carrier_;
    
    /**
     * phone number for the operation
     */
    NSString *phoneNumber_;
    
    /**
     * holder service for the operation
     */
    NSString *holderService_;
    
    /**
	 * amount of the operation
	 */
    NSString *coordinate_;
    
    /**
	 * amount of the operation
	 */
    NSString *seal_;

}
/**
 * Provides read-only access to operation
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to the carrier
 */
@property (nonatomic, readonly, copy) NSString *carrier;

/**
 * Provides read-only access to the phoneNumber
 */
@property (nonatomic, readonly, copy) NSString *phoneNumber;

/**
 * Provides read-only access to the holderService
 */
@property (nonatomic, readonly, copy) NSString *holderService;

/**
 * Provides read-only access to the coordinate
 */
@property (nonatomic, readonly, copy) NSString *coordinate;

/**
 * Provides read-only access to the seal
 */
@property (nonatomic, readonly, copy) NSString *seal;

@end
