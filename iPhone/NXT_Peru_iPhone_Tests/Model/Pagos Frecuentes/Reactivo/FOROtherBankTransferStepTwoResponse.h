//
//  FOROtherBankTransferStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/29/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOROtherBankTransferStepTwoResponse : FOInitialResponse
{
    /**
	 * company to pay
	 */
    NSString *operation_;
    /**
	 * Cuenta abono
	 */
    NSString *paymentAccount_;
    /**
	 * Destination bank account to pay
	 */
    NSString *destinationBankAccount_;
    /**
	 * Name of the beneficiary
	 */
    NSString *beneficiaryName_;
    /**
	 * Description of the document type
	 */
    NSString *docTypeDescription_;
    /**
	 * Number of the identification document
	 */
    NSString *docNumber_;
    /**
	 * supply to enter
	 */
    NSString *coordinate_;
    /**
	 * supply to enter
	 */
    NSString *seal_;
    
}
/**
 * Provides read-only access to the paymentAccount
 */
@property (nonatomic, readonly, copy) NSString *operation;
/**
 * Provides read-only access to the paymentAccount
 */
@property (nonatomic, readonly, copy) NSString *paymentAccount;
/**
 * Provides read-only access to destinationBankAccount
 */
@property (nonatomic, readonly, copy) NSString *destinationBankAccount;
/**
 * Provides read-only access to beneficiaryName
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryName;
/**
 * Provides read-only access to docType
 */
@property (nonatomic, readonly, copy) NSString *docTypeDescription;
/**
 * Provides read-only access to docNumber
 */
@property (nonatomic, readonly, copy) NSString *docNumber;
/**
 * Provides read-only access to the coordinates to enter
 */
@property (nonatomic, readonly, copy) NSString *coordinate;
/**
 * Provides read-only access to oepration seal of the user
 */
@property (nonatomic, readonly, copy) NSString *seal;
@end
