//
//  FORServicePaymentStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORServicePaymentStepThreeResponse : FOInitialResponse
{
    /**
	 * company to pay
	 */
    NSString *operation_;
    /**
	 * company to pay
	 */
    NSString *company_;
    /**
	 * type of service
	 */
    NSString *serviceType_;
    /**
	 * code to enter
	 */
    NSString *code_;
    /**
	 * holder
	 */
    NSString *holder_;
    /**
	 * supply to enter
	 */
    NSString *informativeText_;
    /**
	 * supply to enter
	 */
    NSString *dateHour_;
    
}

/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, copy) NSString *operation;
/**
 * Provides read-only access to the company name
 */
@property (nonatomic, readonly, copy) NSString *company;
/**
 * Provides read-only access to service type
 */
@property (nonatomic, readonly, copy) NSString *serviceType;
/**
 * Provides read-only access to the code
 */
@property (nonatomic, readonly, copy) NSString *code;
/**
 * Provides read-only access to the holder
 */
@property (nonatomic, readonly, copy) NSString *holder;
/**
 * Provides read-only access to the informative text to display
 */
@property (nonatomic, readonly, copy) NSString *informativeText;
/**
 * Provides read-only access to the date hour
 */
@property (nonatomic, readonly, copy) NSString *dateHour;

@end
