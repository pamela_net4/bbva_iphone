//
//  FOROtherBankTransferStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/29/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOROtherBankTransferStepOneResponse.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    forobtsorxeas_AnalyzingPaymentAccount = forirxeas_ElementsCount, //!<Analyzing payment account element
    forobtsorxeas_AnalyzingDestinationBankAccount, //!<Analyzing the Destination bank account element
    forobtsorxeas_AnalyzingBeneficiaryName, //!<Analyzing the beneficiary name element
    forobtsorxeas_AnalyzingDocType, //!<Analyzing the doc type element
    forobtsorxeas_AnalyzingDocDescriptionType, //!<Analyzing the doc description type element
    forobtsorxeas_AnalyzingDocNumber //!<Analyzing the doc number element
    
	
} FOROtherBankTransferStepOneResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FOROtherBankTransferStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOROtherBankTransferStepOneResponseData;

@end


#pragma mark -
@implementation FOROtherBankTransferStepOneResponse

#pragma mark -
#pragma mark Properties

@synthesize paymentAccount = paymentAccount_;
@synthesize destinationBankAccount = destinationBankAccount_;
@synthesize beneficiaryName = beneficiaryName_;
@synthesize docType = docType_;
@synthesize docTypeDescription = docTypeDescription_;
@synthesize docNumber = docNumber_;
@synthesize destionationBank = destinationBank_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFOROtherBankTransferStepOneResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFOROtherBankTransferStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"cuentaabono"]) {
            
            xmlAnalysisCurrentValue_ = forobtsorxeas_AnalyzingPaymentAccount;
            
        } else if ([lname isEqualToString:@"ctabancodestino"]) {
            
            xmlAnalysisCurrentValue_ = forobtsorxeas_AnalyzingDestinationBankAccount;
            
        } else if ([lname isEqualToString:@"nombrebeneficiario"]) {
            
            xmlAnalysisCurrentValue_ = forobtsorxeas_AnalyzingBeneficiaryName;
            
        } else if ([lname isEqualToString:@"tipodoc"]) {
            
            xmlAnalysisCurrentValue_ = forobtsorxeas_AnalyzingDocType;
            
        } else if ([lname isEqualToString:@"desctipodoc"]) {
            
            xmlAnalysisCurrentValue_ = forobtsorxeas_AnalyzingDocDescriptionType;
            
        } else if ([lname isEqualToString:@"nrodoc"]) {
            
            xmlAnalysisCurrentValue_ = forobtsorxeas_AnalyzingDocNumber;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == forobtsorxeas_AnalyzingPaymentAccount) {
            
            [paymentAccount_ release];
            paymentAccount_ = nil;
            paymentAccount_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forobtsorxeas_AnalyzingDestinationBankAccount) {
            
            [destinationBankAccount_ release];
            destinationBankAccount_ = nil;
            destinationBankAccount_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forobtsorxeas_AnalyzingBeneficiaryName) {
            
            [beneficiaryName_ release];
            beneficiaryName_ = nil;
            beneficiaryName_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forobtsorxeas_AnalyzingDocType) {
            
            [docType_ release];
            docType_ = nil;
            docType_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forobtsorxeas_AnalyzingDocDescriptionType) {
            
            [docTypeDescription_ release];
            docTypeDescription_ = nil;
            docTypeDescription_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forobtsorxeas_AnalyzingDocNumber) {
            
            [docNumber_ release];
            docNumber_ = nil;
            docNumber_ = [elementString  copyWithZone:self.zone];
            
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FOROtherBankTransferStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOROtherBankTransferStepOneResponseData {
    
    [paymentAccount_ release];
    paymentAccount_ = nil;
    
    [destinationBankAccount_ release];
    destinationBankAccount_ = nil;
    
    [beneficiaryName_ release];
    beneficiaryName_ = nil;
    
    [docType_ release];
    docType_ = nil;
    
    [docTypeDescription_ release];
    docTypeDescription_ = nil;
    
    [docNumber_ release];
    docNumber_ = nil;
    
}

@end
