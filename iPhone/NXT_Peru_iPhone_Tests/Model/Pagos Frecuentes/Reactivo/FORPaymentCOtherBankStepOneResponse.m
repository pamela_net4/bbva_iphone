//
//  FORPaymentCOtherBankStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORPaymentCOtherBankStepOneResponse.h"
/**
 * Enumerates the analysis states
 */
typedef enum {
    
    forpcobsorxeas_AnalyzingCardNumber = forirxeas_ElementsCount, //!<Analyzing Charged account elment
    forpcobsorxeas_AnalyzingCardClass, //!<Analyzing the Phone number
    forpcobsorxeas_AnalyzingDestinationBank, //!<Analyzing the Phone number
    forpcobsorxeas_AnalyzingEmisionPlace,
    forpcobsorxeas_AnalyzingBeneficiaryName //!<Analyzing the amount
    
	
} FORPaymentCOtherBankStepOneResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FORPaymentCOtherBankStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFORPaymentCOtherBankStepOneResponseData;

@end


#pragma mark -
@implementation FORPaymentCOtherBankStepOneResponse

#pragma mark -
#pragma mark Properties

@synthesize cardNumber = cardNumber_;
@synthesize cardClass = cardClass_;
@synthesize destinationBank = destinationBank_;
@synthesize emissionPlace = emissionPlace_;
@synthesize beneficiaryName=beneficiaryName_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFORPaymentCOtherBankStepOneResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFORPaymentCOtherBankStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"numerotarjeta"]) {
            
            xmlAnalysisCurrentValue_ = forpcobsorxeas_AnalyzingCardNumber;
            
        } else if ([lname isEqualToString:@"clase"]) {
            
            xmlAnalysisCurrentValue_ = forpcobsorxeas_AnalyzingCardClass;
            
        } else if ([lname isEqualToString:@"bancodestino"]) {
            
            xmlAnalysisCurrentValue_ = forpcobsorxeas_AnalyzingDestinationBank;
            
        }  else if ([lname isEqualToString:@"lugaremision"]) {
            
            xmlAnalysisCurrentValue_ = forpcobsorxeas_AnalyzingEmisionPlace;
            
        } else if ([lname isEqualToString:@"nombrebeneficiario"]) {
            
            xmlAnalysisCurrentValue_ = forpcobsorxeas_AnalyzingBeneficiaryName;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == forpcobsorxeas_AnalyzingCardNumber) {
            
            [cardNumber_ release];
            cardNumber_ = nil;
            cardNumber_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forpcobsorxeas_AnalyzingCardClass) {
            
            [cardClass_ release];
            cardClass_ = nil;
            cardClass_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forpcobsorxeas_AnalyzingDestinationBank) {
            
            [destinationBank_ release];
            destinationBank_ = nil;
            destinationBank_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forpcobsorxeas_AnalyzingEmisionPlace) {
            
            [emissionPlace_ release];
            emissionPlace_ = nil;
            emissionPlace_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forpcobsorxeas_AnalyzingBeneficiaryName) {
            
            [beneficiaryName_ release];
            beneficiaryName_ = nil;
            beneficiaryName_ = [elementString  copyWithZone:self.zone];
            
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FORPaymentCOtherBankStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFORPaymentCOtherBankStepOneResponseData {
    
    [cardNumber_ release];
    cardNumber_ = nil;
    
    [cardClass_ release];
    cardClass_ = nil;
    
    [destinationBank_ release];
    destinationBank_ = nil;
    
    [emissionPlace_ release];
    emissionPlace_ = nil;
    
    [beneficiaryName_ release];
    beneficiaryName_ = nil;
    
}

@end
