//
//  FORThirdAccountTransferStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/29/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORThirdAccountTransferStepTwoResponse : FOInitialResponse
{
    /**
	 * company to pay
	 */
    NSString *operation_;
    /**
	 * Cuenta abono
	 */
    NSString *paymentAccount_;
    /**
	 * Name of the beneficiary
	 */
    NSString *holderName_;
    /**
	 * supply to enter
	 */
    NSString *coordinate_;
    /**
	 * supply to enter
	 */
    NSString *seal_;
    
}
/**
 * Provides read-only access to the paymentAccount
 */
@property (nonatomic, readonly, copy) NSString *operation;
/**
 * Provides read-only access to the paymentAccount
 */
@property (nonatomic, readonly, copy) NSString *paymentAccount;
/**
 * Provides read-only access to beneficiaryName
 */
@property (nonatomic, readonly, copy) NSString *holderName;
/**
 * Provides read-only access to the coordinates to enter
 */
@property (nonatomic, readonly, copy) NSString *coordinate;
/**
 * Provides read-only access to oepration seal of the user
 */
@property (nonatomic, readonly, copy) NSString *seal;
@end
