//
//  FORRechargeGiftCardStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORRechargeGiftCardStepThreeResponse : FOInitialResponse
{

    /**
	 * operation
	 */
    NSString *operation_;
    
    /**
	 * card number for the operation
	 */
    NSString *cardNumber_;
    
    /**
	 * Charge account for the operation
	 */
    NSString *chargeAccount_;
    
    /**
	 * disclaimer for the operation
	 */
    NSString *disclaimer_;
    /**
	 * date and hour for the operation
	 */
    NSString *dateAndHourOperation_;
    
}

/**
 * Provides read-only access to operation
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to the chargeAccount
 */
@property (nonatomic, readonly, copy) NSString *chargeAccount;

/**
 * Provides read-only access to the card number
 */
@property (nonatomic, readonly, copy) NSString *cardNumber;

/**
 * Provides read-only access to the mailDisclaimer
 */
@property (nonatomic, readonly, copy) NSString *disclaimer;

/**
 * Provides read-only access to the date and hour
 */
@property (nonatomic, readonly, copy) NSString *dateAndHourOperation;

@end
