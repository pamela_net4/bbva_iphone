//
//  FOROtherBankTransferStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/29/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOROtherBankTransferStepTwoResponse.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    forobtstrxeas_AnalyzingOperation = foirxeas_ElementsCount, //!<Analyzing payment account element
    forobtstrxeas_AnalyzingPaymentAccount, //!<Analyzing payment account element
    forobtstrxeas_AnalyzingDestinationBankAccount, //!<Analyzing the Destination bank account element
    forobtstrxeas_AnalyzingBeneficiaryName, //!<Analyzing the beneficiary name element
    forobtstrxeas_AnalyzingDocDescriptionType, //!<Analyzing the doc description type element
    forobtstrxeas_AnalyzingDocNumber, //!<Analyzing the doc description type element
    forobtstrxeas_AnalyzingDocCoordinate, //!<Analyzing the doc description type element
    forobtstrxeas_AnalyzingSeal //!<Analyzing the doc number element
    
	
} FOROtherBankTransferStepTwoResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FOROtherBankTransferStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOROtherBankTransferStepTwoResponseData;

@end


#pragma mark -
@implementation FOROtherBankTransferStepTwoResponse

#pragma mark -
#pragma mark Properties

@synthesize operation = operation_;
@synthesize paymentAccount = paymentAccount_;
@synthesize destinationBankAccount = destinationBankAccount_;
@synthesize beneficiaryName = beneficiaryName_;
@synthesize docTypeDescription = docTypeDescription_;
@synthesize docNumber = docNumber_;
@synthesize coordinate = coordinate_;
@synthesize seal = seal_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFOROtherBankTransferStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFOROtherBankTransferStepTwoResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"operacion"]) {
            
            xmlAnalysisCurrentValue_ = forobtstrxeas_AnalyzingOperation;
            
        } else if ([lname isEqualToString:@"cuentaabono"]) {
            
            xmlAnalysisCurrentValue_ = forobtstrxeas_AnalyzingPaymentAccount;
            
        } else if ([lname isEqualToString:@"ctabancodestino"]) {
            
            xmlAnalysisCurrentValue_ = forobtstrxeas_AnalyzingDestinationBankAccount;
            
        } else if ([lname isEqualToString:@"nombrebeneficiario"]) {
            
            xmlAnalysisCurrentValue_ = forobtstrxeas_AnalyzingBeneficiaryName;
            
        } else if ([lname isEqualToString:@"desctipodoc"]) {
            
            xmlAnalysisCurrentValue_ = forobtstrxeas_AnalyzingDocDescriptionType;
            
        } else if ([lname isEqualToString:@"nrodoc"]) {
            
            xmlAnalysisCurrentValue_ = forobtstrxeas_AnalyzingDocNumber;
            
        } else if ([lname isEqualToString:@"coordenada"]) {
            
            xmlAnalysisCurrentValue_ = forobtstrxeas_AnalyzingDocCoordinate;
            
        } else if ([lname isEqualToString:@"sello"]) {
            
            xmlAnalysisCurrentValue_ = forobtstrxeas_AnalyzingSeal;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == forobtstrxeas_AnalyzingOperation) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forobtstrxeas_AnalyzingPaymentAccount) {
            
            [paymentAccount_ release];
            paymentAccount_ = nil;
            paymentAccount_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forobtstrxeas_AnalyzingDestinationBankAccount) {
            
            [destinationBankAccount_ release];
            destinationBankAccount_ = nil;
            destinationBankAccount_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forobtstrxeas_AnalyzingBeneficiaryName) {
            
            [beneficiaryName_ release];
            beneficiaryName_ = nil;
            beneficiaryName_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forobtstrxeas_AnalyzingDocDescriptionType) {
            
            [docTypeDescription_ release];
            docTypeDescription_ = nil;
            docTypeDescription_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forobtstrxeas_AnalyzingDocNumber) {
            
            [docNumber_ release];
            docNumber_ = nil;
            docNumber_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forobtstrxeas_AnalyzingDocCoordinate) {
            
            [coordinate_ release];
            coordinate_ = nil;
            coordinate_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forobtstrxeas_AnalyzingSeal) {
            
            [seal_ release];
            seal_ = nil;
            seal_ = [elementString  copyWithZone:self.zone];
            
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FOROtherBankTransferStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOROtherBankTransferStepTwoResponseData {
    
    [operation_ release];
    operation_ = nil;
    
    [paymentAccount_ release];
    paymentAccount_ = nil;
    
    [destinationBankAccount_ release];
    destinationBankAccount_ = nil;
    
    [beneficiaryName_ release];
    beneficiaryName_ = nil;
    
    [docTypeDescription_ release];
    docTypeDescription_ = nil;
    
    [docNumber_ release];
    docNumber_ = nil;
    
    [coordinate_ release];
    coordinate_ = nil;
    
    [seal_ release];
    seal_ = nil;
    
}

@end
