//
//  FORRechargeGiftCardStepThreeResponse.m
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORRechargeGiftCardStepThreeResponse.h"
/**
 * Enumerates the analysis states
 */
typedef enum {
    forrgcstrxeas_AnalyzingOperation= foirxeas_ElementsCount,
    forrgcstrxeas_AnalyzingCardNumber ,
    forrgcstrxeas_AnalyzingChargeAccount,
    forrgcstrxeas_AnalyzingDisclaimer,
    forrgcstrxeas_AnalyzingDateAndHourOperation
    
} FORRechargeGiftCardStepThreeResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FORRechargeGiftCardStepThreeResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFORRechargeGiftCardStepThreeResponseData;

@end

#pragma mark -
@implementation FORRechargeGiftCardStepThreeResponse

#pragma mark -
#pragma mark Properties
@synthesize cardNumber=cardNumber_;
@synthesize chargeAccount=chargeAccount_;
@synthesize operation=operation_;
@synthesize disclaimer=disclaimer_;
@synthesize dateAndHourOperation=dateAndHourOperation_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFORRechargeGiftCardStepThreeResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFORRechargeGiftCardStepThreeResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        if ([lname isEqualToString:@"operacion"]) {
            
            xmlAnalysisCurrentValue_ = forrgcstrxeas_AnalyzingOperation;
            
        }else if ([lname isEqualToString:@"cuentacargo"]) {
            
            xmlAnalysisCurrentValue_ = forrgcstrxeas_AnalyzingChargeAccount;
            
        }else if ([lname isEqualToString:@"tarjeta"]) {
            
            xmlAnalysisCurrentValue_ = forrgcstrxeas_AnalyzingCardNumber;
            
        } else if ([lname isEqualToString:@"textoinformativo"]) {
            
            xmlAnalysisCurrentValue_ = forrgcstrxeas_AnalyzingDisclaimer;
            
        } else if ([lname isEqualToString:@"fechahora"]) {
            
            xmlAnalysisCurrentValue_ = forrgcstrxeas_AnalyzingDateAndHourOperation;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == forrgcstrxeas_AnalyzingOperation ) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forrgcstrxeas_AnalyzingCardNumber) {
            
            [cardNumber_ release];
            cardNumber_ = nil;
            cardNumber_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forrgcstrxeas_AnalyzingChargeAccount) {
            
            [chargeAccount_ release];
            chargeAccount_ = nil;
            chargeAccount_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == forrgcstrxeas_AnalyzingDisclaimer) {
            
            [disclaimer_ release];
            disclaimer_ = nil;
            disclaimer_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == forrgcstrxeas_AnalyzingDateAndHourOperation) {
            
            [dateAndHourOperation_ release];
            dateAndHourOperation_ = nil;
            dateAndHourOperation_ = [elementString  copyWithZone:self.zone];
            
        }
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FORRechargeGiftCardStepThreeResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFORRechargeGiftCardStepThreeResponseData {
    
    [operation_ release];
    operation_=nil;
    
    [cardNumber_ release];
    cardNumber_=nil;
    
    [chargeAccount_ release];
    chargeAccount_ = nil;
    
    [disclaimer_ release];
    disclaimer_ = nil;
    
    [dateAndHourOperation_ release];
    dateAndHourOperation_ = nil;
    
    
}

@end


