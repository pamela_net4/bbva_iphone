//
//  FORCashMobileStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/24/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORCashMobileStepTwoResponse : FOInitialResponse{
    
    /**
	 * operation
	 */
    NSString *operation_;
    /**
	 * Charge account for the operation
	 */
    NSString *chargeAccount_;
    /**
	 * Charge account for the operation
	 */
    NSString *beneficiaryNumber_;

    /**
	 * amount of the operation
	 */
    NSString *amount_;
    /**
	 * amount of the operation
	 */
    NSString *coordinate_;
    /**
	 * amount of the operation
	 */
    NSString *seal_;
}

/**
 * Provides read-only access to operation
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to the chargeAccount
 */
@property (nonatomic, readonly, copy) NSString *chargeAccount;

/**
 * Provides read-only access to operation
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryNumber;

/**
 * Provides read-only access to the amount
 */
@property (nonatomic, readonly, copy) NSString *amount;

/**
 * Provides read-only access to the coordinate
 */
@property (nonatomic, readonly, copy) NSString *coordinate;

/**
 * Provides read-only access to the seal
 */
@property (nonatomic, readonly, copy) NSString *seal;

@end
