//
//  FORInstitutionPaymentStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORInstitutionPaymentStepThreeResponse : FOInitialResponse
{
    /**
	 * company to pay
	 */
    NSString *operation_;
    /**
	 * company to pay
	 */
    NSString *agreement_;
    /**
	 * access Code
	 */
    NSString *accessCode_;
    /**
	 * description of the institution
	 */
    NSString *description_;
    /**
     * date and hour of operation
     */
    NSString *dateAndHourOperation_;
    /**
     * informative text of operation
     */
    NSString *informativeText_;
}
/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, copy) NSString *operation;
/**
 * Provides read-only access to the agreement
 */
@property (nonatomic, readonly, copy) NSString *agreement;
/**
 * Provides read-only access to the accessCode
 */
@property (nonatomic, readonly, copy) NSString *accessCode;
/**
 * Provides read-only access to the description
 */
@property (nonatomic, readonly, copy) NSString *description;
/**
 * Provides read-only access Date and hour of operation
 */
@property (nonatomic, readonly, copy) NSString *dateAndHourOperation;
/**
 *Provides read-only access informative text of operation
 */
@property (nonatomic, readonly, copy) NSString *informativeText;


@end
