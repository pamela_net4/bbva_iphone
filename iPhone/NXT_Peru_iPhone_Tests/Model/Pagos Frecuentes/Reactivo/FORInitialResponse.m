//
//  FOInitialResponse.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/23/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORInitialResponse.h"
#import "ChannelList.h"
#import "ChannelList.h"
#import "FOPhoneList.h"
#import "FOEmailList.h"

#pragma mark -

/**
 * FOThirdCardPayStepTwoResponse private category
 */
@interface FORInitialResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFORInitialResponseData;

@end


#pragma mark -

@implementation FORInitialResponse

#pragma mark - properties
@synthesize service = service_;
@synthesize phoneList = phoneList_;
@synthesize emailList = emailList_;
@synthesize channelList = channelList_;
@synthesize phoneDisclaimer = phoneDisclaimer_;
@synthesize emailDisclaimer = emailDisclaimer_;
@synthesize suggestion = suggestion_;



#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFORInitialResponseData];
    
    [super dealloc];
    
}
#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOThirdCardPayStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOThirdCardPayStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		//self.notificationToPost =  kNotificationFOInitialResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFORInitialResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"servicio"]) {
            
            xmlAnalysisCurrentValue_ = forcmsorxeas_AnalyzingService;
            
        } else if ([lname isEqualToString:@"canales"]) {
            
            xmlAnalysisCurrentValue_ = forirxeas_AnalyzingChannelList;
			[channelList_ release];
            channelList_ = nil;
            channelList_ = [[ChannelList alloc] init];
            channelList_.openingTag = lname;
            [channelList_ setParentParseableObject:self];
            [parser setDelegate:channelList_];
            [channelList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString:@"listacelular"]) {
            
            xmlAnalysisCurrentValue_ = forirxeas_AnalyzingPhoneList;
            [phoneList_ release];
            phoneList_ = nil;
            phoneList_ = [[FOPhoneList alloc] init];
            phoneList_.openingTag = lname;
            [phoneList_ setParentParseableObject:self];
            [parser setDelegate:phoneList_];
            [phoneList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString:@"listacorreo"]) {
            
            xmlAnalysisCurrentValue_ = forirxeas_AnalyzingEmailList;
            [emailList_ release];
            emailList_ = nil;
            emailList_ = [[FOEmailList alloc] init];
            emailList_.openingTag = lname;
            [emailList_ setParentParseableObject:self];
            [parser setDelegate:emailList_];
            [emailList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString:@"disclaimercelulares"]) {
            
            xmlAnalysisCurrentValue_ = forirxeas_AnalyzingPhoneDisclaimer;
            
        }  else if ([lname isEqualToString:@"disclaimercorreos"]) {
            
            xmlAnalysisCurrentValue_ = forirxeas_AnalyzingEmailDisclaimer;
            
        }  else if ([lname isEqualToString:@"sugerencia"])
        {
            xmlAnalysisCurrentValue_ = forirxeas_AnalyzingSuggestion;
            
        }  else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}






/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (xmlAnalysisCurrentValue_ == forcmsorxeas_AnalyzingService ) {
            [service_ release];
            service_ = nil;
            service_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forirxeas_AnalyzingPhoneDisclaimer) {
            
            [phoneDisclaimer_ release];
            phoneDisclaimer_ = nil;
            phoneDisclaimer_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == forirxeas_AnalyzingEmailDisclaimer) {
            
            [emailDisclaimer_ release];
            emailDisclaimer_ = nil;
            emailDisclaimer_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == forirxeas_AnalyzingSuggestion) {
            
            [suggestion_ release];
            suggestion_ = nil;
            suggestion_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    

    
    
}

@end


#pragma mark -

@implementation FORInitialResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFORInitialResponseData{
    
    [service_ release];
    service_ = nil;
    
    [phoneList_ release];
    phoneList_ = nil;
    
    [emailList_ release];
    emailList_ = nil;
    
    [channelList_ release];
    channelList_ = nil;
    
    [phoneDisclaimer_ release];
    phoneDisclaimer_ = nil;
    
    [emailDisclaimer_ release];
    emailDisclaimer_ = nil;
    
    [suggestion_ release];
    suggestion_ = nil;
}

@end
