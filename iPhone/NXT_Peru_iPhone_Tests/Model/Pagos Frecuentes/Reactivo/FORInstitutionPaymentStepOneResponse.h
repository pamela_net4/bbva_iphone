//
//  FORInstitutionPaymentStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORInitialResponse.h"

@interface FORInstitutionPaymentStepOneResponse : FORInitialResponse{
    
    /**
	 * card for the agreement
	 */
    NSString *agreement_;
    /**
	 * access Code
	 */
    NSString *accessCode_;
    /**
	 * description of the institution
	 */
    NSString *description_;
    /**
     * company code
     */
    NSString *companyCode_;
    /**
     * company name
     */
    NSString *companyName_;

}
/**
 * Provides read-only access to the agreement
 */
@property (nonatomic, readonly, copy) NSString *agreement;
/**
 * Provides read-only access to the accessCode
 */
@property (nonatomic, readonly, copy) NSString *accessCode;
/**
 * Provides read-only access to the description
 */
@property (nonatomic, readonly, copy) NSString *description;
/**
 * Provides readwrite access to company code
 */
@property (nonatomic, readwrite, copy) NSString *companyCode;
/**
 * Provides readwrite access to company name
 */
@property (nonatomic, readwrite, copy) NSString *companyName;


@end