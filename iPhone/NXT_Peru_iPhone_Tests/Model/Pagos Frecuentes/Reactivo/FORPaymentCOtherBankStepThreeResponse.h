//
//  FORPaymentCOtherBankStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORPaymentCOtherBankStepThreeResponse : FOInitialResponse
{
    /**
	 * operation name
	 */
    NSString *operation_;
    
    /**
     * Card number
     */
    NSString *cardNumber_;
    
    /**
     * Card type
     */
    NSString *cardType_;
    
    /**
     * Destination Bank
     */
    
    NSString *destinationBank_;
    
    /**
     * Place Emission
     */

    NSString *placeEmission_;
    
    /**
     * Beneficiary Name
     */
    
    NSString *beneficiaryName_;
    
    /**
	 * disclaimer for the operation
	 */
    
    NSString *disclaimer_;
    
    /**
	 * date and hour for the operation
	 */
    NSString *dateAndHourOperation_;
    
}

/**
 * Provides read-only access to operation
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to card number
 */
@property (nonatomic, readonly, copy) NSString *cardNumber;

/**
 * Provides read-only access to card type
 */
@property (nonatomic, readonly, copy) NSString *cardType;

/**
 * Provides read-only access to destination bank
 */
@property (nonatomic, readonly, copy) NSString *destinationBank_;

/**
 * Provides read-only access to place emission
 */
@property (nonatomic, readonly, copy) NSString *placeEmission;

/**
 * Provides read-only access to beneficiary name
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryName;

/**
 * Provides read-only access to the mailDisclaimer
 */
@property (nonatomic, readonly, copy) NSString *disclaimer;

/**
 * Provides read-only access to the date and hour
 */
@property (nonatomic, readonly, copy) NSString *dateAndHourOperation;

@end
