//
//  FORPaymentCOtherBankStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORPaymentCOtherBankStepTwoResponse.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    forpcobstrxeas_AnalyzingOperation = foirxeas_ElementsCount,
    forpcobstrxeas_AnalyzingCardNumber,
    forpcobstrxeas_AnalyzingCardType,
    forpcobstrxeas_AnalyzingDestinationBank,
    forpcobstrxeas_AnalyzingPlaceEmission,
    forpcobstrxeas_AnalyzingBeneficiaryName,
    forpcobstrxeas_AnalyzingCoordinate,
    forpcobstrxeas_AnalyzingSeal
    
	
} FORPaymentCOtherBankStepTwoResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * FORPaymentCOtherBankStepThreeResponse
 */
@interface FORPaymentCOtherBankStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFORPaymentCOtherBankStepTwoResponseData;

@end


#pragma mark
@implementation FORPaymentCOtherBankStepTwoResponse
#pragma mark -
#pragma mark - Properties
@synthesize operation=operation_;
@synthesize cardNumber=cardNumber_;
@synthesize cardType=cardType_;
@synthesize destinationBank=destinationBank;
@synthesize placeEmission=placeEmission_;
@synthesize beneficiaryName=beneficiaryName_;
@synthesize coordinate=coordinate_;
@synthesize seal=seal_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
 
    [self clearFORPaymentCOtherBankStepTwoResponseData];
 
    [super dealloc];
 
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFORPaymentCOtherBankStepTwoResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"operacion"]) {
            
            xmlAnalysisCurrentValue_ = forpcobstrxeas_AnalyzingOperation;
            
        } else if ([lname isEqualToString:@"numerotarjeta"]) {
            
            xmlAnalysisCurrentValue_ = forpcobstrxeas_AnalyzingCardNumber;
            
        } else if ([lname isEqualToString:@"clase"]) {
            
            xmlAnalysisCurrentValue_ = forpcobstrxeas_AnalyzingCardType;
            
        } else if ([lname isEqualToString:@"bancodestino"]) {
            
            xmlAnalysisCurrentValue_ = forpcobstrxeas_AnalyzingDestinationBank;
            
        } else if ([lname isEqualToString:@"lugaremision"]) {
            
            xmlAnalysisCurrentValue_ = forpcobstrxeas_AnalyzingPlaceEmission;
            
        }else if ([lname isEqualToString:@"nombrebeneficiarioc"]) {
            
            xmlAnalysisCurrentValue_ = forpcobstrxeas_AnalyzingBeneficiaryName;
            
        }else if ([lname isEqualToString:@"coordenada"]) {
            
            xmlAnalysisCurrentValue_ = forpcobstrxeas_AnalyzingCoordinate;
            
        } else if ([lname isEqualToString:@"sello"]) {
            
            xmlAnalysisCurrentValue_ = forpcobstrxeas_AnalyzingSeal;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == forpcobstrxeas_AnalyzingOperation ) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == forpcobstrxeas_AnalyzingCardNumber) {
            
            [cardNumber_ release];
            cardNumber_ = nil;
            cardNumber_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == forpcobstrxeas_AnalyzingCardType) {
            
            [cardType_ release];
            cardType_ = nil;
            cardType_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == forpcobstrxeas_AnalyzingDestinationBank) {
            
            [destinationBank release];
            destinationBank = nil;
            destinationBank = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == forpcobstrxeas_AnalyzingPlaceEmission) {
            
            [placeEmission_ release];
            placeEmission_ = nil;
            placeEmission_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == forpcobstrxeas_AnalyzingBeneficiaryName) {
            
            [beneficiaryName_ release];
            beneficiaryName_ = nil;
            beneficiaryName_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == forpcobstrxeas_AnalyzingCoordinate) {
            
            [coordinate_ release];
            coordinate_ = nil;
            coordinate_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == forpcobstrxeas_AnalyzingSeal) {
            
            [seal_ release];
            seal_ = nil;
            seal_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
    
}

@end

#pragma mark -

@implementation FORPaymentCOtherBankStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFORPaymentCOtherBankStepTwoResponseData {
    
    [operation_ release];
    operation_ = nil;
    
    [cardNumber_ release];
    cardNumber_ = nil;
    
    [cardType_ release];
    cardType_ = nil;
    
    [destinationBank release];
    destinationBank = nil;
    
    [placeEmission_ release];
    placeEmission_ = nil;
    
    [beneficiaryName_ release];
    beneficiaryName_ = nil;
    
    [coordinate_ release];
    coordinate_ = nil;
    
    [seal_ release];
    seal_ = nil;
    
    
    
}

@end

