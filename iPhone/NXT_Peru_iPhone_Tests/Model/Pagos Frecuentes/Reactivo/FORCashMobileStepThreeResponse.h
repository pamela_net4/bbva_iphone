//
//  FORCashMobileStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FORCashMobileStepThreeResponse : FOInitialResponse
{
    /**
	 * operation name
	 */
    NSString *operation_;
    /**
	 * Charge account for the operation
	 */
    NSString *chargeAccount_;
    /**
	 * phone for the operation
	 */
    NSString *phone_;
    /**
	 * amount of the operation
	 */
    NSString *amount_;
    /**
	 * disclaimer for the operation
	 */
    NSString *disclaimer_;
    /**
	 * date and hour for the operation
	 */
    NSString *dateAndHourOperation_;
    
}

/**
 * Provides read-only access to service name
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to the chargeAccount
 */
@property (nonatomic, readonly, copy) NSString *chargeAccount;

/**
 * Provides read-only access to the phone
 */
@property (nonatomic, readonly, copy) NSString *phone;

/**
 * Provides read-only access to the amount
 */
@property (nonatomic, readonly, copy) NSString *amount;

/**
 * Provides read-only access to the mailDisclaimer
 */
@property (nonatomic, readonly, copy) NSString *disclaimer;

/**
 * Provides read-only access to the date and hour
 */
@property (nonatomic, readonly, copy) NSString *dateAndHourOperation;

@end
