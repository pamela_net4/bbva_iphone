//
//  FOECarrier.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 2/05/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@interface FOECarrier : StatusEnabledResponse {

    @private
    NSString *carrier_;
    NSString *carrierCode_;
    NSString *max_;
    NSString *min_;
    NSString *message_;
    NSString *description_;
    NSString *carrierDisplayName_;
    
}

@property (nonatomic, readonly, copy) NSString *carrier;
@property (nonatomic, readonly, copy) NSString *carrierCode;
@property (nonatomic, readonly, copy) NSString *max;
@property (nonatomic, readonly, copy) NSString *min;
@property (nonatomic, readonly, copy) NSString *message;
@property (nonatomic, readonly, copy) NSString *description;

-(void)clearData;

@end
