//
//  FOCard.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//
/**
 NSString *cardNumber_;
 NSString *description_;
 */
#import "FOCard.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    focxeas_AnalyzingCardNumber = serxeas_ElementsCount, //!<Analyzing the code
    focxeas_AnalyzingCardType, //!<Analyzing the card type
    focxeas_AnalyzingCurrency, //!<Analyzing the currency
    focxeas_AnalyzingParameter //!<Analyzing the Parameter
    
} FOPlaceXMLElementAnalyzerState;

@interface FOCard(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOCardData;

@end


@implementation FOCard


#pragma mark-
#pragma mark Properties

@synthesize cardNumber = cardNumber_;
@synthesize cardType = cardType_;
@synthesize currency = currency_;
@synthesize param = param_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOCardData];
    [super dealloc];
    
}


#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOCardData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"numerotarjeta"]) {
            
            xmlAnalysisCurrentValue_ = focxeas_AnalyzingCardNumber;
            
        } else if ([lname isEqualToString:@"tipotarjeta"]){
            
            xmlAnalysisCurrentValue_ = focxeas_AnalyzingCardType;
            
        } else if ([lname isEqualToString:@"moneda"]){
            
            xmlAnalysisCurrentValue_ = focxeas_AnalyzingCurrency;
            
        } else if ([lname isEqualToString:@"paramtarjeta"]){
            
            xmlAnalysisCurrentValue_ = focxeas_AnalyzingParameter;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}



/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == focxeas_AnalyzingCardNumber) {
            [cardNumber_ release];
            cardNumber_ = nil;
            cardNumber_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == focxeas_AnalyzingCardType)
        {
            [cardType_ release];
            cardType_ = nil;
            cardType_ =[elementString copyWithZone:self.zone];
        } else if(xmlAnalysisCurrentValue_ == focxeas_AnalyzingCurrency)
        {
            [currency_ release];
            currency_ = nil;
            currency_ =[elementString copyWithZone:self.zone];
        } else if(xmlAnalysisCurrentValue_ == focxeas_AnalyzingParameter)
        {
            [param_ release];
            param_ = nil;
            param_ =[elementString copyWithZone:self.zone];
        }
    }
    else
    {
        informationUpdated_ = soie_InfoAvailable;
    }
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
}




@end

#pragma mark -

@implementation FOCard(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOCardData {
    [cardNumber_ release];
    cardNumber_ = nil;
    
    [cardType_ release];
    cardType_ = nil;
	
    [currency_ release];
    currency_ = nil;
    
    [param_ release];
    param_ = nil;
}

@end



