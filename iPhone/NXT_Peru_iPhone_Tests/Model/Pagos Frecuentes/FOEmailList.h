//
//  FOEmailList.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/27/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@class FOEmail;
@interface FOEmailList : StatusEnabledResponse
{
@private
    FOEmail *auxFOEmail_;
    NSMutableArray *foEmailArray_;
}

@property(nonatomic,readonly,retain) NSMutableArray *foEmailArray;

/**
 * Provides read-only access to the Places count
 */
@property(nonatomic,readonly,assign) NSUInteger foEmailCount;

/**
 * Returns the FOEmail located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the FOEmail is located at
 * @return FOEmail located at the given position, or nil if position is not valid
 */
- (FOEmail *)foEmailAtPosition:(NSUInteger)aPosition;

/**
 * Remove the contained data
 */
- (void)removeData;

@end