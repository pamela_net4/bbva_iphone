//
//  FOCardList.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOCardList.h"
#import "FOCard.h"

#pragma mark -
/**
 * TransactionDetailResponse private category
 */
@interface FOCardList(private)
/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOCardListData;

@end

#pragma mark -

@implementation FOCardList

#pragma mark -
#pragma mark Properties

@dynamic  foCardArray;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearFOCardListData];
	
    [foCardArray_ release];
    foCardArray_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a TransferSuccessResponse instance providing the associated notification
 *
 * @return The initialized TransferDetailResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        foCardArray_ = [[NSMutableArray alloc] init];
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates the Cards list from another Cards list
 */
- (void)updateFrom:(FOCardList *)aCardList {
    
    NSArray *otherCardListArray = aCardList.foCardArray;
    
    [self clearFOCardListData];
    
    for (FOCard *Card in otherCardListArray) {
        
        [foCardArray_ addObject:Card];
        
    }
    
    [self updateFromStatusEnabledResponse:aCardList];
    
    self.informationUpdated = aCardList.informationUpdated;
    
}


/*
 * Remove the contained data
 */
- (void)removeData {
    
	[self clearFOCardListData];
    
}

/*
 * Returns the FOCard located at the given position, or nil if position is not valid
 */
- (FOCard *)foCardAtPosition:(NSUInteger)aPosition {
    
	FOCard *result = nil;
	
	if (aPosition < [foCardArray_ count]) {
        
		result = [foCardArray_ objectAtIndex:aPosition];
        
	}
	
	return result;
    
}

/**
 * Provides read access to the number of Cards stored
 *
 * @return The count
 */
- (NSUInteger) foCardCount {
	return [foCardArray_ count];
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    [self clearFOCardListData];
    
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (auxFOCard_ != nil) {
        [foCardArray_ addObject:auxFOCard_];
        [auxFOCard_ release];
        auxFOCard_ = nil;
    }
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
	        auxFOCard_ = [[FOCard alloc] init];
            [auxFOCard_ setParentParseableObject:self];            
            auxFOCard_.openingTag = lname;
            [parser setDelegate:auxFOCard_];
            [auxFOCard_ parserDidStartDocument:parser];
            
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}


/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    if (auxFOCard_ != nil) {
        
        [foCardArray_ addObject:auxFOCard_];
		[auxFOCard_ release];
		auxFOCard_ = nil;
        
	}
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}
#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the Card list array
 *
 * @return The Card list array
 */
- (NSArray *)foCardArray {
    
    return [NSArray arrayWithArray:foCardArray_];
    
}
@end

#pragma mark -

@implementation FOCardList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOCardListData {
    
    [foCardArray_ removeAllObjects];
    [auxFOCard_ release];
    auxFOCard_ = nil;
    
}
@end
