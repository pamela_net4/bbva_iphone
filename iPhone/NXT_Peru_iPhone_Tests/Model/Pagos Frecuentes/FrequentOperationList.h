//
//  FrequentOperationList.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
@class FrequentOperation;

@interface FrequentOperationList : StatusEnabledResponse
{
    @private
    FrequentOperation *auxFrequentOperation_;
    NSMutableArray *frequentOperationArray_;
}
/**
 * Provides read-only access to the frequent Operation array
 */
@property(nonatomic,readonly,retain) NSMutableArray *frequentOperationArray;

/**
 * Provides read-only access to the Places count
 */
@property(nonatomic,readonly,assign) NSUInteger foPlaceCount;

/**
 * Returns the FOPlace located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the FrequentOperation is located at
 * @return FrequentOperation located at the given position, or nil if position is not valid
 */
- (FrequentOperation *)frequentOperationAtPosition:(NSUInteger)aPosition;

/**
 * Remove the contained data
 */
- (void)removeData;


@end
