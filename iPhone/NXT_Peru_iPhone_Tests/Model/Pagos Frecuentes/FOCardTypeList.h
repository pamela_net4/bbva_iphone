//
//  FOCardTypeList.h
//  NXT_Peru_iPad
//
//  Created by Jose Luis on 23/01/14.
//
//

#import "StatusEnabledResponse.h"
@class  FOCardType;

@interface FOCardTypeList : StatusEnabledResponse
{
    @private
    FOCardType *auxFOCardType_;
    NSMutableArray *foCardTypeArray_;
}

/**
 * Provides read-only access to the CardT ype list array
 */

@property(nonatomic,readonly,retain) NSMutableArray *foCardTypeArray;

/**
 * Provides read-only access to the Card Types count
 */
@property(nonatomic,readonly,assign) NSUInteger foCardTypeCount;

/**
 * Returns the FOCardType located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the AccountTransaction is located at
 * @return CardType located at the given position, or nil if position is not valid
 */
- (FOCardType *)foCardTypeAtPosition:(NSUInteger)aPosition;

/**
 * Remove the contained data
 */
- (void)removeData;

@end
