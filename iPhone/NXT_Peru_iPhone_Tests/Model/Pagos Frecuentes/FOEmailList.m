//
//  FOEmailList.m
//  NXT_Peru_iEmail
//
//  Created by Ricardo on 1/27/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEmailList.h"
#import "FOEmail.h"

#pragma mark -
/**
 * TransactionDetailResponse private category
 */
@interface FOEmailList(private)
/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOEmailListData;

@end

#pragma mark -


@implementation FOEmailList


#pragma mark -
#pragma mark Properties

@synthesize foEmailArray = foEmailArray_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearFOEmailListData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a TransferSuccessResponse instance providing the associated notification
 *
 * @return The initialized TransferDetailResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        foEmailArray_ = [[NSMutableArray alloc] init];
		informationUpdated_ = soie_NoInfo;
		self.notificationToPost = kNotificationTransferSuccessResponseReceived;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark Information management
/*
 * Remove the contained data
 */
- (void)removeData {
    
	[self clearFOEmailListData];
    
}

/*
 * Returns the AccountTransaction located at the given position, or nil if position is not valid
 */
- (FOEmail *)foEmailAtPosition:(NSUInteger)aPosition {
    
	FOEmail *result = nil;
	
	if (aPosition < [foEmailArray_ count]) {
        
		result = [foEmailArray_ objectAtIndex:aPosition];
        
	}
	
	return result;
    
}



#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    [foEmailArray_ release];
    foEmailArray_ = [[NSMutableArray alloc] init];
    
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (auxFOEmail_ != nil) {
        [foEmailArray_ addObject:auxFOEmail_];
        [auxFOEmail_ release];
        auxFOEmail_ = nil;
    }
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
			[auxFOEmail_ release];
            auxFOEmail_ = nil;
            auxFOEmail_ = [[FOEmail alloc] init];
            auxFOEmail_.openingTag = lname;
            [auxFOEmail_ setParentParseableObject:self];
            [parser setDelegate:auxFOEmail_];
            [auxFOEmail_ parserDidStartDocument:parser];
            
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    if (auxFOEmail_ != nil) {
        
        [foEmailArray_ addObject:auxFOEmail_];
		[auxFOEmail_ release];
		auxFOEmail_ = nil;
        
	}
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

#pragma mark -

@implementation FOEmailList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOEmailListData {
    
    [auxFOEmail_ release];
    auxFOEmail_ = nil;
    
    [foEmailArray_ release];
    foEmailArray_ = nil;
    
}
@end