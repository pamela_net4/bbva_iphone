//
//  FODestinationBankList.h
//  NXT_Peru_iPad
//
//  Created by Jose Luis on 23/01/14.
//
//

#import "StatusEnabledResponse.h"
@class  FODestinationBank;

@interface FODestinationBankList : StatusEnabledResponse
{
@private
    FODestinationBank *auxFODestinationBank_;
    NSMutableArray *foDestinationBankList_;
}

/**
 * Provides read-only access to the CardT ype list array
 */

@property(nonatomic,readonly,retain) NSArray *foDestinationBankList;

/**
 * Provides read-only access to the Card Types count
 */
@property(nonatomic,readonly,assign) NSUInteger foDestinationBankCount;

/**
 * Returns the FOCardType located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the AccountTransaction is located at
 * @return CardType located at the given position, or nil if position is not valid
 */
- (FODestinationBank *)foDestinationBankAtPosition:(NSUInteger)aPosition;

/**
 * Remove the contained data
 */
- (void)removeData;

@end
