//
//  FOECarrier.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 2/05/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOECarrier.h"

typedef enum {
    axeas_AnalyzingCarrierCode = serxeas_ElementsCount,
    axeas_AnalyzingCarrierMax,
    axeas_AnalyzingCarrierMin,
    axeas_AnalyzingCarrierMessage
} axeas_AnalyzingFOECarrierXMLElementAnalyzerState;

@implementation FOECarrier

@synthesize carrier = carrier_;
@synthesize carrierCode = carrierCode_;
@synthesize min = min_;
@synthesize max = max_;
@synthesize message = message_;

-(void) clearData{
    
    [carrier_ release];
    carrier_ = nil;
    
    [carrierCode_ release];
    carrierCode_ = nil;
    
    [min_ release];
    min_ = nil;
    
    [message_ release];
    message_ = nil;
}

-(void) parserDidStartDocument:(NSXMLParser *)parser{

    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    [self clearData];
    
}

-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {

    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing){
    
        NSString *lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"operador"]){
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingCarrierCode;
            
        } else if ([lname isEqualToString: @"maximo"]){
        
            xmlAnalysisCurrentValue_ = axeas_AnalyzingCarrierMax;
        
        } else if ([lname isEqualToString: @"minimo"]){
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingCarrierMin;
            
        } else if ([lname isEqualToString: @"mensajerango"]){
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingCarrierMessage;
            
        } else {
        
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
    
    }
}


-(void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing){
    
        if (xmlAnalysisCurrentValue_ == axeas_AnalyzingCarrierCode){
        
            [carrierCode_ release];
            carrierCode_ = nil;
            carrierCode_ = [elementString copyWithZone: self.zone];
        
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingCarrierMax){
        
            [max_ release];
            max_ = nil;
            max_ = [elementString copyWithZone: self.zone];
        
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingCarrierMin){
        
            [min_ release];
            min_ = nil;
            min_ = [elementString copyWithZone: self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingCarrierMessage){
        
            [message_ release];
            message_ = nil;
            message_ = [elementString copyWithZone: self.zone];
        }
        
    }

}

@end
