//
//  FOInitialResponse.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/23/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"
#import "ChannelList.h"

#pragma mark -

/**
 * FOThirdCardPayStepTwoResponse private category
 */
@interface FOInitialResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOInitialResponseData;

@end


#pragma mark -

@implementation FOInitialResponse


@synthesize channelList = channelList_;
@synthesize nickname = nickname_;
@synthesize shortNickname = shortNickname_;
@synthesize dayNotice = dayNotice_;
@synthesize smsMobile = smsMobile_;
@synthesize smsEmail = smsEmail_;
@synthesize service = service_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOInitialResponseData];
    
    [super dealloc];
    
}
#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOThirdCardPayStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOThirdCardPayStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		//self.notificationToPost =  kNotificationFOInitialResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOInitialResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"canales"]) {
            
            xmlAnalysisCurrentValue_ = foirxeas_AnalyzingChannelList;
            [channelList_ release];
            channelList_ = nil;
            channelList_ = [[ChannelList alloc] init];
            channelList_.openingTag = lname;
            [channelList_ setParentParseableObject:self];
            [parser setDelegate:channelList_];
            [channelList_ parserDidStartDocument:parser];
            
            
        } else if ([lname isEqualToString:@"servicio"])
        {
            xmlAnalysisCurrentValue_ = foirxeas_AnalyzingService;
            
        } else if ([lname isEqualToString:@"alias"])
        {
            xmlAnalysisCurrentValue_ = foirxeas_AnalyzingNickname;
            
        } else if ([lname isEqualToString:@"aliascorto"])
        {
            xmlAnalysisCurrentValue_ = foirxeas_AnalyzingShortNickname;
            
        } else if ([lname isEqualToString:@"diaaviso"])
        {
            xmlAnalysisCurrentValue_ = foirxeas_AnalyzingDayNotice;
            
        } else if ([lname isEqualToString:@"smsmovil"])
        {
            xmlAnalysisCurrentValue_ = foirxeas_AnalyzingSMSMobile;
            
        } else if ([lname isEqualToString:@"smsemail"])
        {
            xmlAnalysisCurrentValue_ = foirxeas_AnalyzingSMSEmail;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}






/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
       if (xmlAnalysisCurrentValue_ == foirxeas_AnalyzingService) {
           
           [service_ release];
           service_ = nil;
           service_ = [elementString  copyWithZone:self.zone];
           
       }else if (xmlAnalysisCurrentValue_ == foirxeas_AnalyzingNickname) {
           
           [nickname_ release];
           nickname_ = nil;
           nickname_ = [elementString  copyWithZone:self.zone];
           
           
       }else if (xmlAnalysisCurrentValue_ == foirxeas_AnalyzingShortNickname) {
            
            [shortNickname_ release];
            shortNickname_ = nil;
            shortNickname_ = [elementString  copyWithZone:self.zone];
            
            
        }else if (xmlAnalysisCurrentValue_ == foirxeas_AnalyzingDayNotice) {
            
            [dayNotice_ release];
            dayNotice_ = nil;
            dayNotice_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foirxeas_AnalyzingSMSMobile) {
            
            [smsMobile_ release];
            smsMobile_ = nil;
            smsMobile_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foirxeas_AnalyzingSMSEmail) {
            
            [smsEmail_ release];
            smsEmail_ = nil;
            smsEmail_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
}

@end


#pragma mark -

@implementation FOInitialResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOInitialResponseData{
    
    [channelList_ release];
    channelList_ = nil;
    [nickname_ release];
    nickname_ = nil;
    [shortNickname_ release];
    shortNickname_ = nil;
    [dayNotice_ release];
    dayNotice_ = nil;
    [smsMobile_ release];
    smsMobile_ = nil;
    [smsEmail_ release];
    smsEmail_ = nil;
    [service_ release];
    service_ = nil;
    
        
}

@end
