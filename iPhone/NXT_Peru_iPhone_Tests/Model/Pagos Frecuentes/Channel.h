//
//  Channel.h
//  NXT_Peru_iPad
//
//  Created by usuario on 23/01/14.
//
//

#import "StatusEnabledResponse.h"

@interface Channel : StatusEnabledResponse
{
@private
    
    NSString *code_;
    NSString *description_;
    
}

/**
 * Provides read-only access to the code
 */
@property (nonatomic, readonly, copy) NSString *code;

/**
 * Provides read-only access to the desciption
 */
@property (nonatomic, readonly, copy) NSString *description;



@end
