//
//  FOECarrierList.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 2/05/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
#import "FOECarrier.h"

@interface FOECarrierList : StatusEnabledResponse
{
    @private
    NSMutableArray *carrierList_;
    FOECarrier *carrierAux_;
}

@property (nonatomic, readonly, assign) NSUInteger *carrierListCount;
@property (nonatomic, readonly, copy) NSArray *carrierList;
-(FOECarrier *) carrierAtPosition : (NSUInteger) aPosition;
-(void) removeData;

@end
