//
//  Acount.h
//  NXT_Peru_iPad
//
//  Created by usuario on 23/01/14.
//
//

#import "StatusEnabledResponse.h"

@interface Account : StatusEnabledResponse
{
@private
    
    NSString *currency_;
    NSString *accountType_;
    NSString *subject_;
    NSString *balanceAvailable_;
    NSString *availableBalanceString_;
    NSString *badge_;
    NSString *param_;
    
    
}

/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the accountType
 */
@property (nonatomic, readonly, copy) NSString *accountType;
/**
 * Provides read-only access to the subject
 */
@property (nonatomic, readonly, copy) NSString *subject;

/**
 * Provides read-only access to the balanceAvailable
 */
@property (nonatomic, readonly, copy) NSString *balanceAvailable;
/**
 * Provides read-only access to the balanceAvailable
 */
@property (nonatomic, readonly, copy) NSString *availableBalanceString;
/**
 * Provides read-only access to the badge
 */
@property (nonatomic, readonly, copy) NSString *badge;
/**
 * Provides read-only access to the account param
 */
@property (nonatomic, readonly, copy) NSString *param;
/**
 * Provides read-only access to the account identification plus the description
 */
@property (nonatomic, readonly, copy) NSString *accountIdAndDescription;



@end
