//
//  AccountList.h
//  NXT_Peru_iPad
//
//  Created by usuario on 23/01/14.
//
//

#import "StatusEnabledResponse.h"

//Forward declarations
@class Account;


@interface FOAccountList : StatusEnabledResponse
{
@private
    
    /**
	 * Account array list
	 */
	NSMutableArray *accountList_;
    
	/**
	 * Auxiliar Account instance for parsing
	 */
	Account *auxAccount_;
    
}

/**
 * Provides read-only access to the Account count
 */
@property (nonatomic, readonly, assign) NSUInteger accountCount;

/**
 * Provides read-only access to the Account array list
 */
@property (nonatomic, readonly, retain) NSArray *accountList;


/**
 * Updates the Account list from another Account list
 *
 * @param aAccountList The AccountList instance to update from
 */
- (void)updateFrom:(FOAccountList *)aAccountList;

/**
 * Remove the contained data
 */
- (void)removeData;


/**
 * Returns the Account located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the Account is located at
 * @return Account located at the given position, or nil if position is not valid
 */
- (Account *)accountAtPosition:(NSUInteger)aPosition;





@end
