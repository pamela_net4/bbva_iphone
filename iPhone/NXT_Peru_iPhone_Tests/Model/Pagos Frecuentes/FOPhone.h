//
//  FOPlace.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera on 23/01/14.
//
//

#import "StatusEnabledResponse.h"

@interface FOPhone : StatusEnabledResponse
{
@private
    NSString *phoneNumber_;
    NSString *carrier_;
    NSString *code_;
}
@property (nonatomic, readonly, copy) NSString* code;
@property (nonatomic, readonly, copy) NSString* carrier;
@property (nonatomic, readonly, copy) NSString* phoneNumber;

@end
