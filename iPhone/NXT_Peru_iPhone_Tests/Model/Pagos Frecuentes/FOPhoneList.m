//
//  FOPlaceList.m
//  NXT_Peru_iPad
//
//  Created by Jose Luis on 23/01/14.
//
//

#import "FOPhoneList.h"
#import "FOPhone.h"


#pragma mark -
/**
 * TransactionDetailResponse private category
 */
@interface FOPhoneList(private)
/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPhoneListData;

@end

#pragma mark -


@implementation FOPhoneList


#pragma mark -
#pragma mark Properties

@synthesize foPhoneArray = foPhoneArray_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearFOPhoneListData];
	
     [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a TransferSuccessResponse instance providing the associated notification
 *
 * @return The initialized TransferDetailResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        foPhoneArray_ = [[NSMutableArray alloc] init];
		informationUpdated_ = soie_NoInfo;
		self.notificationToPost = kNotificationTransferSuccessResponseReceived;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark Information management
/*
 * Remove the contained data
 */
- (void)removeData {
    
	[self clearFOPhoneListData];
    
}

/*
 * Returns the AccountTransaction located at the given position, or nil if position is not valid
 */
- (FOPhone *)foPhoneAtPosition:(NSUInteger)aPosition {
    
	FOPhone *result = nil;
	
	if (aPosition < [foPhoneArray_ count]) {
        
		result = [foPhoneArray_ objectAtIndex:aPosition];
        
	}
	
	return result;
    
}



#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    [foPhoneArray_ release];
    foPhoneArray_ = [[NSMutableArray alloc] init];
    
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (auxFOPhone_ != nil) {
        [foPhoneArray_ addObject:auxFOPhone_];
        [auxFOPhone_ release];
        auxFOPhone_ = nil;
    }
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
			[auxFOPhone_ release];
            auxFOPhone_ = nil;
            auxFOPhone_ = [[FOPhone alloc] init];
            auxFOPhone_.openingTag = lname;
            [auxFOPhone_ setParentParseableObject:self];
            [parser setDelegate:auxFOPhone_];
            [auxFOPhone_ parserDidStartDocument:parser];
            
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    if (auxFOPhone_ != nil) {
        
        [foPhoneArray_ addObject:auxFOPhone_];
		[auxFOPhone_ release];
		auxFOPhone_ = nil;
        
	}
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

#pragma mark -

@implementation FOPhoneList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPhoneListData {
    
    [auxFOPhone_ release];
    auxFOPhone_ = nil;
    
    [foPhoneArray_ release];
    foPhoneArray_ = nil;
    
}
@end