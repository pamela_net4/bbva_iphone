//
//  FOEmail.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/27/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@interface FOEmail : StatusEnabledResponse
{
@private
    NSString *email_;
    NSString *code_;
}
@property (nonatomic, readonly, copy) NSString* code;
@property (nonatomic, readonly, copy) NSString* email;
@end
