//
//  FOPDirectTVGeneralStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPDirectTVGeneralStepOneResponse.h"
#import "ChannelList.h"
#import "FOAccountList.h"


/**
 * Enumerates the analysis states
 */
typedef enum {
    
    fopdtgsorxeas_AnalyzingService = serxeas_ElementsCount, //!<Analyzing the channels list
    fopdtgsorxeas_AnalyzingInstitution,
    fopdtgsorxeas_AnalyzingCostumerNumber,
    fopdtgsorxeas_AnalyzingAccountList,
    fopdtgsorxeas_AnalyzingChannelList
    
} FOPDirectTVGeneralStepOneResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * RetentionListResponse private category
 */
@interface FOPDirectTVGeneralStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPDirectTVGeneralStepOneResponseData;

@end

@implementation FOPDirectTVGeneralStepOneResponse

#pragma mark -
#pragma mark Properties
@synthesize service = service_;
@synthesize institution = institution_;
@synthesize costumerNumber=costumerNumber_;
@synthesize channelList=channelList_;
@synthesize accountList=accountList_;


#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearFOPDirectTVGeneralStepOneResponseData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPDirectTVGeneralStepOneResponse instance providing the associated notification
 *
 * @return The initialized FOPDirectTVGeneralStepOneResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationFOPDirectTVGeneralStepOneResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPDirectTVGeneralStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if([lname isEqualToString:@"servicio"])
        {
            xmlAnalysisCurrentValue_ = fopdtgsorxeas_AnalyzingService;
        }
        else if([lname isEqualToString:@"institucion"])
        {
            xmlAnalysisCurrentValue_ = fopdtgsorxeas_AnalyzingInstitution;
        }
        else if([lname isEqualToString:@"numerocliente"])
        {
            xmlAnalysisCurrentValue_ = fopdtgsorxeas_AnalyzingCostumerNumber;
        }
        else if ([lname isEqualToString: @"canales"]) {
            
            xmlAnalysisCurrentValue_ = fopdtgsorxeas_AnalyzingChannelList;
			[channelList_ release];
            channelList_ = nil;
            channelList_ = [[ChannelList alloc] init];
            channelList_.openingTag = lname;
            [channelList_ setParentParseableObject:self];
            [parser setDelegate:channelList_];
            [channelList_ parserDidStartDocument:parser];
            
        }
        else if ([lname isEqualToString: @"cuentas"]) {
            
            xmlAnalysisCurrentValue_ = fopdtgsorxeas_AnalyzingAccountList;
			[accountList_ release];
            accountList_ = nil;
            accountList_ = [[FOAccountList alloc] init];
            accountList_.openingTag = lname;
            [accountList_ setParentParseableObject:self];
            [parser setDelegate:channelList_];
            [accountList_ parserDidStartDocument:parser];
            
            
        } else  {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
      
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

        if(xmlAnalysisCurrentValue_ == fopdtgsorxeas_AnalyzingService)
        {
            [service_ release];
            service_ = nil;
            service_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == fopdtgsorxeas_AnalyzingInstitution)
        {
            [institution_ release];
            institution_ = nil;
            institution_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == fopdtgsorxeas_AnalyzingCostumerNumber)
        {
            [costumerNumber_ release];
            costumerNumber_ = nil;
            costumerNumber_ = [elementString copyWithZone:self.zone];

        }
        else
        if ([lname isEqualToString: @"msg-s"]) {
            
            informationUpdated_ = soie_InfoAvailable;
            [parser setDelegate:parentParseableObject_];
            parentParseableObject_ = nil;
            
        }
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
    }
    
}

@end


#pragma mark -

@implementation FOPDirectTVGeneralStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPDirectTVGeneralStepOneResponseData{
    
    [channelList_ release];
    channelList_ = nil;
    
    [accountList_ release];
    accountList_ = nil;
    
    [service_ release];
    service_ = nil;
    
    [institution_ release];
    institution_ = nil;
    
    [costumerNumber_ release];
    costumerNumber_ = nil;

    
}

@end

