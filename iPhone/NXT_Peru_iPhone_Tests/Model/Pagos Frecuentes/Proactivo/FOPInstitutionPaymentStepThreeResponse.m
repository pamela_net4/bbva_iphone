//
//  FOPInstitutionPaymentStepThreeResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPInstitutionPaymentStepThreeResponse.h"
/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fopipsthrxeas_AnalyzingOperation =  foirxeas_ElementsCount,
    fopipsthrxeas_AnalyzingAgreement,
    fopipsthrxeas_AnalyzingAccessCode,
    fopipsthrxeas_AnalyzingDescription,
    fopipsthrxeas_AnalyzingDateHour,
    fopipsthrxeas_AnalyzingInformativeMessage
    
    
} FOPInstitutionPaymentStepThreeResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPInstitutionPaymentStepThreeResponse private category
 */
@interface FOPInstitutionPaymentStepThreeResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPInstitutionPaymentStepThreeResponseData;

@end


#pragma mark -



@implementation FOPInstitutionPaymentStepThreeResponse



@synthesize operation = operation_;
@synthesize accessCode = accessCode_;
@synthesize description = description_;
@synthesize agreement = agreement_;
@synthesize dateHour = dateHour_;
@synthesize informativemessage = informativemessage_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPInstitutionPaymentStepThreeResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPInstitutionPaymentStepThreeResponse instance providing the associated notification
 *
 * @return The initialized FOPInstitutionPaymentStepThreeResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOPInstitutionPaymentStepThreeResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPInstitutionPaymentStepThreeResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"operacion"]) {
            
            xmlAnalysisCurrentValue_ = fopipsthrxeas_AnalyzingOperation;
            
            
        }  else if ([lname isEqualToString:@"codigoacceso"])
        {
            xmlAnalysisCurrentValue_ = fopipsthrxeas_AnalyzingAccessCode;
            
        } else if ([lname isEqualToString:@"convenio"])
        {
            xmlAnalysisCurrentValue_ = fopipsthrxeas_AnalyzingAgreement;
            
        } else if ([lname isEqualToString:@"descripcion"])
        {
            xmlAnalysisCurrentValue_ = fopipsthrxeas_AnalyzingDescription;
            
        }
        else if ([lname isEqualToString:@"fechahora"])
        {
            xmlAnalysisCurrentValue_ = fopipsthrxeas_AnalyzingDateHour;
            
        }
        else if ([lname isEqualToString:@"mensajeinformativo"])
        {
            xmlAnalysisCurrentValue_ = fopipsthrxeas_AnalyzingInformativeMessage;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}




/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fopipsthrxeas_AnalyzingOperation ) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == fopipsthrxeas_AnalyzingAccessCode) {
            
            [accessCode_ release];
            accessCode_ = nil;
            accessCode_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopipsthrxeas_AnalyzingDescription) {
            
            [description_ release];
            description_ = nil;
            description_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopipsthrxeas_AnalyzingAgreement) {
            
            [agreement_ release];
            agreement_ = nil;
            agreement_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopipsthrxeas_AnalyzingDateHour) {
            
            [dateHour_ release];
            dateHour_ = nil;
            dateHour_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopipsthrxeas_AnalyzingInformativeMessage) {
            
            [informativemessage_ release];
            informativemessage_ = nil;
            informativemessage_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOPInstitutionPaymentStepThreeResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPInstitutionPaymentStepThreeResponseData {
    
    [operation_ release];
    operation_ = nil;
    [accessCode_ release];
    accessCode_ = nil;
    [description_ release];
    description_ = nil;
    [agreement_ release];
    agreement_ = nil;
    [dateHour_ release];
    dateHour_ = nil;
    [informativemessage_ release];
    informativemessage_ = nil;
    
    
}



@end
