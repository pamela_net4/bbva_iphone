//
//  FOPElectricServiceStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOPElectricServiceStepTwoResponse : FOInitialResponse
{
@private
    /**
     * company
     */
    NSString *company_;
    /**
     * account Payment
     */
    NSString *accountPayment_;
    /**
     * currency payment
     */
    NSString *currencyPayment_;
    /**
     * supply
     */
    NSString *supply_;

    
    
}
/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, retain) NSString *company;
/**
 * Provides read-only access to the account Payment
 */
@property (nonatomic, readonly, retain) NSString *accountPayment;
/**
 * Provides read-only access to the currency payment
 */
@property (nonatomic, readonly, retain) NSString *currencyPayment;
/**
 * Provides read-only access to the supply
 */
@property (nonatomic, readonly, retain) NSString *supply;



@end
