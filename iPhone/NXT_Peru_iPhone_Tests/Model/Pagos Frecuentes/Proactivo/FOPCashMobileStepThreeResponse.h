//
//  FOPCashMobileStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOPCashMobileStepThreeResponse : FOInitialResponse
{
@private
    /**
     * operation
     */
    NSString *operation_;
    /**
     * charge account
     */
    NSString *chargeAccount_;
    /**
     * recipent Cell
     */
    NSString *recipentCell_;
    /**
     * amount
     */
    NSString *amount_;
    /**
     * date ad hour
     */
    NSString *dateHour_;
    /**
     * informative message
     */
    NSString *informativemessage_;
    
}

@property (nonatomic, readonly, retain) NSString *operation;
/**
 * Provides read-only access to the charge account
 */
@property (nonatomic, readonly, retain) NSString *chargeAccount;
/**
 * Provides read-only access to the recipent cell
 */
@property (nonatomic, readonly, retain) NSString *recipentCell;
/**
 * Provides read-only access to the amount
 */
@property (nonatomic, readonly, retain) NSString *amount;
/**
 * Provides read-only access to the date hour
 */
@property (nonatomic, readonly, retain) NSString *dateHour;
/**
 * Provides read-only access to the informative message
 */
@property (nonatomic, readonly, retain) NSString *informativemessage;



@end
