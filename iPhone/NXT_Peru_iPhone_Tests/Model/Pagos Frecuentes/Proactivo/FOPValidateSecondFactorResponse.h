//
//  FOPValidateSecondFactorResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@interface FOPValidateSecondFactorResponse : StatusEnabledResponse
{
    @private
    NSString *indication_;
}
/**
 * Provides read-only access to the frequent Operation array
 */
@property(nonatomic,readonly,retain) NSString *indication;
@end
