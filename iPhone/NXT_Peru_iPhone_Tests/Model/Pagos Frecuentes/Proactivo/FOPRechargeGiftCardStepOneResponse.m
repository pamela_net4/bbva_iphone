//
//  FORechargeGiftCardStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPRechargeGiftCardStepOneResponse.h"
#import "FOAccountList.h"
#import "FOCardList.h"
#import "ChannelList.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    forgcsorxeas_AnalyzingChannelList = serxeas_ElementsCount, //!<Analyzing the channels list
    forgcsorxeas_AnalyzingAccountList,
    forgcsorxeas_AnalyzingFOCardList
    
    
} FORechargeGiftCardStepOneResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FORechargeGiftCardStepOneResponse private category
 */
@interface FOPRechargeGiftCardStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPRechargeGiftCardStepOneResponseData;

@end



@implementation FOPRechargeGiftCardStepOneResponse
#pragma mark -
#pragma mark Properties

@synthesize accountList = accountList_;
@synthesize foCardList = foCardList_;
@synthesize channelList = channelList_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearFOPRechargeGiftCardStepOneResponseData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FORechargeGiftCardStepOneResponse instance providing the associated notification
 *
 * @return The initialized FORechargeGiftCardStepOneResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationFOPRechargeGiftCardStepOneResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPRechargeGiftCardStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"cuentas"]) {
            
            xmlAnalysisCurrentValue_ = forgcsorxeas_AnalyzingAccountList;
			[accountList_ release];
            accountList_ = nil;
            accountList_ = [[FOAccountList alloc] init];
            accountList_.openingTag = lname;
            [accountList_ setParentParseableObject:self];
            [parser setDelegate:accountList_];
            [accountList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString: @"tarjetas"]) {
            
            xmlAnalysisCurrentValue_ = forgcsorxeas_AnalyzingFOCardList;
			[foCardList_ release];
            foCardList_ = nil;
            foCardList_ = [[FOCardList alloc] init];
            foCardList_.openingTag = lname;
            [foCardList_ setParentParseableObject:self];
            [parser setDelegate:foCardList_];
            [foCardList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString: @"canales"]) {
            
            xmlAnalysisCurrentValue_ = forgcsorxeas_AnalyzingFOCardList;
			[channelList_ release];
            channelList_ = nil;
            channelList_ = [[ChannelList alloc] init];
            channelList_.openingTag = lname;
            [channelList_ setParentParseableObject:self];
            [parser setDelegate:channelList_];
            [channelList_ parserDidStartDocument:parser];
            
        }    else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
   
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        if ([lname isEqualToString: @"msg-s"]) {
            
            informationUpdated_ = soie_InfoAvailable;
            [parser setDelegate:parentParseableObject_];
            parentParseableObject_ = nil;
            
        }
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
    }

    
    
}




@end

#pragma mark -

@implementation FOPRechargeGiftCardStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPRechargeGiftCardStepOneResponseData{
    
    [channelList_ release];
    channelList_ = nil;
    
    [accountList_ release];
    accountList_ = nil;
    
    [foCardList_ release];
    foCardList_ = nil;


}

@end

