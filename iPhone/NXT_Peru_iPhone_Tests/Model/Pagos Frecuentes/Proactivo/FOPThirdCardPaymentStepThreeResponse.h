//
//  FOThirdCardPayStepTwoResponse.h
//  NXT_Peru_iPad
//
//  Created by usuario on 23/01/14.
//
//

#import "StatusEnabledResponse.h"
#import "FOInitialResponse.h"


@interface FOPThirdCardPaymentStepThreeResponse : FOInitialResponse
{
@private
    /**
     * operation
     */
    NSString *operation_;
    /**
     * charge account
     */
    NSString *chargeAccount_;
    /**
     * card Number
     */
    NSString *cardNumber_;
    /**
     * name Holder
     */
    NSString *nameHolder_;
    /**
     * date ad hour
     */
    NSString *dateHour_;
    /**
     * informative message
     */
    NSString *informativemessage_;
    
    
}
/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, retain) NSString *operation;
/**
 * Provides read-only access to the charge account
 */
@property (nonatomic, readonly, retain) NSString *chargeAccount;
/**
 * Provides read-only access to the card number
 */
@property (nonatomic, readonly, retain) NSString *cardNumber;
/**
 * Provides read-only access to the name Holder
 */
@property (nonatomic, readonly, retain) NSString *nameHolder;
/**
 * Provides read-only access to the date hour
 */
@property (nonatomic, readonly, retain) NSString *dateHour;
/**
 * Provides read-only access to the informative message
 */
@property (nonatomic, readonly, retain) NSString *informativemessage;



@end
