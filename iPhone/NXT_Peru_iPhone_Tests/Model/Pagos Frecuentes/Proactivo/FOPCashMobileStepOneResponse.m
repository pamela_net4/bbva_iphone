//
//  FOPCashMobileStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPCashMobileStepOneResponse.h"
#import "FOAccountList.h"
#import "ChannelList.h"
#import "AlterCarrierList.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fopcmsorxeas_AnalyzingAccountList = serxeas_ElementsCount, //!<Analyzing the account list
    fopcmsorxeas_AnalyzingChannelList, //!<Analyzing the channel list
    fopcmsorxeas_AnalyzingCarrierList
} FOPCashMobileStepOneResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPCashMobileStepOneResponse private category
 */
@interface FOPCashMobileStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPCashMobileStepOneResponseData;

@end


#pragma mark -
@implementation FOPCashMobileStepOneResponse


#pragma mark -
#pragma mark Properties

@synthesize accountList = accountList_;
@synthesize channelList = channelList_;
@synthesize carrierList = carrierList_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPCashMobileStepOneResponseData];
    
    [super dealloc];
    
}


#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPCashMobileStepOneResponse instance providing the associated notification
 *
 * @return The initialized FOPCashMobileStepOneResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOPCashMobileStepOneResponse;
        
    }
	
	return self;
    
}


#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPCashMobileStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"canales"]) {
            
            xmlAnalysisCurrentValue_ = fopcmsorxeas_AnalyzingChannelList;
			[channelList_ release];
            channelList_ = nil;
            channelList_ = [[ChannelList alloc] init];
            channelList_.openingTag = lname;
            [channelList_ setParentParseableObject:self];
            [parser setDelegate:channelList_];
            [channelList_ parserDidStartDocument:parser];
            
            
        } else if ([lname isEqualToString: @"cuentas"]) {
            
            xmlAnalysisCurrentValue_ = fopcmsorxeas_AnalyzingAccountList;
			[accountList_ release];
            accountList_ = nil;
            accountList_ = [[FOAccountList alloc] init];
            accountList_.openingTag = lname;
            [accountList_ setParentParseableObject:self];
            [parser setDelegate:accountList_];
            [accountList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString: @"operadores"]) {
            
            xmlAnalysisCurrentValue_ = fopcmsorxeas_AnalyzingCarrierList;
			[carrierList_ release];
            carrierList_ = nil;
            carrierList_ = [[AlterCarrierList alloc] init];
            carrierList_.openingTag = lname;
            [carrierList_ setParentParseableObject:self];
            [parser setDelegate:carrierList_];
            [carrierList_ parserDidStartDocument:parser];
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}





/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
      
        if ([lname isEqualToString: @"msg-s"]) {
            
            informationUpdated_ = soie_InfoAvailable;
            [parser setDelegate:parentParseableObject_];
            parentParseableObject_ = nil;
            
        }
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
    }
    
}

@end


#pragma mark -

@implementation FOPCashMobileStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPCashMobileStepOneResponseData {
    
    [accountList_ release];
    accountList_ = nil;
    
    [channelList_ release];
    channelList_ = nil;
    
    [carrierList_ release];
    carrierList_ = nil;
    
}


@end
