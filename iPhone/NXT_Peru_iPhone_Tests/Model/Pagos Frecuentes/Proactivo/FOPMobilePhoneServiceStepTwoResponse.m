//
//  FOPMobilePhoneServiceStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPMobilePhoneServiceStepTwoResponse.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fopmpsstwrxeas_AnalyzingOperation = serxeas_ElementsCount,
    fopmpsstwrxeas_AnalyzingSupply,
    fopmpsstwrxeas_AnalyzingChargeAccount
    
    
} FOPMobilePhoneServiceStepTwoResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPMobilePhoneServiceStepTwoResponse private category
 */
@interface FOPMobilePhoneServiceStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPMobilePhoneServiceStepTwoResponseData;

@end


#pragma mark -


@implementation FOPMobilePhoneServiceStepTwoResponse

#pragma mark -
#pragma mark Properties

@synthesize operation = operation_;
@synthesize supply = supply_;
@synthesize chargeAccount= chargeAccount_;


#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearFOPMobilePhoneServiceStepTwoResponseData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPMobilePhoneServiceStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOPMobilePhoneServiceStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationFOPMobilePhoneServiceStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPMobilePhoneServiceStepTwoResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if([lname isEqualToString:@"operacion"])
        {
            xmlAnalysisCurrentValue_ = fopmpsstwrxeas_AnalyzingOperation;
        }
        else if([lname isEqualToString:@"suministro"])
        {
            xmlAnalysisCurrentValue_ = fopmpsstwrxeas_AnalyzingSupply;
        }
        else if ([lname isEqualToString: @"cuentacargo"])
        {
            xmlAnalysisCurrentValue_ =fopmpsstwrxeas_AnalyzingChargeAccount;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(xmlAnalysisCurrentValue_ == fopmpsstwrxeas_AnalyzingOperation)
        {
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == fopmpsstwrxeas_AnalyzingSupply)
        {
            [supply_ release];
            supply_ = nil;
            supply_ = [elementString copyWithZone:self.zone];
            
        }
        else if(xmlAnalysisCurrentValue_ == fopmpsstwrxeas_AnalyzingChargeAccount)
        {
            [chargeAccount_ release];
            chargeAccount_ = nil;
            chargeAccount_ = [elementString copyWithZone:self.zone];
            
        }
        
    }else
    {
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
}


@end


#pragma mark -

@implementation FOPMobilePhoneServiceStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPMobilePhoneServiceStepTwoResponseData{
    
    [operation_ release];
    operation_ = nil;
    
    [supply_ release];
    supply_ = nil;
    
    [chargeAccount_ release];
    chargeAccount_ = nil;

}

@end