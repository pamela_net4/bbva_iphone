//
//  FOPOtherBankTransferStepThreeResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPOtherBankTransferStepThreeResponse.h"
/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fopobtsthrxeas_AnalyzingOperation =  foirxeas_ElementsCount,
    fopobtsthrxeas_AnalyzingAccountPayment,
    fopobtsthrxeas_AnalyzingDestinationBank,
    fopobtsthrxeas_AnalyzingBeneficiaryName,
    fopobtsthrxeas_AnalyzingDocumentType,
    fopobtsthrxeas_AnalyzingDocumentNumber,
    fopobtsthrxeas_AnalyzingDateHour,
    fopobtsthrxeas_AnalyzingInformativeMessage
    
    
} FOPOtherBankTransferStepThreeResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPOtherBankTransferStepThreeResponse private category
 */
@interface FOPOtherBankTransferStepThreeResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPOtherBankTransferStepThreeResponseData;

@end


#pragma mark -




@implementation FOPOtherBankTransferStepThreeResponse



@synthesize operation = operation_;
@synthesize destinationBank = destinationBank_;
@synthesize beneficiaryName = beneficiaryName_;
@synthesize accountPayment = accountPayment_;
@synthesize documentNumber = documentNumber_;
@synthesize documentType = documentType_;
@synthesize dateHour = dateHour_;
@synthesize informativemessage = informativemessage_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPOtherBankTransferStepThreeResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPOtherBankTransferStepThreeResponse instance providing the associated notification
 *
 * @return The initialized FOPOtherBankTransferStepThreeResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOPOtherBankTransferStepThreeResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPOtherBankTransferStepThreeResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"operacion"]) {
            
            xmlAnalysisCurrentValue_ = fopobtsthrxeas_AnalyzingOperation;
            
            
        }  else if ([lname isEqualToString:@"ctabancodestino"])
        {
            xmlAnalysisCurrentValue_ = fopobtsthrxeas_AnalyzingDestinationBank;
            
        } else if ([lname isEqualToString:@"cuentaabono"])
        {
            xmlAnalysisCurrentValue_ = fopobtsthrxeas_AnalyzingAccountPayment;
            
        } else if ([lname isEqualToString:@"nombrebeneficiario"])
        {
            xmlAnalysisCurrentValue_ = fopobtsthrxeas_AnalyzingBeneficiaryName;
            
        }
        else if ([lname isEqualToString:@"nrodoc"])
        {
            xmlAnalysisCurrentValue_ = fopobtsthrxeas_AnalyzingDocumentNumber;
            
        } else if ([lname isEqualToString:@"desctipodoc"])
        {
            xmlAnalysisCurrentValue_ = fopobtsthrxeas_AnalyzingDocumentType;
            
        }
        else if ([lname isEqualToString:@"fechahora"])
        {
            xmlAnalysisCurrentValue_ = fopobtsthrxeas_AnalyzingDateHour;
            
        }
        else if ([lname isEqualToString:@"mensajeinformativo"])
        {
            xmlAnalysisCurrentValue_ = fopobtsthrxeas_AnalyzingInformativeMessage;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}




/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fopobtsthrxeas_AnalyzingOperation ) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == fopobtsthrxeas_AnalyzingDestinationBank) {
            
            [destinationBank_ release];
            destinationBank_ = nil;
            destinationBank_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopobtsthrxeas_AnalyzingBeneficiaryName) {
            
            [beneficiaryName_ release];
            beneficiaryName_ = nil;
            beneficiaryName_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopobtsthrxeas_AnalyzingAccountPayment) {
            
            [accountPayment_ release];
            accountPayment_ = nil;
            accountPayment_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopobtsthrxeas_AnalyzingDocumentNumber) {
            
            [documentNumber_ release];
            documentNumber_ = nil;
            documentNumber_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopobtsthrxeas_AnalyzingDocumentType) {
            
            [documentType_ release];
            documentType_ = nil;
            documentType_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopobtsthrxeas_AnalyzingDateHour) {
            
            [dateHour_ release];
            dateHour_ = nil;
            dateHour_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopobtsthrxeas_AnalyzingInformativeMessage) {
            
            [informativemessage_ release];
            informativemessage_ = nil;
            informativemessage_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOPOtherBankTransferStepThreeResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPOtherBankTransferStepThreeResponseData {
    
    [operation_ release];
    operation_ = nil;
    [destinationBank_ release];
    destinationBank_ = nil;
    [beneficiaryName_ release];
    beneficiaryName_ = nil;
    [accountPayment_ release];
    accountPayment_ = nil;
    [documentNumber_ release];
    documentNumber_ = nil;
    [documentType_ release];
    documentType_ = nil;
    [dateHour_ release];
    dateHour_ = nil;
    [informativemessage_ release];
    informativemessage_ = nil;
    
    
}



@end
