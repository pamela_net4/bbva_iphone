//
//  FOPThirdAccountTransferStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPThirdAccountTransferStepTwoResponse.h"
/**
 * Enumerates the analysis state
 */
typedef enum {
    
    foptatstwrxeas_AnalyzingOperation =  foirxeas_ElementsCount,
    foptatstwrxeas_AnalyzingChargeAccount,
    foptatstwrxeas_AnalyzingBeneficiaryName,
    foptatstwrxeas_AnalyzingAccountPayment,
    foptatstwrxeas_AnalyzingAmount,
    foptatstwrxeas_AnalyzingCurrency
    
    
} FOPThirdAccountTransferStepTwoResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOThirdCardPayStepTwoResponse private category
 */
@interface FOPThirdAccountTransferStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPThirdAccountTransferStepTwoResponseData;

@end


#pragma mark -


@implementation FOPThirdAccountTransferStepTwoResponse


@synthesize operation = operation_;
@synthesize chargeAccount = chargeAccount_;
@synthesize beneficiaryName = beneficiaryName_;
@synthesize accountPayment = accountPayment_;
@synthesize currency = currency_;
@synthesize amount = amount_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPThirdAccountTransferStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPThirdAccountTransferStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOPThirdAccountTransferStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOPThirdAccountTransferStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPThirdAccountTransferStepTwoResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"operacion"]) {
            
            xmlAnalysisCurrentValue_ = foptatstwrxeas_AnalyzingOperation;
            
            
        }  else if ([lname isEqualToString:@"cuentacargo"])
        {
            xmlAnalysisCurrentValue_ = foptatstwrxeas_AnalyzingChargeAccount;
            
        } else if ([lname isEqualToString:@"cuentaabono"])
        {
            xmlAnalysisCurrentValue_ = foptatstwrxeas_AnalyzingAccountPayment;
            
        } else if ([lname isEqualToString:@"nombrebeneficiario"])
        {
            xmlAnalysisCurrentValue_ = foptatstwrxeas_AnalyzingBeneficiaryName;
            
        }
        else if ([lname isEqualToString:@"moneda"])
        {
            xmlAnalysisCurrentValue_ = foptatstwrxeas_AnalyzingCurrency;
            
        } else if ([lname isEqualToString:@"monto"])
        {
            xmlAnalysisCurrentValue_ = foptatstwrxeas_AnalyzingAmount;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}




/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == foptatstwrxeas_AnalyzingOperation ) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == foptatstwrxeas_AnalyzingChargeAccount) {
            
            [chargeAccount_ release];
            chargeAccount_ = nil;
            chargeAccount_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foptatstwrxeas_AnalyzingBeneficiaryName) {
            
            [beneficiaryName_ release];
            beneficiaryName_ = nil;
            beneficiaryName_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foptatstwrxeas_AnalyzingAccountPayment) {
            
            [accountPayment_ release];
            accountPayment_ = nil;
            accountPayment_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foptatstwrxeas_AnalyzingCurrency) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foptatstwrxeas_AnalyzingAmount) {
            
            [amount_ release];
            amount_ = nil;
            amount_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOPThirdAccountTransferStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPThirdAccountTransferStepTwoResponseData {
    
    [operation_ release];
    operation_ = nil;
    [chargeAccount_ release];
    chargeAccount_ = nil;
    [beneficiaryName_ release];
    beneficiaryName_ = nil;
    [accountPayment_ release];
    accountPayment_ = nil;
    [currency_ release];
    currency_ = nil;
    [amount_ release];
    amount_ = nil;
    
    
}


@end
