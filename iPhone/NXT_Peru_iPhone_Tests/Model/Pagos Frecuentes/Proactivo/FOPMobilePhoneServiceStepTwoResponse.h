//
//  FOPMobilePhoneServiceStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOPMobilePhoneServiceStepTwoResponse : FOInitialResponse
{
    @private
    NSString *operation_;
    NSString *supply_;
    NSString *chargeAccount_;
}
/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, retain) NSString *operation;

/**
 * Provides read-only access to the supply
 */
@property (nonatomic, readonly, retain) NSString *supply;

/**
 * Provides read-only access to the supply
 */
@property (nonatomic, readonly, retain) NSString *chargeAccount;

@end
