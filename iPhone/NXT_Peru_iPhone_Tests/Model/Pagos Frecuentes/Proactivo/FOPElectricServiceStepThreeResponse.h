//
//  FOPElectricServiceStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOPElectricServiceStepThreeResponse : FOInitialResponse
{
@private
    /**
     * company
     */
    NSString *company_;
    /**
     * account Payment
     */
    NSString *accountPayment_;
    /**
     * currency payment
     */
    NSString *currencyPayment_;
    /**
     * supply
     */
    NSString *supply_;
    /**
     * date ad hour
     */
    NSString *dateHour_;
    /**
     * informative message
     */
    NSString *informativemessage_;
    
    
}
/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, retain) NSString *company;
/**
 * Provides read-only access to the account Payment
 */
@property (nonatomic, readonly, retain) NSString *accountPayment;
/**
 * Provides read-only access to the currency payment
 */
@property (nonatomic, readonly, retain) NSString *currencyPayment;
/**
 * Provides read-only access to the supply
 */
@property (nonatomic, readonly, retain) NSString *supply;
/**
 * Provides read-only access to the date hour
 */
@property (nonatomic, readonly, retain) NSString *dateHour;
/**
 * Provides read-only access to the informative message
 */
@property (nonatomic, readonly, retain) NSString *informativemessage;


@end
