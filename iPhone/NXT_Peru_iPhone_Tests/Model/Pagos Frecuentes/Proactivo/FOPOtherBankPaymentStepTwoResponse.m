//
//  FOOtherBankPaymentStepTwoResponse.m
//  NXT_Peru_iPad
//
//  Created by Jose Luis on 23/01/14.
//
//

#import "FOPOtherBankPaymentStepTwoResponse.h"



/**
 * Enumerates the analysis states
 */
typedef enum {
    

    focpobpstrxeas_AnalyzingOperation = foirxeas_ElementsCount, //!<Analyzing the channels list
    focpobpstrxeas_AnalyzingCardNumber, //!<Analyzing the places
    focpobpstrxeas_AnalyzingClassName,
    focpobpstrxeas_AnalyzingDestinationBank,
    focpobpstrxeas_AnalyzingEmisionPlace,
    focpobpstrxeas_AnalyzingBeneficiaryName
    
    
    
} FOOtherBankPaymentStepTwoResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPOtherBankPaymentStepTwoResponse private category
 */
@interface FOPOtherBankPaymentStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPOtherBankPaymentStepTwoResponseData;

@end

@implementation FOPOtherBankPaymentStepTwoResponse

#pragma mark -
#pragma mark Properties

@synthesize operation = operation_;
@synthesize cardNumber= cardNumber_;
@synthesize className= className_;
@synthesize destinationBank=destinationBank_;
@synthesize emisionPlace= emisionPlace_;
@synthesize beneficiaryName=beneficiaryName_;


#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearFOPOtherBankPaymentStepTwoResponseData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPOtherBankPaymentStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOPOtherBankPaymentStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationFOPOtherBankPaymentStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPOtherBankPaymentStepTwoResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"operacion"])
        {
            xmlAnalysisCurrentValue_ = focpobpstrxeas_AnalyzingOperation;
        }
        else if ([lname isEqualToString: @"numerotarjeta"])
        {
            xmlAnalysisCurrentValue_ = focpobpstrxeas_AnalyzingCardNumber;
        }
        else if ([lname isEqualToString: @"clase"])
        {
            xmlAnalysisCurrentValue_ = focpobpstrxeas_AnalyzingClassName;
        }
        else if ([lname isEqualToString: @"bancodestino"])
        {
            xmlAnalysisCurrentValue_ = focpobpstrxeas_AnalyzingDestinationBank;
        }
        else if ([lname isEqualToString: @"lugaremision"])
        {
            xmlAnalysisCurrentValue_ = focpobpstrxeas_AnalyzingEmisionPlace;
        }
        else if ([lname isEqualToString: @"nombrebeneficiario"])
        {
            xmlAnalysisCurrentValue_ = focpobpstrxeas_AnalyzingBeneficiaryName;
        }
        else
        {
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
     if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
         
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
         
         if (xmlAnalysisCurrentValue_ == focpobpstrxeas_AnalyzingOperation)
         {
             [operation_ release];
             operation_ = nil;
             operation_ = [elementString copyWithZone:self.zone];
         }
         else if(xmlAnalysisCurrentValue_ == focpobpstrxeas_AnalyzingCardNumber)
         {
             [cardNumber_ release];
             cardNumber_ = nil;
             cardNumber_ = [elementString copyWithZone:self.zone];
         }
         else if(xmlAnalysisCurrentValue_ == focpobpstrxeas_AnalyzingClassName)
         {
             [className_ release];
             className_ = nil;
             className_ = [elementString copyWithZone:self.zone];
         }
         else if (xmlAnalysisCurrentValue_ == focpobpstrxeas_AnalyzingDestinationBank)
         {
             [destinationBank_ release];
             destinationBank_ = nil;
             destinationBank_ = [elementString copyWithZone:self.zone];
         }
         else if (xmlAnalysisCurrentValue_ == focpobpstrxeas_AnalyzingEmisionPlace)
         {
             [emisionPlace_ release];
             emisionPlace_ = nil;
             emisionPlace_ = [elementString copyWithZone:self.zone];
         }
         else if (xmlAnalysisCurrentValue_ == focpobpstrxeas_AnalyzingBeneficiaryName)
         {
             [beneficiaryName_ release];
             beneficiaryName_ = nil;
             beneficiaryName_ = [elementString copyWithZone:self.zone];
         }
     
        
         
     }
     else
     {
         informationUpdated_ = soie_InfoAvailable;
     }
    
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;

    
}




@end


#pragma mark -

@implementation FOPOtherBankPaymentStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPOtherBankPaymentStepTwoResponseData {
    
    [operation_ release];
    operation_ = nil;
        
    [cardNumber_ release];
    cardNumber_ = nil;
    
    [className_ release];
    className_ = nil;
    
    [destinationBank_ release];
    destinationBank_ = nil;
    
    [emisionPlace_ release];
    emisionPlace_ = nil;
    
    [beneficiaryName_ release];
    beneficiaryName_ = nil;
    
}
@end
