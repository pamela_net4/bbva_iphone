//
//  FOPDirectTVGeneralStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOPDirectTVGeneralStepTwoResponse : FOInitialResponse
{
    @private
    /**
     * Operation
     */
    NSString *operation_;
    /**
     * Institution
     */
    NSString *institution_;
    /**
     * Costumer Number
     */
    NSString *costumerNumber_;
    /**
     * Charge Account
     */
    NSString *chargeAccount_;
}
/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, retain) NSString *operation;
/**
 * Provides read-only access to the institution
 */
@property (nonatomic, readonly, retain) NSString *institution;
/**
 * Provides read-only access to the costumer number
 */
@property (nonatomic, readonly, retain) NSString *costumerNumber;
/**
 * Provides read-only access to the charge account
 */
@property (nonatomic, readonly, retain) NSString *chargeAccount;
@end
