//
//  FORechargeGiftCardStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOPRechargeGiftCardStepTwoResponse : FOInitialResponse
{
    @private
    NSString *operation_;
    NSString *acChargeCard_;
    NSString *chargeCurrency_;
    NSString *chargeType_;
    NSString *giftCard_;
    NSString *amount_;
    NSString *currency_;
}
/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, retain) NSString *operation;

/**
 * Provides read-only access to the account charge card
 */
@property (nonatomic, readonly, retain) NSString *acChargeCard;

/**
 * Provides read-only access to the charge currency
 */
@property (nonatomic, readonly, retain) NSString *chargeCurrency;

/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, retain) NSString *chargeType;
/**
 * Provides read-only access to the gift card
 */
@property (nonatomic, readonly, retain) NSString *giftCard;

/**
 * Provides read-only access to the amount
 */
@property (nonatomic, readonly, retain) NSString *amount;

/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, retain) NSString *currency;

@end
