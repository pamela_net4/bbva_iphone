//
//  FOThirdCardPayStepTwoResponse.m
//  NXT_Peru_iPad
//
//  Created by usuario on 23/01/14.
//
//

#import "FOPThirdCardPaymentStepThreeResponse.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fotcpsthrxeas_AnalyzingOperation =  foirxeas_ElementsCount,
    fotcpsthrxeas_AnalyzingService,
    fotcpsthrxeas_AnalyzingChargeAccount,
    fotcpsthrxeas_AnalyzingCardNumber,
    fotcpsthrxeas_AnalyzingNameHolder,
    fotcpsthrxeas_AnalyzingDateHour,
    fotcpsthrxeas_AnalyzingInformativeMessage
    
    
} FOThirdCardPaymentStepThreeResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOThirdCardPayStepTwoResponse private category
 */
@interface FOPThirdCardPaymentStepThreeResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPThirdCardPaymentStepThreeResponseData;

@end


#pragma mark -


@implementation FOPThirdCardPaymentStepThreeResponse

@synthesize operation = operation_;
@synthesize chargeAccount = chargeAccount_;
@synthesize cardNumber = cardNumber_;
@synthesize nameHolder = nameHolder_;
@synthesize dateHour = dateHour_;
@synthesize informativemessage = informativemessage_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPThirdCardPaymentStepThreeResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOThirdCardPayStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOThirdCardPayStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOPThirdCardPaymentStepThreeResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPThirdCardPaymentStepThreeResponseData];
    
}




/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"operacion"]) {
            
            xmlAnalysisCurrentValue_ = fotcpsthrxeas_AnalyzingOperation;
            
            
        }  else if ([lname isEqualToString:@"cuentacargo"])
        {
            xmlAnalysisCurrentValue_ = fotcpsthrxeas_AnalyzingChargeAccount;
            
        } else if ([lname isEqualToString:@"numerotarjeta"])
        {
            xmlAnalysisCurrentValue_ = fotcpsthrxeas_AnalyzingCardNumber;
            
        } else if ([lname isEqualToString:@"nombretitular"])
        {
            xmlAnalysisCurrentValue_ = fotcpsthrxeas_AnalyzingNameHolder;
            
        }
        else if ([lname isEqualToString:@"fechahora"])
        {
            xmlAnalysisCurrentValue_ = fotcpsthrxeas_AnalyzingDateHour;
            
        }
        else if ([lname isEqualToString:@"mensajeinformativo"])
        {
            xmlAnalysisCurrentValue_ = fotcpsthrxeas_AnalyzingInformativeMessage;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}


/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fotcpsthrxeas_AnalyzingOperation ) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == fotcpsthrxeas_AnalyzingChargeAccount) {
            
            [chargeAccount_ release];
            chargeAccount_ = nil;
            chargeAccount_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fotcpsthrxeas_AnalyzingCardNumber) {
            
            [cardNumber_ release];
            cardNumber_ = nil;
            cardNumber_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fotcpsthrxeas_AnalyzingNameHolder) {
            
            [nameHolder_ release];
            nameHolder_ = nil;
            nameHolder_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fotcpsthrxeas_AnalyzingDateHour) {
            
            [dateHour_ release];
            dateHour_ = nil;
            dateHour_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fotcpsthrxeas_AnalyzingInformativeMessage) {
            
            [informativemessage_ release];
            informativemessage_ = nil;
            informativemessage_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOPThirdCardPaymentStepThreeResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPThirdCardPaymentStepThreeResponseData {
    
    [operation_ release];
    operation_ = nil;
    [chargeAccount_ release];
    chargeAccount_ = nil;
    [cardNumber_ release];
    cardNumber_ = nil;
    [nameHolder_ release];
    nameHolder_ = nil;
    [dateHour_ release];
    dateHour_ = nil;
    [informativemessage_ release];
    informativemessage_ = nil;
    
    
}


@end
