//
//  FOPOtherBankTransferStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"
@class FOAccountList;
@class ChannelList;


@interface FOPOtherBankTransferStepOneResponse : StatusEnabledResponse
{
@private
    /**
     * Account list
     */
    FOAccountList *accountList_;
    
    /**
     * Channel list
     */
    ChannelList *channelList_;
    
}

/**
 * Provides read-only access to the Account list
 */
@property (nonatomic, readonly, retain) FOAccountList *accountList;

/**
 * Provides read-only access to the Channel list
 */
@property (nonatomic, readonly, retain) ChannelList *channelList;


@end
