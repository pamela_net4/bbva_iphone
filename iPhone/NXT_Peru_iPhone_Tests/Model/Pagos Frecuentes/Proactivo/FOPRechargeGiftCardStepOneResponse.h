//
//  FORechargeGiftCardStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@class FOAccountList;
@class FOCardList;
@class ChannelList;

@interface FOPRechargeGiftCardStepOneResponse : StatusEnabledResponse
{
    @private
    FOAccountList *accountList_;
    FOCardList *foCardList_;
    ChannelList *channelList_;
}
/**
 * Provides read-only access to the account list
 */
@property(nonatomic,readonly,retain) FOAccountList *accountList;
/**
 * Provides read-only access to the card list
 */
@property(nonatomic,readonly,retain) FOCardList *foCardList;
/**
 * Provides read-only access to the channellist
 */
@property(nonatomic,readonly,retain) ChannelList *channelList;
@end
