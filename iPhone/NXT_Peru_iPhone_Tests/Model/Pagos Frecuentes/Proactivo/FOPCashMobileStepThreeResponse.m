//
//  FOPCashMobileStepThreeResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPCashMobileStepThreeResponse.h"


/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fopcmsthrxeas_AnalyzingOperation =  foirxeas_ElementsCount,
    fopcmsthrxeas_AnalyzingChargeAccount,
    fopcmsthrxeas_AnalyzingRecipentCell,
    fopcmsthrxeas_AnalyzingAmount,
    fopcmsthrxeas_AnalyzingDateHour,
    fopcmsthrxeas_AnalyzingInformativeMessage
    
    
} FOPCashMobileStepThreeResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOThirdCardPayStepTwoResponse private category
 */
@interface FOPCashMobileStepThreeResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPCashMobileStepThreeResponseData;

@end


#pragma mark -


@implementation FOPCashMobileStepThreeResponse


@synthesize operation = operation_;
@synthesize chargeAccount = chargeAccount_;
@synthesize amount = amount_;
@synthesize recipentCell = recipentCell_;
@synthesize dateHour = dateHour_;
@synthesize informativemessage = informativemessage_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPCashMobileStepThreeResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOThirdCardPayStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOThirdCardPayStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOPCashMobileStepThreeResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPCashMobileStepThreeResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"operacion"]) {
            
            xmlAnalysisCurrentValue_ = fopcmsthrxeas_AnalyzingOperation;
            
            
        }  else if ([lname isEqualToString:@"cuentacargo"])
        {
            xmlAnalysisCurrentValue_ = fopcmsthrxeas_AnalyzingChargeAccount;
            
        } else if ([lname isEqualToString:@"monto"])
        {
            xmlAnalysisCurrentValue_ = fopcmsthrxeas_AnalyzingAmount;
            
        } else if ([lname isEqualToString:@"celularbeneficiario"])
        {
            xmlAnalysisCurrentValue_ = fopcmsthrxeas_AnalyzingRecipentCell;
            
        }else if ([lname isEqualToString:@"fechahora"])
        {
            xmlAnalysisCurrentValue_ = fopcmsthrxeas_AnalyzingDateHour;
            
        }
        else if ([lname isEqualToString:@"mensajeinformativo"])
        {
            xmlAnalysisCurrentValue_ = fopcmsthrxeas_AnalyzingInformativeMessage;
            
        }
        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}


/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fopcmsthrxeas_AnalyzingOperation ) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == fopcmsthrxeas_AnalyzingChargeAccount) {
            
            [chargeAccount_ release];
            chargeAccount_ = nil;
            chargeAccount_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopcmsthrxeas_AnalyzingAmount) {
            
            [amount_ release];
            amount_ = nil;
            amount_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopcmsthrxeas_AnalyzingRecipentCell) {
            
            [recipentCell_ release];
            recipentCell_ = nil;
            recipentCell_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopcmsthrxeas_AnalyzingDateHour) {
            
            [dateHour_ release];
            dateHour_ = nil;
            dateHour_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopcmsthrxeas_AnalyzingInformativeMessage) {
            
            [informativemessage_ release];
            informativemessage_ = nil;
            informativemessage_ = [elementString  copyWithZone:self.zone];
            
        }
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOPCashMobileStepThreeResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPCashMobileStepThreeResponseData {
    
    [operation_ release];
    operation_ = nil;
    [chargeAccount_ release];
    chargeAccount_ = nil;
    [amount_ release];
    amount_ = nil;
    [recipentCell_ release];
    recipentCell_ = nil;
    [dateHour_ release];
    dateHour_ = nil;
    [informativemessage_ release];
    informativemessage_ = nil;
    
    
}








@end
