//
//  FOPDirectTVMonthlyPaymentStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOPDirectTVMonthlyPaymentStepThreeResponse : FOInitialResponse
{
@private
    
    /**
     * Institution
     */
    NSString *institution_;
    /**
     * Costumer Number
     */
    NSString *costumerNumber_;
    /**
     * Charge Account
     */
    NSString *chargeAccount_;
    
    /**
     * Date and Hour
     */
    NSString *dateHour_;
    
    /**
     * Informative Message
     */
    NSString *informativeMessage_;
}

/**
 * Provides read-only access to the institution
 */
@property (nonatomic, readonly, retain) NSString *institution;
/**
 * Provides read-only access to the costumer number
 */
@property (nonatomic, readonly, retain) NSString *costumerNumber;
/**
 * Provides read-only access to the charge account
 */
@property (nonatomic, readonly, retain) NSString *chargeAccount;

/**
 * Provides read-only access to the Date and Hour
 */
@property (nonatomic, readonly, retain) NSString *dateHour;

/**
 * Provides read-only access to the Informative Message
 */
@property (nonatomic, readonly, retain) NSString *informativeMessage;

@end
