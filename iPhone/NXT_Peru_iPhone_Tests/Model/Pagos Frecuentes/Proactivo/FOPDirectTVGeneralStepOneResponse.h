//
//  FOPDirectTVGeneralStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@class ChannelList;
@class FOAccountList;

@interface FOPDirectTVGeneralStepOneResponse : StatusEnabledResponse
{
    @private
    NSString *service_;
    NSString *institution_;
    NSString *costumerNumber_;
    ChannelList *channelList_;
    FOAccountList *accountList_;
    
}

@property (nonatomic, readonly, retain)  NSString *service;

@property (nonatomic, readonly, retain)  NSString *institution;

@property (nonatomic, readonly, retain)  NSString *costumerNumber;

@property (nonatomic, readonly, retain)  ChannelList *channelList;

@property (nonatomic, readonly, retain)  FOAccountList *accountList;


@end
