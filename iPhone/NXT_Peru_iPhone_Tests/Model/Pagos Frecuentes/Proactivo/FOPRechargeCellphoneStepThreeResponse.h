//
//  FOPRechargeCellphoneStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOPRechargeCellphoneStepThreeResponse : FOInitialResponse

{
    @private
    NSString *businessName_;
    NSString *telephone_;
    NSString *chargeAccount_;
    NSString *chargeCurrency;
    NSString *amount_;
    NSString *dateHour_;
    NSString *informativeMessage_;
    
}

@property (nonatomic, readonly, retain) NSString *businessName;
@property (nonatomic, readonly, retain)  NSString *telephone;
@property (nonatomic, readonly, retain) NSString *chargeAccount;
@property (nonatomic, readonly, retain) NSString *chargeCurrency;
@property (nonatomic, readonly, retain) NSString *amount;
@property (nonatomic, readonly, retain) NSString *dateHour;
@property (nonatomic, readonly, retain)  NSString *informativeMessage;

@end
