//
//  FOPRechargeCellphoneStepThreeResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPRechargeCellphoneStepThreeResponse.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    
    foprcsthrxeas_AnalyzingBusiness = foirxeas_ElementsCount, //!<Analyzing the Business Name
    foprcsthrxeas_AnalyzingTelephone, //!<Analyzing the telephone
    foprcsthrxeas_AnalyzingChargeAccount,
    foprcsthrxeas_AnalyzingChargeCurrency,
    foprcsthrxeas_AnalyzingAmount,
    foprcsthrxeas_AnalyzingInfoMessage,
    foprcsthrxeas_AnalyzingDateHour
    
    
    
    
    
} FOPRechargeCellphoneStepThreeResponseXMLElementAnalyzerState;

/**
 * FOPRechargeCellphoneStepThreeResponse private category
 */
@interface FOPRechargeCellphoneStepThreeResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPRechargeCellphoneStepThreeResponseData;

@end


@implementation FOPRechargeCellphoneStepThreeResponse
#pragma mark-
#pragma mark Properties

@synthesize businessName = businessName_;
@synthesize chargeAccount = chargeAccount_;
@synthesize chargeCurrency = chargeCurrency_;
@synthesize telephone = telephone_;
@synthesize amount = amount_;
@synthesize dateHour = dateHour_;
@synthesize informativeMessage = informativeMessage_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPRechargeCellphoneStepThreeResponseData];
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPRechargeCellphoneStepThreeResponse instance providing the associated notification
 *
 * @return The initialized FOPRechargeCellphoneStepThreeResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationFOPRechargeCellphoneStepThreeResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPRechargeCellphoneStepThreeResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"empresa"])
        {
            xmlAnalysisCurrentValue_ = foprcsthrxeas_AnalyzingBusiness;
        }
        else if ([lname isEqualToString: @"telefono"])
        {
            xmlAnalysisCurrentValue_ = foprcsthrxeas_AnalyzingTelephone;
        }
        else if ([lname isEqualToString: @"cuentacargo"])
        {
            xmlAnalysisCurrentValue_ = foprcsthrxeas_AnalyzingChargeAccount;
        }
        else if ([lname isEqualToString: @"monedacargo"])
        {
            xmlAnalysisCurrentValue_ = foprcsthrxeas_AnalyzingChargeCurrency;
        }
        else if ([lname isEqualToString: @"monto"])
        {
            xmlAnalysisCurrentValue_ = foprcsthrxeas_AnalyzingAmount;
        }
        else if ([lname isEqualToString: @"mensajeinformativo"])
        {
            xmlAnalysisCurrentValue_ = foprcsthrxeas_AnalyzingInfoMessage;
        }
        else if ([lname isEqualToString: @"fechahora"])
        {
            xmlAnalysisCurrentValue_ = foprcsthrxeas_AnalyzingDateHour;
        }

        else
        {
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == foprcsthrxeas_AnalyzingBusiness)
        {
            [businessName_ release];
            businessName_ = nil;
            businessName_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == foprcsthrxeas_AnalyzingTelephone)
        {
            telephone_ = nil;
            telephone_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == foprcsthrxeas_AnalyzingChargeAccount)
        {
            chargeAccount_ = nil;
            chargeAccount_ = [elementString copyWithZone:self.zone];
        }
        else if (xmlAnalysisCurrentValue_ == foprcsthrxeas_AnalyzingChargeCurrency)
        {
            chargeCurrency_ = nil;
            chargeCurrency_ = [elementString copyWithZone:self.zone];
        }
        else if (xmlAnalysisCurrentValue_ == foprcsthrxeas_AnalyzingAmount)
        {
            amount_ = nil;
            amount_ = [elementString copyWithZone:self.zone];
        }
        else if (xmlAnalysisCurrentValue_ == foprcsthrxeas_AnalyzingInfoMessage)
        {
            informativeMessage_ = nil;
            informativeMessage_ = [elementString copyWithZone:self.zone];
        }
        else if (xmlAnalysisCurrentValue_ == foprcsthrxeas_AnalyzingDateHour)
        {
            dateHour_ = nil;
            dateHour_ = [elementString copyWithZone:self.zone];
        }
        
        
        
    }
    else
    {
        informationUpdated_ = soie_InfoAvailable;
    }
    
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end

#pragma mark -

@implementation FOPRechargeCellphoneStepThreeResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPRechargeCellphoneStepThreeResponseData {
    
    [businessName_ release];
    businessName_ = nil;
    
    [telephone_ release];
    telephone_ = nil;
    
    [chargeAccount_ release];
    chargeAccount_ = nil;
    
    [chargeCurrency_ release];
    chargeCurrency_ = nil;
    
    [amount_ release];
    amount_ = nil;
    
    [dateHour_ release];
    dateHour_ = nil;
    
    [informativeMessage_ release];
    informativeMessage_ = nil;
    
    
}
@end

