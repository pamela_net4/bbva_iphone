//
//  OFOtherBankPaymentStepOneResponse.m
//  NXT_Peru_iPad
//
//  Created by Jose Luis on 23/01/14.
//
//

#import "FOPOtherBankPaymentStepOneResponse.h"
#import "ChannelList.h"
#import "FOAccountList.h"
#import "FOCardTypeList.h"
#import "FOPlaceList.h"
#import "FODestinationBankList.h"



/**
 * Enumerates the analysis states
 */
typedef enum {
    
    foobpsorxeas_AnalyzingChannelList = serxeas_ElementsCount, //!<Analyzing the channels list
    foobpsorxeas_AnalyzingAccountList,
    foobpsorxeas_AnalyzingFOCardTypeList,
    foobpsorxeas_AnalyzingFOPlaceList, //!<Analyzing the places
    foobpsorxeas_AnalyzingFODestinationBankList
    
    
} FOOtherBankPaymentStepOneResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPOtherBankPaymentStepOneResponse private category
 */
@interface FOPOtherBankPaymentStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPOtherBankPaymentStepOneResponseData;

@end


@implementation FOPOtherBankPaymentStepOneResponse

#pragma mark -
#pragma mark Properties

@synthesize channelList = channelList_;
@synthesize accountList = accountList_;
@synthesize foCardTypeList = foCardTypeList_;
@synthesize foPlaceList = foPlaceList_;
@synthesize foDestinationBankList = foDestinationBankList_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearFOPOtherBankPaymentStepOneResponseData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPOtherBankPaymentStepOneResponse instance providing the associated notification
 *
 * @return The initialized FOPOtherBankPaymentStepOneResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationFOPOtherBankPaymentStepOneResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPOtherBankPaymentStepOneResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"canales"]) {
            
            xmlAnalysisCurrentValue_ = foobpsorxeas_AnalyzingChannelList;
			//[channelList_ release];
            channelList_ = nil;
            channelList_ = [[ChannelList alloc] init];
            channelList_.openingTag = lname;
            [channelList_ setParentParseableObject:self];
            [parser setDelegate:channelList_];
            [channelList_ parserDidStartDocument:parser];
            
            
        } else if ([lname isEqualToString: @"cuentas"]) {
            
            xmlAnalysisCurrentValue_ = foobpsorxeas_AnalyzingAccountList;
			//[additionalInformation_ release];
            accountList_ = nil;
            accountList_ = [[FOAccountList alloc] init];
            accountList_.openingTag = lname;
            [accountList_ setParentParseableObject:self];
            [parser setDelegate:accountList_];
            [accountList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString: @"cuentas"]) {
            
            xmlAnalysisCurrentValue_ = foobpsorxeas_AnalyzingAccountList;
			//[additionalInformation_ release];
            accountList_ = nil;
            accountList_ = [[FOAccountList alloc] init];
            accountList_.openingTag = lname;
            [accountList_ setParentParseableObject:self];
            [parser setDelegate:accountList_];
            [accountList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString: @"tipostarjeta"]) {
            
            xmlAnalysisCurrentValue_ = foobpsorxeas_AnalyzingFOCardTypeList;
			//[additionalInformation_ release];
            foCardTypeList_ = nil;
            foCardTypeList_ = [[FOCardTypeList alloc] init];
            foCardTypeList_.openingTag = lname;
            [foCardTypeList_ setParentParseableObject:self];
            [parser setDelegate:foCardTypeList_];
            [foCardTypeList_ parserDidStartDocument:parser];
            
        }  else if ([lname isEqualToString: @"lugares"]) {
            
            xmlAnalysisCurrentValue_ = foobpsorxeas_AnalyzingFOPlaceList;
			//[foPlaceList_ release];
            foPlaceList_ = nil;
            foPlaceList_ = [[FOPlaceList alloc] init];
            foPlaceList_.openingTag = lname;
            [foPlaceList_ setParentParseableObject:self];
            [parser setDelegate:foPlaceList_];
            [foPlaceList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString: @"bancosdestino"]) {
            
            xmlAnalysisCurrentValue_ = foobpsorxeas_AnalyzingFODestinationBankList;
			//[foDestinationBankList_ release];
            foDestinationBankList_ = nil;
            foDestinationBankList_ = [[FODestinationBankList alloc] init];
            foDestinationBankList_.openingTag = lname;
            [foPlaceList_ setParentParseableObject:self];
            [parser setDelegate:foPlaceList_];
            [foPlaceList_ parserDidStartDocument:parser];
            
        }   else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        if ([lname isEqualToString: @"msg-s"]) {
            
            informationUpdated_ = soie_InfoAvailable;
            [parser setDelegate:parentParseableObject_];
            parentParseableObject_ = nil;
            
        }
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
    }
    
}



@end

#pragma mark -

@implementation FOPOtherBankPaymentStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPOtherBankPaymentStepOneResponseData {
    
    //[channelList_ release];
    channelList_ = nil;
    
    //[accountList_ release];
    accountList_ = nil;
    
    //[foCardTypeList_ release];
    foCardTypeList_ = nil;
    
    //[foPlaceList_ release];
    foPlaceList_ = nil;
    
    //[foDestinationBankList_ release];
    foDestinationBankList_ = nil;
}

@end


