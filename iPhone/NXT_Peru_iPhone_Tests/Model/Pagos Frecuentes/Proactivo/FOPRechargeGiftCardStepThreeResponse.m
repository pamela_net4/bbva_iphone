//
//  FORechargeGiftCardStepThreeResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPRechargeGiftCardStepThreeResponse.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    
    foprgcsthrxeas_AnalyzingOperation = foirxeas_ElementsCount, //!<Analyzing the channels list
    foprgcsthrxeas_AnalyzingAcChargeCard, //!<Analyzing the places
    foprgcsthrxeas_AnalyzingChargeCurrency,
    foprgcsthrxeas_AnalyzingChargeType,
    foprgcsthrxeas_AnalyzingGiftCard,
    foprgcsthrxeas_AnalyzingAmount,
    foprgcsthrxeas_AnalyzingCurrency,
    foprgcsthrxeas_AnalyzingDateHour,
    foprgcsthrxeas_AnalyzingInfoMessage
    
    
    
} FOPRechargeGiftCardStepThreeResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * RetentionListResponse private category
 */
@interface FOPRechargeGiftCardStepThreeResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPRechargeGiftCardStepThreeResponseData;

@end

#pragma mark -

@implementation FOPRechargeGiftCardStepThreeResponse

@synthesize operation=  operation_;
@synthesize acChargeCard = acChargeCard_;
@synthesize chargeCurrency = chargeCurrency_;
@synthesize chargeType = chargeType_;
@synthesize giftCard = giftCard_;
@synthesize amount= amount_;
@synthesize currency= currency_;
@synthesize dateHour=dateHour_;
@synthesize informativemessage=informativemessage_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPRechargeGiftCardStepThreeResponseData];
    
    [super dealloc];
    
}


#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPThirdAccountTransferStepThreeResponse instance providing the associated notification
 *
 * @return The initialized FOPThirdAccountTransferStepThreeResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOPRechargeGiftCardStepThreeResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPRechargeGiftCardStepThreeResponseData];
    
}
/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"operacion"]) {
            
            xmlAnalysisCurrentValue_ = foprgcsthrxeas_AnalyzingOperation;
            
            
        }  else if ([lname isEqualToString:@"ctatarjetacargo"])
        {
            xmlAnalysisCurrentValue_ = foprgcsthrxeas_AnalyzingAcChargeCard;
            
        } else if ([lname isEqualToString:@"monedacargo"])
        {
            xmlAnalysisCurrentValue_ = foprgcsthrxeas_AnalyzingChargeCurrency;
            
        } else if ([lname isEqualToString:@"tipocargo"])
        {
            xmlAnalysisCurrentValue_ = foprgcsthrxeas_AnalyzingChargeType;
            
        }
        else if ([lname isEqualToString:@"moneda"])
        {
            xmlAnalysisCurrentValue_ = foprgcsthrxeas_AnalyzingCurrency;
            
        } else if ([lname isEqualToString:@"monto"])
        {
            xmlAnalysisCurrentValue_ = foprgcsthrxeas_AnalyzingAmount;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == foprgcsthrxeas_AnalyzingOperation ) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == foprgcsthrxeas_AnalyzingAcChargeCard) {
            
            [acChargeCard_ release];
            acChargeCard_ = nil;
            acChargeCard_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foprgcsthrxeas_AnalyzingChargeCurrency) {
            
            [chargeCurrency_ release];
            chargeCurrency_ = nil;
            chargeCurrency_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foprgcsthrxeas_AnalyzingChargeType) {
            
            [chargeType_ release];
            chargeType_ = nil;
            chargeType_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foprgcsthrxeas_AnalyzingGiftCard) {
            
            [giftCard_ release];
            giftCard_ = nil;
            giftCard_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foprgcsthrxeas_AnalyzingAmount) {
            
            [amount_ release];
            amount_ = nil;
            amount_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foprgcsthrxeas_AnalyzingCurrency) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foprgcsthrxeas_AnalyzingDateHour) {
            
            [dateHour_ release];
            dateHour_ = nil;
            dateHour_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foprgcsthrxeas_AnalyzingInfoMessage) {
            
            [informativemessage_ release];
            informativemessage_ = nil;
            informativemessage_ = [elementString  copyWithZone:self.zone];
            
        }
        
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}



@end

#pragma mark -

@implementation FOPRechargeGiftCardStepThreeResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPRechargeGiftCardStepThreeResponseData {
    
    [operation_ release];
    operation_ = nil;
    
    [acChargeCard_ release];
    [chargeCurrency_ release];
    [chargeType_ release];
    
    [giftCard_ release];
    [amount_ release];
    [currency_ release];
}



@end
