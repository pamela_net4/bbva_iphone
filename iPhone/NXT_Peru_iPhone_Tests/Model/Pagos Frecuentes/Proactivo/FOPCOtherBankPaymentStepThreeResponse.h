//
//  FOOtherBankPaymentStepTwoResponse.h
//  NXT_Peru_iPad
//
//  Created by Jose Luis on 23/01/14.
//
//

#import "FOInitialResponse.h"


@interface FOPCOtherBankPaymentStepThreeResponse : FOInitialResponse
{
    @private
    NSString *operation_;
    NSString *cardNumber_;
    NSString *className_;
    NSString *destinationBank_;
    NSString *emisionPlace_;
    NSString *beneficiaryName_;
    NSString *dateHour_;
    NSString *informativeMessage_;

}

@property(nonatomic,readonly,retain)  NSString *operation;
@property(nonatomic,readonly,retain)  NSString *cardNumber;
@property(nonatomic,readonly,retain)  NSString *className;
@property(nonatomic,readonly,retain)  NSString *destinationBank;
@property(nonatomic,readonly,retain)  NSString *emisionPlace;
@property(nonatomic,readonly,retain)  NSString *beneficiaryName;
@property(nonatomic,readonly,retain)  NSString *dateHour;
@property(nonatomic,readonly,retain)  NSString *informativeMessage;

@end
