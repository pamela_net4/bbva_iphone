//
//  FOPOtherBankTransferStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOPOtherBankTransferStepThreeResponse : FOInitialResponse
{
@private
    /**
     * operation
     */
    NSString *operation_;
    /**
     * account Payment
     */
    NSString *accountPayment_;
    /**
     * destination bank
     */
    NSString *destinationBank_;
    /**
     * beneficiary Name
     */
    NSString *beneficiaryName_;

    /**
     * description document type
     */
    NSString *documentType_;
    /**
     * document number
     */
    NSString *documentNumber_;
    /**
     * date ad hour
     */
    NSString *dateHour_;
    /**
     * informative message
     */
    NSString *informativemessage_;
    
    
}
/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, retain) NSString *operation;
/**
 * Provides read-only access to the account Payment
 */
@property (nonatomic, readonly, retain) NSString *accountPayment;
/**
 * Provides read-only access to the beneficiary name
 */
@property (nonatomic, readonly, retain) NSString *beneficiaryName;
/**
 * Provides read-only access to the destination bank
 */
@property (nonatomic, readonly, retain) NSString *destinationBank;
/**
 * Provides read-only access to the docuent type
 */
@property (nonatomic, readonly, retain) NSString *documentType;
/**
 * Provides read-only access to the document number
 */
@property (nonatomic, readonly, retain) NSString *documentNumber;
/**
 * Provides read-only access to the date hour
 */
@property (nonatomic, readonly, retain) NSString *dateHour;
/**
 * Provides read-only access to the informative message
 */
@property (nonatomic, readonly, retain) NSString *informativemessage;

@end
