//
//  FOThirdCardPayStepTwoResponse.h
//  NXT_Peru_iPad
//
//  Created by usuario on 23/01/14.
//
//

#import "StatusEnabledResponse.h"
#import "FOInitialResponse.h"


@interface FOPThirdCardPaymentStepTwoResponse : FOInitialResponse
{
@private
    /**
     * operation
     */
    NSString *operation_;
    /**
     * charge account
     */
    NSString *chargeAccount_;
    /**
     * card Number
     */
    NSString *cardNumber_;
    /**
     * name Holder
     */
    NSString *nameHolder_;

    
}

@property (nonatomic, readonly, retain) NSString *operation;
/**
 * Provides read-only access to the charge account
 */
@property (nonatomic, readonly, retain) NSString *chargeAccount;
/**
 * Provides read-only access to the card number
 */
@property (nonatomic, readonly, retain) NSString *cardNumber;
/**
 * Provides read-only access to the name Holder
 */
@property (nonatomic, readonly, retain) NSString *nameHolder;



@end
