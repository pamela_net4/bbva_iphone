//
//  FOPRechargeCellphoneStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOPRechargeCellphoneStepTwoResponse : FOInitialResponse
{
    @private
    NSString *businessName_;
    NSString *telephone_;
    NSString *chargeAccount_;
    NSString *chargeCurrency_;
    NSString *amount_;

}

@property (nonatomic, readonly, retain) NSString *businessName;

@property (nonatomic, readonly, retain) NSString *telephone;

@property (nonatomic, readonly, retain) NSString *chargeAccount;

@property (nonatomic, readonly, retain) NSString *chargeCurrency;

@property (nonatomic, readonly, retain) NSString *amount;




@end
