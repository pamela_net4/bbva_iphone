//
//  FOPOtherBankTransferStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPOtherBankTransferStepTwoResponse.h"
/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fopobtstwrxeas_AnalyzingOperation =  foirxeas_ElementsCount,
    fopobtstwrxeas_AnalyzingAccountPayment,
    fopobtstwrxeas_AnalyzingDestinationBank,
    fopobtstwrxeas_AnalyzingBeneficiaryName,
    fopobtstwrxeas_AnalyzingDocumentType,
    fopobtstwrxeas_AnalyzingDocumentNumber
    
    
} FOPOtherBankTransferStepTwoResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPOtherBankTransferStepTwoResponse private category
 */
@interface FOPOtherBankTransferStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPOtherBankTransferStepTwoResponseData;

@end


#pragma mark -




@implementation FOPOtherBankTransferStepTwoResponse



@synthesize operation = operation_;
@synthesize destinationBank = destinationBank_;
@synthesize beneficiaryName = beneficiaryName_;
@synthesize accountPayment = accountPayment_;
@synthesize documentNumber = documentNumber_;
@synthesize documentType = documentType_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPOtherBankTransferStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPOtherBankTransferStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOPOtherBankTransferStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOPOtherBankTransferStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPOtherBankTransferStepTwoResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"operacion"]) {
            
            xmlAnalysisCurrentValue_ = fopobtstwrxeas_AnalyzingOperation;
            
            
        }  else if ([lname isEqualToString:@"ctabancodestino"])
        {
            xmlAnalysisCurrentValue_ = fopobtstwrxeas_AnalyzingDestinationBank;
            
        } else if ([lname isEqualToString:@"cuentaabono"])
        {
            xmlAnalysisCurrentValue_ = fopobtstwrxeas_AnalyzingAccountPayment;
            
        } else if ([lname isEqualToString:@"nombrebeneficiario"])
        {
            xmlAnalysisCurrentValue_ = fopobtstwrxeas_AnalyzingBeneficiaryName;
            
        }
        else if ([lname isEqualToString:@"nrodoc"])
        {
            xmlAnalysisCurrentValue_ = fopobtstwrxeas_AnalyzingDocumentNumber;
            
        } else if ([lname isEqualToString:@"desctipodoc"])
        {
            xmlAnalysisCurrentValue_ = fopobtstwrxeas_AnalyzingDocumentType;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}




/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fopobtstwrxeas_AnalyzingOperation ) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == fopobtstwrxeas_AnalyzingDestinationBank) {
            
            [destinationBank_ release];
            destinationBank_ = nil;
            destinationBank_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopobtstwrxeas_AnalyzingBeneficiaryName) {
            
            [beneficiaryName_ release];
            beneficiaryName_ = nil;
            beneficiaryName_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopobtstwrxeas_AnalyzingAccountPayment) {
            
            [accountPayment_ release];
            accountPayment_ = nil;
            accountPayment_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopobtstwrxeas_AnalyzingDocumentNumber) {
            
            [documentNumber_ release];
            documentNumber_ = nil;
            documentNumber_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopobtstwrxeas_AnalyzingDocumentType) {
            
            [documentType_ release];
            documentType_ = nil;
            documentType_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOPOtherBankTransferStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPOtherBankTransferStepTwoResponseData {
    
    [operation_ release];
    operation_ = nil;
    [destinationBank_ release];
    destinationBank_ = nil;
    [beneficiaryName_ release];
    beneficiaryName_ = nil;
    [accountPayment_ release];
    accountPayment_ = nil;
    [documentNumber_ release];
    documentNumber_ = nil;
    [documentType_ release];
    documentType_ = nil;
    
    
}



@end
