//
//  FOGeneralMenuStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@class FrequentOperationList;

@interface FOPGeneralMenuStepOneResponse : StatusEnabledResponse
{
    @private
    /*
        Frequent Operation List
     */
    FrequentOperationList *frequentOperationList_;
    /*
     more data
     */
    NSString *moreData_;
    /*
     next page
     */
    NSString *nextPage_;
    /*
     back page
     */
    NSString *backPage_;
}
/**
  * Provides read-only access to the Frequent Operation List
  */
@property (nonatomic, readonly, retain) FrequentOperationList *frequentOperationList;
/**
 * Provides read-only access to the more date
 */
@property (nonatomic, readonly, retain) NSString *moreData;
/**
 * Provides read-only access to the next page
 */
@property (nonatomic, readonly, retain) NSString *nextPage;
/**
 * Provides read-only access to the back page
 */
@property (nonatomic, readonly, retain) NSString *backPage;

@property (nonatomic, readonly, assign) BOOL hasMoreData;

@end
