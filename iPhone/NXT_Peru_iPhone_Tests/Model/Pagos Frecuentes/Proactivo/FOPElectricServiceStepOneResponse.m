//
//  FOPElectricServiceStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPElectricServiceStepOneResponse.h"
#import "FOAccountList.h"
#import "ChannelList.h"


/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fopessorxeas_AnalyzingAccountList = serxeas_ElementsCount, //!<Analyzing the account list
    fopessorxeas_AnalyzingChannelList, //!<Analyzing the channel list
    fopessorxeas_AnalyzingCompany
    
} FOPElectricServiceStepOneResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPElectricServiceStepOneResponse private category
 */
@interface FOPElectricServiceStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPElectricServiceStepOneResponseData;

@end


#pragma mark -




@implementation FOPElectricServiceStepOneResponse
#pragma mark -
#pragma mark Properties

@synthesize accountList = accountList_;
@synthesize channelList = channelList_;
@synthesize company = company_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPElectricServiceStepOneResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPElectricServiceStepOneResponse instance providing the associated notification
 *
 * @return The initialized FOPElectricServiceStepOneResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOPElectricServiceStepOneResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPElectricServiceStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"canales"]) {
            
            xmlAnalysisCurrentValue_ = fopessorxeas_AnalyzingChannelList;
			[channelList_ release];
            channelList_ = nil;
            channelList_ = [[ChannelList alloc] init];
            channelList_.openingTag = lname;
            [channelList_ setParentParseableObject:self];
            [parser setDelegate:channelList_];
            [channelList_ parserDidStartDocument:parser];
            
            
        } else if ([lname isEqualToString: @"cuentas"]) {
            
            xmlAnalysisCurrentValue_ = fopessorxeas_AnalyzingChannelList;
			[accountList_ release];
            accountList_ = nil;
            accountList_ = [[FOAccountList alloc] init];
            accountList_.openingTag = lname;
            [accountList_ setParentParseableObject:self];
            [parser setDelegate:accountList_];
            [accountList_ parserDidStartDocument:parser];
            
        }else if ([lname isEqualToString: @"empresa"]) {
            
            xmlAnalysisCurrentValue_ = fopessorxeas_AnalyzingCompany ;
            
        }  else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}


/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fopessorxeas_AnalyzingCompany ) {
            
            [company_ release];
            company_ = nil;
            company_ = [elementString copyWithZone:self.zone];
            
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end


#pragma mark -

@implementation FOPElectricServiceStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPElectricServiceStepOneResponseData {
    
    [accountList_ release];
    accountList_ = nil;
    
    [channelList_ release];
    channelList_ = nil;
    
    [company_ release];
    company_ = nil;
    
}




@end
