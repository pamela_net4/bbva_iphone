//
//  FOPInstitutionPaymentStepTwoResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOPInstitutionPaymentStepTwoResponse : FOInitialResponse
{
@private
    /**
     * operation
     */
    NSString *operation_;
    /**
     * agreement
     */
    NSString *agreement_;
    /**
     * access code
     */
    NSString *accessCode_;
    /**
     * operation
     */
    NSString *description_;

    
}
/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, retain) NSString *operation;
/**
 * Provides read-only access to the agreement
 */
@property (nonatomic, readonly, retain) NSString *agreement;
/**
 * Provides read-only access to the access code
 */
@property (nonatomic, readonly, retain) NSString *accessCode;
/**
 * Provides read-only access to the description
 */
@property (nonatomic, readonly, retain) NSString *description;



@end
