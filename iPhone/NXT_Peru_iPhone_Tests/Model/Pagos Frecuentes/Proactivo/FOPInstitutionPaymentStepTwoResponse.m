//
//  FOPInstitutionPaymentStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPInstitutionPaymentStepTwoResponse.h"
/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fopipstwrxeas_AnalyzingOperation =  serxeas_ElementsCount,
    fopipstwrxeas_AnalyzingAgreement,
    fopipstwrxeas_AnalyzingAccessCode,
    fopipstwrxeas_AnalyzingDescription
    
    
} FOPInstitutionPaymentStepTwoResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPInstitutionPaymentStepTwoResponse private category
 */
@interface FOPInstitutionPaymentStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPInstitutionPaymentStepTwoResponseData;

@end


#pragma mark -



@implementation FOPInstitutionPaymentStepTwoResponse



@synthesize operation = operation_;
@synthesize accessCode = accessCode_;
@synthesize description = description_;
@synthesize agreement = agreement_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPInstitutionPaymentStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPInstitutionPaymentStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOPInstitutionPaymentStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOPInstitutionPaymentStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPInstitutionPaymentStepTwoResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"operacion"]) {
            
            xmlAnalysisCurrentValue_ = fopipstwrxeas_AnalyzingOperation;
            
            
        }  else if ([lname isEqualToString:@"codigoacceso"])
        {
            xmlAnalysisCurrentValue_ = fopipstwrxeas_AnalyzingAccessCode;
            
        } else if ([lname isEqualToString:@"convenio"])
        {
            xmlAnalysisCurrentValue_ = fopipstwrxeas_AnalyzingAgreement;
            
        } else if ([lname isEqualToString:@"descripcion"])
        {
            xmlAnalysisCurrentValue_ = fopipstwrxeas_AnalyzingDescription;
            
        }
        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}




/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fopipstwrxeas_AnalyzingOperation ) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == fopipstwrxeas_AnalyzingAccessCode) {
            
            [accessCode_ release];
            accessCode_ = nil;
            accessCode_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopipstwrxeas_AnalyzingDescription) {
            
            [description_ release];
            description_ = nil;
            description_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopipstwrxeas_AnalyzingAgreement) {
            
            [agreement_ release];
            agreement_ = nil;
            agreement_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOPInstitutionPaymentStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPInstitutionPaymentStepTwoResponseData {
    
    [operation_ release];
    operation_ = nil;
    [accessCode_ release];
    accessCode_ = nil;
    [description_ release];
    description_ = nil;
    [agreement_ release];
    agreement_ = nil;
    
    
}



@end
