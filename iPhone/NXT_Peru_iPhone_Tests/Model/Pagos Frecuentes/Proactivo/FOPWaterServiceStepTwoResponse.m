//
//  FOPWaterServiceStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPWaterServiceStepTwoResponse.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fopwsstwrxeas_AnalyzingCompany =  foirxeas_ElementsCount,
    fopwsstwrxeas_AnalyzingAccountPayment,
    fopwsstwrxeas_AnalyzingCurrencyPayment,
    fopwsstwrxeas_AnalyzingSupply,
    fopwsstwrxeas_AnalyzingInformativeMessage
    
    
} FOPWaterServiceStepTwoResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPWaterServiceStepTwoResponse private category
 */
@interface FOPWaterServiceStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPWaterServiceStepTwoResponseData;

@end


#pragma mark -



@implementation FOPWaterServiceStepTwoResponse



@synthesize company = company_;
@synthesize currencyPayment = currencyPayment_;
@synthesize supply = supply_;
@synthesize accountPayment = accountPayment_;
@synthesize informativemessage = informativemessage_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPWaterServiceStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPWaterServiceStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOPWaterServiceStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOPWaterServiceStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPWaterServiceStepTwoResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"empresa"]) {
            
            xmlAnalysisCurrentValue_ = fopwsstwrxeas_AnalyzingCompany;
            
            
        }  else if ([lname isEqualToString:@"cuentacargo"])
        {
            xmlAnalysisCurrentValue_ = fopwsstwrxeas_AnalyzingCurrencyPayment;
            
        } else if ([lname isEqualToString:@"monedacargo"])
        {
            xmlAnalysisCurrentValue_ = fopwsstwrxeas_AnalyzingAccountPayment;
            
        } else if ([lname isEqualToString:@"suministro"])
        {
            xmlAnalysisCurrentValue_ = fopwsstwrxeas_AnalyzingSupply;
            
        }else if ([lname isEqualToString:@"mensajeinformativo"])
        {
            xmlAnalysisCurrentValue_ = fopwsstwrxeas_AnalyzingInformativeMessage;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}




/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fopwsstwrxeas_AnalyzingCompany ) {
            
            [company_ release];
            company_ = nil;
            company_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == fopwsstwrxeas_AnalyzingCurrencyPayment) {
            
            [currencyPayment_ release];
            currencyPayment_ = nil;
            currencyPayment_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopwsstwrxeas_AnalyzingSupply) {
            
            [supply_ release];
            supply_ = nil;
            supply_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopwsstwrxeas_AnalyzingAccountPayment) {
            
            [accountPayment_ release];
            accountPayment_ = nil;
            accountPayment_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopwsstwrxeas_AnalyzingInformativeMessage) {
            
            [informativemessage_ release];
            informativemessage_ = nil;
            informativemessage_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOPWaterServiceStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPWaterServiceStepTwoResponseData {
    
    [company_ release];
    company_ = nil;
    [currencyPayment_ release];
    currencyPayment_ = nil;
    [supply_ release];
    supply_ = nil;
    [accountPayment_ release];
    accountPayment_ = nil;
    [informativemessage_ release];
    informativemessage_ = nil;
    
    
}


@end
