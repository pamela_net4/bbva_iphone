//
//  OFThirdCardPayStepOneResponse.h
//  NXT_Peru_iPad
//
//  Created by usuario on 23/01/14.
//
//

#import "StatusEnabledResponse.h"
@class FOAccountList;
@class ChannelList;

@interface FOPThirdCardPaymentGetAccountAndChannelResponse : StatusEnabledResponse
{
@private
    /**
     * Account list
     */
    FOAccountList *accountList_;
    
    /**
     * Channel list
     */
    ChannelList *channelList_;
    
}

/**
 * Provides read-only access to the Account list
 */
@property (nonatomic, readonly, retain) FOAccountList *accountList;

/**
 * Provides read-only access to the Channel list
 */
@property (nonatomic, readonly, retain) ChannelList *channelList;

@end
