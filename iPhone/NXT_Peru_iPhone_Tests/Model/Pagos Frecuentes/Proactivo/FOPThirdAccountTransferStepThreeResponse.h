//
//  FOPThirdAccountTransferStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOPThirdAccountTransferStepThreeResponse : FOInitialResponse
{
@private
    /**
     * operation
     */
    NSString *operation_;
    /**
     * charge account
     */
    NSString *chargeAccount_;
    /**
     * beneficiary Name
     */
    NSString *beneficiaryName_;
    /**
     * account Payment
     */
    NSString *accountPayment_;
    /**
     * currency
     */
    NSString *currency_;
    /**
     * amount
     */
    NSString *amount_;
    /**
     * date ad hour
     */
    NSString *dateHour_;
    /**
     * informative message
     */
    NSString *informativemessage_;
    
    
}
/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, retain) NSString *operation;
/**
 * Provides read-only access to the charge account
 */
@property (nonatomic, readonly, retain) NSString *chargeAccount;
/**
 * Provides read-only access to the beneficiary name
 */
@property (nonatomic, readonly, retain) NSString *beneficiaryName;
/**
 * Provides read-only access to the account payment
 */
@property (nonatomic, readonly, retain) NSString *accountPayment;
/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, retain) NSString *currency;
/**
 * Provides read-only access to the amount
 */
@property (nonatomic, readonly, retain) NSString *amount;
/**
 * Provides read-only access to the date hour
 */
@property (nonatomic, readonly, retain) NSString *dateHour;
/**
 * Provides read-only access to the informative message
 */
@property (nonatomic, readonly, retain) NSString *informativemessage;



@end
