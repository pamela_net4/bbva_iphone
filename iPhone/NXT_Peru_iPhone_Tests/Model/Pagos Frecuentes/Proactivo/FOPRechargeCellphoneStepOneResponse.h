//
//  FOPRechargeCellphoneStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@class AlterCarrierList;
@class FOAccountList;
@class ChannelList;

@interface FOPRechargeCellphoneStepOneResponse : StatusEnabledResponse
{
    @private
    AlterCarrierList *carrierList_;
    NSString *informativeText_;
    FOAccountList *accountList_;
    ChannelList *channelList_;

}
@property (nonatomic, readonly, retain) AlterCarrierList *carrierList;
@property (nonatomic, readonly, retain) NSString *informativeText;
@property (nonatomic, readonly, retain) FOAccountList *accountList;
@property (nonatomic, readonly, retain) ChannelList *channelList;



@end
