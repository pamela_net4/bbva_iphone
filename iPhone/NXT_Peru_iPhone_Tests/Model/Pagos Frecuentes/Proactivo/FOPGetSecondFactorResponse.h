//
//  FOPGetSecondFactorResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@interface FOPGetSecondFactorResponse : StatusEnabledResponse
{
    @private
    NSString *coordinate_;
    NSString *securitySeal_;
}
/**
 * Provides read-only access to the frequent Operation array
 */
@property(nonatomic,readonly,retain) NSString *coordinate;
/**
 * Provides read-only access to the frequent Operation array
 */
@property(nonatomic,readonly,retain) NSString *securitySeal;

@end
