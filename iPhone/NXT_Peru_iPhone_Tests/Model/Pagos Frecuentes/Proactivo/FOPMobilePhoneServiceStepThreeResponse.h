//
//  FOPMobilePhoneServiceStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInitialResponse.h"

@interface FOPMobilePhoneServiceStepThreeResponse : FOInitialResponse
{
@private
    NSString *operation_;
    NSString *chargeAccount_;
    NSString *paymentAccount_;
    NSString *informativeMessage_;
}
/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, retain) NSString *operation;

/**
 * Provides read-only access to the charge account
 */
@property (nonatomic, readonly, retain) NSString *chargeAccount;

/**
 * Provides read-only access to the payment account
 */
@property (nonatomic, readonly, retain) NSString *paymentAccount;

/**
 * Provides read-only access to the informative message
 */
@property (nonatomic, readonly, retain) NSString *informativeMessage;

@end

