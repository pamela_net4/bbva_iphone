//
//  OFOtherBankPaymentStepOneResponse.h
//  NXT_Peru_iPad
//
//  Created by Jose Luis on 23/01/14.
//
//

#import "StatusEnabledResponse.h"

@class ChannelList;
@class FOAccountList;
@class FOCardTypeList;
@class FOPlaceList;
@class FODestinationBankList;

@interface FOPOtherBankPaymentStepOneResponse : StatusEnabledResponse
{
    @private
    ChannelList *channelList_;
    FOAccountList *accountList_;
    FOCardTypeList *foCardTypeList_;
    FOPlaceList *foPlaceList_;
    FODestinationBankList *foDestinationBankList_;
    
}

@property(nonatomic,readonly,retain) ChannelList *channelList;

@property(nonatomic,readonly,retain) FOAccountList *accountList;

@property(nonatomic,readonly,retain) FOCardTypeList *foCardTypeList;

@property(nonatomic,readonly,retain) FOPlaceList *foPlaceList;

@property(nonatomic,readonly,retain) FODestinationBankList *foDestinationBankList;


@end
