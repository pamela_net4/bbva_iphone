//
//  FOPDirectTVInstallationStepThreeResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPDirectTVInstallationStepThreeResponse.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fopdtisthrxeas_AnalyzingInstitution = foirxeas_ElementsCount,
    fopdtisthrxeas_AnalyzingSds,
    fopdtisthrxeas_AnalyzingChargeAccount,
    fopdtisthrxeas_AnalyzingInfoMessage
    
    
} FOPDirectTVInstallationStepThreeResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPDirectTVInstallationStepThreeResponse private category
 */
@interface FOPDirectTVInstallationStepThreeResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPDirectTVInstallationStepThreeResponseData;

@end


#pragma mark -


@implementation FOPDirectTVInstallationStepThreeResponse

#pragma mark -
#pragma mark Properties

@synthesize institution = institution_;
@synthesize sds = sds_;
@synthesize chargeAccount= chargeAccount_;


#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearFOPDirectTVInstallationStepThreeResponseData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPDirectTVInstallationStepThreeResponse instance providing the associated notification
 *
 * @return The initialized FOPDirectTVInstallationStepThreeResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationFOPDirectTVInstallationStepThreeResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPDirectTVInstallationStepThreeResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if([lname isEqualToString:@"institucion"])
        {
            xmlAnalysisCurrentValue_ = fopdtisthrxeas_AnalyzingInstitution;
        }
        else if([lname isEqualToString:@"sds"])
        {
            xmlAnalysisCurrentValue_ = fopdtisthrxeas_AnalyzingSds;
        }
        else if ([lname isEqualToString: @"cuentacargo"])
        {
            xmlAnalysisCurrentValue_ =fopdtisthrxeas_AnalyzingChargeAccount;
            
        } else if ([lname isEqualToString: @"mensajeinformativo"])
        {
            xmlAnalysisCurrentValue_ = fopdtisthrxeas_AnalyzingInfoMessage;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(xmlAnalysisCurrentValue_ == fopdtisthrxeas_AnalyzingInstitution)
        {
            [institution_ release];
            institution_ = nil;
            institution_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == fopdtisthrxeas_AnalyzingSds)
        {
            [sds_ release];
            sds_ = nil;
            sds_ = [elementString copyWithZone:self.zone];
            
        }
        else if(xmlAnalysisCurrentValue_ == fopdtisthrxeas_AnalyzingChargeAccount)
        {
            [chargeAccount_ release];
            chargeAccount_ = nil;
            chargeAccount_ = [elementString copyWithZone:self.zone];
            
        }
        else if(xmlAnalysisCurrentValue_ == fopdtisthrxeas_AnalyzingInfoMessage)
        {
            [informativeMessage_ release];
            informativeMessage_ = nil;
            informativeMessage_ = [elementString copyWithZone:self.zone];
            
        }
    }else
    {
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
}


@end


#pragma mark -

@implementation FOPDirectTVInstallationStepThreeResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPDirectTVInstallationStepThreeResponseData{
    
    [institution_ release];
    institution_ = nil;
    
    [sds_ release];
    sds_ = nil;
    
    [chargeAccount_ release];
    chargeAccount_ = nil;
    
    [dateHour_ release];
    dateHour_ = nil;
    
    [informativeMessage_ release];
    informativeMessage_ = nil;

}

@end

