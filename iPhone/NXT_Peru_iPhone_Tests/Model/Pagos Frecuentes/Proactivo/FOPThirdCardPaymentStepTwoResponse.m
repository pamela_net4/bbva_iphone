//
//  FOThirdCardPayStepTwoResponse.m
//  NXT_Peru_iPad
//
//  Created by usuario on 23/01/14.
//
//

#import "FOPThirdCardPaymentStepTwoResponse.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fotcpstwrxeas_AnalyzingOperation =  foirxeas_ElementsCount,
    fotcpstwrxeas_AnalyzingChargeAccount,
    fotcpstwrxeas_AnalyzingCardNumber,
    fotcpstwrxeas_AnalyzingNameHolder
    
    
} FOThirdCardPaymentStepTwoResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOThirdCardPayStepTwoResponse private category
 */
@interface FOPThirdCardPaymentStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPThirdCardPaymentStepTwoResponseData;

@end


#pragma mark -


@implementation FOPThirdCardPaymentStepTwoResponse

@synthesize operation = operation_;
@synthesize chargeAccount = chargeAccount_;
@synthesize cardNumber = cardNumber_;
@synthesize nameHolder = nameHolder_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPThirdCardPaymentStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOThirdCardPayStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOThirdCardPayStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOPThirdCardPaymentStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPThirdCardPaymentStepTwoResponseData];
    
}




/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"operacion"]) {
            
            xmlAnalysisCurrentValue_ = fotcpstwrxeas_AnalyzingOperation;
            
            
        }  else if ([lname isEqualToString:@"cuentacargo"])
        {
            xmlAnalysisCurrentValue_ = fotcpstwrxeas_AnalyzingChargeAccount;
            
        } else if ([lname isEqualToString:@"numerotarjeta"])
        {
            xmlAnalysisCurrentValue_ = fotcpstwrxeas_AnalyzingCardNumber;
            
        } else if ([lname isEqualToString:@"nombretitular"])
        {
            xmlAnalysisCurrentValue_ = fotcpstwrxeas_AnalyzingNameHolder;
            
        }
       else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}


/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fotcpstwrxeas_AnalyzingOperation ) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == fotcpstwrxeas_AnalyzingChargeAccount) {
            
            [chargeAccount_ release];
            chargeAccount_ = nil;
            chargeAccount_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fotcpstwrxeas_AnalyzingCardNumber) {
            
            [cardNumber_ release];
            cardNumber_ = nil;
            cardNumber_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fotcpstwrxeas_AnalyzingNameHolder) {
            
            [nameHolder_ release];
            nameHolder_ = nil;
            nameHolder_ = [elementString  copyWithZone:self.zone];
            
        }         
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOPThirdCardPaymentStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPThirdCardPaymentStepTwoResponseData {
    
    [operation_ release];
    operation_ = nil;
    [chargeAccount_ release];
    chargeAccount_ = nil;
    [cardNumber_ release];
    cardNumber_ = nil;
    [nameHolder_ release];
    nameHolder_ = nil;

    
}


@end
