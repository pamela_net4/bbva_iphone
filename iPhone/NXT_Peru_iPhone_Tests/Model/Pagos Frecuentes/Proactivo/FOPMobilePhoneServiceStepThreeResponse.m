//
//  FOPMobilePhoneServiceStepThreeResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPMobilePhoneServiceStepThreeResponse.h"

/**
 * Enumerates the analysis state
 */

typedef enum {
    
    fopmpssthrxeas_AnalyzingOperation = foirxeas_ElementsCount,
    fopmpssthrxeas_AnalyzingChargeAccount,
    fopmpssthrxeas_AnalyzingPaymentAccount,
    fopmpssthrxeas_AnalyzingInformativeMessage
    
} FOPMobilePhoneServiceStepThreeResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPMobilePhoneServiceStepThreeResponse private category
 */
@interface FOPMobilePhoneServiceStepThreeResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPMobilePhoneServiceStepThreeResponseData;

@end


#pragma mark -


@implementation FOPMobilePhoneServiceStepThreeResponse

#pragma mark -
#pragma mark Properties

@synthesize operation = operation_;
@synthesize chargeAccount = chargeAccount_;
@synthesize paymentAccount= paymentAccount_;
@synthesize informativeMessage = informativeMessage_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearFOPMobilePhoneServiceStepThreeResponseData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPMobilePhoneServiceStepThreeResponse instance providing the associated notification
 *
 * @return The initialized FOPMobilePhoneServiceStepThreeResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationFOPMobilePhoneServiceStepThreeResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPMobilePhoneServiceStepThreeResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if([lname isEqualToString:@"operacion"])
        {
            xmlAnalysisCurrentValue_ = fopmpssthrxeas_AnalyzingOperation;
        }
        else if ([lname isEqualToString: @"cuentacargo"])
        {
            xmlAnalysisCurrentValue_ =fopmpssthrxeas_AnalyzingChargeAccount;
        }
        else if([lname isEqualToString:@"cuentaabono"])
        {
            xmlAnalysisCurrentValue_ = fopmpssthrxeas_AnalyzingPaymentAccount;
        }
        else if([lname isEqualToString:@"mensajeinformativo"])
        {
            xmlAnalysisCurrentValue_ = fopmpssthrxeas_AnalyzingInformativeMessage;
        }
        else{
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(xmlAnalysisCurrentValue_ == fopmpssthrxeas_AnalyzingOperation)
        {
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == fopmpssthrxeas_AnalyzingPaymentAccount)
        {
            [paymentAccount_ release];
            paymentAccount_ = nil;
            paymentAccount_ = [elementString copyWithZone:self.zone];
            
        }
        else if(xmlAnalysisCurrentValue_ == fopmpssthrxeas_AnalyzingChargeAccount)
        {
            [chargeAccount_ release];
            chargeAccount_ = nil;
            chargeAccount_ = [elementString copyWithZone:self.zone];
            
        }
        else if(xmlAnalysisCurrentValue_ == fopmpssthrxeas_AnalyzingInformativeMessage)
        {
            [informativeMessage_ release];
            informativeMessage_ = nil;
            informativeMessage_ = [elementString copyWithZone:self.zone];
            
        }
        
    }else
    {
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
}


@end


#pragma mark -

@implementation FOPMobilePhoneServiceStepThreeResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPMobilePhoneServiceStepThreeResponseData{
    
    [operation_ release];
    operation_ = nil;
    
    [chargeAccount_ release];
    chargeAccount_ = nil;
    
}

@end