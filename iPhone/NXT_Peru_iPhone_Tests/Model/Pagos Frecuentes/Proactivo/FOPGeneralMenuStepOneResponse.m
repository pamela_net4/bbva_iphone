//
//  FOGeneralMenuStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPGeneralMenuStepOneResponse.h"
#import "FrequentOperationList.h"
/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fogmsorxeas_AnalyzingFrequentOperationList = serxeas_ElementsCount, //!<Analyzing the account list
    fogmsorxeas_AnalyzingMoreData,
    fogmsorxeas_AnalyzingNextPage,
    fogmsorxeas_AnalyzingBackPage
    
} FOGeneralMenuStepOneResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPInstitutionPaymentStepOneResponse private category
 */
@interface FOPGeneralMenuStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPGeneralMenuStepOneResponseData;

@end


#pragma mark -

@implementation FOPGeneralMenuStepOneResponse

#pragma mark -
#pragma mark Properties
@synthesize frequentOperationList = frequentOperationList_;
@synthesize moreData = moreData_;
@synthesize nextPage = nextPage_;
@synthesize backPage = backPage_;
@dynamic hasMoreData;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPGeneralMenuStepOneResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOGeneralMenuStepOneResponse instance providing the associated notification
 *
 * @return The initialized FOGeneralMenuStepOneResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kFOPGeneralMenuStepOneResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPGeneralMenuStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"operaciones"]) {
            
            xmlAnalysisCurrentValue_ = fogmsorxeas_AnalyzingFrequentOperationList;
			[frequentOperationList_ release];
            frequentOperationList_ = nil;
            frequentOperationList_ = [[FrequentOperationList alloc] init];
            frequentOperationList_.openingTag = lname;
            [frequentOperationList_ setParentParseableObject:self];
            [parser setDelegate:frequentOperationList_];
            [frequentOperationList_ parserDidStartDocument:parser];
            
            
        } else if ([lname isEqualToString: @"masdatos"]) {
            
            xmlAnalysisCurrentValue_ = fogmsorxeas_AnalyzingMoreData;
            
        } else if ([lname isEqualToString: @"nextpage"]) {
            
            xmlAnalysisCurrentValue_ = fogmsorxeas_AnalyzingNextPage;
            
        }else if ([lname isEqualToString: @"backpage"]) {
            
            xmlAnalysisCurrentValue_ = fogmsorxeas_AnalyzingBackPage;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}


/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    

    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fogmsorxeas_AnalyzingMoreData)
        {
            [moreData_ release];
            moreData_ = nil;
            moreData_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == fogmsorxeas_AnalyzingNextPage)
        {
            [nextPage_ release];
            nextPage_ = nil;
            nextPage_ = [elementString copyWithZone:self.zone];;
        }
        else if(xmlAnalysisCurrentValue_ == fogmsorxeas_AnalyzingBackPage)
        {
            [backPage_ release];
            backPage_ = nil;
            backPage_ = [elementString copyWithZone:self.zone];
        }
        else if ([elementString isEqualToString: @"msg-s"]) {
            
            informationUpdated_ = soie_InfoAvailable;
            [parser setDelegate:parentParseableObject_];
            parentParseableObject_ = nil;
            
        }
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
    }
    
}

#pragma mark -
#pragma mark properties implementation
- (BOOL)hasMoreData{
    
    BOOL result = FALSE;
    
    if ([@"s" isEqualToString:[moreData_ lowercaseString]] || [@"si" isEqualToString:[moreData_ lowercaseString]]) {
        result = TRUE;
    }
    
    
    return result;
}


@end
#pragma mark -

@implementation FOPGeneralMenuStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPGeneralMenuStepOneResponseData {
    
    [frequentOperationList_ release];
    frequentOperationList_ = nil;
    
    [nextPage_ release];
    nextPage_ = nil;
    
    [backPage_ release];
    backPage_ = nil;
    
    [moreData_ release];
    moreData_ = nil;
    
}


@end
