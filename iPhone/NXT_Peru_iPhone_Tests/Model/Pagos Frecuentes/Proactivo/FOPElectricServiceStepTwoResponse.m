//
//  FOPElectricServiceStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPElectricServiceStepTwoResponse.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    fopesstwrxeas_AnalyzingCompany =  foirxeas_ElementsCount,
    fopesstwrxeas_AnalyzingAccountPayment,
    fopesstwrxeas_AnalyzingCurrencyPayment,
    fopesstwrxeas_AnalyzingSupply
    
    
} FOPElectricServiceStepTwoResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPElectricServiceStepTwoResponse private category
 */
@interface FOPElectricServiceStepTwoResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOPElectricServiceStepTwoResponseData;

@end


#pragma mark -



@implementation FOPElectricServiceStepTwoResponse



@synthesize company = company_;
@synthesize currencyPayment = currencyPayment_;
@synthesize supply = supply_;
@synthesize accountPayment = accountPayment_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOPElectricServiceStepTwoResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOPElectricServiceStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOPElectricServiceStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost =  kNotificationFOPElectricServiceStepTwoResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOPElectricServiceStepTwoResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"empresa"]) {
            
            xmlAnalysisCurrentValue_ = fopesstwrxeas_AnalyzingCompany;
            
            
        }  else if ([lname isEqualToString:@"cuentacargo"])
        {
            xmlAnalysisCurrentValue_ = fopesstwrxeas_AnalyzingCurrencyPayment;
            
        } else if ([lname isEqualToString:@"monedacargo"])
        {
            xmlAnalysisCurrentValue_ = fopesstwrxeas_AnalyzingAccountPayment;
            
        } else if ([lname isEqualToString:@"suministro"])
        {
            xmlAnalysisCurrentValue_ = fopesstwrxeas_AnalyzingSupply;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}




/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == fopesstwrxeas_AnalyzingCompany ) {
            
            [company_ release];
            company_ = nil;
            company_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == fopesstwrxeas_AnalyzingCurrencyPayment) {
            
            [currencyPayment_ release];
            currencyPayment_ = nil;
            currencyPayment_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopesstwrxeas_AnalyzingSupply) {
            
            [supply_ release];
            supply_ = nil;
            supply_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == fopesstwrxeas_AnalyzingAccountPayment) {
            
            [accountPayment_ release];
            accountPayment_ = nil;
            accountPayment_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

@end


#pragma mark -

@implementation FOPElectricServiceStepTwoResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOPElectricServiceStepTwoResponseData {
    
    [company_ release];
    company_ = nil;
    [currencyPayment_ release];
    currencyPayment_ = nil;
    [supply_ release];
    supply_ = nil;
    [accountPayment_ release];
    accountPayment_ = nil;
    
    
}


@end
