//
//  FOCard.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@interface FOCard : StatusEnabledResponse
{
    @private
    NSString *cardNumber_;
    NSString *cardType_;
    NSString *currency_;
    NSString *param_;
}
@property (nonatomic, readonly, copy) NSString *cardNumber;
@property (nonatomic, readonly, copy) NSString *cardType;
@property (nonatomic, readonly, copy) NSString *currency;
@property (nonatomic, readonly, copy) NSString *param;
@end
