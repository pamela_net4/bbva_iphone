//
//  FrequentOperationList.m
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationList.h"
#import "FrequentOperation.h"


#pragma mark -
/**
 * FrequentOperationList private category
 */
@interface FrequentOperationList(private)
/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFrequentOperationListData;

@end

#pragma mark -

@implementation FrequentOperationList
#pragma mark -
#pragma mark Properties

@synthesize frequentOperationArray = frequentOperationArray_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearFrequentOperationListData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a TransferSuccessResponse instance providing the associated notification
 *
 * @return The initialized TransferDetailResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        frequentOperationArray_ = [[NSMutableArray alloc] init];
		informationUpdated_ = soie_NoInfo;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark Information management
/*
 * Remove the contained data
 */
- (void)removeData {
    
	[self clearFrequentOperationListData];
    
}

/*
 * Returns the AccountTransaction located at the given position, or nil if position is not valid
 */
- (FrequentOperation *)frequentOperationAtPosition:(NSUInteger)aPosition {
    
	FrequentOperation *result = nil;
	
	if (aPosition < [frequentOperationArray_ count]) {
        
		result = [frequentOperationArray_ objectAtIndex:aPosition];
        
	}
	
	return result;
    
}

/**
 * Provides read access to the number of Channels stored
 *
 * @return The count
 */
- (NSUInteger) foPlaceCount {
	return [frequentOperationArray_ count];
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    [frequentOperationArray_ release];
    frequentOperationArray_ = [[NSMutableArray alloc] init];
    
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (auxFrequentOperation_ != nil) {
        [frequentOperationArray_ addObject:auxFrequentOperation_];
        //[foCardTypeArray_ release];
       // foPlaceArray_ = nil;
    }
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
			//[auxFOCardType_ release];
            auxFrequentOperation_ = nil;
            auxFrequentOperation_ = [[FrequentOperation alloc] init];
            auxFrequentOperation_.openingTag = lname;
            [auxFrequentOperation_ setParentParseableObject:self];
            [parser setDelegate:auxFrequentOperation_];
            [auxFrequentOperation_ parserDidStartDocument:parser];
            
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    if (auxFrequentOperation_ != nil) {
        
        [frequentOperationArray_ addObject:auxFrequentOperation_];
		[auxFrequentOperation_ release];
		auxFrequentOperation_ = nil;
        
	}
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FrequentOperationList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFrequentOperationListData {
    
    [frequentOperationArray_ release];
    frequentOperationArray_ = nil;
    
    [auxFrequentOperation_ release];
    auxFrequentOperation_ = nil;
    
}
@end