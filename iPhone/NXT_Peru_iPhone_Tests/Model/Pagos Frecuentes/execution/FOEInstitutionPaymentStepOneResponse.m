//
//  FOEInstitutionPaymentStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/30/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEInstitutionPaymentStepOneResponse.h"
#import "FODataList.h"
/**
 * Enumerates the analysis state
 */
typedef enum {
    
    foeipsorxeas_AnalyzingDataList = foeirxeas_ElementsCount, //!<Analyzing the data list
    foeipsorxeas_AnalyzingAgreement, //!<Analyzing the agreement
    foeipsorxeas_AnalyzingAccessCode, //!<Analyzing the access code
    foeipsorxeas_AnalyzingMatriCode, //!<Analyzing the matri code
    foeipsorxeas_AnalyzingClass, //!<Analyzing the class
    foeipsorxeas_AnalyzingArray, //!<Analyzing the array
    foeipsorxeas_AnalyzingFieldLong, //!<Analyzing the FieldLong
    foeipsorxeas_AnalyzingArrayLong, //!<Analyzing the ArrayLong
    foeipsorxeas_AnalyzingBadge, //!<Analyzing the badge
    foeipsorxeas_AnalyzingEditableFieldArray, //!<Analyzing the editable fields identified by "0" as non-editable and "1" as editable
    foeipsorxeas_AnalyzingTitlesArray, //!<Analyzing the titles array
    foeipsorxeas_AnalyzingFO, //!<Analyzing the frequent operation
    foeipsorxeas_AnalyzingParam //!<Analyzing the parameter
    
} FOEInstitutionPaymentStepOneResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOEInstitutionPaymentStepOneResponse private category
 */
@interface FOEInstitutionPaymentStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOEInstitutionPaymentStepOneResponseData;

@end


#pragma mark -

@implementation FOEInstitutionPaymentStepOneResponse

#pragma mark - properties
@synthesize dataList = dataList_;
@synthesize titlesArray = titlesArray_;
@synthesize matriCode = matriCode_;
@synthesize array = array_;
@synthesize agreement = agreement_;
@synthesize accessCode = accessCode_;
@synthesize operationClass = operationClass_;
@synthesize fieldLong = fieldLong_;
@synthesize arrayLong = arrayLong_;
@synthesize editableArray = editableArray_;
@synthesize badge = badge_;
@synthesize frequentOperation = frequentOperation_;
@synthesize param = param_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOEInstitutionPaymentStepOneResponseData];
    
    [super dealloc];
    
}
#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOThirdCardPayStepTwoResponse instance providing the associated notification
 *
 * @return The initialized FOThirdCardPayStepTwoResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		//self.notificationToPost =  kNotificationFOInitialResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOEInstitutionPaymentStepOneResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        //TODO
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"datos"]) {
            
            xmlAnalysisCurrentValue_ = foeipsorxeas_AnalyzingDataList;
			[dataList_ release];
            dataList_ = nil;
            dataList_ = [[FODataList alloc] init];
            dataList_.openingTag = lname;
            [dataList_ setParentParseableObject:self];
            [parser setDelegate:dataList_];
            [dataList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString:@"convenio"]) {
            
            xmlAnalysisCurrentValue_ = foeipsorxeas_AnalyzingAgreement;
            
        } else if ([lname isEqualToString:@"codacceso"]) {
            
            xmlAnalysisCurrentValue_ = foeipsorxeas_AnalyzingAccessCode;
            
        } else if ([lname isEqualToString:@"codmatri"]) {
            
            xmlAnalysisCurrentValue_ = foeipsorxeas_AnalyzingMatriCode;
            
        } else if ([lname isEqualToString:@"clase"]) {
            
            xmlAnalysisCurrentValue_ = foeipsorxeas_AnalyzingClass;
            
        } else if ([lname isEqualToString:@"longcampos"]) {
            
            xmlAnalysisCurrentValue_ = foeipsorxeas_AnalyzingFieldLong;
            
        } else if ([lname isEqualToString:@"longarreglos"]) {
            
            xmlAnalysisCurrentValue_ = foeipsorxeas_AnalyzingArrayLong;
            
        } else if ([lname isEqualToString:@"arreglo"]) {
            
            xmlAnalysisCurrentValue_ = foeipsorxeas_AnalyzingArray;
            
        } else if ([lname isEqualToString:@"divisa"]) {
            
            xmlAnalysisCurrentValue_ = foeipsorxeas_AnalyzingBadge;
            
        } else if ([lname isEqualToString:@"arreglotitulos"]) {
            
            xmlAnalysisCurrentValue_ = foeipsorxeas_AnalyzingTitlesArray;
            
        }else if ([lname isEqualToString:@"campoeditable"]) {
            
            xmlAnalysisCurrentValue_ = foeipsorxeas_AnalyzingEditableFieldArray;
            
        } else if ([lname isEqualToString:@"pfrecuente"]) {
            
            xmlAnalysisCurrentValue_ = foeipsorxeas_AnalyzingFO;
            
        } else if ([lname isEqualToString:@"parametros"]) {
            
            xmlAnalysisCurrentValue_ = foeipsorxeas_AnalyzingParam;
            
        }
    
        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == foeipsorxeas_AnalyzingAgreement) {
            
            [agreement_ release];
            agreement_ = nil;
            agreement_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeipsorxeas_AnalyzingAccessCode) {
            
            [accessCode_ release];
            accessCode_ = nil;
            accessCode_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeipsorxeas_AnalyzingMatriCode) {
            
            [matriCode_ release];
            matriCode_ = nil;
            matriCode_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeipsorxeas_AnalyzingClass) {
            
            [operationClass_ release];
            operationClass_ = nil;
            operationClass_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeipsorxeas_AnalyzingArray) {
            
            [array_ release];
            array_ = nil;
            array_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeipsorxeas_AnalyzingFieldLong) {
            
            [fieldLong_ release];
            fieldLong_ = nil;
            fieldLong_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeipsorxeas_AnalyzingArrayLong) {
            
            [arrayLong_ release];
            arrayLong_ = nil;
            arrayLong_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeipsorxeas_AnalyzingBadge) {
            
            [badge_ release];
            badge_ = nil;
            badge_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeipsorxeas_AnalyzingTitlesArray) {
            
            [titlesArray_ release];
            titlesArray_ = nil;
            titlesArray_ = [elementString  copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == foeipsorxeas_AnalyzingEditableFieldArray) {
            
            [editableArray_ release];
            editableArray_ = nil;
            editableArray_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foeipsorxeas_AnalyzingFO) {
            
            [frequentOperation_ release];
            frequentOperation_ = nil;
            frequentOperation_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeipsorxeas_AnalyzingParam) {
            
            [param_ release];
            param_ = nil;
            param_ = [elementString  copyWithZone:self.zone];
            
        }
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
    }
    
    
}

@end


#pragma mark -

@implementation FOEInstitutionPaymentStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOEInstitutionPaymentStepOneResponseData{

    [dataList_ release];
    dataList_ = nil;
    
    [agreement_ release];
    agreement_ = nil;
    
    [accessCode_ release];
    accessCode_ = nil;
    
    [matriCode_ release];
    matriCode_ = nil;
    
    [operationClass_ release];
    operationClass_ = nil;
    
    [fieldLong_ release];
    fieldLong_ = nil;
    
    [arrayLong_ release];
    arrayLong_ = nil;
    
    [array_ release];
    array_ = nil;
    
    [badge_ release];
    badge_ = nil;
    
    [titlesArray_ release];
    titlesArray_ = nil;
    
    [editableArray_ release];
    editableArray_ = nil;
    
    [frequentOperation_ release];
    frequentOperation_ = nil;
    
    [param_ release];
    param_ = nil;
    
}

@end
