//
//  FOEThirdCardPaymentStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEThirdCardPaymentStepOneResponse.h"


/**
 * Enumerates the analysis states
 */
typedef enum {
    
    foetcpsorxeas_AnalyzingThirdNumber = foeirxeas_ElementsCount,
    foetcpsorxeas_AnalyzingCardType,
    foetcpsorxeas_AnalyzingCurrency,
    foetcpsorxeas_AnalyzingHolderName
} FOEThirdCardPaymentStepOneResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * FOEThirdCardPaymentStepOneResponse
 */
@interface FOEThirdCardPaymentStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOEThirdCardPaymentStepOneResponseData;

@end


#pragma mark -

@implementation FOEThirdCardPaymentStepOneResponse

#pragma mark -
#pragma mark Properties

@synthesize thirdCard =thirdCard_;
@synthesize cardType=cardType_;
@synthesize currency=currency_;
@synthesize holderName=holderName_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFOEThirdCardPaymentStepOneResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFOEThirdCardPaymentStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"tarjetaabono"]) {
            
            xmlAnalysisCurrentValue_ = foetcpsorxeas_AnalyzingThirdNumber;
            
        } else if([lname isEqualToString:@"tipotarjeta"]) {
            
            xmlAnalysisCurrentValue_ = foetcpsorxeas_AnalyzingCardType;
            
        }else if([lname isEqualToString:@"monedatarjeta"]) {
            
            xmlAnalysisCurrentValue_ = foetcpsorxeas_AnalyzingCurrency;
            
        }else if([lname isEqualToString:@"titulartarjeta"]) {
            
            xmlAnalysisCurrentValue_ = foetcpsorxeas_AnalyzingHolderName;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == foetcpsorxeas_AnalyzingThirdNumber) {
            
            [thirdCard_ release];
            thirdCard_ = nil;
            thirdCard_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foetcpsorxeas_AnalyzingCardType) {
            
            [cardType_ release];
            cardType_ = nil;
            cardType_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foetcpsorxeas_AnalyzingCurrency) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foetcpsorxeas_AnalyzingHolderName) {
            
            [holderName_ release];
            holderName_ = nil;
            holderName_ = [elementString  copyWithZone:self.zone];
            
        }
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FOEThirdCardPaymentStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOEThirdCardPaymentStepOneResponseData {
    
    [cardType_ release];
    cardType_ = nil;
    
    [thirdCard_ release];
    thirdCard_ = nil;
    
    
    [currency_ release];
    currency_ = nil;
    
    [holderName_ release];
    holderName_ = nil;

    
    
    
}

@end

