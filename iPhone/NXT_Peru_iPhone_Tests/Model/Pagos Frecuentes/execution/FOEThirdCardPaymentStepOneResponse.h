//
//  FOEThirdCardPaymentStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEInitialResponse.h"

@interface FOEThirdCardPaymentStepOneResponse : FOEInitialResponse
{
    /**
     * Third card number
     */
    NSString *thirdCard_;
    
    /**
     * Card type
     */
    NSString *cardType_;
    
    /**
     * currency
     */
    NSString *currency_;
    
    /**
     * Card's holder name
     */
    NSString *holderName_;

}

/**
 * Provides read-only access to the third card number
 */
@property (nonatomic, readonly, copy) NSString *thirdCard;

/**
 * Provides read-only access to the card type
 */
@property (nonatomic, readonly, copy) NSString *cardType;

/**
 * Provides read-only access to the amount
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the holder name
 */
@property (nonatomic, readonly, copy) NSString *holderName;

@end
