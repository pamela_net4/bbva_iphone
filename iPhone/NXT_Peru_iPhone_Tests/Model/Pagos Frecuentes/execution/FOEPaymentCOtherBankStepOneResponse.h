//
//  FOEPaymentCOtherBankStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEInitialResponse.h"

@interface FOEPaymentCOtherBankStepOneResponse : FOEInitialResponse
{
    /**
     * Card number
     */
    NSString *cardNumber_;
	/**
     * Card type
     */
    NSString *cardType_;
    /**
     * destination bank
     */
    NSString *destinationBank_;
    
    /**
	 * where the card was emited
	 */
    NSString *emissionPlace_;
    
    /**
     * beneficiary Name
     */
    NSString *beneficiaryName_;
    
    /**
     * place code
     */
    NSString *placeCode_;
    
    /**
     * bank code
     */
    NSString *bankCode_;
}

/**
 * Provides read-only access to the card number
 */
@property (nonatomic, readonly, copy) NSString *cardNumber;

/**
 * Provides read-only access to the card type
 */
@property (nonatomic, readonly, copy) NSString *cardType;

/**
 * Provides read-only access to the destination bank
 */
@property (nonatomic, readonly, copy) NSString *destinationBank;

/**
 * Provides read-only access to the emission place
 */
@property (nonatomic, readonly, copy) NSString *emissionPlace;

/**
 * Provides read-only access to the beneficiary name
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryName;

/**
 * Provides read-only access to the place code
 */
@property (nonatomic, readonly, copy) NSString *placeCode;

/**
 * Provides read-only access to the bank code
 */
@property (nonatomic, readonly, copy) NSString *bankCode;

@end
