//
//  FOERechargeCellphoneStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/30/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOERechargeCellphoneStepOneResponse.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    foercsorxeas_AnalyzingCompany = foeirxeas_ElementsCount, //!<Analyzing company name elment
    foercsorxeas_AnalyzingPhoneNumber, //!<Analyzing the Phone number element
    foercsorxeas_Analyzingholder, //!<Analyzing holder name element
    foercsorxeas_AnalyzingCellphone, //!<Analyzing the Cell phone number element
    foercsorxeas_AnalyzingLocality, //!<Analyzing Locality elment
    foercsorxeas_AnalyzingLocalDescription, //!<Analyzing the Phone number
    foercsorxeas_AnalyzingServiceNumber, //!<Analyzing service number elment
    foercsorxeas_AnalyzingSupply, //!<Analyzing the supply element
    foercsorxeas_AnalyzingChargeImport, //!<Analyzing the charge import element
    foercsorxeas_AnalyzingCompanyCode, //!<Analyzing the company code element
    foercsorxeas_AnalyzingServiceType, //!<Analyzing the serviceType code element
    foercsorxeas_AnalyzingFOECarrierList //!<Analyzing carrier List for FOExecution element
	
} FOERechargeCellphoneStepOneResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FOERechargeCellphoneStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOERechargeCellphoneStepOneResponseData;

@end

#pragma mark -
@implementation FOERechargeCellphoneStepOneResponse
#pragma mark -
#pragma mark Properties

@synthesize company = company_;
@synthesize phoneNumber = phoneNumber_;
@synthesize holder = holder_;
@synthesize cellPhone = cellPhone_;
@synthesize locality = locality_;
@synthesize localDescription = localDescription_;
@synthesize serviceNumber = serviceNumber_;
@synthesize supply = supply_;
@synthesize chargeImport = chargeImport_;
@synthesize companyCode = companyCode_;
@synthesize serviceType = serviceType_;
@synthesize foeCarrierList = foeCarrierList_;
#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFOERechargeCellphoneStepOneResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFOERechargeCellphoneStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"empresa"]) {
            
            xmlAnalysisCurrentValue_ = foercsorxeas_AnalyzingCompany;
            
        } else if ([lname isEqualToString:@"telefono"]) {
            
            xmlAnalysisCurrentValue_ = foercsorxeas_AnalyzingPhoneNumber;
            
        } else if ([lname isEqualToString:@"titular"]) {
            
            xmlAnalysisCurrentValue_ = foercsorxeas_Analyzingholder;
            
        } else if ([lname isEqualToString:@"celular"]) {
            
            xmlAnalysisCurrentValue_ = foercsorxeas_AnalyzingCellphone;
            
        } else if ([lname isEqualToString:@"localidad"]) {
            
            xmlAnalysisCurrentValue_ = foercsorxeas_AnalyzingLocality;
            
        } else if ([lname isEqualToString:@"desclocalidad"]) {
            
            xmlAnalysisCurrentValue_ = foercsorxeas_AnalyzingLocalDescription;
            
        } else if ([lname isEqualToString:@"numserv"]) {
            
            xmlAnalysisCurrentValue_ = foercsorxeas_AnalyzingServiceNumber;
            
        } else if ([lname isEqualToString:@"suministro"]) {
            
            xmlAnalysisCurrentValue_ = foercsorxeas_AnalyzingSupply;
            
        } else if ([lname isEqualToString:@"importerecarga"]) {
            
            xmlAnalysisCurrentValue_ = foercsorxeas_AnalyzingChargeImport;
            
        } else if ([lname isEqualToString:@"codempresa"]) {
            
            xmlAnalysisCurrentValue_ = foercsorxeas_AnalyzingCompanyCode;
            
        } else if ([lname isEqualToString:@"tiposervicio"]) {
            
            xmlAnalysisCurrentValue_ = foercsorxeas_AnalyzingServiceType;
            
        } else if([lname isEqualToString:@"operadores"]){
            
            xmlAnalysisCurrentValue_ = foercsorxeas_AnalyzingFOECarrierList;
            [foeCarrierList_ release];
            foeCarrierList_ = nil;
            foeCarrierList_ = [[FOECarrierList alloc] init];
            foeCarrierList_.openingTag = lname;
            [foeCarrierList_ setParentParseableObject:self];
            [parser setDelegate: foeCarrierList_];
            [foeCarrierList_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == foercsorxeas_AnalyzingCompany) {
            
            [company_ release];
            company_ = nil;
            company_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foercsorxeas_AnalyzingPhoneNumber) {
            
            [phoneNumber_ release];
            phoneNumber_ = nil;
            phoneNumber_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foercsorxeas_Analyzingholder) {
            
            [holder_ release];
            holder_ = nil;
            holder_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foercsorxeas_AnalyzingCellphone) {
            
            [cellPhone_ release];
            cellPhone_ = nil;
            cellPhone_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foercsorxeas_AnalyzingLocality) {
            
            [locality_ release];
            locality_ = nil;
            locality_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foercsorxeas_AnalyzingLocalDescription) {
            
            [localDescription_ release];
            localDescription_ = nil;
            localDescription_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foercsorxeas_AnalyzingServiceNumber) {
            
            [serviceNumber_ release];
            serviceNumber_ = nil;
            serviceNumber_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foercsorxeas_AnalyzingSupply) {
            
            [supply_ release];
            supply_ = nil;
            supply_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foercsorxeas_AnalyzingChargeImport) {
            
            [chargeImport_ release];
            chargeImport_ = nil;
            chargeImport_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foercsorxeas_AnalyzingCompanyCode) {
            
            [companyCode_ release];
            companyCode_ = nil;
            companyCode_ = [elementString  copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == foercsorxeas_AnalyzingServiceType) {
            
            [serviceType_ release];
            serviceType_ = nil;
            serviceType_ = [elementString  copyWithZone:self.zone];
            
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FOERechargeCellphoneStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOERechargeCellphoneStepOneResponseData {
    
    [company_ release];
    company_ = nil;
    
    [phoneNumber_ release];
    phoneNumber_ = nil;
    
    [holder_ release];
    holder_ = nil;
    
    [cellPhone_ release];
    cellPhone_ = nil;
    
    [locality_ release];
    locality_ = nil;
    
    [localDescription_ release];
    localDescription_ = nil;
    
    [serviceNumber_ release];
    serviceNumber_ = nil;
    
    [supply_ release];
    supply_ = nil;
    
    [chargeImport_ release];
    chargeImport_ = nil;
    
    [companyCode_ release];
    companyCode_ = nil;
    
    [serviceType_ release];
    serviceType_ = nil;
}

@end
