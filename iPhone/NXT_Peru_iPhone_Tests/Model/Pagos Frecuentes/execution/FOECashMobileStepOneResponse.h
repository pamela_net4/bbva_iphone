//
//  FOECashMobileStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEInitialResponse.h"

@interface FOECashMobileStepOneResponse : FOEInitialResponse
{
    /**
	 * phone number
	 */
    NSString *phoneNumber_;

}

/**
 * Provides read-only access to the company name
 */
@property (nonatomic, readonly, copy) NSString *phoneNumber;

@end
