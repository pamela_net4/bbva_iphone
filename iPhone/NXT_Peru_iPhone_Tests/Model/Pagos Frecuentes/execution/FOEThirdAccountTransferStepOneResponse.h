//
//  FOEThirdAccountTransferStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEInitialResponse.h"

@interface FOEThirdAccountTransferStepOneResponse : FOEInitialResponse
{
    /**
	 * payment account 
	 */
    NSString *paymentAccount_;
    /**
	 * Name of the beneficiary
	 */
    NSString *holderName_;
}

/**
 * Provides read-only access to the paymentAccount
 */
@property (nonatomic, readonly, copy) NSString *paymentAccount;
/**
 * Provides read-only access to beneficiaryName
 */
@property (nonatomic, readonly, copy) NSString *holderName;

@end
