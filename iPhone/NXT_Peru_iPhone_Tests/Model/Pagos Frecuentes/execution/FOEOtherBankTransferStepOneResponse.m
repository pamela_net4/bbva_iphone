//
//  FOEOtherBankTransferStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEOtherBankTransferStepOneResponse.h"
/**
 * Enumerates the analysis states
 */
typedef enum {
    
    foeobtsorxeas_AnalyzingPaymentAccount = foeirxeas_ElementsCount, //!<Analyzing payment account element
    foeobtsorxeas_AnalyzingDestinationBankAccount, //!<Analyzing the Destination bank account element
    foeobtsorxeas_AnalyzingDestinationBank, //!<Analyzing the Destination bank element
    foeobtsorxeas_AnalyzingBeneficiaryName, //!<Analyzing the beneficiary name element
    foeobtsorxeas_AnalyzingDocType, //!<Analyzing the doc type element
    foeobtsorxeas_AnalyzingDocDescriptionType, //!<Analyzing the doc description type element
    foeobtsorxeas_AnalyzingDocNumber //!<Analyzing the doc number element
    
	
} FOEOtherBankTransferStepOneResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FOEOtherBankTransferStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOEOtherBankTransferStepOneResponseData;

@end


#pragma mark -
@implementation FOEOtherBankTransferStepOneResponse

#pragma mark -
#pragma mark Properties

@synthesize paymentAccount = paymentAccount_;
@synthesize destinationBankAccount = destinationBankAccount_;
@synthesize destinationBank =destinationBank_;
@synthesize beneficiaryName = beneficiaryName_;
@synthesize docType = docType_;
@synthesize docTypeDescription = docTypeDescription_;
@synthesize docNumber = docNumber_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFOEOtherBankTransferStepOneResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFOEOtherBankTransferStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"cuentaabono"]) {
            
            xmlAnalysisCurrentValue_ = foeobtsorxeas_AnalyzingPaymentAccount;
            
        } else if ([lname isEqualToString:@"bancodestino"]) {
            
            xmlAnalysisCurrentValue_ = foeobtsorxeas_AnalyzingDestinationBank;
            
        }  else if ([lname isEqualToString:@"ctabancodestino"]) {
            
            xmlAnalysisCurrentValue_ = foeobtsorxeas_AnalyzingDestinationBankAccount;
            
        } else if ([lname isEqualToString:@"nombrebeneficiario"]) {
            
            xmlAnalysisCurrentValue_ = foeobtsorxeas_AnalyzingBeneficiaryName;
            
        } else if ([lname isEqualToString:@"tipodoc"]) {
            
            xmlAnalysisCurrentValue_ = foeobtsorxeas_AnalyzingDocType;
            
        } else if ([lname isEqualToString:@"desctipodoc"]) {
            
            xmlAnalysisCurrentValue_ = foeobtsorxeas_AnalyzingDocDescriptionType;
            
        } else if ([lname isEqualToString:@"nrodoc"]) {
            
            xmlAnalysisCurrentValue_ = foeobtsorxeas_AnalyzingDocNumber;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == foeobtsorxeas_AnalyzingPaymentAccount) {
            
            [paymentAccount_ release];
            paymentAccount_ = nil;
            paymentAccount_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeobtsorxeas_AnalyzingDestinationBankAccount) {
            
            [destinationBankAccount_ release];
            destinationBankAccount_ = nil;
            destinationBankAccount_ = [elementString  copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == foeobtsorxeas_AnalyzingDestinationBank) {
            
            [destinationBank_ release];
            destinationBank_ = nil;
            destinationBank_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeobtsorxeas_AnalyzingBeneficiaryName) {
            
            [beneficiaryName_ release];
            beneficiaryName_ = nil;
            beneficiaryName_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeobtsorxeas_AnalyzingDocType) {
            
            [docType_ release];
            docType_ = nil;
            docType_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeobtsorxeas_AnalyzingDocDescriptionType) {
            
            [docTypeDescription_ release];
            docTypeDescription_ = nil;
            docTypeDescription_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeobtsorxeas_AnalyzingDocNumber) {
            
            [docNumber_ release];
            docNumber_ = nil;
            docNumber_ = [elementString  copyWithZone:self.zone];
            
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FOEOtherBankTransferStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOEOtherBankTransferStepOneResponseData {
    
    [paymentAccount_ release];
    paymentAccount_ = nil;
    
    [destinationBank_ release];
    destinationBank_ = nil;
    
    [destinationBankAccount_ release];
    destinationBankAccount_ = nil;
    
    [beneficiaryName_ release];
    beneficiaryName_ = nil;
    
    [docType_ release];
    docType_ = nil;
    
    [docTypeDescription_ release];
    docTypeDescription_ = nil;
    
    [docNumber_ release];
    docNumber_ = nil;
    
}

@end

