//
//  FOEInitialResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/30/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    foeirxeas_AnalyzingService = serxeas_ElementsCount, //!<Analyzing the service
    foeirxeas_AnalyzingNick, //!<Analyzing the nick
    foeirxeas_AnalyzingAccountList, //!<Analyzing the account list
    foeirxeas_AnalyzingCardList, //!<Analyzing the card list
    foeirxeas_AnalyzingCarrierList, //!<Analyzing the card list
    foeirxeas_ElementsCount //!<last element    
} FOEInitialResponseXMLElementAnalyzerState;

@class FOAccountList;
@class FOCardList;
@class AlterCarrierList;
@interface FOEInitialResponse : StatusEnabledResponse
{
@private
    
    /**
	 * Service name
	 */
    NSString *service_;
    /**
	 * nick
	 */
    NSString *nick_;
    /**
	 * List with accounts
	 */
    FOAccountList *foAccountList_;
    /**
	 * List with cards
	 */
    FOCardList *foCardList_;
    /**
	 * List with carriers
	 */
    AlterCarrierList *foCarrierList_;
}

/**
 * Provides read-only access to service name
 */
@property (nonatomic, readonly, copy) NSString *service;

/**
 * Provides read-only access to the nick
 */
@property (nonatomic, readonly, copy) NSString *nick;
/**
 * Provides read-only access to the account list
 */
@property (nonatomic, readonly, retain) FOAccountList *foAccountList;

/**
 * Provides read-only access to the card list
 */
@property (nonatomic, readonly, retain) FOCardList *foCardList;

/**
 * Provides read-only access to the carrier list
 */
@property (nonatomic, readonly, retain) AlterCarrierList *foCarrierList;

@end
