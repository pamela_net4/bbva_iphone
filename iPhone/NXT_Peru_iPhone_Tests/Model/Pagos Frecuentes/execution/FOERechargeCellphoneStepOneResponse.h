//
//  FOERechargeCellphoneStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/30/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEInitialResponse.h"
#import "FOECarrierList.h"

@interface FOERechargeCellphoneStepOneResponse : FOEInitialResponse
{
    /**
     * company for the operation
     */
    NSString *company_;
    
    /**
     * phone number for the operation
     */
    NSString *phoneNumber_;
    
    /**
     * holder service for the operation
     */
    NSString *holder_;
    /**
     * carrier for the operation
     */
    NSString *cellPhone_;
    
    /**
     * phone number for the operation
     */
    NSString *locality_;
    
    /**
     * holder service for the operation
     */
    NSString *localDescription_;
    /**
     * service number for the operation
     */
    NSString *serviceNumber_;
    /**
     * supply for the operation
     */
    NSString *supply_;
    /**
     * import for the operation
     */
    NSString *chargeImport_;
    /**
     * company code for the operation
     */
    NSString *companyCode_;
    
    /**
     * serviceType for the operation
     */
    NSString *serviceType_;
    
    /**
     * carrierList for the operation
     */
    FOECarrierList *foeCarrierList_;
}

/**
 * Provides read-only access to the carrier
 */
@property (nonatomic, readonly, copy) FOECarrierList *foeCarrierList;

/**
 * Provides read-only access to the carrier
 */
@property (nonatomic, readonly, copy) NSString *serviceType;

/**
 * Provides read-only access to the carrier
 */
@property (nonatomic, readonly, copy) NSString *company;

/**
 * Provides read-only access to the phoneNumber
 */
@property (nonatomic, readonly, copy) NSString *phoneNumber;

/**
 * Provides read-only access to the holderService
 */
@property (nonatomic, readonly, copy) NSString *holder;
/**
 * Provides read-only access to the carrier
 */
@property (nonatomic, readonly, copy) NSString *cellPhone;

/**
 * Provides read-only access to the phoneNumber
 */
@property (nonatomic, readonly, copy) NSString *locality;

/**
 * Provides read-only access to the holderService
 */
@property (nonatomic, readonly, copy) NSString *localDescription;
/**
 * Provides read-only access to the carrier
 */
@property (nonatomic, readonly, copy) NSString *serviceNumber;

/**
 * Provides read-only access to the phoneNumber
 */
@property (nonatomic, readonly, copy) NSString *supply;

/**
 * Provides read-only access to the holderService
 */
@property (nonatomic, readonly, copy) NSString *chargeImport;
/**
 * Provides read-only access to the holderService
 */
@property (nonatomic, readonly, copy) NSString *companyCode;
@end
