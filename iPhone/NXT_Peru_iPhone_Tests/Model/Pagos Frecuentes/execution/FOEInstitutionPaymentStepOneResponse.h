//
//  FOEInstitutionPaymentStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/30/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEInitialResponse.h"
#import "OcurrencyList.h"

@class FODataList;
@interface FOEInstitutionPaymentStepOneResponse : FOEInitialResponse
{
    /**
	 * list of Data for the operation
	 */
    FODataList *dataList_;
    /**
	 * Agreement of the operation
	 */
    NSString *agreement_;
    /**
	 * Code access for the operation
	 */
    NSString *accessCode_;
    /**
	 * Matri code
	 */
    NSString *matriCode_;
    /**
	 * Class of the operation
	 */
    NSString *operationClass_;
    /**
	 * longitud of fields listed
	 */
    NSString *fieldLong_;
    /**
	 * longitu of values on the fields
	 */
    NSString *arrayLong_;
    /**
	 * array with the values
	 */
    NSString *array_;
    /**
	 * the badge for the operation
	 */
    NSString *badge_;
    /**
	 * array with the titles
	 */
    NSString *titlesArray_;
    
    /**
	 * array with the indicator of editable array
	 */
    NSString *editableArray_;

    
    /**
	 * is a frequent operation
	 */
    NSString *frequentOperation_;
    /**
	 * parameter of the operation
	 */
    NSString *param_;
    
    /**
     *
     */
    OcurrencyList *ocurrencyList_;
}

/**
 * Provides read-only access to the dataList
 */
@property(nonatomic,readonly,retain) FODataList *dataList;
/**
 * Provides read-only access to the agreement
 */
@property(nonatomic,readonly,retain) NSString *agreement;
/**
 * Provides read-only access to the accessCode
 */
@property(nonatomic,readonly,retain) NSString *accessCode;
/**
 * Provides read-only access to the matriCode
 */
@property(nonatomic,readonly,retain) NSString *matriCode;
/**
 * Provides read-only access to the operationClass
 */
@property(nonatomic,readonly,retain) NSString *operationClass;
/**
 * Provides read-only access to the fieldLong
 */
@property(nonatomic,readonly,retain) NSString *fieldLong;
/**
 * Provides read-only access to the arrayLong
 */
@property(nonatomic,readonly,retain) NSString *arrayLong;
/**
 * Provides read-only access to the array
 */
@property(nonatomic,readonly,retain) NSString *array;
/**
 * Provides read-only access to the badge
 */
@property(nonatomic,readonly,retain) NSString *badge;
/**
 * Provides read-only access to the titlesArray
 */
@property(nonatomic,readonly,retain) NSString *titlesArray;

/**
 * Provides read-only access to the editableArray
 */
@property(nonatomic,readonly,retain) NSString *editableArray;

/**
 * Provides read-only access to the frequentOperation
 */
@property(nonatomic,readonly,retain) NSString *frequentOperation;
/**
 * Provides read-only access to the param
 */
@property(nonatomic,readonly,retain) NSString *param;

@end
