//
//  FOERechargeGiftCardStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/30/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEInitialResponse.h"

@interface FOERechargeGiftCardStepOneResponse : FOEInitialResponse
{
    /**
	 * gift card for the operation
	 */
    NSString *giftCard_;
}

/**
 * Provides read-only access to the giftCard
 */
@property (nonatomic, readonly, copy) NSString *giftCard;

@end
