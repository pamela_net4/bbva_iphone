//
//  FOEOtherBankTransferStepOneResponse.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEInitialResponse.h"

@interface FOEOtherBankTransferStepOneResponse : FOEInitialResponse
{
    /**
	 * Cuenta abono
	 */
    NSString *paymentAccount_;
    /**
	 * Destination bank account to pay
	 */
    NSString *destinationBankAccount_;
    /**
	 * Bank account to pay
	 */
    NSString *destinationBank_;

    /**
	 * Name of the beneficiary
	 */
    NSString *beneficiaryName_;
    /**
	 * Indentification document type
	 */
    NSString *docType_;
    /**
	 * Description of the document type
	 */
    NSString *docTypeDescription_;
    /**
	 * Number of the identification document
	 */
    NSString *docNumber_;
}

/**
 * Provides read-only access to the paymentAccount
 */
@property (nonatomic, readonly, copy) NSString *paymentAccount;
/**
 * Provides read-only access to destinationBankAccount
 */
@property (nonatomic, readonly, copy) NSString *destinationBankAccount;
/**
 * Provides read-only access to destinationBank
 */
@property (nonatomic, readonly, copy) NSString *destinationBank;
/**
 * Provides read-only access to beneficiaryName
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryName;
/**
 * Provides read-only access to docType
 */
@property (nonatomic, readonly, copy) NSString *docType;
/**
 * Provides read-only access to docType
 */
@property (nonatomic, readonly, copy) NSString *docTypeDescription;
/**
 * Provides read-only access to docNumber
 */
@property (nonatomic, readonly, copy) NSString *docNumber;

@end
