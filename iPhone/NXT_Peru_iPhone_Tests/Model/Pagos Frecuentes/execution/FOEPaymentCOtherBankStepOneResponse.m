//
//  FOEPaymentCOtherBankStepOneResponse.m
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 30/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEPaymentCOtherBankStepOneResponse.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    foepcobsorxeas_AnalyzingCardNumber = foeirxeas_ElementsCount,
    foepcobsorxeas_AnalyzingCardType,
    foepcobsorxeas_AnalyzingDestinationBank,
    foepcobsorxeas_AnalyzingEmissionPlace,
    foepcobsorxeas_AnalyzingBeneficiaryName,
    foepcobsorxeas_AnalyzingPlaceCode,
    foepcobsorxeas_AnalyzingBankCode
	
} FOEPaymentCOtherBankStepOneResponseXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface FOEPaymentCOtherBankStepOneResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOEPaymentCOtherBankStepOneResponseData;

@end


#pragma mark -

@implementation FOEPaymentCOtherBankStepOneResponse

#pragma mark -
#pragma mark Properties

@synthesize cardNumber=cardNumber_;
@synthesize cardType=cardType_;
@synthesize destinationBank=destinationBank_;
@synthesize emissionPlace=emissionPlace_;
@synthesize beneficiaryName=beneficiaryName_;
@synthesize placeCode=placeCode_;
@synthesize bankCode=bankCode_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */

- (void)dealloc {
    
    [self clearFOEPaymentCOtherBankStepOneResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearFOEPaymentCOtherBankStepOneResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"tarjetaabono"]) {
            
            xmlAnalysisCurrentValue_ = foepcobsorxeas_AnalyzingCardNumber;
            
        } else if([lname isEqualToString:@"tipotarjeta"]) {
            
            xmlAnalysisCurrentValue_ = foepcobsorxeas_AnalyzingCardType;
            
        }else if([lname isEqualToString:@"bancodestino"]) {
            
            xmlAnalysisCurrentValue_ = foepcobsorxeas_AnalyzingDestinationBank;
            
        }else if([lname isEqualToString:@"titulartarjeta"]) {
            
            xmlAnalysisCurrentValue_ = foepcobsorxeas_AnalyzingBeneficiaryName;
            
        }else if([lname isEqualToString:@"lugaremision"]) {
            
            xmlAnalysisCurrentValue_ = foepcobsorxeas_AnalyzingEmissionPlace;
            
        }else if([lname isEqualToString:@"codlugar"]) {
            
            xmlAnalysisCurrentValue_ = foepcobsorxeas_AnalyzingPlaceCode;
            
        }else if([lname isEqualToString:@"codbanco"]) {
            
            xmlAnalysisCurrentValue_ = foepcobsorxeas_AnalyzingBankCode;
            
        }
        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == foepcobsorxeas_AnalyzingCardNumber) {
            
            [cardNumber_ release];
            cardNumber_ = nil;
            cardNumber_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foepcobsorxeas_AnalyzingCardType) {
            
            [cardType_ release];
            cardType_ = nil;
            cardType_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foepcobsorxeas_AnalyzingDestinationBank) {
            
            [destinationBank_ release];
            destinationBank_ = nil;
            destinationBank_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foepcobsorxeas_AnalyzingEmissionPlace) {
            
            [emissionPlace_ release];
            emissionPlace_ = nil;
            emissionPlace_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foepcobsorxeas_AnalyzingBeneficiaryName) {
            
            [beneficiaryName_ release];
            beneficiaryName_ = nil;
            beneficiaryName_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foepcobsorxeas_AnalyzingBankCode) {
            
            [bankCode_ release];
            bankCode_ = nil;
            bankCode_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foepcobsorxeas_AnalyzingPlaceCode) {
            
            [placeCode_ release];
            placeCode_ = nil;
            placeCode_ = [elementString  copyWithZone:self.zone];
            
        }
        
    }else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation FOEPaymentCOtherBankStepOneResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOEPaymentCOtherBankStepOneResponseData {
    
    [cardType_ release];
    cardType_ = nil;
    
    [cardNumber_ release];
    cardNumber_ = nil;

    
    [destinationBank_ release];
    destinationBank_ = nil;

    
    [emissionPlace_ release];
    emissionPlace_ = nil;

    
    [beneficiaryName_ release];
    beneficiaryName_ = nil;
    
    [placeCode_ release];
    placeCode_ = nil;
    
    [bankCode_ release];
    bankCode_ = nil;

    
    
}

@end
