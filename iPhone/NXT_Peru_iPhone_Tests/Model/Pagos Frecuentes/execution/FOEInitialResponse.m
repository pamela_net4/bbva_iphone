//
//  FOEInitialResponse.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/30/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEInitialResponse.h"
#import "FOAccountList.h"
#import "FOCardList.h"
#import "AlterCarrierList.h"

#pragma mark -

/**
 * FOEInitialResponse private category
 */
@interface FOEInitialResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFOEInitialResponseData;

@end


#pragma mark -

@implementation FOEInitialResponse

#pragma mark - properties
@synthesize service = service_;
@synthesize nick = nick_;
@synthesize foAccountList = foAccountList_;
@synthesize foCardList = foCardList_;
@synthesize foCarrierList = foCarrierList_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearFOEInitialResponseData];
    
    [super dealloc];
    
}
#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOEInitialResponse instance providing the associated notification
 *
 * @return The initialized FOEInitialResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		//self.notificationToPost =  kNotificationFOInitialResponse;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFOEInitialResponseData];
    
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
       
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"servicio"]) {
            
            xmlAnalysisCurrentValue_ = foeirxeas_AnalyzingService;
            
        } else if ([lname isEqualToString:@"alias"]) {
            
            xmlAnalysisCurrentValue_ = foeirxeas_AnalyzingNick;
            
        } else if ([lname isEqualToString: @"cuentas"]) {
            
            xmlAnalysisCurrentValue_ = foeirxeas_AnalyzingAccountList;
			[foAccountList_ release];
            foAccountList_ = nil;
            foAccountList_ = [[FOAccountList alloc] init];
            foAccountList_.openingTag = lname;
            [foAccountList_ setParentParseableObject:self];
            [parser setDelegate:foAccountList_];
            [foAccountList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString: @"tarjetas"]) {
            
            xmlAnalysisCurrentValue_ = foeirxeas_AnalyzingCardList;
			[foCardList_ release];
            foCardList_ = nil;
            foCardList_ = [[FOCardList alloc] init];
            foCardList_.openingTag = lname;
            [foCardList_ setParentParseableObject:self];
            [parser setDelegate:foCardList_];
            [foCardList_ parserDidStartDocument:parser];
            
            
        } else if ([lname isEqualToString: @"operadoras"]) {
            
            xmlAnalysisCurrentValue_ = foeirxeas_AnalyzingCarrierList;
			[foCarrierList_ release];
            foCarrierList_ = nil;
            foCarrierList_ = [[AlterCarrierList alloc] init];
            foCarrierList_.openingTag = lname;
            [foCarrierList_ setParentParseableObject:self];
            [parser setDelegate:foCarrierList_];
            [foCarrierList_ parserDidStartDocument:parser];
            
        } else{
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (xmlAnalysisCurrentValue_ == foeirxeas_AnalyzingService ) {
            [service_ release];
            service_ = nil;
            service_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == foeirxeas_AnalyzingNick) {
            
            [nick_ release];
            nick_ = nil;
            nick_ = [elementString  copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    
}

@end


#pragma mark -

@implementation FOEInitialResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFOEInitialResponseData{
    
    [service_ release];
    service_ = nil;
    
    [nick_ release];
    nick_ = nil;
    
    [foAccountList_ release];
    foAccountList_ = nil;
    
    [foCardList_ release];
    foCardList_ = nil;
    
    [foCarrierList_ release];
    foCarrierList_ = nil;

}

@end
