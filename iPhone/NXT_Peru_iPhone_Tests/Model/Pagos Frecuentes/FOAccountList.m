//
//  AccountList.m
//  NXT_Peru_iPad
//
//  Created by usuario on 23/01/14.
//
//

#import "FOAccountList.h"
#import "Account.h"

#pragma mark -

/**
 * AccountList private category
 */
@interface FOAccountList(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearAccountListData;

@end


#pragma mark -


@implementation FOAccountList


#pragma mark -
#pragma mark Properties

@dynamic accountList;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearAccountListData];
    
    [accountList_ release];
    accountList_ = nil;
    
      [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a AccountList instance creating the associated array
 *
 * @return The initialized AccountList instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        accountList_ = [[NSMutableArray alloc] init];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates the Accounts list from another Accounts list
 */
- (void)updateFrom:(FOAccountList *)aAccountList {
    
    NSArray *otherAccountListArray = aAccountList.accountList;
    
    [self clearAccountListData];
    
    for (Account *Account in otherAccountListArray) {
        
        [accountList_ addObject:Account];
        
    }
    
    [self updateFromStatusEnabledResponse:aAccountList];
    
    self.informationUpdated = aAccountList.informationUpdated;
    
}

/*
 * Remove the contained data
 */
- (void)removeData {
    
    [self clearAccountListData];
    
}

#pragma mark -
#pragma mark Getters and setters

/*
 * Returns the Account located at the given position, or nil if position is not valid
 */
- (Account *)accountAtPosition:(NSUInteger)aPosition{
    
	Account *result = nil;
	
	if (aPosition < [accountList_ count]) {
        
		result = [accountList_ objectAtIndex:aPosition];
        
	}
	
	return result;
    
}

/**
 * Provides read access to the number of Accounts stored
 *
 * @return The count
 */
- (NSUInteger) accountCount {
	return [accountList_ count];
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearAccountListData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (auxAccount_ != nil) {
        
        [accountList_ addObject:auxAccount_];
        //[auxAccount_ release];
        auxAccount_ = nil;
        
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
            auxAccount_ = [[Account alloc] init];
            [auxAccount_ setParentParseableObject:self];
            auxAccount_.openingTag = lname;
            [parser setDelegate:auxAccount_];
            [auxAccount_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (auxAccount_ != nil) {
        
        [accountList_ addObject:auxAccount_];
        [auxAccount_ release];
        auxAccount_ = nil;
        
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the Accounts list array
 *
 * @return The Accounts list array
 */
- (NSArray *)accountList {
    
    return [NSArray arrayWithArray:accountList_];
    
}

@end


#pragma mark -

@implementation FOAccountList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearAccountListData {
    
    [accountList_ removeAllObjects];
    
    [auxAccount_ release];
    auxAccount_ = nil;
    
}

@end
