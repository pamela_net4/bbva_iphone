//
//  FODataList.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/30/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@class FOData;
@interface FODataList : StatusEnabledResponse
{
@private
    FOData *auxFOData_;
    NSMutableArray *foDataArray_;
}

@property(nonatomic,readonly,retain) NSMutableArray *foDataArray;

/**
 * Provides read-only access to the Places count
 */
@property(nonatomic,readonly,assign) NSUInteger foDataCount;

/**
 * Returns the FOData located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the FOData is located at
 * @return FOData located at the given position, or nil if position is not valid
 */
- (FOData *)foDataAtPosition:(NSUInteger)aPosition;

/**
 * Remove the contained data
 */
- (void)removeData;

@end