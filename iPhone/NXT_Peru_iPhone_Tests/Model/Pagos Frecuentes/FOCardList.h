//
//  FOCardList.h
//  NXT_Peru_iPhone
//
//  Created by Jose Luis on 27/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@class FOCard;

@interface FOCardList : StatusEnabledResponse
{
@private
    FOCard *auxFOCard_;
    NSMutableArray *foCardArray_;
}

@property(nonatomic,readonly,retain) NSArray *foCardArray;

/**
 * Provides read-only access to the Places count
 */
@property(nonatomic,readonly,assign) NSUInteger foCardCount;

/**
 * Returns the FOPlace located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the AccountTransaction is located at
 * @return FOPlace located at the given position, or nil if position is not valid
 */
- (FOCard *)foCardAtPosition:(NSUInteger)aPosition;

/**
 * Remove the contained data
 */
- (void)removeData;

/**
 * Updates the Channel list from another Channel list
 *
 * @param aChannelList The ChannelList instance to update from
 */
- (void)updateFrom:(FOCardList *)aCardList;


@end
