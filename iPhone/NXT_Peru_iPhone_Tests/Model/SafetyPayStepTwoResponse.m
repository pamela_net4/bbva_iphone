//
//  SafetyPayStepTwoResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 21/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//
#import "SafetyPayStepTwoResponse.h"

@implementation SafetyPayStepTwoResponse

@synthesize transactionNumber = transactionNumber_;
@synthesize establishment = establishment_;
@synthesize currency = currency_;
@synthesize amount = amount_;
@synthesize accountList = accountList_;
@synthesize company = company_;
@synthesize service = service_;
@synthesize alterCarrierList = alterCarrierList_;

- (void) parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearSafetyPayStepTwoResponseData];
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing){
        NSString *lname = [elementName lowercaseString];
        
        if([lname isEqualToString: @"informacionadicional"]){
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingSafetyPayStepTwoResponseDetail;
        }
        else if([lname isEqualToString: @"identificador"]){
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingTransactionNumber;
        }
        else if([lname isEqualToString: @"establecimiento"]){
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingEstablishment;
        }
        else if([lname isEqualToString: @"moneda"]){
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingCurrency;
        }
        else if([lname isEqualToString: @"importe"]){
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingAmount;
        }
        else if([lname isEqualToString: @"cuentas"]){
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingAccountList;
            accountList_ = nil;
            accountList_ = [[FOAccountList alloc] init];
            accountList_.openingTag = lname;
            [accountList_ setParentParseableObject: self];
            [parser setDelegate:accountList_];
            [accountList_ parserDidStartDocument:parser];
        }
        else if([lname isEqualToString: @"empresa"]){
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingCompany;
        }
        else if([lname isEqualToString: @"servicio"]){
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingService;
        }
        else if([lname isEqualToString: @"operadoras"]){
            
            xmlAnalysisCurrentValue_ = atrdxeas_AnalyzingCarrierList;
            alterCarrierList_ = nil;
            alterCarrierList_ = [[AlterCarrierList alloc] init];
            alterCarrierList_.openingTag = lname;
            [alterCarrierList_ setParentParseableObject:self];
            [parser setDelegate:alterCarrierList_];
            [alterCarrierList_ parserDidStartDocument:parser];
            
        }
        else {
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
    }
    
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString *elementString = [self.elementString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if(xmlAnalysisCurrentValue_ != serxeas_Nothing){
        
        if(xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingTransactionNumber){
            
            transactionNumber_ = nil;
            transactionNumber_ = elementString;
            
        } else if(xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingEstablishment){
            
            establishment_ = nil;
            establishment_ = elementString;
            
        } else if(xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingCurrency){
            
            currency_ = nil;
            currency_ = elementString;
            
        } else if(xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingAmount){
            
            amount_ = nil;
            amount_ = [[NSDecimalNumber alloc] initWithDouble:elementString.doubleValue];
            
        } else if(xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingCompany){
            
            company_ = nil;
            company_ = elementString;
            
        } else if(xmlAnalysisCurrentValue_ == atrdxeas_AnalyzingService){
            
            service_ = nil;
            service_ = elementString;
            
        }
        
    }
}

- (void) clearSafetyPayStepTwoResponseData {
    transactionNumber_ = nil;
    establishment_ = nil;
    currency_ = nil;
    amount_ = nil;
    accountList_ = nil;
    company_ = nil;
    service_ = nil;
    alterCarrierList_ = nil;
}


@end
