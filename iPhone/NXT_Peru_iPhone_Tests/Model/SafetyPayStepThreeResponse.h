//
//  SafetyPayStepThreeResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 22/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"
#import "BankAccount.h"

typedef enum {

    atrdxeas_AnalyzingSafetyPayStepThreeResponseDetail,
    atrdxeas_AnalyzingTransactionNumber,
    atrdxeas_AnalyzingEstablishment,
    atrdxeas_AnalyzingCurrency,
    atrdxeas_AnalyzingBadge,
    atrdxeas_AnalyzingAmount,
    atrdxeas_AnalyzingAccount,
    atrdxeas_AnalyzingCompany,
    atrdxeas_AnalyzingService,
    atrdxeas_AnalyzingCoordinate,
    atrdxeas_AnalyzingSeal
} atrd_AnalyzingSafetyPayStepThreeResponseXMLElementAnalyzerState;

@interface SafetyPayStepThreeResponse : StatusEnabledResponse
{
@private
    NSString *transactionNumber_;
    NSString *establishment_;
    NSString *currency_;
    NSString *badge_;
    NSDecimalNumber *amount_;
    BankAccount *account_;
    NSString *company_;
    NSString *service_;
    NSString *coordinate_;
    NSString *seal_;
}
@property (nonatomic, readwrite) NSString *transactionNumber;
@property (nonatomic, readwrite) NSString *establishment;
@property (nonatomic, readwrite) NSString *currency;
@property (nonatomic, readwrite) NSString *badge;
@property (nonatomic, readwrite) NSDecimalNumber *amount;
@property (nonatomic, readwrite) BankAccount *account;
@property (nonatomic, readwrite) NSString *company;
@property (nonatomic, readwrite) NSString *service;
@property (nonatomic, readwrite) NSString *coordinate;
@property (nonatomic, readwrite) NSString *seal;
- (void) clearSatefyPayStepThreeResponseData;
@end
