//
//  SafetyPayDetailAdditionalInformation.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "SafetyPayDetailAdditionalInformation.h"

@interface SafetyPayDetailAdditionalInformation (private)

- (void) clearSafetyPayDetailAdditionalInformationData;

@end

@implementation SafetyPayDetailAdditionalInformation

#pragma mark -
#pragma mark Properties

@synthesize transactionNumber = transactionNumber_;
@synthesize establishment = establishment_;
@synthesize currency = currency_;
@synthesize badge = badge_;
@synthesize amount = amount_;
@synthesize account = account_;
@synthesize company = company_;
@synthesize service = service_;
@synthesize coordinate = coordinate_;
@synthesize seal = seal_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearSafetyPayDetailAdditionalInformationData];
    
    [super dealloc];
    
}

- (void) removeData {
    
    [self clearSafetyPayDetailAdditionalInformationData];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
-(void) parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearSafetyPayDetailAdditionalInformationData];
}



/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing){
        
        NSString *lname = [elementName lowercaseString];
        
        if([lname isEqualToString: @"identificador"]){
            xmlAnalysisCurrentValue_ = stpdaixeas_AnalyzingTransactionNumber;
        }
        if([lname isEqualToString: @"establecimiento"]){
            xmlAnalysisCurrentValue_ = stpdaixeas_AnalyzingEstablishment;
        }
        if([lname isEqualToString: @"moneda"]){
            xmlAnalysisCurrentValue_ = stpdaixeas_AnalyzingCurrency;
        }
        if([lname isEqualToString: @"divimporte"]){
            xmlAnalysisCurrentValue_ = stpdaixeas_AnalyzingBadge;
        }
        if([lname isEqualToString: @"importe"]){
            xmlAnalysisCurrentValue_ = stpdaixeas_AnalyzingAmount;
        }
        if([lname isEqualToString: @"cuenta"]){
            xmlAnalysisCurrentValue_ = stpdaixeas_AnalyzingAccount;
            [account_ release];
            account_ = nil;
            account_ = [[BankAccount alloc] init];
            account_.openingTag = lname;
            [account_ setParentParseableObject:self];
            [parser setDelegate:account_];
            [account_ parserDidStartDocument:parser];
        }
        if([lname isEqualToString: @"empresa"]){
            xmlAnalysisCurrentValue_ = stpdaixeas_AnalyzingCompany;
        }
        if([lname isEqualToString: @"servicio"]){
            xmlAnalysisCurrentValue_ = stpdaixeas_AnalyzingService;
        }
        if([lname isEqualToString: @"coordenada"]){
            xmlAnalysisCurrentValue_ = stpdaixeas_AnalyzingCoordinate;
        }
        if([lname isEqualToString: @"selloseg"]){
            xmlAnalysisCurrentValue_ = stpdaixeas_AnalyzingSeal;
        }
        
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if(xmlAnalysisCurrentValue_ != serxeas_Nothing){
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if([lname isEqualToString: @"identificador"]){
            
            transactionNumber_ = nil;
            transactionNumber_ = elementString;
            
        } else if ([lname isEqualToString: @"establecimiento"]) {
            
            establishment_ = nil;
            establishment_ = elementString;
            
        } else if ([lname isEqualToString: @"moneda"]){
            
            currency_ = nil;
            currency_ = elementString;
            
        } else if([lname isEqualToString: @"divimporte"]){
            
            badge_ = nil;
            badge_ = elementString;
            
        } else if ([lname isEqualToString: @"importe"]){
            
            amount_ = nil;
            amount_ = [[NSDecimalNumber alloc] initWithDouble:elementString.doubleValue];
            
        } else if([lname isEqualToString: @"empresa"]){
            
            company_ = nil;
            company_ = elementString;
            
        } else if([lname isEqualToString: @"servicio"]){
            
            service_ = nil;
            service_ = elementString;
            
        } else if([lname isEqualToString: @"coordenada"]){
            
            coordinate_ = nil;
            coordinate_ = elementString;
            
        } else if([lname isEqualToString: @"selloseg"]){
            
            seal_ = nil;
            
            seal_ = [elementString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
            seal_ = [seal_ stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void) clearSafetyPayDetailAdditionalInformationData {
    
    [transactionNumber_ release];
    transactionNumber_ = nil;
    
    [establishment_ release];
    establishment_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [badge_ release];
    badge_ = nil;
    
    [amount_ release];
    amount_ = nil;
    
    [company_ release];
    company_ = nil;
    
    [service_ release];
    service_ = nil;
    
    [coordinate_ release];
    coordinate_ = nil;
    
    [seal_ release];
    seal_ = nil;
    
    [account_ release];
    account_ = nil;
}

@end
