//
//  SafetyPayTransactionInfoResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"
#import "SafetyPayTransactionAdditionalInformation.h"

typedef enum {
    stptxeas_AnalyzingSafetyPayStatusResponseDetail,
    stptxeas_AnalyzingAdditionalInformation,
} stptxeas_AnalyzingSafetyPayStatusResponseXMLElementAnalyzerState;

@interface SafetyPayTransactionInfoResponse : StatusEnabledResponse {
@private
    SafetyPayTransactionAdditionalInformation *additionalInformation_;
}
@property (nonatomic, readonly, retain) SafetyPayTransactionAdditionalInformation *additionalInformation;
@end