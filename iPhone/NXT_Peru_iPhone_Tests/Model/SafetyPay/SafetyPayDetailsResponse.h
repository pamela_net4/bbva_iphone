//
//  SafetyPayDetailsResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"
#import "SafetyPayDetailAdditionalInformation.h"

typedef enum {
    stpdxeas_AnalyzingSafetyPayStatusResponseDetail,
    stpdxeas_AnalyzingAdditionalInformation,
} stpdxeas_AnalyzingSafetyPayStatusResponseXMLElementAnalyzerState;

@interface SafetyPayDetailsResponse : StatusEnabledResponse
{
@private
    SafetyPayDetailAdditionalInformation *additionalInformation_;
}
@property (nonatomic, readonly, retain) SafetyPayDetailAdditionalInformation *additionalInformation;
@end
