//
//  SafetyPayTransactionAdditionalInformation.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"
#import "AccountList.h"
#import "AlterCarrierList.h"

typedef enum {
    stptaixeas_AnalyzingSafetyPayStepTwoResponseDetail,
    stptaixeas_AnalyzingTransactionNumber,
    stptaixeas_AnalyzingEstablishment,
    stptaixeas_AnalyzingCurrency,
    stptaixeas_AnalyzingAmount,
    stptaixeas_AnalyzingAccountList,
    stptaixeas_AnalyzingCompany,
    stptaixeas_AnalyzingService,
    stptaixeas_AnalyzingCarrierList
} stptaixeas_AnalyzingSafetyPayStepTwoResponseXMLElementAnalyzerSate;

@interface SafetyPayTransactionAdditionalInformation : StatusEnabledResponse {
@private
    NSString *transactionNumber_;
    NSString *establishment_;
    NSString *currency_;
    NSDecimalNumber *amount_;
    AccountList *accountList_;
    NSString *company_;
    NSString *service_;
    AlterCarrierList *alterCarrierList_;
}
@property (nonatomic, readonly, copy) NSString *transactionNumber;
@property (nonatomic, readonly, copy) NSString *establishment;
@property (nonatomic, readonly, copy) NSString *currency;
@property (nonatomic, readonly, copy) NSDecimalNumber *amount;
@property (nonatomic, readonly, copy) AccountList *accountList;
@property (nonatomic, readonly, copy) NSString *company;
@property (nonatomic, readonly, copy) NSString *service;
@property (nonatomic, readonly, copy) AlterCarrierList *alterCarrierList;
- (void) removeData;
@end
