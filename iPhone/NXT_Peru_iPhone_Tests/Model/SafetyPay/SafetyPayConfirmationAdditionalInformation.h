//
//  SafetyPayConfirmationAdditionalInformation.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BankAccount.h"

typedef enum {
    stpcaixeas_AnalyzingSafetyPayStepFourResponseDetail,
    stpcaixeas_AnalyzingTransactionNumber,
    stpcaixeas_AnalyzingEstablishment,
    stpcaixeas_AnalyzingCurrency,
    stpcaixeas_AnalyzingBadge,
    stpcaixeas_AnalyzingAmount,
    stpcaixeas_AnalyzingPaidAmount,
    stpcaixeas_AnalyzingAccount,
    stpcaixeas_AnalyzingCompany,
    stpcaixeas_AnalyzingService,
    stpcaixeas_AnalyzingOperationDate,
    stpcaixeas_AnalyzingOperationNumber
} stpcaixeas_AnalyzingSafetyPayStepFourResponseXMLElementAnalyzerState;


@interface SafetyPayConfirmationAdditionalInformation : StatusEnabledResponse
{
@private
    NSString *transactionNumber_;
    NSString *establishment_;
    NSString *currency_;
    NSString *badge_;
    NSDecimalNumber *amount_;
    NSDecimalNumber *paidAmount_;
    NSDecimalNumber *chargedAmount_;
    BankAccount *account_;
    NSString *company_;
    NSString *service_;
    NSString *operationDateString_;
    NSDate *operationDate_;
    NSString *operationNumber_;
}
@property (nonatomic, readonly, copy) NSString *transactionNumber;
@property (nonatomic, readonly, copy) NSString *establishment;
@property (nonatomic, readonly, copy) NSString *currency;
@property (nonatomic, readonly, copy) NSString *badge;
@property (nonatomic, readonly, copy) NSDecimalNumber *amount;
@property (nonatomic, readonly, copy) NSDecimalNumber *paidAmount;
@property (nonatomic, readonly, copy) NSDecimalNumber *chargedAmount;
@property (nonatomic, readonly, copy) BankAccount *account;
@property (nonatomic, readonly, copy) NSString *company;
@property (nonatomic, readonly, copy) NSString *service;
@property (nonatomic, readonly, copy) NSString *operationDateString;
@property (nonatomic, readonly, copy) NSDate *operationDate;
@property (nonatomic, readonly, copy) NSString *operationNumber;
- (void) removeData;
@end
