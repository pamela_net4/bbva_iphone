//
//  AlterCarrier.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 21/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "AlterCarrier.h"

@implementation AlterCarrier

@synthesize code = code_;
@synthesize description = description_;

- (void) parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearAlterCarrierData];
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing){
        NSString *lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"codigo"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingCarrierCode;
            
        } else if ([lname isEqualToString: @"descripcion"]){
                
            xmlAnalysisCurrentValue_ = axeas_AnalyzingDescription;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if(xmlAnalysisCurrentValue_ != serxeas_Nothing){
        
        if(xmlAnalysisCurrentValue_ == axeas_AnalyzingCarrierCode) {
            
            code_ = nil;
            code_ = elementString;
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingDescription) {
            
            description_ = nil;
            description_ = elementString;
            
        }
        
    }
}

- (void) clearAlterCarrierData {
    
    code_ = nil;
    description_ = nil;
    
}

@end
