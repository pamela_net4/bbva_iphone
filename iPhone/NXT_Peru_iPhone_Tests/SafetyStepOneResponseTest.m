//
//  SafetyStepOneResponseTest.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 23/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SafetyPayStepOneResponse.h"
#import "Updater.h"

#define ClienteTieneEstadoActivo @""\
@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"\
@"<informacionadicional>"\
@"<estadoactivo>S</estadoactivo>"\
@"<mensajeinformativo></mensajeinformativo>"\
@"</informacionadicional>"

#define ClienteTieneEstadoInactivo @""\
@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"\
@"<informacionadicional>"\
@"<estadoactivo>N</estadoactivo>"\
@"<mensajeinformativo>No se encuentra habilitado SafetyPay en éste momento.</mensajeinformativo>"\
@"</informacionadicional>"


@interface SafetyStepOneResponseTest : XCTestCase
{
@private
    SafetyPayStepOneResponse *response_;
}
@end

@implementation SafetyStepOneResponseTest

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

#pragma mark El cliente tiene el estado habilitado para SP

- (void) testClienteTieneEstadoActivoDebeDevolverSyMensajeInformativoDebeSerVacio {
    
    
    [self doParsingSafetyPayStepOneResponseWithXMLString : ClienteTieneEstadoActivo];
    
    XCTAssertTrue([response_.estadoActivo isEqualToString:@"S"] && [response_.mensajeInformativo length]==0);
}

- (void) testClienteTieneEstadoInactivoDebeDevolverNyMensajeInformativoTieneDatos {
    
    
    [self doParsingSafetyPayStepOneResponseWithXMLString : ClienteTieneEstadoInactivo];
    
    XCTAssertTrue([response_.estadoActivo isEqualToString:@"N"] && [response_.mensajeInformativo length] > 0);
}

#pragma mark -

- (void) doParsingSafetyPayStepOneResponseWithXMLString : (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    response_ = [SafetyPayStepOneResponse alloc];
    response_.openingTag = @"informacionadicional";
    
    [parser setDelegate: response_];
    
    [response_ parserDidStartDocument: parser];
    
    [parser parse];
}

@end
