/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKDoubleLabel.h"
#import "GraphicElementsStyler.h"


#pragma mark -

@implementation MOKDoubleLabel

#pragma mark -
#pragma mark Properties

@dynamic topLabelText;
@dynamic bottomLabelText;
@dynamic topLabelColor;
@dynamic bottomLabelColor;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory.
 */
- (void)dealloc{
    
    [topLabel_ release];
    topLabel_ = nil;
    
    [bottomLabel_ release];
    bottomLabel_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes the DoubleLabel instance creating the associated labels.
 *
 * @param frame The view original frame.
 * @return The initialized DoubleLabel instance.
 */
- (id)initWithFrame:(CGRect)frame {
    
    if ((self = [super initWithFrame:frame])) {
        
        frame = self.frame;
        CGFloat width = frame.size.width;
        CGFloat height = frame.size.height;
        CGFloat halfHeight = floor(height / 2.0f);
        UIColor *clearColor = [UIColor clearColor];
        UIColor *whiteColor = [UIColor whiteColor];
        
        self.backgroundColor = clearColor;
        
        topLabel_ = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, width, halfHeight)];
        [GraphicElementsStyler styleLabel:topLabel_
                         withBoldFontSize:14.0f
                                    color:whiteColor];
        topLabel_.backgroundColor = clearColor;
        topLabel_.textAlignment = UITextAlignmentCenter;
        [self addSubview:topLabel_];
        
        bottomLabel_ = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, halfHeight, width, halfHeight)];
        [GraphicElementsStyler styleLabel:bottomLabel_
                             withFontSize:12.0f
                                    color:whiteColor];
        bottomLabel_.backgroundColor = clearColor;
        bottomLabel_.textAlignment = UITextAlignmentCenter;
        [self addSubview:bottomLabel_];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark UIView methods

/**
 * Lays out subviews.
 */
- (void)layoutSubviews {
    
    CGRect frame = self.frame;
    CGFloat width = frame.size.width;
    CGFloat height = frame.size.height;
    CGFloat halfHeight = floor(height / 2.0f);
    
    NSString *bottomText = bottomLabel_.text;
    
    NSUInteger bottomTextLenght = [bottomText length];
    
    if (bottomTextLenght == 0) {
        
        topLabel_.frame = CGRectMake(0.0f, 0.0f, width, height);
        bottomLabel_.hidden = YES;
        
    } else {

        topLabel_.frame = CGRectMake(0.0f, 0.0f, width, halfHeight);
        bottomLabel_.frame = CGRectMake(0.0f, halfHeight, width, halfHeight);
        bottomLabel_.hidden = NO;
        
    }
    
}

/**
 * Sets the view frame. This view size is fixed, so the rectangle is updated to maintain that size.
 *
 * @param frame The new view frame.
 */
- (void)setFrame:(CGRect)frame {
    
    [super setFrame:frame];
    [self setNeedsLayout];
    
}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the top label text.
 *
 * @return The top label text.
 */
- (NSString *)topLabelText {
    
    return topLabel_.text;
    
}

/**
 * Sets the top label text and mark the view to layout.
 *
 * @param aText The new top label text.
 */
- (void)setTopLabelText:(NSString *)aText {
    
    topLabel_.text = aText;
    [self setNeedsLayout];
    
}

/*
 * Returns the bottom label text.
 *
 * @return The bottom label text.
 */
- (NSString *)bottomLabelText {
    
    return bottomLabel_.text;
    
}

/**
 * Sets the botom label text and mark the view to layout.
 *
 * @param aText The new bottom label text.
 */
- (void)setBottomLabelText:(NSString *)aText {
    
    bottomLabel_.text = aText;
    [self setNeedsLayout];
    
}

/*
 * Returns the top label color.
 *
 * @return The top label color.
 */
- (UIColor *)topLabelColor {
    
    return topLabel_.textColor;
    
}

/*
 * Sets the top label color.
 *
 * @param topLabelColor The new top label color.
 */
- (void)setTopLabelColor:(UIColor *)topLabelColor {
    
    topLabel_.textColor = topLabelColor;
    
}

/*
 * Returns the bottom label color.
 *
 * @return The bottom label color.
 */
- (UIColor *)bottomLabelColor {
    
    return bottomLabel_.textColor;
    
}

/*
 * Sets the bottom label color.
 *
 * @param bottomLabelColor The new bottom label color.
 */
- (void)setBottomLabelColor:(UIColor *)bottomLabelColor {
    
    bottomLabel_.textColor = bottomLabelColor;
    
}

@end
