/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * Double label view displaying a top label and a bottom label with a samller font.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKDoubleLabel : UIView {
    
@private
    
    /**
     * Top label.
     */
    UILabel *topLabel_;
    
    /**
     * Bottom label.
     */
    UILabel *bottomLabel_;

}

/**
 * Provides read-write access to the top label text.
 */
@property (nonatomic, readwrite, copy) NSString *topLabelText;

/**
 * Provides read-write access to the bottom label text.
 */
@property (nonatomic, readwrite, copy) NSString *bottomLabelText;

/**
 * Provides read-write access to the top label color.
 */
@property (nonatomic, readwrite, copy) UIColor *topLabelColor;

/**
 * Provides read-write access to the bottom label color.
 */
@property (nonatomic, readwrite, copy) UIColor *bottomLabelColor;

@end
