/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * Button to allow the image be located on the right and the title on the left.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKButton : UIButton {
    
@private
    
    /**
     * Image on the right flag. YES when the image is located on the right and the title on the left, NO for the default configuration.
     */
    BOOL imageOnTheRight_;
    
}


/**
 * Porvides read-write access to the image on the right flag.
 */
@property (nonatomic, readwrite, assign) BOOL imageOnTheRight;

@end
