/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKButton.h"


#pragma mark -

@implementation MOKButton

#pragma mark -
#pragma mark Properties

@synthesize imageOnTheRight = imageOnTheRight_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory used by the receiver.
 */
- (void)dealloc {
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes an empty MOKButton instance.
 *
 * @return The initialized MOKButton instance.
 */
- (id)init {
    
    if ((self = [super init])) {
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark UIButton selectors

/**
 * Returns the rectangle in which the receiver draws its title. The image on the right flag is used to determine the frame.
 *
 * @param contentRect The content rectangle for the receiver.
 * @return The rectangle in which the receiver draws its title.
 */
- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    
    CGRect result = [super titleRectForContentRect:contentRect];
    
    if (imageOnTheRight_) {
        
        if (imageOnTheRight_) {
            
            CGFloat width = CGRectGetWidth(contentRect);
            CGFloat heigh = CGRectGetHeight(contentRect);
            
            CGFloat offset = 5.0f;
            
            if (width < 15.0f) {
                
                offset = 0.0f;
                
            }
            
            UIImage *image = [self imageForState:self.state];
            
            if (image != nil) {
                
                CGSize imageSize = image.size;
                CGFloat imageWidth = imageSize.width;
                
                result = CGRectMake(offset,
                                    0.0f,
                                    width - imageWidth - (2.0f * offset),
                                    heigh);
                
            } else {
                
                result = CGRectMake(offset,
                                    0.0f,
                                    width - offset,
                                    heigh);
                
            }
            
        }

    }
    
    return result;
    
}

/**
 * Returns the rectangle in which the receiver draws its image. The image on the right flag is used to determine the frame.
 *
 * @param contentRect The content rectangle for the receiver.
 * @return The rectangle in which the receiver draws its image.
 */
- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    
    CGRect result = [super imageRectForContentRect:contentRect];
    
    if (imageOnTheRight_) {
        
        CGFloat width = CGRectGetWidth(contentRect);
        CGFloat heigh = CGRectGetHeight(contentRect);
        
        CGFloat offset = 5.0f;
        
        if (width < 15.0f) {
            
            offset = 0.0f;
            
        }

        UIImage *image = [self imageForState:self.state];
        
        if (image != nil) {
            
            CGSize imageSize = image.size;
            CGFloat imageWidth = imageSize.width;
            CGFloat imageHeight = imageSize.height;
            
            result = CGRectMake(width - imageWidth - offset,
                                round((heigh - imageHeight) / 2.0f),
                                imageWidth,
                                imageHeight);
            
        } else {
            
            result = CGRectZero;
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the image on the right flag and forces the button to display again.
 *
 * @param imageOnTheRight The new image on the right flag to store.
 */
- (void)setImageOnTheRight:(BOOL)imageOnTheRight {
    
    if (imageOnTheRight_ != imageOnTheRight) {
        
        imageOnTheRight_ = imageOnTheRight;
        
        [self setNeedsDisplay];
        
    }
    
}

@end
