/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "UINavigationGrayBar.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"


#pragma mark -

@implementation UINavigationGrayBar

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated intializer. Initializes a UINavigationGrayWithShadowBar instance with transparent background color.
 *
 * @param frame The view initial frame.
 * @return The initialized UINavigationGrayWithShadowBar instance.
 */
- (id)initWithFrame:(CGRect)frame {
    
    if ((self = [super initWithFrame:frame])) {
        
        self.backgroundColor = [UIColor clearColor];
        self.tintColor = [UIColor clearColor];
        
    }
    
    return self;
    
}

/**
 * Coder intializer. Initializes a UINavigationGrayWithShadowBar instance with transparent background color.
 *
 * @param aDecoder An unarchiver object.
 * @return The initialized UINavigationGrayWithShadowBar instance.
 */
- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if ((self = [super initWithCoder:aDecoder])) {
        
        self.backgroundColor = [UIColor clearColor];
        self.tintColor = [UIColor clearColor];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Drawing selectors

/**
 * Draws the entire image in the specified rectangle, scaling it as needed to fit.
 *
 * @param rect The rectangle (in the coordinate system of the graphics context) in which to draw the image.
 */
- (void)drawRect:(CGRect)rect {
    
    UIImage* bgImage = [[ImagesCache getInstance] imageNamed:IMG_NAVIGATION_BAR_GRAY];
	[bgImage drawInRect:rect];
    
}

@end
