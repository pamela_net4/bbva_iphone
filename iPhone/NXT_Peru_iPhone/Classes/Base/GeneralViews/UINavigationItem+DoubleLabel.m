/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "UINavigationItem+DoubleLabel.h"
#import "MOKDoubleLabel.h"


#pragma mark -

@implementation UINavigationItem(DoubleLabel)

#pragma mark -
#pragma Properties

@dynamic doubleLabelTitle;
@dynamic mainTitle;
@dynamic subtitle;
@dynamic mainTitleColor;
@dynamic subtitleColor;

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the title as a MOKDoubleLabel in case it is a MOKDoubleLabel instance.
 *
 * @return The title as a MOKDoubleLabel.
 */
- (MOKDoubleLabel *)doubleLabelTitle {
    
    MOKDoubleLabel *result = nil;
    
    UIView *titleView = self.titleView;
    
    if ([titleView isKindOfClass:[MOKDoubleLabel class]]) {
        
        result = (MOKDoubleLabel *)titleView;
        
    }
    
    return result;
    
}

/*
 * Returns the main title when the title view is a MOKDoubleLabel.
 *
 * @return The main title when the title view is a MOKDoubleLabel.
 */
- (NSString *)mainTitle {
    
    return self.doubleLabelTitle.topLabelText;
    
}

/*
 * Sets the main title when the title view is a MOKDoubleLabel.
 *
 * @param mainTitle The main title to set.
 */
- (void)setMainTitle:(NSString *)mainTitle {
    
    self.doubleLabelTitle.topLabelText = mainTitle;
    
}

/*
 * Returns the subtitle when the title view is a MOKDoubleLabel.
 *
 * @return The subtitle when the title view is a MOKDoubleLabel.
 */
- (NSString *)subtitle {
    
    return self.doubleLabelTitle.bottomLabelText;
    
}

/*
 * Sets the subtitle when the title view is a MOKDoubleLabel.
 *
 * @param subtitle The subtitle to set.
 */
- (void)setSubtitle:(NSString *)subtitle {
    
    self.doubleLabelTitle.bottomLabelText = subtitle;
    
}

/*
 * Returns the main title color when the title view is a MOKDoubleLabel.
 *
 * @param mainTitleColor The main title color when the title view is aMOKDoubleLabel.
 */
- (UIColor *)mainTitleColor {
    
    return self.doubleLabelTitle.topLabelColor;
    
}

/*
 * Sets the main title color when the title view is a MOKDoubleLabel.
 *
 * @param mainTitleColor The new main title color when the title view is a MOKDoubleLabel.
 */
- (void)setMainTitleColor:(UIColor *)mainTitleColor {
    
    self.doubleLabelTitle.topLabelColor = mainTitleColor;
    
}

/*
 * Returns the subtitle color when the title view is a MOKDoubleLabel.
 *
 * @param subtitleColor The subtitle color when the title view is aMOKDoubleLabel.
 */
- (UIColor *)subtitleColor {
    
    return self.doubleLabelTitle.bottomLabelColor;
    
}

/*
 * Sets the subtitle color when the title view is a MOKDoubleLabel.
 *
 * @param subtitleColor The new subtitle color when the title view is a MOKDoubleLabel.
 */
- (void)setSubtitleColor:(UIColor *)subtitleColor {
    
    self.doubleLabelTitle.bottomLabelColor = subtitleColor;
    
}

@end
