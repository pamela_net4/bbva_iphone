/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


//Forward declarations
@class MOKDoubleLabel;


/**
 * UINavigationItem category to add a property to return the title view as a DoubleLabel in case it is a DoubleLabel instance.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface UINavigationItem(DoubleLabel)

/**
 * Provides read-only access to the title view as a DoubleLabel in case it is a MOKDoubleLabel instance.
 */
@property (nonatomic, readonly, retain) MOKDoubleLabel *doubleLabelTitle;

/**
 * Provides read-write access to the main title string when the title view is a MOKDoubleLabel instance.
 */
@property (nonatomic, readwrite, copy) NSString *mainTitle;

/**
 * Provides read-write access to the subtitle string when the title view is a MOKDoubleLabel instance.
 */
@property (nonatomic, readwrite, copy) NSString *subtitle;

/**
 * Provides read-write access to the main title font color when the title view is a MOKDoubleLabel instance.
 */
@property (nonatomic, readwrite, copy) UIColor *mainTitleColor;

/**
 * Provides read-write access to the subtitle title font color when the title view is a MOKDoubleLabel instance.
 */
@property (nonatomic, readwrite, copy) UIColor *subtitleColor;

@end
