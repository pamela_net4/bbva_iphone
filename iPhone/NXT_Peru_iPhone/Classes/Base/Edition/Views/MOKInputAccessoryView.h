/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


//Forward declarations
@class MOKInputAccessoryView;


/**
 * Pop buttons view delegate to receive notifications when buttons are clicked
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol MOKInputAccessoryViewDelegate

@required

/**
 * Informs the delegate that OK button was tapped.
 *
 * @param inputAccessoryView The imput accessory view triggering the event.
 */
- (void)inputAccessoryViewOKButtonTapped:(MOKInputAccessoryView *)inputAccessoryView;

/**
 * Informs the delegate that auxiliary button button was tapped.
 *
 * @param inputAccessoryView The imput accessory view triggering the event.
 */
- (void)inputAccessoryViewAuxiliaryButtonTapped:(MOKInputAccessoryView *)inputAccessoryView;

/**
 * Informs the delegate that previous responder button was tapped.
 *
 * @param inputAccessoryView The imput accessory view triggering the event.
 */
- (void)inputAccessoryViewPreviousResponderButtonTapped:(MOKInputAccessoryView *)inputAccessoryView;

/**
 * Informs the delegate that next responder button was tapped.
 *
 * @param inputAccessoryView The imput accessory view triggering the event.
 */
- (void)inputAccessoryViewNextResponderButtonTapped:(MOKInputAccessoryView *)inputAccessoryView;

@end


/**
 * View to show a bar with buttons to complement keyboard and picker views
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKInputAccessoryView : UIView {
    
@private
    
    /**
     * Background image view.
     */
    UIImageView *backgroundImageView_;
    
    /**
     * Previous responder button.
     */
    UIButton *previousButton_;
    
    /**
     * Next responder button.
     */
    UIButton *nextButton_;
    
    /**
     * Auxiliary button.
     */
    UIButton *auxiliaryButton_;
    
    /**
     * OK button.
     */
    UIButton *okButton_;
    
    /**
     * Enable button event forwarding flag.
     */
    BOOL enableEventForwarding_;
    
    /**
     * Input accessory view delegate to be informed when buttons are clicked.
     */
    id<MOKInputAccessoryViewDelegate> delegate_;
    
}

/**
 * Provides read-write access to the input accessory view delegate.
 */
@property (nonatomic, readwrite, assign) id<MOKInputAccessoryViewDelegate> delegate;


/**
 * Shows or hides the previous responder and next respoder buttons depending on the provided parameter.
 *
 * @param visible YES to show the previous responder and next responder buttons, NO otherwise.
 */
- (void)previousAndNextResponderButtonsVisible:(BOOL)visible;

/**
 * Enables or disables the previous responder button, depending on the provided parameter.
 *
 * @param enabled YES to enable the previous responder button, NO otherwise.
 */
- (void)previousResponderButtonEnabled:(BOOL)enabled;

/**
 * Enables or disables the next responder button, depending on the provided parameter.
 *
 * @param enable YES to enable the next responder button, NO otherwise.
 */
- (void)nextResponderButtonEnabled:(BOOL)enable;

/**
 * Shows or hides the auxiliary button depending on the provided parameter
 *
 * @param visible YES to show the auxiliary button, NO otherwise
 */
- (void)auxiliaryButtonVisible:(BOOL)visible;

/**
 * Enables or disables the OK button, depending on the provided parameter
 *
 * @param enabled YES to enable the OK button, NO otherwise
 */
- (void)okButtonEnabled:(BOOL)enabled;

/**
 * Sets the previous button title.
 *
 * @param title The previous button title to set.
 */
- (void)setPreviousButtonTitle:(NSString *)title;

/**
 * Sets the next button title.
 *
 * @param title The next button title to set.
 */
- (void)setNextButtonTitle:(NSString *)title;

/**
 * Sets the auxiliary button title.
 *
 * @param title The auxiliary button title to set.
 */
- (void)setAuxiliaryButtonTitle:(NSString *)title;

/**
 * Sets the OK button title.
 *
 * @param title The OK button title to set.
 */
- (void)setOKButtonTitle:(NSString *)title;

/**
 * Enables and disables button events fowarding to delegate
 *
 * @param enabledState YES to enable forwarding, NO otherwise
 */
- (void)enableEventForwarding:(BOOL)enabledState;

@end
