/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKInputAccessoryView.h"
#import "UIColor+BBVA_Colors.h"
#import "NXT_Peru_iPhoneStyler.h"

/**
 * Defines the buttons title size
 */
#define BUTTONS_TITTLE_SIZE                                             14.0f


#pragma mark -

/**
 * MOKInputAccessoryView private extension
 */
@interface MOKInputAccessoryView()

/**
 * Inializes a MOKInputAccessoryView by creating its graphic elements
 *
 * @private
 */
- (void)initializeMOKInputAccessoryView;

/**
 * Invoked by framework when Ok button is tapped. Delegate is notified that OK button was tapped.
 *
 * @private
 */
- (void)okButtonTapped;

/**
 * Invoked by framework when auxiliary button is tapped. Delegate is notified that auxiliary button was tapped.
 *
 * @private
 */
- (void)auxiliaryButtonTapped;

/**
 * Invoked by framework when next responder button is tapped. Delegate is notified that next responder button was tapped.
 *
 * @private
 */
- (void)nextResponderButtonTapped;

/**
 * Invoked by framework when previous responder button is tapped. Delegate is notified that previous responder button was tapped.
 *
 * @private
 */
- (void)previousResponderButtonTapped;

/**
 * Styles a button with the provided background image.
 *
 * @param button The button to style.
 * @param backgroundImage The button background image.
 * @private
 */
- (void)styleButton:(UIButton *)button
     withBackground:(UIImage *)backgroundImage;

@end


#pragma mark -

@implementation MOKInputAccessoryView

#pragma mark -
#pragma mark Properties

@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [backgroundImageView_ release];
    backgroundImageView_ = nil;
    
    [previousButton_ release];
    previousButton_ = nil;
    
    [nextButton_ release];
    nextButton_ = nil;
    
    [auxiliaryButton_ release];
    auxiliaryButton_ = nil;
    
    [okButton_ release];
    okButton_ = nil;
    
    delegate_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Invoked by framework when view awakes from a NIB file. Initializes the graphic elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initializeMOKInputAccessoryView];

}

/**
 * Superclass designated initializer. Initializes a MOKInputAccessoryView instance by creating its graphic elements.
 *
 * @param frame The view initial frame.
 * @return The initialized MOKInputAccesoryView instance.
 */
- (id)initWithFrame:(CGRect)frame {
    
    if ((self = [super initWithFrame:frame])) {
        
        [self initializeMOKInputAccessoryView];
        
    }
    
    return self;
    
}

/*
 * Inializes a MOKInputAccessoryView by creating its graphic elements
 */
- (void)initializeMOKInputAccessoryView {
    
    CGRect frame = CGRectMake(0.0f, 0.0f, 320.0, 45.0f);
    self.frame = frame;
    
    if (backgroundImageView_ == nil) {
        
        backgroundImageView_ = [[UIImageView alloc] initWithFrame:frame];
        UIImage *backgroundImage = [UIImage imageNamed:@"MOKIAViewBackground.png"];
        
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
        backgroundImageView_.image = backgroundImage;
        }
        else
        {
        backgroundImageView_.backgroundColor=[UIColor BBVAGreyToneFiveColor];
        }
        previousButton_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:backgroundImageView_];
        
    }
    
    if (previousButton_ == nil) {
        
        previousButton_ = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        previousButton_.frame = CGRectMake(10.0f, 7.0f, 75.0f, 29.0f);


        UIImage *backgroundImage = [UIImage imageNamed:@"MOKIAPreviousButton.png"];
        
        if ([backgroundImage respondsToSelector:@selector(resizableImageWithCapInsets:)]) {
            
            backgroundImage = [backgroundImage resizableImageWithCapInsets:UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 6.0f)];
            
        } else {
            
            backgroundImage = [backgroundImage stretchableImageWithLeftCapWidth:5.0f
                                                                   topCapHeight:5.0f];
            
        }
        
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {

        [self styleButton:previousButton_
           withBackground:backgroundImage];
        } else {
            previousButton_.layer.borderWidth=1.0f;
            previousButton_.layer.borderColor=[UIColor BBVABlueSpectrumColor].CGColor;
            previousButton_.layer.cornerRadius=2.5f;
            [NXT_Peru_iPhoneStyler styleButton:previousButton_ withBoldFont:NO fontSize:BUTTONS_TITTLE_SIZE color:[UIColor BBVABlueSpectrumColor] disabledColor:[UIColor BBVAGreyToneThreeColor] andShadowColor:[UIColor colorWithWhite:0.5f alpha:0.3f]];
        }
        
        previousButton_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        [previousButton_ addTarget:self
                            action:@selector(previousResponderButtonTapped)
                  forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:previousButton_];
        
    }
    
    if (nextButton_ == nil) {
        
        nextButton_ = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        nextButton_.frame = CGRectMake(84.0f, 7.0f, 75.0f, 29.0f);
        
        UIImage *backgroundImage = [UIImage imageNamed:@"MOKIANextButton.png"];
        
        if ([backgroundImage respondsToSelector:@selector(resizableImageWithCapInsets:)]) {
            
            backgroundImage = [backgroundImage resizableImageWithCapInsets:UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 6.0f)];
            
        } else {
            
            backgroundImage = [backgroundImage stretchableImageWithLeftCapWidth:5.0f
                                                                   topCapHeight:5.0f];
            
        }
        
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            
            [self styleButton:nextButton_
               withBackground:backgroundImage];
        } else {
            [NXT_Peru_iPhoneStyler styleButton:nextButton_ withBoldFont:NO fontSize:BUTTONS_TITTLE_SIZE color:[UIColor BBVABlueSpectrumColor] disabledColor:[UIColor BBVAGreyToneThreeColor] andShadowColor:[UIColor colorWithWhite:0.5f alpha:0.3f]];
            nextButton_.layer.borderWidth=1.0f;
            nextButton_.layer.borderColor=[UIColor BBVABlueSpectrumColor].CGColor;
            nextButton_.layer.cornerRadius=2.5f;
        }
        nextButton_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        [nextButton_ addTarget:self
                        action:@selector(nextResponderButtonTapped)
              forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:nextButton_];
        
    }
    
    if (auxiliaryButton_ == nil) {
        
        auxiliaryButton_ = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        auxiliaryButton_.frame = CGRectMake(180.0f, 7.0f, 70.0f, 29.0f);
        UIImage *backgroundImage = [UIImage imageNamed:@"MOKIAAuxiliaryButton.png"];
        
        if ([backgroundImage respondsToSelector:@selector(resizableImageWithCapInsets:)]) {
            
            backgroundImage = [backgroundImage resizableImageWithCapInsets:UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 6.0f)];
            
        } else {
            
            backgroundImage = [backgroundImage stretchableImageWithLeftCapWidth:5.0f
                                                                   topCapHeight:5.0f];
            
        }
        
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            
            [self styleButton:auxiliaryButton_
               withBackground:backgroundImage];
        } else {
            [NXT_Peru_iPhoneStyler styleButton:auxiliaryButton_ withBoldFont:NO fontSize:BUTTONS_TITTLE_SIZE color:[UIColor BBVABlueSpectrumColor] disabledColor:[UIColor BBVAGreyToneThreeColor] andShadowColor:[UIColor colorWithWhite:0.5f alpha:0.3f]];
        }
        
        
        auxiliaryButton_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
        [auxiliaryButton_ addTarget:self
                             action:@selector(auxiliaryButtonTapped)
                   forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:auxiliaryButton_];
        
    }
    
    if (okButton_ == nil) {
        
        okButton_ = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        okButton_.frame = CGRectMake(270.0f, 7.0f, 40.0f, 29.0f);
        UIImage *backgroundImage = [UIImage imageNamed:@"MOKIAOKButton.png"];
        
        if ([backgroundImage respondsToSelector:@selector(resizableImageWithCapInsets:)]) {
            
            backgroundImage = [backgroundImage resizableImageWithCapInsets:UIEdgeInsetsMake(5.0f, 6.0f, 5.0f, 6.0f)];
            
        } else {
            
            backgroundImage = [backgroundImage stretchableImageWithLeftCapWidth:5.0f
                                                                   topCapHeight:5.0f];
            
        }
        
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            
            [self styleButton:okButton_ withBackground:backgroundImage];
        } else {
            [NXT_Peru_iPhoneStyler styleButton:okButton_ withBoldFont:YES fontSize:BUTTONS_TITTLE_SIZE color:[UIColor BBVABlueSpectrumColor] disabledColor:[UIColor BBVAGreyToneThreeColor] andShadowColor:[UIColor colorWithWhite:0.5f alpha:0.3f]];
        }
        okButton_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
        [okButton_ addTarget:self
                      action:@selector(okButtonTapped)
            forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:okButton_];
        
    }
    
    enableEventForwarding_ = YES;
    
}

#pragma mark -
#pragma mark User interacion

/*
 * Invoked by framework when Ok button is tapped. Delegate is notified that OK button was tapped.
 */
- (void)okButtonTapped {
    
    if (enableEventForwarding_) {
        
        [delegate_ inputAccessoryViewOKButtonTapped:self];
        
    }
    
}

/*
 * Invoked by framework when auxiliary button is tapped. Delegate is notified that auxiliary button was tapped.
 */
- (void)auxiliaryButtonTapped {
    
    if (enableEventForwarding_) {
        
        [delegate_ inputAccessoryViewAuxiliaryButtonTapped:self];
        
    }
    
}

/*
 * Invoked by framework when next responder button is tapped. Delegate is notified that next responder button was tapped.
 */
- (void)nextResponderButtonTapped {
    
    if (enableEventForwarding_) {
        
        [delegate_ inputAccessoryViewNextResponderButtonTapped:self];
        
    }
    
}

/*
 * Invoked by framework when previous responder button is tapped. Delegate is notified that previous responder button was tapped.
 */
- (void)previousResponderButtonTapped {
    
    if (enableEventForwarding_) {
        
        [delegate_ inputAccessoryViewPreviousResponderButtonTapped:self];
        
    }
    
}

#pragma mark -
#pragma mark Buttons styling

/*
 * Styles a button with the provided background image.
 */
- (void)styleButton:(UIButton *)button
     withBackground:(UIImage *)backgroundImage {
    
    UIFont *font = [UIFont systemFontOfSize:BUTTONS_TITTLE_SIZE];
    
    UILabel *titleLabel = [button titleLabel];
    titleLabel.font = font;
    titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    
    [button setTitleColor:[UIColor whiteColor]
                 forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor]
                 forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor grayColor]
                 forState:UIControlStateDisabled];
    
    [button setTitleShadowColor:[UIColor colorWithWhite:0.5f alpha:0.3f]
                       forState:UIControlStateNormal];
    
    button.showsTouchWhenHighlighted = NO;
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        [button setBackgroundImage:backgroundImage
                          forState:UIControlStateNormal];
        
    }else {
        
        [button setBackgroundColor:[UIColor blackColor]];
    }

    

}

#pragma mark -
#pragma mark Buttons configuring

/*
 * Shows or hides the previous responder and next respoder buttons depending on the provided parameter.
 */
- (void)previousAndNextResponderButtonsVisible:(BOOL)visible {
    
    previousButton_.hidden = !visible;
    nextButton_.hidden = !visible;

}

/*
 * Enables or disables the previous responder button, depending on the provided parameter.
 */
- (void)previousResponderButtonEnabled:(BOOL)enabled {
    
    previousButton_.enabled = enabled;

}

/*
 * Enables or disables the next responder button, depending on the provided parameter.
 */
- (void)nextResponderButtonEnabled:(BOOL)enable {
    
    nextButton_.enabled = enable;
    
}

/*
 * Shows or hides the auxiliary button depending on the provided parameter
 */
- (void)auxiliaryButtonVisible:(BOOL)visible {
    
    auxiliaryButton_.hidden = !visible;
    
}

/*
 * Enables or disables the OK button, depending on the provided parameter
 */
- (void)okButtonEnabled:(BOOL)enabled {
    
    okButton_.enabled = enabled;
    
}

/*
 * Sets the previous button title.
 */
- (void)setPreviousButtonTitle:(NSString *)title {
    
    [previousButton_ setTitle:title
                     forState:UIControlStateNormal];

}

/*
 * Sets the next button title.
 */
- (void)setNextButtonTitle:(NSString *)title {
    
    [nextButton_ setTitle:title
                 forState:UIControlStateNormal];

}

/*
 * Sets the auxiliary button title.
 */
- (void)setAuxiliaryButtonTitle:(NSString *)title {
    
    [auxiliaryButton_ setTitle:title
                      forState:UIControlStateNormal];
    
}

/*
 * Sets the OK button title.
 */
- (void)setOKButtonTitle:(NSString *)title {
    
    [okButton_ setTitle:title
               forState:UIControlStateNormal];
    
}

#pragma mark -
#pragma mark Event forwarding management

/*
 * Enables and disables button events fowarding to delegate
 */
- (void)enableEventForwarding:(BOOL)anEnabledState {
    
    enableEventForwarding_ = anEnabledState;
    
}

@end
