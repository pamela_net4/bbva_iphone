/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * Category to compare two views taking into account their frames. Ordering is made top to bottom and left to right.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface UIView(GraphicOrdering)

/**
 * Compares two views taking into account their frames. View located on top is ordered firs, and when
 * they are at the same top position, view on the left is ordered first
 *
 * @param otherView The other view to compare to.
 * @return NSOrderedAscending when receiving view is on top of other view, or being at the same top position, is left of
 * other view, NSOrderedSame when they are both at the same top left position, NSOrderedDescending otherwise.
 */
- (NSComparisonResult)compareFirstTopLeft:(NSObject *)otherView;

@end
