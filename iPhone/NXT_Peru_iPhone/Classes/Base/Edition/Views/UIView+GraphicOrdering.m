/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "UIView+GraphicOrdering.h"


#pragma mark -

@implementation UIView(GraphicOrdering)

#pragma mark -
#pragma mark Ordering operations

/*
 * Compares two views taking into account their frames. View located on top is ordered firs, and when
 * they are at the same top position, view on the left is ordered first
 */
- (NSComparisonResult)compareFirstTopLeft:(NSObject *)otherView {
    
    NSComparisonResult result = NSOrderedAscending;
    
    if ([otherView isKindOfClass:[UIView class]]) {
        
        UIView *view = (UIView *)otherView;
        
        CGPoint currentOrigin = self.frame.origin;
        CGPoint otherOrigin = view.frame.origin;
        
        result = NSOrderedDescending;
        
        if (currentOrigin.y < otherOrigin.y) {
            
            result = NSOrderedAscending;
            
        } else if (currentOrigin.y == otherOrigin.y) {
            
            if (currentOrigin.x < otherOrigin.x) {
                
                result = NSOrderedAscending;
                
            } else if (currentOrigin.x == otherOrigin.x) {
                
                result = NSOrderedSame;
                
            }
            
        }
        
    }
    
    return result;
    
}

@end
