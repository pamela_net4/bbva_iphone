/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


/**
 * Enumerates the available input view types
 */
typedef enum {
    
    mevivtUnknown = 0, //!<Editing is performed using an unknown view, or is undefined
    mevivtKeyboard, //!<Editing is performed using the keyboard
    mevivtStringPicker, //!<Editing is performed using the string picker
    mevivtDatePicker //!<Editing is performed using the date picker
    
} MOKEditViewInputViewType;


//Forward declarations
@protocol MOKEditView;


/**
 * Protocol implemented by the edit views manager. It will receive the events from the edit views in order to manage them.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol MOKEditViewsManager

@required

/**
 * The edit view notifies it is about to become first responder.
 *
 * @param editView The edit view that is about to become first responder.
 */
- (void)editViewWillBecomeFirstResponder:(id<MOKEditView>)editView;

/**
 * The edit view notifies it has resigned first responder status.
 *
 * @param editView The edit view that resigned first responder status.
 */
- (void)editViewDidResignFirstResponder:(id<MOKEditView>)editView;

@end


/**
 * Protocol implemented by views that can be edited. It allows managing the previous and next views.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol MOKEditView <NSObject>

@required

/**
 * Returns the previous edit view in the chain.
 *
 * @return The previous edit view in the chain.
 */
- (id<MOKEditView>)previousEditView;

/**
 * Sets the previous edit view in the chain.
 *
 * @param previousEditView The new previous edit view in the chain to set.
 */
- (void)setPreviousEditView:(id<MOKEditView>)previousEditView;

/**
 * Returns the next edit view in the chain.
 *
 * @return The next edit view in the chain.
 */
- (id<MOKEditView>)nextEditView;

/**
 * Sets the next edit view in the chain.
 *
 * @param nextEditView The new next edit view in the chain to set.
 */
- (void)setNextEditView:(id<MOKEditView>)nextEditView;

/**
 * Returns the edit view manager.
 *
 * @return The edit view manager.
 */
- (id<MOKEditViewsManager>)editViewManager;

/**
 * Sets the edit view manager.
 *
 * @param editViewManager The new edit view manager to set.
 */
- (void)setEditViewManager:(id<MOKEditViewsManager>)editViewManager;


/**
 * Returns the input view.
 *
 * @return The input view.
 */
- (UIView *)inputView;

/**
 * Sets the input view.
 *
 * @param inputView The new input view to set.
 */
- (void)setInputView:(UIView *)inputView;

/**
 * Returns the input accessory view.
 *
 * @return The input accessory view.
 */
- (UIView *)inputAccessoryView;

/**
 * Sets the input accessory view.
 *
 * @param inputAccessoryView The new input accessory view to set.
 */
- (void)setInputAccessoryView:(UIView *)inputAccessoryView;


/**
 * Returns the view frame.
 *
 * @return The view frame.
 */
- (CGRect)frame;

/**
 * Returns the view superview.
 *
 * @return The view superview.
 */
- (UIView *)superview;

/**
 * The edit view is made to resign first responder status.
 *
 * @return YES when the edit view effectively resigns first responder status, NO otherwise.
 */
- (BOOL)resignFirstResponder;

/**
 * Returns the input view type.
 *
 * @return The input view type.
 */
- (MOKEditViewInputViewType)inputViewType;


@optional

/**
 * Returns the selected date. This selector is required when the edit view needs a date picker.
 *
 * @return The selected date.
 */
- (NSDate *)selectedDate;

/**
 * Sets the selected date. This selector is requiered when the edit view needs a date picker.
 *
 * @param selectedDate The new selected date to set.
 */
- (void)setSelectedDate:(NSDate *)selectedDate;

/**
 * Returns the minimum date. This selector is requiered when the edit view needas a date picker.
 *
 * @return The minimum date.
 */
- (NSDate *)minimumDate;

/**
 * Returns the maximum date. This selector is requiered when the edit view needas a date picker.
 *
 * @return The maximum date.
 */
- (NSDate *)maximumDate;


/**
 * Returns the string list array to select from. This selector is requiered when the edit view needas a string picker.
 *
 * @return The string list array to select from.
 */
- (NSArray *)optionStringList;

/**
 * Returns the selected index. This selector is requiered when the edit view needas a string picker.
 *
 * @return The selected index.
 */
- (NSInteger)selectedIndex;

/**
 * Sets the selected index. To unselect the selection, index must be out of bounds from option string list. This selector is requiered
 * when the edit view needas a string picker.
 *
 * @param selectedIndex The new selected index to set.
 */
- (void)setSelectedIndex:(NSInteger)selectedIndex;

@end
