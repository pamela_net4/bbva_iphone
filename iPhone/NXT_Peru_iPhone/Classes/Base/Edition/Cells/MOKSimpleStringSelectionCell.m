/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKSimpleStringSelectionCell.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"


/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"MOKSimpleStringSelectionCell"

/**
 * Define the cell identifier
 */
#define CELL_IDENTIFIER                                             @"MOKSimpleStringSelectionCell"

/**
 * Define the cell height
 */
#define CELL_HEIGHT                                                 44.0f


#pragma mark -

/**
 * MOKSimpleStringSelectionCell private extension.
 */
@interface MOKSimpleStringSelectionCell()

/**
 * Checks the cell styling depending on its selection state.
 *
 * @private
 */
- (void)checkCellStylingForSelectionState;

@end


#pragma mark -

@implementation MOKSimpleStringSelectionCell

#pragma mark -
#pragma mark Properties

@synthesize checkImageView = checkImageView_;
@synthesize stringLabel = stringLabel_;
@synthesize separatorImageView = separatorImageView_;
@dynamic cellString;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory used by the receiver.
 */
- (void)dealloc {
    
    [checkImageView_ release];
    checkImageView_ = nil;
    
    [stringLabel_ release];
    stringLabel_ = nil;
    
    [separatorImageView_ release];
    separatorImageView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles the graphic elements.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    separatorImageView_.image = [imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    [NXT_Peru_iPhoneStyler styleLabel:stringLabel_
                         withFontSize:14.0f color:[UIColor BBVACoolGreyColor]];
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}

/*
 * Creates and returns an autoreleased MOKSimpleStringSelectionCell constructed from a NIB file.
 */
+ (MOKSimpleStringSelectionCell *)MOKSimpleStringSelectionCell {
    
    MOKSimpleStringSelectionCell *result = (MOKSimpleStringSelectionCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    return result;
    
}

#pragma mark -
#pragma mark Cell definition selectors

/*
 * Returns the cell height.
 */
+ (CGFloat)cellHeight {
    
    return CELL_HEIGHT;
    
}

/*
 * Returns the cell identifier.
 */
+ (NSString *)cellIdentifier {
    
    return CELL_IDENTIFIER;
    
}

#pragma mark -
#pragma mark Cell styling

/**
 * Checks the cell styling depending on its selection state.
 *
 * @private
 */
- (void)checkCellStylingForSelectionState {
    
    if (self.selected) {
        
        checkImageView_.image = [[ImagesCache getInstance] imageNamed:IMG_SELECTED_RADIO];
        
    } else {
        
        checkImageView_.image = [[ImagesCache getInstance] imageNamed:IMG_UNSELECTED_RADIO];
        
    }
    
}

#pragma mark -
#pragma mark UITableViewCell selectors

/**
 * Sets the new selected state. The cell style is checked.
 *
 * @param selected  Contains the value YES if the view should display itself as selected.
 */
- (void)setSelected:(BOOL)selected {
    
    [super setSelected:selected];
    [self checkCellStylingForSelectionState];
    
}

/**
 * Sets the selection state of the cell view. The cell style is checked.
 *
 * @param selected  Contains the value YES if the view should display itself as selected.
 * @param animated Set to YES if the change in selection state is animated.
 */
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected
              animated:animated];
    [self checkCellStylingForSelectionState];
    
}

#pragma mark -
#pragma mark Properties selectors

/**
 * Returns the cell string.
 *
 * @return The cell string.
 */
- (NSString *)cellString {
    
    return stringLabel_.text;
}

/**
 * Sets the new cell string.
 *
 * @param cellString The new cell string to set.
 */
- (void)setCellString:(NSString *)cellString {
    
    stringLabel_.text = cellString;
    
}

@end
