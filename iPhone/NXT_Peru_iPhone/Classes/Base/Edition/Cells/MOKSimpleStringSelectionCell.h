/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * MOKSimpleStringSelectionCell provides a cell to select a string from a list selection control when displayed in a table (iPad only).
 * It displays a check and a label.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKSimpleStringSelectionCell : UITableViewCell {
    
@private
    
    /**
     * Check image view.
     */
    UIImageView *checkImageView_;
    
    /**
     * String label.
     */
    UILabel *stringLabel_;
    
    /**
     * Separator image view.
     */
    UIImageView *separatorImageView_;
    
}


/**
 * Provides read-write access to the check image view and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *checkImageView;

/**
 * Provides read-write access to the string label and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *stringLabel;

/**
 * Provides read-write access to the separator image view and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separatorImageView;

/**
 * Provides read-write access to the cell string.
 */
@property (nonatomic, readwrite, copy) NSString *cellString;


/**
 * Creates and returns an autoreleased MOKSimpleStringSelectionCell constructed from a NIB file.
 *
 * @return The autoreleased MOKSimpleStringSelectionCell constructed from a NIB file.
 */
+ (MOKSimpleStringSelectionCell *)MOKSimpleStringSelectionCell;

/**
 * Returns the cell height.
 *
 * @return The cell height.
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier.
 *
 * @return The cell identifier.
 */
+ (NSString *)cellIdentifier;

@end
