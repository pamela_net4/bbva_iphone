/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKEditableStringTableSelectionViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"


/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"MOKEditableStringTableSelectionViewController"


#pragma mark -

/**
 * MOKEditableStringTableSelectionViewController private extension.
 */
@interface MOKEditableStringTableSelectionViewController()

/**
 * Releases the graphic elements not needed when view is hidden.
 *
 * @private
 */
- (void)releaseMOKEditableStringTableSelectionViewControllerGraphicElements;

/**
 * Updates the navigation bar information.
 *
 * @private
 */
- (void)updateNavigationBarInformation;

@end


#pragma mark -

@implementation MOKEditableStringTableSelectionViewController

#pragma mark -
#pragma mark Properties

@synthesize topNavigationBar = topNavigationBar_;
@synthesize tableBackgroundView = tableBackgroundView_;
@synthesize optionsTableView = optionsTableView_;
@synthesize topNavigationBarTitle = topNavigationBarTitle_;
@synthesize editableStringTableSelectionViewControllerDelegate = editableStringTableSelectionViewControllerDelegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseMOKEditableStringTableSelectionViewControllerGraphicElements];
    
    [topNavigationBarTitle_ release];
    topNavigationBarTitle_ = nil;
    
    editableStringTableSelectionViewControllerDelegate_ = nil;
    
    [super dealloc];
    
}

/*
 * Releases the graphic elements not needed when view is hidden.
 */
- (void)releaseMOKEditableStringTableSelectionViewControllerGraphicElements {
    
    [topNavigationBar_ release];
    topNavigationBar_ = nil;
    
    [tableBackgroundView_ release];
    tableBackgroundView_ = nil;
    
    [optionsTableView_ release];
    optionsTableView_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
}

/*
 * Creates and returns an autoreleased MOKEditableStringTableSelectionViewController constructed from a NIB file.
 */
+ (MOKEditableStringTableSelectionViewController *)MOKEditableStringTableSelectionViewController {
    
    MOKEditableStringTableSelectionViewController *result =  [[[MOKEditableStringTableSelectionViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory. Graphic elements are styled.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
	
    tableBackgroundView_.backgroundColor = [UIColor BBVAGreyToneFiveColor];
    self.view.backgroundColor = [UIColor BBVACoolGreyColor];
    
    [[tableBackgroundView_ layer] setCornerRadius:10.0f];
    
    optionsTableView_.backgroundColor = [UIColor clearColor];
    optionsTableView_.rowHeight = cellHeight_;

}

/**
 * Called when the controller’s view is released from memory. Graphic elements are released.
 */
- (void)viewDidUnload {
    
    [self releaseMOKEditableStringTableSelectionViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Notifies the view controller that its view is about to be added to a view hierarchy. Updates the navigation bar information.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self updateNavigationBarInformation];
    [self reloadTableContent];
    
}

#pragma mark -
#pragma mark Table view management

/*
 * Reloads the table view content.
 */
- (void)reloadTableContent {
    
    if (optionsTableView_ != nil) {
        
        [optionsTableView_ reloadData];
        [optionsTableView_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f)
                                      animated:NO];
        
        CGSize contentSize = optionsTableView_.contentSize;
        
        if (contentSize.height <= optionsTableView_.frame.size.height) {
            
            optionsTableView_.scrollEnabled = NO;
            
        } else {
            
            optionsTableView_.scrollEnabled = YES;
            
        }

        NSInteger selectedRow = -1;

        if (editableStringTableSelectionViewControllerDelegate_ != nil) {
            
            selectedRow = [editableStringTableSelectionViewControllerDelegate_ editableStringTableSelectionViewControllerSelectedRow:self];
            
        }
        
        if (selectedRow >= 0) {
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedRow
                                                        inSection:0];
            
            [optionsTableView_ selectRowAtIndexPath:indexPath
                                           animated:NO
                                     scrollPosition:UITableViewScrollPositionNone];
            
        } else {
            
            NSIndexPath *selectedIndexPath = [optionsTableView_ indexPathForSelectedRow];
            
            if (selectedIndexPath != nil) {
                
                [optionsTableView_ deselectRowAtIndexPath:selectedIndexPath
                                                 animated:NO];
                
            }
            
        }
        
    }
    
}

#pragma mark -
#pragma mark Utility selectors

/**
 * Updates the navigation bar information.
 *
 * @private
 */
- (void)updateNavigationBarInformation {
    
    UINavigationItem *navigationItem = [self navigationItem];
    
    if (topNavigationBar_ != nil) {
        
        [topNavigationBar_ setItems:[NSArray arrayWithObjects:navigationItem,
                                     nil]];
        
    }
    
}

#pragma mark -
#pragma mark Information management

/*
 * Sets the table view cell height.
 */
- (void)setTableViewCellHeight:(CGFloat)cellHeight {

    cellHeight_ = cellHeight;
    
    if (optionsTableView_ != nil) {
        
        optionsTableView_.rowHeight = cellHeight_;
        
        [self reloadTableContent];
        
    }
    
}

#pragma mark -
#pragma mark UITableViewDataSource protocol selectors

/**
 * Tells the data source to return the number of rows in a given section of a table view. The delegate is requested about the number of cells.
 *
 * @param tableView The table-view object requesting this information.
 * @param section An index number identifying a section in tableView.
 */
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    
    NSInteger result = 0;
    
    if (editableStringTableSelectionViewControllerDelegate_ != nil) {
        
        result = [editableStringTableSelectionViewControllerDelegate_ editableStringTableSelectionViewControllerNumberOfRows:self];
        
    }
    
    return result;
    
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. The delegate is requested to return the correct cell to display the information.
 *
 * @param tableView A table-view object requesting the cell.
 * @param indexPath  An index path locating a row in tableView.
 * @return An object inheriting from UITableViewCellthat the table view can use for the specified row.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *result = nil;
    
    if (editableStringTableSelectionViewControllerDelegate_ != nil) {
        
        result = [editableStringTableSelectionViewControllerDelegate_ editableStringTableSelectionViewController:self
                                                                                                       tableView:tableView
                                                                                                 cellAtIndexPath:indexPath];
        
    }
    
    return  result;
    
}

#pragma mark -
#pragma mark UITableViewDelegate protocol selectors

/**
 * Tells the delegate that the specified row is now selected. The delegate is notified about the event.
 *
 * @param tableView A table-view object informing the delegate about the new row selection.
 * @param indexPath An index path locating the new selected row in tableView.
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editableStringTableSelectionViewControllerDelegate_ != nil) {
        
        [editableStringTableSelectionViewControllerDelegate_ editableStringTableSelectionViewController:self
                                                                                selectedCellAtIndexPath:indexPath];
        
    }
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Return the navigation item used to represent the view controller. Sets the view title.
 *
 * @return The navigation item used to represent the view controller.
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    result.mainTitle = topNavigationBarTitle_;
    result.mainTitleColor = [UIColor BBVAGreyToneOneColor];
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the top navigation bar title and update the navigation bar.
 *
 * @param topNavigationBarTitle The new top navigation bar title to set.
 */
- (void)setTopNavigationBarTitle:(NSString *)topNavigationBarTitle {
    
    if (![topNavigationBarTitle isEqualToString:topNavigationBarTitle_]) {
        
        [topNavigationBarTitle_ release];
        topNavigationBarTitle_ = nil;
        topNavigationBarTitle_ = [topNavigationBarTitle copy];
        
        [self updateNavigationBarInformation];
        
    }
    
}

@end
