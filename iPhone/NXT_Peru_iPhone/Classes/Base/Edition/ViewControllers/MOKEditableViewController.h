/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKViewController.h"

#import "MOKEditableStringTableSelectionViewController.h"
#import "MOKEditViewProtocols.h"
#import "MOKInputAccessoryView.h"


/**
 * Editable view controller. Base view controller for all application editable view controllers. Implements
 * basic behaviour common to all editable views.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKEditableViewController : MOKViewController <MOKEditViewsManager, MOKInputAccessoryViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIPopoverControllerDelegate, MOKEditableStringTableSelectionViewControllerDelegate> {
    
@private
    
    /**
     * Transparent scroll to provide scrolling capabilities.
     */
    UIScrollView *transparentScroll_;
    
    /**
     * Scrollable view. This view is inserted into the transparentScroll_ and contains all editable views that can be scrolled.
     */
    UIView *scrollableView_;
    
    /**
     * Picker to display options.
     */
    UIPickerView *pickerView_;
    
    /**
     * Date picker to select a date.
     */
    UIDatePicker *datePicker_;
    
    /**
     * Input accessory view
     */
    MOKInputAccessoryView *inputAccessoryView_;
    
    /**
     * View currently being edited.
     */
    id<MOKEditView> editedView_;
    
    /**
     * Old edited view needed to hide input views
     */
    id<MOKEditView> oldEditedView_;
    
    /**
     * Auxiliary view controller to display a picker in a popover controller (for iPad only)
     */
    UIViewController *pickerViewController_;
    
    /**
     * Table view controller to display a selection table in a popover controller (for iPad only)
     */
    MOKEditableStringTableSelectionViewController *tableViewController_;
    
    /**
     * Popover controller to display the picker selector (for iPad only)
     */
    UIPopoverController *pickerPopoverController_;
    
    /**
     * Registered to keyboard events flag. YES when the view controller is registered to keyboard events, NO otherwise.
     */
    BOOL registeredToKeyboardEvents_;
    
    /**
     * Displaying popover flag. YES when the view controller is displaying the popover with the picker selectors, NO otherwise.
     */
    BOOL displayingPopover_;
    
    /**
     * Current text from text view.
     */
    NSString *currentTextViewText_;
    
    /**
     * Scroll nominal frame.
     */
    CGRect scrollNominalFrame_;
    
    /**
     * Flag that indicates that the keyboard is showing.
     */
    BOOL isKeyboardShowing_;
    
    /**
     * Editing view height. The height of current editing view, or 0.0f when no editing view is visible.
     */
    CGFloat editingViewHeight_;
    
    /**
     * Picker strings array. All elements inside the array are NSString instances.
     */
    NSMutableArray *pickerStrings_;
    
    /**
     * Old iOS version flag. YES when the application runs on an iOS version older than 3.2, NO otherwise.
     */
    BOOL oldIOSVersion_;
    
    /**
     * Array containing the input views that can be removed from the superview.
     */
    NSMutableArray *removableInputViewsArray_;
    
    /**
     * Array containing arrays with views to remove from a given operation.
     */
    NSMutableArray *removeViewsArrays_;
    
    /**
     * Keyboard final frame. Needed for pre iOS 3.2 workaround with input accessory views.
     */
    CGRect keyboardFinalFrame_;
    
}


/**
 * Provides read-write access to the scrolable view and exports if for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *scrollableView;


/**
 * Resets the scroll position to the top left
 *
 * @param animated YES to animate the scroll re-position, NO to re-position without animation
 */
- (void)resetScrollToTopLeftAnimated:(BOOL)animated;

/**
 * Determines the edit views available and sets its order.
 */
- (void)lookForEditViews;

/**
 * Registers to keyboard events.
 */
- (void)registerToKeyboardEvents;

/**
 * Unregisters from keyboard events.
 */
- (void)unregisterFromKeyboardEvents;

@end
