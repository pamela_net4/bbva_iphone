/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKEditableViewController+protected.h"

#import "MOKSimpleStringSelectionCell.h"


#pragma mark -

@implementation MOKEditableViewController(protected)

#pragma mark -
#pragma mark Configuration management

/*
 * Determines whether the input accessory view must be displayed. When no accessory view is diplayed, a Done button
 * is displayed at the navigaiton bar. Default implementation returns YES.
 */
- (BOOL)mustDisplayAccessoryView {
    
    return YES;
    
}

/*
 * Determines whether the list editor displayed in a popover (iPad only) must be a picker view or a table view controller. Default implementation displays a picker view.
 */
- (BOOL)popoverStringListEditorDisplaysTableViewForEditView:(id<MOKEditView>)editView {
    
    return NO;
    
}

/*
 * When the string list selection must bhe performed inside a table view (iPad only), subclasses must override this selector to provide the table view header title.
 * Default implementation returns an empty string.
 */
- (NSString *)stringListSelectionTableHeaderForEditView:(id<MOKEditView>)editView {
    
    return @"";
    
}

/*
 * When string list selection must be performed inside a table view (iPad only), subclasses must override this selector to provide the cell height (all
 * cells inside the table must have the same height). Default implementation returns the simple string cell height.
 */
- (CGFloat)stringListSelectionTableCellHeightForEditView:(id<MOKEditView>)editView {
    
    return [MOKSimpleStringSelectionCell cellHeight];
    
}

/*
 * When the string list selection must be performed inside a table view (iPad only), subclasses must override this selector to provide the cell for the provided index path.
 * Default implementation returns a nil cell.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
                   forEditView:(id<MOKEditView>)editView {
    
    return nil;
    
}

- (void)labelForStringListSelectionButton:(UIButton *)button stringForList:(NSString *)string forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    
    
}

/*
 * Sets the scrollable view height.
 */
- (void)setScrollableViewHeight:(CGFloat)height {
    
    if (scrollableView_ != nil) {
        
        CGRect scrollableViewFrame = scrollableView_.frame;
        scrollableViewFrame.size.height = height;
        scrollableView_.frame = scrollableViewFrame;
        
        [transparentScroll_ setContentSize:scrollableViewFrame.size];
        
    }
    
}

/*
 * Sets the scroll content size.
 */
- (void)setScrollContentSize:(CGSize)contentSize {
    
    if (scrollableView_ != nil) {
        
        CGRect scrollableViewFrame = scrollableView_.frame;
        scrollableViewFrame.size = contentSize;
        scrollableView_.frame = scrollableViewFrame;
        
        [transparentScroll_ setContentSize:contentSize];
        
    }
    
}

/*
 * Sets the scroll frame.
 */
- (void)setScrollFrame:(CGRect)frame {
    
    if (transparentScroll_ != nil) {
        
        scrollNominalFrame_ = frame;
        [self adjustScrollToNominalFrameAndInputView];
        
    }
    
}

/*
 * Set the scroll to fill its parent view.
 */
- (void)scrollFillsParent {
    
    UIView *superview = transparentScroll_.superview;
    
    if (superview != nil) {
        
        CGRect scrollFrame = superview.frame;
        scrollFrame.origin.x = 0.0f;
        scrollFrame.origin.y = 0.0f;
        scrollNominalFrame_ = scrollFrame;
        [self adjustScrollToNominalFrameAndInputView];
        
    }
    
}

/*
 * Adjust the scroll to the nominal frame and a possible input view.
 */
- (void)adjustScrollToNominalFrameAndInputView {
    
    UIView *view = transparentScroll_.superview;

    if (editingViewHeight_ != 0.0f) {
        
        UIWindow *window = view.window;
        
        CGRect keyboardFrame = [window convertRect:keyboardFinalFrame_
                                            toView:view];
        CGPoint topPoint = keyboardFrame.origin;
        editingViewHeight_ = CGRectGetHeight(view.frame) - topPoint.y;
        
    }

    CGFloat bottom = CGRectGetHeight(view.frame) - editingViewHeight_;
    
    CGRect frame = scrollNominalFrame_;
    CGFloat height = bottom - CGRectGetMinY(frame);
    
    if (height < frame.size.height) {
        
        frame.size.height = height;
        
    }
    
    transparentScroll_.frame = frame;

}

@end
