    /*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKEditableViewController.h"

#import "MOKEditableViewController+protected.h"
#import "MOKSimpleStringSelectionCell.h"
#import "StringKeys.h"
#import "UIView+GraphicOrdering.h"
#import "UIColor+BBVA_Colors.h"


/**
 * Defines the requred version of the iOS to perform certain operations
 */
#define MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION                          @"3.2"


/**
 * Defines the input view show animation
 */
#define INPUT_VIEW_SHOW_ANIMATION                                            @"net.movilok.MOKEditableViewController.inputViewShowAnimation"

/**
 * Defines the input view hide animation
 */
#define INPUT_VIEW_HIDE_ANIMATION                                            @"net.movilok.MOKEditableViewController.inputViewHideAnimation"

/**
 * Defines the scroll shrink animation name
 */
#define SCROLL_SHRINK_ANIMATION_NAME                                        @"net.movilok.EditableViewController.scrollShrinkAnimation"

/**
 * Input view animation duration, in seconds
 */
#define INPUT_VIEW_ANIMATION_DURATION                                       0.3f

/**
 * Input view animation curve.
 */
#define INPUT_VIEW_ANIMATION_CURVE                                          UIViewAnimationCurveEaseIn


/**
 * Minimum distance from pop buttons view top to edited view bottom
 */
#define MIN_VERTICAL_DISTANCE_TO_EDITING_VIEW                               20.0f


#pragma mark -

/**
 * MOKEditableViewController private extension.
 */
@interface MOKEditableViewController()

/**
 * Provides read-write access to the view currently being edited.
 */
@property (nonatomic, readwrite, retain) id<MOKEditView> editedView;

/**
 * Provides read-only accesso to the array containing the input views that can be removed from the superview
 */
@property (nonatomic, readonly, retain) NSMutableArray *removableInputViewsArray;

/**
 * Provides read-only accesso to the array containing arrays with views to remove from a given operation.
 */
@property (nonatomic, readonly, retain) NSMutableArray *removeViewsArrays;


/**
 * Releases the graphic elements.
 *
 * @private
 */
- (void)releaseEditableViewControllerGraphicElements;

/**
 * Makes the given edit view visible inside the scroller.
 *
 * @param editView The edit view to make visible.
 * @protected
 */
- (void)makeEditViewVisible:(id<MOKEditView>)editView;


/**
 * Invoked by framework when a text field is about to make the keyboard appear. Used to animate the
 * pop buttons view.
 *
 * @param aNotification The notification information
 * @private
 */
- (void)keyboardWillAppear:(NSNotification *)aNotification;

/**
 * Invoked by framework when a text field is about to make the keyboard disappear. Used to animate the
 * pop buttons view.
 *
 * @param aNotification The notification information
 * @private
 */
- (void)keyboardWillDisappear:(NSNotification *)aNotification;

/**
 * The keyboard had dissapeared
 *
 * @param aNotification The notification information
 * @private
 */
- (void)keyboardDidDisappear:(NSNotification *)aNotification;

/**
 * Shows the input views for the provided edit view in case it is hidden. The input view is animated from the window bottom to the final position.
 *
 * @param editView The edit view to display its input view.
 * @return YES when the input view is displayed, NO otherwise.
 * @private
 */
- (BOOL)showInputViewsForEditView:(id<MOKEditView>)editView;

/**
 * Shows the input accessory view for the provided edit view in case it is hidden, for given input view frame. The input accessory view is animated from the window bottom to the final position.
 *
 * @param editView The edit view to display its input view.
 * @param inputViewFrame The final input view frame.
 * @return YES when the input accessory view is displayed, NO otherwise.
 * @private
 */
- (BOOL)showInputAccessoryViewForEditView:(id<MOKEditView>)editView
                           inputViewFrame:(CGRect)inputViewFrame;

/**
 * Hides the input views in case it is visible, The input view is animated to the window bottom.
 *
 * @param editView The edit view to hide its input view.
 * @param includeInputAccessoryView YES to hide the input accessory view to, NO otherwise.
 * @private
 */
- (void)hideInputViewForEditView:(id<MOKEditView>)editView
       includeInputAccessoryView:(BOOL)includeInputAccessoryView;

/**
 * Invoked by framework when scroll shrink animation is finished.
 *
 * @param animationID An NSString containing an optional application-supplied identifier.
 * @param finished An NSNumber object containing a Boolean value. The value is YES if the animation ran to completion before it stopped or NO if it did not.
 * @param context An optional application-supplied context.
 * @private
 */
- (void)scrollShrinkAnimationDidStop:(NSString *)animationID
                            finished:(NSNumber *)finished
                             context:(void *)context;

/**
 * Invoked by framework when animation finishes. If animation is a picker or date picker animation,
 * the associated view is removed from the superview, otherwise. the edited view is made visible.
 *
 * @param animationID An NSString containing an optional application-supplied identifier.
 * @param finished An NSNumber object containing a Boolean value. The value is YES if the animation ran to completion before it stopped or NO if it did not.
 * @param context An optional application-supplied context.
 * @private
 */
- (void)animationDidStop:(NSString *)animationID
                finished:(NSNumber *)finished
                 context:(void *)context;

/**
 * Invoked by framework when date picker date is modified. The subclasses are notified.
 */
- (void)datePickerDateChanged;

/**
 * Starts the look for edit views once the delay is finished. It is done to avoid situations where the interface is
 * updated in a deferred way (for example when a table is reloaded)
 *
 * @private
 */
- (void)doLookForEditingViews;

/**
 * Looks for edit views in the provided view and stores them in the array.
 *
 * @param view The view to navigate and obtain its edit view (at any depth level).
 * @param editViewsArray The edit view array to fill. Only id<MOKEditView> instances must be stored
 * @private
 */
- (void)lookForEditViewsIntoView:(UIView *)view
                  storeIntoArray:(NSMutableArray *)editViewsArray;

/**
 * Attaches the scrollable view to the transparent scroller.
 *
 * @private
 */
- (void)attachScrollableView;

/**
 * Hides the edit view input views and resets the edited view.
 *
 * @private
 */
- (void)hideCurrentEditViewInputViews;

/**
 * Updates the input view with current edit view information.
 *
 * @private
 */
- (void)updateInputViewsWithCurrentEditView;

/**
 * Removes the view stored in an array than are also stored in the removable views.
 *
 * @param viewsToRemoveArray The views to remove array.
 * @private
 */
- (void)removeViewsFromArray:(NSMutableArray *)viewsToRemoveArray;

/**
 * Adjusts the frame correctly after hiding animations
 *
 * @private
 */
- (void)adjustScrollFrameAfterAnimation;

/**
 * Displays the popover with the input view for the provided edit view.
 *
 * @param editView The edit view with popover must be displayed.
 * @private
 */
- (void)displayPopoverForEditView:(id<MOKEditView>)editView;

@end


#pragma mark -

@implementation MOKEditableViewController

#pragma mark -
#pragma mark Properties

@synthesize scrollableView = scrollableView_;
@synthesize editedView = editedView_;
@synthesize removeViewsArrays = removeViewsArrays_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory.
 */
- (void)dealloc {
    
    NSNotificationCenter* notifCenter = [NSNotificationCenter defaultCenter];
    [notifCenter removeObserver:self
                           name:UIKeyboardWillShowNotification
                         object:nil];
    [notifCenter removeObserver:self
                           name:UIKeyboardWillHideNotification
                         object:nil];
    [notifCenter removeObserver:self
                           name:UIKeyboardDidHideNotification
                         object:nil];

    [self releaseEditableViewControllerGraphicElements];
    
    [editedView_ release];
    editedView_ = nil;
    
    [oldEditedView_ release];
    oldEditedView_ = nil;
    
    [pickerViewController_ release];
    pickerViewController_ = nil;
    
    [tableViewController_ release];
    tableViewController_ = nil;
    
    [pickerPopoverController_ release];
    pickerPopoverController_ = nil;
    
    [currentTextViewText_ release];
    currentTextViewText_ = nil;
    
    [pickerStrings_ release];
    pickerStrings_ = nil;
    
    [removableInputViewsArray_ release];
    removableInputViewsArray_ = nil;
    
    [removeViewsArrays_ release];
    removeViewsArrays_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory. Releases graphic elements.
 */
- (void)viewDidUnload {
    
    [self releaseEditableViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/*
 * Releases the graphic elements
 */
- (void)releaseEditableViewControllerGraphicElements {
    
    [transparentScroll_ release];
    transparentScroll_ = nil;
    
    [scrollableView_ release];
    scrollableView_ = nil;
    
    [pickerView_ release];
    pickerView_ = nil;
    
    [datePicker_ release];
    datePicker_ = nil;
    
    [inputAccessoryView_ release];
    inputAccessoryView_ = nil;
    
}

#pragma mark -
#pragma mark View management

/**
 * Invoked by framework when view has loaded from NIB file. Sets the subviews, if any, in the scroll view.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if (pickerStrings_ == nil) {
        
        pickerStrings_ = [[NSMutableArray allocWithZone:self.zone] init];
        
    }
    
    if (scrollableView_ != nil) {
        
        NSZone *zone = self.zone;
        UIView *view = scrollableView_.superview;
        scrollNominalFrame_ = scrollableView_.frame;
        
        if (transparentScroll_ == nil) {
            
            CGRect scrollFrame = scrollNominalFrame_;
            transparentScroll_ = [[UIScrollView allocWithZone:zone] initWithFrame:scrollFrame];
            transparentScroll_.clipsToBounds = NO;
            transparentScroll_.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;//UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            transparentScroll_.userInteractionEnabled = NO;
            transparentScroll_.autoresizesSubviews = YES;
            transparentScroll_.clipsToBounds = YES;
            transparentScroll_.backgroundColor = [UIColor clearColor];
            [view addSubview:transparentScroll_];
            [view bringSubviewToFront:transparentScroll_];
            
        }
        
        if (pickerView_ == nil) {
            
            pickerView_ = [[UIPickerView allocWithZone:zone] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 215.0f)];
            pickerView_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            pickerView_.backgroundColor=[UIColor BBVAGreyToneFourColor];
            //pickerView_.tintColor = [UIColor BBVAGreyToneFourColor];
            
            pickerView_.delegate = self;
            pickerView_.dataSource = self;
            pickerView_.showsSelectionIndicator = YES;
            
        }
        
        if (datePicker_ == nil) {
            
            datePicker_ = [[UIDatePicker allocWithZone:zone] init];
            if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
            {
            datePicker_.backgroundColor = [UIColor BBVAGreyToneFourColor];
            }
            CGRect datePickerFrame = datePicker_.frame;
            datePickerFrame.size.height = 215.0f;
            datePicker_.frame = datePickerFrame;
            datePicker_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            datePicker_.datePickerMode = UIDatePickerModeDate;
            datePicker_.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
            [datePicker_ addTarget:self
                            action:@selector(datePickerDateChanged)
                  forControlEvents:UIControlEventValueChanged];
            
        }
        
        if (inputAccessoryView_ == nil) {
            
            inputAccessoryView_ = [[MOKInputAccessoryView alloc] init];
            [inputAccessoryView_ auxiliaryButtonVisible:NO];
            [inputAccessoryView_ previousAndNextResponderButtonsVisible:YES];
            [inputAccessoryView_ okButtonEnabled:YES];
            inputAccessoryView_.delegate = self;
            [inputAccessoryView_ setPreviousButtonTitle:NSLocalizedString(TXT_MOK_EDITABLE_VIEW_CONTROLLER_PREVIOUS, nil)];
            [inputAccessoryView_ setNextButtonTitle:NSLocalizedString(TXT_MOK_EDITABLE_VIEW_CONTROLLER_NEXT, nil)];
            [inputAccessoryView_ setOKButtonTitle:NSLocalizedString(TXT_MOK_EDITABLE_VIEW_CONTROLLER_OK, nil)];
            
        }
        
        scrollableView_.backgroundColor = [UIColor clearColor];
        scrollNominalFrame_ = transparentScroll_.frame;
        
        [self attachScrollableView];
        
        NSString *version = [[UIDevice currentDevice] systemVersion];
        
        if ([version compare:MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION
                     options:NSNumericSearch] != NSOrderedAscending) {
            
            oldIOSVersion_ = NO;
            
        } else {
            
            oldIOSVersion_ = YES;
            
        }
        
        [self lookForEditViews];
        
    }
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 */
- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    transparentScroll_.frame = scrollNominalFrame_;
    
    self.editedView = nil;
    
}

/**
 * Invoked by framework when view is added to the window. Registers to notification center to receive keyboard notifications
 *
 * @param animated YES if view is going to be animated, NO otherwise
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self registerToKeyboardEvents];
    
}

/**
 * Invoked by frameword when view is about to be dismissed. Unregisters to notification center as keyboard
 * notifications are not needed anymore
 *
 * @param animated YES if view is going to be animated, NO otherwise
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear: animated];
    
    [self unregisterFromKeyboardEvents];
    displayingPopover_ = NO;
    [pickerPopoverController_ dismissPopoverAnimated:animated];

}

#pragma mark -
#pragma mark Rotation management

/**
 * Sent to the view controller just before the user interface begins rotating. When the popover is display, it is dismissed to be displayed
 * again when the rotation is finished.
 *
 * @param toInterfaceOrientation 
 * @param duration 
 */
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation
                                   duration:duration];
    
    if (editedView_ != nil) {
        
        [self makeEditViewVisible:editedView_];
        
    }
    
    if (displayingPopover_) {
        
        [pickerPopoverController_ dismissPopoverAnimated:YES];
        
    }
    
}

/**
 * Sent to the view controller after the user interface rotates. When the popover was displayed before rotation, it is displayed again.
 *
 * @param fromInterfaceOrientation The old orientation of the user interface.
 */
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    if ((displayingPopover_) && (editedView_ != nil)) {
        
        [self displayPopoverForEditView:editedView_];
        
    } else {
        
        displayingPopover_ = NO;
        
    }
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Resets the scroll position to the top left
 */
- (void)resetScrollToTopLeftAnimated:(BOOL)animated {
    
    [transparentScroll_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f)
                                   animated:animated];
    
}

/*
 * Adjusts the frame correctly after hiding animations
 */
- (void)adjustScrollFrameAfterAnimation {
    
    if (editedView_ == nil) {
        
        transparentScroll_.frame = scrollNominalFrame_;
        
    }
    
}

#pragma mark -
#pragma mark Interface management

/*
 * Attaches the scrollable view to the transparent scroller.
 */
- (void)attachScrollableView {
    
    if ((scrollableView_ != nil) && (scrollableView_.superview != transparentScroll_)) {
        
        CGRect scrollableViewFrame = scrollableView_.frame;
        transparentScroll_.frame = scrollableViewFrame;
        scrollNominalFrame_ = scrollableViewFrame;
        
        scrollableViewFrame.origin.x = 0.0f;
        scrollableViewFrame.origin.y = 0.0f;
        scrollableView_.frame = scrollableViewFrame;
        
        [scrollableView_ removeFromSuperview];
        
        scrollableView_.autoresizingMask = UIViewAutoresizingNone;
        
        [transparentScroll_ addSubview:scrollableView_];
        [transparentScroll_ setContentSize:scrollableView_.frame.size];
        
        if (scrollableView_ != nil) {
            
            transparentScroll_.userInteractionEnabled = YES;
            
        } else {
            
            transparentScroll_.userInteractionEnabled = NO;
            
        }
        
    }
    
}

/*
 * Makes the given edit view visible inside the scroller.
 */
- (void)makeEditViewVisible:(id<MOKEditView>)editView {
    
    if (scrollableView_ != nil) {
        
        if (editView != nil) {
            
            CGRect viewFrame = [editView frame];
            UIView *parentView = [editView superview];
            
            if (!oldIOSVersion_) {
                
                CGRect auxFrame = [parentView convertRect:viewFrame
                                                   toView:transparentScroll_];
                CGPoint currentOffset = transparentScroll_.contentOffset;
                
                if (currentOffset.y > auxFrame.origin.y) {
                    
                    viewFrame.origin.y = viewFrame.origin.y - MIN_VERTICAL_DISTANCE_TO_EDITING_VIEW;
                    
                } else {
                    
                    viewFrame.size.height += MIN_VERTICAL_DISTANCE_TO_EDITING_VIEW;
                    
                }
                
                viewFrame = [parentView convertRect:viewFrame
                                             toView:transparentScroll_];
                [transparentScroll_ scrollRectToVisible:viewFrame
                                               animated:YES];
                
            } else {
                
                viewFrame = [parentView convertRect:viewFrame
                                             toView:transparentScroll_];
                viewFrame.size.height += MIN_VERTICAL_DISTANCE_TO_EDITING_VIEW;
                [transparentScroll_ scrollRectToVisible:viewFrame
                                               animated:([editView isKindOfClass:[UITextField class]] ? NO : YES)];
                
            }
            
        }
        
    }

}

#pragma mark -
#pragma mark Date picker notifications

/*
 * Invoked by framework when date picker date is modified. The subclasses are notified
 */
- (void)datePickerDateChanged {
    
    NSDate *date = datePicker_.date;
    
    if ([editedView_ respondsToSelector:@selector(setSelectedDate:)]) {
        
        [editedView_ setSelectedDate:date];
        
    }
    
}

#pragma mark -
#pragma mark Keyboard events management

/*
 * Registers to keyboard events.
 */
- (void)registerToKeyboardEvents {
    
    if (!registeredToKeyboardEvents_) {
        
        if (scrollableView_ != nil) {
            
            NSNotificationCenter* notifCenter = [NSNotificationCenter defaultCenter];
            [notifCenter addObserver:self
                            selector:@selector(keyboardWillAppear:)
                                name:UIKeyboardWillShowNotification
                              object:nil];
            [notifCenter addObserver:self
                            selector:@selector(keyboardWillDisappear:)
                                name:UIKeyboardWillHideNotification
                              object: nil];
            [notifCenter addObserver:self
                            selector:@selector(keyboardDidDisappear:)
                                name:UIKeyboardDidHideNotification
                              object: nil];
            
            registeredToKeyboardEvents_ = YES;
            
        }
        
    }
    
}

/*
 * Unregisters from keyboard events.
 */
- (void)unregisterFromKeyboardEvents {
    
    if (registeredToKeyboardEvents_) {
        
        if (scrollableView_ != nil) {
            
            NSNotificationCenter* notifCenter = [NSNotificationCenter defaultCenter];
            [notifCenter removeObserver:self
                                   name:UIKeyboardWillShowNotification
                                 object:nil];
            [notifCenter removeObserver:self
                                   name:UIKeyboardWillHideNotification
                                 object:nil];
            [notifCenter removeObserver:self
                                   name:UIKeyboardDidHideNotification
                                 object:nil];
            
            registeredToKeyboardEvents_ = NO;
            
        }
        
    }
    
}

/**
 * Invoked by framework when a text field is about to make the keyboard appear. Used to animate the
 * pop buttons view
 *
 * @param aNotification The notification information
 */
- (void)keyboardWillAppear:(NSNotification *)aNotification {
    
    NSDictionary *userInfo = aNotification.userInfo;
    
    UIViewAnimationCurve animationCurve = INPUT_VIEW_ANIMATION_CURVE;
    double animationDuration = INPUT_VIEW_ANIMATION_DURATION;
    NSValue *value = [userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    if (value != nil) {
        
        [value getValue:&animationCurve];
        
        value = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
        
        if (value != nil) {
            
            [value getValue:&animationDuration];
            
        }
        
    }
    
    UIView *inputAccessoryView = [editedView_ inputAccessoryView];
    CGRect accessoryFrame = CGRectZero;
    CGFloat accessoryHeight = 0.0f;
    BOOL displayAccessory = NO;
    
    UIView *view = transparentScroll_.superview;
    UIWindow *window = view.window;

    CGRect keyboardFrame;
    
#if defined(__IPHONE_3_2) && (__IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2)
    value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    [value getValue:&keyboardFrame];
#else
    if (!oldIOSVersion_) {
        
        value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
        [value getValue:&keyboardFrame];
        
    } else {
        
        value = [userInfo objectForKey:UIKeyboardBoundsUserInfoKey];
        [value getValue:&keyboardFrame];
        value = [userInfo objectForKey:UIKeyboardCenterEndUserInfoKey];
        CGPoint centerPoint;
        [value getValue:&centerPoint];
        keyboardFrame.origin.x = centerPoint.x - round(keyboardFrame.size.width / 2.0f);
        keyboardFrame.origin.y = centerPoint.y - round(keyboardFrame.size.height / 2.0f);
        
        if (inputAccessoryView != nil) {
            
            accessoryFrame = inputAccessoryView.frame;
            accessoryHeight = accessoryFrame.size.height;
            displayAccessory = YES;
            
            if ([inputAccessoryView superview] == nil) {
                
                accessoryFrame.origin.y = CGRectGetHeight(window.frame) - accessoryHeight;
                inputAccessoryView.frame = accessoryFrame;
                [window addSubview:inputAccessoryView];
                [window bringSubviewToFront:inputAccessoryView];
                
            }
            
            NSMutableArray *removableInputViews = self.removableInputViewsArray;
            [removableInputViews removeObject:inputAccessoryView];
            
        }
        
    }
#endif

    keyboardFinalFrame_ = keyboardFrame;

    keyboardFrame = [self.view convertRect:keyboardFinalFrame_
                             fromView:window];
    keyboardFrame = [view convertRect:keyboardFrame
                             fromView:self.view];
    CGPoint topPoint = keyboardFrame.origin;
    editingViewHeight_ = CGRectGetHeight(view.bounds) - topPoint.y;
    
    if (displayAccessory) {
        
        accessoryFrame.origin.y = keyboardFrame.origin.y - accessoryHeight;
        
    }
    
    CGRect frame = transparentScroll_.frame;
    CGFloat height = topPoint.y - accessoryHeight - CGRectGetMinY(frame);
    
    if (height < frame.size.height) {
        
        frame.size.height = height;

    }
    
    if ([[UIView class] respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
        
        [UIView animateWithDuration:animationDuration
                              delay:0.0f
                            options:animationCurve
                         animations:^{
                             
                             transparentScroll_.frame = frame;
                             
                             if (displayAccessory) {
                                 
                                 inputAccessoryView.frame = accessoryFrame;
                                 
                             }
                             
                         }
                         completion:^(BOOL finished) {
                             
                             if (finished) {
                                 
                                 [self makeEditViewVisible:editedView_];
                                 
                             }
                             
                         }];
        
    } else {
        
        [UIView beginAnimations:SCROLL_SHRINK_ANIMATION_NAME
                        context:nil];
        [UIView setAnimationDuration:animationDuration];
        [UIView setAnimationCurve:animationCurve];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(scrollShrinkAnimationDidStop:finished:context:)];
        
        transparentScroll_.frame = frame;
        
        if (displayAccessory) {
            
            inputAccessoryView.frame = accessoryFrame;
            
        }

        [UIView commitAnimations];
        
    }

}

/**
 * Invoked by framework when a text field is about to make the keyboard disappear. Used to animate the
 * pop buttons view
 *
 * @param aNotification The notification information
 */
- (void)keyboardWillDisappear:(NSNotification *)aNotification {
    
    NSDictionary *userInfo = aNotification.userInfo;
    
    UIViewAnimationCurve animationCurve = INPUT_VIEW_ANIMATION_CURVE;
    double animationDuration = INPUT_VIEW_ANIMATION_DURATION;
    NSValue *value = [userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    if (value != nil) {
        
        [value getValue:&animationCurve];
        
        value = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
        
        if (value != nil) {
            
            [value getValue:&animationDuration];
            
        }
        
    }
    
    editingViewHeight_ = 0.0f;
    
    UIView *inputAccessoryView = [editedView_ inputAccessoryView];
    BOOL removeAccessoryView = NO;
    CGRect accessoryFrame = CGRectZero;
    CGFloat accessoryHeight = 0.0f;
    NSMutableArray *viewsToRemove = [NSMutableArray array];
    
    if (viewsToRemove != nil) {
        
        NSMutableArray *removeViewsArray = self.removeViewsArrays;
        [removeViewsArray addObject:viewsToRemove];
        
    }
    
    if (((inputAccessoryView != nil) && ([inputAccessoryView superview] != nil)) && (oldIOSVersion_)) {
        
        removeAccessoryView = YES;
        accessoryFrame = inputAccessoryView.frame;
        accessoryHeight = accessoryFrame.size.height;
        
        accessoryFrame.origin.y = CGRectGetHeight(self.view.window.frame) - accessoryHeight;
        
        [viewsToRemove addObject:inputAccessoryView];
        [self.removableInputViewsArray addObject:inputAccessoryView];
        
    }
    
    CGPoint offset = transparentScroll_.contentOffset;
    
    if ([[UIView class] respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
        
        [UIView animateWithDuration:animationDuration
                              delay:0.0f
                            options:animationCurve
                         animations:^{
                             
                             transparentScroll_.frame = scrollNominalFrame_;
                             
                             if (offset.y > 0.0f) {
                                 
                                 transparentScroll_.center = CGPointMake(round(CGRectGetMidX(scrollNominalFrame_)),
                                                                         round(CGRectGetMidY(scrollNominalFrame_)));
                             }
                             
                             if (removeAccessoryView) {
                                 
                                 inputAccessoryView.frame = accessoryFrame;
                                 
                             }

                         }
                         completion:^(BOOL finished) {
                             
                             if (removeAccessoryView) {
                                 
                                 [self removeViewsFromArray:viewsToRemove];
                                 
                             }
                             
                         }];
        
    } else {
        
        [UIView beginAnimations:INPUT_VIEW_HIDE_ANIMATION
                        context:viewsToRemove];
        [UIView setAnimationDuration:animationDuration];
        [UIView setAnimationCurve:animationCurve];

        transparentScroll_.frame = scrollNominalFrame_;
        
        if (offset.y > 0.0f) {
            
            transparentScroll_.center = CGPointMake(round(CGRectGetMidX(scrollNominalFrame_)),
                                                    round(CGRectGetMidY(scrollNominalFrame_)));
        }
        
        if (removeAccessoryView) {
            
            inputAccessoryView.frame = accessoryFrame;
            
        }
        
        [self makeEditViewVisible:editedView_];
        [UIView commitAnimations];
        
    }

}

/*
 * The keyboard had dissapeared
 */
- (void)keyboardDidDisappear:(NSNotification *)aNotification {

    [self adjustScrollFrameAfterAnimation];
    [self makeEditViewVisible:editedView_];
    
    [editedView_ release];
    editedView_ = nil;
    
}

#pragma mark -
#pragma mark Edit views management

/*
 * Determines the edit views available and sets its order.
 */
- (void)lookForEditViews {
    
    [self performSelector:@selector(doLookForEditingViews)
               withObject:nil
               afterDelay:0.0f];
    
}

/*
 * Starts the look for edit views once the delay is finished. It is done to avoid situations where the interface is
 * updated in a deferred way (for example when a table is reloaded)
 */
- (void)doLookForEditingViews {
    
    if (scrollableView_ != nil) {
        
        NSMutableArray *editViewsArray = [NSMutableArray array];
        
        [self lookForEditViewsIntoView:scrollableView_
                        storeIntoArray:editViewsArray];
        
        id<MOKEditView> previousEditView = nil;
        id<MOKEditView> editView = nil;
        
        NSUInteger editViewsCount = [editViewsArray count];
        
        BOOL iPhoneDevice = self.iPhoneDevice;
        BOOL mustShowInputAccessoryView = [self mustDisplayAccessoryView];
        
        for (NSUInteger i = 0; i < editViewsCount; i++) {
            
            editView = [editViewsArray objectAtIndex:i];
            [editView setEditViewManager:self];
            [editView setPreviousEditView:previousEditView];
            [editView setNextEditView:nil];
            
            [previousEditView setNextEditView:editView];
            previousEditView = editView;
            
            if (iPhoneDevice) {
                
                if (mustShowInputAccessoryView) {
                    
                    [editView setInputAccessoryView:inputAccessoryView_];
                    
                }
                
                if ([editView inputViewType] == mevivtStringPicker) {
                    
                    [editView setInputView:pickerView_];
                    
                } else if ([editView inputViewType] == mevivtDatePicker) {
                    
                    [editView setInputView:datePicker_];
                    
                }
                
            }
            
        }
        
        if (editedView_ != nil) {
            
            if (![editViewsArray containsObject:editedView_]) {
                
                [self.view endEditing:YES];
                
                [editedView_ release];
                editedView_ = nil;
                
            }
            
        }
        
    }
    
}

/*
 * Looks for edit views in the provided view and stores them in the array.
 */
- (void)lookForEditViewsIntoView:(UIView *)view
                  storeIntoArray:(NSMutableArray *)editViewsArray {
    
    NSArray *subviewsArray = [view.subviews sortedArrayUsingSelector:@selector(compareFirstTopLeft:)];
    
    Class labelClass = [UILabel class];
    Class buttonClass = [UIButton class];
    Class webViewClass = [UIWebView class];
    
    for (UIView *internalView in subviewsArray) {
        
        if (!internalView.hidden) {
            
            if ((![internalView isKindOfClass:labelClass]) && (![internalView isKindOfClass:webViewClass])) {
                
                if ([internalView conformsToProtocol:@protocol(MOKEditView)]) {
                    
                    [editViewsArray addObject:internalView];
                    
                } else if (![internalView isKindOfClass:buttonClass]) {
                    
                    [self lookForEditViewsIntoView:internalView
                                    storeIntoArray:editViewsArray];
                    
                }
                
            }
            
        }
        
    }
    
}

#pragma mark -
#pragma mark Input views management

/*
 * Shows the input views for the provided edit view in case it is hidden. The input view is animated from the window bottom to the final position.
 */
- (BOOL)showInputViewsForEditView:(id<MOKEditView>)editView {
    
    BOOL result = YES;
    
    UIView *view = transparentScroll_.superview;
    UIWindow *window = view.window;
    
    UIView *inputView = [editedView_ inputView];
    UIView *inputAccessoryView = [editedView_ inputAccessoryView];
    BOOL displayAccessoryView = NO;
    
    if ((inputView != nil) && ([inputView superview] == nil)) {
        
        NSMutableArray *removableViews = self.removableInputViewsArray;
        [removableViews removeObject:inputView];
        
        CGRect accessoryViewFrame = CGRectZero;
        CGFloat accessoryHeight = 0.0f;
        
        if (inputAccessoryView != nil) {
            
            accessoryViewFrame = inputAccessoryView.frame;
            accessoryHeight = accessoryViewFrame.size.height;
            displayAccessoryView = YES;
            [removableViews removeObject:inputAccessoryView];
            
        }
        
        CGRect inputViewFrame = inputView.frame;
        inputViewFrame.origin.y = CGRectGetHeight(window.bounds) + accessoryHeight;
        inputView.frame = inputViewFrame;
        
        [window addSubview:inputView];
        [window bringSubviewToFront:inputView];
        
        inputViewFrame.origin.y = inputViewFrame.origin.y - inputViewFrame.size.height - accessoryHeight;
        
        CGPoint topPoint = [window convertPoint:inputViewFrame.origin
                                         toView:view];
        
        CGRect frame = transparentScroll_.frame;
        CGFloat height = topPoint.y - accessoryHeight - CGRectGetMinY(frame);
        
        if (height < frame.size.height) {
            
            frame.size.height = height;
            
        }
        
        editingViewHeight_ = height;
        
        if (displayAccessoryView) {
            
            accessoryViewFrame.origin.y = CGRectGetHeight(window.bounds);
            
            if ([inputAccessoryView superview] == nil) {
                
                inputAccessoryView.frame = accessoryViewFrame;
                [window addSubview:inputAccessoryView];
                [window bringSubviewToFront:inputAccessoryView];
                
            }
            
            accessoryViewFrame.origin.y = accessoryViewFrame.origin.y - inputViewFrame.size.height - accessoryHeight;
            
        }
        
        if ([[UIView class] respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
            
            [UIView animateWithDuration:INPUT_VIEW_ANIMATION_DURATION
                                  delay:0.0f
                                options:INPUT_VIEW_ANIMATION_CURVE
                             animations:^{
                                 
                                 inputView.frame = inputViewFrame;
                                 transparentScroll_.frame = frame;
                                 
                                 if (displayAccessoryView) {
                                     
                                     inputAccessoryView.frame = accessoryViewFrame;
                                     
                                 }
                                 
                             }
                             completion:^(BOOL finished) {
                                 
                                 if (finished) {
                                     
                                     [self makeEditViewVisible:editedView_];
                                     
                                 }
                                 
                             }];
            
        } else {
            
            [UIView beginAnimations:INPUT_VIEW_SHOW_ANIMATION
                            context:nil];
            [UIView setAnimationCurve:INPUT_VIEW_ANIMATION_CURVE];
            [UIView setAnimationDuration:INPUT_VIEW_ANIMATION_DURATION];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
            
            inputView.frame = inputViewFrame;
            transparentScroll_.frame = frame;
            
            if (displayAccessoryView) {
                
                inputAccessoryView.frame = accessoryViewFrame;
                
            }
            
            [UIView commitAnimations];
            
        }
        
        result = YES;
        
    }
    
    return result;
    
}

/*
 * Shows the input accessory view for the provided edit view in case it is hidden, for given input view frame. The input accessory view is animated from the window bottom to the final position.
 */
- (BOOL)showInputAccessoryViewForEditView:(id<MOKEditView>)editView
                           inputViewFrame:(CGRect)inputViewFrame {
    
    BOOL result = YES;
    
    UIView *view = transparentScroll_.superview;
    UIWindow *window = view.window;
    
    UIView *inputAccessoryView = [editedView_ inputAccessoryView];
    
    if ((inputAccessoryView != nil) && ([inputAccessoryView superview] == nil)) {
        
        NSMutableArray *removableViews = self.removableInputViewsArray;
            
        CGRect accessoryViewFrame = inputAccessoryView.frame;
        CGFloat accessoryHeight = accessoryViewFrame.size.height;
        
        [removableViews removeObject:inputAccessoryView];
        
        accessoryViewFrame.origin.y = CGRectGetHeight(window.bounds);
        
        inputAccessoryView.frame = accessoryViewFrame;
        [window addSubview:inputAccessoryView];
        [window bringSubviewToFront:inputAccessoryView];
        
        CGRect convertedInputViewFrame = [window convertRect:inputViewFrame
                                                      toView:view];
        
        editingViewHeight_ = CGRectGetHeight(view.frame) - CGRectGetMinY(convertedInputViewFrame) + accessoryHeight;
        accessoryViewFrame.origin.y = accessoryViewFrame.origin.y - inputViewFrame.size.height - accessoryHeight;
        
        if ([[UIView class] respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
            
            [UIView animateWithDuration:INPUT_VIEW_ANIMATION_DURATION
                                  delay:0.0f
                                options:INPUT_VIEW_ANIMATION_CURVE
                             animations:^{
                                 
                                 inputAccessoryView.frame = accessoryViewFrame;
                                 
                             }
                             completion:nil];
            
        } else {
            
            [UIView beginAnimations:nil
                            context:nil];
            [UIView setAnimationCurve:INPUT_VIEW_ANIMATION_CURVE];
            [UIView setAnimationDuration:INPUT_VIEW_ANIMATION_DURATION];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
            
            inputAccessoryView.frame = accessoryViewFrame;
            
            [UIView commitAnimations];
            
        }
        
        result = YES;
        
    }
    
    return result;

}

/*
 * Hides the input views in case it is visible, The input view is animated to the window bottom.
 */
- (void)hideInputViewForEditView:(id<MOKEditView>)editView
       includeInputAccessoryView:(BOOL)includeInputAccessoryView {
    
    UIView *inputView = [editView inputView];
    
    if (inputView.superview != nil) {
        
        NSMutableArray *removableViews = self.removableInputViewsArray;
        NSMutableArray *viewsToRemove = [NSMutableArray array];
        
        if (viewsToRemove != nil) {
            
            NSMutableArray *removeViewsArray = self.removeViewsArrays;
            [removeViewsArray addObject:viewsToRemove];
            
        }

        if (![removableViews containsObject:inputView]) {
            
            [removableViews addObject:inputView];
            
        }
        
        [viewsToRemove addObject:inputView];
        
        UIView *view = transparentScroll_.superview;
        UIWindow *window = view.window;
        
        UIView *inputAccessoryView = [editView inputAccessoryView];
        CGRect accessoryFrame = CGRectZero;
        CGFloat accessoryHeight = 0.0f;
        
        editingViewHeight_ = 0.0f;
        
        if ((includeInputAccessoryView) && (inputAccessoryView != nil) && ([inputAccessoryView superview] != nil)) {
            
            accessoryFrame = inputAccessoryView.frame;
            accessoryHeight = accessoryFrame.size.height;
            accessoryFrame.origin.y = CGRectGetHeight(window.bounds);
            
            if (![removableViews containsObject:inputAccessoryView]) {
                
                [removableViews addObject:inputAccessoryView];
                
            }
            
            [viewsToRemove addObject:inputAccessoryView];
            
        }
        
        CGRect inputViewFrame = inputView.frame;
        inputViewFrame.origin.y = CGRectGetHeight(window.bounds) + accessoryHeight;
        
        CGPoint offset = transparentScroll_.contentOffset;
        
        if ([[UIView class] respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
            
            [UIView animateWithDuration:INPUT_VIEW_ANIMATION_DURATION
                                  delay:0.0f
                                options:INPUT_VIEW_ANIMATION_CURVE
                             animations:^{
                                 
                                 inputView.frame = inputViewFrame;
                                 
                                 if (!isKeyboardShowing_) {
                                     
                                     transparentScroll_.frame = scrollNominalFrame_;
                                     
                                     if (offset.y > 0.0f) {
                                         
                                         transparentScroll_.center = CGPointMake(round(CGRectGetMidX(scrollNominalFrame_)),
                                                                                 round(CGRectGetMidY(scrollNominalFrame_)));
                                         
                                     }
                                     
                                 }
                                 
                                 if (includeInputAccessoryView) {
                                     
                                     inputAccessoryView.frame = accessoryFrame;
                                     
                                 }
                                 
                             }
                             completion:^(BOOL finished) {
                                 
                                 [self removeViewsFromArray:viewsToRemove];
                                 
                             }];
            
        } else {
            
            [UIView beginAnimations:INPUT_VIEW_HIDE_ANIMATION
                            context:viewsToRemove];
            [UIView setAnimationCurve:INPUT_VIEW_ANIMATION_CURVE];
            [UIView setAnimationDuration:INPUT_VIEW_ANIMATION_DURATION];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
            
            inputView.frame = inputViewFrame;
            
            if (!isKeyboardShowing_) {
                
                transparentScroll_.frame = scrollNominalFrame_;
                
                if (offset.y > 0.0f) {
                    
                    transparentScroll_.center = CGPointMake(round(CGRectGetMidX(scrollNominalFrame_)),
                                                            round(CGRectGetMidY(scrollNominalFrame_)));
                    
                }
                
            }
            
            if (includeInputAccessoryView) {
                
                inputAccessoryView.frame = accessoryFrame;
                
            }
            
            [UIView commitAnimations];
            
        }
        
    }
    
}

/*
 * Hides the edit view input views and resets the edited view.
 */
- (void)hideCurrentEditViewInputViews {
    
    MOKEditViewInputViewType oldInputType = [oldEditedView_ inputViewType];
    MOKEditViewInputViewType newInputType = [editedView_ inputViewType];
    
    if (editedView_ == oldEditedView_) {
        
        if ((newInputType != mevivtKeyboard) && (oldIOSVersion_)) {
            
            [self hideInputViewForEditView:editedView_
                 includeInputAccessoryView:YES];
            
        }
        
//        [editedView_ release];
//        editedView_ = nil;
        
    } else {
        
        if ((oldInputType != newInputType) && (oldInputType != mevivtKeyboard) && (oldIOSVersion_)) {
            
            [self hideInputViewForEditView:oldEditedView_
                 includeInputAccessoryView:NO];
            
        }
        
    }
    
    [oldEditedView_ release];
    oldEditedView_ = nil;
    
}

/**
 * Updates the input view with current edit view information.
 *
 * @private
 */
- (void)updateInputViewsWithCurrentEditView {
    
    if ([editedView_ previousEditView] != nil) {
        
        [inputAccessoryView_ previousResponderButtonEnabled:YES];
        
    } else {
        
        [inputAccessoryView_ previousResponderButtonEnabled:NO];
        
    }
    
    if ([editedView_ nextEditView] != nil) {
        
        [inputAccessoryView_ nextResponderButtonEnabled:YES];
        
    } else {
        
        [inputAccessoryView_ nextResponderButtonEnabled:NO];
        
    }
    
    if (([editedView_ previousEditView] == nil) && ([editedView_ nextEditView] == nil)) {
        
        [inputAccessoryView_ previousAndNextResponderButtonsVisible:NO];
        
    } else {
        
        [inputAccessoryView_ previousAndNextResponderButtonsVisible:YES];
        
    }
    
    MOKEditViewInputViewType inputViewType = [editedView_ inputViewType];

    if (inputViewType == mevivtStringPicker) {
        
        [pickerStrings_ removeAllObjects];
        
        if ([editedView_ respondsToSelector:@selector(optionStringList)]) {
            
            NSArray *optionStringList = [editedView_ optionStringList];
            
            Class stringClass = [NSString class];
            
            for (NSObject *object in optionStringList) {
                
                if ([object isKindOfClass:stringClass]) {
                    
                    [pickerStrings_ addObject:object];
                    
                }
                
            }
            
            [pickerView_ reloadAllComponents];
            
            if ([editedView_ respondsToSelector:@selector(selectedIndex)]) {
                
                NSInteger selectedIndex = [editedView_ selectedIndex];
                
                if ((selectedIndex >= 0) && (selectedIndex < [pickerStrings_ count])) {
                    
                    [pickerView_ selectRow:selectedIndex
                               inComponent:0
                                  animated:YES];
                    
                } else if ([pickerStrings_ count] > 0) {
                    
                    if ([editedView_ respondsToSelector:@selector(setSelectedIndex:)]) {
                        
                        [editedView_ setSelectedIndex:0];
                        [pickerView_ selectRow:0
                                   inComponent:0
                                      animated:YES];
                        
                    }
                    
                }
                
            }
            
        }
        
    } else if (inputViewType == mevivtDatePicker) {
        
        NSDate *minimumDate = nil;
        NSDate *maximumDate = nil;
        
        if ([editedView_ respondsToSelector:@selector(minimumDate)]) {
            
            minimumDate = [editedView_ minimumDate];
            
        }
        
        if ([editedView_ respondsToSelector:@selector(maximumDate)]) {
            
            maximumDate = [editedView_ maximumDate];
            
        }
        
        datePicker_.minimumDate = minimumDate;
        datePicker_.maximumDate = maximumDate;
        
        NSDate *selectedDate = nil;
        
        if ([editedView_ performSelector:@selector(selectedDate)]) {
            
            selectedDate = [editedView_ selectedDate];
            
        }
        
        datePicker_.date = selectedDate;
        
    }
    
}

/*
 * Removes the view stored in an array than are also stored in the removable views.
 */
- (void)removeViewsFromArray:(NSMutableArray *)viewsToRemoveArray {
    
    NSMutableArray *removableViews = self.removableInputViewsArray;
    NSMutableArray *removeViewsArray = self.removeViewsArrays;
    
    if ([removeViewsArray containsObject:viewsToRemoveArray]) {
        
        while ([viewsToRemoveArray count] > 0) {
            
            UIView *view = viewsToRemoveArray.lastObject;
            
            if ([removableViews containsObject:view]) {
                
                [view removeFromSuperview];
                
                [removableViews removeObject:view];
                
            }
            
            [viewsToRemoveArray removeObject:view];
            
        }
        
        [removeViewsArray removeObject:viewsToRemoveArray];
        
    }

}


/*
 * Displays the popover with the input view for the provided edit view.
 */
- (void)displayPopoverForEditView:(id<MOKEditView>)editView {
    
    MOKEditViewInputViewType editingType = [editView inputViewType];
    
    if ((editingType != mevivtUnknown) && (editingType != mevivtKeyboard)) {
        
        BOOL useTableViewController = NO;
        
        if (editingType == mevivtStringPicker) {
            
            useTableViewController = [self popoverStringListEditorDisplaysTableViewForEditView:editView];
            
        }
        
        UIViewController *popoverViewController = nil;
        CGSize contentSize = CGSizeZero;
        
        if (useTableViewController) {
            
            if (tableViewController_ == nil) {
                
                tableViewController_ = [[MOKEditableStringTableSelectionViewController MOKEditableStringTableSelectionViewController] retain];
                tableViewController_.editableStringTableSelectionViewControllerDelegate = self;
                
            }
            
            popoverViewController = tableViewController_;
            
            NSString *headerTitle = [self stringListSelectionTableHeaderForEditView:editView];
            tableViewController_.topNavigationBarTitle = headerTitle;
            
            CGFloat cellHeight = [self stringListSelectionTableCellHeightForEditView:editView];

            [tableViewController_ setTableViewCellHeight:cellHeight];
            
            NSArray *stringsArray = [editView optionStringList];
            CGFloat tableHeight = cellHeight * (CGFloat)[stringsArray count];
            
            CGFloat viewHeight = tableHeight + 54.0f;
            
            if (viewHeight > 500.0f) {
                
                viewHeight = 500.0f;
                
            }
            
            UIView *tableViewControllerView = tableViewController_.view;
            CGRect frame = tableViewControllerView.frame;
            frame.size.height = viewHeight;
            tableViewControllerView.frame = frame;
            
            contentSize = frame.size;
            
        } else {
            
            if (pickerViewController_ == nil) {
                
                pickerViewController_ = [[UIViewController alloc] init];
                
            }
            
            popoverViewController = pickerViewController_;
            
            UIView *view = pickerViewController_.view;
            view.backgroundColor = [UIColor clearColor];
            
            NSArray *subviewsInPopoverController = [view subviews];
            
            for (UIView *currentView in subviewsInPopoverController) {
                
                [currentView removeFromSuperview];
                
            }
            
            UIView *inputView = nil;
            
            [pickerView_ removeFromSuperview];
            [datePicker_ removeFromSuperview];
            
            if (editingType == mevivtStringPicker) {
                
                inputView = pickerView_;
                
            } else if (editingType == mevivtDatePicker) {
                
                inputView = datePicker_;
                
            }
            
            CGRect inputViewFrame = inputView.frame;
            inputViewFrame.origin.x = 0.0f;
            inputViewFrame.origin.y = 0.0f;
            inputView.frame = inputViewFrame;
            
            contentSize = inputViewFrame.size;
            
            view.frame = inputViewFrame;
			[view addSubview:inputView];

        }
        
        if (pickerPopoverController_ == nil) {
            
            pickerPopoverController_ = [[UIPopoverController alloc] initWithContentViewController:popoverViewController];
            pickerPopoverController_.delegate = self;
            
        }
        
        [pickerPopoverController_ setContentViewController:popoverViewController];
        
        pickerPopoverController_.popoverContentSize = contentSize;
        
        CGRect editViewFrame = [editView frame];
        UIView *editViewSuperview = [editView superview];
        UIView *selfView = self.view;
        CGRect ownViewFrame = [selfView convertRect:editViewFrame
                                           fromView:editViewSuperview];
        
        displayingPopover_ = YES;
        [pickerPopoverController_ presentPopoverFromRect:ownViewFrame
                                                  inView:selfView
                                permittedArrowDirections:UIPopoverArrowDirectionAny
                                                animated:YES];
        
    }
    
}

#pragma mark -
#pragma mark Animation management

/*
 * Invoked by framework when scroll shrink animation is finished
 */
- (void)scrollShrinkAnimationDidStop:(NSString *)animationID
                            finished:(NSNumber *)finished
                             context:(void *)context {
    
    if ([animationID isEqualToString:SCROLL_SHRINK_ANIMATION_NAME]) {
        
        [self makeEditViewVisible:editedView_];
        
    }
    
}

/*
 * Invoked by framework when animation finishes. If animation is a picker or date picker animation,
 * the associated view is removed from the superview, otherwise. the edited view is made visible
 */
- (void)animationDidStop:(NSString *)animationID
                finished:(NSNumber *)finished
                 context:(void *)context {
    
    if ([animationID isEqualToString:INPUT_VIEW_HIDE_ANIMATION]) {
        
        NSMutableArray *viewsToRemove = (NSMutableArray *)context;
        
        [self removeViewsFromArray:viewsToRemove];
        
        [self adjustScrollFrameAfterAnimation];
        
    } else if ([animationID isEqualToString:INPUT_VIEW_SHOW_ANIMATION]) {
        
        if ([finished boolValue]) {
            
            [self makeEditViewVisible:editedView_];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark UIPickerViewDelegate protocol selectors

/**
 * Called by the picker view when it needs the title to use for a given row in a given component. The text in the string array position is returned
 *
 * @param pickerView An object representing the picker view requesting the data
 * @param row A zero-indexed number identifying a row of component
 * @param component A zero-indexed number identifying a component of pickerView
 * @return The text in the string array position
 */
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *result = @"";
    
    if ((row >= 0) && (row < [pickerStrings_ count])) {
        
        result = [pickerStrings_ objectAtIndex:row];
        
    }
    
    return result;
    
}

/**
 * Called by the picker view when it needs the view to use for a given row in a given component.
 *
 * @param pickerView An object representing the picker view requesting the data.
 * @param row A zero-indexed number identifying a row of component. Rows are numbered top-to-bottom.
 * @param component A zero-indexed number identifying a component of pickerView. Components are numbered left-to-right.
 * @param view A view object that was previously used for this row, but is now hidden and cached by the picker view.
 */
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    if (view == nil) {
        
        CGSize size = [pickerView rowSizeForComponent:component];
        
        view = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, size.width, size.height)] autorelease];
        
    } else {
        
        NSArray *views = [view subviews];
        
        for (UIView *view in views) {
            
            [view removeFromSuperview];
            
        }
        
    }

    // label and style to show text
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 0.0f, CGRectGetWidth([view frame]) - 5.0f, CGRectGetHeight([view frame]))];
    [label setText:[pickerStrings_ objectAtIndex:row]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setMinimumFontSize:12.0f];
    [label setAdjustsFontSizeToFitWidth:YES];
    [label setFont:[UIFont boldSystemFontOfSize:18.0f]];
    [label setLineBreakMode:UILineBreakModeHeadTruncation];
    [view addSubview:label];
    
    return view;
    
}

/**
 * Called by the picker view when the user selects a row in a component. The subclass is notified via the protected selector
 *
 * @param pickerView 
 * @param row 
 * @param component 
 */
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if ((row >= 0) && (row < [pickerStrings_ count])) {
        
        if ([editedView_ respondsToSelector:@selector(setSelectedIndex:)]) {
            
            [editedView_ setSelectedIndex:row];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark UIPickerViewDataSource protocol selectors

/**
 * Called by the picker view when it needs the number of components. Returns always 1
 *
 * @param pickerView The picker view requesting the data
 * @return Always 1
 */
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
    
}

/**
 * Called by the picker view when it needs the number of rows for a specified component. Returns the number of strings inside the picker strings array
 *
 * @param pickerView The picker view requesting the data
 * @param component A zero-indexed number identifying a component of pickerView
 * @return The number of strings inside the picker strings array
 */
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [pickerStrings_ count];
    
}

#pragma mark -
#pragma mark MOKEditViewsManager protocol selectors

/**
 * The edit view notifies it is about to become first responder. The edit view is set as the new edited view controller.
 *
 * @param editView The edit view that is about to become first responder.
 */
- (void)editViewWillBecomeFirstResponder:(id<MOKEditView>)editView {
    
    if (editedView_ != editView) {
        
		[self.view.window makeKeyWindow];
		
        [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                 selector:@selector(hideCurrentEditViewInputViews)
                                                   object:nil];
        
        BOOL previousEditionNotNil = (editedView_ != nil);
        MOKEditViewInputViewType previousInputViewType = [editedView_ inputViewType];
        id<MOKEditView> previousEditView = [[editedView_ retain] autorelease];
        
        [editView retain];
        [editedView_ release];
        editedView_ = editView;
        
        [self updateInputViewsWithCurrentEditView];
        
        MOKEditViewInputViewType nextInputViewType = [editedView_ inputViewType];
        
        if ((previousInputViewType != nextInputViewType) && (oldIOSVersion_)) {
                
            if (previousEditionNotNil) {
                
                if (previousInputViewType != mevivtKeyboard) {
                    
                    [self hideInputViewForEditView:previousEditView
                         includeInputAccessoryView:NO];
                    
                }
                
            }

            if (nextInputViewType != mevivtKeyboard) {
                
                [self showInputViewsForEditView:editedView_];
                
            } else {
                
                [self showInputAccessoryViewForEditView:editedView_
                                         inputViewFrame:keyboardFinalFrame_];
                
            }

        }
        
        if (![MOKViewController iPhoneDevice]) {
            
            if ((previousInputViewType != nextInputViewType) && (nextInputViewType == mevivtKeyboard)) {
                
                displayingPopover_ = NO;
                [pickerPopoverController_ dismissPopoverAnimated:YES];
                
            } else if (nextInputViewType != mevivtKeyboard) {
                
                displayingPopover_ = NO;
                [pickerPopoverController_ dismissPopoverAnimated:NO];
                [self displayPopoverForEditView:editedView_];
                
            }
            
        } else {
            
//            if (nextInputViewType != mevivtKeyboard) {
            
                [self makeEditViewVisible:editedView_];
                
//            }
            
        }
        
    }
    
}

/**
 * The edit view notifies it has resigned first responder status. It is removed as the edited view controller.
 *
 * @param editView The edit view that resigned first responder status.
 */
- (void)editViewDidResignFirstResponder:(id<MOKEditView>)editView {
    
    if (editView == editedView_) {
        
        if (editView != oldEditedView_) {
            
            [editView retain];
            [oldEditedView_ release];
            oldEditedView_ = editView;
            
        }
        
        [self performSelector:@selector(hideCurrentEditViewInputViews)
                   withObject:nil
                   afterDelay:0.05f];
        
    }
    
}

#pragma mark -
#pragma mark MOKAccessoryViewDelegate protocol selectors

/**
 * Informs the delegate that OK button was tapped. Editing is stopped in the view
 *
 * @param inputAccessoryView The imput accessory view triggering the event.
 */
- (void)inputAccessoryViewOKButtonTapped:(MOKInputAccessoryView *)inputAccessoryView {
    
    [self.view endEditing:YES];
    
}

/**
 * Informs the delegate that auxiliary button button was tapped.
 *
 * @param inputAccessoryView The imput accessory view triggering the event.
 */
- (void)inputAccessoryViewAuxiliaryButtonTapped:(MOKInputAccessoryView *)inputAccessoryView {
    
    //Do nothing (by now)
    
}

/**
 * Informs the delegate that previous responder button was tapped. The previous responder is checked.
 *
 * @param inputAccessoryView The imput accessory view triggering the event.
 */
- (void)inputAccessoryViewPreviousResponderButtonTapped:(MOKInputAccessoryView *)inputAccessoryView {
    
    id<MOKEditView> previousEditView = [editedView_ previousEditView];
    
    if ([(NSObject *)previousEditView isKindOfClass:[UIResponder class]]) {
        
        UIResponder *responder = (UIResponder *)previousEditView;
        [responder becomeFirstResponder];
        
    }
    
}

/**
 * Informs the delegate that next responder button was tapped. The next responder is checked
 *
 * @param inputAccessoryView The imput accessory view triggering the event.
 */
- (void)inputAccessoryViewNextResponderButtonTapped:(MOKInputAccessoryView *)inputAccessoryView {
    
    id<MOKEditView> nextEditView = [editedView_ nextEditView];
    
    if ([(NSObject *)nextEditView isKindOfClass:[UIResponder class]]) {
        
        UIResponder *responder = (UIResponder *)nextEditView;
        [responder becomeFirstResponder];
        
        
    }
    
}

#pragma mark -
#pragma mark MOKEditableStringTableSelectionViewControllerDelegate protocol selectors

/**
 * Requests the delegate for the numer of rows for the selection table. The number of strings in the editable is returned.
 *
 * @param editableStringTableSelectionViewController The MOKEditableStringTableSelectionViewController instance triggering the event.
 * @return The number of rows for the selection table.
 */
- (NSInteger)editableStringTableSelectionViewControllerNumberOfRows:(MOKEditableStringTableSelectionViewController *)editableStringTableSelectionViewController {
    
    NSInteger result = 0;
    
    if (editedView_ != nil) {
        
        if ([editedView_ inputViewType] == mevivtStringPicker) {
            
            if ([(NSObject *)editedView_ respondsToSelector:@selector(optionStringList)]) {
                
                NSArray *stringsArray = [editedView_ optionStringList];
                
                result = [stringsArray count];
                
            }
            
        }
        
    }
    
    return result;

}

/**
 * Requests the delegate for the cell at the given row.
 *
 * @param editableStringTableSelectionViewController The MOKEditableStringTableSelectionViewController instance triggering the event.
 * @param tableView The table view requesting the cell.
 * @param indexPath The row index path.
 * @return The cell at the given row.
 */
- (UITableViewCell *)editableStringTableSelectionViewController:(MOKEditableStringTableSelectionViewController *)editableStringTableSelectionViewController
                                                      tableView:(UITableView *)tableView
                                                cellAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *result = nil;
    
    NSUInteger row = indexPath.row;
    
    if (editedView_ != nil) {
        
        if ([editedView_ inputViewType] == mevivtStringPicker) {
            
            result = [self tableView:tableView
               cellForRowAtIndexPath:indexPath
                         forEditView:editedView_];
            
            if (result == nil) {
                
                MOKSimpleStringSelectionCell *simpleStringCell = (MOKSimpleStringSelectionCell *)[tableView dequeueReusableCellWithIdentifier:[MOKSimpleStringSelectionCell cellIdentifier]];
                
                if (simpleStringCell == nil) {
                    
                    simpleStringCell = [MOKSimpleStringSelectionCell MOKSimpleStringSelectionCell];
                    
                }
                
                result = simpleStringCell;
                
                simpleStringCell.cellString = @"";
                
                if ([(NSObject *)editedView_ respondsToSelector:@selector(optionStringList)]) {
                    
                    NSArray *stringList = [editedView_ optionStringList];
                    
                    if (row < [stringList count]) {
                        
                        NSString *string = [stringList objectAtIndex:row];
                        simpleStringCell.cellString = string;
                        
                    }
                    
                }
                
            }
            
        }
        
        if (row == [editedView_ selectedIndex]) {
            
            result.selected = YES;
            
        } else {
            
            result.selected = NO;
            
        }
        
    }
    
    return  result;

}

/**
 * Request the delegate for the selected row. The selected index is returned.
 *
 * @param editableStringTableSelectionViewController The MOKEditableStringTableSelectionViewController instance triggering the event.
 * @return The selected row.
 */
- (NSInteger)editableStringTableSelectionViewControllerSelectedRow:(MOKEditableStringTableSelectionViewController *)editableStringTableSelectionViewController {
    
    NSInteger result = -1;
    
    if ((editedView_ != nil) && ([editedView_ inputViewType] == mevivtStringPicker) && ([editedView_ respondsToSelector:@selector(selectedIndex)])) {
        
        result = [editedView_ selectedIndex];
        
    }
    
    return result;
    
}

/**
 * Notifies the delegate that a cell was selected. The string selection control is notified.
 *
 * @param editableStringTableSelectionViewController The MOKEditableStringTableSelectionViewController instance triggering the event.
 * @param indexPath The selected cell index path.
 */
- (void)editableStringTableSelectionViewController:(MOKEditableStringTableSelectionViewController *)editableStringTableSelectionViewController
                           selectedCellAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editedView_ != nil) {
        
        if (([editedView_ inputViewType] == mevivtStringPicker) && ([(NSObject *)editedView_ respondsToSelector:@selector(setSelectedIndex:)])) {
            
            NSUInteger row = indexPath.row;
            [editedView_ setSelectedIndex:row];
            
        }
        
    }

}

#pragma mark -
#pragma mark UIPopoverControllerDelegate protocol selectors

/**
 * Tells the delegate that the popover was dismissed. Any editing control currently being edited is made to resign first responder status.
 *
 * @param popoverController The popover controller that was dismissed.
 */
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    
    if (editedView_ != nil) {
        
        [editedView_ resignFirstResponder];
        
    }
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the array containing the input views that can be removed from the superview, creating it when needed.
 *
 * @return The array containing the input views that can be removed from the superview.
 */
- (NSMutableArray *)removableInputViewsArray {
    
    if (removableInputViewsArray_ == nil) {
        
        removableInputViewsArray_ = [[NSMutableArray alloc] init];
        
    }
    
    return removableInputViewsArray_;
    
}

/*
 * Returns the array containing arrays with views to remove from a given operation.
 *
 * @return The array containing arrays with views to remove from a given operation.
 */
- (NSMutableArray *)removeViewsArrays {
    
    if (removeViewsArrays_ == nil) {
        
        removeViewsArrays_ = [[NSMutableArray alloc] init];
        
    }
    
    return removeViewsArrays_;
    
}

@end
