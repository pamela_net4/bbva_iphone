/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKViewController.h"


//Forward declarations
@class MOKEditableStringTableSelectionViewController;


/**
 * MOKEditableStringTableSelectionViewController delegate. It is requested about the information needed to display the view.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol MOKEditableStringTableSelectionViewControllerDelegate

/**
 * Requests the delegate for the numer of rows for the selection table.
 *
 * @param editableStringTableSelectionViewController The MOKEditableStringTableSelectionViewController instance triggering the event.
 * @return The number of rows for the selection table.
 */
- (NSInteger)editableStringTableSelectionViewControllerNumberOfRows:(MOKEditableStringTableSelectionViewController *)editableStringTableSelectionViewController;

/**
 * Requests the delegate for the cell at the given row.
 *
 * @param editableStringTableSelectionViewController The MOKEditableStringTableSelectionViewController instance triggering the event.
 * @param tableView The table view requesting the cell.
 * @param indexPath The row index path.
 * @return The cell at the given row.
 */
- (UITableViewCell *)editableStringTableSelectionViewController:(MOKEditableStringTableSelectionViewController *)editableStringTableSelectionViewController
                                                      tableView:(UITableView *)tableView
                                                cellAtIndexPath:(NSIndexPath *)indexPath;

/**
 * Request the delegate for the selected row.
 *
 * @param editableStringTableSelectionViewController The MOKEditableStringTableSelectionViewController instance triggering the event.
 * @return The selected row.
 */
- (NSInteger)editableStringTableSelectionViewControllerSelectedRow:(MOKEditableStringTableSelectionViewController *)editableStringTableSelectionViewController;

/**
 * Notifies the delegate that a cell was selected.
 *
 * @param editableStringTableSelectionViewController The MOKEditableStringTableSelectionViewController instance triggering the event.
 * @param indexPath The selected cell index path.
 */
- (void)editableStringTableSelectionViewController:(MOKEditableStringTableSelectionViewController *)editableStringTableSelectionViewController
                           selectedCellAtIndexPath:(NSIndexPath *)indexPath;

@end
 

/**
 * MOKEditableStringTableSelectionViewController provides a table view to select an option from a string list selector editor (used in iPad to provide
 * more complex elements than a picker view when displayed in a popover)
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKEditableStringTableSelectionViewController : MOKViewController <UITableViewDataSource, UITableViewDelegate> {
    
@private
    
    /**
     * Top navigation bar.
     */
    UINavigationBar *topNavigationBar_;
    
    /**
     * Table background view.
     */
    UIView *tableBackgroundView_;
    
    /**
     * Options table view.
     */
    UITableView *optionsTableView_;
    
    /**
     * Top navigation bar title.
     */
    NSString *topNavigationBarTitle_;
    
    /**
     * Cell height.
     */
    CGFloat cellHeight_;
    
    /**
     * MOKEditableStringTableSelectionViewController delegate.
     */
    id<MOKEditableStringTableSelectionViewControllerDelegate> editableStringTableSelectionViewControllerDelegate_;
    
}


/**
 * Provides read-write access to the top navigation bar and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UINavigationBar *topNavigationBar;

/**
 * Provides read-write access to the table background view and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *tableBackgroundView;

/**
 * Provides read-write access to the options table view and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *optionsTableView;

/**
 * Provides read-write access to the top navigation bar title.
 */
@property (nonatomic, readwrite, copy) NSString *topNavigationBarTitle;

/**
 * Provides read-write access to the MOKEditableStringTableSelectionViewController delegate.
 */
@property (nonatomic, readwrite, assign) id<MOKEditableStringTableSelectionViewControllerDelegate> editableStringTableSelectionViewControllerDelegate;


/**
 * Creates and returns an autoreleased MOKEditableStringTableSelectionViewController constructed from a NIB file.
 *
 * @return The autoreleased MOKEditableStringTableSelectionViewController constructed from a NIB file.
 */
+ (MOKEditableStringTableSelectionViewController *)MOKEditableStringTableSelectionViewController;


/**
 * Reloads the table view content.
 */
- (void)reloadTableContent;

/**
 * Sets the table view cell height.
 *
 * @param cellHeight The cell height.
 */
- (void)setTableViewCellHeight:(CGFloat)cellHeight;

@end
