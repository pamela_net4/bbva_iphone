/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKEditableViewController.h"


/**
 * Editable view controller protected category ment to be used only by MOKEditableViewController subclasses.
 * Defines configuration selectors that subclasses must implement if needed.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKEditableViewController(protected)

/**
 * Determines whether the input accessory view must be displayed. When no accessory view is diplayed, a Done button
 * is displayed at the navigaiton bar. Default implementation returns YES.
 *
 * @returns YES to display an input accessory, NO otherwise.
 * @protected
 */
- (BOOL)mustDisplayAccessoryView;

/**
 * Determines whether the list editor displayed in a popover (iPad only) must be a picker view or a table view controller. Default implementation displays a picker view.
 *
 * @param editView The edit view for which the request is done.
 * @return YES to display a table view controller, NO to display a picker view (default value)
 * @protected
 */
- (BOOL)popoverStringListEditorDisplaysTableViewForEditView:(id<MOKEditView>)editView;

/**
 * When the string list selection must be performed inside a table view (iPad only), subclasses must override this selector to provide the table view header title.
 * Default implementation returns an empty string.
 *
 * @param editView The edit view for which the request is done.
 * @protected
 */
- (NSString *)stringListSelectionTableHeaderForEditView:(id<MOKEditView>)editView;

/**
 * When string list selection must be performed inside a table view (iPad only), subclasses must override this selector to provide the cell height (all
 * cells inside the table must have the same height). Default implementation returns the simple string cell height.
 *
 * @param editView The edit view for which the request is done.
 * @return The string list selection table cell height.
 * @protected
 */
- (CGFloat)stringListSelectionTableCellHeightForEditView:(id<MOKEditView>)editView;

/**
 * When the string list selection must be performed inside a table view (iPad only), subclasses must override this selector to provide the cell for the provided index path.
 * Default implementation returns a nil cell.
 *
 * @param editView The edit view for which the request is done.
 * @param tableView A table-view object requesting the cell.
 * @param indexPath  An index path locating a row in tableView.
 * @protected
 */
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
                   forEditView:(id<MOKEditView>)editView;

/**
 * Sets the scrollable view height.
 *
 * @param height The scrollable view new height.
 * @protected
 */
- (void)setScrollableViewHeight:(CGFloat)height;

/**
 * Sets the scroll content size.
 *
 * @param contentSize The scroll content size.
 * @protected
 */
- (void)setScrollContentSize:(CGSize)contentSize;

/**
 * Sets the scroll frame.
 *
 * @param frame The new scroll frame.
 * @protected
 */
- (void)setScrollFrame:(CGRect)frame;

/**
 * Set the scroll to fill its parent view.
 *
 * @protected
 */
- (void)scrollFillsParent;

/**
 * Adjust the scroll to the nominal frame and a possible input view.
 *
 * @protected
 */
- (void)adjustScrollToNominalFrameAndInputView;

@end
