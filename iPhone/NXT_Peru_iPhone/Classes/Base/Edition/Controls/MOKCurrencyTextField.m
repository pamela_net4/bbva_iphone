/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKCurrencyTextField.h"


#pragma mark -

/**
 * Object that performs the delegate operations.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKCurrencyTextFieldDelegate : NSObject <UITextFieldDelegate> {
    
@private
    
    /**
     * Number formatter to format the amounts.
     */
    NSNumberFormatter *numberFormatter_;
    
    /**
     * Default number formatter when no number formatter is provided.
     */
    NSNumberFormatter *defaultNumberFormatter_;
    
    /**
     * External UITextFieldDelegate.
     */
    id<UITextFieldDelegate> delegate_;
    
}


/**
 * Provides read-write access to the number formatter to format the amounts.
 */
@property (nonatomic, readwrite, retain) NSNumberFormatter *numberFormatter;

/**
 * Provides read-only access to the valid number formatter, that is, the number formatter to use to format the amounts (either the default one or the
 * custom one)
 */
@property (nonatomic, readonly, retain) NSNumberFormatter *validNumberFormatter;

/**
 * Provides read-write access to the external UITextFieldDelegate.
 */
@property (nonatomic, readwrite, assign) id<UITextFieldDelegate> delegate;

@end


#pragma mark -

/**
 * MOKUICurrencyTextField private extension.
 */
@interface MOKCurrencyTextField()

/**
 * Initiales the instance.
 *
 * @private
 */
- (void)initialize;

/**
 * Returns the zero string for the control.
 *
 * @return The zero string for the control.
 * @private
 */
- (NSString *)zeroString;

@end


#pragma mark -

@implementation MOKCurrencyTextFieldDelegate

#pragma mark -
#pragma mark Properties

@synthesize numberFormatter = numberFormatter_;
@dynamic validNumberFormatter;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory.
 */
- (void)dealloc {
    
    [numberFormatter_ release];
    numberFormatter_ = nil;
    
    [defaultNumberFormatter_ release];
    defaultNumberFormatter_ = nil;
    
    delegate_ = nil;
    
    [super dealloc];
    
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. It initializes a MOKCurrencyTextFieldDelegate by creatint the default numebr formatter.
 *
 * @return The initialized MOKCurrencyTextFieldDelegate instance.
 */
- (id)init {
    
    if ((self = [super init])) {
        
        NSLocale *locale = [NSLocale currentLocale];
        NSString *decimalSeparator = [locale objectForKey:NSLocaleDecimalSeparator];
        NSString *groupingSeparator = [locale objectForKey:NSLocaleGroupingSeparator];

        defaultNumberFormatter_ = [[NSNumberFormatter alloc] init];
        [defaultNumberFormatter_ setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [defaultNumberFormatter_ setUsesGroupingSeparator:YES];
        [defaultNumberFormatter_ setGroupingSize:3];
        [defaultNumberFormatter_ setGroupingSeparator:groupingSeparator];
        [defaultNumberFormatter_ setDecimalSeparator:decimalSeparator];
        [defaultNumberFormatter_ setMinimumFractionDigits:0];
        [defaultNumberFormatter_ setMaximumFractionDigits:0];
        [defaultNumberFormatter_ setMinimumIntegerDigits:1];
        
    }
    
    return self;
    
}


#pragma mark -
#pragma mark UITextFieldDelegate

/**
 * Asks the delegate if the specified text should be changed. Used to make sure the text field is visible, and maximum length is not exceeded.
 *
 * @param textField The text field containing the text.
 * @param range The range of characters to be replaced.
 * @param string The replacement string.
 * @return YES if text field has not exceeded its maximum length and is constrained to the edit text characteristics (integer number, currency value or free),
 * NO otherwise.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    if ([textField isKindOfClass:[MOKCurrencyTextField class]]) {
        
        NSScanner* scanner = [NSScanner scannerWithString:string];
        
        if (([string length] == 0) || (([scanner scanInteger:nil] == YES) && ([scanner isAtEnd] == YES))) {
            
            NSString *textFieldText = textField.text;
            
            NSNumberFormatter *numberFormatter = self.validNumberFormatter;
            
            NSString *decimalSeparator = [numberFormatter decimalSeparator];
            NSString *groupingSeparator = [numberFormatter groupingSeparator];
            NSString *replacedString = [textFieldText stringByReplacingCharactersInRange:range withString:string];
            replacedString = [replacedString stringByReplacingOccurrencesOfString:decimalSeparator withString:@""];
            replacedString = [replacedString stringByReplacingOccurrencesOfString:groupingSeparator withString:@""];
            
            if ([replacedString length] > 0) {
                
                BOOL canContainCents = ((MOKCurrencyTextField *)textField).canContainCents;
                NSUInteger maximumNumberOfDecimals = ((MOKCurrencyTextField *)textField).maxDecimalNumbers;
                
                if (canContainCents) {
                    
                    [numberFormatter setDecimalSeparator:decimalSeparator];
                    [numberFormatter setMinimumFractionDigits:maximumNumberOfDecimals];
                    [numberFormatter setMaximumFractionDigits:maximumNumberOfDecimals];
                    
                }
                
                scanner = [NSScanner scannerWithString:replacedString];
                NSDecimal intDecimal;
                [scanner scanDecimal:&intDecimal];
                
                NSDecimal resultDecimal;
                
                if (canContainCents) {
                    
                    NSDecimalMultiplyByPowerOf10(&resultDecimal, &intDecimal, -maximumNumberOfDecimals, NSRoundPlain);
                    
                } else {
                    
                    resultDecimal = intDecimal;
                    
                }
                
                NSDecimalNumber *decimalNumber = [NSDecimalNumber decimalNumberWithDecimal:resultDecimal];
                
                if ((((MOKCurrencyTextField *)textField).maxLength < 0) || ([replacedString length] <= ((MOKCurrencyTextField *)textField).maxLength)) {
                    
                    textField.text = [numberFormatter stringFromNumber:decimalNumber];
                    [delegate_ textField:textField shouldChangeCharactersInRange:range replacementString:string];
                    

                }
                
            } else {
                
                [(MOKCurrencyTextField *)textField resetToZero];
                [delegate_ textField:textField shouldChangeCharactersInRange:range replacementString:string];
                

            }
            
        }
        
    }
    
    return result;

}

/**
 * Asks the delegate if the text field’s current contents should be removed. When text field
 * contains a currency value, text field text is reseted to a zero value and no removal is
 * allowed.
 *
 * @param textField The text field containing the text.
 * @return NO it text field contains a currency, YES otherwise.
 */
- (BOOL)textFieldShouldClear:(UITextField *)textField {

    BOOL result = YES;
    
    if ([textField isKindOfClass:[MOKCurrencyTextField class]]) {
        
        [(MOKCurrencyTextField *)textField clearText];
        result = NO;
        
    }
    
    return result;

}

/**
 * Asks the delegate if editing should begin in the specified text field. Forwards it to the external delegate.
 *
 * @param textField The text field for which editing is about to begin.
 * @return YES if an editing session should be initiated; otherwise, NO to disallow editing.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    BOOL result = YES;
    
    if (delegate_ != nil) {
        
        if ([delegate_ respondsToSelector:@selector(textFieldShouldBeginEditing:)]) {
            
            result = [delegate_ textFieldShouldBeginEditing:textField];
        }
        
    }
    
    if ((result) && ([textField isKindOfClass:[MOKCurrencyTextField class]]) && ([textField.text length] == 0)) {
        
        [(MOKCurrencyTextField *)textField resetToZero];
        
    }
    
    return result;    

}

/**
 * Tells the delegate that editing began for the specified text field. Forwards it to the external delegate.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    [textField setTextAlignment:UITextAlignmentLeft];
    
    if ([textField isKindOfClass:[MOKCurrencyTextField class]]) {
        
        NSString *text = textField.text;
        
        if ([text length] == 0) {
            
            [(MOKCurrencyTextField *)textField clearText];
            
        }
        
    }
    
    if ([delegate_ respondsToSelector:@selector(textFieldDidBeginEditing:)]) {
        
        [delegate_ textFieldDidBeginEditing:textField];
        
    }
        
}

/**
 * Asks the delegate if editing should stop in the specified text field. Forwards it to the external delegate.
 *
 * @param textField The text field for which editing is about to end.
 * @return YES if editing should stop; otherwise, NO if the editing session should continue.
 */
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {

    BOOL result = YES;
    
    if (delegate_ != nil) {
        
        if ([delegate_ respondsToSelector:@selector(textFieldShouldEndEditing:)]) {
            
            result = [delegate_ textFieldShouldEndEditing:textField];
            
        }
        
    }
    
    return result;
    
}

/**
 * Tells the delegate that editing stopped for the specified text field. Forwards it to the external delegate.
 *
 * @param textField The text field for which editing ended.
 */
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if ([textField isKindOfClass:[MOKCurrencyTextField class]]) {
        
        if (![(MOKCurrencyTextField *)textField hasValue]) {

            [textField setLeftViewMode:UITextFieldViewModeNever];
            [textField setTextAlignment:UITextAlignmentLeft];
            textField.text = nil;
            
        }
        
    }
    
    if ([delegate_ respondsToSelector:@selector(textFieldDidEndEditing:)]) {
        
        [delegate_ textFieldDidEndEditing:textField];
        
    }
    
}

/**
 * Asks the delegate if the text field should process the pressing of the return button. Forwards it to the external delegate.
 *
 * @param textField The text field whose return button was pressed.
 * @return YES if the text field should implement its default behavior for the return button; otherwise, NO.
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    BOOL result = YES;
    
    if (delegate_ != nil) {
        
        if ([delegate_ respondsToSelector:@selector(textFieldShouldReturn:)]) {
            
            result = [delegate_ textFieldShouldReturn:textField];
            
        }
        
    }
    
    return result;    

}

#pragma mark -
#pragma mark Properties

/*
 * Returns the valid number formatter.
 *
 * @return The valid number formatter.
 */
- (NSNumberFormatter *)validNumberFormatter {
    
    NSNumberFormatter *result = numberFormatter_;
    
    if (result == nil) {
        
        result = defaultNumberFormatter_;
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation MOKCurrencyTextField

#pragma mark -
#pragma mark Properties

@dynamic numberFormatter;
@dynamic validNumberFormatter;
@dynamic currencySymbol;
@synthesize maxLength = maxLength_;
@synthesize canContainCents = canContainCents_;
@synthesize maxDecimalNumbers = maxDecimalNumbers_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory.
 */
- (void)dealloc {
    
    [currencySymbol_ release];
    currencySymbol_ = nil;
    
    [currencyTextFieldDelegate_ release];
    currencyTextFieldDelegate_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Initialization

/*
 * Initializes the instance.
 */
- (void)initialize {
    
	[currencySymbol_ release];
    currencySymbol_ = [[NSString alloc] init];
    
    maxLength_ = 12;
	[currencyTextFieldDelegate_ release];
    currencyTextFieldDelegate_ = [[MOKCurrencyTextFieldDelegate alloc] init];
    self.delegate = currencyTextFieldDelegate_;
    
    [currencyTextFieldDelegate_ textFieldShouldClear:self];
    
    [self setLeftViewMode:UITextFieldViewModeNever];
    [self setTextAlignment:UITextAlignmentLeft];
    
}

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. The view is initialized.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initialize];
    
}

/**
 * Superclass desiganted initializer. It initializes a MOKCurrencyTextField instance.
 *
 * @param frame The initial view frame.
 * @return The initialized MOKCurrencyTextField instance.
 */
- (id)initWithFrame:(CGRect)frame {
    
    if ((self = [super initWithFrame:frame])) {
        
        [self initialize];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Indicates whethe the text field has a value has a value.
 */
- (BOOL)hasValue {
    
    BOOL result = YES;
    
    NSString *zeroString = [self zeroString];
    NSString *text = self.text;
    
    if (([text length] == 0) || ([text isEqualToString:zeroString])) {
        
        result = NO;
        
    }
    
    return result;
    
}

/*
 * Clears the text field.
 */
- (void)clearText {
    
	self.text = nil;
    
}

/*
 * Resets the content to zero.
 */
- (void)resetToZero {
    
    NSString *zeroString = [self zeroString];
    
    self.text = zeroString;
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Returns the zero string for the control.
 */
- (NSString *)zeroString {
    
    NSString *result = @"0";
    
    if ((canContainCents_) && (maxDecimalNumbers_ > 0)) {
        
        NSMutableString *decimalSection = [NSMutableString string];
        
        for (NSUInteger i = 0; i < maxDecimalNumbers_; i++) {
            
            [decimalSection appendString:@"0"];
            
        }
        
        NSString *decimalSeparator = [currencyTextFieldDelegate_.validNumberFormatter decimalSeparator];
        result = [NSString stringWithFormat:@"0%@%@", decimalSeparator, decimalSection];
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark UITextField selectors

/**
 * Returns the UITextField delegate, obtained from the internal delegate.
 *
 * @return The delegate stored in the internal delegate.
 */
- (id<UITextFieldDelegate>)delegate {
    
    return currencyTextFieldDelegate_.delegate;
    
}

/**
 * Sets the UITextField delegate. Passes it to its internal delegate.
 *
 * @param delegate The new UITextField delegate to set.
 */
- (void)setDelegate:(id <UITextFieldDelegate>)delegate {
    
    if (delegate == currencyTextFieldDelegate_) {
        
        [super setDelegate:delegate];
        
    } else {
        
        currencyTextFieldDelegate_.delegate = delegate;
        
    }
    
}

/**
 * Sets the text of the textfield.
 *
 * @param text The new text to set.
 */
- (void)setText:(NSString *)text {
    
    [super setText:text];
    
    if ([text length] == 0) {
        
        [self setLeftViewMode:UITextFieldViewModeNever];
        [self setTextAlignment:UITextAlignmentLeft];
        
    } else {
        
        [self setLeftViewMode:UITextFieldViewModeAlways];
        [self setTextAlignment:UITextAlignmentLeft];
        
    }
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the number formatter.
 *
 * @return The number formatter.
 */
- (NSNumberFormatter *)numberFormatter {
    
    return currencyTextFieldDelegate_.numberFormatter;
    
}

/*
 * Sets the new number formatter.
 *
 * @param numberFormatter The new number formatter to set.
 */
- (void)setNumberFormatter:(NSNumberFormatter *)numberFormatter {
    
    currencyTextFieldDelegate_.numberFormatter = numberFormatter;
    
    [self resetToZero];
    
}

/*
 * Returns the valid number formatter.
 *
 * @return The valid number formatter.
 */
- (NSNumberFormatter *)validNumberFormatter {
    
    return currencyTextFieldDelegate_.validNumberFormatter;
    
}

/*
 * Returns the currency symbol.
 *
 * @return The currency symbol.
 */
- (NSString *)currencySymbol {
    
    return currencySymbol_;
    
}

/*
 * Sets the currency symbol.
 *
 * @param value The new currency symbol to set.
 */
- (void)setCurrencySymbol:(NSString *)value {

    [currencySymbol_ release];
    currencySymbol_ = [value copy];
    
    CGFloat rightGap = 0.0f;
    
    if (self.borderStyle != UITextBorderStyleRoundedRect) {
        
        rightGap = 10.0f;
        
    }
    
    CGSize size = self.frame.size;
    UIFont *font = self.font;
    
    CGSize labelSize = [currencySymbol_ sizeWithFont:font
                                   constrainedToSize:CGSizeMake(size.width, size.height)];
    UILabel *currencyLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, labelSize.width + rightGap, labelSize.height)] autorelease];
    currencyLabel.backgroundColor = [UIColor clearColor];
    currencyLabel.textAlignment = UITextAlignmentCenter;
    currencyLabel.text = currencySymbol_;
    
    currencyLabel.font = font;
    currencyLabel.textColor = self.textColor;
    
    [self setLeftView:currencyLabel];
    
}

@end
