/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKTextField.h"


//Forward declarations
@class MOKCurrencyTextFieldDelegate;


/**
 * Custom text field with a currency symbol and currency behaviour.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKCurrencyTextField : MOKTextField {
    
@private
    
    /**
     * Currency string.
     */
    NSString *currencySymbol_;
    
    /**
     * Maximum length of this text field.
     */
    NSInteger maxLength_;
    
    /**
     * Can contain cents flag. YES when field can contain cents, NO otherwise.
     */
    BOOL canContainCents_;
    
    /**
     * Maximum number of decimals.
     */
    NSUInteger maxDecimalNumbers_;
    
    /**
     * Delegate of this text field.
     */
    MOKCurrencyTextFieldDelegate *currencyTextFieldDelegate_;
    
}


/**
 * Provides read-write access to the umber formatter to format the amounts.
 */
@property (nonatomic, readwrite, retain) NSNumberFormatter *numberFormatter;

/**
 * Provides read-only access to the valid number formatter, that is, the number formatter to use to format the amounts (either the default one or the
 * custom one)
 */
@property (nonatomic, readonly, retain) NSNumberFormatter *validNumberFormatter;

/**
 * Provides read-write access to the currency string.
 */
@property (nonatomic, readwrite, copy) NSString *currencySymbol;

/**
 * Provides read-write access to the maximum length.
 */
@property (nonatomic, readwrite, assign) NSInteger maxLength;

/**
 * Provides read-write access to the can contain cents flag.
 */
@property (nonatomic, readwrite, assign) BOOL canContainCents;

/**
 * Provides read-write access to the maximum number of decimals.
 */
@property (nonatomic, readwrite, assign) NSUInteger maxDecimalNumbers;


/**
 * Indicates if the text field has a value.
 */
- (BOOL)hasValue;

/**
 * Clears the text field.
 */
- (void)clearText;

/**
 * Resets the content to zero.
 */
- (void)resetToZero;

@end
