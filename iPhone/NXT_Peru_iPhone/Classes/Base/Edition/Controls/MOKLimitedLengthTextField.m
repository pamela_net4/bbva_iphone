/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKLimitedLengthTextField.h"


#pragma mark -

/**
 * Object that performs the delegate operations and limits the text field length.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKLimitedLengthTextFieldDelegate : NSObject <UITextFieldDelegate> {
    
@private
    
    /**
     * External UITextFieldDelegate.
     */
    id<UITextFieldDelegate> delegate_;
    
}

/**
 * Provides read-write access to the external UITextFieldDelegate.
 */
@property (nonatomic, readwrite, assign) id<UITextFieldDelegate> delegate;

@end


#pragma mark -

/**
 * MOKLimitedLengthTextField private extension.
 */
@interface MOKLimitedLengthTextField()

/**
 * Initializes the instance.
 *
 * @private
 */
- (void)initialize;

/**
 * Creates and stores the custom UITextFieldDelegate instance in case it is nil.
 *
 * @private
 */
- (void)initializeCustomDelegate;

@end


#pragma mark -

@implementation MOKLimitedLengthTextFieldDelegate

#pragma mark -
#pragma mark Properties

@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory.
 */
- (void)dealloc {
    
    delegate_ = nil;
    
    [super dealloc];
    
    
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/**
 * Asks the delegate if the specified text should be changed. Used to make sure the text field is visible, and maximum length is not exceeded.
 *
 * @param textField The text field containing the text.
 * @param range The range of characters to be replaced.
 * @param string The replacement string.
 * @return YES if text field has not exceeded its maximum length and is constrained to the edit text characteristics (integer number, currency value or free),
 * NO otherwise.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = YES;
    
    if (delegate_ != nil) {
        
        if ([delegate_ respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
            
            result = [delegate_ textField:textField shouldChangeCharactersInRange:range replacementString:string];
            
        }
        
    }
    
    if ([textField isKindOfClass:[MOKLimitedLengthTextField class]]) {
        
        MOKLimitedLengthTextField *customTextField = (MOKLimitedLengthTextField *)textField;
        NSInteger maxLength = customTextField.maxLength;
        
        if ((result) && (maxLength > 0)) {
            
            NSString *textFieldText = textField.text;
            NSString *replacedString = [textFieldText stringByReplacingCharactersInRange:range withString:string];
            
            if ([replacedString length] > maxLength) {
                
                result = NO;
                
            }
            
        }
    }
    
    return result;

}

/**
 * Asks the delegate if the text field’s current contents should be removed. When text field
 * contains a currency value, text field text is reseted to a zero value and no removal is
 * allowed.
 *
 * @param textField The text field containing the text.
 * @return NO it text field contains a currency, YES otherwise.
 */
- (BOOL)textFieldShouldClear:(UITextField *)textField {

    BOOL result = YES;
    
    if (delegate_ != nil) {
        
        if ([delegate_ respondsToSelector:@selector(textFieldShouldClear:)]) {
            
            result = [delegate_ textFieldShouldClear:textField];
            
        }
        
    }
    
    return result;

}

/**
 * Asks the delegate if editing should begin in the specified text field. Forwards it to the external delegate.
 *
 * @param textField The text field for which editing is about to begin.
 * @return YES if an editing session should be initiated; otherwise, NO to disallow editing.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    BOOL result = YES;
    
    if (delegate_ != nil) {
        
        if ([delegate_ respondsToSelector:@selector(textFieldShouldBeginEditing:)]) {
            
            result = [delegate_ textFieldShouldBeginEditing:textField];
        }
        
    }
    
    return result;    

}

/**
 * Tells the delegate that editing began for the specified text field. Forwards it to the external delegate.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (delegate_ != nil) {
        
        if ([delegate_ respondsToSelector:@selector(textFieldDidBeginEditing:)]) {
            
            [delegate_ textFieldDidBeginEditing:textField];
            
        }
        
    }
        
}

/**
 * Asks the delegate if editing should stop in the specified text field. Forwards it to the external delegate.
 *
 * @param textField The text field for which editing is about to end.
 * @return YES if editing should stop; otherwise, NO if the editing session should continue.
 */
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {

    BOOL result = YES;
    
    if (delegate_ != nil) {
        
        if ([delegate_ respondsToSelector:@selector(textFieldShouldEndEditing:)]) {
            
            result = [delegate_ textFieldShouldEndEditing:textField];
            
        }
        
    }
    
    return result;
    
}

/**
 * Tells the delegate that editing stopped for the specified text field. Forwards it to the external delegate.
 *
 * @param textField The text field for which editing ended.
 */
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (delegate_ != nil) {
        
        if ([delegate_ respondsToSelector:@selector(textFieldDidEndEditing:)]) {
            
            [delegate_ textFieldDidEndEditing:textField];
            
        }
        
    }
    
}

/**
 * Asks the delegate if the text field should process the pressing of the return button. Forwards it to the external delegate.
 *
 * @param textField The text field whose return button was pressed.
 * @return YES if the text field should implement its default behavior for the return button; otherwise, NO.
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    BOOL result = YES;
    
    if (delegate_ != nil) {
        
        if ([delegate_ respondsToSelector:@selector(textFieldShouldReturn:)]) {
            
            result = [delegate_ textFieldShouldReturn:textField];
            
        }
        
    }
    
    return result;    

}

@end


#pragma mark -

@implementation MOKLimitedLengthTextField

#pragma mark -
#pragma mark Properties

@synthesize maxLength = maxLength_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory.
 */
- (void)dealloc {
    
    [limitedLengthTextFieldDelegate_ release];
    limitedLengthTextFieldDelegate_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Initialization

/*
 * Initializes the instance.
 */
- (void)initialize {
    
    maxLength_ = 7;
    
    [self initializeCustomDelegate];
    
}


/*
 * Creates and stores the custom UITextFieldDelegate instance in case it is nil.
 */
- (void)initializeCustomDelegate {
    
    if (limitedLengthTextFieldDelegate_ == nil) {
        
        limitedLengthTextFieldDelegate_ = [[MOKLimitedLengthTextFieldDelegate alloc] init];
        self.delegate = limitedLengthTextFieldDelegate_;
        
    }
    
}

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. The view is initialized.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initialize];
    
}

/**
 * Superclass desiganted initializer. It initializes a MOKTextField instance.
 *
 * @param frame The initial view frame.
 * @return The initialized MOKTextField instance.
 */
- (id)initWithFrame:(CGRect)frame {
    
    if ((self = [super initWithFrame:frame])) {
        
        [self initialize];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark UITextField selectors

/**
 * Returns the UITextField delegate, obtained from the internal delegate.
 *
 * @return The delegate stored in the internal delegate.
 */
- (id<UITextFieldDelegate>)delegate {
    
    return limitedLengthTextFieldDelegate_.delegate;
    
}

/**
 * Sets the UITextField delegate. Passes it to its internal delegate.
 *
 * @param delegate The new UITextField delegate to set.
 */
- (void)setDelegate:(id <UITextFieldDelegate>)delegate {
    
    [self initializeCustomDelegate];
    
    if (delegate == limitedLengthTextFieldDelegate_) {
        
        [super setDelegate:delegate];
        
    } else {
        
        limitedLengthTextFieldDelegate_.delegate = delegate;
        
    }
    
}

@end
