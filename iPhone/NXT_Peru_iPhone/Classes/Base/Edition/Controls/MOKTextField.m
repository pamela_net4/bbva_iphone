/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKTextField.h"


#pragma mark -

/**
 * MOKTextField private extension
 */
@interface MOKTextField()

/**
 * Returns a rectangle once the padding is applied to it.
 *
 * @param rect The original rectable.
 * @return The rectangle once the padding is applied.
 */
- (CGRect)applyPaddingToRect:(CGRect)rect;

@end


#pragma mark -

@implementation MOKTextField

#pragma mark -
#pragma mark Properties

@synthesize textPadding = textPadding_;
@synthesize inputView = inputView_;
@synthesize inputAccessoryView = inputAccessoryView_;
@synthesize fillFormMessage = fillFormMessage_;
@dynamic isEmpty;
@synthesize previousEditView = previousEditView_;
@synthesize nextEditView = nextEditView_;
@synthesize editViewManager = editViewManager_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory.
 */
- (void)dealloc {
    
    [inputView_ release];
    inputView_ = nil;    
    
    [inputAccessoryView_ release];
    inputAccessoryView_ = nil;    
    
    [fillFormMessage_ release];
    fillFormMessage_ = nil;    
    
    [previousEditView_ release];
    previousEditView_ = nil;
    
    [nextEditView_ release];
    nextEditView_ = nil;
    
    editViewManager_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark UITextField methods

/**
 * Returns the drawing rectangle for the text field’s text. It adds a padding to the returned text field.
 *
 * @param bounds The bounding rectangle of the receiver.
 * @return The computed drawing rectangle for the label’s text.
 */
- (CGRect)textRectForBounds:(CGRect)bounds {
    
    CGRect result = [super textRectForBounds:bounds];
    result = [self applyPaddingToRect:result];
    
    return result;
    
}

/**
 * Returns the rectangle in which editable text can be displayed. It adds a padding to the returned text field.
 *
 * @param bounds The bounding rectangle of the receiver.
 * @return The computed editing rectangle for the text.
 */
- (CGRect)editingRectForBounds:(CGRect)bounds {
    
    CGRect result = [super textRectForBounds:bounds];
    result = [self applyPaddingToRect:result];
    
    return result;
    
}

#pragma mark -
#pragma mark Padding management

/*
 * Returns a rectangle once the padding is applied to it.
 */
- (CGRect)applyPaddingToRect:(CGRect)rect {
    
    CGRect result = rect;
    
    result.origin.x = CGRectGetMinX(rect) + textPadding_;
    result.size.width = CGRectGetWidth(rect) - (2.0f * textPadding_);
    
    return result;
    
}

#pragma mark -
#pragma mark UIResponder selectors

/**
 * Notifies the receiver that it is about to become first responder in its window. When the superclass allows it, the edit view manager
 * is notified about the view becoming first responder.
 *
 * @return YES if the receiver became first-responder, NO otherwise.
 */
- (BOOL)becomeFirstResponder {
    
    BOOL result = [super becomeFirstResponder];
    
    if (result) {
        
        [self.window makeKeyWindow];
        [editViewManager_ editViewWillBecomeFirstResponder:self];
        
    }
    
    return result;
    
}

/**
 * Notifies the receiver that it has been asked to relinquish its status as first responder in its window. When the superclass allows it, the
 * edit view manager is notified about the view resigining first responder status.
 *
 * @return YES if the receiver resigned first-responder status, NO otherwise.
 */
- (BOOL)resignFirstResponder {
    
    BOOL result = [super resignFirstResponder];
    
    if (result) {
        
        [editViewManager_ editViewDidResignFirstResponder:self];
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark MOKEditView protocol selectors

/**
 * Returns the input view type. In this case, it is the keyboard element.
 *
 * @return The input view type.
 */
- (MOKEditViewInputViewType)inputViewType {
    
    return mevivtKeyboard;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the is empty flag.
 *
 * @return The is empty flag.
 */
- (BOOL)isEmpty {
    
    return (!([[self.text stringByReplacingOccurrencesOfString:@" " withString:@""] length] > 0));
    
}

@end
