/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKTextView.h"


/**
 * Defines the placeholder offset.
 */
#define PLACEHOLDER_OFFSET                          8.0f


#pragma mark -

/**
 * MOKTextView private extension.
 */
@interface MOKTextView()

/**
 * Registers to modification notifications when not already registered.
 *
 * @private
 */
- (void)registerToModificationNotifications;

/**
 * Invoked by framework when the tex view modification notification is received. The view is set to display, in order to remove or display the placeholder.
 *
 * @param notification The NSNotification instance containing the notification information.
 * @private
 */
- (void)textViewModified:(NSNotification *) notification;

@end


#pragma mark -

@implementation MOKTextView

#pragma mark -
#pragma mark Properties

@synthesize inputView = inputView_;
@synthesize inputAccessoryView = inputAccessoryView_;
@dynamic isEmpty;
@synthesize placeholder = placeholder_;
@synthesize placeholderColor = placeholderColor_;
@synthesize previousEditView = previousEditView_;
@synthesize nextEditView = nextEditView_;
@synthesize editViewManager = editViewManager_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory.
 */
- (void)dealloc {
    
    if (registeredToModificationsNotifications_) {
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UITextViewTextDidChangeNotification
                                                      object:self];
        
        registeredToModificationsNotifications_ = NO;
        
    }
    
    [inputView_ release];
    inputView_ = nil;    
    
    [inputAccessoryView_ release];
    inputAccessoryView_ = nil;    
    
    [placeholder_ release];
    placeholder_ = nil;
    
    [placeholderColor_ release];
    placeholderColor_ = nil;
    
    [previousEditView_ release];
    previousEditView_ = nil;
    
    [nextEditView_ release];
    nextEditView_ = nil;
    
    editViewManager_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Register to the modification notification.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    [self registerToModificationNotifications];
    
}

/**
 * Superclass designated initialized. It initialized a MOKTextView instance and registers to the modification notification.
 *
 * @param frame 
 * @return The initialized MOKTextView instance.
 */
- (id)initWithFrame:(CGRect)frame {
    
    if ((self = [super initWithFrame:frame])) {
        
        [self registerToModificationNotifications];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark UIView selectors

/**
 * Draws the receiver’s image within the passed-in rectangle. When no text is present, the placeholder is displayed.
 *
 * @param rect The portion of the view’s bounds that needs to be updated.
 */
- (void)drawRect:(CGRect)rect {
    
    [super drawRect:rect];
    
    if ((![self hasText]) && ([placeholder_ length] > 0)) {
        
        UIFont *font = [self font];
        UIColor *color = placeholderColor_;
        
        if (color == nil) {
            
            color = [UIColor grayColor];
            
        }
        
        CGSize placeholderSize = [placeholder_ sizeWithFont:font];
        CGRect frame = [self frame];
        CGFloat availableWidth = CGRectGetWidth(frame) - (2.0f * PLACEHOLDER_OFFSET);
        CGFloat placeholderWidth = placeholderSize.width;
        
        if (placeholderWidth > availableWidth) {
            
            placeholderWidth = availableWidth;
            
        }
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSaveGState(context);
        
        CGContextSetStrokeColorWithColor(context, [color CGColor]);
        CGContextSetFillColorWithColor(context, [color CGColor]);
        
        [placeholder_ drawInRect:CGRectMake(PLACEHOLDER_OFFSET,
                                            PLACEHOLDER_OFFSET,
                                            placeholderWidth,
                                            placeholderSize.height)
                        withFont:font];
        
        CGContextRestoreGState(context);
        
    }
    
}

#pragma mark -
#pragma mark Notifications management

/*
 * Registers to modification notifications when not already registered.
 */
- (void)registerToModificationNotifications {
    
    if (!registeredToModificationsNotifications_) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textViewModified:)
                                                     name:UITextViewTextDidChangeNotification
                                                   object:self];
        
    }
    
}

/*
 * Invoked by framework when the tex view modification notification is received. The view is set to display, in order to remove or display the placeholder.
 */
- (void)textViewModified:(NSNotification *) notification {
    
    [self setNeedsDisplay];
    
}

#pragma mark -
#pragma mark UIResponder selectors

/**
 * Notifies the receiver that it is about to become first responder in its window. When the superclass allows it, the edit view manager
 * is notified about the view becoming first responder.
 *
 * @return YES if the receiver became first-responder, NO otherwise.
 */
- (BOOL)becomeFirstResponder {
    
    BOOL result = [super becomeFirstResponder];
    
    if (result) {
        [self.window makeKeyWindow];
        [editViewManager_ editViewWillBecomeFirstResponder:self];
        
    }
    
    return result;
    
}

/**
 * Notifies the receiver that it has been asked to relinquish its status as first responder in its window. When the superclass allows it, the
 * edit view manager is notified about the view resigining first responder status.
 *
 * @return YES if the receiver resigned first-responder status, NO otherwise.
 */
- (BOOL)resignFirstResponder {
    
    BOOL result = [super resignFirstResponder];
    
    if (result) {
        
        [self.window makeKeyWindow];
        [editViewManager_ editViewDidResignFirstResponder:self];
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark MOKEditView protocol selectors

/**
 * Returns the input view type. In this case, it is the keyboard element.
 *
 * @return The input view type.
 */
- (MOKEditViewInputViewType)inputViewType {
    
    return mevivtKeyboard;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the is empty flag.
 *
 * @return The is empty flag.
 */
- (BOOL)isEmpty {
    
    return (!([[self.text stringByReplacingOccurrencesOfString:@" " withString:@""] length] > 0));
    
}

/*
 * Sets the new placeholder and marks the view to display.
 *
 * @param placeholder The new placeholder to set.
 */
- (void)setPlaceholder:(NSString *)placeholder {
    
    if (placeholder != placeholder_) {
        
        [placeholder_ release];
        placeholder_ = nil;
        placeholder_ = [placeholder copy];
        
        [self setNeedsDisplay];
        
    }
    
}

/*
 * Sets the new placeholder color and marks the view to display.
 *
 * @param placeholderColor The new placeholder color to set.
 */
- (void)setPlaceholderColor:(UIColor *)placeholderColor {
    
    if (placeholderColor != placeholderColor_) {
        
        [placeholderColor retain];
        [placeholderColor_ release];
        placeholderColor_ = placeholderColor;
        
        [self setNeedsDisplay];
        
    }
    
}

@end
