/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKButton.h"
#import "MOKEditViewProtocols.h"


//Forward declarations
@class MOKStringListSelectionButton;


/**
 * MOKStringListSelection button delegate protocol.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol MOKStringListSelectionButtonDelegate

@required

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton;

@end


/**
 * Custom button to edit using a selection from a string list. It is a MOKEditView, so it can be used with the MOKEditableViewController.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKStringListSelectionButton : MOKButton <MOKEditView> {
    
@private
    
    /**
     * Custom input view to display when the view becomes first responder.
     */
    UIView *inputView_;
    
    /**
     * Custom input accessory view to display when the view becomes first responder.
     */
    UIView *inputAccessoryView_;
    
    /**
     * List containing the strings to select from. All objects are NSString instances.
     */
    NSMutableArray *optionStringList_;
    
    /**
     * Selected index.
     */
    NSInteger selectedIndex_;
    
    /**
     * String to display when no selection is present.
     */
    NSString *noSelectionText_;
    
    /**
     * Previous edit view.
     */
    id<MOKEditView> previousEditView_;
    
    /**
     * Next edit view.
     */
    id<MOKEditView> nextEditView_;

    /**
     * String list selection button delegate.
     */
    id<MOKStringListSelectionButtonDelegate> stringListSelectionButtonDelegate_;
    
    /**
     * Edit view manager.
     */
    id<MOKEditViewsManager> editViewManager_;

}


/**
 * Provides read-write access to the input view.
 */
@property (atomic, readwrite, retain) UIView *inputView;

/**
 * Provides read-write access to the input accesory view.
 */
@property (atomic, readwrite, retain) UIView *inputAccessoryView;

/**
 * Provides read-write access to the string list to select from.
 */
@property (nonatomic, readwrite, retain) NSArray *optionStringList;

/**
 * Provides read-write access to the selected index. To unselect the selection, index must be out of bounds from option string list.
 */
@property (nonatomic, readwrite, assign) NSInteger selectedIndex;

/**
 * Provides read-write access to the string to display when no selection is present.
 */
@property (nonatomic, readwrite, copy) NSString *noSelectionText;

/**
 * Provides read-write access to the previous edit view in the chain.
 */
@property (nonatomic, readwrite, retain) id<MOKEditView> previousEditView;

/**
 * Provides read-write access to the next edit view in the chain.
 */
@property (nonatomic, readwrite, retain) id<MOKEditView> nextEditView;

/**
 * Provides read-write access to the string list selection button delegate.
 */
@property (nonatomic, readwrite, assign) id<MOKStringListSelectionButtonDelegate> stringListSelectionButtonDelegate;

/**
 * Provides read-write access to the edit view manager.
 */
@property (nonatomic, readwrite, assign) id<MOKEditViewsManager> editViewManager;

@end
