/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "MOKEditViewProtocols.h"


/**
 * Specific Movilok text field to return the correct text bounds. It implements the MOKEditView protocol to be used in editable view controllers.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKTextField : UITextField <MOKEditView> {
    
@private
    
    /**
     * The amount of padding to apply to the text bounds.
     */
    CGFloat textPadding_;

    /**
     * Custom input view that shows when the button becomes first responder.
     */
    UIView *inputView_;
    
    /**
     * Custom input accessory view that shows when the button becomes first responder.
     */
    UIView *inputAccessoryView_;
    
    /**
     * Message to show when this control is empty.
     */
    NSString *fillFormMessage_;
    
    /**
     * Previous edit view.
     */
    id<MOKEditView> previousEditView_;
    
    /**
     * Next edit view.
     */
    id<MOKEditView> nextEditView_;
    
    /**
     * Edit view manager.
     */
    id<MOKEditViewsManager> editViewManager_;
    
}


/**
 * Provides read-write access to the text padding.
 */
@property (nonatomic, nonatomic, readwrite) CGFloat textPadding;

/**
 * Provides read-write access to the input view.
 */
@property (atomic, readwrite, retain) UIView *inputView;

/**
 * Provides read-write access to the input accesory view.
 */
@property (atomic, readwrite, retain) UIView *inputAccessoryView;

/**
 * Provides read-write access to the title.
 */
@property (nonatomic, readwrite, copy) NSString *fillFormMessage;

/**
 * Provides read-write access to the is empty flag. YES when the text field has only white space characters or is empty, NO otherwise
 */
@property (nonatomic, readonly, assign) BOOL isEmpty;

/**
 * Provides read-write access to the previous edit view in the chain.
 */
@property (nonatomic, readwrite, retain) id<MOKEditView> previousEditView;

/**
 * Provides read-write access to the next edit view in the chain.
 */
@property (nonatomic, readwrite, retain) id<MOKEditView> nextEditView;

/**
 * Provides read-write access to the edit view manager.
 */
@property (nonatomic, readwrite, assign) id<MOKEditViewsManager> editViewManager;

@end
