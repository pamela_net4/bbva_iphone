/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKTextField.h"


//Forward declarations
@class MOKLimitedLengthTextFieldDelegate;


/**
 * Custom text field to control the maximum length allowed in the text field.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKLimitedLengthTextField : MOKTextField {
    
@private
    
    /**
     * Maximum length of this text field.
     */
    NSInteger maxLength_;
    
    /**
     * Delegate of this text field.
     */
    MOKLimitedLengthTextFieldDelegate *limitedLengthTextFieldDelegate_;
    
}


/**
 * Provides read-write access to the maximum length.
 */
@property (nonatomic, readwrite, assign) NSInteger maxLength;

@end
