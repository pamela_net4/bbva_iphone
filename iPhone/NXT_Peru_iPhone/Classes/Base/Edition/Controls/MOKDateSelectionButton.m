/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKDateSelectionButton.h"


#pragma mark -

/**
 * MOKDateSelectionButton private extension
 */
@interface MOKDateSelectionButton()

/**
 * Initiliazes the date selection button.
 *
 * @private
 */
- (void)initializeDateSelectionButton;

/**
 * Invoked by framework when user taps on the button. It is set first responder
 *
 * @private
 */
- (void)buttonTapped;

@end


#pragma mark -

@implementation MOKDateSelectionButton

#pragma mark -
#pragma mark Properties

@synthesize inputView = inputView_;
@synthesize inputAccessoryView = inputAccessoryView_;
@synthesize selectedDate = selectedDate_;
@synthesize minimumDate = minimumDate_;
@synthesize maximumDate = maximumDate_;
@synthesize noSelectionText = noSelectionText_;
@synthesize previousEditView = previousEditView_;
@synthesize nextEditView = nextEditView_;
@synthesize dateSelectionButtonDelegate = dateSelectionButtonDelegate_;
@synthesize editViewManager = editViewManager_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [inputView_ release];
    inputView_ = nil;
    
    [inputAccessoryView_ release];
    inputAccessoryView_ = nil;
    
    [selectedDate_ release];
    selectedDate_ = nil;
    
    [minimumDate_ release];
    minimumDate_ = nil;
    
    [maximumDate_ release];
    maximumDate_ = nil;
    
    [noSelectionText_ release];
    noSelectionText_ = nil;
    
    [previousEditView_ release];
    previousEditView_ = nil;
    
    [nextEditView_ release];
    nextEditView_ = nil;
    
    dateSelectionButtonDelegate_ = nil;
    
    editViewManager_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Initilization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. The view is initialized.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initializeDateSelectionButton];
    
}

/**
 * Designated initializer. The MOKStringListSelectionButton instance is initialized.
 *
 * @param frame The view original frame.
 * @return The initialized MOKStringListSelectionButton instance.
 */
- (id)initWithFrame:(CGRect)frame {
    
    if ((self = [super initWithFrame:frame])) {
        
        [self initializeDateSelectionButton];
        
    }
    
    return self;
    
}

/*
 * Initiliazes the control.
 */
- (void)initializeDateSelectionButton {
    
    self.imageOnTheRight = YES;
    
    [selectedDate_ release];
    selectedDate_ = nil;
    
    [minimumDate_ release];
    minimumDate_ = nil;
    
    [maximumDate_ release];
    maximumDate_ = nil;
    
    [self addTarget:self
             action:@selector(buttonTapped)
   forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark -
#pragma mark User interaction

/*
 * Invoked by framework when user taps on the button. It is set first responder
 */
- (void)buttonTapped {
    
    [self becomeFirstResponder];
    
}

#pragma mark -
#pragma mark UIResponder selectors

/**
 * Tells the receiver when one or more fingers are raised from a view or window. Disables the listener notifications
 * When the control is first responder we force the control to maintain highlighted bacause when a touch ended the
 * control goes to normal state.
 *
 * @param touches A set of UITouch instances that represent the touches for the ending phase of the event represented by event.
 * @param event An object representing the event to which the touches belong.
 */
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [super touchesEnded:touches withEvent:event];
    
    if ([self isFirstResponder]) {
        
        self.highlighted = YES;
        
    }
    
}

#pragma mark -
#pragma mark UIResponder selectors

/**
 * Returns a Boolean value indicating whether the receiver can become first responder. This implementation returns YES to allow becoming first responder.
 *
 * @return YES if the receiver can become the first responder, NO otherwise.
 */
- (BOOL)canBecomeFirstResponder {
    
    return YES;
    
}

/**
 * Notifies the receiver that it is about to become first responder in its window. When the superclass allows it, the edit view manager
 * is notified about the view becoming first responder.
 *
 * @return YES if the receiver became first-responder, NO otherwise.
 */
- (BOOL)becomeFirstResponder {
    
    BOOL result = [super becomeFirstResponder];
    
    if (result) {
        
        self.highlighted = YES;
        [editViewManager_ editViewWillBecomeFirstResponder:self];
        
    }
    
    return result;
    
}

/**
 * Returns a Boolean value indicating whether the receiver is willing to relinquish first-responder status. This implementation returns YES in order to be able to stop being first responder
 *
 * @return YES if the receiver can resign first-responder status, NO otherwise.
 */
- (BOOL)canResignFirstResponder {
    
    return YES;
    
}

/**
 * Notifies the receiver that it has been asked to relinquish its status as first responder in its window. When the superclass allows it, the
 * edit view manager is notified about the view resigining first responder status.
 *
 * @return YES if the receiver resigned first-responder status, NO otherwise.
 */
- (BOOL)resignFirstResponder {
    
    BOOL result = [super resignFirstResponder];
    
    if (result) {
        
        self.highlighted = NO;
        [editViewManager_ editViewDidResignFirstResponder:self];
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark MOKEditView protocol selectors

/**
 * Returns the input view type. In this case, it is the string list element.
 *
 * @return The input view type.
 */
- (MOKEditViewInputViewType)inputViewType {
    
    return mevivtDatePicker;
    
}

#pragma mark -
#pragma mark Properties selections

/**
 * Sets the selected date.
 *
 * @param selectedDate The new selected date to set
 */
- (void)setSelectedDate:(NSDate *)selectedDate {

    if (selectedDate != selectedDate_) {
        
        [selectedDate_ release];
        selectedDate_ = nil;
        selectedDate_ = [selectedDate copy];
        
        [dateSelectionButtonDelegate_ dateSelectionButtonSelectedDateChanged:self];
        
    }
    
    NSString *title;
    
    if (selectedDate_ == nil) {
        
        title = noSelectionText_;
        
    } else {
        
        title = [dateSelectionButtonDelegate_ formatSelectedDate:selectedDate_];
        
    }

    [self setTitle:title
          forState:UIControlStateNormal];
    
}


/*
 * Sets the new no selection text.
 *
 * @param noSelectionText The new no selection text to set.
 */
- (void)setNoSelectionText:(NSString *)noSelectionText {
    
    if (noSelectionText != noSelectionText_) {
        
        [noSelectionText_ release];
        noSelectionText_ = nil;
        noSelectionText_ = [noSelectionText copy];
        
    }
    
    if (selectedDate_ == nil) {
        
        [self setTitle:noSelectionText_
              forState:UIControlStateNormal];
        
    }
    
}

@end
