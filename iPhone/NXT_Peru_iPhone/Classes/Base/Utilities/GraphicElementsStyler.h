/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>
#import "UIColor+BBVA_Colors.h"

/**
 * Graphic elements styler class provides static selectors to style different application interface elements. This is
 * a centralized way to change application style when needed
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GraphicElementsStyler : NSObject {

}


/**
 * Styles a label with a given font size and text color
 *
 * @param label The label to style
 * @param fontSize The font size
 * @param textColor The text color
 */
+ (void)styleLabel:(UILabel *)label
      withFontSize:(CGFloat)fontSize
             color:(UIColor *)textColor;

/**
 * Styles a label with a given bold font size and text color
 *
 * @param label The label to style
 * @param fontSize The font size
 * @param textColor The text color
 */
+ (void)styleLabel:(UILabel *)label
  withBoldFontSize:(CGFloat)fontSize
             color:(UIColor *)textColor;

/**
 * Styles a label with a given font text color
 *
 * @param label The label to style
 * @param font The font to style
 * @param textColor The text color
 */
+ (void)styleLabel:(UILabel *)label
          withFont:(UIFont *)font
             color:(UIColor *)textColor;


/**
 * Styles a navigation bar with Compass brand colors
 *
 * @param navigationBar The navigation bar to style
 */
+ (void)styleNavigationBar:(UINavigationBar *)navigationBar;


/**
 * Returns the application normal font for a given size
 *
 * @param fontSize The font size to return
 * @return The application normal font for the given size
 */
+ (UIFont *)normalFontForSize:(CGFloat)fontSize;

/**
 * Returns the application bold font for a given size
 *
 * @param fontSize The font size to return
 * @return The application bold font for the given size
 */
+ (UIFont *)boldFontForSize:(CGFloat)fontSize;

/**
 * Returns the application normal font name
 *
 * @return The application normal font name
 */
+ (NSString *)normalFontName;

@end
