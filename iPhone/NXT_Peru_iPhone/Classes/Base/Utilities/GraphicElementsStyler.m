/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GraphicElementsStyler.h"
#import "ImagesCache.h"


#pragma mark -

@implementation GraphicElementsStyler

#pragma mark -
#pragma mark Label styling

/*
 * Styles a label with a given font size and text color
 */
+ (void)styleLabel:(UILabel *)label
      withFontSize:(CGFloat)fontSize
             color:(UIColor *)textColor {
    
    UIFont *font = [GraphicElementsStyler normalFontForSize:fontSize];
    label.font = font;
    label.textColor = textColor;
    label.highlightedTextColor = textColor;
    
}

/*
 * Styles a label with a given bold font size and text color
 */
+ (void)styleLabel:(UILabel *)label
  withBoldFontSize:(CGFloat)fontSize
             color:(UIColor *)textColor {
    
    UIFont *font = [GraphicElementsStyler boldFontForSize:fontSize];
    label.font = font;
    label.textColor = textColor;
    label.highlightedTextColor = textColor;
    
}

/*
 * Styles a label with a given font text color
 */
+ (void)styleLabel:(UILabel *)label
          withFont:(UIFont *)font
             color:(UIColor *)textColor {
    
    label.font = font;
    label.textColor = textColor;
    label.highlightedTextColor = textColor;
    
}

#pragma mark -
#pragma mark Navigation bar styling

/*
 * Styles a navigation bar with Compass brand colors
 */
+ (void)styleNavigationBar:(UINavigationBar *)navigationBar {
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        navigationBar.tintColor = [UIColor BBVABlueSpectrumColor];
    }
    else{
        navigationBar.tintColor = [UIColor BBVAWhiteColor];
    }

}

#pragma mark -
#pragma mark Font management

/*
 * Returns the application normal font for a given size
 */
+ (UIFont *)normalFontForSize:(CGFloat)fontSize {
    
    return [UIFont systemFontOfSize:fontSize];
    
}

/*
 * Returns the application bold font for a given size
 */
+ (UIFont *)boldFontForSize:(CGFloat)fontSize {
    
    return [UIFont boldSystemFontOfSize:fontSize];
    
}

/*
 * Returns the application normal font name
 */
+ (NSString *)normalFontName {
    
    UIFont *font = [GraphicElementsStyler normalFontForSize:12.0f];
    return font.fontName;
    
}

@end
