//
//  FMDBUtil.m
//  NXT_Peru_iPhone
//
//  Created by usuario on 16/07/13.
//  Copyright (c) 2013
//

#import "FMDBUtil.h"

@implementation FMDBUtil

+(FMDBUtil*)fnDameInstanciaFMDBUtil
{
    static FMDBUtil *_sharedClient = nil;
    _sharedClient = [[FMDBUtil alloc] init];

    
    return _sharedClient;
}

+ (NSString *)fnDevuelveBDPath{
    
    NSString *nombreBaseDatosString = @"nxt_peru.sqlite";
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
    NSString *databasePathString = [documentsDir stringByAppendingPathComponent:nombreBaseDatosString];
    
    return databasePathString;
}

-(id)init
{
    self = [super init];
    
    if(self)
    {
     //   NSString *rutaArchivo=
        self.conectordb = [FMDatabase databaseWithPath:[FMDBUtil fnDevuelveBDPath]];
        self.conectordb.traceExecution = YES;
    }
    
    return self;
}


-(FMResultSet*)fnDameConsulta:(NSString*)consultaString
{
    FMResultSet *resultadoResultSet=nil;
    
    if ([self.conectordb open])
    {
         NSMutableString *queryString = [NSMutableString stringWithString:consultaString];
        resultadoResultSet = [self.conectordb executeQuery:queryString];

    }
    
    //[self.conectordb close];
    return resultadoResultSet;
}

-(BOOL)fnEjecutaActualizacion:(NSString*)actualizacionString{
    
    BOOL exito = NO;
    if ([self.conectordb open])
    {
        exito = [self.conectordb executeUpdate:actualizacionString];
        [self.conectordb close];
    }
    
    return exito;
}

-(int)fnDameMaxIdentity:(NSString*)nombreEntidad identity:(NSString *)nombreCampoIdentity{
    
    FMResultSet *resultadoResultSet=nil;
    
    int max = 0;
    
    if([self.conectordb open])
    {
        NSString *consultaString = [NSString stringWithFormat:@"Select MAX(%@) AS Max From %@",nombreCampoIdentity, nombreEntidad];
        resultadoResultSet = [self.conectordb executeQuery:consultaString];
        
        if ([resultadoResultSet next]) {
            max = [resultadoResultSet intForColumn:@"Max"];
        }
        
    }
    
    [self.conectordb close];
    
    return max;
}

-(void)fnCerrarUtilConexion
{
    [self.conectordb close];
}

-(void)fnRemoverEntidad:(NSString*)nombreEntidad conNombreIdentificador:(NSString*)nombreIdentificador eIdentificador:(NSString*)entidadId
{
    if([self.conectordb open])
    {
        NSString *consultaString = [NSString stringWithFormat:@"delete from %@ Where %@ = %@",nombreEntidad,nombreIdentificador,entidadId];
        [self.conectordb executeUpdate:consultaString];
        [self.conectordb close];
    }

}


@end
