//
//  FMDBUtil.h
//  NXT_Peru_iPhone
//
//  Created by usuario on 16/07/13.
//  Copyright (c) 2013
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"

@interface FMDBUtil : NSObject

@property(nonatomic ,strong) FMDatabase *conectordb;

/** Método que devuelve la instancia de la clase
    @return FMDBUtil Instancia de la clase
    @author Jose Luis Contreras
 **/
+(FMDBUtil*)fnDameInstanciaFMDBUtil;

/** Método que devuelve la dirección donde se encuentra la base de datos en el dispositivo
    @return NSString dirección de la base de datos
    @author Jose Luis Contreras
 **/
+ (NSString *)fnDevuelveBDPath;


/**
    Metodo que consulta a la base de datos
    
    @param consultaString consulta que se desea ejecutar
    @return FMResultSet Entidad con los resultados de la consulta
    @author Jose Luis Contreras Sánchez
 */
-(FMResultSet*)fnDameConsulta:(NSString*)consultaString;

/**
    Método que cierra la conexion de la instancia
    
    @author Jose Luis Contreras Sánchez
 */
-(void)fnCerrarUtilConexion;


/**
    Metodo que remueve los registros en una entidad segun la informacion de un campo
    
    @param nombreEntidad nombre de la tabla
    @param nombreIdentificador nombre del campo de la tabla
    @param entidadId informacion del campo 
    @author Jose Luis Contreras Sánchez
    
 */
-(void)fnRemoverEntidad:(NSString*)nombreEntidad conNombreIdentificador:(NSString*)nombreIdentificador eIdentificador:(NSString*)entidadId;

-(int)fnDameMaxIdentity:(NSString*)nombreEntidad identity:(NSString *)nombreCampoIdentity;


-(BOOL)fnEjecutaActualizacion:(NSString*)actualizacionString;
@end
