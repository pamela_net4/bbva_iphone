/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

/**
 * Protocol to be implemented by the children of MOKViewController
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol MOKViewControllerProtocol <NSObject>

@optional

/**
 * Method to listen log out notification
 *
 * @param notification The NSNotification object
 */
- (void)sessionLoggedOut:(NSNotification *)notification;

@end
