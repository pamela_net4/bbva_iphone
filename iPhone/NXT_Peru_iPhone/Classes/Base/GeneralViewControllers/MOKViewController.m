/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKViewController.h"
#import "MOKViewController+protected.h"
#import "MOKViewControllerProtocol.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "GraphicElementsStyler.h"
#import "ImagesCache.h"
#import "MOKNavigationItem.h"
#import "Session.h"
#import "StringKeys.h"
#import "Tools.h"


/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"MOKViewController"


#pragma mark -

/**
 * CompassViewController private extension
 */
@interface MOKViewController()

/**
 * Initializes the internal flags.
 *
 * @private
 */
- (void)initializeMOKViewControllerInternalFlags;

/**
 * Invoked by framework when a user logged out notification is received. Subclasses implementing the MOKViewControllerProtocol are notified.
 *
 * @param notification The NSNotification instance containing the notification information
 * @private
 */
- (void)userLoggedOutNotification:(NSNotification *)notification;

@end


#pragma mark -

@implementation MOKViewController

#pragma mark -
#pragma mark Properties

@dynamic appDelegate;
@synthesize iOS5Version = iOS5Version_;
@synthesize iPhoneDevice = iPhoneDevice_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {

    [customNavigationItem_ release];
    customNavigationItem_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initializes the internal flags.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initializeMOKViewControllerInternalFlags];
    
}

/**
 * Superclass designated initializer. Initializes a MOKViewController instance by setting its flags.
 *
 * @param nibNameOrNil The name of the nib file to associate with the view controller.
 * @param nibBundleOrNil The bundle in which to search for the nib file.
 * @return The initialized MOKViewController instance.
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if ((self = [super initWithNibName:nibNameOrNil
                                bundle:nibBundleOrNil])) {
        
        [self initializeMOKViewControllerInternalFlags];
        
    }
    
    return self;
    
}

/*
 * Creates and returns an autoreleased MOKViewController constructed from a NIB file.
 */
+ (MOKViewController *)mokViewController {
    
    MOKViewController *result =  [[[MOKViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

/*
 * Initializes the internal flags.
 */
- (void)initializeMOKViewControllerInternalFlags {
    
    iPhoneDevice_ = YES;
    
#ifdef UI_USER_INTERFACE_IDIOM
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        iPhoneDevice_ = NO;
        
    }
    
#endif
    
    NSString *iOSVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([iOSVersion compare:@"5.0" options:NSNumericSearch] != NSOrderedAscending) {
        
        iOS5Version_ = YES;
        
    } else {
        
        iOS5Version_ = NO;
        
    }
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];

}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
}

/**
 * Notifies the view controller that its view is about to be become visible. Registers to Updater notifications
 *
 * @param animated If YES, the view is being added to the window using an animation
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [GraphicElementsStyler styleNavigationBar:self.navigationController.navigationBar];
    
    viewVisible_ = YES;
    
}

/**
 * Invoked by frameword when view is about to be dismissed. Unregisters from Updater notifications
 *
 * @param animated YES if view is going to be animated, NO otherwise
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear: animated];
    
    viewVisible_ = NO;
    
    [self.view endEditing:YES];
    
}

#pragma mark -
#pragma mark Notifications management

/*
 * Invoked by framework when a user logged out notification is received. Subclasses implementing the MOKViewControllerProtocol are notified.
 */
- (void)userLoggedOutNotification:(NSNotification *)notification {
    
    if ([self conformsToProtocol:@protocol(MOKViewControllerProtocol)]) {
        
        if ([self respondsToSelector:@selector(sessionLoggedOut:)]) {
            
            [self performSelector:@selector(sessionLoggedOut:)
                       withObject:notification];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns a Boolean value indicating whether the view controller supports the specified orientation. Returns always YES for
 * portrait orientation, NO otherwise
 *
 * @param interfaceOrientation The orientation of the application’s user interface after the rotation
 * @return YES for portrait orientation, NO otherwise
 */
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    if ([MOKViewController iPhoneDevice]) {
        
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
        
    } else {
        
        return YES;
        
    }   
    
}

/**
 * The navigation item used to represent the view controller in a parent’s navigation bar. A custom navigation item is returned.
 *
 * @return The navigation item used to represent the view controllers in a parent's navigation bar.
 */
- (UINavigationItem *)navigationItem {
    
    if (customNavigationItem_ == nil) {
        
        customNavigationItem_ = [[MOKNavigationItem allocWithZone:self.zone] initWithTitle:nil];
        
    }
    
    customNavigationItem_.backBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(BACK_TEXT_KEY, nil)
                                                                                style:UIBarButtonItemStylePlain
                                                                               target:nil
                                                                               action:nil] autorelease];
    
    return customNavigationItem_;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the application delegate
 *
 * @return The application delegate
 */
- (NXT_Peru_iPhone_AppDelegate *)appDelegate {
    
    return (NXT_Peru_iPhone_AppDelegate *)([UIApplication sharedApplication]).delegate;
    
}

/*
 * Determines if this is an iPhone or iPad device
 */
+ (BOOL)iPhoneDevice {
    
    BOOL result = YES;
    
    UIDevice *device = [UIDevice currentDevice];
    
    if ([device respondsToSelector:@selector(userInterfaceIdiom)]) {
        
        if ([device userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            result = NO;
            
        }
        
    }
    
    return result;
    
}

@end
