/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKViewController.h"


//Forward declarations
@class UpdaterOperation;


/**
 * Movilok view controller protected category ment to be used only by MOKViewController subclasses.
 * Defines managemenet elements subclasses may need
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKViewController(protected)

/**
 * Provides read-only access to the view is visible flag
 */
@property (nonatomic, readonly, assign) BOOL viewVisible;

/**
 * Asks the MOKViewController child class whether to hide the activity indicator for the download that finished successfully.
 * Default implementation returns YES
 *
 * @param operation Updater operation containing all associated information
 * @return YES when the activity indicator must be removed, NO otherwise
 * @protected
 */
- (BOOL)mustHideActivityIndicatorForDownloadFinishedSuccessfully:(UpdaterOperation *)operation;

/**
 * Asks the MOKViewController child class whether to hide the activity indicator for the download that finished with an error.
 * Default implementation returns YES
 *
 * @param operation Updater operation containing all associated information
 * @return YES when the activity indicator must be removed, NO otherwise
 * @protected
 */
- (BOOL)mustHideActivityIndicatorForDownloadFinishedWithError:(UpdaterOperation *)operation;

/**
 * Asks the MOKViewController child class whether to hide the activity indicator for the download that was cancelled.
 * Default implementation returns YES
 *
 * @param operation Updater operation containing all associated information
 * @return YES when the activity indicator must be removed, NO otherwise
 * @protected
 */
- (BOOL)mustHideActivityIndicatorForDownloadCancelled:(UpdaterOperation *)operation;

@end
