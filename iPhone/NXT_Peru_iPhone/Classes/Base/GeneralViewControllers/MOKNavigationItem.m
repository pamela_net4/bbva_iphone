/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKNavigationItem.h"
#import "MOKDoubleLabel.h"


#pragma mark -

/**
 * MUINavigationItem private extension
 */
@interface MOKNavigationItem()

/**
 * Initializes the item elements
 *
 * @private
 */
- (void)initializeElements;

@end


#pragma mark -

@implementation MOKNavigationItem

#pragma mark -
#pragma mark Porperties

@synthesize doubleLabelTitle = doubleLabelTitle_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [doubleLabelTitle_ release];
    doubleLabelTitle_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initializes the NXTNavigationItem elements
 */
- (void)awakeFromNib {
    
    [self initializeElements];
    
}

/**
 * Superclass designated initializer. It initializes the NXTNavigationItem elements
 *
 * @param title The string to set as the navigation item’s title displayed in the center of the navigation bar
 * @return The initialized NXTNavigationItem instance
 */
- (id)initWithTitle:(NSString *)title {
    
    if ((self = [super initWithTitle:title])) {
        
        [self initializeElements];
        
    }
    
    return self;
    
}

/*
 * Initializes the item elements
 */
- (void)initializeElements {
    
    if (doubleLabelTitle_ == nil) {
        
        doubleLabelTitle_ = [[MOKDoubleLabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 200.0f, 44.0f)];
        
    }
    
    self.titleView = doubleLabelTitle_;
    
}

@end
