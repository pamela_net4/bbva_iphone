/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKViewController+protected.h"
#import "BaseXMLParserObject.h"
#import "ImagesCache.h"
#import "Updater.h"


#pragma mark -

@implementation MOKViewController(protected)

#pragma mark -
#pragma mark Properties

@dynamic viewVisible;

#pragma mark -
#pragma mark Download activity indicator management

/*
 * Asks the CompassViewController child class whether to hide the activity indicator for the download that finished successfully.
 * Default implementation returns YES
 */
- (BOOL)mustHideActivityIndicatorForDownloadFinishedSuccessfully:(UpdaterOperation *)operation {
    
    return YES;
    
}

/*
 * Asks the CompassViewController child class whether to hide the activity indicator for the download that finished with a error.
 * Default implementation returns YES
 */
- (BOOL)mustHideActivityIndicatorForDownloadFinishedWithError:(UpdaterOperation *)operation {
    
    return YES;
    
}

/*
 * Asks the CompassViewController child class whether to hide the activity indicator for the download that was cancelled.
 * Default implementation returns YES
 */
- (BOOL)mustHideActivityIndicatorForDownloadCancelled:(UpdaterOperation *)operation {
    
    return YES;
    
}

#pragma mark -
#pragma mark Properties selector

/*
 * Returns the view is visible flag
 *
 * @return The view is visible flag
 */
- (BOOL)viewVisible {
    
    return viewVisible_;
    
}

@end
