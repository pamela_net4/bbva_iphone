/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


//Forward declarations
@class NXT_Peru_iPhone_AppDelegate;
@class MOKNavigationItem;


/**
 * NXT application delegate. Manages the application behaviour and the initial navigations
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKViewController : UIViewController {
    
@private
    
#pragma mark Visuals
    
    /**
     * Private custom nativation item
     */
    MOKNavigationItem *customNavigationItem_;
    
#pragma mark Logic
    
    /**
     * Flag to determine whether the view is visible or not
     */
    BOOL viewVisible_;
    
    /**
     * iOS 5 or older version flag. YES if version is >= iOS5, NO if < iOS5
     */
    BOOL iOS5Version_;
    
    /**
     * iPhone device flag. YES when device is an iPhone-like, NO otherwise.
     */
    BOOL iPhoneDevice_;

}

/**
 * Provides read-only access to the application delegate
 */
@property (nonatomic, readonly, retain) NXT_Peru_iPhone_AppDelegate *appDelegate;

/**
 * Provides read-only access to the custom natigation item
 */
@property (nonatomic, readonly, assign) BOOL iOS5Version;

/**
 * Provides read-only access to the iPhone device flag.
 */
@property (nonatomic, readonly, assign) BOOL iPhoneDevice;


/**
 * Creates and returns an autoreleased MOKViewController constructed from a NIB file.
 *
 * @return The autoreleased MOKViewController constructed from a NIB file.
 */
+ (MOKViewController *)mokViewController;


/**
 * Determines if this is an iPhone or iPad device
 */
+ (BOOL)iPhoneDevice;

@end
