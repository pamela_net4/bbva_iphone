/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


//Forward declarations
@class MOKDoubleLabel;


/**
 * UINavigationItem derived class to add title view as a DoubleLabel
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKNavigationItem : UINavigationItem {

@private

    /**
     * Title view as a DoubleLabel
     */
    MOKDoubleLabel *doubleLabelTitle_;
    
}

/**
 * Provides read-only access to the title view as a DoubleLabel
 */
@property (nonatomic, readonly, retain) MOKDoubleLabel *doubleLabelTitle;

@end
