/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * UIColor category to implement colors of BBVA.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface UIColor(BBVA_Colors)

/**
 * Returns a color object whose RGB values are 9, 79, and 164 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVABlueColor;

/**
 * Returns a color object whose RGB values are 0, 110, and 193 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVABlueSpectrumColor;

/**
 * Returns a color object whose RGB values are 0, 158, and 229 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVABlueSpectrumToneOneColor;

/**
 * Returns a color object whose RGB values are 82, 188, and 236 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVABlueSpectrumToneTwoColor;

/**
 * Returns a color object whose RGB values are 139, 209, and 243 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVABlueSpectrumToneThreeColor;

/**
 * Returns a color object whose RGB values are 181, 229, and 249 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVABlueSpectrumToneFourColor;


/**
 * Returns a color object whose RGB values are 253, 189, and 44 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAYellowColor;

/**
 * Returns a color object whose RGB values are 255, 213, and 137 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAYellowToneOneColor;

/**
 * Returns a color object whose RGB values are 255, 219, and 155 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAYellowToneTwoColor;

/**
 * Returns a color object whose RGB values are 255, 228, and 183 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAYellowToneThreeColor;

/**
 * Returns a color object whose RGB values are 255, 232, and 202 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAYellowToneFourColor;

/**
 * Returns a color object whose RGB values are 255, 242, and 221 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAYellowToneFiveColor;

/**
 * Returns a color object whose RGB values are 246, 136, and 30 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAOrangeColor;

/**
 * Returns a color object whose RGB values are 252, 187, and 118 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAOrangeToneOneColor;

/**
 * Returns a color object whose RGB values are 255, 198, and 137 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAOrangeToneTwoColor;

/**
 * Returns a color object whose RGB values are 254, 213, and 169 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAOrangeToneThreeColor;

/**
 * Returns a color object whose RGB values are 254, 223, and 191 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAOrangeToneFourColor;

/**
 * Returns a color object whose RGB values are 255, 234, and 213 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAOrangeToneFiveColor;

/**
 * Returns a color object whose RGB values are 200, 23, and 94 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAMagentaColor;

/**
 * Returns a color object whose RGB values are 255, 122, and 155 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAMagentaToneOneColor;

/**
 * Returns a color object whose RGB values are 224, 141, and 167 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAMagentaToneTwoColor;

/**
 * Returns a color object whose RGB values are 232, 172, and 190 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAMagentaToneThreeColor;

/**
 * Returns a color object whose RGB values are 238, 194, and 207 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAMagentaToneFourColor;

/**
 * Returns a color object whose RGB values are 243, 215, and 222 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAMagentaToneFiveColor;

/**
 * Returns a color object whose RGB values are 134, 200, and 45 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreenColor;

/**
 * Returns a color object whose RGB values are 177, 215, and 138 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreenToneOneColor;

/**
 * Returns a color object whose RGB values are 189, 220, and 155 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreenToneTwoColor;

/**
 * Returns a color object whose RGB values are 209, 230, and 183 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreenToneThreeColor;

/**
 * Returns a color object whose RGB values are 220, 236, and 203 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreenToneFourColor;

/**
 * Returns a color object whose RGB values are 234, 243, and 222 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreenToneFiveColor;

/**
 * Returns a color object whose RGB values are 183, 194, and 4 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreenLemonColor;

/**
 * Returns a color object whose RGB values are 209, 215, and 92 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreenLemonToneOneColor;

/**
 * Returns a color object whose RGB values are 216, 221, and 117 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreenLemonToneTwoColor;

/**
 * Returns a color object whose RGB values are 227, 231, and 154 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreenLemonToneThreeColor;

/**
 * Returns a color object whose RGB values are 234, 237, and 180 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreenLemonToneFourColor;

/**
 * Returns a color object whose RGB values are 244, 246, and 217 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreenLemonToneFiveColor;

/**
 * Returns a color object whose RGB values are 62, 182, and 187 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVATurquoiseColor;

/**
 * Returns a color object whose RGB values are 130, 208, and 211 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVATurquoiseToneOneColor;

/**
 * Returns a color object whose RGB values are 149, 215, and 218 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVATurquoiseToneTwoColor;

/**
 * Returns a color object whose RGB values are 187, 226, and 228 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVATurquoiseToneThreeColor;

/**
 * Returns a color object whose RGB values are 197, 233, and 235 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVATurquoiseToneFourColor;

/**
 * Returns a color object whose RGB values are 226, 244, and 244 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVATurquoiseToneFiveColor;

/**
 * Returns a color object whose grayscale value is 1.0 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAWhiteColor;


/**
 * Returns a color object whose grayscale value is 0.0 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVABlackColor;

/**
 * Returns a color object whose RGB values are 54, 54, and 54 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVACoolGreyColor;

/**
 * Returns a color object whose RGB values are 77, 77, and 79 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreyColor;

/**
 * Returns a color object whose RGB values are 109, 110, and 113 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreyToneOneColor;

/**
 * Returns a color object whose RGB values are 147, 149, and 152 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreyToneTwoColor;

/**
 * Returns a color object whose RGB values are 188, 190, and 192 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreyToneThreeColor;

/**
 * Returns a color object whose RGB values are 209, 211, and 212 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreyToneFourColor;

/**
 * Returns a color object whose grayscale value is 2/3 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVAGreyToneFiveColor;

/**
 * Returns a color object whose RGB values are 122, 154, and 181 and whose alpha value is 1.0.
 *
 * @return The UIColor object.
 */
+ (UIColor *)BBVABlueGreenColor;

@end
