/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "UIColor+BBVA_Colors.h"


#pragma mark -

@implementation UIColor(BBVA_Colors)

#pragma mark -
#pragma mark Primary colors


/*
 * Returns a color object whose RGB values are 9, 79, and 164 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVABlueColor {
    
    return [UIColor colorWithRed:0.0352f green:0.3098f blue:0.6431f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 0, 110, and 193 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVABlueSpectrumColor {
    
    return [UIColor colorWithRed:0.0f green:0.4313f blue:0.7568f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 0, 158, and 229 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVABlueSpectrumToneOneColor {
    
    return [UIColor colorWithRed:0.0f green:0.6196f blue:0.8980f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 82, 188, and 236 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVABlueSpectrumToneTwoColor {
    
    return [UIColor colorWithRed:0.3215f green:0.7372f blue:0.9254f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 139, 209, and 243 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVABlueSpectrumToneThreeColor {
    
    return [UIColor colorWithRed:0.5450f green:0.8196f blue:0.9529f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 181, 229, and 249 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVABlueSpectrumToneFourColor {
    
    return [UIColor colorWithRed:0.7098f green:0.8980f blue:0.9764f alpha:1.0f];
    
}


/*
 * Returns a color object whose grayscale value is 1.0 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAWhiteColor {
    
    return [UIColor colorWithWhite:1.0f alpha:1.0f];
    
}

#pragma mark -
#pragma mark Secundary colors

#pragma mark -
#pragma mark BBVA Yellow

/*
 * Returns a color object whose RGB values are 253, 189, and 44 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAYellowColor {
    
    return [UIColor colorWithRed:0.9921f green:0.7411f blue:0.1725f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 255, 213, and 137 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAYellowToneOneColor {
    
    return [UIColor colorWithRed:1.0f green:0.8352f blue:0.5372f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 255, 219, and 155 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAYellowToneTwoColor {
    
    return [UIColor colorWithRed:1.0f green:0.8588f blue:0.6078f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 255, 228, and 183 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAYellowToneThreeColor {
    
    return [UIColor colorWithRed:1.0f green:0.8941f blue:0.7176f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 255, 232, and 202 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAYellowToneFourColor {
    
    return [UIColor colorWithRed:1.0f green:0.9098f blue:0.7921f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 255, 242, and 221 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAYellowToneFiveColor {
    
    return [UIColor colorWithRed:1.0f green:0.9490f blue:0.8666f alpha:1.0f];
    
}

#pragma mark -
#pragma mark BBVA Orange

/*
 * Returns a color object whose RGB values are 246, 136, and 30 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAOrangeColor {
    
    return [UIColor colorWithRed:0.9647f green:0.5333f blue:0.1176f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 252, 187, and 118 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAOrangeToneOneColor {
    
    return [UIColor colorWithRed:0.9882f green:0.7333f blue:0.4627f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 255, 198, and 137 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAOrangeToneTwoColor {
    
    return [UIColor colorWithRed:1.0f green:0.7764f blue:0.5372f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 254, 213, and 169 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAOrangeToneThreeColor {
    
    return [UIColor colorWithRed:0.9960f green:0.8352f blue:0.6627f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 254, 223, and 191 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAOrangeToneFourColor {
    
    return [UIColor colorWithRed:0.9960f green:0.8745f blue:0.7490f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 255, 234, and 213 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAOrangeToneFiveColor {
    
    return [UIColor colorWithRed:0.9960f green:0.9176f blue:0.8352f alpha:1.0f];
    
}

#pragma mark -
#pragma mark BBVA Magenta

/*
 * Returns a color object whose RGB values are 200, 23, and 94 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAMagentaColor {
    
    return [UIColor colorWithRed:0.7843f green:0.0901f blue:0.3686f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 255, 122, and 155 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAMagentaToneOneColor {
    
    return [UIColor colorWithRed:1.0f green:0.4784f blue:0.6078f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 224, 141, and 167 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAMagentaToneTwoColor {
    
    return [UIColor colorWithRed:0.8784f green:0.5529f blue:0.6549f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 232, 172, and 190 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAMagentaToneThreeColor {
    
    return [UIColor colorWithRed:0.9098f green:0.6745f blue:0.7450f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 238, 194, and 207 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAMagentaToneFourColor {
    
    return [UIColor colorWithRed:0.9333f green:0.7607f blue:0.8117f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 243, 215, and 222 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAMagentaToneFiveColor {
    
    return [UIColor colorWithRed:0.9529f green:0.8431f blue:0.8705f alpha:1.0f];
    
}

#pragma mark -
#pragma mark BBVA Green

/*
 * Returns a color object whose RGB values are 134, 200, and 45 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreenColor {
    
    return [UIColor colorWithRed:0.5254f green:0.7843f blue:0.1764f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 177, 215, and 138 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreenToneOneColor {
    
    return [UIColor colorWithRed:0.6941f green:0.8431f blue:0.5411f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 189, 220, and 155 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreenToneTwoColor {
    
    return [UIColor colorWithRed:0.7411f green:0.8627f blue:0.6078f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 209, 230, and 183 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreenToneThreeColor {
    
    return [UIColor colorWithRed:0.8196f green:0.9019f blue:0.7176f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 220, 236, and 203 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreenToneFourColor {
    
    return [UIColor colorWithRed:0.8627f green:0.9254f blue:0.7960f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 234, 243, and 222 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreenToneFiveColor {
    
    return [UIColor colorWithRed:0.9176f green:0.9529f blue:0.8705f alpha:1.0f];
    
}

#pragma mark -
#pragma mark BBVA Green Lemon

/*
 * Returns a color object whose RGB values are 183, 194, and 4 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreenLemonColor {
    
    return [UIColor colorWithRed:0.7176f green:0.7607f blue:0.0156f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 209, 215, and 92 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreenLemonToneOneColor {
    
    return [UIColor colorWithRed:0.8196f green:0.8431f blue:0.3607f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 216, 221, and 117 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreenLemonToneTwoColor {
    
    return [UIColor colorWithRed:0.8470f green:0.8666f blue:0.4588f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 227, 231, and 154 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreenLemonToneThreeColor {
    
    return [UIColor colorWithRed:0.8901f green:0.9058f blue:0.6039f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 234, 237, and 180 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreenLemonToneFourColor {
    
    return [UIColor colorWithRed:0.9176f green:0.9294f blue:0.7058f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 244, 246, and 217 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreenLemonToneFiveColor {
    
    return [UIColor colorWithRed:0.9568f green:0.9647f blue:0.8509f alpha:1.0f];
    
}

#pragma mark -
#pragma mark BBVA Turquoise

/*
 * Returns a color object whose RGB values are 62, 182, and 187 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVATurquoiseColor {
    
    return [UIColor colorWithRed:0.2431f green:0.7137f blue:0.7333f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 130, 208, and 211 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVATurquoiseToneOneColor {
    
    return [UIColor colorWithRed:0.5098f green:0.8156f blue:0.8274f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 149, 215, and 218 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVATurquoiseToneTwoColor {
    
    return [UIColor colorWithRed:0.5843f green:0.8431f blue:0.8549f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 187, 226, and 228 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVATurquoiseToneThreeColor {
    
    return [UIColor colorWithRed:0.7333f green:0.8862f blue:0.8941f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 197, 233, and 235 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVATurquoiseToneFourColor {
    
    return [UIColor colorWithRed:0.7725f green:0.9137f blue:0.9215f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 226, 244, and 244 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVATurquoiseToneFiveColor {
    
    return [UIColor colorWithRed:0.8862f green:0.9568f blue:0.9568f alpha:1.0f];
    
}


/*
 * Returns a color object whose grayscale value is 0.0 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVABlackColor {
    
    return [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 54, 54, and 54 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVACoolGreyColor {
    
    return [UIColor colorWithRed:0.2117f green:0.2117f blue:0.2117f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 77, 77, and 79 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreyColor {
    
    return [UIColor colorWithRed:0.3019f green:0.3019f blue:0.3098f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 109, 110, and 113 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreyToneOneColor {
    
    return [UIColor colorWithRed:0.4274f green:0.4313f blue:0.4431f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 147, 149, and 152 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreyToneTwoColor {
    
    return [UIColor colorWithRed:0.5764f green:0.5843f blue:0.5960f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 188, 190, and 192 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreyToneThreeColor {
    
    return [UIColor colorWithRed:0.7372f green:0.7450f blue:0.7529f alpha:1.0f];
    
}

/*
 * Returns a color object whose RGB values are 209, 211, and 212 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreyToneFourColor {
    
    return [UIColor colorWithRed:0.8196f green:0.8274f blue:0.8313f alpha:1.0f];
    
}

/*
 * Returns a color object whose grayscale value is 2/3 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVAGreyToneFiveColor {
    
    return [UIColor colorWithWhite:0.9490f alpha:1.0f];
    
}

#pragma mark -
#pragma mark BBVA Blue green

/*
 * Returns a color object whose RGB values are 122, 154, and 181 and whose alpha value is 1.0.
 */
+ (UIColor *)BBVABlueGreenColor {
    
    return [UIColor colorWithRed:0.4784f green:0.6039f blue:0.7098f alpha:1.0f];
    
}

@end
