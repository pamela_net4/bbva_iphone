/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseDataDownloadClient.h"
#import "HTTPInvoker.h"


#pragma mark -

@implementation BaseDataDownloadClient

#pragma mark -
#pragma mark Properties

@synthesize downloadId = downloadId_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {

    [self cancelDownload];
    
    [downloadId_ release];
    downloadId_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Download management

/*
 * Cancels the current data download
 */
- (void)cancelDownload {
    
    [[HTTPInvoker getInstance] cancelDownload:downloadId_];
    
}

#pragma mark -
#pragma mark DataDownloadClient protocol selectors

/**
 * Notifies the client that the download has finished. The data downloaded from the server is provided
 * to analyze it. No action is performed in this implementation
 *
 * @param aDownloadId The string that identifies the download process
 * @param aDownloadedData The data downloaded from the server
 */
- (void)dataDownload:(NSString *)aDownloadId finishedWithData:(NSData *)aDownloadedData {
    
}

/**
 * Notifies the client that an error appeared during data download. Data downloaded and error are provided
 * to the client to analyze them. No action is performed in this implementation
 *
 * @param aDownloadId The string that identifies the download process
 * @param anError The error that appeared during the data download
 * @param aDownloadedData The data downloaded until the error appeared
 */
- (void)dataDownload:(NSString *)aDownloadId finishedWithError:(NSError *)anError downloadedData:(NSData *)aDownloadedData {
    
}

/**
 * Notifies the download has finished with a status code different than OK_STATUS (200). No action is performed in this implementation
 *
 * @param aDownloadId The string that identifies the download process
 * @param aStatusCode The status code that finished the download
 */
- (void)dataDownload:(NSString *)aDownloadId finishedWithStatusCode:(NSInteger)aStatusCode {
    
}

/**
 * Notifies the client that the data download was cancelled. No action is performed in this implementation
 *
 * @param aDownloadId The string that identifies the download process
 */
- (void)dataDownloadCancelled:(NSString *)aDownloadId {
    
}

@end
