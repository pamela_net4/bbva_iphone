/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>
#import "SingletonBase.h"
#import "DownloadListener.h"
#import "Constants.h"

/**
 * Operation types
 */
#define LOGIN                                               1
#define LOGIN_COORDINATE                                    2
#define LOGIN_ENCRYPT                                       3
#define LOGIN_DESENCRYPT                                    4
#define RETRIEVE_GLOBAL_POSITION                            10
#define RETRIEVE_ACCOUNT_TRANSACTION_LIST                   20
#define RETRIEVE_ACCOUNT_TRANSACTION_DETAIL                 21
#define RETRIEVE_CARD_TRANSACTION_LIST                      30
#define TRANSFER_BETWEEN_ACCOUNTS_STARTUP                   40
#define TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION              41
#define TRANSFER_BETWEEN_ACCOUNTS_RESULT                    42
#define RETRIEVE_RETAINS                                    50
#define SEND_TRANSACTION                                    60
#define SEND_TRANSACTION_CONFIRMATION                       61
#define LOGOUT                                              100
#define CLOSE                                               101
#define TRANSFER_TO_THIRD_ACCOUNTS_STARTUP                  110
#define TRANSFER_TO_THIRD_ACCOUNTS_CONFIRMATION             111
#define TRANSFER_TO_THIRD_ACCOUNTS_RESULT                   112
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_STARTUP		120
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_TYPE_CHECKING	123
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION	121
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_RESULT			122
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_STARTUP        150
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_CONFIRMATION   151
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_RESULT         152
#define OBTAIN_CASH_MOBILE_DETAIL_STARTUP                   190
#define OBTAIN_CASH_MOBILE_DETAIL                           191
#define OBTAIN_CASH_MOBILE_DETAIL_RESEND                    192
#define TRANSFER_TO_ACCOUNT_TO_GIFT_CARD_STARTUP            153
#define TRANSFER_TO_ACCOUNT_TO_GIFT_CARD_CONFIRMATION       154
#define TRANSFER_TO_ACCOUNT_TO_GIFT_CARD_RESULT             155

#define PAYMENT_SERVICES_LIST_OPERATION_RESULT									130
#define PAYMENT_PUBLIC_SERVICE_INITIAL                                          140
#define PAYMENT_PUBLIC_SERVICE_INITIAL_ELECT_LUZ_DEL_SUR                        141
#define PAYMENT_PUBLIC_SERVICE_INITIAL_ELECT_EDELNOR                            142
#define PAYMENT_PUBLIC_SERVICE_INITIAL_ELECT_EDELNOR                            142
#define PAYMENT_PUBLIC_SERVICE_INITIAL_WATER_SEDAPAL                            143
#define PAYMENT_PUBLIC_SERVICE_INITIAL_PHONE_TELEFONICA                         144
#define PAYMENT_PUBLIC_SERVICE_INITIAL_CELL_MOVISTAR                            145
#define PAYMENT_PUBLIC_SERVICE_INITIAL_CELL_CLARO                               146
#define PAYMENT_PUBLIC_SERVICE_INITIAL_GAS_REPSOL                               147

#define PAYMENT_PUBLIC_SERVICE_DATA                                             160
#define PAYMENT_PUBLIC_SERVICE_DATA_ELECT_LUZ_DEL_SUR                           161
#define PAYMENT_PUBLIC_SERVICE_DATA_ELECT_EDELNOR                               162
#define PAYMENT_PUBLIC_SERVICE_DATA_WATER_SEDAPAL                               163
#define PAYMENT_PUBLIC_SERVICE_DATA_PHONE_TELEFONICA                            164
#define PAYMENT_PUBLIC_SERVICE_DATA_CELL_MOVISTAR                               165
#define PAYMENT_PUBLIC_SERVICE_DATA_CELL_CLARO                                  166

#define PAYMENT_PUBLIC_SERVICE_CONF                                             180
#define PAYMENT_PUBLIC_SERVICE_CONF_ELECT_LUZ_DEL_SUR                           181
#define PAYMENT_PUBLIC_SERVICE_CONF_ELECT_EDELNOR                               182
#define PAYMENT_PUBLIC_SERVICE_CONF_WATER_SEDAPAL                               183
#define PAYMENT_PUBLIC_SERVICE_CONF_PHONE_TELEFONICA                            184
#define PAYMENT_PUBLIC_SERVICE_CONF_CELL_MOVISTAR                               185
#define PAYMENT_PUBLIC_SERVICE_CONF_CELL_CLARO                                  186

#define PAYMENT_PUBLIC_SERVICE_SUCCESS                                          200
#define PAYMENT_PUBLIC_SERVICE_SUCCESS_ELECT_LUZ_DEL_SUR                        201
#define PAYMENT_PUBLIC_SERVICE_SUCCESS_ELECT_EDELNOR                            202
#define PAYMENT_PUBLIC_SERVICE_SUCCESS_WATER_SEDAPAL                            203
#define PAYMENT_PUBLIC_SERVICE_SUCCESS_PHONE_TELEFONICA                         204
#define PAYMENT_PUBLIC_SERVICE_SUCCESS_CELL_MOVISTAR                            205
#define PAYMENT_PUBLIC_SERVICE_SUCCESS_CELL_CLARO                               206

#define PAYMENT_CARDS_CONT_INITIAL                                              220
#define PAYMENT_CARDS_CONT_OWN_DATA                                             221
#define PAYMENT_CARDS_CONT_OWN_CONF                                             222
#define PAYMENT_CARDS_CONT_OWN_SUCCESS                                          223
#define PAYMENT_CARDS_CONT_THIRD_DATA                                           224
#define PAYMENT_CARDS_CONT_THIRD_CONF                                           225
#define PAYMENT_CARDS_CONT_THIRD_SUCCESS                                        226

#define PAYMENT_CARDS_OTHER_BANK_INITIAL                                        230
#define PAYMENT_CARDS_OTHER_BANK_DATA                                           231
#define PAYMENT_CARDS_OTHER_BANK_CONF                                           232
#define PAYMENT_CARDS_OTHER_BANK_TIN                                           234
#define PAYMENT_CARDS_OTHER_BANK_SUCCESS                                        233

#define PAYMENT_RECHARGE_DATA                                                   240
#define PAYMENT_RECHARGE_CONF                                                   241
#define PAYMENT_RECHARGE_MOVISTAR_SUCCESS                                       242
#define PAYMENT_RECHARGE_CLARO_SUCCESS                                          243

#define PAYMENT_INSTITUTIONS_LIST_OPERATION_RESULT								250
#define PAYMENT_INSTITUTIONS_SEARCH_LIST_RESULT                                 251
#define PAYMENT_INSTITUTIONS_DETAIL                                             252
#define PAYMENT_INSTITUTIONS_PENDING_PAYS_DETAIL                                253
#define PAYMENT_INSTITUTIONS_CONFIRMATION_PAY_DETAIL                            254
#define PAYMENT_INSTITUTIONS_SUCCESS_PAYS_DETAIL                                255

#define RETRIEVE_SAFETYPAY_STATUS_INFO                                          256
#define RETRIEVE_SAFETYPAY_TRANSACTION_INFO                                     257
#define RETRIEVE_SAFETYPAY_PAYMENT_DETAILS                                      258
#define RETRIEVE_SAFETYPAY_PAYMENT_CONFIRM                                      259

#define FREQUENT_OPERATION_OBTAIN_INITIAL_LIST                                  300
#define FREQUENT_OPERATION_PAGINATION_LIST                                      302
#define FREQUENT_OPERATION_OBTAIN_EXECUTE_INFO                                  301



#define FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_ONE                        310
#define FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_ONE                         311
#define FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUHT_STEP_ONE                      312
#define FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_ONE                 313
#define FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_ONE                 314
#define FREQUENT_OPERATION_REACTIVE_GIFT_CARD_STEP_ONE                          315
#define FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_ONE              316
#define FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_ONE                     317
#define FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_ONE                  318
#define FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_ONE                      319
#define FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_ONE                   320
#define FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_ONE                      321
#define FREQUENT_OPERATION_REACTIVE_STEP_TWO                                    322
#define FREQUENT_OPERATION_REACTIVE_STEP_THREE                                  323

#define FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_TWO                        324
#define FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_TWO                         325
#define FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUHT_STEP_TWO                      326
#define FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_TWO                 327
#define FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_TWO                 328
#define FREQUENT_OPERATION_REACTIVE_GIFT_CARD_STEP_TWO                          329
#define FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_TWO              330
#define FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_TWO                     331
#define FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_TWO                  332
#define FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_TWO                      333
#define FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_TWO                   334
#define FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_TWO                      335

#define FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_THREE                        336
#define FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_THREE                         337
#define FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUHT_STEP_THREE                      338
#define FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_THREE                 339
#define FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_THREE                 340
#define FREQUENT_OPERATION_REACTIVE_GIFT_CARD_STEP_THREE                          341
#define FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_THREE              342
#define FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_THREE                     343
#define FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_THREE                  344
#define FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_THREE                      345
#define FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_THREE                   346
#define FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_THREE                      347

#define THIRD_ACCOUNT_OBTAIN_INITIAL_LIST                                         348

#define THIRD_ACCOUNT_PAGINATION_LIST                                             349
#define THIRD_ACCOUNT_NICKNAME_VALIDATION                                         350
#define THIRD_ACCOUNT_OPERATION_CONFIRMATION                                      351
#define THIRD_ACCOUNT_OPERATION_SUCCESS                                           352

#define CAMPAIGN_LIST                                                              353

#define INCREMENT_OF_LINE_INITIAL                                                  354
#define INCREMENT_OF_LINE_CONFIRMATION                                             355
#define INCREMENT_OF_LINE_SUMMARY                                                  356

#define FAST_LOAN_INITIAL                                                          357
#define FAST_LOAN_CONFIRMATION                                                     358
#define FAST_LOAN_SUMMARY                                                          359
#define FAST_LOAN_TERM                                                             360
#define FAST_LOAN_SCHEDULE                                                         361
#define FAST_LOAN_SIMULATION                                                       362


#define SALARY_ADVANCE_AFFILIATION                    363
#define SALARY_ADVANCE_RECEIVE                        364
#define SALARY_ADVANCE_SUMMMARY                       365
#define SALARY_ADVANCE_AFFILIATION_CONFIRM            366

#define FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO                                 @"FORTypeStepTwo"
#define FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE                               @"FORTypeStepThree"
#define THIRD_ACCOUNT_REGISTRATION_ERROR @"2186"

//Forward declarations
@class Stock;


/**
 * This class is responsible of the updating of the model
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Updater : SingletonBase <DownloadListener> {
    
@private
	
	/**
	 * Dictionary of active operations
	 */
	NSMutableDictionary* activeOperations_;
	
}

/**
 * Returns the singleton only instance
 *
 * @result The singleton only instance
 */
+ (Updater *)getInstance;

/**
 * Cancels a download defined by its download file
 *
 * @param aDownloadId The download identification
 */
- (void)cancelDownload: (NSString*) aDownloadId;

/**
 * Cancels all active downloads
 */
- (void)cancelAllDownloads;

/**
 * Returns the number of active downloads
 */
- (NSInteger)numberOfActiveDownloads;


/**
 * Process MCB notifications after an UpdaterOperation invocation
 *
 * @param string to notificate
 * @private
 */
- (void) processMCBNotifications:(NSString *)notification;


/**
 * Login
 *
 * @param identification the user identification
 * @param password the password
 */
- (void)loginWithId:(NSString *)identification andPassword:(NSString *)password;

- (void)loginEncryptWithId:(NSString *)identification andAlias:(NSString *)alias;

- (void)loginDesencryptWithToken:(NSString *)token;

/**
 * Login coordinate
 *
 * @param coordinate the coordinate
 */
- (void)loginWithCoordinate:(NSString *)coordinate; 

/**
 * Obtains the global position
 */
- (void)obtainGlobalPosition;

/**
 * Obtains the account transactions without filters
 *
 * @param anAccountNumber The account number to obtain the transactions from
 */
- (void)obtainAccountTransactionsForAccountNumber:(NSString *)anAccountNumber;

/*
 * Obtains the account transactions detail for a transaction number
 *
 * @param anAccountNumber The account number to obtain the transactions from
 * @param number The transaction number
 */
- (void)obtainAccountTransactionDetailForAccountNumber:(NSString *)anAccountNumber 
                                     transactionNumber:(NSString *)number;

/**
 * Obtains the card transactions
 *
 * @param aCardNumber The card number identifying it inside the list
 */
- (void)obtainCardTransactionsForCardNumber:(NSString *)aCardNumber;

/**
 * Invoke transfer between accounts startup
 */
- (void)transferBetweenAccountsStartup;

/**
 * Invoke transfer between accounts confirmation
 *
 * @param fromAccountNumber from account number
 * @param fromAccountType from account type
 * @param fromCurrency from currency
 * @param fromType from type
 * @param fromIndex from index
 * @param toAccountNumber to account number
 * @param toAccountType to account type
 * @param toCurrency to currency
 * @param toType to type
 * @param toIndex to index
 * @param amount the amount
 * @param currency the currency
 * @param subject the subject
 */
- (void)transferBetweenAccountsConfirmationFromAccountNumber:(NSString *)fromAccountNumber 
                                             fromAccountType:(NSString *)fromAccountType 
                                                fromCurrency:(NSString *)fromCurrency
                                                    fromType:(NSString *)fromType
                                                   fromIndex:(NSString *)fromIndex
                                             toAccountNumber:(NSString *)toAccountNumber 
                                               toAccountType:(NSString *)toAccountType 
                                                  toCurrency:(NSString *)toCurrency
                                                      toType:(NSString *)toType
                                                     toIndex:(NSString *)toIndex
                                                      amount:(NSString *)amount
                                                    currency:(NSString *)currency
                                                  andSubject:(NSString *)subject;
	
/**
 * Invoke transfer between accounts result
 */
- (void)transferBetweenAccountsResult;
	
/**
 * Invoke transfer to third accounts startup
 */
- (void)transferToThirdAccountsStartup;


/**
 * Invoke transfer to third accounts confirmation
 *
 * @param fromAccountType from account type
 * @param fromCurrency from currency
 * @param fromIndex from index
 * @param toBranch to branch number
 * @param toAccount to account number
 * @param amount the amount
 * @param currency the currency
 * @param reference reference
 * @param anEmail1 to send
 * @param anEmail2 to send
 * @param aPhone1 to send 
 * @param aCarrier1 carrier of the first phone
 * @param aPhone2 to send
 * @param aCarrier2 carrier of the second phone
 * @param message the message
 */
- (void)transferToThirdAccountsConfirmationFromAccountType:(NSString *)fromAccountType 
                                              fromCurrency:(NSString *)fromCurrency
                                                 fromIndex:(NSString *)fromIndex
                                                  toBranch:(NSString *)toBranch 
                                                 toAccount:(NSString *)toAccount 
                                                    amount:(NSString *)amount 
                                                  currency:(NSString *)currency 
                                                 reference:(NSString *)reference
                                                    email1:(NSString *)anEmail1 
                                                    email2:(NSString *)anEmail2
                                                    phone1:(NSString *)aPhone1 
                                                  carrier1:(NSString *)aCarrier1 
                                                    phone2:(NSString *)aPhone2
                                                  carrier2:(NSString *)aCarrier2
                                                andMessage:(NSString *)message;



/**
 * Invoke transfer to third accounts result
 *
 * @param secondFactor The second factor key
 */
- (void)transferToThirdAccountsResultFromSecondFactorKey:(NSString *)secondFactor;

/**
 * Invoke transfer to accounts from other bank startup
 */
- (void)transferToAccountsFromOtherBanksStartup;	
	
	
/**
 * Invoke transfer to accounts from other banks confirmation
 *
 * @param fromAccountType from account type
 * @param fromCurrency from currency
 * @param fromIndex from index
 * @param toBank to bank number
 * @param toBranch to branch number
 * @param toAccount to account number
 * @param toCc to bank number
 * @param amount the amount
 * @param currency the currency
 * @param itf the itf
 * @param reference reference
 * @param anEmail1 to send
 * @param anEmail2 to send
 * @param aPhone1 to send 
 * @param aCarrier1 carrier of the first phone
 * @param aPhone2 to send
 * @param aCarrier2 carrier of the second phone
 * @param message the message
 * @param beneficiary the beneficiary
 * @param documentType the document type
 * @param documentNumber the document number
 */
- (void)transferToAccountsFromOtherBanksConfirmationFromAccountType:(NSString *)fromAccountType 
													   fromCurrency:(NSString *)fromCurrency
														  fromIndex:(NSString *)fromIndex
															 toBank:(NSString *)toBank
														   toBranch:(NSString *)toBranch 
														  toAccount:(NSString *)toAccount
															   toCc:(NSString *)toCc
															 amount:(NSString *)amount
														   currency:(NSString *)currency
														  reference:(NSString *)reference
															 email1:(NSString *)anEmail1 
															 email2:(NSString *)anEmail2
															 phone1:(NSString *)aPhone1 
														   carrier1:(NSString *)aCarrier1 
															 phone2:(NSString *)aPhone2
														   carrier2:(NSString *)aCarrier2
															message:(NSString *)message;



/*
 * Invoke transfer to accounts from other bank confirmation
 */
- (void)transferToAccountsFromOtherBanksWithInmediateConfirmation;

/*
 * Invoke transfer to accounts from other bank confirmation
 */
- (void)transferToAccountsFromOtherBanksWithByScheduleConfirmationWithItf:(BOOL)itf
                                                              beneficiary:(NSString *)beneficiary
                                                             documentType:(NSString *)documentType
                                                        andDocumentNumber:(NSString *)documentNumber;

/**
 * Invoke transfer between accounts result
 *
 * @param secondFactor The second factor key
 */
- (void)transferToAccountsFromOtherBanksResultFromSecondFactorKey:(NSString *)secondFactor;

/*
 * Invoke transfer to accounts With Cash Mobile startup
 */
- (void)transferToAccountsWithCashMobileStartup;

/**
 * Invoke transfer to third accounts confirmation
 *
 * @param fromAccountType from account type
 * @param fromCurrency from currency
 * @param fromIndex from index
 * @param toPhone to Phone number
 * @param amount the amount
 * @param currency the currency
 */
- (void)transferWithCashMobileConfirmationFromAccountType:(NSString *)fromAccountType
                                             fromCurrency:(NSString *)fromCurrency
                                                fromIndex:(NSString *)fromIndex
                                                  toPhone:(NSString *)toPhone
                                                   amount:(NSString *)amount
                                                 currency:(NSString *)currency;

/**
 * Invoke transfer with cash mobile result
 *
 * @param secondKeyFactor The second key factor.
 */
- (void)transferWithCashMobileResultFromSecondKeyFactor:(NSString *)secondKeyFactor;

/**
 * Invoke transfer To gift card startup
 */
- (void)transferToGiftCardStartup;

/**
 * Invoke transfer to third accounts confirmation
 *
 * @param fromAccountType from account type
 * @param fromNumber from account or card number
 * @param toNumber to account number
 * @param amount the amount
 * @param currency the currency
 * @param toConfirmationPhone the phone to send the confirmation
 * @param toConfirmationMail the mail to send the confirmation
 * @param carrier the carrier for phones
 * @param referenceMessage the referenceMessage
 */
- (void)transferToGiftCardConfirmationFromAccountType:(NSString *)fromAccountType
                                             fromNumber:(NSString *)fromNumber
                                                toNumber:(NSString *)toNumber
                                                   amount:(NSString *)amount
                                                 currency:(NSString *)currency
                                  toConfirmationPhone:(NSString *)toConfirmationPhone
                                  toConfirmationMail:(NSString *)toConfirmationMail
                                              carrier:(NSString *)carrier
                                     referenceMessage:(NSString *)referenceMessage;

/**
 * Invoke transfer with gift card result
 *
 * @param secondKeyFactor The second key factor.
 */
- (void)TransferToGiftCardResultFromSecondFactorKey:(NSString *)secondFactor;

/*
 * Invoke obtain cash mobile transactions information
 */
- (void)obtainCashMobileTransactionDetail;

/**
 * Invoke transfer with cash mobile result
 *
 * @param operationCode Code of the cash mobile transaction to view.
 */
- (void)obtainCashMobileTransactionDetailWithOperationCode:(NSString *)operationCode;

/*
 * Invoke obtain cash mobile transactions resend
 *
 * @param operationCode Code of the cash mobile transaction to view.
 */
- (void)obtainCashMobileTransactionResend:(NSString *)operationCode;

/**
 * Invoke retains retrieval
 *
 * @param forAccountNumber the account number
 * @param accountType the account type
 * @param balance the balance
 * @param availableBalance the available balance
 * @param currency the currency
 * @param bank the bank number
 * @param office the office number
 * @param controlDigit the control digit
 * @param account the account number
 * @param type the type
 * @param index the index
 */
- (void)retrieveRetainsForAccountNumber:(NSString *)forAccountNumber 
							accountType:(NSString *)accountType 
								balance:(NSString *)balance
					   availableBalance:(NSString *)availableBalance
							   currency:(NSString *)currency
								   bank:(NSString *)bank
								 office:(NSString *)office
						   controlDigit:(NSString *)controlDigit
								account:(NSString *)account
								   type:(NSString *)type
							   andIndex:(NSString *)index;
	

/**
 * Invoke transaction send confirmation
 *
 * @param email1 the first email
 * @param email2 the second email
 * @param carrier1 the first carrier
 * @param carrier2 the second carrier
 * @param phonenumber1 the first phonenumber
 * @param phonenumber2 the second phonenumber
 * @param comments the user comments
 */
- (void)sendTransactionConfirmationForEmail:(NSString *)email1 
									 email2:(NSString *)email2
								   carrier1:(NSString *)carrier1
								   carrier2:(NSString *)carrier2
							   phonenumber1:(NSString *)phonenumber1
							   phonenumber2:(NSString *)phonenumber2
								andComments:(NSString *)comments;


/*
 * Obtains the institutions and copmanies services list
 */
- (void)obtainPaymentInstitutionsAndCompanies;

/*
 * Search a institution by some arguments
 * @param entity the entity code
 * @param searchType the type of the search
 * @param nextMovement the nex movement
 * @param indPag the current page
 * @param searchArg the search argument
 * @param lastDescription the las description recieved
 * @param button the user button
 */
- (void)obtainInstitutionsAndCompaniesForEntity:(NSString *)entity
                                     searchType:(NSString *)searchType
                                   nextMovement:(NSString *)nextMovement
                                         indPag:(NSString *)indPag
                                      searchArg:(NSString *)searchArg
                                      LastDescr:(NSString *)lastDescription
                                         button:(NSString *)button
                                         action:(NSString *)action;

/*
 * Obtains the institution detail
 * @param institutionCode the institution code
 * @param optionType the type operation
 */
- (void)obtainPaymentInstitutionsDetailForCod:(NSString *)institutionCode
                                   optionType:(NSString *)optionType;

/*
 * Obtain the information of the pending payments dor the institution
 * @param code the code of the institution
 * @param arrayLong the longitud of the array
 * @param array the array
 * @param titlesArray collection of titles
 * @param valData Validation Data
 * @param flagModule Flag to know if is a module or not
 */
- (void)obtainInstitutionsAndCompaniesPendingPaysForCode:(NSString *)code
                                               arrayLong:(NSString *)arrayLong
                                                   array:(NSString *)array
                                             titlesArray:(NSString *)titlesArray
                                          validationData:(NSString *)valData
                                              flagModule:(NSString *)flagModule;

/*
 * Obtain the confirmation of the payments for the institution
 * @param code the code of the institution
 * @param arrayLong the longitud of the array
 * @param array the array
 * @param titlesArray collection of titles
 * @param valData Validation Data
 * @param flagModule Flag to know if is a module or not
 */
- (void)obtainInstitutionsAndCompaniesPayConfirmationWithForm:(NSString *)payForm
                                            ownSubjectAccount:(NSString *)ownSubjectAccount
                                               ownSubjectCard:(NSString *)card
                                                    payImport:(NSString *)payImport
                                                 phonenumber1:(NSString *)phonenumber1
                                                 phonenumber2:(NSString *)phonenumber2
                                                       email1:(NSString *)email1
                                                       email2:(NSString *)email2
                                                     carrier1:(NSString *)carrier1
                                                     carrier2:(NSString *)carrier2
                                                  mailMessage:(NSString *)mailMessage
                                                        brand:(NSString *)brand
                                                         type:(int)type;


/*
 * Invoke confirmation payment institutions
 */
- (void)institutionsAndcompaniesConfirmResultFromSecondKeyFactor:(NSString *)secondKeyFactor;

/**
 * Obtains the payments public services list
 */
- (void)obtainPaymentPublicServices;


/**
 * Obtains the public service - electric service - payment inicialization
 *
 * @param company the company
 */
- (void)obtainPaymentPSElectricServicesInitializationForCompany:(NSString *)company;


/**
 * Obtains the public service - electric service - payment data
 *
 * @param company the company
 * @param supplies the supplies
 */
- (void)obtainPaymentPSElectricServicesDataForCompany:(NSString *)company
                                             supplies:(NSString *)supplies;

/**
 * Obtains the public service - electric service - payment confirmation
 *
 * @param company the company
 * @param issue the issue
 * @param idPayment the payment identification
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentPSElectricServicesConfirmationForCompany:(NSString *)company
                                                        issue:(NSString *)issue 
                                                    idPayment:(NSString *)idPayment 
                                                       email1:(NSString *)email1 
                                                       email2:(NSString *)email2 
                                                 phoneNumber1:(NSString *)phoneNumber1 
                                                 phoneNumber2:(NSString *)phoneNumber2 
                                                     carrier1:(NSString *)carrier1 
                                                     carrier2:(NSString *)carrier2 
                                                      message:(NSString *)message;

/**
 * Obtains the public service - electric service - payment success
 *
 * @param company the company
 * @param secondFactorKey The second factor key: sms key or coordinates key
 */
- (void)obtainPaymentPSElectricServicesSuccessForCompany:(NSString *)company
                                         secondFactorKey:(NSString *)secondFactorKey;


/**
 * Obtains the public service - water service - payment inicialization
 *
 * @param company the company
 */
- (void)obtainPaymentPSWaterServicesInitializationForCompany:(NSString *)company;


/**
 * Obtains the public service - water service - payment data
 *
 * @param company the company
 * @param supplies the supplies
 */
- (void)obtainPaymentPSWaterServicesDataForCompany:(NSString *)company
                                          supplies:(NSString *)supplies;

/**
 * Obtains the public service - water service - payment confirmation
 *
 * @param company the company
 * @param issue the issue
 * @param payments the payment identification
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentPSWaterServicesConfirmationForCompany:(NSString *)company
                                                     issue:(NSString *)issue
												idPayments:(NSString *)payments
                                                    email1:(NSString *)email1 
                                                    email2:(NSString *)email2 
                                              phoneNumber1:(NSString *)phoneNumber1 
                                              phoneNumber2:(NSString *)phoneNumber2 
                                                  carrier1:(NSString *)carrier1 
                                                  carrier2:(NSString *)carrier2 
                                                   message:(NSString *)message;

/**
 * Obtains the public service - water service - payment success
 *
 * @param company the company
 * @param secondFactorKey The second factor key: sms key or coordinates key
 */
- (void)obtainPaymentPSWaterServicesSuccessForCompany:(NSString *)company
                                      secondFactorKey:(NSString *)secondFactorKey;




/**
 * Obtains the public service - phone - payment inicialization
 *
 * @param company the company
 */
- (void)obtainPaymentPSPhoneInitializationForCompany:(NSString *)company;

/**
 * Obtains the public service - phone - payment data
 *
 * @param company The company.
 * @param phoneNumber The phone number.
 */
- (void)obtainPaymentPSPhoneServicesDataForCompany:(NSString *)company
                                       phoneNumber:(NSString *)phoneNumber;

/**
 * Obtains the public service - phone - payment confirmation
 *
 * @param company the company
 * @param issue the issue
 * @param idPayment the payment identification
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentPSPhoneConfirmationForCompany:(NSString *)company
                                             issue:(NSString *)issue 
                                         idPayment:(NSString *)idPayment 
                                            email1:(NSString *)email1 
                                            email2:(NSString *)email2 
                                      phoneNumber1:(NSString *)phoneNumber1 
                                      phoneNumber2:(NSString *)phoneNumber2 
                                          carrier1:(NSString *)carrier1 
                                          carrier2:(NSString *)carrier2 
                                           message:(NSString *)message;

/**
 * Obtains the public service - phone - payment success
 *
 * @param company the company
 * @param secondFactorKey The second factor key: sms key or coordinates key
 */
- (void)obtainPaymentPSPhoneSuccessForCompany:(NSString *)company
                              secondFactorKey:(NSString *)secondFactorKey;


/**
 * Obtains the public service - cellular - payment inicialization
 *
 * @param company the company
 */
- (void)obtainPaymentPSCellularInitializationForCompany:(NSString *)company;

/**
 * Obtains the public service - phone - payment data
 *
 * @param company The company
 * @param phoneNumber The phone number
 */
- (void)obtainPaymentPSCellularServicesDataForCompany:(NSString *)company
                                          phoneNumber:(NSString *)phoneNumber;

/**
 * Obtains the public service - Cellular - payment confirmation
 *
 * @param company the company
 * @param issue the issue
 * @param idPayment the payment identification
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentPSCellularConfirmationForCompany:(NSString *)company
                                                issue:(NSString *)issue 
                                            idPayment:(NSString *)idPayment 
                                               email1:(NSString *)email1 
                                               email2:(NSString *)email2 
                                         phoneNumber1:(NSString *)phoneNumber1 
                                         phoneNumber2:(NSString *)phoneNumber2 
                                             carrier1:(NSString *)carrier1 
                                             carrier2:(NSString *)carrier2 
                                              message:(NSString *)message;

/**
 * Obtains the public service - Cellular - payment success
 *
 * @param company the company
 * @param secondFactorKey The second factor key: sms key or coordinates key
 */
- (void)obtainPaymentPSCellularSuccessForCompany:(NSString *)company
                                 secondFactorKey:(NSString *)secondFactorKey;

/**
 * Obtains the public service - cellular - payment inicialization
 *
 * @param company the company
 */
- (void)obtainPaymentPSGasInitializationForCompany:(NSString *)company;

/**
 * Obtains the Continental card payment inicialization
 */
- (void)obtainPaymentCardContinentalInitialization;

/**
 * Obtains the Continental card payment own account data
 *
 * @param issue the issue
 */
- (void)obtainPaymentCardContinentalOwnAccountDataForIssue:(NSString *)issue;

/**
 * Obtains the Continental card payment own account confirmation
 *
 * @param issue the issue
 * @param currency the currency
 * @param amount the amount
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentCardContinentalOwnAccountConfirmationForIssue:(NSString *)issue 
                                                          currency:(NSString *)currency 
                                                            amount:(NSString *)amount 
                                                            email1:(NSString *)email1 
                                                            email2:(NSString *)email2 
                                                      phoneNumber1:(NSString *)phoneNumber1 
                                                      phoneNumber2:(NSString *)phoneNumber2 
                                                          carrier1:(NSString *)carrier1 
                                                          carrier2:(NSString *)carrier2 
                                                           message:(NSString *)message;

/**
 * Obtains the Continental card payment own account success
 *
 */
- (void)obtainPaymentCardContinentalOwnAccountSuccess;

/**
 * Obtains the Continental card payment third account data
 *
 * @param issue the issue
 */
- (void)obtainPaymentCardContinentalThirdAccountDataForIssue:(NSString *)issue;

/**
 * Obtains the Continental card payment third account confirmation
 *
 * @param issue the issue
 * @param currency the currency
 * @param amount the amount
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentCardContinentalThirdAccountConfirmationForIssue:(NSString *)issue 
                                                            currency:(NSString *)currency 
                                                              amount:(NSString *)amount 
                                                              email1:(NSString *)email1 
                                                              email2:(NSString *)email2 
                                                        phoneNumber1:(NSString *)phoneNumber1 
                                                        phoneNumber2:(NSString *)phoneNumber2 
                                                            carrier1:(NSString *)carrier1 
                                                            carrier2:(NSString *)carrier2 
                                                             message:(NSString *)message;

/**
 * Obtains the Continental card payment third account success
 *
 * @param secondFactorKey The second factor key
 */
- (void)obtainPaymentCardContinentalThirdAccountSuccessForSecondFactorKey:(NSString *)secondFactorKey;

/**
 * Obtains the Other banks card payment inicialization
 */
- (void)obtainPaymentCardOtherBanksInitialization;

/**
 * Obtains the other banks card payment data
 *
 * @param aClass A class
 * @param destBank The destination bank
 * @param accountNumber The account number
 */
- (void)obtainPaymentCardOtherBankDataForClass:(NSString *)aClass 
                                      destBank:(NSString *)destBank 
                                 accountNumber:(NSString *)accountNumber;

/**
 * Obtains the other banks card payment confirmation
 *
 * @param locality the locality
 * @param issue the issue
 * @param currency the currency
 * @param amount the amount
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentCardCardOtherBankConfirmationForLocality:(NSString *)locality
                                                        issue:(NSString *)issue 
                                                     currency:(NSString *)currency 
                                                       amount:(NSString *)amount 
                                                       email1:(NSString *)email1 
                                                       email2:(NSString *)email2 
                                                 phoneNumber1:(NSString *)phoneNumber1 
                                                 phoneNumber2:(NSString *)phoneNumber2 
                                                     carrier1:(NSString *)carrier1 
                                                     carrier2:(NSString *)carrier2 
                                                      message:(NSString *)message;


/**
 * Obtains the other banks card payment confirmation
 *
 * @param locality the locality
 * @param issue the issue
 * @param currency the currency
 * @param amount the amount
 * @param beneficiary the beneficiary
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainFOPaymentCardCardOtherBankConfirmationForIndicatorOP:(NSString*)indicator
                                                           Service:(NSString*)service
                                                       Description:(NSString*)description
                                                        CardNumber:(NSString*)cardNumber
                                                         ClassCard:(NSString*)classCard
                                                   DestinationBank:(NSString*)destinationBank
                                                          Locality:(NSString *)locality
                                                             issue:(NSString *)issue
                                                          currency:(NSString *)currency
                                                            amount:(NSString *)amount
                                                            email1:(NSString *)email1
                                                            email2:(NSString *)email2
                                                      phoneNumber1:(NSString *)phoneNumber1
                                                      phoneNumber2:(NSString *)phoneNumber2
                                                          carrier1:(NSString *)carrier1
                                                          carrier2:(NSString *)carrier2
                                                           message:(NSString *)message;
/*
 * Obtains the other banks card payment confirmation
 *
 * @param flag the flow selection
 * @param beneficiary the beneficiary
 */
- (void)obtainPaymentCardCardOtherBankTINConfirmationForFlag:(NSString *)flag
                                                 beneficiary:(NSString *)beneficiary;

/**
 * Obtains the other banks card payment success
 *
 * @param secondFactorKey The second factor key: sms key or coordinates key
 */
- (void)obtainPaymentCardOtherBankSuccessForSecondFactorKey:(NSString *)secondFactorKey;

/**
 * Obtains the recharge payment data
 *
 * @param company the company
 */
- (void)obtainPaymentRechargeDataForCompany:(NSString *)company;

/**
 * Obtains the recharge payment data
 *
 * @param phoneNumber the phoneNumber
 * @param issue the issue
 * @param amount the amount
 * @param email1 the email1
 * @param email2 the email2
 * @param phoneNumber1 the phoneNumber1
 * @param phoneNumber2 the phoneNumber2
 * @param carrier1 the carrier1
 * @param carrier2 the carrier2
 * @param message the message
 */
- (void)obtainPaymentRechargeConfirmationForPhoneNumber:(NSString *)phoneNumber
                                                  issue:(NSString *)issue 
                                                 amount:(NSString *)amount 
                                                 email1:(NSString *)email1 
                                                 email2:(NSString *)email2 
                                           phoneNumber1:(NSString *)phoneNumber1 
                                           phoneNumber2:(NSString *)phoneNumber2 
                                               carrier1:(NSString *)carrier1 
                                               carrier2:(NSString *)carrier2 
                                                message:(NSString *)message;


/**
 * Obtains the recharge payment success of Movistar or Claro
 *
 * @param secondFactorKey The second factor key: sms key or coordinates key
 * @param carrier the carrier to get information.
 */
- (void)obtainPaymentRechargeSuccessForSecondFactorKey:(NSString *)secondFactorKey
                                               carrier:(NSString *)carrier;

#pragma mark Frequent Operation Reactive Methods
-(void)obtainCashMobileFrequentOperationReactiveStepOne;
-(void)obtainTranferToOtherBankFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                       andbeneficiaryAccount:(NSString *)beneficiaryAccount
                                                          andDestinationBank:(NSString *)destinationBank
                                                          andBeneficiaryName:(NSString *)beneficiaryName
                                                            andOriginAccount:(NSString *)originAccount
                                                                   andAmount:(NSString *)amount
                                                                 andCurrency:(NSString *)currency
                                                              andDescription:(NSString *)description
                                                           andDocumentNumber:(NSString *)documentNumber
                                                             andDocumentCode:(NSString *)documentCode
                                                                      andITF:(BOOL)itf;

-(void)obtainTranferToGiftCardFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                      andbeneficiaryAccount:(NSString *)beneficiaryAccount
                                                                andCurrency:(NSString *)destinationBank
                                                           andOriginAccount:(NSString *)originAccount
                                                                  andAmount:(NSString *)amount;

-(void)obtainTranferToThirdBankFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                       andbeneficiaryAccount:(NSString *)beneficiaryAccount
                                                          andBeneficiaryName:(NSString *)beneficiaryName
                                                            andOriginAccount:(NSString *)originAccount
                                                                   andAmount:(NSString *)amount
                                                      andBeneficiaryCurrency:(NSString *)beneficiaryCurrency
                                                           andOriginCurrency:(NSString *)originCurrency;

-(void)obtainPaymentToThirdBankFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                       andDestinationAccount:(NSString *)destinationAccount
                                                   andDestinationAccountType:(NSString *)destinationAccountType
                                                              andBeneficiary:(NSString *)beneficiary
                                                                     andFlag:(NSString *)flag
                                                                 andCurrency:(NSString *)currency
                                                            andOriginAccount:(NSString *)originAccount
                                                                   andAmount:(NSString *)amount;

-(void)obtainPaymentToOtherBankFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                       andDestinationAccount:(NSString *)destinationAccount
                                                   andDestinationAccountType:(NSString *)destinationAccountType
                                                      andNameDestinationBank:(NSString *)nameDestinationBank
                                                                 andBankCode:(NSString *)bankCode
                                                             andLocalityName:(NSString *)localityName
                                                             andLocalityCode:(NSString *)localityCode
                                                              andBeneficiary:(NSString *)beneficiary
                                                                 andCurrency:(NSString *)currency
                                                            andOriginAccount:(NSString *)originAccount
                                                                   andAmount:(NSString *)amount;

-(void)obtainPaymentToInstitutionAndCompaniesFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                                               andCodMatri:(NSString *)codMatri
                                                                                 andsClase:(NSString *)clase
                                                                               andsEntidad:(NSString *)entidad
                                                                            andsCodEntidad:(NSString *)codEntidad
                                                                                andArreglo:(NSString *)arreglo
                                                                           andsDescripcion:(NSString *)descripcion
                                                                            andCuentaCargo:(NSString *)cuentaCargo
                                                                             andHolderName:(NSString *)sNombre;


-(void)obtainRechargeMovistarCellPhoneFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                                         andCompany:(NSString *)company
                                                                     andServiceType:(NSString *)serviceType
                                                                 andCellPhoneNumber:(NSString *)cellPhoneNumber
                                                                         andAccount:(NSString *)account
                                                                          andAmount:(NSString *)amount;

-(void)obtainRechargeClaroCellPhoneFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                                  andServiceType:(NSString *)serviceType
                                                                      andAccount:(NSString *)account
                                                              andCellPhoneNumber:(NSString *)cellPhoneNumber;

-(void)obtainServiceClaroCellPhoneFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                                 andServiceType:(NSString *)serviceType
                                                             andCellPhoneHolder:(NSString *)cellPhoneHolder
                                                                     andAccount:(NSString *)account
                                                                   andCellPhone:(NSString *)cellPhone;

-(void)obtainServiceMovistarCellPhoneFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                                        andCompany:(NSString *)company
                                                                    andServiceType:(NSString *)serviceType
                                                                andCellPhoneNumber:(NSString *)cellPhoneNumber
                                                                         andHolder:(NSString *)holder
                                                                        andAccount:(NSString *)account;

-(void)obtainServiceWaterFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                            andCompany:(NSString *)company
                                                       andSupplyNumber:(NSString *)supplyNumber
                                                             andHolder:(NSString *)holder
                                                        andServiceType:(NSString *)serviceType
                                                            andAccount:(NSString *)account
                                                             andAmount:(NSString *)amount
                                                           andCurrency:(NSString *)currency;

-(void)obtainFrequentOperationReactiveStepTwoWithOperation:(NSString *)operation
                                                andChannel:(NSString *)channel
                                             andShortAlias:(NSString *)shortAlias
                                             andLargeAlias:(NSString *)largeAlias
                                                    andDay:(NSString *)day
                                                andCellSMS:(NSString *)cellSMS
                                            andCellSMSCode:(NSString *)cellSMSCode
                                                  andEmail:(NSString *)email
                                               anEmailCode:(NSString *)emailCode;

- (void)obtainFrequentOperationReactiveStepThreeFromSecondKeyFactor:(NSString *)secondKeyFactor
                                                       andOperation:(NSString *)operation;

- (void)obtainCampaignList;


#pragma mark -
#pragma mark SafetyPay methods

- (void) obtainSafetyPayStatusInfo ;

- (void) obtainSafetyPayTransactionInfoByTransactionNumber: (NSString *) transactionNumber AndAmount :(NSString *) amount;

- (void) obtainSafetyPayPaymentDetailsByAccountNumber: (NSString *) accountNumber
                                    cellphoneNumber1 : (NSString *) cellphoneNumber1
                                    cellphoneNumber2 : (NSString *) cellphoneNumber2
                                            carrier1 : (NSString *) carrier1
                                            carrier2 : (NSString *) carrier2
                                              email1 : (NSString *) email1
                                              email2 : (NSString *) email2
                                             message : (NSString *) message;

//- (void) obtainSafetyPaySuccessFromConfirmPaymentWithSecondFactor : (NSString *) secondFactor;
- (void) safetyPayResultFromSecondKeyFactor : (NSString *) secondKeyFactor;

#pragma mark - Manage  third account defaults
/**
 * Obtain the Initial list of third account
 *
 */
- (void) obtainThirdAccountInformationInitialListInfo:(NSString *)nextPage type:(NSString*)type;

- (void) obtainValidationThirdAccountFromNickName:(NSString*)nickName;

- (void) obtainRegisterThirdAccountFromAccountNumber:(NSString *)accountNumber
                                        officeNumber:(NSString *)officeNumber
                                         accountType:(NSString*)accountType
                                            nickName:(NSString*)nickName;

- (void) obtainRegisterOtherBankFromAccountNumber:(NSString *)accountNumber
                                     entityNumber:(NSString*)entityNumber
                                     officeNumber:(NSString *)officeNumber
                                         ccNumber:(NSString*)ccNumber
                                      accountType:(NSString*)accountType
                                         nickName:(NSString*)nickName;

- (void) obtainDeleteThirdAccountFromAccountType:(NSString *)accountType
                                        nickName:(NSString*)nickName andSecondFactorKey:(NSString *)secondFactorKey;

- (void) confirmOperationAccountFromSecondKeyFactor:(NSString *) secondKeyFactor;

#pragma mark - Sesion

/**
 * Obtain the Initial list of frequent operation
 *
 */
- (void) obtainFrequentInformationInitialListInfo:(NSString *)nextPage;

/**
 * Obtain the execute of frequent operation
 *
 * @param codService Unique code service
 * @param afilitionNum The afiliation number
 * @param asociationNick the asociation nick for the operation
 * @param afilitationType the afiliation type
 */- (void) obtainFrequentOperationExecuteInfoFromCodService:(NSString *)codService
                                                AfilitionNum:(NSString *)afilitionNum
                                              AsociationNick:(NSString *)asociationNick
                                             AfilitationType:(NSString *)afilitationType;



/**
 * Obtain the Initial incremedt of line
 */
- (void) incrementLineOperationStartup;

/**
 * Obtain the Confirmation incremedt of line
 */
- (void) incrementLineOperationConfirmationFromCodeCard:(NSString*)codeCard
                                      amountToIncrement:(NSString*)amountToIncrement
                                                  email:(NSString*)email;
/**
 * Obtain the Sucess incremedt of line
 */
- (void) incrementLineOperationSucess:(NSString*)secondFactorKey;

/**
 * Obtain the Initial Fast Loan
 */
- (void) fastLoanOperationStartup;

/**
 * Obtain the Fast Loan Term List
 */
- (void) fastLoanOperationTerm:(NSString*)amount;

/**
 * Obtain the Fast Loan Simulation
 */
- (void) fastLoanOperationSimulation:(NSString*)amount
                                term:(NSString*)term
                            dayOfPay:(NSString*)dayOfPay
                           quotaType:(NSString*)quotaType
                         monthDobles:(NSString*)monthDobles
                        accountIndex:(NSString*)accountIndex
                      emailSelection:(NSString*)emailSelection
                       emailEditable:(NSString*)emailEditable
                              action:(NSString*)action;

/**
 * Obtain the Fast Loan Confirm
 */
- (void) fastLoanOperationConfirm;

/**
 * Obtain the Fast Loan Schedule
 */
- (void) fastLoanOperatioSchedule:(NSString*)opAlternative quotaNumber:(NSString*)quotaNumber;

/**
 * Obtain the Fast Loan Sucess
 */
- (void) fastLoanOperationSucess:(NSString*)secondFactorKey;


/**
 * Obtain the Salary Advance Affiliation
 */
- (void) salaryAdvanceAffiliationOperation;

/**
 * Obtain the Salary Advance Receive
 */
- (void) salaryAdvanceReceiveOperation:(BOOL)isSilent;

/**
 * Obtain the Salary Advance Summary
 */
- (void) salaryAdvanceSummaryOperation:(NSString*)secondFactorKey;

/**
 * Obtain the Salary Advance Summary
 */
- (void) salaryAdvanceAffiliationConfirmOperation:(NSString*)secondFactorKey :(NSString *)email ;


#pragma mark - Sesion

/**
 * Close
 */
- (void)close;

/**
 * Logout
 */
- (void)logout;


- (NSString *) returnEscapedParameterSequenceForSafetyPayTransactionInfoWithTransactionNumber : (NSString *) transactionNumber AndAmount : (NSString *) amount;

- (NSString *) returnEscapedParameterSequenceForSafetyPayPaymentInfoWithAccountNumber:(NSString *) accountNumber
                                                                             email1:(NSString *)email1
                                                                             email2:(NSString *)email2
                                                                       phoneNumber1:(NSString *)phoneNumber1
                                                                       phoneNumber2:(NSString *)phoneNumber2
                                                                           carrier1:(NSString *)carrier1
                                                                           carrier2:(NSString *)carrier2
                                                                            message:(NSString *)message;

- (NSString *) returnEscapedParameterSequenceSafetyPayResultFromSecondKeyFactor :(NSString *) secondFactorKey;
@end
