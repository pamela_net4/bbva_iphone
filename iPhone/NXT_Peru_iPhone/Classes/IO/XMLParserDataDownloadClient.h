/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseDataDownloadClient.h"


/**
 * Defines the XMLParserDataDownloadClient error domain
 */
extern NSString * const XMLParserDataDownloadClientErrorDomain;


/**
 * Enumerates the error codes for the XMLParserDataDownloadClient error domain
 */
typedef enum {

    XMLParserDataDownloadClientNoParserObjectError = 1 //!<The parser object provided by the child class is nil. User information is always nil
    
} XMLParserDataDownloadClientError;


/**
 * Base class for DataDownloadClients instances that must analyze the downloaded data
 * as an XML document. XML parsing is performed in a separate thread
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface XMLParserDataDownloadClient : BaseDataDownloadClient {

}

@end
