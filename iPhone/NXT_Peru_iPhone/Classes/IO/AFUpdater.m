//
//  VDAConnectionManager.m
//  VidoorAdmin
//
//  Created by Alvaro Herrera Cotrina on 2/12/16.
//  Copyright © 2016 Alvaro Herrera. All rights reserved.
//

#import "AFUpdater.h"
#import "XMLReader.h"
#import "MCBFacade.h"
#import "Constants.h"
#import "MATLogNames.h"
#import "HTTPInvoker.h"
#import "LoanList.h"
#import "MATLogNames.h"
#import "MCBFacade.h"
#import "MutualFundList.h"
#import "NSString+URLAndHTMLUtils.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Updater.h"
#import "StringKeys.h"
#import "Tools.h"







@implementation AFUpdater

@synthesize url, delegate, manager;

#pragma mark -
#pragma mark - Lifecycle

+ (id)sharedManager {
    static AFUpdater *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (id)init {
    if (self = [super init]) {
        manager = [AFHTTPRequestOperationManager manager];
        AFHTTPRequestSerializer * requestSerializer = [AFHTTPRequestSerializer serializer];
        AFHTTPResponseSerializer * responseSerializer = [AFHTTPResponseSerializer serializer];
        
        NSString *ua = @"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25";
        [requestSerializer setValue:ua forHTTPHeaderField:@"User-Agent"];
        //    [requestSerializer setValue:@"application/xml" forHTTPHeaderField:@"Content-type"];
        responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml", nil];
        manager.responseSerializer = responseSerializer;
        manager.requestSerializer = requestSerializer;
        [self resetUrl];
    }
    return self;
}

#pragma mark -
#pragma mark - Private

- (void)resetUrl {
    url = kBaseUrl;
    AFHTTPRequestSerializer * requestSerializer = [AFHTTPRequestSerializer serializer];
    AFHTTPResponseSerializer * responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSString *ua = @"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25";
    [requestSerializer setValue:ua forHTTPHeaderField:@"User-Agent"];
    //    [requestSerializer setValue:@"application/xml" forHTTPHeaderField:@"Content-type"];
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml", nil];
    
}

- (void)postUrl:(NSString *)urlString WithParams:(NSDictionary *)params OnCompletionTypeArray:(CompletionBlockWithFinishedArray)completion {
    url = [url stringByAppendingString:urlString];
    
    
    [manager POST:url parameters:params
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSArray *array = (NSArray *)responseObject;
         if(completion) completion(YES, array);
         
        
         
     }
          failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         if(completion) completion(NO, nil);
     }];
    [self resetUrl];
}

- (void)postUrl:(NSString *)urlString WithParams:(NSDictionary *)params OnCompletionTypeDictionary:(CompletionBlockWithFinishedDictionary)completion {
    url = [url stringByAppendingString:urlString];
   // NSString *encoded = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"URL: %@",url);
    NSLog(@"Parametros: %@",params);
    
    [manager POST:url
       parameters:params
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSData * data = (NSData *)responseObject;
              fetchedXML = [NSString stringWithCString:[data bytes] encoding:NSISOLatin1StringEncoding];
              
              if ([fetchedXML rangeOfString:@"<ERROR>"].location == NSNotFound) {
                  
              } else {
                  
                  NSRange r1 = [fetchedXML rangeOfString:@"<MENSAJE>"];
                  NSRange r2 = [fetchedXML rangeOfString:@"</MENSAJE>"];
                  NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
                  NSString *sub = [fetchedXML substringWithRange:rSub];
                  [[NSUserDefaults standardUserDefaults] setObject:sub forKey:@"mensajeError"];
                  NSLog(@"%@",sub);
                  
              }
              
              NSLog(@"Respyesta: %@", fetchedXML);
              
              NSRange r1 = [fetchedXML rangeOfString:@"<MSG-S>"];
              NSRange r2 = [fetchedXML rangeOfString:@"</MSG-S>"];
              NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
              NSString *sub = [fetchedXML substringWithRange:rSub];
              NSString *subAEnviar = [NSString stringWithFormat:@"<MSG-S>%@</MSG-S>",sub];
              
              // Parse the XML into a dictionary
              NSError *parseError = nil;
              NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLString:subAEnviar error:&parseError];
              
              if(completion) completion(YES, xmlDictionary);
              
          }
          failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         if(completion) completion(NO, nil);
     }];
    [self resetUrl];
}

- (void)getUrl:(NSString *)urlString WithParams:(NSDictionary *)params OnCompletionTypeDictionary:(CompletionBlockWithFinishedDictionary)completion {
    url = [url stringByAppendingString:urlString];
    [manager GET:url parameters:params
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSDictionary *dictionary = (NSDictionary *)responseObject;
         if(completion) completion(YES, dictionary);
     }
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         if(completion) completion(NO, nil);
     }];
    [self resetUrl];
}

- (void)getUrl:(NSString *)urlString WithParams:(NSDictionary *)params OnCompletionTypeArray:(CompletionBlockWithFinishedArray)completion {
    url = [url stringByAppendingString:urlString];
    [manager GET:url parameters:params
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([responseObject isKindOfClass:[NSArray class]]) {
             NSArray *array = (NSArray *)responseObject;
             if(completion) completion(YES, array);
         }
     }
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         if(completion) completion(NO, nil);
     }];
    [self resetUrl];
}

- (void)deleteUrl:(NSString *)urlString WithParams:(NSDictionary *)params OnCompletionTypeDictionary:(CompletionBlockWithFinishedDictionary)completion {
    url = [url stringByAppendingString:urlString];
    [manager DELETE:url parameters:params
            success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSDictionary *dictionary = (NSDictionary *)responseObject;
         if(completion) completion(YES, dictionary);
     }
            failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         if(completion) completion(NO, nil);
     }];
    [self resetUrl];
}

- (void)deleteUrl:(NSString *)urlString WithParams:(NSDictionary *)params OnCompletion:(CompletionBlockWithFinishedArray)completion {
    url = [url stringByAppendingString:urlString];
    [manager DELETE:url parameters:params
            success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([responseObject isKindOfClass:[NSArray class]]) {
             NSArray *array = (NSArray *)responseObject;
             if(completion) completion(YES, array);
         }
     }
            failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         if(completion) completion(NO, nil);
     }];
    [self resetUrl];
}

- (void)putUrl:(NSString *)urlString WithParams:(NSDictionary *)params OnCompletionTypeDictionary:(CompletionBlockWithFinishedDictionary)completion {
    url = [url stringByAppendingString:urlString];
    [manager PUT:url parameters:params
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSDictionary *dictionary = (NSDictionary *)responseObject;
         if(completion) completion(YES, dictionary);
     }
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         if(completion) completion(NO, nil);
     }];
    [self resetUrl];
}

- (void)putUrl:(NSString *)urlString WithParams:(NSDictionary *)params OnCompletion:(CompletionBlockWithFinishedArray)completion {
    url = [url stringByAppendingString:urlString];
    [manager PUT:url parameters:params
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([responseObject isKindOfClass:[NSArray class]]) {
             NSArray *array = (NSArray *)responseObject;
             if(completion) completion(YES, array);
         }
     }
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         if(completion) completion(NO, nil);
     }];
    [self resetUrl];
}

#pragma mark -
#pragma mark - Alta

- (void)verifyUser:(NSString*)numTarjeta andClave:(NSString*)claveTarjeta andParameters:(NSDictionary *)parameters OnCompletion:(CompletionBlockWithFinishedDictionary)completion {
    
    
   
    
    NSString *string1 = [NSString stringWithFormat:@"%@%@",BDPN_TARGET_PATH,VERIFY_OPERATION];
    
    NSDictionary *params=@{@"numtar1":numTarjeta,@"Nip":claveTarjeta};
    
    [self postUrl:string1 WithParams:params OnCompletionTypeDictionary:^(BOOL finished, NSDictionary *dictionary) {
        if (completion)
        {
            NSString *Verifying = [[NSUserDefaults standardUserDefaults] objectForKey:@"Verifying"];
            
            if ([Verifying isEqualToString:@"alta"]) {
                
                [self processMCBNotifications:MAT_LOG_SUSCRIPTION_CLIENT_VERIFY];
                
            }
            if ([Verifying isEqualToString:@"modificacion"]) {
                
                [self processMCBNotifications:MAT_LOG_CHANGE_CLIENT_VERIFY];
                
            }
            
            completion(finished, dictionary);
            
        }
    }];
    
}


- (void)openSession:(NSString*)idPlace andParameters:(NSDictionary *)parameters OnCompletion:(CompletionBlockWithFinishedDictionary)completion {
    
    NSString *string = [NSString stringWithFormat:@"%@%@", BDPN_TARGET_PATH,REGISTRATION_REGISTER];
    
    [self postUrl:string WithParams:nil OnCompletionTypeDictionary:^(BOOL finished, NSDictionary *dictionary) {
        if (completion)
        {
            
            completion(finished, dictionary);
            
        }
    }];
    
}


- (void)doAlta:(NSString*)email andContrasenia:(NSString*)contrasenia andParameters:(NSDictionary *)parameters OnCompletion:(CompletionBlockWithFinishedDictionary)completion {
    
    
    NSString *string1 = [NSString stringWithFormat:@"%@%@",BDPN_TARGET_PATH,DO_REGISTRATION_CHANGE];
    
    NSDictionary *params=@{@"email":email,@"verified":@"N",@"PasswordSistema":contrasenia};
    
    [self postUrl:string1 WithParams:params OnCompletionTypeDictionary:^(BOOL finished, NSDictionary *dictionary) {
        if (completion)
        {
            
            NSString *registration = [[NSUserDefaults standardUserDefaults] objectForKey:@"registration"];
            
            if ([registration isEqualToString:@"alta"]) {
                
                [self processMCBNotifications:MAT_LOG_SUSCRIPTION_CLIENT_DO];
                
            }
            if ([registration isEqualToString:@"modificacion"]) {
                
                [self processMCBNotifications:MAT_LOG_CHANGE_CLIENT_DO];
                
            }

            
            completion(finished, dictionary);
        }
    }];
    
}

#pragma mark -
#pragma mark - Olvido

- (void)openSessionOlvido:(NSString*)idPlace andParameters:(NSDictionary *)parameters OnCompletion:(CompletionBlockWithFinishedDictionary)completion {
    
    
    NSString *string = [NSString stringWithFormat:@"%@%@", BDPN_TARGET_PATH,CHANGE_REGISTER];
    
    [self postUrl:string WithParams:nil OnCompletionTypeDictionary:^(BOOL finished, NSDictionary *dictionary) {
        
        if (completion)
        {
            
            completion(finished, dictionary);
        }
    }];
    
    
}

/**
 * Process MCB notifications after an UpdaterOperation invocation
 *
 * @param notification The notification.
 * @private
 */
- (void)processMCBNotifications:(NSString *)notification {
    
    [MCBFacade invokeLogInvokedAppMethodWithAppName:MCB_APPLICATION_NAME
                                             appKey:MCB_APPLICATION_KEY
                                         appCountry:APP_COUNTRY
                                         appVersion:APP_VERSION_STRING
                                           latitude:0.0f
                                          longitude:0.0f
                                      validLocation:NO
                                          appMethod:notification];
}

@end
