/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


/**
 * DonwloadListener must be implemented by those interested on download progress
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol DownloadListener

@required

/**
 * Notifies the download has just started
 *
 * @param aDownloadId The local download identification
 * @param aSize The downloaded data size if known, 0 if unknown
 */
- (void)startDownload:(NSString *)aDownloadId withSize:(unsigned long long)aSize;

/**
 * Notifies the download has just finished
 *
 * @param aDownloadId The local download identificaiton
 * @param aSize The downloaded data size
 */
- (void)endDownload:(NSString *)aDownloadId withSize:(unsigned long long)aSize;

/**
 * Notifies the download has finished in error
 *
 * @param aDownloadId The local download identificaiton
 * @param anError The download error
 * @param aXMLString string will be parsed
 */
- (void)download:(NSString *)aDownloadId error:(NSError *)anError xmlString:(NSString *)aXMLString;

/**
 * Notifies the download has finished with a status code different than OK_STATUS (200)
 *
 * @param aDownloadId The local download identificaiton
 * @param aStatusCode The status code that finished the download
 */
- (void)download:(NSString *)aDownloadId finishedWithStatusCode:(NSInteger)aStatusCode;

/**
 * Notifies the download was cancelled
 *
 * @param aDownloadId The local download identificaiton
 */
- (void)cancelledDownload:(NSString *)aDownloadId;

@optional

/**
 * Notifies the download progress
 *
 * @param aDownloadId The local download identificaiton
 * @param aSize The current data size downloaded
 */
- (void)downloadProgress:(NSString *)aDownloadId withSize:(unsigned long long)aSize;

@end
