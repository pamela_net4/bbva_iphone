//
//  VDAConnectionManager.h
//  VidoorAdmin
//
//  Created by Alvaro Herrera Cotrina on 2/12/16.
//  Copyright © 2016 Alvaro Herrera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "Constants.h"


static NSString *const kBaseUrl = TARGET_SERVER;

typedef void (^CompletionBlockWithFinishedDictionary) (BOOL finished, NSDictionary *dictionary);
typedef void (^CompletionBlockWithFinishedArray) (BOOL finished, NSArray *array);
typedef void (^CompletionBlock) ();

@protocol AFUpdaterProtocol

@required -(void)authenticationFailedBlock;
@required -(void)anotherWeirdErrorBlock;
@optional -(void)successBlockWithArray:(NSArray *)response;
@optional -(void)successBlockWithDictionary:(NSDictionary *)response;
@optional -(void)failureBlock;

@end

@interface AFUpdater: NSObject{
    AFHTTPRequestOperationManager *manager;

    NSString *fetchedXML;
}

@property (nonatomic, strong) AFHTTPRequestOperationManager *manager;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, weak) id<AFUpdaterProtocol> delegate;

+ (id)sharedManager;


//Suscription

- (void)openSession:(NSString*)idPlace andParameters:(NSDictionary *)parameters OnCompletion:(CompletionBlockWithFinishedDictionary)completion;
- (void)verifyUser:(NSString*)numTarjeta andClave:(NSString*)claveTarjeta andParameters:(NSDictionary *)parameters OnCompletion:(CompletionBlockWithFinishedDictionary)completion;
- (void)doAlta:(NSString*)email andContrasenia:(NSString*)contrasenia andParameters:(NSDictionary *)parameters OnCompletion:(CompletionBlockWithFinishedDictionary)completion;
- (void)openSessionOlvido:(NSString*)idPlace andParameters:(NSDictionary *)parameters OnCompletion:(CompletionBlockWithFinishedDictionary)completion;
@end
