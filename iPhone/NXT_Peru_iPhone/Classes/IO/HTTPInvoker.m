/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "HTTPInvoker.h"
#import "DownloadListener.h"
#import "DirConfiguration.h"
#import "StringKeys.h"
#import "Constants.h"
#import "Tools.h"
#import "DataDownloadClient.h"

/**
 * HTTP post content type
 */
#define XML_CONTENT_TYPE							@"application/xml; charset=UTF-8"

/**
 * HTTP post content type
 */
#define PARAMETERS_CONTENT_TYPE						@"application/x-www-form-urlencoded; charset=UTF-8"

/**
 * Content type header field
 */
#define CONTENT_TYPE_HEADER_FIELD					@"Content-Type"

/**
 * Cache control header field
 */
#define CACHE_CONTROL_HEADER_FIELD					@"Cache-Control"

/**
 * Cache control no cache value
 */
#define CACHE_CONTROL_NO_CACHE_VALUE               @"no-cache, no-store"

/**
 * Time-out connection interval (in seconds)
 */
#define TIME_OUT_INTERVAL							60.0f

/**
 * Defines the in memory data download maximum size in bytes
 */
#define IN_MEMORY_DOWNLOAD_CACHE					1024

/**
 * Defines the content type header field
 */
NSString * const kContentTypeHeaderFieldInvoker = @"Content-Type";

/*
 * Defines the download start notification name. It is issued when a download is started
 */
NSString * const kDownloadStartNotificationName = @"net.movilok.HTTPInvoker.downloadSarts";

/*
 * Defines the download ends notification name. It is issued whenever a download ends, both successfully orin failure
 */
NSString * const kDownloadStopNotificationName = @"net.movilok.HTTPInvoker.downloadStops";


#pragma mark -

/**
 * Contains information and manages a single data download process
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface DownloadInformation : NSObject {

@private
    
	/**
	 * Local download identification
	 */
	NSString *downloadId_;
	
	/** 
	 * Remote URL where data can be found
	 */
	NSURL *remoteURL_;
	
	/**
	 * Connection to invoke HTTP operation
	 */
	NSURLConnection *connection_;
	
	/**
	 * Connection address as an NSNumber for easy access
	 */
	NSNumber *connectionAddress_;
	
	/** 
	 * Data being retrieved while download is still active
	 */
	NSMutableData *data_;
	
	/**
	 * Download listener to be informed when download events appear
	 */
	id <DownloadListener> downloadListener_;
	
    /**
	 * Download listener to be informed when download events appear
	 */
	id<DataDownloadClient> downloadClient_;
    
	/**
	 * XML parser delegate if any. When not nil, the downloaded information is parsed using this NSXMLParser delegate before notifying the download listener
	 */
	id xmlParserDelegate_;
    
}

/**
 * Provides read only access to the download identification
 */
@property (nonatomic, readonly, copy) NSString *downloadId;

#if defined(DEBUG)
/** 
 * Provides read only access to the data being retrieved while download is still active (only in debug mode)
 */
@property (nonatomic, readonly, copy) NSMutableData *data;
#endif

/**
 * Provides read only access to the remote URL where data can be found
 */
@property (nonatomic, readonly, assign) NSURL *remoteURL;

/**
 * Provides read only access to the memory address where connection object exists
 */
@property (nonatomic, readonly, assign) NSNumber *connectionAddress;

/**
 * Designated instance initializer. Initializes the DownloadInformation instance with the provided information
 *
 * @param aDownloadId The download identification
 * @param aRemoteURL Remote URL to store
 * @param aConnection Connection to store
 * @param aDownloadClient The download client to be informed when events arise
 * @result Initialized DownloadInformation instance
 */
- (id)initWithDownloadId:(NSString *)aDownloadId remoteURL:(NSURL *)aRemoteURL connection:(NSURLConnection *)aConnection
          downloadClient:(id<DataDownloadClient>)aDownloadClient;

/**
 * Designated instance initializer. Initializes the DownloadInformation instance with the provided information
 *
 * @param aDownloadId The download identification
 * @param aRemoteURL Remote URL to store
 * @param aConnection Connection to store
 * @param anXMLParserDelegate The XML parser delegate to store, if any
 * @param aDownloadListener The download listener to store
 * @result Initialized DownloadInformation instance
 */
- (id)initWithDownloadId:(NSString *)aDownloadId remoteURL:(NSURL *)aRemoteURL connection:(NSURLConnection *)aConnection
       xmlParserDelegate:(id)anXMLParserDelegate andDownloadListener:(id<DownloadListener>)aDownloadListener;

/**
 * Appends another chunk of data into the downloaded information
 *
 * @param aDataChunk Chunk of data to append to existing data
 */
- (void)appendData:(NSData *)aDataChunk;

/**
 * Download has started. Download listener is notified
 */
- (void)downloadStarts;

/**
 * Download has finished. Download listener is notified. If an XML parser is defined and parsing finds an error,
 * the listener is notified of the error, not the download finishing
 */
- (void)downloadFinished;

/**
 * Cancels the download. Download listener is notified
 */
- (void)cancel;

/**
 * Download has finished in error. Download listener is notified
 *
 * @param anError The download error
 */
- (void)downloadFinishedWithError:(NSError *)anError;

/**
 * Download has finished due to a status code different than OK_STATUS (200). Download listener is notified
 *
 * @param aStatusCode The status code that caused the download to finish
 */
- (void)downloadFinishedWithStatusCode:(NSInteger)aStatusCode;

/**
 * Parse the content of the received XML on another thread
 */
- (void)beginParseOfXML;

/**
 * Ends the parsing of the XML and returns to main thread
 *
 * @param errorDict parsingError of the parse
 */
- (void)endParseOfXML:(NSDictionary *)errorDict;

/**
 * Ends download
 *
 * @param notifyDownloadFinished notify finished download
 */
- (void)endDownload:(BOOL)notifyDownloadFinished;
	
@end


#pragma mark -

@implementation DownloadInformation

#pragma mark -
#pragma mark Properties

@synthesize downloadId = downloadId_;
#if defined(DEBUG)
@synthesize data = data_;
#endif
@synthesize remoteURL = remoteURL_;
@synthesize connectionAddress = connectionAddress_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates memory used
 */
- (void)dealloc {
    
	[data_ release];
	data_ = nil;
	
	[connectionAddress_ release];
	connectionAddress_ = nil;
	
	[connection_ cancel];
	[connection_ release];
	connection_ = nil;
	
	[remoteURL_ release];
	remoteURL_ = nil;
	
	[downloadId_ release];
	downloadId_ = nil;
	
	downloadListener_ = nil;
	
	[super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated instance initializer. This initializer returns a nil instance because empty DownloadInformation instances are not allowed
 */
- (id) init {
    
	[self autorelease];
	return nil;
    
}

/*
 * Designated instance initializer. Initializes the DownloadInformation instance with the provided information
 */
- (id)initWithDownloadId:(NSString *)aDownloadId remoteURL:(NSURL *)aRemoteURL connection:(NSURLConnection *)aConnection
          downloadClient:(id<DataDownloadClient>)aDownloadClient {
    
	if (self = [super init]) {
        
		downloadId_ = [aDownloadId copy];
		remoteURL_ = [aRemoteURL copy];
		connection_ = [aConnection retain];
		connectionAddress_ = [[NSNumber numberWithInteger:(NSInteger)connection_] retain];
		data_ = [[NSMutableData alloc] init];
		downloadClient_ = aDownloadClient;
        
	}
	
	return self;
    
}

/*
 * Designated instance initializer. Initializes the DownloadInformation instance with the provided information
 */
- (id)initWithDownloadId:(NSString *)aDownloadId remoteURL:(NSURL *)aRemoteURL connection:(NSURLConnection *)aConnection
       xmlParserDelegate:(id)anXMLParserDelegate andDownloadListener:(id<DownloadListener>)aDownloadListener {
    
	if (self = [super init]) {
        
		downloadId_ = [aDownloadId copy];
		remoteURL_ = [aRemoteURL copy];
		connection_ = [aConnection retain];
		connectionAddress_ = [[NSNumber numberWithInteger:(NSInteger)connection_] retain];
		data_ = [[NSMutableData alloc] initWithCapacity:IN_MEMORY_DOWNLOAD_CACHE];
		downloadListener_ = aDownloadListener;
		xmlParserDelegate_ = anXMLParserDelegate;
        
	}
	
	return self;
    
}

#pragma mark -
#pragma mark Data management

/*
 * Appends another chunk of data into the downloaded information
 */
- (void)appendData:(NSData *)aDataChunk {
    
    [data_ appendData:aDataChunk];
    
}

#pragma mark -
#pragma mark Download progress

/*
 * Download has started. Download listener is notified
 */
- (void)downloadStarts {
    
	[downloadListener_ startDownload:downloadId_ withSize:0];
    
}

/*
 * Download has finished. Download listener is notified. If an XML parser is defined and parsing finds an error,
 * the listener is notified of the error, not the download finishing
 */
- (void)downloadFinished {
    
	if (xmlParserDelegate_ != nil) {
        
        NSThread *thread = [[[NSThread alloc] initWithTarget:self selector:@selector(beginParseOfXML) object:nil] autorelease];
        [thread start];
        
    } else {
        
		[self endDownload:YES];
        
	}
	
	DLog(@"Data received...");
	DLog(@"%@", [[[NSString alloc] initWithData:data_ encoding:NSISOLatin1StringEncoding] autorelease]);

    [downloadClient_ dataDownload:downloadId_
                 finishedWithData:data_];
}

/**
 * Cancels the download. Download listener is notified
 */
- (void)cancel {
    
    [connection_ cancel];
    [downloadListener_ cancelledDownload:downloadId_];
    
}

/*
 * Download has finished in error. Download listener is notified
 */
- (void)downloadFinishedWithError:(NSError *)anError {

    [downloadListener_ download:downloadId_ error:anError xmlString:nil];
    [downloadClient_ dataDownload:downloadId_
                finishedWithError:anError
                   downloadedData:data_];
	DLog(@"Data received...");
	DLog(@"%@", [[[NSString alloc] initWithData:data_ encoding:NSISOLatin1StringEncoding] autorelease]);
    DLog(@"Error in connection: %@", [anError description]);

}

/*
 * Download has finished due to a status code different than OK_STATUS (200). Download listener is notified
 */
- (void)downloadFinishedWithStatusCode:(NSInteger)aStatusCode {

    [downloadListener_ download:downloadId_ finishedWithStatusCode:aStatusCode];
    [downloadClient_ dataDownload:downloadId_ finishedWithStatusCode:aStatusCode];

}

#pragma mark -
#pragma mark Parsing on background thread

/*
 * Parse the content of the received XML on another thread (entry point)
 */
- (void)beginParseOfXML {
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
#if defined(SIMULATE_HTTP_CONNECTION)
    [NSThread sleepForTimeInterval:SIMULATED_HTTP_CONNECTION_DELAY];
#endif
    
    
    
    BOOL exceptError = NO;
    NSString *docString = [[[NSString alloc] initWithData:data_
                                                 encoding:NSISOLatin1StringEncoding] autorelease];
    if(docString && [docString hasPrefix:@"response="]){
        
        NSString * response = [docString substringFromIndex:9];
        
        NSString *xml = [NSString stringWithFormat:
                         @"<?xml version='1.0' encoding='iso-8859-1'?>"
                         "<MSG-S>"
                         "<RESPONSE>"
                         "%@"
                         "</RESPONSE>"
                         "</MSG-S>",
                         response];

        //DLog(@"xml modified : %@", xml);
        
        data_ = [[xml dataUsingEncoding:NSUTF8StringEncoding] mutableCopy];
        exceptError = YES;
    }
    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data_];
    [parser setDelegate: xmlParserDelegate_];
    [parser setShouldProcessNamespaces: NO];
    [parser setShouldReportNamespacePrefixes: NO];
    [parser setShouldResolveExternalEntities: NO];
    [parser parse];
    
    NSObject *parsingError = [parser parserError];
    
#if defined(DEBUG)
    
    if (parsingError != nil) {
        
        NSString *filePath = [[DirConfiguration downloadDirectory] stringByAppendingPathComponent:downloadId_];
        [data_ writeToFile:filePath atomically:YES];
        
    }
    
#endif
    

    if(exceptError){
        parsingError = nil;
    }

    
    NSDictionary *errorDict = [[[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:
                                                                      ((parsingError == nil) ? [NSNull null] : parsingError), docString, nil]
                                                            forKeys:[NSArray arrayWithObjects:@"kParsingError", @"kXMLString", nil]] autorelease];
    
    [self performSelectorOnMainThread:@selector(endParseOfXML:) withObject:errorDict waitUntilDone:YES];
	
	[parser release];
    
    [pool release];
    
}

/*
 * Ends the parsing of the XML and returns to main thread
 */
- (void)endParseOfXML:(NSDictionary *)errorDict {
    
    BOOL notifyDownloadFinished = YES;
	
    NSObject *parsingError = [errorDict objectForKey:@"kParsingError"];
    NSString *docString = [errorDict objectForKey:@"kXMLString"];
    
    if ([parsingError isKindOfClass:[NSError class]]) {
        
        notifyDownloadFinished = NO;
        [downloadListener_ download:downloadId_ error:(NSError *)parsingError xmlString:docString];
        
    }
    
	[self endDownload:notifyDownloadFinished];
    
}

/*
 * Ends download
 */
- (void)endDownload:(BOOL)notifyDownloadFinished {

	if (notifyDownloadFinished == YES) {
        
		[downloadListener_ endDownload:downloadId_ withSize:[data_ length]];

	}
	
}

@end


#pragma mark -

/**
 * HTTPInvoker private extension
 */
@interface HTTPInvoker()

/**
 * Notifies a download has ended
 *
 * @private
 */
- (void)notifyDownloadStopped;

/**
 * Notifies a download has started
 *
 * @private
 */
- (void)notifyDownloadStarted;

/**
 * Creates a new data download identification
 *
 * @return New data download identification
 * @private
 */
- (NSString *)prepareNewDataDownloadId;

/**
 * Creates a new empty file to store downloading data, and returns its name (including directory)
 *
 * @return Name of new file
 */
- (NSString *)prepareNewFileForStoringData;

/**
 * Invokes a network POST operation to the given URL
 *
 * @param aURLString The URL string to invoke the operation
 * @param aContentType The body data content type
 * @param aBodyData The body data of the request
 * @param anXMLParserDelegate The NXSMLParser delegate to parse the information received in case it is an XML document. The information is parsed before notifying the delegate
 * @param aDownloadListener The download listener to notify when download events arise
 * @param clearCookies flag to require to clear cookies before invoking
 * @return A download identification
 */
- (NSString *)invokePOSTOperationToURL:(NSString *)aURLString 
					   withContentType:(NSString *)aContentType 
							  withBody:(NSData *)aBodyData
				  andXMLParserDelegate:(id)anXMLParserDelegate 
						   forListener:(id<DownloadListener>)aDownloadListener
					   clearingCookies:(BOOL)clearCookies;

/**
 * Invokes a network operation to the given URL
 *
 * @param anURLRequest The request
 * @param anXMLParserDelegate The NXSMLParser delegate to parse the information received in case it is an XML document. The information is parsed before notifying the delegate
 * @param aDownloadListener The download listener to notify when download events arise
 * @return A download identification
 */
- (NSString *)invokeOperationToURLRequest:(NSMutableURLRequest *)anURLRequest 
					withXMLParserDelegate:(id)anXMLParserDelegate 
							  forListener:(id<DownloadListener>)aDownloadListener;

/**
 * Invokes a network GET or POST operation to the given URL sending a body content
 *
 * @param aURL URL where data can be found
 * @param isGETFlag YES when the operation invoked must be a GET, NO to invoke a POST operation
 * @param aBodyData The request body data
 * @param aContentType The content type sent via the request body
 * @param aHeaderFieldsDictionary The header fields to insert into the HTTP protocol headers. Both keys and objects must be NSString instances
 * @param aDownloadClient The download client to notify when download events arise. HTTPInvoker client must maintain this instance alive while the download is in process
 * @return YES when download can be invoked, NO otherwise
 * @private
 */
- (BOOL)obtainRemoteDataFromURL:(NSURL *)aURL
                 isGETOperation:(BOOL)isGETFlag
                       withBody:(NSData *)aBodyData
                    contentType:(NSString *)aContentType
               httpHeaderFields:(NSDictionary *)aHeaderFieldsDictionary
                 downloadClient:(id<DataDownloadClient>)aDownloadClient;
@end


#pragma mark -

@implementation HTTPInvoker

#pragma mark -
#pragma mark Properties

@dynamic numberOfActiveDownloads;

#pragma mark -
#pragma mark Static attributes

/**
 * Singleton only instance
 */
static HTTPInvoker *httpInvokerInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([HTTPInvoker class]) {
        
		if (httpInvokerInstance_ == nil) {
            
			httpInvokerInstance_ = [super allocWithZone:zone];
			return httpInvokerInstance_;
            
		}
        
	}
	
	return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (HTTPInvoker *)getInstance {
    
	if (httpInvokerInstance_ == nil) {
        
		@synchronized ([HTTPInvoker class]) {
            
			if (httpInvokerInstance_ == nil) {
                
				httpInvokerInstance_ = [[HTTPInvoker alloc] init];
                
			}
            
		}
        
	}
	
	return httpInvokerInstance_;
    
}

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates memory used
 */
- (void)dealloc {
    
	[activeDownloads_ release];
	activeDownloads_ = nil;
	
    [reservedIdentifiers_ release];
    reservedIdentifiers_ = nil;
    
	httpInvokerInstance_ = nil;
	
	[super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initialized. Initializes the singleton instance
 */
- (id)init {
    
	static BOOL initialized = NO;
	
	if ((initialized == NO) && (self = [super init])) {
        
		activeDownloads_ = [[NSMutableDictionary alloc] init];
        reservedIdentifiers_ = [[NSMutableArray alloc] init];
		initialized = YES;
        
	}
	
	return self;
    
}

#pragma mark -
#pragma mark HTTP operations

/*
 * Invokes a network GET or POST operation to the given URL sending a body content
 */
- (BOOL)obtainRemoteDataFromURL:(NSURL *)aURL
                 isGETOperation:(BOOL)isGETFlag
                       withBody:(NSData *)aBodyData
                    contentType:(NSString *)aContentType
               httpHeaderFields:(NSDictionary *)aHeaderFieldsDictionary
                 downloadClient:(id<DataDownloadClient>)aDownloadClient {
    
    BOOL result = NO;
    
    DLog(@"Remote URL: %@", [aURL absoluteString]);
    
	if (aURL != nil) {
        
        NSString *dataDownloadId = nil;
        
		NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:aURL
                                                                  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                              timeoutInterval:TIME_OUT_INTERVAL];
		
		NSString *httpMethod = @"GET";
        
        if (!isGETFlag) {
            
            httpMethod = @"POST";
            
        }
        
        if ([aBodyData length] > 0) {
            
            [urlRequest setValue: CACHE_CONTROL_NO_CACHE_VALUE forHTTPHeaderField: CACHE_CONTROL_HEADER_FIELD];
            [urlRequest setValue:aContentType forHTTPHeaderField:kContentTypeHeaderFieldInvoker];
            [urlRequest setHTTPBody:aBodyData];
            DLog(@"Body:\n%@", [[[NSString alloc] initWithData:aBodyData
                                                      encoding:NSUTF8StringEncoding] autorelease]);
            //DLog(@"Body size: %d", [aBodyData length]);
            
        }
        
        for (NSString *headerField in [aHeaderFieldsDictionary allKeys]) {
            
            NSString *value = [aHeaderFieldsDictionary objectForKey:headerField];
            [urlRequest setValue:value forHTTPHeaderField:headerField];
            
        }
		
		[urlRequest setHTTPMethod:httpMethod];
        
		dataDownloadId = [self prepareNewDataDownloadId];
        
        if ([dataDownloadId length] > 0) {
            
            [aDownloadClient setDownloadId:dataDownloadId];
            
            NSURLConnection *urlConnection = [NSURLConnection connectionWithRequest:urlRequest
                                                                           delegate:self];

            if (urlConnection != nil) {
                
                DownloadInformation *downloadInfo = [[[DownloadInformation alloc] initWithDownloadId:dataDownloadId
                                                                                           remoteURL:aURL
                                                                                          connection:urlConnection
                                                                                      downloadClient:aDownloadClient] autorelease];
                
                if (downloadInfo != nil) {
                    
                    [activeDownloads_ setObject:downloadInfo
                                         forKey:downloadInfo.connectionAddress];
                    
                    if ([(NSObject *)aDownloadClient respondsToSelector:@selector(dataDownloadStarted:)]) {
                        
                        [aDownloadClient dataDownloadStarted:dataDownloadId];
                        
                    }
                    
                    result = YES;
                    
                    [self notifyDownloadStarted];
                    
                } else {
                    
                    [urlConnection cancel];
                    
                }
                
            }
            
            [reservedIdentifiers_ removeObject:dataDownloadId];
            
        }
        
	}
    
    return result;
    
}

#pragma mark -
#pragma mark NSURLConnection delegate selectors

/**
 * Sent when a connection has finished loading successfully. Associated DownloadInformation instance is notified
 * that information was correctly download and is removed from the pending downloads list
 *
 * @param connection The connection that finished successfully
 */
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
	DownloadInformation *downloadInfo = [activeDownloads_ objectForKey:[NSNumber numberWithInteger:(NSInteger)connection]];
	
	if (downloadInfo != nil) {
        
		[downloadInfo downloadFinished];
		[activeDownloads_ removeObjectForKey:downloadInfo.connectionAddress];
        [self notifyDownloadStopped];

	}
    
}

/**
 * Sent when a connection fails to load its request successfully. Associated DownloadInformation instance is
 * notified that the connection failed and is removed from the pending downloads list
 *
 * @param connection The connection that failled to load
 * @param error The error information
 */
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
	DownloadInformation *downloadInfo = [activeDownloads_ objectForKey:[NSNumber numberWithInteger:(NSInteger)connection]];
	
	if (downloadInfo != nil) {
        
		[downloadInfo downloadFinishedWithError:error];
        //DLog(@"Data received...");
        //DLog(@"%@", [[[NSString alloc] initWithData:downloadInfo.data encoding:NSISOLatin1StringEncoding] autorelease]);
		[activeDownloads_ removeObjectForKey:downloadInfo.connectionAddress];
        
	}
    
    //DLog(@"Error in connection: %@", [error description]);
    
}

/**
 * Sent as a connection loads data incrementally. Information is stored
 * in the associated DownloadInformation instance
 *
 * @param connection The connection that received the data
 * @param data The data received
 */
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
	DownloadInformation *downloadInfo = [activeDownloads_ objectForKey:[NSNumber numberWithInteger:(NSInteger)connection]];
	
	if (downloadInfo != nil) {
        
		[downloadInfo appendData:data];
        
	}
    
}

/**
 * Sent when the connection has received sufficient data to construct the URL response for its request.
 * If status code is not OK or unauthorized, the associated DownloadInformatio instance is notified that the download failed,
 * and is removed from the pending downloads list
 *
 * @param connection The connection that received the URL response
 * @param response The response information received
 */
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
	if ([response isKindOfClass:[NSHTTPURLResponse class]] == YES) {
        
		DownloadInformation *downloadInfo = [activeDownloads_ objectForKey:[NSNumber numberWithInteger:(NSInteger)connection]];
		
		NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
		
		NSInteger statusCode = httpResponse.statusCode;
		
		if (statusCode != OK_STATUS_CODE) {

			//DLog(@"Status Code = %i", statusCode);

            [downloadInfo downloadFinishedWithStatusCode:statusCode];
			[activeDownloads_ removeObjectForKey:downloadInfo.connectionAddress];
            [self notifyDownloadStopped];

            
		} else {
            
			[downloadInfo downloadStarts];
            
		}
        
	}
    
}

#if defined(ALLOW_UNTRUSTED_HTTPS_CERTIFICATES)
/**
 * Sent when a connection must authenticate a challenge in order to download its request.
 *
 * @param connection The connection sending the message
 * @param challenge The challenge that connection must authenticate in order to download its request
 */
- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
		[challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
	}
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

/**
 * Sent when a connection ask for authentication on protection spaces
 *
 * @param connection The connection sending the message
 * @param protectionSpace The protection space
 */
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}
#endif

#pragma mark -
#pragma mark Public interface

/*
 * Cancels the given download process and notifies the client. The associated
 * DownloadInformation instance is removed from the pending downloads list
 */
- (void)cancelDownload:(NSString *)aDownloadId {
    
	if ([aDownloadId length] > 0) {
		NSArray *pendingDownload = [activeDownloads_ allValues];
		
		NSNumber *foundKey = nil;
		DownloadInformation *downloadInformation = nil;
		
		for (downloadInformation in pendingDownload) {
            
			if ([downloadInformation.downloadId isEqualToString:aDownloadId] == YES) {
                
				foundKey = downloadInformation.connectionAddress;
				break;
                
			}
            
		}
		
		if (foundKey != nil) {
    
            [[downloadInformation retain] autorelease];
			[activeDownloads_ removeObjectForKey:foundKey];
			[downloadInformation cancel];
            
		}
        
	}
    
}

/*
 * Cancels all downloads. All the DownloadInformation instances are removed from the pending
 * downlodas list
 */
- (void)cancelAllDownloads {
    
	NSArray *keyArray = [activeDownloads_ allKeys];
	
	for (NSNumber *key in keyArray) {
        
		DownloadInformation *downloadInformation = [activeDownloads_ objectForKey:key];
		[[downloadInformation retain] autorelease];
		[activeDownloads_ removeObjectForKey:key];
		[downloadInformation cancel];
        
	}
    
}

/*
 * Invokes a network POST operation to the given URL clearing cookies
 */
- (NSString *)invokePOSTOperationClearingCookiedWithParametersToURL:(NSString *)aURLString 
														   withBody:(NSData *)aBodyData
											   andXMLParserDelegate:(id)anXMLParserDelegate 
														forListener:(id<DownloadListener>)aDownloadListener {
	
	return [self invokePOSTOperationToURL:aURLString 
						  withContentType:PARAMETERS_CONTENT_TYPE 
								 withBody:aBodyData 
					 andXMLParserDelegate:anXMLParserDelegate 
							  forListener:aDownloadListener
						  clearingCookies:YES];
	
}

/*
 * Invokes a network POST operation to the given URL
 */
- (NSString *)invokePOSTOperationWithParametersToURL:(NSString *)aURLString 
											withBody:(NSData *)aBodyData
								andXMLParserDelegate:(id)anXMLParserDelegate 
										 forListener:(id<DownloadListener>)aDownloadListener {
	
	return [self invokePOSTOperationToURL:aURLString 
						  withContentType:PARAMETERS_CONTENT_TYPE 
								 withBody:aBodyData 
					 andXMLParserDelegate:anXMLParserDelegate 
							  forListener:aDownloadListener
						  clearingCookies:NO];
	
}

/*
 * Invokes a network POST operation to the given URL
 */
- (NSString *)invokePOSTOperationWithXMLToURL:(NSString *)aURLString 
									 withBody:(NSData *)aBodyData
						 andXMLParserDelegate:(id)anXMLParserDelegate 
								  forListener:(id<DownloadListener>)aDownloadListener {
	
	return [self invokePOSTOperationToURL:aURLString 
						  withContentType:XML_CONTENT_TYPE 
								 withBody:aBodyData 
					 andXMLParserDelegate:anXMLParserDelegate 
							  forListener:aDownloadListener
						  clearingCookies:NO];
	
}


/*
 * Invokes a network POST operation to the given URL
 */
- (NSString *)invokePOSTOperationToURL:(NSString *)aURLString 
					   withContentType:(NSString *)aContentType 
							  withBody:(NSData *)aBodyData
				  andXMLParserDelegate:(id)anXMLParserDelegate 
						   forListener:(id<DownloadListener>)aDownloadListener
					   clearingCookies:(BOOL)clearCookies {
	
	if ((aURLString != nil) && (aBodyData != nil)) {
		
		DLog(@"POST operation URL = %@", aURLString);
		//DLog(@"Content-Type  = %@", aContentType);
		DLog(@"Body Contents = %@", [[[NSString alloc] initWithData:aBodyData encoding:NSUTF8StringEncoding] autorelease]);
		
		NSURL *url = [NSURL URLWithString: aURLString];

		if (clearCookies) {
			//DLog(@"Clearing cookies before invocation");
			NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
			NSArray *cookies = [storage cookiesForURL:url];
			for (NSHTTPCookie *cookie in cookies) {
				//DLog(@"Clearing cookie = %@", [cookie name]);
				[storage deleteCookie:cookie];
			}
		}
		
		NSMutableURLRequest* URLRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:TIME_OUT_INTERVAL];
		 
        [URLRequest setValue: CACHE_CONTROL_NO_CACHE_VALUE forHTTPHeaderField: CACHE_CONTROL_HEADER_FIELD];
		[URLRequest setValue: aContentType forHTTPHeaderField: CONTENT_TYPE_HEADER_FIELD];
		[URLRequest setHTTPBody:aBodyData];
		
		[URLRequest setHTTPMethod: @"POST"];
		
		return [self invokeOperationToURLRequest:URLRequest withXMLParserDelegate:anXMLParserDelegate forListener:aDownloadListener];
		
	} else {
		
		return nil;
		
	}
	
}

/*
 * Invokes a network operation to the given request
 */
- (NSString *)invokeOperationToURLRequest:(NSMutableURLRequest *)anURLRequest withXMLParserDelegate:(id)anXMLParserDelegate forListener:(id<DownloadListener>)aDownloadListener {
    
	NSString *result = nil;
	
	if ((anURLRequest != nil) && (aDownloadListener != nil)) {
        
		result = [self prepareNewFileForStoringData];
        NSString *reservedIdentifier = result;
		
		if ([result length] > 0) {
            
			@try {
                
                [anURLRequest setTimeoutInterval:HTTP_REQUEST_TIMEOUT];
				NSURLConnection *URLConnection = [NSURLConnection connectionWithRequest:anURLRequest delegate:self];
                
				DownloadInformation *downloadInfo = [[[DownloadInformation alloc] initWithDownloadId:result remoteURL:[anURLRequest URL] connection:URLConnection
                                                                                   xmlParserDelegate:anXMLParserDelegate andDownloadListener:aDownloadListener] autorelease];
                
				if (downloadInfo != nil) {
                    
					[activeDownloads_ setObject:downloadInfo forKey:downloadInfo.connectionAddress];
                    
				}
                
			}
			@catch (id exception) {
                
				result = nil;
                
			}
            @finally {
                
                [reservedIdentifiers_ removeObject:reservedIdentifier];
                
            }
            
		}
        
	}
	
	return result;
    
}

/*
 * Invokes a network POST operation to the given URL sending a body content. The download identification is provided via de DataDownloadClient protocol
 */
- (BOOL)invokePOSTOperationToURL:(NSURL *)aURL 
                        withBody:(NSData *)aBodyData
                     contentType:(NSString *)aContentType
                httpHeaderFields:(NSDictionary *)aHeaderFieldsDictionary
                  downloadClient:(id<DataDownloadClient>)aDownloadClient {
    
    return [self obtainRemoteDataFromURL:aURL
                          isGETOperation:NO
                                withBody:aBodyData
                             contentType:aContentType
                        httpHeaderFields:aHeaderFieldsDictionary
                          downloadClient:aDownloadClient];
	
}

/*
 * Invokes a network GET operation to the given URL sending a body content. The download identification is provided via de DataDownloadClient protocol
 */
- (BOOL)invokeGETOperationToURL:(NSURL *)aURL 
                       withBody:(NSData *)aBodyData
                    contentType:(NSString *)aContentType
               httpHeaderFields:(NSDictionary *)aHeaderFieldsDictionary
                 downloadClient:(id<DataDownloadClient>)aDownloadClient {
    
    return [self obtainRemoteDataFromURL:aURL
                          isGETOperation:YES
                                withBody:aBodyData
                             contentType:aContentType
                        httpHeaderFields:aHeaderFieldsDictionary
                          downloadClient:aDownloadClient];
	
}

/*
 * Removes cache.db from register
 */
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}

#pragma mark -
#pragma mark Getters and setters

- (NSInteger)numberOfActiveDownloads {
    
	return [activeDownloads_ count];
    
}

#pragma mark -
#pragma mark Auxiliary selectors

/*
 * Creates a new empty file to store downloading data, and returns its name (including directory)
 */
- (NSString *)prepareNewFileForStoringData {
    
	NSString *result = nil;
	NSArray *keysArray = [activeDownloads_ allKeys];
    
    do {

        result = [NSString stringWithFormat:@"%u", arc4random()];

		if (([result length] > 0) && (![keysArray containsObject:result]) && (![reservedIdentifiers_ containsObject:result])) {
            
            [reservedIdentifiers_ addObject:result];
            
        } else {
            
            result = nil;
        }
        
    } while (result == nil);
	
	return result;
    
}

#pragma mark -
#pragma mark Auxiliary selectors

/*
 * Creates a new data download identification
 */
- (NSString *)prepareNewDataDownloadId {
    
	NSString *result = nil;
	NSArray *keysArray = [activeDownloads_ allKeys];
    
    do {
        
        result = [NSString stringWithFormat:@"%u", arc4random()];
        
		if (([result length] > 0) && (![keysArray containsObject:result]) && (![reservedIdentifiers_ containsObject:result])) {
            
            [reservedIdentifiers_ addObject:result];
            
        } else {
            
            result = nil;
        }
        
    } while (result == nil);
	
	return result;
    
}

#pragma mark -
#pragma mark Notifications

/*
 * Notifies a download has started
 */
- (void)notifyDownloadStarted {
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:kDownloadStartNotificationName
                                      object:self];
    
}

/*
 * Notifies a download has ended
 */
- (void)notifyDownloadStopped {
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:kDownloadStopNotificationName
                                      object:self];
    
}

@end


