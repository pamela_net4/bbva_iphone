/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "SingletonBase.h"

//Forward declarations
@protocol DownloadListener;


//Forward declarations
@protocol DataDownloadClient;

/**
 * Sends operation using HTTP protocol
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface HTTPInvoker : SingletonBase {
    
@private
    
	/**
	 * Dictionary of active connections
	 */
	NSMutableDictionary *activeDownloads_;
    
    /**
     * Reserved download identifiers (still not in use)
     */
    NSMutableArray *reservedIdentifiers_;
    
}

/**
 * Returns the number of active downloads
 */
@property (nonatomic, readonly) NSInteger numberOfActiveDownloads;

/**
 * Returns the singleton only instance
 *
 * @result The singleton only instance
 */
+ (HTTPInvoker *)getInstance;

/**
 * Invokes a network POST operation to the given URL sending an XML content
 *
 * @param aURLString The URL string to invoke the operation
 * @param aBodyData The body data of the request
 * @param anXMLParserDelegate The NXSMLParser delegate to parse the information received in case it is an XML document. The information is parsed before notifying the delegate
 * @param aDownloadListener The download listener to notify when download events arise
 * @return A download identifier
 */
- (NSString *)invokePOSTOperationWithParametersToURL:(NSString *)aURLString 
											withBody:(NSData *)aBodyData
                                andXMLParserDelegate:(id)anXMLParserDelegate 
										 forListener:(id<DownloadListener>)aDownloadListener;

/**
 * Invokes a network POST operation to the given URL sending an XML content clearing cookies before invocation
 *
 * @param aURLString The URL string to invoke the operation
 * @param aBodyData The body data of the request
 * @param anXMLParserDelegate The NXSMLParser delegate to parse the information received in case it is an XML document. The information is parsed before notifying the delegate
 * @param aDownloadListener The download listener to notify when download events arise
 * @return A download identifier
 */
- (NSString *)invokePOSTOperationClearingCookiedWithParametersToURL:(NSString *)aURLString 
														   withBody:(NSData *)aBodyData
											   andXMLParserDelegate:(id)anXMLParserDelegate 
														forListener:(id<DownloadListener>)aDownloadListener;
	
/**
 * Invokes a network GET operation to the given URL sending a body content. The download identification is provided via de DataDownloadClient protocol
 *
 * @param aURL The URL to invoke the operation
 * @param aBodyData The request body data
 * @param aContentType The content type sent via the request body
 * @param aHeaderFieldsDictionary The header fields to insert into the HTTP protocol headers. Both keys and objects must be NSString instances
 * @param aDownloadClient The download client to notify when download events arise. HTTPInvoker client must maintain this instance alive while the download is in process
 * @return YES when download can be invoked, NO otherwise
 */
- (BOOL)invokeGETOperationToURL:(NSURL *)aURL 
                       withBody:(NSData *)aBodyData
                    contentType:(NSString *)aContentType
               httpHeaderFields:(NSDictionary *)aHeaderFieldsDictionary
                 downloadClient:(id<DataDownloadClient>)aDownloadClient;

/**
 * Invokes a network POST operation to the given URL sending a body content. The download identification is provided via de DataDownloadClient protocol
 *
 * @param aURL The URL to invoke the operation
 * @param aBodyData The request body data
 * @param aContentType The content type sent via the request body
 * @param aHeaderFieldsDictionary The header fields to insert into the HTTP protocol headers. Both keys and objects must be NSString instances
 * @param aDownloadClient The download client to notify when download events arise. HTTPInvoker client must maintain this instance alive while the download is in process
 * @return YES when download can be invoked, NO otherwise
 */
- (BOOL)invokePOSTOperationToURL:(NSURL *)aURL 
                        withBody:(NSData *)aBodyData
                     contentType:(NSString *)aContentType
                httpHeaderFields:(NSDictionary *)aHeaderFieldsDictionary
                  downloadClient:(id<DataDownloadClient>)aDownloadClient;

/**
 * Cancels the given download process and notifies the client. The associated
 * DownloadInformation instance is removed from the pending downloads list
 *
 *
 * @param aDownloadId The download identification
 */
- (void)cancelDownload:(NSString *)aDownloadId;

/**
 * Cancels all downloads. All the DownloadInformation instances are removed from the pending
 * downlodas list
 */
- (void)cancelAllDownloads;

@end
