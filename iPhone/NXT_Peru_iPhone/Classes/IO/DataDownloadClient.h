/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */



/**
 * Protocol implemented by classes that must receive download notifications. Every download
 * needs a DataDownloadClient implemented in order to manage the information
 * downloaded and respond to errors during download
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol DataDownloadClient

@required

/**
 * Sets the download identification
 *
 * @param aDownloadId The download identification to set
 */
- (void)setDownloadId:(NSString *)aDownloadId;

/**
 * Notifies the client that the download has finished. The data downloaded from the server is provided
 * to analyze it
 *
 * @param aDownloadId The string that identifies the download process
 * @param aDownloadedData The data downloaded from the server
 */
- (void)dataDownload:(NSString *)aDownloadId finishedWithData:(NSData *)aDownloadedData;

/**
 * Notifies the client that an error appeared during data download. Data downloaded and error are provided
 * to the client to analyze them
 *
 * @param aDownloadId The string that identifies the download process
 * @param anError The error that appeared during the data download
 * @param aDownloadedData The data downloaded until the error appeared
 */
- (void)dataDownload:(NSString *)aDownloadId finishedWithError:(NSError *)anError downloadedData:(NSData *)aDownloadedData;

/**
 * Notifies the download has finished with a status code different than OK_STATUS (200)
 *
 * @param aDownloadId The string that identifies the download process
 * @param aStatusCode The status code that finished the download
 */
- (void)dataDownload:(NSString *)aDownloadId finishedWithStatusCode:(NSInteger)aStatusCode;

/**
 * Notifies the client that the data download was cancelled
 *
 * @param aDownloadId The string that identifies the download process
 */
- (void)dataDownloadCancelled:(NSString *)aDownloadId;

@optional

/**
 * Notifies the client that a download has started
 *
 * @param aDownloadId The string that identifies the download process
 */
- (void)dataDownloadStarted:(NSString *)aDownloadId;

@end
