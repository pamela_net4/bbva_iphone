/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "DataDownloadClient.h"


/**
 * Base class implementing the DataDownloadCient protocol. It just defines an attribute
 * to store the data download identification and manages it
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BaseDataDownloadClient : NSObject <DataDownloadClient> {

@private
    
    /**
     * Download identification
     */
    NSString *downloadId_;
    
}

/**
 * Provides read-write access to the data download identification
 */
@property (nonatomic, readwrite, copy) NSString *downloadId;


/**
 * Cancels the current data download
 */
- (void)cancelDownload;

@end
