/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "XMLParserDataDownloadClient.h"
#import "XMLParserDataDownloadClient+protected.h"
#import "BaseXMLParserObject.h"


/**
 * Defines the XML error parameter key inside a parameter dictionary
 */
#define XML_ERROR_PARAMETER_KEY                             @"xmlErrorParameterKey"

/**
 * Defines the downloaded data parameter key inside a parameter dictionary
 */
#define DOWNLOADED_DATA_PARAMETER_KEY                       @"downloadedDataParameterKey"

/**
 * Defines the XML parser object key inside a parameter dictionary
 */
#define XML_PARSER_OBJECT_PARAMETER_KEY                     @"xmlParserObjectParameterKey"


/*
 * Defines the XMLParserDataDownloadClient error domain
 */
NSString * const XMLParserDataDownloadClientErrorDomain = @"kIOXMLParserDataDownloadClientErrorDomain";


#pragma mark -

/**
 * XMLParserDataDownloadClient private category
 */
@interface XMLParserDataDownloadClient(private)

/**
 * Parses the provided data as an XML document. The information is analyzed by
 * the BaseXMLParserObject provided by child clases
 *
 * @param aDownloadedData The downloaded data to analyze. It must never be nil
 * @private
 */
- (void)analyzeXMLDocumentFromData:(NSData *)aDownloadedData;

/**
 * The XML parsing was successful. This selector is invoked in the application main thread from the parsing thread.
 * Child class is notified
 *
 * @param aParameters Dictionary containing the parameters. The XML_PARSER_OBJECT_PARAMETER_KEY key provides
 * access to the XML parser object, while the DOWNLOADED_DATA_PARAMETER_KEY key provides access to
 * the downloaded data parsed
 * @private
 */
- (void)xmlParsingFinishedWithParameters:(NSDictionary *)aParameters;

/**
 * The XML parsing has failed. This selector is invoked in the application main thread from the parsing thread.
 * Child class is notified
 *
 * @param aParameters Dictionary containing the parameters. The XML_ERROR_PARAMETER_KEY key provides
 * access to the XML parser error, while the DOWNLOADED_DATA_PARAMETER_KEY key provides access to
 * the downloaded data that failed when parsing
 * @private
 */
- (void)xmlParsingFailedWithParameters:(NSDictionary *)aParameters;

@end


#pragma mark -

@implementation XMLParserDataDownloadClient

#pragma mark -
#pragma mark DataDownloadClient protocol selectors

/**
 * Notifies the client that the download has finished. The data downloaded from the server is provided
 * to analyze it. Analyzes the downloaded data as an XML document in a separate thread
 *
 * @param aDownloadId The string that identifies the download process
 * @param aDownloadedData The data downloaded from the server
 */
- (void)dataDownload:(NSString *)aDownloadId finishedWithData:(NSData *)aDownloadedData {
    
    [super dataDownload:aDownloadId finishedWithData:aDownloadedData];
    
    if ([aDownloadId isEqualToString:self.downloadId]) {
        
        [NSThread detachNewThreadSelector:@selector(analyzeXMLDocumentFromData:)
                                 toTarget:self
                               withObject:aDownloadedData];
        
    }
    
}

@end


#pragma mark -

@implementation XMLParserDataDownloadClient(protected)

#pragma mark -
#pragma mark Notifying XML parsing result

/*
 * Notifies the child class that the XML parsing finished correctly. Default implementation
 * performs no action
 */
- (void)xmlParsingFinishedWithResult:(BaseXMLParserObject *)anXMLParserObject
                            fromData:(NSData *)aDownloadedData {
    
}

/*
 * Notifies the child class that the XML parsing was not possible due to an error. Default implementation
 * performs no action
 */
- (void)xmlParsingFinishedWithError:(NSError *)anXMLError
                           fromData:(NSData *)aDownloadedData {
    
}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the BaseXMLParserObject that must analyze the downloaded data.
 * Base implementation returns a nil instance
 *
 * @return The BaseXMLParserObject that must analyze the downloaded data
 */
- (BaseXMLParserObject *)parserObject {
    
    return nil;
    
}

@end


#pragma mark -

@implementation XMLParserDataDownloadClient(private)

#pragma mark -
#pragma mark Data parsing

/*
 * Parses the provided data as an XML document. The information is analyzed by
 * the BaseXMLParserObject provided by child clases
 */
- (void)analyzeXMLDocumentFromData:(NSData *)aDownloadedData {
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

    BaseXMLParserObject *parserObject = self.parserObject;
    
    if (parserObject != nil) {
        
        NSString *asdf = [[[NSString alloc] initWithData:aDownloadedData encoding:NSUTF8StringEncoding] autorelease];
        
        //NSLog(@"%@", asdf);
        
        NSXMLParser *parser = [[[NSXMLParser alloc] initWithData:aDownloadedData] autorelease];
        [parser setDelegate:parserObject];
        [parser setShouldProcessNamespaces:NO];
        [parser setShouldReportNamespacePrefixes:NO];
        [parser setShouldResolveExternalEntities:NO];
        [parser parse];
        
        NSError *parsingError = [parser parserError];
        
        if (parsingError != nil) {
            
            NSMutableDictionary *parametersDictionary = [[[NSMutableDictionary alloc] init] autorelease];
            [parametersDictionary setObject:parsingError
                                     forKey:XML_ERROR_PARAMETER_KEY];
            [parametersDictionary setObject:aDownloadedData
                                     forKey:DOWNLOADED_DATA_PARAMETER_KEY];
            [self performSelectorOnMainThread:@selector(xmlParsingFailedWithParameters:)
                                   withObject:parametersDictionary
                                waitUntilDone:YES];
            
        } else {
            
            NSMutableDictionary *parametersDictionary = [[[NSMutableDictionary alloc] init] autorelease];
            [parametersDictionary setObject:parserObject
                                     forKey:XML_PARSER_OBJECT_PARAMETER_KEY];
            [parametersDictionary setObject:aDownloadedData
                                     forKey:DOWNLOADED_DATA_PARAMETER_KEY];
            [self performSelectorOnMainThread:@selector(xmlParsingFinishedWithParameters:)
                                   withObject:parametersDictionary
                                waitUntilDone:YES];
            
        }
        
    } else {
        
        NSError *error = [NSError errorWithDomain:XMLParserDataDownloadClientErrorDomain
                                             code:XMLParserDataDownloadClientNoParserObjectError
                                         userInfo:nil];
        
        NSMutableDictionary *parametersDictionary = [[[NSMutableDictionary alloc] init] autorelease];
        [parametersDictionary setObject:error
                                 forKey:XML_ERROR_PARAMETER_KEY];
        [parametersDictionary setObject:aDownloadedData
                                 forKey:DOWNLOADED_DATA_PARAMETER_KEY];
        [self performSelectorOnMainThread:@selector(xmlParsingFailedWithParameters:)
                               withObject:parametersDictionary
                            waitUntilDone:YES];
        
    }
    
    [pool release];
    
}

/*
 * The XML parsing was successful. This selector is invoked in the application main thread from the parsing thread.
 * Child class is notified
 */
- (void)xmlParsingFinishedWithParameters:(NSDictionary *)aParameters {
    
    BaseXMLParserObject *parserObject = [aParameters objectForKey:XML_PARSER_OBJECT_PARAMETER_KEY];
    NSData *downloadedData = [aParameters objectForKey:DOWNLOADED_DATA_PARAMETER_KEY];

    [self xmlParsingFinishedWithResult:parserObject
                              fromData:downloadedData];
    
}

/*
 * The XML parsing has failed. This selector is invoked in the application main thread from the parsing thread.
 * Child class is notified
 */
- (void)xmlParsingFailedWithParameters:(NSDictionary *)aParameters {
    
    NSError *error = [aParameters objectForKey:XML_ERROR_PARAMETER_KEY];
    NSData *downloadedData = [aParameters objectForKey:DOWNLOADED_DATA_PARAMETER_KEY];
    
    [self xmlParsingFinishedWithError:error
                             fromData:downloadedData];
    
}

@end
