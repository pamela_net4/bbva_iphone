/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "XMLParserDataDownloadClient.h"


//Forward declarations
@class BaseXMLParserObject;


/**
 * XMLParserDataDownloadClient protected category
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface XMLParserDataDownloadClient(protected)

/**
 * Provides read-only access to the BaseXMLParserObject that must analyze the downloaded data
 */
@property (nonatomic, readonly, retain) BaseXMLParserObject *parserObject;

/**
 * Notifies the child class that the XML parsing finished correctly. Default implementation
 * performs no action
 *
 * @param anXMLParserObject The object containing the parsed XML document
 * @param aDownloadedData The downloaded data that generated the parsed XML document
 * @protected
 */
- (void)xmlParsingFinishedWithResult:(BaseXMLParserObject *)anXMLParserObject
                            fromData:(NSData *)aDownloadedData;

/**
 * Notifies the child class that the XML parsing was not possible due to an error. Default implementation
 * performs no action
 *
 * @param anXMLError The error that happened while parsing
 * @param aDownloadedData The downloaded data that failed to be parsed as an XML document
 * @protected
 */
- (void)xmlParsingFinishedWithError:(NSError *)anXMLError
                           fromData:(NSData *)aDownloadedData;

@end
