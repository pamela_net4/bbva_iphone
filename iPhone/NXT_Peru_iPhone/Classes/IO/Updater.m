/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "Updater.h"

#import "AccountList.h"
#import "AccountTransactionDetailResponse.h"
#import "AccountTransactionsResponse.h"
#import "BankAccount.h"
#import "Card.h"
#import "CardList.h"
#import "CardTransactionsResponse.h"
#import "Carrier.h"
#import "CoordinateResponse.h"
#import "DepositList.h"
#import "FOECashMobileStepOneResponse.h"
#import "FOEInstitutionPaymentStepOneResponse.h"
#import "FOEOtherBankTransferStepOneResponse.h"
#import "FOEPaymentCOtherBankStepOneResponse.h"
#import "FOERechargeCellphoneStepOneResponse.h"
#import "FOERechargeGiftCardStepOneResponse.h"
#import "FOEServicePaymentStepOneResponse.h"
#import "FOEThirdAccountTransferStepOneResponse.h"
#import "FOEThirdCardPaymentStepOneResponse.h"
#import "FOPGeneralMenuStepOneResponse.h"
#import "GeneralLoginResponse.h"
#import "GeneralLoginEncryptResponse.h"
#import "GeneralStatusResponse.h"
#import "GlobalAdditionalInformation.h"
#import "GlobalPositionResponse.h"
#import "HTTPInvoker.h"
#import "LoanList.h"
#import "MATLogNames.h"
#import "MCBFacade.h"
#import "MutualFundList.h"
#import "NSString+URLAndHTMLUtils.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentCCardInitialResponse.h"
#import "PaymentCCOtherBanksInitialResponse.h"
#import "PaymentConfirmationResponse.h"
#import "PaymentDataResponse.h"
#import "PaymentInstitutionsAndCompaniesInitialResponse.h"
#import "PaymentInstitutionAndCompaniesConfirmationInformationResponse.h"
#import "PaymentInstitutionAndCompaniesSuccessConfirmationResponse.h"
#import "PaymentInstitutionsAndCompaniesDetailResponse.h"
#import "PaymentInstitutionAndCompaniesConfirmationResponse.h"
#import "PaymentRechargeInitialResponse.h"
#import "PaymentTINOnlineResponse.h"
#import "PaymentSuccessResponse.h"
#import "PaymentsConstants.h"
#import "PublicServiceResponse.h"
#import "RetentionListResponse.h"
#import "ServiceList.h"
#import "ServiceResponse.h"
#import "Session.h"
#import "StatusEnabledResponse.h"
#import "StockMarketAccountList.h"
#import "StringKeys.h"
#import "Tools.h"
#import "TransferConfirmationResponse.h"
#import "TransferStartupResponse.h"
#import "TransferTypePaymentResponse.h"
#import "TransferSuccessResponse.h"
#import "TransferDetailResponse.h"
#import "TransferShowDetailResponse.h"
#import "GiftCardStepOneResponse.h"
#import "GiftCardStepTwoResponse.h"
#import "GiftCardStepThreeResponse.h"
#import "FORRechargeGiftCardStepOneResponse.h"
#import "AlterGlobalPositionResponse.h"
#import "FORCashMobileStepOneResponse.h"
#import "FOROtherBankTransferStepOneResponse.h"
#import "FORThirdAccountTransferStepOneResponse.h"
#import "FORRechargeGiftCardStepOneResponse.h"
#import "FORPaymentCOtherBankStepOneResponse.h"
#import "FORInstitutionPaymentStepOneResponse.h"
#import "FORRechargeCellphoneStepOneResponse.h"
#import "FORServicePaymentStepOneResponse.h"

#import "FORCashMobileStepTwoResponse.h"
#import "FOROtherBankTransferStepTwoResponse.h"
#import "FORThirdAccountTransferStepTwoResponse.h"
#import "FORRechargeGiftCardStepTwoResponse.h"
#import "FORPaymentCOtherBankStepTwoResponse.h"
#import "FORThirdCardPaymentStepTwoResponse.h"
#import "FORInstitutionPaymentStepTwoResponse.h"
#import "FORRechargeCellphoneStepTwoResponse.h"
#import "FORServicePaymentStepTwoResponse.h"

#import "FORCashMobileStepThreeResponse.h"
#import "FOROtherBankTransferStepThreeResponse.h"
#import "FORThirdAccountTransferStepThreeResponse.h"
#import "FORRechargeGiftCardStepThreeResponse.h"
#import "FORPaymentCOtherBankStepThreeResponse.h"
#import "FORThirdCardPaymentStepThreeResponde.h"
#import "FORInstitutionPaymentStepThreeResponse.h"
#import "FORRechargeCellphoneStepThreeResponse.h"
#import "FORServicePaymentStepThreeResponse.h"

#import "FORThirdCardPaymentStepOneResponse.h"
#import "FORRechargeGiftCardStepTwoResponse.h"
#import "FORRechargeGiftCardStepThreeResponse.h"

#import "SafetyPayStatusResponse.h"
#import "SafetyPayTransactionInfoResponse.h"
#import "SafetyPayDetailsResponse.h"
#import "SafetyPayConfirmationResponse.h"

#import "ThirdAccountListResponse.h"
#import "NicknameValidationResponse.h"
#import "RegistrationThirdAccountConfirmationResponse.h"
#import "RegistrationThirdAccountSuccessResponse.h"

#import "CampaignList.h"
#import "IncrementCreditLineStartUpResponse.h"
#import "IncrementCreditLineConfirmResponse.h"
#import "IncrementCreditLineResultResponse.h"

#import "FastLoanStartup.h"
#import "FastLoanTermList.h"
#import "FastLoanSchedule.h"
#import "FastLoanSimulation.h"
#import "FastLoanConfirm.h"
#import "FastLoanSummary.h"

#import "SalaryAdvanceReceive.h"
#import "SalaryAdvanceSummary.h"
#import "SalaryAdvanceAffiliation.h"

/**
 * Contains operation information for the operation invocation process
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface OperationInformation : NSObject {
    
@private
	
	/**
	 * Local download file that identifies the download
	 */
	NSString* downloadFile_;
	
	/**
	 * The operation type
	 */
	NSInteger operationType_;
	
	/**
	 * The operation result
	 */
	BOOL operationResult_;
    
	/**
	 * If fails do not show message
	 */
	BOOL silent_;
    
	/**
	 * Operation info
	 */
	id operationParams_;
	
	/**
	 * XML parser delegate if any. When not nil, the downloaded information is parsed using this NSXMLParser delegate before notifying the download listener
	 */
	id xmlParserDelegate_;
	
}

/**
 * Provides read only access to the download file where data can be found
 */
@property (nonatomic, readonly, copy) NSString* downloadFile;

/**
 * Provides read only access to the operation type
 */
@property (nonatomic, readonly, assign) NSInteger operationType;

/**
 * Provides read only access to the operation result
 */
@property (nonatomic, readwrite, assign) BOOL operationResult;

/**
 * Provides read only access to the silent flag
 */
@property (nonatomic, readwrite, assign) BOOL silent;

/**
 * Provides read write access to the XML parser delegate
 */
@property (nonatomic, readwrite, retain) id xmlParserDelegate;

/**
 * Provides read write access to the operation parameters
 */
@property (nonatomic, readwrite, retain) id operationParams;

/**
 * Designated instance initializer. Initializes the OperationInformation instance with the provided information
 *
 * @param aDownloadId The download information
 * @param anOperationType Operation type
 * @result Initialized OperationInformation instance
 */
- (id)initWithDownloadId:(NSString*)aDownloadId operationType:(NSInteger)anOperationType;


@end


#pragma mark -

@implementation OperationInformation

@synthesize downloadFile = downloadFile_;
@synthesize operationType = operationType_;
@synthesize operationResult = operationResult_;
@synthesize silent = silent_;
@synthesize operationParams = operationParams_;
@synthesize xmlParserDelegate = xmlParserDelegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates memory used
 */
- (void) dealloc {
	[downloadFile_ release];
	downloadFile_ = nil;
	
	[operationParams_ release];
	operationParams_ = nil;
	
	[xmlParserDelegate_ release];
	xmlParserDelegate_ = nil;
	
	[super dealloc];
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated instance initializer. This initializer returns a nil instance because empty OperationInformation instances are not allowed
 */
- (id) init {
	[self autorelease];
	return nil;
}

/*
 * Designated instance initializer. Initializes the OperationInformation instance with the provided information
 */
- (id)initWithDownloadId:(NSString*)aDownloadId operationType:(NSInteger)anOperationType {
	if ((self = [super init])) {
        [downloadFile_ release];
		downloadFile_ = [aDownloadId copy];
        
		operationType_ = anOperationType;
	}
	return self;
}

@end

#pragma mark -

/**
 * Updater private category
 */
@interface Updater()

/**
 * Process a finished download
 *
 * @param operationInfo The operation info
 * @param andResult The operation result
 */
- (void) processFinishedDownload:(OperationInformation *)operationInfo andResult:(BOOL)success;



@end

/**
 * XML entries for the operations
 */
@interface Updater(XMLEntryGenerators)

/**
 * Generate the entry for login operations
 *
 * @param identification the user identification
 * @param password the password
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceLoginWithId:(NSString *)identification
						                    andPassword:(NSString *)password;


/**
 * Generate the entry for login with coordinate operations
 *
 * @param coordinate the coordinate
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceLoginWithCoordinate:(NSString *)coordinate;

/**
 * Generates the entry of the account transactions
 *
 * @param aType The account type to obtain the transactions list from
 * @param aSubject The account subject to obtain the transactions list from
 * @return The string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceAccountTransactionsWithType:(NSString *)aType
                                                                subject:(NSString *)aSubject;

/*
 * Generates the entry of the account transaction detail
 *
 * @param accountNumber The account number
 * @param transactionNumber The transaction number
 * @return The string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceAccountTransactionsDetailWithAccountNumber:(NSString *)accountNumber
                                                                     transactionNumber:(NSString *)transactionNumber;

/**
 * Generates the entry of the card transactions
 *
 * @param aSubject The card subject identifying it inside the list
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceCardTransactionsWithSubject:(NSString *)aSubject;

/**
 * Generates the entry of transfer confirmation
 *
 * @param fromAccountNumber from account number
 * @param fromAccountType from account type
 * @param fromCurrency from currency
 * @param fromType from type
 * @param fromIndex from index
 * @param toAccountNumber to account number
 * @param toAccountType to account type
 * @param toCurrency to currency
 * @param toType to type
 * @param toIndex to index
 * @param amount the amount
 * @param currency the currency
 * @param subject the subject
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceTransferConfirmationFromAccountNumber:(NSString *)fromAccountNumber
																  fromAccountType:(NSString *)fromAccountType
																	 fromCurrency:(NSString *)fromCurrency
																		 fromType:(NSString *)fromType
																		fromIndex:(NSString *)fromIndex
																  toAccountNumber:(NSString *)toAccountNumber
																	toAccountType:(NSString *)toAccountType
																	   toCurrency:(NSString *)toCurrency
																		   toType:(NSString *)toType
																		  toIndex:(NSString *)toIndex
																		   amount:(NSString *)amount
																		 currency:(NSString *)currency
																	   andSubject:(NSString *)subject;
/**
 * Generates the entry of transfer result
 *
 * @param secondFactor The second factor key
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceTransferResultFromSecondFactorKey:(NSString *)secondFactor;

/**
 * Generates the entry of transfer result to third accounts
 *
 * @param secondFactor The second factor key
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceTransferToThirdAccountsResultFromSecondFactorKey:(NSString *)secondFactor;

/**
 * Generate the entry for transfer With cash mobile confirmation
 *
 * @param fromAccountType From account type
 * @param fromCurrency From currency
 * @param fromIndex From index
 * @param toPhone To phone number
 * @param amount The amount
 * @param currency The currency
 */
- (NSString *)returnEscapedParameterSequenceWithCashMobileConfirmationFromAccountType:(NSString *)fromAccountType
                                                                         fromCurrency:(NSString *)fromCurrency
                                                                            fromIndex:(NSString *)fromIndex
                                                                              toPhone:(NSString *)toPhone
                                                                               amount:(NSString *)amount
                                                                             currency:(NSString *)currency;

/**
 * Generates the entry to obtain cash mobile transaction
 *
 * @param operationCode The code of the operation.
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceObtainCashMobileTransactionResultFromOperationCode:(NSString *)operationCode;

/**
 * Invoke transfer to gift card confirmation
 *
 * @param fromAccountType from account type
 * @param fromNumber from account or card number
 * @param toNumber to account number
 * @param amount the amount
 * @param currency the currency
 * @param toConfirmationPhone the phone to send the confirmation
 * @param toConfirmationMail the mail to send the confirmation
 * @param carrier the carrier for phones
 * @param referenceMessage the referenceMessage
 */
- (NSString *)returnEscapedParameterSequenceToGiftCardConfirmationFromAccountType:(NSString *)fromAccountType
                                                                       fromNumber:(NSString *)fromNumber
                                                                         toNumber:(NSString *)toNumber
                                                                           amount:(NSString *)amount
                                                                         currency:(NSString *)currency
                                                              toConfirmationPhone:(NSString *)toConfirmationPhone
                                                               toConfirmationMail:(NSString *)toConfirmationMail
                                                                          carrier:(NSString *)carrier
                                                                 referenceMessage:(NSString *)referenceMessage;

/**
 * Invoke transfer confirmation
 *
 * @param forAccountNumber the account number
 * @param accountType the account type
 * @param balance the balance
 * @param availableBalance the available balance
 * @param currency the currency
 * @param bank the bank number
 * @param office the office number
 * @param controlDigit the control digit
 * @param account the account number
 * @param type the type
 * @param index the index
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceRetrieveRetainsForAccountNumber:(NSString *)forAccountNumber
																accountType:(NSString *)accountType
																	balance:(NSString *)balance
														   availableBalance:(NSString *)availableBalance
																   currency:(NSString *)currency
																	   bank:(NSString *)bank
																	 office:(NSString *)office
															   controlDigit:(NSString *)controlDigit
																	account:(NSString *)account
																	   type:(NSString *)type
																   andIndex:(NSString *)index;

/**
 * Invoke transaction send
 *
 * @param operation the operation
 * @param email1 the first email
 * @param email2 the second email
 * @param carrier1 the first carrier
 * @param carrier2 the second carrier
 * @param phonenumber1 the first phonenumber
 * @param phonenumber2 the second phonenumber
 * @param comments the user comments
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceSendTransactionForOperation:(NSString *)operation
																 email1:(NSString *)email1
																 email2:(NSString *)email2
															   carrier1:(NSString *)carrier1
															   carrier2:(NSString *)carrier2
														   phonenumber1:(NSString *)phonenumber1
														   phonenumber2:(NSString *)phonenumber2
															andComments:(NSString *)comments;

/*
 * Invoke transaction send confirmation
 *
 * @param email1 the first email
 * @param email2 the second email
 * @param carrier1 the first carrier
 * @param carrier2 the second carrier
 * @param phonenumber1 the first phonenumber
 * @param phonenumber2 the second phonenumber
 * @param comments the user comments
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceSendTransactionConfirmationForEmail:(NSString *)email1
																		 email2:(NSString *)email2
																	   carrier1:(NSString *)carrier1
																	   carrier2:(NSString *)carrier2
																   phonenumber1:(NSString *)phonenumber1
																   phonenumber2:(NSString *)phonenumber2
																	andComments:(NSString *)comments;

/*
 * Invoke search institutions
 *
 * @param entity the entity to look for
 * @param searchType type of the search(group or by text field)
 * @param nextMovement indicates if the search is forward or backward
 * @param indPag the pag index
 * @param searchArg the search argument
 * @param lastDescription last description
 * @param button the button to look forward or backward
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceSearchInstitutionsForEntity:(NSString *)entity
                                                             searchType:(NSString *)searchType
                                                           nextMovement:(NSString *)nextMovement
                                                                 indPag:(NSString *)indPag
                                                              searchArg:(NSString *)searchArg
                                                              LastDescr:(NSString *)lastDescription
                                                                 button:(NSString *)button;

/*
 *  * Generate the entry for Detail institution
 *
 * @param code the entity code of the institution
 * @param operationType type of the operation
 *
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceDetailInstitutionsForEntity:(NSString *)code
                                                             optionType:(NSString *)optionType;

/*
 *  * Invoke Detail pending paysinstitution
 *
 * @param code the code of the person
 * @param arrayLong type of the arrayLong
 * @param array the array with all the codes
 * @param titlesArray the array with the titles
 * @param valData the validation data recieved in the service
 * @param flagModule the flag recieved in the service
 *
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequencePendingPaysInstitutionsForCode:(NSString *)code
                                                                 arrayLong:(NSString *)arrayLong
                                                                     array:(NSString *)array
                                                               titlesArray:(NSString *)titlesArray
                                                            validationData:(NSString *)valData
                                                                flagModule:(NSString *)flagModule;

/*
 *  * Invoke Detail confirmation pay institution
 *
 * @param payForm the way that the person selected to pay
 * @param ownSubjectAccount the account type
 * @param ownSubjectCard the account number
 * @param phone1 the phone to send the confirmation
 * @param phone2 the phone to send the confirmation
 * @param email1 the email to send the confirmation
 * @param email2 the email to send the confirmation
 * @param carrier1 the carrier of the first phone
 * @param carrier2 the carrier of the second phone
 * @param message message to send
 * @param brand the of the payment
 *
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceConfirmationPayInstitutionsForPayFom:(NSString *)payForm
                                                                         account:(NSString *)ownSubjectAccount
                                                                            card:(NSString *)ownSubjectCard
                                                                       payImport:(NSString *)payImport
                                                                          phone1:(NSString *)phone1
                                                                          phone2:(NSString *)phone2
                                                                          email1:(NSString *)email1
                                                                          email2:(NSString *)email2
                                                                        carrier1:(NSString *)carrier1
                                                                        carrier2:(NSString *)carrier2
                                                                         message:(NSString *)message
                                                                           brand:(NSString *)brand
                                                                            type:(int)type;

/**
 * Generate the entry for close operations
 *
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceClose;

/**
 * Generate the entry for logout operations
 *
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceLogout;

/**
 * Generate the entry for transfer to third accounts confirmation
 *
 * @param fromAccountType From account type
 * @param fromCurrency From currency
 * @param fromIndex From index
 * @param toBranch To branch number
 * @param toAccount To account number
 * @param amount The amount
 * @param currency The currency
 * @param reference The reference
 * @param anEmail1 To send
 * @param anEmail2 To send
 * @param aPhone1 To send
 * @param aCarrier1 Carrier of the first phone
 * @param aPhone2 To send
 * @param aCarrier2 Carrier of the second phone
 * @param message The message
 */
- (NSString *)returnEscapedParameterSequenceTransferToThirdAccountsConfirmationFromAccountType:(NSString *)fromAccountType
                                                                                  fromCurrency:(NSString *)fromCurrency
                                                                                     fromIndex:(NSString *)fromIndex
                                                                                      toBranch:(NSString *)toBranch
                                                                                     toAccount:(NSString *)toAccount
                                                                                        amount:(NSString *)amount
                                                                                      currency:(NSString *)currency
                                                                                     reference:(NSString *)reference
                                                                                        email1:(NSString *)anEmail1
                                                                                        email2:(NSString *)anEmail2
                                                                                        phone1:(NSString *)aPhone1
                                                                                      carrier1:(NSString *)aCarrier1
                                                                                        phone2:(NSString *)aPhone2
                                                                                      carrier2:(NSString *)aCarrier2
                                                                                    andMessage:(NSString *)message;

/**
 * Generate the entry for transfer to accounts from other banks confirmation
 *
 * @param fromAccountType From account type
 * @param fromCurrency From currency
 * @param fromIndex From index
 * @param toBank To banck number
 * @param toBranch To branch number
 * @param toAccount To account number
 * @param toCc To branch number
 * @param amount The amount
 * @param currency The currency
 * @param reference The reference
 * @param itf The itf
 * @param anEmail1 To send
 * @param anEmail2 To send
 * @param aPhone1 To send
 * @param aCarrier1 Carrier of the first phone
 * @param aPhone2 To send
 * @param aCarrier2 Carrier of the second phone
 * @param message The message
 * @param beneficiary The beneficiary
 * @param documentType The document type
 * @param documentNumber The document number
 */
- (NSString *)returnEscapedParameterSequenceTransferToAccountsFromOtherBanksConfirmationFromAccountType:(NSString *)fromAccountType
																						   fromCurrency:(NSString *)fromCurrency
																							  fromIndex:(NSString *)fromIndex
																								 toBank:(NSString *)toBank
																							   toBranch:(NSString *)toBranch
																							  toAccount:(NSString *)toAccount
																								   toCc:(NSString *)toCc
																								 amount:(NSString *)amount
																							   currency:(NSString *)currency
																							  reference:(NSString *)reference
																								 email1:(NSString *)anEmail1
																								 email2:(NSString *)anEmail2
																								 phone1:(NSString *)aPhone1
																							   carrier1:(NSString *)aCarrier1
																								 phone2:(NSString *)aPhone2
																							   carrier2:(NSString *)aCarrier2
																								message:(NSString *)message;

/**
 * Generate the entry for public service initial operation that needs a supply
 *
 * @param supply The supply number
 */
- (NSString *)returnEscapedParameterSequenceForPSInitialOpSupply:(NSString *)supply;

/**
 * Generate the entry for public service cellular initial operation
 *
 * @param telephone The telephone number
 */
- (NSString *)returnEscapedParameterSequenceForPSCellular:(NSString *)telephone;

/**
 * Generate the entry for public service phone initial operation
 *
 * @param telephone The telephone number
 */
- (NSString *)returnEscapedParameterSequenceForPSPhoneNumber:(NSString *)telephone;

/**
 * Generate the entry for payment public service confirmation operation
 *
 * @param issue The issue
 * @param idPayment The payment id
 * @param email1 The email1
 * @param email2 The email2
 * @param phoneNumber1 The phoneNumber1
 * @param phoneNumber2 The phoneNumber2
 * @param carrier1 The carrier1
 * @param carrier2 The carrier2
 * @param message The message
 */
- (NSString *)returnEscapedParameterSequencePaymentPSConfirmationForIssue:(NSString *)issue
                                                                idPayment:(NSString *)idPayment
                                                                   email1:(NSString *)email1
                                                                   email2:(NSString *)email2
                                                             phoneNumber1:(NSString *)phoneNumber1
                                                             phoneNumber2:(NSString *)phoneNumber2
                                                                 carrier1:(NSString *)carrier1
                                                                 carrier2:(NSString *)carrier2
                                                                  message:(NSString *)message;

/**
 * Generate the entry for payment public service confirmation operation
 *
 * @param issue The issue
 * @param payments The payment id
 * @param email1 The email1
 * @param email2 The email2
 * @param phoneNumber1 The phoneNumber1
 * @param phoneNumber2 The phoneNumber2
 * @param carrier1 The carrier1
 * @param carrier2 The carrier2
 * @param message The message
 */
- (NSString *)returnEscapedParameterSequencePaymentPSConfirmationForIssue:(NSString *)issue
																 payments:(NSString *)payments
                                                                   email1:(NSString *)email1
                                                                   email2:(NSString *)email2
                                                             phoneNumber1:(NSString *)phoneNumber1
                                                             phoneNumber2:(NSString *)phoneNumber2
                                                                 carrier1:(NSString *)carrier1
                                                                 carrier2:(NSString *)carrier2
                                                                  message:(NSString *)message;

/**
 * Generate the entry for payment public service data operation
 *
 * @param issue The issue
 * @param idPayment The payment id
 * @param email1 The email1
 * @param email2 The email2
 * @param phoneNumber1 The phoneNumber1
 * @param phoneNumber2 The phoneNumber2
 * @param carrier1 The carrier1
 * @param carrier2 The carrier2
 * @param message The message
 */
- (NSString *)returnEscapedParameterSequencePaymentPSConfirmationUpperForIssue:(NSString *)issue
																	 idPayment:(NSString *)idPayment
																		email1:(NSString *)email1
																		email2:(NSString *)email2
																  phoneNumber1:(NSString *)phoneNumber1
																  phoneNumber2:(NSString *)phoneNumber2
																	  carrier1:(NSString *)carrier1
																	  carrier2:(NSString *)carrier2
																	   message:(NSString *)message;

/**
 * Generate the entry for a payment success operation
 *
 * @param secondFactorKey The second factor key
 */
- (NSString *)returnEscapedParameterSequenceForPaymentSuccessSecondFactorKey:(NSString *)secondFactorKey;

/**
 * Generate the entry for a payment recharge operation.
 *
 * @param company: The company key.
 */
- (NSString *)returnEscapedParamenteSequenceForPaymentRechargeCompany:(NSString *)company;

/**
 * Generate the entry for payment continental card confirmation
 *
 */
- (NSString *)returnEscapedParamenteSequenceForPaymentRechargeConfirmationNumber:(NSString *)phoneNumber
																		   issue:(NSString *)issue
																		  amount:(NSString *)amount
																		  email1:(NSString *)email1
																		  email2:(NSString *)email2
																	phoneNumber1:(NSString *)phoneNumber1
																	phoneNumber2:(NSString *)phoneNumber2
																		carrier1:(NSString *)carrier1
																		carrier2:(NSString *)carrier2
																		 message:(NSString *)message;

/**
 * Generate the entry for a card payment data operation
 *
 * @param issue The issue
 */
- (NSString *)returnEscapedParameterSequenceForIssue:(NSString *)issue;

/**
 * Generate the entry for payment continental card confirmation
 *
 * @param issue The issue
 * @param currency The currency
 * @param amount The amount
 * @param email1 The email1
 * @param email2 The email2
 * @param phoneNumber1 The phoneNumber1
 * @param phoneNumber2 The phoneNumber2
 * @param carrier1 The carrier1
 * @param carrier2 The carrier2
 * @param message The message
 */
- (NSString *)returnEscapedParameterSequencePaymentContCardConfirmationForIssue:(NSString *)issue
                                                                       currency:(NSString *)currency
                                                                         amount:(NSString *)amount
                                                                         email1:(NSString *)email1
                                                                         email2:(NSString *)email2
                                                                   phoneNumber1:(NSString *)phoneNumber1
                                                                   phoneNumber2:(NSString *)phoneNumber2
                                                                       carrier1:(NSString *)carrier1
                                                                       carrier2:(NSString *)carrier2
                                                                        message:(NSString *)message;

/**
 * Generate the entry for payment other bank card confirmation
 *
 * @param issue The issue
 * @param locality The locality
 * @param currency The currency
 * @param amount The amount
 * @param email1 The email1
 * @param email2 The email2
 * @param phoneNumber1 The phoneNumber1
 * @param phoneNumber2 The phoneNumber2
 * @param carrier1 The carrier1
 * @param carrier2 The carrier2
 * @param message The message
 */
- (NSString *)returnEscapedParameterSequencePaymentOtherBankCardConfirmationForIssue:(NSString *)issue
                                                                            locality:(NSString *)locality
                                                                            currency:(NSString *)currency
                                                                              amount:(NSString *)amount
                                                                              email1:(NSString *)email1
                                                                              email2:(NSString *)email2
                                                                        phoneNumber1:(NSString *)phoneNumber1
                                                                        phoneNumber2:(NSString *)phoneNumber2
                                                                            carrier1:(NSString *)carrier1
                                                                            carrier2:(NSString *)carrier2
                                                                             message:(NSString *)message;


/**
 * Generate the entry for other bank card payment data operation
 *
 * @param class The class
 * @param bank The bank
 * @param account The account
 */
- (NSString *)returnEscapedParameterSequencePaymentOtherBankDataForClass:(NSString *)class
                                                                    bank:(NSString *)bank
                                                                 account:(NSString *)account;


/**
 * Generate the entry for a third card payment data operation
 *
 * @param thirdCard The third Card
 */
- (NSString *)returnEscapedParameterSequenceForThirdCard:(NSString *)thirdCard;

/**
 * Generate the entry for the Frequent operation list data operation
 *
 * @param registers indicator
 */
- (NSString *)returnEscapedParameterFrequentOperationInitial:(NSString *)nextPage;

/**
 * Generate the entry for a frequent operation execute data
 *
 * @param codService Unique code service
 * @param afilitionNum The afiliation number
 * @param asociationNick the asociation nick for the operation
 * @param afilitationType the afiliation type
 */
- (NSString *)returnEscapedParameterFrequentOperationExecuteCodService:(NSString *)codService
                                                          AfilitionNum:(NSString *)afilitionNum
                                                        AsociationNick:(NSString *)asociationNick
                                                       AfilitationType:(NSString *)afilitationType;

- (NSString *) returnEscapedParameterSequenceForFrequentOperationOtherBankPaymentWithOperation:(NSString *)operation
                                                                         andDestinationAccount:(NSString *)destinationAccount
                                                                     andDestinationAccountType:(NSString *)destinationAccountType
                                                                        andNameDestinationBank:(NSString *)nameDestinationBank
                                                                                   andBankCode:(NSString *)bankCode
                                                                               andLocalityName:(NSString *)localityName
                                                                               andLocalityCode:(NSString *)localityCode
                                                                                andBeneficiary:(NSString *)beneficiary
                                                                                   andCurrency:(NSString *)currency
                                                                              andOriginAccount:(NSString *)originAccount
                                                                                     andAmount:(NSString *)amount;
- (NSString *) returnEscapedParameterSequenceForFrequentOperationGiftCardWithOperation:(NSString *)operation
                                                                 andbeneficiaryAccount:(NSString *)beneficiaryAccount
                                                                           andCurrency:(NSString *)currency
                                                                      andOriginAccount:(NSString *)originAccount
                                                                             andAmount:(NSString *)amount;

- (NSString *) returnEscapedParameterSequenceForRechargeMovistarCellPhoneFrequentOperationWithOperation:(NSString *)operation
                                                                                             andCompany:(NSString *)company
                                                                                         andServiceType:(NSString *)serviceType
                                                                                     andCellPhoneNumber:(NSString *)cellPhoneNumber
                                                                                             andAccount:(NSString *)account
                                                                                              andAmount:(NSString *)amount;

- (NSString *) returnEscapedParameterSequenceForRechargeClaroCellPhoneFrequentOperationWithOperation:(NSString *)operation
                                                                                      andServiceType:(NSString *)serviceType
                                                                                          andAccount:(NSString *)account
                                                                                  andCellPhoneNumber:(NSString *)cellPhoneNumber;

- (NSString *) returnEscapedParameterSequenceForServiceClaroCellPhoneFrequentOperationWithOperation:(NSString *)operation
                                                                                     andServiceType:(NSString *)serviceType
                                                                                 andCellPhoneHolder:(NSString *)cellPhoneHolder
                                                                                         andAccount:(NSString *)account
                                                                                       andCellPhone:(NSString *)cellPhone;

- (NSString *) returnEscapedParameterSequenceForServiceMovistarCellPhoneFrequentOperationWithOperation:(NSString *)operation
                                                                                            andCompany:(NSString *)company
                                                                                        andServiceType:(NSString *)serviceType
                                                                                    andCellPhoneNumber:(NSString *)cellPhoneNumber
                                                                                             andHolder:(NSString *)holder
                                                                                            andAccount:(NSString *)account;

- (NSString *) returnEscapedParameterSequenceForServiceWaterFrequentOperationWithOperation:(NSString *)operation
                                                                                andCompany:(NSString *)company
                                                                           andSupplyNumber:(NSString *)supplyNumber
                                                                                 andHolder:(NSString *)holder
                                                                            andServiceType:(NSString *)serviceType
                                                                                andAccount:(NSString *)account
                                                                                 andAmount:(NSString *)amount
                                                                               andCurrency:(NSString *)currency;

- (NSString *) returnEscapedParameterSequenceForFrequentOperationInstitutionAndCompaniesWithOperation:(NSString *)operation
                                                                                          andCodMatri:(NSString *)codMatri
                                                                                            andsClase:(NSString *)clase
                                                                                          andsEntidad:(NSString *)entidad
                                                                                       andsCodEntidad:(NSString *)codEntidad
                                                                                           andArreglo:(NSString *)arreglo
                                                                                      andsDescripcion:(NSString *)descripcion
                                                                                       andCuentaCargo:(NSString *)cuentaCargo
                                                                                        andHolderNmae:(NSString *)sNombre;

- (NSString *) returnEscapedParameterSequenceForFrequentOperationTwoStepWithChannel:(NSString *)channel
                                                                      andShortAlias:(NSString *)shortAlias
                                                                      andLargeAlias:(NSString *)largeAlias
                                                                             andDay:(NSString *)day
                                                                         andCellSMS:(NSString *)cellSMS
                                                                     andCellSMSCode:(NSString *)cellSMSCode
                                                                           andEmail:(NSString *)email
                                                                        anEmailCode:(NSString *)emailCode;

- (NSString *)returnEscapedParameterSequenceForFrequentOperationStepThreeWithSecondFactor:(NSString *)secondKeyFactor;

@end


#pragma mark -

@implementation Updater

/**
 * Singleton only instance
 */
static Updater* updaterInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods


/**
 * The allocation returns the singleton only instance
 */
+ (id) allocWithZone:(NSZone *)zone {
    @synchronized([Updater class]) {
		if (updaterInstance_ == nil) {
			updaterInstance_ = [super allocWithZone: zone];
			return updaterInstance_;
		}
	}
	
	return nil;
}

/*
 * Returns the singleton only instance
 */
+ (Updater *)getInstance {
	if (updaterInstance_ == nil) {
		@synchronized ([Updater class]) {
			if (updaterInstance_ == nil) {
				updaterInstance_ = [[Updater alloc] init];
			}
		}
	}
	
	return updaterInstance_;
}

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void) dealloc {
	
	[activeOperations_ release];
	activeOperations_ = nil;
    
	[super dealloc];
}

#pragma mark -
#pragma mark Initialization

/**
 * Object initializer
 *
 * @return An initialized instance
 */
- (id) init {
	
	if ((self = [super init])) {
        [activeOperations_ release];
		activeOperations_ = [[NSMutableDictionary alloc] initWithCapacity: 5];
		
	}
	
	return self;
}

#pragma mark -
#pragma mark Operations

/**
 * Login
 */
- (void)loginWithId:(NSString *)identification andPassword:(NSString *)password{
	
	GeneralLoginResponse *login = [[GeneralLoginResponse alloc] init];
    NSMutableString *uri = nil;
#warning -  use_test x MDP_ENVIRONMENT
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:LOGIN_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
    
#elif defined(MDP_ENVIRONMENT)
    NSString *credentials = [NSString stringWithFormat:LOGIN_OPERATION,identification, password ];
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@%@", TARGET_SERVER, credentials]];
#else
    
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@%@", TARGET_SERVER, LOGIN_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceLoginWithId:identification
                                                                    andPassword:password];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationClearingCookiedWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:login forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:LOGIN] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = login;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[login release];
	
}

- (void)loginEncryptWithId:(NSString *)identification andAlias:(NSString *)alias{
    
    NSString * uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@%@", TARGET_SERVER, LOGIN_OPERATION_ENCRYPT]];
    GeneralLoginEncryptResponse *operationResultResponse = [[GeneralLoginEncryptResponse alloc] init];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceLoginEncryptWithId:identification andAlias:alias];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc]
                                            initWithDownloadId:downloadFile operationType:LOGIN_ENCRYPT] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

- (void)loginDesencryptWithToken:(NSString *)token{
    
    NSString * uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@%@", TARGET_SERVER, LOGIN_OPERATION_ENCRYPT]];
    GeneralLoginEncryptResponse *operationResultResponse = [[GeneralLoginEncryptResponse alloc] init];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceLoginDesencryptWithToken:token];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc]
                                            initWithDownloadId:downloadFile operationType:LOGIN_DESENCRYPT] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

/**
 * Login
 */
- (void)loginWithCoordinate:(NSString *)coordinate {
	
	CoordinateResponse *login = [[CoordinateResponse alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:LOGIN_COORDINATE_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@%@", TARGET_SERVER, LOGIN_COORDINATE_OPERATION]];
	
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceLoginWithCoordinate:coordinate];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:login forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:LOGIN_COORDINATE] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = login;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[login release];
	
}

/*- (void) obtenerCuentas {
 
 AlterGlobalPositionResponse *alterResponse = [[AlterGlobalPositionResponse alloc] init];
 NSMutableString *uri = nil;
 uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, GLOBAL_POSITION_OPERATION]];
 NSString *escapedParameters = @"";
 NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
 NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:alterResponse forListener:self] retain];
 
 OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:RETRIEVE_ALTER_GLOBAL_POSITION] autorelease];
 
 if (operationInfo != nil){
 operationInfo.xmlParserDelegate = alterResponse;
 operationInfo.operationParams = nil;
 [activeOperations_ setObject:operationInfo forKey:downloadFile];
 }
 
 [downloadFile release];
 [alterResponse release];
 
 }*/

/*
 * Obtains the global position
 */
- (void)obtainGlobalPosition {
    
	GlobalPositionResponse *globalPositionResponse = [[GlobalPositionResponse alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:GLOBAL_POSITION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, GLOBAL_POSITION_OPERATION]];
	
#endif
    
	NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:globalPositionResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:RETRIEVE_GLOBAL_POSITION] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = globalPositionResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[globalPositionResponse release];
}

/*
 * Obtains the account transactions without filters
 */
- (void)obtainAccountTransactionsForAccountNumber:(NSString *)anAccountNumber {
    
    
	BankAccount *account = [[Session getInstance].accountList accountFromAccountNumber:anAccountNumber];
    
    if (account != nil) {
        
        AccountTransactionsResponse *accountTransactionsResponse = [[AccountTransactionsResponse alloc] init];
        NSMutableString *uri = nil;
        
#if defined(SIMULATE_HTTP_CONNECTION)
        
        NSString *staticFile = [[NSBundle mainBundle] pathForResource:ACCOUNT_TRANSACTION_LIST_OPERATION ofType:nil];
        NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
        uri = [NSMutableString stringWithString:[staticURL absoluteString]];
        
#else
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, ACCOUNT_TRANSACTION_LIST_OPERATION]];
        
#endif
        
        NSString *escapedParameters = [self returnEscapedParameterSequenceAccountTransactionsWithType:account.type
                                                                                              subject:account.subject];
        NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
        NSString *downloadFile = [[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri
                                                                                          withBody:parametersData
                                                                              andXMLParserDelegate:accountTransactionsResponse
                                                                                       forListener:self];
        
        OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile
                                                                                  operationType:RETRIEVE_ACCOUNT_TRANSACTION_LIST] autorelease];
        
        if (operationInfo != nil) {
            
            operationInfo.xmlParserDelegate = accountTransactionsResponse;
            operationInfo.operationParams = anAccountNumber;
            [activeOperations_ setObject: operationInfo forKey: downloadFile];
            
        }
        
        [accountTransactionsResponse release];
        
    }
    
}

/*
 * Obtains the account transactions detail for a transaction number
 */
- (void)obtainAccountTransactionDetailForAccountNumber:(NSString *)anAccountNumber
                                     transactionNumber:(NSString *)number {
    
    
	AccountTransactionDetailResponse *accountTransactionDetailResponse = [[AccountTransactionDetailResponse alloc] init];
    
    if (accountTransactionDetailResponse != nil) {
        
        NSMutableString *uri = nil;
        
#if defined(SIMULATE_HTTP_CONNECTION)
        
        NSString *staticFile = [[NSBundle mainBundle] pathForResource:ACCOUNT_TRANSACTION_DETAIL_OPERATION ofType:nil];
        NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
        uri = [NSMutableString stringWithString:[staticURL absoluteString]];
        
#else
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, ACCOUNT_TRANSACTION_DETAIL_OPERATION]];
        
#endif
        
        NSString *escapedParameters = [self returnEscapedParameterSequenceAccountTransactionsDetailWithAccountNumber:anAccountNumber
                                                                                                   transactionNumber:number];
        NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
        NSString *downloadFile = [[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri
                                                                                          withBody:parametersData
                                                                              andXMLParserDelegate:accountTransactionDetailResponse
                                                                                       forListener:self];
        
        OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile
                                                                                  operationType:RETRIEVE_ACCOUNT_TRANSACTION_DETAIL] autorelease];
        
        if (operationInfo != nil) {
            
            operationInfo.xmlParserDelegate = accountTransactionDetailResponse;
            operationInfo.operationParams = anAccountNumber;
            [activeOperations_ setObject: operationInfo forKey: downloadFile];
            
        }
        
        [accountTransactionDetailResponse release];
        
    }
    
}

/*
 * Obtains the card transactions
 */
- (void)obtainCardTransactionsForCardNumber:(NSString *)aCardNumber {
	
    Card *card = [[Session getInstance].cardList cardFromCardNumber:aCardNumber];
    
    if (card != nil) {
        
        CardTransactionsResponse *cardTransactionsResponse = [[CardTransactionsResponse alloc] init];
        
        NSMutableString *uri = nil;
        
#if defined(SIMULATE_HTTP_CONNECTION)
        
        NSString *staticFile = [[NSBundle mainBundle] pathForResource:CARD_TRANSACTION_LIST_OPERATION ofType:nil];
        NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
        uri = [NSMutableString stringWithString:[staticURL absoluteString]];
        
#else
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, CARD_TRANSACTION_LIST_OPERATION]];
        
#endif
        
        NSString *escapedParameters = [self returnEscapedParameterSequenceCardTransactionsWithSubject:card.subject];
        NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
        NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:cardTransactionsResponse forListener:self] retain];
        
        OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:RETRIEVE_CARD_TRANSACTION_LIST] autorelease];
        if (operationInfo != nil) {
            operationInfo.xmlParserDelegate = cardTransactionsResponse;
            operationInfo.operationParams = aCardNumber;
            [activeOperations_ setObject: operationInfo forKey: downloadFile];
        }
        
        [downloadFile release];
        [cardTransactionsResponse release];
        
    }
    
}

#pragma mark - Transfer methods
/*
 * Invoke transfer between accounts startup
 */
- (void)transferBetweenAccountsStartup {
    
	TransferStartupResponse *operationResultResponse = [[TransferStartupResponse alloc] init];
    NSMutableString *uri = nil;
    //    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_BETWEEN_ACCOUNTS_STARTUP_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_BETWEEN_ACCOUNTS_STARTUP_OPERATION]];
    
#endif
    
	NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_BETWEEN_ACCOUNTS_STARTUP] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Invoke transfer between accounts confirmation
 */
- (void)transferBetweenAccountsConfirmationFromAccountNumber:(NSString *)fromAccountNumber
                                             fromAccountType:(NSString *)fromAccountType
                                                fromCurrency:(NSString *)fromCurrency
                                                    fromType:(NSString *)fromType
                                                   fromIndex:(NSString *)fromIndex
                                             toAccountNumber:(NSString *)toAccountNumber
                                               toAccountType:(NSString *)toAccountType
                                                  toCurrency:(NSString *)toCurrency
                                                      toType:(NSString *)toType
                                                     toIndex:(NSString *)toIndex
                                                      amount:(NSString *)amount
                                                    currency:(NSString *)currency
                                                  andSubject:(NSString *)subject {
    
	TransferConfirmationResponse *operationResultResponse = [[TransferConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
    //    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceTransferConfirmationFromAccountNumber:fromAccountNumber
																							fromAccountType:fromAccountType
																							   fromCurrency:fromCurrency
																								   fromType:fromType
																								  fromIndex:fromIndex
																							toAccountNumber:toAccountNumber
																							  toAccountType:toAccountType
																								 toCurrency:toCurrency
																									 toType:toType
																									toIndex:toIndex
																									 amount:amount
																								   currency:currency
																								 andSubject:subject];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Invoke transfer between accounts result
 */
- (void)transferBetweenAccountsResult {
    
	TransferSuccessResponse *operationResultResponse = [[TransferSuccessResponse alloc] init];
    NSMutableString *uri = nil;
    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_BETWEEN_ACCOUNTS_RESULT_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_BETWEEN_ACCOUNTS_RESULT_OPERATION]];
    
#endif
    
    NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_BETWEEN_ACCOUNTS_RESULT] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Invoke transfer to third accounts startup
 */
- (void)transferToThirdAccountsStartup {
    
	TransferStartupResponse *operationResultResponse = [[TransferStartupResponse alloc] init];
    NSMutableString *uri = nil;
    //    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_TO_THIRD_ACCOUNTS_STARTUP_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_TO_THIRD_ACCOUNTS_STARTUP_OPERATION]];
    
#endif
    
	NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_THIRD_ACCOUNTS_STARTUP] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Invoke transfer to third accounts confirmation
 */
- (void)transferToThirdAccountsConfirmationFromAccountType:(NSString *)fromAccountType
                                              fromCurrency:(NSString *)fromCurrency
                                                 fromIndex:(NSString *)fromIndex
                                                  toBranch:(NSString *)toBranch
                                                 toAccount:(NSString *)toAccount
                                                    amount:(NSString *)amount
                                                  currency:(NSString *)currency
                                                 reference:(NSString *)reference
                                                    email1:(NSString *)anEmail1
                                                    email2:(NSString *)anEmail2
                                                    phone1:(NSString *)aPhone1
                                                  carrier1:(NSString *)aCarrier1
                                                    phone2:(NSString *)aPhone2
                                                  carrier2:(NSString *)aCarrier2
                                                andMessage:(NSString *)message {
    
    TransferConfirmationResponse *operationResultResponse = [[TransferConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
    //    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_TO_THIRD_ACCOUNTS_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_TO_THIRD_ACCOUNTS_WITH_VALIDATION_CONFIRMATION_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceTransferToThirdAccountsConfirmationFromAccountType:fromAccountType
                                                                                                            fromCurrency:fromCurrency
                                                                                                               fromIndex:fromIndex
                                                                                                                toBranch:toBranch
                                                                                                               toAccount:toAccount
                                                                                                                  amount:amount
                                                                                                                currency:currency
                                                                                                               reference:reference
                                                                                                                  email1:anEmail1
                                                                                                                  email2:anEmail2
                                                                                                                  phone1:aPhone1
                                                                                                                carrier1:aCarrier1
                                                                                                                  phone2:aPhone2
                                                                                                                carrier2:aCarrier2
                                                                                                              andMessage:message];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_THIRD_ACCOUNTS_CONFIRMATION] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}


/*
 * Invoke transfer to third accounts result
 */
- (void)transferToThirdAccountsResultFromSecondFactorKey:(NSString *)secondFactor {
    
	TransferSuccessResponse *operationResultResponse = [[TransferSuccessResponse alloc] init];
    NSMutableString *uri = nil;
    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_TO_THIRD_ACCOUNTS_RESULT_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_TO_THIRD_ACCOUNTS_WITH_VALIDATION_RESULT_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceTransferToThirdAccountsResultFromSecondFactorKey:secondFactor];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_THIRD_ACCOUNTS_RESULT] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}



/*
 * Invoke transfer to accounts from other bank startup
 */
- (void)transferToAccountsFromOtherBanksStartup {
    
	TransferStartupResponse *operationResultResponse = [[TransferStartupResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_STARTUP_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_STARTUP_OPERATION]];
    
    //uri = [NSMutableString stringWithString:@"http://192.168.0.3/bbva/transferenciaPaso1.xml"];
#endif
    
	NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_STARTUP] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Invoke transfer to accounts from other bank confirmation
 */
- (void)transferToAccountsFromOtherBanksConfirmationFromAccountType:(NSString *)fromAccountType
													   fromCurrency:(NSString *)fromCurrency
														  fromIndex:(NSString *)fromIndex
															 toBank:(NSString *)toBank
														   toBranch:(NSString *)toBranch
														  toAccount:(NSString *)toAccount
															   toCc:(NSString *)toCc
															 amount:(NSString *)amount
														   currency:(NSString *)currency
														  reference:(NSString *)reference
															 email1:(NSString *)anEmail1
															 email2:(NSString *)anEmail2
															 phone1:(NSString *)aPhone1
														   carrier1:(NSString *)aCarrier1
															 phone2:(NSString *)aPhone2
														   carrier2:(NSString *)aCarrier2
															message:(NSString *)message {
    
    TransferTypePaymentResponse *operationResultResponse = [[TransferTypePaymentResponse alloc] init];
    NSMutableString *uri = nil;
    //    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_WITH_VALIDATION_CONFIRMATION_OPERATION]];
    
    //uri = [NSMutableString stringWithString:@"http://192.168.0.3/bbva/transferenciaPaso2.xml"];
#endif
	
	NSString *escapedParameters = [self returnEscapedParameterSequenceTransferToAccountsFromOtherBanksConfirmationFromAccountType:fromAccountType
																													 fromCurrency:fromCurrency
																														fromIndex:fromIndex
																														   toBank:toBank
																														 toBranch:toBranch
																														toAccount:toAccount
																															 toCc:toCc
																														   amount:amount
																														 currency:currency
																														reference:@""
																														   email1:anEmail1
																														   email2:anEmail2
																														   phone1:aPhone1
																														 carrier1:aCarrier1
																														   phone2:aPhone2
																														 carrier2:aCarrier2
																														  message:message];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_TYPE_CHECKING] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}


/*
 * Invoke transfer to accounts from other bank confirmation
 */
- (void)transferToAccountsFromOtherBanksWithInmediateConfirmation{
    
    TransferConfirmationResponse *operationResultResponse = [[TransferConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
    //    [Session getInstance].globalPositionRequestServerData = YES;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_INMEDIATE_CONFIRMATION_OPERATION]];
    
#endif

    NSString *escapedParameters = [self returnEscapedParameterSequenceClose];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];

}


/*
 * Invoke transfer to accounts from other bank confirmation
 */
- (void)transferToAccountsFromOtherBanksWithByScheduleConfirmationWithItf:(BOOL)itf
                                                              beneficiary:(NSString *)beneficiary
                                                             documentType:(NSString *)documentType
                                                        andDocumentNumber:(NSString *)documentNumber
{
    TransferConfirmationResponse *operationResultResponse = [[TransferConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
    //    [Session getInstance].globalPositionRequestServerData = YES;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_BY_SCHEDULE_CONFIRMATION_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceTransferByScheduleWithItf:itf
                                                                                    beneficiary:beneficiary documentType:documentType andDocumentNumber:documentNumber];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
}

/*
 * Invoke transfer between accounts result
 */
- (void)transferToAccountsFromOtherBanksResultFromSecondFactorKey:(NSString *)secondFactor {
    
	TransferSuccessResponse *operationResultResponse = [[TransferSuccessResponse alloc] init];
    NSMutableString *uri = nil;
    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_RESULT_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_WITH_VALIDATION_RESULT_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceTransferResultFromSecondFactorKey:secondFactor];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_RESULT] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Invoke transfer to accounts With Cash Mobile startup
 */
- (void)transferToAccountsWithCashMobileStartup {
    
    TransferStartupResponse *operationResultResponse = [[TransferStartupResponse alloc] init];
    NSMutableString *uri = nil;
    //    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_TO_THIRD_ACCOUNTS_STARTUP_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_STARTUP_OPERATION]];
    
#endif
    
	NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_STARTUP] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Invoke transfer with cash mobile confirmation
 */
- (void)transferWithCashMobileConfirmationFromAccountType:(NSString *)fromAccountType
                                             fromCurrency:(NSString *)fromCurrency
                                                fromIndex:(NSString *)fromIndex
                                                  toPhone:(NSString *)toPhone
                                                   amount:(NSString *)amount
                                                 currency:(NSString *)currency {
    
    TransferConfirmationResponse *operationResultResponse = [[TransferConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_TO_THIRD_ACCOUNTS_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_CONFIRMATION_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceWithCashMobileConfirmationFromAccountType:fromAccountType
                                                                                                   fromCurrency:fromCurrency
                                                                                                      fromIndex:fromIndex
                                                                                                        toPhone:toPhone
                                                                                                         amount:amount
                                                                                                       currency:currency];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_CONFIRMATION] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Invoke transfer with cash mobile result
 */
- (void)transferWithCashMobileResultFromSecondKeyFactor:(NSString *)secondKeyFactor {
    
	TransferSuccessResponse *operationResultResponse = [[TransferSuccessResponse alloc] init];
    NSMutableString *uri = nil;
    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_TO_THIRD_ACCOUNTS_RESULT_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH,TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_RESULT_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceTransferResultFromSecondFactorKey:secondKeyFactor];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_RESULT  ] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Invoke transfer to accounts from other bank startup
 */
- (void)transferToGiftCardStartup {
    
	GiftCardStepOneResponse *operationResultResponse = [[GiftCardStepOneResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_GIFT_CARD_STARTUP_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_GIFT_CARD_STARTUP_OPERATION]];
    
#endif
    
	NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_ACCOUNT_TO_GIFT_CARD_STARTUP] autorelease];
    
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/**
 * Invoke transfer to gift card confirmation
 *
 * @param fromAccountType from account type
 * @param fromNumber from account or card number
 * @param toNumber to account number
 * @param amount the amount
 * @param currency the currency
 * @param toConfirmationPhone the phone to send the confirmation
 * @param toConfirmationMail the mail to send the confirmation
 * @param carrier the carrier for phones
 * @param referenceMessage the referenceMessage
 */
- (void)transferToGiftCardConfirmationFromAccountType:(NSString *)fromAccountType
                                           fromNumber:(NSString *)fromNumber
                                             toNumber:(NSString *)toNumber
                                               amount:(NSString *)amount
                                             currency:(NSString *)currency
                                  toConfirmationPhone:(NSString *)toConfirmationPhone
                                   toConfirmationMail:(NSString *)toConfirmationMail
                                              carrier:(NSString *)carrier
                                     referenceMessage:(NSString *)referenceMessage{
    
    GiftCardStepTwoResponse *operationResultResponse = [[GiftCardStepTwoResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_TO_THIRD_ACCOUNTS_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_GIFT_CARD_CONFIRMATION_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceToGiftCardConfirmationFromAccountType:fromAccountType
                                                                                                 fromNumber:fromNumber
                                                                                                   toNumber:toNumber
                                                                                                     amount:amount
                                                                                                   currency:currency
                                                                                        toConfirmationPhone:toConfirmationPhone
                                                                                         toConfirmationMail:toConfirmationMail
                                                                                                    carrier:carrier
                                                                                           referenceMessage:referenceMessage];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_ACCOUNT_TO_GIFT_CARD_CONFIRMATION] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Invoke transfer to gift card result
 */
- (void)TransferToGiftCardResultFromSecondFactorKey:(NSString *)secondFactor {
    
	GiftCardStepThreeResponse *operationResultResponse = [[GiftCardStepThreeResponse alloc] init];
    NSMutableString *uri = nil;
    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:TRANSFER_TO_THIRD_ACCOUNTS_RESULT_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, TRANSFER_GIFT_CARD_RESULT_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceTransferResultFromSecondFactorKey:secondFactor];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:TRANSFER_TO_ACCOUNT_TO_GIFT_CARD_RESULT] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

#pragma mark - Obtain methods
/*
 * Invoke obtain cash mobile transactions information
 */
- (void)obtainCashMobileTransactionDetail {
    
	TransferDetailResponse *operationResultResponse = [[TransferDetailResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:OBTAIN_CASH_MOBILE_DETAIL_STARTUP_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, OBTAIN_CASH_MOBILE_DETAIL_STARTUP_OPERATION]];
    
#endif
    
	NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:OBTAIN_CASH_MOBILE_DETAIL_STARTUP] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}


/*
 * Invoke obtain transaction with operation code
 */
- (void)obtainCashMobileTransactionDetailWithOperationCode:(NSString *)operationCode
{
    
    TransferShowDetailResponse *operationResultResponse = [[TransferShowDetailResponse alloc] init];
    NSMutableString *uri = nil;
    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:OBTAIN_CASH_MOBILE_SHOW_DETAIL_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH,OBTAIN_CASH_MOBILE_SHOW_DETAIL_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceObtainCashMobileTransactionResultFromOperationCode:operationCode];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:OBTAIN_CASH_MOBILE_DETAIL  ] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Invoke obtain cash mobile transactions resend
 */
- (void)obtainCashMobileTransactionResend:(NSString *)operationCode {
    
	TransferSuccessResponse *operationResultResponse = [[TransferSuccessResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:OBTAIN_CASH_MOBILE_RESEND_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, OBTAIN_CASH_MOBILE_RESEND_OPERATION]];
    
#endif
    
	NSString *escapedParameters = [self returnEscapedParameterSequenceObtainCashMobileTransactionResultFromOperationCode:operationCode];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:OBTAIN_CASH_MOBILE_DETAIL_RESEND] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}



/*
 * Invoke transfer confirmation
 */
- (void)retrieveRetainsForAccountNumber:(NSString *)forAccountNumber
							accountType:(NSString *)accountType
								balance:(NSString *)balance
					   availableBalance:(NSString *)availableBalance
							   currency:(NSString *)currency
								   bank:(NSString *)bank
								 office:(NSString *)office
						   controlDigit:(NSString *)controlDigit
								account:(NSString *)account
								   type:(NSString *)type
							   andIndex:(NSString *)index {
    
	RetentionListResponse *operationResultResponse = [[RetentionListResponse alloc] init];
    NSMutableString *uri = nil;
    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:RETRIEVE_RETAINS_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, RETRIEVE_RETAINS_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceRetrieveRetainsForAccountNumber:forAccountNumber
																						  accountType:accountType
																							  balance:balance
																					 availableBalance:availableBalance
																							 currency:currency
																								 bank:bank
																							   office:office
																						 controlDigit:controlDigit
																							  account:account
																								 type:type
																							 andIndex:index];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:RETRIEVE_RETAINS] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Invoke transaction send confirmation
 */
- (void)sendTransactionConfirmationForEmail:(NSString *)email1
									 email2:(NSString *)email2
								   carrier1:(NSString *)carrier1
								   carrier2:(NSString *)carrier2
							   phonenumber1:(NSString *)phonenumber1
							   phonenumber2:(NSString *)phonenumber2
								andComments:(NSString *)comments {
    
	GeneralStatusResponse *operationResultResponse = [[GeneralStatusResponse alloc] init];
    NSMutableString *uri = nil;
    //    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:SEND_TRANSACTION_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, SEND_TRANSACTION_CONFIRMATION_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceSendTransactionConfirmationForEmail:email1
																								   email2:email2
																								 carrier1:carrier1
																								 carrier2:carrier2
																							 phonenumber1:phonenumber1
																							 phonenumber2:phonenumber2
																							  andComments:comments];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:SEND_TRANSACTION_CONFIRMATION] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Obtains the institutions and copmanies services list
 */
- (void)obtainPaymentInstitutionsAndCompanies {
	
    PaymentInstitutionsAndCompaniesInitialResponse *operationResultResponse = [[PaymentInstitutionsAndCompaniesInitialResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_INSTITUTIONS_LIST_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@consultar", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_INSTITUTIONS_LIST_OPERATION]];
    
#endif
    NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_INSTITUTIONS_LIST_OPERATION_RESULT] autorelease];
	
	if (operationInfo != nil) {
		
		[operationInfo setXmlParserDelegate:operationResultResponse];
		[operationInfo setOperationParams:nil];
		[activeOperations_ setObject:operationInfo forKey:downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Search a company by some arguments
 */
- (void)obtainInstitutionsAndCompaniesForEntity:(NSString *)entity
                                     searchType:(NSString *)searchType
                                   nextMovement:(NSString *)nextMovement
                                         indPag:(NSString *)indPag
                                      searchArg:(NSString *)searchArg
                                      LastDescr:(NSString *)lastDescription
                                         button:(NSString *)button
                                         action:(NSString *)action
{
    
    PaymentInstitutionsAndCompaniesInitialResponse *operationResultResponse = [[PaymentInstitutionsAndCompaniesInitialResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_INSTITUTIONS_LIST_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_INSTITUTIONS_LIST_OPERATION, action]];
    
#endif
    NSString *escapedParameters = [self returnEscapedParameterSequenceSearchInstitutionsForEntity:entity
                                                                                       searchType:searchType
                                                                                     nextMovement:nextMovement
                                                                                           indPag:indPag
                                                                                        searchArg:searchArg
                                                                                        LastDescr:lastDescription
                                                                                           button:button];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_INSTITUTIONS_SEARCH_LIST_RESULT] autorelease];
	
	if (operationInfo != nil) {
		
		[operationInfo setXmlParserDelegate:operationResultResponse];
		[operationInfo setOperationParams:nil];
		[activeOperations_ setObject:operationInfo forKey:downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

/*
 * Obtains the institution detail
 */
- (void)obtainPaymentInstitutionsDetailForCod:(NSString *)institutionCode
                                   optionType:(NSString *)optionType {
	
    PaymentInstitutionsAndCompaniesDetailResponse *operationResultResponse = [[PaymentInstitutionsAndCompaniesDetailResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_INSTITUTIONS_DETAIL_RESULT ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_INSTITUTIONS_DETAIL_RESULT]];
    
#endif
    NSString *escapedParameters = [self returnEscapedParameterSequenceDetailInstitutionsForEntity:institutionCode
                                                                                       optionType:optionType];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_INSTITUTIONS_DETAIL] autorelease];
	
	if (operationInfo != nil) {
		
		[operationInfo setXmlParserDelegate:operationResultResponse];
		[operationInfo setOperationParams:nil];
		[activeOperations_ setObject:operationInfo forKey:downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Obtains the institution panding pays
 */
- (void)obtainInstitutionsAndCompaniesPendingPaysForCode:(NSString *)code
                                               arrayLong:(NSString *)arrayLong
                                                   array:(NSString *)array
                                             titlesArray:(NSString *)titlesArray
                                          validationData:(NSString *)valData
                                              flagModule:(NSString *)flagModule
{
    PaymentInstitutionAndCompaniesConfirmationResponse *operationResultResponse = [[PaymentInstitutionAndCompaniesConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_INSTITUTIONS_INFORMATION_TO_PAY_RESULT ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_INSTITUTIONS_INFORMATION_TO_PAY_RESULT]];
    
#endif
    NSString *escapedParameters = [self returnEscapedParameterSequencePendingPaysInstitutionsForCode:code
                                                                                           arrayLong:arrayLong
                                                                                               array:array
                                                                                         titlesArray:titlesArray
                                                                                      validationData:valData
                                                                                          flagModule:flagModule];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_INSTITUTIONS_PENDING_PAYS_DETAIL] autorelease];
	
	if (operationInfo != nil) {
		
		[operationInfo setXmlParserDelegate:operationResultResponse];
		[operationInfo setOperationParams:nil];
		[activeOperations_ setObject:operationInfo forKey:downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

/*
 * Obtains the confirmation payment for the institution
 */
- (void)obtainInstitutionsAndCompaniesPayConfirmationWithForm:(NSString *)payForm
                                            ownSubjectAccount:(NSString *)ownSubjectAccount
                                               ownSubjectCard:(NSString *)card
                                                    payImport:(NSString *)payImport
                                                 phonenumber1:(NSString *)phonenumber1
                                                 phonenumber2:(NSString *)phonenumber2
                                                       email1:(NSString *)email1
                                                       email2:(NSString *)email2
                                                     carrier1:(NSString *)carrier1
                                                     carrier2:(NSString *)carrier2
                                                  mailMessage:(NSString *)mailMessage
                                                        brand:(NSString *)brand
                                                         type:(int)type
{
    PaymentInstitutionAndCompaniesConfirmationInformationResponse *operationResultResponse = [[PaymentInstitutionAndCompaniesConfirmationInformationResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_INSTITUTIONS_CONFIRMATION_OF_PAY_RESULT ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_INSTITUTIONS_CONFIRMATION_OF_PAY_RESULT]];
    
#endif
    NSString *escapedParameters = [self returnEscapedParameterSequenceConfirmationPayInstitutionsForPayFom:payForm
                                                                                                   account:ownSubjectAccount
                                                                                                      card:card
                                                                                                 payImport:payImport
                                                                                                    phone1:phonenumber1
                                                                                                    phone2:phonenumber2
                                                                                                    email1:email1
                                                                                                    email2:email2
                                                                                                  carrier1:carrier1
                                                                                                  carrier2:carrier2
                                                                                                   message:mailMessage
                                                                                                     brand:brand type:type];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_INSTITUTIONS_CONFIRMATION_PAY_DETAIL] autorelease];
	
	if (operationInfo != nil) {
		
		[operationInfo setXmlParserDelegate:operationResultResponse];
		[operationInfo setOperationParams:nil];
		[activeOperations_ setObject:operationInfo forKey:downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Invoke confirmation payment institutions
 */
- (void)institutionsAndcompaniesConfirmResultFromSecondKeyFactor:(NSString *)secondKeyFactor {
    
	PaymentInstitutionAndCompaniesSuccessConfirmationResponse *operationResultResponse = [[PaymentInstitutionAndCompaniesSuccessConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_INSTITUTIONS_SUCCSESS_OF_PAY_RESULT ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH,PAYMENT_INSTITUTIONS_SUCCSESS_OF_PAY_RESULT]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceTransferResultFromSecondFactorKey:secondKeyFactor];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_INSTITUTIONS_SUCCESS_PAYS_DETAIL  ] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Obtains the payments public services list
 */
- (void)obtainPaymentPublicServices {
	
    ServiceResponse *operationResultResponse = [[ServiceResponse alloc] init];
    NSMutableString *uri = nil;
    //    [Session getInstance].globalPositionRequestServerData = YES;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_SERVICES_LIST_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_SERVICES_LIST_OPERATION]];
    
#endif
    
    NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_SERVICES_LIST_OPERATION_RESULT] autorelease];
	
	if (operationInfo != nil) {
		
		[operationInfo setXmlParserDelegate:operationResultResponse];
		[operationInfo setOperationParams:nil];
		[activeOperations_ setObject:operationInfo forKey:downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}


/*
 * Obtains the public service - electric service - payment inicialization
 */
- (void)obtainPaymentPSElectricServicesInitializationForCompany:(NSString *)company {
	
    PublicServiceResponse *operationResultResponse = [[PublicServiceResponse alloc] init];
	[operationResultResponse setPaymentOperationType:PTEPaymentPSElectricServices];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_ELECTRIC_SERVICE_INITIAL_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:COMPANY_LUZ_DEL_SUR]) {
        operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_ELECT_LUZ_DEL_SUR;
    } else if ([auxCompany isEqualToString:COMPANY_EDELNOR]) {
        operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_ELECT_EDELNOR;
    }
    
#else
    
    NSString *action = @"";
    
    NSString *auxCompany = [company lowercaseString];
	
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:COMPANY_LUZ_DEL_SUR]) {
        action = LUZDELSUR_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_ELECT_LUZ_DEL_SUR;
        
    } else if ([auxCompany isEqualToString:COMPANY_EDELNOR]) {
        action = EDELNOR_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_ELECT_EDELNOR;
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_ELECTRIC_SERVICE_INITIAL_OPERATION, action]];
    
#endif
    
    NSString *escapedParameters = @"";
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
	
}


/*
 * Obtains the public service - electric service - payment data
 */
- (void)obtainPaymentPSElectricServicesDataForCompany:(NSString *)company
                                             supplies:(NSString *)supplies {
    
    PaymentDataResponse *operationResultResponse = [[PaymentDataResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_ELECTRIC_SERVICE_DATA_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:COMPANY_LUZ_DEL_SUR]) {
        operationType = PAYMENT_PUBLIC_SERVICE_DATA_ELECT_LUZ_DEL_SUR;
    } else if ([auxCompany isEqualToString:COMPANY_EDELNOR]) {
        operationType = PAYMENT_PUBLIC_SERVICE_DATA_ELECT_EDELNOR;
    }
    
#else
    
    NSString *action = @"";
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:COMPANY_LUZ_DEL_SUR]) {
        action = LUZDELSUR_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_DATA_ELECT_LUZ_DEL_SUR;
    } else if ([auxCompany isEqualToString:COMPANY_EDELNOR]) {
        action = EDELNOR_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_DATA_ELECT_EDELNOR;
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_ELECTRIC_SERVICE_DATA_OPERATION, action]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForPSInitialOpSupply:supplies];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
	
}

/*
 * Obtains the public service - electric service - payment confirmation
 */
- (void)obtainPaymentPSElectricServicesConfirmationForCompany:(NSString *)company
                                                        issue:(NSString *)issue
                                                    idPayment:(NSString *)idPayment
                                                       email1:(NSString *)email1
                                                       email2:(NSString *)email2
                                                 phoneNumber1:(NSString *)phoneNumber1
                                                 phoneNumber2:(NSString *)phoneNumber2
                                                     carrier1:(NSString *)carrier1
                                                     carrier2:(NSString *)carrier2
                                                      message:(NSString *)message {
    
    PaymentConfirmationResponse *operationResultResponse = [[PaymentConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_ELECTRIC_SERVICE_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:COMPANY_LUZ_DEL_SUR]) {
        operationType = PAYMENT_PUBLIC_SERVICE_CONF_ELECT_LUZ_DEL_SUR;
    } else if ([auxCompany isEqualToString:COMPANY_EDELNOR]) {
        operationType = PAYMENT_PUBLIC_SERVICE_CONF_ELECT_EDELNOR;
    }
    
#else
    
    NSString *action = @"";
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:COMPANY_LUZ_DEL_SUR]) {
        action = LUZDELSUR_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_CONF_ELECT_LUZ_DEL_SUR;
    } else if ([auxCompany isEqualToString:COMPANY_EDELNOR]) {
        action = EDELNOR_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_CONF_ELECT_EDELNOR;
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_ELECTRIC_SERVICE_CONFIRMATION_OPERATION, action]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequencePaymentPSConfirmationUpperForIssue:issue
																							   idPayment:idPayment
																								  email1:email1
																								  email2:email2
																							phoneNumber1:phoneNumber1
																							phoneNumber2:phoneNumber2
																								carrier1:carrier1
																								carrier2:carrier2
																								 message:message];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
	
}

/*
 * Obtains the public service - electric service - payment success
 */
- (void)obtainPaymentPSElectricServicesSuccessForCompany:(NSString *)company
                                         secondFactorKey:(NSString *)secondFactorKey {
    
    PaymentSuccessResponse *operationResultResponse = [[PaymentSuccessResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_ELECTRIC_SERVICE_SUCCESS_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    if ([auxCompany isEqualToString:COMPANY_LUZ_DEL_SUR]) {
        operationType = PAYMENT_PUBLIC_SERVICE_SUCCESS_ELECT_LUZ_DEL_SUR;
    } else if ([auxCompany isEqualToString:COMPANY_EDELNOR]) {
        operationType = PAYMENT_PUBLIC_SERVICE_SUCCESS_ELECT_EDELNOR;
    }
    
#else
    
    NSString *action = @"";
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    if ([auxCompany isEqualToString:COMPANY_LUZ_DEL_SUR]) {
        action = LUZDELSUR_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_SUCCESS_ELECT_LUZ_DEL_SUR;
    } else if ([auxCompany isEqualToString:COMPANY_EDELNOR]) {
        action = EDELNOR_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_SUCCESS_ELECT_EDELNOR;
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_ELECTRIC_SERVICE_SUCCESS_OPERATION, action]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForPaymentSuccessSecondFactorKey:secondFactorKey];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}


/*
 * Obtains the public service - water service - payment inicialization
 */
- (void)obtainPaymentPSWaterServicesInitializationForCompany:(NSString *)company {
	
    PublicServiceResponse *operationResultResponse = [[PublicServiceResponse alloc] init];
	[operationResultResponse setPaymentOperationType:PTEPaymentPSWaterServices];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_WATER_SERVICE_INITIAL_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    NSInteger operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_WATER_SEDAPAL;
#else
    
    NSString *action = @"";
    NSInteger operationType = NSNotFound;
    NSString *auxCompany = [company lowercaseString];
    
    if ([auxCompany isEqualToString:COMPANY_SEDAPAL]) {
        action = SEDAPAL_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_WATER_SEDAPAL;
    } else if ([auxCompany isEqualToString:COMPANY_SEDAPAR]){
        action = SEDAPAR_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_WATER_SEDAPAL;
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_WATER_SERVICE_INITIAL_OPERATION, action]];
    
#endif
    
    NSString *escapedParameters = @"";
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
	
}


/*
 * Obtains the public service - water service - payment data
 */
- (void)obtainPaymentPSWaterServicesDataForCompany:(NSString *)company
                                          supplies:(NSString *)supplies {
    
    PaymentDataResponse *operationResultResponse = [[PaymentDataResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_WATER_SERVICE_DATA_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    NSInteger operationType = PAYMENT_PUBLIC_SERVICE_DATA_WATER_SEDAPAL;
#else
    
    NSString *action = @"";
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:COMPANY_SEDAPAL]) {
        action = SEDAPAL_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_DATA_WATER_SEDAPAL;
        
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_WATER_SERVICE_DATA_OPERATION, action]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForPSInitialOpSupply:supplies];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
	
}

/*
 * Obtains the public service - water service - payment confirmation
 */
- (void)obtainPaymentPSWaterServicesConfirmationForCompany:(NSString *)company
                                                     issue:(NSString *)issue
												idPayments:(NSString *)payments
                                                    email1:(NSString *)email1
                                                    email2:(NSString *)email2
                                              phoneNumber1:(NSString *)phoneNumber1
                                              phoneNumber2:(NSString *)phoneNumber2
                                                  carrier1:(NSString *)carrier1
                                                  carrier2:(NSString *)carrier2
                                                   message:(NSString *)message {
    
    PaymentConfirmationResponse *operationResultResponse = [[PaymentConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_WATER_SERVICE_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    if ([auxCompany isEqualToString:COMPANY_SEDAPAL]) {
        operationType = PAYMENT_PUBLIC_SERVICE_CONF_WATER_SEDAPAL;
    }
    
#else
    
    NSString *action = @"";
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:COMPANY_SEDAPAL]) {
        action = SEDAPAL_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_CONF_WATER_SEDAPAL;
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_WATER_SERVICE_CONFIRMATION_OPERATION, action]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequencePaymentPSConfirmationForIssue:issue
																						   payments:payments
                                                                                             email1:email1
                                                                                             email2:email2
                                                                                       phoneNumber1:phoneNumber1
                                                                                       phoneNumber2:phoneNumber2
                                                                                           carrier1:carrier1
                                                                                           carrier2:carrier2
                                                                                            message:message];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
	
}


/*
 * Obtains the public service - water service - payment success
 */
- (void)obtainPaymentPSWaterServicesSuccessForCompany:(NSString *)company
                                      secondFactorKey:(NSString *)secondFactorKey {
    
    PaymentSuccessResponse *operationResultResponse = [[PaymentSuccessResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_WATER_SERVICE_SUCCESS_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:COMPANY_SEDAPAL]) {
        operationType = PAYMENT_PUBLIC_SERVICE_SUCCESS_WATER_SEDAPAL;
    }
    
#else
    
    NSString *action = @"";
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:COMPANY_SEDAPAL]) {
        action = SEDAPAL_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_SUCCESS_WATER_SEDAPAL;
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_WATER_SERVICE_SUCCESS_OPERATION, action]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForPaymentSuccessSecondFactorKey:secondFactorKey];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
	
}


/*
 * Obtains the public service - phone - payment inicialization
 */
- (void)obtainPaymentPSPhoneInitializationForCompany:(NSString *)company {
	
    PublicServiceResponse *operationResultResponse = [[PublicServiceResponse alloc] init];
	[operationResultResponse setPaymentOperationType:PTEPaymentPSPhone];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_PHONE_SERVICE_INITIAL_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    NSInteger operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_PHONE_TELEFONICA;
    
#else
    
    NSString *action = @"";
    NSInteger operationType = NSNotFound;
    NSString *auxCompany = [company lowercaseString];
    
    if ([auxCompany isEqualToString:COMPANY_TELEFONICA]) {
        action = TELEFONICA_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_PHONE_TELEFONICA;
        
    } else if ([auxCompany isEqualToString:COMPANY_CLARO3PLAY]) {
        action = TELEFONICA_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_PHONE_TELEFONICA;
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_PHONE_SERVICE_INITIAL_OPERATION, action]];
    
#endif
    
    NSString *escapedParameters = @"";
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

/*
 * Obtains the public service - phone - payment data
 */
- (void)obtainPaymentPSPhoneServicesDataForCompany:(NSString *)company
                                       phoneNumber:(NSString *)phoneNumber {
    
    PaymentDataResponse *operationResultResponse = [[PaymentDataResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_PHONE_SERVICE_DATA_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    NSInteger operationType = PAYMENT_PUBLIC_SERVICE_DATA_PHONE_TELEFONICA;
    
#else
    
    //    NSString *action = @"";
    
    //    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    
    //    if ([auxCompany isEqualToString:COMPANY_TELEFONICA]) {
    //    action = TELEFONICA_ACTION;
    operationType = PAYMENT_PUBLIC_SERVICE_DATA_PHONE_TELEFONICA;
    //    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_PHONE_SERVICE_DATA_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForPSPhoneNumber:phoneNumber];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

/*
 * Obtains the public service - phone - payment confirmation
 */
- (void)obtainPaymentPSPhoneConfirmationForCompany:(NSString *)company
                                             issue:(NSString *)issue
                                         idPayment:(NSString *)idPayment
                                            email1:(NSString *)email1
                                            email2:(NSString *)email2
                                      phoneNumber1:(NSString *)phoneNumber1
                                      phoneNumber2:(NSString *)phoneNumber2
                                          carrier1:(NSString *)carrier1
                                          carrier2:(NSString *)carrier2
                                           message:(NSString *)message {
	
    PaymentConfirmationResponse *operationResultResponse = [[PaymentConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_PHONE_SERVICE_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    if ([auxCompany isEqualToString:[COMPANY_TELEFONICA lowercaseString]]) {
        operationType = PAYMENT_PUBLIC_SERVICE_CONF_PHONE_TELEFONICA;
    }
#else
    
    NSString *action = @"";
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    if ([auxCompany isEqualToString:COMPANY_TELEFONICA]) {
        action = TELEFONICA_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_CONF_PHONE_TELEFONICA;
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_PHONE_SERVICE_CONFIRMATION_OPERATION, action]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequencePaymentPSConfirmationForIssue:issue
                                                                                          idPayment:idPayment
                                                                                             email1:email1
                                                                                             email2:email2
                                                                                       phoneNumber1:phoneNumber1
                                                                                       phoneNumber2:phoneNumber2
                                                                                           carrier1:carrier1
                                                                                           carrier2:carrier2
                                                                                            message:message];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
	
}

/*
 * Obtains the public service - phone - payment success
 */
- (void)obtainPaymentPSPhoneSuccessForCompany:(NSString *)company
                              secondFactorKey:(NSString *)secondFactorKey {
    
    PaymentSuccessResponse *operationResultResponse = [[PaymentSuccessResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_PHONE_SERVICE_SUCCESS_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    if ([auxCompany isEqualToString:COMPANY_TELEFONICA]) {
        operationType = PAYMENT_PUBLIC_SERVICE_SUCCESS_PHONE_TELEFONICA;
    }
    
#else
    
    NSString *action = @"";
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    if ([auxCompany isEqualToString:COMPANY_TELEFONICA]) {
        action = TELEFONICA_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_SUCCESS_PHONE_TELEFONICA;
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_PHONE_SERVICE_SUCCESS_OPERATION, action]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForPaymentSuccessSecondFactorKey:secondFactorKey];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
	
}


/*
 * Obtains the public service - cellular - payment inicialization
 */
- (void)obtainPaymentPSCellularInitializationForCompany:(NSString *)company {
	
    PublicServiceResponse *operationResultResponse = [[PublicServiceResponse alloc] init];
	[operationResultResponse setPaymentOperationType:PTEPaymentPSCellular];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = @"";
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:kCarrierMovistarCode]) {
        staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_MOVISTAR_INITIAL_OPERATION ofType:nil];
        operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_CELL_MOVISTAR;
        
    } else if ([auxCompany isEqualToString:kCarrierClaroCode]) {
        staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CLARO_INITIAL_OPERATION ofType:nil];
        operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_CELL_CLARO;
        
    }
    
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
    NSString *action = @"";
    NSString *auxCompany = [company lowercaseString];
	NSString *operation = @"";
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:kCarrierMovistarCode]) {
        operation = PAYMENT_MOVISTAR_INITIAL_OPERATION;
        action = MOVISTAR_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_CELL_MOVISTAR;
    } else if ([auxCompany isEqualToString:kCarrierClaroCode]) {
        operation = PAYMENT_CLARO_INITIAL_OPERATION;
        action = CLARO_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_INITIAL_CELL_CLARO;
        
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, operation, action]];
    
#endif
    
    NSString *escapedParameters = @"";
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

/*
 * Obtains the public service - phone - payment data
 */
- (void)obtainPaymentPSCellularServicesDataForCompany:(NSString *)company
                                          phoneNumber:(NSString *)phoneNumber {
	
    PaymentDataResponse *operationResultResponse = [[PaymentDataResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = @"";
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:kCarrierMovistarCode]) {
        staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_MOVISTAR_DATA_OPERATION ofType:nil];
        operationType = PAYMENT_PUBLIC_SERVICE_DATA_CELL_MOVISTAR;
    } else if ([auxCompany isEqualToString:kCarrierClaroCode]) {
        staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CLARO_DATA_OPERATION ofType:nil];
        operationType = PAYMENT_PUBLIC_SERVICE_DATA_CELL_CLARO;
    }
    
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
    NSString *action = @"";
    NSString *auxCompany = [company lowercaseString];
	NSString *operation = @"";
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:kCarrierMovistarCode]) {
        operation = PAYMENT_MOVISTAR_DATA_OPERATION;
        action = MOVISTAR_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_DATA_CELL_MOVISTAR;
    } else if ([auxCompany isEqualToString:kCarrierClaroCode]) {
        operation = PAYMENT_CLARO_DATA_OPERATION;
        action = CLARO_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_DATA_CELL_CLARO;
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, operation, action]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForPSCellular:phoneNumber];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

/*
 * Obtains the public service - Cellular - payment confirmation
 */
- (void)obtainPaymentPSCellularConfirmationForCompany:(NSString *)company
                                                issue:(NSString *)issue
                                            idPayment:(NSString *)idPayment
                                               email1:(NSString *)email1
                                               email2:(NSString *)email2
                                         phoneNumber1:(NSString *)phoneNumber1
                                         phoneNumber2:(NSString *)phoneNumber2
                                             carrier1:(NSString *)carrier1
                                             carrier2:(NSString *)carrier2
                                              message:(NSString *)message {
    
    PaymentConfirmationResponse *operationResultResponse = [[PaymentConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = @"";
	
    NSString *auxCompany = [company lowercaseString];
    
    if ([auxCompany isEqualToString:kCarrierMovistarCode]) {
        staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_MOVISTAR_CONFIRMATION_OPERATION ofType:nil];
    } else if ([auxCompany isEqualToString:kCarrierClaroCode]) {
        staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CLARO_CONFIRMATION_OPERATION ofType:nil];
    }
    
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:kCarrierMovistarCode]) {
        operationType = PAYMENT_PUBLIC_SERVICE_CONF_CELL_MOVISTAR;
    } else if ([auxCompany isEqualToString:kCarrierClaroCode]) {
        operationType = PAYMENT_PUBLIC_SERVICE_CONF_CELL_CLARO;
    }
    
#else
    
    NSString *action = @"";
	NSString *auxCompany = [company lowercaseString];
	NSString *operation = @"";
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:kCarrierMovistarCode]) {
        operation = PAYMENT_MOVISTAR_CONFIRMATION_OPERATION;
        action = MOVISTAR_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_CONF_CELL_MOVISTAR;
    } else if ([auxCompany isEqualToString:kCarrierClaroCode]) {
        operation = PAYMENT_CLARO_CONFIRMATION_OPERATION;
        action = CLARO_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_CONF_CELL_CLARO;
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, operation, action]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequencePaymentPSConfirmationForIssue:issue
                                                                                          idPayment:idPayment
                                                                                             email1:email1
                                                                                             email2:email2
                                                                                       phoneNumber1:phoneNumber1
                                                                                       phoneNumber2:phoneNumber2
                                                                                           carrier1:carrier1
                                                                                           carrier2:carrier2
                                                                                            message:message];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
	
	
}

/*
 * Obtains the public service - Cellular - payment success
 */
- (void)obtainPaymentPSCellularSuccessForCompany:(NSString *)company
                                 secondFactorKey:(NSString *)secondFactorKey {
    
    PaymentSuccessResponse *operationResultResponse = [[PaymentSuccessResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = @"";
    
    NSString *auxCompany = [company lowercaseString];
    
    if ([auxCompany isEqualToString:kCarrierMovistarCode]) {
        staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_MOVISTAR_SUCCESS_OPERATION ofType:nil];
    } else if ([auxCompany isEqualToString:kCarrierClaroCode]) {
        staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CLARO_SUCCESS_OPERATION ofType:nil];
    }
    
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:kCarrierMovistarCode]) {
        operationType = PAYMENT_PUBLIC_SERVICE_SUCCESS_CELL_MOVISTAR;
    } else if ([auxCompany isEqualToString:kCarrierClaroCode]) {
        operationType = PAYMENT_PUBLIC_SERVICE_SUCCESS_CELL_CLARO;
    }
    
#else
    
    NSString *action = @"";
	NSString *auxCompany = [company lowercaseString];
	NSString *operation = @"";
    NSInteger operationType = NSNotFound;
    
    if ([auxCompany isEqualToString:kCarrierMovistarCode]) {
        operation = PAYMENT_MOVISTAR_SUCCESS_OPERATION;
        action = MOVISTAR_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_SUCCESS_CELL_MOVISTAR;
    } else if ([auxCompany isEqualToString:kCarrierClaroCode]) {
        operation = PAYMENT_CLARO_SUCCESS_OPERATION;
        action = CLARO_ACTION;
        operationType = PAYMENT_PUBLIC_SERVICE_SUCCESS_CELL_CLARO;
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, operation, action]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForPaymentSuccessSecondFactorKey:secondFactorKey];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

/**
 * Obtains the public service - Gas - payment inicialization
 *
 * @param company the company
 */
- (void)obtainPaymentPSGasInitializationForCompany:(NSString *)company
{
    PublicServiceResponse *operationResultResponse = [[PublicServiceResponse alloc] init];
	[operationResultResponse setPaymentOperationType:PTEPaymentPSGas];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = @"";
    
    NSString *auxCompany = [company lowercaseString];
    NSInteger operationType = NSNotFound;
    
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_GAS_INITIAL_OPERATION]];
    
#endif
    
    NSString *escapedParameters = @"";
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_PUBLIC_SERVICE_INITIAL_GAS_REPSOL] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
}

/*
 * Obtains the Continental card payment inicialization
 */
- (void)obtainPaymentCardContinentalInitialization {
	
    PaymentCCardInitialResponse *operationResultResponse = [[PaymentCCardInitialResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
	NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CARD_CONT_INITIAL_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_CARD_CONT_INITIAL_OPERATION]];
    
#endif
    
    NSString *escapedParameters = @"";
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_CARDS_CONT_INITIAL] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

/*
 * Obtains the Continental card payment own account data
 */
- (void)obtainPaymentCardContinentalOwnAccountDataForIssue:(NSString *)issue {
	
    PaymentDataResponse *operationResultResponse = [[PaymentDataResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CARD_OWN_DATA_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_CARD_OWN_DATA_OPERATION]];
	
#endif
	
    NSString *escapedParameters = [self returnEscapedParameterSequenceForIssue:issue];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_CARDS_CONT_OWN_DATA] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
	
    [downloadFile release];
    [operationResultResponse release];
}

/*
 * Obtains the Continental card payment own account confirmation
 */
- (void)obtainPaymentCardContinentalOwnAccountConfirmationForIssue:(NSString *)issue
                                                          currency:(NSString *)currency
                                                            amount:(NSString *)amount
                                                            email1:(NSString *)email1
                                                            email2:(NSString *)email2
                                                      phoneNumber1:(NSString *)phoneNumber1
                                                      phoneNumber2:(NSString *)phoneNumber2
                                                          carrier1:(NSString *)carrier1
                                                          carrier2:(NSString *)carrier2
                                                           message:(NSString *)message {
    
    
    PaymentConfirmationResponse *operationResultResponse = [[PaymentConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CARD_OWN_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_CARD_OWN_CONFIRMATION_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequencePaymentContCardConfirmationForIssue:issue
                                                                                                 currency:currency
                                                                                                   amount:amount
                                                                                                   email1:email1
                                                                                                   email2:email2
                                                                                             phoneNumber1:phoneNumber1
                                                                                             phoneNumber2:phoneNumber2
                                                                                                 carrier1:carrier1
                                                                                                 carrier2:carrier2
                                                                                                  message:message];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_CARDS_CONT_OWN_CONF] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
	
}

/*
 * Obtains the Continental card payment own account success
 */
- (void)obtainPaymentCardContinentalOwnAccountSuccess {
	
    PaymentSuccessResponse *operationResultResponse = [[PaymentSuccessResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CARD_OWN_SUCCESS_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_CARD_OWN_SUCCESS_OPERATION]];
    
#endif
    
    NSString *escapedParameters = @"";
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_CARDS_CONT_OWN_SUCCESS] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}


/*
 * Obtains the Continental card payment third account data
 */
- (void)obtainPaymentCardContinentalThirdAccountDataForIssue:(NSString *)issue {
	
    PaymentDataResponse *operationResultResponse = [[PaymentDataResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CARD_THIRDS_DATA_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_CARD_THIRDS_DATA_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForThirdCard:issue];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_CARDS_CONT_THIRD_DATA] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

/*
 * Obtains the Continental card payment third account confirmation
 */
- (void)obtainPaymentCardContinentalThirdAccountConfirmationForIssue:(NSString *)issue
                                                            currency:(NSString *)currency
                                                              amount:(NSString *)amount
                                                              email1:(NSString *)email1
                                                              email2:(NSString *)email2
                                                        phoneNumber1:(NSString *)phoneNumber1
                                                        phoneNumber2:(NSString *)phoneNumber2
                                                            carrier1:(NSString *)carrier1
                                                            carrier2:(NSString *)carrier2
                                                             message:(NSString *)message {
    
    PaymentConfirmationResponse *operationResultResponse = [[PaymentConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CARD_THIRDS_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_CARD_THIRDS_CONFIRMATION_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequencePaymentContCardConfirmationForIssue:issue
                                                                                                 currency:currency
                                                                                                   amount:amount
                                                                                                   email1:email1
                                                                                                   email2:email2
                                                                                             phoneNumber1:phoneNumber1
                                                                                             phoneNumber2:phoneNumber2
                                                                                                 carrier1:carrier1
                                                                                                 carrier2:carrier2
                                                                                                  message:message];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_CARDS_CONT_THIRD_CONF] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
	
}

/*
 * Obtains the Continental card payment third account success
 */
- (void)obtainPaymentCardContinentalThirdAccountSuccessForSecondFactorKey:(NSString *)secondFactorKey {
    
    PaymentSuccessResponse *operationResultResponse = [[PaymentSuccessResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CARD_THIRDS_SUCCESS_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_CARD_THIRDS_SUCCESS_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForPaymentSuccessSecondFactorKey:secondFactorKey];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_CARDS_CONT_THIRD_SUCCESS] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
	
}


/*
 * Obtains the Other banks card payment inicialization
 */
- (void)obtainPaymentCardOtherBanksInitialization {
	
    PaymentCCOtherBanksInitialResponse *operationResultResponse = [[PaymentCCOtherBanksInitialResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CARD_OTHER_BANK_INITIAL_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_CARD_OTHER_BANK_INITIAL_OPERATION]];
    
#endif
    
    NSString *escapedParameters = @"";
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_CARDS_OTHER_BANK_INITIAL] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

/*
 * Obtains the other banks card payment data
 */
- (void)obtainPaymentCardOtherBankDataForClass:(NSString *)aClass
                                      destBank:(NSString *)destBank
                                 accountNumber:(NSString *)accountNumber {
    
    PaymentDataResponse *operationResultResponse = [[PaymentDataResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CARD_OTHER_BANK_DATA_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_CARD_OTHER_BANK_DATA_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequencePaymentOtherBankDataForClass:aClass
                                                                                              bank:destBank
                                                                                           account:accountNumber];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_CARDS_OTHER_BANK_DATA] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
	
}

/*
 * Obtains the other banks card payment confirmation
 */
- (void)obtainPaymentCardCardOtherBankConfirmationForLocality:(NSString *)locality
                                                        issue:(NSString *)issue
                                                     currency:(NSString *)currency
                                                       amount:(NSString *)amount
                                                       email1:(NSString *)email1
                                                       email2:(NSString *)email2
                                                 phoneNumber1:(NSString *)phoneNumber1
                                                 phoneNumber2:(NSString *)phoneNumber2
                                                     carrier1:(NSString *)carrier1
                                                     carrier2:(NSString *)carrier2
                                                      message:(NSString *)message {
	
    //PaymentConfirmationResponse *operationResultResponse = [[PaymentConfirmationResponse alloc] init];
    PaymentTINOnlineResponse *operationResultResponse = [[PaymentTINOnlineResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CARD_OTHER_BANK_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_CARD_OTHER_BANK_CONFIRMATION_OPERATION]];
    //uri = [NSMutableString stringWithString:@"http://10.17.197.253/bbva/pagoTarjetaPaso2.xml"];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequencePaymentOtherBankCardConfirmationForIssue:issue
                                                                                                      locality:locality
                                                                                                      currency:currency
                                                                                                        amount:amount
                                                                                                        email1:email1
                                                                                                        email2:email2
                                                                                                  phoneNumber1:phoneNumber1
                                                                                                  phoneNumber2:phoneNumber2
                                                                                                      carrier1:carrier1
                                                                                                      carrier2:carrier2
                                                                                                       message:message];
	
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_CARDS_OTHER_BANK_TIN] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
	
}

/*
 * Obtains the other banks card payment confirmation
 */
- (void)obtainFOPaymentCardCardOtherBankConfirmationForIndicatorOP:(NSString*)indicator
                                                           Service:(NSString*)service
                                                       Description:(NSString*)description
                                                        CardNumber:(NSString*)cardNumber
                                                         ClassCard:(NSString*)classCard
                                                   DestinationBank:(NSString*)destinationBank
                                                          Locality:(NSString *)locality
                                                             issue:(NSString *)issue
                                                          currency:(NSString *)currency
                                                            amount:(NSString *)amount
                                                            email1:(NSString *)email1
                                                            email2:(NSString *)email2
                                                      phoneNumber1:(NSString *)phoneNumber1
                                                      phoneNumber2:(NSString *)phoneNumber2
                                                          carrier1:(NSString *)carrier1
                                                          carrier2:(NSString *)carrier2
                                                           message:(NSString *)message {
    
    //PaymentConfirmationResponse *operationResultResponse = [[PaymentConfirmationResponse alloc] init];
    PaymentTINOnlineResponse *operationResultResponse = [[PaymentTINOnlineResponse alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CARD_OTHER_BANK_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_CARD_OTHER_BANK_CONFIRMATION_OPERATION]];
    //uri = [NSMutableString stringWithString:@"http://192.168.0.3/bbva/pagoTarjetaPaso2.xml"];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequencePaymentOtherBankCardConfirmationIndicatorOP:indicator
                                                                                                          Service:service
                                                                                                      Description:description
                                                                                                       CardNumber:cardNumber
                                                                                                        ClassCard:classCard
                                                                                                  destinationBank:destinationBank
                                                                                                         locality:locality
                                                                                                            Issue:issue
                                                                                                         currency:currency
                                                                                                           amount:amount
                                                                                                           email1:email1
                                                                                                           email2:email2
                                                                                                     phoneNumber1:phoneNumber1
                                                                                                     phoneNumber2:phoneNumber2
                                                                                                         carrier1:carrier1
                                                                                                         carrier2:carrier2
                                                                                                          message:message];
    
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_CARDS_OTHER_BANK_TIN] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}


/*
 * Obtains the other banks card payment confirmation
 */
- (void)obtainPaymentCardCardOtherBankTINConfirmationForFlag:(NSString *)flag
                                                        beneficiary:(NSString *)beneficiary
{
    PaymentConfirmationResponse *operationResultResponse = [[PaymentConfirmationResponse alloc] init];
    
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CARD_OTHER_BANK_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    NSString *action = @"";
    if ([flag isEqualToString:@"PH"]) {
        action = @"seleccionarPorHorarios";
    }else{
        action = @"seleccionarInmediata";
    }
    
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_CARD_OTHER_BANK_TIN_CONFIRMATION_OPERATION,action]];
    //uri = [NSMutableString stringWithString:@"http://192.168.0.3/bbva/pagoTarjetaPaso2.xml"];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequencePaymentOtherBankCardTINConfirmationWithBeneficiary:beneficiary];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_CARDS_OTHER_BANK_CONF] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}

/*
 * Obtains the other banks card payment success
 */
- (void)obtainPaymentCardOtherBankSuccessForSecondFactorKey:(NSString *)secondFactorKey {
	
    PaymentSuccessResponse *operationResultResponse = [[PaymentSuccessResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_CARD_OTHER_BANK_SUCCESS_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_CARD_OTHER_BANK_SUCCESS_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForPaymentSuccessSecondFactorKey:secondFactorKey];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_CARDS_OTHER_BANK_SUCCESS] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}

/*
 * Obtains the recharge payment data
 */
- (void)obtainPaymentRechargeDataForCompany:(NSString *)company {
	
	PaymentRechargeInitialResponse *operationResultResponse = [[PaymentRechargeInitialResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = @"";
    
    if ([[company lowercaseString] isEqualToString:kCarrierMovistarCode]) {
		
        staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_RECHARGE_OPERATION ofType:nil];
		
    } else if ([[company lowercaseString] isEqualToString:kCarrierClaroCode]) {
		
        staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_RECHARGE_OPERATION ofType:nil];
		
    }
    
    
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
    NSString *action = @"";
	
	NSString *operation = @"";
    
    if ([[company lowercaseString] isEqualToString:kCarrierMovistarCode]) {
		
        operation = PAYMENT_RECHARGE_OPERATION;
        action = MOVISTAR_ACTION;
		
    } else if ([[company lowercaseString] isEqualToString:kCarrierClaroCode]) {
		
		operation = PAYMENT_RECHARGE_OPERATION;
        action = CLARO_ACTION;
		
    }
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, operation]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParamenteSequenceForPaymentRechargeCompany:company];
	
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_RECHARGE_DATA] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
	
}

/*
 * Obtains the recharge payment data
 */
- (void)obtainPaymentRechargeConfirmationForPhoneNumber:(NSString *)phoneNumber
                                                  issue:(NSString *)issue
                                                 amount:(NSString *)amount
                                                 email1:(NSString *)email1
                                                 email2:(NSString *)email2
                                           phoneNumber1:(NSString *)phoneNumber1
                                           phoneNumber2:(NSString *)phoneNumber2
                                               carrier1:(NSString *)carrier1
                                               carrier2:(NSString *)carrier2
                                                message:(NSString *)message {
	
    PaymentConfirmationResponse *operationResultResponse = [[PaymentConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_RECHARGE_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, PAYMENT_RECHARGE_CONFIRMATION_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParamenteSequenceForPaymentRechargeConfirmationNumber:phoneNumber
																									 issue:issue
																									amount:amount
																									email1:email1
																									email2:email2
																							  phoneNumber1:phoneNumber1
																							  phoneNumber2:phoneNumber2
																								  carrier1:carrier1
																								  carrier2:carrier2
																								   message:message];
	
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:PAYMENT_RECHARGE_CONF] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        operationInfo.silent = YES;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
	
}


/*
 * Obtains the recharge payment success of Movistar or Claro
 */
- (void)obtainPaymentRechargeSuccessForSecondFactorKey:(NSString *)secondFactorKey
                                               carrier:(NSString *)carrier {
	
	PaymentSuccessResponse *operationResultResponse = [[PaymentSuccessResponse alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
	
    NSString *staticFile;
    NSInteger operationType = NSNotFound;
    
	if ([[carrier lowercaseString] isEqualToString:kCarrierMovistarCode]) {
		
		staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_RECHARGE_MOVISTAR_OPERATION ofType:nil];
		operationType = PAYMENT_RECHARGE_MOVISTAR_SUCCESS;
        
	} else {
		
		staticFile = [[NSBundle mainBundle] pathForResource:PAYMENT_RECHARGE_CLARO_OPERATION ofType:nil];
        operationType = PAYMENT_RECHARGE_CLARO_SUCCESS;
        
	}
	
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
    NSInteger operationType;
	NSString *action;
    
	if ([[carrier lowercaseString] isEqualToString:kCarrierMovistarCode] ) {
        operationType = PAYMENT_RECHARGE_MOVISTAR_SUCCESS;
		action = PAYMENT_RECHARGE_MOVISTAR_OPERATION;
	} else {
        operationType = PAYMENT_RECHARGE_CLARO_SUCCESS;
		action = PAYMENT_RECHARGE_CLARO_OPERATION;
	}
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, action]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForPaymentSuccessSecondFactorKey:secondFactorKey];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:operationType] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
	
}

#pragma mark Frequent Operation Reactive Methods

-(void)obtainCashMobileFrequentOperationReactiveStepOne
{
    FORCashMobileStepOneResponse *forCashMobileStepOneResponse = [[FORCashMobileStepOneResponse alloc] init];
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_CASH_MOBILE_STEP_ONE]];
    
    
    NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:forCashMobileStepOneResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId: downloadFile operationType: FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_ONE] autorelease];
    
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = forCashMobileStepOneResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo
                              forKey:downloadFile];
        [downloadFile release];
        [forCashMobileStepOneResponse release];
    }
    
}

-(void)obtainTranferToGiftCardFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                      andbeneficiaryAccount:(NSString *)beneficiaryAccount
                                                                andCurrency:(NSString *)destinationBank
                                                           andOriginAccount:(NSString *)originAccount
                                                                  andAmount:(NSString *)amount
{
    FORRechargeGiftCardStepOneResponse *forRechargeGiftCardStepOneResponse = [[FORRechargeGiftCardStepOneResponse alloc] retain];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_GIFT_CARD_STEP_ONE]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForFrequentOperationGiftCardWithOperation:operation andbeneficiaryAccount:beneficiaryAccount andCurrency:destinationBank andOriginAccount:originAccount andAmount:amount];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:forRechargeGiftCardStepOneResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_REACTIVE_GIFT_CARD_STEP_ONE] autorelease];
    if(operationInfo != nil)
    {
        operationInfo.xmlParserDelegate = forRechargeGiftCardStepOneResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [forRechargeGiftCardStepOneResponse release];
    }
    
    
    
}

-(void)obtainTranferToOtherBankFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                       andbeneficiaryAccount:(NSString *)beneficiaryAccount
                                                          andDestinationBank:(NSString *)destinationBank
                                                          andBeneficiaryName:(NSString *)beneficiaryName
                                                            andOriginAccount:(NSString *)originAccount
                                                                   andAmount:(NSString *)amount
                                                                 andCurrency:(NSString *)currency
                                                              andDescription:(NSString *)description
                                                           andDocumentNumber:(NSString *)documentNumber
                                                             andDocumentCode:(NSString *)documentCode
                                                                      andITF:(BOOL)itf
{
    FOROtherBankTransferStepOneResponse *forOtherBankTransferStepOneResponse = [[FOROtherBankTransferStepOneResponse alloc] retain];
    [forOtherBankTransferStepOneResponse setDestionationBank: destinationBank];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_OTHER_BANK_STEP_ONE]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForFrequentOperationOtherBankWithOperation:operation andbeneficiaryAccount:beneficiaryAccount andDestinationBank:destinationBank andBeneficiaryName:beneficiaryName andOriginAccount:originAccount andAmount:amount andCurrency:currency andDescription:description andDocumentNumber:documentNumber andDocumentCode:documentCode andITF:itf];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:forOtherBankTransferStepOneResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_ONE] autorelease];
    if(operationInfo != nil)
    {
        operationInfo.xmlParserDelegate = forOtherBankTransferStepOneResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [forOtherBankTransferStepOneResponse release];
    }
    
    
    
}


-(void)obtainTranferToThirdBankFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                       andbeneficiaryAccount:(NSString *)beneficiaryAccount
                                                          andBeneficiaryName:(NSString *)beneficiaryName
                                                            andOriginAccount:(NSString *)originAccount
                                                                   andAmount:(NSString *)amount
                                                      andBeneficiaryCurrency:(NSString *)beneficiaryCurrency
                                                           andOriginCurrency:(NSString *)originCurrency

{
    FORThirdAccountTransferStepOneResponse *forThirdAccountTransferStepOneResponse = [[FORThirdAccountTransferStepOneResponse alloc] retain];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_THIRD_ACCOUNT_STEP_ONE]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForFrequentOperationThirdAccountWithOperation:operation andbeneficiaryAccount:beneficiaryAccount andBeneficiaryName:beneficiaryName andOriginAccount:originAccount andAmount:amount andBeneficiaryCurrency:beneficiaryCurrency andOriginCurrency:originCurrency];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:forThirdAccountTransferStepOneResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUHT_STEP_ONE] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = forThirdAccountTransferStepOneResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [forThirdAccountTransferStepOneResponse release];
    }
    
    
    
}

-(void)obtainPaymentToThirdBankFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                       andDestinationAccount:(NSString *)destinationAccount
                                                   andDestinationAccountType:(NSString *)destinationAccountType
                                                              andBeneficiary:(NSString *)beneficiary
                                                                     andFlag:(NSString *)flag
                                                                 andCurrency:(NSString *)currency
                                                            andOriginAccount:(NSString *)originAccount
                                                                   andAmount:(NSString *)amount
{
    
    FORThirdCardPaymentStepOneResponse *forThirdCardPaymentStepOneResponse = [[FORThirdCardPaymentStepOneResponse alloc] retain];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_THIRD_CARD_PAYMENT_STEP_ONE]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForFrequentOperationThirdAccountPaymentWithOperation:operation andDestinationAccount:destinationAccount andDestinationAccountType:destinationAccountType andBeneficiary:beneficiary andFlag:flag andCurrency:currency andOriginAccount:originAccount andAmount:amount];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:forThirdCardPaymentStepOneResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_ONE] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = forThirdCardPaymentStepOneResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [forThirdCardPaymentStepOneResponse release];
    }
    
}

-(void)obtainPaymentToOtherBankFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                       andDestinationAccount:(NSString *)destinationAccount
                                                   andDestinationAccountType:(NSString *)destinationAccountType
                                                      andNameDestinationBank:(NSString *)nameDestinationBank
                                                                 andBankCode:(NSString *)bankCode
                                                             andLocalityName:(NSString *)localityName
                                                             andLocalityCode:(NSString *)localityCode
                                                              andBeneficiary:(NSString *)beneficiary
                                                                 andCurrency:(NSString *)currency
                                                            andOriginAccount:(NSString *)originAccount
                                                                   andAmount:(NSString *)amount
{
    
    FORPaymentCOtherBankStepOneResponse *forPaymentCOtherBankStepOneResponse = [[FORPaymentCOtherBankStepOneResponse alloc] retain];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_OTHER_BANK_PAYMENT_STEP_ONE]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForFrequentOperationOtherBankPaymentWithOperation:operation andDestinationAccount:destinationAccount andDestinationAccountType:destinationAccountType andNameDestinationBank:nameDestinationBank andBankCode:bankCode andLocalityName:localityName andLocalityCode:localityCode andBeneficiary:beneficiary andCurrency:currency andOriginAccount:originAccount andAmount:amount];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:forPaymentCOtherBankStepOneResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_ONE] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = forPaymentCOtherBankStepOneResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [forPaymentCOtherBankStepOneResponse release];
    }
    
}

-(void)obtainPaymentToInstitutionAndCompaniesFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                                               andCodMatri:(NSString *)codMatri
                                                                                 andsClase:(NSString *)clase
                                                                               andsEntidad:(NSString *)entidad
                                                                            andsCodEntidad:(NSString *)codEntidad
                                                                                andArreglo:(NSString *)arreglo
                                                                           andsDescripcion:(NSString *)descripcion
                                                                            andCuentaCargo:(NSString *)cuentaCargo
                                                                             andHolderName:(NSString *)sNombre
{
    
    FORInstitutionPaymentStepOneResponse *forInstitutionPaymentStepOneResponse = [[FORInstitutionPaymentStepOneResponse alloc] retain];
    
    [forInstitutionPaymentStepOneResponse setCompanyCode: codEntidad];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_INSTITUTION_COMPANIES_STEP_ONE]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForFrequentOperationInstitutionAndCompaniesWithOperation:operation                                                                                                                   andCodMatri:codMatri andsClase:clase andsEntidad:entidad andsCodEntidad:codEntidad andArreglo:arreglo andsDescripcion:descripcion andCuentaCargo:cuentaCargo andHolderNmae:sNombre];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:forInstitutionPaymentStepOneResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_ONE] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = forInstitutionPaymentStepOneResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [forInstitutionPaymentStepOneResponse release];
    }
    
}
#pragma mark servicios sin probar

-(void)obtainRechargeMovistarCellPhoneFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                                         andCompany:(NSString *)company
                                                                     andServiceType:(NSString *)serviceType
                                                                 andCellPhoneNumber:(NSString *)cellPhoneNumber
                                                                         andAccount:(NSString *)account
                                                                          andAmount:(NSString *)amount
{
    
    FORRechargeCellphoneStepOneResponse *forRechargeCellphoneStepOneResponse = [[FORRechargeCellphoneStepOneResponse alloc] retain];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_RECHARGE_MOVISTAR_STEP_ONE]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForRechargeMovistarCellPhoneFrequentOperationWithOperation:operation                                                                                                                      andCompany:company andServiceType:serviceType andCellPhoneNumber:cellPhoneNumber andAccount:account andAmount:amount];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:forRechargeCellphoneStepOneResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_ONE] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = forRechargeCellphoneStepOneResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [forRechargeCellphoneStepOneResponse release];
    }
    
}

-(void)obtainRechargeClaroCellPhoneFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                                     andServiceType:(NSString *)serviceType
                                                                         andAccount:(NSString *)account
                                                                 andCellPhoneNumber:(NSString *)cellPhoneNumber
{
    
    FORRechargeCellphoneStepOneResponse *forRechargeCellphoneStepOneResponse = [[FORRechargeCellphoneStepOneResponse alloc] retain];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_RECHARGE_CLARO_STEP_ONE]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForRechargeClaroCellPhoneFrequentOperationWithOperation:operation andServiceType:serviceType andAccount:account andCellPhoneNumber:cellPhoneNumber];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:forRechargeCellphoneStepOneResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_ONE] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = forRechargeCellphoneStepOneResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [forRechargeCellphoneStepOneResponse release];
    }
    
}

-(void)obtainServiceClaroCellPhoneFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                                 andServiceType:(NSString *)serviceType
                                                             andCellPhoneHolder:(NSString *)cellPhoneHolder
                                                                     andAccount:(NSString *)account
                                                                   andCellPhone:(NSString *)cellPhone
{
    
    FORServicePaymentStepOneResponse *forServicePaymentStepOneResponse = [[FORServicePaymentStepOneResponse alloc] retain];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_SERVICE_CLARO_STEP_ONE]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForServiceClaroCellPhoneFrequentOperationWithOperation:operation andServiceType:serviceType andCellPhoneHolder:cellPhoneHolder andAccount:account andCellPhone:cellPhone];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:forServicePaymentStepOneResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_ONE] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = forServicePaymentStepOneResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [forServicePaymentStepOneResponse release];
    }
    
}

-(void)obtainServiceMovistarCellPhoneFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                                        andCompany:(NSString *)company
                                                                    andServiceType:(NSString *)serviceType
                                                                andCellPhoneNumber:(NSString *)cellPhoneNumber
                                                                         andHolder:(NSString *)holder
                                                                        andAccount:(NSString *)account

{
    
    FORServicePaymentStepOneResponse *forServicePaymentStepOneResponse = [[FORServicePaymentStepOneResponse alloc] retain];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_SERVICE_MOVISTAR_STEP_ONE]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForServiceMovistarCellPhoneFrequentOperationWithOperation:operation andCompany:company andServiceType:serviceType andCellPhoneNumber:cellPhoneNumber andHolder:holder andAccount:account];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:forServicePaymentStepOneResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_ONE] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = forServicePaymentStepOneResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [forServicePaymentStepOneResponse release];
    }
    
}

-(void)obtainServiceWaterFrequentOperationReactiveStepOneWithOperation:(NSString *)operation
                                                            andCompany:(NSString *)company
                                                       andSupplyNumber:(NSString *)supplyNumber
                                                             andHolder:(NSString *)holder
                                                        andServiceType:(NSString *)serviceType
                                                            andAccount:(NSString *)account
                                                             andAmount:(NSString *)amount
                                                           andCurrency:(NSString *)currency
{
    
    FORServicePaymentStepOneResponse *forServicePaymentStepOneResponse = [[FORServicePaymentStepOneResponse alloc] retain];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_SERVICE_WATER_STEP_ONE]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForServiceWaterFrequentOperationWithOperation:operation andCompany:company andSupplyNumber:supplyNumber andHolder:holder andServiceType:serviceType andAccount:account andAmount:amount andCurrency:currency];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:forServicePaymentStepOneResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_ONE] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = forServicePaymentStepOneResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [forServicePaymentStepOneResponse release];
    }
    
}





#pragma mark Servicios del paso 2 y 3

-(void)obtainFrequentOperationReactiveStepTwoWithOperation:(NSString *)operation
                                                andChannel:(NSString *)channel
                                             andShortAlias:(NSString *)shortAlias
                                             andLargeAlias:(NSString *)largeAlias
                                                    andDay:(NSString *)day
                                                andCellSMS:(NSString *)cellSMS
                                            andCellSMSCode:(NSString *)cellSMSCode
                                                  andEmail:(NSString *)email
                                               anEmailCode:(NSString *)emailCode

{
    
    id forPaymentCStepTwoResponse;
    NSMutableString *uri;
    
  
    
    if([[operation lowercaseString] isEqualToString:[@"Recarga de Tarjeta Regalo" lowercaseString]])
    {
        forPaymentCStepTwoResponse = [[FORRechargeGiftCardStepTwoResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_GIFT_CARD_STEP_TWO]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_GIFT_CARD_STEP_TWO]  forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO];
    }else if([[operation lowercaseString] containsString:[@"Pago de tarjeta de otros bancos" lowercaseString]])
    {
        forPaymentCStepTwoResponse = [[FORPaymentCOtherBankStepTwoResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_OTHER_BANK_PAYMENT_STEP_TWO]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_TWO]  forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO];
        
    } else if([[operation lowercaseString] isEqualToString:[@"Pago de Tarjeta de terceros" lowercaseString]])
    {
        forPaymentCStepTwoResponse = [[FORThirdCardPaymentStepTwoResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_THIRD_CARD_PAYMENT_STEP_TWO]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_TWO]  forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO];
    }else if([[operation lowercaseString] containsString:[@"Transferencia a cuentas de otros bancos" lowercaseString]])
    {
        forPaymentCStepTwoResponse = [[FOROtherBankTransferStepTwoResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_OTHER_BANK_STEP_TWO]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_TWO]  forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO];
    } else if([[operation lowercaseString] isEqualToString:[@"Transferencia a cuentas de terceros" lowercaseString]])
    {
        forPaymentCStepTwoResponse = [[FORThirdAccountTransferStepTwoResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_THIRD_ACCOUNT_STEP_TWO]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUHT_STEP_TWO]  forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO];
    } else if([[operation lowercaseString] isEqualToString:[@"Efectivo Móvil" lowercaseString]])
    {
        forPaymentCStepTwoResponse = [[FORCashMobileStepTwoResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_CASH_MOBILE_STEP_TWO]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_TWO]  forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO];
    } else if([[operation lowercaseString] isEqualToString:[@"Pago de Instituciones y Empresas" lowercaseString]] || [[operation lowercaseString] isEqualToString:[@"Pago de SERV" lowercaseString]] || [[operation lowercaseString] isEqualToString:[@"Instituciones y Empresas" lowercaseString]] || [[operation lowercaseString] isEqualToString:[@"Pago de servicios" lowercaseString]] || [[operation lowercaseString] isEqualToString:[@"Pago de servicios eléctricos" lowercaseString]])
    {
        forPaymentCStepTwoResponse = [[FORInstitutionPaymentStepTwoResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_INSTITUTION_COMPANIES_STEP_TWO]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_TWO]  forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO];
        
    } else if([[operation lowercaseString] isEqualToString:[@"Pago de servicios de agua" lowercaseString]])
    {
        forPaymentCStepTwoResponse = [[FORServicePaymentStepTwoResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_SERVICE_WATER_STEP_TWO]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_TWO]   forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO];
        
    } else if([[operation lowercaseString] isEqualToString:[@"Pago de telefonía móvil Claro" lowercaseString]])
    {
        forPaymentCStepTwoResponse = [[FORServicePaymentStepTwoResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_SERVICE_CLARO_STEP_TWO]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_TWO]  forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO];
        
        
    } else if([[operation lowercaseString] isEqualToString:[@"Pago de telefonía móvil" lowercaseString]] || [[operation lowercaseString] isEqualToString:[@"Pago de telefonía fija" lowercaseString]])
    {
        forPaymentCStepTwoResponse = [[FORServicePaymentStepTwoResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_SERVICE_MOVISTAR_STEP_TWO]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_TWO]  forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO];
        
        
    } else if([[operation lowercaseString] isEqualToString:[@"Recarga de celular - Claro" lowercaseString]])
    {
        forPaymentCStepTwoResponse = [[FORRechargeCellphoneStepTwoResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_RECHARGE_CLARO_STEP_TWO]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_TWO]  forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO];
        
    } else if([[operation lowercaseString] isEqualToString:[@"Recarga de Teléfonos Móviles" lowercaseString]])
    {
        forPaymentCStepTwoResponse = [[FORRechargeCellphoneStepTwoResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_RECHARGE_MOVISTAR_STEP_TWO]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_TWO]  forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO];
        
        
    } else{
        [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORStepTwo
                                                            object:nil];
        return;
    }

    
    
    
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForFrequentOperationTwoStepWithChannel:channel andShortAlias:shortAlias andLargeAlias:largeAlias andDay:day andCellSMS:cellSMS andCellSMSCode:cellSMSCode andEmail:email anEmailCode:emailCode];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:forPaymentCStepTwoResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_REACTIVE_STEP_TWO] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = forPaymentCStepTwoResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [forPaymentCStepTwoResponse release];
    }
    
}

- (void)obtainFrequentOperationReactiveStepThreeFromSecondKeyFactor:(NSString *)secondKeyFactor
                                                       andOperation:(NSString *)operation
{
    
    id forStepThreeResponse;
    NSMutableString *uri;
    
    
    if([[operation lowercaseString] isEqualToString:[@"Recarga de Tarjeta Regalo" lowercaseString]])
    {
        forStepThreeResponse = [[FORRechargeGiftCardStepThreeResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_GIFT_CARD_STEP_THREE]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_GIFT_CARD_STEP_THREE] forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE];
        
        
    }else if([[operation lowercaseString] containsString:[@"Pago de tarjeta de otros bancos" lowercaseString]])
    {
        forStepThreeResponse = [[FORPaymentCOtherBankStepThreeResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_OTHER_BANK_PAYMENT_STEP_THREE]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_THREE] forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE];
        
    } else if([[operation lowercaseString] isEqualToString:[@"Pago de Tarjeta de terceros" lowercaseString]])
    {
        forStepThreeResponse = [[FORThirdCardPaymentStepThreeResponde alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_THIRD_CARD_PAYMENT_STEP_THREE]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_THREE] forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE];
        
    }else if([[operation lowercaseString] containsString:[@"Transferencia a cuentas de otros bancos" lowercaseString]])
    {
        forStepThreeResponse = [[FOROtherBankTransferStepThreeResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_OTHER_BANK_STEP_THREE]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_THREE] forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE];
        
    } else if([[operation lowercaseString] isEqualToString:[@"Transferencia a cuentas de terceros" lowercaseString]])
    {
        forStepThreeResponse = [[FORThirdAccountTransferStepThreeResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_THIRD_ACCOUNT_STEP_THREE]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUHT_STEP_THREE] forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE];
        
    } else if([[operation lowercaseString] isEqualToString:[@"Efectivo Móvil" lowercaseString]])
    {
        forStepThreeResponse = [[FORCashMobileStepThreeResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_CASH_MOBILE_STEP_THREE]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_THREE] forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE];
        
    } else if([[operation lowercaseString] isEqualToString:[@"Pago de Instituciones y Empresas" lowercaseString]] || [[operation lowercaseString] isEqualToString:[@"Pago de SERV" lowercaseString]] || [[operation lowercaseString] isEqualToString:[@"Pago de Servicios" lowercaseString]] || [[operation lowercaseString] isEqualToString:[@"Instituciones y Empresas" lowercaseString]] || [[operation lowercaseString] isEqualToString:[@"Pago de servicios eléctricos" lowercaseString]])
    {
        forStepThreeResponse = [[FORInstitutionPaymentStepThreeResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_INSTITUTION_COMPANIES_STEP_THREE]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_THREE] forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE];
        
    } else if([[operation lowercaseString] isEqualToString:[@"Pago de servicios de agua" lowercaseString]])
    {
        forStepThreeResponse = [[FORServicePaymentStepThreeResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_SERVICE_WATER_STEP_THREE]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_THREE] forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE];
        
    } else if([[operation lowercaseString] isEqualToString:[@"Pago de telefonía móvil Claro" lowercaseString]])
    {
        forStepThreeResponse = [[FORServicePaymentStepThreeResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_SERVICE_CLARO_STEP_THREE]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_THREE] forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE];
        
    } else if([[operation lowercaseString] isEqualToString:[@"Pago de telefonía móvil" lowercaseString]]|| [[operation lowercaseString] isEqualToString:[@"Pago de telefonía fija" lowercaseString]])
    {
        forStepThreeResponse = [[FORServicePaymentStepThreeResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_SERVICE_MOVISTAR_STEP_THREE]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_THREE] forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE];
        
    } else if([[operation lowercaseString] isEqualToString:[@"Recarga de celular - Claro" lowercaseString]])
    {
        forStepThreeResponse = [[FORRechargeCellphoneStepThreeResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_RECHARGE_CLARO_STEP_THREE]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_THREE] forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE];
        
    } else if([[operation lowercaseString] isEqualToString:[@"Recarga de Teléfonos Móviles" lowercaseString]])
    {
        forStepThreeResponse = [[FORRechargeCellphoneStepThreeResponse alloc] retain];
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FOR_RECHARGE_MOVISTAR_STEP_THREE]];
        [[NSUserDefaults standardUserDefaults] setObject: [NSString stringWithFormat:@"%d",FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_THREE] forKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE];
        
    }
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForFrequentOperationStepThreeWithSecondFactor:secondKeyFactor];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:forStepThreeResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_REACTIVE_STEP_THREE] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = forStepThreeResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [forStepThreeResponse release];
    }

    
}

#pragma mark SafetyPay Methods

- (void) obtainSafetyPayStatusInfo {
    
    SafetyPayStatusResponse *safetyPayStatusResponse = [[SafetyPayStatusResponse alloc] init];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, SAFETYPAY_STATUS_OPERATION]];
    
    NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:safetyPayStatusResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId: downloadFile operationType: RETRIEVE_SAFETYPAY_STATUS_INFO] autorelease];
    
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = safetyPayStatusResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo
                              forKey:downloadFile];
        [downloadFile release];
        [safetyPayStatusResponse release];
    }
}

- (void) obtainSafetyPayTransactionInfoByTransactionNumber:(NSString *)transactionNumber AndAmount:(NSString *)amount {
    SafetyPayTransactionInfoResponse *safetyPayTransactionInfoResponse = [[SafetyPayTransactionInfoResponse alloc] retain];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, SAFETYPAY_TRANSACTION_INFO_OPERATION]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForSafetyPayTransactionInfoWithTransactionNumber : transactionNumber AndAmount:amount];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:safetyPayTransactionInfoResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:RETRIEVE_SAFETYPAY_TRANSACTION_INFO] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = safetyPayTransactionInfoResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [safetyPayTransactionInfoResponse release];
    }
}

- (void) obtainSafetyPayPaymentDetailsByAccountNumber:(NSString *) accountNumber cellphoneNumber1:(NSString *)cellphoneNumber1 cellphoneNumber2:(NSString *)cellphoneNumber2 carrier1:(NSString *)carrier1 carrier2:(NSString *)carrier2 email1:(NSString *)email1 email2:(NSString *)email2 message:(NSString *)message {
    
    SafetyPayDetailsResponse *safetyPayDetailsResponse = [[SafetyPayDetailsResponse alloc] retain];
    NSMutableString *uri = [NSMutableString stringWithString: [NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, SAFETYPAY_PAYMENT_DETAILS]];
    NSString *escapedParameters = [self returnEscapedParameterSequenceForSafetyPayPaymentInfoWithAccountNumber: accountNumber  email1: email1 email2:email2 phoneNumber1:cellphoneNumber1 phoneNumber2:cellphoneNumber2 carrier1:carrier1 carrier2:carrier2 message:message];
    NSData *parametersData = [escapedParameters dataUsingEncoding: NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:safetyPayDetailsResponse forListener: self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:RETRIEVE_SAFETYPAY_PAYMENT_DETAILS] autorelease];
    
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = safetyPayDetailsResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [safetyPayDetailsResponse release];
    }
}


- (void)safetyPayResultFromSecondKeyFactor:(NSString *)secondKeyFactor {
    
	SafetyPayConfirmationResponse *operationResultResponse = [[SafetyPayConfirmationResponse alloc] init];
    NSMutableString *uri = nil;
    [Session getInstance].globalPositionRequestServerData = YES;
	
    
	uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, SAFETYPAY_CONFIRM_PAYMENT]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceSafetyPayResultFromSecondKeyFactor:secondKeyFactor];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId: downloadFile operationType:RETRIEVE_SAFETYPAY_PAYMENT_CONFIRM] autorelease];
    
	if (operationInfo != nil) {
        
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
        
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

#pragma mark Frequent Operation

/**
 * Obtain the Initial list of frequent operation
 */
- (void) obtainFrequentInformationInitialListInfo:(NSString *)nextPage{
    
    FOPGeneralMenuStepOneResponse *operationResultResponse = [[FOPGeneralMenuStepOneResponse alloc] init];
    NSMutableString *uri = nil;
	
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:FREQUENT_OPERATIONS_INITIAL_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
    if ([@"-1" isEqualToString:nextPage]) {
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FREQUENT_OPERATIONS_INITIAL_OPERATION]];
    }else{
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FREQUENT_OPERATIONS_PAGIN_OPERATION]];
    }
    
#endif
    
    NSString *escapedParameters = @"";
    
    if (![@"-1" isEqualToString:nextPage]) {
        escapedParameters = [self returnEscapedParameterFrequentOperationInitial:nextPage];
	}
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:([@"-1" isEqualToString:nextPage])?FREQUENT_OPERATION_OBTAIN_INITIAL_LIST:FREQUENT_OPERATION_PAGINATION_LIST] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = operationResultResponse;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[operationResultResponse release];
    
}

/**
 * Obtain the execute of frequent operation
 *
 * @param codService Unique code service
 * @param afilitionNum The afiliation number
 * @param asociationNick the asociation nick for the operation
 * @param afilitationType the afiliation type
 */- (void) obtainFrequentOperationExecuteInfoFromCodService:(NSString *)codService
                                                AfilitionNum:(NSString *)afilitionNum
                                              AsociationNick:(NSString *)asociationNick
                                             AfilitationType:(NSString *)afilitationType{
     
     id operationResultResponse = nil;
     
     if ([FO_TRANS_TO_OTHER_BANK_CODE isEqualToString:codService]) {
         operationResultResponse = [[FOEOtherBankTransferStepOneResponse alloc] init];
     } else if ([FO_TRANS_TO_THIRD_CODE isEqualToString:codService]){
         operationResultResponse = [[FOEThirdAccountTransferStepOneResponse alloc] init];
     } else if ([FO_TRANS_TO_THIRD_CODE isEqualToString:codService]){
         operationResultResponse = [[FOEThirdAccountTransferStepOneResponse alloc] init];
     } else if ([FO_PAY_AGREEMENT isEqualToString:codService]){
         operationResultResponse = [[FOEInstitutionPaymentStepOneResponse alloc] init];
     } else if ([FO_PAY_CLARO_SERVICE isEqualToString:codService] ||
                [FO_PAY_TELE_SERVICE isEqualToString:codService] ||
                [FO_PAY_WATER_SERVICE isEqualToString:codService]){
         operationResultResponse = [[FOEServicePaymentStepOneResponse alloc] init];
     } else if ([FO_PAY_OTHER_BANK_CARD isEqualToString:codService]){
         operationResultResponse = [[FOEPaymentCOtherBankStepOneResponse alloc] init];
     } else if ([FO_PAY_THIRD_CARD isEqualToString:codService]){
         operationResultResponse = [[FOEThirdCardPaymentStepOneResponse alloc] init];
     } else if ([FO_RECHARGE_CELL_PHONE isEqualToString:codService]){
         operationResultResponse = [[FOERechargeCellphoneStepOneResponse alloc] init];
     } else if ([FO_RECHARGE_GIFT_CARD isEqualToString:codService]){
         operationResultResponse = [[FOERechargeGiftCardStepOneResponse alloc] init];
     } else if ([FO_CASH_MOBILE isEqualToString:codService]){
         operationResultResponse = [[FOECashMobileStepOneResponse alloc] init];
     }
     
     NSMutableString *uri = nil;
     
#if defined(SIMULATE_HTTP_CONNECTION)
     
     NSString *staticFile = [[NSBundle mainBundle] pathForResource:FREQUENT_OPERATIONS_INITIAL_OPERATION ofType:nil];
     NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
     uri = [NSMutableString stringWithString:[staticURL absoluteString]];
     
#else
     
     uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FREQUENT_OPERATIONS_EXECUTE_OPERATION]];
     
#endif
     
     NSString *escapedParameters = [self returnEscapedParameterFrequentOperationExecuteCodService:codService
                                                                                     AfilitionNum:afilitionNum
                                                                                   AsociationNick:asociationNick
                                                                                  AfilitationType:afilitationType];
     
     NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
     NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
     
     OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FREQUENT_OPERATION_OBTAIN_EXECUTE_INFO] autorelease];
     if (operationInfo != nil) {
         operationInfo.xmlParserDelegate = operationResultResponse;
         operationInfo.operationParams = nil;
         [activeOperations_ setObject: operationInfo forKey: downloadFile];
     }
     
     [downloadFile release];
     [operationResultResponse release];
 }
#pragma mark Register third account operation
/**
 * delete a Third Account
 *
 * @param accountNumber account number
 * @param accountType type of account

 */
- (void) obtainDeleteThirdAccountFromAccountType:(NSString *)accountType
                                        nickName:(NSString*)nickName andSecondFactorKey:(NSString *)secondFactorKey{
    
    RegistrationThirdAccountSuccessResponse *operationResultResponse = [[RegistrationThirdAccountSuccessResponse alloc] init];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH,THIRD_ACCOUNT_DELETE_CONFIRMATION_OPERATION ]];
    
    //uri = [NSMutableString stringWithString:@"http://192.168.1.103/bbva/newDeleteAccountConfirmation.xml"];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForThirdAccountRegistrationWithAccountType:accountType andNickname:nickName andSecondFactorKey:secondFactorKey];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:THIRD_ACCOUNT_OPERATION_SUCCESS] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [operationResultResponse release];
    }
 }
/**
 * validate a Third Account
 *
 * @param accountNumber account number
 * @param accountType type of account
 
 */
- (void) obtainValidationThirdAccountFromNickName:(NSString*)nickName{
    
    NicknameValidationResponse *operationResultResponse = [[NicknameValidationResponse alloc] init];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH,THIRD_ACCOUNT_VALIDATE_NICKNAME_OPERATION ]];
    
    //uri = [NSMutableString stringWithString:@"http://192.168.1.103/bbva/validateNickname.xml"];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForThirdAccountRegistrationNickname:nickName];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:THIRD_ACCOUNT_NICKNAME_VALIDATION] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [operationResultResponse release];
    }
}
- (void) obtainRegisterThirdAccountFromAccountNumber:(NSString *)accountNumber
                                         officeNumber:(NSString *)officeNumber
                                       accountType:(NSString*)accountType
                                            nickName:(NSString*)nickName{
    
    RegistrationThirdAccountConfirmationResponse *operationResultResponse = [[RegistrationThirdAccountConfirmationResponse alloc] init];

    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, THIRD_ACCOUNT_REGISTER_CONFIRMATION_OPERATION]];
    
    //uri = [NSMutableString stringWithString:@"http://192.168.1.103/bbva/newDeleteAccountConfirmation.xml"];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForThirdAccountRegistration:accountNumber officeNumber:officeNumber AccountType:accountType andNickname:nickName];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:THIRD_ACCOUNT_OPERATION_CONFIRMATION] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [operationResultResponse release];
    }
    

}

- (void) obtainRegisterOtherBankFromAccountNumber:(NSString *)accountNumber
                                     entityNumber:(NSString*)entityNumber
                                        officeNumber:(NSString *)officeNumber
                                            ccNumber:(NSString*)ccNumber
                                         accountType:(NSString*)accountType
                                            nickName:(NSString*)nickName{
    
    RegistrationThirdAccountConfirmationResponse *operationResultResponse = [[RegistrationThirdAccountConfirmationResponse alloc] init];
    
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat: @"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, THIRD_ACCOUNT_REGISTER_CONFIRMATION_OPERATION]];
    
    //uri = [NSMutableString stringWithString:@"http://192.168.1.103/bbva/newDeleteAccountConfirmation.xml"];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceForOtherBankAccountRegistration:accountNumber entityNumber:entityNumber officeNumber:officeNumber AccountType:accountType ccNumber:ccNumber andNickname:nickName];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL: uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:THIRD_ACCOUNT_OPERATION_CONFIRMATION] autorelease];
    if(operationInfo != nil){
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject:operationInfo forKey:downloadFile];
        [downloadFile release];
        [operationResultResponse release];
    }
    
    
}


- (void) confirmOperationAccountFromSecondKeyFactor:(NSString *) secondKeyFactor{
 
    RegistrationThirdAccountSuccessResponse *operationResultResponse = [[RegistrationThirdAccountSuccessResponse alloc] init];
    NSMutableString *uri = nil;
    [Session getInstance].globalPositionRequestServerData = YES;
    
    
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, THIRD_ACCOUNT_SUCCESS_OPERATION]];
    
    //uri = [NSMutableString stringWithString:@"http://192.168.1.103/bbva/newDeleteAccountSummary.xml"];
    
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceSafetyPayResultFromSecondKeyFactor:secondKeyFactor];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId: downloadFile operationType:THIRD_ACCOUNT_OPERATION_SUCCESS] autorelease];
    
    if (operationInfo != nil) {
        
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
        
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}

/**
 * Obtain the Initial list of Third Account
 */
- (void) obtainThirdAccountInformationInitialListInfo:(NSString *)nextPage type:(NSString*)type{
    
    ThirdAccountListResponse *operationResultResponse = [[ThirdAccountListResponse alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:FREQUENT_OPERATIONS_INITIAL_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
    if ([@"-1" isEqualToString:nextPage]) {
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, THIRD_ACCOUNT_INITIAL_OPERATION]];
    }else{
        
        uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, THIRD_ACCOUNT_PAGIN_OPERATION]];
    }
    
    //uri = [NSMutableString stringWithString:@"http://192.168.1.103/bbva/listaccounts.xml"];
#endif
    
    NSString *escapedParameters = @"";
    
    if (![@"-1" isEqualToString:nextPage]) {
        escapedParameters = [self returnEscapedParameterThirdAccountListInitial:nextPage andOperationType:type];
    }
    else{
        escapedParameters = [self returnEscapedParamenterSequenceForThirdAccountListWithType:type];
        
    }
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:([@"-1" isEqualToString:nextPage])?THIRD_ACCOUNT_OBTAIN_INITIAL_LIST:THIRD_ACCOUNT_PAGINATION_LIST] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}

/*
 * Obtains the campaign list
 */
- (void)obtainCampaignList {
    
    CampaignList *response = [[CampaignList alloc] init];
    NSMutableString *uri = nil;
    
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, CAMPAIGN_LIST_OPERATION]];
    
    NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:response forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:CAMPAIGN_LIST] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = response;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [response release];
}

/**
 * Obtain the Initial incremedt of line
 */
- (void) incrementLineOperationStartup{
    
    IncrementCreditLineStartUpResponse *operationResultResponse = [[IncrementCreditLineStartUpResponse alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:FREQUENT_OPERATIONS_INITIAL_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, INCREMENT_OF_LINE_INITIAL_OPERATION]];
#endif
    
    NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:INCREMENT_OF_LINE_INITIAL] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}

/**
 * Obtain the Confirmation incremedt of line
 */
- (void) incrementLineOperationConfirmationFromCodeCard:(NSString*)codeCard
                                      amountToIncrement:(NSString*)amountToIncrement
                                                  email:(NSString*)email{
    
    IncrementCreditLineConfirmResponse *operationResultResponse = [[IncrementCreditLineConfirmResponse alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:FREQUENT_OPERATIONS_INITIAL_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, INCREMENT_OF_LINE_CONFIRMATION_OPERATION]];
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceIncrementOfLineConfirmFromCodeCard:codeCard
                                                                                       amountToIncrement:amountToIncrement
                                                                                                   email:email];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:INCREMENT_OF_LINE_CONFIRMATION] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}


/**
 * Obtain the Sucess incremedt of line
 */
- (void) incrementLineOperationSucess:(NSString*)secondFactorKey{
    
    IncrementCreditLineResultResponse *operationResultResponse = [[IncrementCreditLineResultResponse alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:FREQUENT_OPERATIONS_INITIAL_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, INCREMENT_OF_LINE_SUMMARY_OPERATION]];
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceIncrementOfLineResultFromSecondKeyFactor:secondFactorKey];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:INCREMENT_OF_LINE_SUMMARY] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        operationInfo.silent = YES;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}



/**
 * Obtain the Initial Fast Loan
 */
- (void) fastLoanOperationStartup{
    
    FastLoanStartup *operationResultResponse = [[FastLoanStartup alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:FAST_LOAN_INITIAL_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FAST_LOAN_INITIAL_OPERATION]];
#endif
    
    NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FAST_LOAN_INITIAL] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}

/**
 * Obtain the Fast Loan Term List
 */
- (void) fastLoanOperationTerm:(NSString*)amount{
    
    FastLoanTermList *operationResultResponse = [[FastLoanTermList alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:FAST_LOAN_TERM_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FAST_LOAN_TERM_OPERATION]];
#endif
    
    NSString *escapedParameters = [NSString stringWithFormat:@"op_monto_consulta_in=%@",
                                   (amount != nil) ? [amount urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FAST_LOAN_TERM] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}

/**
 * Obtain the Fast Loan Simulation
 */
- (void) fastLoanOperationSimulation:(NSString*)amount
                                term:(NSString*)term
                                dayOfPay:(NSString*)dayOfPay
                                quotaType:(NSString*)quotaType
                                monthDobles:(NSString*)monthDobles
                                accountIndex:(NSString*)accountIndex
                                emailSelection:(NSString*)emailSelection
                                emailEditable:(NSString*)emailEditable
                                action:(NSString*)action
{
    
    FastLoanSimulation *operationResultResponse = [[FastLoanSimulation alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:FAST_LOAN_SIMULATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FAST_LOAN_SIMULATION_OPERATION]];
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceFastLoanSimulationFromAmount:amount
                                                                                  term:term dayOfPay:dayOfPay quotaType:quotaType monthDobles:monthDobles accountIndex:accountIndex emailSelection:emailSelection emailEditable:emailEditable action:action];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FAST_LOAN_SIMULATION] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}


/**
 * Obtain the Fast Loan Confirm
 */
- (void) fastLoanOperationConfirm{
    
    FastLoanConfirm *operationResultResponse = [[FastLoanConfirm alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:FAST_LOAN_CONFIRMATION_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FAST_LOAN_CONFIRMATION_OPERATION]];
    
#endif
    
    NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FAST_LOAN_CONFIRMATION] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}

/**
 * Obtain the Fast Loan Schedule
 */
- (void) fastLoanOperatioSchedule:(NSString*)opAlternative quotaNumber:(NSString*)quotaNumber{
    
    FastLoanSchedule *operationResultResponse = [[FastLoanSchedule alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:FAST_LOAN_SCHEDULE_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FAST_LOAN_SCHEDULE_OPERATION]];
#endif
    
    NSString *escapedParameters =
                            [NSString stringWithFormat:@"op_alternativa_in=%@&op_num_cuotas_in=%@",
                             (opAlternative != nil) ? [opAlternative urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
                             (quotaNumber != nil) ? [quotaNumber urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FAST_LOAN_SCHEDULE] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}

/**
 * Obtain the Fast Loan Sucess
 */
- (void) fastLoanOperationSucess:(NSString*)secondFactorKey{
    
    FastLoanSummary *operationResultResponse = [[FastLoanSummary alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:FAST_LOAN_SUMMARY_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, FAST_LOAN_SUMMARY_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceFastLoanResultFromSecondKeyFactor:secondFactorKey];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:FAST_LOAN_SUMMARY] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        operationInfo.silent = YES;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}


/**
 * Obtain the Salary Advance Affiliation
 */
- (void) salaryAdvanceAffiliationOperation{
    
    SalaryAdvanceAffiliation *operationResultResponse = [[SalaryAdvanceAffiliation alloc] init];
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, SALARY_ADVANCE_AFFILIATION_OPERATION]];
    
    NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:SALARY_ADVANCE_AFFILIATION] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}

/**
 * Obtain the Salary Advance Receive
 */
- (void) salaryAdvanceReceiveOperation:(BOOL)isSilent{
    
    SalaryAdvanceReceive *operationResultResponse = [[SalaryAdvanceReceive alloc] init];
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, SALARY_ADVANCE_RECEIVE_OPERATION]];
    
    NSString *escapedParameters = @"";
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:SALARY_ADVANCE_RECEIVE] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        operationInfo.silent = isSilent;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
}

/**
 * Obtain the Salary Advance Summary
 */
- (void) salaryAdvanceSummaryOperation:(NSString*)secondFactorKey{
    
    SalaryAdvanceSummary *operationResultResponse = [[SalaryAdvanceSummary alloc] init];
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, SALARY_ADVANCE_SUMMMARY_OPERATION]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceSalaryAdvanceResultFromSecondKeyFactor:secondFactorKey];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:SALARY_ADVANCE_SUMMMARY] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        operationInfo.silent = YES;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
}

/**
 * Obtain the Salary Affiliation Confirm
 */
- (void) salaryAdvanceAffiliationConfirmOperation:(NSString*)secondFactorKey :(NSString *)email {
    
    SalaryAdvanceReceive *operationResultResponse = [[SalaryAdvanceReceive alloc] init];
    NSMutableString *uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, SALARY_ADVANCE_AFFILIATION_CONFIRM_OPERATION]];
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceSalaryAdvanceAffiliationConfirmFromSecondKeyFactor:secondFactorKey :email];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:operationResultResponse forListener:self] retain];
    
    OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:SALARY_ADVANCE_AFFILIATION_CONFIRM] autorelease];
    if (operationInfo != nil) {
        operationInfo.xmlParserDelegate = operationResultResponse;
        operationInfo.operationParams = nil;
        operationInfo.silent = YES;
        [activeOperations_ setObject: operationInfo forKey: downloadFile];
    }
    
    [downloadFile release];
    [operationResultResponse release];
    
}

#pragma mark Sesion methods
/**
 * Logout
 */
- (void)logout {
	
	GeneralStatusResponse *logout = [[GeneralStatusResponse alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:LOGOUT_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@/%@", TARGET_SERVER, TARGET_ROOT_PATH, LOGOUT_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceLogout];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:logout forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:LOGOUT] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = logout;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[logout release];
	
}

/**
 * Close
 */
- (void)close {
	
	GeneralStatusResponse *close = [[GeneralStatusResponse alloc] init];
    NSMutableString *uri = nil;
    
#if defined(SIMULATE_HTTP_CONNECTION)
    
    NSString *staticFile = [[NSBundle mainBundle] pathForResource:CLOSE_OPERATION ofType:nil];
    NSURL *staticURL = [NSURL fileURLWithPath:staticFile];
    uri = [NSMutableString stringWithString:[staticURL absoluteString]];
    
#else
    
    uri = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@", TARGET_SERVER, CLOSE_OPERATION]];
    
#endif
    
    NSString *escapedParameters = [self returnEscapedParameterSequenceClose];
    NSData *parametersData = [escapedParameters dataUsingEncoding:NSUTF8StringEncoding];
    NSString *downloadFile = [[[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri withBody:parametersData andXMLParserDelegate:close forListener:self] retain];
	
	OperationInformation *operationInfo = [[[OperationInformation alloc] initWithDownloadId:downloadFile operationType:CLOSE] autorelease];
	if (operationInfo != nil) {
		operationInfo.xmlParserDelegate = close;
		operationInfo.operationParams = nil;
		[activeOperations_ setObject: operationInfo forKey: downloadFile];
	}
    
	[downloadFile release];
	[close release];
	
}

#pragma mark -
#pragma mark DownloadListener selectors

/**
 * Notifies the download has just started. No action is performed
 *
 * @param aDownloadId The download identification
 * @param aSize The downloaded data size if known, 0 if unknown
 */
- (void) startDownload: (NSString*) aDownloadId withSize: (unsigned long long) aSize {
}

/**
 * Notifies the download has just finished.
 *
 * @param aDownloadId The download identification
 * @param aSize The downloaded data size
 */
- (void) endDownload: (NSString*) aDownloadId withSize: (unsigned long long) aSize {
	
	OperationInformation* operationInfo = [[activeOperations_ objectForKey:aDownloadId] retain];
	BOOL success = YES;
	BOOL isLogoutProcedure = NO;
	NSString *logicalErrorCode = nil;
	NSString *logicalErrorMessage = nil;
	StatusEnabledResponse* response = nil;
    
	if (operationInfo.xmlParserDelegate != nil) {
		response = [operationInfo.xmlParserDelegate retain];
		switch (operationInfo.operationType) {
			case LOGIN:
                logicalErrorCode = [response.errorCode retain];
                logicalErrorMessage = [response.errorMessage retain];
                success = ((response.isError == NO) && (response.isLoginError == NO));
				break;
			default:
                logicalErrorCode = [response.errorCode retain];
				logicalErrorMessage = [response.errorMessage retain];
				success = ((response.isError == NO) && (response.isLoginError == NO));
				break;
		}
	}
    
    switch (operationInfo.operationType) {
		case CLOSE:
		case LOGOUT:
			isLogoutProcedure = YES;
			break;
	}
    
	if (success) {
		DLog(@"Operation success: %qu bytes", aSize);
	} else {
		if (logicalErrorMessage != nil) {
			//DLog(@"Operation failed: %@", logicalErrorMessage);
		} else {
            //DLog(@"Operation failed");
		}
	}
	
	[self processFinishedDownload:operationInfo andResult:success];
	
	[activeOperations_ removeObjectForKey:aDownloadId];
    
	if (success == NO) {
		NSString *errorMessage = nil;
		if (([logicalErrorCode isEqualToString: SESSION_EXPIRATION_CODE]) && (!isLogoutProcedure)) {
			NXT_Peru_iPhone_AppDelegate *delegate = (NXT_Peru_iPhone_AppDelegate *) [UIApplication sharedApplication].delegate;
			[delegate sessionExpired];
		} else if (logicalErrorMessage != nil) {
			errorMessage = logicalErrorMessage;
		} else if (response.isLoginError) {
			errorMessage = NSLocalizedString(LOGIN_ERROR_SERVER_ERROR_KEY, nil);
		} else {
			//TODO: Create default error
		}
		if ((errorMessage != nil) && (!isLogoutProcedure)) {
			
			if (operationInfo.operationType != RETRIEVE_ACCOUNT_TRANSACTION_DETAIL &&
				operationInfo.operationType != RETRIEVE_RETAINS &&
                operationInfo.operationType != TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_RESULT &&
                operationInfo.operationType != TRANSFER_BETWEEN_ACCOUNTS_RESULT &&
                operationInfo.operationType != TRANSFER_TO_THIRD_ACCOUNTS_RESULT && ((operationInfo.operationType != TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION || operationInfo.operationType != TRANSFER_TO_THIRD_ACCOUNTS_CONFIRMATION) && ![response.errorCode isEqualToString:THIRD_ACCOUNT_REGISTRATION_ERROR] ) ) {
				
                if (!operationInfo.silent) {
                    
                    [Tools showErrorWithMessage:errorMessage];
                    
                }
				
			}
		}
	}
	
	[logicalErrorCode release];
	[logicalErrorMessage release];
	[response release];
	[operationInfo release];
	
}

/**
 * Notifies the download has finished in error. Shows the error message
 *
 * @param aDownloadId The download identification
 * @param anError The download error
 * @param aXMLString string to parse
 */
- (void) download: (NSString*) aDownloadId error: (NSError*) anError xmlString:(NSString *)aXMLString {
    
	//DLog(@"Operation failed with error: %@", [anError localizedDescription]);
	
	OperationInformation* operationInfo = [[activeOperations_ objectForKey:aDownloadId] retain];
    
	BOOL isSessionExpiration = NO;
	BOOL isLogoutProcedure = NO;
	BOOL success = NO;
	
	switch (operationInfo.operationType) {
		case CLOSE:
		case LOGOUT:
			// Logon case is due to a session failure in server: must be invoked after session expiration on logout procedure
			isLogoutProcedure = YES;
			success = YES;
			break;
	}
	
	if (aXMLString != nil) {
		//DLog(@"Failed response message: %@", aXMLString);
		NSRange range = [aXMLString rangeOfString:@"</logon>"];
		if (range.location == 0) {
			isSessionExpiration = YES;
			success = YES;
			if (operationInfo.xmlParserDelegate != nil) {
			}
		}
	}
	
	if ((!isLogoutProcedure) && (isSessionExpiration)) {
		NXT_Peru_iPhone_AppDelegate *delegate = (NXT_Peru_iPhone_AppDelegate *) [UIApplication sharedApplication].delegate;
		[delegate sessionExpired];
	}
    
	if (!success) {
		if (operationInfo.xmlParserDelegate != nil) {
			StatusEnabledResponse* response = operationInfo.xmlParserDelegate;
			if (!response.isError) {
				response.isError = YES;
			}
		}
        
        if (!operationInfo.silent) {
            
            //		[Tools showErrorWithMessage:[NSString stringWithFormat:NSLocalizedString(GENERAL_ERROR_KEY,nil), [anError domain], [anError code]]];
            [Tools showErrorWithMessage:NSLocalizedString(GENERAL_ERROR2_KEY,nil)];
            
        }
        
	}
    
	[self processFinishedDownload:operationInfo andResult:success];
    
	[activeOperations_ removeObjectForKey:aDownloadId];
	
	[operationInfo release];
    
}


/**
 * Notifies the download has finished with a status code different than OK_STATUS (200). Shows an error message
 *
 * @param aDownloadId The download identification
 * @param aStatusCode The status code that finished the download
 */
- (void) download: (NSString*) aDownloadId finishedWithStatusCode: (NSInteger) aStatusCode {
	//DLog(@"Operation failed with HTTP error code: %d", aStatusCode);
    
	OperationInformation* operationInfo = [[activeOperations_ objectForKey:aDownloadId] retain];
    
	if (operationInfo.xmlParserDelegate != nil) {
        
        if ([operationInfo.xmlParserDelegate respondsToSelector:@selector(setIsError:)]) {
            [operationInfo.xmlParserDelegate setIsError:TRUE];
        }
	}
	
	[self processFinishedDownload:operationInfo andResult:NO];
    
	[activeOperations_ removeObjectForKey:aDownloadId];
    
    if (!operationInfo.silent) {
        
        [Tools showErrorWithMessage:NSLocalizedString(COMMUNICATIONS_ERROR_TEXT_KEY, nil)];
        
    }
    
	[operationInfo release];
    
}

/**
 * Notifies the download was cancelled. Removes the entries in the download dictionaries
 * but no client is notified
 *
 * @param aDownloadId The download identification
 */
- (void) cancelledDownload: (NSString*) aDownloadId {
	//DLog(@"Operation cancelled");
	
	OperationInformation* operationInfo = [[activeOperations_ objectForKey:aDownloadId] retain];
    
	[self processFinishedDownload:operationInfo andResult:NO];
    
	[activeOperations_ removeObjectForKey:aDownloadId];
	
	[operationInfo release];
    
}

/**
 * Process a finished download
 *
 * @param operationInfo The operation info
 * @param success The operation result
 */
- (void) processFinishedDownload:(OperationInformation *)operationInfo andResult:(BOOL)success {
    
	if (operationInfo != nil) {
        
		switch (operationInfo.operationType) {
			case LOGIN:
				if (!success) {
                    
                    GeneralLoginResponse *loginResponse = (GeneralLoginResponse *)operationInfo.xmlParserDelegate;
                    loginResponse.isError = YES;
                    
				}
				break;
            case LOGIN_ENCRYPT:
				if (!success) {
                    
                    GeneralLoginResponse *loginResponse = (GeneralLoginResponse *)operationInfo.xmlParserDelegate;
                    loginResponse.isError = YES;
                    
				}
				break;
            case LOGIN_DESENCRYPT:
				if (!success) {
                    
                    GeneralLoginResponse *loginResponse = (GeneralLoginResponse *)operationInfo.xmlParserDelegate;
                    loginResponse.isError = YES;
                    
				}
				break;
			case RETRIEVE_GLOBAL_POSITION:
				if (success) {
					GlobalPositionResponse *globalPositionResponse = operationInfo.xmlParserDelegate;
                    Session *session = [Session getInstance];
                    [session.additionalInformation updateFrom:globalPositionResponse.additionalInformation];
					[session.accountList updateFrom:globalPositionResponse.accountList];
					[session.cardList updateFrom:globalPositionResponse.cardList];
					[session.depositsList updateFrom:globalPositionResponse.depositsList];
					[session.loansList updateFrom:globalPositionResponse.loansList];
					[session.stockMarketAccountsList updateFrom:globalPositionResponse.stockMarketAccountsList];
					[session.mutualFundsList updateFrom:globalPositionResponse.mutualFundsList];
				}
				break;
			case RETRIEVE_ACCOUNT_TRANSACTION_LIST: {
                
				if (success) {
                    
					AccountTransactionsResponse *accountTransactionsResponse = operationInfo.xmlParserDelegate;
                    BankAccount *account = [[Session getInstance].accountList accountFromAccountNumber:operationInfo.operationParams];
                    [account updateAccountTransactionsList:accountTransactionsResponse.accountTransactionList];
                    [account updateAccountTransactionsAdditionalInformation:accountTransactionsResponse.additionalInformation];
                    
				}
                
				break;
                
            }
                
            case RETRIEVE_ACCOUNT_TRANSACTION_DETAIL: {
                
				if (success) {
                    
				}
                
				break;
                
            }
                
			case RETRIEVE_CARD_TRANSACTION_LIST: {
                
                if (success) {
                    
					CardTransactionsResponse *cardTransactionsResponse = operationInfo.xmlParserDelegate;
                    Card *card = [[Session getInstance].cardList cardFromCardNumber:operationInfo.operationParams];
                    [card updateCardTransactionsList:cardTransactionsResponse.cardTransactionList];
                    [card updateCardTransactionsAdditionalInformation:cardTransactionsResponse.additionalInformation];
                    
				}
                
				break;
                
            }
                
			case TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION:
				if (success) {
				}
				break;
			case TRANSFER_BETWEEN_ACCOUNTS_RESULT:
				if (success) {
				}
				break;
			case RETRIEVE_RETAINS:
				if (success) {
				}
				break;
			case SEND_TRANSACTION:
				if (success) {
				}
				break;
			case SEND_TRANSACTION_CONFIRMATION:
				if (success) {
				}
				break;
                
            case CLOSE:
				if (success) {
				}
                break;
            case LOGOUT:
				if (success) {
				}
                break;
            case TRANSFER_TO_THIRD_ACCOUNTS_STARTUP:
				if (success) {
                    TransferStartupResponse *response = operationInfo.xmlParserDelegate;
                    [Session getInstance].mobileCarriers = response.mobileCarriers;
                }
				break;
			case TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_STARTUP:
                if (success) {
                    TransferStartupResponse *response = operationInfo.xmlParserDelegate;
                    [Session getInstance].mobileCarriers = response.mobileCarriers;
                }
				break;
            case TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_STARTUP:
                if (success) {
                    TransferStartupResponse *response = operationInfo.xmlParserDelegate;
                    [Session getInstance].accounts = response.accounts;
                }
				break;
			default:
				break;
		}
        
        operationInfo.operationResult = success;
        
        if (success) {
            
            if (operationInfo.xmlParserDelegate != nil) {
                ParseableObject *xmlParserDelegate = operationInfo.xmlParserDelegate;
                if ([xmlParserDelegate respondsToSelector:@selector(notificationToPost)] && [xmlParserDelegate notificationToPost] != nil) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:xmlParserDelegate.notificationToPost object:xmlParserDelegate];
                }
            }
            
        }
        
        // For "end of operation" notifications
        switch (operationInfo.operationType) {
            case LOGIN:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_LOGIN];
                break;
            case LOGIN_COORDINATE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginCoordinateEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_VALIDATE_COORDINATES];
                break;
            case LOGIN_ENCRYPT:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginEncryptEnds object:operationInfo.xmlParserDelegate];
                //[self processMCBNotifications:MAT_LOG_VALIDATE_COORDINATES];
                break;
            case LOGIN_DESENCRYPT:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginDesencryptEnds object:operationInfo.xmlParserDelegate];
                //[self processMCBNotifications:MAT_LOG_VALIDATE_COORDINATES];
                break;
			case RETRIEVE_GLOBAL_POSITION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGlobalPositionEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_GLOBAL_POSITION];
				break;
			case RETRIEVE_ACCOUNT_TRANSACTION_LIST:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRetrieveAccountTransactionListEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_ACCOUNTS_TRANSACTIONS];
				break;
			case RETRIEVE_CARD_TRANSACTION_LIST:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRetrieveCardTransactionListEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_CARDS_TRANSACTIONS];
				break;
            case TRANSFER_BETWEEN_ACCOUNTS_STARTUP:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferBetweenAccountsStartupEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_OWN_ACCOUNTS_TRANSFER_SEND];
				break;
			case TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferConfirmationResponseReceived object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_OWN_ACCOUNTS_TRANSFER_CONFIRMATION];
				break;
			case TRANSFER_BETWEEN_ACCOUNTS_RESULT:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferSuccessResponseReceived object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_OWN_ACCOUNTS_TRANSFER_SUCCESS];
                break;
			case RETRIEVE_RETAINS:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRetrieveRetainsEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_RETENTIONS];
				break;
			case SEND_TRANSACTION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSendTransactionEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_SEND_TRANSACTION];
				break;
			case SEND_TRANSACTION_CONFIRMATION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSendTransactionConfirmationEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_SEND_TRANSACTION_CONFIRMATION];
				break;
            case CLOSE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_LOGOUT];
                break;
            case LOGOUT:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLogoutEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_DISCONNECT];
                break;
            case TRANSFER_TO_THIRD_ACCOUNTS_STARTUP:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferToThirdAccountsStartupEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_OTHER_BANK_CUSTOMER_TRANSFER_SEND];
                break;
            case TRANSFER_TO_THIRD_ACCOUNTS_CONFIRMATION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferConfirmationResponseReceived object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_OTHER_BANK_CUSTOMER_TRANSFER_CONFIRMATION];
                break;
			case TRANSFER_TO_THIRD_ACCOUNTS_RESULT:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferSuccessResponseReceived object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_OTHER_BANK_CUSTOMER_TRANSFER_SUCCESS];
                break;
			case TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_STARTUP:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferToAccountFromOtherBanksStartupEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_OTHER_BANK_TRANSFER_SEND];
                break;
            case TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_TYPE_CHECKING:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferTypeCheckResponseReceived object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_OTHER_BANK_TRANSFER_CHECK_TYPE];
                break;
            case TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferConfirmationResponseReceived object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_OTHER_BANK_TRANSFER_CONFIRMATION];
                break;
			case TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_RESULT:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferSuccessResponseReceived object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_OTHER_BANK_TRANSFER_SUCCESS];
                break;
            case TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_STARTUP:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferWithCashMobileStartupEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_CASH_MOBILE_TRANSFER_SEND];
                break;
            case TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_CONFIRMATION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferConfirmationResponseReceived object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_CASH_MOBILE_TRANSFER_CONFIRMATION];
                break;
			case TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_RESULT:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferSuccessResponseReceived object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_CASH_MOBILE_TRANSFER_SUCCESS];
                break;
            case OBTAIN_CASH_MOBILE_DETAIL_STARTUP:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferWithCashMobileDetailStartupEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_CASH_MOBILE_CONSULT];
                break;
            case OBTAIN_CASH_MOBILE_DETAIL:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferWithCashMobileDetailEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_CASH_MOBILE_DETAIL];
                break;
            case OBTAIN_CASH_MOBILE_DETAIL_RESEND:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferWithCashMobileDetailResendEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_CASH_MOBILE_RESEND];
                break;
                
            case TRANSFER_TO_ACCOUNT_TO_GIFT_CARD_STARTUP:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferToGiftCardStartupEnds object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_GIFT_CARD_SEND];
                break;
            case TRANSFER_TO_ACCOUNT_TO_GIFT_CARD_CONFIRMATION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferConfirmationResponseReceived object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_GIFT_CARD_CONFIRMATION];
                break;
			case TRANSFER_TO_ACCOUNT_TO_GIFT_CARD_RESULT:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferSuccessResponseReceived object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_GIFT_CARD_SUCCESS];
                break;
                
                
            case PAYMENT_INSTITUTIONS_LIST_OPERATION_RESULT:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentInstitutionsListResultEnds
																	object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_INST_COMP_LIST];
                break;
            case PAYMENT_INSTITUTIONS_SEARCH_LIST_RESULT:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentInstitutionsSearchListResult
																	object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_INST_COMP_ENTITY_LIST];
                break;
            case PAYMENT_INSTITUTIONS_DETAIL:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentInstitutionsDetailResult
																	object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_INST_COMP_SOLI_PAY];
                break;
            case PAYMENT_INSTITUTIONS_PENDING_PAYS_DETAIL:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentInstitutionsPendingPaysDetailResult
																	object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_INST_COMP_CONFIRM_PAY];
                break;
            case PAYMENT_INSTITUTIONS_CONFIRMATION_PAY_DETAIL:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentInstitutionsConfirmationPayResult
																	object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_INST_COMP_PEDING_PAYS];
                break;
            case PAYMENT_INSTITUTIONS_SUCCESS_PAYS_DETAIL:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentInstitutionsSuccessPayResult
																	object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_INST_COMP_DO_PAY];
                break;
            case PAYMENT_SERVICES_LIST_OPERATION_RESULT:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentServicesListResultEnds
																	object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_START];
                break;
                
            case PAYMENT_PUBLIC_SERVICE_INITIAL_ELECT_LUZ_DEL_SUR:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentServicesListResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_LUZ_DEL_SUR_LIST];
                break;
                
            case PAYMENT_PUBLIC_SERVICE_INITIAL_ELECT_EDELNOR:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentServicesListResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_EDELNOR_LIST];
                break;
            case PAYMENT_PUBLIC_SERVICE_INITIAL_WATER_SEDAPAL:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentServicesListResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_SEDAPAL_LIST];
                break;
                
            case PAYMENT_PUBLIC_SERVICE_INITIAL_PHONE_TELEFONICA:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentServicesListResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_TELEFONICA_LANDLINE_LIST];
                break;
                
            case PAYMENT_PUBLIC_SERVICE_INITIAL_CELL_MOVISTAR:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentServicesListResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_MOVISTAR_CELULAR_LIST];
                break;
                
            case PAYMENT_PUBLIC_SERVICE_INITIAL_CELL_CLARO:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentServicesListResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_CLARO_CELULAR_LIST];
                break;
            case PAYMENT_PUBLIC_SERVICE_INITIAL_GAS_REPSOL:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentServicesListResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_CLARO_CELULAR_LIST];
                break;
			case PAYMENT_PUBLIC_SERVICE_DATA_ELECT_LUZ_DEL_SUR:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentDataResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_LUZ_DEL_SUR_DATA];
                break;
            case PAYMENT_PUBLIC_SERVICE_DATA_ELECT_EDELNOR:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentDataResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_EDELNOR_DATA];
                break;
				
			case PAYMENT_PUBLIC_SERVICE_DATA_WATER_SEDAPAL:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentDataResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_SEDAPAL_DATA];
                break;
				
            case PAYMENT_PUBLIC_SERVICE_DATA_PHONE_TELEFONICA:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentDataResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_TELEFONICA_LANDLINE_DATA];
                break;
            case PAYMENT_PUBLIC_SERVICE_DATA_CELL_MOVISTAR:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentDataResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_MOVISTAR_CELULAR_DATA];
                break;
            case PAYMENT_PUBLIC_SERVICE_DATA_CELL_CLARO:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentDataResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_CLARO_CELULAR_DATA];
                break;
                
			case PAYMENT_PUBLIC_SERVICE_CONF_ELECT_LUZ_DEL_SUR:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentConfirmationResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_LUZ_DEL_SUR_CONFIRMATION];
				break;
                
            case PAYMENT_PUBLIC_SERVICE_CONF_ELECT_EDELNOR:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentConfirmationResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_EDELNOR_CONFIRMATION];
				break;
                
            case PAYMENT_PUBLIC_SERVICE_CONF_WATER_SEDAPAL:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentConfirmationResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_SEDAPAL_CONFIRMATION];
				break;
                
            case PAYMENT_PUBLIC_SERVICE_CONF_PHONE_TELEFONICA:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentConfirmationResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_TELEFONICA_LANDLINE_CONFIRMATION];
				break;
                
            case PAYMENT_PUBLIC_SERVICE_CONF_CELL_MOVISTAR:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentConfirmationResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_MOVISTAR_CELULAR_CONFIRMATION];
				break;
                
            case PAYMENT_PUBLIC_SERVICE_CONF_CELL_CLARO:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentConfirmationResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_CLARO_CELULAR_CONFIRMATION];
				break;
                
			case PAYMENT_PUBLIC_SERVICE_SUCCESS_ELECT_LUZ_DEL_SUR:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentSuccessResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_LUZ_DEL_SUR_SUCCESS];
				break;
            case PAYMENT_PUBLIC_SERVICE_SUCCESS_ELECT_EDELNOR:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentSuccessResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_EDELNOR_SUCCESS];
				break;
            case PAYMENT_PUBLIC_SERVICE_SUCCESS_WATER_SEDAPAL:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentSuccessResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_SEDAPAL_SUCCESS];
				break;
            case PAYMENT_PUBLIC_SERVICE_SUCCESS_PHONE_TELEFONICA:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentSuccessResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_TELEFONICA_LANDLINE_SUCCESS];
				break;
            case PAYMENT_PUBLIC_SERVICE_SUCCESS_CELL_MOVISTAR:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentSuccessResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_MOVISTAR_CELULAR_SUCCESS];
				break;
            case PAYMENT_PUBLIC_SERVICE_SUCCESS_CELL_CLARO:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentSuccessResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_CLARO_CELULAR_SUCCESS];
				break;
                
            case PAYMENT_CARDS_CONT_INITIAL:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentContinentalInicilizationResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_OWN_CARDS_LIST];
				break;
                
            case PAYMENT_CARDS_CONT_OWN_DATA:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentDataResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_OWN_CARDS_DATA];
				break;
                
            case PAYMENT_CARDS_CONT_OWN_CONF:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentConfirmationResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_OWN_CARDS_CONFIRMATION];
                break;
                
            case PAYMENT_CARDS_CONT_OWN_SUCCESS:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentSuccessResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_OWN_CARDS_SUCCESS];
                break;
				
            case PAYMENT_CARDS_CONT_THIRD_DATA:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentDataResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_OTHERS_CARDS_DATA];
				break;
                
            case PAYMENT_CARDS_CONT_THIRD_CONF:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentConfirmationResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_OTHERS_CARDS_CONFIRMATION];
                break;
                
            case PAYMENT_CARDS_CONT_THIRD_SUCCESS:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentSuccessResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_OTHERS_CARDS_SUCCESS];
                break;
                
            case PAYMENT_CARDS_OTHER_BANK_INITIAL:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentOtherBankInicilizationResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_LIST];
				break;
                
            case PAYMENT_CARDS_OTHER_BANK_DATA:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentDataResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_DATA];
                break;
            case PAYMENT_CARDS_OTHER_BANK_TIN:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentTINOnlineResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_TIN];
                break;
            case PAYMENT_CARDS_OTHER_BANK_CONF:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentConfirmationResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_CONFIRMATION];
                break;
                
			case PAYMENT_CARDS_OTHER_BANK_SUCCESS:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentSuccessResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_SUCCESS];
				break;
                
            case PAYMENT_RECHARGE_DATA:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentRechargesInitialResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_PREPAY_DATA];
                break;
                
            case PAYMENT_RECHARGE_CONF:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentConfirmationResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_PREPAY_CONFIRMATION];
                break;
                
			case PAYMENT_RECHARGE_MOVISTAR_SUCCESS:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentSuccessResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_PREPAY_TELEFONICA_SUCCESS];
				break;
                
            case PAYMENT_RECHARGE_CLARO_SUCCESS:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPaymentSuccessResultEnds
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_PAYMENT_PREPAY_CLARO_SUCCESS];
				break;
                
            case FREQUENT_OPERATION_OBTAIN_INITIAL_LIST:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFrequentOperationsInitialListResponse
                                                                    object:operationInfo.xmlParserDelegate];
                
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_LIST];
				break;
            case FREQUENT_OPERATION_PAGINATION_LIST:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFrequentOperationsInitialListResponse
                                                                    object:operationInfo.xmlParserDelegate];
                
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_PAGINATION_LIST];
				break;
                
            case FREQUENT_OPERATION_OBTAIN_EXECUTE_INFO:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFrequentOperationsExecuteResponse
                                                                    object:operationInfo.xmlParserDelegate];
                
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_EXECUTE];
                break;
                
            case RETRIEVE_SAFETYPAY_STATUS_INFO:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSafetyPayStatusResponseReceived
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_SAFETYPAY_STATUS_INFO];
                break;
                
            case RETRIEVE_SAFETYPAY_TRANSACTION_INFO:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSafetyPayTransactionInfoResponseReceived object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_SAFETYPAY_TRANSACTION_INFO];
                break;
                
            case RETRIEVE_SAFETYPAY_PAYMENT_DETAILS:
				[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSafetyPayDetailsResponseReceived
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_SAFETYPAY_DETAILS];
                break;
                
            case RETRIEVE_SAFETYPAY_PAYMENT_CONFIRM:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSafetyPayConfirmationResponseReceived
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_SAFETYPAY_CONFIRM];
                break;
            case FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_ONE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORCashMobileStepOne
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_ONE];
                break;
                
            case FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_ONE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFOROtherBankStepOne
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_ONE];
                break;
                
            case FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUHT_STEP_ONE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORThirdAccountStepOne
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUNT_STEP_ONE];
                break;
                
            case FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_ONE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORPaymentThirdCardStepOne
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_ONE];
                break;
                
            case FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_ONE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORPaymentOtherBankStepOne
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_ONE];
                break;
                
            case FREQUENT_OPERATION_REACTIVE_GIFT_CARD_STEP_ONE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORPaymentGiftCardStepOne
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_REACTIVE_GIFT_CARD_PAYMENT_STEP_ONE];
                break;
                
            case FREQUENT_OPERATION_REACTIVE_STEP_TWO:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORStepTwo
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:[self NameOfMatLogStepTwo]];
                break;
                
            case FREQUENT_OPERATION_REACTIVE_STEP_THREE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORStepThree
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:[self NameOfMatLogStepThree]];
                break;
            case FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_ONE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORInstitutionAndCompaniesStepOne
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_ONE];
                break;
                
            case FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_ONE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORRechargeClaroStepOne
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_ONE];
                break;
                
            case FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_ONE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORRechargeMovistarStepOne
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_ONE];
                break;
                
            case FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_ONE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORServiceClaroStepOne
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_ONE];
                break;
                
            case FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_ONE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORServiceMovistarStepOne
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_ONE];
                break;
                
            case FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_ONE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFORServiceWaterStepOne
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_ONE];
                break;
            case THIRD_ACCOUNT_OBTAIN_INITIAL_LIST:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRegisterAccountInitialListResponse
            object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_THIRD_ACCOUNT_REGISTRATE_LIST];
                break;
            case THIRD_ACCOUNT_PAGINATION_LIST:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRegisterAccountInitialListResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_THIRD_ACCOUNT_REGISTRATE_LIST];
                break;

            case THIRD_ACCOUNT_NICKNAME_VALIDATION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationValidateThirdAccountResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_THIRD_ACCOUNT_NICKNAME_VALIDATE_OPERATION];
                break;

            case THIRD_ACCOUNT_OPERATION_CONFIRMATION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferConfirmationResponseReceived
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_THIRD_ACCOUNT_REGISTRATE_CONFIRM_OPERATION];
                break;
            case THIRD_ACCOUNT_OPERATION_SUCCESS:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTransferSuccessResponseReceived
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_THIRD_ACCOUNT_REGISTRATE_SUCCESS_OPERATION];
                break;
            case CAMPAIGN_LIST:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCampaignListResponseReceived
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_CAMPAIGN_LIST_OPERATION];
                break;
            case INCREMENT_OF_LINE_INITIAL:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationIncrementOfLineStepOneResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_INCREMENT_OF_LINE_STEP_ONE_OPERATION];
                break;
            case INCREMENT_OF_LINE_CONFIRMATION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationIncrementOfLineStepTwoResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_INCREMENT_OF_LINE_STEP_TWO_OPERATION];
                break;
            case INCREMENT_OF_LINE_SUMMARY:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationIncrementOfLineStepThreeResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_INCREMENT_OF_LINE_STEP_THREE_OPERATION];
                break;
            case FAST_LOAN_INITIAL:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFastLoanStepOneResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FAST_LOAN_STEP_ONE_OPERATION];
                break;
            case FAST_LOAN_CONFIRMATION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFastLoanStepTwoResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FAST_LOAN_STEP_TWO_OPERATION];
                break;
            case FAST_LOAN_SUMMARY:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFastLoanStepThreeResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FAST_LOAN_STEP_THREE_OPERATION];
                break;
            case FAST_LOAN_TERM:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFastLoanTermResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FAST_LOAN_TERM_OPERATION];
                break;
            case FAST_LOAN_SCHEDULE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFastLoanScheduleResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FAST_LOAN_SCHEDULE_OPERATION];
                break;
            case FAST_LOAN_SIMULATION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationFastLoanSimulationResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_FAST_LOAN_SIMULATION_OPERATION];
                break;
            case SALARY_ADVANCE_AFFILIATION:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSalaryAdvanceAffiliationResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_SALARY_ADVANCE_AFFILIATION_OPERATION];
                break;
            case SALARY_ADVANCE_RECEIVE:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSalaryAdvanceReceiveResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_SALARY_ADVANCE_RECEIVE_OPERATION];
                break;
            case SALARY_ADVANCE_SUMMMARY:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSalaryAdvanceSummaryResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_SALARY_ADVANCE_SUMMARY_OPERATION];
                break;
            case SALARY_ADVANCE_AFFILIATION_CONFIRM:
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSalaryAdvanceAffiliationConfirmResponse
                                                                    object:operationInfo.xmlParserDelegate];
                [self processMCBNotifications:MAT_LOG_SALARY_ADVANCE_AFFILIATION_CONFIRM_OPERATION];
                break;
                
            default:
                break;
        }
        
        operationInfo.xmlParserDelegate = nil;
    }
}
#pragma mark -
#pragma mark Data of MATLOG
/*
 * The name of the MATLOG for the confirmation service
 */
- (NSString *)NameOfMatLogStepTwo {

    NSInteger FORType =	[[[NSUserDefaults standardUserDefaults] objectForKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_TWO] integerValue];
NSString *MatLogName=@"";
    
    switch (FORType) {
        case  FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_TWO           :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_TWO ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_TWO            :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_TWO ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUHT_STEP_TWO         :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUNT_STEP_TWO ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_TWO    :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_TWO ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_TWO    :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_TWO ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_GIFT_CARD_STEP_TWO             :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_GIFT_CARD_PAYMENT_STEP_TWO ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_TWO :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_TWO ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_TWO        :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_TWO ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_TWO     :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_TWO ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_TWO         :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_TWO ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_TWO      :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_TWO ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_TWO         :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_TWO ;
            break;
        }
        
    }

    
    return MatLogName;
    
}
/*
* The name of the MATLOG for the validation service
*/
- (NSString *)NameOfMatLogStepThree {
    
    NSInteger FORType =	[[[NSUserDefaults standardUserDefaults] objectForKey:FREQUENT_OPERATION_REACTIVE_TYPE_STEP_THREE] integerValue];
    NSString *MatLogName=@"";
    
    switch (FORType) {
        case  FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_THREE           :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_THREE ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_THREE            :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_THREE ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUHT_STEP_THREE         :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUNT_STEP_THREE ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_THREE    :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_THREE ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_THREE    :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_THREE ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_GIFT_CARD_STEP_THREE             :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_GIFT_CARD_PAYMENT_STEP_THREE ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_THREE :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_THREE ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_THREE        :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_THREE ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_THREE     :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_THREE ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_THREE         :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_THREE ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_THREE      :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_THREE ;
            break;
        }
            
        case  FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_THREE         :
        {
            MatLogName = MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_THREE ;
            break;
        }
            
    }
    
    
    return MatLogName;
    
}


#pragma mark -
#pragma mark Cancelling downloads

/*
 * Cancels a download defined by its download file
 */
- (void)cancelDownload:(NSString *)aDownloadId {
	
	[[HTTPInvoker getInstance] cancelDownload:aDownloadId];
}

/*
 * Cancels all active downloads
 */
- (void)cancelAllDownloads {
	
	[[HTTPInvoker getInstance] cancelAllDownloads];
	
}

#pragma mark -
#pragma mark Getters and setters

/*
 * Returns the number of active downloads
 */
- (NSInteger)numberOfActiveDownloads {
	return [HTTPInvoker getInstance].numberOfActiveDownloads;
}

#pragma mark -
#pragma mark XML entry generators

/*
 * Generate the entry for login operations
 */
- (NSString *)returnEscapedParameterSequenceLoginWithId:(NSString *)identification
											andPassword:(NSString *)password{
    
#if defined(SEND_VERSION_PARAMETES_ON_LOGIN)
    
    UIDevice *device = [UIDevice currentDevice];
    NSString *encodedModel = [device.model urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *encodedOSVersion = [device.systemVersion urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *encodedAppVersion = [APP_VERSION_STRING urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *encodedUIDHash = [[Tools  getUID] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    NSString *encodedDestinationURL = [[NSString stringWithFormat:@"/bdmv_pe_web/bdmv_pe_web/LogonOperacionServlet?proceso=operaciones_generales_pr&operacion=inicio_op&accion=inicio&tipo_smartphone=P&indicador=2&version=%@&pf=%@&os=%@&uid=%@",
                                        encodedAppVersion, encodedModel, encodedOSVersion, encodedUIDHash] urlEncodeUsingEncoding:NSUTF8StringEncoding];
    
#else
    
    NSString *encodedDestinationURL = [@"/bdmv_pe_web/bdmv_pe_web/LogonOperacionServlet?proceso=operaciones_generales_pr&operacion=inicio_op&accion=inicio&tipo_smartphone=P&indicador=2" urlEncodeUsingEncoding:NSUTF8StringEncoding];
    
#endif //SEND_VERSION_PARAMETES_ON_LOGIN
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"eai_user=%@"\
								  @"&eai_password=%@"\
								  @"&origen=contimovil"\
								  @"&eai_tipoCP=up"\
								  @"&eai_URLDestino=%@",
								  (identification != nil) ? [identification urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (password != nil) ? [password urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  encodedDestinationURL];
	
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
    
}

/*
 * Generate the entry for login encrypt/decrypt operations
 */
- (NSString *)returnEscapedParameterSequenceLoginEncryptWithId:(NSString *)identification
                                              andAlias:(NSString *)alias{
    
    NSInteger lengthAlias = alias.length;
    if(lengthAlias % 8 != 0){
        NSInteger countAditional = (8 - (lengthAlias % 8));
        NSMutableString * resultAlias = [[NSMutableString alloc] initWithString:alias];
        for (int i = 0; i < countAditional; i++) {
            [resultAlias appendString:@"*"];
        }
        alias = (NSString*)resultAlias;
    }
    
    NSString *parametersString = [NSString stringWithFormat:
								  @"PAN=%@&Alias=%@&Function=%@",
								  (identification != nil) ? [identification urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (alias != nil) ? [alias urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
                                   @"0" ];
	
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
}

- (NSString *)returnEscapedParameterSequenceLoginDesencryptWithToken:(NSString *)token{
    
    NSString * tokenResult = (token != nil) ? [token urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"";
    NSString *parametersString = [NSString stringWithFormat:
								  @"Cryptogram=%@&Function=1",
								  tokenResult];
	
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
}


/*
 * Generate the entry for login with coordinate operations
 */
- (NSString *)returnEscapedParameterSequenceLoginWithCoordinate:(NSString *)coordinate {
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"eai_OTPTC=%@",
								  (coordinate != nil) ? [coordinate urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
    
}

/*
 * Generates the entry of the account transactions
 */
- (NSString *)returnEscapedParameterSequenceAccountTransactionsWithType:(NSString *)aType
                                                                subject:(NSString *)aSubject {
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"desTipo=%@"\
								  @"&AsuntoPropio=%@",
								  (aType != nil) ? [aType urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (aSubject != nil) ? [aSubject urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
    
}

/*
 * Generates the entry of the account transaction detail
 */
- (NSString *)returnEscapedParameterSequenceAccountTransactionsDetailWithAccountNumber:(NSString *)accountNumber
                                                                     transactionNumber:(NSString *)transactionNumber {
    
    NSUInteger accountNumberLength = accountNumber.length;
    
    if (accountNumberLength >= 8) {
        
        accountNumber = [accountNumber substringFromIndex:(accountNumberLength - 8)];
        
    }
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"ctanromov=%@$%@",
								  [accountNumber urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  [transactionNumber urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
    
}

/*
 * Generates the entry of the card transactions
 */
- (NSString *)returnEscapedParameterSequenceCardTransactionsWithSubject:(NSString *)aSubject {
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"SeleccionMes=0"\
								  @"&AsuntoPropio=%@",
								  (aSubject != nil) ? [aSubject urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
    
}

/*
 * Generates the entry of transfer confirmation
 */
- (NSString *)returnEscapedParameterSequenceTransferConfirmationFromAccountNumber:(NSString *)fromAccountNumber
                                                                  fromAccountType:(NSString *)fromAccountType
                                                                     fromCurrency:(NSString *)fromCurrency
                                                                         fromType:(NSString *)fromType
                                                                        fromIndex:(NSString *)fromIndex
                                                                  toAccountNumber:(NSString *)toAccountNumber
                                                                    toAccountType:(NSString *)toAccountType
                                                                       toCurrency:(NSString *)toCurrency
                                                                           toType:(NSString *)toType
                                                                          toIndex:(NSString *)toIndex
                                                                           amount:(NSString *)amount
                                                                         currency:(NSString *)currency
                                                                       andSubject:(NSString *)subject {
    
	NSString *fromText = [NSString stringWithFormat:@"%@$%@$%@$%@$%@",
						  (fromAccountNumber != nil) ? fromAccountNumber : @"",
						  (fromAccountType != nil) ? fromAccountType : @"",
						  (fromCurrency != nil) ? fromCurrency : @"",
						  (fromType != nil) ? fromType : @"",
						  (fromIndex != nil) ? fromIndex : @""];
	
	NSString *toText = [NSString stringWithFormat:@"%@$%@$%@$%@$%@",
                        (toAccountNumber != nil) ? toAccountNumber : @"",
                        (toAccountType != nil) ? toAccountType : @"",
                        (toCurrency != nil) ? toCurrency : @"",
                        (toType != nil) ? toType : @"",
                        (toIndex != nil) ? toIndex : @""];
	
	NSString *parametersString = [NSString stringWithFormat:
								  @"AsuntoPropioCargo=%@"\
								  @"&AsuntoPropioAbono=%@"\
								  @"&Importe=%@"\
								  @"&Divisa=%@"\
								  @"&Motivo=%@",
								  [fromText urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  [toText urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  (amount != nil) ? [amount urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (currency != nil) ? [currency urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (subject != nil) ? [subject urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
	
}

/*
 * Generates the entry of transfer result
 */
- (NSString *)returnEscapedParameterSequenceTransferResultFromSecondFactorKey:(NSString *)secondFactor {
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"ClaveSegundoFactor=%@",
								  (secondFactor != nil) ? [secondFactor urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
	
}


/*
 * Generates the entry of transfer result
 */
- (NSString *)returnEscapedParameterSequenceTransferToThirdAccountsResultFromSecondFactorKey:(NSString *)secondFactor {
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"IndicadorTerceros=%@"\
								  @"&ClaveSegundoFactor=%@",
								  @"S",
								  (secondFactor != nil) ? [secondFactor urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
	
}

/*
 * Generate the entry for transfer With Cash Mobile confirmation
 */
- (NSString *)returnEscapedParameterSequenceWithCashMobileConfirmationFromAccountType:(NSString *)fromAccountType
                                                                         fromCurrency:(NSString *)fromCurrency
                                                                            fromIndex:(NSString *)fromIndex
                                                                              toPhone:(NSString *)toPhone
                                                                               amount:(NSString *)amount
                                                                             currency:(NSString *)currency {
    
    fromIndex = [fromIndex stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
	NSString *fromText = [NSString stringWithFormat:@"%@$%@$%@",
						  (fromAccountType != nil) ? fromAccountType : @"",
						  (fromCurrency != nil) ? fromCurrency : @"",
						  (fromIndex != nil) ? fromIndex : @""];
    
    NSString *parametersString = [NSString stringWithFormat:
								  @"AsuntoPropio=%@"\
                                  @"&importe=%@"\
                                  @"&divisa=%@"\
                                  @"&celularBeneficiario=%@",
                                  [fromText urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  (amount != nil) ? [amount urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
                                  (currency != nil) ? [currency urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
                                  [toPhone urlEncodeUsingEncoding:NSUTF8StringEncoding]];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}

/*
 * Generates the entry of trans detail
 */
- (NSString *)returnEscapedParameterSequenceObtainCashMobileTransactionResultFromOperationCode:(NSString *)operationCode
{
    NSString *parametersString = [NSString stringWithFormat:
								  @"numeroOperacion=%@",
								  (operationCode != nil) ? [operationCode urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
}

/**
 * Invoke transfer to third accounts confirmation
 *
 * @param fromAccountType from account type
 * @param fromNumber from account or card number
 * @param toNumber to account number
 * @param amount the amount
 * @param currency the currency
 * @param toConfirmationPhone the phone to send the confirmation
 * @param toConfirmationMail the mail to send the confirmation
 * @param carrier the carrier for phones
 * @param referenceMessage the referenceMessage
 */
- (NSString *)returnEscapedParameterSequenceToGiftCardConfirmationFromAccountType:(NSString *)fromAccountType
                                                                       fromNumber:(NSString *)fromNumber
                                                                         toNumber:(NSString *)toNumber
                                                                           amount:(NSString *)amount
                                                                         currency:(NSString *)currency
                                                              toConfirmationPhone:(NSString *)toConfirmationPhone
                                                               toConfirmationMail:(NSString *)toConfirmationMail
                                                                          carrier:(NSString *)carrier
                                                                 referenceMessage:(NSString *)referenceMessage{
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"TipoTarjeta=%@"\
                                  @"&AsuntoPropio=%@"\
                                  @"&TarjetaRegalo=%@"\
                                  @"&Importe=%@"\
                                  @"&Divisa=%@"\
                                  @"&Celular=%@"\
                                  @"&Correo=%@"\
                                  @"&Operadora=%@"\
                                  @"&MensajeEmail=%@",
                                  [fromAccountType urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  (fromNumber != nil) ? [fromNumber urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
                                  (toNumber != nil) ? [toNumber urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
                                  (amount != nil) ? [amount urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
                                  (currency != nil) ? [currency urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
                                  (toConfirmationPhone != nil) ? toConfirmationPhone : @"",
                                  (toConfirmationMail != nil) ? toConfirmationMail : @"",
                                  (carrier != nil) ? carrier : @"",
                                  (referenceMessage != nil) ? [referenceMessage urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""
                                  ];
    
	return parametersString;
    
}

/*
 * Invoke transfer confirmation
 */
- (NSString *)returnEscapedParameterSequenceRetrieveRetainsForAccountNumber:(NSString *)forAccountNumber
                                                                accountType:(NSString *)accountType
                                                                    balance:(NSString *)balance
                                                           availableBalance:(NSString *)availableBalance
                                                                   currency:(NSString *)currency
                                                                       bank:(NSString *)bank
                                                                     office:(NSString *)office
                                                               controlDigit:(NSString *)controlDigit
                                                                    account:(NSString *)account
                                                                       type:(NSString *)type
                                                                   andIndex:(NSString *)index {
    
	NSString *text = [NSString stringWithFormat:@"%@$%@$%@$%@$%@$%@$%@$%@$%@$%@$$%@",
					  (forAccountNumber != nil) ? forAccountNumber : @"",
					  (accountType != nil) ? accountType : @"",
					  (balance != nil) ? balance : @"",
					  (availableBalance != nil) ? availableBalance : @"",
					  (currency != nil) ? currency : @"",
					  (bank != nil) ? bank : @"",
					  (office != nil) ? office : @"",
					  (controlDigit != nil) ? controlDigit : @"",
					  (account != nil) ? account : @"",
					  (type != nil) ? type : @"",
					  (index != nil) ? index : @""];
	
	NSString *parametersString = [NSString stringWithFormat:
								  @"ClaveAsunto=%@",
								  [text urlEncodeUsingEncoding:NSUTF8StringEncoding]];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
    
}

/*
 * Invoke transaction send
 */
- (NSString *)returnEscapedParameterSequenceSendTransactionForOperation:(NSString *)operation
                                                                 email1:(NSString *)email1
                                                                 email2:(NSString *)email2
                                                               carrier1:(NSString *)carrier1
                                                               carrier2:(NSString *)carrier2
                                                           phonenumber1:(NSString *)phonenumber1
                                                           phonenumber2:(NSString *)phonenumber2
                                                            andComments:(NSString *)comments {
    
    comments = [comments stringByReplacingOccurrencesOfString:@"\n"
                                                   withString:@"\r\n"];
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"Descripcion=%@"\
								  @"&Correo1=%@"\
								  @"&Correo2=%@"\
								  @"&Operadora1=%@"\
								  @"&Operadora2=%@"\
								  @"&Telefono1=%@"\
								  @"&Telefono2=%@"\
								  @"&Comentario=%@",
								  (operation != nil) ? [operation urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (email1 != nil) ? [email1 urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (email2 != nil) ? [email2 urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (carrier1 != nil) ? [carrier1 urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (carrier2 != nil) ? [carrier2 urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (phonenumber1 != nil) ? [phonenumber1 urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (phonenumber2 != nil) ? [phonenumber2 urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (comments != nil) ? [comments urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
	
}

/*
 * Invoke transaction send confirmation
 */
- (NSString *)returnEscapedParameterSequenceSendTransactionConfirmationForEmail:(NSString *)email1
                                                                         email2:(NSString *)email2
                                                                       carrier1:(NSString *)carrier1
                                                                       carrier2:(NSString *)carrier2
                                                                   phonenumber1:(NSString *)phonenumber1
                                                                   phonenumber2:(NSString *)phonenumber2
                                                                    andComments:(NSString *)comments {
    
    comments = [comments stringByReplacingOccurrencesOfString:@"\n"
                                                   withString:@"\r\n"];
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"&Correo1=%@"\
								  @"&Correo2=%@"\
								  @"&Operadora1=%@"\
								  @"&Operadora2=%@"\
								  @"&Telefono1=%@"\
								  @"&Telefono2=%@"\
								  @"&Comentario=%@",
								  (email1 != nil) ? [email1 urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (email2 != nil) ? [email2 urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (carrier1 != nil) ? [carrier1 urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (carrier2 != nil) ? [carrier2 urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (phonenumber1 != nil) ? [phonenumber1 urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (phonenumber2 != nil) ? [phonenumber2 urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (comments != nil) ? [comments urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
	
}

/*
 * Invoke search institutions
 */
- (NSString *)returnEscapedParameterSequenceSearchInstitutionsForEntity:(NSString *)entity
                                                             searchType:(NSString *)searchType
                                                           nextMovement:(NSString *)nextMovement
                                                                 indPag:(NSString *)indPag
                                                              searchArg:(NSString *)searchArg
                                                              LastDescr:(NSString *)lastDescription
                                                                 button:(NSString *)button{
    
	
	NSString *parametersString = [NSString stringWithFormat:
								  @"Entidad=%@"\
								  @"&Tipo_Busq=%@"\
								  @"&SiguienteMovimiento=%@"\
								  @"&Ind_Paginacion=%@"\
								  @"&Arg_Busqueda=%@"\
								  @"&Ult_Descripcion=%@"\
								  @"&boton=%@",
								  entity, searchType,
                                  nextMovement,
                                  indPag,
                                  searchArg,
                                  lastDescription,
                                  button];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
	
}

/*
 * Invoke Detail institution
 */
- (NSString *)returnEscapedParameterSequenceDetailInstitutionsForEntity:(NSString *)code
                                                             optionType:(NSString *)optionType
{
    NSString *parametersString = [NSString stringWithFormat:
								  @"Codentidad=%@"\
								  @"&TipoOpcion=%@",
								  [code urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  optionType
                                  ];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}

/*
 *  * Invoke Detail pending paysinstitution
 *
 * @param code the code of the person
 * @param arrayLong type of the arrayLong
 * @param array the array with all the codes
 * @param titlesArray the array with the titles
 * @param valData the validation data recieved in the service
 * @param flagModule the flag recieved in the service
 *
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequencePendingPaysInstitutionsForCode:(NSString *)code
                                                                 arrayLong:(NSString *)arrayLong
                                                                     array:(NSString *)array
                                                               titlesArray:(NSString *)titlesArray
                                                            validationData:(NSString *)valData
                                                                flagModule:(NSString *)flagModule
{
    NSString *dataValidationClean = [valData stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *parametersString = [NSString stringWithFormat:
								  @"CodMatri=%@"\
								  @"&longitud_arreglo=%@"\
                                  @"&arreglo=%@"\
                                  @"&arreglotitulos=%@"\
                                  @"&dataValidacion=%@"\
                                  @"&flagModulo=%@",
								  [code urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [arrayLong urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [array urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [titlesArray urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [dataValidationClean urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [flagModule urlEncodeUsingEncoding:NSUTF8StringEncoding]
                                  ];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}

/*
 *  * Invoke Detail confirmation pay institution
 *
 * @param payForm the way that the person selected to pay
 * @param ownSubjectAccount the account type
 * @param ownSubjectCard the account number
 * @param phone1 the phone to send the confirmation
 * @param phone2 the phone to send the confirmation
 * @param email1 the email to send the confirmation
 * @param email2 the email to send the confirmation
 * @param carrier1 the carrier of the first phone
 * @param carrier2 the carrier of the second phone
 * @param message message to send
 * @param brand the of the payment
 *
 * @return the string with the parameter sequence
 */
- (NSString *)returnEscapedParameterSequenceConfirmationPayInstitutionsForPayFom:(NSString *)payForm
                                                                         account:(NSString *)ownSubjectAccount
                                                                            card:(NSString *)ownSubjectCard
                                                                       payImport:(NSString *)payImport
                                                                          phone1:(NSString *)phone1
                                                                          phone2:(NSString *)phone2
                                                                          email1:(NSString *)email1
                                                                          email2:(NSString *)email2
                                                                        carrier1:(NSString *)carrier1
                                                                        carrier2:(NSString *)carrier2
                                                                         message:(NSString *)message
                                                                           brand:(NSString *)brand
                                                                            type:(int)type
{
    
    
    NSString *parametersString = @"";
    
    
    parametersString = [NSString stringWithFormat:@"forma_pago=%@", payForm];
    
    if (![@"" isEqualToString:ownSubjectAccount ]) {
        
        NSString *account = [NSString stringWithFormat:@"&AsuntoPropio_cuen=%@", [ownSubjectAccount urlEncodeUsingEncoding:NSUTF8StringEncoding]];
        
        parametersString = [parametersString stringByAppendingString:account];
    }
    
    if (![@"" isEqualToString:ownSubjectCard ]) {
        
        NSString *account = [NSString stringWithFormat:@"&AsuntoPropio_tarj=%@", [ownSubjectCard urlEncodeUsingEncoding:NSUTF8StringEncoding]];
        
        parametersString = [parametersString stringByAppendingString:account];
    }
    
    if (![@"" isEqualToString:payImport ] && payImport != nil) {
        
        NSString *account = [NSString stringWithFormat:@"&importe_ingresado=%@", payImport];
        
        parametersString = [parametersString stringByAppendingString:account];
    }
    
    if (![@"" isEqualToString:phone1 ]) {
        
        NSString *account = [NSString stringWithFormat:@"&numCelular1=%@", phone1];
        
        parametersString = [parametersString stringByAppendingString:account];
    }
    
    if (![@"" isEqualToString:phone2 ]) {
        
        NSString *account = [NSString stringWithFormat:@"&numCelular2=%@", phone2];
        
        parametersString = [parametersString stringByAppendingString:account];
    }
    
    if (![@"" isEqualToString:email1 ]) {
        
        NSString *account = [NSString stringWithFormat:@"&email1=%@", email1];
        
        parametersString = [parametersString stringByAppendingString:account];
    }
    
    if (![@"" isEqualToString:email2 ]) {
        
        NSString *account = [NSString stringWithFormat:@"&email2=%@", email2];
        
        parametersString = [parametersString stringByAppendingString:account];
    }
    
    if (![@"" isEqualToString:carrier1 ]) {
        
        NSString *account = [NSString stringWithFormat:@"&operadorCelular1=%@", carrier1];
        
        parametersString = [parametersString stringByAppendingString:account];
    }
    
    if (![@"" isEqualToString:carrier2 ]) {
        
        NSString *account = [NSString stringWithFormat:@"&operadorCelular2=%@", carrier2];
        
        parametersString = [parametersString stringByAppendingString:account];
    }
    
    if (![@"" isEqualToString:message ]) {
        
        NSString *account = [NSString stringWithFormat:@"&mensajeEmail=%@", message];
        
        parametersString = [parametersString stringByAppendingString:account];
    }
    
    if (![@"" isEqualToString:brand ]) {
        
        NSString *account = [NSString stringWithFormat:@"&Marca=%@", brand];
        
        parametersString = [parametersString stringByAppendingString:account];
    }
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}

/*
 * Generate the entry for close operations
 */
- (NSString *)returnEscapedParameterSequenceClose {
    
    NSString *xmlString = @"";
    
    //DLog(@"Operation parameters: %@", xmlString);
    
    return xmlString;
}

/*
 * Generate the entry for logout operations
 */
- (NSString *)returnEscapedParameterSequenceLogout {
    
    NSString *xmlString = @"";
    
    //DLog(@"Operation parameters: %@", xmlString);
    
    return xmlString;
}

/*
 * Generate the entry for transfer to third accounts confirmation
 */
- (NSString *)returnEscapedParameterSequenceTransferToThirdAccountsConfirmationFromAccountType:(NSString *)fromAccountType
                                                                                  fromCurrency:(NSString *)fromCurrency
                                                                                     fromIndex:(NSString *)fromIndex
                                                                                      toBranch:(NSString *)toBranch
                                                                                     toAccount:(NSString *)toAccount
                                                                                        amount:(NSString *)amount
                                                                                      currency:(NSString *)currency
                                                                                     reference:(NSString *)reference
                                                                                        email1:(NSString *)anEmail1
                                                                                        email2:(NSString *)anEmail2
                                                                                        phone1:(NSString *)aPhone1
                                                                                      carrier1:(NSString *)aCarrier1
                                                                                        phone2:(NSString *)aPhone2
                                                                                      carrier2:(NSString *)aCarrier2
                                                                                    andMessage:(NSString *)message {
    
	NSString *fromText = [NSString stringWithFormat:@"%@$%@$%@",
						  (fromAccountType != nil) ? fromAccountType : @"",
						  (fromCurrency != nil) ? fromCurrency : @"",
						  (fromIndex != nil) ? fromIndex : @""];
    
    message = [message stringByReplacingOccurrencesOfString:@"\n"
                                                 withString:@"\r\n"];
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"AsuntoPropio=%@"\
								  @"&Oficinab=%@"\
								  @"&Cuentab=%@"\
								  @"&Importe=%@"\
								  @"&Divisa=%@"\
								  @"&eMail1=%@"\
								  @"&eMail2=%@"\
								  @"&numCelular1=%@"\
								  @"&numCelular2=%@"\
								  @"&operadorCelular1=%@"\
								  @"&operadorCelular2=%@"\
								  @"&mensajeReferencia=%@"\
								  @"&mensajeEmail=%@",
								  [fromText urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  [toBranch urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [toAccount urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  (amount != nil) ? [amount urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (currency != nil) ? [currency urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
                                  [anEmail1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [anEmail2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [aPhone1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [aPhone2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [aCarrier1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [aCarrier2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  (reference != nil) ? [reference urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (message != nil) ? [message urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}

/*
 * Generate the entry for transfer to accounts from other bancks confirmation
 */
- (NSString *)returnEscapedParameterSequenceTransferToAccountsFromOtherBanksConfirmationFromAccountType:(NSString *)fromAccountType
																						   fromCurrency:(NSString *)fromCurrency
																							  fromIndex:(NSString *)fromIndex
																								 toBank:(NSString *)toBank
																							   toBranch:(NSString *)toBranch
																							  toAccount:(NSString *)toAccount
																								   toCc:(NSString *)toCc
																								 amount:(NSString *)amount
																							   currency:(NSString *)currency
																							  reference:(NSString *)reference
																								 email1:(NSString *)anEmail1
																								 email2:(NSString *)anEmail2
																								 phone1:(NSString *)aPhone1
																							   carrier1:(NSString *)aCarrier1
																								 phone2:(NSString *)aPhone2
																							   carrier2:(NSString *)aCarrier2
																								message:(NSString *)message {
    
	NSString *fromText = [NSString stringWithFormat:@"%@$%@$%@",
						  (fromAccountType != nil) ? fromAccountType : @"",
						  (fromCurrency != nil) ? fromCurrency : @"",
						  (fromIndex != nil) ? fromIndex : @""];
	
    message = [message stringByReplacingOccurrencesOfString:@"\n"
                                                 withString:@"\r\n"];
	
	NSString *parametersString = [NSString stringWithFormat:
								  @"AsuntoPropio=%@"\
								  @"&w3importe=%@"\
								  @"&moneda=%@"\
								  @"&w1banco=%@"\
								  @"&w1oficina=%@"\
								  @"&w1cuenta=%@"\
								  @"&w1control=%@"\
								  @"&eMail1=%@"\
								  @"&eMail2=%@"\
								  @"&numCelular1=%@"\
								  @"&numCelular2=%@"\
								  @"&operadorCelular1=%@"\
								  @"&operadorCelular2=%@"\
								  @"&mensajeEmail=%@",
								  [fromText urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  (amount != nil) ? [amount urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  (currency != nil) ? [currency urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
								  [toBank urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  [toBranch urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [toAccount urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  [toCc urlEncodeUsingEncoding:NSUTF8StringEncoding],

                                  [anEmail1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [anEmail2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [aPhone1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [aPhone2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [aCarrier1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [aCarrier2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  (message != nil) ? [message urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}

/*
 * Generate the entry for transfer to accounts from other banks by schedule confirmation
 */
- (NSString *)returnEscapedParameterSequenceTransferByScheduleWithItf:(BOOL)itf
                                                          beneficiary:(NSString *)beneficiary
                                                         documentType:(NSString *)documentType
                                                    andDocumentNumber:(NSString *)documentNumber{
    
    NSString *parametersString = [NSString stringWithFormat: @"&itf=%@"\
                                  @"&beneficiario=%@"\
                                  @"&TipoDocumento=%@"\
                                  @"&NroDocumento=%@",
                                  itf ? @"S" : @"N",
                                  (beneficiary != nil) ? [beneficiary urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
                                  (documentType != nil) ? [documentType urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
                                  (documentNumber != nil) ? [documentNumber urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    
    return parametersString;
}

/**
 * Process MCB notifications after an UpdaterOperation invocation
 *
 * @param notification The notification.
 * @private
 */
- (void)processMCBNotifications:(NSString *)notification {
    
    [MCBFacade invokeLogInvokedAppMethodWithAppName:MCB_APPLICATION_NAME
                                             appKey:MCB_APPLICATION_KEY
                                         appCountry:APP_COUNTRY
                                         appVersion:APP_VERSION_STRING
                                           latitude:0.0f
                                          longitude:0.0f
                                      validLocation:NO
                                          appMethod:notification];
}

/*
 * Generate the entry for public service initial operation that needs a supply
 */
- (NSString *)returnEscapedParameterSequenceForPSInitialOpSupply:(NSString *)supply {
	
    NSString *parametersString = [NSString stringWithFormat:
								  @"suministro=%@",
								  [supply urlEncodeUsingEncoding:NSUTF8StringEncoding]];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
    
}

/*
 * Generate the entry for public service cellular initial operation
 */
- (NSString *)returnEscapedParameterSequenceForPSCellular:(NSString *)telephone {
    
    NSString *parametersString = [NSString stringWithFormat:
								  @"telefono=%@",
								  [telephone urlEncodeUsingEncoding:NSUTF8StringEncoding]];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}

/*
 * Generate the entry for public service phone initial operation
 */
- (NSString *)returnEscapedParameterSequenceForPSPhoneNumber:(NSString *)telephone {
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"telefono=%@",
								  [telephone urlEncodeUsingEncoding:NSUTF8StringEncoding]];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}

/*
 * Generate the entry for payment public service data operation
 */
- (NSString *)returnEscapedParameterSequencePaymentPSConfirmationForIssue:(NSString *)issue
                                                                idPayment:(NSString *)idPayment
                                                                   email1:(NSString *)email1
                                                                   email2:(NSString *)email2
                                                             phoneNumber1:(NSString *)phoneNumber1
                                                             phoneNumber2:(NSString *)phoneNumber2
                                                                 carrier1:(NSString *)carrier1
                                                                 carrier2:(NSString *)carrier2
                                                                  message:(NSString *)message {
	
    NSString *carrierOne = ([phoneNumber1 length] > 0) ? carrier1 : @"";
    NSString *carrierTwo = ([phoneNumber2 length] > 0) ? carrier2 : @"";
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"asuntopropio=%@"\
								  @"&idpagos=%@"\
								  @"&eMail1=%@"\
								  @"&eMail2=%@"\
								  @"&numCelular1=%@"\
								  @"&numCelular2=%@"\
								  @"&operadorCelular1=%@"\
								  @"&operadorCelular2=%@"\
								  @"&mensajeReferencia=%@",
                                  [issue urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [idPayment urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierOne urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierTwo urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  (message != nil) ? [message urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}

/*
 * Generate the entry for payment public service data operation
 */
- (NSString *)returnEscapedParameterSequencePaymentPSConfirmationUpperForIssue:(NSString *)issue
																	 idPayment:(NSString *)idPayment
																		email1:(NSString *)email1
																		email2:(NSString *)email2
																  phoneNumber1:(NSString *)phoneNumber1
																  phoneNumber2:(NSString *)phoneNumber2
																	  carrier1:(NSString *)carrier1
																	  carrier2:(NSString *)carrier2
																	   message:(NSString *)message {
	
    NSString *carrierOne = ([phoneNumber1 length] > 0) ? carrier1 : @"";
    NSString *carrierTwo = ([phoneNumber2 length] > 0) ? carrier2 : @"";
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"asuntopropio=%@"\
								  @"&idPagos=%@"\
								  @"&eMail1=%@"\
								  @"&eMail2=%@"\
								  @"&numCelular1=%@"\
								  @"&numCelular2=%@"\
								  @"&operadorCelular1=%@"\
								  @"&operadorCelular2=%@"\
								  @"&mensajeReferencia=%@",
                                  [issue urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [idPayment urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierOne urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierTwo urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  (message != nil) ? [message urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}

/*
 * Generate the entry for payment public service data operation
 */
- (NSString *)returnEscapedParameterSequencePaymentPSConfirmationForIssue:(NSString *)issue
																 payments:(NSString *)payments
                                                                   email1:(NSString *)email1
                                                                   email2:(NSString *)email2
                                                             phoneNumber1:(NSString *)phoneNumber1
                                                             phoneNumber2:(NSString *)phoneNumber2
                                                                 carrier1:(NSString *)carrier1
                                                                 carrier2:(NSString *)carrier2
                                                                  message:(NSString *)message {
	
    NSString *carrierOne = ([phoneNumber1 length] > 0) ? carrier1 : @"";
    NSString *carrierTwo = ([phoneNumber2 length] > 0) ? carrier2 : @"";
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"asuntopropio=%@"\
								  @"&idPagos=%@"\
								  @"&eMail1=%@"\
								  @"&eMail2=%@"\
								  @"&numCelular1=%@"\
								  @"&numCelular2=%@"\
								  @"&operadorCelular1=%@"\
								  @"&operadorCelular2=%@"\
								  @"&mensajeReferencia=%@",
                                  [issue urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  [payments urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierOne urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierTwo urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  (message != nil) ? [message urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}

/*
 * Generate the entry for a payment success operation
 */
- (NSString *)returnEscapedParameterSequenceForPaymentSuccessSecondFactorKey:(NSString *)secondFactorKey {
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"ClaveSegundoFactor=%@",
								  (secondFactorKey != nil) ? [secondFactorKey urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
    
}

/*
 * Generate the entry for a card payment data operation
 */
- (NSString *)returnEscapedParameterSequenceForIssue:(NSString *)issue  {
	
	NSString *parametersString = [NSString stringWithFormat:
								  @"asuntopropio=%@",
								  (issue != nil) ? [issue urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
	
}

/*
 * Generate the entry for a payment recharge operation.
 */
- (NSString *)returnEscapedParamenteSequenceForPaymentRechargeCompany:(NSString *)company {
	
	NSString *parametersString = [NSString stringWithFormat:
								  @"empresa=%@",
								  (company != nil) ? [company urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
	
}

/*
 * Generate the entry for payment continental card confirmation
 */
- (NSString *)returnEscapedParamenteSequenceForPaymentRechargeConfirmationNumber:(NSString *)phoneNumber
																		   issue:(NSString *)issue
																		  amount:(NSString *)amount
																		  email1:(NSString *)email1
																		  email2:(NSString *)email2
																	phoneNumber1:(NSString *)phoneNumber1
																	phoneNumber2:(NSString *)phoneNumber2
																		carrier1:(NSString *)carrier1
																		carrier2:(NSString *)carrier2
																		 message:(NSString *)message {
	
    NSString *carrierOne = ([phoneNumber1 length] > 0) ? carrier1 : @"";
    NSString *carrierTwo = ([phoneNumber2 length] > 0) ? carrier2 : @"";
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"telefono=%@"\
                                  @"&asuntopropio=%@"\
								  @"&importe=%@"\
								  @"&eMail1=%@"\
								  @"&eMail2=%@"\
								  @"&numCelular1=%@"\
								  @"&numCelular2=%@"\
								  @"&operadorCelular1=%@"\
								  @"&operadorCelular2=%@"\
								  @"&mensajeReferencia=%@",
                                  [phoneNumber urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [issue urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [amount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierOne urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierTwo urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  (message != nil) ? [message urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}

/*
 * Generate the entry for payment continental card confirmation
 */
- (NSString *)returnEscapedParameterSequencePaymentContCardConfirmationForIssue:(NSString *)issue
                                                                       currency:(NSString *)currency
                                                                         amount:(NSString *)amount
                                                                         email1:(NSString *)email1
                                                                         email2:(NSString *)email2
                                                                   phoneNumber1:(NSString *)phoneNumber1
                                                                   phoneNumber2:(NSString *)phoneNumber2
                                                                       carrier1:(NSString *)carrier1
                                                                       carrier2:(NSString *)carrier2
                                                                        message:(NSString *)message {
	
    NSString *carrierOne = ([phoneNumber1 length] > 0) ? carrier1 : @"";
    NSString *carrierTwo = ([phoneNumber2 length] > 0) ? carrier2 : @"";
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"asuntopropio=%@"\
                                  @"&tipomoneda=%@"\
								  @"&importe=%@"\
								  @"&eMail1=%@"\
								  @"&eMail2=%@"\
								  @"&numCelular1=%@"\
								  @"&numCelular2=%@"\
								  @"&operadorCelular1=%@"\
								  @"&operadorCelular2=%@"\
								  @"&mensajeEmail=%@",
                                  [issue urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [currency urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [amount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierOne urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierTwo urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  (message != nil) ? [message urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}

/*
 * Generate the entry for payment other bank card confirmation
 */
- (NSString *)returnEscapedParameterSequencePaymentOtherBankCardConfirmationIndicatorOP:(NSString*)indicator
                                                                                Service:(NSString*)service
                                                                            Description:(NSString*)description
                                                                             CardNumber:(NSString*)cardNumber
                                                                              ClassCard:(NSString*)classCard
                                                                              destinationBank:(NSString*)destinationBank
                                                                               locality:(NSString *)locality
                                                                               Issue:(NSString *)issue
                                                                            currency:(NSString *)currency
                                                                              amount:(NSString *)amount
                                                                              email1:(NSString *)email1
                                                                              email2:(NSString *)email2
                                                                        phoneNumber1:(NSString *)phoneNumber1
                                                                        phoneNumber2:(NSString *)phoneNumber2
                                                                            carrier1:(NSString *)carrier1
                                                                            carrier2:(NSString *)carrier2
                                                                             message:(NSString *)message {
    
    NSString *carrierOne = ([phoneNumber1 length] > 0) ? carrier1 : @"";
    NSString *carrierTwo = ([phoneNumber2 length] > 0) ? carrier2 : @"";
    
	NSString *parametersString = [NSString stringWithFormat:
                                  @"indicador_OP=%@"\
                                  @"&servicio=%@"\
                                  @"&descripcion=%@"\
                                  @"&num_tarjeta=%@"\
                                  @"&clase_tarjeta=%@"\
                                  @"&banco_destino=%@"\
                                  @"&localidad=%@"\
								  @"&asuntopropio=%@"\
								  @"&importe=%@"\
                                  @"&moneda=%@"\
								  @"&eMail1=%@"\
								  @"&eMail2=%@"\
								  @"&numCelular1=%@"\
								  @"&numCelular2=%@"\
								  @"&operadorCelular1=%@"\
								  @"&operadorCelular2=%@"\
								  @"&mensajeReferencia=%@",
                                  [indicator urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [service urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [description urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [cardNumber urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [classCard urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [destinationBank urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [locality urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [issue urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [amount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [[currency uppercaseString] urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierOne urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierTwo urlEncodeUsingEncoding:NSUTF8StringEncoding],
								  (message != nil) ? [message urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}


/*
 * Generate the entry for payment other bank card confirmation
 */
- (NSString *)returnEscapedParameterSequencePaymentOtherBankCardConfirmationForIssue:(NSString *)issue
                                                                            locality:(NSString *)locality
                                                                            currency:(NSString *)currency
                                                                              amount:(NSString *)amount
                                                                              email1:(NSString *)email1
                                                                              email2:(NSString *)email2
                                                                        phoneNumber1:(NSString *)phoneNumber1
                                                                        phoneNumber2:(NSString *)phoneNumber2
                                                                            carrier1:(NSString *)carrier1
                                                                            carrier2:(NSString *)carrier2
                                                                             message:(NSString *)message {
    
    NSString *carrierOne = ([phoneNumber1 length] > 0) ? carrier1 : @"";
    NSString *carrierTwo = ([phoneNumber2 length] > 0) ? carrier2 : @"";
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"localidad=%@"\
                                  @"&asuntopropio=%@"\
                                  @"&importe=%@"\
                                  @"&moneda=%@"\
                                  @"&eMail1=%@"\
                                  @"&eMail2=%@"\
                                  @"&numCelular1=%@"\
                                  @"&numCelular2=%@"\
                                  @"&operadorCelular1=%@"\
                                  @"&operadorCelular2=%@"\
                                  @"&mensajeReferencia=%@",
                                  [locality urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [issue urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [amount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [[currency uppercaseString] urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [email2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber1 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumber2 urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierOne urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [carrierTwo urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  (message != nil) ? [message urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
}

/*
 * Generate the entry for payment other bank card confirmation
 */
- (NSString *)returnEscapedParameterSequencePaymentOtherBankCardTINConfirmationWithBeneficiary:(NSString *)beneficiary
{
    NSString *parametersString = ([beneficiary length]>0)?[NSString stringWithFormat:
                                  @"&nom_ingresado=%@",
                                   beneficiary]:@"";
    return parametersString;
}
                                    

/*
 * Generate the entry for other bank card payment data operation
 */
- (NSString *)returnEscapedParameterSequencePaymentOtherBankDataForClass:(NSString *)class
                                                                    bank:(NSString *)bank
                                                                 account:(NSString *)account {
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"clase=%@"\
								  @"&banco_destino=%@"\
								  @"&w1ctacargo=%@",
                                  [class urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [bank urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [account urlEncodeUsingEncoding:NSUTF8StringEncoding]];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
}


/*
 * Generate the entry for a third card payment data operation
 */
- (NSString *)returnEscapedParameterSequenceForThirdCard:(NSString *)thirdCard  {
	
	NSString *parametersString = [NSString stringWithFormat:
								  @"tarjeta_tercero=%@",
								  (thirdCard != nil) ? [thirdCard urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
	
	//DLog(@"Operation parameters: %@", parametersString);
	
	return parametersString;
	
}


#pragma mark Url's para el flujo de Operaciones Frecuentes

- (NSString *) returnEscapedParameterSequenceForFrequentOperationGiftCardWithOperation:(NSString *)operation
                                                                 andbeneficiaryAccount:(NSString *)beneficiaryAccount
                                                                           andCurrency:(NSString *)currency
                                                                      andOriginAccount:(NSString *)originAccount
                                                                             andAmount:(NSString *)amount
{
    if(operation == nil)
        operation = @"";
    
    if(beneficiaryAccount == nil)
        beneficiaryAccount = @"";
    
    if(originAccount == nil)
        originAccount = @"";
    
    if(amount == nil)
        amount = @"";
    
    if(currency == nil)
        currency = @"";
    
    if([amount rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        amount = [amount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"Servicio=%@"\
                                  @"&TarjetaDestino=%@"\
                                  @"&MonedaTraspaso=%@"\
                                  @"&CuentaCargo=%@"\
                                  @"&ImporteTraspaso=%@",
                                  [operation urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [beneficiaryAccount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [currency urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [originAccount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [amount urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
}

- (NSString *) returnEscapedParameterSequenceForFrequentOperationOtherBankWithOperation:(NSString *)operation
                                                                  andbeneficiaryAccount:(NSString *)beneficiaryAccount
                                                                     andDestinationBank:(NSString *)destinationBank
                                                                     andBeneficiaryName:(NSString *)beneficiaryName
                                                                       andOriginAccount:(NSString *)originAccount
                                                                              andAmount:(NSString *)amount
                                                                            andCurrency:(NSString *)currency
                                                                         andDescription:(NSString *)description
                                                                      andDocumentNumber:(NSString *)documentNumber
                                                                        andDocumentCode:(NSString *)documentCode
                                                                                 andITF:(BOOL)itf
{
    if(operation == nil)
        operation = @"";
    
    if(beneficiaryAccount == nil)
        beneficiaryAccount = @"";
    
    if(destinationBank == nil)
        destinationBank = @"";
    
    if(originAccount == nil)
        originAccount = @"";
    
    if(amount == nil)
        amount = @"";
    
    if(currency == nil)
        currency = @"";
    
    if(description == nil)
        description = @"";
    
    if(documentNumber == nil)
        documentNumber = @"";
    
    if(documentCode == nil)
        documentCode = @"";
    
    if([amount rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        amount = [amount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"Servicio=%@"\
                                  @"&CuentaAbono=%@"\
                                  @"&BancoDestino=%@"\
                                  @"&NombreBeneficiario=%@"\
                                  @"&CuentaCargo=%@"\
                                  @"&Importe=%@"\
                                  @"&Divisa=%@"\
                                  @"&DescripcionDocumento=%@"\
                                  @"&NumeroDocumento=%@"\
                                  @"&TipoDocumento=%@"\
                                  @"&ITF=%@",
                                  [operation urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [beneficiaryAccount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [destinationBank urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [beneficiaryName urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [originAccount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [amount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [currency urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [description urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [documentNumber urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [documentCode urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  (!itf) ? @"S" : @"N" ];
    return parametersString;
    
}

- (NSString *) returnEscapedParameterSequenceForFrequentOperationThirdAccountWithOperation:(NSString *)operation
                                                                     andbeneficiaryAccount:(NSString *)beneficiaryAccount
                                                                        andBeneficiaryName:(NSString *)beneficiaryName
                                                                          andOriginAccount:(NSString *)originAccount
                                                                                 andAmount:(NSString *)amount
                                                                    andBeneficiaryCurrency:(NSString *)beneficiaryCurrency
                                                                         andOriginCurrency:(NSString *)originCurrency
{
    if(operation == nil)
        operation = @"";
    
    if(beneficiaryAccount == nil)
        beneficiaryAccount = @"";
    
    if(beneficiaryName == nil)
        beneficiaryName = @"";
    
    if(originAccount == nil)
        originAccount = @"";
    
    if(amount == nil)
        amount = @"";
    
    if(beneficiaryCurrency == nil)
        beneficiaryCurrency = @"";
    
    if(originCurrency == nil)
        originCurrency = @"";
    
    
    if([amount rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        amount = [amount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"Servicio=%@"\
                                  @"&CuentaAbono=%@"\
                                  @"&MonedaCuentaAbono=%@"\
                                  @"&NombreBeneficiario=%@"\
                                  @"&CuentaCargo=%@"\
                                  @"&ImporteTraspaso=%@"\
                                  @"&MonedaTraspaso=%@",
                                  [operation urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [beneficiaryAccount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [beneficiaryCurrency urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [beneficiaryName urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [originAccount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [amount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [originCurrency urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
}

- (NSString *) returnEscapedParameterSequenceForFrequentOperationThirdAccountPaymentWithOperation:(NSString *)operation
                                                                            andDestinationAccount:(NSString *)destinationAccount
                                                                        andDestinationAccountType:(NSString *)destinationAccountType
                                                                                   andBeneficiary:(NSString *)beneficiary
                                                                                          andFlag:(NSString *)flag
                                                                                      andCurrency:(NSString *)currency
                                                                                 andOriginAccount:(NSString *)originAccount
                                                                                        andAmount:(NSString *)amount
{
    if(operation == nil)
        operation = @"";
    
    if(destinationAccount == nil)
        destinationAccount = @"";
    
    if(destinationAccountType == nil)
        destinationAccountType = @"";
    
    if(beneficiary == nil)
        beneficiary = @"";
    
    if(flag == nil)
        flag = @"";
    
    if(currency == nil)
        currency = @"";
    
    if(originAccount == nil)
        originAccount = @"";
    
    if(amount == nil)
        amount = @"";
    
    
    if([amount rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        amount = [amount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"Servicio=%@"\
                                  @"&TarjetaDestino=%@"\
                                  @"&TipoTarjetaDestino=%@"\
                                  @"&TitularTarjetaDestino=%@"\
                                  @"&FlagTerceros=%@"\
                                  @"&Moneda=%@"\
                                  @"&CuentaCargo=%@"\
                                  @"&ImporteTraspaso=%@",
                                  [operation urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [destinationAccount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [destinationAccountType urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [beneficiary urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [flag urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [currency urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [originAccount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [amount urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
}


- (NSString *) returnEscapedParameterSequenceForFrequentOperationOtherBankPaymentWithOperation:(NSString *)operation
                                                                         andDestinationAccount:(NSString *)destinationAccount
                                                                     andDestinationAccountType:(NSString *)destinationAccountType
                                                                        andNameDestinationBank:(NSString *)nameDestinationBank
                                                                                   andBankCode:(NSString *)bankCode
                                                                               andLocalityName:(NSString *)localityName
                                                                               andLocalityCode:(NSString *)localityCode
                                                                                andBeneficiary:(NSString *)beneficiary
                                                                                   andCurrency:(NSString *)currency
                                                                              andOriginAccount:(NSString *)originAccount
                                                                                     andAmount:(NSString *)amount
{
    if(operation == nil)
        operation = @"";
    
    if(destinationAccount == nil)
        destinationAccount = @"";
    
    if(destinationAccountType == nil)
        destinationAccountType = @"";
    
    if(nameDestinationBank == nil)
        nameDestinationBank = @"";
    
    if(bankCode == nil)
        bankCode = @"";
    
    if(localityName == nil)
        localityName = @"";
    
    if(localityCode == nil)
        localityCode = @"";
    
    if(beneficiary == nil)
        beneficiary = @"";
    
    if(currency == nil)
        currency = @"";
    
    if(originAccount == nil)
        originAccount = @"";
    
    if(amount == nil)
        amount = @"";
    
    
    if([amount rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        amount = [amount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"Servicio=%@"\
                                  @"&TarjetaAbono=%@"\
                                  @"&ClaseTarjeta=%@"\
                                  @"&NombreBanco=%@"\
                                  @"&CodigoBanco=%@"\
                                  @"&Lugar=%@"\
                                  @"&NombreLugar=%@"\
                                  @"&NombreBeneficiario=%@"\
                                  @"&CuentaCargo=%@"\
                                  @"&ImporteTraspaso=%@"\
                                  @"&MonedaTraspaso=%@",
                                  [operation urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [destinationAccount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [destinationAccountType urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [nameDestinationBank urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [bankCode urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [localityCode urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [localityName urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [beneficiary urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [originAccount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [amount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [currency urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
    
}

- (NSString *) returnEscapedParameterSequenceForRechargeMovistarCellPhoneFrequentOperationWithOperation:(NSString *)operation
                                                                                    andCompany:(NSString *)company
                                                                                andServiceType:(NSString *)serviceType
                                                                            andCellPhoneNumber:(NSString *)cellPhoneNumber
                                                                                    andAccount:(NSString *)account
                                                                                     andAmount:(NSString *)amount
{
    if(operation == nil)
        operation = @"";
    
    if(company == nil)
        company = @"";
    
    if(serviceType == nil)
        serviceType = @"";
    
    if(cellPhoneNumber == nil)
        cellPhoneNumber = @"";
    
    if(account == nil)
        account = @"";
    
    if(amount == nil)
        amount = @"";
    
    if([amount rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        amount = [amount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"Servicio=%@"\
                                  @"&NombreEmpresa=%@"\
                                  @"&TipoServicio=%@"\
                                  @"&Suministro=%@"\
                                  @"&CuentaCargo=%@"\
                                  @"&importeRecarga=%@",
                                  [operation urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [company urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [serviceType urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [cellPhoneNumber urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [account urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [amount urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
    
}

- (NSString *) returnEscapedParameterSequenceForRechargeClaroCellPhoneFrequentOperationWithOperation:(NSString *)operation
                                                                                andServiceType:(NSString *)serviceType
                                                                                    andAccount:(NSString *)account
                                                                            andCellPhoneNumber:(NSString *)cellPhoneNumber{
    if(operation == nil)
        operation = @"";
    
    if(serviceType == nil)
        serviceType = @"";
    
    if(account == nil)
        account = @"";
    
    if(cellPhoneNumber == nil)
        cellPhoneNumber = @"";
    
   
    NSString *parametersString = [NSString stringWithFormat:
                                  @"ServicioClaro=%@"\
                                  @"&TipoServicio=%@"\
                                  @"&CuentaCargo=%@"\
                                  @"&Celular=%@",
                                  [operation urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [serviceType urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [account urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [cellPhoneNumber urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
    
}

- (NSString *) returnEscapedParameterSequenceForServiceClaroCellPhoneFrequentOperationWithOperation:(NSString *)operation
                                                                                     andServiceType:(NSString *)serviceType
                                                                                 andCellPhoneHolder:(NSString *)cellPhoneHolder
                                                                                         andAccount:(NSString *)account
                                                                                       andCellPhone:(NSString *)cellPhone
{
    if(operation == nil)
        operation = @"";
    
    if(serviceType == nil)
        serviceType = @"";
    
    if(cellPhoneHolder == nil)
        cellPhoneHolder = @"";
    
    if(account == nil)
        account = @"";
    
    if(cellPhone == nil)
        cellPhone = @"";
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"Servicio=%@"\
                                  @"&TipoServicio=%@"\
                                  @"&TitularCelular=%@"\
                                  @"&CuentaCargo=%@"\
                                  @"&Celular=%@",
                                  [operation urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [serviceType urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [cellPhoneHolder urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [account urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [cellPhone urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
    
}


- (NSString *) returnEscapedParameterSequenceForServiceMovistarCellPhoneFrequentOperationWithOperation:(NSString *)operation
                                                                                    andCompany:(NSString *)company
                                                                                andServiceType:(NSString *)serviceType
                                                                            andCellPhoneNumber:(NSString *)cellPhoneNumber
                                                                                     andHolder:(NSString *)holder
                                                                                    andAccount:(NSString *)account
{
    if(operation == nil)
        operation = @"";
    
    if(company == nil)
        company = @"";
    
    if(serviceType == nil)
        serviceType = @"";
    
    if(cellPhoneNumber == nil)
        cellPhoneNumber = @"";
    
    if(holder == nil)
        holder = @"";
    
    if(account == nil)
        account = @"";

    
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"Servicio=%@"\
                                  @"&NombreEmpresa=%@"\
                                  @"&TipoServicio=%@"\
                                  @"&Suministro=%@"\
                                  @"&Titular=%@"\
                                  @"&CuentaCargo=%@",
                                  [operation urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [company urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [serviceType urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [cellPhoneNumber urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [holder urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [account urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
    
}

- (NSString *) returnEscapedParameterSequenceForServiceWaterFrequentOperationWithOperation:(NSString *)operation
                                                                                andCompany:(NSString *)company
                                                                           andSupplyNumber:(NSString *)supplyNumber
                                                                                 andHolder:(NSString *)holder
                                                                            andServiceType:(NSString *)serviceType
                                                                                andAccount:(NSString *)account
                                                                                 andAmount:(NSString *)amount
                                                                               andCurrency:(NSString *)currency
{
    if(operation == nil)
        operation = @"";
    
    if(company == nil)
        company = @"";
    
    if(supplyNumber == nil)
        supplyNumber = @"";
    
    if(holder == nil)
        holder = @"";
    
    if(serviceType == nil)
        serviceType = @"";
    
    if(account == nil)
        account = @"";
    
    if(amount == nil)
        amount = @"";
    
    if(currency == nil)
        currency = @"";
   
    
    if([amount rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        amount = [amount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"Servicio=%@"\
                                  @"&NombreEmpresa=%@"\
                                  @"&Suministro=%@"\
                                  @"&Titular=%@"\
                                  @"&TipoServicio=%@"\
                                  @"&CuentaCargo=%@"\
                                  @"&ImporteTraspaso=%@"\
                                  @"&MonedaTraspaso=%@",
                                  [operation urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [company urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [supplyNumber urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [holder urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [serviceType urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [account urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [amount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [currency urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
    
}


- (NSString *) returnEscapedParameterSequenceForFrequentOperationInstitutionAndCompaniesWithOperation:(NSString *)operation
                                                                                          andCodMatri:(NSString *)codMatri
                                                                                            andsClase:(NSString *)clase
                                                                                          andsEntidad:(NSString *)entidad
                                                                                       andsCodEntidad:(NSString *)codEntidad
                                                                                           andArreglo:(NSString *)arreglo
                                                                                      andsDescripcion:(NSString *)descripcion
                                                                                       andCuentaCargo:(NSString *)cuentaCargo
                                                                                        andHolderNmae:(NSString *)sNombre
{
    if(sNombre == nil)
        sNombre = @"";
    
    if(operation == nil)
        operation = @"";
    
    if(codMatri == nil)
        codMatri = @"";
    
    if(clase == nil)
        clase = @"";
    
    if(entidad == nil)
        entidad = @"";
    
    if(codEntidad == nil)
        codEntidad = @"";
    
    if(arreglo == nil)
        arreglo = @"";
    
    if(descripcion == nil)
        descripcion = @"";
    
    if(cuentaCargo == nil)
        cuentaCargo = @"";

    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"sNombre=%@"\
                                  @"&Servicio=%@"\
                                  @"&CodMatri=%@"\
                                  @"&sClase=%@"\
                                  @"&sEntidad=%@"\
                                  @"&sCodEntidad=%@"\
                                  @"&arreglo=%@"\
                                  @"&sDescripcion=%@"\
                                  @"&CuentaCargo=%@",
                                  [sNombre urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [operation urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [codMatri urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [clase urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [entidad urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [codEntidad urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [arreglo urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [descripcion urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [cuentaCargo urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
    
}

- (NSString *) returnEscapedParameterSequenceForFrequentOperationTwoStepWithChannel:(NSString *)channel
                                                                      andShortAlias:(NSString *)shortAlias
                                                                      andLargeAlias:(NSString *)largeAlias
                                                                             andDay:(NSString *)day
                                                                         andCellSMS:(NSString *)cellSMS
                                                                     andCellSMSCode:(NSString *)cellSMSCode
                                                                           andEmail:(NSString *)email
                                                                        anEmailCode:(NSString *)emailCode
{
    if(channel == nil)
        channel = @"";
    
    if(shortAlias == nil)
        shortAlias = @"";
    
    if(largeAlias == nil)
        largeAlias = @"";
    
    if(day == nil || [day isEqualToString:@""])
        day = @"0";
    
    if(cellSMS == nil)
        cellSMS = @"";
    
    if(cellSMSCode == nil)
        cellSMSCode = @"";
    
    if(email == nil)
        email = @"";
    
    if(emailCode == nil)
        emailCode = @"";

    NSMutableString *parametersString = [NSMutableString stringWithFormat:
                                  @"Canales=%@"\
                                  @"&Alias=%@"\
                                  @"&AliasCorto=%@"\
                                  @"&DiaAviso=%@",
                                  [channel urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [largeAlias urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [shortAlias urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [day urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    
    if(![day isEqualToString:@"0"])
    {
        [parametersString appendFormat:
         @"&CelSMS=%@"\
         @"&Email=%@"\
         @"&CodCelSMS=%@"\
         @"&CodEmail=%@",
         [cellSMS urlEncodeUsingEncoding:NSUTF8StringEncoding],
         [email urlEncodeUsingEncoding:NSUTF8StringEncoding],
         ([cellSMSCode isEqualToString:@""] || cellSMSCode == nil) ? @"0" : [cellSMSCode urlEncodeUsingEncoding:NSUTF8StringEncoding],
         ([emailCode isEqualToString:@""] || emailCode == nil) ? @"0" : [emailCode urlEncodeUsingEncoding:NSUTF8StringEncoding]];
        
	}
    return parametersString;
    
    
}

- (NSString *)returnEscapedParameterSequenceForFrequentOperationStepThreeWithSecondFactor:(NSString *)secondKeyFactor {
    
    if(secondKeyFactor == nil)
        secondKeyFactor = @"";
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"ClaveSegundoFactor=%@",
								  [secondKeyFactor urlEncodeUsingEncoding:NSUTF8StringEncoding] ];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
	
}



#pragma mark Url's para el flujo de SafetyPay

- (NSString *) returnEscapedParameterSequenceForSafetyPayTransactionInfoWithTransactionNumber: (NSString *) transactionNumber AndAmount : (NSString *) amount{
    if(transactionNumber == nil)
        transactionNumber = @"";
    
    if(amount == nil)
        amount = @"";
    
    if([transactionNumber rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        transactionNumber = [transactionNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if([amount rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        amount = [amount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"Identificador=%@"\
                                  @"&Importe=%@",
                                  [transactionNumber urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [amount urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
}

- (NSString *) returnEscapedParameterSequenceForSafetyPayPaymentInfoWithAccountNumber:(NSString *) accountNumber
                                                                               email1:(NSString *)email1
                                                                               email2:(NSString *)email2
                                                                         phoneNumber1:(NSString *)phoneNumber1
                                                                         phoneNumber2:(NSString *)phoneNumber2
                                                                             carrier1:(NSString *)carrier1
                                                                             carrier2:(NSString *)carrier2
                                                                              message:(NSString *)message
{
    
    if(accountNumber == nil)
        accountNumber = @"";
    
    if(email1 == nil)
        email1 = @"";
    
    if(email2 == nil)
        email2 = @"";
    
    if(carrier1 == nil)
        carrier1 = @"";
    
    if(carrier2 == nil)
        carrier2 = @"";
    
    if(phoneNumber1 == nil)
        phoneNumber1 = @"";
    
    if(phoneNumber2 == nil)
        phoneNumber2 = @"";
    
    if(message == nil)
        message = @"";
    
    if([accountNumber rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        accountNumber = [accountNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if([phoneNumber1 rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        phoneNumber1 = [phoneNumber1 stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    
    if([phoneNumber2 rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        phoneNumber2 = [phoneNumber2 stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    
    if([carrier1 rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        carrier1 = [carrier1 stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    
    if([carrier2 rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        carrier2 = [carrier2 stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    
    if([email1 rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        email1 = [email1 stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    
    if([email2 rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        email2 = [email2 stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    
    if([message rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        message = [message stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    
    NSString *phoneNumberComplete;
    NSString *carrierComplete;
    
    if([phoneNumber2 isEqualToString:@""] && [carrier2 isEqualToString:@""])
    {
        phoneNumberComplete = phoneNumber1;
        carrierComplete = carrier1;
    }
    else
    {
        phoneNumberComplete = [NSString stringWithFormat:@"%@,%@",phoneNumber1,phoneNumber2];
        carrierComplete = [NSString stringWithFormat:@"%@,%@",carrier1,carrier2];
    }
    
    NSString *emailComplete;
    
    if([email2 isEqualToString:@""])
    {
        emailComplete = email1;
    }
    else
    {
        emailComplete = [NSString stringWithFormat:@"%@,%@",email1,email2];
    }
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"Cuenta=%@"\
                                  @"&Celular=%@"\
                                  @"&Correo=%@"\
                                  @"&Operadora=%@"\
                                  @"&MensajeEmail=%@",
                                  [accountNumber urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [phoneNumberComplete urlEncodeUsingEncoding: NSUTF8StringEncoding],
                                  [emailComplete urlEncodeUsingEncoding: NSUTF8StringEncoding],
                                  [carrierComplete urlEncodeUsingEncoding: NSUTF8StringEncoding],
                                  [message urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    return parametersString;
    
}

- (NSString *)returnEscapedParameterSequenceSafetyPayResultFromSecondKeyFactor:(NSString *)secondKeyFactor {
    
	NSString *parametersString = [NSString stringWithFormat:
								  @"ClaveSegundoFactor=%@",
								  (secondKeyFactor != nil) ? [secondKeyFactor urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
	
}

- (NSString *)returnEscapedParameterFrequentOperationInitial:(NSString *)nextPage{
    
    NSString *parameterString = @"";
    
    parameterString = [NSString stringWithFormat:@"NextPage=%@", (nextPage != nil)?nextPage:@""];
    
    return parameterString;
}

/**
 * Generate the entry for a frequent operation execute data
 *
 * @param codService Unique code service
 * @param afilitionNum The afiliation number
 * @param asociationNick the asociation nick for the operation
 * @param afilitationType the afiliation type
 */
- (NSString *)returnEscapedParameterFrequentOperationExecuteCodService:(NSString *)codService
                                                          AfilitionNum:(NSString *)afilitionNum
                                                        AsociationNick:(NSString *)asociationNick
                                                       AfilitationType:(NSString *)afilitationType{
    
    NSString *parameterString = @"";
    
    parameterString = [NSString stringWithFormat:
                       @"CodServicio=%@"\
                       @"&NumAfil=%@"\
                       @"&aliasAsoc=%@"\
                       @"&formAfil=%@",
                       (codService != nil)?codService:@"",
                       (afilitionNum != nil)?afilitionNum:@"",
                       (asociationNick != nil)?asociationNick:@"",
                       (afilitationType != nil)?afilitationType:@""];
    
    return parameterString;
    
}

#pragma mark Url's para el flujo de INSCRIPCION DE CUENTAS
- (NSString *) returnEscapedParameterSequenceForThirdAccountRegistration:(NSString *)numberAccount officeNumber:(NSString*)officeNumber  AccountType : (NSString *) accountType andNickname:(NSString*)nickname{
    
    if(officeNumber== nil)
        officeNumber = @"";

    if(numberAccount == nil)
        numberAccount = @"";
    
    if(accountType == nil)
        accountType = @"";
    
    if(nickname == nil)
        nickname = @"";
    
    if([officeNumber rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        officeNumber = [officeNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    if([numberAccount rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        numberAccount = [numberAccount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if([accountType rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        accountType = [accountType stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if([nickname rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        nickname = [nickname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"alias=%@"\
                                  @"&tipoOperacion=%@"
                                  @"&Oficinab=%@"\
                                  @"&Cuentab=%@",
                                  [nickname urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [accountType urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [officeNumber urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [numberAccount urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
}

- (NSString *) returnEscapedParameterSequenceForOtherBankAccountRegistration:(NSString *)numberAccount entityNumber:(NSString*)entityNumber officeNumber:(NSString*)officeNumber  AccountType : (NSString *)accountType ccNumber:(NSString*)ccNumber  andNickname:(NSString*)nickname{
    
    if(entityNumber== nil)
        officeNumber = @"";
    
    if(ccNumber == nil)
        ccNumber = @"";
    
    if(officeNumber== nil)
        officeNumber = @"";
    
    if(numberAccount == nil)
        numberAccount = @"";
    
    if(accountType == nil)
        accountType = @"";
    
    if(nickname == nil)
        nickname = @"";
    
    if([officeNumber rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        officeNumber = [officeNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if([numberAccount rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        numberAccount = [numberAccount stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if([accountType rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        accountType = [accountType stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if([nickname rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        nickname = [nickname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"alias=%@"\
                                  @"&tipoOperacion=%@"
                                  @"&w1banco=%@"\
                                  @"&w1oficina=%@"\
                                  @"&w1cuenta=%@"\
                                  @"&w1control=%@",
                                  [nickname urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [accountType urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [entityNumber urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [officeNumber urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [numberAccount urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [ccNumber urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
}

- (NSString *) returnEscapedParameterSequenceForThirdAccountRegistrationWithAccountType : (NSString *) accountType andNickname:(NSString*)nickname andSecondFactorKey:(NSString *)secondFactorKey{
    
    
    
    if(accountType == nil)
        accountType = @"";
    
    if(nickname == nil)
        nickname = @"";
    
    if([accountType rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        accountType = [accountType stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if([nickname rangeOfCharacterFromSet: [NSCharacterSet whitespaceCharacterSet]].location != NSNotFound)
        nickname = [nickname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"&alias_elim=%@"\
                                  @"&tipoOpera=%@"\
                                  @"&ClaveSegundoFactor=%@",
                                  [nickname urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [accountType urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                  [secondFactorKey urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;
    
}

- (NSString *) returnEscapedParameterSequenceForThirdAccountRegistrationNickname:(NSString*)nickname{
    
    if(nickname == nil)
        nickname = @"";
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"alias=%@",
                                  [nickname urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    return parametersString;

}
-(NSString*)returnEscapedParamenterSequenceForThirdAccountListWithType:(NSString*)operationtype
{
    if(operationtype == nil)
        operationtype = @"";
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"tipo=%@",
                                  [operationtype urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    DLog(@"Operation parameters: %@", parametersString);
    return parametersString;
}

- (NSString *)returnEscapedParameterThirdAccountListInitial:(NSString *)nextPage andOperationType:(NSString*)operationtype
{
    
    NSString *parameterString = @"";
    
    parameterString = [NSString stringWithFormat:@"tipo=%@"\
                                                 @"&NextPage=%@",[operationtype urlEncodeUsingEncoding:NSUTF8StringEncoding],
                                                 (nextPage != nil)?nextPage:@""];
    DLog(@"Operation parameters: %@", parameterString);
    return parameterString;
}



- (NSString *)returnEscapedParameterSequenceIncrementOfLineConfirmFromCodeCard:(NSString*)codeCard
                                                             amountToIncrement:(NSString*)amountToIncrement
                                                                         email:(NSString*)emailr {
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"op_codigo_tarjeta_in=%@"\
                                  @"&op_monto_incremento_in=%@"\
                                  @"&op_email_in=%@",
    (codeCard != nil ? [codeCard urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""),
    (amountToIncrement != nil ? [amountToIncrement urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""),
    (emailr != nil ? [emailr urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"")];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
    
}

- (NSString *)returnEscapedParameterSequenceIncrementOfLineResultFromSecondKeyFactor:(NSString *)secondKeyFactor {
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"ClaveSegundoFactor=%@",
                                  (secondKeyFactor != nil) ? [secondKeyFactor urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
    
}


- (NSString *)returnEscapedParameterSequenceFastLoanResultFromSecondKeyFactor:(NSString *)secondKeyFactor {
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"ClaveSegundoFactor=%@",
                                  (secondKeyFactor != nil) ? [secondKeyFactor urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
    
}

- (NSString *)returnEscapedParameterSequenceSalaryAdvanceResultFromSecondKeyFactor:(NSString *)secondKeyFactor {
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"ClaveSegundoFactor=%@",
                                  (secondKeyFactor != nil) ? [secondKeyFactor urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
    
}

- (NSString *)returnEscapedParameterSequenceSalaryAdvanceAffiliationConfirmFromSecondKeyFactor:(NSString *)secondKeyFactor :(NSString *)email {
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"ClaveSegundoFactor=%@"\
                                  @"&op_email=%@",
                                  (secondKeyFactor != nil) ? [secondKeyFactor urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"",
                                  (email != nil) ? [email urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    return parametersString;
    
}

-(NSString*)returnEscapedParameterSequenceFastLoanSimulationFromAmount:(NSString*)amount
                                                                  term:(NSString*)term
                                                                dayOfPay:(NSString*)dayOfPay
                                                             quotaType:(NSString*)quotaType
                                                           monthDobles:(NSString*)monthDobles
                                                          accountIndex:(NSString*)accountIndex
                                                        emailSelection:(NSString*)emailSelection
                                                         emailEditable:(NSString*)emailEditable
                                                                action:(NSString*)action{
    
    NSString *parametersString = [NSString stringWithFormat:
                                  @"op_monto_in=%@"\
                                  @"&op_plazo_in=%@"\
                                  @"&op_dia_pago_in=%@"\
                                  @"&op_tipo_cuota_in=%@"\
                                  @"&op_meses_cuotas_in=%@"\
                                  @"&op_indice_cta_in=%@"\
                                  @"&op_correo_electronico_in=%@"\
                                  @"&op_correo_electronico_adicional_in=%@"\
                                  @"&op_accion_interna_in=%@",
                    (amount != nil ? [amount urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""),
                    (term != nil ? [term urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""),
                    (dayOfPay != nil ? [dayOfPay urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""),
                    (quotaType != nil ? [quotaType urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""),
                    (monthDobles != nil ? [monthDobles urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""),
                    (accountIndex != nil ? [accountIndex urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""),
                    (emailSelection != nil ? [emailSelection urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""),
                    (emailEditable != nil ? [emailEditable urlEncodeUsingEncoding:NSUTF8StringEncoding] : @""),
                     (action != nil ? [action urlEncodeUsingEncoding:NSUTF8StringEncoding] : @"")];
    
    //DLog(@"Operation parameters: %@", parametersString);
    
    
    return parametersString;


}


@end
