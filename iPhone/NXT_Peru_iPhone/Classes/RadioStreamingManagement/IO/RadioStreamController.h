//
//  RadioStreamController.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/30/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MPNowPlayingInfoCenter.h>
#import <MediaPlayer/MPMediaItem.h>

@class AudioStreamer;

typedef enum {
    npia_Connecting = 0,
    npia_Play,
    npia_Stop,
    npia_ErrorNetwork,
    npia_Error
    
} RadioPlayerStatus;

@protocol RadioStreamingDelegate <NSObject>

@optional
-(void)updateTrackName:(NSString*)trackName;
-(void)radioStreamingStatus:(RadioPlayerStatus)status;
-(void)streamingValueAudioLeft:(NSArray*)valueLeft valueAudioRight:(NSArray*)valueRight;
@end


@interface RadioStreamController : NSObject
{
   BOOL isPlayingRadio_;
  
   NSString *lastTrackName_;
    
    NSURL *streamURL_;
    
    AudioStreamer *streamer_;
    NSTimer *levelMeterUpdateTimer;
    
    id<RadioStreamingDelegate> radioStreamingDelegate_;
}
/**
 * Provides read-write access to the isPlayingRadio
 */
@property (nonatomic, readwrite, assign) id<RadioStreamingDelegate> radioStreamingDelegate;

/**
 * Provides read-write access to the Streamer
 */
@property (nonatomic, readwrite,retain) AudioStreamer *streamer;
/**
 * Provides read-write access to the isPlayingRadio
 */
@property (nonatomic, readwrite,retain) NSURL *streamURL;
/**
 * Provides read-write access to the isPlayingRadio
 */
@property (nonatomic, readwrite,retain) NSString *lastTrackName;
/**
 * Provides read-write access to the isPlayingRadio
 */
@property (nonatomic, readwrite, assign) BOOL isPlayingRadio;


-(id)initWithURL:(NSString*)url;

-(void)playRadioStream;
-(void)stopRadioStream;
-(void)stopFinalRadioStream;
-(void)toggleStreamAction;
@end
