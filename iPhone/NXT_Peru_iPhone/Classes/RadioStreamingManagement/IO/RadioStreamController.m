//
//  RadioStreamController.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/30/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "RadioStreamController.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "AudioStreamer.h"
#import "Reachability.h"
#import <MediaPlayer/MPRemoteCommandCenter.h>
#import <MediaPlayer/MPRemoteCommand.h>


@implementation RadioStreamController


@synthesize isPlayingRadio = isPlayingRadio_;
@synthesize radioStreamingDelegate = radioStreamingDelegate_;
@synthesize lastTrackName = lastTrackName_;
@synthesize streamURL = streamURL_;
@synthesize streamer = streamer_;

//@"http://65.60.2.26:8042/stream"
-(id)initWithURL:(NSString*)url{
    self = [super init];
    if(self) {
        
        isPlayingRadio_ = NO;
        
        streamURL_ =[[NSURL alloc] initWithString:url];
     
        
    }
    return self;
}


-(void)playRadioStream{
    
    [self createStreamer];
    
    [self.radioStreamingDelegate radioStreamingStatus:npia_Connecting];

    [streamer_ start];

    
    
}

-(void)stopFinalRadioStream{
    [streamer_ stop];
}
-(void)stopRadioStream{
    isPlayingRadio_ = NO;
    lastTrackName_ = @"";
    
    
    
    [streamer_ pause];
    [streamer_ stop];
    [self.radioStreamingDelegate radioStreamingStatus:npia_Stop];
    
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:nil];
}



-(void)setInfoSong:(NSString*)trackName{
    NSMutableDictionary *songInfo = [[NSMutableDictionary alloc] init];
    MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc] initWithImage:[[ImagesCache getInstance] imageNamed:IMG_BBVA_RADIO_LOGO_ART]];
    if(trackName.length == 0){
        [songInfo setObject:@"Estas Escuchando Radio BBVA" forKey:MPMediaItemPropertyTitle];
    }
    else{
         [songInfo setObject:trackName forKey:MPMediaItemPropertyTitle];
         [songInfo setObject:(self.isPlayingRadio ?  @(1.0f):@(0.0f) ) forKey:MPNowPlayingInfoPropertyPlaybackRate];
        
    }
    [songInfo setObject:@"Radio BBVA" forKey:MPMediaItemPropertyArtist];
   // [songInfo setObject:@"" forKey:MPMediaItemPropertyAlbumTitle];
    [songInfo setObject:albumArt forKey:MPMediaItemPropertyArtwork];
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:songInfo];
    
    if (floor(NSFoundationVersionNumber) >= NSFoundationVersionNumber_iOS_7_1){
        [MPRemoteCommandCenter sharedCommandCenter].skipBackwardCommand.enabled = NO;
        [MPRemoteCommandCenter sharedCommandCenter].seekBackwardCommand.enabled = NO;
        [MPRemoteCommandCenter sharedCommandCenter].previousTrackCommand.enabled = NO;
        
        [MPRemoteCommandCenter sharedCommandCenter].nextTrackCommand.enabled = NO;
    }
  
    
  
}




-(void)dealloc{
    
    [super dealloc];
    
}
//
// destroyStreamer
//
// Removes the streamer, the UI update timer and the change notification
//
- (void)destroyStreamer
{
    if (streamer_)
    {
        
        if (floor(NSFoundationVersionNumber) >= NSFoundationVersionNumber_iOS_7_1){
            [[MPRemoteCommandCenter sharedCommandCenter].playCommand removeTarget:self];
            [[MPRemoteCommandCenter sharedCommandCenter].pauseCommand removeTarget:self];
        }

        [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:nil];
        
        [[NSNotificationCenter defaultCenter]
         removeObserver:self
         name:ASStatusChangedNotification
         object:streamer_];
        
        [[NSNotificationCenter defaultCenter]
         removeObserver:self
         name:ASUpdateMetadataNotification
         object:streamer_];

        
        [streamer_ stop];
        [streamer_ release];
        streamer_ = nil;
    }
}
-(void)play{
    [streamer_ pause];
}
//
// createStreamer
//
// Creates or recreates the AudioStreamer object.
//
- (void)createStreamer
{
    if (streamer_)
    {
        return;
    }
    
    
    
 
    [self destroyStreamer];
    
    if (floor(NSFoundationVersionNumber) >= NSFoundationVersionNumber_iOS_7_1){
          [[MPRemoteCommandCenter sharedCommandCenter].playCommand addTarget:self action:@selector(play)];
          [[MPRemoteCommandCenter sharedCommandCenter].pauseCommand addTarget:self action:@selector(play)];
    }
    
    NSString *escapedValue =
    [(NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                         nil,
                                                         (CFStringRef)[streamURL_ relativeString] ,
                                                         NULL,
                                                         NULL,
                                                         kCFStringEncodingUTF8)
     autorelease];
    
    NSURL *url = [NSURL URLWithString:escapedValue];
    streamer_ = [[AudioStreamer alloc] initWithURL:url] ;
    
    levelMeterUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:.1
                                                             target:self
                                                           selector:@selector(updateLevelMeters:)
                                                           userInfo:nil 
                                                            repeats:YES];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(playbackStateChanged:)
     name:ASStatusChangedNotification
     object:streamer_];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(metadataChanged:)
     name:ASUpdateMetadataNotification
     object:streamer_];
    
}
- (void)updateLevelMeters:(NSTimer *)timer {
    if([streamer_ isMeteringEnabled] /*&& self.appDelegate.uiIsVisible*/) {
      
        float left =[streamer_ averagePowerForChannel:0];
        float peakLeft =[streamer_ peakPowerForChannel:0];
        float right =[streamer_ averagePowerForChannel:([streamer_ numberOfChannels] > 1 ? 1 : 0)];
        float peakRight =[streamer_ peakPowerForChannel:([streamer_ numberOfChannels] > 1 ? 1 : 0)];
        
       // NSLog(@"iz : %f  max:%f",left,peakLeft);
       // NSLog(@"der %f max:%f",right,peakRight);
        NSArray *leftValues = @[@(left),@(peakLeft),@((left+peakLeft)/2)];
        NSArray *rigthValues =@[@(right),@(peakRight),@((right+peakRight)/2)];
        
        [radioStreamingDelegate_ streamingValueAudioLeft:leftValues valueAudioRight:rigthValues];
    }
}
//
// playbackStateChanged:
//
// Invoked when the AudioStreamer
// reports that its playback status has changed.
//
- (void)playbackStateChanged:(NSNotification *)aNotification
{
    if ([streamer_ isWaiting])
    {
        
        NSLog(@"Esperando");
          isPlayingRadio_ = NO;
        [streamer_ setMeteringEnabled:NO];
        [radioStreamingDelegate_ radioStreamingStatus:npia_Connecting];
    }
    else if ([streamer_ isPlaying])
    {
        
        NSLog(@"Sonando");
        isPlayingRadio_ = YES;
        [streamer_ setMeteringEnabled:YES];
        [radioStreamingDelegate_ radioStreamingStatus:npia_Play];
    }
    else if ([streamer_ isPaused])
    {
        NSLog(@"Pausado");
        isPlayingRadio_ = NO;
        [streamer_ setMeteringEnabled:NO];
        [radioStreamingDelegate_ radioStreamingStatus:npia_Stop];
    }
    else if ([streamer_ isIdle])
    {
        

        
          isPlayingRadio_ = NO;
        
        AudioStreamer *temp = (AudioStreamer *)[aNotification object];
        
        NSLog(@"ENtre a isIdle error %i",[temp errorCode]);
        
        if([temp errorCode]!= AS_NO_ERROR ){
            
            Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
            if (networkStatus == NotReachable){
                 [radioStreamingDelegate_ radioStreamingStatus:npia_ErrorNetwork];
            }else{
                [radioStreamingDelegate_ radioStreamingStatus:npia_Error];
            }
            
        
           
        }else{
            [radioStreamingDelegate_ radioStreamingStatus:npia_Stop];
        }
        [self destroyStreamer];

        
        
    }
}


- (void)metadataChanged:(NSNotification *)aNotification
{
    NSArray *metaParts = [[[aNotification userInfo] objectForKey:@"metadata"] componentsSeparatedByString:@";"];
    NSString *item;
    NSMutableDictionary *hash = [[NSMutableDictionary alloc] init];
    for (item in metaParts) {
        // split the key/value pair
        NSArray *pair = [item componentsSeparatedByString:@"="];
        // don't bother with bad metadata
        if ([pair count] == 2)
            [hash setObject:[pair objectAtIndex:1] forKey:[pair objectAtIndex:0]];
    }
    
    // do something with the StreamTitle
    NSString *streamString = [[hash objectForKey:@"StreamTitle"] stringByReplacingOccurrencesOfString:@"'" withString:@""];
  
    lastTrackName_ = [streamString retain];
    [radioStreamingDelegate_ updateTrackName:streamString];
    [self setInfoSong:lastTrackName_];

  
}

-(void)toggleStreamAction{
    [streamer_ pause];
}


@end
