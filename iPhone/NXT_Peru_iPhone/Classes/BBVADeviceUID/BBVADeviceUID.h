/*
 * Copyright (c) 2013 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>


/**
 * Provides selectors to obtain different types of device UIDs.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BBVADeviceUID : NSObject

#pragma mark -
#pragma mark Device UID

/**
 * Returns the device UID. When vendorIdentifier is avaible, it is the value returned, when it is not available, an
 * alternative value based on SecureUDID is returned.
 *
 * @return The device UID.
 */
+ (NSString *)deviceUID;

/**
 * Returns the default SecureUDID.
 *
 * @return The default SecureUDID.
 */
+ (NSString *)secureUID;

/**
 * Returns the SecureUDID for the provided domain and key.
 *
 * @param domain The domain to calculate the value for.
 * @param key The key to calculate the key for.
 * @return The SecureUDID for the provided domain and key.
 */
+ (NSString *)secureUIDWithDomain:(NSString *)domain
                              key:(NSString *)key;

@end
