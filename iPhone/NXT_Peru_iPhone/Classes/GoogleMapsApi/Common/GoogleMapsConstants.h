/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#pragma mark -
#pragma mark Result status

/**
 * Defines the operation OK status result
 */
#define GOOGLE_MAPS_OPERATION_OK_STATUS                                         @"OK"

/**
 * Defines the GoogleMaps geocoding API administrative area second level type (in Spain it equals the province)
 */
#define GOOGLE_MAPS_ADMINISTRATIVE_AREA_SECOND_LEVEL_TYPE                       @"administrative_area_level_2"

/**
 * Defines the GoogleMaps geocoding API country type
 */
#define GOOGLE_MAPS_COUNTRY_TYPE                                                @"country"
