/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


//Forward declarations
@class GoogleMapsGeocodingResult;


/**
 * Provides utility tools to help analyzing Google Maps API responses, or creating requests
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsApiTools : NSObject {

}

/**
 * Creates an array list containing a route points geographic coordinates decoded from a string using Google's Polyline algorithm.
 * All array elements are NSValues containing CLLocationCoordinates2D values
 *
 * @param anEncodedPolyline The polyline encoded using Google's Polyline algorithm to decode
 * @return An array list containing the geographic coordinates that defines the polyline
 */
+ (NSArray *)decodeGeographicPolyline:(NSString *)anEncodedPolyline;

/**
 * Creates an array list containing a polyline points levels decoded from a string using Google's Polyline algorithm.
 * All array elements are NSNumbers
 *
 * @param anEncodedLevelList The levels encoded using Google's Polyline algorithm to decode
 * @return An array list containing the polyline points levels
 */
+ (NSArray *)decodeLevels:(NSString *)anEncodedLevelList;

/**
 * Encodes a route and the associated levels using Google's polyline algorithm. Route array must contain NSValue instances, each one storing a CLLocationCoordinate2D value.
 * Only NSValues instances found inside the array are used to calculate the encoded polyline. Levels arrays must contain NSNumbers. Only NSNumber instances found
 * inside the array are used to calculate the encoded levels. Both route array and levels array must contain the same number of elements, or an empty response is created
 *
 * @param aRouteArray The route geographic points stored as NSValues instances containing CLLocationCoordinate2D values
 * @param aLevelArray The levels array, stored as NSNumber instances
 * @return An array containing two strings, the first one represents the encoded polyline, and the second one the encoded levels list. When any precondition is not met,
 * the array is empty
 */
+ (NSArray *)encodeRoute:(NSArray *)aRouteArray
                   levels:(NSArray *)aLevelArray;

/**
 * Reduces a geographic polyline using the Douglas Peucker simplification algorithm. Both route polyline and levels are prunned from the original array
 * according to the provided tolerance. We must bear in mind that the tolerance is measured in degrees, so the distance from original line to the simplified
 * one is greater near the ecuator than near the poles
 *
 * @param aRouteArray The route geographic points stored as NSValues instances containing CLLocationCoordinate2D values to simplify
 * @param aLevelArray The levels array, stored as NSNumber instances
 * @param aTolerance The simplified line tolerance measured in degrees
 * @return An array containing two point arrays, the first contains the simplified route points, and the second the levels associated to those points.
 * Both arrays contain objects similar to the original arrays
 */
+ (NSArray *)douglasPeuckerReductionForRoute:(NSArray *)aRouteArray
                                      levels:(NSArray *)aLevelArray
                               withTolerance:(double)aTolerance;

/**
 * Obtains the administrative level second area from a geocoding result
 *
 * @param aGeocodingResult The geocoding result to analyze
 * @return The administrative level second area for the provided geocoding result
 */
+ (NSString *)administrativeLevelSecondAreaFromGeocodingResult:(GoogleMapsGeocodingResult *)aGeocodingResult;

/**
 * Obtains the country from a geocoding result
 *
 * @param aGeocodingResult The geocoding result to analyze
 * @return The country for the provided geocoding result
 */
+ (NSString *)countryFromGeocodingResult:(GoogleMapsGeocodingResult *)aGeocodingResult;

@end
