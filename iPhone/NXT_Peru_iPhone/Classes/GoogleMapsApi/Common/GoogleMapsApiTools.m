/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsApiTools.h"
#import <CoreLocation/CoreLocation.h>
#import "GoogleMapsConstants.h"
#import "GoogleMapsGeocodingResult.h"
#import "GoogleMapsAddressComponent.h"


#pragma mark -

/**
 * GoogleMapsApiTools private catalog
 */
@interface GoogleMapsApiTools(private)

/**
 * Analyzes an encoded array containing groups of 5 bits (relative to the Google's polyline algorithm) to decode its double representation
 *
 * @param aGroupsArray The array containing groups of 5 bits to decore
 * @param anArrayCount The number of groups of 5 bits contained in the array
 * @return The decoded double represented by the groups of 5 bits
 * @private
 */
+ (double)analyzeDoubleFromFiveBitsGroups:(unsigned char[7])aGroupsArray
                        withValidElements:(NSUInteger)anArrayCount;

/**
 * Analyzes an encoded array containing groups of 5 bits (relative to the Google's polyline algorithm) to decode its unsigned integer representation
 *
 * @param aGroupsArray The array containing groups of 5 bits to decore
 * @param anArrayCount The number of groups of 5 bits contained in the array
 * @return The decoded unsigned integer represented by the groups of 5 bits
 * @private
 */
+ (NSUInteger)analyzeUnsignedIntegerFromFiveBitsGroups:(unsigned char[7])aGroupsArray
                                     withValidElements:(NSUInteger)anArrayCount;

/**
 * Encodes a floating point value into an NSString using Google's polyline algorithm
 *
 * @param aValue The floating point value to encode
 * @return The encoded floating point value as an NSString
 * @private
 */
+ (NSString *)encodeFloatingPoint:(double)aValue;

/**
 * Encodes an unsigned integer value into an NSString using Google's polyline algorithm
 *
 * @param aValue The unsigned integer value to encode
 * @return The encoded unsigned integer value as an NSString
 * @private
 */
+ (NSString *)encodeUnsignedInteger:(NSUInteger)aValue;

/**
 * Calculates the indexes to use to reduce a geographic polyline to a given tolerance using a Douglas Peucker simplification algorithm
 *
 * @param aRouteArray The route geographic points stored as NSValues instances containing CLLocationCoordinate2D values to simplify
 * @param aTolerance The simplified line tolerance measured in degrees
 * @return An array containing NSNumber instances that represents the array indexes that must be used to simplify the polyline. When
 * less than three points are provided, an empty array is returned
 */
+ (NSArray *)douglasPeuckerIndexesForRoute:(NSArray *)aRouteArray
                             withTolerance:(double)aTolerance;

/**
 * Calculates the indexes to use to reduce a geographic polyline to a given tolerance using a Douglas Peucker simplification algorithm. The
 * calculation is reduced to the segment between two indexes
 *
 * @param aRouteArray The route geographic points stored as NSValues instances containing CLLocationCoordinate2D values to simplify
 * @param aFirstIndex The first inex in the array to use
 * @param aLastIndex The last index in the array to use. Must be greater that the first index
 * @param aTolerance The simplified line tolerance measured in degrees
 * @param anIndexesArray The array containing NSNumber instances that represents the array indexes that must be used to simplify the polyline
 */
+ (void)douglasPeuckerIndexesForRoute:(NSArray *)aRouteArray
                                 fromIndex:(NSUInteger)aFirstIndex
                                   toIndex:(NSUInteger)aLastIndex
                             withTolerance:(double)aTolerance
                          intoMutableArray:(NSMutableArray *)anIndexesArray;

/**
 * Calculates the "perpendicular" distance from a geographic segment, defined by its two limiting points, to another given point
 *
 * @param anOriginLocation The origin location
 * @param aDestinationLocation The destination location
 * @param aReferenceLocation The reference location
 * @return The "perpendicular" distance measured in degrees
 */
+ (double)perpendicularDistanceFromSegmentPoint1:(CLLocationCoordinate2D)anOriginLocation
                                   segmentPoint2:(CLLocationCoordinate2D)aDestinationLocation
                                         toPoint:(CLLocationCoordinate2D)aReferenceLocation;

@end


#pragma mark -

@implementation GoogleMapsApiTools

#pragma mark -
#pragma mark Directions API decoders

/*
 * Creates an array list containing a route points geographic coordinates decoded from a string using Google's Polyline algorithm.
 * All array elements are NSValues containing CL2DCoordinates values
 */
+ (NSArray *)decodeGeographicPolyline:(NSString *)anEncodedPolyline {
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    NSUInteger encodedPolylineLength = [anEncodedPolyline length];
    unsigned char blocks[7];
    
    NSUInteger blockPos = 0;
    unsigned char polylineChar;
    double coordinateComponent;
    BOOL analyzingLatitude = YES;
    double previousLatitude = 0.0f;
    double previousLongitude = 0.0f;
    CLLocationCoordinate2D coordinates;
    NSValue *coordinatesValue = nil;
    
    for (NSUInteger i = 0; i < encodedPolylineLength; i ++) {
        
        polylineChar = ((unsigned char)[anEncodedPolyline characterAtIndex:i] - 63);
        
        blocks[blockPos] = polylineChar;
        blockPos ++;
        
        if ((polylineChar & 0X20) == 0) {
            
            coordinateComponent = [GoogleMapsApiTools analyzeDoubleFromFiveBitsGroups:blocks
                                                                    withValidElements:blockPos];
            
            if (analyzingLatitude) {
                
                previousLatitude += coordinateComponent;
                analyzingLatitude = !analyzingLatitude;
                
            } else {
                
                previousLongitude += coordinateComponent;
                analyzingLatitude = !analyzingLatitude;
                
                coordinates.latitude = previousLatitude;
                coordinates.longitude = previousLongitude;
                
                coordinatesValue = [NSValue valueWithBytes:(const void *)&coordinates
                                                  objCType:@encode(CLLocationCoordinate2D)];
                
                if (coordinatesValue != nil) {
                    
                    [mutableResult addObject:coordinatesValue];
                    coordinatesValue = nil;
                    
                }
                
            }
            
            blockPos = 0;
            
        }
        
    }
    
    return [NSArray arrayWithArray:mutableResult];
    
}

/*
 * Creates an array list containing a polyline points levels decoded from a string using Google's Polyline algorithm.
 * All array elements are NSNumbers
 */
+ (NSArray *)decodeLevels:(NSString *)anEncodedLevelList {
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    NSUInteger encodedLevelListLength = [anEncodedLevelList length];
    unsigned char blocks[7];
    
    NSUInteger blockPos = 0;
    unsigned char levelChar;
    NSUInteger analyzedUnsignedInteger;
    NSNumber *analyzedNumber;
    
    for (NSUInteger i = 0; i < encodedLevelListLength; i ++) {
     
        levelChar = ((unsigned char)[anEncodedLevelList characterAtIndex:i] - 63);

        
        blocks[blockPos] = levelChar;
        blockPos ++;
        
        if ((levelChar & 0X20) == 0) {
            
            analyzedUnsignedInteger = [GoogleMapsApiTools analyzeUnsignedIntegerFromFiveBitsGroups:blocks
                                                                                 withValidElements:blockPos];
            
            analyzedNumber = [NSNumber numberWithUnsignedInteger:analyzedUnsignedInteger];
            
            if (analyzedNumber != nil) {
                
                [mutableResult addObject:analyzedNumber];
                analyzedNumber = nil;
                
            }
            
            blockPos = 0;
            
        }
        
    }
    
    return [NSArray arrayWithArray:mutableResult];
    
}

#pragma mark -
#pragma mark Directions API encoders

/*
 * Encodes a route and the associated levels using Google's polyline algorithm. Route array must contain NSValue instances, each one storing a CLLocationCoordinate2D value.
 * Only NSValues instances found inside the array are used to calculate the encoded polyline. Levels arrays must contain NSNumbers. Only NSNumber instances found
 * inside the array are used to calculate the encoded levels. Both route array and levels array must contain the same number of elements, or an empty response is created
 */
+ (NSArray *)encodeRoute:(NSArray *)aRouteArray
                  levels:(NSArray *)aLevelArray {
    
    NSMutableString *mutablePolyline = [NSMutableString string];
    NSMutableString *mutableLevels = [NSMutableString string];
    
    NSUInteger pointsCount = [aRouteArray count];
    NSUInteger levelsCount = [aLevelArray count];
    
    if (/*(pointsCount == levelsCount) &&*/ (pointsCount > 0)) {
        
        NSValue *coordinateValue;
        NSNumber *levelNumber;
        CLLocationCoordinate2D coordinate;
        CLLocationCoordinate2D previousCoordinate;
        previousCoordinate.latitude = 0.0f;
        previousCoordinate.longitude = 0.0f;
        NSDecimalNumber *zero = [NSDecimalNumber zero];
        BOOL userLevels = YES;
        
        if (pointsCount != levelsCount) {
            
            userLevels = NO;
            
        }

        CLLocationCoordinate2D auxCoordinate;
        NSObject *object;
        
        for (NSUInteger i = 0; i < pointsCount; i++) {
        
            object = [aRouteArray objectAtIndex:i];
            
            if ([object isKindOfClass:[NSValue class]]) {
                
                coordinateValue = (NSValue *)object;
                [coordinateValue getValue:&coordinate];
                
                auxCoordinate.latitude = coordinate.latitude - previousCoordinate.latitude;
                auxCoordinate.longitude = coordinate.longitude - previousCoordinate.longitude;
                
                if ((auxCoordinate.latitude != 0.0f) || (auxCoordinate.longitude != 0.0f)) {
                    
                    if (userLevels) {
                        
                        object = [aLevelArray objectAtIndex:i];
                        
                    } else {
                        
                        object = zero;
                        
                    }
                    
                    if ([object isKindOfClass:[NSNumber class]]) {
                        
                        levelNumber = (NSNumber *)object;
                        
                        [mutablePolyline appendString:[GoogleMapsApiTools encodeFloatingPoint:auxCoordinate.latitude]];
                        [mutablePolyline appendString:[GoogleMapsApiTools encodeFloatingPoint:auxCoordinate.longitude]];
                        
                        [mutableLevels appendString:[GoogleMapsApiTools encodeUnsignedInteger:[levelNumber unsignedIntegerValue]]];
                        
                        previousCoordinate.latitude = round(coordinate.latitude * 100000.0f) / 100000.0f;
                        previousCoordinate.longitude = round(coordinate.longitude * 100000.0f) / 100000.0f;
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    NSArray *result = nil;
    
    if (([mutablePolyline length] > 0) && ([mutableLevels length] > 0)) {
        
        result = [NSArray arrayWithObjects:[NSString stringWithString:mutablePolyline], [NSString stringWithString:mutableLevels], nil];
        
    } else {
        
        result = [NSArray array];
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Polyline simplification

/*
 * Reduces a geographic polyline using the Douglas Peucker simplification algorithm. Both route polyline and levels are prunned from the original array
 * according to the provided tolerance. We must bear in mind that the tolerance is measured in degrees, so the distance from original line to the simplified
 * one is greater near the ecuator than near the poles
 */
+ (NSArray *)douglasPeuckerReductionForRoute:(NSArray *)aRouteArray
                                      levels:(NSArray *)aLevelArray
                               withTolerance:(double)aTolerance {

    NSMutableArray *simplifiedRouteArray = nil;
    NSMutableArray *simplifiedLevelArray = nil;
    
    BOOL informationOK = NO;
    NSUInteger routeArrayCount = [aRouteArray count];
    NSUInteger levelArrayCount = [aLevelArray count];
    
    if (routeArrayCount == levelArrayCount) {
        
        informationOK = YES;
        NSObject *object;
        NSValue *auxValue;
        
        for (NSUInteger i = 0; i < routeArrayCount; i ++) {
            
            object = [aRouteArray objectAtIndex:i];
            
            if ([object isKindOfClass:[NSValue class]]) {
                
                auxValue = (NSValue *)object;
                
                if (strcmp([auxValue objCType], @encode(CLLocationCoordinate2D)) != 0) {
                    
                    informationOK = NO;
                    break;
                    
                }
                
            } else {
                
                informationOK = NO;
                break;
                
            }
            
            object = [aLevelArray objectAtIndex:i];
            
            if (![object isKindOfClass:[NSNumber class]]) {
                
                informationOK = NO;
                break;
                
            }
            
        }
        
    }
    
    if (informationOK) {
        
        NSArray *usedIndexes = [GoogleMapsApiTools douglasPeuckerIndexesForRoute:aRouteArray
                                                                   withTolerance:aTolerance];
        
        if ([usedIndexes count] > 0) {
            
            NSArray *orderedIndexes = [usedIndexes sortedArrayUsingSelector:@selector(compare:)];

            simplifiedRouteArray = [NSMutableArray array];
            simplifiedLevelArray = [NSMutableArray array];

            NSUInteger index;
            
            for (NSNumber *number in orderedIndexes) {
                
                index = [number unsignedIntegerValue];
                
                if (index < routeArrayCount) {
                    
                    [simplifiedRouteArray addObject:[aRouteArray objectAtIndex:index]];
                    [simplifiedLevelArray addObject:[aLevelArray objectAtIndex:index]];
                    
                } else {
                    
                    simplifiedRouteArray = nil;
                    simplifiedLevelArray = nil;
                    break;
                    
                }
                
            }
            
        }
        
    }
    
    NSArray *result = nil;
    
    if (([simplifiedRouteArray count] == 0) || ([simplifiedLevelArray count] == 0)) {
        
        result = [NSArray arrayWithObjects:aRouteArray, aLevelArray, nil];
        
    } else {
        
        result = [NSArray arrayWithObjects:[NSArray arrayWithArray:simplifiedRouteArray], [NSArray arrayWithArray:simplifiedLevelArray], nil];
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Extract information from structures

/*
 * Obtains the administrative level second area from a geocoding result
 */
+ (NSString *)administrativeLevelSecondAreaFromGeocodingResult:(GoogleMapsGeocodingResult *)aGeocodingResult {
  
    NSString *result = nil;
    GoogleMapsAddressComponent *addressComponent = nil;
    NSArray *addressComponentsList = aGeocodingResult.addressComponents;
    NSString *type = nil;
    
    for (addressComponent in addressComponentsList) {
        
        for (type in addressComponent.types) {
            
            if ((result == nil) && ([type isEqualToString:GOOGLE_MAPS_ADMINISTRATIVE_AREA_SECOND_LEVEL_TYPE])) {
                
                result = addressComponent.shortName;
                
            } else if (result != nil) {
                
                break;
                
            }
            
        }
        
        if (result != nil) {
            
            break;
            
        }
        
    }
    
    return result;
    
}

/*
 * Obtains the country from a geocoding result
 */
+ (NSString *)countryFromGeocodingResult:(GoogleMapsGeocodingResult *)aGeocodingResult {
    
    NSString *result = nil;
    GoogleMapsAddressComponent *addressComponent = nil;
    NSArray *addressComponentsList = aGeocodingResult.addressComponents;
    NSString *type = nil;
    
    for (addressComponent in addressComponentsList) {
        
        for (type in addressComponent.types) {
            
            if ((result == nil) && ([type isEqualToString:GOOGLE_MAPS_COUNTRY_TYPE])) {
                
                result = addressComponent.shortName;
                
            } else if (result != nil) {
                
                break;
                
            }
            
        }
        
        if (result != nil) {
            
            break;
            
        }
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation GoogleMapsApiTools(private)

#pragma mark -
#pragma mark Decoding

/*
 * Analyzes an encoded array containing groups of 5 bits (relative to the Google' polyline algorithm) to decode its double representation
 */
+ (double)analyzeDoubleFromFiveBitsGroups:(unsigned char[7])aGroupsArray
                        withValidElements:(NSUInteger)anArrayCount {
    
    double result = 0.0f;
    unsigned char block;
    NSInteger resultInteger = 0;
    BOOL negativeNumber = NO;
    
    for (NSInteger i = (anArrayCount - 1); i >= 0; i--) {
        
        resultInteger = resultInteger << 5;
        block = aGroupsArray[i] & 0X1F;
        
        if ((i == 0) && ((block & 0X01) > 0)) {
            
            negativeNumber = YES;
            
        }
        
        resultInteger |= (NSUInteger)block;
        
    }
    
    if (negativeNumber) {
        
        resultInteger = resultInteger ^ 0XFFFFFFFF;
        
    }
    
    resultInteger = resultInteger >> 1;
    
    if (negativeNumber) {
        
        resultInteger |= 0X80000000;
        
    } else {
        
        resultInteger &= 0X7FFFFFFF;
        
    }
    
    result = (double)resultInteger / 100000.0f;
    
    return result;
    
}

/*
 * Analyzes an encoded array containing groups of 5 bits (relative to the Google' polyline algorithm) to decode its unsigned integer representation
 */
+ (NSUInteger)analyzeUnsignedIntegerFromFiveBitsGroups:(unsigned char[7])aGroupsArray
                                     withValidElements:(NSUInteger)anArrayCount {
    NSUInteger result = 0;
    unsigned char block;
    
    for (NSInteger i = (anArrayCount - 1); i >= 0; i--) {
        
        result = result << 5;
        block = aGroupsArray[i] & 0X1F;
        
        result |= (NSUInteger)block;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Encoding

/*
 * Encodes a floating point value into an NSString using Google's polyline algorithm
 */
+ (NSString *)encodeFloatingPoint:(double)aValue {
    
    NSMutableString *mutableResult = [NSMutableString string];
    
    NSInteger intValue = (NSInteger)round(aValue * 100000.0f);
    
    BOOL isNegative = (intValue < 0);
    BOOL isZero = (intValue == 0);
    
    intValue = intValue << 1;
    
    if (isNegative) {
        
        intValue = intValue ^ 0XFFFFFFFF;
        
    }
    
    unsigned char blocks[7];
    NSUInteger blocksLength = 0;
    
    if (isZero) {
        
        blocksLength = 1;
        blocks[0] = 0;

    } else {
         
        for (NSUInteger i = 0; i < 7; i++) {
            
            blocks[i] = (unsigned char)(intValue & 0X1F);
            intValue = intValue >> 5;
            
        }
        
        for (NSInteger i = 6; i >= 0; i--) {
            
            if ((blocks[i] != 0) && (blocksLength == 0)) {
                
                blocksLength = i + 1;
                continue;
                
            }
            
            if (blocksLength > 0) {
                
                blocks[i] = blocks[i] | 0X20;
                
            }
            
        }

    }
    
    for (NSUInteger i = 0; i < blocksLength; i ++) {
        
        [mutableResult appendFormat:@"%c", (blocks[i] + 63)];
        
    }

    return [NSString stringWithString:mutableResult];
    
}

/*
 * Encodes an unsigned integer value into an NSString using Google's polyline algorithm
 */
+ (NSString *)encodeUnsignedInteger:(NSUInteger)aValue {
    
    NSMutableString *mutableResult = [NSMutableString string];
    
    NSUInteger auxValue = aValue;
    
    BOOL isZero = (auxValue == 0);
    
    unsigned char blocks[7];
    NSUInteger blocksLength = 0;
    
    if (isZero) {
        
        blocksLength = 1;
        blocks[0] = 0;
        
    } else {
        
        for (NSUInteger i = 0; i < 7; i++) {
            
            blocks[i] = (unsigned char)(auxValue & 0X1F);
            auxValue = auxValue >> 5;
            
        }
        
        for (NSInteger i = 6; i >= 0; i--) {
            
            if ((blocks[i] != 0) && (blocksLength == 0)) {
                
                blocksLength = i + 1;
                continue;
                
            }
            
            if (blocksLength > 0) {
                
                blocks[i] = blocks[i] | 0X20;
                
            }
            
        }
        
    }
    
    for (NSUInteger i = 0; i < blocksLength; i ++) {
        
        [mutableResult appendFormat:@"%c", (blocks[i] + 63)];
        
    }
    
    return [NSString stringWithString:mutableResult];
    
}

#pragma mark -
#pragma mark Polyline reduction auxiliary selectors

/*
 * Calculates the indexes to use to reduce a geographic polyline to a given tolerance using a Douglas Peucker simplification algorithm
 */
+ (NSArray *)douglasPeuckerIndexesForRoute:(NSArray *)aRouteArray
                             withTolerance:(double)aTolerance {

    NSMutableArray *mutableResult = [NSMutableArray array];
    
    NSUInteger routeArrayCount = [aRouteArray count];
    
    if (routeArrayCount > 2) {
        
        NSUInteger firstIndex = 0;
        NSUInteger lastIndex = routeArrayCount - 1;

        [mutableResult addObject:[NSNumber numberWithUnsignedInteger:firstIndex]];
        [mutableResult addObject:[NSNumber numberWithUnsignedInteger:lastIndex]];
        
        NSValue *auxValue = [aRouteArray objectAtIndex:firstIndex];
        CLLocationCoordinate2D firstLocation;
        [auxValue getValue:&firstLocation];
        
        auxValue = [aRouteArray objectAtIndex:lastIndex];
        CLLocationCoordinate2D lastLocation;
        [auxValue getValue:&lastLocation];
        
        while ((lastIndex > 0) && (firstLocation.latitude == lastLocation.latitude) && (firstLocation.latitude == lastLocation.longitude)) {
            
            lastIndex--;
            auxValue = [aRouteArray objectAtIndex:lastIndex];
            [auxValue getValue:&lastLocation];
            
        }
        
        [GoogleMapsApiTools douglasPeuckerIndexesForRoute:aRouteArray
                                                fromIndex:firstIndex
                                                  toIndex:lastIndex
                                            withTolerance:aTolerance
                                         intoMutableArray:mutableResult];

    }
    
    return [NSArray arrayWithArray:mutableResult];
    
}

/*
 * Calculates the indexes to use to reduce a geographic polyline to a given tolerance using a Douglas Peucker simplification algorithm. The
 * calculation is reduced to the segment between two indexes
 */
+ (void)douglasPeuckerIndexesForRoute:(NSArray *)aRouteArray
                                 fromIndex:(NSUInteger)aFirstIndex
                                   toIndex:(NSUInteger)aLastIndex
                             withTolerance:(double)aTolerance
                     intoMutableArray:(NSMutableArray *)anIndexesArray {
    
    if ((aFirstIndex < aLastIndex) && (aLastIndex < [aRouteArray count])) {
        
        double maxDistance = 0;
        NSUInteger indexFarthest = 0;
        
        NSValue *auxValue;
        auxValue = [aRouteArray objectAtIndex:aFirstIndex];
        CLLocationCoordinate2D originLocation;
        [auxValue getValue:&originLocation];
        
        auxValue = [aRouteArray objectAtIndex:aLastIndex];
        CLLocationCoordinate2D destinationLocation;
        [auxValue getValue:&destinationLocation];
        
        CLLocationCoordinate2D location;
        double distance;
        
        for (NSUInteger index = aFirstIndex; index < aLastIndex; index++) {
            
            auxValue = [aRouteArray objectAtIndex:index];
            [auxValue getValue:&location];
            
            distance = [GoogleMapsApiTools perpendicularDistanceFromSegmentPoint1:originLocation
                                                                    segmentPoint2:destinationLocation
                                                                          toPoint:location];
            
            if (distance > maxDistance) {
                
                maxDistance = distance;
                indexFarthest = index;
                
            }
            
        }
        
        if ((maxDistance > aTolerance) && (indexFarthest != 0)) {
            
            [anIndexesArray addObject:[NSNumber numberWithUnsignedInteger:indexFarthest]];
            
            [GoogleMapsApiTools douglasPeuckerIndexesForRoute:aRouteArray
                                                    fromIndex:aFirstIndex
                                                      toIndex:indexFarthest
                                                withTolerance:aTolerance
                                             intoMutableArray:anIndexesArray];
            
            [GoogleMapsApiTools douglasPeuckerIndexesForRoute:aRouteArray
                                                    fromIndex:indexFarthest
                                                      toIndex:aLastIndex
                                                withTolerance:aTolerance
                                             intoMutableArray:anIndexesArray];
            
        }

    }
    
}

/*
 * Calculates the "perpendicular" distance from a geographic segment, defined by its two limiting points, to another given point
 */
+ (double)perpendicularDistanceFromSegmentPoint1:(CLLocationCoordinate2D)anOriginLocation
                                   segmentPoint2:(CLLocationCoordinate2D)aDestinationLocation
                                         toPoint:(CLLocationCoordinate2D)aReferenceLocation {
    
    double area = fabs(0.5f * ((anOriginLocation.latitude * aDestinationLocation.longitude) + 
                               (aDestinationLocation.latitude * aReferenceLocation.longitude) +
                               (aReferenceLocation.latitude * anOriginLocation.longitude) -
                               (aDestinationLocation.latitude * anOriginLocation.longitude) -
                               (aReferenceLocation.latitude * aDestinationLocation.longitude) -
                               (anOriginLocation.latitude * aReferenceLocation.longitude)));
    
    double bottom = sqrt(pow((anOriginLocation.latitude - aDestinationLocation.latitude), 2) +
                         pow((anOriginLocation.longitude - aDestinationLocation.longitude), 2));
    
    double height = 0.0f;
    
    if (bottom > 0.0f) {
        
        height = area / bottom * 2;
        
    }
    
    return height;
    
}

@end