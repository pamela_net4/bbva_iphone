/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsRoute.h"
#import "BaseXMLParserObject+protected.h"
#import "GoogleMapsPolyline.h"
#import "GoogleMapsLeg.h"
#import "GoogleMapsStep.h"
#import "GoogleMapsApiTools.h"


/**
 * Defines the summary element tag
 */
#define SUMMARY_ELEMENT_TAG                                     @"summary"

/**
 * Defines the leg element tag (this element can appear multiple times)
 */
#define LEG_ELEMENT_TAG                                         @"leg"

/**
 * Defines the polyline element tag
 */
#define POLYLINE_ELEMENT_TAG                                    @"overview_polyline"

/**
 * Defines the copyrights element tag
 */
#define COPYRIGHTS_ELEMENT_TAG                                  @"copyrights"

/**
 * Defines the warning element tag (this element can appear multiple times)
 */
#define WARNING_ELEMENT_TAG                                     @"warning"


#pragma mark -

/**
 * GoogleMapsRoute private category
 */
@interface GoogleMapsRoute(private)

/**
 * Resets all attributes
 *
 * @private
 */
- (void)resetGoogleMapsRouteAttributes;

@end


#pragma mark -

@implementation GoogleMapsRoute

#pragma mark -
#pragma mark Properties

@synthesize summary = summary_;
@dynamic legs;
@synthesize polyline = polyline_;
@synthesize detailledPolyline = detailledPolyline_;
@synthesize copyrights = copyrights_;
@dynamic warnings;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [self resetGoogleMapsRouteAttributes];
    
    [legs_ release];
    legs_ = nil;
    
    [warnings_ release];
    warnings_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. All attributes are released
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
    [super parserDidStartDocument:parser];
    
    [self resetGoogleMapsRouteAttributes];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Element tag is analyzed to determine which
 * information is being analyzed
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (leg_ != nil) {
        
        [legs_ addObject:leg_];
        [leg_ release];
        leg_ = nil;
        
    }
    
    if (xmlAnalysisState_ == BXPOAnalyzingStateIdle) {
        
        NSString *lowercaseElementName = [elementName lowercaseString];

        if ([lowercaseElementName isEqualToString:SUMMARY_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMRAnalyzingSummary;
            
        } else if ([lowercaseElementName isEqualToString:LEG_ELEMENT_TAG]) {

            leg_ = [[GoogleMapsLeg alloc] init];
            [self switchParser:parser
                toParserObject:leg_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:POLYLINE_ELEMENT_TAG]) {
            
            [polyline_ release];
            polyline_ = nil;
            polyline_ = [[GoogleMapsPolyline alloc] init];
            [self switchParser:parser
                toParserObject:polyline_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:COPYRIGHTS_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMRAnalyzingCopyrights;
            
        } else if ([lowercaseElementName isEqualToString:WARNING_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMRAnalyzingWarning;
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. The analyzed element is stored in the corresponding attribute
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    NSInteger xmlAnalysisState = xmlAnalysisState_;
	
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (leg_ != nil) {
        
        [legs_ addObject:leg_];
        [leg_ release];
        leg_ = nil;
        
    }
    
    if (xmlAnalysisState == GMRAnalyzingSummary) {
        
        [summary_ release];
        summary_ = nil;
        summary_ = [self.elementString copyWithZone:[self zone]];
        
    } else if (xmlAnalysisState == GMRAnalyzingCopyrights) {
        
        [copyrights_ release];
        copyrights_ = nil;
        copyrights_ = [self.elementString copyWithZone:[self zone]];
        
    } else if (xmlAnalysisState == GMRAnalyzingWarning) {
        
        NSString *warning = [NSString stringWithString:self.elementString];
        
        if (warning != nil) {
            
            [warnings_ addObject:warning];
            
        }
        
    }
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the legs array
 *
 * @return The legs array
 */
- (NSArray *)legs {
    
    return [NSArray arrayWithArray:legs_];
    
}

/*
 * Returns the route detailled polyline, creating it if necesary
 *
 * @return The route detailled polyline
 */
- (GoogleMapsPolyline *)detailledPolyline {

    if (detailledPolyline_ == nil) {
        
        NSMutableArray *detailledPolylineArray = [NSMutableArray array];
        NSMutableArray *detailledLevelsArray = [NSMutableArray array];
        
        for (GoogleMapsLeg *leg in legs_) {
            
            for (GoogleMapsStep *step in leg.steps) {
                
                GoogleMapsPolyline *polyline = step.polyline;
                [detailledPolylineArray addObjectsFromArray:polyline.polylineArray];
                [detailledLevelsArray addObjectsFromArray:polyline.levelsArray];
                
            }
            
        }
        
        if (([detailledPolylineArray count] > 0) /*&& ([detailledLevelsArray count] > 0)*/) {

            NSArray *encodedStrings = [GoogleMapsApiTools encodeRoute:detailledPolylineArray
                                                               levels:detailledLevelsArray];
            
            if ([encodedStrings count] == 2) {
                
                detailledPolyline_ = [[GoogleMapsPolyline alloc] initWithPolyline:[encodedStrings objectAtIndex:0]
                                                                           levels:[encodedStrings objectAtIndex:1]];
            }
            
        }
        
    }
    
    return detailledPolyline_;
    
}

/*
 * Returns the warnings array
 *
 * @return The warnings array
 */
- (NSArray *)warnings {
    
    return [NSArray arrayWithArray:warnings_];
    
}

@end


#pragma mark -

@implementation GoogleMapsRoute(private)

#pragma mark -
#pragma mark Memory management

/*
 * Resets all attributes
 */
- (void)resetGoogleMapsRouteAttributes {
    
    [summary_ release];
    summary_ = nil;
    
    if (legs_ == nil) {
        
        legs_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [legs_ removeAllObjects];
        
    }

    [leg_ release];
    leg_ = nil;
    
    [polyline_ release];
    polyline_ = nil;
    
    [detailledPolyline_ release];
    detailledPolyline_ = nil;
    
    [copyrights_ release];
    copyrights_ = nil;
    
    if (warnings_ == nil) {
        
        warnings_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [warnings_ removeAllObjects];
        
    }
    
}

@end
