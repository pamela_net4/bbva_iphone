/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsPolyline.h"
#import "BaseXMLParserObject+protected.h"
#import "GoogleMapsApiTools.h"


/**
 * Defines the polyline points element tag
 */
#define POINTS_ELEMENT_TAG                              @"points"

/**
 * Defines the polyline levels element tag
 */
#define LEVELS_ELEMENT_TAG                              @"levels"


#pragma mark -

@implementation GoogleMapsPolyline

#pragma mark -
#pragma mark Properties

@synthesize polylineString = polylineString_;
@dynamic polylineArray;
@synthesize levelsString = levelsString_;
@dynamic levelsArray;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {

    [polylineString_ release];
    polylineString_ = nil;
    
    [polylineArray_ release];
    polylineArray_ = nil;
    
    [levelsString_ release];
    levelsString_ = nil;
    
    [levelsArray_ release];
    levelsArray_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes a GoogleMapsPolyline instance without polyline or level information
 *
 * @return The initialized GoogleMapsPolyline instance
 */
- (id)init {

    return [self initWithPolyline:nil
                           levels:nil];
    
}

/*
 * Designated initializer. Initializes a GoogleMapsPolyline instance with the provided polyline string and level string.
 * When any one of those components cannot be analyzed, both the polyline string and level string (and the associated arrays)
 * are empty
 */
- (id)initWithPolyline:(NSString *)aPolylineString
                levels:(NSString *)aLevelString {
    
    if (self = [super init]) {
        
        if (([aPolylineString length] > 0) && ([aLevelString length] > 0)) {
            
            polylineString_ = [aPolylineString copy];
            levelsString_ = [aLevelString copy];
            
            polylineArray_ = [[GoogleMapsApiTools decodeGeographicPolyline:polylineString_] retain];
            levelsArray_ = [[GoogleMapsApiTools decodeLevels:levelsString_] retain];
            
            if (([polylineArray_ count] == 0) || ([levelsArray_ count] == 0)) {
                
                [polylineString_ release];
                polylineString_ = nil;
                
                [polylineArray_ release];
                polylineArray_ = nil;
                
                [levelsString_ release];
                levelsString_ = nil;
                
                [levelsArray_ release];
                levelsArray_ = nil;
                
            }
            
        }
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Returns the simplified version of the polyline for a given tolerance
 */
- (GoogleMapsPolyline *)simplifiedPolylineWithTolerance:(double)aTolerance {

    GoogleMapsPolyline *result = nil;
    
    NSArray *simplifiedArrays = [GoogleMapsApiTools douglasPeuckerReductionForRoute:polylineArray_
                                                                             levels:levelsArray_
                                                                      withTolerance:aTolerance];
    
    if ([simplifiedArrays count] == 2) {
        
        NSArray *encodedStrings = [GoogleMapsApiTools encodeRoute:[simplifiedArrays objectAtIndex:0]
                                                           levels:[simplifiedArrays objectAtIndex:1]];
        
        if ([encodedStrings count] == 2) {
            
            result = [[[GoogleMapsPolyline alloc] initWithPolyline:[encodedStrings objectAtIndex:0]
                                                            levels:[encodedStrings objectAtIndex:1]] autorelease];
        }
        
    }
    
    return result;

}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. Polyline points and levels are released
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
    [super parserDidStartDocument:parser];
    
    [polylineString_ release];
    polylineString_ = nil;
    
    [polylineArray_ release];
    polylineArray_ = nil;
    
    [levelsString_ release];
    levelsString_ = nil;
    
    [levelsArray_ release];
    levelsString_ = nil;
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Element tag is analyzed to determine which
 * information is being analyzed
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisState_ == BXPOAnalyzingStateIdle) {
        
        NSString *lowercaseElementName = [elementName lowercaseString];

        if ([lowercaseElementName isEqualToString:POINTS_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMPAnalyzingPoints;
            
        } else if ([lowercaseElementName isEqualToString:LEVELS_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMPAnalyzingLevels;
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. The analyzed element is stored in the corresponding attribute
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    NSInteger xmlAnalysisState = xmlAnalysisState_;
	
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisState == GMPAnalyzingPoints) {
        
        [polylineString_ release];
        polylineString_ = nil;
        polylineString_ = [self.elementString copyWithZone:[self zone]];
        
        [polylineArray_ release];
        polylineArray_ = nil;
        
    } else if (xmlAnalysisState == GMPAnalyzingLevels) {
        
        [levelsString_ release];
        levelsString_ = nil;
        levelsString_ = [self.elementString copyWithZone:[self zone]];
        
        [levelsArray_ release];
        levelsArray_ = nil;
         
    }
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the polyline points as an array
 *
 * @return The polyline points as an array
 */
- (NSArray *)polylineArray {

    if (polylineArray_ == nil) {
        
        if ([polylineString_ length] > 0) {
            
            polylineArray_ = [[GoogleMapsApiTools decodeGeographicPolyline:polylineString_] retain];
            
        }
        
    }
    
    return polylineArray_;
    
}

/*
 * Returns the polyline levels as an array
 *
 * @return The polyline levels as an array
 */
- (NSArray *)levelsArray {
    
    if (levelsArray_ == nil) {
        
        if ([levelsString_ length] > 0) {
            
            levelsArray_ = [[GoogleMapsApiTools decodeLevels:levelsString_] retain];
            
        }
        
    }
    
    return levelsArray_;
    
}

@end
