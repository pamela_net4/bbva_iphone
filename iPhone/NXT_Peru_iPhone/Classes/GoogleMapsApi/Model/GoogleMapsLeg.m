/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsLeg.h"
#import "BaseXMLParserObject+protected.h"
#import "GoogleMapsDistance.h"
#import "GoogleMapsDuration.h"
#import "GoogleMapsLocation.h"
#import "GoogleMapsStep.h"


/**
 * Defines the step element tag (this element can appear multiple times)
 */
#define STEP_ELEMENT_TAG                                        @"step"

/**
 * Defines the distance element tag
 */
#define DISTANCE_ELEMENT_TAG                                    @"distance"

/**
 * Defines the duration element tag
 */
#define DURATION_ELEMENT_TAG                                    @"duration"

/**
 * Defines the start location element tag
 */
#define START_LOCATION_ELEMENT_TAG                              @"start_location"

/**
 * Defines the end location element tag
 */
#define END_LOCATION_ELEMENT_TAG                                @"end_location"

/**
 * Defines the start address element tag
 */
#define START_ADDRESS_ELEMENT_TAG                               @"start_address"

/**
 * Defines the end address element tag
 */
#define END_ADDRESS_ELEMENT_TAG                                 @"end_address"


#pragma mark -

/**
 * GoogleMapsLeg private category
 */
@interface GoogleMapsLeg(private)

/**
 * Resets all attributes
 *
 * @private
 */
- (void)resetGoogleMapsLegAttributes;

@end


#pragma mark -

@implementation GoogleMapsLeg

#pragma mark -
#pragma mark Properties

@dynamic steps;
@synthesize distance = distance_;
@synthesize duration = duration_;
@synthesize startLocation = startLocation_;
@synthesize endLocation = endLocation_;
@synthesize startAddress = startAddress_;
@synthesize endAddress = endAddress_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [self resetGoogleMapsLegAttributes];
    
    [steps_ release];
    steps_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. All attributes are released
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
    [super parserDidStartDocument:parser];
    
    [self resetGoogleMapsLegAttributes];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Element tag is analyzed to determine which
 * information is being analyzed
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (step_ != nil) {
        
        [steps_ addObject:step_];
        [step_ release];
        step_ = nil;
        
    }
    
    if (xmlAnalysisState_ == BXPOAnalyzingStateIdle) {
        
        NSString *lowercaseElementName = [elementName lowercaseString];

        if ([lowercaseElementName isEqualToString:STEP_ELEMENT_TAG]) {

            step_ = [[GoogleMapsStep alloc] init];
            [self switchParser:parser
                toParserObject:step_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:DISTANCE_ELEMENT_TAG]) {
            
            [distance_ release];
            distance_ = nil;
            distance_ = [[GoogleMapsDistance alloc] init];
            [self switchParser:parser
                toParserObject:distance_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:DURATION_ELEMENT_TAG]) {
            
            [duration_ release];
            duration_ = nil;
            duration_ = [[GoogleMapsDuration alloc] init];
            [self switchParser:parser
                toParserObject:duration_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:START_LOCATION_ELEMENT_TAG]) {
            
            [startLocation_ release];
            startLocation_ = nil;
            startLocation_ = [[GoogleMapsLocation alloc] init];
            [self switchParser:parser
                toParserObject:startLocation_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:END_LOCATION_ELEMENT_TAG]) {
            
            [endLocation_ release];
            endLocation_ = nil;
            endLocation_ = [[GoogleMapsLocation alloc] init];
            [self switchParser:parser
                toParserObject:endLocation_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:START_ADDRESS_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMLEAnalyzingStartAddress;
            
        } else if ([lowercaseElementName isEqualToString:END_ADDRESS_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMLEAnalyzingEndAddress;
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. The analyzed element is stored in the corresponding attribute
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    NSInteger xmlAnalysisState = xmlAnalysisState_;
	
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (step_ != nil) {
        
        [steps_ addObject:step_];
        [step_ release];
        step_ = nil;
        
    }
    
    if (xmlAnalysisState == GMLEAnalyzingStartAddress) {
        
        [startAddress_ release];
        startAddress_ = nil;
        startAddress_ = [self.elementString copyWithZone:[self zone]];
        
    } else if (xmlAnalysisState == GMLEAnalyzingEndAddress) {
        
        [endAddress_ release];
        endAddress_ = nil;
        endAddress_ = [self.elementString copyWithZone:[self zone]];
        
    }
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the steps array
 *
 * @return The steps array
 */
- (NSArray *)steps {
    
    return [NSArray arrayWithArray:steps_];
    
}

@end


#pragma mark -

@implementation GoogleMapsLeg(private)

#pragma mark -
#pragma mark Memory management

/*
 * Resets all attributes
 */
- (void)resetGoogleMapsLegAttributes {
    
    if (steps_ == nil) {
        
        steps_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [steps_ removeAllObjects];
        
    }

    [step_ release];
    step_ = nil;
    
    [distance_ release];
    distance_ = nil;
    
    [duration_ release];
    duration_ = nil;
    
    [startLocation_ release];
    startLocation_ = nil;
    
    [endLocation_ release];
    endLocation_ = nil;
    
    [startAddress_ release];
    startAddress_ = nil;
    
    [endAddress_ release];
    endAddress_ = nil;
    
}

@end
