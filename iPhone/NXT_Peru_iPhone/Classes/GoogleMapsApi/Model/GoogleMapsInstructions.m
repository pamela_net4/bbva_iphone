/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsInstructions.h"
#import "BaseXMLParserObject+protected.h"
#import "NSString+URLAndHTMLUtils.h"


#pragma mark -

/**
 * GoogleMapsInstructions private category
 */
@interface GoogleMapsInstructions(private)

/**
 * Removes the HTML marks from a string
 *
 * @param aString The original string;
 * @return The original string without the HTML marks
 * @private
 */
- (NSString *)removeHTML:(NSString *)aString;

@end


#pragma mark -

@implementation GoogleMapsInstructions

#pragma mark -
#pragma mark Properties

@dynamic string;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [string_ release];
    string_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. The instructions string is reseted
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
    [super parserDidStartDocument:parser];
    
    if (string_ == nil) {
        
        string_ = [[NSMutableString alloc] init];
        
    } else {
        
        [string_ setString:@""];
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. An HTML mark element is assumed
 * information is being analyzed
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    xmlAnalysisState_ = GMIAnalyzingHTMLMark;
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. The read string is accumulated into the instructions string
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    [string_ appendString:[self removeHTML:self.elementString]];
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the instructions string
 *
 * @return The instructions string
 */
- (NSString *)string {
    
    NSString *result = @"";
    
    if ([string_ length] > 0) {
        
        result = [NSString stringWithString:string_];
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation GoogleMapsInstructions(private)

#pragma mark-
#pragma mark String selectors

/**
 * Removes the HTML marks from a string
 *
 * @param aString The original string;
 * @return The original string without the HTML marks
 * @private
 */
- (NSString *)removeHTML:(NSString *)aString {

    NSString *result = aString;
    NSScanner *scanner = [NSScanner scannerWithString:aString];
    NSString *htmlMark = nil;
    BOOL removed = NO;
    
    while ([scanner isAtEnd] == NO) {

        [scanner scanUpToString:@"<"
                     intoString:nil];
        [scanner scanUpToString:@">"
                     intoString:&htmlMark];
        
        NSArray *components = [htmlMark componentsSeparatedByString:@" "];
        removed = NO;
        
        if ([components count] > 0) {
            
            NSString *firstComponent = [components objectAtIndex:0];
            firstComponent = [firstComponent lowercaseString];
            
            if ([firstComponent isEqualToString:@"<div"]) {
                
                result = [result stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", htmlMark]
                                                           withString:@" ("];
                removed = YES;
                
            } else if ([firstComponent isEqualToString:@"</div"]) {
                
                result = [result stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", htmlMark]
                                                           withString:@")"];
                removed = YES;
                
                
            }
            
        }
        
        if (!removed) {
            
            result = [result stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", htmlMark]
                                                       withString:@""];
            
        }
        
    }
    
    return [result htmlEscapedCharactersTransformed];
    
}

@end
