/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsGeocodingResponse.h"
#import "BaseXMLParserObject+protected.h"
#import "GoogleMapsGeocodingResult.h"


/**
 * Defines the status element tag
 */
#define STATUS_ELEMENT_TAG                                      @"status"

/**
 * Defines the geocoding result element tag (this element can appear multiple times)
 */
#define RESULT_ELEMENT_TAG                                      @"result"


#pragma mark -

/**
 * GoogleMapsGeocodingResponse private category
 */
@interface GoogleMapsGeocodingResponse(private)

/**
 * Resets all attributes
 *
 * @private
 */
- (void)resetGoogleMapsGeocodingResponseAttributes;

@end


#pragma mark -

@implementation GoogleMapsGeocodingResponse

#pragma mark -
#pragma mark Properties

@synthesize status = status_;
@dynamic results;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [self resetGoogleMapsGeocodingResponseAttributes];
    
    [results_ release];
    results_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. All attributes are released
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
    [super parserDidStartDocument:parser];
    
    [self resetGoogleMapsGeocodingResponseAttributes];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Element tag is analyzed to determine which
 * information is being analyzed
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (result_ != nil) {
        
        [results_ addObject:result_];
        [result_ release];
        result_ = nil;
        
    }
    
    if (xmlAnalysisState_ == BXPOAnalyzingStateIdle) {
        
        NSString *lowercaseElementName = [elementName lowercaseString];

        if ([lowercaseElementName isEqualToString:STATUS_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMGREAnalyzingStatus;
            
        } else if ([lowercaseElementName isEqualToString:RESULT_ELEMENT_TAG]) {

            result_ = [[GoogleMapsGeocodingResult alloc] init];
            [self switchParser:parser
                toParserObject:result_
                withOpeningTag:lowercaseElementName];
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. The analyzed element is stored in the corresponding attribute
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    NSInteger xmlAnalysisState = xmlAnalysisState_;
	
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (result_ != nil) {
        
        [results_ addObject:result_];
        [result_ release];
        result_ = nil;
        
    }
    
    if (xmlAnalysisState == GMGREAnalyzingStatus) {
        
        [status_ release];
        status_ = nil;
        status_ = [self.elementString copyWithZone:[self zone]];
        
    }
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the results array
 *
 * @return The results array
 */
- (NSArray *)results {
    
    return [NSArray arrayWithArray:results_];
    
}

@end


#pragma mark -

@implementation GoogleMapsGeocodingResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Resets all attributes
 */
- (void)resetGoogleMapsGeocodingResponseAttributes {
    
    [status_ release];
    status_ = nil;
    
    if (results_ == nil) {
        
        results_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [results_ removeAllObjects];
        
    }

    [result_ release];
    result_ = nil;
    
}

@end
