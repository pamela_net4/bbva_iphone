/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsGeocodingResult.h"
#import "BaseXMLParserObject+protected.h"
#import "GoogleMapsAddressComponent.h"
#import "GoogleMapsGeometry.h"


/**
 * Defines the type element tag (this element can appear multiple times)
 */
#define TYPE_ELEMENT_TAG                                        @"type"

/**
 * Defines the formatted address element tag
 */
#define FORMATTED_ADDRESS_ELEMENT_TAG                           @"formatted_address"

/**
 * Defines the address component element tag (this element can appear multiple times)
 */
#define ADDRESS_COMPONENT_ELEMENT_TAG                           @"address_component"

/**
 * Defines the geometry element tag
 */
#define GEOMETRY_ELEMENT_TAG                                    @"geometry"

/**
 * Defines the partial match element tag
 */
#define PARTIAL_MATCH_ELEMENT_TAG                               @"partial_match"


/**
 * Defines the partial match positive value
 */
#define PARTIAL_MATCH_POSITIVE_VALUE                            @"true"


#pragma mark -

/**
 * GoogleMapsGeocodingResult private category
 */
@interface GoogleMapsGeocodingResult(private)

/**
 * Resets all attributes
 *
 * @private
 */
- (void)resetGoogleMapsGeocodingResultAttributes;

@end


#pragma mark -

@implementation GoogleMapsGeocodingResult

#pragma mark -
#pragma mark Properties

@dynamic types;
@synthesize formattedAddress = formattedAddress_;
@dynamic addressComponents;
@synthesize geometry = geometry_;
@synthesize partialMatch = partialMatch_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [self resetGoogleMapsGeocodingResultAttributes];
    
    [types_ release];
    types_ = nil;
    
    [addressComponents_ release];
    addressComponents_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. All attributes are released
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
    [super parserDidStartDocument:parser];
    
    [self resetGoogleMapsGeocodingResultAttributes];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Element tag is analyzed to determine which
 * information is being analyzed
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (addressComponent_ != nil) {
        
        [addressComponents_ addObject:addressComponent_];
        [addressComponent_ release];
        addressComponent_ = nil;
        
    }
    
    if (xmlAnalysisState_ == BXPOAnalyzingStateIdle) {
        
        NSString *lowercaseElementName = [elementName lowercaseString];

        if ([lowercaseElementName isEqualToString:TYPE_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMGRUAnalyzingType;
            
        } else if ([lowercaseElementName isEqualToString:FORMATTED_ADDRESS_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMGRUAnalyzingFormattedAddress;
            
        } else if ([lowercaseElementName isEqualToString:ADDRESS_COMPONENT_ELEMENT_TAG]) {

            addressComponent_ = [[GoogleMapsAddressComponent alloc] init];
            [self switchParser:parser
                toParserObject:addressComponent_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:GEOMETRY_ELEMENT_TAG]) {
            
            [geometry_ release];
            geometry_ = nil;
            geometry_ = [[GoogleMapsGeometry alloc] init];
            [self switchParser:parser
                toParserObject:geometry_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:PARTIAL_MATCH_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMGRUAnalyzingPartialMatch;
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. The analyzed element is stored in the corresponding attribute
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    NSInteger xmlAnalysisState = xmlAnalysisState_;
	
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (addressComponent_ != nil) {
        
        [addressComponents_ addObject:addressComponent_];
        [addressComponent_ release];
        addressComponent_ = nil;
        
    }
    
    if (xmlAnalysisState == GMGRUAnalyzingType) {
        
        NSString *auxString = [NSString stringWithString:self.elementString];
        
        if (auxString != nil) {
            
            [types_ addObject:auxString];
            
        }
        
    } else if (xmlAnalysisState == GMGRUAnalyzingFormattedAddress) {
        
        [formattedAddress_ release];
        formattedAddress_ = nil;
        formattedAddress_ = [self.elementString copyWithZone:[self zone]];
        
    } else if (xmlAnalysisState == GMGRUAnalyzingPartialMatch) {
        
        if ([[self.elementString lowercaseString] isEqualToString:PARTIAL_MATCH_POSITIVE_VALUE]) {
            
            partialMatch_ = YES;
            
        } else {
            
            partialMatch_ = NO;
            
        }
        
    }
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the types array
 *
 * @return The types array
 */
- (NSArray *)types {
    
    return [NSArray arrayWithArray:types_];
    
}

/*
 * Returns the address components array
 *
 * @return The address components array
 */
- (NSArray *)addressComponents {
    
    return [NSArray arrayWithArray:addressComponents_];
    
}

@end


#pragma mark -

@implementation GoogleMapsGeocodingResult(private)

#pragma mark -
#pragma mark Memory management

/*
 * Resets all attributes
 */
- (void)resetGoogleMapsGeocodingResultAttributes {
    
    if (types_ == nil) {
        
        types_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [types_ removeAllObjects];
        
    }

    [formattedAddress_ release];
    formattedAddress_ = nil;
    
    if (addressComponents_ == nil) {
        
        addressComponents_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [addressComponents_ removeAllObjects];
        
    }
    
    [addressComponent_ release];
    addressComponent_ = nil;
    
    [geometry_ release];
    geometry_ = nil;
    
    partialMatch_ = NO;
    
}

@end
