/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"


/**
 * Enumerates the Google Maps duration analyzing states
 */
typedef enum {
    
    GMDUAnalyzingSeconds = BXPOAnalyzingStateCount, //!<Analyzing the seconds element
    GMDUAnalyzingString, //!<Analyzing the string element
    GMDUAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMDUAnalyzingState;


/**
 * Parses and stores a Google Maps duration XML element, containing both duration in seconds and as a string
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsDuration : BaseXMLParserObject {

@private
    
    /**
     * Duration in seconds
     */
    NSInteger seconds_;
    
    /**
     * Duration as a human readable string
     */
    NSString *string_;

}

/**
 * Provides read-only access to the duration in seconds
 */
@property (nonatomic, readonly, assign) NSInteger seconds;

/**
 * Provides read-only access to the duration as a human readable string
 */
@property (nonatomic, readonly, copy) NSString *string;

@end
