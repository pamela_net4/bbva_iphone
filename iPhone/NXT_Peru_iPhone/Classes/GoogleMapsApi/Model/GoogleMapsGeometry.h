/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"


//Forward declarations
@class GoogleMapsLocation;
@class GoogleMapsViewport;


/**
 * Enumerates the Google Maps geometry analyzing states
 */
typedef enum {
    
    GMGAnalyzingLocationType = BXPOAnalyzingStateCount, //!<Analyzing the geometry location type
    GMGAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMGAnalyzingState;


/**
 * Parses and stores a Google Maps geometry XML element
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsGeometry : BaseXMLParserObject {

@private
    
    /**
     * Geometry location type
     */
    NSString *locationType_;
    
    /**
     * Geomtry location
     */
    GoogleMapsLocation *location_;
    
    /**
     * Geometry viewport
     */
    GoogleMapsViewport *viewport_;
    
    /**
     * Geometry bounds (it may be nil)
     */
    GoogleMapsViewport *bounds_;
    
}



/**
 * Provides read-only access to the geomtry location type
 */
@property (nonatomic, readonly, copy) NSString *locationType;

/**
 * Provides read-only access to the geomtry location
 */
@property (nonatomic, readonly, retain) GoogleMapsLocation *location;

/**
 * Provides read-only access to the geometry viewport
 */
@property (nonatomic, readonly, retain) GoogleMapsViewport *viewport;

/**
 * Provides read-only access to the geometry bounds
 */
@property (nonatomic, readonly, retain) GoogleMapsViewport *bounds;

@end
