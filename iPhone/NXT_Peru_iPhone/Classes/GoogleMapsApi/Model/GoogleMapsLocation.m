/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsLocation.h"
#import "BaseXMLParserObject+protected.h"


/**
 * Defines the latitude element tag
 */
#define LATITUDE_ELEMENT_TAG                            @"lat"

/**
 * Defines the longitude element tag
 */
#define LONGITUDE_ELEMENT_TAG                           @"lng"


#pragma mark -

@implementation GoogleMapsLocation

#pragma mark -
#pragma mark Properties

@synthesize latitude = latitude_;
@synthesize longitude = longitude_;
@dynamic coordinate;

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. The latitude and longitude are reseted to zero
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
    [super parserDidStartDocument:parser];
    
    latitude_ = 0.0f;
    longitude_ = 0.0f;
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Element tag is analyzed to determine which
 * information is being analyzed
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisState_ == BXPOAnalyzingStateIdle) {
        
        NSString *lowercaseElementName = [elementName lowercaseString];

        if ([lowercaseElementName isEqualToString:LATITUDE_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMLOAnalyzingLatitude;
            
        } else if ([lowercaseElementName isEqualToString:LONGITUDE_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMLOAnalyzingLongitude;
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. The analyzed element is stored in the corresponding attribute
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    NSInteger xmlAnalysisState = xmlAnalysisState_;
	
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisState == GMLOAnalyzingLatitude) {
        
        NSScanner *scanner = [NSScanner scannerWithString:self.elementString];
        
        if (![scanner scanDouble:&latitude_]) {
            
            latitude_ = 0.0f;
        }
        
    } else if (xmlAnalysisState == GMLOAnalyzingLongitude) {
        
        NSScanner *scanner = [NSScanner scannerWithString:self.elementString];
        
        if (![scanner scanDouble:&longitude_]) {
            
            longitude_ = 0.0f;
            
        }

    }
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the analyzed coordinate
 *
 * @return The analyzed coordinate
 */
- (CLLocationCoordinate2D)coordinate {

    CLLocationCoordinate2D result;
    result.latitude = latitude_;
    result.longitude = longitude_;
    
    return result;
    
}

@end
