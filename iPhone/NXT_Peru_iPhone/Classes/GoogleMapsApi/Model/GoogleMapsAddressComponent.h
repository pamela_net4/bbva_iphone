/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"


/**
 * Enumerates the Google Maps address component analyzing states
 */
typedef enum {
    
    GMACAnalyzingType = BXPOAnalyzingStateCount, //!<Analyzing the address component type
    GMACAnalyzingLongName, //!<Analyzing the address component long name
    GMACAnalyzingShortName, //!<Analyzing the address component short name
    GMACAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMACAnalyzingState;


/**
 * Parses and stores a Google Maps address component XML element
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsAddressComponent : BaseXMLParserObject {

@private
    
    /**
     * Address component types array. All objects are NSString instances
     */
    NSMutableArray *types_;
    
    /**
     * Address component long name
     */
    NSString *longName_;
    
    /**
     * Address component short name
     */
    NSString *shortName_;
    
}


/**
 * Provides read-only access to the address component types array
 */
@property (nonatomic, readonly, retain) NSArray *types;

/**
 * Provides read-only access to the address component long name
 */
@property (nonatomic, readonly, copy) NSString *longName;

/**
 * Provides read-only access to the address component short name
 */
@property (nonatomic, readonly, copy) NSString *shortName;

@end
