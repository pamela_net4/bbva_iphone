/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"


//Forward declarations
@class GoogleMapsAddressComponent;
@class GoogleMapsGeometry;


/**
 * Enumerates the Google Maps geocoding result analyzing states
 */
typedef enum {
    
    GMGRUAnalyzingType = BXPOAnalyzingStateCount, //!<Analyzing the geocoding result type
    GMGRUAnalyzingFormattedAddress, //!<Analyzing the geocoding result formatted address
    GMGRUAnalyzingPartialMatch, //!<Analyzing the geocoding result partial match
    GMGRUAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMGRUAnalyzingState;


/**
 * Parses and stores a Google Maps geocoding result XML element
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsGeocodingResult : BaseXMLParserObject {

@private
    
    /**
     * Geocoding result types array. All objects are NSString instances
     */
    NSMutableArray *types_;
    
    /**
     * Geocoding result formatted address
     */
    NSString *formattedAddress_;
    
    /**
     * Geocoding result address components array. All objects are GoogleMapsAddressComponent instances
     */
    NSMutableArray *addressComponents_;
    
    /**
     * Auxiliary geocoding result address component to analyze one address component
     */
    GoogleMapsAddressComponent *addressComponent_;
    
    /**
     * Geocoding result geometry
     */
    GoogleMapsGeometry *geometry_;
    
    /**
     * Geocoding result partial match flag
     */
    BOOL partialMatch_;
    
}


/**
 * Provides read-only access to the geocoding result types array
 */
@property (nonatomic, readonly, retain) NSArray *types;

/**
 * Provides read-only access to the geocoding result formatted address
 */
@property (nonatomic, readonly, copy) NSString *formattedAddress;

/**
 * Provides read-only access to the geocoding result address components array. All objects are GoogleMapsAddressComponent instances
 */
@property (nonatomic, readonly, retain) NSArray *addressComponents;

/**
 * Provides read-only access to the geocoding result geometry
 */
@property (nonatomic, readonly, retain) GoogleMapsGeometry *geometry;

/**
 * Provides read-only access to the geocoding result partial match flag
 */
@property (nonatomic, readonly, assign) BOOL partialMatch;

@end
