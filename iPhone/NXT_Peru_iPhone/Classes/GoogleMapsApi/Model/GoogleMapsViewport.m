/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsViewport.h"
#import "BaseXMLParserObject+protected.h"
#import "GoogleMapsLocation.h"


/**
 * Defines the viewport southwest element tag
 */
#define SOUTHWEST_ELEMENT_TAG                                       @"southwest"

/**
 * Defines the viewport northeast element tag
 */
#define NORTHEAST_ELEMENT_TAG                                       @"northeast"


#pragma mark -

/**
 * GoogleMapsViewport private category
 */
@interface GoogleMapsViewport(private)

/**
 * Resets all attributes
 *
 * @private
 */
- (void)resetGoogleMapsViewportAttributes;

@end


#pragma mark -

@implementation GoogleMapsViewport

#pragma mark -
#pragma mark Properties

@synthesize southwest = southwest_;
@synthesize northeast = northeast_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self resetGoogleMapsViewportAttributes];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. All attributes are released
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
    [super parserDidStartDocument:parser];
    
    [self resetGoogleMapsViewportAttributes];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Element tag is analyzed to determine which
 * information is being analyzed
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisState_ == BXPOAnalyzingStateIdle) {
        
        NSString *lowercaseElementName = [elementName lowercaseString];
        
        if ([lowercaseElementName isEqualToString:SOUTHWEST_ELEMENT_TAG]) {
            
            [southwest_ release];
            southwest_ = nil;
            southwest_ = [[GoogleMapsLocation alloc] init];
            [self switchParser:parser
                toParserObject:southwest_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:NORTHEAST_ELEMENT_TAG]) {
            
            [northeast_ release];
            northeast_ = nil;
            northeast_ = [[GoogleMapsLocation alloc] init];
            [self switchParser:parser
                toParserObject:northeast_
                withOpeningTag:lowercaseElementName];
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. The analyzed element is stored in the corresponding attribute
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

@end


#pragma mark -

@implementation GoogleMapsViewport(private)

#pragma mark -
#pragma mark Memory management

/*
 * Resets all attributes
 */
- (void)resetGoogleMapsViewportAttributes {
    
    [southwest_ release];
    southwest_ = nil;
    
    [northeast_ release];
    northeast_ = nil;
    
}

@end

