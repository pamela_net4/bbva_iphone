/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"


/**
 * Enumerates the Google Maps distance analyzing states
 */
typedef enum {
    
    GMDIAnalyzingMeters = BXPOAnalyzingStateCount, //!<Analyzing the meters element
    GMDIAnalyzingString, //!<Analyzing the string element
    GMDIAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMDIAnalyzingState;


/**
 * Parses and stores a Google Maps distance XML element, containing both distance in meters and as a string
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsDistance : BaseXMLParserObject {

@private
    
    /**
     * Distance in meters
     */
    NSInteger meters_;
    
    /**
     * Distance as a human readable string
     */
    NSString *string_;

}

/**
 * Provides read-only access to the Distance in meters
 */
@property (nonatomic, readonly, assign) NSInteger meters;

/**
 * Provides read-only access to the distance as a human readable string
 */
@property (nonatomic, readonly, copy) NSString *string;

@end
