/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsDirectionsResponse.h"
#import "BaseXMLParserObject+protected.h"
#import "GoogleMapsRoute.h"


/**
 * Defines the status element tag
 */
#define STATUS_ELEMENT_TAG                                      @"status"

/**
 * Defines the route element tag (this element can appear multiple times)
 */
#define ROUTE_ELEMENT_TAG                                       @"route"


#pragma mark -

/**
 * GoogleMapsDirectionsResponse private category
 */
@interface GoogleMapsDirectionsResponse(private)

/**
 * Resets all attributes
 *
 * @private
 */
- (void)resetGoogleMapsDirectionsResponseAttributes;

@end


#pragma mark -

@implementation GoogleMapsDirectionsResponse

#pragma mark -
#pragma mark Properties

@synthesize status = status_;
@dynamic routes;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [self resetGoogleMapsDirectionsResponseAttributes];
    
    [routes_ release];
    routes_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. All attributes are released
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
    [super parserDidStartDocument:parser];
    
    [self resetGoogleMapsDirectionsResponseAttributes];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Element tag is analyzed to determine which
 * information is being analyzed
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (route_ != nil) {
        
        [routes_ addObject:route_];
        [route_ release];
        route_ = nil;
        
    }
    
    if (xmlAnalysisState_ == BXPOAnalyzingStateIdle) {
        
        NSString *lowercaseElementName = [elementName lowercaseString];

        if ([lowercaseElementName isEqualToString:STATUS_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMDRAnalyzingStatus;
            
        } else if ([lowercaseElementName isEqualToString:ROUTE_ELEMENT_TAG]) {

            route_ = [[GoogleMapsRoute alloc] init];
            [self switchParser:parser
                toParserObject:route_
                withOpeningTag:lowercaseElementName];
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. The analyzed element is stored in the corresponding attribute
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    NSInteger xmlAnalysisState = xmlAnalysisState_;
	
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (route_ != nil) {
        
        [routes_ addObject:route_];
        [route_ release];
        route_ = nil;
        
    }
    
    if (xmlAnalysisState == GMDRAnalyzingStatus) {
        
        [status_ release];
        status_ = nil;
        status_ = [self.elementString copyWithZone:[self zone]];
        
    }
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the routes array
 *
 * @return The routes array
 */
- (NSArray *)routes {
    
    if (routes_ == nil) {
        
        return nil;
        
    } else {
     
        return [NSArray arrayWithArray:routes_];
        
    }
    
}

@end


#pragma mark -

@implementation GoogleMapsDirectionsResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Resets all attributes
 */
- (void)resetGoogleMapsDirectionsResponseAttributes {
    
    [status_ release];
    status_ = nil;
    
    if (routes_ == nil) {
        
        routes_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [routes_ removeAllObjects];
        
    }

    [route_ release];
    route_ = nil;
    
}

@end
