/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"
#import <CoreLocation/CoreLocation.h>


/**
 * Enumerates the Google Maps polyline analyzing states
 */
typedef enum {
    
    GMPAnalyzingPoints = BXPOAnalyzingStateCount, //!<Analyzing the polyline points
    GMPAnalyzingLevels, //!<Analyzing the polyline levels
    GMPAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMPAnalyzingState;


/**
 * Parses and stores a Google Maps polyline, both in string and array form. It includes
 * the polyline points and the points levels
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsPolyline : BaseXMLParserObject {

@private
    
    /**
     * Polyline points in string format
     */
    NSString *polylineString_;
    
    /**
     * Polyline points in array format. Each object in the array is an NSValue instance
     * containing CLLocationCoordinate2D value
     */
    NSArray *polylineArray_;
    
    /**
     * Polyline points levels in string format
     */
    NSString *levelsString_;
    
    /**
     * Polyline points levels in array format. Each object in the array is an NSNumber instance
     */
    NSArray *levelsArray_;

}



/**
 * Provides read-only access to the polyline points in string format
 */
@property (nonatomic, readonly, copy) NSString *polylineString;

/**
 * Provides read-only access to the polyline points in array format
 */
@property (nonatomic, readonly, retain) NSArray *polylineArray;

/**
 * Provides read-only access to the polyline points levels in string format
 */
@property (nonatomic, readonly, copy) NSString *levelsString;

/**
 * Provides read-only access to the polyline points levels in array format
 */
@property (nonatomic, readonly, retain) NSArray *levelsArray;


/**
 * Designated initializer. Initializes a GoogleMapsPolyline instance with the provided polyline string and level string.
 * When any one of those components cannot be analyzed, both the polyline string and level string (and the associated arrays)
 * are empty
 *
 * @param aPolylineString The polyline string to store. It must conform to the GoogleMaps polyline format
 * @param aLevelString The level string to store. It must conform to the GoogleMaps polyline format
 * @return The initialized GoogleMapsPolyline instance
 */
- (id)initWithPolyline:(NSString *)aPolylineString
                levels:(NSString *)aLevelString;

/**
 * Returns the simplified version of the polyline for a given tolerance
 *
 * @param aTolerance The tolerance from the original to the simplified polyline, measured in degrees
 * @return The simplified polyline
 */
- (GoogleMapsPolyline *)simplifiedPolylineWithTolerance:(double)aTolerance;

@end
