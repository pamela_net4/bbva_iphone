/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"


//Forward declarations
@class GoogleMapsDistance;
@class GoogleMapsDuration;
@class GoogleMapsInstructions;
@class GoogleMapsLocation;
@class GoogleMapsPolyline;


/**
 * Enumerates the Google Maps step analyzing states
 */
typedef enum {
    
    GMSAnalyzingTravelMode = BXPOAnalyzingStateCount, //!<Analyzing the step travel mode
    GMSAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMSAnalyzingState;


/**
 * Parses and stores a Google Maps step XML element
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsStep : BaseXMLParserObject {

@private
    
    /**
     * Step travel mode
     */
    NSString *travelMode_;
    
    /**
     * Step instructions
     */
    GoogleMapsInstructions *instructions_;
    
    /**
     * Step distance (may be nil if not known)
     */
    GoogleMapsDistance *distance_;
    
    /**
     * Step duration (may be nil if not known)
     */
    GoogleMapsDuration *duration_;
    
    /**
     * Step start location
     */
    GoogleMapsLocation *startLocation_;
    
    /**
     * Step end location
     */
    GoogleMapsLocation *endLocation_;
    
    /**
     * Step polyline
     */
    GoogleMapsPolyline *polyline_;
    
}


/**
 * Provides read-only access to the step travel mode
 */
@property (nonatomic, readonly, copy) NSString *travelMode;

/**
 * Provides read-only access to the step instructions
 */
@property (nonatomic, readonly, retain) GoogleMapsInstructions *instructions;

/**
 * Provides read-only access to the step distance
 */
@property (nonatomic, readonly, retain) GoogleMapsDistance *distance;

/**
 * Provides read-only access to the step duration
 */
@property (nonatomic, readonly, retain) GoogleMapsDuration *duration;

/**
 * Provides read-only access to the step start location
 */
@property (nonatomic, readonly, retain) GoogleMapsLocation *startLocation;

/**
 * Provides read-only access to the step end location
 */
@property (nonatomic, readonly, retain) GoogleMapsLocation *endLocation;

/**
 * Provides read-only access to the step polyline
 */
@property (nonatomic, readonly, retain) GoogleMapsPolyline *polyline;

@end
