/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"


/**
 * Enumerates the Google Maps instructions analyzing states
 */
typedef enum {
    
    GMIAnalyzingHTMLMark = BXPOAnalyzingStateCount, //!<Analyzing an HTML mark inside the element
    GMIAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMIAnalyzingState;


/**
 * Parses and stores a Google Maps HTML instructions XML element. The HTML marks are removed from the final string
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsInstructions : BaseXMLParserObject {

@private
    
    /**
     * Instructions string
     */
    NSMutableString *string_;

}

/**
 * Provides read-only access to the instructions string
 */
@property (nonatomic, readonly, copy) NSString *string;

@end
