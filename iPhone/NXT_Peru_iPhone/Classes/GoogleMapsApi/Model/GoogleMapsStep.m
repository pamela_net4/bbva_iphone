/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsStep.h"
#import "BaseXMLParserObject+protected.h"
#import "GoogleMapsDistance.h"
#import "GoogleMapsDuration.h"
#import "GoogleMapsInstructions.h"
#import "GoogleMapsLocation.h"
#import "GoogleMapsPolyline.h"


/**
 * Defines the travel mode element tag
 */
#define TRAVEL_MODE_ELEMENT_TAG                                 @"travel_mode"

/**
 * Defines the instructions element tag
 */
#define INSTRUCTIONS_ELEMENT_TAG                                @"html_instructions"

/**
 * Defines the distance element tag
 */
#define DISTANCE_ELEMENT_TAG                                    @"distance"

/**
 * Defines the duration element tag
 */
#define DURATION_ELEMENT_TAG                                    @"duration"

/**
 * Defines the start location element tag
 */
#define START_LOCATION_ELEMENT_TAG                              @"start_location"

/**
 * Defines the end location element tag
 */
#define END_LOCATION_ELEMENT_TAG                                @"end_location"

/**
 * Defines the polyline element tag
 */
#define POLYLINE_ELEMENT_TAG                                    @"polyline"


#pragma mark -

/**
 * GoogleMapsStep private category
 */
@interface GoogleMapsStep(private)

/**
 * Resets all attributes
 *
 * @private
 */
- (void)resetGoogleMapsStepAttributes;

@end


#pragma mark -

@implementation GoogleMapsStep

#pragma mark -
#pragma mark Properties

@synthesize travelMode = travelMode_;
@synthesize instructions = instructions_;
@synthesize distance = distance_;
@synthesize duration = duration_;
@synthesize startLocation = startLocation_;
@synthesize endLocation = endLocation_;
@synthesize polyline = polyline_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [self resetGoogleMapsStepAttributes];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. All attributes are released
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
    [super parserDidStartDocument:parser];
    
    [self resetGoogleMapsStepAttributes];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Element tag is analyzed to determine which
 * information is being analyzed
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisState_ == BXPOAnalyzingStateIdle) {
        
        NSString *lowercaseElementName = [elementName lowercaseString];

        if ([lowercaseElementName isEqualToString:TRAVEL_MODE_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMSAnalyzingTravelMode;
            
        } else if ([lowercaseElementName isEqualToString:INSTRUCTIONS_ELEMENT_TAG]) {
            
            [instructions_ release];
            instructions_ = nil;
            instructions_ = [[GoogleMapsInstructions alloc] init];
            [self switchParser:parser
                toParserObject:instructions_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:DISTANCE_ELEMENT_TAG]) {
            
            [distance_ release];
            distance_ = nil;
            distance_ = [[GoogleMapsDistance alloc] init];
            [self switchParser:parser
                toParserObject:distance_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:DURATION_ELEMENT_TAG]) {
            
            [duration_ release];
            duration_ = nil;
            duration_ = [[GoogleMapsDuration alloc] init];
            [self switchParser:parser
                toParserObject:duration_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:START_LOCATION_ELEMENT_TAG]) {
            
            [startLocation_ release];
            startLocation_ = nil;
            startLocation_ = [[GoogleMapsLocation alloc] init];
            [self switchParser:parser
                toParserObject:startLocation_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:END_LOCATION_ELEMENT_TAG]) {
            
            [endLocation_ release];
            endLocation_ = nil;
            endLocation_ = [[GoogleMapsLocation alloc] init];
            [self switchParser:parser
                toParserObject:endLocation_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:POLYLINE_ELEMENT_TAG]) {
            
            [polyline_ release];
            polyline_ = nil;
            polyline_ = [[GoogleMapsPolyline alloc] init];
            [self switchParser:parser
                toParserObject:polyline_
                withOpeningTag:lowercaseElementName];
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. The analyzed element is stored in the corresponding attribute
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    NSInteger xmlAnalysisState = xmlAnalysisState_;
	
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisState == GMSAnalyzingTravelMode) {
        
        [travelMode_ release];
        travelMode_ = nil;
        travelMode_ = [self.elementString copyWithZone:[self zone]];
        
    }
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

@end


#pragma mark -

@implementation GoogleMapsStep(private)

#pragma mark -
#pragma mark Memory management

/*
 * Resets all attributes
 */
- (void)resetGoogleMapsStepAttributes {
    
    [travelMode_ release];
    travelMode_ = nil;
    
    [instructions_ release];
    instructions_ = nil;
    
    [distance_ release];
    distance_ = nil;
    
    [duration_ release];
    duration_ = nil;
    
    [startLocation_ release];
    startLocation_ = nil;
    
    [endLocation_ release];
    endLocation_ = nil;
    
    [polyline_ release];
    polyline_ = nil;
    
}

@end
