/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"


//Forward declarations
@class GoogleMapsGeocodingResult;


/**
 * Enumerates the Google Maps geocoding response analyzing states
 */
typedef enum {
    
    GMGREAnalyzingStatus = BXPOAnalyzingStateCount, //!<Analyzing the geocoding response status
    GMGREAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMGREAnalyzingState;


/**
 * Parses and stores a Google Maps Geocoding response
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsGeocodingResponse : BaseXMLParserObject {

@private
    
    /**
     * Geocoding response status
     */
    NSString *status_;
    
    /**
     * Geocoding results array. All objects are GoogleMapsGeocodingResult instances
     */
    NSMutableArray *results_;
    
    /**
     * Auxiliary geocoding result to analyze one geocoding result
     */
    GoogleMapsGeocodingResult *result_;
    
}



/**
 * Provides read-only access to the directions response status
 */
@property (nonatomic, readonly, copy) NSString *status;

/**
 * Provides read-only access to the geocoding results array
 */
@property (nonatomic, readonly, retain) NSArray *results;

@end
