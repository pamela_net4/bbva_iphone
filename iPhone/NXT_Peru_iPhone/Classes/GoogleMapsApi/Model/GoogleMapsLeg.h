/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"


//Forward declarations
@class GoogleMapsDistance;
@class GoogleMapsDuration;
@class GoogleMapsLocation;
@class GoogleMapsStep;


/**
 * Enumerates the Google Maps leg analyzing states
 */
typedef enum {
    
    GMLEAnalyzingStartAddress = BXPOAnalyzingStateCount, //!<Analyzing the start address
    GMLEAnalyzingEndAddress, //!<Analyzing the end address
    GMLEAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMLEAnalyzingState;


/**
 * Parses and stores a Google Maps leg XML element
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsLeg : BaseXMLParserObject {

@private
    
    /**
     * Leg steps array. All objects are GoogleMapsStep instances
     */
    NSMutableArray *steps_;
    
    /**
     * Auxiliary leg step to analyze one leg step
     */
    GoogleMapsStep *step_;
    
    /**
     * Leg distance (may be nil if not known)
     */
    GoogleMapsDistance *distance_;
    
    /**
     * Leg duration (may be nil if not known)
     */
    GoogleMapsDuration *duration_;
    
    /**
     * Leg start location
     */
    GoogleMapsLocation *startLocation_;
    
    /**
     * Leg end location
     */
    GoogleMapsLocation *endLocation_;
    
    /**
     * Leg start address
     */
    NSString *startAddress_;
    
    /**
     * Leg end address
     */
    NSString *endAddress_;
    
}



/**
 * Provides read-only access to the leg steps array
 */
@property (nonatomic, readonly, retain) NSArray *steps;

/**
 * Provides read-only access to the leg distance (may be nil if not known)
 */
@property (nonatomic, readonly, retain) GoogleMapsDistance *distance;

/**
 * Provides read-only access to the leg duration (may be nil if not known)
 */
@property (nonatomic, readonly, retain) GoogleMapsDuration *duration;

/**
 * Provides read-only access to the leg start location
 */
@property (nonatomic, readonly, retain) GoogleMapsLocation *startLocation;

/**
 * Provides read-only access to the leg end location
 */
@property (nonatomic, readonly, retain) GoogleMapsLocation *endLocation;

/**
 * Provides read-only access to the leg start address
 */
@property (nonatomic, readonly, copy) NSString *startAddress;

/**
 * Provides read-only access to the leg end address
 */
@property (nonatomic, readonly, copy) NSString *endAddress;

@end
