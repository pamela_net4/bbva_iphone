/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsAddressComponent.h"
#import "BaseXMLParserObject+protected.h"
#import "GoogleMapsDistance.h"
#import "GoogleMapsDuration.h"
#import "GoogleMapsLocation.h"
#import "GoogleMapsStep.h"


/**
 * Defines the types element tag (this element can appear multiple times)
 */
#define TYPE_ELEMENT_TAG                                        @"type"

/**
 * Defines the long name element tag
 */
#define LONG_NAME_ELEMENT_TAG                                   @"long_name"

/**
 * Defines the short name element tag
 */
#define SHORT_NAME_ELEMENT_TAG                                  @"short_name"


#pragma mark -

/**
 * GoogleMapsAddressComponent private category
 */
@interface GoogleMapsAddressComponent(private)

/**
 * Resets all attributes
 *
 * @private
 */
- (void)resetGoogleMapsAddressComponentAttributes;

@end


#pragma mark -

@implementation GoogleMapsAddressComponent

#pragma mark -
#pragma mark Properties

@dynamic types;
@synthesize longName = longName_;
@synthesize shortName = shortName_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [self resetGoogleMapsAddressComponentAttributes];
    
    [types_ release];
    types_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. All attributes are released
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
    [super parserDidStartDocument:parser];
    
    [self resetGoogleMapsAddressComponentAttributes];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Element tag is analyzed to determine which
 * information is being analyzed
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisState_ == BXPOAnalyzingStateIdle) {
        
        NSString *lowercaseElementName = [elementName lowercaseString];

        if ([lowercaseElementName isEqualToString:TYPE_ELEMENT_TAG]) {

            xmlAnalysisState_ = GMACAnalyzingType;
            
        } else if ([lowercaseElementName isEqualToString:LONG_NAME_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMACAnalyzingLongName;
            
        } else if ([lowercaseElementName isEqualToString:SHORT_NAME_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMACAnalyzingShortName;
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. The analyzed element is stored in the corresponding attribute
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    NSInteger xmlAnalysisState = xmlAnalysisState_;
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];

    if (xmlAnalysisState == GMACAnalyzingType) {
        
        NSString *auxString = [NSString stringWithString:self.elementString];
        
        if (auxString != nil) {
            
            [types_ addObject:auxString];
            
        }
        
    } else if (xmlAnalysisState == GMACAnalyzingLongName) {
        
        [longName_ release];
        longName_ = nil;
        longName_ = [self.elementString copyWithZone:[self zone]];
        
    } else if (xmlAnalysisState == GMACAnalyzingShortName) {
        
        [shortName_ release];
        shortName_ = nil;
        shortName_ = [self.elementString copyWithZone:[self zone]];
        
    }
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the types array
 *
 * @return The types array
 */
- (NSArray *)types {
    
    return [NSArray arrayWithArray:types_];
    
}

@end


#pragma mark -

@implementation GoogleMapsAddressComponent(private)

#pragma mark -
#pragma mark Memory management

/*
 * Resets all attributes
 */
- (void)resetGoogleMapsAddressComponentAttributes {
    
    if (types_ == nil) {
        
        types_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [types_ removeAllObjects];
        
    }

    [longName_ release];
    longName_ = nil;
    
    [shortName_ release];
    shortName_ = nil;
    
}

@end
