/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"
#import <CoreLocation/CoreLocation.h>


/**
 * Enumerates the Google Maps location analyzing states
 */
typedef enum {
    
    GMLOAnalyzingLatitude = BXPOAnalyzingStateCount, //!<Analyzing the latitude element
    GMLOAnalyzingLongitude, //!<Analyzing the longitude element
    GMLOAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMLOAnalyzingState;


/**
 * Parses and stores a Google Maps location XML element, containing latitude and longitude
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsLocation : BaseXMLParserObject {

@private
    
    /**
     * Location latitude expressed as a floating point number
     */
    CLLocationDegrees latitude_;
    
    /**
     * Location longitude expressed as a floating point number
     */
    CLLocationDegrees longitude_;

}

/**
 * Provides read-only access to the location latitude expressed as a floating point number
 */
@property (nonatomic, readonly, assign) CLLocationDegrees latitude;

/**
 * Provides read-only access to the location longitude expressed as a floating point number
 */
@property (nonatomic, readonly, assign) CLLocationDegrees longitude;

/**
 * Provides read-only access to the location coordinates
 */
@property (nonatomic, readonly, assign) CLLocationCoordinate2D coordinate;

@end
