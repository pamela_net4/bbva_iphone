/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"


//Forward declarations
@class GoogleMapsPolyline;
@class GoogleMapsDuration;
@class GoogleMapsLocation;
@class GoogleMapsLeg;


/**
 * Enumerates the Google Maps route analyzing states
 */
typedef enum {
    
    GMRAnalyzingSummary = BXPOAnalyzingStateCount, //!<Analyzing the route summary
    GMRAnalyzingCopyrights, //!<Analyzing the route copyright
    GMRAnalyzingWarning, //!<Analyzing the route warning
    GMRAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMRAnalyzingState;


/**
 * Parses and stores a Google Maps route XML element
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsRoute : BaseXMLParserObject {

@private
    
    /**
     * Route summary
     */
    NSString *summary_;
    
    /**
     * Route legs array. All objects are GoogleMapsLeg instances
     */
    NSMutableArray *legs_;
    
    /**
     * Auxiliary route leg to analyze one route leg
     */
    GoogleMapsLeg *leg_;
    
    /**
     * Route polyline
     */
    GoogleMapsPolyline *polyline_;
    
    /**
     * Route detailled polyline (calculated by appending all legs polylines)
     */
    GoogleMapsPolyline *detailledPolyline_;
    
    /**
     * Route copyrights
     */
    NSString *copyrights_;
    
    /**
     * Route warnings. All objects are NSString instances
     */
    NSMutableArray *warnings_;
    
}



/**
 * Provides read-only access to the route summary
 */
@property (nonatomic, readonly, copy) NSString *summary;

/**
 * Provides read-only access to the route legs array
 */
@property (nonatomic, readonly, retain) NSArray *legs;

/**
 * Provides read-only access to the route polyline
 */
@property (nonatomic, readonly, retain) GoogleMapsPolyline *polyline;

/**
 * Provides read-only access to the route detailled polyline
 */
@property (nonatomic, readonly, retain) GoogleMapsPolyline *detailledPolyline;

/**
 * Provides read-only access to the route copyrights
 */
@property (nonatomic, readonly, copy) NSString *copyrights;

/**
 * Provides read-only access to the route warnings
 */
@property (nonatomic, readonly, retain) NSArray *warnings;

@end
