/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsGeometry.h"
#import "BaseXMLParserObject+protected.h"
#import "GoogleMapsLocation.h"
#import "GoogleMapsViewport.h"


/**
 * Defines the location element tag
 */
#define LOCATION_ELEMENT_TAG                                    @"location"

/**
 * Defines the location type element tag
 */
#define LOCATION_TYPE_ELEMENT_TAG                               @"location_type"

/**
 * Defines the viewport element tag
 */
#define VIEWPORT_ELEMENT_TAG                                    @"viewport"

/**
 * Defines the bounds element tag
 */
#define BOUNDS_ELEMENT_TAG                                      @"bounds"


#pragma mark -

/**
 * GoogleMapsGeometry private category
 */
@interface GoogleMapsGeometry(private)

/**
 * Resets all attributes
 *
 * @private
 */
- (void)resetGoogleMapsGeometryAttributes;

@end


#pragma mark -

@implementation GoogleMapsGeometry

#pragma mark -
#pragma mark Properties

@synthesize locationType = locationType_;
@synthesize location = location_;
@synthesize viewport = viewport_;
@synthesize bounds = bounds_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [self resetGoogleMapsGeometryAttributes];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. All attributes are released
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
    [super parserDidStartDocument:parser];
    
    [self resetGoogleMapsGeometryAttributes];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Element tag is analyzed to determine which
 * information is being analyzed
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisState_ == BXPOAnalyzingStateIdle) {
        
        NSString *lowercaseElementName = [elementName lowercaseString];

        if ([lowercaseElementName isEqualToString:LOCATION_TYPE_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMGAnalyzingLocationType;
            
        } else if ([lowercaseElementName isEqualToString:LOCATION_ELEMENT_TAG]) {
            
            [location_ release];
            location_ = nil;
            location_ = [[GoogleMapsLocation alloc] init];
            [self switchParser:parser
                toParserObject:location_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:VIEWPORT_ELEMENT_TAG]) {
            
            [viewport_ release];
            viewport_ = nil;
            viewport_ = [[GoogleMapsViewport alloc] init];
            [self switchParser:parser
                toParserObject:viewport_
                withOpeningTag:lowercaseElementName];
            
        } else if ([lowercaseElementName isEqualToString:BOUNDS_ELEMENT_TAG]) {
            
            [bounds_ release];
            bounds_ = nil;
            bounds_ = [[GoogleMapsViewport alloc] init];
            [self switchParser:parser
                toParserObject:bounds_
                withOpeningTag:lowercaseElementName];
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. The analyzed element is stored in the corresponding attribute
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    NSInteger xmlAnalysisState = xmlAnalysisState_;
	
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisState == GMGAnalyzingLocationType) {
        
        [locationType_ release];
        locationType_ = nil;
        locationType_ = [self.elementString copyWithZone:[self zone]];
        
    }
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

@end


#pragma mark -

@implementation GoogleMapsGeometry(private)

#pragma mark -
#pragma mark Memory management

/*
 * Resets all attributes
 */
- (void)resetGoogleMapsGeometryAttributes {
    
    [locationType_ release];
    locationType_ = nil;
    
    [location_ release];
    location_ = nil;
    
    [viewport_ release];
    viewport_ = nil;
    
    [bounds_ release];
    bounds_ = nil;
    
}

@end
