/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsDuration.h"
#import "BaseXMLParserObject+protected.h"


/**
 * Defines the duration in seconds element tag
 */
#define SECONDS_ELEMENT_TAG                             @"value"

/**
 * Defines the duration as string element tag
 */
#define STRING_ELEMENT_TAG                              @"text"


#pragma mark -

@implementation GoogleMapsDuration

#pragma mark -
#pragma mark Properties

@synthesize seconds = seconds_;
@synthesize string = string_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [string_ release];
    string_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. The duration seconds and duration string are reseted
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
    [super parserDidStartDocument:parser];
    
    seconds_ = 0;
    [string_ release];
    string_ = nil;
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Element tag is analyzed to determine which
 * information is being analyzed
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisState_ == BXPOAnalyzingStateIdle) {
        
        NSString *lowercaseElementName = [elementName lowercaseString];

        if ([lowercaseElementName isEqualToString:SECONDS_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMDUAnalyzingSeconds;
            
        } else if ([lowercaseElementName isEqualToString:STRING_ELEMENT_TAG]) {
            
            xmlAnalysisState_ = GMDUAnalyzingString;
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. The analyzed element is stored in the corresponding attribute
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
    NSInteger xmlAnalysisState = xmlAnalysisState_;
	
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisState == GMDUAnalyzingSeconds) {
        
        NSScanner *scanner = [NSScanner scannerWithString:self.elementString];
        
        if (![scanner scanInteger:&seconds_]) {
            
            seconds_ = 0;
        }
        
    } else if (xmlAnalysisState == GMDUAnalyzingString) {
        
        [string_ release];
        string_ = nil;
        string_ = [self.elementString copyWithZone:[self zone]];

    }
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

@end
