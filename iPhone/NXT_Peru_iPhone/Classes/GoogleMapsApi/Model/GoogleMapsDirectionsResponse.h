/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"


//Forward declarations
@class GoogleMapsRoute;


/**
 * Enumerates the Google Maps directions response analyzing states
 */
typedef enum {
    
    GMDRAnalyzingStatus = BXPOAnalyzingStateCount, //!<Analyzing the directions response status
    GMDRAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMDRAnalyzingState;


/**
 * Parses and stores a Google Maps directions response
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsDirectionsResponse : BaseXMLParserObject {

@private
    
    /**
     * Directions response status
     */
    NSString *status_;
    
    /**
     * Directions response routes array. All objects are GoogleMapsRoute instances
     */
    NSMutableArray *routes_;
    
    /**
     * Auxiliary directions response route to analyze one directions response route
     */
    GoogleMapsRoute *route_;
    
}



/**
 * Provides read-only access to the directions response status
 */
@property (nonatomic, readonly, copy) NSString *status;

/**
 * Provides read-only access to the directions response routes array
 */
@property (nonatomic, readonly, retain) NSArray *routes;

@end
