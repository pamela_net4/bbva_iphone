/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"


//Forward declarations
@class GoogleMapsLocation;


/**
 * Enumerates the Google Maps viewport analyzing states
 */
typedef enum {
    
    GMVAnalyzingStateCount = BXPOAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} GMVAnalyzingState;


/**
 * Parses and stores a Google Maps viewport XML element
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsViewport : BaseXMLParserObject {

@private
    
    /**
     * Viewport sothwest point
     */
    GoogleMapsLocation *southwest_;
    
    /**
     * Viewport northeast point
     */
    GoogleMapsLocation *northeast_;
    
}

/**
 * Provides read-only access to the viewport sothwest point
 */
@property (nonatomic, readonly, retain) GoogleMapsLocation *southwest;

/**
 * Provides read-only access to the viewport northeast point
 */
@property (nonatomic, readonly, retain) GoogleMapsLocation *northeast;

@end
