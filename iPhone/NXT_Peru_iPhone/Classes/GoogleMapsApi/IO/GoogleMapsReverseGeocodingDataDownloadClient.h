/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "XMLParserDataDownloadClient.h"
#import <CoreLocation/CoreLocation.h>


//Forward declarations
@class GoogleMapsGeocodingResponse;


/**
 * Data download client that analyzes a GoogleMaps API reverse geocoding response
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsReverseGeocodingDataDownloadClient : XMLParserDataDownloadClient {

@private
    
    /**
     * Reverse geocoding coordinate. The address for this coordinate will be obtained
     */
    CLLocationCoordinate2D coordinate_;
    
    /**
     * Reverse geociding language code
     */
    NSString *languageCode_;
    
    /**
     * GoogleMaps API geocoding response analyzer. It analyzes the XML returned from a GoogleMaps Geocoding API call
     */
    GoogleMapsGeocodingResponse *geocodingResponse_;
    
    /**
     * The analysis will be performed from cached information flag. YES when analysis will be performed from
     * cached data, NO when analysis will be performed from downloaded data
     */
    BOOL analysisFromCache_;
    
}


/**
 * Provides read-only access to the reverse geocoding coordinate
 */
@property (nonatomic, readonly, assign) CLLocationCoordinate2D coordinate;

/**
 * Provides read-only access to the reverse geociding language code
 */
@property (nonatomic, readonly, copy) NSString *languageCode;

/**
 * Provides read-only access to the GoogleMaps API geocoding response analyzed
 */
@property (nonatomic, readonly, retain) GoogleMapsGeocodingResponse *geocodingResponse;


/**
 * Designated initializer. Initializes an GoogleMapsReverseGeocodingDataDownloadClient instance with the provided information
 *
 * @param aCoordinate The Earth coordinate to reverse geocode
 * @param aLanguage The language code to obtain the response in
 * @return The initialized GoogleMapsReverseGeocodingDataDownloadClient instance
 */
- (id)initWithCoordinate:(CLLocationCoordinate2D)aCoordinate
                language:(NSString *)aLanguage;

@end
