/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsReverseGeocodingDataDownloadClient.h"
#import "GoogleMapsReverseGeocodingDataDownloadClient+caching.h"
#import "GoogleMapsGeocodingResponse.h"
#import "XMLParserDataDownloadClient+protected.h"
#import "GoogleMapsApiInvoker+fileCaching.h"


#pragma mark -

@implementation GoogleMapsReverseGeocodingDataDownloadClient

#pragma mark -
#pragma mark Properties

@synthesize coordinate = coordinate_;
@synthesize languageCode = languageCode_;
@synthesize geocodingResponse = geocodingResponse_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {

    [languageCode_ release];
    languageCode_ = nil;
    
    [geocodingResponse_ release];
    geocodingResponse_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes a GoogleMapsReverseGeocodingDataDownloadClient instance with default download parameters
 *
 * @return The initialized GoogleMapsReverseGeocodingDataDownloadClient instance
 */
- (id)init {
    
    CLLocationCoordinate2D location;
    location.latitude = 0.0f;
    location.longitude = 0.0f;
    
    return [self initWithCoordinate:location
                           language:@""];
    
}

/*
 * Designated initializer. Initializes an GoogleMapsReverseGeocodingDataDownloadClient instance with the provided information
 */
- (id)initWithCoordinate:(CLLocationCoordinate2D)aCoordinate
                language:(NSString *)aLanguage {
    
    if (self = [super init]) {
        
        coordinate_ = aCoordinate;
        languageCode_ = [aLanguage copyWithZone:[self zone]];
        geocodingResponse_ = [[GoogleMapsGeocodingResponse alloc] init];

    }
    
    return self;
    
}

#pragma mark -
#pragma mark XMLParserDataDownloadClient selectors

/**
 * Notifies the child class that the XML parsing finished correctly. The downloaded information is stored
 * inside the cache
 *
 * @param anXMLParserObject The error that happened while parsing
 * @param aDownloadedData The downloaded data that failed to be parsed as an XML document
 */
- (void)xmlParsingFinishedWithResult:(BaseXMLParserObject *)anXMLParserObject
                            fromData:(NSData *)aDownloadedData {
    
    [super xmlParsingFinishedWithResult:anXMLParserObject
                               fromData:aDownloadedData];
    
    if (!analysisFromCache_) {
        
        [GoogleMapsApiInvoker storeReverseGeocodingAPIData:aDownloadedData
                                              associatedTo:self];

    }
    
}

#pragma mark -
#pragma mark XMLParserDataDownloadClient selectors

/*
 * Returns the BaseXMLParserObject that must analyze the downloaded data, in this case,
 * the GoogleMaps Geocoding API XML analyzer
 *
 * @return The GoogleMaps Geocoding API XML analyzer
 */
- (BaseXMLParserObject *)parserObject {
    
    return geocodingResponse_;
    
}

@end


#pragma mark -

@implementation GoogleMapsReverseGeocodingDataDownloadClient(caching)

#pragma mark -
#pragma mark Caching management

/*
 * Returns the cached analysis flag
 * 
 * @return The cached analysis flag
 */
- (BOOL)analysisFromCache {
    
    return analysisFromCache_;
    
}

/*
 * Sets the new cached analysis flag
 *
 * @param aValue The new cached analysis flag
 */
- (void)setAnalysisFromCache:(BOOL)aValue {
    
    analysisFromCache_ = aValue;
    
}

@end
