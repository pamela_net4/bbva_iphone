/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsGeocodingDataDownloadClient.h"
#import "GoogleMapsGeocodingDataDownloadClient+caching.h"
#import "GoogleMapsGeocodingResponse.h"
#import "XMLParserDataDownloadClient+protected.h"
#import "GoogleMapsApiInvoker+fileCaching.h"


#pragma mark -

@implementation GoogleMapsGeocodingDataDownloadClient

#pragma mark -
#pragma mark Properties

@synthesize addressString = addressString_;
@synthesize languageCode = languageCode_;
@synthesize geocodingResponse = geocodingResponse_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {

    [addressString_ release];
    addressString_ = nil;
    
    [languageCode_ release];
    languageCode_ = nil;
    
    [geocodingResponse_ release];
    geocodingResponse_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes a GoogleMapsGeocodingDataDownloadClient instance with default download parameters
 *
 * @return The initialized GoogleMapsGeocodingDataDownloadClient instance
 */
- (id)init {
    
    return [self initWithString:@""
                       language:@""];
    
}

/*
 * Designated initializer. Initializes an GoogleMapsGeocodingDataDownloadClient instance with the provided information
 */
- (id)initWithString:(NSString *)anAddressString
            language:(NSString *)aLanguage {
    
    if (self = [super init]) {
        
        NSZone *zone = [self zone];
        addressString_ = [anAddressString copyWithZone:zone];
        languageCode_ = [aLanguage copyWithZone:zone];
        geocodingResponse_ = [[GoogleMapsGeocodingResponse alloc] init];

    }
    
    return self;
    
}

#pragma mark -
#pragma mark XMLParserDataDownloadClient selectors

/**
 * Notifies the child class that the XML parsing finished correctly. The downloaded information is stored
 * inside the cache
 *
 * @param anXMLParserObject The error that happened while parsing
 * @param aDownloadedData The downloaded data that failed to be parsed as an XML document
 */
- (void)xmlParsingFinishedWithResult:(BaseXMLParserObject *)anXMLParserObject
                            fromData:(NSData *)aDownloadedData {
    
    [super xmlParsingFinishedWithResult:anXMLParserObject
                               fromData:aDownloadedData];
    
    if (!analysisFromCache_) {
        
        [GoogleMapsApiInvoker storeGeocodingAPIData:aDownloadedData
                                       associatedTo:self];

    }
    
}

#pragma mark -
#pragma mark XMLParserDataDownloadClient selectors

/*
 * Returns the BaseXMLParserObject that must analyze the downloaded data, in this case,
 * the GoogleMaps Geocoding API XML analyzer
 *
 * @return The GoogleMaps Geocoding API XML analyzer
 */
- (BaseXMLParserObject *)parserObject {
    
    return geocodingResponse_;
    
}

@end


#pragma mark -

@implementation GoogleMapsGeocodingDataDownloadClient(caching)

#pragma mark -
#pragma mark Caching management

/*
 * Returns the cached analysis flag
 * 
 * @return The cached analysis flag
 */
- (BOOL)analysisFromCache {
    
    return analysisFromCache_;
    
}

/*
 * Sets the new cached analysis flag
 *
 * @param aValue The new cached analysis flag
 */
- (void)setAnalysisFromCache:(BOOL)aValue {
    
    analysisFromCache_ = aValue;
    
}

@end
