/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "MapRouteDirectionsDataDownloadClient.h"

#import "HTTPInvoker.h"
#import "XMLParserDataDownloadClient+protected.h"

@implementation MapRouteDirectionsDataDownloadClient

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {

delegate_ = nil;

[super dealloc];

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. A MapRouteDirectionsDataDownloadClient without associated MapRouteViewController instance is initalized
 *
 * @return The initialized MapRouteDirectionsDataDownloadClient instance
 */
- (id)initWithOriginPoint:(CLLocationCoordinate2D)anOriginPoint
         destinationPoint:(CLLocationCoordinate2D)aDestinationPoint
                 language:(NSString *)aLanguage
               routeByCar:(BOOL)routeByCarFlag
             metricSystem:(BOOL)metricSystemFlag {
    
    return [self initWithOriginPoint:anOriginPoint
                    destinationPoint:aDestinationPoint
                            language:aLanguage
                          routeByCar:routeByCarFlag
                        metricSystem:metricSystemFlag
mapRouteDirectionsDataDownloadClientDelegate:nil];
    
}

/*
 * Initializes a MapsRouteDirectionsDataDownloadClient instance providing it with the associated MapRouteViewController instance
 */
- (id)initWithOriginPoint:(CLLocationCoordinate2D)anOriginPoint
         destinationPoint:(CLLocationCoordinate2D)aDestinationPoint
                 language:(NSString *)aLanguage
               routeByCar:(BOOL)routeByCarFlag
             metricSystem:(BOOL)metricSystemFlag
mapRouteDirectionsDataDownloadClientDelegate:(id<MapRouteDirectionsDataDownloadClientDelegate>)delegate {
    
    if ((self = [super initWithOriginPoint:anOriginPoint
                          destinationPoint:aDestinationPoint
                                  language:aLanguage
                                routeByCar:routeByCarFlag
                              metricSystem:metricSystemFlag])) {
        
        delegate_ = delegate;
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark XMLParserDataDownloadClient selectors

/**
 * Notifies the child class that the XML parsing finished correctly. Displays the route inside the map view
 * and the activity indicator is hidden
 *
 * @param anXMLParserObject The object containing the parsed XML document
 * @param aDownloadedData The downloaded data that generated the parsed XML document
 */
- (void)xmlParsingFinishedWithResult:(BaseXMLParserObject *)anXMLParserObject
                            fromData:(NSData *)aDownloadedData {
    
    [super xmlParsingFinishedWithResult:anXMLParserObject
                               fromData:aDownloadedData];
    
    [delegate_ routeInformationParsed:self];
    
}

/**
 * Notifies the child class that the XML parsing was not possible due to an error. Notifies the user
 * about the error and the activity indicator is hidden
 *
 * @param anXMLError The error that happened while parsing
 * @param aDownloadedData The downloaded data that failed to be parsed as an XML document
 */
- (void)xmlParsingFinishedWithError:(NSError *)anXMLError
                           fromData:(NSData *)aDownloadedData {
    
    [super xmlParsingFinishedWithError:anXMLError
                              fromData:aDownloadedData];
    
    [delegate_ routeParsingProcessError:anXMLError];
    
}

#pragma mark -
#pragma mark DataDownloadClient protocol selectors

/**
 * Notifies the client that the download has finished with a status code different to OK. The data downloaded from the server is provided
 * to analyze it, aswell as the status code. The user is notified about the error and activity indicator is hidden
 *
 * @param aDownloadId The string that identifies the download process
 * @param aDownloadedData The data downloaded from the server
 * @param aStatusCode The operation status code
 */
- (void)dataDownload:(NSString *)aDownloadId finishedWithData:(NSData *)aDownloadedData statusCode:(NSInteger)aStatusCode {
    
    //    [super dataDownload:aDownloadId
    //       finishedWithData:aDownloadedData
    //             statusCode:aStatusCode];
    [delegate_ routeDownloadProcessError:nil];
    
}

/**
 * Notifies the client that an error appeared during data download. Data downloaded and error are provided
 * to the client to analyze them. The user is notified about the error and the activity indicator is hidden
 *
 * @param aDownloadId The string that identifies the download process
 * @param anError The error that appeared during the data download
 * @param aDownloadedData The data downloaded until the error appeared
 */
- (void)dataDownload:(NSString *)aDownloadId finishedWithError:(NSError *)anError downloadedData:(NSData *)aDownloadedData {
    
    [super dataDownload:aDownloadId
      finishedWithError:anError
         downloadedData:aDownloadedData];
    
    [delegate_ routeDownloadProcessError:anError];
    
}

/**
 * Notifies the client that the data download was cancelled. Activity indicator is hidden
 *
 * @param aDownloadId The string that identifies the download process
 */
- (void)dataDownloadCancelled:(NSString *)aDownloadId {
    
    [super dataDownloadCancelled:aDownloadId];
    
    [delegate_ routeDownloadCancelled];
    
}

@end
