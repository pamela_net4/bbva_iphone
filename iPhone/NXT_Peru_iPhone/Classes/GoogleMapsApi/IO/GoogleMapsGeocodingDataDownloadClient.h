/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "XMLParserDataDownloadClient.h"
#import <CoreLocation/CoreLocation.h>


//Forward declarations
@class GoogleMapsGeocodingResponse;


/**
 * Data download client that analyzes a GoogleMaps API geocoding response
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsGeocodingDataDownloadClient : XMLParserDataDownloadClient {

@private
    
    /**
     * Geocoding string. Addresses similar to this string will be obtained
     */
    NSString *addressString_;
    
    /**
     * Geociding language code
     */
    NSString *languageCode_;
    
    /**
     * GoogleMaps API geocoding response analyzer. It analyzes the XML returned from a GoogleMaps Geocoding API call
     */
    GoogleMapsGeocodingResponse *geocodingResponse_;
    
    /**
     * The analysis will be performed from cached information flag. YES when analysis will be performed from
     * cached data, NO when analysis will be performed from downloaded data
     */
    BOOL analysisFromCache_;
    
}


/**
 * Provides read-only access to the geocoding string
 */
@property (nonatomic, readonly, copy) NSString *addressString;

/**
 * Provides read-only access to the reverse geociding language code
 */
@property (nonatomic, readonly, copy) NSString *languageCode;

/**
 * Provides read-only access to the GoogleMaps API geocoding response analyzed
 */
@property (nonatomic, readonly, retain) GoogleMapsGeocodingResponse *geocodingResponse;


/**
 * Designated initializer. Initializes an GoogleMapsGeocodingDataDownloadClient instance with the provided information
 *
 * @param anAddressString The string to look for
 * @param aLanguage The language code to obtain the response in
 * @return The initialized GoogleMapsGeocodingDataDownloadClient instance
 */
- (id)initWithString:(NSString *)anAddressString
            language:(NSString *)aLanguage;

@end
