/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsApiInvoker.h"
#import <CoreLocation/CoreLocation.h>
#import "GoogleMapsDirectionsDataDownloadClient.h"
#import "GoogleMapsDirectionsDataDownloadClient+caching.h"
#import "GoogleMapsReverseGeocodingDataDownloadClient.h"
#import "GoogleMapsReverseGeocodingDataDownloadClient+caching.h"
#import "GoogleMapsGeocodingDataDownloadClient.h"
#import "GoogleMapsGeocodingDataDownloadClient+caching.h"
#import "DownloadFileCache.h"
#import "NSString+URLAndHTMLUtils.h"
#import "HTTPInvoker.h"
#import "Tools.h"


/**
 * Defines the GoogleMaps Directions API associated directory name (it stores the caches and additional information)
 */
#define GOOGLE_MAPS_DIRECTIONS_API_DIRECTORY                                        @"GoogleMapsApiInvoker_Directions"

/**
 * Defines the GoogleMaps Directions API cache configuration file name
 */
#define GOOGLE_MAPS_DIRECTIONS_API_CACHE_CONFIGURATION_FILE_NAME                    @"DirectionsAPICacheConfig.plist"

/**
 * Defines the GoogleMaps Reverse Geocoding API associated directory name (it stores the caches and additional information)
 */
#define GOOGLE_MAPS_REVERSE_GEOCODING_API_DIRECTORY                                 @"GoogleMapsApiInvoker_ReverseGeocoding"

/**
 * Defines the GoogleMaps Reverse Geocoding API cache configuration file name
 */
#define GOOGLE_MAPS_REVERSE_GEOCODING_API_CACHE_CONFIGURATION_FILE_NAME             @"ReverseGeocodingAPICacheConfig.plist"

/**
 * Defines the GoogleMaps Geocoding API associated directory name (it stores the caches and additional information)
 */
#define GOOGLE_MAPS_GEOCODING_API_DIRECTORY                                         @"GoogleMapsApiInvoker_Geocoding"

/**
 * Defines the GoogleMaps Geocoding API cache configuration file name
 */
#define GOOGLE_MAPS_GEOCODING_API_CACHE_CONFIGURATION_FILE_NAME                     @"GeocodingAPICacheConfig.plist"



/**
 * Defines the maximum time directions information can be cached (measured in seconds, and currenly set to 5 days)
 */
#define DIRECTIONS_MAXIMUM_CACHING_TIME                                             (8600.0f * 5.0f)

/**
 * Defines the maximum GoogleMaps Directions API used space (set to 4Mb)
 */
#define MAXIMUM_GOOGLE_MAPS_DIRECTIONS_API_USED_SPACE                               4194304

/**
 * Defines the maximum time reverse geocoding information can be cached (measured in seconds, and currenly set to 5 days)
 */
#define REVERSE_GEOCODING_MAXIMUM_CACHING_TIME                                      (8600.0f * 5.0f)

/**
 * Defines the maximum GoogleMaps Reverse Geocoding API used space (set to 4Mb)
 */
#define MAXIMUM_GOOGLE_MAPS_REVERSE_GEOCODING_API_USED_SPACE                        4194304

/**
 * Defines the maximum time geocoding information can be cached (measured in seconds, and currenly set to 5 days)
 */
#define GEOCODING_MAXIMUM_CACHING_TIME                                              (8600.0f * 5.0f)

/**
 * Defines the maximum GoogleMaps Geocoding API used space (set to 4Mb)
 */
#define MAXIMUM_GOOGLE_MAPS_GEOCODING_API_USED_SPACE                                4194304


/**
 * Defines the GoogleMaps directions API URL initial section (up to, but not including, the parameters)
 */
#define GOOGLE_MAPS_DIRECITONS_API_URL_INITIAL_SECTION                              @"http://maps.googleapis.com/maps/api/directions/xml?"

/**
 * Defines the route origin point URL parameter
 */
#define ROUTE_ORIGIN_POINT                                                          @"origin"

/**
 * Defines the route destination point URL parameter
 */
#define ROUTE_DESTINATION_POINT                                                     @"destination"

/**
 * Defines the route traveling mode URL parameter
 */
#define ROUTE_TRAVELING_MODE                                                        @"mode"

/**
 * Defines the route units URL parameter
 */
#define ROUTE_UNITS                                                                 @"units"

/**
 * Defines the route language URL parameter
 */
#define ROUTE_LANGUAGE                                                              @"language"

/**
 * Defines the route obtained from location sensor URL parameter
 */
#define ROUTE_SENSOR                                                                @"sensor"

/**
 * Defines the route driving traveling mode URL parameter value
 */
#define ROUTE_DRIVING_TRAVELING_MODE_VALUE                                          @"driving"

/**
 * Defines the route walking traveling mode URL parameter value
 */
#define ROUTE_WALKING_TRAVELING_MODE_VALUE                                          @"walking"

/**
 * Defines the route metric units URL parameter value
 */
#define ROUTE_METRIC_UNITS_VALUE                                                    @"metric"

/**
 * Defines the route imperial units URL parameter value
 */
#define ROUTE_IMPERIAL_UNITS_VALUE                                                  @"imperial"


/**
 * Defines the GoogleMaps Geocoding API URL initial section (up to, but not including, the parameters)
 */
#define GOOGLE_MAPS_GEOCODING_API_URL_INITIAL_SECTION                               @"http://maps.googleapis.com/maps/api/geocode/xml?"

/**
 * Defines the reverse geocoding coordinate URL parameter
 */
#define REVERSE_GEOCODING_COORDINATE                                                @"latlng"

/**
 * Defines the geocoding address URL parameter
 */
#define GEOCODING_ADDRESS                                                           @"address"

/**
 * Defines the geocoding language URL parameter
 */
#define GEOCODING_LANGUAGE                                                          @"language"

/**
 * Defines the geocoding coordinate obtained from location sensor URL parameter
 */
#define GEOCODING_SENSOR                                                            @"sensor"

/**
 * Defines the geocoding region parameter
 */
#define GEOCODING_REGION                                                            @"region"

/**
 * Defines the geocoding region parameter value
 */
#define GEOCODING_REGION_VALUE                                                      @"es_pe"


/**
 * Defines the TRUE value for a boolean URL parameter value
 */
#define PARAMETER_TRUE_VALUE                                                        @"true"

/**
 * Defines the FALSE value for a boolean URL parameter value
 */
#define PARAMETER_FALSE_VALUE                                                       @"false"


#pragma mark -

/**
 * GoogleMapsApiInvoker private category
 */
@interface GoogleMapsApiInvoker(private)

/**
 * Returns the singleton only instance
 *
 * @result The singleton only instance
 * @private
 */
+ (GoogleMapsApiInvoker *)getInstance;

/**
 * Constructs the parameter string to perform a GoogleMaps Directions API request with the provided information
 *
 * @param aGoogleMapsDirectionsDataDownloadClient The object containing the information to construct the parameter string
 * @return The resulting parameter string
 * @private
 */
+ (NSString *)directionsAPIParameterStringForData:(GoogleMapsDirectionsDataDownloadClient *)aGoogleMapsDirectionsDataDownloadClient;

/**
 * Constructs the parameter string to perform a GoogleMaps Reverse Geocoding API request with the provided information
 *
 * @param aGoogleMapsReverseGeocodingDataDownloadClient The object containing the information to construct the parameter string
 * @return The resulting parameter string
 * @private
 */
+ (NSString *)reverseGeocodingAPIParameterStringForData:(GoogleMapsReverseGeocodingDataDownloadClient *)aGoogleMapsReverseGeocodingDataDownloadClient;

/**
 * Constructs the parameter string to perform a GoogleMaps Geocoding API request with the provided information
 *
 * @param aGoogleMapsGeocodingDataDownloadClient The object containing the information to construct the parameter string
 * @return The resulting parameter string
 * @private
 */
+ (NSString *)geocodingAPIParameterStringForData:(GoogleMapsGeocodingDataDownloadClient *)aGoogleMapsGeocodingDataDownloadClient;

/**
 * Returns the NSData containing the information associated to the given GoogleMaps Directions API parameter string. When
 * no information is stored, nil is returned
 *
 * @param aDirectionsAPIParameterString The GoogleMaps Directions API parameter string to look for
 * @return The NSData containing the information associated to the given GoogleMaps Directions API parameter string
 * @private
 */
- (NSData *)dataForDirectionsAPIParameterString:(NSString *)aDirectionsAPIParameterString;

/**
 * Returns the NSData containing the information associated to the given GoogleMaps Reverse Geocoding API parameter string. When
 * no information is stored, nil is returned
 *
 * @param aReverseGeocodingAPIParameterString The GoogleMaps Reverse Geocoding API parameter string to look for
 * @return The NSData containing the information associated to the given GoogleMaps Reverse Geocoding API parameter string
 * @private
 */
- (NSData *)dataForReverseGeocodingAPIParameterString:(NSString *)aReverseGeocodingAPIParameterString;

/**
 * Returns the NSData containing the information associated to the given GoogleMaps Geocoding API parameter string. When
 * no information is stored, nil is returned
 *
 * @param aGeocodingAPIParameterString The GoogleMaps Geocoding API parameter string to look for
 * @return The NSData containing the information associated to the given GoogleMaps Geocoding API parameter string
 * @private
 */
- (NSData *)dataForGeocodingAPIParameterString:(NSString *)aGeocodingAPIParameterString;

@end


#pragma mark -

@implementation GoogleMapsApiInvoker

#pragma mark -
#pragma mark Static attributes

/**
 * Singleton only instance
 */
static GoogleMapsApiInvoker *googleMapsApiInvokerInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * Allocates the singleton instance, in case it is not already allocated
 *
 * @param zone The memory zone to allocate the singleton in
 * @return The newly allocated singleton instance, or nil if singleton was already allocated
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([GoogleMapsApiInvoker class]) {
        
		if (googleMapsApiInvokerInstance_ == nil) {
            
			googleMapsApiInvokerInstance_ = [super allocWithZone:zone];
			return googleMapsApiInvokerInstance_;
            
		}
        
	}
	
	return nil;
    
}

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [directionsAPICache_ release];
    directionsAPICache_ = nil;
    
    [reverseGeocodingAPICache_ release];
    reverseGeocodingAPICache_ = nil;
    
    [geocodingAPICache_ release];
    geocodingAPICache_ = nil;
    
    googleMapsApiInvokerInstance_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a GoogleMapsApiInvoker instance, creating the necesary elements
 *
 * @return The initialized GoogleMapsApiInvoker instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        directionsAPICache_ = [[DownloadFileCache alloc] initWithCacheDirectory:GOOGLE_MAPS_DIRECTIONS_API_DIRECTORY
                                                       cacheInformationFileName:GOOGLE_MAPS_DIRECTIONS_API_CACHE_CONFIGURATION_FILE_NAME
                                                                 expirationTime:DIRECTIONS_MAXIMUM_CACHING_TIME
                                                                   maximumSpace:MAXIMUM_GOOGLE_MAPS_DIRECTIONS_API_USED_SPACE];
        
        reverseGeocodingAPICache_ = [[DownloadFileCache alloc] initWithCacheDirectory:GOOGLE_MAPS_REVERSE_GEOCODING_API_DIRECTORY
                                                             cacheInformationFileName:GOOGLE_MAPS_REVERSE_GEOCODING_API_CACHE_CONFIGURATION_FILE_NAME
                                                                       expirationTime:REVERSE_GEOCODING_MAXIMUM_CACHING_TIME
                                                                         maximumSpace:MAXIMUM_GOOGLE_MAPS_REVERSE_GEOCODING_API_USED_SPACE];
        
        geocodingAPICache_ = [[DownloadFileCache alloc] initWithCacheDirectory:GOOGLE_MAPS_GEOCODING_API_DIRECTORY
                                                      cacheInformationFileName:GOOGLE_MAPS_GEOCODING_API_CACHE_CONFIGURATION_FILE_NAME
                                                                expirationTime:GEOCODING_MAXIMUM_CACHING_TIME
                                                                  maximumSpace:MAXIMUM_GOOGLE_MAPS_GEOCODING_API_USED_SPACE];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark GoogleMaps directions API

/*
 * Invokes the directions API from an origin point to a destination point, using the fastest route
 */
+ (BOOL)routeForDirectionClient:(GoogleMapsDirectionsDataDownloadClient *)aDataDownloadClient
             obtainedFromSensor:(BOOL)aSensorFlag {

    BOOL result = NO;
    
    NSString *parameterString = [GoogleMapsApiInvoker directionsAPIParameterStringForData:aDataDownloadClient];
    
    if ([parameterString length] > 0) {
        
        GoogleMapsApiInvoker *apiInvoker = [GoogleMapsApiInvoker getInstance];

        [apiInvoker->directionsAPICache_ checkCachePolicy];
        
        NSData *cachedData = [apiInvoker dataForDirectionsAPIParameterString:parameterString];
        
        if (cachedData != nil) {
            
            static NSString * const cachedDownloadId = @"cacheInfo";
            
            aDataDownloadClient.analysisFromCache = YES;
            [aDataDownloadClient setDownloadId:cachedDownloadId];
            
            if ([aDataDownloadClient respondsToSelector:@selector(dataDownloadStarted:)]) {
                
                [aDataDownloadClient dataDownloadStarted:cachedDownloadId];
                
            }
            
            [aDataDownloadClient dataDownload:cachedDownloadId
                             finishedWithData:cachedData];
            
            [apiInvoker->directionsAPICache_ updateCacheInformationForKey:parameterString];
            
            result = YES;
            
        } else {
            
            NSMutableString *urlString = [NSMutableString stringWithString:GOOGLE_MAPS_DIRECITONS_API_URL_INITIAL_SECTION];
            [urlString appendString:parameterString];
            
            NSString *sensorFlagString = PARAMETER_FALSE_VALUE;
            
            if (aSensorFlag) {
                
                sensorFlagString = PARAMETER_TRUE_VALUE;
                
            }
            
            [urlString appendFormat:@"&%@=%@", ROUTE_SENSOR, [sensorFlagString urlEncodeUsingEncoding:NSUTF8StringEncoding]];
            
            NSURL *url = [NSURL URLWithString:urlString];
            HTTPInvoker *httpInvoker = [HTTPInvoker getInstance];
            result = [httpInvoker invokeGETOperationToURL:url
                                                 withBody:nil
                                              contentType:nil
                                         httpHeaderFields:nil
                                           downloadClient:aDataDownloadClient];
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark GoogleMaps reverse geocoding API

/*
 * Invokes the reverse geocoding API for an Earth coordinate
 */
+ (BOOL)addressForReverseGeocodingClient:(GoogleMapsReverseGeocodingDataDownloadClient *)aDataDownloadClient
                      obtainedFromSensor:(BOOL)aSensorFlag {
    
    BOOL result = NO;
    
    NSString *parameterString = [GoogleMapsApiInvoker reverseGeocodingAPIParameterStringForData:aDataDownloadClient];
    
    if ([parameterString length] > 0) {
        
        GoogleMapsApiInvoker *apiInvoker = [GoogleMapsApiInvoker getInstance];
        
        [apiInvoker->reverseGeocodingAPICache_ checkCachePolicy];
        
        NSData *cachedData = [apiInvoker dataForReverseGeocodingAPIParameterString:parameterString];
        
        if (cachedData != nil) {
            
            static NSString * const cachedDownloadId = @"cacheInfo";
            
            aDataDownloadClient.analysisFromCache = YES;
            [aDataDownloadClient setDownloadId:cachedDownloadId];
            
            if ([aDataDownloadClient respondsToSelector:@selector(dataDownloadStarted:)]) {
                
                [aDataDownloadClient dataDownloadStarted:cachedDownloadId];
                
            }
            
            [aDataDownloadClient dataDownload:cachedDownloadId
                             finishedWithData:cachedData];
            
            [apiInvoker->reverseGeocodingAPICache_ updateCacheInformationForKey:parameterString];
            
            result = YES;
            
        } else {
            
            NSMutableString *urlString = [NSMutableString stringWithString:GOOGLE_MAPS_GEOCODING_API_URL_INITIAL_SECTION];
            [urlString appendString:parameterString];
            
            NSString *sensorFlagString = PARAMETER_FALSE_VALUE;
            
            if (aSensorFlag) {
                
                sensorFlagString = PARAMETER_TRUE_VALUE;
                
            }
            
            [urlString appendFormat:@"&%@=%@", GEOCODING_SENSOR, [sensorFlagString urlEncodeUsingEncoding:NSUTF8StringEncoding]];
            
            NSURL *url = [NSURL URLWithString:urlString];
            HTTPInvoker *httpInvoker = [HTTPInvoker getInstance];
            result = [httpInvoker invokeGETOperationToURL:url
                                                 withBody:nil
                                              contentType:nil
                                         httpHeaderFields:nil
                                           downloadClient:aDataDownloadClient];
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark GoogleMaps geocoding API

/*
 * Invokes the geocoding API for an address string
 */
+ (BOOL)addressForGeocodingClient:(GoogleMapsGeocodingDataDownloadClient *)aDataDownloadClient {
    
    BOOL result = NO;
    
    NSString *parameterString = [GoogleMapsApiInvoker geocodingAPIParameterStringForData:aDataDownloadClient];
    
    if ([parameterString length] > 0) {
        
        GoogleMapsApiInvoker *apiInvoker = [GoogleMapsApiInvoker getInstance];
        
        [apiInvoker->geocodingAPICache_ checkCachePolicy];
        
        NSData *cachedData = [apiInvoker dataForGeocodingAPIParameterString:parameterString];
        
        if (cachedData != nil) {
            
            static NSString * const cachedDownloadId = @"cacheInfo";
            
            aDataDownloadClient.analysisFromCache = YES;
            [aDataDownloadClient setDownloadId:cachedDownloadId];
            
            if ([aDataDownloadClient respondsToSelector:@selector(dataDownloadStarted:)]) {
                
                [aDataDownloadClient dataDownloadStarted:cachedDownloadId];
                
            }
            
            [aDataDownloadClient dataDownload:cachedDownloadId
                             finishedWithData:cachedData];
            
            [apiInvoker->geocodingAPICache_ updateCacheInformationForKey:parameterString];
            
            result = YES;
            
        } else {
            
            NSMutableString *urlString = [NSMutableString stringWithString:GOOGLE_MAPS_GEOCODING_API_URL_INITIAL_SECTION];
            [urlString appendString:parameterString];
            
            NSString *sensorFlagString = PARAMETER_FALSE_VALUE;
            
            [urlString appendFormat:@"&%@=%@", GEOCODING_SENSOR, [sensorFlagString urlEncodeUsingEncoding:NSUTF8StringEncoding]];
            
            NSURL *url = [NSURL URLWithString:urlString];
            HTTPInvoker *httpInvoker = [HTTPInvoker getInstance];
            result = [httpInvoker invokeGETOperationToURL:url
                                                 withBody:nil
                                              contentType:nil
                                         httpHeaderFields:nil
                                           downloadClient:aDataDownloadClient];
            
        }
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation GoogleMapsApiInvoker(fileCaching)

#pragma mark -
#pragma mark Data caching

/*
 * Stores the provided data associated to the GoogleMaps Directions API download parameters. When previous data is associated
 * to the download parameters, it is replaced by the new one
 */
+ (void)storeDirectionsAPIData:(NSData *)aDownloadedData
                  associatedTo:(GoogleMapsDirectionsDataDownloadClient *)aGoogleMapsDirectionsDataDownloadClient {
    
    if ([aDownloadedData length] > 0) {
        
        NSString *parametersString = [GoogleMapsApiInvoker directionsAPIParameterStringForData:aGoogleMapsDirectionsDataDownloadClient];
        
        if ([parametersString length] > 0) {
            
            GoogleMapsApiInvoker *apiInvoker = [GoogleMapsApiInvoker getInstance];
            [apiInvoker->directionsAPICache_ storeData:aDownloadedData
                                          forKeyString:parametersString];
            
        }
        
    }
    
}

/*
 * Stores the provided data associated to the GoogleMaps Reverse Geocoding API download parameters. When previous data is associated
 * to the download parameters, it is replaced by the new one
 */
+ (void)storeReverseGeocodingAPIData:(NSData *)aDownloadedData
                        associatedTo:(GoogleMapsReverseGeocodingDataDownloadClient *)aGoogleMapsReverseGeocodingDataDownloadClient {
    
    if ([aDownloadedData length] > 0) {
        
        NSString *parametersString = [GoogleMapsApiInvoker reverseGeocodingAPIParameterStringForData:aGoogleMapsReverseGeocodingDataDownloadClient];
        
        if ([parametersString length] > 0) {
            
            GoogleMapsApiInvoker *apiInvoker = [GoogleMapsApiInvoker getInstance];
            [apiInvoker->reverseGeocodingAPICache_ storeData:aDownloadedData
                                                forKeyString:parametersString];
            
        }
        
    }
    
}

/*
 * Stores the provided data associated to the GoogleMaps Geocoding API download parameters. When previous data is associated
 * to the download parameters, it is replaced by the new one
 */
+ (void)storeGeocodingAPIData:(NSData *)aDownloadedData
                 associatedTo:(GoogleMapsGeocodingDataDownloadClient *)aGoogleMapsGeocodingDataDownloadClient {
    
    if ([aDownloadedData length] > 0) {
        
        NSString *parametersString = [GoogleMapsApiInvoker geocodingAPIParameterStringForData:aGoogleMapsGeocodingDataDownloadClient];
        
        if ([parametersString length] > 0) {
            
            GoogleMapsApiInvoker *apiInvoker = [GoogleMapsApiInvoker getInstance];
            [apiInvoker->geocodingAPICache_ storeData:aDownloadedData
                                         forKeyString:parametersString];
            
        }
        
    }
    
}

@end


#pragma mark -

@implementation GoogleMapsApiInvoker(private)

#pragma mark -
#pragma mark Singleton selectors

/*
 * Returns the singleton only instance
 */
+ (GoogleMapsApiInvoker *)getInstance {
    
	if (googleMapsApiInvokerInstance_ == nil) {
        
		@synchronized ([GoogleMapsApiInvoker class]) {
            
			if (googleMapsApiInvokerInstance_ == nil) {
                
				googleMapsApiInvokerInstance_ = [[GoogleMapsApiInvoker alloc] init];
                
			}
            
		}
        
	}
	
	return googleMapsApiInvokerInstance_;
    
}

#pragma mark -
#pragma mark Directions API

/*
 * Constructs the parameter string to perform a GoogleMaps Directions API request with the provided information
 */
+ (NSString *)directionsAPIParameterStringForData:(GoogleMapsDirectionsDataDownloadClient *)aGoogleMapsDirectionsDataDownloadClient {
    
    NSMutableString *mutableResult = [NSMutableString string];
    
    CLLocationCoordinate2D originPoint = aGoogleMapsDirectionsDataDownloadClient.originPoint;
    NSString *originLatitude = [NSString stringWithFormat:@"%.7f", originPoint.latitude];
    NSString *originLongitude = [NSString stringWithFormat:@"%.7f", originPoint.longitude];
    NSString *originString = [NSString stringWithFormat:@"%@,%@", originLatitude, originLongitude];
    
    [mutableResult appendFormat:@"%@=%@", ROUTE_ORIGIN_POINT, [originString urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    CLLocationCoordinate2D destinationPoint = aGoogleMapsDirectionsDataDownloadClient.destinationPoint;
    NSString *destinationLatitude = [NSString stringWithFormat:@"%.7f", destinationPoint.latitude];
    NSString *destinationLongitude = [NSString stringWithFormat:@"%.7f", destinationPoint.longitude];
    NSString *destinationString = [NSString stringWithFormat:@"%@,%@", destinationLatitude, destinationLongitude];
    
    [mutableResult appendFormat:@"&%@=%@", ROUTE_DESTINATION_POINT, [destinationString urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *travelingMode = ROUTE_DRIVING_TRAVELING_MODE_VALUE;
    
    if (!aGoogleMapsDirectionsDataDownloadClient.routeByCarFlag) {
        
        travelingMode = ROUTE_WALKING_TRAVELING_MODE_VALUE;
        
    }
    
    [mutableResult appendFormat:@"&%@=%@", ROUTE_TRAVELING_MODE, [travelingMode urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *units = ROUTE_METRIC_UNITS_VALUE;
    
    if (!aGoogleMapsDirectionsDataDownloadClient.metricSystemFlag) {
        
        units = ROUTE_IMPERIAL_UNITS_VALUE;
        
    }
    
    [mutableResult appendFormat:@"&%@=%@", ROUTE_UNITS, [units urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *language = aGoogleMapsDirectionsDataDownloadClient.languageCode;
    
    [mutableResult appendFormat:@"&%@=%@", ROUTE_LANGUAGE, [language urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    return [NSString stringWithString:mutableResult];
    
}

/*
 * Returns the NSData containing the information associated to the given GoogleMaps Directions API parameter string. When
 * no information is stored, nil is returned
 */
- (NSData *)dataForDirectionsAPIParameterString:(NSString *)aDirectionsAPIParameterString {
    
    NSData *result = nil;
    
    NSString *cachedFile = [directionsAPICache_ cachedFilePathForKey:aDirectionsAPIParameterString];
    
    if ([cachedFile length] > 0) {
        
        result = [NSData dataWithContentsOfFile:cachedFile];
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Reverse geocoding API

/*
 * Constructs the parameter string to perform a GoogleMaps Reverse Geocoding API request with the provided information
 */
+ (NSString *)reverseGeocodingAPIParameterStringForData:(GoogleMapsReverseGeocodingDataDownloadClient *)aGoogleMapsReverseGeocodingDataDownloadClient {
    
    NSMutableString *mutableResult = [NSMutableString string];
    
    CLLocationCoordinate2D coordinate = aGoogleMapsReverseGeocodingDataDownloadClient.coordinate;
    NSString *coordinateLatitude = [NSString stringWithFormat:@"%.7f", coordinate.latitude];
    NSString *coordinateLongitude = [NSString stringWithFormat:@"%.7f", coordinate.longitude];
    NSString *coordinateString = [NSString stringWithFormat:@"%@,%@", coordinateLatitude, coordinateLongitude];
    
    [mutableResult appendFormat:@"%@=%@", REVERSE_GEOCODING_COORDINATE, [coordinateString urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *language = aGoogleMapsReverseGeocodingDataDownloadClient.languageCode;
    
    [mutableResult appendFormat:@"&%@=%@", GEOCODING_LANGUAGE, [language urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    return [NSString stringWithString:mutableResult];
    
}

/*
 * Returns the NSData containing the information associated to the given GoogleMaps Reverse Geocoding API parameter string. When
 * no information is stored, nil is returned
 */
- (NSData *)dataForReverseGeocodingAPIParameterString:(NSString *)aReverseGeocodingAPIParameterString {
    
    NSData *result = nil;
    
    NSString *cachedFile = [reverseGeocodingAPICache_ cachedFilePathForKey:aReverseGeocodingAPIParameterString];
    
    if ([cachedFile length] > 0) {
        
        result = [NSData dataWithContentsOfFile:cachedFile];
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Geocoding API

/*
 * Constructs the parameter string to perform a GoogleMaps Geocoding API request with the provided information
 */
+ (NSString *)geocodingAPIParameterStringForData:(GoogleMapsGeocodingDataDownloadClient *)aGoogleMapsGeocodingDataDownloadClient {
    
    NSMutableString *mutableResult = [NSMutableString string];
    
    NSString *addressString = aGoogleMapsGeocodingDataDownloadClient.addressString;
    
    [mutableResult appendFormat:@"%@=%@", GEOCODING_ADDRESS, [addressString urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *language = aGoogleMapsGeocodingDataDownloadClient.languageCode;
    
    [mutableResult appendFormat:@"&%@=%@", GEOCODING_LANGUAGE, [language urlEncodeUsingEncoding:NSUTF8StringEncoding]];
//    
//    [mutableResult appendFormat:@"&%@=%@", GEOCODING_REGION, [GEOCODING_REGION_VALUE urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    return [NSString stringWithString:mutableResult];
    
}

/*
 * Returns the NSData containing the information associated to the given GoogleMaps Geocoding API parameter string. When
 * no information is stored, nil is returned
 */
- (NSData *)dataForGeocodingAPIParameterString:(NSString *)aGeocodingAPIParameterString {
    
    NSData *result = nil;
    
    NSString *cachedFile = [geocodingAPICache_ cachedFilePathForKey:aGeocodingAPIParameterString];
    
    if ([cachedFile length] > 0) {
        
        result = [NSData dataWithContentsOfFile:cachedFile];
    }
    
    return result;
    
}

@end
