/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "SingletonBase.h"


//Forward declarations
@class GoogleMapsDirectionsDataDownloadClient;
@class GoogleMapsReverseGeocodingDataDownloadClient;
@class GoogleMapsGeocodingDataDownloadClient;
@class DownloadFileCache;


/**
 * Provides an interface to invoke GoogleMaps API methods. All exposed methods are static. It uses the HTTPInvoker to perform the download.
 * This class is also a singleton to manage the internal GoogleMaps API caches
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsApiInvoker : SingletonBase {

@private
    
    /**
     * Maintains the GoogleMaps Directions API cached files information
     */
    DownloadFileCache *directionsAPICache_;
    
    /**
     * Maintains the GoogleMaps Reverse Geocoding API cached files information
     */
    DownloadFileCache *reverseGeocodingAPICache_;
    
    /**
     * Maintains the GoogleMaps Geocoding API cached files information
     */
    DownloadFileCache *geocodingAPICache_;
    
}

/**
 * Invokes the directions API from an origin point to a destination point, using the fastest route
 *
 * @param aDataDownloadClient The data download client to be informed about the download evolution. It also contains the route parameters
 * @param aSensorFlag YES when a route point was obtained from a locations sensor (for example a GPS), NO otherwise
 * @return YES when download can be invoked, NO otherwise
 */
+ (BOOL)routeForDirectionClient:(GoogleMapsDirectionsDataDownloadClient *)aDataDownloadClient
             obtainedFromSensor:(BOOL)aSensorFlag;

/**
 * Invokes the reverse geocoding API for an Earth coordinate
 *
 * @param aDataDownloadClient The data download client to be informed about the download evolution. It also contains the parameters to invoke the API
 * @param aSensorFlag YES when the coordinate was obtained from a locations sensor (for example a GPS), NO otherwise
 * @return YES when download can be invoked, NO otherwise
 */
+ (BOOL)addressForReverseGeocodingClient:(GoogleMapsReverseGeocodingDataDownloadClient *)aDataDownloadClient
                      obtainedFromSensor:(BOOL)aSensorFlag;

/**
 * Invokes the geocoding API for an address string
 *
 * @param aDataDownloadClient The data download client to be informed about the download evolution. It also contains the parameters to invoke the API
 * @return YES when download can be invoked, NO otherwise
 */
+ (BOOL)addressForGeocodingClient:(GoogleMapsGeocodingDataDownloadClient *)aDataDownloadClient;

@end
