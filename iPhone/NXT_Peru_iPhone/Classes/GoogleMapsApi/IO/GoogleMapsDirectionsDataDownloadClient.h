/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "XMLParserDataDownloadClient.h"
#import <CoreLocation/CoreLocation.h>


//Forward declarations
@class GoogleMapsDirectionsResponse;


/**
 * Data download client that analyzes a GoogleMaps directions API response
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsDirectionsDataDownloadClient : XMLParserDataDownloadClient {

@private
    
    /**
     * Route origin point
     */
    CLLocationCoordinate2D originPoint_;
    
    /**
     * Route destination point
     */
    CLLocationCoordinate2D destinationPoint_;
    
    /**
     * Route language code
     */
    NSString *languageCode_;
    
    /**
     * Route by car flag. YES when route is by car, NO when route is on foot
     */
    BOOL routeByCarFlag_;
    
    /**
     * Metric system flag. YES to obtain the response in metric units, NO to obtain the response in imperial units
     */
    BOOL metricSystemFlag_;
    
    /**
     * GoogleMaps directions API result analyzed. It analyzes the XML returned from a GoogleMaps directions API call
     */
    GoogleMapsDirectionsResponse *directionsResponse_;
    
    /**
     * The analysis will be performed from cached information flag. YES when analysis will be performed from
     * cached data, NO when analysis will be performed from downloaded data
     */
    BOOL analysisFromCache_;
    
}

/**
 * Provides read-only access to the route origin point
 */
@property (nonatomic, readonly, assign) CLLocationCoordinate2D originPoint;

/**
 * Provides read-only access to the route destination point
 */
@property (nonatomic, readonly, assign) CLLocationCoordinate2D destinationPoint;

/**
 * Provides read-only access to the route language code
 */
@property (nonatomic, readonly, copy) NSString *languageCode;

/**
 * Provides read-only access to the route by car flag
 */
@property (nonatomic, readonly, assign) BOOL routeByCarFlag;

/**
 * Provides read-only access to the metric system flag
 */
@property (nonatomic, readonly, assign) BOOL metricSystemFlag;

/**
 * Provides read-only access to the GoogleMaps directions API result analyzed
 */
@property (nonatomic, readonly, retain) GoogleMapsDirectionsResponse *directionsResponse;


/**
 * Designated initializer. Initializes an GoogleMapsDirectionsDataDownloadClient instance with the provided information
 *
 * @param anOriginPoint The route start point
 * @param aDestinationPoint The route final point
 * @param aLanguage The language code to obtain the response in
 * @param routeByCarFlag YES when the response route must be by car, NO when the response route must be on foot
 * @param metricSystemFlag YES when the response must be expresed in metric units, NO for imperial units
 * @return The initialized GoogleMapsDirectionsDataDownloadClient instance
 */
- (id)initWithOriginPoint:(CLLocationCoordinate2D)anOriginPoint
         destinationPoint:(CLLocationCoordinate2D)aDestinationPoint
                 language:(NSString *)aLanguage
               routeByCar:(BOOL)routeByCarFlag
             metricSystem:(BOOL)metricSystemFlag;

@end
