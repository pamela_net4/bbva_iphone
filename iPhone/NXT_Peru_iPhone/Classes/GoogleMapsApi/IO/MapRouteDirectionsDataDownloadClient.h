/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "GoogleMapsConstants.h"
#import "GoogleMapsApiInvoker.h"
#import "GoogleMapsDirectionsDataDownloadClient.h"

@class MapRouteDirectionsDataDownloadClient;

@protocol MapRouteDirectionsDataDownloadClientDelegate

/**
 * Invoked when the GoogleMaps directions API information was downloaded and parsed successfully. The information is analyzed
 * to create the new route
 *
 * @param aRouteDirectionsDataDownloadClient The data client notifiying the event
 * @private
 */
- (void)routeInformationParsed:(MapRouteDirectionsDataDownloadClient *)aRouteDirectionsDataDownloadClient;

/**
 * The GoogleMaps directions API response was parsed correcty. The response is analyze to obtain the route information information and display it
 *
 * @param aGoogleMapsDirectionsResponse The GoogleMaps directions API response to analyze
 * @private
 */
- (void)analyzeRouteResponse:(GoogleMapsDirectionsResponse *)aGoogleMapsDirectionsResponse;

/**
 * The route parsing process finished in error. The user is notified about the situation
 *
 * @param anError The error that happend while parsing the route information. It can be nil
 */
- (void)routeParsingProcessError:(NSError *)anError;

/**
 * The route download process finished in error. The user is notified about the situation
 *
 * @param anError The error that happend while downloading the route information. It can be nil
 */
- (void)routeDownloadProcessError:(NSError *)anError;

/**
 * The route download process was cancelled
 */
- (void)routeDownloadCancelled;

@end

/**
 * Downloads the map route from GoogleMaps directions API and displays it inside the map view
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MapRouteDirectionsDataDownloadClient : GoogleMapsDirectionsDataDownloadClient {
    
@private
    
    /**
     * Delegate of class.
     */
    id<MapRouteDirectionsDataDownloadClientDelegate> delegate_;
    
}

/**
 * Designated initializer. Initializes a MapsRouteDirectionsDataDownloadClient instance providing it with the associated MapRouteViewController instance
 *
 * @param anOriginPoint The route start point
 * @param aDestinationPoint The route final point
 * @param aLanguage The language code to obtain the response in
 * @param routeByCarFlag YES when the response route must be by car, NO when the response route must be on foot
 * @param metricSystemFlag YES when the response must be expresed in metric units, NO for imperial units
 * @param delegate The delegate to receive the notifications
 * @return The initialized MapsRouteDirectionsDataDownloadClient instance
 */
- (id)initWithOriginPoint:(CLLocationCoordinate2D)anOriginPoint
         destinationPoint:(CLLocationCoordinate2D)aDestinationPoint
                 language:(NSString *)aLanguage
               routeByCar:(BOOL)routeByCarFlag
             metricSystem:(BOOL)metricSystemFlag
mapRouteDirectionsDataDownloadClientDelegate:(id<MapRouteDirectionsDataDownloadClientDelegate>)delegate;

@end
