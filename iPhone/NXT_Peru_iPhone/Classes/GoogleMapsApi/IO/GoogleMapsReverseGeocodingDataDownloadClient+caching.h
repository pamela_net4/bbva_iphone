/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsReverseGeocodingDataDownloadClient.h"


/**
 * GoogleMapsReverseGeocodingDataDownloadClient caching category. Defines selectors associated to
 * the caching management
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsReverseGeocodingDataDownloadClient(caching)

/**
 * Provides read-write access to the cache analysis flag
 */
@property (nonatomic, readwrite, assign) BOOL analysisFromCache;

@end
