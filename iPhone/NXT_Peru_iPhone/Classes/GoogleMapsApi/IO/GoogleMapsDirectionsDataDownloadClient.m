/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsDirectionsDataDownloadClient.h"
#import "GoogleMapsDirectionsDataDownloadClient+caching.h"
#import "GoogleMapsDirectionsResponse.h"
#import "XMLParserDataDownloadClient+protected.h"
#import "GoogleMapsApiInvoker+fileCaching.h"


#pragma mark -

@implementation GoogleMapsDirectionsDataDownloadClient

#pragma mark -
#pragma mark Properties

@synthesize originPoint = originPoint_;
@synthesize destinationPoint = destinationPoint_;
@synthesize languageCode = languageCode_;
@synthesize routeByCarFlag = routeByCarFlag_;
@synthesize metricSystemFlag = metricSystemFlag_;
@synthesize directionsResponse = directionsResponse_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {

    [languageCode_ release];
    languageCode_ = nil;
    
    [directionsResponse_ release];
    directionsResponse_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes a GoogleMapsDirectionsDataDownloadClient instance with default download parameters
 *
 * @return The initialized GoogleMapsDirectionsDataDownloadClient instance
 */
- (id)init {
    
    CLLocationCoordinate2D location;
    location.latitude = 0.0f;
    location.longitude = 0.0f;
    
    return [self initWithOriginPoint:location
                    destinationPoint:location
                            language:@""
                          routeByCar:YES
                        metricSystem:YES];
    
}

/*
 * Designated initializer. Initializes an GoogleMapsDirectionsDataDownloadClient instance with the provided information
 */
- (id)initWithOriginPoint:(CLLocationCoordinate2D)anOriginPoint
         destinationPoint:(CLLocationCoordinate2D)aDestinationPoint
                 language:(NSString *)aLanguage
               routeByCar:(BOOL)routeByCarFlag
             metricSystem:(BOOL)metricSystemFlag {
    
    if (self = [super init]) {
        
        originPoint_ = anOriginPoint;
        destinationPoint_ = aDestinationPoint;
        languageCode_ = [aLanguage copyWithZone:[self zone]];
        routeByCarFlag_ = routeByCarFlag;
        metricSystemFlag_ = metricSystemFlag;
        directionsResponse_ = [[GoogleMapsDirectionsResponse alloc] init];

    }
    
    return self;
    
}

#pragma mark -
#pragma mark XMLParserDataDownloadClient selectors

/**
 * Notifies the child class that the XML parsing finished correctly. The downloaded information is stored
 * inside the cache
 *
 * @param anXMLParserObject The error that happened while parsing
 * @param aDownloadedData The downloaded data that failed to be parsed as an XML document
 */
- (void)xmlParsingFinishedWithResult:(BaseXMLParserObject *)anXMLParserObject
                            fromData:(NSData *)aDownloadedData {
    
    [super xmlParsingFinishedWithResult:anXMLParserObject
                               fromData:aDownloadedData];
    
    if (!analysisFromCache_) {
        
        [GoogleMapsApiInvoker storeDirectionsAPIData:aDownloadedData
                                        associatedTo:self];

    }
    
}

#pragma mark -
#pragma mark XMLParserDataDownloadClient selectors

/*
 * Returns the BaseXMLParserObject that must analyze the downloaded data, in this case,
 * the GoogleMaps directions API XML analyzer
 *
 * @return The GoogleMaps directions API XML analyzer
 */
- (BaseXMLParserObject *)parserObject {
    
    return directionsResponse_;
    
}

@end


#pragma mark -

@implementation GoogleMapsDirectionsDataDownloadClient(caching)

#pragma mark -
#pragma mark Caching management

/*
 * Returns the cached analysis flag
 * 
 * @return The cached analysis flag
 */
- (BOOL)analysisFromCache {
    
    return analysisFromCache_;
    
}

/*
 * Sets the new cached analysis flag
 *
 * @param aValue The new cached analysis flag
 */
- (void)setAnalysisFromCache:(BOOL)aValue {
    
    analysisFromCache_ = aValue;
    
}

@end
