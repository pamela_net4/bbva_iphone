/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GoogleMapsApiInvoker.h"


//Forward declarations
@class GoogleMapsDirectionsDataDownloadClient;
@class GoogleMapsReverseGeocodingDataDownloadClient;
@class GoogleMapsGeocodingDataDownloadClient;


/**
 * GoogleMapsApiInvoker file caching category
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GoogleMapsApiInvoker(fileCaching)

/**
 * Stores the provided data associated to the GoogleMaps Directions API download parameters. When previous data is associated
 * to the download parameters, it is replaced by the new one
 *
 * @param aDownloadedData The data downloaded from the GoogleMaps Directions API
 * @param aGoogleMapsDirectionsDataDownloadClient The object containing the GoogleMaps Directions API download parameters
 * @protected
 */
+ (void)storeDirectionsAPIData:(NSData *)aDownloadedData
                  associatedTo:(GoogleMapsDirectionsDataDownloadClient *)aGoogleMapsDirectionsDataDownloadClient;

/**
 * Stores the provided data associated to the GoogleMaps Reverse Geocoding API download parameters. When previous data is associated
 * to the download parameters, it is replaced by the new one
 *
 * @param aDownloadedData The data downloaded from the GoogleMaps Reverse Geocoding API
 * @param aGoogleMapsReverseGeocodingDataDownloadClient The object containing the GoogleMaps Reverse Geocoding API download parameters
 * @protected
 */
+ (void)storeReverseGeocodingAPIData:(NSData *)aDownloadedData
                        associatedTo:(GoogleMapsReverseGeocodingDataDownloadClient *)aGoogleMapsReverseGeocodingDataDownloadClient;

/**
 * Stores the provided data associated to the GoogleMaps Geocoding API download parameters. When previous data is associated
 * to the download parameters, it is replaced by the new one
 *
 * @param aDownloadedData The data downloaded from the GoogleMaps Geocoding API
 * @param aGoogleMapsGeocodingDataDownloadClient The object containing the GoogleMaps Geocoding API download parameters
 * @protected
 */
+ (void)storeGeocodingAPIData:(NSData *)aDownloadedData
                 associatedTo:(GoogleMapsGeocodingDataDownloadClient *)aGoogleMapsGeocodingDataDownloadClient;

@end
