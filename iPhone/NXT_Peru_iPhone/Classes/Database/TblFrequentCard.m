//
//  TblFrequentCard.m
//  NXT_Peru_iPhone
//
//  Created by MDP on 6/10/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "TblFrequentCard.h"
#import "FrequentCard.h"
#import "FMResultSet.h"
#import "FMDBUtil.h"
#import "FMDatabaseQueue.h"
#import "Tools.h"

@implementation TblFrequentCard

+ (TblFrequentCard *)fnGetInstanceFrequentCard {
    
    static TblFrequentCard * objTblFrequentCard = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        objTblFrequentCard = [[TblFrequentCard alloc] init];
    });
    return objTblFrequentCard;
}

-(NSMutableArray*)fnGetListFrequentCard{
    
    NSString *consultaString = [NSString stringWithFormat:@"select * from frequent_cards order by code desc"];
    FMResultSet *resultadoResultSet =  [[FMDBUtil fnDameInstanciaFMDBUtil] fnDameConsulta:consultaString];
    
    NSMutableArray *resultadoArray = [[NSMutableArray alloc] init];
    
    while ([resultadoResultSet next])
    {
        FrequentCard *objDocumento  = [[FrequentCard alloc] initWithDatos:resultadoResultSet];
        [resultadoArray addObject:objDocumento];
    }
    
    [resultadoResultSet close];
    [[FMDBUtil fnDameInstanciaFMDBUtil] fnCerrarUtilConexion];
    
    return resultadoArray;
    
}



-(void)fnInsertFrequentCard:(FrequentCard *)entity{
    
    if([entity isEqual:[NSNull null]])
    {
        return;
    }
    
    int32_t code = [[FMDBUtil fnDameInstanciaFMDBUtil] fnDameMaxIdentity:@"frequent_cards" identity:@"code"];
    entity.code = [[NSNumber alloc] initWithInt: ( code + 1 )];
    entity.flag_last_user = [[NSNumber alloc] initWithInt:1];
    
    if([self fnIsFrequentCardValid:entity.alias]){
        
        [[FMDBUtil fnDameInstanciaFMDBUtil] fnEjecutaActualizacion:@"update frequent_cards set flag_last_user=0 where code>0"];
        
        NSString * query = [NSString stringWithFormat:
                           @"insert into frequent_cards(code, alias, value, flag_last_user,masked) values(%i,'%@','%@',%i,'%@')",
                           [entity.code intValue],
                           entity.alias,
                           entity.value,
                           [entity.flag_last_user intValue],
                           entity.masked];
        
        [[FMDBUtil fnDameInstanciaFMDBUtil] fnEjecutaActualizacion:query];
        
    }
    else{
        
        [[FMDBUtil fnDameInstanciaFMDBUtil] fnEjecutaActualizacion:@"update frequent_cards set flag_last_user=0 where code>0"];
        
        NSString * query = [NSString stringWithFormat:
                           @"update frequent_cards set alias='%@',value='%@',masked='%@',flag_last_user=1 where alias like '%@'",
                           entity.alias,
                           entity.value,
                           entity.masked,
                           entity.alias
                           ];
        
        [[FMDBUtil fnDameInstanciaFMDBUtil] fnEjecutaActualizacion:query];

    }
  
    
}

-(BOOL) fnIsFrequentCardValid:(NSString *)alias{
    
    NSString *consultaString = [NSString stringWithFormat:@"select * from frequent_cards where alias like'%@'",
                                alias
                                ];
    FMResultSet *resultadoResultSet =  [[FMDBUtil fnDameInstanciaFMDBUtil] fnDameConsulta:consultaString];
    
    BOOL isValid = YES;
    if([resultadoResultSet next]){
        
        isValid = NO;
        
    }
    
    [resultadoResultSet close];
    [[FMDBUtil fnDameInstanciaFMDBUtil] fnCerrarUtilConexion];
    
    return isValid;
}





-(void)fnUpdateLastFrequentCard:(FrequentCard *)entity{
    
    if([entity isEqual:[NSNull null]])
    {
        return;
    }
    
    [[FMDBUtil fnDameInstanciaFMDBUtil] fnEjecutaActualizacion:
                        @"update frequent_cards set flag_last_user=0 where code>0"];
    
    
    NSString * query = [NSString stringWithFormat:
                       @"update frequent_cards set flag_last_user=1 where code=%i",
                       [entity.code intValue] ];
    
    [[FMDBUtil fnDameInstanciaFMDBUtil] fnEjecutaActualizacion:query];

    
}


-(void)fnRemoveFrequentCard:(FrequentCard *) entity{
    
    NSString * codeString = [NSString stringWithFormat:@"%i",[entity.code intValue]];
    [[FMDBUtil fnDameInstanciaFMDBUtil] fnRemoverEntidad:@"frequent_cards"
                                  conNombreIdentificador:@"code"
                                          eIdentificador:codeString];
    
}

@end
