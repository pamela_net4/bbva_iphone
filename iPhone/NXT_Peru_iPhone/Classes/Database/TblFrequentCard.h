//
//  TblFrequentCard.h
//  NXT_Peru_iPhone
//
//  Created by MDP on 6/10/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FrequentCard.h"

@interface TblFrequentCard : NSObject

+ (TblFrequentCard *)fnGetInstanceFrequentCard;

-(NSMutableArray*)fnGetListFrequentCard;

-(void)fnInsertFrequentCard:(FrequentCard *)entity;
-(void)fnUpdateLastFrequentCard:(FrequentCard *)entity;
-(void)fnRemoveFrequentCard:(FrequentCard *) entity;

@end
