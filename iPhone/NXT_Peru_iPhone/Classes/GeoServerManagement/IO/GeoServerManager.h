/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "SingletonBase.h"
#import "DownloadListener.h"


#if defined(DEBUG)
/**
 * Defines the Movilok environment. Uncomment this line to use Movilok server to obtain information form
 */
//#define MOVILOK_GEOSERVER
#endif

/**
 * Defines the geoserver URL
 */
#if defined(MOVILOK_GEOSERVER)
#define GEOSERVER_URL                                          @"http://movilok.net/csapi/"
#else
#define GEOSERVER_URL                                          @"http://mat.gobernalianet.org/csapi/"       //CSAPI Amazon hosted production server
#endif

/**
 * Defines the geoserver URL
 */
#if defined(MOVILOK_GEOSERVER)
#define GET_CLOSEST_POIS_OPERATION                             @"CsapiPoiService.asmx/GetClosestPois"
#else
#define GET_CLOSEST_POIS_OPERATION                             @"http://geoserver.gobernalianet.org/geosearch.asmx/GetClosestPois"
#endif

/**
 * Defines the application ID
 */
#if defined(MOVILOK_GEOSERVER)
//#define APPLICATION_ID                                         @"nxt"
#define APPLICATION_ID                                         @"NXT"
#else
//#define APPLICATION_ID                                         @"nxt"
#define APPLICATION_ID                                         @"nxt_pe"
#endif

/**
 * Defines the application KEY
 */
#if defined(MOVILOK_GEOSERVER)
#define APPLICATION_KEY                                        @"nxt"
#else
#define APPLICATION_KEY                                        @"ab099724-6819-4176-9dda-2349c4a3359f"
#endif

/**
 * Defines the ATM type
 */
#if defined(MOVILOK_GEOSERVER)
#define ATM_TYPE											   @"banking.atm.cajero_bbva"
#else
#define ATM_TYPE											   @"banking.atm.atm_pe_bbva"
#endif

/**
 * Defines the ATM banco credito type
 */
#if defined(MOVILOK_GEOSERVER)
#define ATM_BC_TYPE											   @"banking.atm.cajero_bbva"
#else
#define ATM_BC_TYPE											   @"banking.atm.atm_pe_bc"
#endif

/**
 * Defines the ATM ae type
 */
#if defined(MOVILOK_GEOSERVER)
#define ATM_INTERBANK_TYPE										@"banking.atm.cajero_bbva"
#else
#define ATM_INTERBANK_TYPE										@"banking.atm.atm_pe_interbank"
#endif

/**
 * Defines the ATM scotia Bank type
 */
#if defined(MOVILOK_GEOSERVER)
#define ATM_SCOTIABANK_TYPE										@"banking.atm.cajero_bbva"
#else
#define ATM_SCOTIABANK_TYPE										@"banking.atm.atm_pe_scotiabank"
#endif

/**
* Defines the agent kasnet type
*/
#if defined(MOVILOK_GEOSERVER)
#define AGENT_KASNET_TYPE                                         @"banking.atm.cajero_bbva"
#else
#define AGENT_KASNET_TYPE                                         @"banking.atm.atm_pe_kasnet"
#endif

/**
* Defines the agent aeplus type
*/
#if defined(MOVILOK_GEOSERVER)
#define AGENT_AEPLUS_TYPE                                         @"banking.atm.cajero_bbva"
#else
#define AGENT_AEPLUS_TYPE                                         @"banking.atm.atm_pe_aeplus"
#endif

/**
* Defines the agent multifacil type
*/
#if defined(MOVILOK_GEOSERVER)
#define AGENT_MULTIFACIL_TYPE										@"banking.atm.cajero_bbva"
#else
#define AGENT_MULTIFACIL_TYPE										@"banking.atm.atm_pe_multifacil"
#endif

/**
 * Defines the express agent type
 */
#if defined(MOVILOK_GEOSERVER)
#define EXPRESS_AGENT_TYPE                                     @"banking.expressAgent.express_agent_bbva"
#else
#define EXPRESS_AGENT_TYPE                                     @"banking.atm.atm_pe_ae"
#endif

/**
 * Defines the BRANCH type
 */
#if defined(MOVILOK_GEOSERVER)
#define BRANCH_TYPE											   @"banking.branch.oficina_bbva"
#else
#define BRANCH_TYPE											   @"banking.branch.branch_pe_bbva"
#endif

/**
 * Defines the interface version
 */
#if defined(MOVILOK_GEOSERVER)
#define INTERFACE_VERSION                                      @"1.1"
#else
#define INTERFACE_VERSION                                      @"1.1"
#endif

/**
 * Defines the country
 */
#if defined(MOVILOK_GEOSERVER)
#define COUNTRY												   @"pe"
#else
#define COUNTRY                                                @"pe"
#endif

/**
 * Defines the application ID
 */
#if defined(MOVILOK_GEOSERVER)
#define PLATFORM											   @"iphone"
#else
#define PLATFORM                                               @"iphone"
#endif

/**
 * Defines the address like URI
 */
#if defined(MOVILOK_GEOSERVER)
#define GET_ADDRESS_LIKE_OPERATION                              @"CsapiGeoService.asmx/GetAddressLike"
#else
#define GET_ADDRESS_LIKE_OPERATION                              @"http://csapi.gobernalianet.org/CsapiGeoService.asmx/GetAddressLike"
#endif

/**
 * Defines the address for coordinates URI
 */
#if defined(MOVILOK_GEOSERVER)
#define GET_ADDRESS_FOR_COORDINATES_URI                         @"CsapiGeoService.asmx/GetClosestPlacename"
#else
#define GET_ADDRESS_FOR_COORDINATES_URI                         @"http://csapi.gobernalianet.org/CsapiGeoService.asmx/GetClosestPlacename"
#endif

/**
 * Defines the network of the ATMs and branch
 */
#if defined(MOVILOK_GEOSERVER)
#define ATM_NETWORK_BBVA                                        @"cajero_bbva"
#define ATM_NETWORK_SERVIRED                                    @"cajero_servired"
#define ATM_NETWORK_4B                                          @"cajero_4b"
#define ATM_NETWORK_E6000                                       @"cajero_r6000"
#define BRANCH_NETWORK_BBVA                                     @"oficina_bbva"
#else
#define ATM_NETWORK_BBVA                                        @"atm_es_bbva"
#define ATM_NETWORK_SERVIRED                                    @"atm_es_servired"
#define ATM_NETWORK_4B                                          @"atm_es_4b"
#define ATM_NETWORK_E6000                                       @"atm_es_e6000"
#define BRANCH_NETWORK_BBVA                                     @"branch_es_bbva"
#endif

/**
 * Defines the readable values of the ATMs and branches
 */
#if defined(MOVILOK_GEOSERVER)
#define ATM_NETWORK_BBVA_TEXT                                   @"Cajero BBVA"
#define ATM_NETWORK_SERVIRED_TEXT                               @"Cajero Servired"
#define ATM_NETWORK_4B_TEXT                                     @"Cajero 4B"
#define ATM_NETWORK_E6000_TEXT                                  @"Cajero Red 6000"
#define BRANCH_NETWORK_BBVA_TEXT                                @"Oficina BBVA"
#else
#define ATM_NETWORK_BBVA_TEXT                                   @"Cajero BBVA"
#define ATM_NETWORK_SERVIRED_TEXT                               @"Cajero Servired"
#define ATM_NETWORK_4B_TEXT                                     @"Cajero 4B"
#define ATM_NETWORK_E6000_TEXT                                  @"Cajero Red 6000"
#define BRANCH_NETWORK_BBVA_TEXT                                @"Oficina BBVA"
#endif

//Forward declarations
@class OperationResult;
@class ATMList;
@class BranchesList;

/**
 * Singelton to manage all interaction with GeoServer server
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GeoServerManager : SingletonBase <DownloadListener> {

@private
    
    /**
     * Pending ATM list requests dictionary
     */
    NSMutableDictionary *atmListRequests_;
    
    /**
     * Pending express agent list requests dictionary
     */
    NSMutableDictionary *expressAgentListRequests_;
    
    /**
     * Pending branches list requests dictionary
     */
    NSMutableDictionary *branchesListRequests_;
    
    /**
     * User digested ID
     */
    NSString *uuid_;
	
}

/**
 * Obtains the list of ATMs nearby a latitude and longitude asynchronously
 *
 * @param aLatitude Latitude the query is about
 * @param aLongitude Longitude the query is about
 * @param aDownloadListener The instance to be notified on any download event
 * @param anATMList The ATM list to store the final result
 * @param currentResult the operation Result where we receive the pois
 * @param positionCharge number of pois
 * @return The file name where download will be stored, used as download identificaiton, or nil if there is an error or the manager is not configured
 */
+ (NSString *)atmListForLatitute:(float)aLatitude andLongitude:(float)aLongitude
                     forListener:(id<DownloadListener>)aDownloadListener 
                      withOpertaionResult:(OperationResult *)currentResult positionNewCharge:(NSInteger)positionCharge 
                      storedInto:(ATMList *)anATMList;

/**
 * Obtains the list of express agents nearby a latitude and longitude asynchronously
 *
 * @param aLatitude Latitude the query is about
 * @param aLongitude Longitude the query is about
 * @param aDownloadListener The instance to be notified on any download event
 * @param anExpressAgentList The express agent list to store the final result
 * @param currentResult the operation Result where we receive the pois
 * @param positionCharge number of pois
 * @return The file name where download will be stored, used as download identificaiton, or nil if there is an error or the manager is not configured
 */
+ (NSString *)expressAgentListForLatitute:(float)aLatitude andLongitude:(float)aLongitude
                              forListener:(id<DownloadListener>)aDownloadListener 
                                withOpertaionResult:(OperationResult *)currentResult positionNewCharge:(NSInteger)positionCharge 
                               storedInto:(ATMList *)anExpressAgentList;

/**
 * Obtains the list of banking centers nearby a latitude and longitude asynchronously
 *
 * @param aLatitude Latitude the query is about
 * @param aLongitude Longitude the query is about
 * @param aDownloadListener The instance to be notified on any download event
 * @param aBranchList The branch list to store the final result
 * @param currentResult the operation Result where we receive the pois
 * @param positionCharge number of pois
 * @return The file name where download will be stored, used as download identificaiton, or nil if there is an error or the manager is not configured
 */
+ (NSString *)branchListForLatitute:(float)aLatitude andLongitude:(float)aLongitude
                               forListener:(id<DownloadListener>)aDownloadListener 
                                withOpertaionResult:(OperationResult *)currentResult positionNewCharge:(NSInteger)positionCharge 
                                storedInto:(BranchesList *)aBranchList;

/**
 * Obtains the address for a latitude and longitude asynchronously
 *
 * @param aLatitude Latitude the query is about
 * @param aLongitude Longitude the query is about
 * @param aDownloadListener The instance to be notified on any download event
 * @param anXMLParserDelegate The NSXMLParser delegate that will analyze the XML document
 * @return The file name where download will be stored, used as download identificaiton, or nil if there is an error or the manager is not configured
 */
+ (NSString *)addressForLatitute:(float)aLatitude longitude:(float)aLongitude
                     forListener:(id<DownloadListener>)aDownloadListener 
                      analyzedBy:(id)anXMLParserDelegate;


/**
 * Cancels the given BBVAContigo download process
 *
 *
 * @param aDownloadId The download identification
 */
+ (void)cancelDownload:(NSString *)aDownloadId;

/**
 * Cancels all BBVAContigo downloads
 */
+ (void)cancelAllDownloads;


/**
 * Obtains all addresses similar to a given string asynchronously
 *
 * @param aString String that addresses must match
 * @param aDownloadListener The instance to be notified on any download event
 * @param anXMLParserDelegate The NSXMLParser delegate that will analyze the XML document
 * @return The file name where download will be stored, used as download identificaiton, or nil if there is an error or the manager is not configured
 */
+ (NSString *)streetNamesLikeString:(NSString *)aString forListener:(id<DownloadListener>)aDownloadListener
                         analyzedBy:(id)anXMLParserDelegate;

@end
