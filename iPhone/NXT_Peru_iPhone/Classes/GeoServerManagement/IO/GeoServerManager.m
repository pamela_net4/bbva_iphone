/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GeoServerManager.h"
#import "OperationResult.h"
#import "ExecutionResult.h"
#import "ATMList.h"
#import "BranchesList.h"
#import "GeocodedAddressList.h"
#import "POIList.h"
#import "OperationResult.h"
#import "HTTPInvoker.h"
#import "Tools.h"
#import "NSString+URLAndHTMLUtils.h"


/**
 * Defines the geoserver getPois uri
 */
#define GEOSERVER_GET_POIS_URI                                              [NSString stringWithFormat:GET_CLOSEST_POIS_OPERATION]

/**
 * Defines the geoserver get address like uri
 */
#define GEOSERVER_GET_ADDRESS_LIKE_URI                                      [NSString stringWithFormat:GET_ADDRESS_LIKE_OPERATION]

/**
 * Defines the geoserver get address for coordinates uri
 */
#define GEOSERVER_GET_ADDRESS_FOR_COORDINATES_URI                           [NSString stringWithFormat:GET_ADDRESS_FOR_COORDINATES_URI]

#pragma mark -

/**
 * Class to contain the ATM list pending requests information
 */
@interface ATMListPendingRequestInfo : NSObject {
    
@private
    
    /**
     * ATM list to store the result
     */
    ATMList *atmList_;
    
    /**
     * Original operation result
     */
    OperationResult *originalOperationResult_;
    
    /**
     * The operation result analyzing the complete response
     */
    OperationResult *operationResult_;
    
    /**
     * The POI list to analyze the XML information
     */
    POIList *poiList_;
    
    /**
     * Observer that must be notified
     */
    id<DownloadListener> downloadListener_;
    
}

/**
 * Provides read-only access to the ATM list to store the result
 */
@property (nonatomic, readonly, retain) ATMList *atmList;

/**
 * Provides read-only access to the original operation result
 */
@property (nonatomic, readonly, retain) OperationResult *originalOperationResult;

/**
 * Provides read-only access to the operation result analyzing the complete response
 */
@property (nonatomic, readonly, retain) OperationResult *operationResult;

/**
 * Provides read-only access to the POI list to analyze the XML information
 */
@property (nonatomic, readonly, retain) POIList *poiList;

/**
 * Provides read-only access to the observer that must be notified
 */
@property (nonatomic, readonly, assign) id<DownloadListener> downloadListener;


/**
 * Designated initializer. Initializes an ATMListPendingRequestInfo instance with the given information
 *
 * @param anATMList The ATM list to store
 * @param anOriginalOperationResult the original operation result
 * @param anOperationResult The operation result analyzing the complete response
 * @param aPOIList The POI list to analyze the XML
 * @param aDownloadListener The download listener to be notified
 * @return The intialized ATMListPendingRequestInfo instance
 */
- (id)initWithATMList:(ATMList *)anATMList
originalOperationResult:(OperationResult *)anOriginalOperationResult
      operationResult:(OperationResult *)anOperationResult
              poiList:(POIList *)aPOIList
     downloadListener:(id<DownloadListener>)aDownloadListener;

@end


#pragma mark -

/**
 * Class to contain the express agent list pending requests information
 */
@interface ExpressAgentListPendingRequestInfo : NSObject {
    
@private
    
    /**
     * Express agent list to store the result
     */
    ATMList *expressAgentList_;
    
    /**
     * Original operation result
     */
    OperationResult *originalOperationResult_;
    
    /**
     * The operation result analyzing the complete response
     */
    OperationResult *operationResult_;
    
    /**
     * The POI list to analyze the XML information
     */
    POIList *poiList_;
    
    /**
     * Observer that must be notified
     */
    id<DownloadListener> downloadListener_;
    
}

/**
 * Provides read-only access to the express agent list to store the result
 */
@property (nonatomic, readonly, retain) ATMList *expressAgentList;

/**
 * Provides read-only access to the original operation result
 */
@property (nonatomic, readonly, retain) OperationResult *originalOperationResult;

/**
 * Provides read-only access to the operation result analyzing the complete response
 */
@property (nonatomic, readonly, retain) OperationResult *operationResult;

/**
 * Provides read-only access to the POI list to analyze the XML information
 */
@property (nonatomic, readonly, retain) POIList *poiList;

/**
 * Provides read-only access to the observer that must be notified
 */
@property (nonatomic, readonly, assign) id<DownloadListener> downloadListener;


/**
 * Designated initializer. Initializes an ExpressAgentListPendingRequestInfo instance with the given information
 *
 * @param anExpressAgentList The expressAgent list to store
 * @param anOriginalOperationResult The original operation result
 * @param anOperationResult The operation result analyzing the complete response
 * @param aPOIList The POI list to analyze the XML
 * @param aDownloadListener The download listener to be notified
 * @return The intialized ExpressAgentListPendingRequestInfo instance
 */
- (id)initWithExpressAgentList:(ATMList *)anExpressAgentList
       originalOperationResult:(OperationResult *)anOriginalOperationResult
               operationResult:(OperationResult *)anOperationResult
                       poiList:(POIList *)aPOIList
              downloadListener:(id<DownloadListener>)aDownloadListener;

@end


#pragma mark -
/**
 * Class to contain the branch list pending requests information
 */
@interface BranchListPendingRequestInfo : NSObject {
    
@private
    
    /**
     * Branch list to store the result
     */
    BranchesList *branchList_;
    
    /**
     * Original operation result
     */
    OperationResult *originalOperationResult_;
    
    /**
     * The operation result analyzing the complete response
     */
    OperationResult *operationResult_;
    
    /**
     * The POI list to analyze the XML information
     */
    POIList *poiList_;
    
    /**
     * Observer that must be notified
     */
    id<DownloadListener> downloadListener_;
    
}

/**
 * Provides read-only access to the branch list to store the result
 */
@property (nonatomic, readonly, retain) BranchesList *branchList;

/**
 * Provides read-only access to the original operation result
 */
@property (nonatomic, readonly, retain) OperationResult *originalOperationResult;

/**
 * Provides read-only access to the operation result analyzing the complete response
 */
@property (nonatomic, readonly, retain) OperationResult *operationResult;

/**
 * Provides read-only access to the POI list to analyze the XML information
 */
@property (nonatomic, readonly, retain) POIList *poiList;

/**
 * Provides read-only access to the observer that must be notified
 */
@property (nonatomic, readonly, assign) id<DownloadListener> downloadListener;


/**
 * Designated initializer. Initializes an BranchListPendingRequestInfo instance with the given information
 *
 * @param aBranchList The branch list to store
 * @param anOriginalOperationResult The original operation result
 * @param anOperationResult The operation result analyzing the complete response
 * @param aPOIList The POI list to analyze the XML
 * @param aDownloadListener The download listener to be notified
 * @return The intialized BranchListPendingRequestInfo instance
 */
- (id)initWithBranchList:(BranchesList *)aBranchList
 originalOperationResult:(OperationResult *)anOriginalOperationResult
         operationResult:(OperationResult *)anOperationResult
                 poiList:(POIList *)aPOIList
        downloadListener:(id<DownloadListener>)aDownloadListener;

@end


#pragma mark -

/**
 * GeoServerManager private category
 */
@interface GeoServerManager(private)

/**
 * Returns the GeoServerManager singleton only instance
 *
 * @return The GeoServerManager singleton only instance
 */
+ (GeoServerManager *)getInstance;

/**
 * Generates a new parameter string for the address like list request list
 *
 * @param aParameterString Original parameter string
 * @return New parameter string
 */
+ (NSString *)completeParametersForAddressLikeListRequest:(NSString *)aParameterString;

/**
 * Generates a new parameter string containig the platform and version parameters
 *
 * @param aParameterString Original parameter string
 * @param anInterfaceVersion Interface version used to complete the parameters
 * @return New parameter string
 */
+ (NSString *)completeParameters:(NSString *)aParameterString withInterfaceVersion:(NSString *)anInterfaceVersion;

/**
 * Generates a new parameter string for the ATM list request list
 *
 * @param aParameterString Original parameter string
 * @return New parameter string
 */
+ (NSString *)completeParametersForATMListRequest:(NSString *)aParameterString;

/**
 * Generates a new parameter string for the express agent list request list
 *
 * @param aParameterString Original parameter string
 * @return New parameter string
 */
+ (NSString *)completeParametersForExpressAgentListRequest:(NSString *)aParameterString;

/**
 * Generates a new parameter string for the branch list request list
 *
 * @param aParameterString Original parameter string
 * @return New parameter string
 */
+ (NSString *)completeParametersForBranchListRequest:(NSString *)aParameterString;

/**
 * Generates a new parameter string for the address for coordinates list request list
 *
 * @param aParameterString Original parameter string
 * @return New parameter string
 */
+ (NSString *)completeParametersForAddressForCoordinatesListRequest:(NSString *)aParameterString;

/**
 * Fills the ATM list with the POI list information
 *
 * @param anATMList The ATM list to fil
 * @param aPOIList The POI list containing the information to fill with
 */
- (void)fillATMList:(ATMList *)anATMList
        withPOIList:(POIList *)aPOIList;

/**
 * Fills the express agent list with the POI list information
 *
 * @param anExpressAgentList The express agent list to fil
 * @param aPOIList The POI list containing the information to fill with
 */
- (void)fillExpressAgentList:(ATMList *)anExpressAgentList
                 withPOIList:(POIList *)aPOIList;

/**
 * Fills the branches list with the POI list information
 *
 * @param aBranchList The branches list to fil
 * @param aPOIList The POI list containing the information to fill with
 */
- (void)fillBranchList:(BranchesList *)aBranchList
           withPOIList:(POIList *)aPOIList;

@end


#pragma mark -

@implementation ATMListPendingRequestInfo

#pragma mark -
#pragma mark Properties

@synthesize atmList = atmList_;
@synthesize originalOperationResult = originalOperationResult_;
@synthesize operationResult = operationResult_;
@synthesize poiList = poiList_;
@synthesize downloadListener = downloadListener_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [atmList_ release];
    atmList_ = nil;
    
    [originalOperationResult_ release];
    originalOperationResult_ = nil;
    
    [operationResult_ release];
    operationResult_ = nil;
    
    [poiList_ release];
    poiList_ = nil;
    
    downloadListener_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes an empty ATMListPendingRequestInfo
 *
 * @return The empty ATMListPendingRequestInfo instance
 */
- (id)init {
    
    return [self initWithATMList:nil
         originalOperationResult:nil
                 operationResult:nil
                         poiList:nil
                downloadListener:nil];
    
}

/*
 * Designated initializer. Initializes an ATMListPendingRequestInfo instance with the given information
 */
- (id)initWithATMList:(ATMList *)anATMList
originalOperationResult:(OperationResult *)anOriginalOperationResult
      operationResult:(OperationResult *)anOperationResult
              poiList:(POIList *)aPOIList
     downloadListener:(id<DownloadListener>)aDownloadListener {
    
    if (self = [super init]) {
        
        atmList_ = [anATMList retain];
        originalOperationResult_ = [anOriginalOperationResult retain];
        operationResult_ = [anOperationResult retain];
        poiList_ = [aPOIList retain];
        downloadListener_ = aDownloadListener;
        
    }
    
    return self;
    
}

@end


#pragma mark -

@implementation ExpressAgentListPendingRequestInfo

#pragma mark -
#pragma mark Properties

@synthesize expressAgentList = expressAgentList_;
@synthesize originalOperationResult = originalOperationResult_;
@synthesize operationResult = operationResult_;
@synthesize poiList = poiList_;
@synthesize downloadListener = downloadListener_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [expressAgentList_ release];
    expressAgentList_ = nil;
    
    [originalOperationResult_ release];
    originalOperationResult_ = nil;
    
    [operationResult_ release];
    operationResult_ = nil;
    
    [poiList_ release];
    poiList_ = nil;
    
    downloadListener_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes an empty ExpressAgentListPendingRequestInfo
 *
 * @return The empty ExpressAgentListPendingRequestInfo instance
 */
- (id)init {
    
    return [self initWithExpressAgentList:nil
                  originalOperationResult:nil
                          operationResult:nil
                                  poiList:nil
                         downloadListener:nil];
    
}

/*
 * Designated initializer. Initializes an ExpressAgentListPendingRequestInfo instance with the given information
 */
- (id)initWithExpressAgentList:(ATMList *)anExpressAgentList
       originalOperationResult:(OperationResult *)anOriginalOperationResult
               operationResult:(OperationResult *)anOperationResult
                       poiList:(POIList *)aPOIList
              downloadListener:(id<DownloadListener>)aDownloadListener {
    
    if (self = [super init]) {
        
        expressAgentList_ = [anExpressAgentList retain];
        originalOperationResult_ = [anOriginalOperationResult retain];
        operationResult_ = [anOperationResult retain];
        poiList_ = [aPOIList retain];
        downloadListener_ = aDownloadListener;
        
    }
    
    return self;
    
}

@end


#pragma mark -

@implementation BranchListPendingRequestInfo

#pragma mark -
#pragma mark Properties

@synthesize branchList = branchList_;
@synthesize originalOperationResult = originalOperationResult_;
@synthesize operationResult = operationResult_;
@synthesize poiList = poiList_;
@synthesize downloadListener = downloadListener_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [branchList_ release];
    branchList_ = nil;
    
    [originalOperationResult_ release];
    originalOperationResult_ = nil;
    
    [operationResult_ release];
    operationResult_ = nil;
    
    [poiList_ release];
    poiList_ = nil;
    
    downloadListener_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes an empty BranchListPendingRequestInfo
 *
 * @return The empty BranchListPendingRequestInfo instance
 */
- (id)init {
    
    return [self initWithBranchList:nil
            originalOperationResult:nil
                    operationResult:nil
                            poiList:nil
                   downloadListener:nil];
    
}

/*
 * Designated initializer. Initializes an BranchListPendingRequestInfo instance with the given information
 */
- (id)initWithBranchList:(BranchesList *)aBranchList
 originalOperationResult:(OperationResult *)anOriginalOperationResult
         operationResult:(OperationResult *)anOperationResult
                 poiList:(POIList *)aPOIList
        downloadListener:(id<DownloadListener>)aDownloadListener {
    
    if (self = [super init]) {
        
        branchList_ = [aBranchList retain];
        originalOperationResult_ = [anOriginalOperationResult retain];
        operationResult_ = [anOperationResult retain];
        poiList_ = [aPOIList retain];
        downloadListener_ = aDownloadListener;
        
    }
    
    return self;
    
}

@end


#pragma mark -

@implementation GeoServerManager

#pragma mark -
#pragma mark Global elements

/**
 * GeoServerManager only instance
 */
static GeoServerManager* geoServerManagerInstance_ = nil;

#pragma mark -
#pragma mark Singleton selectors

/**
 * The allocation returns the singleton only instance
 *
 * @param zone received zone to be assigned
 * @return The empty geoServerManager instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([GeoServerManager class]) {
        
        if (geoServerManagerInstance_ == nil) {
            
            geoServerManagerInstance_ = [super allocWithZone:zone];
            return geoServerManagerInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the GeoServerManager singleton only instance
 */
+ (GeoServerManager *)getInstance {
    
    if (geoServerManagerInstance_ == nil) {
        
        @synchronized([GeoServerManager class]) {
            
            if (geoServerManagerInstance_ == nil) {
                
                geoServerManagerInstance_ = [[GeoServerManager alloc] init];
                
            }
            
        }
        
    }
    
    return geoServerManagerInstance_;
    
}

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [atmListRequests_ release];
    atmListRequests_ = nil;
    
    [expressAgentListRequests_ release];
    expressAgentListRequests_ = nil;
    
    [branchesListRequests_ release];
    branchesListRequests_ = nil;
    
    [uuid_ release];
    uuid_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated intializer. Initializes a GeoServerManager instance
 *
 * @return The initialized GeoServerManager instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        atmListRequests_ = [[NSMutableDictionary alloc] init];
        expressAgentListRequests_ = [[NSMutableDictionary alloc] init];
        branchesListRequests_ = [[NSMutableDictionary alloc] init];
        uuid_ = [[Tools getUID] retain];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Nearby ATMs, express agents and banking centers management

/*
 * Obtains the list of ATMs nearby a latitude and longitude asynchronously
 */
+ (NSString *)atmListForLatitute:(float)aLatitude andLongitude:(float)aLongitude
                     forListener:(id<DownloadListener>)aDownloadListener
             withOpertaionResult:(OperationResult *)currentResult positionNewCharge:(NSInteger)positionCharge
                      storedInto:(ATMList *)anATMList {
    
    NSString *result = nil;
    GeoServerManager *manager = [GeoServerManager getInstance];
    
    if ((aLatitude >= -90.0f) && (aLatitude <= 90.0f) && (aLongitude >= -180.0f) && (aLongitude <= 180.0f)) {
        
        NSString *latitudeString = [[NSString stringWithFormat:@"%3.5f", aLatitude] urlEncodeUsingEncoding:NSUTF8StringEncoding];
        NSString *longitudeString = [[NSString stringWithFormat:@"%3.5f", aLongitude] urlEncodeUsingEncoding:NSUTF8StringEncoding];
        NSString *bodyString = [NSString stringWithFormat:@"latitude=%@&longitude=%@", latitudeString, longitudeString];
        bodyString = [bodyString stringByAppendingString:[NSString stringWithFormat:@"&radius=&limit=&startAt=%d",positionCharge]];
        bodyString = [GeoServerManager completeParametersForATMListRequest:bodyString ];
        NSData *bodyData = [NSData dataWithBytes:[bodyString UTF8String]
                                          length:strlen([bodyString UTF8String])];
        
        POIList *poiList = [[[POIList alloc] init] autorelease];
        OperationResult *operationResult = [[[OperationResult alloc] initWithDelegate:poiList] autorelease];;
        
        NSString *uri = GEOSERVER_GET_POIS_URI;
        
        result = [[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri
                                                                          withBody:bodyData
                                                              andXMLParserDelegate:operationResult
                                                                       forListener:manager];
        
        if ([result length] > 0) {
            
            ATMListPendingRequestInfo *pendingInfo = [[[ATMListPendingRequestInfo alloc] initWithATMList:anATMList
                                                                                 originalOperationResult:currentResult
                                                                                         operationResult:operationResult
                                                                                                 poiList:poiList
                                                                                        downloadListener:aDownloadListener] autorelease];
            [manager->atmListRequests_ setObject:pendingInfo forKey:result];
            
        }
        
    }
    
    return result;
    
}

/*
 * Obtains the list of express agents nearby a latitude and longitude asynchronously
 */
+ (NSString *)expressAgentListForLatitute:(float)aLatitude andLongitude:(float)aLongitude
                              forListener:(id<DownloadListener>)aDownloadListener
                      withOpertaionResult:(OperationResult *)currentResult positionNewCharge:(NSInteger)positionCharge
                               storedInto:(ATMList *)anExpressAgentList {
    
    NSString *result = nil;
    GeoServerManager *manager = [GeoServerManager getInstance];
    
    if ((aLatitude >= -90.0f) && (aLatitude <= 90.0f) && (aLongitude >= -180.0f) && (aLongitude <= 180.0f)) {
        
        NSString *latitudeString = [[NSString stringWithFormat:@"%3.5f", aLatitude] urlEncodeUsingEncoding:NSUTF8StringEncoding];
        NSString *longitudeString = [[NSString stringWithFormat:@"%3.5f", aLongitude] urlEncodeUsingEncoding:NSUTF8StringEncoding];
        NSString *bodyString = [NSString stringWithFormat:@"latitude=%@&longitude=%@", latitudeString, longitudeString];
        bodyString = [bodyString stringByAppendingString:[NSString stringWithFormat:@"&radius=&limit=&startAt=%d",positionCharge]];
        bodyString = [GeoServerManager completeParametersForExpressAgentListRequest:bodyString];
        NSData *bodyData = [NSData dataWithBytes:[bodyString UTF8String]
                                          length:strlen([bodyString UTF8String])];
        
        POIList *poiList = [[[POIList alloc] init] autorelease];
        OperationResult *operationResult = [[[OperationResult alloc] initWithDelegate:poiList] autorelease];;
        
        NSString *uri = GEOSERVER_GET_POIS_URI;
        
        result = [[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri
                                                                          withBody:bodyData
                                                              andXMLParserDelegate:operationResult
                                                                       forListener:manager];
        
        if ([result length] > 0) {
            
            ExpressAgentListPendingRequestInfo *pendingInfo = [[[ExpressAgentListPendingRequestInfo alloc] initWithExpressAgentList:anExpressAgentList
                                                                                                            originalOperationResult:currentResult
                                                                                                                    operationResult:operationResult
                                                                                                                            poiList:poiList
                                                                                                                   downloadListener:aDownloadListener] autorelease];
            [manager->expressAgentListRequests_ setObject:pendingInfo forKey:result];
            
        }
        
    }
    
    return result;
    
}

/*
 * Obtains the list of banking centers nearby a latitude and longitude asynchronously
 */
+ (NSString *)branchListForLatitute:(float)aLatitude andLongitude:(float)aLongitude
                        forListener:(id<DownloadListener>)aDownloadListener
                withOpertaionResult:(OperationResult *)currentResult positionNewCharge:(NSInteger)positionCharge
                         storedInto:(BranchesList *)aBranchList {
    
    NSString *result = nil;
    GeoServerManager *manager = [GeoServerManager getInstance];
    
    if ((aLatitude >= -90.0f) && (aLatitude <= 90.0f) && (aLongitude >= -180.0f) && (aLongitude <= 180.0f)) {
        
        NSString *latitudeString = [[NSString stringWithFormat:@"%3.5f", aLatitude] urlEncodeUsingEncoding:NSUTF8StringEncoding];
        NSString *longitudeString = [[NSString stringWithFormat:@"%3.5f", aLongitude] urlEncodeUsingEncoding:NSUTF8StringEncoding];
        NSString *bodyString = [NSString stringWithFormat:@"latitude=%@&longitude=%@", latitudeString, longitudeString];
        bodyString = [bodyString stringByAppendingString:[NSString stringWithFormat:@"&radius=&limit=&startAt=%ld",(long)positionCharge]];
        bodyString = [GeoServerManager completeParametersForBranchListRequest:bodyString];
        NSData *bodyData = [NSData dataWithBytes:[bodyString UTF8String]
                                          length:strlen([bodyString UTF8String])];
        
        POIList *poiList = [[[POIList alloc] init] autorelease];
        OperationResult *operationResult = [[[OperationResult alloc] initWithDelegate:poiList] autorelease];;
        
        NSString *uri = GEOSERVER_GET_POIS_URI;
        
        result = [[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri
                                                                          withBody:bodyData
                                                              andXMLParserDelegate:operationResult
                                                                       forListener:manager];
        
        if ([result length] > 0) {
            
            BranchListPendingRequestInfo *pendingInfo = [[[BranchListPendingRequestInfo alloc] initWithBranchList:aBranchList
                                                                                          originalOperationResult:currentResult
                                                                                                  operationResult:operationResult
                                                                                                          poiList:poiList
                                                                                                 downloadListener:aDownloadListener] autorelease];
            
            [manager->branchesListRequests_ setObject:pendingInfo forKey:result];
            
        }
        
        
    }
    
    return result;
    
}

/*
 * Obtains the address for a latitude and longitude asynchronously
 */
+ (NSString *)addressForLatitute:(float)aLatitude longitude:(float)aLongitude
                     forListener:(id<DownloadListener>)aDownloadListener analyzedBy:(id)anXMLParserDelegate {
    
    NSString *result = nil;
    
    if ((aLatitude >= -90.0f) && (aLatitude <= 90.0f) && (aLongitude >= -180.0f) && (aLongitude <= 180.0f)) {
        
        NSString *latitudeString = [[NSString stringWithFormat:@"%3.5f", aLatitude] urlEncodeUsingEncoding:NSUTF8StringEncoding];
        NSString *longitudeString = [[NSString stringWithFormat:@"%3.5f", aLongitude] urlEncodeUsingEncoding:NSUTF8StringEncoding];
        NSString *bodyString = [NSString stringWithFormat:@"latitude=%@&longitude=%@", latitudeString, longitudeString];
        bodyString = [GeoServerManager completeParametersForAddressForCoordinatesListRequest:bodyString];
        NSData *bodyData = [NSData dataWithBytes:[bodyString UTF8String]
                                          length:strlen([bodyString UTF8String])];
        
        NSString *uri = GEOSERVER_GET_ADDRESS_FOR_COORDINATES_URI;
        
        result = [[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri
                                                                          withBody:bodyData
                                                              andXMLParserDelegate:anXMLParserDelegate
                                                                       forListener:aDownloadListener];
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Cancel management

/**
 * Cancels the given BBVAContigo download process
 *
 *
 * @param aDownloadId The download identification
 */
+ (void)cancelDownload:(NSString *)aDownloadId {
    
    [[HTTPInvoker getInstance] cancelDownload:aDownloadId];
    
}

/**
 * Cancels all BBVAContigo downloads
 */
+ (void)cancelAllDownloads {
    
    [[HTTPInvoker getInstance] cancelAllDownloads];
    
}

#pragma mark -
#pragma mark DownloadListener methods

/**
 * Notifies the download has just started. No operation is performed
 *
 * @param aDownloadId The download identification
 * @param aSize The downloaded data size if known, 0 if unknown
 */
- (void)startDownload:(NSString *)aDownloadId withSize:(unsigned long long)aSize {
    
    if ([[atmListRequests_ allKeys] containsObject:aDownloadId]) {
        
        ATMListPendingRequestInfo *pendingInfo = [atmListRequests_ objectForKey:aDownloadId];
        [pendingInfo.downloadListener startDownload:aDownloadId
                                           withSize:aSize];
        
    } else if ([[expressAgentListRequests_ allKeys] containsObject:aDownloadId]) {
        
        ExpressAgentListPendingRequestInfo *pendingInfo = [expressAgentListRequests_ objectForKey:aDownloadId];
        [pendingInfo.downloadListener startDownload:aDownloadId
                                           withSize:aSize];
        
    }else if ([[branchesListRequests_ allKeys] containsObject:aDownloadId]) {
        
        BranchListPendingRequestInfo *pendingInfo = [branchesListRequests_ objectForKey:aDownloadId];
        [pendingInfo.downloadListener startDownload:aDownloadId
                                           withSize:aSize];
        
    }
    
}

/**
 * Notifies the download has just finished. Response is analyzed
 *
 * @param aDownloadId The download identification
 * @param aSize The downloaded data size
 */
- (void)endDownload:(NSString *)aDownloadId withSize:(unsigned long long)aSize {
    
    if ([[atmListRequests_ allKeys] containsObject:aDownloadId]) {
        
        ATMListPendingRequestInfo *pendingInfo = [atmListRequests_ objectForKey:aDownloadId];
        [self fillATMList:pendingInfo.atmList
              withPOIList:pendingInfo.poiList];
        [pendingInfo.downloadListener endDownload:aDownloadId
                                         withSize:aSize];
        
        OperationResult *originalOperationResult = pendingInfo.originalOperationResult;
        OperationResult *activeOperationResult = pendingInfo.operationResult;
        
        originalOperationResult.result.code = activeOperationResult.result.code;
        originalOperationResult.result.hasMoreData = activeOperationResult.result.hasMoreData;
        originalOperationResult.result.info = activeOperationResult.result.info;
        
        [atmListRequests_ removeObjectForKey:aDownloadId];
        
    } else if ([[expressAgentListRequests_ allKeys] containsObject:aDownloadId]) {
        
        ExpressAgentListPendingRequestInfo *pendingInfo = [expressAgentListRequests_ objectForKey:aDownloadId];
        [self fillExpressAgentList:pendingInfo.expressAgentList
                       withPOIList:pendingInfo.poiList];
        [pendingInfo.downloadListener endDownload:aDownloadId
                                         withSize:aSize];
        
        OperationResult *originalOperationResult = pendingInfo.originalOperationResult;
        OperationResult *activeOperationResult = pendingInfo.operationResult;
        
        originalOperationResult.result.code = activeOperationResult.result.code;
        originalOperationResult.result.hasMoreData = activeOperationResult.result.hasMoreData;
        originalOperationResult.result.info = activeOperationResult.result.info;
        
        [expressAgentListRequests_ removeObjectForKey:aDownloadId];
        
    } else if ([[branchesListRequests_ allKeys] containsObject:aDownloadId]) {
        
        BranchListPendingRequestInfo *pendingInfo = [branchesListRequests_ objectForKey:aDownloadId];
        [self fillBranchList:pendingInfo.branchList
                 withPOIList:pendingInfo.poiList];
        [pendingInfo.downloadListener endDownload:aDownloadId
                                         withSize:aSize];
        OperationResult *originalOperationResult = pendingInfo.originalOperationResult;
        OperationResult *activeOperationResult = pendingInfo.operationResult;
        
        originalOperationResult.result.code = activeOperationResult.result.code;
        originalOperationResult.result.hasMoreData = activeOperationResult.result.hasMoreData;
        originalOperationResult.result.info = activeOperationResult.result.info;
        
        [branchesListRequests_ removeObjectForKey:aDownloadId];
        
    }
    
    
    
}

/**
 * Notifies the download has finished in error. The configuration fail flag is set to YES
 *
 * @param aDownloadId The download identification
 * @param anError The download error
 * @param aXMLString The received string
 */
- (void)download:(NSString *)aDownloadId error:(NSError *)anError xmlString:(NSString *)aXMLString {
    
    if ([[atmListRequests_ allKeys] containsObject:aDownloadId]) {
        
        ATMListPendingRequestInfo *pendingInfo = [atmListRequests_ objectForKey:aDownloadId];
        [pendingInfo.downloadListener download:aDownloadId error:anError xmlString:nil];
        [atmListRequests_ removeObjectForKey:aDownloadId];
        
    } else if ([[expressAgentListRequests_ allKeys] containsObject:aDownloadId]) {
        
        ExpressAgentListPendingRequestInfo *pendingInfo = [expressAgentListRequests_ objectForKey:aDownloadId];
        [pendingInfo.downloadListener download:aDownloadId error:anError xmlString:nil];
        [expressAgentListRequests_ removeObjectForKey:aDownloadId];
        
    } else if ([[branchesListRequests_ allKeys] containsObject:aDownloadId]) {
        
        BranchListPendingRequestInfo *pendingInfo = [branchesListRequests_ objectForKey:aDownloadId];
        [pendingInfo.downloadListener download:aDownloadId error:anError xmlString:nil];
        [branchesListRequests_ removeObjectForKey:aDownloadId];
        
    }
    
}

/**
 * Notifies the download has finished with a status code different than OK_STATUS (200). The configuration fail flag is set to YES
 *
 * @param aDownloadId The download identification
 * @param aStatusCode The status code that finished the download
 */
- (void)download:(NSString *)aDownloadId finishedWithStatusCode:(NSInteger)aStatusCode {
    
    if ([[atmListRequests_ allKeys] containsObject:aDownloadId]) {
        
        ATMListPendingRequestInfo *pendingInfo = [atmListRequests_ objectForKey:aDownloadId];
        [pendingInfo.downloadListener download:aDownloadId
                        finishedWithStatusCode:aStatusCode];
        [atmListRequests_ removeObjectForKey:aDownloadId];
        
    } else if ([[expressAgentListRequests_ allKeys] containsObject:aDownloadId]) {
        
        ExpressAgentListPendingRequestInfo *pendingInfo = [expressAgentListRequests_ objectForKey:aDownloadId];
        [pendingInfo.downloadListener download:aDownloadId
                        finishedWithStatusCode:aStatusCode];
        [expressAgentListRequests_ removeObjectForKey:aDownloadId];
        
    } else if ([[branchesListRequests_ allKeys] containsObject:aDownloadId]) {
        
        BranchListPendingRequestInfo *pendingInfo = [branchesListRequests_ objectForKey:aDownloadId];
        [pendingInfo.downloadListener download:aDownloadId
                        finishedWithStatusCode:aStatusCode];
        [branchesListRequests_ removeObjectForKey:aDownloadId];
        
    }
    
}

/**
 * Notifies the download was cancelled. Configuration information is reseted
 *
 * @param aDownloadId The download identification
 */
- (void)cancelledDownload:(NSString *)aDownloadId {
    
    if ([[atmListRequests_ allKeys] containsObject:aDownloadId]) {
        
        ATMListPendingRequestInfo *pendingInfo = [atmListRequests_ objectForKey:aDownloadId];
        [pendingInfo.downloadListener cancelledDownload:aDownloadId];
        [atmListRequests_ removeObjectForKey:aDownloadId];
        
    } else if ([[expressAgentListRequests_ allKeys] containsObject:aDownloadId]) {
        
        ExpressAgentListPendingRequestInfo *pendingInfo = [expressAgentListRequests_ objectForKey:aDownloadId];
        [pendingInfo.downloadListener cancelledDownload:aDownloadId];
        [expressAgentListRequests_ removeObjectForKey:aDownloadId];
        
    } else if ([[branchesListRequests_ allKeys] containsObject:aDownloadId]) {
        
        BranchListPendingRequestInfo *pendingInfo = [branchesListRequests_ objectForKey:aDownloadId];
        [pendingInfo.downloadListener cancelledDownload:aDownloadId];
        [branchesListRequests_ removeObjectForKey:aDownloadId];
        
    }
    
}

#pragma mark -
#pragma mark Utility methods

/*
 * Generates a new parameter string for the ATM list request list
 */
+ (NSString *)completeParametersForATMListRequest:(NSString *)aParameterString{
    
    NSMutableString* result = [NSMutableString string];
    
    if ([aParameterString length] > 0) {
        
        [result appendString:aParameterString];
        [result appendString:@"&"];
        
    }
    
    [result appendString:@"filter="];
    
    NSString *ATMS =[NSString stringWithFormat:@"%@,%@,%@,%@",ATM_TYPE,ATM_BC_TYPE,ATM_INTERBANK_TYPE,ATM_SCOTIABANK_TYPE];
    
    [result appendString:[ATMS urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    [result appendString:@"&interface_version="];
    [result appendString:[INTERFACE_VERSION urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&key="];
    [result appendString:[APPLICATION_KEY urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&applicationId="];
    [result appendString:[APPLICATION_ID urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    [result appendString:@"&appVersion="];
    //	[result appendString:[APPLICATION_VERSION urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:[APP_VERSION_STRING urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&platform="];
    [result appendString:[PLATFORM urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&country="];
    [result appendString:[COUNTRY urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    [result appendString:@"&user="];
    [result appendString:[[GeoServerManager getInstance]->uuid_ urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    return result;
    
}

/*
 * Generates a new parameter string for the express agent list request list
 *
 * @param aParameterString Original parameter string
 * @return New parameter string
 */
+ (NSString *)completeParametersForExpressAgentListRequest:(NSString *)aParameterString {
    
    NSMutableString* result = [NSMutableString string];
    
    if ([aParameterString length] > 0) {
        
        [result appendString:aParameterString];
        [result appendString:@"&"];
        
    }
    
    [result appendString:@"filter="];
    
    NSString *AGENTS =[NSString stringWithFormat:@"%@,%@,%@,%@",EXPRESS_AGENT_TYPE,AGENT_AEPLUS_TYPE,AGENT_KASNET_TYPE,AGENT_MULTIFACIL_TYPE];
    
    [result appendString:[AGENTS urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    [result appendString:@"&interface_version="];
    [result appendString:[INTERFACE_VERSION urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&key="];
    [result appendString:[APPLICATION_KEY urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&applicationId="];
    [result appendString:[APPLICATION_ID urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    [result appendString:@"&appVersion="];
    //	[result appendString:[APPLICATION_VERSION urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:[APP_VERSION_STRING urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&platform="];
    [result appendString:[PLATFORM urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&country="];
    [result appendString:[COUNTRY urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    [result appendString:@"&user="];
    [result appendString:[[GeoServerManager getInstance]->uuid_ urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    return result;
    
}

/*
 * Generates a new parameter string for the branch list request list
 */
+ (NSString *)completeParametersForBranchListRequest:(NSString *)aParameterString {
    
    NSMutableString* result = [NSMutableString string];
    
    if ([aParameterString length] > 0) {
        
        [result appendString:aParameterString];
        [result appendString:@"&"];
        
    }
    
    [result appendString:@"filter="];
    [result appendString:[BRANCH_TYPE urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    [result appendString:@"&interface_version="];
    [result appendString:[INTERFACE_VERSION urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&key="];
    [result appendString:[APPLICATION_KEY urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&applicationId="];
    [result appendString:[APPLICATION_ID urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    [result appendString:@"&appVersion="];
    //[result appendString:[APPLICATION_VERSION urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:[APP_VERSION_STRING urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&platform="];
    [result appendString:[PLATFORM urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&country="];
    [result appendString:[COUNTRY urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    [result appendString:@"&user="];
    [result appendString:[[GeoServerManager getInstance]->uuid_ urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    return result;
    
}

/*
 * Fills the ATM list with the POI list information
 */
- (void)fillATMList:(ATMList *)anATMList
        withPOIList:(POIList *)aPOIList {
    
    NSArray *poiList = aPOIList.poiList;
    
    float latitude;
    float longitude;
    NSString *poiId = nil;
    NSString *type = nil;
    NSString *telephone = nil;
    NSString *status = nil;
    NSString *network = nil;
    NSString *bankName = nil;
    NSString *firstAddressLine = nil;
    NSString *secondAddressLine = nil;
    NSString *addressDescription = nil;
    NSString *distanceString = nil;
    NSString *routingAvailableString = nil;
    NSString *hoursMf = nil;
    NSString *hoursSat = nil;
    NSString *hoursSun = nil;
    
    for (POI *poi in poiList) {
        
        poiId = poi.poiId;
        latitude = poi.latitude;
        longitude = poi.longitude;
        
        NSDictionary *poiAttributes = poi.attributesDictionary;
        
        type = [poiAttributes objectForKey:@"type"];
        telephone = [poiAttributes objectForKey:@"telephone"];
        status = [poiAttributes objectForKey:@"status"];
        network = [poiAttributes objectForKey:@"network"];
        //        bankName = [poiAttributes objectForKey:@"id_bank"];
        bankName = [poiAttributes objectForKey:@"description"];
        firstAddressLine = [poiAttributes objectForKey:@"address1"];
        secondAddressLine = [poiAttributes objectForKey:@"address2"];
        addressDescription = [poiAttributes objectForKey:@"description"];
        distanceString = [poiAttributes objectForKey:@"distance"];
        routingAvailableString = [poiAttributes objectForKey:@"routingAvailable"];
        hoursMf = [poiAttributes objectForKey:@"hours_mf"];
        hoursSat = [poiAttributes objectForKey:@"hours_sat"];
        hoursSun = [poiAttributes objectForKey:@"hours_sun"];
        
        ATMData *atm = [[[ATMData alloc] init] autorelease];
        
        GeocodedAddressData *geocodedAddress = atm.geocodedAddress;
        GeoPositionData *geoPosition = geocodedAddress.geoPosition;
        geoPosition.latitude = latitude;
        geoPosition.longitude = longitude;
        AddressData *address = geocodedAddress.address;
        address.addressFirstLine = firstAddressLine;
        address.addressSecondLine = secondAddressLine;
        address.addressDescription = addressDescription;
        GeoFeaturesData *geoFeatures = geocodedAddress.geoFeatures;
        geoFeatures.addressDistance = [distanceString integerValue];
        geoFeatures.routingAvailable = [routingAvailableString isEqualToString:@"true"];
        atm.poiId = poiId;
        atm.type = type;
        atm.telephone = telephone;
        atm.status = status;
        atm.network = network;
        atm.bankName = bankName;
        atm.hoursMf = hoursMf;
        atm.hoursSat = hoursSat;
        atm.hoursSun = hoursSun;
        
        
        
        if([poi.poiCategory isEqualToString:ATM_TYPE]){
            atm.category = atmType;
        }else if ([poi.poiCategory isEqualToString:ATM_BC_TYPE]){
            atm.category = atmBCType;
        }else if ([poi.poiCategory isEqualToString:ATM_INTERBANK_TYPE]){
            atm.category = atmInterbankType;
        }else if ([poi.poiCategory isEqualToString:ATM_SCOTIABANK_TYPE]){
            atm.category = atmScotiabankType;
        }
        
        [anATMList addATM:atm];
        
        
        
    }
    
}

/*
 * Fills the express agent list with the POI list information
 */
- (void)fillExpressAgentList:(ATMList *)anExpressAgentList
                 withPOIList:(POIList *)aPOIList {
    
    NSArray *poiList = aPOIList.poiList;
    
    float latitude;
    float longitude;
    NSString *poiId = nil;
    NSString *type = nil;
    NSString *telephone = nil;
    NSString *status = nil;
    NSString *network = nil;
    NSString *bankName = nil;
    NSString *firstAddressLine = nil;
    NSString *secondAddressLine = nil;
    NSString *addressDescription = nil;
    NSString *distanceString = nil;
    NSString *routingAvailableString = nil;
    NSString *hoursMf = nil;
    NSString *hoursSat = nil;
    NSString *hoursSun = nil;
    
    for (POI *poi in poiList) {
        
        poiId = poi.poiId;
        latitude = poi.latitude;
        longitude = poi.longitude;
        
        NSDictionary *poiAttributes = poi.attributesDictionary;
        
        type = [poiAttributes objectForKey:@"type"];
        telephone = [poiAttributes objectForKey:@"telephone"];
        status = [poiAttributes objectForKey:@"status"];
        network = [poiAttributes objectForKey:@"network"];
        //        bankName = [poiAttributes objectForKey:@"id_bank"];
        bankName = [poiAttributes objectForKey:@"description"];
        firstAddressLine = [poiAttributes objectForKey:@"address1"];
        secondAddressLine = [poiAttributes objectForKey:@"address2"];
        addressDescription = [poiAttributes objectForKey:@"description"];
        distanceString = [poiAttributes objectForKey:@"distance"];
        routingAvailableString = [poiAttributes objectForKey:@"routingAvailable"];
        hoursMf = [poiAttributes objectForKey:@"hours_mf"];
        hoursSat = [poiAttributes objectForKey:@"hours_sat"];
        hoursSun = [poiAttributes objectForKey:@"hours_sun"];
        
        ATMData *expressAgent = [[[ATMData alloc] init] autorelease];
        
        GeocodedAddressData *geocodedAddress = expressAgent.geocodedAddress;
        GeoPositionData *geoPosition = geocodedAddress.geoPosition;
        geoPosition.latitude = latitude;
        geoPosition.longitude = longitude;
        AddressData *address = geocodedAddress.address;
        address.addressFirstLine = firstAddressLine;
        address.addressSecondLine = secondAddressLine;
        address.addressDescription = addressDescription;
        GeoFeaturesData *geoFeatures = geocodedAddress.geoFeatures;
        geoFeatures.addressDistance = [distanceString integerValue];
        geoFeatures.routingAvailable = [routingAvailableString isEqualToString:@"true"];
        expressAgent.poiId = poiId;
        expressAgent.type = type;
        expressAgent.telephone = telephone;
        expressAgent.status = status;
        expressAgent.network = network;
        expressAgent.bankName = bankName;
        expressAgent.hoursMf = hoursMf;
        expressAgent.hoursSat = hoursSat;
        expressAgent.hoursSun = hoursSun;
        
        if([poi.poiCategory isEqualToString:EXPRESS_AGENT_TYPE]){
            expressAgent.category = expressAgentType;
        }else if ([poi.poiCategory isEqualToString:AGENT_AEPLUS_TYPE]){
            expressAgent.category = expressAgentPlusType;
        }else if ([poi.poiCategory isEqualToString:AGENT_KASNET_TYPE]){
            expressAgent.category = kasnetType;
        }else if ([poi.poiCategory isEqualToString:AGENT_MULTIFACIL_TYPE]){
            expressAgent.category = multifacilType;
        }
        
        [anExpressAgentList addATM:expressAgent];
        
    }
    
}

/*
 * Fills the branches list with the POI list information
 */
- (void)fillBranchList:(BranchesList *)aBranchList
           withPOIList:(POIList *)aPOIList {
    
    NSArray *poiList = aPOIList.poiList;
    
    float latitude;
    float longitude;
    NSString *poiId = nil;
    NSString *type = nil;
    NSString *telephone = nil;
    NSString *status = nil;
    NSString *network = nil;
    NSString *bankName = nil;
    NSString *firstAddressLine = nil;
    NSString *secondAddressLine = nil;
    NSString *addressDescription = nil;
    NSString *distanceString = nil;
    NSString *routingAvailableString = nil;
    NSString *hoursMf = nil;
    NSString *hoursSat = nil;
    NSString *hoursSun = nil;
    
    for (POI *poi in poiList) {
        
        poiId = poi.poiId;
        latitude = poi.latitude;
        longitude = poi.longitude;
        
        NSDictionary *poiAttributes = poi.attributesDictionary;
        
        type = [poiAttributes objectForKey:@"type"];
        telephone = [poiAttributes objectForKey:@"telephone"];
        status = [poiAttributes objectForKey:@"status"];
        network = [poiAttributes objectForKey:@"network"];
        //        bankName = [poiAttributes objectForKey:@"id_bank"];
        bankName = [poiAttributes objectForKey:@"description"];
        firstAddressLine = [poiAttributes objectForKey:@"address1"];
        secondAddressLine = [poiAttributes objectForKey:@"address2"];
        addressDescription = [poiAttributes objectForKey:@"description"];
        distanceString = [poiAttributes objectForKey:@"distance"];
        routingAvailableString = [poiAttributes objectForKey:@"routingAvailable"];
        hoursMf = [poiAttributes objectForKey:@"hours_mf"];
        hoursSat = [poiAttributes objectForKey:@"hours_sat"];
        hoursSun = [poiAttributes objectForKey:@"hours_sun"];
        
        BranchData *branch = [[[BranchData alloc] init] autorelease];
        
        GeocodedAddressData *geocodedAddress = branch.geocodedAddress;
        GeoPositionData *geoPosition = geocodedAddress.geoPosition;
        geoPosition.latitude = latitude;
        geoPosition.longitude = longitude;
        AddressData *address = geocodedAddress.address;
        address.addressFirstLine = firstAddressLine;
        address.addressSecondLine = secondAddressLine;
        address.addressDescription = addressDescription;
        GeoFeaturesData *geoFeatures = geocodedAddress.geoFeatures;
        geoFeatures.addressDistance = [distanceString integerValue];
        geoFeatures.routingAvailable = [routingAvailableString isEqualToString:@"true"];
        branch.poiId = poiId;
        branch.type = type;
        branch.telephone = telephone;
        branch.status = status;
        branch.network = network;
        branch.bankName = bankName;
        branch.category =branchType;
        branch.hoursMf = hoursMf;
        branch.hoursSat = hoursSat;
        branch.hoursSun = hoursSun;
        
        [aBranchList addBranch:branch];
        
    }
    
}

#pragma mark -
#pragma mark Geocoding management

/*
 * Obtains all addresses similar to a given string asynchronously
 */
+ (NSString *)streetNamesLikeString:(NSString *)aString forListener:(id<DownloadListener>)aDownloadListener
                         analyzedBy:(id)anXMLParserDelegate {
    
    
    NSString *result = nil;
    
    if ([aString length] > 0) {
        
        NSString *bodyString = [NSString stringWithFormat:@"address=%@", [aString urlEncodeUsingEncoding:NSUTF8StringEncoding]];
        bodyString = [GeoServerManager completeParametersForAddressLikeListRequest:bodyString];
        NSData *bodyData = [NSData dataWithBytes:[bodyString UTF8String]
                                          length:strlen([bodyString UTF8String])];
        
        NSString *uri = GEOSERVER_GET_ADDRESS_LIKE_URI;
        
        result = [[HTTPInvoker getInstance] invokePOSTOperationWithParametersToURL:uri
                                                                          withBody:bodyData
                                                              andXMLParserDelegate:anXMLParserDelegate
                                                                       forListener:aDownloadListener];
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Utility methods

/*
 * Generates a new parameter string for the address like list request list
 */
+ (NSString *)completeParametersForAddressLikeListRequest:(NSString *)aParameterString {
    
    NSMutableString* result = [NSMutableString string];
    
    if ([aParameterString length] > 0) {
        
        [result appendString:aParameterString];
        [result appendString:@"&"];
        
    }
    
    [result appendString:@"&interface_version="];
    [result appendString:[INTERFACE_VERSION urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&key="];
    [result appendString:[APPLICATION_KEY urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&applicationId="];
    [result appendString:[APPLICATION_ID urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    [result appendString:@"&app_version="];
    //[result appendString:[APPLICATION_VERSION urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:[APP_VERSION_STRING urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&platform="];
    [result appendString:[PLATFORM urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&country="];
    [result appendString:[COUNTRY urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    [result appendString:@"&user="];
    [result appendString:[[GeoServerManager getInstance]->uuid_ urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    return result;
    
}

/*
 * Generates a new parameter string for the address for coordinates list request list
 */
+ (NSString *)completeParametersForAddressForCoordinatesListRequest:(NSString *)aParameterString {
    
    NSMutableString* result = [NSMutableString string];
    
    if ([aParameterString length] > 0) {
        
        [result appendString:aParameterString];
        [result appendString:@"&"];
        
    }
    
    [result appendString:@"&interface_version="];
    [result appendString:[INTERFACE_VERSION urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&key="];
    [result appendString:[APPLICATION_KEY urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&applicationId="];
    [result appendString:[APPLICATION_ID urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    [result appendString:@"&app_version="];
    //[result appendString:[APPLICATION_VERSION urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:[APP_VERSION_STRING urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&platform="];
    [result appendString:[PLATFORM urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    [result appendString:@"&country="];
    [result appendString:[COUNTRY urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    [result appendString:@"&user="];
    [result appendString:[[GeoServerManager getInstance]->uuid_ urlEncodeUsingEncoding:NSUTF8StringEncoding]];
    
    return result;
    
}

@end
