/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ParseableObject.h"
#import "ATMList.h"

/**
 * Contains data needed to describe a branch
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BranchData : ATMData {

}

@end


/**
 * List of braqnches information
 */
@interface BranchesList : ParseableObject {
	
@private
	
	/**
	 * Array containing the branches information
	 */
	NSMutableArray *branchList_;
	
	/**
	 * Auxiliar branch object for parsing
	 */
	BranchData *auxBranch_;
	
}

/**
 * Provides read-only access to branchList.
 */
@property (nonatomic, readonly, retain) NSMutableArray *branchList;

/**
 * Provides read access to the number of branches stored
 */
@property (readonly) NSUInteger branchCount;

/**
 * Returns the branch located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the branch is located
 *
 * @return Branch located at the given position, or nil if position is not valid
 */
- (BranchData *)branchAtPostion:(NSUInteger)aPosition;

/**
 * Returns the branch position inside the list
 *
 * @param  aBranchData BranchData object which position we are looking for
 *
 * @return Branch position inside the list
 */
- (NSUInteger)branchPosition:(BranchData *)aBranchData;

/**
 * Adds an branch at the end of the list
 *
 * @param  aBranch The branch to add
 */
- (void)addBranch:(BranchData *)aBranch;

@end
