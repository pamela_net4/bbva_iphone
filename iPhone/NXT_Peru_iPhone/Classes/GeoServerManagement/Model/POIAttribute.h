/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ParseableObject.h"


/**
 * Analyzes a POI attribute
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface POIAttribute : ParseableObject {

@private
    
    /**
     * Attribute name
     */
    NSString *name_;
    
    /**
     * Attribute value
     */
    NSString *value_;
    
}


/**
 * Provides read-only access to the attribute name
 */
@property (nonatomic, readwrite, copy) NSString *name;

/**
 * Provides read-only access to the attribute value
 */
@property (nonatomic, readwrite, copy) NSString *value;

@end
