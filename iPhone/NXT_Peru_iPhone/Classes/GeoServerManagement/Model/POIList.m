/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "POIList.h"
#import "POIAttribute.h"


#pragma mark -
#pragma mark Type definitions

/**
 * Enumerates the POI element analysis states
 */
typedef enum {
	
	pxeas_Nothing = 0, //!<No element is being analyzed
	pxeas_AnalyzingId, //!<The POI identification is being analyzed
	pxeas_AnalyzingCategory, //!<The POI category is being analyzed
	pxeas_AnalyzingLatitude, //!<The POI latitude is being analyzed
	pxeas_AnalyzingLongitude, //!<The POI longitude is being analyzed
	pxeas_AnalyzingAttributeList, //!<The POI attribute list is being analyzed
    pxeas_AnalyzingAttribute //!<A POI attribute is being analyzed
	
} POIXMLElementAnalyzerState;


#pragma mark -

/**
 * POI private category
 */
@interface POI(private)

/**
 * Prepares the instance to start analyzing a new POI element
 */
- (void)prepareToAnalyze;

@end


#pragma mark -

/**
 * POI list private category
 */
@interface POIList(private)

/**
 * Prepares the instance to start analyzing a new POI list
 */
- (void)prepareToAnalyze;

@end


#pragma mark -

@implementation POI

#pragma mark -
#pragma mark Properties

@synthesize poiId = poiId_;
@synthesize poiCategory = poiCategory_;
@synthesize latitude = latitude_;
@synthesize longitude = longitude_;
@synthesize attributesDictionary = attributesDictionary_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [poiId_ release];
    poiId_ = nil;
    
    [poiCategory_ release];
    poiCategory_ = nil;
    
    [poiAttribute_ release];
    poiAttribute_ = nil;
    
    [attributesDictionary_ release];
    attributesDictionary_ = nil;

    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes the POI instance creating the necesary elements
 *
 * @return The initialized POI instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        poiAttribute_ = [[POIAttribute alloc] init];
        attributesDictionary_ = [[NSMutableDictionary alloc] init];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Prepares the instance to start analyzing a new POI element
 */
- (void)prepareToAnalyze {
    
    [poiId_ release];
    poiId_ = nil;
    
    [poiCategory_ release];
    poiCategory_ = nil;
    
    [attributesDictionary_ removeAllObjects];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	[super parserDidStartDocument:parser];
	
	xmlAnalysisCurrentValue_ = pxeas_Nothing;
	[self prepareToAnalyze];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
	
	NSString* lname = [elementName lowercaseString];
	
	if ((xmlAnalysisCurrentValue_ == pxeas_Nothing) && ([lname isEqualToString:@"idpoi"])) {
		
		xmlAnalysisCurrentValue_ = pxeas_AnalyzingId;
		
	} else if ((xmlAnalysisCurrentValue_ == pxeas_Nothing) && ([lname isEqualToString:@"category"])) {
		
		xmlAnalysisCurrentValue_ = pxeas_AnalyzingCategory;
		
	} else if ((xmlAnalysisCurrentValue_ == pxeas_Nothing) && ([lname isEqualToString:@"latitude"])) {
		
		xmlAnalysisCurrentValue_ = pxeas_AnalyzingLatitude;
		
	} else if ((xmlAnalysisCurrentValue_ == pxeas_Nothing) && ([lname isEqualToString:@"longitude"])) {
		
		xmlAnalysisCurrentValue_ = pxeas_AnalyzingLongitude;
		
	} else if ((xmlAnalysisCurrentValue_ == pxeas_Nothing) && ([lname isEqualToString:@"attributes"])) {
		
		xmlAnalysisCurrentValue_ = pxeas_AnalyzingAttributeList;
		
	} else if (((xmlAnalysisCurrentValue_ == pxeas_AnalyzingAttributeList) || (xmlAnalysisCurrentValue_ == pxeas_AnalyzingAttribute)) &&
               ([lname isEqualToString:@"poiattribute"])) {
		
        if (xmlAnalysisCurrentValue_ == pxeas_AnalyzingAttribute) {
        
            NSString *value = poiAttribute_.value;
            NSString *name = poiAttribute_.name;
            
            if ([name length] > 0) {
            
                if (value == nil) {
                    
                    value = @"";
                    
                }
                
                [attributesDictionary_ setObject:value forKey:name];
                
            }
            
        }
        
		xmlAnalysisCurrentValue_ = pxeas_AnalyzingAttribute;
        poiAttribute_.openingTag = @"poiattribute";
        [poiAttribute_ setParentParseableObject:self];
        [parser setDelegate:poiAttribute_];
        [poiAttribute_ parserDidStartDocument:parser];
		
	} else {
		
		xmlAnalysisCurrentValue_ = pxeas_Nothing;
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	[super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
	switch (xmlAnalysisCurrentValue_) {
			
		case pxeas_AnalyzingId: {
			
			[poiId_ release];
			poiId_ = nil;
			poiId_ = [self.elementString copy];
			break;
			
		}
		case pxeas_AnalyzingCategory: {
            
			[poiCategory_ release];
			poiCategory_ = nil;
			poiCategory_ = [self.elementString copy];
			break;
			
		}
		case pxeas_AnalyzingLatitude: {
            
            latitude_ = [self.elementString floatValue];
			break;
			
		}
		case pxeas_AnalyzingLongitude: {
            
            longitude_ = [self.elementString floatValue];
			break;
			
		}
		case pxeas_AnalyzingAttributeList:
        case pxeas_AnalyzingAttribute: {
            
            NSString *value = poiAttribute_.value;
            NSString *name = poiAttribute_.name;
            
            if ([name length] > 0) {
                
                if (value == nil) {
                    
                    value = @"";
                    
                }
                
                [attributesDictionary_ setObject:value forKey:name];
                
            }

			break;
			
		}
		default: {
			
			break;
			
		}
			
	}
	
	xmlAnalysisCurrentValue_ = pxeas_Nothing;
	
}

@end


#pragma mark -
#pragma mark Type definitions

/**
 * Enumerates the POI list analysis states
 */
typedef enum {
	
	plxeas_Nothing = 0, //!<No element is being analyzed
	plxeas_AnalyzingPOIList //!<The POI list is being analyzed
	
} POIListXMLElementAnalyzerState;


#pragma mark -

@implementation POIList

#pragma mark -
#pragma mark Properties

@synthesize poiList = poiList_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {

    [poiList_ release];
    poiList_ = nil;
    
    [auxPOI_ release];
    auxPOI_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes the POI instance creating the necesary elements
 *
 * @return The initialized POI instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        poiList_ = [[NSMutableArray alloc] init];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Utility selectors

/**
 * Prepares the instance to start analyzing a new POI list
 */
- (void)prepareToAnalyze {
    
    [auxPOI_ release];
    auxPOI_ = nil;
    
    [poiList_ removeAllObjects];

}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	[super parserDidStartDocument:parser];
	
	xmlAnalysisCurrentValue_ = plxeas_Nothing;
	[self prepareToAnalyze];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
	
	NSString* lname = [elementName lowercaseString];
	
	if (auxPOI_ != nil) {
		
		[poiList_ addObject:auxPOI_];
		[auxPOI_ release];
		auxPOI_ = nil;
		
	}
	
	if ([lname isEqualToString:@"poi"]) {
		
		xmlAnalysisCurrentValue_ = plxeas_AnalyzingPOIList;
		auxPOI_ = [[POI alloc] init];
		auxPOI_.openingTag = @"poi";
		[auxPOI_ setParentParseableObject:self];
		[parser setDelegate:auxPOI_];
		[auxPOI_ parserDidStartDocument:parser];
		
	} else {
		
		xmlAnalysisCurrentValue_ = plxeas_Nothing;
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	[super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
	
	NSString* lname = [elementName lowercaseString];
	
	if ([lname isEqualToString:self.openingTag]) {
		
		if (auxPOI_ != nil) {
			
            [poiList_ addObject:auxPOI_];
            [auxPOI_ release];
            auxPOI_ = nil;
			
		}
		
	}
	
	xmlAnalysisCurrentValue_ = plxeas_Nothing;
	
}

@end
