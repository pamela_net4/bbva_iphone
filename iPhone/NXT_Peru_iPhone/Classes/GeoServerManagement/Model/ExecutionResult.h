/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "ParseableObject.h"

#define NO_DATA_WARNING					-2
#define CLIENT_ERROR					-1

#define RESULT_OK						0

#define WARNING_NEW_VERSION				1
#define ERROR_NEW_VERSION				2
#define ERROR_PARAMETERS_SENT			3


/**
 * Contains information about how operation execution finished
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ExecutionResult : ParseableObject {
	
@private
	
	/**
	 * Execution result code
	 */
	NSInteger code_;
    
    /**
     * The response is part of a larger list flag. YES when more elements are missing from the list, NO otherwise
     */
    BOOL hasMoreData_;
	
	/**
	 * Execution result additional information
	 */
	NSString* info_;
	
}

/**
 * Provides read/write access to the execution result code
 */
@property (nonatomic, readwrite) NSInteger code;

/**
 * Provides read/write access to the the response is part of a larger list flag
 */
@property (nonatomic, readwrite) BOOL hasMoreData;

/**
 * Provides read/write access to the execution result additional information
 */
@property (readwrite, copy) NSString* info;

@end
