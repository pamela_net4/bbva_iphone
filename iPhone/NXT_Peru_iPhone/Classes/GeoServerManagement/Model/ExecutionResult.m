/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ExecutionResult.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
	
	erxeas_Nothing = 0,
	erxeas_AnalyzingValue,
    erxeas_AnalyzingHasMoreData,
	erxeas_AnalyzingInfo
	
} ExecutionResultXMLElementAnalyzerState;

@implementation ExecutionResult

@synthesize code = code_;
@synthesize hasMoreData = hasMoreData_;
@synthesize info = info_;

/**
 * Deallocates used memory
 */
- (void) dealloc {
	
	[info_ release];
	info_ = nil;
	
	[super dealloc];
	
}

/**	
 * Object initializer
 *
 * @return An initialized instance
 */
- (id) init {
	
	if (self = [super init]) {
		
		code_ = RESULT_OK;
		info_ = nil;
		
	}
	
	return self;
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void) parserDidStartDocument: (NSXMLParser*) parser {
	
	[super parserDidStartDocument:parser];
	
	[info_ release];
	info_ = nil;
	
	xmlAnalysisCurrentValue_ = erxeas_Nothing;
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void) parser: (NSXMLParser*) parser didStartElement: (NSString*) elementName namespaceURI: (NSString*) namespaceURI 
  qualifiedName: (NSString*) qualifiedName attributes: (NSDictionary*) attributeDict {
	
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
	
	NSString* lname = [elementName lowercaseString];
	
	if ([lname isEqualToString: @"value"]) {
		
		xmlAnalysisCurrentValue_ = erxeas_AnalyzingValue;
		
	} else if ([lname isEqualToString:@"hasmoredata"]) {
      
        xmlAnalysisCurrentValue_ = erxeas_AnalyzingHasMoreData;
        
    } else if ([lname isEqualToString: @"info"]) {
		
		xmlAnalysisCurrentValue_ = erxeas_AnalyzingInfo;
		
	} else {
		
		xmlAnalysisCurrentValue_ = erxeas_Nothing;
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void) parser: (NSXMLParser*) parser didEndElement: (NSString*) elementName namespaceURI: (NSString*) namespaceURI qualifiedName: (NSString*) qName {
	
	[super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
	
	switch (xmlAnalysisCurrentValue_) {
			
		case erxeas_AnalyzingValue: {
			
			code_ = [self.elementString integerValue];
			break;
			
		}
        case erxeas_AnalyzingHasMoreData: {
            
            hasMoreData_ = [[self.elementString lowercaseString] isEqualToString:@"true"];
            break;
            
        }
		case erxeas_AnalyzingInfo: {
			
			[info_ release];
			info_ = nil;
			info_ = [self.elementString copy];
			break;
			
		}
		default: {
			
			break;
			
		}
			
	}
	
	xmlAnalysisCurrentValue_ = erxeas_Nothing;
	
}

@end
