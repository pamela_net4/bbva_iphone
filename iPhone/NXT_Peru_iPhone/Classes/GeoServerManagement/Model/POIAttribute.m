/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "POIAttribute.h"


/**
 * Enumerates the different analysis states
 */
typedef enum {
	
	paxeas_Nothing = 0, //!<No element is being analyzed
	paxeas_AnalyzingName, //!<The atribute name is being analyzed
    paxeas_AnalyzingValue //!<The attribute value is being analyzed
	
} POIAttributeXMLElementAnalyzerState;


#pragma mark -

/**
 * POIAttribute private category
 */
@interface POIAttribute(private)

/**
 * Releases the memory used
 */
- (void)releaseMemory;

@end


#pragma mark -

@implementation POIAttribute

#pragma mark -
#pragma mark Properties

@synthesize name = name_;
@synthesize value = value_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self releaseMemory];
    
    [super dealloc];
    
}

/**
 * Releases the memory used
 */
- (void)releaseMemory {
    
    [name_ release];
    name_ = nil;
    
    [value_ release];
    value_ = nil;
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	[super parserDidStartDocument:parser];
	
	xmlAnalysisCurrentValue_ = paxeas_Nothing;
	[self releaseMemory];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
	
	NSString* lname = [elementName lowercaseString];
	
	if ([lname isEqualToString:@"name"]) {
		
		xmlAnalysisCurrentValue_ = paxeas_AnalyzingName;
		
	} else if ([lname isEqualToString:@"value"]) {
		
		xmlAnalysisCurrentValue_ = paxeas_AnalyzingValue;
		
	} else {
		
		xmlAnalysisCurrentValue_ = paxeas_Nothing;
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	[super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	switch (xmlAnalysisCurrentValue_) {
			
		case paxeas_AnalyzingName: {
			
			[name_ release];
			name_ = nil;
			name_ = [elementString copy];
			break;
			
		}
		case paxeas_AnalyzingValue: {
            
			[value_ release];
			value_ = nil;
			value_ = [elementString copy];
			break;
			
		}
		default: {
			
			break;
			
		}
			
	}
	
	xmlAnalysisCurrentValue_ = paxeas_Nothing;
	
}

@end
