/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ParseableObject.h"


//Forward declarations
@class POIAttribute;


/**
 * Contains the information that defines a POI
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface POI : ParseableObject {
    
@private
    
    /**
     * POI identification
     */
    NSString *poiId_;
    
    /**
     * POI category
     */
    NSString *poiCategory_;
    
    /**
     * POI latitude
     */
    float latitude_;
    
    /**
     * POI longitude
     */
    float longitude_;
    
    /**
     * Analyzes the POI attributes
     */
    POIAttribute *poiAttribute_;
    
    /**
     * Attributes dictionary
     */
    NSMutableDictionary *attributesDictionary_;
    
}

/**
 * Provides read-only access to the POI identification
 */
@property (nonatomic, readonly, copy) NSString *poiId;

/**
 * Provides read-only access to the POI category
 */
@property (nonatomic, readonly, copy) NSString *poiCategory;

/**
 * Provides read-only access to the POI latitude
 */
@property (nonatomic, readonly) float latitude;

/**
 * Provides read-only access to the POI longitude
 */
@property (nonatomic, readonly) float longitude;

/**
 * Provides read-only access to the attributes dictionary
 */
@property (nonatomic, readonly, retain) NSDictionary *attributesDictionary;

@end


/**
 * Contains the information that defines a POI list
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface POIList : ParseableObject {

@private
    
    /**
     * Contains the POI list analyzed
     */
    NSMutableArray *poiList_;
    
    /**
     * Auxiliary POI instance to analyze the XML document
     */
    POI *auxPOI_;
    
}

/**
 * Provides read-only access to the POI list
 */
@property (nonatomic, readonly, retain) NSArray *poiList;

@end
