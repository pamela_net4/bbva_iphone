/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "BranchesList.h"
#import "GeocodedAddressList.h"


#pragma mark -

/**
 * BranchesList private category
 */
@interface BranchesList(private)

/**
 * Clears the object data
 */
- (void)clearData;

@end


#pragma mark -
#pragma mark Type definitions

/**
 * Enumeration for the state of the analysis
 */
typedef enum {
	
	aobxeas_Nothing = 0,
	aobxeas_AnalyzingArrayOfBranch
	
} ArrayOfBranchXMLElementAnalyzerState;


#pragma mark -

@implementation BranchData

@end


#pragma mark -

@implementation BranchesList

#pragma mark -
#pragma mark Properties
@synthesize branchList = branchList_;
@dynamic branchCount;

#pragma mark -
#pragma mark List methods

/*
 * Adds an branch at the end of the list
 */
- (void)addBranch:(BranchData *)aBranchData {
	
	if (aBranchData != nil) {
		
		[branchList_ addObject:aBranchData];
		
	}
	
}

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
	
	[self clearData];
	
	[super dealloc];
	
}

/*
 * Clears the object data
 */
- (void)clearData {
	
	[branchList_ release];
	branchList_ = nil;
	
	[auxBranch_ release];
	auxBranch_ = nil;
	
}

#pragma mark -
#pragma mark Initialization

/**	
 * Object initializer
 *
 * @return An initialized instance
 */
- (id)init {
	
	if (self = [super init]) {
		
		branchList_ = [[NSMutableArray alloc] init];
		
	}
	
	return self;
	
}

#pragma mark -
#pragma mark Data access

/*
 * Returns the branch located at the given position, or nil if position	is not valid
 */
- (BranchData *)branchAtPostion:(NSUInteger)aPosition {
	
	BranchData* result = nil;
	
	if (aPosition < [branchList_ count]) {
		
		result = [branchList_ objectAtIndex: aPosition];
		
	}
	
	return result;
	
}

/*	
 * Returns the branch position inside the list
 */
- (NSUInteger)branchPosition:(BranchData *)aBranchData {
	
	return [branchList_ indexOfObject:aBranchData];
	
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Provides read access to the number of branches stored
 *
 * @return The count
 */
- (NSUInteger)branchCount {
	
	return [branchList_ count];
	
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	[super parserDidStartDocument:parser];
	
	xmlAnalysisCurrentValue_ = aobxeas_Nothing;
	[self clearData];
	
	branchList_ = [[NSMutableArray alloc] init];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *) parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
	
	NSString* lname = [elementName lowercaseString];
	
	if (auxBranch_ != nil) {
		
		[self addBranch:auxBranch_];
		[auxBranch_ release];
		auxBranch_ = nil;
		
	}
	
	if ([lname isEqualToString:@"bankingcenter"]) {
		
		xmlAnalysisCurrentValue_ = aobxeas_AnalyzingArrayOfBranch;
		auxBranch_ = [[BranchData alloc] init];
		auxBranch_.openingTag = @"bankingcenter";
		[auxBranch_ setParentParseableObject:self];
		[parser setDelegate:auxBranch_];
		[auxBranch_ parserDidStartDocument:parser];
		
	} else {
		
		xmlAnalysisCurrentValue_ = aobxeas_Nothing;
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	[super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
	
	NSString* lname = [elementName lowercaseString];
	
	if ([lname isEqualToString:self.openingTag]) {
		
		if (auxBranch_ != nil) {
			
			[self addBranch:auxBranch_];
			[auxBranch_ release];
			auxBranch_ = nil;
			
		}
		
	}
	
	xmlAnalysisCurrentValue_ = aobxeas_Nothing;
	
}

@end
