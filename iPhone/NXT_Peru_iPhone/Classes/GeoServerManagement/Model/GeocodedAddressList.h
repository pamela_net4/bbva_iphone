/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ParseableObject.h"

/**
 * Latitude and longitude that represent a point on Earth's surface
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GeoPositionData : ParseableObject {
	
@private
	
	/**
	 * Point's latitude represented as degrees
	 */
	float latitude_;

	/**
	 * Point's longitude represented as degrees
	 */
	float longitude_;
	
}

/**
 * Provides read/write access to the latitude represented as degrees
 */
@property (readwrite) float latitude;

/**
 * Provides read/write access to the longitude represented as degrees
 */
@property (readwrite) float longitude;

/**
 * Designated initializer. Initializes a GeoPositionData with the information provider
 *
 * @param aLatitude Point's latitude represented as degrees
 * @param aLongitude Point longitude represented as degrees
 * @return Initialized GeoPositionData instance
 */
- (id)initWithLatitude:(float)aLatitude andLongitude:(float)aLongitude;

@end

/**
 * Information representing an address direction
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AddressData : ParseableObject {
	
@private
	
	/**
	 * Address first text line
	 */
	NSString *addressFirstLine_;

	/**
	 * Address second text line
	 */
	NSString *addressSecondLine_;

	/**
	 * Address description
	 */
	NSString *addressDescription_;
	
}

/**
 * Provides read/write access to the address first text line
 */
@property (readwrite, copy) NSString *addressFirstLine;

/**
 * Provides read/write access to the address second text line
 */
@property (readwrite, copy) NSString *addressSecondLine;

/**
 * Provides read/write access to the address description
 */
@property (readwrite, copy) NSString *addressDescription;

/**
 * Designated initializer. Initializes an AddressData instance with the provided data
 *
 * @param anAddressFirstLine Address first text line
 * @param anAddressSecondLine Address second text line
 * @return Initialized AddressData instance
 */
- (id)initWithFirstLine:(NSString *)anAddressFirstLine andSecondLine:(NSString *)anAddressSecondLine;

/**
 * Clears the data of the object
 */
- (void)clearData;

@end


/**
 * Features associated to the geocoded address
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GeoFeaturesData : ParseableObject {
	
@private
	
	/**
	 * Routing service is available for this address
	 */
	BOOL routingAvailable_;

	/**
	 * Address distance to a given reference (known by server and client)
	 */
	NSInteger addressDistance_;
	
}

/**
 * Provides read/write access to the routing available feature
 */
@property (readwrite) BOOL routingAvailable;

/**
 * Provides read/write access to the address distance
 */
@property (readwrite) NSInteger addressDistance;

@end


/**
 * Geocoded address
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GeocodedAddressData : ParseableObject {
	
@private
	
	/**
	 * Address information
	 */
	AddressData *addressData_;

	/**
	 * Geoposition information
	 */
	GeoPositionData *geoPositionData_;

	/**
	 * Features associated to the geocoded address
	 */
	GeoFeaturesData *geoFeaturesData_;
	
	/**
	 * Location obtained using GPS flag
	 */
	BOOL obtainedViaGPS_;
	
}

/**
 * Provides read access to the address information
 */
@property (readonly, assign) AddressData *address;

/**
 * Provides read access to the geoposition information
 */
@property (readonly, assign) GeoPositionData *geoPosition;

/**
 * Provides read access to the associated features
 */
@property (readonly, assign) GeoFeaturesData *geoFeatures;

/**
 * Provides read/write access to the location obtained using GPS flag
 */
@property (nonatomic, readwrite, assign) BOOL obtainedViaGPS;

/**
 * Clears the data of the object
 */
- (void)clearData;

@end


/**
 * List of geocoded addresses
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GeocodedAddressList : ParseableObject {
	
@private
	
	/**
	 * Array containing the geocoded addresses
	 */
	NSMutableArray *geocodedAddressList_;
	
	/**
	 * Auxiliar GeocodedAddressData for parsing
	 */
	GeocodedAddressData *auxGeocodedAddress_;
	
}

/**
 * Provides read access to the number of geocoded addreses stored
 */
@property (readonly) NSUInteger geocodedAddressCount;


/**
 * Returns the geocoded address located at the given position, or nil if position is not valid
 *
 * @param aPosition Position the geocoded address is located at
 * @return Geocoded address located at the given position, or nil if position is not valid
 */
- (GeocodedAddressData *)geocodedAddressAtPosition:(NSUInteger)aPosition;

/**
 * Adds a geocoded address at the end of the list
 *
 * @param aGeocodedAddress Geocoded address to add
 */
- (void)addGeocodedAddress:(GeocodedAddressData *)aGeocodedAddress;

/**
 * Clears the data of the object
 */
- (void)clearData;

@end
