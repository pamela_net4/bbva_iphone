/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ATMList.h"
#import "GeocodedAddressList.h"


#pragma mark -
#pragma mark Type definitions

/**
 * Enumeration for the state of the analysis
 */
typedef enum {
	
	axeas_Nothing = 0,
	axeas_AnalyzingType,
	axeas_AnalyzingTelephone,
	axeas_AnalyzingGeocodedAddress,
	axeas_AnalyzingStatus,
	axeas_AnalyzingNetwork,
	axeas_AnalyzingBankName,
    axeas_AnalyzingBankHoursMf,
    axeas_AnalyzingBankHoursSat,
    axeas_AnalyzingBankHoursSun
	
} ATMXMLElementAnalyzerState;


#pragma mark -

/**
 * ATMList private category
 */
@interface ATMList(private)

/**
 * Clears the object data
 */
- (void)clearData;

@end


#pragma mark -

@implementation ATMData

#pragma mark -
#pragma mark Properties

@synthesize poiId = poiId_;
@synthesize type = type_;
@synthesize telephone = telephone_;
@synthesize status = status_;
@synthesize network = network_;
@synthesize bankName = bankName_;
@synthesize category = category_;
@synthesize geocodedAddress = geocodedAddress_;
@synthesize hoursMf = hoursMf_;
@synthesize hoursSat = hoursSat_;
@synthesize hoursSun = hoursSun_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
	
	[self clearData];
	
	[super dealloc];
	
}


/*
 * Clears the object data
 */
- (void)clearData {
	
	[geocodedAddress_ release];
	geocodedAddress_ = nil;
	
	[bankName_ release];
	bankName_ = nil;
    
    [hoursMf_ release];
    hoursMf_ = nil;
    
    [hoursSat_ release];
    hoursSat_ = nil;
    
    [hoursSun_ release];
    hoursSun_= nil;
	
	[network_ release];
	network_ = nil;
	
	[status_ release];
	status_ = nil;
	
	[telephone_ release];
	telephone_ = nil;
	
	[type_ release];
	type_ = nil;	
	
	[poiId_ release];
	poiId_ = nil;	
	
}

#pragma mark -
#pragma mark Initialization

/**	
 * Object initializer
 *
 * @return An initialized instance
 */
- (id)init {
	
	if (self = [super init]) {
		
		geocodedAddress_ = [[GeocodedAddressData alloc] init];
		
	}
	
	return self;
	
}

#pragma mark -
#pragma mark Comparison

/*
 * Compares class objects by distance
 */
- (NSComparisonResult)compareByDistance:(ATMData *)atmData {
	
	NSComparisonResult result = NSOrderedSame;
	
    NSInteger selfDistance = self.geocodedAddress.geoFeatures.addressDistance;
    NSInteger anotherDistance = atmData.geocodedAddress.geoFeatures.addressDistance;
    
    if (selfDistance < anotherDistance) {
        
        result = NSOrderedAscending;
		
    } else if (selfDistance > anotherDistance) {
        
        result = NSOrderedDescending;
    }
	
	return result;    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	[super parserDidStartDocument:parser];
	
	xmlAnalysisCurrentValue_ = axeas_Nothing;
	[self clearData];
	
	geocodedAddress_ = [[GeocodedAddressData alloc] init];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
	
	NSString* lname = [elementName lowercaseString];
	
	if ([lname isEqualToString:@"type"]) {
		
		xmlAnalysisCurrentValue_ = axeas_AnalyzingType;
		
	} else if ([lname isEqualToString:@"telephone"]) {
		
		xmlAnalysisCurrentValue_ = axeas_AnalyzingTelephone;
		
	} else if ([lname isEqualToString:@"location"]) {
		
		xmlAnalysisCurrentValue_ = axeas_AnalyzingGeocodedAddress;
		geocodedAddress_.openingTag = @"location";
		[geocodedAddress_ setParentParseableObject:self];
		[parser setDelegate:geocodedAddress_];
		[geocodedAddress_ parserDidStartDocument: parser];
		
	} else if ([lname isEqualToString:@"status"]) {
		
		xmlAnalysisCurrentValue_ = axeas_AnalyzingStatus;
		
	} else if ([lname isEqualToString:@"network"]) {
		
		xmlAnalysisCurrentValue_ = axeas_AnalyzingNetwork;
		
	} else if ([lname isEqualToString:@"bankname"]) {
		
		xmlAnalysisCurrentValue_ = axeas_AnalyzingBankName;
		
	} else if ([lname isEqualToString:@"hours_mf"]) {
		
		xmlAnalysisCurrentValue_ = axeas_AnalyzingBankHoursMf;
		
	}else if ([lname isEqualToString:@"hours_sat"]) {
		
		xmlAnalysisCurrentValue_ = axeas_AnalyzingBankHoursSat;
		
	}else if ([lname isEqualToString:@"hours_sun"]) {
		
		xmlAnalysisCurrentValue_ = axeas_AnalyzingBankHoursSun;
		
	}else {
		
		xmlAnalysisCurrentValue_ = axeas_Nothing;
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	[super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];

	switch (xmlAnalysisCurrentValue_) {
			
		case axeas_AnalyzingType: {
			
			[type_ release];
			type_ = nil;
			type_ = [self.elementString copy];
			break;
			
		}
		case axeas_AnalyzingTelephone: {

			[telephone_ release];
			telephone_ = nil;
			telephone_ = [self.elementString copy];
			break;
			
		}
		case axeas_AnalyzingStatus: {

			[status_ release];
			status_ = nil;
			status_ = [self.elementString copy];
			break;
			
		}
		case axeas_AnalyzingNetwork: {

			[network_ release];
			network_ = nil;
			network_ = [self.elementString copy];
			break;
			
		}
		case axeas_AnalyzingBankName: {

			[bankName_ release];
			bankName_ = nil;
			bankName_ = [self.elementString copy];
			break;
			
		}
        case axeas_AnalyzingBankHoursMf: {
            
			[hoursMf_ release];
			hoursMf_ = nil;
			hoursMf_ = [self.elementString copy];
			break;
			
		}
        case axeas_AnalyzingBankHoursSat: {
            
			[hoursSat_ release];
			hoursSat_ = nil;
			hoursSat_ = [self.elementString copy];
			break;
			
		}
        case axeas_AnalyzingBankHoursSun: {
            
			[hoursSun_ release];
			hoursSun_ = nil;
			hoursSun_ = [self.elementString copy];
			break;
			
		}
		default: {
			
			break;
			
		}
			
	}
	
	xmlAnalysisCurrentValue_ = axeas_Nothing;
	
}

@end


#pragma mark -
#pragma mark Type definitions

/**
 * Enumeration for the state of the analysis
 */
typedef enum {
	
	aoaxeas_Nothing = 0,
	aoaxeas_AnalyzingArrayOfPOI
	
} ArrayOfATMXMLElementAnalyzerState;

#pragma mark -

@implementation ATMList

#pragma mark -
#pragma mark Properties

@synthesize atmList = atmList_;
@dynamic atmCount;

#pragma mark -
#pragma mark List methods

/*	
 * Adds an ATM at the end of the list
 */
- (void)addATM:(ATMData *)anATMData {
	
	if (anATMData != nil) {
		
		[ATMList_ addObject:anATMData];
		
	}
	
}

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
	
	[self clearData];
	
	[super dealloc];
	
}

/*
 * Clears the object data
 */
- (void)clearData {
	
	[ATMList_ release];
	ATMList_ = nil;
	
	[auxATM_ release];
	auxATM_ = nil;
	
}

#pragma mark -
#pragma mark View management

/**	
 * Object initializer
 *
 * @return An initialized instance
 */
- (id)init {
	
	if (self = [super init]) {
		
		ATMList_ = [[NSMutableArray alloc] init];
		
	}
	
	return self;
	
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Provides read access to the number of ATM stored
 *
 * @return The ATM count
 */
- (NSUInteger)atmCount {
	
	return [ATMList_ count];
	
}

#pragma mark -
#pragma mark Data access

/*
 * Returns the ATM located at the given position, or nil if position is not valid
 */
- (ATMData *)atmAtPostion:(NSUInteger)aPosition {
	
	ATMData *result = nil;
	
	if (aPosition < [ATMList_ count]) {
		
		result = [ATMList_ objectAtIndex: aPosition];
		
	}
	
	return result;
	
}

/*
 * Returns the ATM position inside the list
 */
- (NSUInteger)atmPosition:(ATMData *)anATMData {
	
	return [ATMList_ indexOfObject:anATMData];
	
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	[super parserDidStartDocument:parser];
	
	xmlAnalysisCurrentValue_ = aoaxeas_Nothing;
	[self clearData];
	
	ATMList_ = [[NSMutableArray alloc] init];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
	
	NSString* lname = [elementName lowercaseString];
	
	if (auxATM_ != nil) {
		
		[self addATM:auxATM_];
		[auxATM_ release];
		auxATM_ = nil;
		
	}
	
	if ([lname isEqualToString:@"atm"]) {
		
		xmlAnalysisCurrentValue_ = aoaxeas_AnalyzingArrayOfPOI;
		auxATM_ = [[ATMData alloc] init];
		auxATM_.openingTag = @"atm";
		[auxATM_ setParentParseableObject:self];
		[parser setDelegate:auxATM_];
		[auxATM_ parserDidStartDocument:parser];
		
	} else {
		
		xmlAnalysisCurrentValue_ = aoaxeas_Nothing;
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	[super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
	
	NSString* lname = [elementName lowercaseString];
	
	if ([lname isEqualToString:self.openingTag]) {
		
		if (auxATM_ != nil) {
			
			[self addATM:auxATM_];
			[auxATM_ release];
			auxATM_ = nil;
			
		}
		
	}
	
	xmlAnalysisCurrentValue_ = aoaxeas_Nothing;
	
}

@end
