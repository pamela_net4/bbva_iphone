/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "ParseableObject.h"


//Forward declarations
@class ExecutionResult;


/**
 * Contains information about operation result
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface OperationResult : ParseableObject {
	
@private
	
	/**
	 * Execution result of the operation
	 */
	ExecutionResult *result_;
	
	/**
	 * Data of the operation
	 */
	ParseableObject *parserDelegate_;
	
	/**
	 * Data detected
	 */
	BOOL dataElementFound_;
	
	/**
	 * Flag that indicate that requires data element
	 */
	BOOL requiresDataElement_;
	
}

/**
 * Provides readwrite access to the execution result
 */
@property (nonatomic, readonly, retain) ExecutionResult* result;

/**
 * Provides readwrite access to the execution result
 */
@property (nonatomic, readwrite) BOOL requiresDataElement;

/**
 * Initializes the object 
 *
 * @param theParserDelegate Delegate object that parses the Data tag
 */
- (id)initWithDelegate:(ParseableObject *)theParserDelegate;

@end
