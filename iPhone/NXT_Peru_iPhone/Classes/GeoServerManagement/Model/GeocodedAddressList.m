/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GeocodedAddressList.h"


#pragma mark -
#pragma mark Types definition

/**
 * Enumeration for the state of the analysis
 */
typedef enum {
	
	gpxeas_Nothing = 0,
	gpxeas_AnalyzingLatitude,
	gpxeas_AnalyzingLatitudeDegrees,
	gpxeas_AnalyzingLongitude,
	gpxeas_AnalyzingLongitudeDegrees
	
} GeoPositionXMLElementAnalyzerState;


#pragma mark -

@implementation GeoPositionData

#pragma mark -
#pragma mark Properties

@synthesize latitude = latitude_;
@synthesize longitude = longitude_;

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes a GeoPositionData located at 0N 0E
 *
 * @return The initialized GeoPostionData
 */
- (id)init {
	
	return [self initWithLatitude:0.0f andLongitude:0.0f];
	
}

/*
 * Designated initializer. Initializes a GeoPositionData with the information provider
 */
- (id)initWithLatitude:(float)aLatitude andLongitude:(float)aLongitude {
	
	if (self = [super init]) {
		
		latitude_ = aLatitude;
		longitude_ = aLongitude;
		
	}
	
	return self;
	
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	[super parserDidStartDocument:parser];
	
	xmlAnalysisCurrentValue_ = gpxeas_Nothing;
	
	latitude_ = 0.0f;
	longitude_ = 0.0f;
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
	
	NSString* lname = [elementName lowercaseString];
	
	if (xmlAnalysisCurrentValue_ == gpxeas_AnalyzingLatitude && [lname isEqualToString:@"degrees"]) {

		xmlAnalysisCurrentValue_ = gpxeas_AnalyzingLatitudeDegrees;
		
	} else if (xmlAnalysisCurrentValue_ == gpxeas_AnalyzingLongitude && [lname isEqualToString:@"degrees"]) {
		
		xmlAnalysisCurrentValue_ = gpxeas_AnalyzingLongitudeDegrees;
		
	} else {
		
		if ([lname isEqualToString:@"latitude"]) {
			
			xmlAnalysisCurrentValue_ = gpxeas_AnalyzingLatitude;
			
		} else if ([lname isEqualToString:@"longitude"]) {
			
			xmlAnalysisCurrentValue_ = gpxeas_AnalyzingLongitude;
			
		} else {
			
			xmlAnalysisCurrentValue_ = gpxeas_Nothing;
			
		}
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	[super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
	
	switch (xmlAnalysisCurrentValue_) {
			
		case gpxeas_AnalyzingLatitudeDegrees: {
			
			latitude_ = [self.elementString floatValue];
			break;
			
		}
			
		case gpxeas_AnalyzingLongitudeDegrees: {
			
			longitude_ = [self.elementString floatValue];
			break;
			
		}
			
		default: {
			
			break;
			
		}
			
	}
	
	xmlAnalysisCurrentValue_ = gpxeas_Nothing;
	
}

@end

#pragma mark -

/**
 * Enumeration for the state of the analysis
 */
typedef enum {
	
	axeas_Nothing = 0,
	axeas_AnalyzingAddress1,
	axeas_AnalyzingAddress2,
	axeas_AnalyzingDescription
	
} AddressXMLElementAnalyzerState;


#pragma mark -

@implementation AddressData

@synthesize addressFirstLine = addressFirstLine_;
@synthesize addressSecondLine = addressSecondLine_;
@synthesize addressDescription = addressDescription_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
	
	[self clearData];
	
	[super dealloc];
	
}

/*
 * Clears the data of the object
 */
- (void)clearData {
	
	[addressDescription_ release];
	addressDescription_ = nil;
	
	[addressSecondLine_ release];
	addressSecondLine_ = nil;
	
	[addressFirstLine_ release];
	addressFirstLine_ = nil;
	
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes an empty AddressData
 *
 * @return The initialized AddressData
 */
- (id)init {
	
	return [self initWithFirstLine:@"" andSecondLine:@""];
	
}

/*
 * Designated initializer. Initializes an AddressData instance with the provided data
 */
- (id)initWithFirstLine:(NSString *)anAddressFirstLine andSecondLine:(NSString *)anAddressSecondLine {
	
	if (self = [super init]) {
		
		addressFirstLine_ = [anAddressFirstLine copy];
		addressSecondLine_ = [anAddressSecondLine copy];
		addressDescription_ = nil;
		
	}
	
	return self;
	
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	[super parserDidStartDocument:parser];
	
	xmlAnalysisCurrentValue_ = axeas_Nothing;
	[self clearData];
	
	addressFirstLine_ = @"";
	addressSecondLine_ = @"";
	addressDescription_ = nil;
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
	
	NSString* lname = [elementName lowercaseString];
	
	if ([lname isEqualToString: @"address1"]) {
		
		xmlAnalysisCurrentValue_ = axeas_AnalyzingAddress1;
		
	} else if ([lname isEqualToString: @"address2"]) {
		
		xmlAnalysisCurrentValue_ = axeas_AnalyzingAddress2;
		
	} else if ([lname isEqualToString: @"description"]) {
		
		xmlAnalysisCurrentValue_ = axeas_AnalyzingDescription;
		
	} else {
		
		xmlAnalysisCurrentValue_ = axeas_Nothing;
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	[super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
	
    NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

	switch (xmlAnalysisCurrentValue_) {
			
		case axeas_AnalyzingAddress1: {
			
			[addressFirstLine_ release];
			addressFirstLine_ = nil;
			addressFirstLine_ = [elementString copy];
			break;
			
		}
		case axeas_AnalyzingAddress2: {

			[addressSecondLine_ release];
			addressSecondLine_ = nil;
			addressSecondLine_ = [elementString copy];
			break;
			
		}
		case axeas_AnalyzingDescription: {

			[addressDescription_ release];
			addressDescription_ = nil;
			addressDescription_ = [elementString copy];
			break;
		}
		default: {
			
			break;
			
		}
	}
	
	xmlAnalysisCurrentValue_ = axeas_Nothing;
	
}

@end


#pragma mark -

/**
 * Enumeration for the state of the analysis
 */
typedef enum {
	
	gfxeas_Nothing = 0,
	gfxeas_AnalyzingRoutingAvailable,
	gfxeas_AnalyzingDistance
	
} GeoFeaturesXMLElementAnalyzerState;

@implementation GeoFeaturesData

@synthesize routingAvailable = routingAvailable_;
@synthesize addressDistance = addressDistance_;

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a GeoFeaturesData instance
 *
 * @return The initialized GeoFeaturesData instance
 */
- (id)init {
	
	if (self = [super init]) {
		
		routingAvailable_ = false;
		addressDistance_ = -1;
		
	}
	
	return self;
	
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	[super parserDidStartDocument:parser];
	
	xmlAnalysisCurrentValue_ = gfxeas_Nothing;
	
	routingAvailable_ = false;
	addressDistance_ = -1;
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *) parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
	
	NSString* lname = [elementName lowercaseString];
	
	if ([lname isEqualToString: @"routingavailable"]) {
		
		xmlAnalysisCurrentValue_ = gfxeas_AnalyzingRoutingAvailable;
		
	} else if ([lname isEqualToString: @"distance"]) {
		
		xmlAnalysisCurrentValue_ = gfxeas_AnalyzingDistance;
		
	} else {
		
		xmlAnalysisCurrentValue_ = gfxeas_Nothing;
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void) parser: (NSXMLParser*) parser didEndElement: (NSString*) elementName namespaceURI: (NSString*) namespaceURI qualifiedName: (NSString*) qName {
	
	[super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
	
	switch (xmlAnalysisCurrentValue_) {
			
		case gfxeas_AnalyzingRoutingAvailable: {
			
			routingAvailable_ = [self.elementString boolValue];
			break;
			
		}
		case gfxeas_AnalyzingDistance: {
			
			addressDistance_ = [self.elementString integerValue];
			break;
			
		}
		default: {
			
			break;
			
		}
			
	}
	
	xmlAnalysisCurrentValue_ = gfxeas_Nothing;
	
}

@end

#pragma mark -

/**
 * Enumeration for the state of the analysis
 */
typedef enum {
	
	gcadxeas_Nothing = 0,
	gcadxeas_AnalyzingAddress,
	gcadxeas_AnalyzingPosition,
	gcadxeas_AnalyzingGeoFeatures
	
} GeocodedAddressXMLElementAnalyzerState;

@implementation GeocodedAddressData

@synthesize address = addressData_;
@synthesize geoPosition = geoPositionData_;
@synthesize geoFeatures = geoFeaturesData_;
@synthesize obtainedViaGPS = obtainedViaGPS_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
	
	[self clearData];
	
	[super dealloc];
	
}

/*
 * Clears the data of the object
 */
- (void)clearData {
	
	[geoFeaturesData_ release];
	geoFeaturesData_ = nil;
	
	[geoPositionData_ release];
	geoPositionData_ = nil;
	
	[addressData_ release];
	addressData_= nil;
	
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initialier. Initializes a GeocodedAddressData instance
 *
 * @return The initialized GeocodedAddressData instance
 */
- (id)init {
	
	if (self = [super init]) {
		
		addressData_ = [[AddressData alloc] init];
		geoPositionData_ = [[GeoPositionData alloc] init];
		geoFeaturesData_ = [[GeoFeaturesData alloc] init];
		
	}
	
	return self;
	
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	[super parserDidStartDocument:parser];
	
	xmlAnalysisCurrentValue_ = gcadxeas_Nothing;
	[self clearData];
	
	addressData_ = [[AddressData alloc] init];
	geoPositionData_ = [[GeoPositionData alloc] init];
	geoFeaturesData_ = [[GeoFeaturesData alloc] init];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
	
	NSString* lname = [elementName lowercaseString];
	
	if ([lname isEqualToString: @"address"]) {
		
		xmlAnalysisCurrentValue_ = gcadxeas_AnalyzingAddress;
		[addressData_ setParentParseableObject:self];
		addressData_.openingTag = @"address";
		[parser setDelegate:addressData_];
		[addressData_ parserDidStartDocument:parser];
		
	} else if ([lname isEqualToString: @"position"]) {
		
		xmlAnalysisCurrentValue_ = gcadxeas_AnalyzingPosition;
		[geoPositionData_ setParentParseableObject:self];
		geoPositionData_.openingTag = @"position";
		[parser setDelegate:geoPositionData_];
		[geoPositionData_ parserDidStartDocument:parser];
		
	} else if ([lname isEqualToString: @"geofeatures"]) {
		
		xmlAnalysisCurrentValue_ = gcadxeas_AnalyzingGeoFeatures;
		[geoFeaturesData_ setParentParseableObject:self];
		geoFeaturesData_.openingTag = @"geofeatures";
		[parser setDelegate:geoFeaturesData_];
		[geoFeaturesData_ parserDidStartDocument:parser];
		
	} else {
		
		xmlAnalysisCurrentValue_ = gcadxeas_Nothing;
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	[super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
	
	xmlAnalysisCurrentValue_ = gcadxeas_Nothing;
	
}

@end


#pragma mark -

/**
 * Enumeration for the state of the analysis
 */
typedef enum {
	
	aogaxeas_Nothing = 0,
	aogaxeas_AnalyzingGeocodedAddress
	
} ArrayOfGeocodedAddressXMLElementAnalyzerState;

@implementation GeocodedAddressList

@dynamic geocodedAddressCount;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
	
	[self clearData];
	[super dealloc];
	
}

/*
 * Clears the data of the object
 */
- (void)clearData {
	
	[geocodedAddressList_ release];
	geocodedAddressList_ = nil;
	
	[auxGeocodedAddress_ release];
	auxGeocodedAddress_ = nil;
	
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a GeocodedAddressList instance
 *
 * @return The initialized GeocodedAddressList insntace
 */
- (id)init {
	
	if (self = [super init]) {
		
		geocodedAddressList_ = [[NSMutableArray alloc] init];
		
	}
	
	return self;
	
}

#pragma mark -
#pragma mark List management

/*
 * Adds a geocoded address at the end of the list
 */
- (void)addGeocodedAddress:(GeocodedAddressData *)aGeocodedAddress {
	
	if (aGeocodedAddress != nil) {
		
		[geocodedAddressList_ addObject:aGeocodedAddress];
		
	}
	
}

#pragma mark -
#pragma mark Information access

/*
 * Provides read access to the number of geocoded addreses stored
 *
 * @return The number of geocoded addresses stored
 */
- (NSUInteger)geocodedAddressCount {
	
	return [geocodedAddressList_ count];
	
}

/*
 * Returns the geocoded address located at the given position, or nil if position is not valid
 */
- (GeocodedAddressData *)geocodedAddressAtPosition:(NSUInteger)aPosition {
	
	GeocodedAddressData* result = nil;
	
	if (aPosition < [geocodedAddressList_ count]) {
		
		result = [geocodedAddressList_ objectAtIndex:aPosition];
		
	}
	
	return result;
	
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	[super parserDidStartDocument:parser];
	
	xmlAnalysisCurrentValue_ = aogaxeas_Nothing;
	[self clearData];
	
	geocodedAddressList_ = [[NSMutableArray alloc] init];
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
	
	NSString* lname = [elementName lowercaseString];
	
	if (auxGeocodedAddress_ != nil) {
		
		[self addGeocodedAddress:auxGeocodedAddress_];
		[auxGeocodedAddress_ release];
		auxGeocodedAddress_ = nil;
		
	}		
	
	if ([lname isEqualToString: @"geocodedaddress"]) {
		
		xmlAnalysisCurrentValue_ = aogaxeas_AnalyzingGeocodedAddress;
		auxGeocodedAddress_ = [[GeocodedAddressData alloc] init];
		auxGeocodedAddress_.openingTag = @"geocodedaddress";
		[auxGeocodedAddress_ setParentParseableObject:self];
		[parser setDelegate:auxGeocodedAddress_];
		[auxGeocodedAddress_ parserDidStartDocument:parser];
		
	} else {
		
		xmlAnalysisCurrentValue_ = aogaxeas_Nothing;
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	[super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
	
	NSString* lname = [elementName lowercaseString];
	
	if ([lname isEqualToString:self.openingTag]) {
		
		if (auxGeocodedAddress_ != nil) {
			
			[self addGeocodedAddress:auxGeocodedAddress_];
			[auxGeocodedAddress_ release];
			auxGeocodedAddress_ = nil;
			
		}
		
	}
	
	xmlAnalysisCurrentValue_ = aogaxeas_Nothing;
	
}

@end
