/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "OperationResult.h"
#import "ExecutionResult.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
	
	orxeas_Nothing = 0,
	orxeas_AnalyzingResult,
	orxeas_AnalyzingData
	
} OperationResultXMLElementAnalyzerState;


#pragma mark -

@implementation OperationResult

#pragma mark -
#pragma mark Properties

@synthesize result = result_;
@synthesize requiresDataElement = requiresDataElement_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
	
	[result_ release];
	result_ = nil;
	
	[parserDelegate_ release];
	parserDelegate_ = nil;
	
	[super dealloc];
	
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Object initializer
 */
- (id)initWithDelegate:(ParseableObject *)theParserDelegate {
	
	if (self = [super init]) {
		
		result_  = [[ExecutionResult alloc] init];
		parserDelegate_ = [theParserDelegate retain];
		dataElementFound_ = NO;
		requiresDataElement_ = YES;
		
	}
	
	return self;
	
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	[super parserDidStartDocument:parser];
	
	xmlAnalysisCurrentValue_ = orxeas_Nothing;
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
	
	NSString* lname = [elementName lowercaseString];
	
	if ([lname isEqualToString: @"result"]) {
		
		xmlAnalysisCurrentValue_ = orxeas_AnalyzingResult;
		result_.openingTag = @"result";
		result_.parentParseableObject = self;
		[parser setDelegate:result_];
		[result_ parserDidStartDocument:parser];
		
	} else if ([lname isEqualToString: @"data"]) {
		
		dataElementFound_ = YES;
		xmlAnalysisCurrentValue_ = orxeas_AnalyzingData;
		parserDelegate_.openingTag = @"data";
		parserDelegate_.parentParseableObject = self;
		[parser setDelegate:parserDelegate_];
		[parserDelegate_ parserDidStartDocument:parser];
		
		
	} else {
		
		xmlAnalysisCurrentValue_ = orxeas_Nothing;
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	[super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];

	// If app must be updated ignores the condition
	if ((!dataElementFound_) && (requiresDataElement_) && (result_.code != ERROR_NEW_VERSION)) {
		
		result_.code = NO_DATA_WARNING;
		
	}
	
	xmlAnalysisCurrentValue_ = orxeas_Nothing;
	
}

@end