/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ParseableObject.h"


//Forward declarations
@class GeocodedAddressData;


/**
 * Contains data needed to describe an ATM
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ATMData : ParseableObject {
	
@private
	
	/**
	 * ATM id
	 */
	NSString *poiId_;
	
	/**
	 * ATM type
	 */
	NSString *type_;
	
	/**
	 * ATM associated telephone
	 */
	NSString *telephone_;

	/**
	 * ATM status (if any)
	 */
	NSString *status_;

	/**
	 * ATM network (if any)
	 */
	NSString *network_;
	
	/**
	 * ATM bank name (if any)
	 */
	NSString *bankName_;
    
    /**
	 * ATM bank hoursMf (if any)
	 */
	NSString *hoursMf_;
    
    /**
	 * ATM bank hoursSat (if any)
	 */
	NSString *hoursSat_;
    
    /**
	 * ATM bank hoursSun (if any)
	 */
	NSString *hoursSun_;
    
    /**
     * ATM Bank category
     */
    NSInteger category_;
	
	/**
	 * ATM geocoded address
	 */
	GeocodedAddressData *geocodedAddress_;
	
}

/**
 * Provides read/write access to the ATM id
 */
@property (readwrite, copy) NSString* poiId;

/**
 * Provides read/write access to the ATM type
 */
@property (readwrite, copy) NSString* type;

/**
 * Provides read/write access to the ATM associated telephone
 */
@property (readwrite, copy) NSString* telephone;

/**
 * Provides read/write access to the ATM status
 */
@property (readwrite, copy) NSString* status;

/**
 * Provides read/write access to the ATM network
 */
@property (readwrite, copy) NSString* network;

/**
 * Provides read/write access to the ATM bank name
 */
@property (readwrite, copy) NSString* bankName;

/**
 * Provides read/write access to the ATM bank hoursMf
 */
@property (readwrite, copy) NSString *hoursMf;

/**
 * Provides read/write access to the ATM bank hoursSat
 */
@property (readwrite, copy) NSString *hoursSat;

/**
 * Provides read/write access to the ATM bank hoursSun
 */
@property (readwrite, copy) NSString *hoursSun;

/**
 * Provides read/write access to the ATM bank category
 */
@property (readwrite, assign) NSInteger category;


/**
 * Provides read access to the ATM geocoded address
 */
@property (readonly, assign) GeocodedAddressData* geocodedAddress;

/**
 * Clears the object data
 */
- (void)clearData;

/**
 * Compares class objects by distance
 *
 * @param atmData The object to compare receiver with
 */
- (NSComparisonResult)compareByDistance:(ATMData *)atmData;

@end


/**
 * List of ATM information
 */
@interface ATMList : ParseableObject {
	
@private
	
	/**
	 * Array containing the ATM information
	 */
	NSMutableArray *ATMList_;
	
	/**
	 * Auxiliar ATM object for parsing
	 */
	ATMData *auxATM_;
	
}

/**
 * Provides read-only access to atmList.
 */
@property (nonatomic, readonly, retain) NSMutableArray *atmList;

/**	
 * Provides read access to the number of ATM stored
 */
@property (readonly) NSUInteger atmCount;

/**
 * Returns the ATM located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the ATM is located
 *
 * @return ATM located at the given position, or nil if position is not valid
 */
- (ATMData *)atmAtPostion:(NSUInteger)aPosition;

/**
 * Returns the ATM position inside the list
 *
 * @param  anATMData ATMData object which position we are looking for
 *
 * @return ATM position inside the list
 */
- (NSUInteger)atmPosition:(ATMData *)anATMData;

/**
 * Adds an ATM at the end of the list
 *
 * @param  anATM The ATM to add
 */
- (void)addATM:(ATMData *)anATM;

@end
