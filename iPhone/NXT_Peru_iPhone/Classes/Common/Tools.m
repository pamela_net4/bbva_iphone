/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "Tools.h"

#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>

#import "BankAccount.h"
#import "BBVADeviceUID.h"
#import "Constants.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "StringKeys.h"
#import "TransactionsFilterModalViewController.h"


/**
 * Auxiliar ASCII table
 */
static int asciitable[128] = {
	99,99,99,99, 99,99,99,99, 99,99,99,99, 99,99,99,99, 
	99,99,99,99, 99,99,99,99, 99,99,99,99, 99,99,99,99,
	99,99,99,99, 99,99,99,99, 99,99,99,99, 99,99,99,99,
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 99,99, 99,99,99,99,	// 0..9
	99,10,11,12, 13,14,15,99, 99,99,99,99, 99,99,99,99, // a..f
	99,99,99,99, 99,99,99,99, 99,99,99,99, 99,99,99,99,
	99,10,11,12, 13,14,15,99, 99,99,99,99, 99,99,99,99, // A..F
	99,99,99,99, 99,99,99,99, 99,99,99,99, 99,99,99,99
};


#pragma mark -

@interface Tools() 

/**
 * Fill the target array with integers from source string
 */
+ (void)fillFromString:(NSString *)aString toDigitsArray:(NSMutableArray *)anArray andSize:(NSInteger)aSize;

/**
 * Fill the target array with integers from source string
 */
+ (NSInteger)calculateWeightWithDigitsArray:(NSArray *)aDigitsArray andWeightsArray:(NSArray *)aWeightsArray;

/**
 * Calculate the control digit
 */
+ (NSInteger)calculateControlDigit:(NSInteger)aWeight;

/**
 * Creates the currency symbol dictionary if nil
 */
+ (void)createCurrencySymbolDictionary;

/**
 * Does the DES given operation over the given data
 *
 * @param enc data to operate over
 * @param operation to do over the data
 * @return the data
 */
+ (NSData *)DES:(NSData *)enc operation:(CCOperation)operation;

/**
 * Convert hexadecimal value to binary data
 *
 * @param hex to convert on binary data
 * @return Data converted
 */
+ (NSData *)hex2raw:(NSData *)hex;

/**
 * Convert binary data to hexadecimal value
 *
 * @param data to convert on hexadecimal value
 * @return Data as string value
 */
+ (NSString *)raw2hex:(NSData *)data;

/**
 * Returns the date formatter initialized
 *
 * @return The date formatter initialized
 * @private
 */
+ (NSDateFormatter *)dateFormatter;

/**
 * Returns the long format (dd/MM/yyyy) date formatter initialized.
 *
 * @return The long format (dd/MM/yyyy) date formatter initialized.
 * @private
 */
+ (NSDateFormatter *)longFormatDateFormatter;

/**
 * Returns the no separator format (yyyyMMdd) date formatter initialized.
 *
 * @return The no separator format (yyyyMMdd) date formatter initialized.
 * @private
 */
+ (NSDateFormatter *)noSeparatorDateFormatter;

/**
 * Returns the calendar initialized
 *
 * @return The calendar initialized
 * @private
 */
+ (NSCalendar *)calendar;

@end


#pragma mark -

@implementation Tools

#pragma mark -
#pragma mark Static attributes

/**
 * Contains the translation from the server currency symbol to the client currency symbol for selected currencies
 */
static NSMutableDictionary *currencySymbolsDict_ = nil;

/**
 * Number formatter.
 */
static NSNumberFormatter *serverNumberFormatter_ = nil;

/**
 * Date formatter.
 */
static NSDateFormatter *dateFormatter_ = nil;

/**
 * Long format date formatter.
 */
static NSDateFormatter *longFormatDateFormatter_ = nil;

/**
 * No separator format date formatter.
 */
static NSDateFormatter *noSeparatorFormatDateFormatter_ = nil;

/**
 * Calendar
 */
static NSCalendar *calendar_ = nil;

/**
 * Amount to local string formatter.
 */
static NSNumberFormatter *amountToLocalStringFormatter_ = nil;

#pragma mark -
#pragma mark Application information

/*
 * Returns the bundle version
 */
+ (NSString *)bundleVersion {
    
    NSString *result = nil;
    
    NSBundle *bundle = [NSBundle mainBundle];
    result = [bundle objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    
    return result;
    
}

#pragma mark -
#pragma mark String utility functions

/**
 * Returns the number formatter used for number conversion from strings.
 *
 * @return The number formatter.
 */
+ (NSNumberFormatter *)serverNumberFormatter {
    if( !serverNumberFormatter_ ) {
        serverNumberFormatter_ = [[NSNumberFormatter alloc] init];
        [serverNumberFormatter_ setDecimalSeparator:SERVER_NUMBER_DECIMAL_SEPARATOR];
        [serverNumberFormatter_ setGroupingSeparator:SERVER_NUMBER_GROUPING_SEPARATOR];
        [serverNumberFormatter_ setNumberStyle:NSNumberFormatterDecimalStyle];
        [serverNumberFormatter_ setGeneratesDecimalNumbers:YES];
    }
    return serverNumberFormatter_;
}

/*
 * Extracts a decimal number from the given server string
 */
+ (NSDecimalNumber *)decimalFromServerString:(NSString *)aString {
    return (NSDecimalNumber *)[[Tools serverNumberFormatter] numberFromString:aString];
}


/*
 * Converts a decimal number into a CGFloat. When decimal number is not a number, zero is assumed
 */
+ (CGFloat)convertDecimalNumber:(NSDecimalNumber *)aDecimalNumber {
    
    CGFloat result = 0.0f;
    
    if ([aDecimalNumber compare:[NSDecimalNumber notANumber]] != NSOrderedSame) {
        
        result = (CGFloat)[aDecimalNumber doubleValue];
        
    }
    
    return result;
    
}

/*
 * Converts username into a formatted username
 */
+ (NSString *)formatUsername:(NSString *)aUsername {
    
	NSString *result = nil;
	if (aUsername != nil) {
        
		NSString *username = [aUsername uppercaseString];
		NSUInteger usernameLength = [aUsername length];
		NSUInteger usernameLast = usernameLength - 1;
        
		NSCharacterSet *letters = [NSCharacterSet uppercaseLetterCharacterSet];
		unichar nifCharacter = [username characterAtIndex:usernameLast];
		if ([letters characterIsMember:nifCharacter]) {
			if (usernameLength <= 10) {
				NSString *nif = [username substringToIndex:usernameLast];
				NSInteger nifValue = [nif integerValue];
				if (nifValue != 0) {
					NSInteger nifIndex = nifValue % 23;
					if ((nifIndex >= 0) && (nifIndex < 23)) {
						NSString *validNIFCharacters = @"TRWAGMYFPDXBNJZSQVHLCKE";
						unichar validNIFCharacter = [validNIFCharacters characterAtIndex:nifIndex];
						if (validNIFCharacter == nifCharacter) {
							result = @"0019-";
							NSUInteger zeros = 10 - usernameLength;
							for (NSUInteger i = 0; i < zeros; i++) {
								result = [result stringByAppendingString:@"0"];
							}
							result = [result stringByAppendingString:username];
						}
					}
				}
			}
		} else if (usernameLength == 9) {
			NSInteger usernameValue = [username integerValue];
			if (usernameValue != 0) {
				NSString *card = @"404134";
				card = [card stringByAppendingString:username];
				NSUInteger digits = [card length];
				NSInteger even = 0;
				NSInteger odd = 0;
				NSInteger code = 0;
				NSInteger evenTotal = 0;
				NSInteger oddTotal = 0;
				NSInteger oddSingleTotal = 0;
				NSInteger value;
				NSRange range;
				range.location = 0;
				range.length = 1;
				for (NSUInteger i = 0; i < digits; i++) {
					range.location = i;
					value = [[card substringWithRange:range] integerValue];
					if ((i % 2) != 0) {
						even = value;
						evenTotal += even;
					} else {
						odd = value * 2;
						if (odd > 9) {
							oddTotal += (odd / 10) + (odd % 10);
						} else {
							oddSingleTotal += odd;
						}
					}
				}
				code = ((evenTotal + oddTotal + oddSingleTotal) % 10);
				if (code > 0) {
					code = 10 - code;
				}
				result = [card stringByAppendingFormat:@"%d", code];
			}
		} else if ((usernameLength == 16) || (usernameLength == 7)) {
			result = username;
		}
		
	}
    
    return result;
    
}

/*
 * Removes the not numeric characters from a string.
 */
+ (NSString *)stringByRemovingNotNumericCharacters:(NSString *)string {
    
    NSCharacterSet *invalidCharacters = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    
    NSString *result = [[string componentsSeparatedByCharactersInSet:invalidCharacters] componentsJoinedByString:@""];
    
    return result;
    
}

/*
 * Extracts a mobile number like string from annother string. To to this, not numeric characters are removed from the string, and it is set to as much as 9 characters
 * from the last characters (that is, initial characters will be removed until a leght equal or smaller that 9 is achieved).
 */
+ (NSString *)mobileNumberLikeFromString:(NSString *)string {
    
    NSString *result = [Tools stringByRemovingNotNumericCharacters:string];
    NSUInteger stringLength = [result length];
    
    if (stringLength > 9) {
        
        result = [result substringFromIndex:(stringLength - 9)];
        
    }
    
    return result;
    
    
}

/*
 * Checks whether a string is a valid mobile phone number. The string must begin with a 9 digit, has only digits, and a length of 9.
 */
+ (BOOL)isValidMobilePhoneNumberString:(NSString *)string {
    
    BOOL result = NO;
    
    if ([string length] == 9) {
        
        unichar firstCharacter = [string characterAtIndex:0];
        
        if (firstCharacter == '9') {
            
            NSScanner *scanner = [NSScanner scannerWithString:string];
            
            if (([scanner scanInteger:nil]) && ([scanner isAtEnd])) {
                
                result = YES;
                
            }
            
        }
        
    }
    
    return result;
    
}

+(BOOL)isValidEmail:(NSString*)email{
    
    if(email==nil || [email length]==0) return NO;
    
    BOOL result = NO;
        
    NSArray *atSeparetedStrings = [email componentsSeparatedByString:@"@"];
    NSUInteger atSeparetedStringsCount = [atSeparetedStrings count];
    
    if (atSeparetedStringsCount == 2) {
        
        NSString *firstString = [atSeparetedStrings objectAtIndex:0];
        
        if ([firstString length] > 0) {
            
            NSString *otherString = [atSeparetedStrings objectAtIndex:1];
            NSRange dotRange = [otherString rangeOfString:@"."];
            NSUInteger dotLocation = dotRange.location;
            
            if( !(((dotLocation > 0) && (dotLocation != NSNotFound)) && (dotLocation < ([otherString length] - 1))) ) {
                
                result = NO;
                
            }
            else{
                result = YES;
            }
            
        }
    }
    return result;
}

/*
 * Returns an NSString not nil equivalent to the provided string. When the input string is nil, "" is returned
 */
+ (NSString *)notNilString:(NSString *)string {
    
    return (string == nil) ? @"" : string;
    
}

/*
 * Returns an NSSring not nil equivalent by default to the provided string. When the input string is nil, default is returned.
 */
+ (NSString *)notNilString:(NSString *)string byDefaylt:(NSString *)defaultString {
    
    return ((string == nil) || ([string length] == 0)) ? defaultString : string;
    
}

/*
 * Returns an NSSring Capitalizing just the first word.  When the input string is nil, default is returned.
 */
+ (NSString *)capitalizeFirstWordOnly:(NSString *)defaultString {
    
    NSString *result = defaultString;
    
    if (defaultString != nil && [defaultString length] != 0) {
        result = [defaultString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[defaultString substringToIndex:1] uppercaseString]];
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Business logic and formatting

/*
 * Returns an amount formated with comma as decimal separator with the dot as decimal separator
 */
+ (NSString *)formatAmountWithDotDecimalSeparator:(NSString *)amountString {
    NSLocale* locale = [NSLocale currentLocale];
    NSString* decimalSeparator = [locale objectForKey: NSLocaleDecimalSeparator];
    NSString* groupingSeparator = [locale objectForKey: NSLocaleGroupingSeparator];
    NSString* result = nil;  
    
    result = [amountString stringByReplacingOccurrencesOfString:decimalSeparator withString:@"_"];
    result = [result stringByReplacingOccurrencesOfString:groupingSeparator withString:@""];
    result = [result stringByReplacingOccurrencesOfString:@"_" withString:@"."];
    
    BOOL hasCurrency = ![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[result characterAtIndex:result.length - 1]];
    
    if (result.length > 2 &&  hasCurrency) {
        
        result = [result substringToIndex:result.length - 1];
    }
    
    return ([result length] > 0 ? result : @"");
}

/*
 * Formats an amount, including the currency symbol provided
 */
+ (NSString *)formatAmount:(NSDecimalNumber *)anAmount withCurrency:(NSString *)aCurrency currenctyPrecedesAmount:(BOOL)aCurrencyPreceder
              decimalCount:(NSInteger)aDecimalCount {
    
    NSString *currencySymbol = nil;
    
    if (aCurrency != nil && [aCurrency length] > 0) {
        currencySymbol = [NSString stringWithFormat:@"%@ ",[Tools translateServerCurrencySymbolIntoClientCurrencySymbol:aCurrency]];
    } else {
        currencySymbol = [NSString stringWithFormat:@"%@ ",[Tools mainCurrencySymbol]];
    }
        
    NSMutableString *result = [NSMutableString stringWithString:@""];
    
    if (aCurrencyPreceder == YES && [currencySymbol length] > 0) {
        
        [result appendString:currencySymbol];
        
    }
    
    [result appendString:[Tools formatAmount:anAmount
                            withDecimalCount:aDecimalCount
                                    plusSign:NO]];
    
    if ((aCurrencyPreceder == NO) && ([currencySymbol length] > 0) && [result length] > 0) {
        
        [result appendString:currencySymbol];
        
    }
    
    return ([result length] > 0 ? result : @"");
    
}

/*
 * Formats an amount
 */
+ (NSString *)formatAmount:(NSDecimalNumber *)anAmount
          withDecimalCount:(NSInteger)aDecimalCount
                  plusSign:(BOOL)plusSign {
    
    NSString *result = nil;
    
    if (anAmount != nil) {
            
        //NSLocale *locale = [NSLocale currentLocale];
        NSString *decimalSeparator = OUTPUT_NUMBER_DECIMAL_SEPARATOR;//[locale objectForKey:NSLocaleDecimalSeparator];
        NSString *groupingSeparator = OUTPUT_NUMBER_GROUPING_SEPARATOR;// [locale objectForKey:NSLocaleGroupingSeparator];
        NSNumberFormatter *numberFormater = [[[NSNumberFormatter alloc] init] autorelease];
        [numberFormater setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [numberFormater setDecimalSeparator:decimalSeparator];
        [numberFormater setUsesGroupingSeparator:YES];
        [numberFormater setGroupingSize:3];
        [numberFormater setGroupingSeparator:groupingSeparator];
        [numberFormater setMinimumFractionDigits:aDecimalCount];
        [numberFormater setMaximumFractionDigits:aDecimalCount];
        [numberFormater setMinimumIntegerDigits:1];
        
        result = [numberFormater stringFromNumber:anAmount];
        
        if ((plusSign) && ([anAmount compare:[NSDecimalNumber zero]] != NSOrderedAscending)) {
         
            result = [NSString stringWithFormat:@"+%@", result];
            
        }

    }
    
    return ([result length] > 0) ? result : @"";
    
}

/**
 * Formats an amount
 *
 * @param anAmount The decimal number representing the amount to format
 * @return The formated amount
 */
+ (NSString *)formatAmount:(NSDecimalNumber *)anAmount {
    return [Tools formatAmount:anAmount withCurrency:nil currenctyPrecedesAmount:NO decimalCount:OUTPUT_NUMBER_DECIMAL_COUNT];
}

/**
 * Formats an amount, including the currency symbol provided
 *
 * @param anAmount The decimal number representing the amount to format
 * @param aCurrency The currency symbol
 * @return The formated amount
 */
+ (NSString *)formatAmount:(NSDecimalNumber *)anAmount withCurrency:(NSString *)aCurrency {
    //    BOOL precedesFlag;
    //    
    //    if ([aCurrency isEqualToString:CURRENCY_EUR_LITERAL]) {
    //        precedesFlag = NO;
    //    } else {
    //        precedesFlag = YES;
    //    }
    //    
    return [Tools formatAmount:anAmount withCurrency:aCurrency currenctyPrecedesAmount:YES decimalCount:OUTPUT_NUMBER_DECIMAL_COUNT];
}

/*
 * Formats an amount, including the currency symbol provided and a + symbol if necessary
 */
+ (NSString *)formatAmount:(NSDecimalNumber *)anAmount withCurrency:(NSString *)aCurrency decimalCount:(NSInteger)decimalCount andPlusSymbol:(BOOL)flag {
    NSString *result = [self formatAmount:anAmount withCurrency:aCurrency currenctyPrecedesAmount:NO decimalCount:decimalCount];;
    
    if (flag) {
        
        NSRange range = [result rangeOfString:@"-"];
        if (range.length == 0) {
            result = [NSString stringWithFormat:@"+%@", result];
        }
        
    }
    
    return ([result length] > 1 ? result : @"");
}

/*
 * Obfuscates an account number
 */
+ (NSString *)obfuscateAccountNumber:(NSString *)anAccountNumber {
    
    NSInteger accountNumberLength = [anAccountNumber length];
    NSString *substring = nil;
    
    if (accountNumberLength > 0) {
        
        if (accountNumberLength < 4) {
            
            substring = [anAccountNumber substringFromIndex:(accountNumberLength - 1)];
            
        } else {
            
            substring = [anAccountNumber substringFromIndex:(accountNumberLength - 4)];
            
        }
        
    }
    
    if (substring == nil) {
        return @"";
    }
    
    return [NSString stringWithFormat:@"*%@", substring];
    
}

/*
 * Obfuscates a card number
 */
+ (NSString *)obfuscateCardNumber:(NSString *)aCardNumber {
    
    NSInteger cardNumberLength = [aCardNumber length];
    NSString *substring = nil;
    
    if (cardNumberLength > 0) {
        
        if (cardNumberLength < 4) {
            
            substring = [aCardNumber substringFromIndex:(cardNumberLength - 1)];
            
        } else {
            
            substring = [aCardNumber substringFromIndex:(cardNumberLength - 4)];
            
        }
        
    }
    
    return [NSString stringWithFormat:@"*%@", substring];
    
}
+(NSString*)formatCardNumber:(NSString*)aCardNumber
{
    int len=0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    while( (len+4)<[aCardNumber length]) {
        [array addObject:[aCardNumber substringWithRange:NSMakeRange(len,4)]];
        len+=4;
    }
    [array addObject:[aCardNumber substringFromIndex:len]];

    return [NSString stringWithFormat:@"%@-%@-%@-%@",[array objectAtIndex:0],[array objectAtIndex:1],[array objectAtIndex:2],[array objectAtIndex:3]];
}

/*
 * Obfuscates a username
 */
+ (NSString *)obfuscateUserName:(NSString *)aUserName {
    
    NSInteger userNameLength = [aUserName length];
    NSString *substring = nil;
    
    if (userNameLength > 0) {
        
        if (userNameLength < 4) {
            
            substring = [aUserName substringFromIndex:(userNameLength - 1)];
            
        } else {
            
            substring = [aUserName substringFromIndex:(userNameLength - 4)];
            
        }
        
    }
    
    NSString *result = @"";
    
    if ([substring length] > 0) {
        
        result = [NSString stringWithFormat:@"*%@", substring];
        
    }
    
    return result;
    
}

/*
 * Obfuscates a username
 */
+ (NSString *)obfuscateUserNameLarge:(NSString *)aUserName {
    
    NSInteger userNameLength = [aUserName length];
    NSString *substring = nil;
    
    if (userNameLength > 0) {
        
        if (userNameLength < 4) {
            
            substring = [aUserName substringFromIndex:(userNameLength - 1)];
            
        } else {
            
            substring = [aUserName substringFromIndex:(userNameLength - 4)];
            
        }
        
    }
    
    NSString *result = @"";
    
    if ([substring length] > 0) {
        
        result = [NSString stringWithFormat:@"************%@", substring];
        
    }
    
    return result;
    
}

/*
 * Fill the target array with integers from source string
 */
+ (void)fillFromString:(NSString *)aString toDigitsArray:(NSMutableArray *)anArray andSize:(NSInteger)aSize {
    
	NSUInteger delta = aSize - [aString length];
	NSUInteger pos;
	for (NSUInteger i = 0; i < aSize; i++) {
		if (i < delta) {
			NSNumber *number = [[NSNumber alloc] initWithInt:0];
			[anArray insertObject:number atIndex:i];
			[number release];
		} else {
			pos = i - delta;
			NSString *text = [aString substringWithRange:NSMakeRange(pos, 1)];
			NSNumber *number = [[NSNumber alloc] initWithInteger:[text integerValue]];
			[anArray insertObject:number atIndex:i];
			[number release];
		}
	}
    
}

/*
 * Fill the target array with integers from source string
 */
+ (NSInteger)calculateWeightWithDigitsArray:(NSArray *)aDigitsArray andWeightsArray:(NSArray *)aWeightsArray {
    
	NSInteger result = 0;
	NSNumber *digit;
	NSNumber *weight;
	NSUInteger size = [aDigitsArray count];
	for (NSUInteger i = 0; i < size; i++) {
		digit = [aDigitsArray objectAtIndex:i];
		weight = [aWeightsArray objectAtIndex:i];
		result += ([digit integerValue] * [weight integerValue]);
	}
    return result;
    
}

/*
 * Calculate the control digit
 */
+ (NSInteger)calculateControlDigit:(NSInteger)aWeight {
    
	NSInteger mod = (aWeight % 11);
	NSInteger result = ((mod == 0) ? 11 : ((mod == 1) ? 10 : mod)); 
    return 11 - result;
    
}

/*
 * Provides access to the amount to local string formatter.
 */
+ (NSNumberFormatter *)amountToLocalStringFormatter {
    
    if (amountToLocalStringFormatter_ == nil) {
        
        @synchronized([Tools class]) {
            
            if (amountToLocalStringFormatter_ == nil) {
                
                NSLocale *currentLocale = [NSLocale currentLocale];
                NSString *decimalSeparator = [currentLocale objectForKey:NSLocaleDecimalSeparator];
                NSString *groupingSeparator = [currentLocale objectForKey:NSLocaleGroupingSeparator];
                
                amountToLocalStringFormatter_ = [[NSNumberFormatter alloc] init];
                [amountToLocalStringFormatter_ setFormatterBehavior:NSNumberFormatterBehavior10_4];
                [amountToLocalStringFormatter_ setDecimalSeparator:decimalSeparator];
                [amountToLocalStringFormatter_ setGroupingSeparator:groupingSeparator];
                [amountToLocalStringFormatter_ setGroupingSize:3];
                [amountToLocalStringFormatter_ setUsesGroupingSeparator:YES];
                [amountToLocalStringFormatter_ setMinimumFractionDigits:2];
                [amountToLocalStringFormatter_ setMaximumFractionDigits:2];
                [amountToLocalStringFormatter_ setMinimumIntegerDigits:1];
                
            }
            
        }
        
    }
    
    return amountToLocalStringFormatter_;
    
}

/*
 * Returns a decimal number from a local string.
 */
+ (NSDecimalNumber *)decimalNumberFromLocalString:(NSString *)string {
    
    NSDecimalNumber *result = [NSDecimalNumber zero];
    
    if ([string length] > 0) {
        
        NSNumberFormatter *formatter = [Tools amountToLocalStringFormatter];
        NSNumber *number = [formatter numberFromString:string];
        NSDecimal decimal = number.decimalValue;
        result = [NSDecimalNumber decimalNumberWithDecimal:decimal];
        
        if (result == nil) {
            
            result = [NSDecimalNumber zero];
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Graphic elements

/*
 * Shows an error alert view
 */
+ (void)showErrorWithMessage:(NSString *)anErrorMessage {
    
    if ([anErrorMessage length] > 0) {
        
        NSString *errorTitle = NSLocalizedString(INFO_MESSAGE_TITLE_KEY, nil);
        
        [Tools showAlertWithMessage:anErrorMessage title:errorTitle];
        
    }
    
}

/*
 * Shows an info alert view
 */
+ (void)showInfoWithMessage:(NSString *)aInfoMessage {
    
    if ([aInfoMessage length] > 0) {
        
        NSString *infoTitle = NSLocalizedString(INFO_MESSAGE_TITLE_KEY, nil);
        
        [Tools showAlertWithMessage:aInfoMessage title:infoTitle];
        
    }
    
}

/*
 * Shows a message to the user inside an alert view, using a given title
 */
+ (void)showAlertWithMessage:(NSString *)aMessage title:(NSString *)aTitle {

    if ([aMessage length] > 0) {
        
        NSString *buttonText = NSLocalizedString(OK_TEXT_KEY, nil);
        
        UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:aTitle message:aMessage delegate:nil cancelButtonTitle:buttonText otherButtonTitles:nil] autorelease];
        [alertView show];
        
    }
    
}

/*
 * Enables or disables a table scrolling depending on whether its content height is greater than its own height
 */
+ (void)checkTableScrolling:(UITableView *)aTableView {
    
    NSInteger sections = [aTableView numberOfSections];
    
    CGRect totalRect = CGRectZero;
    
    for (NSInteger count = 0; count < sections; count++) {
        
        CGRect sectionRect = [aTableView rectForSection:count];
        totalRect = CGRectUnion(totalRect, sectionRect);
        
    }
    
    if (CGRectGetHeight(totalRect) > CGRectGetHeight(aTableView.frame)) {
        
        aTableView.scrollEnabled = YES;
        
    } else {
        
        aTableView.scrollEnabled = NO;
        
    }
    
}

/*
 * Sets a given text to a label, aligning the text vertically to the label top. Resizes the label to fit the text in height
 */
+ (void)setText:(NSString *)aText toLabel:(UILabel *)aLabel withMaxLines:(NSInteger)aMaxLines {
    
	UIFont *fontUsed = aLabel.font;
	CGRect frame = aLabel.frame;
	CGSize maxSize = CGSizeMake(frame.size.width, aMaxLines * (fontUsed.pointSize + 10.0f));
	
	CGSize textSize = [aText sizeWithFont:fontUsed constrainedToSize:maxSize];
	
	aLabel.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, textSize.height);
	aLabel.text = aText;
    
}

#pragma mark -
#pragma mark Numeric operations

/*
 * Returns the nearest value with only one digit which is not zero. For example, a 24 input returns 20, a 25 input returns 30, a 0.043 input returns 0.04 and a 0.045 input returns 0.05.
 * It assumes the input value is positive
 */
+ (double)roundToOneNonZeroDigit:(double)anInput {

    double result = 0.0f;
    
    if (anInput > 0.0f) {
            
        if (anInput < 1.0f) {
            
            double nextValue = 10.0f * anInput;
            NSInteger exponent = -1;
            
            while (nextValue < 1.0f) {
                
                nextValue *= 10.0f;
                exponent -= 1;
                
            }
            
            result = round(nextValue) * pow(10.0f, exponent);
            
        } else if (anInput < 10.0f) {
            
            result = round(anInput);
            
        } else {
            
            NSUInteger previousValue = (NSUInteger)anInput;
            NSUInteger nextValue = previousValue / 10;
            NSUInteger tenPower = 10;
            
            while ((nextValue / 10) >= 1) {
                
                previousValue = nextValue;
                nextValue = previousValue / 10;
                tenPower *= 10;
                
            }
            
            result = round((double)previousValue / 10.0f) * (double)tenPower;
            
        }
        
    }

    return result;
    
}

#pragma mark -
#pragma mark Date conversion

/*
 * Returns the current date and time but in UTC. That means that date/time 2011/01/01 12:00:00 UTC+2 will be converted to 2011/01/01 12:00:00 UTC+0 (not 2011/01/01 10:00:00 UTC+0) and
 * 2011/01/01 12:00:00 UTC-5 will be converted to 2011/01/01 12:00:00 UTC+0 (not (2011/01/01 17:00:00 UTC+0)
 */
+ (NSDate *)now {
    
    NSDate *result = [NSDate date];
    NSCalendar *currentLocaleCalendar = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    [currentLocaleCalendar setLocale:[NSLocale currentLocale]];
    NSDateComponents *dateComponents = [currentLocaleCalendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit
                                                                fromDate:result];
    
    NSCalendar *calendarForUTC = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    [calendarForUTC setLocale:[NSLocale currentLocale]];
    [calendarForUTC setTimeZone:[NSTimeZone timeZoneWithName:0]];
    result = [calendarForUTC dateFromComponents:dateComponents];
    
    return result;
    
}

/*
 * Returns a string from a given NSDate
 */
+ (NSString *) getStringFromDate: (NSDate *) aDate {
	NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
	[dateFormatter setDateFormat:DATE_OUT_FORMAT];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	NSString* dateAsString = [dateFormatter stringFromDate:aDate];
    
	return ([dateAsString length] > 0 ? dateAsString : @"");
}

/*
 * Returns a string from a given NSDate in current locale time zone with the specified format
 */
+ (NSString *)stringFromCurrentLocaleDate:(NSDate *)date
                               withFormat:(NSString *)dateFormat {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [NSLocale currentLocale];
    
    [dateFormatter setLocale:locale];
	[dateFormatter setDateFormat:dateFormat];
	NSString *dateAsString = [dateFormatter stringFromDate:date];
    
    [dateFormatter release];
    
	return ([dateAsString length] > 0 ? dateAsString : @"");

}

/*
 * Returns a string from a given NSDate in UTC time zone with the specified format
 */
+ (NSString *)stringFromUTCDate:(NSDate *)date
                     withFormat:(NSString *)dateFormat {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [NSLocale currentLocale];
    
    [dateFormatter setLocale:locale];
	[dateFormatter setDateFormat:dateFormat];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	NSString *dateAsString = [dateFormatter stringFromDate:date];
    
    [dateFormatter release];
    
	return ([dateAsString length] > 0 ? dateAsString : @"");
    
}

/*
 * Returns a string with hour from a given NSDate
 */
+ (NSString *)stringWithHourWithCurrentLocaleFromDate: (NSDate *) aDate {
    
    return [self stringFromCurrentLocaleDate:aDate withFormat:DATE_HOUR_OUT_FORMAT];

}

/*
 * Returns a NSDate from a given string
 */
+ (NSDate*) dateFromString: (NSString*) aString {
	NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
	[dateFormatter setDateFormat:DATE_IN_FORMAT];
//    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	NSDate* myDate = [dateFormatter dateFromString: aString];
    
	return [Tools dateWithTimeToZero:myDate];
}

/*
 * Returns the year of the given date as a string
 */
+ (NSString *)yearFromDate: (NSDate *) aDate {
    NSCalendar *calendar = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
//    [calendar setLocale:[NSLocale currentLocale]];
    [calendar setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSWeekdayCalendarUnit;
    NSDateComponents *currentDateComps = [calendar components:unitFlags fromDate:aDate];
    
    return [NSString stringWithFormat:@"%i", currentDateComps.year];
}

/*
 * Returns the month of the given date as a string
 */
+ (NSString *)monthFromDate: (NSDate *) aDate {
    NSCalendar *calendar = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
//    [calendar setLocale:[NSLocale currentLocale]];
    [calendar setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSWeekdayCalendarUnit;
    NSDateComponents *currentDateComps = [calendar components:unitFlags fromDate:aDate];
    
    NSString *monthString = [NSString stringWithFormat:@"%i", currentDateComps.month];
    if ([monthString length] < 2) {
        monthString = [NSString stringWithFormat:@"0%i", currentDateComps.month];
    }
    
    return ([monthString length] > 0 ? monthString : @"");
}

/*
 * Returns the last day of the month of the given date
 */
+ (NSString *)lastDayOfDate:(NSDate *)aDate {
    NSCalendar *calendar = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
//    [calendar setLocale:[NSLocale currentLocale]];
    [calendar setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    NSRange days = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:aDate];
    
    NSString *dayString = [NSString stringWithFormat:@"%i", days.length];
    if ([dayString length] < 2) {
        dayString = [NSString stringWithFormat:@"0%i", days.length];
    }
    
    return ([dayString length] > 0 ? dayString : @"");
}

/*
 * Returns a date with hours, minutes and seconds to 00
 */
+ (NSDate*)getDateWithTimeToZero:(NSDate *)date {
    
	NSCalendar *calendar = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    [calendar setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *currentDateComps = [calendar components:unitFlags fromDate:date];
    [currentDateComps setHour:0];
    [currentDateComps setMinute:0];
    [currentDateComps setSecond:0];
    
    NSDate *convertedDate = [calendar dateFromComponents:currentDateComps];
    
    return convertedDate;
}

/*
 * Returns a string with hour from a given NSDate
 */
+ (NSString *)getStringWithHourWithCurrentLocaleFromDate:(NSDate *)date {
    
    NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
	[dateFormatter setDateFormat:DATE_HOUR_OUT_FORMAT];
    [dateFormatter setLocale:[NSLocale currentLocale]];
	NSString* dateAsString = [dateFormatter stringFromDate:date];
    
	return ([dateAsString length] > 0 ? dateAsString : @"");

}

/*
 * Returns the date formatter initialized
 */
+ (NSDateFormatter *)dateFormatter {
    
    if (dateFormatter_ == nil) {
        
        @synchronized([Tools class]) {
            
            if (dateFormatter_ == nil) {
                
                dateFormatter_ = [[NSDateFormatter alloc] init];
                [dateFormatter_ setDateFormat:DATE_IN_FORMAT];
                [dateFormatter_ setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                
            }
            
        }
        
    }
    
    return dateFormatter_;
    
}

/*
 * Returns the long format (dd/MM/yyyy) date formatter initialized.
 */
+ (NSDateFormatter *)longFormatDateFormatter {
    
    if (longFormatDateFormatter_ == nil) {
        
        @synchronized([Tools class]) {
            
            if (longFormatDateFormatter_ == nil) {
                
                longFormatDateFormatter_ = [[NSDateFormatter alloc] init];
                [longFormatDateFormatter_ setDateFormat:@"dd/MM/yyyy"];
                [longFormatDateFormatter_ setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                
            }
            
        }
        
    }
    
    return longFormatDateFormatter_;
    
}

/*
 * Returns the no separator format (yyyyMMdd) date formatter initialized.
 */
+ (NSDateFormatter *)noSeparatorDateFormatter {
    
    if (noSeparatorFormatDateFormatter_ == nil) {
        
        @synchronized([Tools class]) {
            
            if (noSeparatorFormatDateFormatter_ == nil) {
                
                noSeparatorFormatDateFormatter_ = [[NSDateFormatter alloc] init];
                [noSeparatorFormatDateFormatter_ setDateFormat:@"yyyyMMdd"];
                [noSeparatorFormatDateFormatter_ setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                
            }
            
        }
        
    }
    
    return noSeparatorFormatDateFormatter_;

}

/*
 * Returns the calendar initialized
 */
+ (NSCalendar *)calendar {
    
    if (calendar_ == nil) {
        
        calendar_ = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [calendar_ setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        
    }
    
    return calendar_;
    
}

/*
 * Returns a date with hours, minutes and seconds set to 00
 */
+ (NSDate *)dateWithTimeToZero:(NSDate *)aDate {
    
    NSDate *result = nil;
    
    if (aDate != nil) {
        
        NSCalendar *calendar = [Tools calendar];
        NSDate *auxDate = nil;
        
        if ([calendar rangeOfUnit:NSDayCalendarUnit
                        startDate:&auxDate
                         interval:nil
                          forDate:aDate]) {
            
            result = auxDate;
            
        }
        
    }
    
    return result;
    
}

/*
 * Extracts a date from a server string. The date time is set to 00:00 (midnight) and the time zone is UTC
 */
+ (NSDate *)dateFromServerString:(NSString *)aString {
    
    NSDate *result = nil;
    
    if ([aString length] > 0) {

        NSDateFormatter *dateFormatter = [Tools dateFormatter];
        result = [dateFormatter dateFromString:aString];
        
        if (result == nil) {
            
            dateFormatter = [Tools longFormatDateFormatter];
            result = [dateFormatter dateFromString:aString];
            
            if (result == nil) {
                
                dateFormatter = [Tools noSeparatorDateFormatter];
                result = [dateFormatter dateFromString:aString];
                
            }
            
        }
        
        result = [Tools dateWithTimeToZero:result];

    }
    
    return result;
    
}



#pragma mark -
#pragma mark Currency conversion

/**
 * Creates the currency symbol dictionary if nil
 */
+ (void)createCurrencySymbolDictionary {
    
    if (currencySymbolsDict_ == nil) {
        
        currencySymbolsDict_ = [[NSMutableDictionary alloc] initWithCapacity:6];
        [currencySymbolsDict_ setObject:CURRENCY_EUR_SYMBOL forKey:CURRENCY_EUR_LITERAL];
        [currencySymbolsDict_ setObject:CURRENCY_JPY_SYMBOL forKey:CURRENCY_JPY_LITERAL];
        [currencySymbolsDict_ setObject:CURRENCY_CNY_SYMBOL forKey:CURRENCY_CNY_LITERAL];
        [currencySymbolsDict_ setObject:CURRENCY_GBP_SYMBOL forKey:CURRENCY_GBP_LITERAL];
        [currencySymbolsDict_ setObject:CURRENCY_USD_SYMBOL forKey:CURRENCY_USD_LITERAL];
        [currencySymbolsDict_ setObject:CURRENCY_KRW_SYMBOL forKey:CURRENCY_KRW_LITERAL];
        // Perú
        [currencySymbolsDict_ setObject:CURRENCY_SOLES_SYMBOL forKey:CURRENCY_SOLES_LITERAL];
        [currencySymbolsDict_ setObject:CURRENCY_SOLES_SYMBOL forKey:CURRENCY_SOLES_LITERAL_2];
        [currencySymbolsDict_ setObject:CURRENCY_DOLARES_SYMBOL forKey:CURRENCY_DOLARES_LITERAL];
    }
    
}

/*
 * Converts the server currency symbol into a client currency symbol. When no translation exists, the provided currency is returned
 */
+ (NSString *)translateServerCurrencySymbolIntoClientCurrencySymbol:(NSString *)aServerCurrencySymbol {
    
    [Tools createCurrencySymbolDictionary];
    
    NSString *result = [currencySymbolsDict_ objectForKey:aServerCurrencySymbol];
    
    if ([result length] == 0) {
        
        result = aServerCurrencySymbol;
        
    }
    
    return result;
    
}

/*
 * Converts the server currency symbol into a client currency symbol. When no translation exists, the provided currency is returned
 */
+ (NSString *)clientCurrencySymbolForServerCurrency:(NSString *)aServerCurrencySymbol {
    
    [Tools createCurrencySymbolDictionary];
    
    if(aServerCurrencySymbol){
        aServerCurrencySymbol = [aServerCurrencySymbol uppercaseString];
    }
    
    NSString *result = [currencySymbolsDict_ objectForKey:aServerCurrencySymbol];
    
    if ([result length] == 0) {
        
        result = @"";
        
    }
    
    return result;
    
}

/*
 * Returns the client application main currency literal (EUR)
 */
+ (NSString *)mainCurrencyLiteral {
    return CURRENCY_SOLES_LITERAL;
}
/*
 * Returns the client application main currency symbol (€)
 */
+ (NSString *)mainCurrencySymbol {
    
    return CURRENCY_SOLES_SYMBOL;
    
}

/*
 * Return the currency literal
 */
+(NSString *)getCurrencyLiteral:(NSString *)aCurrency{
    
    NSString *newCurrency = @"";
    
    if([aCurrency isEqualToString:CURRENCY_SOLES_LITERAL]){
        
        newCurrency = NSLocalizedString(SOLES_CURRENCY_TEXT_KEY,nil);
    
    }else if([aCurrency isEqualToString:CURRENCY_EUR_LITERAL]){
        
        newCurrency = NSLocalizedString(EUROS_CURRENCY_TEXT_KEY,nil);
        
    }else if([aCurrency isEqualToString:CURRENCY_DOLARES_LITERAL]){
        
        newCurrency = NSLocalizedString(DOLLARS_CURRENCY_TEXT_KEY,nil);
        
    }    
    return newCurrency;

}
/*
 * Return the currency server
 */
+(NSString *)getCurrencyServer:(NSString *)aCurrency{
    
    NSString *newCurrency = @"";
    
    if([aCurrency isEqualToString:NSLocalizedString(SOLES_CURRENCY_TEXT_KEY,nil)]){
        
        newCurrency = CURRENCY_SOLES_LITERAL;
        
    }else if([aCurrency isEqualToString:NSLocalizedString(EUROS_CURRENCY_TEXT_KEY,nil)]){
        
        newCurrency = CURRENCY_EUR_LITERAL;
        
    }else if([aCurrency isEqualToString:NSLocalizedString(DOLLARS_CURRENCY_TEXT_KEY,nil)]){
        
        newCurrency = CURRENCY_DOLARES_LITERAL;
        
    }
    return newCurrency;
}

/*
 *Return the currency simbol
 */
+(NSString *)getCurrencySimbol:(NSString *)aCurrency{
    
    NSString *newSimbol = @"";
    
    if([aCurrency isEqualToString:CURRENCY_SOLES_LITERAL]||[aCurrency isEqualToString:CURRENCY_NUEVOS_SOLES_LITERAL] ||[aCurrency isEqualToString:CURRENCY_SOLES_LITERAL_2] ){
        
        newSimbol = NSLocalizedString(CURRENCY_SOLES_SYMBOL,nil);
        
    }else if([aCurrency isEqualToString:CURRENCY_EUR_LITERAL]){
        
        newSimbol = NSLocalizedString(CURRENCY_EUR_LITERAL,nil);
        
    }else if([aCurrency isEqualToString:CURRENCY_DOLARES_LITERAL]||[aCurrency isEqualToString:CURRENCY_DOLARES_ACCENT_LITERAL] || [aCurrency isEqualToString:CURRENCY_USD_LITERAL]){
        
        newSimbol = NSLocalizedString(CURRENCY_DOLARES_SYMBOL,nil);
        
    }    
    return newSimbol;
}

#pragma mark -
#pragma mark Currency checker

/**
 * Compares two currencies
 *
 * @param firstCurrency
 * @param secondCurrency
 * @return YES if both are equal
 */
+ (BOOL)currency:(NSString *)firstCurrency isEqualToCurrency:(NSString *)secondCurrency {
    
    BOOL result = NO;
    
    NSString *firstCurrencyAux = [[Tools notNilString:firstCurrency] lowercaseString];
    NSString *secondCurrencyAux = [[Tools notNilString:secondCurrency] lowercaseString];
    
    result = [firstCurrencyAux isEqualToString:secondCurrencyAux];
    
    return result;
    
}

#pragma mark -
#pragma mark Time and distance conversion into string

/*
 * Returns the distance as a string
 */
+ (NSString *)distanceAsString:(NSInteger)aDistance {
    
	NSString *result = @"";
	
	if (aDistance >= 0) {
        
		if (aDistance >= 1000) {
            
			NSString *largeDistanceUnitText = NSLocalizedString(LARGE_DISTANCE_UNIT_ACRONYM_KEY, nil);
			result = [NSString stringWithFormat:@"%.1f %@", ((float)aDistance)/1000.0f, largeDistanceUnitText];
			result = [result stringByReplacingOccurrencesOfString:@"." withString:@","];
            
		} else {
            
			NSString *smallDistanceUnitText = NSLocalizedString(SMALL_DISTANCE_UNIT_ACRONYM_KEY, nil);
			result = [NSString stringWithFormat:@"%d %@", aDistance, smallDistanceUnitText];
            
		}
        
	}
	
	return result;
    
}

/*
 * Returns the time as a string
 */
+ (NSString *)timeAsString:(NSInteger)aTime {
    
	NSString *result = @"";
	
	if (aTime >=0) {
        
		if (aTime < 60) {
            
			result = NSLocalizedString(LESS_THAN_A_MINUTE_KEY, nil);
            
		} else {
            
			NSInteger timeInMinutes = (NSInteger)(aTime / 60.0f);
			
			if ((timeInMinutes * 60.0f) != aTime) {
                
				timeInMinutes ++;
                
			}
			
			NSString *minutes = NSLocalizedString(MINUTES_KEY, nil);
			result = [NSString stringWithFormat:@"%d %@", timeInMinutes, minutes];
            
		}
        
	}
	
	return result;
    
}

#pragma mark -
#pragma mark Device identification

/*
 * Get the UID of the device on a NSData format
 */
+ (NSString*) getUID {
	
	NSString* uidString = [BBVADeviceUID deviceUID];
	NSMutableData* uidData = [NSMutableData dataWithBytes: [uidString UTF8String] length: [uidString lengthOfBytesUsingEncoding: NSUTF8StringEncoding]];
	return [Tools getMD5StringFromData:uidData];

}

/*
 * Calculates the given data MD5 digest and returns it as an hexadecimal string capitalized
 */
+ (NSString*) getMD5StringFromData: (NSData*) aData {
	NSMutableString* result = [NSMutableString stringWithCapacity: 32];
	
	NSInteger dataLength = [aData length];
	unsigned char data[dataLength];
	[aData getBytes: data length: dataLength];
	unsigned char digest[CC_MD5_DIGEST_LENGTH];
	
	CC_MD5(data, dataLength, digest);
	
	for (NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++) {
		[result appendFormat: @"%02X", digest[count]];
	}
	
	return result;
}

#pragma mark -
#pragma mark Ciphering and deciphering

/*
 * Ciphers the provided string
 */
+ (NSString *)cipherString:(NSString *)aString {
    
	int padLen = [aString length] % 8;
	
	if (padLen > 0) {		
		aString = [[@"        " substringFromIndex:padLen] stringByAppendingString:aString];
	}
	
    NSData *dataToCipher = [aString dataUsingEncoding:NSUTF8StringEncoding];
    NSData *dataCiphered = [self DES:dataToCipher
                           operation:kCCEncrypt];
    
    NSString *result = [self raw2hex:dataCiphered];
    
    return result;
    
}

/*
 * Deciphers the provided data into a string
 */
+ (NSString *)decipherString:(NSString *)aString {
    
    NSData *auxData = [aString dataUsingEncoding:NSASCIIStringEncoding];
    NSData *dataToDecipher = [self hex2raw:auxData];
    NSData *dataDeciphered = [self DES:dataToDecipher
                             operation:kCCDecrypt];
    
    NSString *result = [[[[NSString alloc] initWithData:dataDeciphered
                                               encoding:NSUTF8StringEncoding] autorelease] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    return result;
    
}

/*
 * Does the DES given operation over the given data
 */
+ (NSData *)DES:(NSData *)enc operation:(CCOperation)operation {
    
    NSString *initialValue = [self getUID];
    NSString *raw_asc = [self getMD5StringFromData:[initialValue dataUsingEncoding:NSUTF8StringEncoding]];
    raw_asc = [[NSString stringWithFormat:@"%@%@", raw_asc, raw_asc] substringToIndex:48];
	NSString *iv_asc = @"854fec10a422cafd";
    
	NSData *key =  [self hex2raw:[raw_asc dataUsingEncoding:NSASCIIStringEncoding]];
	NSData *iv =  [self hex2raw:[iv_asc dataUsingEncoding:NSASCIIStringEncoding]];
	
	unsigned char *buffer = NULL;
	CCCryptorStatus status;
	CCCryptorRef decrypt_ref;
	size_t decrypted_length = 0;
	
	status = CCCryptorCreate(operation, kCCAlgorithm3DES, 0, (const void *)[key bytes], [key length], (const void *)[iv bytes], &decrypt_ref);
	if (status != kCCSuccess) {
	    //DLog(@"CCCryptorCreate failed");
	    return nil;
	}
	
	NSMutableData *encM = [[[NSMutableData alloc] initWithLength:[enc length]] autorelease];
	buffer = [encM mutableBytes];
	
	status = CCCryptorUpdate(decrypt_ref, (const void *)[enc bytes], [enc length], buffer, [enc length], &decrypted_length);
	if (status != kCCSuccess) {
	    //DLog(@"CCCryptorUpdate failed");
	    return nil;
	}
	
	int plainLength = decrypted_length;
	
	status = CCCryptorFinal(decrypt_ref, buffer + decrypted_length, [enc length] - decrypted_length, &decrypted_length);
	if (status != kCCSuccess) {
	    //DLog(@"CCCryptorFinal failed");
	    return nil;
	}
	
    CCCryptorRelease(decrypt_ref);
	
    NSMutableData *plain = [NSMutableData dataWithLength:plainLength];
	[plain setData:[encM subdataWithRange:NSMakeRange(0, plainLength)]];
	
    return plain; 
    
}


/*
 * Convert hexadecimal value to binary data
 */
+ (NSData *)hex2raw:(NSData *)hex {
    // Based on Erik Doernenburg's NSData+MIME.m
    const char *source, *endOfSource;
    NSMutableData *decodedData;
    char *dest;
	
    source = [hex bytes];
    endOfSource = source + [hex length];
    decodedData = [NSMutableData dataWithLength:[hex length]];
    dest = [decodedData mutableBytes];
    
    while (source < endOfSource) {
	    if (isxdigit(*source) && isxdigit(*(source+1))) {
	        *dest++ = asciitable[(int)*source] * 16 + asciitable[(int)*(source+1)];
	        source += 2;     
	    } else
	        return nil;
	}
	
	[decodedData setLength:(unsigned int)((void *)dest - [decodedData mutableBytes])];
	
    return decodedData;
}

/*
 * Convert binary data to hexadecimal value
 */
+ (NSString *)raw2hex:(NSData *) data {
    NSMutableString *hex = [NSMutableString string];
    unsigned char *bytes = (unsigned char *)[data bytes];
    char temp[3];
    int i = 0;
    for (i = 0; i < [data length]; i++) {
        temp[0] = temp[1] = temp[2] = 0;
        (void)sprintf(temp, "%02x", bytes[i]);
        [hex appendString:[NSString stringWithUTF8String: temp]];
    }
    return hex;
}


/**
 * Returns the total frame of an UITableView.
 */
+(CGRect)tableFrame:(UITableView *)tableView {
    CGRect tableRect = CGRectZero;
    for( NSInteger i=[tableView numberOfSections]-1; i>=0; i-- ) {
        tableRect = CGRectUnion( tableRect, [tableView rectForSection:i] );
    }
    
    return tableRect;
}

#pragma mark -
#pragma mark Label utilities

/*
 * Sets a given text to a label, aligning the text vertically to the label top.
 * Resizes the label to fit the text in height
 */
+ (void)setText:(NSString *)text
        toLabel:(UILabel *)aLabel {
    
    CGFloat height = [Tools labelHeight:aLabel
                                forText:text];
    
    CGRect frame = aLabel.frame;
    frame.size.height = height;
    aLabel.frame = frame;
    aLabel.text = text;
    
}

/*
 * Adjusts a label width to fit the text displayed. Label number of lines must be 1
 */
+ (void)fitLabelWidth:(UILabel *)aLabel {
    
    if (aLabel.numberOfLines == 1) {
        
        UIFont *font = aLabel.font;
        NSString *text = aLabel.text;
        CGSize textSize = [text sizeWithFont:font];
        CGRect labelFrame = aLabel.frame;
        labelFrame.size.width = textSize.width;
        aLabel.frame = labelFrame;
        
    }
    
}

/*
 * Checks whether the provided directory is created. It creates the directory when it is not found
 */
+ (void)checkAndCreateDirectory:(NSString *)aDirectoryPath {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:aDirectoryPath]) {
        
        [fileManager createDirectoryAtPath:aDirectoryPath
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:nil];
        
    }
    
}

/*
 * Creates a random file name not used inside a given directory, creates an empy file with that name inside the directory
 * and returns th file name (not including the path)
 */
+ (NSString *)findAndCreateNotUsedFileNameInsideDirectory:(NSString *)aDirectory {
    
    NSString *result = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath;
    
    do {
        
        result = [NSString stringWithFormat:@"%u", arc4random()];
        
		if ([result length] > 0) {
            
            filePath = [aDirectory stringByAppendingPathComponent:result];
            
            if (![fileManager fileExistsAtPath:filePath]) {
                
                [fileManager createFileAtPath:filePath
                                     contents:nil
                                   attributes:nil];
                
            } else {
                
                result = nil;
                
            }
            
        } else {
            
            result = nil;
        }
        
    } while (result == nil);
	
	return result;
    
}

/*
 * Calculates the label height to fit the given text. It takes into account the label font and the number of lines
 * Resizes the label to fit the text in height
 */
+ (CGFloat)labelHeight:(UILabel *)aLabel
               forText:(NSString *)aText {
    
	UIFont *fontUsed = aLabel.font;
	
	CGRect frame = aLabel.frame;
    CGFloat labelWidth = frame.size.width;
    NSInteger numberOfLines = aLabel.numberOfLines;
    CGSize maxSize = CGSizeMake(labelWidth, 1000000.0f);
    CGSize textSize = CGSizeZero;
    
    textSize = [aText sizeWithFont:fontUsed
                 constrainedToSize:maxSize];
    
    CGFloat maxHeight = textSize.height;
    
    if (numberOfLines > 0) {
        
        textSize = [aText sizeWithFont:fontUsed
                              forWidth:labelWidth
                         lineBreakMode:UILineBreakModeWordWrap];
        
        if (numberOfLines > 1) {
            
            CGFloat oneLineHeight = textSize.height;
            NSUInteger linesDisplayed = 1;
            CGFloat lastCumulativeHeight = oneLineHeight;
            CGFloat nextHeightLimit = oneLineHeight + oneLineHeight;
            CGSize nextLimitSize = CGSizeZero;
            
            while ((linesDisplayed < numberOfLines) && (maxHeight > lastCumulativeHeight)) {
                
                while (textSize.height == lastCumulativeHeight) {
                    
                    nextLimitSize = CGSizeMake(labelWidth, nextHeightLimit);
                    textSize = [aText sizeWithFont:fontUsed
                                 constrainedToSize:nextLimitSize];
                    
                    nextHeightLimit += oneLineHeight;
                    
                }
                
                lastCumulativeHeight = textSize.height;
                nextHeightLimit = lastCumulativeHeight;
                linesDisplayed++;
                
            }
            
        }
        
    }
    
    return textSize.height;
    
}

#pragma mark -
#pragma mark Transactions filtering

/*
 * Filters the given array with the given filter object and returns an autoreleased array with the filtering
 */
+ (NSMutableArray *)filterTrasactions:(NSArray *)transactionsArray withFilter:(TransactionsFilterObject *)filterObject {
    	    
    NSString *predicateString = nil;
    NSString *initDate = [[NSExpression expressionForConstantValue:filterObject.fromDate] description];
    NSString *finalDate = [[NSExpression expressionForConstantValue:filterObject.toDate] description];

    predicateString = [NSString stringWithFormat:@"operationDate >= %@ AND operationDate <= %@", initDate, finalDate];
            
    if (filterObject.showIncomes && filterObject.showPayments) {
        predicateString = [predicateString stringByAppendingFormat:@" AND (amount >= 0 OR amount < 0)"];
    } else if (filterObject.showIncomes && !filterObject.showPayments) {
        predicateString = [predicateString stringByAppendingFormat:@" AND amount >= 0"];
    } else if (!filterObject.showIncomes && filterObject.showPayments) {
        predicateString = [predicateString stringByAppendingFormat:@" AND amount < 0"];
    } else if (!filterObject.showIncomes && !filterObject.showPayments) {
        predicateString = [predicateString stringByAppendingFormat:@" AND amount < 0 AND amount >= 0"];
    }
    
    //DLog(@"%@", predicateString);
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];

    return [NSMutableArray arrayWithArray:[transactionsArray filteredArrayUsingPredicate:predicate]];

}

/*
 * Returns a NSData with the decoded data obtained from a base 64 string.
 */
+ (void)convertFromBase64Text:(NSString *)aBase64Text toData:(NSMutableData *)aData {
	NSString *charset = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
   
	// remove all the = symbols from the end and add "A" symbol to the end until the length is divisible by 4
	NSString *encoded = [[aBase64Text stringByReplacingOccurrencesOfString:@"=" withString:@""]stringByPaddingToLength:(ceil([aBase64Text length] / 4) * 4 ) withString:@"A" startingAtIndex:0];	

	char a, b, c, d;
	UInt32 z;
	Byte *byteData = (Byte*)malloc(3);
	for (int i = 0; i < [encoded length]; i += 4) {
		
		a = [charset rangeOfString:[encoded substringWithRange:NSMakeRange(i + 0, 1)]].location;
		b = [charset rangeOfString:[encoded substringWithRange:NSMakeRange(i + 1, 1)]].location;
		c = [charset rangeOfString:[encoded substringWithRange:NSMakeRange(i + 2, 1)]].location;
		d = [charset rangeOfString:[encoded substringWithRange:NSMakeRange(i + 3, 1)]].location;		
		z = ((UInt32)a << 18) + ((UInt32)b << 12) + ((UInt32)c << 6) + ((UInt32)d);
		
        
		byteData[0] = (z >> 16);
		byteData[1] = (z >> 8);
		byteData[2] = z;
		[aData appendBytes:byteData length:3];
	}	
	
	free(byteData);
	byteData = nil;
	[charset release];
	charset = nil;
	
}

/*
 * Returns a NSData from the given base64 string
 */
+ (NSData *)base64DataFromString:(NSString *)string {
    unsigned long ixtext, lentext;
    unsigned char ch, inbuf[4], outbuf[4];
    short i, ixinbuf;
    Boolean flignore, flendtext = false;
    const unsigned char *tempcstring;
    NSMutableData *theData;
    
    if (string == nil)
    {
        return [NSData data];
    }
    
    ixtext = 0;
    
    tempcstring = (const unsigned char *)[string UTF8String];
    
    lentext = [string length];
    
    theData = [NSMutableData dataWithCapacity: lentext];
    
    ixinbuf = 0;
    
    while (true)
    {
        if (ixtext >= lentext)
        {
            break;
        }
        
        ch = tempcstring [ixtext++];
        
        flignore = false;
        
        if ((ch >= 'A') && (ch <= 'Z'))
        {
            ch = ch - 'A';
        }
        else if ((ch >= 'a') && (ch <= 'z'))
        {
            ch = ch - 'a' + 26;
        }
        else if ((ch >= '0') && (ch <= '9'))
        {
            ch = ch - '0' + 52;
        }
        else if (ch == '+')
        {
            ch = 62;
        }
        else if (ch == '=')
        {
            flendtext = true;
        }
        else if (ch == '/')
        {
            ch = 63;
        }
        else
        {
            flignore = true; 
        }
        
        if (!flignore)
        {
            short ctcharsinbuf = 3;
            Boolean flbreak = false;
            
            if (flendtext)
            {
                if (ixinbuf == 0)
                {
                    break;
                }
                
                if ((ixinbuf == 1) || (ixinbuf == 2))
                {
                    ctcharsinbuf = 1;
                }
                else
                {
                    ctcharsinbuf = 2;
                }
                
                ixinbuf = 3;
                
                flbreak = true;
            }
            
            inbuf [ixinbuf++] = ch;
            
            if (ixinbuf == 4)
            {
                ixinbuf = 0;
                
                outbuf[0] = (inbuf[0] << 2) | ((inbuf[1] & 0x30) >> 4);
                outbuf[1] = ((inbuf[1] & 0x0F) << 4) | ((inbuf[2] & 0x3C) >> 2);
                outbuf[2] = ((inbuf[2] & 0x03) << 6) | (inbuf[3] & 0x3F);
                
                for (i = 0; i < ctcharsinbuf; i++)
                {
                    [theData appendBytes: &outbuf[i] length: 1];
                }
            }
            
            if (flbreak)
            {
                break;
            }
        }
    }
    
    return theData;
}


/*
 * Indicates the text is valid for a character set
 */
+(BOOL)isValidText:(NSString *)text forCharacterSet:(NSCharacterSet *)characterSet {
	
    BOOL valid = YES;
    
	unichar charExtracted;
	NSInteger size = [text length];
	for (NSInteger i = 0; (i < size) && (valid); i++) {
		charExtracted = [text characterAtIndex:i];
		valid = [characterSet characterIsMember:charExtracted];
	}
    
    return valid;
    
}

/*
 * Indicates the text is valid for a character string
 */
+(BOOL)isValidText:(NSString *)text forCharacterString:(NSString *)characterString {
	
    BOOL valid = [Tools isValidText:text forCharacterSet:[NSCharacterSet characterSetWithCharactersInString:characterString]];
    
    return valid;
    
}

/**
 * Indicates the account is valid for transfers
 *
 * @param account the account
 */
+ (BOOL)isValidAccountForTransfers:(BankAccount *)account {
    
    BOOL valid = ([CHECKING_ACCOUNT_TYPE isEqualToString:account.type] || [SAVINGS_ACCOUNT_TYPE isEqualToString:account.type]) &&
                 ([CURRENCY_SOLES_LITERAL isEqualToString:account.currency] || [CURRENCY_DOLARES_LITERAL isEqualToString:account.currency]);
    
    return valid;

}

#pragma mark -
#pragma mark Alert messages

/*
 * Shows a message to the user inside an alert view, using a given title
 */
+ (void)showAlertWithMessage:(NSString *)aMessage title:(NSString *)aTitle andDelegate:(id)aDelegate {
    
    if ([aMessage length] > 0) {
        
        NSString *buttonText = NSLocalizedString(OK_TEXT_KEY, nil);
        
        UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:aTitle message:aMessage delegate:aDelegate cancelButtonTitle:buttonText otherButtonTitles:nil] autorelease];
        [alertView show];
    }
}

/*
 * Shows an error alert view
 */
+ (void)showErrorWithMessage:(NSString *)anErrorMessage andDelegate:(id)aDelegate {
    
    if ([anErrorMessage length] > 0) {
        
        NSString *errorTitle = NSLocalizedString(INFO_MESSAGE_TITLE_KEY, nil);
        
        [Tools showAlertWithMessage:anErrorMessage title:errorTitle andDelegate:aDelegate];        
    }    
}

/*
 * Shows an info alert view
 */
+ (void)showAlertWithMessage:(NSString *)aInfoMessage {
    
    if ([aInfoMessage length] > 0) {
        
        NSString *infoTitle = NSLocalizedString(ALERT_MESSAGE_TITLE_KEY, nil);
        
        [Tools showAlertWithMessage:aInfoMessage title:infoTitle];
        
    }
    
}

/*
 * Shows a confirmation dialog with the geiven message and the given delegate to manage response
 */
+ (void)showConfirmationDialogWithMessage:(NSString *)aMessage andDelegate:(id)aDelegate {
    
    NSString *infoTitle = NSLocalizedString(INFO_MESSAGE_TITLE_KEY, nil);
    
    UIAlertView* alertView = [[[UIAlertView alloc] initWithTitle:infoTitle message:aMessage delegate:aDelegate cancelButtonTitle:nil otherButtonTitles:nil] autorelease];
    
    [alertView addButtonWithTitle:NSLocalizedString(OK_TEXT_KEY, nil)];
    [alertView addButtonWithTitle:NSLocalizedString(CANCEL_TEXT_BUTTON_KEY, nil)];
    
    [alertView show];
}

#pragma mark -
#pragma mark Miscellaneous

/*
 * Starts a phone call to a given number
 */
+ (void)startPhoneCallToNumber:(NSString *)aPhoneNumber {
    
    if ([aPhoneNumber length] > 0) {
        
		NSString *thePhoneNumber = [aPhoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        thePhoneNumber = [thePhoneNumber stringByReplacingOccurrencesOfString:@"("
                                                                   withString:@""];
        thePhoneNumber = [thePhoneNumber stringByReplacingOccurrencesOfString:@")"
                                                                   withString:@""];
        thePhoneNumber = [thePhoneNumber stringByReplacingOccurrencesOfString:@"-"
                                                                   withString:@""];
        NSString *urlScheme = [NSString stringWithFormat:@"tel:%@", thePhoneNumber];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: urlScheme]];
        
    }
    
}


@end
