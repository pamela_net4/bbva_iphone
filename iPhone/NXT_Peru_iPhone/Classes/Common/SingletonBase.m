/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "SingletonBase.h"


#pragma mark -

@implementation SingletonBase

#pragma mark -
#pragma mark Singleton common methods

/**
 * The singleton cannot autoreleased
 */
- (id)autorelease {
    
    return self;
    
}

/**
 * The singleton cannot be copied to another zone
 */
- (id)copyWithZone:(NSZone *)zone {
    
    return self;
    
}

/**
 * The singleton cannot be released
 */
- (oneway void)release {
    
    //do nothing
    
}

/**
 * The singleton cannot be retained
 */
- (id)retain {
    
    return self;
    
}

/**
 * The singleton retain count denotes that it cannot be released
 */
- (NSUInteger)retainCount {
    
    return NSUIntegerMax;
    
}

@end
