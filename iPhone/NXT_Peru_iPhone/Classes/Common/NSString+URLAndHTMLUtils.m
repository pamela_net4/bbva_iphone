/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NSString+URLAndHTMLUtils.h"


#pragma mark -

@implementation NSString(URLAndHTMLUtils)

#pragma mark -
#pragma mark Static attributes

/**
 * Dictionary containing the HTML escape characters and their transltion, except for the & character
 */
static NSMutableDictionary *escapedCharactersDictionary_ = nil;

#pragma mark -
#pragma mark URL encoding

/*
 * Returns an autoreleased NSString with the URL encoded version of the receiving NSString using the provided encoding
 */
- (NSString *)urlEncodeUsingEncoding:(NSStringEncoding)anEncoding {
    
    NSString *result = (NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                           NULL,
                                                                           (CFStringRef)self,
                                                                           NULL,
                                                                           (CFStringRef)@"\";:@&=+$,/?%#[]% ",
                                                                           CFStringConvertNSStringEncodingToEncoding(anEncoding));
    return [result autorelease];
    
}

#pragma mark -
#pragma mark HTML escaping

/*
 * Returns an autoreleased NSString with the HTML escaped characters transformed into the correct characters
 */
- (NSString *)htmlEscapedCharactersTransformed {
    
    NSMutableString *mutableResult = [NSMutableString stringWithString:self];

    if (escapedCharactersDictionary_ == nil) {
        
        escapedCharactersDictionary_ = [[NSMutableDictionary alloc] init];
        [escapedCharactersDictionary_ setObject:@"€"
                                         forKey:@"&euro;"];
        [escapedCharactersDictionary_ setObject:@" "
                                         forKey:@"&nbsp;"];
        [escapedCharactersDictionary_ setObject:@"\""
                                         forKey:@"&quot;"];
        [escapedCharactersDictionary_ setObject:@"<"
                                         forKey:@"&lt;"];
        [escapedCharactersDictionary_ setObject:@">"
                                         forKey:@"&gt;"];
        [escapedCharactersDictionary_ setObject:@"¢"
                                         forKey:@"&cent;"];
        [escapedCharactersDictionary_ setObject:@"£"
                                         forKey:@"&pound;"];
        [escapedCharactersDictionary_ setObject:@"¤"
                                         forKey:@"&curren;"];
        [escapedCharactersDictionary_ setObject:@"¤"
                                         forKey:@"&curren;"];
        [escapedCharactersDictionary_ setObject:@"¥"
                                         forKey:@"&yen;"];
        [escapedCharactersDictionary_ setObject:@"©"
                                         forKey:@"&copy;"];
        [escapedCharactersDictionary_ setObject:@"ª"
                                         forKey:@"&ordf;"];
        [escapedCharactersDictionary_ setObject:@"®"
                                         forKey:@"&reg;"];
        [escapedCharactersDictionary_ setObject:@"°"
                                         forKey:@"&deg;"];
        
    }
    
    NSArray *escapedCharacters = [escapedCharactersDictionary_ allKeys];
    NSString *translatedCharacter = nil;
    
    for (NSString *escapedCharacter in escapedCharacters) {
        
        translatedCharacter = [escapedCharactersDictionary_ objectForKey:escapedCharacter];
        
        if (translatedCharacter != nil) {
            
            [mutableResult replaceOccurrencesOfString:escapedCharacter
                                           withString:translatedCharacter
                                              options:0
                                                range:NSMakeRange(0, [mutableResult length])];
            
        }
        
    }
    
    [mutableResult replaceOccurrencesOfString:@"&amp;"
                                   withString:@"&"
                                      options:0
                                        range:NSMakeRange(0, [mutableResult length])];
    
    return [NSString stringWithString:mutableResult];
    
}

@end
