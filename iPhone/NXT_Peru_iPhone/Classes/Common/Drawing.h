/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

/**
 * Implement common methods for Drawing.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Drawing : NSObject {
    
}

/**
 * Adds a rounded rect to given rectangle
 *
 * @param aContext graphic context
 * @param aRectangle rect boordered rectangle
 * @param anOvalWidth Oval with for roundered path
 * @param anOvalHeight Oval height for roundered path
 */
+ (void)addRoundedRectToPath:(CGContextRef)aContext rectangle:(CGRect)aRectangle ovalWidth:(CGFloat)anOvalWidth andOvalHeight:(CGFloat)anOvalHeight;

/**
 * Fills a rect with a linear vertical gradient
 *
 * @param aContext graphic context
 * @param aRectangle rect boordered rectangle
 * @param aColors array fwith colors (each has 4 components)
 * @param aNumberOfColors Number of colors
 * @param aLocations gradient locations
 */
+ (void)fillRectWithLinearGradient:(CGContextRef)aContext rectangle:(CGRect)aRectangle colors:(CGFloat[])aColors numberOfColors:(NSInteger)aNumberOfColors andLocations:(CGFloat[])aLocations;

/**
 * Fills a rect with a linear horizontal gradient
 *
 * @param aContext graphic context
 * @param aRectangle rect boordered rectangle
 * @param aColors array fwith colors (each has 4 components)
 * @param aNumberOfColors Number of colors
 * @param aLocations gradient locations
 */
+ (void)fillRectWithHorizontalGradient:(CGContextRef)aContext rectangle:(CGRect)aRectangle colors:(CGFloat[])aColors numberOfColors:(NSInteger)aNumberOfColors andLocations:(CGFloat[])aLocations;

@end
