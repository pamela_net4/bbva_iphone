//
//  FrequentOperationConstants.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/13/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#ifndef NXT_Peru_iPhone_FrequentOperationConstants_h
#define NXT_Peru_iPhone_FrequentOperationConstants_h

/**
 * Enumerates the different types of transfer the user can make
 */
typedef enum {
    
    FOTETransferToOtherBankAccount = 0, //!<The user is performing a frequent operation transfer between his/her own accounts
    FOTETransferToOtherUserAccount,     //!<The user is performing a frequent operation transfer to another user with a BBVA account
    FOTETransferToGiftCard,             //!<The user is performing a frequent operation transfer to gift card
    FOTETransferCashMobile,             //!<The user is performing a frequent operation transfer with cash mobile
    FOTEPaymentInstAndComp,              //!<The user is performing a frequent operation payment agreements
    FOTEPaymentWaterService,              //!<The user is performing a frequent operation payment water service
    FOTEPaymentClaroService,              //!<The user is performing a frequent operation payment claro service
    FOTEPaymentTelfService,              //!<The user is performing a frequent operation payment telefonica service
    FOTEPaymentThirdCard,               //!<The user is performing a frequent operation payment to third card
    FOTEPaymentOtherBankCard,           //!<The user is performing a frequent operation payment other card bank
    FOTEPaymentRechargeCellPhone        //!<The user is performing a frequent operation payment recharge cell phone
    
} FOTypeEnum;

#endif
