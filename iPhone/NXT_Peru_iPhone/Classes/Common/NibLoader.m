/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NibLoader.h"


#pragma mark -

/**
 * NibLoader private extension
 */
@interface NibLoader()

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 * @private
 */
+ (NibLoader *)getInstance;

/**
 * Creates and returns an autoreleased NSObject constructed from a NIB file
 *
 * @param nibFile The NIB file to construct the object from
 * @return The autoreleased UIView constructed from a NIB file
 * @private
 */
- (NSObject *)loadObjectFromNIBFile:(NSString *)nibFile;

@end


#pragma mark -

@implementation NibLoader

#pragma mark -
#pragma mark Properties

@synthesize auxObject = auxObject_;

#pragma mark -
#pragma mark Static attributes

/**
 * NibLoader singleton only instance
 */
static NibLoader *nibLoaderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * Allocates the singleton instance, in case it is not already allocated
 *
 * @param zone The memory zone to allocate the singleton in
 * @return The newly allocated singleton instance, or nil if singleton was already allocated
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([NibLoader class]) {
        
        if (nibLoaderInstance_ == nil) {
            
            nibLoaderInstance_ = [super allocWithZone:zone];
            return nibLoaderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (NibLoader *)getInstance {
    
    if (nibLoaderInstance_ == nil) {
        
        @synchronized([NibLoader class]) {
            
            if (nibLoaderInstance_ == nil) {
                
                nibLoaderInstance_ = [[NibLoader alloc] init];
                
            }
            
        }
        
    }
    
    return nibLoaderInstance_;
    
}

#pragma mark -
#pragma mark Memory manager

/**
 * Releases used memory
 */
- (void)dealloc {
    
    nibLoaderInstance_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Object construction

/*
 * Creates the autoreleased object from the NIB file
 */
+ (NSObject *)loadObjectFromNIBFile:(NSString *)nibFile {
    
    NibLoader *nibLoader = [NibLoader getInstance];
    
    return [nibLoader loadObjectFromNIBFile:nibFile];
    
}

/*
 * Creates and returns an autoreleased object constructed from a NIB file
 */
- (NSObject *)loadObjectFromNIBFile:(NSString *)NibFile {
    
    [[NSBundle mainBundle] loadNibNamed:NibFile owner:self options:nil];
    
    NSObject *result = auxObject_;
    auxObject_ = nil;
    
    return result;
    
}

@end
