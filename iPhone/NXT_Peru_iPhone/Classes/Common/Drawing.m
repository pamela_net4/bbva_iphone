// Modified source file

//
//  drawing.m
//  PDColoredProgressViewDemo
//
//  Created by Pascal Widdershoven on 03-01-09.
//  Copyright 2009 P-development. All rights reserved.
//

/* 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Computer, Inc. ("Apple") in consideration of your agreement to the
 following terms, and your use, installation, modification or
 redistribution of this Apple software constitutes acceptance of these
 terms.  If you do not agree with these terms, please do not use,
 install, modify or redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software. 
 Neither the name, trademarks, service marks or logos of Apple Computer,
 Inc. may be used to endorse or promote products derived from the Apple
 Software without specific prior written permission from Apple.  Except
 as expressly stated in this notice, no other rights or licenses, express
 or implied, are granted by Apple herein, including but not limited to
 any patent rights that may be infringed by your derivative works or by
 other works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2005 Apple Computer, Inc., All Rights Reserved
 
 @source http://developer.apple.com/mac/library/samplecode/QuartzShapes/listing10.html
 See license above
 */


#import "Drawing.h"


#pragma mark -

@implementation Drawing

#pragma mark -
#pragma mark Drawind methods

/**
 * Adds a rounded rect to given rectangle
 *
 * @param aContext graphic context
 * @param aRectangle rect boordered rectangle
 * @param anOvalWidth Oval with for roundered path
 * @param anOvalHeight Oval height for roundered path
 */
+ (void)addRoundedRectToPath:(CGContextRef)aContext rectangle:(CGRect)aRectangle ovalWidth:(CGFloat)anOvalWidth andOvalHeight:(CGFloat)anOvalHeight {
    
    CGFloat fw, fh;
    // If the width or height of the corner oval is zero, then it reduces to a right angle,
    // so instead of a rounded rectangle we have an ordinary one.
    if (anOvalWidth == 0 || anOvalHeight == 0) {
        
        CGContextAddRect(aContext, aRectangle);
        return;
        
    }
    
    //  Save the context's state so that the translate and scale can be undone with a call
    //  to CGContextRestoreGState.
    CGContextSaveGState(aContext);
    
    //  Translate the origin of the contex to the lower left corner of the rectangle.
    CGContextTranslateCTM(aContext, CGRectGetMinX(aRectangle), CGRectGetMinY(aRectangle));
    
    //Normalize the scale of the context so that the width and height of the arcs are 1.0
    CGContextScaleCTM(aContext, anOvalWidth, anOvalHeight);
    
    // Calculate the width and height of the rectangle in the new coordinate system.
    fw = CGRectGetWidth(aRectangle) / anOvalWidth;
    fh = CGRectGetHeight(aRectangle) / anOvalHeight;
    
    // CGContextAddArcToPoint adds an arc of a circle to the context's path (creating the rounded
    // corners).  It also adds a line from the path's last point to the begining of the arc, making
    // the sides of the rectangle.
    CGContextMoveToPoint(aContext, fw, fh/2);  // Start at lower right corner
    CGContextAddArcToPoint(aContext, fw, fh, fw/2, fh, 1);  // Top right corner
    CGContextAddArcToPoint(aContext, 0, fh, 0, fh/2, 1); // Top left corner
    CGContextAddArcToPoint(aContext, 0, 0, fw/2, 0, 1); // Lower left corner
    CGContextAddArcToPoint(aContext, fw, 0, fw, fh/2, 1); // Back to lower right
    
    // Close the path
    CGContextClosePath(aContext);
    
    // Restore the context's state. This removes the translation and scaling
    // but leaves the path, since the path is not part of the graphics state.
    CGContextRestoreGState(aContext);
    
}

/**
 * Fills a rect with a linear vertical gradient
 *
 * @param aContext graphic context
 * @param aRectangle rect boordered rectangle
 * @param aColors array fwith colors (each has 4 components)
 * @param aNumberOfColors Number of colors
 * @param aLocations gradient locations
 */
+ (void)fillRectWithLinearGradient:(CGContextRef)aContext rectangle:(CGRect)aRectangle colors:(CGFloat[])aColors numberOfColors:(NSInteger)aNumberOfColors andLocations:(CGFloat[])aLocations {
    
    CGContextSaveGState(aContext);
    
    if (!CGContextIsPathEmpty(aContext)) {

        CGContextClip(aContext);
        
    }
    
    CGContextTranslateCTM(aContext, CGRectGetMinX(aRectangle), CGRectGetMinY(aRectangle));
    CGPoint start = CGPointMake(0, 0);
    CGPoint end = CGPointMake(0, aRectangle.size.height);
    
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(space, aColors, aLocations, aNumberOfColors);
    CGContextDrawLinearGradient(aContext, gradient, end, start, 0);
    CGContextRestoreGState(aContext);
    
    CGColorSpaceRelease(space);
    CGGradientRelease(gradient);
    
}

/**
 * Fills a rect with a linear horizontal gradient
 *
 * @param aContext graphic context
 * @param aRectangle rect boordered rectangle
 * @param aColors array fwith colors (each has 4 components)
 * @param aNumberOfColors Number of colors
 * @param aLocations gradient locations
 */
+ (void)fillRectWithHorizontalGradient:(CGContextRef)aContext rectangle:(CGRect)aRectangle colors:(CGFloat[])aColors numberOfColors:(NSInteger)aNumberOfColors andLocations:(CGFloat[])aLocations {
    
    CGContextSaveGState(aContext);
    
    if (!CGContextIsPathEmpty(aContext)) {

        CGContextClip(aContext);
        
    }
    
    CGContextTranslateCTM(aContext, CGRectGetMinX(aRectangle), CGRectGetMinY(aRectangle));
    CGPoint start = CGPointMake(0, 0);
    CGPoint end = CGPointMake(aRectangle.size.width, 0);
    
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(space, aColors, aLocations, aNumberOfColors);
    CGContextDrawLinearGradient(aContext, gradient, end, start, 0);
    CGContextRestoreGState(aContext);
    
    CGColorSpaceRelease(space);
    CGGradientRelease(gradient);
    
}

@end
