/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "DirConfiguration.h"


/**
 * Defines the application general directory
 */
#define LOCAL_GENERAL_DOCUMENTS_DIR             @".NXT_iPad"

/**
 * Defines the download directory (inside the application general directory)
 */
#define DOWNLOAD_DIR                            @"DownloadCache"

/**
 * Defines the house keeping directory (inside the application general directory)
 */
#define HOUSE_KEEPING_DIR                       @"HouseKeeping"


#pragma mark -

/**
 * DirConfiguration private category
 */
@interface DirConfiguration(category)

/**
 * Returns application generic documents directory name
 *
 * @return Application generic documents directory name
 */
+ (NSString*) appDocumentsDirectory;

@end


#pragma mark -

@implementation DirConfiguration

#pragma mark -
#pragma mark Directory creation

/**
 * Creates application's directory structure
 */
+ (void)createDirectoryStructure {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];    
    
    NSString *downloadDir = [DirConfiguration downloadDirectory];
    
    if ([fileManager fileExistsAtPath:downloadDir] == NO) {
        
        [fileManager createDirectoryAtPath:downloadDir withIntermediateDirectories:YES attributes:nil error:nil];
        
    }
    
    NSString *houseKeepingDir = [DirConfiguration houseKeepingDirectory];
    
    if ([fileManager fileExistsAtPath:houseKeepingDir] == NO) {
        
        [fileManager createDirectoryAtPath:houseKeepingDir withIntermediateDirectories:YES attributes:nil error:nil];
        
    }

}

#pragma mark -
#pragma mark Directorys name access

/*
 * Returns application generic documents directory name
 */
+ (NSString *)appDocumentsDirectory {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
    
}

/*
 * Returns application's house keeping directory name
 */
+ (NSString *)houseKeepingDirectory {
    
    return [[DirConfiguration localDocumentsDirectory] stringByAppendingPathComponent:HOUSE_KEEPING_DIR];
    
}

/*
 * Returns application's download directory name
 */
+ (NSString *)downloadDirectory {
    
    return [[DirConfiguration localDocumentsDirectory] stringByAppendingPathComponent:DOWNLOAD_DIR];
    
}

/*
 * Returns application's documents directory name
 */
+ (NSString *)localDocumentsDirectory {
    
    return [[DirConfiguration appDocumentsDirectory] stringByAppendingPathComponent:LOCAL_GENERAL_DOCUMENTS_DIR];
    
}

@end
