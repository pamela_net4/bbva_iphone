/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ImagesCache.h"


#pragma mark -

/**
 * Stores the left and top cap stretching for an image
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface StretchingInformation : NSObject {
    
@private
    /**
     * Image left cap stretching
     */
    NSInteger leftCapStretching_;
    
    /**
     * Image top cap stretching
     */
    NSInteger topCapStretching_;
    
}

/**
 * Provides read-only access to the left cap stretching
 */
@property (nonatomic, readonly) NSInteger leftCapStretching;

/**
 * Provides read-only access to the top cap stretching
 */
@property (nonatomic, readonly) NSInteger topCapStretching;

/**
 * Initializes a StretchingInformation instance using the left and top cap stretching provided
 *
 * @param aLeftCapStretching The left cap stretching to store
 * @param aTopCapStretching The top cap stretching to store
 * @return The initialized StertchingInformation instance
 */
- (id)initWithLeftCapStertching:(NSInteger)aLeftCapStretching andTopCapStretching:(NSInteger)aTopCapStretching;

@end


#pragma mark -

@implementation StretchingInformation

#pragma mark -
#pragma mark Properties

@synthesize leftCapStretching = leftCapStretching_;
@synthesize topCapStretching = topCapStretching_;

#pragma mark -
#pragma mark Instance initialization

/*
 * Initializes a StretchingInformation instance using the left and top cap stretching provided
 */
- (id)initWithLeftCapStertching:(NSInteger)aLeftCapStretching andTopCapStretching:(NSInteger)aTopCapStretching {
    
    if (self = [super init]) {
        
        leftCapStretching_ = aLeftCapStretching;
        topCapStretching_ = aTopCapStretching;
        
    }
    
    return self;
    
}

@end


#pragma mark -

@implementation ImagesCache

#pragma mark -
#pragma mark Static attributes

/**
 * Singleton only instance
 */
static ImagesCache *imagesCacheInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([ImagesCache class]) {
        
        if (imagesCacheInstance_ == nil) {
            
            imagesCacheInstance_ = [super allocWithZone:zone];
            return imagesCacheInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (ImagesCache *)getInstance {
    
    if (imagesCacheInstance_ == nil) {
        
        @synchronized([ImagesCache class]) {
            
            if (imagesCacheInstance_ == nil) {
                
                imagesCacheInstance_ = [[ImagesCache alloc] init];
                
            }
            
        }
        
    }
    
    return imagesCacheInstance_;
    
}

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [imagesCache_ release];
    imagesCache_ = nil;
    
    [stretchingDictionary_ release];
    stretchingDictionary_ = nil;
    
    [super dealloc];
    
}

/*
 * Removes all cached images
 */
- (void)removeCachedImages {
    
    [imagesCache_ removeAllObjects];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Initializes an ImagesCache instance. Dictionaries are created and stretching dictionary is populated
 *
 * @return The initialized ImagesCache instance
 */
- (id)init {
    
    static BOOL initialized = NO;
    
    if (initialized == NO) {
        
    if (self = [super init]) {
        
        imagesCache_ = [[NSMutableDictionary alloc] init];
        stretchingDictionary_ = [[NSMutableDictionary alloc] init];
        
        doubleFactorImages_ = NO;
        tripleFactorImages_ = NO;
        
        UIScreen *screen = [UIScreen mainScreen];
        UIDevice *device = [UIDevice currentDevice];
        NSString *iOSVersion = device.systemVersion;
        
        if (([screen respondsToSelector:@selector(scale)]) &&
            ([iOSVersion compare:@"4.0" options:NSNumericSearch] != NSOrderedAscending)) {
            
            CGFloat scale = screen.scale;
            
            if (scale == 2.0f) {
                
                doubleFactorImages_ = YES;
            }else{
                tripleFactorImages_ = YES;
            }
            
        }
        
    }
        
        initialized = YES;
        
    }
    
    return self;
    
}

/*
 * Sets the left an top cap stretching for the image with the given name
 */
- (void)setStretchableLeftCapWidth:(NSInteger)aLeftCapWidth andTopCapHeight:(NSInteger)aTopCapHeight forImageName:(NSString *)anImageName {
    
    if (([anImageName length] > 0) && ((aLeftCapWidth > 0) || (aTopCapHeight > 0))) {
        
        if (![[stretchingDictionary_ allKeys] containsObject:anImageName]) {
            
            StretchingInformation *stretchingInfo = [[StretchingInformation alloc] initWithLeftCapStertching:aLeftCapWidth andTopCapStretching:aTopCapHeight];
            [stretchingDictionary_ setObject:stretchingInfo forKey:anImageName];
            [stretchingInfo release];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark Images cache

/*
 * Returns the image which file name is provided
 */
- (UIImage *)imageNamed:(NSString *)anImageFileName {
    
    
    UIImage *result = [imagesCache_ objectForKey:anImageFileName];
    
    if (result == nil) {
        
        NSString *filePath = nil;
        NSString *doubleResolutionFileName = nil;
        
        if (tripleFactorImages_) {
            
            NSString *fileExtension = [anImageFileName pathExtension];
            NSString *stringFormat = nil;
            
            if ([fileExtension length] > 0) {
                
                stringFormat = @"%@@3x.%@";
                
            } else {
                
                stringFormat = @"%@@3x%@";
            }
            
            doubleResolutionFileName = [NSString stringWithFormat:stringFormat, [anImageFileName stringByDeletingPathExtension], fileExtension];
            
            NSString *pathAndFileName = [[NSBundle mainBundle] pathForResource:doubleResolutionFileName ofType:nil];
            if (![[NSFileManager defaultManager] fileExistsAtPath:pathAndFileName]){
                doubleFactorImages_ = YES;
            }else{
                doubleFactorImages_ = NO;
            }

            
        }

        
        if (doubleFactorImages_) {
            
            NSString *fileExtension = [anImageFileName pathExtension];
            NSString *stringFormat = nil;
            
            if ([fileExtension length] > 0) {
                
                stringFormat = @"%@@2x.%@";
                
            } else {
            
                stringFormat = @"%@@2x%@";
            }
            
            doubleResolutionFileName = [NSString stringWithFormat:stringFormat, [anImageFileName stringByDeletingPathExtension], fileExtension];
            
            
        }
        
        if ([doubleResolutionFileName length] > 0) {
            
            NSString *extension = [doubleResolutionFileName pathExtension];
            NSString *fileName = [doubleResolutionFileName stringByDeletingPathExtension];
            
            filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:extension];
            result = [[[UIImage alloc] initWithContentsOfFile:filePath] autorelease];
            
            if ((result != nil) && ([UIImage instancesRespondToSelector:@selector(initWithCGImage:scale:orientation:)])) {
                
                result = [[[UIImage alloc] initWithCGImage:result.CGImage scale:2.0f orientation:UIImageOrientationUp] autorelease];
                
            }
            
        }
        
        if (result == nil) {
            
            NSString *extension = [anImageFileName pathExtension];
            NSString *fileName = [anImageFileName stringByDeletingPathExtension];
            
            filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:extension];
            result = [[[UIImage alloc] initWithContentsOfFile:filePath] autorelease];

        }
        
        if (result != nil) {
            
            StretchingInformation *stretchingInfo = [stretchingDictionary_ objectForKey:anImageFileName];
            
            if (stretchingInfo != nil) {
                
                result = [result stretchableImageWithLeftCapWidth:stretchingInfo.leftCapStretching topCapHeight:stretchingInfo.topCapStretching];
                
            }
            
            [imagesCache_ setObject:result forKey:anImageFileName];
            
        }
        
    }
    
    return result;
    
}

@end
