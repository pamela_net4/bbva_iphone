/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


/**
 * Implement common methods for singleton classes. These methods are related to
 * retain/release cycle. Singletons can derive from this class to avoid implementing
 * those methods
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface SingletonBase : NSObject {

}

@end
