/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "Session.h"

#import "ListaCuentas.h"
#import "AccountList.h"
#import "AccountTransaction.h"
#import "AccountTransactionDetail.h"
#import "AccountTransactionDetailResponse.h"
#import "AccountTransactionList.h"
#import "BankAccount.h"
#import "Card.h"
#import "CardList.h"
#import "Constants.h"
#import "Deposit.h"
#import "DepositList.h"
#import "GlobalAdditionalInformation.h"
#import "Loan.h"
#import "LoanList.h"
#import "MutualFund.h"
#import "MutualFundList.h"
#import "StockMarketAccount.h"
#import "StockMarketAccountList.h"
#import "Tools.h"
#import "Updater.h"


#pragma mark -

/**
 * AccountTransactionsListViewController private extension
 */
@interface Session()

/**
 * The transaction has been updated
 * @private
 */
- (void)transactionUpdated:(NSNotification *)notification;

@end

#pragma mark -

@implementation Session

#pragma mark -
#pragma mark Properties

@synthesize additionalInformation = additionalInformation_;
@synthesize accountList = accountList_;
@synthesize cardList = cardList_;
@synthesize depositsList = depositsList_;
@synthesize mutualFundsList = mutualFundsList_;
@synthesize loansList = loansList_;
@synthesize stockMarketAccountsList = stockMarketAccountsList_;
@synthesize globalPositionRequestServerData = globalPositionRequestServerData_;
@synthesize mobileCarriers = mobileCarriers_;
@synthesize accounts = accounts_;
@synthesize isLogged = isLogged_;
@synthesize localityDictionary = localityDictionary_;
@synthesize listaCuentas = listaCuentas_;

#pragma mark -
#pragma mark Static attributes

/**
 * Singleton only instance
 */
static Session *sessionInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([Session class]) {
        
        if (sessionInstance_ == nil) {
            
            sessionInstance_ = [super allocWithZone:zone];
            return sessionInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (Session *)getInstance {
    
    if (sessionInstance_ == nil) {
        
        @synchronized([Session class]) {
            
            if (sessionInstance_ == nil) {
                
                sessionInstance_ = [[Session alloc] init];
                
            }
            
        }
        
    }
    
    return sessionInstance_;
    
}

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void) dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationAccountTransactionDetailResponseReceived 
                                                  object:nil];
    
    [additionalInformation_ release];
    additionalInformation_ = nil;
    
    [accountList_ release];
    accountList_ = nil;
    
    [listaCuentas_ release];
    listaCuentas_ = nil;
    
    [cardList_ release];
    cardList_ = nil;
    
    [depositsList_ release];
    depositsList_ = nil;
    
    [mutualFundsList_ release];
    mutualFundsList_ = nil;

    [loansList_ release];
    loansList_ = nil;
	
	[stockMarketAccountsList_ release];
    stockMarketAccountsList_ = nil;
    
    [mobileCarriers_ release];
    mobileCarriers_ = nil;
    
    [accounts_ release];
    accounts_ = nil;
    
    [updatingTransaction_ release];
    updatingTransaction_ = nil;
    
    [updatingAccount_ release];
    updatingAccount_ = nil;
    
    [localityDictionary_ release];
    localityDictionary_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initalizer. Initializes session properties and instances
 *
 * @return The initialized Session instance
 */
- (id)init {
    
    if ((self = [super init])) {
        
        additionalInformation_ = [[GlobalAdditionalInformation alloc] init];
        accountList_ = [[AccountList alloc] init];
        cardList_ = [[CardList alloc] init];
        depositsList_ = [[DepositList alloc] init];
        mutualFundsList_ = [[MutualFundList alloc] init];
        loansList_ = [[LoanList alloc] init];
		stockMarketAccountsList_ = [[StockMarketAccountList alloc] init];
        
        listaCuentas_ = [[ListaCuentas alloc] init];
        
        globalPositionRequestServerData_ = YES;
        
        localityDictionary_ = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:@"1",
																	 @"41",
																	 @"43", 
																	 @"83", 
																	 @"54",
																	 @"66",
																	 @"76",
																	 @"63",
																	 @"84",
																	 @"67",
																	 @"62",
																	 @"56",
																	 @"64",
																	 @"44",
																	 @"74",
																	 @"65",
																	 @"82",
																	 @"53",
																	 @"73",
																	 @"51",
																	 @"92",
																	 @"52",
																	 @"72",
																	 @"61",
																	 nil]
															forKeys:[NSArray arrayWithObjects:@"Lima-Callao",
																	 @"Amazonas",
																	 @"Ancash", 
																	 @"Apurimac", 
																	 @"Arequipa",
																	 @"Ayacucho",
																	 @"Cajamarca",
																	 @"Cerro de Pasco",
																	 @"Cusco",
																	 @"Huancavelica",
																	 @"Huanuco",
																	 @"Ica",
																	 @"Junin",
																	 @"La Libertad",
																	 @"Lambayeque",
																	 @"Loreto",
																	 @"Madre de Dios",
																	 @"Moquegua",
																	 @"Piura",
																	 @"Puno",
																	 @"San Martin",
																	 @"Tacna",
																	 @"Tumbes",
																	 @"Ucayali",
																	 nil]];

		
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(transactionUpdated:)
                                                 name:kNotificationAccountTransactionDetailResponseReceived
                                               object:nil];
    
    return self;
    
}

/*
 * Remove session data
 */
- (void)removeData {
    
    [listaCuentas_ removeData];
    
    [accountList_ removeData];
    [cardList_ removeData];
    [depositsList_ removeData];
    [mutualFundsList_ removeData];
    [loansList_ removeData];
	[stockMarketAccountsList_ removeData];
    [additionalInformation_ removeData];

    [mobileCarriers_ release];
    mobileCarriers_ = nil;
    
    [updatingTransaction_ release];
    updatingTransaction_ = nil;
    
    [updatingAccount_ release];
    updatingAccount_ = nil;
    
    [accounts_ release];
    accounts_ = nil;
    
    [listaCuentas_ release];
    listaCuentas_ = nil;
    
	globalPositionRequestServerData_ = YES;
	
}


/**
 * Update transactions for an account
 *
 * @param account The account
 */
- (void)updateTransactionsForAccount:(BankAccount *)account {

    if (updatingAccount_ != nil) {
        [updatingAccount_ release];
        updatingAccount_ = nil;
    }
    
    updatingAccount_ = [account retain];
    
    BOOL firstFound = NO;
    
    AccountTransactionList *accountTransactionsList = account.accountTransactionsList;
     
    NSArray *array = [[NSArray alloc] initWithArray:accountTransactionsList.accountTransactionList];
    
    for (AccountTransaction *transaction in array) {  //while mejor
        
        if ((!transaction.detailDownloaded) && !firstFound) {
            
            if (transaction != updatingTransaction_) {
                
                [transaction retain];
                [updatingTransaction_ release];
                updatingTransaction_ = transaction;
                
            }
            
            firstFound = YES;
            
            [[Updater getInstance] obtainAccountTransactionDetailForAccountNumber:[account number]
                                                                transactionNumber:[transaction number]];
            
        }
        
    }
    
    [array release];

}

#pragma mark -
#pragma mark Notifications


/*
 * The transaction has been updated
 */
- (void)transactionUpdated:(NSNotification *)notification {
    
    //DLog(@"transactionUpdated");
    
    AccountTransactionDetailResponse *response = [notification object];
    AccountTransactionDetail *detail = [response accountTransactionDetail];
    
    NSArray *transactionList = updatingAccount_.accountTransactionsList.accountTransactionList;
    
    NSInteger updatingTransactionIndex = [transactionList indexOfObject:updatingTransaction_];
        
    [updatingTransaction_ setDeatilDescription:[detail description]];
    [updatingTransaction_ setOperationHour:[detail operationHour]];
    [updatingTransaction_ setAccountingDateString:[detail accountingDateString]];
    [updatingTransaction_ setAccountingDate:[detail accountingDate]];
    [updatingTransaction_ setCheckNumber:[detail checkNumber]];
    [updatingTransaction_ setType:[detail type]];
    [updatingTransaction_ setCenter:[detail center]];
    [updatingTransaction_ setLiteral:[detail literal]];
    updatingTransaction_.detailDownloaded = YES;
    
    
    NSInteger transactionsListCount = [transactionList count];
    
    updatingTransactionIndex = updatingTransactionIndex + 1;
    
    if (updatingTransactionIndex < transactionsListCount) {
        
        AccountTransaction *transaction = [transactionList objectAtIndex:updatingTransactionIndex];
        
        if (transaction != updatingTransaction_) {
            
            [transaction retain];
            [updatingTransaction_ release];
            updatingTransaction_ = transaction;
            
        }
        
        [[Updater getInstance] obtainAccountTransactionDetailForAccountNumber:[updatingAccount_ number]
                                                            transactionNumber:[updatingTransaction_ number]];        
        
    }
    
}


@end
