/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>


/**
 * Category to include the URL enconding selector and HTML escaping into all NSStrings
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NSString(URLAndHTMLUtils)

/**
 * Returns an autoreleased NSString with the URL encoded version of the receiving NSString using the provided encoding
 * 
 * @param anEncoding The encoding to use
 * @return The autoreleased NSString with the URL encoded version of the receiving NSString
 */
- (NSString *)urlEncodeUsingEncoding:(NSStringEncoding)anEncoding;

/**
 * Returns an autoreleased NSString with the HTML escaped characters transformed into the correct characters
 *
 * @return The autoreleased NSString with the HTML escaped characters transformed
 */
- (NSString *)htmlEscapedCharactersTransformed;

@end
