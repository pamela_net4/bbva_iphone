/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "SingletonBase.h"


/**
 * Image cache to access all images in the application. If necessary, images are stretched
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ImagesCache : SingletonBase {
    
@private
    
    /**
     * Dictionary to store appliation images referenced by file name
     */
    NSMutableDictionary* imagesCache_;
    
    /**
     * Dictionary to store the stretching applied to every image (if no stretching is needed,
     * there no entry will exist) Stretching information is associated to the file name
     */
    NSMutableDictionary* stretchingDictionary_;
    
    /**
     * Use double factor images flag. When executing on an iPhone 4 or any device with double pixel densitiy, this flag is YES, otherwise, it is NO
     */
    BOOL doubleFactorImages_;
    
    BOOL tripleFactorImages_;
}

/**
 * Provides access to the singleton only instance
 *
 * @return The singleton only instance
 */
+ (ImagesCache *)getInstance;

/**
 * Sets the left an top cap stretching for the image with the given name
 *
 * @param aLeftCapWidth The image left cap width
 * @param aTopCapHeight The image top cap height
 * @param anImageName The image name to set its left an top cap stretching
 */
- (void)setStretchableLeftCapWidth:(NSInteger)aLeftCapWidth andTopCapHeight:(NSInteger)aTopCapHeight forImageName:(NSString *)anImageName;

/**
 * Returns the image which file name is provided
 *
 * @param anImageFileName The file's name where the image is stored
 * @return The image stored at the given file. If necessary, stretching is applied
 */
- (UIImage *)imageNamed:(NSString *)anImageFileName;

/**
 * Removes all cached images
 */
- (void)removeCachedImages;

@end
