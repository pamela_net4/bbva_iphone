/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "UIView+Frame.h"

@implementation UIView(Frame)

@dynamic originX;
@dynamic originY;
@dynamic width;
@dynamic height;

/**
 * Gets frame.origin.x value
 *
 * @return CGFloat value
 */
- (CGFloat)originX {
    return CGRectGetMinX(self.frame);
}

/**
 * Sets frame.origin.x value
 *
 * @param originX The X origin
 */
- (void)setOriginX:(CGFloat)originX {
    
    CGRect frame = self.frame;
    frame.origin.x = originX;
    self.frame = frame;
}

/**
 * Gets frame.origin.y value
 *
 * @return CGFloat value
 */
- (CGFloat)originY {
    return CGRectGetMinY(self.frame);
}

/**
 * Sets frame.origin.y value
 *
 * @param originY The Y origin
 */
- (void)setOriginY:(CGFloat)originY {
    
    CGRect frame = self.frame;
    frame.origin.y = originY;
    self.frame = frame;
}

/**
 * Gets frame.size.width value
 *
 * @return CGFloat value
 */
- (CGFloat)width {
    return CGRectGetWidth(self.frame);
}

/**
 * Sets frame.size.width value
 *
 * @param width The width to set
 */
- (void)setWidth:(CGFloat)width {
    
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

/**
 * Gets frame.size.height value
 *
 * @return CGFloat value
 */
- (CGFloat)height {
    return CGRectGetHeight(self.frame);
}

/**
 * Sets frame.size.height value
 *
 * @param height The height to set
 */
- (void)setHeight:(CGFloat)height {
    
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

@end
