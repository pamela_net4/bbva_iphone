/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#pragma mark -
#pragma mark Splash string key

/* Splash view strings */

#define TXT_SPLASH_VIEW_VERSION_KEY															@"TXT_SPLASH_VIEW_VERSION"
#define TXT_SPLASH_VIEW_ERROR_KEY															@"TXT_SPLASH_VIEW_ERROR"
#define TXT_SPLASH_VIEW_TRY_AGAIN_KEY														@"TXT_SPLASH_VIEW_TRY_AGAIN"


#pragma mark -
#pragma mark Miscelaneous string keys

/* Defines the Google Maps Directions API strings keys */

#define GOOGLE_MAPS_DIRECTIONS_API_LANGUAGE_CODE_KEY                                        @"GOOGLE_MAPS_DIRECTIONS_API_LANGUAGE_CODE"

/**
 * Defines the miscelaneous string keys
 */
#define INFO_MESSAGE_TITLE_KEY                                                              @"INFO_MESSAGE_TITLE"
#define OK_TEXT_KEY                                                                         @"OK_TEXT"
#define BACK_TEXT_KEY                                                                       @"BACK_TEXT"
#define ALERT_MESSAGE_TITLE_KEY                                                             @"ALERT_MESSAGE_TITLE"
#define YES_TEXT_KEY                                                                        @"YES_TEXT"
#define NO_TEXT_KEY                                                                         @"NO_TEXT"
#define WARNING_MESSAGE_TITLE_KEY                                                           @"WARNING_MESSAGE_TITLE"
#define COMMUNICATIONS_ERROR_TEXT_KEY                                                       @"COMMUNICATIONS_ERROR_TEXT"
#define CLOSE_TEXT_KEY                                                                      @"CLOSE_TEXT"
#define ENTER_CONCEPT_TEXT_KEY                                                              @"ENTER_CONCEPT_TEXT"
#define EMAIL_NOT_CONFIGURED_ERROR_TEXT_KEY                                                 @"EMAIL_NOT_CONFIGURED_ERROR_TEXT"
#define EMAIL_SENT_TEXT_KEY                                                                 @"EMAIL_SENT_TEXT"
#define DISCONNECT_TEXT_KEY                                                                 @"DISCONNECT_TEXT"
#define AVAILABLE_AMOUNT_TEXT_KEY                                                           @"AVAILABLE_AMOUNT_TEXT"
#define MUST_INTRODUCE_OTP_KEY_TEXT_KEY                                                     @"MUST_INTRODUCE_OTP_KEY_TEXT"


#define MUST_INTRODUCE_IS_OTP_SMSKEY_TEXT_KEY                                               @"MUST_INTRODUCE_IS_OTP_SMSKEY_TEXT"
#define MUST_INTRODUCE_IS_OTP_COORDINATES_TEXT_KEY                                          @"MUST_INTRODUCE_IS_OTP_COORDINATES_TEXT"

#define MUST_ACCEPT_LEGAL_TERMS_TEXT_KEY													@"MUST_ACCEPT_LEGAL_TERMS_TEXT"
#define MOVISTAR_CARRIER_TEXT_KEY                                                           @"MOVISTAR_CARRIER_TEXT"
#define CLARO_CARRIER_TEXT_KEY                                                              @"CLARO_CARRIER_TEXT"
#define NEXTEL_CARRIER_TEXT                                                                 @"NEXTEL_CARRIER_TEXT"
#define AVAILABLE_AMOUNT_TEXT_KEY                                                           @"AVAILABLE_AMOUNT_TEXT"
#define OPTIONAL_TEXT_KEY                                                                   @"OPTIONAL_TEXT"
#define CCC_ENTITY_TEXT_KEY                                                                 @"CCC_ENTITY_TEXT"
#define CCC_BRANCH_TEXT_KEY                                                                 @"CCC_BRANCH_TEXT"
#define CCC_ACCOUNT_TEXT_KEY                                                                @"CCC_ACCOUNT_TEXT"
#define CCC_CC_TEXT_KEY                                                                     @"CCC_CC_TEXT"
#define TELEPHONE_TEXT_KEY                                                                  @"TELEPHONE_TEXT"
#define ADDRESS_TEXT_KEY                                                                    @"ADDRESS_TEXT"
#define BBVA_CONTINNENTAL_TEXT_KEY                                                          @"BBVA_CONTINNENTAL_TEXT"
#define CONFIRM_TEXT_KEY                                                                    @"CONFIRM_TEXT"
#define DATE_FORMATER_TEXT_KEY																@"DATE_FORMATER_TEXT"
#define CONTINUE_TEXT_KEY                                                                   @"CONTINUE_TEXT"
#define DATE_OF_ISSUE_TEXT_KEY                                                              @"DATE_OF_ISSUE_TEXT"
#define STATE_TEXT_KEY                                                                      @"STATE_TEXT"
#define STATES_TEXT_KEY                                                                     @"STATES_TEXT"
#define TYPE_TEXT_KEY                                                                       @"TYPE_TEXT"
#define DOCUMENT_TITLE_KEY                                                                  @"DOCUMENT_TITLE"
#define CHANGE_TYPE_TITLE_KEY                                                               @"CHANGE_TYPE_TITLE"
#define CHANGE_TYPE_CAPITILIZE_TITLE_KEY                                                    @"CHANGE_TYPE_CAPITILIZE_TITLE"
#define EXECUTE_TITLE_KEY                                                                   @"EXECUTE_TITLE"
#define REGISTER_ACCOUNT_TITLE_KEY                                                                   @"REGISTER_ACCOUNT_TITLE"
#define DELETE_ACCOUNT_TITLE_KEY                                                                   @"DELETE_ACCOUNT_TITLE"
#define SERVICE_TITLE_KEY                                                                   @"SERVICE_TITLE"
#define CHANGE_TYPE_TITLE_KEY                                                               @"CHANGE_TYPE_TITLE"
#define NICKNAME_ACCOUNT_TEXT_KEY                                                            @"NICKNAME_ACCOUNT_TEXT"
#define OPERATION_TYPE_ACCOUNT_TEXT_KEY                                                            @"OPERATION_TYPE_ACCOUNT_TEXT"
#define ACCOUNT_NUMBER_TEXT_KEY                                                            @"ACCOUNT_NUMBER_TEXT"

#pragma mark -
#pragma mark Currecies strings

/**
 * Define the currencies string keys
 */
#define SOLES_CURRENCY_TEXT_KEY                                                             @"SOLES_CURRENCY_TEXT"
#define EUROS_CURRENCY_TEXT_KEY                                                             @"EUROS_CURRENCY_TEXT"
#define DOLLARS_CURRENCY_TEXT_KEY                                                           @"DOLLARS_CURRENCY_TEXT"


#pragma mark -
#pragma mark BBVA line string keys

/**
 * Defines the BBVA line string keys
 */
#define CUSTOMER_CARE_PHONE_CALL_TEXT_KEY                                                   @"CUSTOMER_CARE_PHONE_CALL_TEXT"
#define BBVA_LINE_DIALOG_TITLE_KEY                                                          @"BBVA_LINE_DIALOG_TITLE"
#define BBVA_LINE_DIALOG_MESSAGE_KEY                                                        @"BBVA_LINE_DIALOG_MESSAGE"
#define BBVA_LINE_DIALOG_CANCEL_BUTTON_TITLE_KEY                                            @"BBVA_LINE_DIALOG_CANCEL_BUTTON_TITLE"
#define BBVA_LINE_DIALOG_OK_BUTTON_TITLE_KEY                                                @"BBVA_LINE_DIALOG_OK_BUTTON_TITLE"
#define BBVA_LINE_DIALOG_TEXT_KEY															@"BBVA_LINE_DIALOG_TEXT"
#define CALL_TO_POI_PHONE_TEXT_KEY                                                          @"CALL_TO_POI_PHONE_TEXT"


#pragma mark -
#pragma mark Pop buttons view strings keys

/**
 * Defines the pop buttons view strings keys
 */
#define POP_BUTTONS_PREVIOUS_KEY                                                            @"POP_BUTTONS_PREVIOUS"
#define POP_BUTTONS_NEXT_KEY                                                                @"POP_BUTTONS_NEXT"
#define POP_BUTTONS_LIST_KEY                                                                @"POP_BUTTONS_LIST"
#define POP_BUTTONS_KEYBOARD_KEY                                                            @"POP_BUTTONS_KEYBOARD"
#define POP_BUTTONS_OK_KEY                                                                  @"POP_BUTTONS_OK"

#pragma mark -
#pragma mark Tab bar view controller string keys

/**
 * Defines the tab bar view controller string keys
 */
#define MORE_TABS_TAB_TEXT_KEY                                                              @"MORE_TABS_TAB_TEXT"


#pragma mark -
#pragma mark Login information cell string keys

/**
 * Defines the login information cell string keys
 */
#define USER_TEXT_KEY                                                                       @"USER_TEXT"
#define PASSWORD_TEXT_KEY                                                                   @"PASSWORD_TEXT"
#define HINT_USER_TEXT_KEY                                                                  @"HINT_USER_TEXT"
#define HINT_PASSWORD_TEXT_KEY                                                              @"HINT_PASSWORD_TEXT"
#define LOGIN_TEXT_KEY                                                                      @"LOGIN_TEXT"
#define LOGIN_HINT_TEXT_KEY                                                                 @"LOGIN_HINT_TEXT"
#define FORGOT_PASSWORD_HELPER_TEXT_KEY                                                     @"FORGOT_PASSWORD_HELPER_TEXT"
#define SUSCRIBE_TEXT_KEY                                                                   @"SUSCRIBE_TEXT"
#define HELP_US_IMPROVE_TEXT_KEY                                                            @"HELP_US_IMPROVE_TEXT"
#define HELP_US_IMPROVE_MAIL_SUBJECT_TEXT_KEY                                               @"HELP_US_IMPROVE_MAIL_SUBJECT_TEXT"
#define YOUR_COMMENTS_TEXT_KEY                                                              @"YOUR_COMMENTS_TEXT"
#define YOUR_COMMENTS_MAIL_SUBJECT_TEXT_KEY                                                 @"YOUR_COMMENTS_MAIL_SUBJECT_TEXT"
#define RECOMMEND_TO_A_FRIEND_TEXT_KEY                                                      @"RECOMMEND_TO_A_FRIEND_TEXT"
#define RECOMMEND_TO_A_FRIEND_MAIL_SUBJECT_TEXT_KEY                                         @"RECOMMEND_TO_A_FRIEND_MAIL_SUBJECT_TEXT"
#define RECOMMEND_TO_A_FRIEND_MAIL_BODY_TEXT_KEY                                            @"RECOMMEND_TO_A_FRIEND_MAIL_BODY_TEXT"

#define FREQUENT_CARD_TEXT_KEY                                                              @"FREQUENT_CARD_TEXT"
#define ALIAS_HINT_TEXT_KEY                                                                 @"ALIAS_HINT_TEXT"
#define ADD_FREQUENT_CARD_TEXT_KEY                                                          @"ADD_FREQUENT_CARD_TEXT"
#define REMEMBER_FREQUENT_CARD_TEXT_KEY                                                     @"REMEMBER_FREQUENT_CARD_TEXT"
#define DELETE_FREQUENT_CARD_TEXT_KEY                                                       @"DELETE_FREQUENT_CARD_TEXT"


#pragma mark -
#pragma mark Login view controller string keys

/**
 * Defines the login view controller string keys
 */
#define MOBILE_BANKING_ACCESS_TEXT_KEY                                                      @"MOBILE_BANKING_ACCESS_TEXT"
#define ATM_AND_BRANCHES_LOCATOR_TEXT_KEY                                                   @"ATM_AND_BRANCHES_LOCATOR_TEXT"
#define SIGNUP_TEXT_KEY                                                                     @"SIGNUP_TEXT"
#define LOGIN_INFO_TEXT_KEY                                                                 @"LOGIN_INFO_TEXT"
#define BENEFITS_FOR_YOU_OPTION_TEXT_KEY                                                    @"BENEFITS_FOR_YOU_OPTION_TEXT"
#define CONTACT_US_TEXT_KEY                                                                 @"CONTACT_US_TEXT"
#define CALL_US_TEXT_KEY                                                                    @"CALL_US_TEXT"
#define YOUR_OPINION_TEXT_KEY                                                               @"YOUR_OPINION_TEXT"
#define CONFIRM_DELETE_FREQUENT_OPERATION_TEXT_KEY                                          @"CONFIRM_DELETE_FREQUENT_OPERATION_TEXT"


#pragma mark -
#pragma mark Login check error messages string keys

/**
 Defines the login check error messages string keys
 */
#define LOGIN_ERROR_USER_EMPTY_KEY                                                          @"LOGIN_ERROR_USER_EMPTY"
#define LOGIN_ERROR_INVALID_USER_KEY                                                        @"LOGIN_ERROR_INVALID_USER"
#define LOGIN_ERROR_PASSWORD_EMPTY_KEY                                                      @"LOGIN_ERROR_PASSWORD_EMPTY"
#define LOGIN_ERROR_ALIAS_EMPTY_KEY                                                         @"LOGIN_ERROR_ALIAS_EMPTY"
#define LOGIN_ERROR_ALIAS_ALREADY_EXISTS_KEY                                                @"LOGIN_ERROR_ALIAS_ALREADY_EXISTS"
#define LOGIN_ERROR_USER_AND_PASSWORD_EMPTY_KEY                                             @"LOGIN_ERROR_USER_AND_PASSWORD_EMPTY"
#define LOGIN_ERROR_USER_INVALID_KEY                                                        @"LOGIN_ERROR_USER_INVALID"
#define LOGIN_ERROR_PASSWORD_SYMBOLS_KEY                                                    @"LOGIN_ERROR_PASSWORD_SYMBOLS"

#pragma mark -
#pragma mark Signup information popup

#define SIGNUP_TITLE_TEXT_KEY                                                               @"SIGNUP_TITLE_TEXT"
#define SIGNUP_HEADER_TEXT_KEY                                                              @"SIGNUP_HEADER_TEXT"
#define SIGNUP_STEP1_TEXT_KEY                                                               @"SIGNUP_STEP1_TEXT"
#define SIGNUP_STEP2_TEXT_KEY                                                               @"SIGNUP_STEP2_TEXT"
#define SIGNUP_STEP3_TEXT_KEY                                                               @"SIGNUP_STEP3_TEXT"
#define SIGNUP_STEP3_BOLD_TEXT_KEY                                                          @"SIGNUP_STEP3_BOLD_TEXT"
#define SIGNUP_STEP4_TEXT_KEY                                                               @"SIGNUP_STEP4_TEXT"
#define SIGNUP_STEP4_BOLD_TEXT_KEY                                                          @"SIGNUP_STEP4_BOLD_TEXT"
#define SIGNUP_URL_KEY                                                                      @"SIGNUP_URL"


#pragma mark -
#pragma mark Forgot password information popup

#define FORGOT_PASSWORD_TITLE_TEXT_KEY                                                      @"FORGOT_PASSWORD_TITLE_TEXT"
#define FORGOT_PASSWORD_HEADER_TEXT_KEY                                                     @"FORGOT_PASSWORD_HEADER_TEXT"
#define FORGOT_PASSWORD_STEP1_TEXT_KEY                                                      @"FORGOT_PASSWORD_STEP1_TEXT"
#define FORGOT_PASSWORD_STEP2_TEXT_KEY                                                      @"FORGOT_PASSWORD_STEP2_TEXT"
#define FORGOT_PASSWORD_STEP2_BOLD_TEXT_KEY                                                 @"FORGOT_PASSWORD_STEP2_BOLD_TEXT"
#define FORGOT_PASSWORD_STEP3_TEXT_KEY                                                      @"FORGOT_PASSWORD_STEP3_TEXT"
#define FORGOT_PASSWORD_STEP3_BOLD_TEXT_KEY                                                 @"FORGOT_PASSWORD_STEP3_BOLD_TEXT"
#define FORGOT_PASSWORD_URL_KEY                                                             @"FORGOT_PASSWORD_URL"


#pragma mark -
#pragma mark Request coordinates

#define COORDINATE_REQUEST_TITLE_TEXT_KEY                                                   @"COORDINATE_REQUEST_TITLE_TEXT"
#define COORDINATE_REQUEST_HEADER_TEXT_KEY                                                  @"COORDINATE_REQUEST_HEADER_TEXT"
#define COORDINATE_REQUEST_INFO_TEXT_KEY                                                    @"COORDINATE_REQUEST_INFO_TEXT"
#define COORDINATE_REQUEST_PASSWORD_EMPTY_ERROR_TEXT                                        @"COORDINATE_REQUEST_PASSWORD_EMPTY_ERROR_TEXT"

#pragma mark -
#pragma mark Route steps strings

/* Define the route steps strings keys */

#define ADVANCE_STEP_TEXT_KEY                                                               @"ADVANCE_STEP_TEXT"
#define ARRIVAL_STEP_TEXT_KEY                                                               @"ARRIVAL_STEP_TEXT"

#pragma mark -
#pragma mark Login operation error messages string keys

/**
 * Defines the login operation error mesages string keys
 */
#define LOGIN_ERROR_SERVER_ERROR_KEY                                                        @"LOGIN_ERROR_SERVER_ERROR"

#pragma mark -
#pragma mark General error messages string keys

/* General error messages */
#define COMMUNICATION_ERROR_KEY                                                             @"COMMUNICATION_ERROR"
#define GENERAL_ERROR_KEY																	@"GENERAL_ERROR"
#define GENERAL_ERROR2_KEY																	@"GENERAL_ERROR2"
#define DISCONNECTED_MESSAGE_KEY                                                            @"DISCONNECTED_MESSAGE"
#define SESSION_EXPIRED_ERROR_KEY                                                           @"SESSION_EXPIRED_ERROR"

#pragma mark -
#pragma mark Connected tab bar buttons texts

/* Connected tab bar buttons texts */
#define GLOBAL_POSITION_TAB_BAR_BUTTON_TEXT_KEY                                             @"GLOBAL_POSITION_TAB_BAR_BUTTON_TEXT"
#define TRANSFERS_POSITION_TAB_BAR_BUTTON_TEXT_KEY                                          @"TRANSFERS_POSITION_TAB_BAR_BUTTON_TEXT"
#define POI_MAP_POSITION_TAB_BAR_BUTTON_TEXT_KEY                                            @"POI_MAP_POSITION_TAB_BAR_BUTTON_TEXT"

#pragma mark -
#pragma mark Global position strings

/**
 * Defines the global position view texts
 */
#define GLOBAL_POSITION_TITLE_TEXT_KEY                                                      @"GLOBAL_POSITION_TITLE_TEXT"
#define TXT_GLOBAL_POSITION_WELCOME_KEY                                                     @"TXT_GLOBAL_POSITION_WELCOME"
#define TRANSFERS_POSITION_TITLE_TEXT_KEY                                                   @"TRANSFERS_POSITION_TITLE_TEXT"
#define RESEND_POSITION_TITLE_TEXT_KEY                                                      @"RESEND_POSITION_TITLE_TEXT"
#define LOCATION_POSITION_TITLE_TEXT_KEY                                                    @"LOCATION_POSITION_TITLE_TEXT"
#define SAVINGS_TITLE_TEXT_KEY                                                              @"SAVINGS_TITLE_TEXT"
#define INVESTMENTS_TITLE_TEXT_KEY                                                          @"INVESTMENTS_TITLE_TEXT"
#define PRODUCTS_TITLE_TEXT_KEY                                                             @"PRODUCTS_TITLE_TEXT"
#define FINANCING_TITLE_TEXT_KEY                                                            @"FINANCING_TITLE_TEXT"
#define ACCOUNTS_PRODUCT_GROUP_KEY                                                          @"ACCOUNTS_PRODUCT_GROUP"
#define STOCKS_PRODUCT_GROUP_KEY                                                            @"STOCKS_PRODUCT_GROUP"
#define DEPOSITS_PRODUCT_GROUP_KEY                                                          @"DEPOSITS_PRODUCT_GROUP"
#define FUNDS_PRODUCT_GROUP_KEY                                                             @"FUNDS_PRODUCT_GROUP"
#define RETIREMENT_PLANS_PRODUCT_GROUP_KEY                                                  @"RETIREMENT_PLANS_PRODUCT_GROUP"
#define LOANS_PRODUCT_GROUP_KEY                                                             @"LOANS_PRODUCT_GROUP"
#define CARDS_PRODUCT_GROUP_KEY                                                             @"CARDS_PRODUCT_GROUP"
#define INTERNET_SHOPPING_GROUP_KEY                                                         @"INTERNET_SHOPPING_GROUP"
#define TERM_ACCOUNTS_PRODUCT_GROUP_KEY                                                     @"TERM_ACCOUNTS_PRODUCT_GROUP"
#define CUSTOMER_AVAILABLE_TEXT_KEY                                                         @"CUSTOMER_AVAILABLE_TEXT"

//
#define CUSTOMER_TITLE_KEY                                                                  @"CUSTOMER_TITLE"
#define ACCUMULATED_POINTS_TITLE_KEY                                                        @"ACCUMULATED_POINTS_TITLE"
#define EXCHANGE_TITLE_KEY                                                                  @"EXCHANGE_TITLE"
#define PURCHASE_EXCHANGE_TITLE_KEY                                                         @"PURCHASE_EXCHANGE_TITLE"
#define SALE_EXCHANGE_TITLE_KEY                                                             @"SALE_EXCHANGE_TITLE"
#define CURRENT_DATE_TITLE_KEY                                                              @"CURRENT_DATE_TITLE"
#define LAST_ACCESS_TITLE_KEY                                                               @"LAST_ACCESS_TITLE"
#define VIP_TITLE_KEY                                                                       @"VIP_TITLE"
//
#define GP_HEADER_MENU_OPTION_IAC_KEY                                                       @"GP_HEADER_MENU_OPTION_IAC"
#define GP_HEADER_MENU_OPTION_CONTACT_KEY                                                   @"GP_HEADER_MENU_OPTION_CONTACT"
//
#define ACCOUNT_AMOUNT_HEADER2_KEY                                                          @"ACCOUNT_AMOUNT_HEADER2"
#define ACCOUNT_AMOUNT_HEADER3_KEY                                                          @"ACCOUNT_AMOUNT_HEADER3"
#define DEPOSIT_AMOUNT_HEADER2_KEY                                                          @"DEPOSIT_AMOUNT_HEADER2"
#define DEPOSIT_AMOUNT_HEADER3_KEY                                                          @"DEPOSIT_AMOUNT_HEADER3"
#define TERM_ACCOUNT_AMOUNT_HEADER2_KEY                                                     @"TERM_ACCOUNT_AMOUNT_HEADER2"
#define TERM_ACCOUNT_AMOUNT_HEADER3_KEY                                                     @"TERM_ACCOUNT_AMOUNT_HEADER3"
#define MUTUAL_FUND_AMOUNT_HEADER1_KEY                                                      @"MUTUAL_FUND_AMOUNT_HEADER1"
#define MUTUAL_FUND_AMOUNT_HEADER2_KEY                                                      @"MUTUAL_FUND_AMOUNT_HEADER2"
#define MUTUAL_FUND_AMOUNT_HEADER3_KEY                                                      @"MUTUAL_FUND_AMOUNT_HEADER3"
#define STOCK_MARKET_AMOUNT_HEADER2_KEY                                                     @"STOCK_MARKET_AMOUNT_HEADER2"
#define STOCK_MARKET_AMOUNT_HEADER3_KEY                                                     @"STOCK_MARKET_AMOUNT_HEADER3"
#define CARD_AMOUNT_HEADER1_KEY                                                             @"CARD_AMOUNT_HEADER1"
#define CARD_AMOUNT_HEADER2_KEY                                                             @"CARD_AMOUNT_HEADER2"
#define CARD_AMOUNT_HEADER3_KEY                                                             @"CARD_AMOUNT_HEADER3"
#define LOAN_AMOUNT_HEADER2_KEY                                                             @"LOAN_AMOUNT_HEADER2"
#define LOAN_AMOUNT_HEADER3_KEY                                                             @"LOAN_AMOUNT_HEADER3"
#define LOAN_PASS_DEBT_TEXT_KEY                                                             @"LOAN_PASS_DEBT_TEXT"
#define LOAN_PENDING_TEXT_KEY                                                               @"LOAN_PENDING_TEXT"
#define LOAN_DEBT_TEXT_KEY                                                                  @"LOAN_DEBT_TEXT"

#pragma mark -
#pragma mark IAC Popup string keys

#define IAC_POPUP_TITLE_TEXT_KEY                                                            @"IAC_POPUP_TITLE_TEXT"
#define IAC_ACCOUNT_TITLE_TEXT_KEY                                                          @"IAC_ACCOUNT_TITLE_TEXT"
#define IAC_IAC_TITLE_TEXT_KEY                                                              @"IAC_IAC_TITLE_TEXT"
#define IAC_INFO_TEXT_KEY                                                                   @"IAC_INFO_TEXT"


#pragma mark -
#pragma mark POI location mapa messages string keys

/**
 * Defines the POI location map messages string keys
 */
#define ORIGIN_LOCATION_TEXT_KEY                                                            @"ORIGIN_LOCATION_TEXT"
#define DESTINATION_LOCATION_TEXT_KEY                                                       @"DESTINATION_LOCATION_TEXT"
#define UNABLE_TO_OBTAIN_ROUTE_ERROR_KEY                                                    @"UNABLE_TO_OBTAIN_ROUTE_ERROR"
#define POI_LOCATION_MAP_NAVIGATION_BAR_TITLE_KEY                                           @"POI_LOCATION_MAP_NAVIGATION_BAR_TITLE"
#define UNABLE_TO_USE_LOCATION_SERVICE_KEY                                                  @"UNABLE_TO_USE_LOCATION_SERVICE"
#define ERROR_LOCATING_USER_KEY                                                             @"ERROR_LOCATING_USER"
#define UNABLE_TO_OBTAIN_USER_LOCATION_ADDRESS_KEY                                          @"UNABLE_TO_OBTAIN_USER_LOCATION_ADDRESS"
#define ATM_TITLE_START_STRING_KEY                                                          @"ATM_TITLE_START_STRING"
#define EXPRESS_AGENT_TITLE_START_STRING_KEY                                                @"EXPRESS_AGENT_TITLE_START_STRING"
#define BRANCH_TITLE_START_STRING_KEY                                                       @"BRANCH_TITLE_START_STRING"
#define ABOUT_ROUTE_TEXT_KEY                                                                @"ABOUT_ROUTE_TEXT"
#define LOCATION_TEXT_KEY                                                                   @"LOCATION_TEXT"    
#define BBVA_TEXT_KEY                                                                       @"BBVA_TEXT"
#define SEARCH_TEXT_KEY                                                                     @"SEARCH_TEXT"
#define LOCATION_STEPS_TEXT_KEY                                                             @"LOCATION_STEPS_TEXT"
#define TECNICAL_ERROR_TEXT_KEY                                                             @"TECNICAL_ERROR_TEXT"
#define MAP_FILTER_CONFIGURATION_TITLE_KEY                                                  @"MAP_FILTER_CONFIGURATION_TITLE"
#define MAP_FILTER_BRANCH_BBVA_TEXT_KEY                                                     @"MAP_FILTER_BRANCH_BBVA_TEXT"
#define MAP_FILTER_ATM_BBVA_TEXT_KEY                                                        @"MAP_FILTER_ATM_BBVA_TEXT"
#define MAP_FILTER_ATM_4B_TEXT_KEY                                                          @"MAP_FILTER_ATM_4B_TEXT"
#define MAP_FILTER_ATM_SERVIRED_TEXT_KEY                                                    @"MAP_FILTER_ATM_SERVIRED_TEXT"
#define MAP_FILTER_ATM_E6000_TEXT_KEY                                                       @"MAP_FILTER_ATM_E6000_TEXT"
#define MAP_SEARCH_PLACEHOLDER_TEXT_KEY                                                     @"MAP_SEARCH_PLACEHOLDER_TEXT"
#define SELECTED_ONE_LOCATION_TEXT_KEY														@"SELECTED_ONE_LOCATION_TEXT"

#pragma mark -
#pragma mark ATM and branches download messages

/**
 * Defines the ATM and branches download messages string keys
 */
#define CLIENT_ERROR_DOWNLOADING_ATM_LIST_KEY                                               @"CLIENT_ERROR_DOWNLOADING_ATM_LIST"
#define CLIENT_ERROR_DOWNLOADING_EXPRESS_AGENTS_LIST_KEY                                    @"CLIENT_ERROR_DOWNLOADING_EXPRESS_AGENTS_LIST"
#define CLIENT_ERROR_DOWNLOADING_BRANCHES_LIST_KEY                                          @"CLIENT_ERROR_DOWNLOADING_BRANCHES_LIST"
#define SERVER_ERROR_DOWNLOADING_ATM_LIST_KEY                                               @"SERVER_ERROR_DOWNLOADING_ATM_LIST"
#define SERVER_ERROR_DOWNLOADING_EXPRESS_AGENTS_LIST_KEY                                    @"SERVER_ERROR_DOWNLOADING_EXPRESS_AGENTS_LIST"
#define SERVER_ERROR_DOWNLOADING_BRANCHES_LIST_KEY                                          @"SERVER_ERROR_DOWNLOADING_BRANCHES_LIST"
#define COMMUNICATIONS_ERROR_DOWNLOADING_ATM_LIST_KEY                                       @"COMMUNICATIONS_ERROR_DOWNLOADING_ATM_LIST"
#define COMMUNICATIONS_ERROR_DOWNLOADING_BRANCHES_LIST_KEY                                  @"COMMUNICATIONS_ERROR_DOWNLOADING_BRANCHES_LIST"
#define ERROR_SENDING_ATMS_LIST_REQUEST_KEY                                                 @"ERROR_SENDING_ATMS_LIST_REQUEST"
#define ERROR_SENDING_EXPRESS_AGENTS_LIST_REQUEST_KEY                                       @"ERROR_SENDING_EXPRESS_AGENTS_LIST_REQUEST"
#define ERROR_SENDING_BANCHES_LIST_REQUEST_KEY                                              @"ERROR_SENDING_BANCHES_LIST_REQUEST"
#define ERROR_SENDING_ATMS_AND_BRANCHES_LISTS_REQUEST_KEY                                   @"ERROR_SENDING_ATMS_AND_BRANCHES_LISTS_REQUEST"
#define ERROR_SENDING_ATMS_AND_EXPRESS_AGENTS_LISTS_REQUEST_KEY                             @"ERROR_SENDING_ATMS_AND_EXPRESS_AGENTS_LISTS_REQUEST"
#define ERROR_SENDING_EXPRESS_AGENTS_AND_BRANCHES_LISTS_REQUEST_KEY                         @"ERROR_SENDING_EXPRESS_AGENTS_AND_BRANCHES_LISTS_REQUEST"
#define ERROR_SENDING_THREE_LISTS_REQUEST_KEY                                               @"ERROR_SENDING_THREE_LISTS_REQUEST"
#define NO_NEARBY_ATMS_KEY                                                                  @"NO_NEARBY_ATMS"
#define NO_NEARBY_EXPRESS_AGENTS_KEY                                                        @"NO_NEARBY_EXPRESS_AGENTS"
#define NO_NEARBY_BRANCHES_KEY                                                              @"NO_NEARBY_BRANCHES"
#define NO_NEARBY_ATMS_OR_BRANCHES_KEY                                                      @"NO_NEARBY_ATMS_OR_BRANCHES"
#define NO_NEARBY_ATMS_OR_EXPRESS_AGENTS_KEY                                                @"NO_NEARBY_ATMS_OR_EXPRESS_AGENTS"
#define NO_NEARBY_EXPRESS_AGENTS_OR_BRANCHES_KEY                                            @"NO_NEARBY_EXPRESS_AGENTS_OR_BRANCHES"
#define NO_NEARBY_ATMS_OR_EXPRESS_AGENTS_OR_BRANCHES_KEY                                    @"NO_NEARBY_ATMS_OR_EXPRESS_AGENTS_OR_BRANCHES"

#define CURRENT_LOCATION_POI_TITLE_KEY                                                      @"CURRENT_LOCATION_POI_TITLE"
#define SCHEDULE_TITLE_KEY                                                                  @"SCHEDULE_TITLE"
#define SCHEDULE_WEEK_TITLE_KEY                                                             @"SCHEDULE_WEEK_TITLE"
#define SCHEDULE_SATURDAY_TITLE_KEY                                                         @"SCHEDULE_SATURDAY_TITLE"
#define SCHEDULE_SUNDAY_TITLE_KEY                                                           @"SCHEDULE_SUNDAY_TITLE"
#define DISTANCE_TITLE_KEY                                                                  @"DISTANCE_TITLE"
#define SCHEDULE_24_TITLE_KEY                                                               @"SCHEDULE_24_TITLE"
#define AGENT_SCHEDULE_TITLE_KEY                                                            @"AGENT_SCHEDULE_TITLE"

#pragma mark -
#pragma mark ATM and branches detail view texts string keys

/**
 * Defines the ATM or branches detail view texts
 */
#define ROUTE_ON_FOOT_BUTTON_TEXT_KEY                                                       @"ROUTE_ON_FOOT_BUTTON_TEXT"
#define ROUTE_BY_CAR_BUTTON_TEXT_KEY                                                        @"ROUTE_BY_CAR_BUTTON_TEXT"


#pragma mark -
#pragma mark Accounts list view strings

/**
 * Defines the accounts view strings
 */
#define ACCOUNTS_VIEW_HEADER_TITLE_KEY                                                      @"ACCOUNTS_VIEW_HEADER_TITLE"
#define CURRENT_BALANCE_ACCOUNT_KEY                                                         @"CURRENT_BALANCE_ACCOUNT"
#define LIMIT_BALANCE_ACCOUNT_KEY															@"LIMIT_BALANCE_ACCOUNT"
#define AVAILABLE_BALANCE_ACCOUNT_KEY                                                       @"AVAILABLE_BALANCE_ACCOUNT"
#define SHOW_RETENCTIONS_BUTTON_TEXT_KEY                                                    @"SHOW_RETENCTIONS_BUTTON_TEXT"
#define ACCOUNT_OPERATION_DATE_TEXT_KEY                                                     @"ACCOUNT_OPERATION_DATE_TEXT"
#define ACCOUNT_VALUE_DATE_TEXT_KEY                                                         @"ACCOUNT_VALUE_DATE_TEXT"
#define ACCOUNT_TRANSACTON_NUMBER_TEXT_KEY                                                  @"ACCOUNT_TRANSACTON_NUMBER_TEXT"
#define ACCOUNT_ITF_TEXT_KEY                                                                @"ACCOUNT_ITF_TEXT"
#define ACCOUNT_TRANSACTION_ITF_TEXT                                                        @"ACCOUNT_TRANSACTION_ITF_TEXT"
#define ACCOUNT_DOES_NOT_HAVE_TRANSACTIONS_TEXT_KEY                                         @"ACCOUNT_DOES_NOT_HAVE_TRANSACTIONS_TEXT"
#define ACCOUNT_DOES_NOT_HAVE_MOVEMENTS_TEXT_KEY                                            @"ACCOUNT_DOES_NOT_HAVE_MOVEMENTS_TEXT"

#pragma mark -
#pragma mark Stocks strings keys


/**
 * Defines the stocks strings
 */

#define STOCK_MARKET_TEXT_KEY                                                               @"STOCK_MARKET_TEXT"
#define STOCK_TITLE_CALLOUT_KEY                                                             @"STOCK_TITLE_CALLOUT"
#define STOCK_TITLE_SELECTED_DAY_CALLOUT_KEY                                                @"STOCK_TITLE_SELECTED_DAY_CALLOUT"


#pragma mark -
#pragma mark Account popover retentions

#define RETENTIONS_TEXT_KEY                                                                 @"RETENTIONS_TEXT"
#define DUE_DATE_RETENTION_TEXT_KEY                                                         @"DUE_DATE_RETENTION_TEXT"
#define DISCHARGE_DATE_RETENTION_TEXT_KEY                                                   @"DISCHARGE_DATE_RETENTION_TEXT"
#define NUMBER_RETENTION_TEXT_KEY                                                           @"NUMBER_RETENTION_TEXT"
#define REMARKS_RETENTION_TEXT_KEY                                                          @"REMARKS_RETENTION_TEXT" 
#define AMOUNT_RETENTION_TEXT_KEY                                                           @"AMOUNT_RETENTION_TEXT"
#define RETENTIONS_TOTAL_RETAINED_TEXT_KEY                                                  @"RETENTIONS_TOTAL_RETAINED_TEXT"
#define AMOUNT_RETENTION_TEXT_2_KEY                                                         @"AMOUNT_RETENTION_TEXT_2"


#pragma mark -
#pragma mark Transactions filter

#define TRANSACTION_FILTER_INCOMPLETE_WARNING_KEY                                           @"TRANSACTION_FILTER_INCOMPLETE_WARNING"
#define TRANSACTION_FILTER_TITLE_TEXT_KEY                                                   @"TRANSACTION_FILTER_TITLE_TEXT"
#define TRANSACTION_FILTER_HEADER_TEXT_KEY                                                  @"TRANSACTION_FILTER_HEADER_TEXT"
#define TRANSACTION_FILTER_HEADER2_TEXT_KEY                                                 @"TRANSACTION_FILTER_HEADER2_TEXT"
#define TRANSACTION_FILTER_SHOW_LAST_TEXT_KEY                                               @"TRANSACTION_FILTER_SHOW_LAST_TEXT"
#define TRANSACTION_FILTER_TAGGED_LIKE_TEXT_KEY                                             @"TRANSACTION_FILTER_TAGGED_LIKE_TEXT"
#define TRANSACTION_FILTER_SHOW_INCOMES_TEXT_KEY                                            @"TRANSACTION_FILTER_SHOW_INCOMES_TEXT"   
#define TRANSACTION_FILTER_SHOW_PAYMENTS_TEXT_KEY                                           @"TRANSACTION_FILTER_SHOW_PAYMENTS_TEXT"
#define TRANSACTION_FILTER_OK_TEXT_KEY                                                      @"TRANSACTION_FILTER_OK_TEXT"
#define TRANSACTION_FILTER_ALL_TAGS_TEXT_KEY                                                @"TRANSACTION_FILTER_ALL_TAGS_TEXT"
#define TRANSACTION_FILTER_PERIOD_TEXT_KEY                                                  @"TRANSACTION_FILTER_PERIOD_TEXT"
#define TRANSACTION_FILTER_NOONE_TEXT_KEY                                                   @"TRANSACTION_FILTER_NOONE_TEXT"
#define NO_TRANSACTIONS_FOR_FILTER_KEY                                                      @"NO_TRANSACTIONS_FOR_FILTER"
#define TRANSACTION_FILTER_WITH_DATE_KEY                                                    @"TRANSACTION_FILTER_WITH_DATE"
#define TRANSACTION_DOCUMENT_TEXT_KEY                                                       @"TRANSACTION_DOCUMENT_TEXT"
#define TRANSACTION_FILTER_TO_TEXT_KEY                                                      @"TRANSACTION_FILTER_TO_TEXT"
#define TRANSACTION_FILTER_FROM_TEXT_KEY                                                    @"TRANSACTION_FILTER_FROM_TEXT"

#pragma mark -
#pragma mark Transactions detail

#define TRANSACTION_ACCOUNT_DETAIL_OP_DESCRIPTION_KEY                                       @"TRANSACTION_ACCOUNT_DETAIL_OP_DESCRIPTION"
#define TRANSACTION_ACCOUNT_DETAIL_MOV_NUMBER_KEY                                           @"TRANSACTION_ACCOUNT_DETAIL_MOV_NUMBER"
#define TRANSACTION_ACCOUNT_DETAIL_CHECK_NUMBER_KEY                                         @"TRANSACTION_ACCOUNT_DETAIL_CHECK_NUMBER"
#define TRANSACTION_ACCOUNT_DETAIL_TYPE_KEY                                                 @"TRANSACTION_ACCOUNT_DETAIL_TYPE"
#define TRANSACTION_ACCOUNT_DETAIL_VALUE_DATE_KEY                                           @"TRANSACTION_ACCOUNT_DETAIL_VALUE_DATE"
#define TRANSACTION_ACCOUNT_DETAIL_CONTABLE_DATE_KEY                                        @"TRANSACTION_ACCOUNT_DETAIL_CONTABLE_DATE"
#define TRANSACTION_ACCOUNT_DETAIL_DATE_TIME_KEY                                            @"TRANSACTION_ACCOUNT_DETAIL_DATE_TIME"
#define TRANSACTION_ACCOUNT_DETAIL_CENTER_KEY                                               @"TRANSACTION_ACCOUNT_DETAIL_CENTER"
#define TRANSACTION_ACCOUNT_DETAIL_SEND_KEY                                                 @"TRANSACTION_ACCOUNT_DETAIL_SEND"
#define TRANSACTION_ACCOUNT_DETAIL_SEND_SUBTITLE_KEY                                        @"TRANSACTION_ACCOUNT_DETAIL_SEND_SUBTITLE"


#pragma mark -
#pragma mark Cards lists view

/**
 * Defines the cards view strings
 */

#define CARDS_AVAILABLE_BALANCE_PLACEHOLDER_KEY                                             @"CARDS_AVAILABLE_BALANCE_PLACEHOLDER"
#define CARDS_AVAILABLE_BALANCE_FORMAT_KEY                                                  @"CARDS_AVAILABLE_BALANCE_FORMAT"
#define MINIMUN_PAYMENT_CARD_KEY                                                            @"MINIMUN_PAYMENT_CARD"
#define TOTAL_PAYMENT_CARD_KEY                                                              @"TOTAL_PAYMENT_CARD"
#define DELAY_PAYMENT_CARD_KEY                                                              @"DELAY_PAYMENT_CARD"
#define CLOSED_DAY_CARD_KEY                                                                 @"CLOSED_DAY_CARD"
#define LAST_PAYMENT_DAY_CARD_KEY                                                           @"LAST_PAYMENT_DAY_CARD"
#define CARD_OPERATION_TYPE_TEXT_KEY                                                        @"CARD_OPERATION_TYPE_TEXT"
#define CARD_DOLARS_OPERATION_TEXT_KEY                                                      @"CARD_DOLARS_OPERATION_TEXT"
#define CARD_SOLES_OPERATION_TEXT_KEY                                                       @"CARD_SOLES_OPERATION_TEXT"
#define CARD_OPERATION_DATE_TEXT_KEY                                                        @"CARD_OPERATION_DATE_TEXT"
#define CARD_DOES_NOT_HAVE_TRANSACTIONS_TEXT_KEY                                            @"CARD_DOES_NOT_HAVE_TRANSACTIONS_TEXT"
#define CARD_DOES_NOT_HAVE_MOVEMENTS_TEXT_KEY                                               @"CARD_DOES_NOT_HAVE_MOVEMENTS_TEXT"
#define CARD_TRANSACTION_OPERATION_IN_SOLES_TEXT_KEY                                        @"CARD_TRANSACTION_OPERATION_IN_SOLES_TEXT"


#pragma mark -
#pragma mark Deposit strings

/**
 * Defines the deposit strings keys
 */
#define DEPOSIT_TYPE_CONSTANT_TEXT_KEY                                                      @"DEPOSIT_TYPE_CONSTANT_TEXT"


#pragma mark -
#pragma mark Transfer strings

#define TRANSFER_NO_ACCOUNT_AVAILABLE_KEY                                                   @"TRANSFER_NO_ACCOUNT_AVAILABLE"
#define ORIGIN_TITLE_TEXT_KEY                                                               @"ORIGIN_TITLE_TEXT"
#define DESTINATION_TITLE_TEXT_KEY                                                          @"DESTINATION_TITLE_TEXT"
#define DESTINATION_NUMBER_TITLE_KEY                                                         @"DESTINATION_NUMBER_TITLE"
#define DESTINATION_NUMBER_TITLE_TEXT_KEY                                                   @"DESTINATION_NUMBER_TITLE_TEXT"
#define DESTINATION_PHONE_NUMBER_TITLE_TEXT_KEY                                                   @"DESTINATION_PHONE_NUMBER_TITLE_TEXT"
#define AMOUNT_TEXT_KEY                                                                     @"AMOUNT_TEXT"
#define TRANSFER_MUST_SELECT_ORIGIN_ACCOUNT_ERROR_TEXT_KEY                                  @"TRANSFER_MUST_SELECT_ORIGIN_ACCOUNT_ERROR_TEXT"
#define TRANSFER_DOCUMENT_ERROR_TEXT_KEY                                                    @"TRANSFER_DOCUMENT_ERROR_TEXT"
#define TRANSFER_MUST_SELECT_DESTINATION_ACCOUNT_ERROR_TEXT_KEY                             @"TRANSFER_MUST_SELECT_DESTINATION_ACCOUNT_ERROR_TEXT"
#define TRANSFER_MUST_SELECT_CURRENCY_ERROR_TEXT_KEY                                        @"TRANSFER_MUST_SELECT_CURRENCY_ERROR_TEXT"
#define TRANSFER_MUST_SELECT_AMOUNT_ERROR_TEXT_KEY                                          @"TRANSFER_MUST_SELECT_AMOUNT_ERROR_TEXT"
#define TRANSFER_CONCEPT_TOO_LONG_ERROR_TEXT_KEY                                            @"TRANSFER_CONCEPT_TOO_LONG_ERROR_TEXT"
#define TRANSFER_BENEFICIARY_TOO_LONG_ERROR_TEXT_KEY                                        @"TRANSFER_BENEFICIARY_TOO_LONG_ERROR_TEXT"
#define TRANSFER_DOCUMENT_TOO_LONG_ERROR_TEXT_KEY                                           @"TRANSFER_DOCUMENT_TOO_LONG_ERROR_TEXT"
#define TRANSFER_EMAIL_MESSAGE_TOO_LONG_ERROR_TEXT_KEY                                      @"TRANSFER_EMAIL_MESSAGE_TOO_LONG_ERROR_TEXT"
#define TRANSFER_BETWEEN_ACCOUNTS_TEXT_1_KEY                                                @"TRANSFER_BETWEEN_ACCOUNTS_TEXT_1"
#define TRANSFER_BETWEEN_ACCOUNTS_TEXT_2_KEY                                                @"TRANSFER_BETWEEN_ACCOUNTS_TEXT_2"
#define TRANSFER_TO_THIRD_ACCOUNTS_TEXT_1_KEY                                               @"TRANSFER_TO_THIRD_ACCOUNTS_TEXT_1"
#define TRANSFER_TO_THIRD_ACCOUNTS_TEXT_2_KEY                                               @"TRANSFER_TO_THIRD_ACCOUNTS_TEXT_2"
#define TRANSFER_TO_ANOTHER_BANK_TEXT_1_KEY                                                 @"TRANSFER_TO_ANOTHER_BANK_TEXT_1"
#define TRANSFER_TO_ANOTHER_BANK_TEXT_2_KEY                                                 @"TRANSFER_TO_ANOTHER_BANK_TEXT_2"
#define TRANSFER_WITH_CASH_MOBILE_TEXT_1_KEY                                                @"TRANSFER_WITH_CASH_MOBILE_TEXT_1"
#define TRANSFER_WITH_CASH_MOBILE_SEND_TEXT_1_KEY                                           @"TRANSFER_WITH_CASH_MOBILE_SEND_TEXT_1"
#define TRANSFER_WITH_CASH_MOBILE_DETAIL_TEXT_1_KEY                                         @"TRANSFER_WITH_CASH_MOBILE_DETAIL_TEXT_1"
#define TRANFER_MESSAGE_HEADER_BETWEEN_ACCOUNTS_TEXT_KEY									@"TRANFER_MESSAGE_HEADER_BETWEEN_ACCOUNTS_TEXT"
#define TRANSFER_MESSAGE_HEADER_TO_THIRD_ACCOUNTS_TEXT_KEY									@"TRANSFER_MESSAGE_HEADER_TO_THIRD_ACCOUNTS_TEXT"
#define TRANSFER_MESSAGE_HEADER_TO_ANOTHER_BANK_TEXT_KEY									@"TRANSFER_MESSAGE_HEADER_TO_ANOTHER_BANK_TEXT"
#define TRANSFER_MESSAGE_HEADER_WITH_CASH_MOBILE_TEXT_KEY									@"TRANSFER_MESSAGE_HEADER_WITH_CASH_MOBILE_TEXT"
#define TRANSFER_MESSAGE_HEADER_TO_GIFT_CARD_TEXT_KEY                                       @"TRANSFER_MESSAGE_HEADER_TO_GIFT_CARD_TEXT"
#define TRANSFER_GIFT_CARD_TEXT_1_KEY                                                       @"TRANSFER_GIFT_CARD_TEXT_1"
#define TRANSFER_GIFT_CARD_CHARGED_ACCOUNT_CARD_KEY                                         @"TRANSFER_GIFT_CARD_CHARGED_ACCOUNT_CARD"
#define TRANSFER_GIFT_CARD_TITLE_KEY                                                        @"TRANSFER_GIFT_CARD_TITLE"
#define TRANSFER_INSCRIP_CARD_TITLE_KEY                                                        @"TRANSFER_INSCRIP_CARD_TITLE"
#define TRANSFER_GIFT_CARD_AMOUNT_TO_PAY_KEY                                                @"TRANSFER_GIFT_CARD_AMOUNT_TO_PAY"


#define REFERENCE_INFO_TEXT_KEY                                                             @"REFERENCE_INFO_TEXT"
#define OPERATION_REFERENCE_TEXT_KEY                                                        @"OPERATION_REFERENCE_TEXT"
#define ENTER_REFERENCE_TEXT_KEY                                                            @"ENTER_REFERENCE_TEXT"
#define TRANSFER_REPORT_TO_BENEFICIARY_TEXT_KEY												@"TRANSFER_REPORT_TO_BENEFICIARY_TEXT"
#define DESTINATION_BANK_TEXT_KEY															@"DESTINATION_BANK_TEXT"
#define DESTINATION_BANK_LOWER_CASE_TEXT_KEY                                                @"DESTINATION_BANK_LOWER_CASE_TEXT"
#define BENEFICIARY_TEXT_KEY																@"BENEFICIARY_TEXT"
#define DOC_TEXT_KEY                                                                        @"DOC_TEXT"
#define TRANSACTION_PHONE_TEXT_KEY                                                          @"TRANSACTION_PHONE_TEXT"
#define TRANSACTION_MAIL_TEXT_KEY                                                           @"TRANSACTION_MAIL_TEXT"

#pragma mark - Cash Mobile
#define CASH_MOBILE_ACCOUNT_CHARGED_TEXT_KEY                                                @"CASH_MOBILE_ACCOUNT_CHARGED_TEXT"
#define CASH_MOBILE_NUMBER_OPERATION_TITLE_TEXT_KEY                                         @"CASH_MOBILE_NUMBER_OPERATION_TITLE_TEXT"
#define CASH_MOBILE_ORINGIN_ACCOUNT_OWNER_TEXT_KEY                                          @"CASH_MOBILE_ORINGIN_ACCOUNT_OWNER_TEXT"
#define CASH_MOBILE_AMOUNT_PAID_TITLE_TEXT_KEY                                              @"CASH_MOBILE_AMOUNT_PAID_TITLE_TEXT"

#define TRANSFERED_IMPORT_TEXT_KEY															@"TRANSFERED_IMPORT_TEXT"
#define TRANSFERED_IMPORT_CHARGE_TEXT_KEY															@"TRANSFERED_IMPORT_CHARGE_TEXT"

#define TRANSFERED_SEND_IMPORT_TEXT_KEY															@"TRANSFERED_SEND_IMPORT_TEXT"
#define COMISSION_TEXT_KEY																	@"COMISSION_TEXT"
#define USE_TEXT_KEY																		@"USE_TEXT"
#define FINAL_AMOUNT_TEXT_KEY																@"FINAL_AMOUNT_TEXT"
#define INTRODUCE_OPERATIONS_KEY_TEXT_KEY													@"INTRODUCE_OPERATIONS_KEY_TEXT"
#define LEGAL_TERMS_TEXT_KEY																@"LEGAL_TERMS_TEXT"
#define ORINGIN_ACCOUNT_OWNER_TEXT_KEY														@"ORINGIN_ACCOUNT_OWNER_TEXT"
#define TOTAL_AMOUNT_CHARGED_TEXT_KEY														@"TOTAL_AMOUNT_CHARGED_TEXT"
#define DESTINATION_ACCOUNT_OWNER_TEXT_KEY													@"DESTINATION_ACCOUNT_OWNER_TEXT"
#define TRANSFER_OPERATION_TEXT_KEY															@"TRANSFER_OPERATION_TEXT"
#define TRANSFER_REFERENCE_TEXT_KEY															@"TRANSFER_REFERENCE_TEXT"
#define TRANSFER_DATE_HOUR_SHORT_TEXT_KEY                                                   @"TRANSFER_DATE_HOUR_SHORT_TEXT"
#define TRANSFER_EXPIRATION_DATE_HOUR_SHORT_TEXT_KEY                                            @"TRANSFER_EXPIRATION_DATE_HOUR_SHORT_TEXT"
#define COST_RESEND_TEXT_KEY                                                                @"COST_RESEND_TEXT"
#define TRANSFER_SELECT_ACCOUNT_TEXT_KEY                                                    @"TRANSFER_SELECT_ACCOUNT_TEXT"
#define SEND_EMAIL_TEXT_KEY                                                                 @"SEND_EMAIL_TEXT"
#define SEND_PHONE_TEXT_KEY                                                                 @"SEND_PHONE_TEXT"
#define SEND_ACTIVE_NOTIFICATION_TEXT_KEY @"SEND_ACTIVE_NOTIFICATION_TEXT"

#define SEND_PHONE_DESTINARY_TEXT_KEY                                                       @"SEND_PHONE_DESTINARY_TEXT"
#define SEND_PHONE_INDICATION_TEXT_KEY                                                      @"SEND_PHONE_INDICATION_TEXT"
#define TRANSFER_EXCHANGE_RATE_TEXT_KEY                                                     @"TRANSFER_EXCHANGE_RATE_TEXT"
#define AMOUNT_MULTIPLE_TWENTY_KEY                                                          @"AMOUNT_MULTIPLE_TWENTY"

#pragma mark -
#pragma mark Transfer to my accounts string

#define TRANSFER_TO_MY_ACCOUNTS_CAPITAL_TITLE_KEY                                           @"TRANSFER_TO_MY_ACCOUNTS_CAPITAL_TITLE"

#pragma mark -
#pragma mark Consult Cash Mobile Movements

#define NO_MOVEMENTS_TO_LIST_KEY                                                            @"NO_MOVEMENTS_TO_LIST"
#define CASH_MOBILE_MOVEMENTS_KEY                                                           @"CASH_MOBILE_MOVEMENTS"

#pragma mark -
#pragma mark Share payment

#define CONFIRM_TRANSFER_TEXT_KEY                                                           @"CONFIRM_TRANSFER_TEXT"
#define LEGAL_TERMS_TRANSFER_TEXT                                                           @"LEGAL_TERMS_TRANSFER_TEXT"
#define ERROR_SENDING_MAIL_TEXT_KEY                                                         @"ERROR_SENDING_MAIL_TEXT"
#define SHARE_RETURN_GLOBAL_BUTTON_TEXT_KEY                                                 @"SHARE_RETURN_GLOBAL_BUTTON_TEXT"
#define SHARE_RETURN_TRANSFER_BUTTON_TEXT_KEY                                               @"SHARE_RETURN_TRANSFER_BUTTON_TEXT"
#define SHARE_SEAL_TEXT_KEY                                                                 @"SHARE_SEAL_TEXT"
#define SHARE_COORDINATE_TEXT_KEY                                                           @"SHARE_COORDINATE_TEXT"
#define MARKETPLACE_COMMISION_TEXT_KEY                                                      @"MARKETPLACE_COMMISION_TEXT"
#define NUMBER_OPERATION_TITLE_TEXT_KEY                                                     @"NUMBER_OPERATION_TITLE_TEXT"
#define OPERATION_TITLE_TEXT_KEY                                                            @"OPERATION_TITLE_TEXT"
#define HOLDER_TITLE_TEXT_KEY                                                               @"HOLDER_TITLE_TEXT"    
#define AMOUNT_PAID_TITLE_TEXT_KEY                                                          @"AMOUNT_PAID_TITLE_TEXT"
#define AMOUNT_PAID_DETAIL_TITLE_TEXT_KEY                                                   @"AMOUNT_PAID_DETAIL_TITLE_TEXT"
#define AMOUNT_PAID_LOWERCASE_DETAIL_TITLE_TEXT_KEY                                         @"AMOUNT_PAID_LOWERCASE_DETAIL_TITLE_TEXT"
#define AMOUNT_CHARGERD_TITLE_TEXT_KEY                                                      @"AMOUNT_CHARGERD_TITLE_TEXT"
#define DATE_TRANSFER_TITLE_TEXT_KEY                                                        @"DATE_TRANSFER_TITLE_TEXT"
#define SEND_TO_MY_EMAIL_TITLE_KEY                                                          @"SEND_TO_MY_EMAIL_TITLE"
#define SEND_TO_MY_TELEPHONE_TITLE_KEY                                                      @"SEND_TO_MY_TELEPHONE_TITLE"
#define SEND_TO_CONTACT_BY_EMAIL_TITLE_KEY                                                  @"SEND_TO_CONTACT_BY_EMAIL_TITLE"
#define SEND_TO_CONTACT_BY_TELEPHONE_TITLE_KEY                                              @"SEND_TO_CONTACT_BY_TELEPHONE_TITLE"
#define SUBJECT_TEXT_KEY                                                                    @"SUBJECT_TEXT"
#define SEND_TRANSFER_BUTTON_TEXT_KEY                                                       @"SEND_TRANSFER_BUTTON_TEXT"
#define SHARE_PAYMENT_ERROR_EMAIL_TEXT_KEY                                                  @"SHARE_PAYMENT_ERROR_EMAIL_TEXT" 
#define SHARE_PAYMENT_ADD_ADDRESSEE_TEXT_KEY                                                @"SHARE_PAYMENT_ADD_ADDRESSEE_TEXT"
#define DELETE_TITLE_TEXT_KEY                                                               @"DELETE_TITLE_TEXT"
#define SHARE_PAYMENT_SEND_TO_THEIR_EMAIL_TEXT_KEY                                          @"SHARE_PAYMENT_SEND_TO_THEIR_EMAIL_TEXT"
#define ENTER_EMAIL_TEXT_KEY                                                                @"ENTER_EMAIL_TEXT"
#define EXCHANGE_RATE_TEXT_KEY                                                              @"EXCHANGE_RATE_TEXT"
#define SERVICE_TITULAR_TEXT_KEY                                                            @"SERVICE_TITULAR_TEXT"
#define SERVICE_TITULAR_LOWERCASE_TEXT_KEY                                                  @"SERVICE_TITULAR_LOWERCASE_TEXT"
#define NUMBER_OF_RECEIPTS_TEXT_KEY                                                         @"NUMBER_OF_RECEIPTS_TEXT"
#define RECEIPT_VALUE_TEXT                                                                  @"RECEIPT_VALUE_TEXT"
#define AMOUNT_TO_PAY_TEXT_KEY                                                              @"AMOUNT_TO_PAY_TEXT"
#define AMOUNT_OF_RECHARGE_KEY                                                              @"AMOUNT_OF_RECHARGE"

#pragma mark -
#pragma mark Share payment contact telephone info

#define TRANSFER_TO_MOBILE_ENTER_PHONE_TEXT_KEY                                             @"TRANSFER_TO_MOBILE_ENTER_PHONE_TEXT"
#define TRANSFER_TO_MOBILE_DESTINATION_TEXT_KEY                                             @"TRANSFER_TO_MOBILE_DESTINATION_TEXT"
#define TRANSFER_TO_MOBILE_NO_PHONE_ERROR_TEXT_KEY                                          @"TRANSFER_TO_MOBILE_NO_PHONE_ERROR_TEXT"
#define TRANSFER_TO_ANOTHER_ADD_NUMBER_TEXT_KEY                                             @"TRANSFER_TO_ANOTHER_ADD_NUMBER_TEXT"

#pragma mark -
#pragma mark Stock detail strings

#define VALUE_BALANCE_TEXT_KEY                                                              @"VALUE_BALANCE_TEXT"
#define CASH_BALANCE_TEXT_KEY                                                               @"CASH_BALANCE_TEXT"
#define EXPIRED_ACCOUNT_TEXT_KEY                                                            @"EXPIRED_ACCOUNT_TEXT"


#pragma mark -
#pragma mark Transfers strings

#define TRANSFER_ONLY_ONE_ACCOUNT_TEXT_KEY                                                  @"TRANSFER_ONLY_ONE_ACCOUNT_TEXT"
#define TRANSFER_NO_ACCOUNT_TEXT_KEY                                                        @"TRANSFER_NO_ACCOUNT_TEXT"
#define TRANSFER_TITLE_TEXT_KEY                                                             @"TRANSFER_TITLE_TEXT"
#define TRANSFER_TO_CARD_TEXT_KEY                                                           @"TRANSFER_TO_CARD_TEXT"
#define ORDER_NUMBER_TEXT_KEY                                                               @"ORDER_NUMBER_TEXT"
#define TRANSFER_SELECT_ORIGIN_ACCOUNT_TEXT_KEY                                             @"TRANSFER_SELECT_ORIGIN_ACCOUNT_TEXT"
#define TRANSFER_AMOUNT_TEXT_KEY                                                            @"TRANSFER_AMOUNT_TEXT"
#define TRANSFER_AMOUNT_HINT_TEXT_KEY                                                       @"TRANSFER_AMOUNT_HINT_TEXT"
#define TRANSFER_RECHARGE_AMOUNT_TEXT_KEY                                                   @"TRANSFER_RECHARGE_AMOUNT_TEXT"
#define TRANSFER_SELECT_DESTINATION_ACCOUNT_TEXT_KEY                                        @"TRANSFER_SELECT_DESTINATION_ACCOUNT_TEXT"
#define SEND_AMOUNT_TEXT_KEY                                                                @"SEND_AMOUNT_TEXT"
#define NOTIFICATION_TYPE_TEXT_KEY                                                          @"NOTIFICATION_TYPE_TEXT"
#define SEND_TRANSFER_TITLE_KEY                                                             @"NOTIFICATION_TYPE_TEXT"
#define SEND_TRANSFER_TEXT_KEY                                                              @"SEND_TRANSFER_TEXT" 
#define SEND_CONFIM_MAIL_TEXT_KEY                                                           @"SEND_CONFIM_MAIL_TEXT"
#define ACCOUND_PAID_TEXT_KEY                                                               @"ACCOUND_PAID_TEXT"
#define ACCOUND_CHARGED_TEXT_KEY                                                            @"ACCOUND_CHARGED_TEXT"
#define SEND_TRANSFER_VIEW_TITLE_KEY                                                        @"SEND_TRANSFER_VIEW_TITLE"
#define UNABLE_TO_PERFORM_TRANSFER_TEXT_KEY                                                 @"UNABLE_TO_PERFORM_TRANSFER_TEXT"
#define TRANSFER_STEP_1_TEXT_KEY                                                            @"TRANSFER_STEP_1_TEXT"
#define TRANSFER_STEP_2_TEXT_KEY                                                            @"TRANSFER_STEP_2_TEXT"
#define TRANSFER_STEP_3_TEXT_KEY                                                            @"TRANSFER_STEP_3_TEXT"
#define TRANSFER_INFO_TEXT_KEY                                                              @"TRANSFER_INFO_TEXT"
#define TRANSACTION_CURRENCY_TEXT_KEY                                                       @"TRANSACTION_CURRENCY_TEXT"
#define REFERENCE_HINT_TEXT_KEY                                                             @"REFERENCE_HINT_TEXT"
#define REFERENCE_TEXT_KEY                                                                  @"REFERENCE_TEXT"
#define DESTINATION_INTER_ACCOUNT_TEXT_KEY                                                  @"DESTINATION_INTER_ACCOUNT_TEXT"
#define BENEFICIARY_NAME_TEXT_KEY                                                           @"BENEFICIARY_NAME_TEXT"
#define BENEFICIARY_HINT_TEXT_KEY                                                           @"BENEFICIARY_HINT_TEXT"
#define DOCT_TYPE_LABEL_TEXT_KEY                                                            @"DOCT_TYPE_LABEL_TEXT"
#define DOCT_TYPE_HINT_TEXT_KEY                                                             @"DOCT_TYPE_HINT_TEXT"
#define OWN_ACCOUNT_DESTINATION_TEXT_KEY                                                    @"OWN_ACCOUNT_DESTINATION_TEXT"
#define OWN_ACCOUNT_LABEL_TEXT_KEY                                                          @"OWN_ACCOUNT_LABEL_TEXT"
#define BOTTOM_LABEL_OWN_ACCOUNT_TEXT_KEY                                                   @"BOTTOM_LABEL_OWN_ACCOUNT_TEXT"
#define TRANSACTION_DEST_BANK_TEXT_KEY                                                      @"TRANSACTION_DEST_BANK_TEXT"
#define TRANSFERS_TO_THIRD_ACCOUNTS_SAME_ACCOUNT_ERROR_KEY                                  @"TRANSFERS_TO_THIRD_ACCOUNTS_SAME_ACCOUNT_ERROR"
#define TRANSFERS_TO_THIRD_ACCOUNTS_DEST_ACCOUNT_IN_OWN_ACCOUNTS_ERROR_KEY                  @"TRANSFERS_TO_THIRD_ACCOUNTS_DEST_ACCOUNT_IN_OWN_ACCOUNTS_ERROR"

#define GIFT_CARD_KEY                                                                       @"GIFT_CARD"

#pragma mark -
#pragma mark Transfers error strings

#define TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY                                              @"TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT"
#define TRANSFER_ERROR_CASHMOBILE_ORIGIN_ACCOUNT_TEXT_KEY                                   @"TRANSFER_ERROR_CASHMOBILE_ORIGIN_ACCOUNT_TEXT"
#define TRANSFER_ERROR_DESTINATION_ACCOUNT_TEXT_KEY                                         @"TRANSFER_ERROR_DESTINATION_ACCOUNT_TEXT"
#define TRANSFER_ERROR_CURRENCY_TEXT_KEY                                                    @"TRANSFER_ERROR_CURRENCY_TEXT"
#define TRANSFER_ERROR_AMOUNT_TEXT_KEY                                                      @"TRANSFER_ERROR_AMOUNT_TEXT"
#define TRANSFER_MUST_BE_MULTIPLE_TWENTY_KEY                                                @"TRANSFER_MUST_BE_MULTIPLE_TWENTY"
#define TRANSFER_MUST_BE_LESS_THAN_KEY                                                      @"TRANSFER_MUST_BE_LESS_THAN"
#define TRANSFER_AMOUNT_EXCEEDED_KEY                                                        @"TRANSFER_AMOUNT_EXCEEDED"
#define TRANSFER_ERROR_OTP_KEY_TEXT_KEY                                                     @"TRANSFER_ERROR_OTP_KEY_TEXT"
#define TRANSFER_ERROR_COORDINATES_TEXT_KEY                                                 @"TRANSFER_ERROR_COORDINATES_TEXT"
#define TRANSFER_ERROR_DESTINATION_ACCOUNT_OFFICE_TEXT_KEY                                  @"TRANSFER_ERROR_DESTINATION_ACCOUNT_OFFICE_TEXT"
#define TRANSFER_ERROR_DESTINATION_ACCOUNT_ENTITY_TEXT_KEY                                  @"TRANSFER_ERROR_DESTINATION_ACCOUNT_ENTITY_TEXT"
#define TRANSFER_ERROR_DESTINATION_ACCOUNT_ACCNUMBER_TEXT_KEY                               @"TRANSFER_ERROR_DESTINATION_ACCOUNT_ACCNUMBER_TEXT"
#define TRANSFER_ERROR_DESTINATION_ACCOUNT_CC_TEXT_KEY                                      @"TRANSFER_ERROR_DESTINATION_ACCOUNT_CC_TEXT"
#define TRANSFER_ERROR_EMAIL_TEXT_KEY                                                       @"TRANSFER_ERROR_EMAIL_TEXT"
#define TRANSFER_ERROR_EMAIL_MESSAGE_TEXT_KEY                                               @"TRANSFER_ERROR_EMAIL_MESSAGE_TEXT"
#define TRANSFER_ERROR_CARRIER_TEXT_KEY                                                     @"TRANSFER_ERROR_CARRIER_TEXT"
#define TRANSFER_ERROR_PHONE_NUMBER_TEXT_KEY                                                @"TRANSFER_ERROR_PHONE_NUMBER_TEXT"
#define TRANSFER_ERROR_LEGAL_TERMS_TEXT_KEY                                                 @"TRANSFER_ERROR_LEGAL_TERMS_TEXT"
#define TRANSFER_ERROR_DESTINATION_PHONE_NUMBER_TEXT_KEY                                    @"TRANSFER_ERROR_DESTINATION_PHONE_NUMBER_TEXT"
#define TRANSFER_ERROR_CARD_NUMBER_TEXT_KEY                                                 @"TRANSFER_ERROR_CARD_NUMBER_TEXT"
#define TRANSFER_ERROR_VALID_GIFT_CARD_TEXT_KEY                                             @"TRANSFER_ERROR_VALID_GIFT_CARD_TEXT"
#define TRANSFER_ERROR_TRANSFER_AMOUNT_TEXT_KEY                                             @"TRANSFER_ERROR_TRANSFER_AMOUNT_TEXT"



#pragma mark -
#pragma mark Transfers to another account strings

#define TRANSFER_TO_ANOTHER_CONCEPT_TEXT_KEY                                                @"TRANSFER_TO_ANOTHER_CONCEPT_TEXT"

#pragma mark -
#pragma mark Transfer to account from other banks

#define TRANSFER_TO_ANOTHER_BANK_DESTINATION_TITLE_TEXT_KEY									@"TRANSFER_TO_ANOTHER_BANK_DESTINATION_TITLE"
#define TRANSFER_TO_ANOTHER_BANK_DESTINATION_TITLE_2_TEXT_KEY								@"TRANSFER_TO_ANOTHER_BANK_DESTINATION_TITLE_2"
#define TRANSFER_TO_ANOTHER_BANK_DESTINATION_SUBTITLE_2_TEXT_KEY							@"TRANSFER_TO_ANOTHER_BANK_DESTINATION_SUBTITLE_2"
#define TRANSFER_TO_ANOTHER_BANK_OWNED_ACCOUNT_TEXT_TEXT_KEY								@"TRANSFER_TO_ANOTHER_BANK_OWNED_ACCOUNT_TEXT"
#define TRANSFER_TO_ANOTHER_BANK_NOT_OWNED_ACCOUNT_TEXT_TEXT_KEY							@"TRANSFER_TO_ANOTHER_BANK_NOT_OWNED_ACCOUNT_TEXT"
#define TRANSFER_TO_ANOTHER_BANK_NOT_OWNED_ACCOUNT_TEXT_2_TEXT_KEY							@"TRANSFER_TO_ANOTHER_BANK_NOT_OWNED_ACCOUNT_TEXT_2"
#define TRANSFER_TO_ANOTHER_BANK_MUST_WRITE_A_BENEFICIARY_TEXT_KEY							@"TRANSFER_TO_ANOTHER_BANK_MUST_WRITE_A_BENEFICIARY"
#define TITLE_DOCUMENT_BENEFICIARY_TEXT_KEY                                                 @"TITLE_DOCUMENT_BENEFICIARY_TEXT"
#define NUMBER_DOCUMENT_BENEFICIARY_TEXT_KEY                                                @"NUMBER_DOCUMENT_BENEFICIARY_TEXT"
#define TRANSFER_TO_ANOTHER_BANK_MUST_WRITE_A_DOCUMENT_TYPE_TEXT_KEY                        @"TRANSFER_TO_ANOTHER_BANK_MUST_WRITE_A_DOCUMENT_TYPE_TEXT"
#define TRANSFER_TO_ANOTHER_BANK_MUST_WRITE_A_DOCUMENT_NUMBER_TEXT_KEY                      @"TRANSFER_TO_ANOTHER_BANK_MUST_WRITE_A_DOCUMENT_NUMBER_TEXT"


#pragma mark -
#pragma mark Transfer to my accounts string

#define TRANSFER_TO_MY_ACCOUNTS_HEADER2_TEXT_KEY											@"TRANSFER_TO_MY_ACCOUNTS_HEADER2_TEXT"
#define TRANSFER_TO_MY_ACCOUNTS_AMOUNT_TEXT_KEY												@"TRANSFER_TO_MY_ACCOUNTS_AMOUNT_TEXT"
#define TRANSFER_TO_MY_ACCOUNTS_TAX_TEXT_KEY                                                @"TRANSFER_TO_MY_ACCOUNTS_TAX_TEXT"
#define TRANSFER_TO_MY_ACCOUNTS_SUCCEDDED_TEXT_KEY                                          @"TRANSFER_TO_MY_ACCOUNTS_SUCCEDDED_TEXT"
#define TRANSFER_TO_MY_ACCOUNTS_SEND_CONFIRMATION_TITLE_TEXT_KEY                            @"TRANSFER_TO_MY_ACCOUNTS_SEND_CONFIRMATION_TITLE_TEXT"
#define TRANSFER_DO_TRANSFER_TEXT_KEY                                                       @"TRANSFER_DO_TRANSFER_TEXT"
#define TRANSFER_SEND_OPERATION_MUST_ELEMENT_TO_SEND_VALUES_ERROR_TEXT_KEY                  @"TRANSFER_SEND_OPERATION_MUST_ELEMENT_TO_SEND_VALUES_ERROR_TEXT"
#define TRANSFER_SEND_OPERATION_MUST_WRITE_MESSAGE_ERROR_TEXT_KEY                           @"TRANSFER_SEND_OPERATION_MUST_WRITE_MESSAGE_ERROR_TEXT"
#define TRANSFER_SEND_OPERATION_SUCCESSFULL_TEXT_KEY                                        @"TRANSFER_SEND_OPERATION_SUCCESSFULL_TEXT"
#define BEFORE_TRANSFER_TO_MY_ACCOUNTS_AMOUNT_TEXT_KEY                                      @"BEFORE_TRANSFER_TO_MY_ACCOUNTS_AMOUNT_TEXT"
#define TRANSFER_LEGAL_TERMS_KEY                                                            @"TRANSFER_LEGAL_TERMS"

#pragma mark -
#pragma mark Transfer step two string

#define TRANSFER_STEP_TWO_LEGAL_TERMS_TEXT_KEY                                              @"TRANSFER_STEP_TWO_LEGAL_TERMS_TEXT"
#define TRANSFER_STEP_TWO_OTP_KEY_TEXT_KEY                                                  @"TRANSFER_STEP_TWO_OTP_KEY_TEXT"
#define TRANSFER_STEP_TWO_COORD_KEY_TEXT_KEY                                                @"TRANSFER_STEP_TWO_COORD_KEY_TEXT"
#define TRANSFER_STEP_TWO_SEAL_TEXT_KEY                                                     @"TRANSFER_STEP_TWO_SEAL_TEXT"
#define TRANSFER_STEP_ITF_OWN_TEXT_KEY                                                      @"TRANSFER_STEP_ITF_OWN_TEXT"
#define TRANSFER_STEP_ITF_NOT_OWN_TEXT_KEY                                                  @"TRANSFER_STEP_ITF_NOT_OWN_TEXT"

#pragma mark -
#pragma mark Transfer sthree two string

#define TRANSFER_STEP_THREE_DONE_TEXT_KEY                                                   @"TRANSFER_STEP_THREE_DONE_TEXT"
#define TRANSFER_STEP_THREE_CASH_MOBILE_DONE_TEXT_KEY                                       @"TRANSFER_STEP_THREE_CASH_MOBILE_DONE_TEXT"

#pragma mark -
#pragma mark Exit form app strings

#define EXIT_BUTTON_TITLE_KEY                                                               @"EXIT_BUTTON_TITLE"
#define EXIT_DIALOG_MESSAGE_TEXT_KEY                                                        @"EXIT_DIALOG_MESSAGE_TEXT"


#pragma mark -
#pragma mark Route steps strings

/* Define the route steps strings keys */

#define ROUTE_ON_FOOT_BUTTON_TEXT_KEY                                                       @"ROUTE_ON_FOOT_BUTTON_TEXT"
#define ROUTE_BY_CAR_BUTTON_TEXT_KEY                                                        @"ROUTE_BY_CAR_BUTTON_TEXT"
#define ADVANCE_STEP_TEXT_KEY                                                               @"ADVANCE_STEP_TEXT"
#define ARRIVAL_STEP_TEXT_KEY                                                               @"ARRIVAL_STEP_TEXT"
#define NO_SIMILAR_ADDRESSES_TO_STRING_KEY                                                  @"NO_SIMILAR_ADDRESSES_TO_STRING"


#pragma mark -
#pragma mark Deposits list strings

#define CURRENT_BALANCE_DEPOSIT_TEXT_KEY                                                    @"CURRENT_BALANCE_DEPOSIT_TEXT"
#define AVAILABLE_BALANCE_DEPOSIT_TEXT_KEY                                                  @"AVAILABLE_BALANCE_DEPOSIT_TEXT"
#define SUBJECT_DEPOSIT_TEXT_KEY                                                            @"SUBJECT_DEPOSIT_TEXT"


#pragma mark -
#pragma mark Browser strings

#define DISCLAIMER_TITLE_KEY                                                                @"DISCLAIMER_TITLE"
#define PRICE_LIST_TITLE_KEY                                                                @"PRICE_LIST_TITLE"
#define DISCLAIMER_EMAIL_SUBJECT_KEY                                                        @"DISCLAIMER_EMAIL_SUBJECT"
#define DISCLAIMER_EMAIL_BODY_KEY                                                           @"DISCLAIMER_EMAIL_BODY"
#define EMAIL_DOCUMENT_VERSION_KEY                                                          @"EMAIL_DOCUMENT_VERSION"
#define EMAIL_BODY_END_KEY                                                                  @"EMAIL_BODY_END"
#define DOCUMENT_SENT_SUCCESSFULLY_KEY                                                      @"DOCUMENT_SENT_SUCCESSFULLY"

#pragma mark -
#pragma mark strings for page More

#define MORE_TEXT_TITLE_KEY                                                                 @"MORE_TEXT_TITLE"
#define MORE_TITLE_TEXT_KEY                                                                 @"MORE_TABS_TAB_TEXT"

#define CANCEL_TEXT_BUTTON_KEY                                                              @"CANCEL_TEXT_BUTTON"


#pragma mark -
#pragma mark Benefits for you strings

#define BENEFITS_FOR_YOU_TITLE_TEXT_KEY                                                     @"BENEFITS_FOR_YOU_TITLE_TEXT"

#pragma mark -
#pragma mark Contact us

#define CONTACT_US_TEXT_KEY                                                                 @"CONTACT_US_TEXT"
#define RADIO_BBVA_OPTION_TEXT_KEY @"RADIO_BBVA_OPTION_TEXT"

#pragma mark -
#pragma mark Distance and time strings

#define LARGE_DISTANCE_UNIT_ACRONYM_KEY                                                     @"LARGE_DISTANCE_UNIT_ACRONYM"
#define SMALL_DISTANCE_UNIT_ACRONYM_KEY                                                     @"SMALL_DISTANCE_UNIT_ACRONYM"
#define LESS_THAN_A_MINUTE_KEY                                                              @"LESS_THAN_A_MINUTE"
#define MINUTES_KEY                                                                         @"MINUTES"

#pragma mark -
#pragma mark Detail Cashmobile

#define BENEFICIARY_DETAIL_CASH_MOBILE_TITLE                                                @"BENEFICIARY_DETAIL_CASH_MOBILE_TITLE"
#define AMOUNT_DETAIL_CASH_MOBILE_TITLE                                                     @"AMOUNT_DETAIL_CASH_MOBILE_TITLE"
#define STATE_DETAIL_CASH_MOBILE_TITLE                                                      @"STATE_DETAIL_CASH_MOBILE_TITLE"
#define DATE_DETAIL_CASH_MOBILE_TITLE                                                       @"DATE_DETAIL_CASH_MOBILE_TITLE"
#define NUM_OPER_DETAIL_CASH_MOBILE_TITLE                                                   @"NUM_OPER_DETAIL_CASH_MOBILE_TITLE"
#define STATE_RESEND_ACTIVE_TEXT_KEY                                                        @"STATE_RESEND_ACTIVE_TEXT"

#pragma mark -
#pragma mark map strings

#define MAP_FILTER_MAP_KEY                                                                  @"MAP_FILTER_MAP"
#define MAP_FILTER_SEARCH_KEY                                                               @"MAP_FILTER_SEARCH"
#define MAP_LIST_BUTTON_KEY                                                                 @"MAP_LIST_BUTTON"
#define LIST_POI_MAP_KEY                                                                    @"LIST_POI_MAP"
#define ATM_POI_MAP_KEY                                                                     @"ATM_POI_MAP"
#define BRANCH_POI_MAP_KEY                                                                  @"BRANCH_POI_MAP"
#define AGENTS_POI_MAP_KEY                                                                  @"AGENTS_POI_MAP"
#define SINGLE_ATM_POI_MAP_KEY                                                              @"SINGLE_ATM_POI_MAP"
#define SINGLE_BRANCH_POI_MAP_KEY                                                           @"SINGLE_BRANCH_POI_MAP"
#define SINGLE_AGENT_POI_MAP_KEY                                                            @"SINGLE_AGENTS_POI_MAP"
#define LIST_ROUTE_TITLE_STRING_KEY                                                         @"LIST_ROUTE_TITLE_STRING"
#define POI_LIST_FILTER_PLACEHOLDER_TEXT_KEY												@"POI_LIST_FILTER_PLACEHOLDER_TEXT"
#define BBVA_CONTINNENTAL_KEY                                                               @"BBVA_CONTINNENTAL"
#define BBVA_BCP_KEY                                                                        @"BBVA_BCP"
#define BBVA_INTERBANK_KEY                                                                  @"BBVA_INTERBANK"
#define BBVA_SCOTIA_KEY                                                                     @"BBVA_SCOTIA"    
#define BBVA_AGENT_KEY                                                                      @"BBVA_AGENT"
#define BBVA_KASNET_KEY                                                                     @"BBVA_KASNET"
#define BBVA_AGENT_PLUS_KEY                                                                 @"BBVA_AGENT_PLUS"
#define BBVA_MULTIFACIL_KEY                                                                 @"BBVA_MULTIFACIL"
#define BBVA_CONTINENTAL_BRANCH_KEY                                                         @"BBVA_CONTINENTAL_BRANCH"
#define BBVA_CONTINENTAL_ATM_KEY                                                            @"BBVA_CONTINENTAL_ATM"
#define BBVA_BC_ATM_KEY                                                                     @"BBVA_BC_ATM"
#define BBVA_INTERBANK_ATM_KEY                                                              @"BBVA_INTERBANK_ATM"
#define BBVA_SCOTIABANK_ATM_KEY                                                             @"BBVA_SCOTIABANK_ATM"
#define BBVA_EXPRESSAGENT_AGENT_KEY                                                         @"BBVA_EXPRESSAGENT_AGENT"
#define BBVA_EXPRESSAGENTPLUS_AGENT_KEY                                                     @"BBVA_EXPRESSAGENTPLUS_AGENT"
#define BBVA_KASNET_AGENT_KEY                                                               @"BBVA_KASNET_AGENT"
#define BBVA_MULTIFACIL_AGENT_KEY                                                           @"BBVA_MULTIFACIL_AGENT"

#pragma mark -
#pragma mark Notifications table

#define NOTIFICATIONS_TABLE_SEND_PHONE_TEXT_KEY                                             @"NOTIFICATIONS_TABLE_SEND_PHONE_TEXT"
#define NOTIFICATIONS_TABLE_SEND_EMAIL_TEXT_KEY                                             @"NOTIFICATIONS_TABLE_SEND_EMAIL_TEXT"
#define NOTIFICATIONS_TABLE_ADD_PHONE_TEXT_KEY                                              @"NOTIFICATIONS_TABLE_ADD_PHONE_TEXT"
#define NOTIFICATIONS_TABLE_ADD_EMAIL_TEXT_KEY                                              @"NOTIFICATIONS_TABLE_ADD_EMAIL_TEXT"
#define NOTIFICATIONS_TABLE_SELECT_CARRIER_TEXT_KEY                                         @"NOTIFICATIONS_TABLE_SELECT_CARRIER_TEXT"
#define NOTIFICATIONS_TABLE_ENTER_MESSAGE_TEXT_KEY                                          @"NOTIFICATIONS_TABLE_ENTER_MESSAGE_TEXT"
#define NOTIFICATIONS_TABLE_SELECT_CARRIER_FILL_KEY                                         @"NOTIFICATIONS_TABLE_SELECT_CARRIER_FILL"
#define NOTIFICATIONS_TABLE_ENTER_MESSAGE_FILL_KEY                                          @"NOTIFICATIONS_TABLE_ENTER_MESSAGE_FILL"
#define NOTIFICATIONS_TABLE_EMAIL_PLACEHOLDER_KEY                                           @"NOTIFICATIONS_TABLE_EMAIL_PLACEHOLDER"
#define NOTIFICATIONS_TABLE_EMAIL_FILL_KEY                                                  @"NOTIFICATIONS_TABLE_EMAIL_FILL"
#define NOTIFICATIONS_TABLE_PHONE_FILL_KEY                                                  @"NOTIFICATIONS_TABLE_PHONE_FILL"
#define NOTIFICATIONS_TABLE_PHONE_ERROR_KEY                                                 @"NOTIFICATIONS_TABLE_PHONE_ERROR" 
#define NOTIFICATIONS_TABLE_PHONE_NO_EXIST_TEXT_KEY                                         @"NOTIFICATIONS_TABLE_PHONE_NO_EXIST_TEXT"
#define NOTIFICATIONS_TABLE_EMAIL_NO_EXIST_TEXT_KEY                                         @"NOTIFICATIONS_TABLE_EMAIL_NO_EXIST_TEXT"
#define NOTIFICATIONS_TABLE_PHONE_EMPTY_KEY                                                 @"NOTIFICATIONS_TABLE_PHONE_EMPTY"

#pragma mark -
#pragma mark Add phone email strings

#define SELECT_CONTACT_TEXT_KEY                                                             @"SELECT_CONTACT_TEXT"
#define NO_PRESENT_INFO_FOR_THIS_CONTACT_TEXT_KEY                                           @"NO_PRESENT_INFO_FOR_THIS_CONTACT_TEXT"

#pragma mark -
#pragma mark Mobile carrier selection

#define MOBILE_CARRIER_SELECTION_TITLE_KEY                                                  @"MOBILE_CARRIER_SELECTION_TITLE"
#define SELECT_MOBILE_CARRIER_KEY                                                           @"SELECT_MOBILE_CARRIER"

#pragma mark -
#pragma mark Account transactions list

#define ACCOUNT_TRANSACTION_LIST_TAB_1_TEXT_KEY                                             @"ACCOUNT_TRANSACTION_LIST_TAB_1_TEXT"
#define ACCOUNT_TRANSACTION_LIST_TAB_2_TEXT_KEY                                             @"ACCOUNT_TRANSACTION_LIST_TAB_2_TEXT"

#pragma mark -
#pragma mark Account graphic view strings

/**
 * Defines the account graphic view strings
 */
#define ACCOUNT_GRAPHIC_HELPER_TEXT_KEY                                                     @"ACCOUNT_GRAPHIC_HELPER_TEXT"

#pragma mark -
#pragma mark Payments title texts

#define PAYMENT_TITLE_TEXT_KEY                                                              @"PAYMENT_TITLE_TEXT"
#define PAYMENT_OF_TEXT_KEY																	@"PAYMENT_OF_TEXT"
#define PAYMENT_CONFIRMATION_OF_TEXT_KEY                                                    @"PAYMENT_CONFIRMATION_OF_TEXT"
#define PAYMENT_INSTITUTIONS_AND_COMPANIES_TITLE_TEXT_KEY									@"PAYMENT_INSTITUTIONS_AND_COMPANIES_TITLE_TEXT"
#define PAYMENT_TO_BUYS_FROM_INTERNET_TITLE_TEXT_KEY										@"PAYMENT_TO_BUYS_FROM_INTERNET_TITLE_TEXT"
#define PAYMENT_RECHARGES_TITLE_TEXT_KEY													@"PAYMENT_RECHARGES_TITLE_TEXT"
#define PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY												@"PAYMENT_PUBLIC_SERVICES_TITLE_TEXT"
#define PAYMENT_PUBLIC_SERVICES_HEADER_TITLE_TEXT_KEY                                       @"PAYMENT_PUBLIC_SERVICES_HEADER_TITLE_TEXT"
#define PAYMENT_CARDS_TITLE_TEXT_KEY														@"PAYMENT_CARDS_TITLE_TEXT"
#define PAYMENT_CARDS_TITLE_LOWER_TEXT_KEY @"PAYMENT_CARDS_TITLE_LOWER_TEXT"
#define PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT_KEY											@"PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT"
#define PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT_LOWER_KEY											@"PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT_LOWER"
#define PAYMENT_REPORT_TO_BENEFICIARY_TEXT_KEY                                              @"PAYMENT_REPORT_TO_BENEFICIARY_TEXT"
#define PAYMENT_SELECT_PAYMENT_MODE_KEY                                                     @"PAYMENT_SELECT_PAYMENT_MODE"

#pragma mark -
#pragma mark Public services

#define PUBLIC_SERVICE_WATER_TEXT_KEY														@"PUBLIC_SERVICE_WATER_TEXT"
#define PUBLIC_SERVICE_WATER_HEADER_TEXT_KEY                                                @"PUBLIC_SERVICE_WATER_HEADER_TEXT"
#define PUBLIC_SERVICE_ELECTRICITY_TEXT_KEY                                                 @"PUBLIC_SERVICE_ELECTRICITY_TEXT"
#define PUBLIC_SERVICE_ELECTRICITY_TEXT_LOWER_KEY                                                 @"PUBLIC_SERVICE_ELECTRICITY_TEXT_LOWER"
#define PUBLIC_SERVICE_LANDLINE_TEXT_KEY                                                    @"PUBLIC_SERVICE_LANDLINE_TEXT"
#define PUBLIC_SERVICE_NOT_CAP_LANDLINE_TEXT_KEY                                                    @"PUBLIC_SERVICE_NOT_CAP_LANDLINE_TEXT"
#define PUBLIC_SERVICE_CELLULAR_TEXT_KEY                                                    @"PUBLIC_SERVICE_CELLULAR_TEXT"
#define PUBLIC_SERVICE_CELLULAR_TEXT_LOWER_KEY                                                    @"PUBLIC_SERVICE_CELLULAR_TEXT_LOWER"

#define PUBLIC_SERVICE_GAS_TEXT_KEY                                                         @"PUBLIC_SERVICE_GAS_TEXT"
#define PUBLIC_SERVICE_CABLE_TEXT_KEY                                                       @"PUBLIC_SERVICE_CABLE_TEXT"
#define PUBLIC_SERVICE_OTPKEY_TEXT_KEY                                                      @"PUBLIC_SERVICE_OTPKEY_TEXT"
#define PUBLIC_SERVICE_LEGAL_TERMS_TEXT_KEY                                                 @"PUBLIC_SERVICE_LEGAL_TERMS_TEXT"
#define PUBLIC_SERVICE_SERVICE_OF_TEXT_KEY                                                  @"PUBLIC_SERVICE_SERVICE_OF_TEXT"
#define PUBLIC_SERVICE_SERVICES_OF_TEXT_KEY                                                 @"PUBLIC_SERVICE_SERVICES_OF_TEXT"
#define PUBLIC_SERVICE_SERVICE_NOT_FOUND_KEY                                                @"PUBLIC_SERVICE_SERVICE_NOT_FOUND"

#pragma mark -
#pragma mark Payments generic error

#define PAYMENT_ERROR_LEGAL_TERMS_TEXT_KEY                                                  @"PAYMENT_ERROR_LEGAL_TERMS_TEXT"
#define PAYMENT_ERROR_OPERATIONS_KEY_TEXT_KEY                                               @"PAYMENT_ERROR_OPERATIONS_KEY_TEXT"
#define PAYMENT_ERROR_COORDINATES_TEXT_KEY                                                  @"PAYMENT_ERROR_COORDINATES_TEXT"
#define PAYMENT_ERROR_PAYMENT_METHOD_TEXT_KEY                                               @"PAYMENT_ERROR_PAYMENT_METHOD_TEXT"
#define PAYMENT_ERROR_CARD_PAYMENT_TEXT_KEY                                                 @"PAYMENT_ERROR_CARD_PAYMENT_TEXT"
#define PAYMENT_ERROR_ACCOUNT_PAYMENT_TEXT_KEY                                              @"PAYMENT_ERROR_ACCOUNT_PAYMENT_TEXT"
#define PAYMENT_ERROR_AMOUNT_TEXT_KEY                                                       @"PAYMENT_ERROR_AMOUNT_TEXT"
#define PAYMENT_ERROR_CURRENCY_TEXT_KEY                                                     @"PAYMENT_ERROR_CURRENCY_TEXT"
#define PAYMENT_ERROR_EMAIL_TEXT_KEY                                                        @"PAYMENT_ERROR_EMAIL_TEXT"
#define PAYMNET_ERROR_PHONE_TEXT_KEY                                                        @"PAYMNET_ERROR_PHONE_TEXT"
#define PAYMENT_ERROR_CARRIER_TEXT_KEY                                                      @"PAYMENT_ERROR_CARRIER_TEXT"
#define PAYMENT_ERROR_BUSINESS_TEXT_KEY                                                     @"PAYMENT_ERROR_BUSINESS_TEXT"
#define PAYMENT_ERROR_PHONE_NUMBER_TEXT_KEY                                                 @"PAYMENT_ERROR_PHONE_NUMBER_TEXT"
#define PAYMENT_ERROR_INVALID_PHONE_OR_CODE_TEXT_KEY                                        @"PAYMENT_ERROR_INVALID_PHONE_OR_CODE_TEXT"
#define PAYMENT_ERROR_OUT_OF_RANGE_PHONE_OR_CODE_TEXT_KEY                                   @"PAYMENT_ERROR_OUT_OF_RANGE_PHONE_OR_CODE_TEXT"
#define PAYMENT_ERROR_BENEFICIARY_NAME_TEXT_KEY                                             @"PAYMENT_ERROR_BENEFICIARY_NAME_TEXT"
#define PAYMENT_ERROR_LOCALITY_TEXT_KEY                                                     @"PAYMENT_ERROR_LOCALITY_TEXT"
#define PAYMENT_SELECT_RECEIPT_TO_CANCEL_TEXT_KEY											@"PAYMENT_SELECT_RECEIPT_TO_CANCEL_TEXT"
#define PAYMENT_ERROR_SUPPLY_NUMBER_TEXT_KEY                                                @"PAYMENT_ERROR_SUPPLY_NUMBER_TEXT"
#define PAYMENT_ERROR_RECEIPTS_MAX_TEXT_KEY                                                 @"PAYMENT_ERROR_RECEIPTS_MAX_TEXT"
#define PAYMENT_ERROR_RECEIPTS_MIN_TEXT_KEY                                                 @"PAYMENT_ERROR_RECEIPTS_MIN_TEXT"
#define PAYMENT_ERROR_RECEIPTS_NOT_OLDER_RECEPIPS_TEXT_KEY                                  @"PAYMENT_ERROR_RECEIPTS_NOT_OLDER_RECEPIPS_TEXT"
#define PAYMENT_ERROR_DIFF_CURRENCIES_TEXT_KEY                                              @"PAYMENT_ERROR_DIFF_CURRENCIES_TEXT"
#define PAYMENT_ERROR_NOT_FOUND_BANK_TEXT_KEY                                               @"PAYMENT_ERROR_NOT_FOUND_BANK_TEXT"
#define PAYMENT_ERROR_CANT_ACCESS_RECHARGES_TEXT_KEY                                        @"PAYMENT_ERROR_CANT_ACCESS_RECHARGES_TEXT"

#define PAYMENT_ERROR_CANT_ACCESS_INTERNET_SHOPPING_TEXT_KEY                                        @"PAYMENT_ERROR_CANT_ACCESS_INTERNET_SHOPPING_TEXT"

#define PAYMENT_ERROR_CANT_ACCESS_PUBLIC_SERVICES_TEXT_KEY                                  @"PAYMENT_ERROR_CANT_ACCESS_PUBLIC_SERVICES_TEXT"
#define PAYMENT_ERROR_CANT_ACCESS_CARD_PAYMENTS_TEXT_KEY                                    @"PAYMENT_ERROR_CANT_ACCESS_CARD_PAYMENTS_TEXT"
#define PAYMENT_ERROR_SELECT_RECEIPT_TO_CANCEL_TEXT_KEY                                     @"PAYMENT_ERROR_SELECT_RECEIPT_TO_CANCEL_TEXT"
#define PAYMENT_ERROR_VALID_AMOUNT_KEY                                                      @"PAYMENT_ERROR_VALID_AMOUNT"
#define PAYMENT_ERROR_AMOUNT_MUST_IN_RANGE_KEY                                              @"PAYMENT_ERROR_AMOUNT_MUST_IN_RANGE"
#define PAYMENT_ERROR_MIN_CHARACTERS_TO_SEARCH_KEY                                          @"PAYMENT_ERROR_MIN_CHARACTERS_TO_SEARCH"


#pragma mark -
#pragma Institutions and Companies

#define INSTITUTIONS_COMPANIES_SEARCH_TITLE_KEY                                             @"INSTITUTIONS_COMPANIES_SEARCH_TITLE"
#define INSTITUTIONS_COMPANIES_LIST_KEY                                                     @"INSTITUTIONS_COMPANIES_LIST"
#define INSTITUTIONS_COMPANIES_SELECTED_SERVICE_TITLE_KEY                                   @"INSTITUTIONS_COMPANIES_SELECTED_SERVICE_TITLE"
#define INSTITUTIONS_COMPANIES_TITLE_TEXT_KEY                                               @"INSTITUTIONS_COMPANIES_TITLE_TEXT"
#define INSTITUTIONS_COMPANIES_ERROR_EMPTY_DATA_KEY                                         @"INSTITUTIONS_COMPANIES_ERROR_EMPTY_DATA"
#define INSTITUTIONS_COMPANIES_HOLDER_NAME_KEY                                              @"INSTITUTIONS_COMPANIES_HOLDER_NAME"
#define INSTITUTIONS_COMPANIES_MIN_AMOUNT_KEY                                               @"INSTITUTIONS_COMPANIES_MIN_AMOUNT"
#define INSTITUTIONS_COMPANIES_MAX_AMOUNT_KEY                                               @"INSTITUTIONS_COMPANIES_MAX_AMOUNT"
#define INSTITUTIONS_COMPANIES_DOC_SELECTION_KEY                                            @"INSTITUTIONS_COMPANIES_DOC_SELECTION"
#define INSTITUTIONS_COMPANIES_PARTIAL_AMOUNT_KEY                                           @"INSTITUTIONS_COMPANIES_PARTIAL_AMOUNT"
#define INSTITUTIONS_COMPANIES_PAY_TYPE_KEY                                                 @"INSTITUTIONS_COMPANIES_PAY_TYPE"
#define INSTITUTIONS_COMPANIES_MIN_AMOUNT_KEY                                               @"INSTITUTIONS_COMPANIES_MIN_AMOUNT"
#define INSTITUTIONS_COMPANIES_MAX_AMOUNT_KEY                                               @"INSTITUTIONS_COMPANIES_MAX_AMOUNT"
#define INSTITUTIONS_COMPANIES_CONFIRMATION_AMOUNT_KEY                                      @"INSTITUTIONS_COMPANIES_CONFIRMATION_AMOUNT"
#define INSTITUTIONS_COMPANIES_DOCUMENTS_NUMBER_KEY                                         @"INSTITUTIONS_COMPANIES_DOCUMENTS_NUMBER"
#define INSTITUTIONS_COMPANIES_PAYED_AMOUNT_KEY                                             @"INSTITUTIONS_COMPANIES_PAYED_AMOUNT"
#define INSTITUTIONS_COMPANIES_CHARGED_AMOUNT_KEY                                           @"INSTITUTIONS_COMPANIES_CHARGED_AMOUNT"
#define INSTITUTIONS_COMPANIES_CONFIRM_TEXT_KEY                                             @"INSTITUTIONS_COMPANIES_CONFIRM_TEXT"
#define INSTITUTIONS_SELECT_GROUP_KEY                                                       @"INSTITUTIONS_SELECT_GROUP"


#pragma mark -
#pragma mark Internet Shopping texts

#define INTERNET_SHOPPING_STEP_ONE_SELECT_BUSINESS_KEY @"INTERNET_SHOPPING_STEP_ONE_SELECT_BUSINESS"

#define INTERNET_SHOPPING_COMPANY_TEXT_KEY @"INTERNET_SHOPPING_COMPANY_TEXT"

#define INTERNET_SHOPPING_TRANS_NUMBER_TEXT_KEY @"INTERNET_SHOPPING_TRANS_NUMBER_TEXT"

#define INTERNET_SHOPPING_BUSINESS_TEXT_KEY @"INTERNET_SHOPPING_BUSINESS_TEXT"

#define INTERNET_SHOPPING_CURRENCY_TEXT_KEY @"INTERNET_SHOPPING_CURRENCY_TEXT"

#define INTERNET_SHOPPING_EXCHANGE_RATE_TEXT_KEY @"INTERNET_SHOPPING_EXCHANGE_RATE_TEXT"

#define INTERNET_SHOPPING_AMOUNT_TEXT_KEY @"INTERNET_SHOPPING_AMOUNT_TEXT"

#define INTERNET_SHOPPING_AMOUNT_PAYED_TEXT_KEY @"INTERNET_SHOPPING_AMOUNT_PAYED_TEXT"

#define INTERNET_SHOPPING_AMOUNT_CHARGED_TEXT_KEY @"INTERNET_SHOPPING_AMOUNT_CHARGED_TEXT"


#define INTERNET_SHOPPING_OPERATION_TEXT_KEY @"INTERNET_SHOPPING_OPERATION_TEXT"

#define INTERNET_SHOPPING_ACCOUNT_CHARGE_TEXT_KEY @"INTERNET_SHOPPING_ACCOUNT_CHARGE_TEXT"

#define INTERNET_SHOPPING_STEP_ONE_TITLE_TEXT_KEY @"INTERNET_SHOPPING_STEP_ONE_TITLE_TEXT"

#define INTERNET_SHOPPING_STEP_TWO_TITLE_TEXT_KEY @"INTERNET_SHOPPING_STEP_TWO_TITLE_TEXT"

#define INTERNET_SHOPPING_STEP_TWO_ENTER_TRANSACTION_NUMBER_KEY @"INTERNET_SHOPPING_STEP_TWO_ENTER_TRANSACTION_NUMBER"

#define INTERNET_SHOPPING_STEP_TWO_ENTER_AMOUNT_KEY @"INTERNET_SHOPPING_STEP_TWO_ENTER_AMOUNT"

#define INTERNET_SHOPPING_STEP_TWO_ENTER_DATA_DESCRIP_KEY @"INTERNET_SHOPPING_STEP_TWO_ENTER_DATA_DESCRIP"

#define INTERNET_SHOPPING_CHOOSE_PAYMENT_MODE_TEXT_KEY                                           @"INTERNET_SHOPPING_CHOOSE_PAYMENT_MODE_TEXT"

#define INTERNET_SHOPPING_ERROR_TRANS_NUMBER_TEXT_KEY                                                @"INTERNET_SHOPPING_ERROR_TRANS_NUMBER_TEXT"

#define INTERNET_SHOPPING_ERROR_AMOUNT_TEXT_KEY                                                @"INTERNET_SHOPPING_ERROR_AMOUNT_TEXT"

#define INTERNET_SHOPPING_ERROR_ACCOUNT_PAYMENT_TEXT_KEY                                              @"INTERNET_SHOPPING_ERROR_ACCOUNT_PAYMENT_TEXT"

#define INTERNET_SHOPPING_OPERATION_NUMBER_TEXT_KEY                                              @"INTERNET_SHOPPING_OPERATION_NUMBER_TEXT"

#define INTERNET_SHOPPING_TITLE_TEXT_KEY                                              @"INTERNET_SHOPPING_TITLE_TEXT"

#define INTERNET_SHOPPING_OPERATION_DATE_TEXT_KEY                                                @"INTERNET_SHOPPING_OPERATION_DATE_TEXT"



#pragma mark -
#pragma mark Public service steps texts

#define PUBLIC_SERVICE_STEP_ONE_LOCATION_TEXT_KEY											@"PUBLIC_SERVICE_STEP_ONE_LOCATION_TEXT"
#define PUBLIC_SERVICE_STEP_ONE_SELECT_BUSINESS_KEY											@"PUBLIC_SERVICE_STEP_ONE_SELECT_BUSINESS"
#define PUBLIC_SERVICE_STEP_ONE_SELECT_COUNTRY_POP_UP_KEY									@"PUBLIC_SERVICE_STEP_ONE_SELECT_COUNTRY_POP_UP"
#define PUBLIC_SERVICE_STEP_ONE_SELECT_COUNTRY_KEY											@"PUBLIC_SERVICE_STEP_ONE_SELECT_COUNTRY"
#define PUBLIC_SERVICE_STEP_ONE_SELECT_PAYMENT_OPTION_TEXT_KEY								@"PUBLIC_SERVICE_STEP_ONE_SELECT_PAYMENT_OPTION_TEXT"
#define PUBLIC_SERVICE_STEP_ONE_ENTRY_NUMBER_OF_SUPPLY_KEY									@"PUBLIC_SERVICE_STEP_ONE_ENTRY_NUMBER_OF_SUPPLY"
#define PUBLIC_SERVICE_STEP_ONE_NUMBER_OF_SUPPLY_KEY										@"PUBLIC_SERVICE_STEP_ONE_NUMBER_OF_SUPPLY"
#define PUBLIC_SERVICE_STEP_ONE_ENTRY_PHONE_NUMBER_OR_CLIENT_CODE_KEY						@"PUBLIC_SERVICE_STEP_ONE_ENTRY_PHONE_NUMBER_OR_CLIENT_CODE"
#define PUBLIC_SERVICE_STEP_ONE_ENTRY_PHONE_NUMBER_KEY										@"PUBLIC_SERVICE_STEP_ONE_ENTRY_PHONE_NUMBER"
#define PUBLIC_SERVICE_STEP_ONE_SELECT_BUSINESS_TITLE_TEXT_KEY                              @"PUBLIC_SERVICE_STEP_ONE_SELECT_BUSINESS_TITLE_TEXT"
#define PUBLIC_SERVICE_STEP_TWO_SELECTED_SERVICE_TEXT_KEY									@"PUBLIC_SERVICE_STEP_TWO_SELECTED_SERVICE_TEXT"
#define PUBLIC_SERVICE_STEP_THREE_CONFIRM_OPERATION_TEXT_KEY								@"PUBLIC_SERVICE_STEP_THREE_CONFIRM_OPERATION_TEXT"
#define PUBLIC_SERVICE_STEP_FOUR_CORRECT_PAYMENT_TITLE_TEXT_KEY								@"PUBLIC_SERVICE_STEP_FOUR_CORRECT_PAYMENT_TITLE_TEXT"
#define RECHARGE_STEP_FOUR_CORRECT_PAYMENT_TITLE_TEXT_LEY                                   @"RECHARGE_STEP_FOUR_CORRECT_PAYMENT_TITLE_TEXT"
#define PUBLIC_SERVICE_STEP_TWO_SELECTED_DUE_DATE_TEXT_KEY                                  @"PUBLIC_SERVICE_STEP_TWO_SELECTED_DUE_DATE_TEXT"
#define PUBLIC_SERVICE_STEP_TWO_SELECTED_INVOICE_DATE_TEXT_KEY								@"PUBLIC_SERVICE_STEP_TWO_SELECTED_INVOICE_DATE_TEXT"
#define PUBLIC_SERVICE_STEP_ONE_PHONE_NUMBER_OR_CLIENT_CODE_KEY                             @"PUBLIC_SERVICE_STEP_ONE_PHONE_NUMBER_OR_CLIENT_CODE"
#define PUBLIC_SERVICE_STEP_ONE_SELECT_BUSINESS_ERROR_KEY                                   @"PUBLIC_SERVICE_STEP_ONE_SELECT_BUSINESS_ERROR"
#define PUBLIC_SERVICE_SETP_TWO_SELECT_LANDLINE_NUMBER_ERROR_KEY                            @"PUBLIC_SERVICE_SETP_TWO_SELECT_LANDLINE_NUMBER_ERROR"
#pragma mark -
#pragma mark Cards payment texts

#define CARD_PAYMENT_TITLE_TEXT_KEY                                                         @"CARD_PAYMENT_TITLE_TEXT"
#define PAYMENT_SEARCH_BY_NAME_KEY                                                         @"PAYMENT_SEARCH_BY_NAME"
#define PAYMENT_SEARCH_BY_GROUP_KEY                                                         @"PAYMENT_SEARCH_BY_GROUP"
#define CARD_PAYMENT_CARD_SELECTION_TEXT_KEY                                                @"CARD_PAYMENT_CARD_SELECTION_TEXT"
#define CARD_PAYMENT_INSTITUTIONS_COMPANIES_TEXT_KEY                                        @"CARD_PAYMENT_INSTITUTIONS_COMPANIES_TEXT"
#define CARD_PAYMENT_BBVA_CARDS_TEXT_KEY                                                    @"CARD_PAYMENT_BBVA_CARDS_TEXT"
#define CARD_PAYMENT_OTHER_BANKS_CARDS_TEXT_KEY                                             @"CARD_PAYMENT_OTHER_BANKS_CARDS_TEXT"
#define CARD_PAYMENT_OWN_CARD_TEXT_KEY                                                      @"CARD_PAYMENT_OWN_CARD_TEXT"
#define CARD_PAYMENT_OWN_CARD_TEXT_LOWER_KEY                                                      @"CARD_PAYMENT_OWN_CARD_TEXT_LOWER"
#define CARD_PAYMENT_THIRD_CARD_TEXT_KEY                                                    @"CARD_PAYMENT_THIRD_CARD_TEXT"
#define CARD_PAYMENT_THIRD_CARD_TEXT_LOWER_KEY                                                    @"CARD_PAYMENT_THIRD_CARD_TEXT_LOWER"
#define CARD_PAYMEN_AMOUNT_TO_PAY_TEXT_KEY                                                  @"CARD_PAYMEN_AMOUNT_TO_PAY_TEXT"
#define CARD_PAYMENT_AMOUNT_TO_PAY_ERROR_KEY                                                @"CARD_PAYMENT_AMOUNT_TO_PAY_ERROR"
#define CARD_PAYMENT_SELECTION_TEXT_KEY                                                     @"CARD_PAYMENT_SELECTION_TEXT"
#define CARD_PAYMENT_RECHARGE_CARD_TEXT_KEY													@"CARD_PAYMENT_RECHARGE_CARD_TEXT"
#define CARD_PAYMENT_SELECT_CARD_TEXT_KEY													@"CARD_PAYMENT_SELECT_CARD_TEXT"
#define CARD_PAYMENT_SELECT_ACCOUNT_TEXT_KEY												@"CARD_PAYMENT_SELECT_ACCOUNT_TEXT"
#define CARD_PAYMENT_BBVA_CARD_PAYMENT_TEXT_KEY                                             @"CARD_PAYMENT_BBVA_CARD_PAYMENT_TEXT"
#define CARD_PAYMENT_MUST_SELECT_A_CARD_ERROR_TEXT_KEY                                      @"CARD_PAYMENT_MUST_SELECT_A_CARD_ERROR_TEXT"
#define CARD_PAYMENT_MUST_INTRODUCE_A_CARD_NUMBER_ERROR_TEXT_KEY                            @"CARD_PAYMENT_MUST_INTRODUCE_A_CARD_NUMBER_ERROR_TEXT"
#define CARD_PAYMENT_MUST_SELECT_A_CHOICE_ERROR_TEXT_KEY                                    @"CARD_PAYMENT_MUST_SELECT_A_CHOICE_ERROR_TEXT"
#define CARD_PAYMENT_THIRD_CARD_TITLE_TEXT_KEY                                              @"CARD_PAYMENT_THIRD_CARD_TITLE_TEXT"
#define CARD_PAYMENT_SELECT_AMOUNT_TEXT_KEY                                                 @"CARD_PAYMENT_SELECT_AMOUNT_TEXT"
#define CARD_PAYMENT_THIRD_ACCOUTS_CARD_NUMBER_TEXT_KEY                                     @"CARD_PAYMENT_THIRD_ACCOUTS_CARD_NUMBER_TEXT"
#define CARD_PAYMENT_THIRD_ACCOUTS_CARD_OWNER_TEXT_KEY                                      @"CARD_PAYMENT_THIRD_ACCOUTS_CARD_OWNER_TEXT"
#define CARD_PAYMENT_THIRD_ACCOUTS_CARD_TYPE_TEXT_KEY                                       @"CARD_PAYMENT_THIRD_ACCOUTS_CARD_TYPE_TEXT"
#define CARD_PAYMENT_THIRD_ACCOUTS_CURRENCY_TEXT_KEY                                        @"CARD_PAYMENT_THIRD_ACCOUTS_CURRENCY_TEXT"
#define CARD_PAYMENT_OPERATION_TEXT_KEY                                                     @"CARD_PAYMENT_OPERATION_TEXT"
#define CARD_PAYMENT_ORIGIN_ACCOUNT_TEXT_KEY                                                @"CARD_PAYMENT_ORIGIN_ACCOUNT_TEXT"
#define CARD_PAYMENT_CARD_NUMBER_TEXT_KEY                                                   @"CARD_PAYMENT_CARD_NUMBER_TEXT"
#define CARD_PAYMENT_OWNER_TEXT_KEY                                                         @"CARD_PAYMENT_OWNER_TEXT"
#define CARD_PAYMENT_OWNER_HINT_TEXT_KEY                                                    @"CARD_PAYMENT_OWNER_HINT_TEXT"
#define CARD_PAYMENT_AMOUNT_TO_PAY_TEXT_KEY                                                 @"CARD_PAYMENT_AMOUNT_TO_PAY_TEXT"
#define CARD_PAYMENT_OPERATION_NUMBER_TEXT_KEY                                              @"CARD_PAYMENT_OPERATION_NUMBER_TEXT"
#define CARD_PAYMENT_OPERATION_DATE_TEXT_KEY                                                @"CARD_PAYMENT_OPERATION_DATE_TEXT"
#define CARD_PAYMENT_VALUE_DATE_TEXT_KEY                                                    @"CARD_PAYMENT_VALUE_DATE_TEXT"
#define CARD_PAYMENT_CHARGED_AMOUNT_TEXT_KEY                                                @"CARD_PAYMENT_CHARGED_AMOUNT_TEXT"
#define CARD_PAYMENT_CHAGE_TYPE_TEXT_KEY                                                    @"CARD_PAYMENT_CHAGE_TYPE_TEXT"
#define CARD_PAYMENT_PAYED_AMOUNT_TEXT_KEY                                                  @"CARD_PAYMENT_PAYED_AMOUNT_TEXT"
#define CARD_PAYMENT_OWN_CARD_CONFIRMATION_TITLE_TEXT_KEY                                   @"CARD_PAYMENT_OWN_CARD_CONFIRMATION_TITLE_TEXT"
#define CARD_PAYMENT_THIRD_CARD_CONFIRMATION_TITLE_TEXT_KEY                                 @"CARD_PAYMENT_THIRD_CARD_CONFIRMATION_TITLE_TEXT"
#define CARD_PAYMENT_CONFIRMATION_BOTTOM_TEXT_KEY                                           @"CARD_PAYMENT_CONFIRMATION_BOTTOM_TEXT"
#define CARD_PAYMENT_SUCCESS_BOTTOM_TEXT_KEY                                                @"CARD_PAYMENT_SUCCESS_BOTTOM_TEXT"
#define CARD_PAYMENT_OWN_CARD_TITLE_TEXT_KEY                                                @"CARD_PAYMENT_OWN_CARD_TITLE_TEXT"
#define CARD_PAYMENT_ACCOUNT_STATUS_TEXT_KEY                                                @"CARD_PAYMENT_ACCOUNT_STATUS_TEXT"
#define CARD_PAYMENT_CREDIT_LINE_TEXT_KEY                                                   @"CARD_PAYMENT_CREDIT_LINE_TEXT"
#define CARD_PAYMENT_AVAILABLE_CREDIT_TEXT_KEY                                              @"CARD_PAYMENT_AVAILABLE_CREDIT_TEXT"
#define CARD_PAYMENT_CURRENCY_TEXT_KEY                                                      @"CARD_PAYMENT_CURRENCY_TEXT"
#define CARD_PAYMENT_LAST_PAYMENT_DAY_TEXT_KEY                                              @"CARD_PAYMENT_LAST_PAYMENT_DAY_TEXT"
#define CARD_PAYMENT_TOTAL_DEBT_SOLES_TEXT_KEY                                              @"CARD_PAYMENT_TOTAL_DEBT_SOLES_TEXT"
#define CARD_PAYMENT_TOTAL_DEBT_DOLARS_TEXT_KEY                                             @"CARD_PAYMENT_TOTAL_DEBT_DOLARS_TEXT"
#define CARD_PAYMENT_SUMMARY_TEXT_KEY                                                       @"CARD_PAYMENT_SUMMARY_TEXT"
#define CARD_PAYMENT_MIN_PAYMENT_TEXT_KEY                                                   @"CARD_PAYMENT_MIN_PAYMENT_TEXT"
#define CARD_PAYMENT_DELAY_TEXT_KEY                                                         @"CARD_PAYMENT_DELAY_TEXT"
#define CARD_PAYMENT_PAID_AMOUNT_TEXT_KEY                                                   @"CARD_PAYMENT_PAID_AMOUNT_TEXT"
#define CARD_PAYMENT_MONTH_TOTAL_PAYMENT_TEXT_KEY                                           @"CARD_PAYMENT_MONTH_TOTAL_PAYMENT_TEXT"
#define CARD_PAYMENT_OTHER_BANK_TITLE_TEXT_KEY                                              @"CARD_PAYMENT_OTHER_BANK_TITLE_TEXT"
#define CARD_PAYMENT_OTHER_BANK_TITLE_TEXT_LOWER_KEY                                              @"CARD_PAYMENT_OTHER_BANK_TITLE_TEXT_LOWER"
#define CARD_PAYMENT_OTHER_BANK_INTRO_TEXT_KEY                                              @"CARD_PAYMENT_OTHER_BANK_INTRO_TEXT"
#define CARD_PAYMENT_OTHER_BANK_CLASS_TEXT_KEY                                              @"CARD_PAYMENT_OTHER_BANK_CLASS_TEXT"
#define CARD_PAYMENT_OTHER_BANK_DEST_BANK_TEXT_KEY                                          @"CARD_PAYMENT_OTHER_BANK_DEST_BANK_TEXT"
#define CARD_PAYMENT_OTHER_BANK_CLASS_SELECTION_TEXT_KEY                                    @"CARD_PAYMENT_OTHER_BANK_CLASS_SELECTION_TEXT"
#define CARD_PAYMENT_OTHER_BANK_BANK_SELECTION_TEXT_KEY                                     @"CARD_PAYMENT_OTHER_BANK_BANK_SELECTION_TEXT"
#define CARD_PAYMENT_OTHER_BANK_CLASS_SELECTION_ERROR_TEXT_KEY                              @"CARD_PAYMENT_OTHER_BANK_CLASS_SELECTION_ERROR_TEXT"
#define CARD_PAYMENT_OTHER_BANK_BANK_SELECTION_ERROR_TEXT_KEY                               @"CARD_PAYMENT_OTHER_BANK_BANK_SELECTION_ERROR_TEXT"
#define CARD_PAYMENT_OTHER_BANK_NAME_OF_BENEFICIARY_TEXT_KEY                                @"CARD_PAYMENT_OTHER_BANK_NAME_OF_BENEFICIARY_TEXT"
#define CARD_PAYMENT_OTHER_BANK_SITE_OF_EMISION_TEXT_KEY                                    @"CARD_PAYMENT_OTHER_BANK_SITE_OF_EMISION_TEXT"
#define CARD_PAYMENT_OTHER_BANK_PAYMENT_TO_CARD_TEXT_KEY									@"CARD_PAYMENT_OTHER_BANK_PAYMENT_TO_CARD_TEXT"
#define CARD_PAYMENT_OTHER_BANK_DEST_BANK_LONG_TEXT_KEY                                     @"CARD_PAYMENT_OTHER_BANK_DEST_BANK_LONG_TEXT"
#define CARD_PAYMENT_OTHER_BANK_BENEFICIARY_TEXT_KEY                                        @"CARD_PAYMENT_OTHER_BANK_BENEFICIARY_TEXT"
#define CARD_PAYMENT_OTHER_BANK_AMOUNT_TO_TRANSFER_TEXT_KEY									@"CARD_PAYMENT_OTHER_BANK_AMOUNT_TO_TRANSFER_TEXT"
#define CARD_PAYMENT_OTHER_BANK_TAX_TEXT_KEY                                                @"CARD_PAYMENT_OTHER_BANK_TAX_TEXT"
#define CARD_PAYMENT_OTHER_BANK_NETWORK_USE_TEXT_KEY                                        @"CARD_PAYMENT_OTHER_BANK_NETWORK_USE_TEXT"
#define CARD_PAYMENT_OTHER_BANK_OWNER_TEXT_KEY                                              @"CARD_PAYMENT_OTHER_BANK_OWNER_TEXT"
#define CARD_PAYMENT_OTHER_BANK_AMOUNT_TEXT_KEY                                             @"CARD_PAYMENT_OTHER_BANK_AMOUNT_TEXT"
#define CARD_PAYMENT_OTHER_BANK_TOTAL_AMOUNT_TEXT_KEY                                       @"CARD_PAYMENT_OTHER_BANK_TOTAL_AMOUNT_TEXT"
#define CARD_PAYMENT_OTHER_BANK_FINAL_AMOUNT_TEXT_KEY                                       @"CARD_PAYMENT_OTHER_BANK_FINAL_AMOUNT_TEXT"
#define CARD_PAYMENT_PAYMENT_CURRENCY_TEXT_KEY                                              @"CARD_PAYMENT_PAYMENT_CURRENCY_TEXT"
#define CARD_PAYMENT_CHOOSE_PAYMENT_MODE_TEXT_KEY                                           @"CARD_PAYMENT_CHOOSE_PAYMENT_MODE_TEXT"
#define CARD_PAYMENT_OTHER_BANK_CURRENCY_TEXT_KEY                                           @"CARD_PAYMENT_OTHER_BANK_CURRENCY_TEXT"
#define CARD_PAYMENT_OTHER_BANK_CARD_DATA_TEXT_KEY                                          @"CARD_PAYMENT_OTHER_BANK_CARD_DATA_TEXT"
#define CARD_PAYMENT_OTHER_BANK_AMOUNT_TO_PAY_TEXT_KEY                                      @"CARD_PAYMENT_OTHER_BANK_AMOUNT_TO_PAY_TEXT"

#pragma mark -
#pragma mark Payment recharge texts

#define PAYMENT_RECHARGE_RECHARGE_TITLE_KEY                                                 @"PAYMENT_RECHARGE_RECHARGE_TITLE"
#define PAYMENT_RECHARGE_NUMBER_OF_OPERATION_KEY											@"PAYMENT_RECHARGE_NUMBER_OF_OPERATION"
#define PAYMENT_RECHARGE_DATE_KEY															@"PAYMENT_RECHARGE_DATE"
#define PAYMENT_RECHARGE_OPERATION_TEXT_KEY													@"PAYMENT_RECHARGE_OPERATION_TEXT"
#define PAYMENT_RECHARGE_BUSINESS_TEXT_KEY													@"PAYMENT_RECHARGE_BUSINESS_TEXT"
#define PAYMENT_RECHARGE_ORIGIN_ACCOUNT_TEXT_KEY											@"PAYMENT_RECHARGE_ORIGIN_ACCOUNT_TEXT"
#define PAYMENT_RECHARGE_PHONE_AND_CLIENT_CODE_TEXT_KEY										@"PAYMENT_RECHARGE_PHONE_AND_CLIENT_CODE_TEXT"
#define PAYMENT_RECHARGE_AMOUNT_OF_RECHARGE_TEXT_KEY										@"PAYMENT_RECHARGE_AMOUNT_OF_RECHARGE_TEXT"
#define PAYMENT_RECHARGE_AMOUNT_TO_RECHARGE_TEXT_KEY										@"PAYMENT_RECHARGE_AMOUNT_TO_RECHARGE_TEXT"
#define PAYMENT_RECHARGE_AMOUNT_CHARGED_TEXT_KEY											@"PAYMENT_RECHARGE_AMOUNT_CHARGED_TEXT"
#define PAYMENT_RECHARGE_DAYS_OF_USE_TEXT_KEY												@"PAYMENT_RECHARGE_DAYS_OF_USE_TEXT"
#define PAYMENT_RECHARGE_STEP_TWO_CONFIRMATION_TEXT_KEY										@"PAYMENT_RECHARGE_STEP_TWO_CONFIRMATION_TEXT"
#define PAYMENT_RECHARGE_TITLE_TEXT_KEY                                                     @"PAYMENT_RECHARGE_TITLE_TEXT"
#define PAYMENT_RECHARGE_OPERATION_MOVISTAR_TEXT_KEY										@"PAYMENT_RECHARGE_OPERATION_MOVISTAR_TEXT"
#define PAYMENT_RECHARGE_DAYS_OF_USE_TEXT_KEY												@"PAYMENT_RECHARGE_DAYS_OF_USE_TEXT"
#define PAYMENT_RECHARGE_OPERATION_CLARO_TEXT_KEY											@"PAYMENT_RECHARGE_OPERATION_CLARO_TEXT"
#define PAYMENT_RECHARGE_ERROR_NUMBER_OF_PHONE_TEXT_KEY										@"PAYMENT_RECHARGE_ERROR_NUMBER_OF_PHONE_TEXT"
#define PAYMENT_RECHARGE_ERROR_INVALID_NUMBER_TEXT_KEY										@"PAYMENT_RECHARGE_ERROR_INVALID_NUMBER_TEXT"
#define PAYMENT_RECHARGE_ERROR_INVALID_MONTO_TEXT_KEY										@"PAYMENT_RECHARGE_ERROR_INVALID_MONTO_TEXT"
#define PAYMENT_RECHARGE_ERROR_EMPTY_MONTO_TEXT_KEY                                         @"PAYMENT_RECHARGE_ERROR_EMPTY_MONTO_TEXT"
#define PAYMENT_RECHARGE_ERROR_INVALID_BUSINESS_TEXT_KEY									@"PAYMENT_RECHARGE_ERROR_INVALID_BUSINESS_TEXT"
#define PAYMENT_RECHARGE_BUSINESS_TITLE_TEXT_KEY                                            @"PAYMENT_RECHARGE_BUSINESS_TITLE_TEXT"
#define PAYMENT_RECHARGE_BUSINESS_COMBO_DEFAULT_TEXT_KEY                                    @"PAYMENT_RECHARGE_BUSINESS_COMBO_DEFAULT_TEXT"
#define PAYMENT_RECHARGE_AMOUNT_TEXT_KEY                                                    @"PAYMENT_RECHARGE_AMOUNT_TEXT"
#define PAYMENT_RECHARGE_SET_NUMBRE_TEXT_KEY                                                @"PAYMENT_RECHARGE_SET_NUMBRE_TEXT"

#pragma mark -
#pragma mark Frequents Operation

#define FO_TITLE_TEXT_REACTIVE_KEY @"FO_TITLE_TEXT_REACTIVE"

#define FO_AFFILIATION_TITLE_TEXT_KEY @"FO_AFFILIATION_TITLE_TEXT"

#define FO_TITLE_TEXT_KEY                                                                   @"FO_TITLE_TEXT"
#define FO_POSITION_TITLE_TEXT_KEY                                                          @"FO_POSITION_TITLE_TEXT"
#define FO_ACTIVE_DAY_TEXT_KEY                                                              @"FO_ACTIVE_DAY_TEXT"
#define FO_ENROLL_TEXT_KEY                                                                  @"FO_ENROLL_TEXT"
#define FO_HEADER_TITLE_KEY                                                                 @"FO_HEADER_TITLE"
#define FO_CONFIRM_HEADER_TITLE_KEY                                                         @"FO_CONFIRM_HEADER_TITLE"
#define FO_OPERATION_DONE_KEY                                                               @"FO_OPERATION_DONE"
#define FO_AMOUNT_TITLE_TEXT_KEY                                                            @"FO_AMOUNT_TITLE_TEXT"
#define FO_AMOUNT_TO_CHARGE_TITLE_TEXT_KEY                                                  @"FO_AMOUNT_TO_CHARGE_TITLE_TEXT"
#define FO_CURRENCY_TITLE_TEXT_KEY                                                          @"FO_CURRENCY_TITLE_TEXT"
#define FO_CHARGE_ACCOUNT_TITLE_TEXT_KEY                                                    @"FO_CHARGE_ACCOUNT_TITLE_TEXT"
#define FO_NUMBER_CHARGE_ACCOUNT_TITLE_TEXT_KEY                                             @"FO_NUMBER_CHARGE_ACCOUNT_TITLE_TEXT"
#define FO_COMPLEMENTARY_DATA_TITLE_TEXT_KEY                                                @"FO_COMPLEMENTARY_DATA_TITLE_TEXT"
#define FO_ACCOUNT_DESTINATION_TEXT_KEY                                                     @"FO_ACCOUNT_DESTINATION_TEXT"
#define FO_HOLDER_ACCOUNT_TEXT_KEY                                              @"FO_HOLDER_ACCOUNT_TEXT"
#define FO_OPERATION_TYPE_TEXT_KEY                                                          @"FO_OPERATION_TYPE_TEXT"
#define FO_BENEFICIARY_ACCOUNT_NUMBER_TEXT_KEY                                                   @"FO_BENEFICIARY_ACCOUNT_NUMBER_TEXT"

#define FO_ERROR_SELECT_OPERATION_KEY                                                       @"FO_ERROR_SELECT_OPERATION"
#define FO_REPORT_TO_BENEFICIARY_TEXT_KEY                                                   @"FO_REPORT_TO_BENEFICIARY_TEXT"
#define FO_ASSOCIATED_ALIAS_TEXT_KEY                                                        @"FO_ASSOCIATED_ALIAS_TEXT"
#define FO_SELECT_CHANNELS_TEXT_KEY                                                         @"FO_SELECT_CHANNELS_TEXT"

#define FO_SELECT_ONLINE_BANKING_TEXT_KEY                                                   @"FO_SELECT_ONLINE_BANKING_TEXT"

#define FO_SELECT_MOBILE_BANKING_TEXT_KEY                                                   @"FO_SELECT_MOBILE_BANKING_TEXT"
#define FO_OPERATION_DONE_TITLE_TEXT_KEY                                                    @"FO_OPERATION_DONE_TITLE_TEXT"
#define FO_TRANSFER_TO_ANOTHER_BANK_DESTINATION_TITLE                                       @"FO_TRANSFER_TO_ANOTHER_BANK_DESTINATION_TITLE"
#define FO_DESTINATION_BANK_TITLE_TEXT_KEY                                                  @"FO_DESTINATION_BANK_TITLE_TEXT"

#define FO_COMPANY_NAME_TITLE_TEXT_KEY                                                      @"FO_COMPANY_NAME_TITLE_TEXT"
#define FO_NUMBER_OF_SUPPLY_TITLE_TEXT_KEY                                                  @"FO_NUMBER_OF_SUPPLY_TITLE_TEXT"

#define FO_SERVICE_TITLE_KEY                                                                @"FO_SERVICE_TITLE_TEXT"
#define FO_SERVICE_TYPE_TITLE_TEXT_KEY                                                      @"FO_SERVICE_TYPE_TITLE_TEXT"
#define FO_PAYMENT_CODE_TITLE_TEXT_KEY                                                      @"FO_PAYMENT_CODE_TITLE_TEXT"
#define FO_SDS_NUMBER_TITLE_TEXT_KEY                                                        @"FO_SDS_NUMBER_TITLE_TEXT"
#define FO_AGREEMENT_TITLE_TEXT_KEY                                                         @"FO_AGREEMENT_TITLE_TEXT"
#define FO_ACCESS_CODE_TITLE_TEXT_KEY                                                       @"FO_ACCESS_CODE_TITLE_TEXT"
#define FO_PAYMENT_DESCRIPTION_TITLE_TEXT_KEY                                               @"FO_PAYMENT_DESCRIPTION_TITLE_TEXT"
#define FO_GIFT_CARD_TITLE_TEXT_KEY                                                         @"FO_GIFT_CARD_TITLE_TEXT"

#define FO_OPERATION_TITLE_TEXT_KEY                                                         @"FO_OPERATION_TITLE_TEXT"
#define FO_SERVICE_TITLE_TEXT_KEY                                                           @"FO_SERVICE_TITLE_TEXT"
#define FO_ORIGIN_TITLE_TEXT_KEY                                                            @"FO_ORIGIN_TITLE_TEXT"
#define FO_PAYMENT_ACCOUNT_TEXT_KEY                                                         @"FO_PAYMENT_ACCOUNT_TEXT"
#define FO_HOLDER_NAME_TITLE_TEXT_KEY                                                       @"FO_HOLDER_NAME_TITLE_TEXT"

#define FO_PHONE_AND_CLIENT_CODE_TEXT_KEY                                                   @"FO_PHONE_AND_CLIENT_CODE_TEXT"

#define FO_SERVICE_HOLDER_NAME_TITLE_TEXT_KEY                                               @"FO_SERVICE_HOLDER_NAME_TITLE_TEXT"



#define FO_HOLDER_BENEFICIARY_ACCOUNT_TEXT_KEY                                              @"FO_HOLDER_BENEFICIARY_ACCOUNT_TEXT"
#define FO_INTERBANK_ACCOUNT_NUMBER_TITLE_TEXT_KEY                                               @"FO_INTERBANK_ACCOUNT_NUMBER_TITLE_TEXT"

#define FO_OWN_ACCOUNT_TITLE_TEXT_KEY                                                       @"FO_OWN_ACCOUNT_TITLE_TEXT"

#define FO_TRANSFER_TO_ANOTHER_BANK_DESTINATION_TITLE_TEXT_KEY @"FO_TRANSFER_TO_ANOTHER_BANK_DESTINATION_TITLE_TEXT"

#define FO_CARD_NUMBER_TITLE_TEXT_KEY                                                       @"FO_CARD_NUMBER_TITLE_TEXT"
#define FO_LARGE_ALIAS_TITLE_TEXT_KEY                                                       @"FO_LARGE_ALIAS_TITLE_TEXT"
#define FO_SHORT_ALIAS_TITLE_TEXT_KEY                                                       @"FO_SHORT_ALIAS_TITLE_TEXT"
#define FO_DAY_REMINDER_TITLE_TEXT_KEY                                                      @"FO_DAY_REMINDER_TITLE_TEXT"
#define FO_CELLPHONE_TITLE_TEXT_KEY                                                         @"FO_CELLPHONE_TITLE"
#define FO_EMAIL_TITLE_TEXT_KEY                                                             @"FO_EMAIL_TITLE_TEXT"
#define FO_CHANNELS_TITLE_TEXT_KEY                                                          @"FO_CHANNELS_TITLE_TEXT"
#define FO_INSCRIPTION_DATE_TITLE_TEXT_KEY                                                  @"FO_INSCRIPTION_DATE_TITLE_TEXT"
#define FO_THIRD_CARD_TITLE_TEXT_KEY                                                        @"FO_THIRD_CARD_TITLE_TEXT"
#define FO_PAYMENT_THIRD_CARD_TYPE_TITLE_TEXT_KEY                                           @"FO_PAYMENT_THIRD_CARD_TYPE_TITLE_TEXT"
#define FO_CARD_PAYMENT_OTHER_BANK_SITE_OF_EMISION_TEXT_KEY                                 @"FO_CARD_PAYMENT_OTHER_BANK_SITE_OF_EMISION_TEXT"
#define FO_BENEFICIARY_NAME_TITLE_TEXT_KEY                                                  @"FO_BENEFICIARY_NAME_TITLE_TEXT"

#define FO_BENEFICIARY_PHONE_NUMBER_TITLE_KEY                                               @"FO_BENEFICIARY_PHONE_NUMBER_TITLE"
#define FO_BENEFICIARY_TITLE_TEXT_KEY                                                       @"FO_BENEFICIARY_TITLE_TEXT"
#define FO_DOCUMENT_TYPE_TITLE_TEXT_KEY                                                     @"FO_DOCUMENT_TYPE_TITLE_TEXT"
#define FO_BENEFICIARY_DOCUMENT_NUMBER_TITLE_TEXT_KEY                                           @"FO_BENEFICIARY_DOCUMENT_NUMBER_TITLE_TEXT"
#define FO_CHARGE_AMOUNT_TITLE_TEXT_KEY                                                     @"FO_CHARGE_AMOUNT_TITLE_TEXT"
#define FO_CARD_PAYMENT_CARD_NUMBER_TEXT_KEY                                                @"FO_CARD_PAYMENT_CARD_NUMBER_TEXT"
#define FO_CARD_PAYMENT_OTHER_BANK_CLASS_TEXT_KEY                                           @"FO_CARD_PAYMENT_OTHER_BANK_CLASS_TEXT"
#define FO_CARD_PAYMENT_OTHER_BANK_OWNER_TEXT_KEY                                           @"FO_CARD_PAYMENT_OTHER_BANK_OWNER_TEXT"
#define FO_CARD_PAYMENT_OTHER_BANK_AMOUNT_TEXT_KEY                                          @"FO_CARD_PAYMENT_OTHER_BANK_AMOUNT_TEXT"
#define FO_CARD_PAYMENT_OTHER_BANK_TOTAL_AMOUNT_TEXT_KEY                                    @"FO_CARD_PAYMENT_OTHER_BANK_TOTAL_AMOUNT_TEXT"

#define FO_BACK_TO_OPERATION_TITLE_TEXT_KEY                                                 @"FO_BACK_TO_OPERATION_TITLE_TEXT"
#define FO_PAYMENT_RECHARGE_PHONE_AND_CLIENT_CODE_TEXT_KEY                                  @"FO_PAYMENT_RECHARGE_PHONE_AND_CLIENT_CODE_TEXT"
#define FO_INSTITUTIONS_COMPANIES_TITLE_TEXT_KEY                                            @"FO_INSTITUTIONS_COMPANIES_TITLE_TEXT"
#define FO_AGREEMENTS_DATA_TITLE_TEXT_KEY                                                   @"FO_AGREEMENTS_DATA_TITLE_TEXT"
#define FO_OPERATION_DATE_TITLE_TEXT_KEY                                                    @"FO_OPERATION_DATE_TITLE_TEXT"
#define FO_SERVICE_CODE_TITLE_TEXT_KEY                                                      @"FO_SERVICE_CODE_TITLE_TEXT"

#define FO_ERROR_SELECT_AT_LEAST_ONE_CHANNEL_TEXT_KEY                                       @"FO_ERROR_SELECT_AT_LEAST_ONE_CHANNEL_TEXT"
#define FO_ERROR_ENTER_OPERATION_DESCRIPTION_TEXT_KEY                                       @"FO_ERROR_ENTER_OPERATION_DESCRIPTION_TEXT"
#define FO_ERROR_ENTER_SHORT_ALIAS_TEXT_KEY                                                 @"FO_ERROR_ENTER_SHORT_ALIAS_TEXT"
#define FO_ERROR_SHORT_ALIAS_MINLENGTH_TEXT_KEY                                             @"FO_ERROR_SHORT_ALIAS_MINLENGTH_TEXT"
#define FO_ERROR_SHORT_ALIAS_CONTAINS_WHITESPACE_TEXT_KEY                                   @"FO_ERROR_SHORT_ALIAS_CONTAINS_WHITESPACE_TEXT"
#define FO_ERROR_SELECT_NOTIFICATION_TYPE_TEXT_KEY                                          @"FO_ERROR_SELECT_NOTIFICATION_TYPE_TEXT"
#define FO_ERROR_VALID_EMAIL_TEXT_KEY                                                       @"FO_ERROR_VALID_EMAIL_TEXT"
#define FO_ERROR_FILL_PHONE_TEXT_KEY                                                        @"FO_ERROR_FILL_PHONE_TEXT"
#define FO_ERROR_NOT_PHONE_AND_EMAIL_TEXT_KEY                                               @"FO_ERROR_NOT_PHONE_AND_EMAIL_TEXT"
#pragma mark -
#pragma mark OTP Service strings

#define OTP_SERVICE_NO_ACTIVE_TEXT_KEY                                                      @"OTP_SERVICE_NO_ACTIVE_TEXT" 

#pragma mark -
#pragma mark MOK Editable texts

#define TXT_MOK_EDITABLE_VIEW_CONTROLLER_PREVIOUS                                           @"TXT_MOK_EDITABLE_VIEW_CONTROLLER_PREVIOUS"
#define TXT_MOK_EDITABLE_VIEW_CONTROLLER_NEXT                                               @"TXT_MOK_EDITABLE_VIEW_CONTROLLER_NEXT"
#define TXT_MOK_EDITABLE_VIEW_CONTROLLER_OK                                                 @"TXT_MOK_EDITABLE_VIEW_CONTROLLER_OK"

#define FC_MESSAGE_INFO_TEXT_KEY                                                            @"FC_MESSAGE_INFO_TEXT"
