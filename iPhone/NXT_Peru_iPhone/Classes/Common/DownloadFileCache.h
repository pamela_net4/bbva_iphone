/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


/**
 * Maintains a cache of downloaded files inside a directory. It manages the cache
 * size, aplying policies to remove old files and free space when a critical limit
 * is reached
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface DownloadFileCache : NSObject {
    
@private
    
    /**
     * Dictionary to reference the cache information.  The information associates is an NSMutableArray with four elements:
     * 1.- The NSTimeInterval since 2001/01/01 when the information was downloaded (stored as a double type stored inside an NSNumber)
     * 2.- The NSTimeInterval since 2001/01/01 when the information was last accessed (stored as a double type stored inside an NSNumber)
     * 3.- The number of times the cached information was accessed (stored as an NSUInteger inside a NSNumber)
     * 4.- The file name where the associated cached information is stored (the path is not included). Files names are random numbers
     */
    NSMutableDictionary *cacheInformation_;
    
    /**
     * Cache directory (application's document directory not included)
     */
    NSString *cacheDirectory_;
    
    /**
     * Cache information file name (directory is not included)
     */
    NSString *cacheInformationFileName_;
    
    /**
     * Maximum time a downloaded file can be cached, measured in seconds. When maximum time is set to zero, the downloaded file never expires
     */
    NSTimeInterval expirationTime_;
    
    /**
     * Maximum space ocupied by the cached files
     */
    unsigned long long maximumCacheUsedSpace_;
    
}

/**
 * Designated initializer. It initializes a DownloadFileCache instance that stores information in the provided directory
 * and uses the provided cache information file name. Cache directory and cache information file name cannot be empty, or
 * the instance will be released and nil returned
 *
 * @param aCacheDirectory The cache directory
 * @param aCacheInformationFileName The cache information file name (directory is not included)
 * @param anExpirationTime The maximum time downloaded files can be cached. Zero to avoid file expiration
 * @param aMaximumCacheUsedSpace The maximum space ocupied by the cached files
 * @return The initialized DownloadFileCahce instance, or nil if directory or information file name are empty
 */
- (id)initWithCacheDirectory:(NSString *)aCacheDirectory
    cacheInformationFileName:(NSString *)aCacheInformationFileName
              expirationTime:(NSTimeInterval)anExpirationTime
                maximumSpace:(unsigned long long)aMaximumCacheUsedSpace;

/**
 * Returns the cached file path (directory included) associated to the key string
 *
 * @param aKeyString The key string which associated cache file must be retrieved
 * @return The cached file path associated to the key string
 */
- (NSString *)cachedFilePathForKey:(NSString *)aKeyString;

/**
 * Stores the provided data associated to the download key string. When previous data is associated to the download key string,
 * it is replaced by the new one
 *
 * @param aDownloadedData The data downloaded
 * @param aKeyString The download key string
 */
- (void)storeData:(NSData *)aDownloadedData
     forKeyString:(NSString *)aKeyString;

/**
 * Removes the data associated to the provided key
 *
 * @param aKeyString The key refering to the data to remove
 */
- (void)removeDataForKey:(NSString *)aKeyString;

/**
 * Updates the cache information for the given download key string. The last access time is updated to current
 * time and accesses count is added one
 *
 * @param aKeyString The download key string to update its cache information
 */
- (void)updateCacheInformationForKey:(NSString *)aKeyString;


/**
 * Checks the cache policy in case it is necesary to delete some cached files. Cached files older that a defined time
 * are automaticaly deleted, even if no space is needed
 */
- (void)checkCachePolicy;

@end
