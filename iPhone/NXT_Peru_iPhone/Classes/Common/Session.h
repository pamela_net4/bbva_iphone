/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "SingletonBase.h"


//Forward declarations
@class GlobalAdditionalInformation;
@class AccountList;
@class CardList;
@class DepositList;
@class LoanList;
@class StockMarketAccountList;
@class MutualFundList;
@class BankAccount;
@class AccountTransaction;
@class ListaCuentas;
@class SafetyPayStatusAdditionalInformation;
@class SafetyPayTransactionAdditionalInformation;
@class SafetyPayDetailsAdditionalInformation;
@class SafetyPayConfirmationAdditionalInformation;

/**
 * Session singleton. Contains the user banking information needed to operate
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Session : SingletonBase {
    
@private
    
    ListaCuentas *listaCuentas_;
    /**
     * Global position additional information
     */
    GlobalAdditionalInformation *additionalInformation_;
    
    /**
     * Account list
     */
    AccountList *accountList_;
    
    /**
     * Credit card list
     */
    CardList *cardList_;
    
    /**
     * List of deposits
     */
    DepositList *depositsList_;
    
    /**
     * List of mutual funds
     */
    MutualFundList *mutualFundsList_;
    
    /**
     * List of loans
     */
    LoanList *loansList_;
	
	/**
     * List of stock market accounts
     */
	StockMarketAccountList *stockMarketAccountsList_;
    
    /**
     * Account transaction that is being updated
     */
    AccountTransaction *updatingTransaction_;
    
    /**
     * Account that is being updated
     */
    BankAccount *updatingAccount_;
    
    /**
     * Flag to load the global position view in every appear
     */
    BOOL globalPositionRequestServerData_;
    
    /**
     * Mobile carriers
     */
    NSArray *mobileCarriers_;
    
    /**
     * Accounts
     */
    NSArray *accounts_;

    /**
     * Flag to indicate the user is logged
     */
    BOOL isLogged_;
    
    /**
	 * The locality dictionary.
	 */
	NSDictionary *localityDictionary_;
    
}

/**
 * Provides readwrite access to the mobileCarriers
 */
@property (nonatomic, readwrite, retain) NSArray *mobileCarriers;

/**
 * Provides readwrite access to the accounts
 */
@property (nonatomic, readwrite, retain) NSArray *accounts;

/**
 * Provides read-write access to the globalPositionRequestServerData flag
 */
@property (nonatomic, readwrite) BOOL globalPositionRequestServerData;

/**
 * Provides read-only access to the global position additional information
 */
@property (nonatomic, readonly, retain) GlobalAdditionalInformation *additionalInformation;


@property (nonatomic, readonly, retain) ListaCuentas *listaCuentas;

/**
 * Provides read-only access to the account list
 */
@property (nonatomic, readonly, retain) AccountList *accountList;

/**
 * Provides read-only access to the credit card list
 */
@property (nonatomic, readonly, retain) CardList *cardList;

/**
 * Provides read-only access to the deposits list
 */
@property (nonatomic, readonly, retain) DepositList *depositsList;

/**
 * Provides read-only access to the mutual funds list
 */
@property (nonatomic, readonly, retain) MutualFundList *mutualFundsList;

/**
 * Provides read-only access to the loans list
 */
@property (nonatomic, readonly, retain) LoanList *loansList;

/**
 * Provides read-only access to the stock market accounts list
 */
@property (nonatomic, readonly, retain) StockMarketAccountList *stockMarketAccountsList;

/**
 * Provides read-write access to the isLogged flag
 */
@property (nonatomic, readwrite) BOOL isLogged;

/**
 * Provides read-write access to localityDictionary.
 */
@property (nonatomic, readwrite, retain) NSDictionary *localityDictionary;


/**
 * Provides access to the singleton only instance
 *
 * @return The singleton only instance
 */
+ (Session *)getInstance;

/**
 * Remove session data
 */
- (void)removeData;

/**
 * Update transactions for an account
 *
 * @param account The account
 */
- (void)updateTransactionsForAccount:(BankAccount *)account;

@end
