/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


/**
 * Group of class functions to manage application directories
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface DirConfiguration : NSObject {

}

/**
 * Creates application directory structure
 */
+ (void)createDirectoryStructure;

/**
 * Returns application documents directory name
 *
 * @return Application documents directory name
 */
+ (NSString *)localDocumentsDirectory;

/**
 * Returns application download directory name
 *
 * @return Application download directory name
 */
+ (NSString *)downloadDirectory;

/**
 * Returns application house keeping directory name
 *
 * @return Application house keeping directory name
 */
+ (NSString *)houseKeepingDirectory;

@end
