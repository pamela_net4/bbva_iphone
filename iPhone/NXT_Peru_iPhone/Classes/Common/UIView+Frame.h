/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>

/**
 * Category to get and set UIView's frame properties directly, without having to get the frame at the beginning
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface UIView(Frame)

/**
 * Provides readwrite access to frame.origin.x
 */
@property (nonatomic, readwrite, assign) CGFloat originX;

/**
 * Provides readwrite access to frame.origin.y
 */
@property (nonatomic, readwrite, assign) CGFloat originY;

/**
 * Provides readwrite access to frame.size.width
 */
@property (nonatomic, readwrite, assign) CGFloat width;

/**
 * Provides readwrite access to frame.size.height
 */
@property (nonatomic, readwrite, assign) CGFloat height;

@end
