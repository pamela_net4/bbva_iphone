/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "SingletonBase.h"


/**
 * Singleton to load an object from a NIB file. The Nib loader must be the owner of that NIB
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NibLoader : SingletonBase {

@private

    /**
     * Auxiliary NSObject to construct and autoreleased object from the NIB file
     */
    NSObject *auxObject_;
    
}

/**
 * Provides read-write access to the auxiliary NSObject to construct an autoreleased object from the NIB file and exports it for Interface Builder
 */
@property (nonatomic, readwrite, assign) IBOutlet NSObject *auxObject;

/**
 * Creates the autoreleased object from the NIB file
 *
 * @param nibFile The NIB file to construct the view from
 */
+ (NSObject *)loadObjectFromNIBFile:(NSString *)nibFile;

@end
