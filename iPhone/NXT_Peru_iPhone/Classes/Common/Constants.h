/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

//TEST

/**
 * Defines the application version format as shown to user
 */
#define APP_VERSION_FORMAT                                              @"%@"

/**
 * Defines the application version as shown to user
 */
#define APP_VERSION_STRING                                              [NSString stringWithFormat:APP_VERSION_FORMAT, [Tools bundleVersion]]

/*
 * Defines the application country
 */
#define APP_COUNTRY                                                     @"PE"

/**
 * Defines the send version parameters in the login operation. Comment this line to avoid sending the version parameters (app version, device model, and OS version)
 */
//#define SEND_VERSION_PARAMETES_ON_LOGIN

/**
 * Automatic log-out time-out (measured in seconds)
 */
#ifndef DEBUG
#define AUTOMATIC_LOGOUT_TIMEOUT                                        300.0f
#else
#define AUTOMATIC_LOGOUT_TIMEOUT                                        300.0f
#endif

/** Max length for login user */
#define LOGIN_USER_MAX_LENGTH                                           16

/** Max length for login password */
#define LOGIN_PASSWORD_MAX_LENGTH                                       9

/** Max length for alias */
#define TARGET_ALIAS_MAX_LENGTH                                         30

/**
 * Define the Coordinates key maximum lenght
 */
#define COORDINATES_MAXIMUM_LENGHT                                      3

/**
 * Define the operation key maximum lenght
 */
#define OTP_MAXIMUM_LENGHT                                              6


#if defined(DEBUGLOG)
/**
 * Defines the remove log enable flag. To send application use logs to MAT, comment this line. Uncomment this line to avoid sending
 * logs to MAT. This flag is only available on debug mode (on release mode, MAT log is always enabled)
 */
//#define NOT_MAT_LOGGING_ENABLED
#endif //DEBUGLOG


#define SIGNUP_URL                                                      @"http://www.bbvacontinental.pe"
#define FORGOT_PASSWORD_URL                                             @"http://www.bbvacontinental.pe"

/**
 * URL of "Beneficios para ti" option
 */
#define BENEFITS_FOR_YOU_URL                                            @"http://www.contiweb.com.pe/tridium/ipad/index.html"

/**
 * Default delay for the performSelector
 */
#define PERFORM_SELECTOR_DEFAULT_DELAY                                  1.0

/**
 * Defines the value of a day in seconds
 */
#define A_DAY_IN_SECONDS                                                86400.0f

/**
 * Timeout used on the request (in seconds)
 */
#define HTTP_REQUEST_TIMEOUT                                            120

/**
 * Defines the delay of the simulated http connection (in seconds)
 */
#define SIMULATED_HTTP_CONNECTION_DELAY									0.3

/**
 * HTTP OK status code
 */
#define OK_STATUS_CODE                                                  200

/**
 * HTTP not found status code
 */
#define NOT_FOUND_STATUS_CODE                                           404 

#pragma mark -
#pragma mark NXT application configuration

/**
 * Contact us mail
 */
#define CONTACT_US_EMAIL                                                @"bancamovil.peru@bbva.com"

/**
 * Defines the application country
 */
#define NXT_APPLICATION_COUNTRY                                         @"PE"


/**
 * Defines the numeric formats
 */
#define SERVER_NUMBER_GROUPING_SEPARATOR                                @","
#define SERVER_NUMBER_DECIMAL_SEPARATOR                                 @"."

#define OUTPUT_NUMBER_GROUPING_SEPARATOR                                @","
#define OUTPUT_NUMBER_DECIMAL_SEPARATOR                                 @"."
#define OUTPUT_NUMBER_DECIMAL_COUNT                                     2

/**
 * Defines the currency literals as comes from the server
 */
#define CURRENCY_EUR_LITERAL                                            @"EUROS"
#define CURRENCY_JPY_LITERAL                                            @"JPY"
#define CURRENCY_CNY_LITERAL                                            @"YUANES"
#define CURRENCY_GBP_LITERAL                                            @"GBP"
#define CURRENCY_USD_LITERAL                                            @"USD"
#define CURRENCY_KRW_LITERAL                                            @"KRW"
#define CURRENCY_SOLES_LITERAL                                          @"SOLES"
#define CURRENCY_SOLES_LITERAL_2                                          @"PEN"


#define CURRENCY_NUEVOS_SOLES_LITERAL                                   @"NUEVOS SOLES"

#define CURRENCY_DOLARES_LITERAL                                        @"DOLARES"
#define CURRENCY_DOLARES_ACCENT_LITERAL                              @"DÓLARES"
/**
 * Defines the currency symbols go get from the literal
 */
#define CURRENCY_EUR_SYMBOL                                             @"€"
#define CURRENCY_JPY_SYMBOL                                             @"¥"
#define CURRENCY_CNY_SYMBOL                                             @"¥"
#define CURRENCY_GBP_SYMBOL                                             @"£"
#define CURRENCY_USD_SYMBOL                                             @"$"
#define CURRENCY_KRW_SYMBOL                                             @"₩"
#define CURRENCY_SOLES_SYMBOL                                           @"S/."
#define CURRENCY_DOLARES_SYMBOL                                         @"US$"

/**
 * Indicates the currency symbol precedes the amount
 */
#define CURRENCY_PRECEDES_AMOUNT										YES

/**
 * BBVA entity number
 */
#define BBVA_CCC_ENTITY_NUMBER                                          @"0011"

/**
 * Session expiration code
 */
#define SESSION_EXPIRATION_CODE                                         @"002"


/**
 * BBVA Line line phone
 */
#define BBVA_LINE_PHONE													@"(01) 595-0000"

#pragma mark -
#pragma mark HTTP connections


//#if defined(DEBUGSERVER)

/**
 * Defines the flag to simulate HTTP connections with local files. To perform real HTTP connections, comment this line
 */
//#define SIMULATE_HTTP_CONNECTION

/**
 * Defines the Movilok environment. Uncomment this line to use Movilok server to obtain information form
 */
//#define MOVILOK_ENVIRONMENT

/**
 * Defines the MDP environment. Uncomment this line to use mdp server to obtain information form. Use in combination with test
 */
//#define MDP_ENVIRONMENT

/**
 * Defines the test environment. Comment this line to use production environment (has only meaning when Movilok environment is not used)
 */
//#define USE_TEST

/**
 * Defines the preproduction environment. Comment this line to use production environment (has only meaning when Movilok environment is not used)
 */
//#define USE_PREPRODUCTION

/**
 * Allow untrusted https certificates (comment it out for production)
 */
//#define ALLOW_UNTRUSTED_HTTPS_CERTIFICATES

//#endif //DEBUGSERVER


/**
 * Defines the target server
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TARGET_SERVER												@""
#elif defined(MOVILOK_ENVIRONMENT)
#define TARGET_SERVER												@"http://movilok.net"
#elif defined(MDP_ENVIRONMENT)
#define TARGET_SERVER												@"http://192.168.164.1:9080"
#elif defined(USE_TEST)
#define TARGET_SERVER												@"https://200.107.164.147:743"
#elif defined(USE_PREPRODUCTION)
#define TARGET_SERVER												@"https://200.48.202.108:743" 
#else
#define TARGET_SERVER												@"https://bancaporinternet.bbvacontinental.pe/"
#endif

/**
 * Defines the target root path
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TARGET_ROOT_PATH											@""
#elif defined(MOVILOK_ENVIRONMENT)
#define TARGET_ROOT_PATH											@"nxt"
#elif defined(MDP_ENVIRONMENT)
#define TARGET_ROOT_PATH											@"BancaMovilBBVA"
#elif defined(USE_TEST)
#define TARGET_ROOT_PATH											@"bdmv_pe_web/bdmv_pe_web" //@"BancaMovilBBVA"//
#elif defined(USE_PREPRODUCTION)
#define TARGET_ROOT_PATH											@"bdmv_pe_web/bdmv_pe_web"
#else
#define TARGET_ROOT_PATH											@"bdmv_pe_web/bdmv_pe_web"
#endif


/**
 * Defines the BDPN_TARGET_PATH
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define BDPN_TARGET_PATH											@"/bdpn_pe_web/bdpn_pe_web"
#elif defined(MOVILOK_ENVIRONMENT)
#define BDPN_TARGET_PATH											@"/bdpn_pe_web/bdpn_pe_web"
#elif defined(MDP_ENVIRONMENT)
#define BDPN_TARGET_PATH											@"/bdpn_pe_web/bdpn_pe_web"
#elif defined(USE_TEST)
#define BDPN_TARGET_PATH											@"/bdpn_pe_web/bdpn_pe_web"
#elif defined(USE_PREPRODUCTION)
#define BDPN_TARGET_PATH											@"/bdpn_pe_web/bdpn_pe_web"
#else
#define BDPN_TARGET_PATH											@"/bdpn_pe_web/bdpn_pe_web"
#endif


/**
 * Defines the CHANGE_REGISTER
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define CHANGE_REGISTER											@"/LogonAnonimoServlet?proceso=operaciones_generales_pr&operacion=bm_inscripcion_op&accion=continuar&tipoAccion=olvido"
#elif defined(MOVILOK_ENVIRONMENT)
#define CHANGE_REGISTER											@"/LogonAnonimoServlet?proceso=operaciones_generales_pr&operacion=bm_inscripcion_op&accion=continuar&tipoAccion=olvido"
#elif defined(MDP_ENVIRONMENT)
#define CHANGE_REGISTER											@"/LogonAnonimoServlet?proceso=operaciones_generales_pr&operacion=bm_inscripcion_op&accion=continuar&tipoAccion=olvido"
#elif defined(USE_TEST)
#define CHANGE_REGISTER											@"/LogonAnonimoServlet?proceso=operaciones_generales_pr&operacion=bm_inscripcion_op&accion=continuar&tipoAccion=olvido"
#elif defined(USE_PREPRODUCTION)
#define CHANGE_REGISTER											@"/LogonAnonimoServlet?proceso=operaciones_generales_pr&operacion=bm_inscripcion_op&accion=continuar&tipoAccion=olvido"
#else
#define CHANGE_REGISTER											@"/LogonAnonimoServlet?proceso=operaciones_generales_pr&operacion=bm_inscripcion_op&accion=continuar&tipoAccion=olvido"
#endif

/**
 * Defines the REGISTRATION_REGISTER
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define REGISTRATION_REGISTER											@"/LogonAnonimoServlet?proceso=operaciones_generales_pr&operacion=bm_inscripcion_op&accion=continuar&tipoAccion=alta"
#elif defined(MOVILOK_ENVIRONMENT)
#define REGISTRATION_REGISTER											@"/LogonAnonimoServlet?proceso=operaciones_generales_pr&operacion=bm_inscripcion_op&accion=continuar&tipoAccion=alta"
#elif defined(MDP_ENVIRONMENT)
#define REGISTRATION_REGISTER											@"/LogonAnonimoServlet?proceso=operaciones_generales_pr&operacion=bm_inscripcion_op&accion=continuar&tipoAccion=alta"
#elif defined(USE_TEST)
#define REGISTRATION_REGISTER											@"/LogonAnonimoServlet?proceso=operaciones_generales_pr&operacion=bm_inscripcion_op&accion=continuar&tipoAccion=alta"
#elif defined(USE_PREPRODUCTION)
#define REGISTRATION_REGISTER											@"/LogonAnonimoServlet?proceso=operaciones_generales_pr&operacion=bm_inscripcion_op&accion=continuar&tipoAccion=alta"
#else
#define REGISTRATION_REGISTER											@"/LogonAnonimoServlet?proceso=operaciones_generales_pr&operacion=bm_inscripcion_op&accion=continuar&tipoAccion=alta"
#endif


/**
 * Defines the VERIFY_OPERATION
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define VERIFY_OPERATION											@"/OperacionCBTFServlet?proceso=operaciones_generales_pr&operacion=bm_confirmacion_op&accion=continuar"
#elif defined(MOVILOK_ENVIRONMENT)
#define VERIFY_OPERATION											@"/OperacionCBTFServlet?proceso=operaciones_generales_pr&operacion=bm_confirmacion_op&accion=continuar"
#elif defined(MDP_ENVIRONMENT)
#define VERIFY_OPERATION											@"/OperacionCBTFServlet?proceso=operaciones_generales_pr&operacion=bm_confirmacion_op&accion=continuar"
#elif defined(USE_TEST)
#define VERIFY_OPERATION											@"/OperacionCBTFServlet?proceso=operaciones_generales_pr&operacion=bm_confirmacion_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define VERIFY_OPERATION											@"/OperacionCBTFServlet?proceso=operaciones_generales_pr&operacion=bm_confirmacion_op&accion=continuar"
#else
#define VERIFY_OPERATION											@"/OperacionCBTFServlet?proceso=operaciones_generales_pr&operacion=bm_confirmacion_op&accion=continuar"
#endif

//&operacion=bm_realizar_op&accion=realizar&email=%@&verified=N&PasswordSistema=%@

/**
 * Defines the DO_REGISTRATION_CHANGE
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define DO_REGISTRATION_CHANGE											@"/OperacionCBTFServlet?proceso=operaciones_generales_pr&operacion=bm_realizar_op&accion=realizar"
#elif defined(MOVILOK_ENVIRONMENT)
#define DO_REGISTRATION_CHANGE											@"/OperacionCBTFServlet?proceso=operaciones_generales_pr&operacion=bm_realizar_op&accion=realizar"
#elif defined(MDP_ENVIRONMENT)
#define DO_REGISTRATION_CHANGE											@"/OperacionCBTFServlet?proceso=operaciones_generales_pr&operacion=bm_realizar_op&accion=realizar"
#elif defined(USE_TEST)
#define DO_REGISTRATION_CHANGE											@"/OperacionCBTFServlet?proceso=operaciones_generales_pr&operacion=bm_realizar_op&accion=realizar"
#elif defined(USE_PREPRODUCTION)
#define DO_REGISTRATION_CHANGE											@"/OperacionCBTFServlet?proceso=operaciones_generales_pr&operacion=bm_realizar_op&accion=realizar"
#else
#define DO_REGISTRATION_CHANGE											@"/OperacionCBTFServlet?proceso=operaciones_generales_pr&operacion=bm_realizar_op&accion=realizar"
#endif


/**
 * Define the login operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define LOGIN_OPERATION												@"Acceso.xml"
//#define LOGIN_OPERATION												@"XMLAccesoSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define LOGIN_OPERATION												@"XMLAccesoSalida.xml"
#elif defined(MDP_ENVIRONMENT)
#define LOGIN_OPERATION												@"/BancaMovilBBVA/LogonOperacionServlet?proceso=operaciones_generales_pr&operacion=inicio_op&accion=inicio&username=%@&password=%@&indicador=2"//@"/BancaMovilBBVA/LogonBancaMovilServlet?proceso=operaciones_generales_pr&operacion=inicio_op&accion=inicio&username=%@&password=%@&indicador=1"
#elif defined(USE_TEST)
#define LOGIN_OPERATION												@"/slod_pe_web/slod/DFServlet" //@"/BancaMovilBBVA/LogonOperacionServlet?proceso=operaciones_generales_pr&operacion=inicio_op&accion=inicio&username=%@&password=%@&indicador=2"//
#elif defined(USE_PREPRODUCTION)
#define LOGIN_OPERATION												@"/slod_pe_web/slod/DFServlet"
#else
#define LOGIN_OPERATION												@"/slod_pe_web/slod/DFServlet"
#endif

#define LOGIN_OPERATION_ENCRYPT										@"/slod_pe_web/NNigmaServlet/nNigmaServlet"

/**
 * Define the login coordinate operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define LOGIN_COORDINATE_OPERATION									@"ValidacionCoordenadas.xml"
//#define LOGIN_COORDINATE_OPERATION									@"XMLAccesoCoordinateSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define LOGIN_COORDINATE_OPERATION									@"XMLAccesoCoordinateSalida.xml"
#elif defined(USE_TEST)
#define LOGIN_COORDINATE_OPERATION									@"/slod_pe_web/slod/DFServlet"
#elif defined(USE_PREPRODUCTION)
#define LOGIN_COORDINATE_OPERATION									@"/slod_pe_web/slod/DFServlet"
#else
#define LOGIN_COORDINATE_OPERATION									@"/slod_pe_web/slod/DFServlet"
#endif

/**
 * Define the Frequenty Operation REactive
 */


#pragma ONE STEP FREQUENT OPERATION

#define FOR_CASH_MOBILE_STEP_ONE @"OperacionCBTFServlet?proceso=efectivo_pr&operacion=inscripcionEM_op&accion=inscribir"

#define FOR_OTHER_BANK_STEP_ONE @"OperacionCBTFServlet?proceso=pfPago_frecuenteOB_pr&operacion=pfInscripcionOB_op&accion=inscribir"

#define FOR_THIRD_ACCOUNT_STEP_ONE @"OperacionCBTFServlet?proceso=pfPago_frecuenteTP_pr&operacion=pfInscripcionTP_op&accion=inscribir"

#define FOR_GIFT_CARD_STEP_ONE @"OperacionCBTFServlet?proceso=pfPago_frecuenteTR_pr&operacion=pfInscripcionTR_op&accion=inscribir"

#define FOR_THIRD_CARD_PAYMENT_STEP_ONE @"OperacionCBTFServlet?proceso=pfPago_frecuentePT_pr&operacion=pfInscripcionPT_op&accion=inscribir"

#define FOR_OTHER_BANK_PAYMENT_STEP_ONE @"OperacionCBTFServlet?proceso=pfPago_frecuenteTOB_pr&operacion=pfInscripcionTOB_op&accion=inscribir"

#define FOR_INSTITUTION_COMPANIES_STEP_ONE @"OperacionCBTFServlet?proceso=pfPago_frecuenteRC_pr&operacion=pfInscripcionRC_op&accion=inscribir"

#define FOR_RECHARGE_CLARO_STEP_ONE @"OperacionCBTFServlet?proceso=pfPago_frecuenteTIM_pr&operacion=pfInscripcionTIM_op&accion=inscribir"

#define FOR_RECHARGE_MOVISTAR_STEP_ONE @"OperacionCBTFServlet?proceso=pfPago_frecuentePSTelefonia_pr&operacion=pfInscripcionPSTelefonia_op&accion=inscribir"

#define FOR_SERVICE_CLARO_STEP_ONE @"OperacionCBTFServlet?proceso=pfPago_frecuenteTIM_pr&operacion=pfInscripcionTIM_op&accion=inscribir"

#define FOR_SERVICE_MOVISTAR_STEP_ONE @"OperacionCBTFServlet?proceso=pfPago_frecuentePSTelefonia_pr&operacion=pfInscripcionPSTelefonia_op&accion=inscribir"

#define FOR_SERVICE_WATER_STEP_ONE @"OperacionCBTFServlet?proceso=pfPago_frecuentePSAgua_pr&operacion=pfInscripcionPSAgua_op&accion=inscribir"



#pragma TWO STEP FREQUENT OPERATION

#define FOR_GIFT_CARD_STEP_TWO @"OperacionCBTFServlet?proceso=pfPago_frecuenteTR_pr&operacion=of_valida_TR_op&accion=validar"

#define FOR_OTHER_BANK_PAYMENT_STEP_TWO @"OperacionCBTFServlet?proceso=pfPago_frecuenteTOB_pr&operacion=of_valida_TOB_op&accion=validar"

#define FOR_THIRD_CARD_PAYMENT_STEP_TWO @"OperacionCBTFServlet?proceso=pfPago_frecuentePT_pr&operacion=of_valida_PT_op&accion=validar"

#define FOR_OTHER_BANK_STEP_TWO @"OperacionCBTFServlet?proceso=pfPago_frecuenteOB_pr&operacion=of_valida_OB_op&accion=validar"

#define FOR_THIRD_ACCOUNT_STEP_TWO @"OperacionCBTFServlet?proceso=pfPago_frecuenteTP_pr&operacion=of_valida_TP_op&accion=validar"

#define FOR_CASH_MOBILE_STEP_TWO @"OperacionCBTFServlet?proceso=efectivo_pr&operacion=valida_alias_EM_op&accion=validar"

#define FOR_INSTITUTION_COMPANIES_STEP_TWO @"OperacionCBTFServlet?proceso=pfPago_frecuenteRC_pr&operacion=of_valida_RC_op&accion=validar"

#define FOR_RECHARGE_CLARO_STEP_TWO @"OperacionCBTFServlet?proceso=pfPago_frecuenteTIM_pr&operacion=of_valida_TIM_op&accion=validar"

#define FOR_RECHARGE_MOVISTAR_STEP_TWO @"OperacionCBTFServlet?proceso=pfPago_frecuentePSTelefonia_pr&operacion=of_valida_PSTelefonia_op&accion=validar"

#define FOR_SERVICE_CLARO_STEP_TWO @"OperacionCBTFServlet?proceso=pfPago_frecuenteTIM_pr&operacion=of_valida_TIM_op&accion=validar"

#define FOR_SERVICE_MOVISTAR_STEP_TWO @"OperacionCBTFServlet?proceso=pfPago_frecuentePSTelefonia_pr&operacion=of_valida_PSTelefonia_op&accion=validar"

#define FOR_SERVICE_WATER_STEP_TWO @"OperacionCBTFServlet?proceso=pfPago_frecuentePSAgua_pr&operacion=of_valida_PSAgua_op&accion=validar"


#pragma THREE STEP FREQUENT OPERATION

#define FOR_GIFT_CARD_STEP_THREE @"OperacionCBTFServlet?proceso=pfPago_frecuenteTR_pr&operacion=pfRealizarTR_op&accion=realizar"

#define FOR_OTHER_BANK_PAYMENT_STEP_THREE @"OperacionCBTFServlet?proceso=pfPago_frecuenteTOB_pr&operacion=pfRealizarTOB_op&accion=realizar"

#define FOR_THIRD_CARD_PAYMENT_STEP_THREE @"OperacionCBTFServlet?proceso=pfPago_frecuentePT_pr&operacion=pfRealizarPT_op&accion=realizar"

#define FOR_OTHER_BANK_STEP_THREE @"OperacionCBTFServlet?proceso=pfPago_frecuenteOB_pr&operacion=pfRealizarOB_op&accion=realizar"

#define FOR_THIRD_ACCOUNT_STEP_THREE @"OperacionCBTFServlet?proceso=pfPago_frecuenteTP_pr&operacion=pfRealizarTP_op&accion=realizar"

#define FOR_CASH_MOBILE_STEP_THREE @"OperacionCBTFServlet?proceso=efectivo_pr&operacion=realizarEM_op&accion=realiza"

#define FOR_INSTITUTION_COMPANIES_STEP_THREE @"OperacionCBTFServlet?proceso=pfPago_frecuenteRC_pr&operacion=pfRealizarRC_op&accion=realizar"

#define FOR_RECHARGE_CLARO_STEP_THREE @"OperacionCBTFServlet?proceso=pfPago_frecuenteTIM_pr&operacion=pfRealizarTIM_op&accion=realizar"

#define FOR_RECHARGE_MOVISTAR_STEP_THREE @"OperacionCBTFServlet?proceso=pfPago_frecuentePSTelefonia_pr&operacion=pfRealizarPSTelefonia_op&accion=realizar"

#define FOR_SERVICE_CLARO_STEP_THREE @"OperacionCBTFServlet?proceso=pfPago_frecuenteTIM_pr&operacion=pfRealizarTIM_op&accion=realizar"

#define FOR_SERVICE_MOVISTAR_STEP_THREE @"OperacionCBTFServlet?proceso=pfPago_frecuentePSTelefonia_pr&operacion=pfRealizarPSTelefonia_op&accion=realizar"

#define FOR_SERVICE_WATER_STEP_THREE @"OperacionCBTFServlet?proceso=pfPago_frecuentePSAgua_pr&operacion=pfRealizarPSAgua_op&accion=realizar"

/**
 * Define the safetypay status operation
 */
#define SAFETYPAY_STATUS_OPERATION @"OperacionCBTFServlet?proceso=trans_stp_pr&operacion=stp_ingresa_datos_op&accion=continuar"
#define SAFETYPAY_TRANSACTION_INFO_OPERATION @"OperacionCBTFServlet?proceso=trans_stp_pr&operacion=stp_muestra_pago_pendiente_op&accion=continuar"
#define SAFETYPAY_PAYMENT_DETAILS @"OperacionCBTFServlet?proceso=trans_stp_pr&operacion=stp_confirm_pago_op&accion=continuar"
#define SAFETYPAY_CONFIRM_PAYMENT @"OperacionCBTFServlet?proceso=trans_stp_pr&operacion=stp_realiza_operacion_op&accion=continuar"
/**
 * Define the global position operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define GLOBAL_POSITION_OPERATION                                   @"TodasMisCuentas.xml"
//#define GLOBAL_POSITION_OPERATION                                   @"XMLPosicionGlobalSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define GLOBAL_POSITION_OPERATION                                   @"XMLPosicionGlobalSalida.xml"
#elif defined(USE_TEST)
#define GLOBAL_POSITION_OPERATION									@"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=posicion_global_op&accion=menuPosicion"
#elif defined(USE_PREPRODUCTION)
#define GLOBAL_POSITION_OPERATION									@"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=posicion_global_op&accion=menuPosicion"
#else
#define GLOBAL_POSITION_OPERATION									@"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=posicion_global_op&accion=menuPosicion"
#endif

/**
 * Define the account transactions list operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
//#define ACCOUNT_TRANSACTION_LIST_OPERATION                          @"MovimientosCuenta.xml"
#define ACCOUNT_TRANSACTION_LIST_OPERATION                          @"XMLCuentaListadoMovimientosSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define ACCOUNT_TRANSACTION_LIST_OPERATION                          @"XMLCuentaListadoMovimientosSalida.xml"
#elif defined(USE_TEST)
#define ACCOUNT_TRANSACTION_LIST_OPERATION                          @"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=movimientos_ctas_op&accion=movimientos"
#elif defined(USE_PREPRODUCTION)
#define ACCOUNT_TRANSACTION_LIST_OPERATION                          @"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=movimientos_ctas_op&accion=movimientos"
#else
#define ACCOUNT_TRANSACTION_LIST_OPERATION                          @"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=movimientos_ctas_op&accion=movimientos"
#endif

/**
 * Define the account detail operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define ACCOUNT_TRANSACTION_DETAIL_OPERATION                        @"XMLCuentaMovimientoDetalleSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define ACCOUNT_TRANSACTION_DETAIL_OPERATION                        @"XMLCuentaMovimientoDetalleSalida.xml"
#elif defined(USE_TEST)
#define ACCOUNT_TRANSACTION_DETAIL_OPERATION                        @"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=movimientos_ctas_op&accion=detalle"
#elif defined(USE_PREPRODUCTION)
#define ACCOUNT_TRANSACTION_DETAIL_OPERATION                        @"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=movimientos_ctas_op&accion=detalle"
#else
#define ACCOUNT_TRANSACTION_DETAIL_OPERATION                        @"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=movimientos_ctas_op&accion=detalle"
#endif

/**
 * Define the card transactions list operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define CARD_TRANSACTION_LIST_OPERATION                             @"MovimientosTarjeta.xml"
//#define CARD_TRANSACTION_LIST_OPERATION                             @"XMLTarjetaListadoMovimientosSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define CARD_TRANSACTION_LIST_OPERATION                             @"XMLTarjetaListadoMovimientosSalida.xml"
#elif defined(USE_TEST)
#define CARD_TRANSACTION_LIST_OPERATION                             @"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=movimientos_tarjs_op&accion=movimientos"
#elif defined(USE_PREPRODUCTION)
#define CARD_TRANSACTION_LIST_OPERATION                             @"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=movimientos_tarjs_op&accion=movimientos"
#else
#define CARD_TRANSACTION_LIST_OPERATION                             @"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=movimientos_tarjs_op&accion=movimientos"
#endif

#pragma mark - Transfers
/**
 * Define the transfer between accounts startup operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_BETWEEN_ACCOUNTS_STARTUP_OPERATION                 @"XMLEnvioDineroInicioSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_BETWEEN_ACCOUNTS_STARTUP_OPERATION                 @"XMLEnvioDineroInicioSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_BETWEEN_ACCOUNTS_STARTUP_OPERATION                 @"OperacionCBTFServlet?proceso=traspasos_pr&operacion=ctas_beneficiarias_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_BETWEEN_ACCOUNTS_STARTUP_OPERATION                 @"OperacionCBTFServlet?proceso=traspasos_pr&operacion=ctas_beneficiarias_op&accion=continuar"
#else
#define TRANSFER_BETWEEN_ACCOUNTS_STARTUP_OPERATION                 @"OperacionCBTFServlet?proceso=traspasos_pr&operacion=ctas_beneficiarias_op&accion=continuar"
#endif

/**
 * Define the transfer between accounts confirmation operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION_OPERATION            @"XMLEnvioDineroConfirmacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION_OPERATION            @"XMLEnvioDineroConfirmacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION_OPERATION            @"OperacionCBTFServlet?proceso=traspasos_pr&operacion=confirmar_tras_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION_OPERATION            @"OperacionCBTFServlet?proceso=traspasos_pr&operacion=confirmar_tras_op&accion=continuar"
#else
#define TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION_OPERATION            @"OperacionCBTFServlet?proceso=traspasos_pr&operacion=confirmar_tras_op&accion=continuar"
#endif

/**
 * Define the transfer between accounts result operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_BETWEEN_ACCOUNTS_RESULT_OPERATION                  @"XMLEnvioDineroOperacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_BETWEEN_ACCOUNTS_RESULT_OPERATION					@"XMLEnvioDineroOperacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_BETWEEN_ACCOUNTS_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=traspasos_pr&operacion=realizar_tras_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_BETWEEN_ACCOUNTS_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=traspasos_pr&operacion=realizar_tras_op&accion=continuar"
#else
#define TRANSFER_BETWEEN_ACCOUNTS_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=traspasos_pr&operacion=realizar_tras_op&accion=continuar"
#endif

/**
 * Define the transfer to third accounts startup operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_THIRD_ACCOUNTS_STARTUP_OPERATION                @"XMLEnvioDineroTercerosInicioSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_THIRD_ACCOUNTS_STARTUP_OPERATION                @"XMLEnvioDineroTercerosInicioSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_THIRD_ACCOUNTS_STARTUP_OPERATION                @"OperacionCBTFServlet?proceso=tp_trasferir_pr&operacion=tp_cuentas_terceros_op&accion=menuOrdTras"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_THIRD_ACCOUNTS_STARTUP_OPERATION                @"OperacionCBTFServlet?proceso=tp_trasferir_pr&operacion=tp_cuentas_terceros_op&accion=menuOrdTras"
#else
#define TRANSFER_TO_THIRD_ACCOUNTS_STARTUP_OPERATION                @"OperacionCBTFServlet?proceso=tp_trasferir_pr&operacion=tp_cuentas_terceros_op&accion=menuOrdTras"
#endif

/**
 * Define the transfer to third accounts confirmation operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_THIRD_ACCOUNTS_CONFIRMATION_OPERATION           @"XMLEnvioDineroTercerosConfirmacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_THIRD_ACCOUNTS_CONFIRMATION_OPERATION           @"XMLEnvioDineroTercerosConfirmacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_THIRD_ACCOUNTS_CONFIRMATION_OPERATION           @"OperacionCBTFServlet?proceso=traspasos_pr&operacion=confirmar_ter_op&accion=continuar_ter"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_THIRD_ACCOUNTS_CONFIRMATION_OPERATION           @"OperacionCBTFServlet?proceso=traspasos_pr&operacion=confirmar_ter_op&accion=continuar_ter"
#else
#define TRANSFER_TO_THIRD_ACCOUNTS_CONFIRMATION_OPERATION           @"OperacionCBTFServlet?proceso=traspasos_pr&operacion=confirmar_ter_op&accion=continuar_ter"
#endif

/**
 * Define the transfer to third accounts confirmation operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_THIRD_ACCOUNTS_WITH_VALIDATION_CONFIRMATION_OPERATION           @"XMLEnvioDineroTercerosConfirmacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_THIRD_ACCOUNTS_WITH_VALIDATION_CONFIRMATION_OPERATION           @"XMLEnvioDineroTercerosConfirmacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_THIRD_ACCOUNTS_WITH_VALIDATION_CONFIRMATION_OPERATION           @"OperacionCBTFServlet?proceso=tp_trasferir_pr&operacion=tp_confirmar_transferencia_terceros_op&accion=continuar_ter"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_THIRD_ACCOUNTS_WITH_VALIDATION_CONFIRMATION_OPERATION           @"OperacionCBTFServlet?proceso=tp_trasferir_pr&operacion=tp_confirmar_transferencia_terceros_op&accion=continuar_ter"
#else
#define TRANSFER_TO_THIRD_ACCOUNTS_WITH_VALIDATION_CONFIRMATION_OPERATION           @"OperacionCBTFServlet?proceso=tp_trasferir_pr&operacion=tp_confirmar_transferencia_terceros_op&accion=continuar_ter"
#endif
/**
 * Define the transfer to third accounts result operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_THIRD_ACCOUNTS_RESULT_OPERATION                 @"XMLEnvioDineroTercerosOperacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_THIRD_ACCOUNTS_RESULT_OPERATION					@"XMLEnvioDineroTercerosOperacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_THIRD_ACCOUNTS_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=traspasos_pr&operacion=realizar_tras_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_THIRD_ACCOUNTS_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=traspasos_pr&operacion=realizar_tras_op&accion=continuar"
#else
#define TRANSFER_TO_THIRD_ACCOUNTS_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=traspasos_pr&operacion=realizar_tras_op&accion=continuar"
#endif

/**
 * Define the transfer to third accounts result operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_THIRD_ACCOUNTS_WITH_VALIDATION_RESULT_OPERATION                 @"XMLEnvioDineroTercerosOperacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_THIRD_ACCOUNTS_WITH_VALIDATION_RESULT_OPERATION					@"XMLEnvioDineroTercerosOperacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_THIRD_ACCOUNTS_WITH_VALIDATION_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=tp_trasferir_pr&operacion=tp_realizar_trasferencia_terceros_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_THIRD_ACCOUNTS_WITH_VALIDATION_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=tp_trasferir_pr&operacion=tp_realizar_trasferencia_terceros_op&accion=continuar"
#else
#define TRANSFER_TO_THIRD_ACCOUNTS_WITH_VALIDATION_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=tp_trasferir_pr&operacion=tp_realizar_trasferencia_terceros_op&accion=continuar"
#endif

/**
 * Define the transfer to accounts from other banks startup operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_STARTUP_OPERATION			@"XMLEnvioDineroTercerosInicioSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_STARTUP_OPERATION			@"XMLEnvioDineroTercerosInicioSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_STARTUP_OPERATION			@"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_ordenar_transferencias_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_STARTUP_OPERATION			@"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_ordenar_transferencias_op&accion=continuar"
#else
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_STARTUP_OPERATION			@"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_ordenar_transferencias_op&accion=continuar"
#endif

/**
 * Define the transfer to accounts from other banks confirmation operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION_OPERATION		@"XMLEnvioDineroOtrosBancosConfirmacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION_OPERATION		@"XMLEnvioDineroOtrosBancosConfirmacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION_OPERATION		@"OperacionCBTFServlet?proceso=ob_transferencias_cuentas_otros_bancos_pr&operacion=ob_confirme_transferencias_op&accion=aceptar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION_OPERATION     @"OperacionCBTFServlet?proceso=ob_transferencias_cuentas_otros_bancos_pr&operacion=ob_confirme_transferencias_op&accion=aceptar"
#else
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_CONFIRMATION_OPERATION     @"OperacionCBTFServlet?proceso=ob_transferencias_cuentas_otros_bancos_pr&operacion=ob_confirme_transferencias_op&accion=aceptar"
#endif


/**
 * Define the transfer to accounts from other banks confirmation operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_WITH_VALIDATION_CONFIRMATION_OPERATION		@"XMLEnvioDineroOtrosBancosConfirmacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_WITH_VALIDATION_CONFIRMATION_OPERATION		@"XMLEnvioDineroOtrosBancosConfirmacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_WITH_VALIDATION_CONFIRMATION_OPERATION		@"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_consultar_flujo_transferencia_op&accion=consultar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_WITH_VALIDATION_CONFIRMATION_OPERATION     @"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_consultar_flujo_transferencia_op&accion=consultar"
#else
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_WITH_VALIDATION_CONFIRMATION_OPERATION     @"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_consultar_flujo_transferencia_op&accion=consultar"
#endif

/**
 * Define the transfer to accounts from other banks confirmation operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_INMEDIATE_CONFIRMATION_OPERATION		@"XMLEnvioDineroOtrosBancosConfirmacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_INMEDIATE_CONFIRMATION_OPERATION		@"XMLEnvioDineroOtrosBancosConfirmacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_INMEDIATE_CONFIRMATION_OPERATION		@"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_confirmar_transferencias_op&accion=seleccionarInmediata"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_INMEDIATE_CONFIRMATION_OPERATION     @"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_confirmar_transferencias_op&accion=seleccionarInmediata"
#else
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_INMEDIATE_CONFIRMATION_OPERATION     @"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_confirmar_transferencias_op&accion=seleccionarInmediata"
#endif

/**
 * Define the transfer to accounts from other banks confirmation operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_BY_SCHEDULE_CONFIRMATION_OPERATION		@"XMLEnvioDineroOtrosBancosConfirmacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_BY_SCHEDULE_CONFIRMATION_OPERATION		@"XMLEnvioDineroOtrosBancosConfirmacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_BY_SCHEDULE_CONFIRMATION_OPERATION		@"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_confirmar_transferencias_op&accion=seleccionarPorHorarios"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_BY_SCHEDULE_CONFIRMATION_OPERATION     @"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_confirmar_transferencias_op&accion=seleccionarPorHorarios"
#else
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_BY_SCHEDULE_CONFIRMATION_OPERATION     @"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_confirmar_transferencias_op&accion=seleccionarPorHorarios"
#endif


/**
 * Define the transfer to accounts from other banks result operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_RESULT_OPERATION			@"XMLEnvioDineroOtrosBancosOperacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_RESULT_OPERATION			@"XMLEnvioDineroTercerosOperacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_RESULT_OPERATION			@"OperacionCBTFServlet?proceso=ob_transferencias_cuentas_otros_bancos_pr&operacion=ob_realiza_transaccion_op&accion=aceptar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_RESULT_OPERATION			@"OperacionCBTFServlet?proceso=ob_transferencias_cuentas_otros_bancos_pr&operacion=ob_realiza_transaccion_op&accion=aceptar"
#else
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_RESULT_OPERATION			@"OperacionCBTFServlet?proceso=ob_transferencias_cuentas_otros_bancos_pr&operacion=ob_realiza_transaccion_op&accion=aceptar"
#endif

/**
 * Define the transfer to accounts from other banks result operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_WITH_VALIDATION_RESULT_OPERATION			@"XMLEnvioDineroOtrosBancosOperacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_WITH_VALIDATION_RESULT_OPERATION			@"XMLEnvioDineroTercerosOperacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_WITH_VALIDATION_RESULT_OPERATION			@"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_realizar_transaccion_op&accion=aceptar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_WITH_VALIDATION_RESULT_OPERATION			@"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_realizar_transaccion_op&accion=aceptar"
#else
#define TRANSFER_TO_ACCOUNT_FROM_OTHER_BANKS_WITH_VALIDATION_RESULT_OPERATION			@"OperacionCBTFServlet?proceso=ob3_transferir_a_otros_bancos_pr&operacion=ob3_realizar_transaccion_op&accion=aceptar"
#endif

/**
 * Define the transfer to accounts with cash mobile startup operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_STARTUP_OPERATION			@"XMLEnvioDineroTercerosInicioSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_STARTUP_OPERATION			@"XMLEnvioDineroTercerosInicioSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_STARTUP_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=ctas_efectivo_op&accion=menuOrdEnvio"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_STARTUP_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=ctas_efectivo_op&accion=menuOrdEnvio"
#else
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_STARTUP_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=ctas_efectivo_op&accion=menuOrdEnvio"
#endif

/**
 * Define the transfer to accounts with cash mobile result operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_RESULT_OPERATION			@"XMLEnvioDineroOtrosBancosOperacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_RESULT_OPERATION			@"XMLEnvioDineroTercerosOperacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_RESULT_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=realizar_efectivo_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_RESULT_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=realizar_efectivo_op&accion=continuar"
#else
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_RESULT_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=realizar_efectivo_op&accion=continuar"
#endif

/**
 * Define the transfer to accounts with cash mobile confirmation operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_CONFIRMATION_OPERATION		@"XMLEnvioDineroOtrosBancosConfirmacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_CONFIRMATION_OPERATION		@"XMLEnvioDineroOtrosBancosConfirmacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_CONFIRMATION_OPERATION		@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=confirmar_efectivo_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_CONFIRMATION_OPERATION     @"OperacionCBTFServlet?proceso=efectivo_pr&operacion=confirmar_efectivo_op&accion=continuar"
#else
#define TRANSFER_TO_ACCOUNT_WITH_CASH_MOBILE_CONFIRMATION_OPERATION     @"OperacionCBTFServlet?proceso=efectivo_pr&operacion=confirmar_efectivo_op&accion=continuar"
#endif

/**
 * Define the obtain cash mobile transactions startup operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define OBTAIN_CASH_MOBILE_DETAIL_STARTUP_OPERATION			@"XMLEnvioDineroTercerosInicioSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define OBTAIN_CASH_MOBILE_DETAIL_STARTUP_OPERATION			@"XMLEnvioDineroTercerosInicioSalida.xml"
#elif defined(USE_TEST)
#define OBTAIN_CASH_MOBILE_DETAIL_STARTUP_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=consulta_efectivo_op&accion=consultar"
#elif defined(USE_PREPRODUCTION)
#define OBTAIN_CASH_MOBILE_DETAIL_STARTUP_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=consulta_efectivo_op&accion=consultar"
#else
#define OBTAIN_CASH_MOBILE_DETAIL_STARTUP_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=consulta_efectivo_op&accion=consultar"
#endif

/**
 * Define the obtain cash mobile transactions detail
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define OBTAIN_CASH_MOBILE_SHOW_DETAIL_OPERATION			@"XMLEnvioDineroTercerosInicioSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define OBTAIN_CASH_MOBILE_SHOW_DETAIL_OPERATION			@"XMLEnvioDineroTercerosInicioSalida.xml"
#elif defined(USE_TEST)
#define OBTAIN_CASH_MOBILE_SHOW_DETAIL_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=detalle_efectivo_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define OBTAIN_CASH_MOBILE_SHOW_DETAIL_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=detalle_efectivo_op&accion=continuar"
#else
#define OBTAIN_CASH_MOBILE_SHOW_DETAIL_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=detalle_efectivo_op&accion=continuar"
#endif

/**
 * Define the obtain cash mobile resend transactions
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define OBTAIN_CASH_MOBILE_RESEND_OPERATION			@"XMLEnvioDineroTercerosInicioSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define OBTAIN_CASH_MOBILE_RESEND_OPERATION			@"XMLEnvioDineroTercerosInicioSalida.xml"
#elif defined(USE_TEST)
#define OBTAIN_CASH_MOBILE_RESEND_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=reenvio_efectivo_op&accion=reenviar"
#elif defined(USE_PREPRODUCTION)
#define OBTAIN_CASH_MOBILE_RESEND_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=reenvio_efectivo_op&accion=reenviar"
#else
#define OBTAIN_CASH_MOBILE_RESEND_OPERATION			@"OperacionCBTFServlet?proceso=efectivo_pr&operacion=reenvio_efectivo_op&accion=reenviar"
#endif


/**
 * Define the transfer to gift card startup operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_GIFT_CARD_STARTUP_OPERATION                 @"XMLEnvioDineroInicioSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_GIFT_CARD_STARTUP_OPERATION                 @"XMLEnvioDineroInicioSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_GIFT_CARD_STARTUP_OPERATION                 @"OperacionCBTFServlet?proceso=tar_regalo_pr&operacion=tar_regalo_op&accion=ingreso"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_GIFT_CARD_STARTUP_OPERATION                 @"OperacionCBTFServlet?proceso=tar_regalo_pr&operacion=tar_regalo_op&accion=ingreso"
#else
#define TRANSFER_GIFT_CARD_STARTUP_OPERATION                 @"OperacionCBTFServlet?proceso=tar_regalo_pr&operacion=tar_regalo_op&accion=ingreso"
#endif

/**
 * Define the transfer to to gift card result operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_GIFT_CARD_CONFIRMATION_OPERATION			@"XMLEnvioDineroOtrosBancosOperacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_GIFT_CARD_CONFIRMATION_OPERATION			@"XMLEnvioDineroTercerosOperacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_GIFT_CARD_CONFIRMATION_OPERATION			@"OperacionCBTFServlet?proceso=tar_regalo_pr&operacion=tar_validacion_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_GIFT_CARD_CONFIRMATION_OPERATION			@"OperacionCBTFServlet?proceso=tar_regalo_pr&operacion=tar_validacion_op&accion=continuar"
#else
#define TRANSFER_GIFT_CARD_CONFIRMATION_OPERATION			@"OperacionCBTFServlet?proceso=tar_regalo_pr&operacion=tar_validacion_op&accion=continuar"
#endif

/**
 * Define the transfer to gift card result operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_GIFT_CARD_RESULT_OPERATION                 @"XMLEnvioDineroTercerosOperacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_GIFT_CARD_RESULT_OPERATION					@"XMLEnvioDineroTercerosOperacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_GIFT_CARD_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=tar_regalo_pr&operacion=tar_resultado_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_GIFT_CARD_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=tar_regalo_pr&operacion=tar_resultado_op&accion=continuar"
#else
#define TRANSFER_GIFT_CARD_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=tar_regalo_pr&operacion=tar_resultado_op&accion=continuar"
#endif

/**
 * Define the transfer between accounts confirmation operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION_OPERATION            @"XMLEnvioDineroConfirmacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION_OPERATION            @"XMLEnvioDineroConfirmacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION_OPERATION            @"OperacionCBTFServlet?proceso=traspasos_pr&operacion=confirmar_tras_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION_OPERATION            @"OperacionCBTFServlet?proceso=traspasos_pr&operacion=confirmar_tras_op&accion=continuar"
#else
#define TRANSFER_BETWEEN_ACCOUNTS_CONFIRMATION_OPERATION            @"OperacionCBTFServlet?proceso=traspasos_pr&operacion=confirmar_tras_op&accion=continuar"
#endif

/**
 * Define the transfer between accounts result operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define TRANSFER_BETWEEN_ACCOUNTS_RESULT_OPERATION                  @"XMLEnvioDineroOperacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define TRANSFER_BETWEEN_ACCOUNTS_RESULT_OPERATION					@"XMLEnvioDineroOperacionSalida.xml"
#elif defined(USE_TEST)
#define TRANSFER_BETWEEN_ACCOUNTS_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=traspasos_pr&operacion=realizar_tras_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define TRANSFER_BETWEEN_ACCOUNTS_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=traspasos_pr&operacion=realizar_tras_op&accion=continuar"
#else
#define TRANSFER_BETWEEN_ACCOUNTS_RESULT_OPERATION					@"OperacionCBTFServlet?proceso=traspasos_pr&operacion=realizar_tras_op&accion=continuar"
#endif

/**
 * Define the transfer result operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define RETRIEVE_RETAINS_OPERATION									@"XMLRecuperarRetencionesSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define RETRIEVE_RETAINS_OPERATION									@"XMLRecuperarRetencionesSalida.xml"
#elif defined(USE_TEST)
#define RETRIEVE_RETAINS_OPERATION									@"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=movimientos_ctas_op&accion=detalleret"
#elif defined(USE_PREPRODUCTION)
#define RETRIEVE_RETAINS_OPERATION									@"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=movimientos_ctas_op&accion=detalleret"
#else
#define RETRIEVE_RETAINS_OPERATION									@"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=movimientos_ctas_op&accion=detalleret"
#endif

/**
 * Defines the send transaction operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define SEND_TRANSACTION_OPERATION                                  @"XMLEnvioMovimientoSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define SEND_TRANSACTION_OPERATION                                  @"XMLEnvioMovimientoSalida.xml"
#elif defined(USE_TEST)
#define SEND_TRANSACTION_OPERATION                                  @"OperacionCBTFServlet?proceso=cf_confirmacion_oper_pr&operacion=cf_rea_conf_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define SEND_TRANSACTION_OPERATION                                  @"OperacionCBTFServlet?proceso=cf_confirmacion_oper_pr&operacion=cf_rea_conf_op&accion=continuar"
#else
#define SEND_TRANSACTION_OPERATION                                  @"OperacionCBTFServlet?proceso=cf_confirmacion_oper_pr&operacion=cf_rea_conf_op&accion=continuar"
#endif

/**
 * Defines the send transaction operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define SEND_TRANSACTION_CONFIRMATION_OPERATION                     @"XMLEnvioMovimientoConfirmacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define SEND_TRANSACTION_CONFIRMATION_OPERATION                     @"XMLEnvioMovimientoConfirmacionSalida.xml"
#elif defined(USE_TEST)
#define SEND_TRANSACTION_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=cf_confirmacion_oper_pr&operacion=cf_rea_conf_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define SEND_TRANSACTION_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=cf_confirmacion_oper_pr&operacion=cf_rea_conf_op&accion=continuar"
#else
#define SEND_TRANSACTION_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=cf_confirmacion_oper_pr&operacion=cf_rea_conf_op&accion=continuar"
#endif

/*
 *Defines the payment start operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define   PAYMENT_STARTUP_OPERATION                                 @".xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define   PAYMENT_STARTUP_OPERATION                                 @"OperacionCBTFServlet?proceso=ps2_pagoservicios_pr&operacion=ps2_lista_op&accion=inicial"
#elif defined(USE_TEST)
#define   PAYMENT_STARTUP_OPERATION                                 @"OperacionCBTFServlet?proceso=ps2_pagoservicios_pr&operacion=ps2_lista_op&accion=inicial"
#elif defined(USE_PREPRODUCTION)
#define   PAYMENT_STARTUP_OPERATION                                 @"OperacionCBTFServlet?proceso=ps2_pagoservicios_pr&operacion=ps2_lista_op&accion=inicial"
#else
#define   PAYMENT_STARTUP_OPERATION                                 @"OperacionCBTFServlet?proceso=ps2_pagoservicios_pr&operacion=ps2_lista_op&accion=inicial"
#endif

/**
 *Defines the payment list operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define   PAYMENT_ELECTRIC_LDS_LIST_OPERATION                        @".xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define   PAYMENT_ELECTRIC_LDS_LIST_OPERATION                        @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_lista_op&accion=continuarLuzdelsur"
#elif defined(USE_TEST)
#define   PAYMENT_ELECTRIC_LDS_LIST_OPERATION                        @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_lista_op&accion=continuarLuzdelsur"
#elif defined(USE_PREPRODUCTION)
#define   PAYMENT_ELECTRIC_LDS_LIST_OPERATION                        @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_lista_op&accion=continuarLuzdelsur"
#else
#define   PAYMENT_ELECTRIC_LDS_LIST_OPERATION                        @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_lista_op&accion=continuarLuzdelsur"
#endif

/**
 *Defines the payment values operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define   PAYMENT_ELECTRIC_LDS_VALUES_OPERATION                      @".xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define   PAYMENT_ELECTRIC_LDS_VALUES_OPERATION                      @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_datos_op&accion=continuarLuzdelsur"
#elif defined(USE_TEST)
#define   PAYMENT_ELECTRIC_LDS_VALUES_OPERATION                      @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_datos_op&accion=continuarLuzdelsur"
#elif defined(USE_PREPRODUCTION)
#define   PAYMENT_ELECTRIC_LDS_VALUES_OPERATION                      @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_datos_op&accion=continuarLuzdelsur"
#else
#define   PAYMENT_ELECTRIC_LDS_VALUES_OPERATION                      @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_datos_op&accion=continuarLuzdelsur"
#endif

/**
 * Defines the payment confirmation operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define   PAYMENT_ELECTRIC_CONFIRMATION_OPERATION                   @".xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define   PAYMENT_ELECTRIC_CONFIRMATION_OPERATION                   @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_confirmacion_op&accion=continuarLuzdelsur"
#elif defined(USE_TEST)
#define   PAYMENT_ELECTRIC_CONFIRMATION_OPERATION                   @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_confirmacion_op&accion=continuarLuzdelsur"
#elif defined(USE_PREPRODUCTION)
#define   PAYMENT_ELECTRIC_CONFIRMATION_OPERATION                   @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_confirmacion_op&accion=continuarLuzdelsur"
#else
#define   PAYMENT_ELECTRIC_CONFIRMATION_OPERATION                   @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_confirmacion_op&accion=continuarLuzdelsur"
#endif

/**
 *
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define   PAYMENT_ELECTRIC_SUCCESS_OPERATION                        @""
#elif defined(MOVILOK_ENVIRONMENT)
#define   PAYMENT_ELECTRIC_SUCCESS_OPERATION                        @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_exito_op&accion=continuarLuzdelsur"
#elif defined(USE_TEST)
#define   PAYMENT_ELECTRIC_SUCCESS_OPERATION                        @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_exito_op&accion=continuarLuzdelsur"
#elif defined(USE_PREPRODUCTION)
#define   PAYMENT_ELECTRIC_SUCCESS_OPERATION                        @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_exito_op&accion=continuarLuzdelsur"
#else
#define   PAYMENT_ELECTRIC_SUCCESS_OPERATION                        @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_exito_op&accion=continuarLuzdelsur"
#endif

/**
 * Define the payment services list operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_SERVICES_LIST_OPERATION                             @"XMLPagosServiciosPublicos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_SERVICES_LIST_OPERATION                             @"XMLPagosServiciosPublicos.xml"
#elif defined(USE_TEST)
#define PAYMENT_SERVICES_LIST_OPERATION                             @"OperacionCBTFServlet?proceso=ps2_pagoservicios_pr&operacion=ps2_lista_op&accion=inicial"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_SERVICES_LIST_OPERATION                             @"OperacionCBTFServlet?proceso=ps2_pagoservicios_pr&operacion=ps2_lista_op&accion=inicial"
#else
#define PAYMENT_SERVICES_LIST_OPERATION                             @"OperacionCBTFServlet?proceso=ps2_pagoservicios_pr&operacion=ps2_lista_op&accion=inicial"
#endif

/**
 * Define the close operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define CLOSE_OPERATION											    @"pkmslogout.form"
#elif defined(MOVILOK_ENVIRONMENT)
#define CLOSE_OPERATION                                             @"pkmslogout.form"
#elif defined(USE_TEST)
#define CLOSE_OPERATION												@"pkmslogout.form"
#elif defined(USE_PREPRODUCTION)
#define CLOSE_OPERATION												@"pkmslogout.form"
#else
#define CLOSE_OPERATION												@"pkmslogout.form"
#endif

/**
 * Define the global position operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define LOGOUT_OPERATION											@"Desconexion.xml"
//#define LOGOUT_OPERATION											@"XMLLogoutSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define LOGOUT_OPERATION											@"XMLLogoutSalida.xml"
#elif defined(USE_TEST)
#define LOGOUT_OPERATION											@"OperacionCBTFServlet?proceso=desconexion_pr&operacion=desconexion_op&accion=inicioDesconexion"
#elif defined(USE_PREPRODUCTION)
#define LOGOUT_OPERATION											@"OperacionCBTFServlet?proceso=desconexion_pr&operacion=desconexion_op&accion=inicioDesconexion"
#else
#define LOGOUT_OPERATION											@"OperacionCBTFServlet?proceso=desconexion_pr&operacion=desconexion_op&accion=inicioDesconexion"
#endif

/**
 * Define companies
 */
#define COMPANY_LUZ_DEL_SUR                                         @"0001196"
#define COMPANY_EDELNOR                                             @"0001197"
#define COMPANY_ELECTRONORTE                                        @"0000171"
#define COMPANY_ELECTROSUR                                          @"0000328"
#define COMPANY_ELECTROSURESTE                                      @"0001246"
#define COMPANY_HIDRANDINA                                          @"0001001"
#define COMPANY_ELECTROPUNO                                         @"0000065"
#define COMPANY_ELECTROCENTRO                                       @"0000239"
#define COMPANY_ELECTRONORTEOESTE                                   @"0000241"
#define COMPANY_ELECTROSEAL                                         @"0001002"
#define COMPANY_SEDAPAL                                             @"sedapal"
#define COMPANY_SEDAPAR                                             @"0000790"
#define COMPANY_TELEFONICA                                          @"tele"
#define COMPANY_CLARO3PLAY                                          @"0000316"
#define COMPANY_REPSOL												@"0000296"
#define COMPANY_DIRECTTV                                            @"directv"
#define COMPANY_COD_DIRECTV                                         @"0001321"
/**
 * Define companies actions
 */
#define WITHOUT_ACION                                              @""
#define LUZDELSUR_ACTION                                            @"&accion=continuarLuzdelsur"
#define EDELNOR_ACTION                                              @"&accion=continuarEdelnor"
#define ELECTIONOR_ACTION                                           @"&accion=continuarElectroNorte"
#define SEDAPAL_ACTION                                              @"&accion=continuar"
#define SEDAPAR_ACTION                                              @"&accion=continuar"
#define TELEFONICA_ACTION                                           @"&accion=continuarFijo"
#define MOVISTAR_ACTION                                             @"&accion=continuarMoviStar"
#define CLARO_ACTION                                                @"&accion=continuar"

/**
 * Define the payment institutions and companies list operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_INSTITUTIONS_LIST_OPERATION                          @"XMLPagosServiciosPublicos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_INSTITUTIONS_LIST_OPERATION                          @"XMLPagosServiciosPublicos.xml"
#elif defined(USE_TEST)
#define PAYMENT_INSTITUTIONS_LIST_OPERATION                          @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_lista_entidades_op&accion="
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_INSTITUTIONS_LIST_OPERATION                          @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_lista_entidades_op&accion="
#else
#define PAYMENT_INSTITUTIONS_LIST_OPERATION                          @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_lista_entidades_op&accion="
#endif

/**
 * Define the payment institutions and companies detail for code
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_INSTITUTIONS_DETAIL_RESULT                            @"XMLPagosServiciosPublicos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_INSTITUTIONS_DETAIL_RESULT                            @"XMLPagosServiciosPublicos.xml"
#elif defined(USE_TEST)
#define PAYMENT_INSTITUTIONS_DETAIL_RESULT                            @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_solicitud_pago_op&accion=mostrar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_INSTITUTIONS_DETAIL_RESULT                            @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_solicitud_pago_op&accion=mostrar"
#else
#define PAYMENT_INSTITUTIONS_DETAIL_RESULT                            @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_solicitud_pago_op&accion=mostrar"
#endif

/**
 * Detail for the pay of institutions and companies
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_INSTITUTIONS_INFORMATION_TO_PAY_RESULT                            @"XMLPagosServiciosPublicos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_INSTITUTIONS_INFORMATION_TO_PAY_RESULT                            @"XMLPagosServiciosPublicos.xml"
#elif defined(USE_TEST)
#define PAYMENT_INSTITUTIONS_INFORMATION_TO_PAY_RESULT                            @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_pagos_pendientes_op&accion=pagos"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_INSTITUTIONS_INFORMATION_TO_PAY_RESULT                            @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_pagos_pendientes_op&accion=pagos"
#else
#define PAYMENT_INSTITUTIONS_INFORMATION_TO_PAY_RESULT                            @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_pagos_pendientes_op&accion=pagos"
#endif

/**
 * Confirmation of the pay of institutions and companies
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_INSTITUTIONS_CONFIRMATION_OF_PAY_RESULT                            @"XMLPagosServiciosPublicos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_INSTITUTIONS_CONFIRMATION_OF_PAY_RESULT                            @"XMLPagosServiciosPublicos.xml"
#elif defined(USE_TEST)
#define PAYMENT_INSTITUTIONS_CONFIRMATION_OF_PAY_RESULT                            @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_confirma_pago_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_INSTITUTIONS_CONFIRMATION_OF_PAY_RESULT                            @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_confirma_pago_op&accion=continuar"
#else
#define PAYMENT_INSTITUTIONS_CONFIRMATION_OF_PAY_RESULT                            @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_confirma_pago_op&accion=continuar"
#endif

/**
 * Success pay of institutions and companies for coordinate cards
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_INSTITUTIONS_SUCCSESS_OF_PAY_RESULT                            @"XMLPagosServiciosPublicos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_INSTITUTIONS_SUCCSESS_OF_PAY_RESULT                            @"XMLPagosServiciosPublicos.xml"
#elif defined(USE_TEST)
#define PAYMENT_INSTITUTIONS_SUCCSESS_OF_PAY_RESULT                            @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_realizar_pago_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_INSTITUTIONS_SUCCSESS_OF_PAY_RESULT                            @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_realizar_pago_op&accion=continuar"
#else
#define PAYMENT_INSTITUTIONS_SUCCSESS_OF_PAY_RESULT                            @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_realizar_pago_op&accion=continuar"
#endif

/**
 * Defines the initial request for electric service payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_ELECTRIC_SERVICE_INITIAL_OPERATION                  @"XMLPagosSPServiciosElectInicial.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_ELECTRIC_SERVICE_INITIAL_OPERATION                  @"XMLPagosSPServiciosElectInicial.xml"
#elif defined(USE_TEST)
#define PAYMENT_ELECTRIC_SERVICE_INITIAL_OPERATION                  @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_lista_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_ELECTRIC_SERVICE_INITIAL_OPERATION                  @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_lista_op"
#else
#define PAYMENT_ELECTRIC_SERVICE_INITIAL_OPERATION                  @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_lista_op"
#endif

/**
 * Defines the data request for electric service payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_ELECTRIC_SERVICE_DATA_OPERATION                     @"XMLPagosSPServiciosElectDatos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_ELECTRIC_SERVICE_DATA_OPERATION                     @"XMLPagosSPServiciosElectDatos.xml"
#elif defined(USE_TEST) 
#define PAYMENT_ELECTRIC_SERVICE_DATA_OPERATION                     @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_datos_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_ELECTRIC_SERVICE_DATA_OPERATION                     @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_datos_op"
#else
#define PAYMENT_ELECTRIC_SERVICE_DATA_OPERATION                     @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_datos_op"
#endif

/**
 * Defines the confirmation request for electric service payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_ELECTRIC_SERVICE_CONFIRMATION_OPERATION             @"XMLPagosSPServiciosElectConfirmacion.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_ELECTRIC_SERVICE_CONFIRMATION_OPERATION             @"XMLPagosSPServiciosElectConfirmacion.xml"
#elif defined(USE_TEST)
#define PAYMENT_ELECTRIC_SERVICE_CONFIRMATION_OPERATION             @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_confirmacion_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_ELECTRIC_SERVICE_CONFIRMATION_OPERATION             @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_confirmacion_op"
#else
#define PAYMENT_ELECTRIC_SERVICE_CONFIRMATION_OPERATION             @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_confirmacion_op"
#endif

/**
 * Defines the success request for electric service payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_ELECTRIC_SERVICE_SUCCESS_OPERATION                  @"XMLPagosSPServiciosElectExito.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_ELECTRIC_SERVICE_SUCCESS_OPERATION                  @"XMLPagosSPServiciosElectExito.xml"
#elif defined(USE_TEST)
#define PAYMENT_ELECTRIC_SERVICE_SUCCESS_OPERATION                  @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_exito_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_ELECTRIC_SERVICE_SUCCESS_OPERATION                  @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_exito_op"
#else
#define PAYMENT_ELECTRIC_SERVICE_SUCCESS_OPERATION                  @"OperacionCBTFServlet?proceso=ps_luz_pr&operacion=ps_luz_exito_op"
#endif

/**
 * Defines the initial request for water service payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_WATER_SERVICE_INITIAL_OPERATION                     @"XMLPagosSPServiciosAguaInicial.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_WATER_SERVICE_INITIAL_OPERATION                     @"XMLPagosSPServiciosAguaInicial.xml"
#elif defined(USE_TEST)
#define PAYMENT_WATER_SERVICE_INITIAL_OPERATION                     @"OperacionCBTFServlet?proceso=ps_agua_pr&operacion=ps_agua_lista_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_WATER_SERVICE_INITIAL_OPERATION                     @"OperacionCBTFServlet?proceso=ps_agua_pr&operacion=ps_agua_lista_op"
#else
#define PAYMENT_WATER_SERVICE_INITIAL_OPERATION                     @"OperacionCBTFServlet?proceso=ps_agua_pr&operacion=ps_agua_lista_op"
#endif

/**
 * Defines the data request for water service payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_WATER_SERVICE_DATA_OPERATION                        @"XMLPagosSPServiciosAguaDatos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_WATER_SERVICE_DATA_OPERATION                        @"XMLPagosSPServiciosAguaDatos.xml"
#elif defined(USE_TEST) 
#define PAYMENT_WATER_SERVICE_DATA_OPERATION                        @"OperacionCBTFServlet?proceso=ps_agua_pr&operacion=ps_agua_datos_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_WATER_SERVICE_DATA_OPERATION                        @"OperacionCBTFServlet?proceso=ps_agua_pr&operacion=ps_agua_datos_op"
#else
#define PAYMENT_WATER_SERVICE_DATA_OPERATION                        @"OperacionCBTFServlet?proceso=ps_agua_pr&operacion=ps_agua_datos_op"
#endif

/**
 * Defines the confirmation request for water services payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_WATER_SERVICE_CONFIRMATION_OPERATION                @"XMLPagosSPServiciosAguaConfirmacion.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_WATER_SERVICE_CONFIRMATION_OPERATION                @"XMLPagosSPServiciosAguaConfirmacion.xml"
#elif defined(USE_TEST)
#define PAYMENT_WATER_SERVICE_CONFIRMATION_OPERATION                @"OperacionCBTFServlet?proceso=ps_agua_pr&operacion=ps_agua_confirmacion_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_WATER_SERVICE_CONFIRMATION_OPERATION                @"OperacionCBTFServlet?proceso=ps_agua_pr&operacion=ps_agua_confirmacion_op"
#else
#define PAYMENT_WATER_SERVICE_CONFIRMATION_OPERATION                @"OperacionCBTFServlet?proceso=ps_agua_pr&operacion=ps_agua_confirmacion_op"
#endif

/**
 * Defines the success request for water services payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_WATER_SERVICE_SUCCESS_OPERATION                     @"XMLPagosSPServiciosAguaExito.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_WATER_SERVICE_SUCCESS_OPERATION                     @"XMLPagosSPServiciosAguaExito.xml"
#elif defined(USE_TEST)
#define PAYMENT_WATER_SERVICE_SUCCESS_OPERATION                     @"OperacionCBTFServlet?proceso=ps_agua_pr&operacion=ps_agua_exito_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_WATER_SERVICE_SUCCESS_OPERATION                     @"OperacionCBTFServlet?proceso=ps_agua_pr&operacion=ps_agua_exito_op"
#else
#define PAYMENT_WATER_SERVICE_SUCCESS_OPERATION                     @"OperacionCBTFServlet?proceso=ps_agua_pr&operacion=ps_agua_exito_op"
#endif

/**
 * Defines the initial request for phone service payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_PHONE_SERVICE_INITIAL_OPERATION                     @"XMLPagosSPTelefoniaFijaInicial.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_PHONE_SERVICE_INITIAL_OPERATION                     @"XMLPagosSPTelefoniaFijaInicial.xml"
#elif defined(USE_TEST)
#define PAYMENT_PHONE_SERVICE_INITIAL_OPERATION                     @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_lista_op&accion=continuarFijo"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_PHONE_SERVICE_INITIAL_OPERATION                     @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_lista_op&accion=continuarFijo"
#else
#define PAYMENT_PHONE_SERVICE_INITIAL_OPERATION                     @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_lista_op&accion=continuarFijo"
#endif

/**
 * Defines the data request for phone service payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_PHONE_SERVICE_DATA_OPERATION                        @"XMLPagosSPTelefoniaFijaDatos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_PHONE_SERVICE_DATA_OPERATION                        @"XMLPagosSPTelefoniaFijaDatos.xml"
#elif defined(USE_TEST) 
#define PAYMENT_PHONE_SERVICE_DATA_OPERATION                        @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_datos_op&accion=continuarFijo"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_PHONE_SERVICE_DATA_OPERATION                        @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_datos_op&accion=continuarFijo"
#else
#define PAYMENT_PHONE_SERVICE_DATA_OPERATION                        @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_datos_op&accion=continuarFijo"
#endif

/**
 * Defines the confirmation request for phone service payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_PHONE_SERVICE_CONFIRMATION_OPERATION                @"XMLPagosSPTelefoniaFijaConfirmacion.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_PHONE_SERVICE_CONFIRMATION_OPERATION                @"XMLPagosSPTelefoniaFijaConfirmacion.xml"
#elif defined(USE_TEST)
#define PAYMENT_PHONE_SERVICE_CONFIRMATION_OPERATION                @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_confirmacion_op&accion=continuarFijo"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_PHONE_SERVICE_CONFIRMATION_OPERATION                @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_confirmacion_op&accion=continuarFijo"
#else
#define PAYMENT_PHONE_SERVICE_CONFIRMATION_OPERATION                @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_confirmacion_op&accion=continuarFijo"
#endif

/**
 * Defines the success request for phone service payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_PHONE_SERVICE_SUCCESS_OPERATION                     @"XMLPagosSPTelefoniaFijaExito.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_PHONE_SERVICE_SUCCESS_OPERATION                     @"XMLPagosSPTelefoniaFijaExito.xml"
#elif defined(USE_TEST)
#define PAYMENT_PHONE_SERVICE_SUCCESS_OPERATION                     @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_exito_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_PHONE_SERVICE_SUCCESS_OPERATION                     @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_exito_op"
#else
#define PAYMENT_PHONE_SERVICE_SUCCESS_OPERATION                     @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_exito_op"
#endif

/**
 * Defines the initial request for movistar payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_MOVISTAR_INITIAL_OPERATION                          @"XMLPagosSPTelefoniaMovilInicial.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_MOVISTAR_INITIAL_OPERATION                          @"XMLPagosSPTelefoniaMovilInicial.xml"
#elif defined(USE_TEST)
#define PAYMENT_MOVISTAR_INITIAL_OPERATION                          @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_lista_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_MOVISTAR_INITIAL_OPERATION                          @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_lista_op"
#else
#define PAYMENT_MOVISTAR_INITIAL_OPERATION                          @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_lista_op"
#endif

/**
 * Defines the data request for movistar payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_MOVISTAR_DATA_OPERATION                             @"XMLPagosSPTelefoniaMovilDatos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_MOVISTAR_DATA_OPERATION                             @"XMLPagosSPTelefoniaMovilDatos.xml"
#elif defined(USE_TEST) 
#define PAYMENT_MOVISTAR_DATA_OPERATION                             @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_datos_op&accion=continuarMoviStar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_MOVISTAR_DATA_OPERATION                             @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_datos_op&accion=continuarMoviStar"
#else
#define PAYMENT_MOVISTAR_DATA_OPERATION                             @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_datos_op&accion=continuarMoviStar"
#endif

/**
 * Defines the confirmation request for movistar payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_MOVISTAR_CONFIRMATION_OPERATION                     @"XMLPagosSPTelefoniaMovilConfirmacion.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_MOVISTAR_CONFIRMATION_OPERATION                     @"XMLPagosSPTelefoniaMovilConfirmacion.xml"
#elif defined(USE_TEST)
#define PAYMENT_MOVISTAR_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_confirmacion_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_MOVISTAR_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_confirmacion_op"
#else
#define PAYMENT_MOVISTAR_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_confirmacion_op"
#endif

/**
 * Defines the success request for movistar payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_MOVISTAR_SUCCESS_OPERATION                          @"XMLPagosSPTelefoniaMovilExito.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_MOVISTAR_SUCCESS_OPERATION                          @"XMLPagosSPTelefoniaMovilExito.xml"
#elif defined(USE_TEST)
#define PAYMENT_MOVISTAR_SUCCESS_OPERATION                          @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_exito_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_MOVISTAR_SUCCESS_OPERATION                          @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_exito_op"
#else
#define PAYMENT_MOVISTAR_SUCCESS_OPERATION                          @"OperacionCBTFServlet?proceso=ps_telefonia_pagoservicios_pr&operacion=ps_telefonica_exito_op"
#endif

/**
 * Defines the initial request for movistar payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CLARO_INITIAL_OPERATION                             @"XMLPagosSPTelefoniaMovilInicial.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CLARO_INITIAL_OPERATION                             @"XMLPagosSPTelefoniaMovilInicial.xml"
#elif defined(USE_TEST)
#define PAYMENT_CLARO_INITIAL_OPERATION                             @"OperacionCBTFServlet?proceso=ps_claro_pagoservicios_pr&operacion=ps_claro_lista_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CLARO_INITIAL_OPERATION                             @"OperacionCBTFServlet?proceso=ps_claro_pagoservicios_pr&operacion=ps_claro_lista_op"
#else
#define PAYMENT_CLARO_INITIAL_OPERATION                             @"OperacionCBTFServlet?proceso=ps_claro_pagoservicios_pr&operacion=ps_claro_lista_op"
#endif

/**
 * Defines the data request for claro payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CLARO_DATA_OPERATION                                @"XMLPagosSPTelefoniaMovilDatos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CLARO_DATA_OPERATION                                @"XMLPagosSPTelefoniaMovilDatos.xml"
#elif defined(USE_TEST) 
#define PAYMENT_CLARO_DATA_OPERATION                                @"OperacionCBTFServlet?proceso=ps_claro_pagoservicios_pr&operacion=ps_claro_datos_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CLARO_DATA_OPERATION                                @"OperacionCBTFServlet?proceso=ps_claro_pagoservicios_pr&operacion=ps_claro_datos_op"
#else       
#define PAYMENT_CLARO_DATA_OPERATION                                @"OperacionCBTFServlet?proceso=ps_claro_pagoservicios_pr&operacion=ps_claro_datos_op"
#endif

/**
 * Defines the confirmation request for claro payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CLARO_CONFIRMATION_OPERATION                        @"XMLPagosSPTelefoniaMovilConfirmacion.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CLARO_CONFIRMATION_OPERATION                        @"XMLPagosSPTelefoniaMovilConfirmacion.xml"
#elif defined(USE_TEST)
#define PAYMENT_CLARO_CONFIRMATION_OPERATION                        @"OperacionCBTFServlet?proceso=ps_claro_pagoservicios_pr&operacion=ps_claro_confirmacion_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CLARO_CONFIRMATION_OPERATION                        @"OperacionCBTFServlet?proceso=ps_claro_pagoservicios_pr&operacion=ps_claro_confirmacion_op"
#else
#define PAYMENT_CLARO_CONFIRMATION_OPERATION                        @"OperacionCBTFServlet?proceso=ps_claro_pagoservicios_pr&operacion=ps_claro_confirmacion_op"
#endif

/**
 * Defines the success request for claro payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CLARO_SUCCESS_OPERATION                             @"XMLPagosSPTelefoniaMovilExito.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CLARO_SUCCESS_OPERATION                             @"XMLPagosSPTelefoniaMovilExito.xml"
#elif defined(USE_TEST)
#define PAYMENT_CLARO_SUCCESS_OPERATION                             @"OperacionCBTFServlet?proceso=ps_claro_pagoservicios_pr&operacion=ps_claro_exito_op"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CLARO_SUCCESS_OPERATION                             @"OperacionCBTFServlet?proceso=ps_claro_pagoservicios_pr&operacion=ps_claro_exito_op"
#else
#define PAYMENT_CLARO_SUCCESS_OPERATION                             @"OperacionCBTFServlet?proceso=ps_claro_pagoservicios_pr&operacion=ps_claro_exito_op"
#endif

/**
 * Defines the request for recharge payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_RECHARGE_OPERATION									@"XMLRecargaMovilInicioSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_RECHARGE_OPERATION									@"XMLRecargaMovilInicioSalida.xml"
#elif defined(USE_TEST)
#define PAYMENT_RECHARGE_OPERATION									@"OperacionCBTFServlet?proceso=recarga_celular_pr&operacion=rc_datos_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_RECHARGE_OPERATION									@"OperacionCBTFServlet?proceso=recarga_celular_pr&operacion=rc_datos_op&accion=continuar"
#else
#define PAYMENT_RECHARGE_OPERATION									@"OperacionCBTFServlet?proceso=recarga_celular_pr&operacion=rc_datos_op&accion=continuar"
#endif

/**
 * Defines the request for recharge payments confirmation operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_RECHARGE_CONFIRMATION_OPERATION						@"XMLRecargaMovilConfirmacionSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_RECHARGE_CONFIRMATION_OPERATION						@"XMLRecargaMovilConfirmacionSalida.xml"
#elif defined(USE_TEST)
#define PAYMENT_RECHARGE_CONFIRMATION_OPERATION						@"OperacionCBTFServlet?proceso=recarga_celular_pr&operacion=rc_confirmacion_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_RECHARGE_CONFIRMATION_OPERATION						@"OperacionCBTFServlet?proceso=recarga_celular_pr&operacion=rc_confirmacion_op&accion=continuar"
#else
#define PAYMENT_RECHARGE_CONFIRMATION_OPERATION						@"OperacionCBTFServlet?proceso=recarga_celular_pr&operacion=rc_confirmacion_op&accion=continuar"
#endif

/**
 * Defines the request for recharge payments claro exit operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_RECHARGE_CLARO_OPERATION							@"XMLRecargaMovilClaroResultadoSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_RECHARGE_CLARO_OPERATION							@"XMLRecargaMovilClaroResultadoSalida.xml"
#elif defined(USE_TEST)
#define PAYMENT_RECHARGE_CLARO_OPERATION							@"OperacionCBTFServlet?proceso=recarga_celular_pr&operacion=rc_claro_exito_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_RECHARGE_CLARO_OPERATION							@"OperacionCBTFServlet?proceso=recarga_celular_pr&operacion=rc_claro_exito_op&accion=continuar"
#else
#define PAYMENT_RECHARGE_CLARO_OPERATION							@"OperacionCBTFServlet?proceso=recarga_celular_pr&operacion=rc_claro_exito_op&accion=continuar"
#endif

/**
 * Defines the request for recharge payments movistar exit operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_RECHARGE_MOVISTAR_OPERATION							@"XMLRecargaMovilMovistarResultadoSalida.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_RECHARGE_MOVISTAR_OPERATION							@"XMLRecargaMovilMovistarResultadoSalida.xml"
#elif defined(USE_TEST)
#define PAYMENT_RECHARGE_MOVISTAR_OPERATION							@"OperacionCBTFServlet?proceso=recarga_celular_pr&operacion=rc_telefonica_exito_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_RECHARGE_MOVISTAR_OPERATION							@"OperacionCBTFServlet?proceso=recarga_celular_pr&operacion=rc_telefonica_exito_op&accion=continuar"
#else
#define PAYMENT_RECHARGE_MOVISTAR_OPERATION							@"OperacionCBTFServlet?proceso=recarga_celular_pr&operacion=rc_telefonica_exito_op&accion=continuar"
#endif

/**
 * Defines the initial request for gas payments operation
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_GAS_INITIAL_OPERATION                          @"XMLPagosSPTelefoniaMovilInicial.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_GAS_INITIAL_OPERATION                          @"XMLPagosSPTelefoniaMovilInicial.xml"
#elif defined(USE_TEST)
#define PAYMENT_GAS_INITIAL_OPERATION                          @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_solicitud_pago_op&accion=mostrar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_GAS_INITIAL_OPERATION                          @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_solicitud_pago_op&accion=mostrar"
#else
#define PAYMENT_GAS_INITIAL_OPERATION                          @"OperacionCBTFServlet?proceso=rc2_recaudaciones_pr&operacion=rc2_solicitud_pago_op&accion=mostrar"
#endif

/**
 * Defines the continental card payment inicialization
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CARD_CONT_INITIAL_OPERATION                         @"XMLPagosTarjCreditoInicial.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CARD_CONT_INITIAL_OPERATION                         @"XMLPagosTarjCreditoInicial.xml"
#elif defined(USE_TEST)
#define PAYMENT_CARD_CONT_INITIAL_OPERATION                         @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_lista_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CARD_CONT_INITIAL_OPERATION                         @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_lista_op&accion=continuar"
#else
#define PAYMENT_CARD_CONT_INITIAL_OPERATION                         @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_lista_op&accion=continuar"
#endif

/**
 * Defines the data request card payment own cards
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CARD_OWN_DATA_OPERATION                             @"XMLPagosTCPropiaDatos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CARD_OWN_DATA_OPERATION                             @"XMLPagosTCPropiaDatos.xml"
#elif defined(USE_TEST)
#define PAYMENT_CARD_OWN_DATA_OPERATION                             @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_datos_op&accion=datosPago"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CARD_OWN_DATA_OPERATION                             @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_datos_op&accion=datosPago"
#else
#define PAYMENT_CARD_OWN_DATA_OPERATION                             @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_datos_op&accion=datosPago"
#endif

/**
 * Defines the confirmation request card payment own cards
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CARD_OWN_CONFIRMATION_OPERATION                     @"XMLPagosTCPropiaConfirmacion.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CARD_OWN_CONFIRMATION_OPERATION                     @"XMLPagosTCPropiaConfirmacion.xml"
#elif defined(USE_TEST)
#define PAYMENT_CARD_OWN_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_confirmacion_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CARD_OWN_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_confirmacion_op&accion=continuar"
#else
#define PAYMENT_CARD_OWN_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_confirmacion_op&accion=continuar"
#endif


/**
 * Defines the success request card payment own cards
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CARD_OWN_SUCCESS_OPERATION                          @"XMLPagosTCPropiaExito.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CARD_OWN_SUCCESS_OPERATION                          @"XMLPagosTCPropiaExito.xml"
#elif defined(USE_TEST) 
#define PAYMENT_CARD_OWN_SUCCESS_OPERATION                          @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_exito_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CARD_OWN_SUCCESS_OPERATION                          @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_exito_op&accion=continuar"
#else
#define PAYMENT_CARD_OWN_SUCCESS_OPERATION                          @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_exito_op&accion=continuar"
#endif


/**
 * Defines the data request card payment to third cards
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CARD_THIRDS_DATA_OPERATION                          @"XMLPagosTCTercerosDatos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CARD_THIRDS_DATA_OPERATION                          @"XMLPagosTCTercerosDatos.xml"
#elif defined(USE_TEST)
#define PAYMENT_CARD_THIRDS_DATA_OPERATION                          @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_datos_op&accion=continuarTerceros"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CARD_THIRDS_DATA_OPERATION                          @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_datos_op&accion=continuarTerceros"
#else
#define PAYMENT_CARD_THIRDS_DATA_OPERATION                          @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_datos_op&accion=continuarTerceros"
#endif

/**
 * Defines the confirmation request card payment to third cards
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CARD_THIRDS_CONFIRMATION_OPERATION                  @"XMLPagosTCTercerosConfirmacion.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CARD_THIRDS_CONFIRMATION_OPERATION                  @"XMLPagosTCTercerosConfirmacion.xml"
#elif defined(USE_TEST)
#define PAYMENT_CARD_THIRDS_CONFIRMATION_OPERATION                  @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_confirmacion_op&accion=continuarTerceros"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CARD_THIRDS_CONFIRMATION_OPERATION                  @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_confirmacion_op&accion=continuarTerceros"
#else
#define PAYMENT_CARD_THIRDS_CONFIRMATION_OPERATION                  @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_confirmacion_op&accion=continuarTerceros"
#endif


/**
 * Defines the success request card payment to third cards
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CARD_THIRDS_SUCCESS_OPERATION                       @"XMLPagosTCTercerosExito.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CARD_THIRDS_SUCCESS_OPERATION                       @"XMLPagosTCTercerosExito.xml"
#elif defined(USE_TEST) 
#define PAYMENT_CARD_THIRDS_SUCCESS_OPERATION                       @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_exito_op&accion=continuarTerceros"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CARD_THIRDS_SUCCESS_OPERATION                       @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_exito_op&accion=continuarTerceros"
#else
#define PAYMENT_CARD_THIRDS_SUCCESS_OPERATION                       @"OperacionCBTFServlet?proceso=pt_pagocta_tarjs_pr&operacion=pt_tarjetabbva_exito_op&accion=continuarTerceros"
#endif

/**
 * Defines the continental card payment inicialization
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CARD_OTHER_BANK_INITIAL_OPERATION                   @"XMLPagosTCOtrosBancosInicial.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CARD_OTHER_BANK_INITIAL_OPERATION                   @"XMLPagosTCOtrosBancosInicial.xml"
#elif defined(USE_TEST)
#define PAYMENT_CARD_OTHER_BANK_INITIAL_OPERATION                   @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_listar_otros_bancos_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CARD_OTHER_BANK_INITIAL_OPERATION                   @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_listar_otros_bancos_op&accion=continuar"
#else
#define PAYMENT_CARD_OTHER_BANK_INITIAL_OPERATION                   @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_listar_otros_bancos_op&accion=continuar"
#endif


/**
 * Defines the data request card payment own cards
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CARD_OTHER_BANK_DATA_OPERATION                      @"XMLPagosTCOtrosBancosDatos.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CARD_OTHER_BANK_DATA_OPERATION                      @"XMLPagosTCOtrosBancosDatos.xml"
#elif defined(USE_TEST)
#define PAYMENT_CARD_OTHER_BANK_DATA_OPERATION                      @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_recuperar_datos_otro_banco_op&accion=continuar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CARD_OTHER_BANK_DATA_OPERATION                      @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_recuperar_datos_otro_banco_op&accion=continuar"
#else
#define PAYMENT_CARD_OTHER_BANK_DATA_OPERATION                      @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_recuperar_datos_otro_banco_op&accion=continuar"
#endif

/**
 * Defines the confirmation request card payment own cards
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CARD_OTHER_BANK_CONFIRMATION_OPERATION              @"XMLPagosTCOtrosBancosConfirmacion.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CARD_OTHER_BANK_CONFIRMATION_OPERATION              @"XMLPagosTCOtrosBancosConfirmacion.xml"
#elif defined(USE_TEST)
#define PAYMENT_CARD_OTHER_BANK_CONFIRMATION_OPERATION              @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_consultar_flujo_pago_op&accion=consultar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CARD_OTHER_BANK_CONFIRMATION_OPERATION              @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_consultar_flujo_pago_op&accion=consultar"
#else
#define PAYMENT_CARD_OTHER_BANK_CONFIRMATION_OPERATION              @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_consultar_flujo_pago_op&accion=consultar"
#endif

/**
 * Defines the confirmation request card payment own cards
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CARD_OTHER_BANK_TIN_CONFIRMATION_OPERATION              @"XMLPagosTCOtrosBancosConfirmacion.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CARD_OTHER_BANK_TIN_CONFIRMATION_OPERATION              @"XMLPagosTCOtrosBancosConfirmacion.xml"
#elif defined(USE_TEST)
#define PAYMENT_CARD_OTHER_BANK_TIN_CONFIRMATION_OPERATION              @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_confirmar_pago_otro_banco_op&accion="
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CARD_OTHER_BANK_TIN_CONFIRMATION_OPERATION              @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_confirmar_pago_otro_banco_op&accion="
#else
#define PAYMENT_CARD_OTHER_BANK_TIN_CONFIRMATION_OPERATION              @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_confirmar_pago_otro_banco_op&accion="
#endif
/**
 * Defines the success request card payment own cards
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define PAYMENT_CARD_OTHER_BANK_SUCCESS_OPERATION                   @"XMLPagosTCOtrosBancosExito.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define PAYMENT_CARD_OTHER_BANK_SUCCESS_OPERATION                   @"XMLPagosTCOtrosBancosExito.xml"
#elif defined(USE_TEST) 
#define PAYMENT_CARD_OTHER_BANK_SUCCESS_OPERATION                   @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_realizar_pago_otro_banco_op&accion=confirmar"
#elif defined(USE_PREPRODUCTION)
#define PAYMENT_CARD_OTHER_BANK_SUCCESS_OPERATION                   @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_realizar_pago_otro_banco_op&accion=confirmar"
#else
#define PAYMENT_CARD_OTHER_BANK_SUCCESS_OPERATION                   @"OperacionCBTFServlet?proceso=pt_pago_tarjetas_otros_bancos_pr&operacion=pt_realizar_pago_otro_banco_op&accion=confirmar"
#endif

#pragma mark -
#pragma mark Frequent operations
/**
 * Defines the frequent operations initial list request
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define FREQUENT_OPERATIONS_INITIAL_OPERATION                   @"XMLPagosTCOtrosBancosExito.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define FREQUENT_OPERATIONS_INITIAL_OPERATION                   @"XMLPagosTCOtrosBancosExito.xml"
#elif defined(USE_TEST)
#define FREQUENT_OPERATIONS_INITIAL_OPERATION                   @"OperacionCBTFServlet?proceso=ofPagos_Frecuentes_pr&operacion=ofListadoPagos_op&accion=listar"
#elif defined(USE_PREPRODUCTION)
#define FREQUENT_OPERATIONS_INITIAL_OPERATION                   @"OperacionCBTFServlet?proceso=ofPagos_Frecuentes_pr&operacion=ofListadoPagos_op&accion=listar"
#else
#define FREQUENT_OPERATIONS_INITIAL_OPERATION                   @"OperacionCBTFServlet?proceso=ofPagos_Frecuentes_pr&operacion=ofListadoPagos_op&accion=listar"
#endif

/**
 * Defines the frequent operations pagin list request
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define FREQUENT_OPERATIONS_INITIAL_OPERATION                   @"XMLPagosTCOtrosBancosExito.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define FREQUENT_OPERATIONS_INITIAL_OPERATION                   @"XMLPagosTCOtrosBancosExito.xml"
#elif defined(USE_TEST)
#define FREQUENT_OPERATIONS_PAGIN_OPERATION                     @"OperacionCBTFServlet?proceso=ofPagos_Frecuentes_pr&operacion=ofListadoPagos_op&accion=adelante"
#elif defined(USE_PREPRODUCTION)
#define FREQUENT_OPERATIONS_PAGIN_OPERATION                     @"OperacionCBTFServlet?proceso=ofPagos_Frecuentes_pr&operacion=ofListadoPagos_op&accion=adelante"
#else
#define FREQUENT_OPERATIONS_PAGIN_OPERATION                     @"OperacionCBTFServlet?proceso=ofPagos_Frecuentes_pr&operacion=ofListadoPagos_op&accion=adelante"
#endif

/**
 * Defines the frequent operations execution request
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define FREQUENT_OPERATIONS_EXECUTE_OPERATION                   @"XMLPagosTCOtrosBancosExito.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define FREQUENT_OPERATIONS_EXECUTE_OPERATION                   @"XMLPagosTCOtrosBancosExito.xml"
#elif defined(USE_TEST)
#define FREQUENT_OPERATIONS_EXECUTE_OPERATION                   @"OperacionCBTFServlet?proceso=ofPagos_Frecuentes_pr&operacion=ofEjecucion_op&accion=ejecucion"
#elif defined(USE_PREPRODUCTION)
#define FREQUENT_OPERATIONS_EXECUTE_OPERATION                   @"OperacionCBTFServlet?proceso=ofPagos_Frecuentes_pr&operacion=ofEjecucion_op&accion=ejecucion"
#else
#define FREQUENT_OPERATIONS_EXECUTE_OPERATION                   @"OperacionCBTFServlet?proceso=ofPagos_Frecuentes_pr&operacion=ofEjecucion_op&accion=ejecucion"
#endif


#pragma mark -
#pragma mark Third Account Selection
/**
 * Defines the third account initial list request
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define THIRD_ACCOUNT_INITIAL_OPERATION                   @"XMLThirdAccountList.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define THIRD_ACCOUNT_INITIAL_OPERATION                   @"XMLThirdAccountList.xml"
#elif defined(USE_TEST)
#define THIRD_ACCOUNT_INITIAL_OPERATION                   @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_listado_cuentas_op&accion=listar"
#elif defined(USE_PREPRODUCTION)
#define THIRD_ACCOUNT_INITIAL_OPERATION                   @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_listado_cuentas_op&accion=listar"
#else
#define THIRD_ACCOUNT_INITIAL_OPERATION                   @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_listado_cuentas_op&accion=listar"
#endif


/**
 * Defines the third account paging list request
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define THIRD_ACCOUNT_PAGIN_OPERATION                   @"XMLThirdAccountList.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define THIRD_ACCOUNT_PAGIN_OPERATION                   @"XMLThirdAccountList.xml"
#elif defined(USE_TEST)
#define THIRD_ACCOUNT_PAGIN_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_listado_cuentas_op&accion=masDatos"
#elif defined(USE_PREPRODUCTION)
#define THIRD_ACCOUNT_PAGIN_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_listado_cuentas_op&accion=masDatos"
#else
#define THIRD_ACCOUNT_PAGIN_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_listado_cuentas_op&accion=masDatos"
#endif

/**
 * Defines the third account confirmation request
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define THIRD_ACCOUNT_REGISTER_CONFIRMATION_OPERATION                   @"XMLThirdAccountList.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define THIRD_ACCOUNT_REGISTER_CONFIRMATION_OPERATION                   @"XMLThirdAccountList.xml"
#elif defined(USE_TEST)
#define THIRD_ACCOUNT_REGISTER_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_validacion_alias_op&accion=validaAlias"
#elif defined(USE_PREPRODUCTION)
#define THIRD_ACCOUNT_REGISTER_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_validacion_alias_op&accion=validaAlias"
#else
#define THIRD_ACCOUNT_REGISTER_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_validacion_alias_op&accion=validaAlias"
#endif

/**
 * Defines the third account confirmation request
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define THIRD_ACCOUNT_DELETE_CONFIRMATION_OPERATION                   @"XMLThirdAccountList.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define THIRD_ACCOUNT_DELETE_CONFIRMATION_OPERATION                   @"XMLThirdAccountList.xml"
#elif defined(USE_TEST)
#define THIRD_ACCOUNT_DELETE_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_eliminacion_op&accion=eliminar"
#elif defined(USE_PREPRODUCTION)
#define THIRD_ACCOUNT_DELETE_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_eliminacion_op&accion=eliminar"
#else
#define THIRD_ACCOUNT_DELETE_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_eliminacion_op&accion=eliminar"
#endif


/**
 * Defines the third account confirmation request
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define THIRD_ACCOUNT_VALIDATE_NICKNAME_OPERATION                   @"XMLThirdAccountList.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define THIRD_ACCOUNT_VALIDATE_NICKNAME_OPERATION                   @"XMLThirdAccountList.xml"
#elif defined(USE_TEST)
#define THIRD_ACCOUNT_VALIDATE_NICKNAME_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_validacion_op&accion=valida"
#elif defined(USE_PREPRODUCTION)
#define THIRD_ACCOUNT_VALIDATE_NICKNAME_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_validacion_op&accion=valida"
#else
#define THIRD_ACCOUNT_VALIDATE_NICKNAME_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_validacion_op&accion=valida"
#endif

/**
 * Defines the third account success request
 */
#if defined(SIMULATE_HTTP_CONNECTION)
#define THIRD_ACCOUNT_SUCCESS_OPERATION                   @"XMLThirdAccountList.xml"
#elif defined(MOVILOK_ENVIRONMENT)
#define THIRD_ACCOUNT_SUCCESS_OPERATION                   @"XMLThirdAccountList.xml"
#elif defined(USE_TEST)
#define THIRD_ACCOUNT_SUCCESS_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_realiza_inscripcion_op&accion=realizaInscripcion"
#elif defined(USE_PREPRODUCTION)
#define THIRD_ACCOUNT_SUCCESS_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_realiza_inscripcion_op&accion=realizaInscripcion"
#else
#define THIRD_ACCOUNT_SUCCESS_OPERATION                     @"OperacionCBTFServlet?proceso=icb_inscribir_cuenta_beneficiaria_pr&operacion=icb_realiza_inscripcion_op&accion=realizaInscripcion"
#endif

/**
 * Banner Campaña Digital
 */
#define CAMPAIGN_LIST_OPERATION									@"OperacionCBTFServlet?proceso=posicion_global_pr&operacion=og_campanias_datazo_op&accion=consulta"
/**
* Increment Of Line
*/
#define INCREMENT_OF_LINE_INITIAL_OPERATION                     @"OperacionCBTFServlet?proceso=il_incremento_linea_pr&operacion=il_solicitud_incremento_op&accion=mostrar"
#define INCREMENT_OF_LINE_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=il_incremento_linea_pr&operacion=il_solicitud_incremento_op&accion=solicitarOTP"
#define INCREMENT_OF_LINE_SUMMARY_OPERATION                     @"OperacionCBTFServlet?proceso=il_incremento_linea_pr&operacion=il_solicitud_incremento_op&accion=procesar"

/**
 * Fast Loan
 */
#define FAST_LOAN_INITIAL_OPERATION                     @"OperacionCBTFServlet?proceso=pi_prestamo_inmediato_pr&operacion=pi_solicitud_prestamo_op&accion=mostrar"
#define FAST_LOAN_CONFIRMATION_OPERATION                     @"OperacionCBTFServlet?proceso=pi_prestamo_inmediato_pr&operacion=pi_confirma_prestamo_op&accion=continuar"
#define FAST_LOAN_SUMMARY_OPERATION                     @"OperacionCBTFServlet?proceso=pi_prestamo_inmediato_pr&operacion=pi_realiza_prestamo_op&accion=continuar"
#define FAST_LOAN_TERM_OPERATION                     @"OperacionCBTFServlet?proceso=pi_prestamo_inmediato_pr&operacion=pi_solicitud_prestamo_op&accion=obtenerPlazos"
#define FAST_LOAN_SCHEDULE_OPERATION                     @"OperacionCBTFServlet?proceso=pi_prestamo_inmediato_pr&operacion=pi_simula_prestamo_op&accion=vercronograma"
#define FAST_LOAN_SIMULATION_OPERATION                     @"OperacionCBTFServlet?proceso=pi_prestamo_inmediato_pr&operacion=pi_simula_prestamo_op&accion=simular"


/**
 * Salary Advance
 */
#define SALARY_ADVANCE_AFFILIATION_OPERATION                     @"OperacionCBTFServlet?proceso=as_adelanto_sueldo_pr&operacion=as_afiliacion_ingreso_op&accion=continuar"
#define SALARY_ADVANCE_RECEIVE_OPERATION                     @"OperacionCBTFServlet?proceso=as_adelanto_sueldo_pr&operacion=as_disposicion_ingreso_op&accion=continuar"
#define SALARY_ADVANCE_SUMMMARY_OPERATION                     @"OperacionCBTFServlet?proceso=as_adelanto_sueldo_pr&operacion=as_disposicion_realiza_op&accion=continuar"
#define SALARY_ADVANCE_AFFILIATION_CONFIRM_OPERATION                     @"OperacionCBTFServlet?proceso=as_adelanto_sueldo_pr&operacion=as_afiliacion_realiza_op&accion=continuar"



#pragma mark -
#pragma mark Types of movements

/**
 * Reasons of card locking
 */
#define LOCK_CARD_REASONS_OBJECTS                                   NSLocalizedString(LOCK_CARD_REASON_STEALING_KEY, nil), NSLocalizedString(LOCK_CARD_REASON_LOSS_KEY, nil), nil

/**
 * Defines the first splash delay
 */
#define FIRST_SPLASH_DELAY                                          4.0f

/**
 * Defines the splash delay
 */
#define SPLASH_DELAY												2.0f

/**
 * Define states of the information we get from server 
 */
typedef enum {
	soie_NoInfo = 0,
	soie_InfoAvailable,
	soie_Updating,
	soie_Incomplete,
} StateOfInformationEnumeration;

#pragma mark -
#pragma mark Browser urls

#define BROWSER_URL_DISCLAIMER                                      @"http://www.bbva.es/TLBS/fsbin/mult/BBVAMoviliPhonev2_tcm423-237451.pdf"
#define BROWSER_URL_MORE                                            @"http://movilok.net/nxt/More.html"

#pragma mark -
#pragma mark Notifications

#define kNotificationFORCashMobileStepOne @"kNotificationFORCashMobileStepOne"
#define kNotificationFOROtherBankStepOne @"kNotificationFOROtherBankStepOne"
#define kNotificationFORThirdAccountStepOne @"kNotificationFORThirdAccountStepOne"
#define kNotificationFORPaymentThirdCardStepOne @"kNotificationFORPaymentThirdCardStepOne"
#define kNotificationFORPaymentOtherBankStepOne @"kNotificationFORPaymentOtherBankStepOne"
#define kNotificationFORPaymentGiftCardStepOne @"kNotificationFORPaymentGiftCardStepOne"
#define kNotificationFORRechargeClaroStepOne  @"kNotificationFORRechargeClaroStepOne"
#define kNotificationFORRechargeMovistarStepOne @"kNotificationFORRechargeMovistarStepOne"
#define kNotificationFORServiceClaroStepOne @"kNotificationFORServiceClaroStepOne"
#define kNotificationFORServiceMovistarStepOne @"kNotificationFORServiceMovistarStepOne"
#define kNotificationFORServiceWaterStepOne @"kNotificationFORServiceWaterStepOne"
#define kNotificationFORInstitutionAndCompaniesStepOne @"kNotificationFORInstitutionAndCompaniesStepOne"
#define kNotificationFORStepTwo @"kNotificationFORStepTwo"
#define kNotificationFORStepThree @"kNotificationFORStepThree"

#define kNotificationGeneralLoginUpdated                            @"kNotificationGeneralLoginUpdated"
#define kNotificationCoordinateLoginUpdated                         @"kNotificationCoordinateLoginUpdated"

#define kNotificationAlterGlobalPositionResponseReceived                 @"kNotificationAlterGlobalPositionResponseReceived"

#define kNotificationSafetyPayStatusResponseReceived @"kNotificationSafetyPayStatusResponseReceived"
#define kNotificationSafetyPayTransactionInfoResponseReceived @"kNotificationSafetyPayTransactionInfoResponseReceived"
#define kNotificationSafetyPayDetailsResponseReceived @"kNotificationSafetyPayDetailsResponseReceived"
#define kNotificationSafetyPayConfirmationResponseReceived @"kNotificationSafetyPayConfirmationResponseReceived"

#define kNotificationGlobalPositionResponseReceived                 @"kNotificationGlobalPositionResponseReceived"
#define kNotificationRetentionListResponseReceived                  @"kNotificationRetentionListResponseReceived"
#define kNotificationTransferConfirmationResponseReceived           @"kNotificationTransferConfirmationResponseReceived"
#define kNotificationTransferTypeCheckResponseReceived           @"kNotificationTransferTypeCheckResponseReceived"
#define kNotificationTransferSuccessResponseReceived                @"kNotificationTransferSuccessResponseReceived"
#define kNotificationAccountTransactionsResponseReceived            @"kNotificationAccountTransactionsResponseReceived"
#define kNotificationAccountTransactionDetailResponseReceived       @"kNotificationAccountTransactionDetailResponseReceived"
#define kNotificationCardTransactionsResponseReceived               @"kNotificationCardTransactionsResponseReceived"

#define kNotificationCuentaBancoUpdated                                 @"kNotificationCuentaBancoUpdated"

#define kNotificationAccountUpdated                                 @"kNotificationAccountUpdated"
#define kNotificationAccountTransactionUpdated                      @"kNotificationAccountTransactionUpdated"
#define kNotificationCardTransactionUpdated                         @"kNotificationCardTransactionUpdated"
#define kNotificationAccountTransactionsAdditionalnformationUpdated  @"kNotificationAccountTransactionsAdditionalnformationUpdated"
#define kNotificationCardTransactionsAdditionalnformationUpdated    @"kNotificationCardTransactionsAdditionalnformationUpdated"
#define kNotificationCardUpdated                                    @"kNotificationCardUpdated"

#define kNotificationListaCuentasReceived                            @"kNotificationListaCuentasReceived"

#define kNotificationAlterCarrierListReceived @"kNotificationAlterCarrierListReceived"

#define kNotificationAccountListReceived                            @"kNotificationAccountListReceived"
#define kNotificationAccountTransactionListReceived                 @"kNotificationAccountTransactionListReceived"
#define kNotificationCardTransactionListReceived                    @"kNotificationCardTransactionListReceived"
#define kNotificationLoginEnds                                      @"kNotificationLoginEnds"
#define kNotificationLoginCoordinateEnds							@"kNotificationLoginCoordinateEnds"
#define kNotificationLoginEncryptEnds							    @"kNotificationLoginEncryptEnds"
#define kNotificationLoginDesencryptEnds							@"kNotificationLoginDesencryptEnds"

#define kNotificationGlobalPositionEnds								@"kNotificationGlobalPositionEnds"
#define kNotificationRetrieveAccountTransactionListEnds             @"kNotificationRetrieveAccountTransactionListEnds"
#define kNotificationRetrieveCardTransactionListEnds				@"kNotificationRetrieveCardTransactionListEnds"
#define kNotificationTransferBetweenAccountsStartupEnds             @"kNotificationTransferBetweenAccountsStartupEnds"

#define kNotificationRetrieveRetainsEnds							@"kNotificationRetrieveRetainsEnds"
#define kNotificationSendTransactionEnds							@"kNotificationSendTransactionEnds"
#define kNotificationSendTransactionConfirmationEnds				@"kNotificationSendTransactionConfirmationEnds"
#define kNotificationCloseEnds                                      @"kNotificationCloseEnds"
#define kNotificationLogoutEnds                                     @"kNotificationLogoutEnds"
#define kNotificationTransferToThirdAccountsStartupEnds             @"kNotificationTransferToThirdAccountsStartupEnds"
#define kNotificationTransferToAccountFromOtherBanksStartupEnds     @"kNotificationTransferToAccountFromOtherBanksStartupEnds"

#define kNotificationTransferWithCashMobileStartupEnds              @"kNotificationTransferWithCashMobileStartupEnds"
#define kNotificationTransferWithCashMobileConfirmationEnds         @"kNotificationTransferWithCashMobileConfirmationEnds"
#define kNotificationTransferWithCashMobileResultEnds               @"kNotificationTransferWithCashMobileResultEnds"
#define kNotificationTransferWithCashMobileDetailStartupEnds        @"kNotificationTransferWithCashMobileDetailStartupEnds"
#define kNotificationTransferWithCashMobileDetailEnds               @"kNotificationTransferWithCashMobileDetailEnds"
#define kNotificationTransferWithCashMobileDetailResendEnds         @"kNotificationTransferWithCashMobileDetailResendEnds"

#define kNotificationTransferToGiftCardStartupEnds                  @"kNotificationTransferToGiftCardStartupEnds"

#define kNotificationMATConfigurationReceived                       @"kNotificationMATConfigurationReceived"
#define kNotificationDateLegalTermsReceived                         @"kNotificationDateLegalTermsReceived"

#define kNotificationPaymentServicesListResultEnds                  @"kNotificationPaymentServicesListResultEnds"
#define kNotificationPaymentPublicServiceInitialEnds                @"kNotificationPaymentPublicServiceInitialEnds"
#define kNotificationPaymentServicesListResultEnds                  @"kNotificationPaymentServicesListResultEnds"
#define kNotificationPaymentDataResultEnds                          @"kNotificationPaymentDataResultEnds"
#define kNotificationPaymentTINOnlineResultEnds                  @"kNotificationPaymentTINOnlineResultEnds"
#define kNotificationPaymentConfirmationResultEnds                  @"kNotificationPaymentConfirmationResultEnds"
#define kNotificationPaymentSuccessResultEnds                       @"kNotificationPaymentSuccessResultEnds"
#define kNotificationPaymentRechargesInitialResultEnds				@"kNotificationPaymentRechargesInitialResultEnds"
#define kNotificationPaymentContinentalInicilizationResultEnds		@"kNotificationPaymentContinentalInicilizationResultEnds"
#define kNotificationPaymentOtherBankInicilizationResultEnds		@"kNotificationPaymentOtherBankInicilizationResultEnds"

#define kNotificationPaymentInstitutionsListResultEnds              @"kNotificationPaymentInstitutionsListResultEnds"
#define kNotificationPaymentInstitutionsSearchListResult            @"kNotificationPaymentInstitutionsSearchListResult"
#define kNotificationPaymentInstitutionsDetailResult                @"kNotificationPaymentInstitutionsDetailResult"
#define kNotificationPaymentInstitutionsPendingPaysDetailResult     @"kNotificationPaymentInstitutionsPendingPaysDetailResult"
#define kNotificationPaymentInstitutionsConfirmationPayResult       @"kNotificationPaymentInstitutionsConfirmationPayResult"
#define kNotificationPaymentInstitutionsSuccessPayResult            @"kNotificationPaymentInstitutionsSuccessPayResult"


#define kNotificationFOInitialResponse                              @"kNotificationFOInitialResponse"
#define kNotificationFOPThirdCardPaymentStepOneResponse            @"kNotificationFOPThirdCardPaymentStepOneResponse"
#define kNotificationFOPThirdCardPaymentStepTwoResponse            @"kNotificationFOPThirdCardPaymentStepTwoResponse"
#define kNotificationFOPThirdCardPaymentStepThreeResponse            @"kNotificationFOPThirdCardPaymentStepThreeResponse"

#define kNotificationFOPOtherBankPaymentStepOneResponse @"kNotificationFOPOtherBankPaymentStepOneResponse"
#define kNotificationFOPOtherBankPaymentStepTwoResponse @"kNotificationFOPOtherBankPaymentStepTwoResponse"
#define kNotificationFOPCOtherBankPaymentStepThreeResponse  @"kNotificationFOPCOtherBankPaymentStepThreeResponse"

#define kNotificationFOPCashMobileStepOneResponse            @"kNotificationFOPCashMobileStepOneResponse"
#define kNotificationFOPCashMobileStepTwoResponse            @"kNotificationFOPCashMobileStepTwoResponse"
#define kNotificationFOPCashMobileStepThreeResponse            @"kNotificationFOPCashMobileStepThreeResponse"
#define kNotificationFOPRechargeCellphoneStepTwoResponseReceived  @"kNotificationFOPRechargeCellphoneStepTwoResponseReceived"

#define kNotificationFOPThirdAccountTransferStepOneResponse            @"kNotificationFOPThirdAccountTransferStepOneResponse"
#define kNotificationFOPThirdAccountTransferStepTwoResponse            @"kNotificationFOPThirdAccountTransferStepTwoResponse"
#define kNotificationFOPThirdAccountTransferStepThreeResponse            @"kNotificationFOPThirdAccountTransferStepThreeResponse"

#define kNotificationFOPRechargeGiftCardStepTwoResponse @"kNotificationFOPRechargeGiftCardStepTwoResponse"
#define kNotificationFOPRechargeGiftCardStepThreeResponse @"kNotificationFOPRechargeGiftCardStepThreeResponse"

#define kNotificationFOPOtherBankTransferStepOneResponse            @"kNotificationFOPOtherBankTransferStepOneResponse"
#define kNotificationFOPOtherBankTransferStepTwoResponse            @"kNotificationFOPOtherBankTransferStepTwoResponse"
#define kNotificationFOPOtherBankTransferStepThreeResponse            @"kNotificationFOPOtherBankTransferStepThreeResponse"
#define kNotificationFOPRechargeCellphoneStepTwoResponse @"kNotificationFOPRechargeCellphoneStepTwoResponse"
#define kNotificationFOPRechargeCellphoneStepThreeResponse @"kNotificationFOPRechargeCellphoneStepThreeResponse"
#define kNotificationFOPRechargeGiftCardStepOneResponse @"kNotificationFOPRechargeGiftCardStepOneResponse"


#define kNotificationFOPElectricServiceStepOneResponse            @"kNotificationFOPElectricServiceStepOneResponse"
#define kNotificationFOPElectricServiceStepTwoResponse            @"kNotificationFOPElectricServiceStepTwoResponse"
#define kNotificationFOPElectricServiceStepThreeResponse            @"kNotificationFOPElectricServiceStepThreeResponse"

#define kNotificationFOPWaterServiceStepOneResponse            @"kNotificationFOPWaterServiceStepOneResponse"
#define kNotificationFOPWaterServiceStepTwoResponse            @"kNotificationFOPWaterServiceStepTwoResponse"
#define kNotificationFOPWaterServiceStepThreeResponse            @"kNotificationFOPWaterServiceStepThreeResponse"

#define kNotificationFOPInstitutionPaymentStepOneResponse            @"kNotificationFOPInstitutionPaymentStepOneResponse"
#define kNotificationFOPInstitutionPaymentStepTwoResponse            @"kNotificationFOPInstitutionPaymentStepTwoResponse"
#define kNotificationFOPInstitutionPaymentStepThreeResponse            @"kNotificationFOPInstitutionPaymentStepThreeResponse"

#define kNotificationFOPDirectTVGeneralStepOneResponse @"kNotificationFOPDirectTVGeneralStepOneResponse"
#define kNotificationFOPDirectTVGeneralStepTwoResponse @"kNotificationFOPDirectTVGeneralStepTwoResponse"
#define kNotificationFOPDirectTVMonthlyPaymentStepThreeResponse @"kNotificationFOPDirectTVMonthlyPaymentStepThreeResponse"
#define kNotificationFOPDirectTVInstallationStepThreeResponse @"kNotificationFOPDirectTVInstallationStepThreeResponse"

#define kNotificationFOPMobilePhoneServiceStepTwoResponse @"kNotificationFOPMobilePhoneServiceStepTwoResponse"
#define kNotificationFOPMobilePhoneServiceStepThreeResponse @"kNotificationFOPMobilePhoneServiceStepThreeResponse"

#define kFOPGeneralMenuStepOneResponse @"kFOPGeneralMenuStepOneResponse"

#define kNotificationSafetyPayStatusResponseReceivedTest                @"kNotificationSafetyPayStatusResponseReceivedTest"
#define kNotificationLoginEndsTest                                      @"kNotificationLoginEndsTest"
#define kNotificationSafetyPayTransaccionInfoResponseReceivedTest @"kNotificationSafetyPayTransaccionInfoResponseReceivedTest"
#define kNotificationSafetyPayDetailsResponseReceivedTest @"kNotificationSafetyPayDetailsResponseReceivedTest"
#define kNotificationSafetyPayConfirmationResponseReceivedTest @"kNotificationSafetyPayConfirmationResponseReceivedTest"


#define kNotificationFOMGeneralStepOneResponse            @"kNotificationFOMGeneralStepOneResponse"
#define kNotificationFOMGeneralStepThreeResponse            @"kNotificationFOMGeneralStepThreeResponse"
#define kNotificationFOMGeneralStepFourResponse            @"kNotificationFOMGeneralStepFourResponse"

#define kNotificationFOMCashMobileStepTwoResponse            @"kNotificationFOMCashMobileStepTwoResponse"
#define kNotificationFOMCashMobileStepThreeResponse            @"kNotificationFOMCashMobileStepThreeResponse"
#define kNotificationFOMCashMobileStepFourResponse            @"kNotificationFOMCashMobileStepFourResponse"
#define kNotificationFOMCOtherBankPaymentStepTwoResponse @"kNotificationFOMCOtherBankPaymentStepTwoResponse"
#define kNotificationFOMThirdCardPaymentStepTwoResponse @"kNotificationFOMThirdCardPaymentStepTwoResponse"

#define kNotificationFOMThirdAccountTransferStepTwoResponse            @"kNotificationFOMThirdAccountTransferStepTwoResponse"
#define kNotificationFOMOtherBankTransferStepTwoResponse            @"kNotificationFOMOtherBankTransferStepTwoResponse"
#define kNotificationFOMRechargeCellphoneStepTwoResponse            @"kNotificationFOMRechargeCellphoneStepTwoResponse"
#define kNotificationFOMRechargeGiftCardStepTwoResponse @"kNotificationFOMRechargeGiftCardStepTwoResponse"
#define kNotificationFOMElectricServiceStepTwoResponse @"kNotificationFOMElectricServiceStepTwoResponse"
#define kNotificationFOMWaterServiceStepTwoResponse @"kNotificationFOMWaterServiceStepTwoResponse"
#define kNotificationFOMDirectTVStepTwoResponse @"kNotificationFOMDirectTVStepTwoResponse"
#define kNotificationFOMInstitutionPaymentStepTwoResponse @"kNotificationFOMInstitutionPaymentStepTwoResponse"

#define kNotificationFOMRechargeGiftCardStepTwoResponse @"kNotificationFOMRechargeGiftCardStepTwoResponse"
#define kNotificationFOMPhoneServiceClaroStepTwoResponse @"kNotificationFOMPhoneServiceClaroStepTwoResponse"
#define kNotificationFOMPhoneServiceMovistarStepTwoResponse @"kNotificationFOMPhoneServiceMovistarStepTwoResponse"


#define kNotificationGiftCardStepOneResponse @"kNotificationGiftCardStepOneResponse"
#define kNotificationGiftCardStepTwoResponse @"kNotificationGiftCardStepTwoResponse"
#define kNotificationGiftCardStepThreeResponse @"kNotificationGiftCardStepThreeResponse"

#define kNotificationFrequentOperationsInitialListResponse      @"kNotificationFrequentOperationsInitialListResponse"
#define kNotificationFrequentOperationsExecuteResponse          @"kNotificationFrequentOperationsExecuteResponse"


#define kNotificationRegisterAccountInitialListResponse      @"kNotificationRegisterAccountInitialListResponse"
#define kNotificationDeleteThirdAccountResponse     @"kNotificationDeleteThirdAccountResponse"
#define kNotificationRegisterThirdAccountConfirmation      @"kNotificationRegisterThirdAccountConfirmation"
#define kNotificationRegisterThirdAccountResult      @"kNotificationRegisterThirdAccountResult"
#define kNotificationThirdAccountUpdated                      @"kNotificationThirdAccountUpdated"
#define kNotificationThirdAccountListReceived           @"kNotificationThirdAccountListReceived"
#define kNotificationThirdAccountListResponse       @"ThirdAccountListResponse"
#define kNotificationValidateThirdAccountResponse      @"kNotificationValidateThirdAccountResponse"

#define kNotificationCampaignListResponseReceived                 @"kNotificationCampaignListResponseReceived"

#define kNotificationIncrementOfLineStepOneResponse               @"kNotificationIncrementOfLineStepOneResponse"
#define kNotificationIncrementOfLineStepTwoResponse               @"kNotificationIncrementOfLineStepTwoResponse"
#define kNotificationIncrementOfLineStepThreeResponse             @"kNotificationIncrementOfLineStepThreeResponse"

#define kNotificationFastLoanStepOneResponse                      @"kNotificationFastLoanStepOneResponse"
#define kNotificationFastLoanStepTwoResponse                      @"kNotificationFastLoanStepTwoResponse"
#define kNotificationFastLoanStepThreeResponse                    @"kNotificationFastLoanStepThreeResponse"
#define kNotificationFastLoanTermResponse                         @"kNotificationFastLoanTermResponse"
#define kNotificationFastLoanScheduleResponse                     @"kNotificationFastLoanScheduleResponse"
#define kNotificationFastLoanSimulationResponse              @"kNotificationFastLoanSimulationResponse"

#define kNotificationSalaryAdvanceAffiliationResponse                @"kNotificationSalaryAdvanceAffiliationResponse"
#define kNotificationSalaryAdvanceAffiliationConfirmResponse         @"kNotificationSalaryAdvanceAffiliationConfirmResponse"
#define kNotificationSalaryAdvanceReceiveResponse                    @"kNotificationSalaryAdvanceReceiveResponse"
#define kNotificationSalaryAdvanceSummaryResponse                    @"kNotificationSalaryAdvanceSummaryResponse"

/*
 * Enumerates the atms states
 */
typedef enum {
    branchType=0,
    atmType, 
	atmBCType, 
	atmInterbankType,
    atmScotiabankType,
    expressAgentType ,
    expressAgentPlusType,
    kasnetType,
    multifacilType
} AgentStates;

/**
 * Password valid characters
 */
#define PASSWORD_VALID_CHARACTERS							@"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

/*
 * Email text valid characters
 */
#define EMAIL_TEXT_VALID_CHARACTERS                         @"0123456789abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ@.-_"

/**
 * Operation key valid characters
 */
#define OPERATION_KEY_VALID_CHARACTERS                      @"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

/**
 * Reference valid characters
 */
#define REFERENCE_VALID_CHARACTERS                          @"0123456789abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ "
/**
 * alphabetic valid characters
 */
#define ALPHABET_VALID_CHARACTERS                          @"abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ "
/**
 * OTP key valid characters
 */
#define OTP_VALID_CHARACTERS                                @"0123456789"

/**
 * Day of month valid characters
 */
#define DAY_OF_MONTH_VALID_CHARACTERS                                @"0123456789"

/**
 * Frist transfer account code identifier.
 */
#define CHECKING_ACCOUNT_TYPE   @"01"

/**
 * Second transfer account code identifier.
 */
#define SAVINGS_ACCOUNT_TYPE   @"02"

/*
 * Dni document type
 */
#define DOC_TYPE_DNI                @"L"

/*
 * RUC document type
 */
#define DOC_TYPE_RUC                @"R"

/*
 * militar id document type
 */
#define DOC_TYPE_MILITAR_ID         @"M"

/*
 * pasaport document type
 */
#define DOC_TYPE_PASAPORT           @"P"

/*
 * inmigration card document type
 */
#define DOC_TYPE_INMIGRATION_ID     @"E"




#pragma mark -
#pragma mark MCB configuration

//#define MCB_AUTHORITY_SERVER                                        @"192.168.2.184:8089"
//79.125.125.123
#define MCB_AUTHORITY_SERVER                                        @"api.bbva.com"
#define MCB_APPLICATION_NAME                                        @"com.bbva.nxt_pe"
#define MCB_APPLICATION_KEY                                         @"3b3874a11a2cd2c1adf9ab9dd9ef35a376896a83"

#pragma mark -
#pragma mark Global position cell arrows

#define ACCOUNTS_ARROW                                          @"YES"
#define DEPOSITS_ARROW                                          @"NO"
#define STEP_ACCOUNTS_ARROW                                     @"NO"
#define FFMM_ARROW                                              @"NO"
#define CONTINENTAL_STOCK_ARROW                                 @"NO"
#define CARDS_ARROW                                             @"YES"
#define LOANS_ARROW                                             @"NO"


#pragma mark -
#pragma mark Other bank accounts elements sizes

/**
 * Defines the maximum number of other bank entity digits
 */
#define MAX_OTHER_BANK_NUMBER_ENTITY                            3

/**
 * Defines the maximum number of other bank office digits
 */
#define MAX_OTHER_BANK_NUMBER_OFFICE                            3

/**
 * Defines the maximum number of other bank account number digits
 */
#define MAX_OTHER_BANK_NUMBER_ACCOUNT                           12

/**
 * Defines the maximum number of other bank account CC digits
 */
#define MAX_OTHER_BANK_NUMBER_CC                                2


#pragma mark -
#pragma mark Public services codes

#define PAYMENT_WATER_SERVICE_CODE										@"01"
#define PAYMENT_ELECTRICAL_SERVICE_CODE									@"02"
#define PAYMENT_LANDLINE_SERVICE_CODE									@"03"
#define PAYMENT_MOBILE_PHONE_SERVICE_CODE								@"04"
#define PAYMENT_GAS_SERVICE_CODE                                        @"05"
#define PAYMENT_CABLE_SERVICE_CODE                                      @"06"

#pragma mark -
#pragma mark Frequent operations codes

#define FO_TRANS_TO_THIRD_CODE                                          @"P001"
#define FO_TRANS_TO_OTHER_BANK_CODE                                     @"P002"
#define FO_PAY_THIRD_CARD                                               @"P004"
#define FO_PAY_OTHER_BANK_CARD                                          @"P005"
#define FO_PAY_AGREEMENT                                                @"P006"
#define FO_PAY_WATER_SERVICE                                            @"P007"
#define FO_PAY_TELE_SERVICE                                             @"P008"
#define FO_RECHARGE_GIFT_CARD                                           @"P010"
#define FO_PAY_CLARO_SERVICE                                            @"P011"
#define FO_RECHARGE_CELL_PHONE                                          @"P012"
#define FO_CASH_MOBILE                                                  @"P017"

#define FO_LANDLINE                                                     @"10"
#define FO_CELLPHONE                                                    @"11"
#define FO_RECHARGE                                                     @"14"

#define  REMAIN_ONE_TRY_CONFIRM_CODE_ERROR      "1201"
#define  WRONG_CONFIRM_CODE_ERROR               "1206"
#define  SMS_CODE_EXPIRED_ERROR                 "1204"
#define  MAX_TRY_CONFIRM_CODE_ERROR             "1205"
#define  WRONG_COORDINATE_CONFIRM_CODE_ERROR    "-205"

#define  SALARY_ADVANCE_AFILIATED      "as_disposicion_ingreso_op"
#define  SALARY_ADVANCE_NO_AFILIATED    "as_afiliacion_ingreso_op"

#define DEEPLINK_NONE                                                   -1
#define DEEPLINK_RADIO_BBVA                                             1




