/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#ifndef NXT_Peru_iPad_PaymentsConstants_h
#define NXT_Peru_iPad_PaymentsConstants_h

/**
 * Enumerates the different types of transfer the user can make
 */
typedef enum {
    
    PTEPaymentPSElectricServices = 0,    //!<The user is performing a public service payment to an Electric Service company
    PTEPaymentPSWaterServices,           //!<The user is performing a public service payment to a Water Service company
    PTEPaymentPSPhone,                   //!<The user is performing a public service payment to a Phone company
    PTEPaymentPSCellular,                //!<The user is performing a public service payment to a Cellular company
    PTEPaymentPSGas,                     //!<The user is performing a public service payment to a Gas company
    PTEPaymentPSCable,                   //!<The user is performing a public service payment to a Cable company
    PTEPaymentRecharge,                  //!<The user is performing a receipt payment
    PTEPaymentContOwnCard,               //!<The user is performing a continental own card payment
    PTEPaymentContThirdCard,             //!<The user is performing a continental third card payment
    PTEPaymentOtherBank,                  //!<The user is performing an other bank card payment
    PTEPaymentISService                          //!<The user is performing an internet shopping payment

} PaymentTypeEnum;

#endif
