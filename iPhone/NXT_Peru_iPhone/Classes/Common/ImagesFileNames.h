/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#pragma mark -
#pragma mark General 
#pragma mark -
#pragma mark Application header logo

/**
 * Defines the BBVA logo image file name
 */
#define BBVA_LOGO_IMAGE_FILE_NAME                                                   @"BBVALogo.png"

/**
 * Splash window background image file name
 */
#define IMG_SPLASH_WINDOW_BACKGROUND                                                @"Splash.png"

/**
 * BBVA logo image file name
 */
#define IMG_BBVA_LOGO                                                               @"BBVALogo.png"

#pragma mark -
#pragma mark Action icons

/**
 *  The gradient button blue image.
 */
#define GRADIENT_BUTTON_IMAGE_FILE                                                  @"HeaderButtonNormal.png"

/**
 * Defines the arrow icon image to navigate to the following view
 */
#define ARROW_ICON_IMAGE_FILE_NAME                                                  @"ArrowIcon.png"
#define ARROW_ICON_LEFT_IMAGE_FILE_NAME                                             @"ArrowIconLeft.png"

/**
 * Defines the less information icon to hide part of a view content
 */
#define LESS_INFORMATION_ICON_IMAGE_FILE_NAME                                       @"LessInfoIcon.png"

/**
 * Defines the more information icon to show part of a view content previously hidden
 */
#define MORE_INFORMATION_ICON_IMAGE_FILE_NAME                                       @"MoreInfoIcon.png"

/**
 * Defines the image used as arrow on the combo boxes
 */
#define COMBO_DISCLOSURE_ARROW_IMAGE_FILE_NAME                                      @"ComboDisclosureArrow.png"

/**
 * Defines the image used as left enabled arrow on the navigable browser
 */
#define BROWSER_LEFT_ENABLED_IMAGE_FILE_NAME                                        @"BrowserLeftEnabled.png"

/**
 * Defines the image used as right enabled arrow on the navigable browser
 */
#define BROWSER_RIGHT_ENABLED_IMAGE_FILE_NAME                                       @"BrowserRightEnabled.png"

/**
 * Defines the image used as left disabled arrow on the navigable browser
 */
#define BROWSER_LEFT_DISABLED_IMAGE_FILE_NAME                                       @"BrowserLeftDisabled.png"

/**
 * Defines the image used as right disabled arrow on the navigable browser
 */
#define BROWSER_RIGHT_DISABLED_IMAGE_FILE_NAME                                      @"BrowserRightDisabled.png"

#pragma mark -
#pragma mark Information icons

/**
 * Defines the cell phone icon to access mobile banking
 */
#define CELL_PHONE_ICON_IMAGE_FILE_NAME                                             @"CellPhoneIcon.png"

/**
 * Defines the locator icon to show inside the login view
 */
#define LOCATOR_ICON_IMAGE_FILE_NAME                                                @"LocatorIcon.png"

/**
 * Defines the blue icon mobile bank to show inside the login view.
 */
#define MOBILE_BANK_ICON_IMAGE_FILE_NAME                                            @"BlueIconBancaMovil.png"

/**
 * Defines the blue icon customer to show inside the login view.
 */
#define CUSTOMER_ICON_IMAGE_FILE_NAME                                               @"BlueIconCustomer.png"


/**
 * Defines the blue icon customer to show inside the login view.
 */
#define RADIO_BBVA_ICON_IMAGE_FILE_NAME                                               @"BlueIconRadioBBVA.png"


/**
 * Defines the blue icon find us to show inside the login view.
 */
#define FIND_US_ICON_IMAGE_FILE_NAME                                                @"BlueIconFindUs.png"

/**
 * Defines the blue icon alert to show in legal terms row.
 */
#define BLUE_ICON_ALERT_IMAGE_FILE_NAME                                             @"blue_icon_alert.png"

/**
 * Defines the blue icon alert to show in legal terms row.
 */
#define BLUE_ICON_ALERT_NO_BORDER_IMAGE_FILE_NAME                                   @"blue_icon_alert_no_border.png"

/**
 * Defines the blue icon alert to show in legal terms row.
 */
#define RED_ICON_ALERT_NO_BORDER_IMAGE_FILE_NAME                                    @"red_icon_alert_no_border.png"

#pragma mark -
#pragma mark Buttons backgrounds

/**
 * Defines the blue buttons background image
 */
#define BLUE_BUTTON_BACKGROUND_IMAGE_FILE_NAME                                      @"BlueButton.png"

/**
 * Defines the white buttons background image.
 */
#define WHITE_BUTTON_BACKGROUND_IMAGE_FILE_NAME                                     @"white_button.png"

/**
 * Defines the gray buttons background image
 */
#define GRAY_BUTTON_BACKGROUND_IMAGE_FILE_NAME                                      @"GrayButton.png"

/**
 * Defines the gray plain buttons background image
 */
#define GRAY_PLAIN_BUTTON_BACKGROUND_IMAGE_FILE_NAME                                @"GrayPlainButton.png"

/**
 * Defines the stocks button normal background image
 */
#define STOCK_BUTTON_NORMAL_BACKGROUND_IMAGE_FILE_NAME                              @"StockButtonNormalBackground.png"

/**
 * Defines the stocks button selected background image
 */
#define STOCK_BUTTON_SELECTED_BACKGROUND_IMAGE_FILE_NAME                            @"StockButtonSelectedBackground.png"

/**
 * Defines the orders button normal background image
 */
#define ORDER_BUTTON_NORMAL_BACKGROUND_IMAGE_FILE_NAME                              @"OrderButtonNormalBackground.png"

/**
 * Defines the orders button selected background image
 */
#define ORDER_BUTTON_SELECTED_BACKGROUND_IMAGE_FILE_NAME                            @"OrderButtonSelectedBackground.png"

/**
 * Defines the selected stock graphic type button background image
 */
#define SELECTED_STOCK_GRAPHIC_TYPE_BUTTON_BACKGROUND_IMAGE_FILE_NAME               @"StockGraphicTypeButtonBackground.png"

/**
 * Defines the add contact button background image
 */
#define ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME                               @"SelectContactsButton.png"

#define ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME                               @"add.png"

/**
 * Defines the delete contact button background image
 */
#define DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME                            @"DeselectContactsButton.png"

#pragma mark -
#pragma mark General backgrounds

/**
 * Ads background image
 */
#define AD_BLUE_BACKGROUND_IMAGE_FILE_NAME                                          @"BlueAdsBackground.png"

#define BLUE_BALLOON_IMAGE_FILE_NAME                                                @"BlueBalloon.png"
#define ALERT_BOX_IMAGE_FILE_NAME                                                   @"AlertBox.png"

/**
 * Header background image file name
 */
#define HEADER_BACKGROUND_IMAGE_FILE_NAME                                           @"HeaderBackground.png"

/**
 * Text fields background image file name
 */
#define TEXT_FIELD_BACKGROUND_IMAGE_FILE_NAME                                       @"text_field_bg.png"

/**
 * Defies the text field icon to search the institution
 */
#define TEXT_FIELD_ICON_SEARCH_WHITE                                                @"btn_search2.png"

#pragma mark -
#pragma mark Branding elements

/**
 * BBVA branding header image
 */
#define BBVA_BRANDING_HEADER_IMAGE_FILE_NAME                                        @"BBVABrandingHeader.png"

/**
 * Branding gradient image
 */
#define BRANDING_GRADIENT_IMAGE_FILE_NAME                                           @"BrandingGradient.png"

/**
 * BBVA branding header image for IOS7
 */
#define BBVA_BRANDING_HEADER_IOS7_IMAGE_FILE_NAME                                        @"IOS7-BBVABrandingHeader.png"

/**
 * Branding splash image
 */
#define BRANDING_SPLASH_IMAGE_FILE_NAME												@"Splash.png"

/**
 * Splash text image
 */
#define SPLASH_TEXT_IMAGE_FILE_NAME                                                 @"SplashTextImage.png"


#pragma mark -
#pragma mark Bevel background

/**
 * First bevel background image, used in text views
 */
#define BEVEL_BACKGROUND_1_IMAGE_FILE_NAME                                          @"BevelBackground1.png"

/**
 * Second bevel background image, used in some buttons
 */
#define BEVEL_BACKGROUND_2_IMAGE_FILE_NAME                                          @"BevelBackground2.png"


#pragma mark -
#pragma mark Division images

/**
 * Horizontal division line
 */
#define HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME                                    @"HorizontalDivisionLine.png"
#define VERTICAL_DIVISION_LINE_IMAGE_FILE_NAME                                      @"VerticalDivisionLine.png"
#define BUTTON_BAR_DIVISION_LINE_IMAGE_FILE_NAME                                    @"ButtonBarDivisionLine.png"

#pragma mark -
#pragma mark PopButtonsView images

/**
 * Defines the pop buttons view background image
 */
#define POP_BUTTONS_VIEW_BACKGROUND_IMAGE                                           @"PopButtonsBackground.png"

/**
 * Defines the previous responder button background image
 */
#define PREVIOUS_BUTTON_BACKGROUND_IMAGE_FILE                                       @"LeftBlackButton.png"

/**
 * Defines the next responder button background image
 */
#define NEXT_BUTTON_BACKGROUND_IMAGE_FILE                                           @"RightBlackButton.png"

/**
 * Defines the show available texts button background image
 */
#define SHOW_AVAILABLE_TEXTS_BUTTON_BACKGROUND_IMAGE_FILE                           @"BlackButton.png"


#pragma mark -
#pragma mark BBVATabBarViewController images

/**
 * Defines the selected icon used inside the more tab
 */
#define MORE_TAB_SELECTED_ICON                                                      @"MoreTabSelectedIcon.png"

/**
 * Defines the unselected icon used inside the more tab
 */
#define MORE_TAB_UNSELECTED_ICON                                                    @"MoreTabUnselectedIcon.png"


#pragma mark -
#pragma mark NXT tab bar button icons

/**
 * Defines the global position unselected tab bar button icon
 */
#define GLOBAL_POSITION_TAB_BAR_UNSELECTED_BUTTON_ICON_FILE_NAME                    @"main_menu_ico_miscuentas_off.png"

/**
 * Defines the transfer position unselected tab bar button icon.
 */
#define TRANSFER_POSITION_TAB_BAR_UNSELECTED_BUTTON_ICON_FILE_NAME                  @"main_menu_ico_transferencias_off.png"

/**
 * Defines the transfer position unselected tab bar button icon.
 */
#define FREQUENTS_POSITION_TAB_BAR_UNSELECTED_BUTTON_ICON_FILE_NAME                  @"main_menu_ico_frecuentes_off.png"

/**
 * Defines the poi map position unselected tab bar button icon.
 */
#define LOCALIZABLE_POSITION_TAB_BAR_UNSELECTED_BUTTON_ICON_FILE_NAME               @"main_menu_ico_localizacion_off.png"

/**
 * Defines the stocks selected tab bar button icon
 */
#define STOCKS_TAB_BAR_SELECTED_BUTTON_ICON_FILE_NAME                               @"StocksTabSelected.png"

/**
 * Defines the stocks unselected tab bar button icon
 */
#define STOCKS_TAB_BAR_UNSELECTED_BUTTON_ICON_FILE_NAME                             @"StocksTabUnselected.png"

/**
 * Defines the transfers selected tab bar button icon
 */
#define TRANSFERS_TAB_BAR_SELECTED_BUTTON_ICON_FILE_NAME                            @"TransfersTabSelected.png"

/**
 * Defines the transfers unselected tab bar button icon
 */
#define TRANSFERS_TAB_BAR_UNSELECTED_BUTTON_ICON_FILE_NAME                          @"TransfersTabUnselected.png"

/**
 * Defines the favorites selected tab bar button icon
 */
#define FAVORITES_TAB_BAR_SELECTED_BUTTON_ICON_FILE_NAME                            @"FavoritesTabSelected.png"

/**
 * Defines the favorites unselected tab bar button icon
 */
#define FAVORITES_TAB_BAR_UNSELECTED_BUTTON_ICON_FILE_NAME                          @"FavoritesTabUnselected.png"

/**
 * Defines the office search icon
 */
#define TAB_OFFICE_SEARCH_ICON_FILE_NAME                                            @"LocatorIcon.png"

/**
 * Defines the legal info icon
 */
#define FAVORITES_TAB_MORE_LEGAL_ICON_FILE_NAME										@"MoreInfo.png"

/**
 * Defines the rates icon
 */
#define FAVORITES_TAB_MORE_RATES_ICON_FILE_NAME                                     @"MoreEuro.png"


#pragma mark -
#pragma mark POI location map images

/**
 * Defines the ATM pin image file
 */
#define LOCATION_MAP_ATM_PIN_FILE_NAME                                              @"MKATMPin.png"

/**
 * Defines the branch pin image file
 */
#define LOCATION_MAP_BRANCH_PIN_FILE_NAME                                           @"MKBranchPin.png"

/**
 * Defines the annotation view callout left image
 */
#define LOCATION_MAP_CALLOUT_LEFT_IMAGE_FILE_NAME                                   @"MKCalloutLeftImage.png"

#pragma mark -
#pragma mark POIs images

/**
 * Defines the ATM POI annotation view image file name
 */
#define ATM_POI_ANNOTATION_VIEW_IMAGE_FILE_NAME                                     @"map_tooltip_escotia.png"

/**
 * Defines the branch POI annotation view image file name
 */
#define BRANCH_POI_ANNOTATION_VIEW_IMAGE_FILE_NAME                                  @"map_tooltip_bbva_branch.png"

/**
 * Defines the user location annotation view image file name
 */
#define USER_LOCATION_ANNOTATION_VIEW_IMAGE_FILE_NAME                               @"UserLocationMapIcon.png"

/**
 * Defines the route origin annotation view image file name
 */
#define ROUTE_ORIGIN_ANNOTATION_VIEW_IMAGE_FILE_NAME                                @"RouteOriginMapIcon.png"

/**
 * Defines the route destination annotation view image file name
 */
#define ROUTE_DESTINATION_ANNOTATION_VIEW_IMAGE_FILE_NAME                           @"RouteDestinationMapIcon.png"

/**
 * Defines the route step annotation view image file name
 */
#define ROUTE_STEP_ANNOTATION_VIEW_IMAGE_FILE_NAME                                  @"RouteStepMapIcon.png"

/**
 * Defines the route modal view controller background image file name
 */
#define ROUTE_MODAL_VIEW_CONTROLLER_BACKGROUND_IMAGE_FILE_NAME                      @"RouteModalViewBackground.png"

/**
 * Defines the route modal view controller button background image file name
 */
#define ROUTE_MODAL_VIEW_CONTROLLER_BUTTON_BACKGROUND_IMAGE_FILE_NAME               @"RouteModalViewBackground.png"

/**
 * The icon to filter map pois
 */
#define MAP_FILTER_IMAGE_FILE                                                       @"poi_filter_icon.png"

/**
 * The icon to filter bbva continental branch
 */
#define MAP_FILTER_BBVA_CONTINENTAL_BRACH_IMAGE_FILE                                @"map_ico_bbva_branch.png"

/**
 * The icon to filter bbva continental
 */
#define MAP_FILTER_BBVA_CONTINENTAL_IMAGE_FILE                                  	@"map_ico_bbva.png"

/**
 * The icon to filter BCP
 */
#define MAP_FILTER_BCP_IMAGE_FILE                                                   @"map_ico_bcp.png"

/**
 * The icon to filter interbank globalnet.
 */
#define MAP_FILTER_INTERBANK_GLOBALNET_IMAGE_FILE                                  	@"map_ico_interbank.png"

/**
 * The icon to filter bbva continental
 */
#define MAP_FILTER_SCOTIABANK_IMAGE_FILE                                            @"map_ico_escotia.png"

/**
 * The icon to filter express agent
 */
#define MAP_FILTER_EXPRESS_AGENT_IMAGE_FILE                                         @"map_ico_express.png"

/**
 * The icon to filter express agent plus.
 */
#define MAP_FILTER_EXPRESS_AGENT_PLUS_IMAGE_FILE                                    @"map_ico_express_plus.png"

/**
 * The icon to filter kasnet.
 */
#define MAP_FILTER_KASNET_IMAGE_FILE                                                @"map_ico_kasnet.png"

/**
 * The icon to filter multifacil
 */
#define MAP_FILTER_MULTIFACIL_IMAGE_FILE                                            @"map_ico_multiface.png"

/**
 * The icon to filter bbva continental
 */
#define MAP_TOOLTIP_BBVA_CONTINENTAL_IMAGE_FILE                                  	@"map_tooltip_bbva.png"

/**
 * The icon to filter BCP
 */
#define MAP_TOOLTIP_BCP_IMAGE_FILE                                                  @"map_tooltip_bcp.png"

/**
 * The icon to filter interbank globalnet.
 */
#define MAP_TOOLTIP_INTERBANK_GLOBALNET_IMAGE_FILE                                  @"map_tooltip_interbank.png"

/**
 * The icon to filter bbva continental
 */
#define MAP_TOOLTIP_SCOTIABANK_IMAGE_FILE                                           @"map_tooltip_escotia.png"

/**
 * The icon to filter express agent
 */
#define MAP_TOOLTIP_EXPRESS_AGENT_IMAGE_FILE                                        @"map_tooltip_express.png"

/**
 * The icon to filter express agent plus.
 */
#define MAP_TOOLTIP_EXPRESS_AGENT_PLUS_IMAGE_FILE                                   @"map_tooltip_express_plus.png"

/**
 * The icon to filter kasnet.
 */
#define MAP_TOOLTIP_KASNET_IMAGE_FILE                                               @"map_tooltip_kasnet.png"

/**
 * The icon to filter multifacil
 */
#define MAP_TOOLTIP_MULTIFACIL_IMAGE_FILE                                           @"map_tooltip_multiface.png"

/**
 * The icon to show poi list.
 */
#define MAP_POI_LIST_IMAGE_FILE                                                 	@"map_list_icon.png"

/**
 * The call image file.
 */
#define MAP_ICON_CALL_IMAGE_FILE													@"map_icon_call.png"

/**
 * The map icon car.
 */
#define MAP_ICON_CAR_IMAGE_FILE														@"map_icon_car.png"

/**
 * The map icon food.
 */
#define MAP_ICON_FOOD_IMAGE_FILE													@"map_icon_walking.png"

/**
 * The icon to location user.
 */
#define MAP_LOCATION_IMAGE_FILE                                                     @"location_arrow.png"

#pragma mark -
#pragma mark Route steps images

/**
 * Defines the next route step image file name
 */
#define NEXT_ROUTE_STEP_IMAGE_FILE_NAME                                             @"NextStep.png"

/**
 * Defines the previous route step image file name
 */
#define PREVIOUS_ROUTE_STEP_IMAGE_FILE_NAME                                         @"PreviousStep.png"

/**
 * Defines the route by car image file name
 */
#define ROUTE_BY_CAR_IMAGE_FILE_NAME                                                @"RouteByCar.png"

/**
 * Defines the route on foot image file name
 */
#define ROUTE_ON_FOOT_IMAGE_FILE_NAME                                               @"RouteOnFoot.png"


#pragma mark -
#pragma mark POI detail view controller

/**
 * Defines the map icon to display inside the POI detail header
 */
#define POI_DETAIL_MAP_ICON_FILE_NAME                                               @"POIDetailMapIcon.png"


#pragma mark -
#pragma mark POI route detail view

/**
 * Defines the POI route detail view images
 */
#define ROUTE_ON_FOOT_IMAGE                                                         @"RouteOnFoot.png"
#define ROUTE_BY_CAR_IMAGE                                                          @"RouteByCar.png"


#pragma mark -
#pragma mark Icons

#define ICON_STAR_FILE_NAME                                                         @"IcoStar.png"
#define ICON_LOCK_FILE_NAME                                                         @"IcoLock.png"
#define ICON_RENAME_FILE_NAME                                                       @"IcoRename.png"
#define ICON_FILTER_FILE_NAME                                                       @"IcoFilter.png"
#define ICON_CHART_FILE_NAME                                                        @"IcoChart.png"
#define ICON_ACCOUNT_TO_CARD_FILE_NAME                                              @"IcoAccountToCard.png"
#define ICON_CARD_TO_ACCOUNT_FILE_NAME                                              @"IcoCardToAccount.png"
#define ICON_FAVORITE_BAR_BUTTON_FILLED_FILE_NAME                                   @"FavoriteStarBarButtonIconFilled.png"
#define ICON_FAVORITE_BAR_BUTTON_EMPTY_FILE_NAME	                                @"FavoriteStarBarButtonIconEmpty.png"
#define ICON_CUSTOM_PAYMENT_FILE_NAME                                               @"IcoCustomPayment.png"
#define ICON_DUPLICATE_FILE_NAME                                                    @"IcoDuplicate.png"
#define ICON_SEND_FILE_NAME                                                         @"IcoSend.png"
#define ICON_SHARE_PAYMENT_FILE_NAME                                                @"IcoShare.png"
#define ICON_HOME_FILE_NAME                                                         @"IcoHome.png"
#define ICON_CONTACT_PHOTO_PLACEHOLDER_FILE_NAME                                    @"ContactPhotoPlaceholder.png"
#define ICON_STOCK_SELL_FILE_NAME                                                   @"IcoStockSell.png"
#define ICON_STOCK_BUY_FILE_NAME                                                    @"IcoStockBuy.png"
#define ICON_STOCK_SHARE_FILE_NAME                                                  @"IcoStockShare.png"
#define ICON_STOCK_CHART_FILE_NAME                                                  @"IcoStockChart.png"
#define ICON_TO_MY_ACCOUNTS_FILE_NAME                                               @"IcoToMyAccounts.png"
#define ICON_TO_MY_ACCOUNTS_SELECTED_FILE_NAME                                      @"IcoToMyAccountsSelected.png"
#define ICON_TO_ANOTHER_ACCOUNT_FILE_NAME                                           @"IcoToAnotherAccount.png"
#define ICON_TO_CARD_FILE_NAME                                                      @"IcoToCard.png"
#define ICON_TO_CARD_SELECTED_FILE_NAME                                             @"IcoToCardSelected.png"
#define ICON_TO_EMAIL_MOBILE_FILE_NAME                                              @"IcoToEmailMobile.png"
#define ICON_HELP_FILE_NAME                                                         @"IcoHelp.png"
#define ICON_PLAY_VIDEO_FILE_NAME                                                   @"PlayVideoIcon.png"
#define ICON_MENU_HELP_FILE_NAME                                                    @"HelpIcon.png"
#define ICON_RECHARGE_CARD_FILE_NAME                                                @"card-menu-icon-recharge-card.png"
#define ICON_RECHARGE_MOBILE_NAME                                                   @"iconRechargeMobile.png"
#define TAB_TO_MY_ACCOUNTS_FILE_NAME                                                @"TabToMyAccounts.png"
#define TAB_TO_CARD_FILE_NAME                                                       @"TabToCard.png"
#define TAB_PAYMENT_SELETED_FILE_NAME                                               @"PaymentTabIconSelected.png"
#define TAB_PAYMENT_UNSELETED_FILE_NAME                                             @"PaymentTabIconUnselected.png"

/**
 * Personal area icon file name
 */
#define ICON_PERSONAL_AREA_FILE_NAME                                                @"IconPersonalArea.png"
#define ICON_ACTIVE_PHONE_FILE_NAME                                                 @"IconToPhone.png"
#define ICON_ACTIVE_MAIL_FILE_NAME                                                  @"IconToMail.png"

#pragma mark -
#pragma mark AccountTransactionsListViewController images

#define BOTTOM_BUTTON_BAR_BACKGROUND_IMAGE_FILE_NAME                                @"ButtonBarBackground.png"
#define BLUE_CELL_BACKGROUND                                                        @"blue_cell_background.png"

#pragma mark -
#pragma mark TransactionsFilter strings

#define TRANSACTION_FILTER_LAST_DAYS_LEFT_BG_FILE_NAME                              @"TransactionsFilterBoxLeft.png"
#define TRANSACTION_FILTER_LAST_DAYS_MIDDLE_BG_FILE_NAME                            @"TransactionsFilterBoxMiddle.png"
#define TRANSACTION_FILTER_LAST_DAYS_RIGHT_BG_FILE_NAME                             @"TransactionsFilterBoxRight.png"

#pragma mark -
#pragma mark Custom payments

#define INSTALLMENTS_BALL_IMAGE_FILE_NAME                                           @"InstallmentsSliderBall.png"

#define STOCK_CHART_FAKE_IMAGE_FILE_NAME                                            @"StockFakeChart.png"

#define STOCK_GRAPHIC_TOP_SHADOW_IMAGE_FILE_NAME                                    @"StockGraphicShadow.png"

#pragma mark -
#pragma mark Help images

#define HELP_IMAGE_1_FILE_NAME                                                      @"HelpImage1.png"
#define HELP_IMAGE_2_FILE_NAME                                                      @"HelpImage2.png"
#define HELP_IMAGE_3_FILE_NAME                                                      @"HelpImage3.png"

#define BLUE_CELL_BACKGROUND_IMAGE_FILENAME                                         @"blue_cell_background.png"

#define ICON_QR_FILE_NAME                                                           @"qrcode.png"

#pragma mark -
#pragma mark Login icons.

#define CALL_CUSTOMER_IMAGE_FILEAME                                                 @"CallCustomer.png"

#define REMOTE_MANAGER_IMAGE_FILEAME                                                @"CallCustomer.png"

#pragma mark -
#pragma mark Transfer icons

#define TRANSFER_BETWEEN_ACCOUNTS_IMAGE_FILE_NAME									@"IconToMyAccounts.png"
#define TRANSFER_TO_OTHERS_IMAGE_FILE_NAME											@"IconToAnotherAccount.png"
#define TRANSFER_WITH_CASH_MOBILE_FILE_NAME                                         @"IconWithCashMobile.png"
#define CONSULT_CASH_MOBILE_FILE_NAME                                               @"IconConsultCashMobile.png"
#define TRANSFER_DONE_IMAGE_FILE_NAME                                               @"transfertick_background.png"
#define TRANSFER_TICK_IMAGE_FILE_NAME                                               @"transfer_tick.png"

#pragma mark -
#pragma mark Payment icons

#define PAYMENT_LIST_ICON_IMAGE_FILE_NAME                                           @"IconPayment.png"

#pragma mark -
#pragma mark Radio buttons

#define IMG_SELECTED_RADIO                                                          @"radio_selected.png"
#define IMG_UNSELECTED_RADIO                                                        @"radio_unselected.png"


#define IMG_NAVIGATION_BAR_GRAY                                                     @""


#pragma mark -
#pragma mark BBVA Radio image assests


#define IMG_SPLASH_RADIO_LARGE_WINDOW_BACKGROUND                                                @"Background_radio-568h.png"

/**
 * Splash window background image file name
 */
#define IMG_SPLASH_RADIO_WINDOW_BACKGROUND                                                @"Background_radio.png"

/**
 * BBVA logo image file name
 */
#define IMG_BBVA_RADIO_LOGO                                                               @"logo_radio_bbva_color.png"
/**
 * BBVA logo image file name
 */
#define IMG_BBVA_RADIO_LOGO_ART                                                               @"logo_radio_bbva_color_art.png"


/**
 * BBVA logo image file name
 */
#define IMG_BBVA_RADIO_WHITE_LOGO                                                               @"logo_radio_bbva_white.png"


/**
 * Defines the blue icon customer to show inside the login view.
 */
#define RADIO_PLAYER_ICON_IMAGE_FILE_NAME                                               @"ico_radio_menu_play.png"

/**
 * Defines the blue icon customer to show inside the login view.
 */
#define RADIO_BE_PART_ICON_IMAGE_FILE_NAME                                               @"ico_radio_menu_bepart.png"
/**
 * Defines the blue icon customer to show inside the login view.
 */
#define RADIO_TERMS_ICON_IMAGE_FILE_NAME                                               @"ico_radio_menu_terms.png"
/**
 * Defines the blue icon customer to show inside the login view.
 */
#define RADIO_BACK_MAIN_ICON_IMAGE_FILE_NAME                                               @"ico_radio_menu_exit.png"

#define RADIO_RIGHT_BUTTON_ICON_IMAGE_FILE_NAME                                               @"ico_radio_drawer.png"

#define RADIO_BIG_PLAY_BUTTON_IMAGE_FILE_NAME                                               @"ico_big_on_player.png"

#define RADIO_BIG_STOP_BUTTON_IMAGE_FILE_NAME                                               @"ico_big_off_player.png"


#define RADIO_PLAY_BUTTON_IMAGE_FILE_NAME                                               @"ico_on_player.png"

#define RADIO_STOP_BUTTON_IMAGE_FILE_NAME                                               @"ico_off_player.png"

#define RADIO_GO_PLAYER_BUTTON_IMAGE_FILE_NAME                                               @"ico_up_player.png"


