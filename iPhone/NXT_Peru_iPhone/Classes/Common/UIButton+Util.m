/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "UIButton+Util.h"

#pragma mark -

@implementation UIButton(Util)

#pragma mark -
#pragma mark Utility selectors

/*
 * Creates an autoreleased button with an icon.<br>
 * The size of the returned button is the size of the background image.
 */
+ (UIButton *)buttonWithIcon:(UIImage *)icon background:(UIImage *)background {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:background forState:UIControlStateNormal];
    [button setImage:icon forState:UIControlStateNormal];
    button.frame = CGRectMake(0.0f, 0.0f, background.size.width, background.size.height );
    
    return button;
    
}

@end
