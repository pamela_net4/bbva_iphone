/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>


#define DATE_IN_FORMAT                              @"dd/MM/yy"
#define DATE_HOUR_IN_FORMAT                         @"yyyyMMdd HH:mm:ss"
#define DATE_OUT_FORMAT                             @"dd/MM/yy"
#define DATE_HOUR_OUT_FORMAT                        @"dd/MM/yy HH:mm:ss"
#define DATE_OUT_YYYY_FORMAT                        @"dd/MM/yyyy"
#define DATE_WITH_DAY_OF_WEEK_FORMAT                @"EEEE dd/MM/yy"
#define DATE_SMALL_WITH_DAY_OF_WEEK_FORMAT          @"EEE dd/MM/yy"
#define DATE_TRANSACTION_FORMAT                     DATE_WITH_DAY_OF_WEEK_FORMAT


@class TransactionsFilterObject;
@class BankAccount;

/**
 * Class to provide static utility functions
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Tools : NSObject {

}

/**
 * Returns the bundle version
 *
 * @return The application version
 */
+ (NSString *)bundleVersion;

/**
 * Returns the number formatter used for number conversion from strings.
 *
 * @return The number formatter.
 */
+ (NSNumberFormatter *)serverNumberFormatter;

/**
 * Formats an amount, including the currency symbol provided
 *
 * @param anAmount The decimal number representing the amount to format
 * @param aCurrency The currency symbol
 * @param aCurrencyPreceder YES when currencty precedes the amount (i.e $4.34), NO otherwise (i.e. 53,32EUR)
 * @param aDecimalCount The number of decimal positions to show
 * @return The formated amount
 */
+ (NSString *)formatAmount:(NSDecimalNumber *)anAmount withCurrency:(NSString *)aCurrency currenctyPrecedesAmount:(BOOL)aCurrencyPreceder
              decimalCount:(NSInteger)aDecimalCount;

/**
 * Formats an amount
 *
 * @param anAmount The decimal number representing the amount to format
 * @param aDecimalCount The number of decimal positions to show
 * @param plusSign YES to force the plus sign in the result, NO otherwise
 * @return The formated amount
 */
+ (NSString *)formatAmount:(NSDecimalNumber *)anAmount
          withDecimalCount:(NSInteger)aDecimalCount
                  plusSign:(BOOL)plusSign;

/**
 * Formats an amount
 *
 * @param anAmount The decimal number representing the amount to format
 * @return The formated amount
 */
+ (NSString *)formatAmount:(NSDecimalNumber *)anAmount;

/**
 * Formats an amount, including the currency symbol provided
 *
 * @param anAmount The decimal number representing the amount to format
 * @param aCurrency The currency symbol
 * @return The formated amount
 */
+ (NSString *)formatAmount:(NSDecimalNumber *)anAmount withCurrency:(NSString *)aCurrency;

/**
 * Formats an amount, including the currency symbol provided and a + symbol if necessary
 *
 * @param anAmount The decimal number representing the amount to format
 * @param aCurrency The currency symbol
 * @param decimalCount number of decimals to be showed
 * @param flag to indicate if is necessary add a + symbol before the number
 * @return The formated amount
 */
+ (NSString *)formatAmount:(NSDecimalNumber *)anAmount withCurrency:(NSString *)aCurrency decimalCount:(NSInteger)decimalCount andPlusSymbol:(BOOL)flag;

/**
 * Extracts a decimal number from the given server string
 *
 * @param aString The server string containing the decimal number
 * @return The decimal number extracted from the string
 */
+ (NSDecimalNumber *)decimalFromServerString:(NSString *)aString;

/**
 * Extracts a date from a server string. The date time is set to 00:00 (midnight) and the time zone is UTC
 *
 * @param aString The server date string to convert
 * @return The date extracted from the string
 */
+ (NSDate *)dateFromServerString:(NSString *)aString;

/**
 * Converts a decimal number into a CGFloat. When decimal number is not a number, zero is assumed
 *
 * @param aDecimalNumber The decimal number to convert
 * @return The CGFloat that is approximately the decimal number
 */
+ (CGFloat)convertDecimalNumber:(NSDecimalNumber *)aDecimalNumber;

/**
 * Obfuscates an account number
 *
 * @param anAccountNumber The account number to obfuscate
 * @return The obfuscated account number
 */
+ (NSString *)obfuscateAccountNumber:(NSString *)anAccountNumber;

/**
 * Obfuscates a card number
 *
 * @param aCardNumber The card number to obfuscate
 * @return The obfuscated card number
 */
+ (NSString *)obfuscateCardNumber:(NSString *)aCardNumber;


+(NSString*)formatCardNumber:(NSString*)aCardNumber;
/**
 * Obfuscates a username
 *
 * @param aUserName The user name to obfuscate
 * @return The obfuscated user name
 */
+ (NSString *)obfuscateUserName:(NSString *)aUserName;

/**
 * Obfuscates a username
 *
 * @param aUserName The user name to obfuscate
 * @return The obfuscated user name large
 */
+ (NSString *)obfuscateUserNameLarge:(NSString *)aUserName;

/**
 * Shows an error alert view
 *
 * @param anErrorMessage The error alert view message
 */
+ (void)showErrorWithMessage:(NSString *)anErrorMessage;

/**
 * Shows an info alert view
 *
 * @param aInfoMessage The infor alert view message
 */
+ (void)showInfoWithMessage:(NSString *)aInfoMessage;

/**
 * Shows a message to the user inside an alert view, using a given title
 *
 * @param aMessage The alert view message
 * @param aTitle The alert view title
 */
+ (void)showAlertWithMessage:(NSString *)aMessage title:(NSString *)aTitle;

/**
 * Enables or disables a table scrolling depending on whether its content height is greater than its own height
 *
 * @param aTableView The table view to check
 */
+ (void)checkTableScrolling:(UITableView *)aTableView;

/**
 * Returns the nearest value with only one digit which is not zero. For example, a 24 input returns 20, a 25 input returns 30, a 0.043 input returns 0.04 and a 0.045 input returns 0.05.
 * It assumes the input value is positive. When input is zero or negative, the value returned is zero
 *
 * @param anInput The input value to round
 * @return The number that results from the rounding, or zero if input is zero or negative
 */
+ (double)roundToOneNonZeroDigit:(double)anInput;


/**
 * Returns a string from a given NSDate in current locale time zone with the specified format
 *
 * @param date The date to translate
 * @param dateFormat The date format to use in the translation
 * @return The date translated into a string using the given format
 */
+ (NSString *)stringFromCurrentLocaleDate:(NSDate *)date
                               withFormat:(NSString *)dateFormat;

/**
 * Returns a string from a given NSDate in UTC time zone with the specified format
 *
 * @param date The date to translate
 * @param dateFormat The date format to use in the translation
 * @return The date translated into a string using the given format
 */
+ (NSString *)stringFromUTCDate:(NSDate *)date
                     withFormat:(NSString *)dateFormat;


/**
 * Returns a string with hour from a given NSDate
 *
 * @param aDate to convert
 */
+ (NSString *)stringWithHourWithCurrentLocaleFromDate: (NSDate *) aDate;

/**
 * Returns the year of the given date as a string
 *
 * @param aDate to get the year
 */
+ (NSString *)yearFromDate: (NSDate *) aDate;

/**
 * Returns the month of the given date as a string
 *
 * @param aDate to get the month
 */
+ (NSString *)monthFromDate: (NSDate *) aDate;

/**
 * Returns the last day of the month of the given date
 *
 * @param aDate to get the last day
 */
+ (NSString *)lastDayOfDate:(NSDate *)aDate;

/**
 * Returns a date with hours, minutes and seconds to 00
 *
 * @param date to change
 */
+ (NSDate*)getDateWithTimeToZero:(NSDate *)date;

/**
 * Returns a string with hour from a given NSDate
 *
 * @param date The date to convert
 * @return The string with the hour from the provided NSDate
 */
+ (NSString *)getStringWithHourWithCurrentLocaleFromDate:(NSDate *)date;

/**
 * Returns a NSDate from a given string
 *
 * @param aString The string to convert
 */
+ (NSDate*)dateFromString: (NSString*) aString;

/**
 * Returns a date with hours, minutes and seconds set to 00
 *
 * @param aDate The date to change
 * @return The input date with the time section set to zero
 */
+ (NSDate *)dateWithTimeToZero:(NSDate *)aDate;

/**
 * Converts the server currency symbol into a client currency symbol. When no translation exists, the provided currency is returned
 *
 * @param aServerCurrencySymbol The server currency symbol to translate
 * @return The client currency symbol, or the same server currency symbol when no translation exits
 */
+ (NSString *)translateServerCurrencySymbolIntoClientCurrencySymbol:(NSString *)aServerCurrencySymbol;

/**
 * Converts the server currency symbol into a client currency symbol. When no translation exists, nil is returned
 *
 * @param aServerCurrencySymbol The server currency symbol to translate
 * @return The client currency symbol, or nil when no translation exits
 */
+ (NSString *)clientCurrencySymbolForServerCurrency:(NSString *)aServerCurrencySymbol;
	
/**
 * Converts username into a formatted username
 *
 * @param aUsername The username
 * @return The formatted username
 */
+ (NSString *)formatUsername:(NSString *)aUsername;

/**
 * Removes the not numeric characters from a string.
 *
 * @param string The string to format.
 * @return A new string where the not numeric characters has been removed.
 */
+ (NSString *)stringByRemovingNotNumericCharacters:(NSString *)string;

/**
 * Extracts a mobile number like string from annother string. To to this, not numeric characters are removed from the string, and it is set to as much as 9 characters
 * from the last characters (that is, initial characters will be removed until a leght equal or smaller that 9 is achieved).
 *
 * @param string The string to format.
 * @return The mobile number like string.
 */
+ (NSString *)mobileNumberLikeFromString:(NSString *)string;

/**
 * Checks whether a string is a valid mobile phone number. The string must begin with a 9 digit, has only digits, and a length of 9.
 *
 * @param string The string to check.
 * @return YES when the string is a valid mobile phone number, NO otherwise.
 */
+ (BOOL)isValidMobilePhoneNumberString:(NSString *)string;


+ (BOOL)isValidEmail:(NSString*)email;

/**
 * Return the currency literal
 *
 * @param aCurrency is the received currency from the server
 * @return the literal currency
 */
+(NSString *)getCurrencyLiteral:(NSString *)aCurrency;

/**
 * Return the currency server
 *
 * @param aCurrency is the received literal from the server
 * @return the literal currency
 */
+(NSString *)getCurrencyServer:(NSString *)aCurrency;

/**
 * Return the currency literal
 *
 * @param aCurrency is the received currency from the server
 * @return the literal currency
 */
+(NSString *)getCurrencySimbol:(NSString *)aCurrency;

/**
 * Returns the client application main currency literal (EUR)
 *
 * @return The client application main currency literal
 */
+ (NSString *)mainCurrencyLiteral;

/**
 * Returns the client application main currency symbol (€)
 *
 * @return The client application main currency symbol
 */
+ (NSString *)mainCurrencySymbol;

/**
 * Sets a given text to a label, aligning the text vertically to the label top. Resizes the label to fit the text in height
 *
 * @param aText The text to set
 * @param aLabel The label to show the text
 * @param aMaxLines Maximum number of lines for the label
 */
+ (void)setText:(NSString *)aText toLabel:(UILabel *)aLabel withMaxLines:(NSInteger)aMaxLines;

/**
 * Returns the distance as a string
 *
 * @param aDistance The distance to convert into a string
 * @return The distance converted into a string
 */
+ (NSString *)distanceAsString:(NSInteger)aDistance;

/**
 * Returns the time as a string
 * 
 * @param aTime The time (measured in seconds) to convert into a string
 * @return The time converted into a string
 */
+ (NSString *)timeAsString:(NSInteger)aTime;

/**
 * Get the UID of the device
 *
 * @return The UID string
 */
+ (NSString*) getUID;

/**
 * Calculates the given data MD5 digest and returns it as an hexadecimal string capitalized
 *
 * @param aData The data to calculate its MD5
 * @return The MD5 digest as hexadecimal string
 */
+ (NSString*) getMD5StringFromData: (NSData*) aData;

/**
 * Ciphers the provided string
 *
 * @param aString The string to cipher
 * @return The ciphered data
 */
+ (NSString *)cipherString:(NSString *)aString;

/**
 * Deciphers the provided data into a string
 *
 * @param aString The data to decipher
 * @return The deciphered string
 */
+ (NSString *)decipherString:(NSString *)aString;


/**
 * Returns the total frame of an UITableView.
 *
 * @param tableView The table to obtain is frame.
 * @return Total frame of the table.
 */
+(CGRect)tableFrame:(UITableView *)tableView;

/*
 * Sets a given text to a label, aligning the text vertically to the label top.
 * Resizes the label to fit the text in height
 */
+ (void)setText:(NSString *)text
        toLabel:(UILabel *)aLabel;

/**
 * Checks whether the provided directory is created. It creates the directory when it is not found
 *
 * @param aDirectoryPath The directory path to check
 */
+ (void)checkAndCreateDirectory:(NSString *)aDirectoryPath;

/**
 * Creates a random file name not used inside a given directory, creates an empy file with that name inside the directory
 * and returns th file name (not including the path)
 *
 * @param aDirectory The directory to contain the file
 * @return The new file name (not including the path)
 */
+ (NSString *)findAndCreateNotUsedFileNameInsideDirectory:(NSString *)aDirectory;

/**
 * Compares two currencies
 *
 * @param firstCurrency
 * @param secondCurrency
 * @return YES if both are equal
 */
+ (BOOL)currency:(NSString *)firstCurrency isEqualToCurrency:(NSString *)secondCurrency;

#pragma mark -
#pragma mark Label utilities

/**
 * Calculates the label height to fit the given text. It takes into account the label font and the number of lines
 * Resizes the label to fit the text in height
 *
 * @param aLabel The label to show the text
 * @param aText The text to set
 * @return The label height to fit the given text
 */
+ (CGFloat)labelHeight:(UILabel *)aLabel
               forText:(NSString *)aText;

/**
 * Adjusts a label width to fit the text displayed. Label number of lines must be 1
 *
 * @param aLabel The label to fit
 */
+ (void)fitLabelWidth:(UILabel *)aLabel;

/**
 * Filters the given array with the given filter object and returns an autoreleased array with the filtering
 *
 * @param transactionsArray to filter over
 * @param filterObject to filter with
 *
 * @return the array filtered
 */
+ (NSMutableArray *)filterTrasactions:(NSArray *)transactionsArray withFilter:(TransactionsFilterObject *)filterObject;

/**
 * Converts to NSData with the decoded data obtained from a base 64 string.
 *
 * @param aBase64Data NSString containing the original base64 data to decode
 * @param aData NSMutableData containing the target base64
 */ 
+ (void)convertFromBase64Text:(NSString *)aBase64Data toData:(NSMutableData *)aData;

/**
 * Returns a NSData from the given base64 string
 *
 * @param string The string encoded
 * @return The data decoded
 */
+ (NSData *)base64DataFromString:(NSString *)string;

/**
 * Indicates the text is valid for a character set
 *
 * @param text the input text to validate
 * @param characterSet the valid characters
 */
+ (BOOL)isValidText:(NSString *)text forCharacterSet:(NSCharacterSet *)characterSet;

/**
 * Indicates the text is valid for a character string
 *
 * @param text the input text to validate
 * @param characterString the valid characters
 */
+ (BOOL)isValidText:(NSString *)text forCharacterString:(NSString *)characterString;

/**
 * Indicates the account is valid for transfers
 *
 * @param account the account
 */
+ (BOOL)isValidAccountForTransfers:(BankAccount *)account;

/**
 * Returns an NSString not nil equivalent to the provided string. When the input string is nil, "" is returned
 *
 * @param string The string to convert
 * @return An empty string when input string is nil, the input string otherwise
 */
+ (NSString *)notNilString:(NSString *)string;

/**
 * Returns an NSSring not nil equivalent by default to the provided string. When the input string is nil, default is returned.
 *
 * @param string: The string to convert.
 * @param defaultString: The default string if string param is nil.
 * @return An empty string when input string is nil, the input string otherwise
 */
+ (NSString *)notNilString:(NSString *)string byDefaylt:(NSString *)defaultString;

/**
 * Returns an NSSring Capitalizing just the first word.  When the input string is nil, default is returned.
 *
 * @param string The string to convert
 * @return The defaultString when is empty or nil, the string with the first word capitalized
 */
+ (NSString *)capitalizeFirstWordOnly:(NSString *)defaultString;

/**
 * Shows a message to the user inside an alert view, using a given title
 *
 * @param aMessage The alert view message
 * @param aTitle The alert view title
 * @param aDelegate The alert view delegate to receive the notifications
 */
+ (void)showAlertWithMessage:(NSString *)aMessage title:(NSString *)aTitle andDelegate:(id)aDelegate;

/**
 * Shows an error alert view
 *
 * @param anErrorMessage The error alert view message
 * @param aDelegate The alert view delegate to receive the notifications
 */
+ (void)showErrorWithMessage:(NSString *)anErrorMessage andDelegate:(id)aDelegate;

/**
 * Shows an alert alert view
 *
 * @param aInfoMessage The alert alert view message
 */
+ (void)showAlertWithMessage:(NSString *)aInfoMessage;

/**
 * Shows a message to the user inside an alert view, using a given title
 *
 * @param aMessage The alert view message
 * @param aTitle The alert view title
 */
//+ (void)showAlertWithMessage:(NSString *)aMessage title:(NSString *)aTitle;

/**
 * Shows a confirmation dialog with the geiven message and the given delegate to manage response
 *
 * @param aMessage The alert view message
 * @param aDelegate Delegate of the UIAlertViewDelegate
 */
+ (void)showConfirmationDialogWithMessage:(NSString *)aMessage andDelegate:(id)aDelegate;

/**
 * Starts a phone call to a given number
 *
 * @param aPhoneNumber The phone number to call to
 */
+ (void)startPhoneCallToNumber:(NSString *)aPhoneNumber;

/**
 * Returns an amount formated with comma as decimal separator with the dot as decimal separator
 *
 * @param anAmount The string representing the amount to format
 * @return The formated amount
 */
+ (NSString *)formatAmountWithDotDecimalSeparator:(NSString *)anAmount;

/**
 * Returns a string from a given NSDate
 *
 * @param aDate to convert
 */
+ (NSString *)getStringFromDate: (NSDate *) aDate;

/**
 * Returns the current date and time but in UTC. That means that date/time 2011/01/01 12:00:00 UTC+2 will be converted to 2011/01/01 12:00:00 UTC+0 (not 2011/01/01 10:00:00 UTC+0) and
 * 2011/01/01 12:00:00 UTC-5 will be converted to 2011/01/01 12:00:00 UTC+0 (not (2011/01/01 17:00:00 UTC+0)
 *
 * @return The current date and time transformed into UTC
 */
+ (NSDate *)now;

/**
 * Provides access to the amount to local string formatter.
 *
 * @return The amount to loval string formatter.
 */
+ (NSNumberFormatter *)amountToLocalStringFormatter;

/**
 * Returns a decimal number from a local string.
 *
 * @param string The local string to translate into a decimal number.
 * @return The decimal number representing the local string, or zero when it cannot be translated.
 */
+ (NSDecimalNumber *)decimalNumberFromLocalString:(NSString *)string;

@end
