/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "DownloadFileCache.h"
#import "Tools.h"


/**
 * Defines the number of elements stored in the NSMutableArray containing the cache information associated with a key string
 */
#define CACHE_INFORMATION_COUNT                                                     4

/**
 * Defines the information cached download time position inside the associated NMutableSArray
 */
#define CACHE_DOWNLOAD_TIME_POSITION                                                0

/**
 * Defines the cached file last access time position inside the associated NMutableSArray
 */
#define CACHE_FILE_LAST_ACCESS_TIME_POSITION                                        1

/**
 * Defines the cached file accesses count position inside the associated NMutableSArray
 */
#define CACHE_FILE_ACCESSES_COUNT_POSITION                                          2

/**
 * Defines the cached file name position inside the associated NSMutableArray
 */
#define CACHE_FILE_NAME_POSITION                                                    3


#pragma mark -

/**
 * Contains the information needed to perform the cache policy and provides selectors to order the
 * cache information in an array in order to remove the worst qualified cached files appliying the
 * the cache policy
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CachePolicyInformation : NSObject {
    
@private
    
    /**
     * Download time
     */
    NSTimeInterval downloadTime_;
    
    /**
     * Last access time
     */
    NSTimeInterval lastAccessTime_;
    
    /**
     * Accesses count
     */
    NSUInteger accessesCount_;
    
    /**
     * Maximum time the downloaded file can be cached, measured in seconds. When maximum time is set to zero, the downloaded file never expires
     */
    NSTimeInterval expirationTime_;
    
    /**
     * Cache file path
     */
    NSString *cacheFilePath_;
    
    /**
     * Cached file size
     */
    NSUInteger cacheFileSize_;
    
    /**
     * Cache dictionary key
     */
    NSString *key_;
    
    /**
     * Cache dictionary where information is stored
     */
    NSMutableDictionary *cacheDictionary_;
    
}

/**
 * Provides read-only access to the download time
 */
@property (nonatomic, readonly, assign) NSTimeInterval downloadTime;

/**
 * Provides read-only access to the last access time
 */
@property (nonatomic, readonly, assign) NSTimeInterval lastAccessTime;

/**
 * Provides read-only access to the accesses count
 */
@property (nonatomic, readonly, assign) NSUInteger accessesCount;

/**
 * Provides read-only access to the maximum time the downloaded file can be cached, measured in seconds
 */
@property (nonatomic, readonly, assign) NSTimeInterval expirationTime;

/**
 * Provides read-only access to the cache file path
 */
@property (nonatomic, readonly, copy) NSString *cacheFilePath;

/**
 * Provides read-only access to the cached file size
 */
@property (nonatomic, readonly, assign) NSUInteger cacheFileSize;

/**
 * Provides read-only access to the cache dictionary key
 */
@property (nonatomic, readonly, copy) NSString *key;

/**
 * Provides read-only access to the cache dictionary where information is stored
 */
@property (nonatomic, readonly, retain) NSMutableDictionary *cacheDictionary;


/**
 * Creates and returns an autorelease CachePolicyInformation instance initialized with the provided information
 *
 * @param aDownloadTime The download time to store
 * @param aLastAccessTime The last access time to store
 * @param anAccessesCount The accesses count to store
 * @param anExpirationTime The maximum time the downloaded file can be cached. Zero to avoid file expiration
 * @param aCacheFilePath The cache file path to store
 * @param aCacheFileSize The cache file size to store
 * @param aCacheDictionaryKey The cache dictionary key to store
 * @param aCacheDictionary The cache dictionary to store
 * @return The autoreleased CachePolicyInformation instance
 */
+ (id)cachePolicyInformationWithDownloadTime:(NSTimeInterval)aDownloadTime
                              lastAccessTime:(NSTimeInterval)aLastAccessTime
                               accessesCount:(NSUInteger)anAccessesCount
                              expirationTime:(NSTimeInterval)anExpirationTime
                               cacheFilePath:(NSString *)aCacheFilePath
                               cacheFileSize:(NSUInteger)aCacheFileSize
                          cacheDictionaryKey:(NSString *)aCacheDictionaryKey
                             cacheDictionary:(NSMutableDictionary *)aCacheDictionary;

/**
 * Compares a CachePolicyInformation instance with the receiver. Two CachePolicyInformation instances younger than half
 * the caching time are ordered by their last access time. Two CachePolicyInformation instances older than half the caching
 * time are ordered by the accesses frecuency (instance age divided by the number of accesses). A CachePolicyInformation
 * instance younger than half the caching time is always better ranked than a CachePolicyInformation instance older than half
 * the caching time. In case both instances are equal, the greatest file size is better ranked
 *
 * @param aCachePolicyInformation The CachePolicyInformation instance to compare with
 * @return NSOrderedAscending if receiver precedes aCachePolicyInformation in cache policy,
 * NSOrderedDescending if it follows aCachePolicyInformation, and NSOrderedSame otherwise
 */
- (NSComparisonResult)cachePolicyInformationCompare:(CachePolicyInformation *)aCachePolicyInformation;

@end


#pragma mark -

/**
 * DownloadFileCache private category
 */
@interface DownloadFileCache(private)

/**
 * Returns the cache directory path
 *
 * @return The cache directory path
 * @private
 */
- (NSString *)cacheDirectoryPath;

/**
 * Returns the cache configuration file path
 *
 * @return The cache configuration file path
 * @private
 */
- (NSString *)cacheConfigurationFilePath;

/**
 * Checks whether the cache directory is created. It creates the directory when it is not found
 *
 * @private
 */
- (void)checkAndCreateDirectory;

/**
 * Removes expired and orphan cache files from directory
 *
 * @private
 */
- (void)removeExpiredAndOrphanCacheFiles;

@end


#pragma mark -

@implementation CachePolicyInformation

#pragma mark -
#pragma mark Proterties

@synthesize downloadTime = downloadTime_;
@synthesize lastAccessTime = lastAccessTime_;
@synthesize accessesCount = accessesCount_;
@synthesize expirationTime = expirationTime_;
@synthesize cacheFilePath = cacheFilePath_;
@synthesize cacheFileSize = cacheFileSize_;
@synthesize key = key_;
@synthesize cacheDictionary = cacheDictionary_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [cacheFilePath_ release];
    cacheFilePath_ = nil;
    
    [key_ release];
    key_ = nil;
    
    [cacheDictionary_ release];
    cacheDictionary_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autorelease CachePolicyInformation instance initialized with the provided information
 */
+ (id)cachePolicyInformationWithDownloadTime:(NSTimeInterval)aDownloadTime
                              lastAccessTime:(NSTimeInterval)aLastAccessTime
                               accessesCount:(NSUInteger)anAccessesCount
                              expirationTime:(NSTimeInterval)anExpirationTime
                               cacheFilePath:(NSString *)aCacheFilePath
                               cacheFileSize:(NSUInteger)aCacheFileSize
                          cacheDictionaryKey:(NSString *)aCacheDictionaryKey
                             cacheDictionary:(NSMutableDictionary *)aCacheDictionary {
    
    CachePolicyInformation *result = [[[CachePolicyInformation alloc] init] autorelease];
	
	if (result != nil) {

		NSZone *zone = [result zone];
		
		result->downloadTime_ = aDownloadTime;
		result->lastAccessTime_ = aLastAccessTime;
		result->accessesCount_ = anAccessesCount;
		result->expirationTime_ = anExpirationTime;
		result->cacheFilePath_ = [aCacheFilePath copyWithZone:zone];
		result->cacheFileSize_ = aCacheFileSize;
		result->key_ = [aCacheDictionaryKey copyWithZone:zone];
		result->cacheDictionary_ = [aCacheDictionary copyWithZone:zone];
    
	}
	
    return result;
    
}

#pragma mark -
#pragma mark Cache policy

/**
 * Compares a CachePolicyInformation instance with the receiver. Two CachePolicyInformation instances younger than half
 * the caching time are ordered by their last access time. Two CachePolicyInformation instances older than half the caching
 * time are ordered by the accesses frecuency (instance age divided by the number of accesses). A CachePolicyInformation
 * instance younger than half the caching time is always better ranked than a CachePolicyInformation instance older than half
 * the caching time. In case both instances are equal, the greatest file size is better ranked
 */
- (NSComparisonResult)cachePolicyInformationCompare:(CachePolicyInformation *)aCachePolicyInformation {
    
    NSComparisonResult result = NSOrderedSame;
    
    NSTimeInterval curTime = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval halfAge = curTime - (expirationTime_ / 2.0f);
    BOOL curYoungerHalfAge = (downloadTime_ > halfAge);
    halfAge = curTime - (aCachePolicyInformation.expirationTime / 2.0f);
    BOOL otherYoungerHalfAge = (aCachePolicyInformation.downloadTime > halfAge);
    
    if ((curYoungerHalfAge) && (!otherYoungerHalfAge)) {
        
        result = NSOrderedDescending;
        
    } else if ((!curYoungerHalfAge) && (otherYoungerHalfAge)) {
        
        result = NSOrderedAscending;
        
    } else if ((curYoungerHalfAge) && (otherYoungerHalfAge)) {
        
        NSTimeInterval accessDifference = aCachePolicyInformation.lastAccessTime - lastAccessTime_;
        
        if (accessDifference > 0.0f) {
            
            result = NSOrderedDescending;
            
        } else if (accessDifference < 0.0f) {
            
            result = NSOrderedAscending;
            
        }
        
    } else {
        
        double curPeriod = 0.0f;
        
        if (accessesCount_ > 0) {
            
            NSTimeInterval curAge = curTime - downloadTime_;
            curPeriod = fabs(curAge / (double)accessesCount_);
            
        }
        
        double otherPeriod = 0.0f;
        
        if (aCachePolicyInformation.accessesCount > 0) {
            
            NSTimeInterval otherAge = curTime - aCachePolicyInformation.downloadTime;
            otherPeriod = fabs(otherAge / (double)aCachePolicyInformation.accessesCount);
            
        }
        
        double periodDifference = otherPeriod - curPeriod;
        
        if (periodDifference > 0.0f) {
            
            result = NSOrderedDescending;
            
        } else if (periodDifference < 0.0f) {
            
            result = NSOrderedAscending;
            
        }
        
    }
    
    if (result == NSOrderedSame) {
        
        NSInteger sizeDifference = aCachePolicyInformation.cacheFileSize - cacheFileSize_;
        
        if (sizeDifference > 0) {
            
            result = NSOrderedAscending;
            
        } else if (sizeDifference < 0) {
            
            result = NSOrderedDescending;
            
        }
        
    }
	
	return result;
    
}

@end


#pragma mark -

@implementation DownloadFileCache

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [cacheInformation_ release];
    cacheInformation_ = nil;
    
    [cacheDirectory_ release];
    cacheDirectory_ = nil;
    
    [cacheInformationFileName_ release];
    cacheInformationFileName_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. This initializer is not allowed, so the instance is released and a nil instance is returned
 *
 * @return Always returns nil
 */
- (id)init {
    
    [self autorelease];
    
    return nil;
    
}

/*
 * Designated initializer. It initializes a DownloadFileCache instance that stores information in the provided directory
 * and uses the provided cache information file name
 */
- (id)initWithCacheDirectory:(NSString *)aCacheDirectory
    cacheInformationFileName:(NSString *)aCacheInformationFileName
              expirationTime:(NSTimeInterval)anExpirationTime
                maximumSpace:(unsigned long long)aMaximumCacheUsedSpace {
    
    if (([aCacheDirectory length] == 0) || ([aCacheInformationFileName length] == 0)) {
        
        [self autorelease];
        
        self = nil;
        
    } else if (self = [super init]) {
        
        NSZone *zone = [self zone];
        
        cacheDirectory_ = [aCacheDirectory copyWithZone:zone];
        cacheInformationFileName_ = [aCacheInformationFileName copyWithZone:zone];
        expirationTime_ = fabs(anExpirationTime);
        maximumCacheUsedSpace_ = aMaximumCacheUsedSpace;
        
        [self checkAndCreateDirectory];

        NSString *cacheConfigurationFileName = [self cacheConfigurationFilePath];
        
        cacheInformation_ = [[NSMutableDictionary alloc] initWithContentsOfFile:cacheConfigurationFileName];
        
        if (cacheInformation_ == nil) {
            
            cacheInformation_ = [[NSMutableDictionary alloc] init];
            
        }
        
        [self checkCachePolicy];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Cached information access

/*
 * Returns the cached file path (directory included) associated to the key string
 */
- (NSString *)cachedFilePathForKey:(NSString *)aKeyString {
    
    NSString *result = nil;
    
    NSMutableArray *parametersAssociatedInformation = [cacheInformation_ objectForKey:aKeyString];
    
    if ([parametersAssociatedInformation count] == CACHE_INFORMATION_COUNT) {
        
        result = [[self cacheDirectoryPath]
                  stringByAppendingPathComponent:[parametersAssociatedInformation objectAtIndex:CACHE_FILE_NAME_POSITION]];
        
    }
    
    return result;
    
}

/*
 * Stores the provided data associated to the download key string. When previous data is associated to the download key string,
 * it is replaced by the new one
 */
- (void)storeData:(NSData *)aDownloadedData
     forKeyString:(NSString *)aKeyString {
    
    NSMutableArray *associatedInformation = [cacheInformation_ objectForKey:aKeyString];
    
    NSString *directoryPath = [self cacheDirectoryPath];

    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
    NSNumber *currentTimeNumber = [NSNumber numberWithDouble:currentTime];
    NSNumber *accessesCount = [NSNumber numberWithUnsignedInteger:0];
    
    if ([associatedInformation count] != CACHE_INFORMATION_COUNT) {
        
        NSString *fileName = [Tools findAndCreateNotUsedFileNameInsideDirectory:directoryPath];
        
        associatedInformation = [NSMutableArray arrayWithObjects:currentTimeNumber, currentTimeNumber, accessesCount, fileName, nil];
        
        NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
        [aDownloadedData writeToFile:filePath
                          atomically:YES];
        
        [cacheInformation_ setObject:associatedInformation
                              forKey:aKeyString];
        
    } else {
        
        [associatedInformation replaceObjectAtIndex:CACHE_DOWNLOAD_TIME_POSITION
                                         withObject:currentTimeNumber];
        [associatedInformation replaceObjectAtIndex:CACHE_FILE_LAST_ACCESS_TIME_POSITION
                                         withObject:currentTimeNumber];
        [associatedInformation replaceObjectAtIndex:CACHE_FILE_ACCESSES_COUNT_POSITION
                                         withObject:accessesCount];
        NSString *fileName = [associatedInformation objectAtIndex:CACHE_FILE_NAME_POSITION];
        NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
        
        [aDownloadedData writeToFile:filePath
                          atomically:YES];
        
    }
    
    [cacheInformation_ writeToFile:[self cacheConfigurationFilePath]
                        atomically:YES];
    
}

/*
 * Removes the data associated to the provided key
 */
- (void)removeDataForKey:(NSString *)aKeyString {
    
    NSArray *cacheInformation = [cacheInformation_ objectForKey:aKeyString];
    NSString *fileName = [cacheInformation objectAtIndex:CACHE_FILE_NAME_POSITION];

    if ([fileName length] > 0) {
        
        NSString *directoryPath = [self cacheDirectoryPath];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
        [fileManager removeItemAtPath:filePath
                                error:nil];
        
    }
    
    [cacheInformation_ removeObjectForKey:aKeyString];
    [cacheInformation_ writeToFile:[self cacheConfigurationFilePath]
                        atomically:YES];
    
}

/*
 * Updates the cache information for the given download key string. The last access time is updated to current
 * time and accesses count is added one
 */
- (void)updateCacheInformationForKey:(NSString *)aKeyString {
    
    NSMutableArray *directionsACacheApiArray = [cacheInformation_ objectForKey:aKeyString];
    
    if ([directionsACacheApiArray count] == CACHE_INFORMATION_COUNT) {
        
        NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
        NSNumber *lastAccessTime = [NSNumber numberWithDouble:currentTime];
        
        NSNumber *accessesCount = [directionsACacheApiArray objectAtIndex:CACHE_FILE_ACCESSES_COUNT_POSITION];
        accessesCount = [NSNumber numberWithUnsignedInteger:([accessesCount unsignedIntegerValue] + 1)];
        
        [directionsACacheApiArray replaceObjectAtIndex:CACHE_FILE_LAST_ACCESS_TIME_POSITION
                                            withObject:lastAccessTime];
        [directionsACacheApiArray replaceObjectAtIndex:CACHE_FILE_ACCESSES_COUNT_POSITION
                                            withObject:accessesCount];
        
        [cacheInformation_ writeToFile:[self cacheConfigurationFilePath]
                            atomically:YES];
        
    }
    
}

#pragma mark -
#pragma mark Cached files management

/*
 * Checks the cache policy in case it is necesary to delete some cached files. Cached files older that a defined time
 * are automaticaly deleted, even if no space is needed
 */
- (void)checkCachePolicy {
    
    [self removeExpiredAndOrphanCacheFiles];
    
    NSArray *keys = [cacheInformation_ allKeys];
    NSString *directoryPath = [self cacheDirectoryPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableArray *policyMutableArray = [NSMutableArray array];
    NSDictionary *fileSystemAttributes = [fileManager attributesOfFileSystemForPath:directoryPath
                                                                              error:nil];
    NSNumber *freeSizeNumber = [fileSystemAttributes objectForKey:NSFileSystemFreeSize];
    long long freeSize = [freeSizeNumber longLongValue];
    long long halfFreeSize = freeSize / 2;
    
    NSNumber *downloadTimeNumber;
    NSTimeInterval downloadTime;
    NSNumber *lastAccessTimeNumber;
    NSTimeInterval lastAccessTime;
    NSNumber *accessesCountNumber;
    NSUInteger accessesCount;
    NSString *fileName;
    NSString *filePath;
    NSDictionary *fileAttributes;
    unsigned long long fileSize;
    unsigned long long totalSize = 0;
    CachePolicyInformation *cachePolicyInformation;
    
    for (NSString *key in keys) {
        
        NSArray *cacheInformation = [cacheInformation_ objectForKey:key];
        
        fileName = [cacheInformation objectAtIndex:CACHE_FILE_NAME_POSITION];
        
        if ([fileName length] > 0) {
            
            filePath = [directoryPath stringByAppendingPathComponent:fileName];
            downloadTimeNumber = [cacheInformation objectAtIndex:CACHE_DOWNLOAD_TIME_POSITION];
            downloadTime = [downloadTimeNumber doubleValue];
            lastAccessTimeNumber = [cacheInformation objectAtIndex:CACHE_FILE_LAST_ACCESS_TIME_POSITION];
            lastAccessTime = [lastAccessTimeNumber doubleValue];
            accessesCountNumber = [cacheInformation objectAtIndex:CACHE_FILE_ACCESSES_COUNT_POSITION];
            accessesCount = [accessesCountNumber unsignedIntegerValue];
            fileAttributes = [fileManager attributesOfItemAtPath:filePath
                                                           error:nil];
            fileSize = [fileAttributes fileSize];
            totalSize += fileSize;
            
            cachePolicyInformation = [CachePolicyInformation cachePolicyInformationWithDownloadTime:downloadTime
                                                                                     lastAccessTime:lastAccessTime
                                                                                      accessesCount:accessesCount
                                                                                     expirationTime:expirationTime_
                                                                                      cacheFilePath:filePath
                                                                                      cacheFileSize:fileSize
                                                                                 cacheDictionaryKey:key
                                                                                    cacheDictionary:cacheInformation_];
            
            if (cacheInformation != nil) {
                
                [policyMutableArray addObject:cachePolicyInformation];
                
            }
            
        }
        
    }
    
    NSArray *policyArray = [policyMutableArray sortedArrayUsingSelector:@selector(cachePolicyInformationCompare:)];
    NSInteger pos = [policyArray count] - 1;
    
    long long directionsAPICacheFreeSpace = halfFreeSize;
    NSString *key;
    NSMutableDictionary *cacheDictionary;
    
    while ((totalSize > maximumCacheUsedSpace_) && (pos >= 0)) {
        
        cachePolicyInformation = [policyArray objectAtIndex:pos];
        fileSize = cachePolicyInformation.cacheFileSize;
        directionsAPICacheFreeSpace += fileSize;
        totalSize -= fileSize;
        
        filePath = cachePolicyInformation.cacheFilePath;
        [fileManager removeItemAtPath:filePath
                                error:nil];
        
        key = cachePolicyInformation.key;
        cacheDictionary = cachePolicyInformation.cacheDictionary;
        [cacheDictionary removeObjectForKey:key];
        
    }
    
    [cacheInformation_ writeToFile:[self cacheConfigurationFilePath]
                        atomically:YES];
    
}

@end


#pragma mark -

@implementation DownloadFileCache(private)

#pragma mark -
#pragma mark Directory and file cache management

/*
 * Returns the cache directory path
 */
- (NSString *)cacheDirectoryPath {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
	return [documentsPath stringByAppendingPathComponent:cacheDirectory_];
    
}

/*
 * Returns the cache configuration file path
 */
- (NSString *)cacheConfigurationFilePath {
    
    return [[self cacheDirectoryPath] stringByAppendingPathComponent:cacheInformationFileName_];
    
}

/*
 * Checks whether the cache directory is created. It creates the directory when it is not found
 */
- (void)checkAndCreateDirectory {
    
    [Tools checkAndCreateDirectory:[self cacheDirectoryPath]];
    
}

/*
 * Removes expired and orphan cache files from directory
 */
- (void)removeExpiredAndOrphanCacheFiles {
    
    NSArray *keys = [cacheInformation_ allKeys];
    NSString *directoryPath = [self cacheDirectoryPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *cacheFiles = [fileManager contentsOfDirectoryAtPath:directoryPath
                                                           error:nil];
    NSMutableArray *mutableCacheFiles = [NSMutableArray arrayWithArray:cacheFiles];
    [mutableCacheFiles removeObject:cacheInformationFileName_];
    
    NSNumber *downloadTimeNumber;
    NSString *fileName;
    NSString *filePath;
    

    NSTimeInterval oldestDownloadTimeAllowed = [NSDate timeIntervalSinceReferenceDate] - expirationTime_;
        
    for (NSString *key in keys) {
        
        NSArray *cacheInformation = [cacheInformation_ objectForKey:key];
        downloadTimeNumber = [cacheInformation objectAtIndex:CACHE_DOWNLOAD_TIME_POSITION];
        fileName = [cacheInformation objectAtIndex:CACHE_FILE_NAME_POSITION];
        
        [mutableCacheFiles removeObject:fileName];
        
        if ((expirationTime_ > 0.0f) && ([downloadTimeNumber doubleValue] < oldestDownloadTimeAllowed)) {
            
            if ([fileName length] > 0) {
                
                filePath = [directoryPath stringByAppendingPathComponent:fileName];
                [fileManager removeItemAtPath:filePath
                                        error:nil];
                
            }
            
            [cacheInformation_ removeObjectForKey:key];
            
        }
        
    }
    
    for (NSString *orphanFile in mutableCacheFiles) {
        
        if ([orphanFile length] > 0) {
            
            filePath = [directoryPath stringByAppendingPathComponent:orphanFile];
            [fileManager removeItemAtPath:filePath
                                    error:nil];
            
        }
        
    }
    
    [cacheInformation_ writeToFile:[self cacheConfigurationFilePath]
                        atomically:YES];
    
}

@end
