/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>


#pragma mark -

/**
 * UIButton utility category. Provide convenience selectors to manage buttons
 */
@interface UIButton(Util)

/**
 * Creates an autoreleased button with an icon.<br>
 * The size of the returned button is the size of the background image.
 *
 * @param icon Button icon.
 * @param background Button background
 */
+ (UIButton *)buttonWithIcon:(UIImage *)icon background:(UIImage *)background;

@end
