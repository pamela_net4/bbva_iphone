/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#ifndef NXT_Peru_iPhone_TransfersConstants_h
#define NXT_Peru_iPhone_TransfersConstants_h

/**
 * Enumerates the different types of transfer the user can make
 */
typedef enum {
    
    TTETransferBetweenUserAccounts = 0, //!<The user is performing a transfer between his/her own accounts
    TTETransferToOtherUserAccount,      //!<The user is performing a transfer to another user with a BBVA account
    TTETransferToOtherBankAccount,      //!<The user is performing a transfer to another bank account
    TTETransferWithCashMobile,          //!<The user is performing a transfer with cash mobile
    TTETransferConsultCashMobile,       //!<The user is performing a consult of cash mobile operations
    TTETransferToGiftCard,               //!<The user is performing a transfer to gift card
    TTEThirdTransferAccount //!<The user is performing a create account
    
} TransferTypeEnum;

#endif
