/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import "MCBConfiguration.h"

@implementation MCBConfiguration

@synthesize methods = methods_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates memory used
 */
- (void)dealloc {
    
    [methods_ release];
	methods_ = nil;
    
	[super dealloc];
    
}

#pragma mark -
#pragma mark Utils

/*
 * Checks if the given method has to be traced
 */
- (BOOL)checkTracingOfMethod:(NSString *)aMethod {
    BOOL result = NO;
    
    for (NSString *method in methods_) {
        
        if ([[method lowercaseString] isEqualToString:[aMethod lowercaseString]]) {
            result = YES;
            break;
        }
    }
    
    return result;
}

@end
