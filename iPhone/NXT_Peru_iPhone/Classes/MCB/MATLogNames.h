/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#ifndef NXT_Peru_iPhone_MATLogNames_h
#define NXT_Peru_iPhone_MATLogNames_h


#define MAT_LOG_LOGIN                                       @"Acceso"
#define MAT_LOG_VALIDATE_COORDINATES                        @"ValidacionCoordenadas"
#define MAT_LOG_LOGOUT                                      @"CierreSesion"
#define MAT_LOG_DISCONNECT                                  @"DesconexionWAS"
#define MAT_LOG_SESSION_EXPIRED                             @"ExpiracionSesion"

#define MAT_LOG_GLOBAL_POSITION                             @"PosicionGlobal"
#define MAT_LOG_ACCOUNTS_TRANSACTIONS                       @"MovimientosCuenta"
#define MAT_LOG_CARDS_TRANSACTIONS                          @"MovimientosTarjeta"
#define MAT_LOG_RETENTIONS                                  @"Retenciones"
#define MAT_LOG_SEND_TRANSACTION                            @"MovimientosEnvio"
#define MAT_LOG_SEND_TRANSACTION_CONFIRMATION               @"MovimientosConfirmacion"

#define MAT_LOG_OWN_ACCOUNTS_TRANSFER_SEND                  @"TFsCuentasPropiasEnvio"
#define MAT_LOG_OWN_ACCOUNTS_TRANSFER_CONFIRMATION          @"TFsCuentasPropiasConfirmacion"
#define MAT_LOG_OWN_ACCOUNTS_TRANSFER_SUCCESS               @"TFsCuentasPropiasExito"

#define MAT_LOG_OTHER_BANK_CUSTOMER_TRANSFER_SEND           @"TFsTercerosEnvio"
#define MAT_LOG_OTHER_BANK_CUSTOMER_TRANSFER_CONFIRMATION   @"TFsTercerosConfirmacion"
#define MAT_LOG_OTHER_BANK_CUSTOMER_TRANSFER_SUCCESS        @"TFsTercerosExito"

#define MAT_LOG_OTHER_BANK_TRANSFER_SEND                    @"TFsOtrosBancosEnvio"
#define MAT_LOG_OTHER_BANK_TRANSFER_CONFIRMATION            @"TFsOtrosBancosConfirmacion"
#define MAT_LOG_OTHER_BANK_TRANSFER_CHECK_TYPE              @"TFsOtrosBancosTypeConfirmacion"
#define MAT_LOG_OTHER_BANK_TRANSFER_SUCCESS                 @"TFsOtrosBancosExito"

#define MAT_LOG_CASH_MOBILE_TRANSFER_SEND                   @"Efectivo Móvil/Envio"
#define MAT_LOG_CASH_MOBILE_TRANSFER_CONFIRMATION           @"Efectivo Móvil/Confirmacion"
#define MAT_LOG_CASH_MOBILE_TRANSFER_SUCCESS                @"Efectivo Móvil/Exito"

#define MAT_LOG_CASH_MOBILE_CONSULT                         @"Efectivo Móvil/Consulta"
#define MAT_LOG_CASH_MOBILE_DETAIL                          @"Efectivo Móvil/Detalle"
#define MAT_LOG_CASH_MOBILE_RESEND                          @"Efectivo Móvil/Reenvio"

#define MAT_LOG_GIFT_CARD_SEND                              @"RecargaTarjetaRegalo/Ingreso"
#define MAT_LOG_GIFT_CARD_CONFIRMATION                      @"RecargaTarjetaRegalo/ValidacionRecarga"
#define MAT_LOG_GIFT_CARD_SUCCESS                           @"RecargaTarjetaRegalo/RealizarRecarga"

#define MAT_LOG_PAYMENT_START                               @"PagosInicio"

#define MAT_LOG_PAYMENT_INST_COMP_LIST                      @"Pago instituciones y empresas/Envio"
#define MAT_LOG_PAYMENT_INST_COMP_ENTITY_LIST               @"Pago instituciones y empresas/ListaEntidades"
#define MAT_LOG_PAYMENT_INST_COMP_SOLI_PAY                  @"Pago instituciones y empresas/SolicitudPAgo"
#define MAT_LOG_PAYMENT_INST_COMP_CONFIRM_PAY               @"Pago de instituciones y empresas/Confirmar Pago"
#define MAT_LOG_PAYMENT_INST_COMP_PEDING_PAYS               @"Pago instituciones y empresas/ListPagoPendiente"
#define MAT_LOG_PAYMENT_INST_COMP_DO_PAY                    @"Pago instituciones y empresas/RealizarPago"

#define MAT_LOG_PAYMENT_LUZ_DEL_SUR_LIST                    @"PagosSBElectricosLuzDelSurLista"
#define MAT_LOG_PAYMENT_LUZ_DEL_SUR_DATA                    @"PagosSBElectricosLuzDelSurDatos"
#define MAT_LOG_PAYMENT_LUZ_DEL_SUR_CONFIRMATION            @"PagosSBElectricosLuzDelSurConfirmacion"
#define MAT_LOG_PAYMENT_LUZ_DEL_SUR_SUCCESS                 @"PagosSBElectricosLuzDelSurExito"

#define MAT_LOG_PAYMENT_EDELNOR_LIST                        @"PagosSBElectricosEdelnorLista"
#define MAT_LOG_PAYMENT_EDELNOR_DATA                        @"PagosSBElectricosEdelnorDatos"
#define MAT_LOG_PAYMENT_EDELNOR_CONFIRMATION                @"PagosSBElectricosEdelnorConfirmacion"
#define MAT_LOG_PAYMENT_EDELNOR_SUCCESS                     @"PagosSBElectricosEdelnorExito"

#define MAT_LOG_PAYMENT_SEDAPAL_LIST                        @"PagosSBAguaSedapalLista"
#define MAT_LOG_PAYMENT_SEDAPAL_DATA                        @"PagosSBAguaSedapalDatos"
#define MAT_LOG_PAYMENT_SEDAPAL_CONFIRMATION                @"PagosSBAguaSedapalConfirmacion"
#define MAT_LOG_PAYMENT_SEDAPAL_SUCCESS                     @"PagosSBAguaSedapalExito"

#define MAT_LOG_PAYMENT_TELEFONICA_LANDLINE_LIST            @"PagosSBTelefonoFijoTelefonicaLista"
#define MAT_LOG_PAYMENT_TELEFONICA_LANDLINE_DATA            @"PagosSBTelefonoFijoTelefonicaDatos"
#define MAT_LOG_PAYMENT_TELEFONICA_LANDLINE_CONFIRMATION    @"PagosSBTelefonoFijoTelefonicaConfirmacion"
#define MAT_LOG_PAYMENT_TELEFONICA_LANDLINE_SUCCESS         @"PagosSBTelefonoFijoTelefonicaExito"

#define MAT_LOG_PAYMENT_MOVISTAR_CELULAR_LIST               @"PagosSBTelefonoCelularMovistarLista"
#define MAT_LOG_PAYMENT_MOVISTAR_CELULAR_DATA               @"PagosSBTelefonoCelularMovistarDatos"
#define MAT_LOG_PAYMENT_MOVISTAR_CELULAR_CONFIRMATION       @"PagosSBTelefonoCelularMovistarConfirmacion"
#define MAT_LOG_PAYMENT_MOVISTAR_CELULAR_SUCCESS            @"PagosSBTelefonoCelularMovistarExito"

#define MAT_LOG_PAYMENT_CLARO_CELULAR_LIST                  @"PagosSBTelefonoCelularClaroLista"
#define MAT_LOG_PAYMENT_CLARO_CELULAR_DATA                  @"PagosSBTelefonoCelularClaroDatos"
#define MAT_LOG_PAYMENT_CLARO_CELULAR_CONFIRMATION          @"PagosSBTelefonoCelularClaroConfirmacion"
#define MAT_LOG_PAYMENT_CLARO_CELULAR_SUCCESS               @"PagosSBTelefonoCelularClaroExito"

#define MAT_LOG_PAYMENT_OWN_CARDS_LIST                      @"PagosTarjetasPropiasLista"
#define MAT_LOG_PAYMENT_OWN_CARDS_DATA                      @"PagosTarjetasPropiasDatos"
#define MAT_LOG_PAYMENT_OWN_CARDS_CONFIRMATION              @"PagosTarjetasPropiasConfirmacion"
#define MAT_LOG_PAYMENT_OWN_CARDS_SUCCESS                   @"PagosTarjetasPropiasExito"

#define MAT_LOG_PAYMENT_OTHERS_CARDS_DATA                   @"PagosTarjetasTercerosDatos"
#define MAT_LOG_PAYMENT_OTHERS_CARDS_CONFIRMATION           @"PagosTarjetasTercerosConfirmacion"
#define MAT_LOG_PAYMENT_OTHERS_CARDS_SUCCESS                @"PagosTarjetasTercerosExito"

#define MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_LIST              @"PagosTarjetasOtrosBancosLista"
#define MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_DATA              @"PagosTarjetasOtrosBancosDatos"
#define MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_TIN      @"PagosTarjetasOtrosBancosTINOnline"
#define MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_CONFIRMATION      @"PagosTarjetasOtrosBancosConfirmacion"
#define MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_SUCCESS           @"PagosTarjetasOtrosBancosExito"

#define MAT_LOG_PAYMENT_PREPAY_DATA                         @"PagosRecargasDatos"
#define MAT_LOG_PAYMENT_PREPAY_CONFIRMATION                 @"PagosRecargasConfirmacion"
#define MAT_LOG_PAYMENT_PREPAY_TELEFONICA_SUCCESS           @"PagosRecargasTelefonicaExito"
#define MAT_LOG_PAYMENT_PREPAY_CLARO_SUCCESS                @"PagosRecargasClaroExito"

#define MAT_LOG_SAFETYPAY_STATUS_INFO                       @"PagoSaftyPay/Acceso"
#define MAT_LOG_SAFETYPAY_TRANSACTION_INFO                  @"PagoSaftyPay/Validacion"
#define MAT_LOG_SAFETYPAY_DETAILS                           @"PagoSaftyPay/Confirmacion"
#define MAT_LOG_SAFETYPAY_CONFIRM                           @"PagoSaftyPay/RealizarPago"

#define MAT_LOG_FREQUENT_OPERATION_LIST                     @"ListadoOF"
#define MAT_LOG_FREQUENT_OPERATION_PAGINATION_LIST          @"PaginaciónOF"
#define MAT_LOG_FREQUENT_OPERATION_EXECUTE                  @"OFEjecucion"
#define MAT_LOG_FREQUENT_OPERATION_CONFIRMATION             @"TFsEfectivoMovilReenvio"

#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_ONE @"OFReactivo/EfectivoMovil/Inscripcion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_ONE @"OFReactivo/TransferenciaOtrosBancos/Inscripcion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUNT_STEP_ONE @"OFReactivo/TransferenciaTerceros/Inscripcion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_ONE @"OFReactivo/PagoTarjetasTerceros/Inscripcion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_ONE @"OFReactivo/PagoTarjetaOtrosBancos/Inscripcion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_GIFT_CARD_PAYMENT_STEP_ONE @"OFReactivo/RecargaTarjetaRegalo/Inscripcion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_ONE @"OFReactivo/RecargaCelularesClaro/Inscripcion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_ONE @"OFReactivo/RecargaCelularesMovistar/Inscripcion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_ONE @"OFReactivo/PagoServiciosClaro/Inscripcion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_ONE @"OFReactivo/PagoServiciosMovistar/Inscripcion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_ONE @"OFReactivo/PagoServiciosAgua/Inscripcion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_ONE @"OFReactivo/PagoInstitucionesEmpresas/Inscripcion"


#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_TWO @"OFReactivo/EfectivoMovil/Ingreso"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_TWO @"OFReactivo/TransferenciaOtrosBancos/Ingreso"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUNT_STEP_TWO @"OFReactivo/TransferenciaTerceros/Ingreso"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_TWO @"OFReactivo/PagoTarjetasTerceros/Ingreso"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_TWO @"OFReactivo/PagoTarjetaOtrosBancos/Ingreso"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_GIFT_CARD_PAYMENT_STEP_TWO @"OFReactivo/RecargaTarjetaRegalo/Ingreso"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_TWO @"OFReactivo/RecargaCelularesClaro/Ingreso"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_TWO @"OFReactivo/RecargaCelularesMovistar/Ingreso"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_TWO @"OFReactivo/PagoServiciosClaro/Ingreso"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_TWO @"OFReactivo/PagoServiciosMovistar/Ingreso"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_TWO @"OFReactivo/PagoServiciosAgua/Ingreso"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_TWO @"OFReactivo/PagoInstitucionesEmpresas/Ingreso"

#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_CASH_MOBILE_STEP_THREE @"OFReactivo/EfectivoMovil/Confirmacion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_OTHER_BANK_STEP_THREE @"OFReactivo/TransferenciaOtrosBancos/Confirmacion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_THIRD_ACCOUNT_STEP_THREE @"OFReactivo/TransferenciaTerceros/Confirmacion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_THIRD_CARD_PAYMENT_STEP_THREE @"OFReactivo/PagoTarjetasTerceros/Confirmacion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_OTHER_BANK_PAYMENT_STEP_THREE @"OFReactivo/PagoTarjetaOtrosBancos/Confirmacion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_GIFT_CARD_PAYMENT_STEP_THREE @"OFReactivo/RecargaTarjetaRegalo/Confirmacion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_RECHARGE_CLARO_STEP_THREE @"OFReactivo/RecargaCelularesClaro/Confirmacion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_RECHARGE_MOVISTAR_STEP_THREE @"OFReactivo/RecargaCelularesMovistar/Confirmacion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_CLARO_STEP_THREE @"OFReactivo/PagoServiciosClaro/Confirmacion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_MOVISTAR_STEP_THREE @"OFReactivo/PagoServiciosMovistar/Confirmacion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_SERVICE_WATER_STEP_THREE @"OFReactivo/PagoServiciosAgua/Confirmacion"
#define MAT_LOG_FREQUENT_OPERATION_REACTIVE_INSTITUTION_COMPANIES_STEP_THREE @"OFReactivo/PagoInstitucionesEmpresas/Confirmacion"

#define MAT_LOG_THIRD_ACCOUNT_REGISTRATE_LIST                     @"CuentaInscrita/Lista"
#define MAT_LOG_THIRD_ACCOUNT_REGISTRATE_CONFIRM_OPERATION        @"CuentaInscrita/Confirm"
#define MAT_LOG_THIRD_ACCOUNT_REGISTRATE_SUCCESS_OPERATION        @"CuentaInscrita/Result"
#define MAT_LOG_THIRD_ACCOUNT_NICKNAME_VALIDATE_OPERATION        @"CuentaInscrita/Validate"


#define MAT_LOG_CAMPAIGN_LIST_OPERATION       @"BannerDigitalCampania"

#define MAT_LOG_INCREMENT_OF_LINE_STEP_ONE_OPERATION       @"IncrementoDeLineaConsultar"
#define MAT_LOG_INCREMENT_OF_LINE_STEP_TWO_OPERATION       @"IncrementoDeLineaEnviarSMS"
#define MAT_LOG_INCREMENT_OF_LINE_STEP_THREE_OPERATION       @"IncrementoDeLineaRealizarIncremento"

#define MAT_LOG_FAST_LOAN_STEP_ONE_OPERATION       @"PrestamoAlToqueConsultar"
#define MAT_LOG_FAST_LOAN_STEP_TWO_OPERATION       @"PrestamoAlToqueConfirmar"
#define MAT_LOG_FAST_LOAN_STEP_THREE_OPERATION     @"PrestamoAlToqueRealizar"
#define MAT_LOG_FAST_LOAN_TERM_OPERATION           @"PrestamoAlToqueObtenerPlazos"
#define MAT_LOG_FAST_LOAN_SIMULATION_OPERATION     @"PrestamoAlToqueSimulacion"
#define MAT_LOG_FAST_LOAN_SCHEDULE_OPERATION       @"PrestamoAlToqueVerCronograma"

#define MAT_LOG_SALARY_ADVANCE_AFFILIATION_OPERATION                @"AdelantoDeSueldoConsultar"
#define MAT_LOG_SALARY_ADVANCE_RECEIVE_OPERATION                    @"AdelantoDeSueldoConfirmarDisposicion"
#define MAT_LOG_SALARY_ADVANCE_SUMMARY_OPERATION                    @"AdelantoDeSueldoRealizarDisposicion"
#define MAT_LOG_SALARY_ADVANCE_AFFILIATION_CONFIRM_OPERATION        @"AdelantoDeSueldoRealizarAfiliacion"



#define MAT_LOG_SUSCRIPTION_CLIENT_IN       @"AltaCliente/Ingreso"
#define MAT_LOG_CHANGE_CLIENT_IN            @"ModificacionClave/Ingreso"
#define MAT_LOG_SUSCRIPTION_CLIENT_VERIFY   @"AltaCliente/Confirmar"
#define MAT_LOG_CHANGE_CLIENT_VERIFY        @"ModificacionClave/Confirmar"
#define MAT_LOG_SUSCRIPTION_CLIENT_DO       @"AltaCliente/Realizar"
#define MAT_LOG_CHANGE_CLIENT_DO            @"ModificacionClave/Realizar"
#define MAT_LOG_SUSCRIPTION_CLIENT_TERMS    @"AltaCliente/Terminos"
#define MAT_LOG_CHANGE_CLIENT_DO_TERMS      @"ModificacionClave/Terminos"


#define MAT_LOG_RADIO_BBVA_OPERATION        @"RadioBBVA"


#endif
