/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import <Foundation/Foundation.h>


/**
 * Object that stores the configuration of MAT, that is, an array with the methods of the
 * application that have to be traced. If a method is not in this array, then that method in not traced
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MCBConfiguration : NSObject

/**
 * Methods array
 */
@property (nonatomic, readwrite, strong) NSArray *methods;

/**
 * Checks if the given method has to be traced
 *
 * @param method to check
 */
- (BOOL)checkTracingOfMethod:(NSString *)method;

@end