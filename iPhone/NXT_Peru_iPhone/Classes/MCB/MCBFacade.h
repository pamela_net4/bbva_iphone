/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>

@class MCBConfiguration;


/**
 * Defines the facade to the library client will interact with. It provides static selectors to access the MAT library funcionality.
 * When including the library, the project must be linked against the Foundation, SystemConfiguration and UIKit frameworks
 *
 * <h3>Initialization</h3>
 * The library provides an default configuration and can be used as is.
 * The default configuration can be seen (but not changed) in Constants.h
 * file.
 *
 * For some applications it may be preferable to set some other values.
 * For instance, the target server:
 * @code
 *   [MCBFacade setMATServerAuthority:@"79.125.125.123:80"];
 * @endcode
 *
 * <h3>Application Method Configuration</h3>
 * This is required to enable/disable specific application methods. MCBFacade
 * receives a MCBConfiguration instance containing an array with the names
 * of all methods to trace (objects inside this array must be NSString instances)
 * Only methods inside this array will be traced to the server
 * @code
 *   NSArray *methods = [NSArray arrayWithObjects:@"method1", @"method2", nil];
 *   MCBConfiguration *configuration = [[[MCBConfiguration alloc] init] autorelease];
 *   configuration.methods = methods;
 *   [MCBFacade setMATConfiguration:configuration];
 * @endcode
 * The library will only send to the server those methods included in MCBConfiguration.
 *
 * <h3>Application Startup Log</h3>
 * This call should be the first notification to be sent to the server.
 * Indicates the application startup.
 * @code
 *   [MCBFacade invokeLogAppDetailsWithAppName:@"appName"
 *                                      appKey:@"xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
 *                                  appCountry:@"ES"
 *                                  appVersion:@"1.0"
 *                                    latitude:0.0f
 *                                   longitude:0.0f
 *                               validLocation:NO];
 * @endcode
 *
 * or
 *
 * @code
 *   [MCBFacade invokeLogAppDetailsWithAppName:@"appName"
 *                                      appKey:@"xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
 *                                  appCountry:@"ES"
 *                                  appVersion:@"1.0"
 *                                     appLang:@"es"
 *                                      extra1:@"extra1"
 *                                      extra2:@"extra2"
 *                                      extra3:@"extra3"
 *                                      userId:@"userId"];
 * @endcode
 *
 * <h3>Device Location Log</h3>
 * This call can be invoked to notify a location change to the server.
 * @code
 *   [MCBFacade invokeLogDeviceLocationWithAppName:@"appName"
 *                                          appKey:@"xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
 *                                      appCountry:@"ES"
 *                                      appVersion:@"1.0"
 *                                        latitude:37.33182f
 *                                       longitude:-122.03118f
 *                                         address:@"1 Infinite Loop"];
 * @endcode
 *
 * or
 *
 * @code
 *   [MCBFacade invokeLogDeviceLocationWithAppName:@"appName"
 *                                          appKey:@"xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
 *                                      appCountry:@"ES"
 *                                      appVersion:@"1.0"
 *                                         appLang:@"es"
 *                                          extra1:@"extra1"
 *                                          extra2:@"extra2"
 *                                          extra3:@"extra3"
 *                                        latitude:37.33182f
 *                                       longitude:-122.03118f
 *                                         address:@"1 Infinite Loop"];
 * @endcode
 *
 * <h3>Application Method Log</h3>
 * This call is related to a specific application functionality. The actual
 * invocation will be performed if this method has been previously
 * configured via MCBConfiguration.
 * @code
 *   [MCBFacade invokeLogInvokedAppMethodWithAppName:@"appName"
 *                                            appKey:@"xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
 *                                        appCountry:@"ES"
 *                                        appVersion:@"1.0"
 *                                          latitude:0.0f
 *                                         longitude:0.0f
 *                                     validLocation:NO
 *                                         appMethod:@"Login"];
 * @endcode
 *
 * or
 *
 * @code
 *   [MCBFacade invokeLogInvokedAppMethodWithAppName:@"appName"
 *                                            appKey:@"xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
 *                                        appCountry:@"ES"
 *                                        appVersion:@"1.0"
 *                                           appLang:@"es"
 *                                            extra1:@"extra1"
 *                                            extra2:@"extra2"
 *                                            extra3:@"extra3"
 *                                         appMethod:@"Login"
 *                                              step:0];
 * @endcode
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MCBFacade : NSObject {
    
}

/**
 * Sets the MAT server scheme name (that is protocol to use, http://, https:// ...). When this selector is not invoked,
 * a default scheme name is used.
 *
 * @param schemeName The scheme name used to define the MAT server URL.
 */
+ (void)setMATServerSchemeName:(NSString *)schemeName;

/**
 * Sets the MAT server URL authority (that is, the host name or IP address and port). When this selector is not invoked,
 * a default authority is used.
 *
 * @param authority The URL autority used to define the MAT server URL.
 */
+ (void)setMATServerAuthority:(NSString *)authority;

/**
 * Sets the MAT server URL initial path (that is, the path from the authority part up to the method, but not including it). When this selector
 * is not invoked, a default initial path is used.
 *
 * @param initialPath The URL initial path used to define the MAT server URL.
 */
+ (void)setMATServerInitialPath:(NSString *)initialPath;

/**
 * Sets the MAT platform prefix. When this value is not set or is nil, a default value is used.
 *
 * @param platformPrefix The platform prefix to send to the server.
 */
+ (void)setMATPlatformPrefix:(NSString *)platformPrefix;

/**
 * Sets the configuration.
 *
 * @param configuration to set.
 */
+ (void)setMATConfiguration:(MCBConfiguration *)configuration;

/**
 * Invokes the log application details method.
 *
 * @param appName The application name being logged.
 * @param appKey The application key.
 * @param appCountry The application country.
 * @param appVersion The application version.
 * @param latitude The device latitude.
 * @param longitude The device longitude.
 * @param validLocation YES when the provided latitude and longitude can be used as a valid location, NO otherwise (that information will not be sent to the server).
 * @return YES when the method can be invoked, NO otherwise.
 */
+ (BOOL)invokeLogAppDetailsWithAppName:(NSString *)appName
                                appKey:(NSString *)appKey
                            appCountry:(NSString *)appCountry
                            appVersion:(NSString *)appVersion
                              latitude:(double)latitude
                             longitude:(double)longitude
                         validLocation:(BOOL)validLocation;

/**
 * Invokes the log application details method.
 *
 * @param appName The application name being logged.
 * @param appKey The application key.
 * @param appCountry The application country.
 * @param appVersion The application version.
 * @param appLang The application language.
 * @param extra1 The extra 1 parameter.
 * @param extra2 The extra 2 parameter.
 * @param extra3 The extra 3 parameter.
 * @param userId The user id.
 * @return YES when the method can be invoked, NO otherwise.
 */
+ (BOOL)invokeLogAppDetailsWithAppName:(NSString *)appName
                                appKey:(NSString *)appKey
                            appCountry:(NSString *)appCountry
                            appVersion:(NSString *)appVersion
                               appLang:(NSString *)appLang
                                extra1:(NSString *)extra1
                                extra2:(NSString *)extra2
                                extra3:(NSString *)extra3
                                userId:(NSString *)userId;

/**
 * Invokes the log device location method.
 *
 * @param appName The application name being logged.
 * @param appKey The application key.
 * @param appCountry The application country.
 * @param appVersion The application version.
 * @param latitude The device latitude.
 * @param longitude The device longitude.
 * @param address The address corresponding to the latitude and longitude.
 * @return YES when the method can be invoked, NO otherwise.
 */
+ (BOOL)invokeLogDeviceLocationWithAppName:(NSString *)appName
                                    appKey:(NSString *)appKey
                                appCountry:(NSString *)appCountry
                                appVersion:(NSString *)appVersion
                                  latitude:(double)latitude
                                 longitude:(double)longitude
                                   address:(NSString *)address;

/**
 * Invokes the log device location method.
 *
 * @param appName The application name being logged.
 * @param appKey The application key.
 * @param appCountry The application country.
 * @param appVersion The application version.
 * @param appLang The application language.
 * @param userId The user identifier.
 * @param extra1 The extra 1 parameter.
 * @param extra2 The extra 2 parameter.
 * @param extra3 The extra 3 parameter.
 * @param latitude The device latitude.
 * @param longitude The device longitude.
 * @return YES when the method can be invoked, NO otherwise.
 */
+ (BOOL)invokeLogDeviceLocationWithAppName:(NSString *)appName
                                    appKey:(NSString *)appKey
                                appCountry:(NSString *)appCountry
                                appVersion:(NSString *)appVersion
                                   appLang:(NSString *)appLang
                                    userId:(NSString *)userId
                                    extra1:(NSString *)extra1
                                    extra2:(NSString *)extra2
                                    extra3:(NSString *)extra3
                                  latitude:(double)latitude
                                 longitude:(double)longitude;

/**
 * Invokes the log invoked application method details method.
 *
 * @param appName The application name being logged.
 * @param appKey The application key.
 * @param appCountry The application country.
 * @param appVersion The application version.
 * @param latitude The device latitude.
 * @param longitude The device longitude.
 * @param validLocation YES when the provided latitude and longitude can be used as a valid location, NO otherwise (that information will not be sent to the server).
 * @param appMethod A string identifying the application method being invoked.
 * @return YES when the method can be invoked, NO otherwise.
 */
+ (BOOL)invokeLogInvokedAppMethodWithAppName:(NSString *)appName
                                      appKey:(NSString *)appKey
                                  appCountry:(NSString *)appCountry
                                  appVersion:(NSString *)appVersion
                                    latitude:(double)latitude
                                   longitude:(double)longitude
                               validLocation:(BOOL)validLocation
                                   appMethod:(NSString *)appMethod;

/**
 * Invokes the log invoked application method details method.
 *
 * @param appName The application name being logged.
 * @param appKey The application key.
 * @param appCountry The application country.
 * @param appVersion The application version.
 * @param appLang The application language.
 * @param userId The user identifier.
 * @param extra1 The extra 1 parameter.
 * @param extra2 The extra 2 parameter.
 * @param extra3 The extra 3 parameter.
 * @param appMethod A string identifying the application method being invoked.
 * @param step The method step to trace.
 * @return YES when the method can be invoked, NO otherwise.
 */
+ (BOOL)invokeLogInvokedAppMethodWithAppName:(NSString *)appName
                                      appKey:(NSString *)appKey
                                  appCountry:(NSString *)appCountry
                                  appVersion:(NSString *)appVersion
                                     appLang:(NSString *)appLang
                                      userId:(NSString *)userId
                                      extra1:(NSString *)extra1
                                      extra2:(NSString *)extra2
                                      extra3:(NSString *)extra3
                                   appMethod:(NSString *)appMethod
                                        step:(NSUInteger)step;

/**
 * Sets the enabled flag to allow or avoid logging.
 *
 * @param flag YES to enable logging, NO to avoid it.
 */
+ (void)setEnabled:(BOOL)flag;

@end
