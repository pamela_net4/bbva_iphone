//
//  SuscriptionLegales.m
//  NXT_Peru_iPhone
//
//  Created by Cardenas Torres, B. on 4/04/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "SecurityTerms.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "XMLReader.h"
#import "MCBFacade.h"
#import "Constants.h"
#import "MATLogNames.h"
#import "HTTPInvoker.h"
#import "LoanList.h"
#import "MATLogNames.h"
#import "MCBFacade.h"
#import "MutualFundList.h"
#import "NSString+URLAndHTMLUtils.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Updater.h"
#import "StringKeys.h"
#import "Tools.h"

@interface SecurityTerms ()
@property (retain, nonatomic) IBOutlet UIView *punto1;
@property (retain, nonatomic) IBOutlet UIView *punto2;
@property (retain, nonatomic) IBOutlet UIView *punto3;

@end

@implementation SecurityTerms

- (void)viewDidLoad {
    [super viewDidLoad];

    
    
    
    
    //circulares
    _punto1.layer.cornerRadius = _punto1.frame.size.width/2;
    _punto1.layer.masksToBounds = YES;
    
    //circulares
    _punto2.layer.cornerRadius = _punto2.frame.size.width/2;
    _punto2.layer.masksToBounds = YES;
    
    //circulares
    _punto3.layer.cornerRadius = _punto3.frame.size.width/2;
    _punto3.layer.masksToBounds = YES;
    
    
    NSString *vieneDeChangePass=[[NSUserDefaults standardUserDefaults] objectForKey:@"vieneDeChangePass"];
    
    if ([vieneDeChangePass isEqualToString:@"YES"]) {
        
        _lblTitle.text = @"Modifica tu contraseña";
        [self processMCBNotifications:MAT_LOG_CHANGE_CLIENT_DO_TERMS];
        
    }
    else
    {
        
        _lblTitle.text = @"Afíliate";
        [self processMCBNotifications:MAT_LOG_SUSCRIPTION_CLIENT_TERMS];
        
    }
    
}


- (IBAction)onClickBack:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"aceptaAviso"];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (void)dealloc {
    [_punto1 release];
    [_punto2 release];
    [_punto3 release];
    [_lblTitle release];
    [super dealloc];
}

/**
 * Process MCB notifications after an UpdaterOperation invocation
 *
 * @param notification The notification.
 * @private
 */
- (void)processMCBNotifications:(NSString *)notification {
    
    [MCBFacade invokeLogInvokedAppMethodWithAppName:MCB_APPLICATION_NAME
                                             appKey:MCB_APPLICATION_KEY
                                         appCountry:APP_COUNTRY
                                         appVersion:APP_VERSION_STRING
                                           latitude:0.0f
                                          longitude:0.0f
                                      validLocation:NO
                                          appMethod:notification];
}
@end
