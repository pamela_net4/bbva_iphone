/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */



#import "BlueButtonCell.h"
#import "Constants.h"
#import "GeneralLoginResponse.h"
#import "IconActionCell.h"
#import "LoginInfoCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "LoginInfoCell.h"
#import "LoginViewController.h"
#import "Tools.h"
#import "NXTNavigationItem.h"
#import "Updater.h"
#import "MCBFacade.h"
#import "MOKUILimitedLengthTextField.h"
#import "LoginCoordinatesViewController.h"
#import "LoginInfoViewController.h"
#import "POIMapViewController.h"
#import "Session.h"
#import "UIColor+BBVA_Colors.h"
#import "YourOpinionMattersViewController.h"
#import "NXTEditableViewController+protected.h"
#import "TblFrequentCard.h"
#import "FrequentCard.h"
#import "GeneralLoginEncryptResponse.h"
#import "SplashRadioViewController.h"
#import "SlideOutNavigationViewController.h"
#import "SuscriptionStep1.h"
#import "ChangePassStep1.h"

#import "MATLogNames.h"

/**
 * Define the LoginViewController NIB file name
 */
#define NIB_FILE_NAME                                                           @"LoginViewController"

/**
 * Minimum distance from pop buttons view top to edited view bottom
 */
#define MIN_VERTICAL_DISTANCE_TO_EDITING_VIEW                               80.0f

/**
 * Defines the advertising text size
 */
#define ADVERTISING_TEXT_SIZE                                                   13.0f

/**
 * Defines the row mobile banking.
 */
#define MOBILE_BANKING_ROW                                                      0

/**
 * Defines the row to show login.
 */
#define LOGIN_ROW                                                               1

/**
 * Defines the row to search ATM.
 */
#define ATM_LOCATOR_ROW                                                         2

/**
 * Defines the contact us row position
 */
#define CONTACT_US_ROW                                                          3

/**
 * Defines the your opinion row
 */
#define RADIO_BBVA_ROW                                                          4

/**
 * Defines the lenght of username string
 */
#define USERNAME_LENGHT                                                         16


/**
 * Defines
 */
#define CALL_ALERT                                                              1

/**
 * Defines
 */
#define DELETE_FREQUENT_CARD_ALERT                                              2



#pragma mark -

/**
 * LoginViewController private extension
 */
@interface LoginViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releaseLoginViewControllerGraphicElements;

/**
 * Mobile banking cell action. Executed when user clicks the button inside the mobile banking cell. It shows or hides the
 * login information cell
 *
 * @private
 */
- (void)mobileBankingCellAction;

/**
 * Login action. Executed when user touches up inside the login button in the login information cell. It starts the login process with
 * the information provided by the user. Some checks are performed before the login process starts (e.g. user and password must not
 * be empty)
 *
 * @private
 */
- (void)loginAction;

/**
 * Password help action. Executed when user touches up inside the pasword help button in the login information cell. It navigates to the
 * password helper view
 *
 * @private
 */
- (void)passwordHelpAction;

/**
 * Suscribe action. Executed when user touches up inside the suscribe button in the login information cell. It navigates to suscribe view.
 *
 * @private
 */
- (void)suscribeAction;

/**
 * ATM and branches location action. Executed when user clicks the button inside the bank POIs locator cell. It starts user location and once
 * located, shows the map with the nearest branches and ATMs
 *
 * @private
 */
- (void)locateBankPOIsAction;

/**
 * Contact us cell action. Executed when user clicks the button inside the contact us cell. It shows or hides contact us options cells
 *
 * @private
 */
- (void)contactUsCellAction;

/**
 * Call customer cell action. Executed when user clicks the button inside the customer care call cell. On devices that can make a phone call (e.g. iPhone)
 * it shows the user a dialog to confirm the call. On devices that cannot make a phone call (e.g. iPod) shows a message with the phone number
 *
 * @private
 */
- (void)callCustomerCenterAction;

/**
 * Your opinion cell action. Executed when user clicks the button inside the your opinion cell. It displays the view associated to your opinion action
 *
 * @private
 */
- (void)yourOpinionCellAction;

- (void)radioBBVACellAction;

/**
 * Checks the user and password provided to perform the login operation. When user and/or password are not suitable for the login operation,
 * an error message is displayed
 *
 * @param aUserName The user name provided to perform the login operation
 * @param aPassword The password provided to perform the login operation
 * @param aliasCard The alias card provided to perform the login operation
 * @return YES if user and password are suitable for a login operation, NO otherwise
 * @private
 */
- (BOOL)checkLoginSuitabilityForUserName:(NSString *)aUserName password:(NSString *)aPassword
                               aliasCard:(NSString *) aliasCard;

/**
 * Invoked by framework when the login response is received. The response is analyzed and the coordinates view is displayed when needed
 *
 * @param notification The NSNotification instance containing the notification information
 * @private
 */
- (void)loginResponseReceived:(NSNotification *)notification;

/**
 * Normalizes the cell row depending on the elements collapsed or expanded
 *
 * @param originalRow The original cell row to normalize
 * @return The normalized cell row
 * @private
 */
- (NSUInteger)normalizeCellRow:(NSUInteger)originalRow;

@end



#pragma mark -

@implementation LoginViewController

#pragma mark -
#pragma mark Properties

@synthesize brandingHeader = brandingHeader_;
@synthesize actionsTableView = actionsTableView_;
@synthesize frequentCards = frequentCards_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc{

    [self releaseLoginViewControllerGraphicElements];
    
    [loginCoordinatesViewController_ release];
    loginCoordinatesViewController_ = nil;
    
    [editableViews_ release];
    editableViews_ = nil;
    
    [frequentCards_ release];
    frequentCards_ = nil;
    
    [super dealloc];
    
}

/*
 * Releases elements not needed when controller view is unloaded
 */
- (void)releaseLoginViewControllerGraphicElements {
    
    [brandingHeader_ release];
    brandingHeader_ = nil;
    
    [actionsTableView_ release];
    actionsTableView_ = nil;
    
    [mobileBankingActionCell_ release];
    mobileBankingActionCell_ = nil;
    
    [loginInfoCell_ release];
    loginInfoCell_ = nil;
    
    [bankPOILocatorActionCell_ release];
    bankPOILocatorActionCell_ = nil;
    
    [contactUsActionCell_ release];
    contactUsActionCell_ = nil;
    
    [callUsActionCell_ release];
    callUsActionCell_ = nil;
    
    [yourOpinionActionCell_ release];
    yourOpinionActionCell_ = nil;
    
    [loginInfoViewController_ release];
    loginInfoViewController_ = nil;
    
    [yourOpinionMattersViewController_ release];
    yourOpinionMattersViewController_ = nil;
    
    [poiMapViewController_ release];
    poiMapViewController_ = nil;
    
    [editableViews_ removeAllObjects];
    

}

/**
 * Called when the controller’s view is released from memory. Unneeded memory is released
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseLoginViewControllerGraphicElements];

}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseLoginViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Sets the show login information cell flag its initial value
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    loginInformationExpanded_ = NO;
    
}

/*
 * Creates and returns an autoreleased LoginViewController constructed from a NIB file
 */
+ (LoginViewController *)loginViewController {
    
    LoginViewController *result = [[[LoginViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory. View elements are initialized
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];

    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        self.navigationController.navigationBar.tintColor = [UIColor BBVABlueSpectrumColor];
        
    } else
    {
        self.navigationController.navigationBar.barTintColor = [UIColor BBVABlueSpectrumColor];
        self.navigationController.navigationBar.translucent = NO;
        
    }
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        CGFloat heightTableView = self.view.bounds.size.height - 48;
        brandingHeader_.image = [imagesCache imageNamed:BBVA_BRANDING_HEADER_IMAGE_FILE_NAME];
        brandingHeader_.frame = CGRectMake(0, 0, 320, 48);
        actionsTableView_.frame =  CGRectMake(0, 48, 320, heightTableView);
    } else
    {
        CGFloat heightTableView = self.view.bounds.size.height - 68;
        brandingHeader_.image = [imagesCache imageNamed:BBVA_BRANDING_HEADER_IOS7_IMAGE_FILE_NAME];
        brandingHeader_.frame = CGRectMake(0, 0, 320, 68);
        actionsTableView_.frame =  CGRectMake(0, 68, 320, heightTableView);
    }
    
    [mobileBankingActionCell_ release];
    mobileBankingActionCell_ = [[IconActionCell iconActionCell] retain];
    [mobileBankingActionCell_ setActionText:NSLocalizedString(MOBILE_BANKING_ACCESS_TEXT_KEY, nil)];
    [mobileBankingActionCell_ setActionIcon:[imagesCache imageNamed:MOBILE_BANK_ICON_IMAGE_FILE_NAME]];
    
    if (loginInformationExpanded_) {
        
        [mobileBankingActionCell_ setActionButtonImage:[imagesCache imageNamed:LESS_INFORMATION_ICON_IMAGE_FILE_NAME]];
        
    } else {
        
        [mobileBankingActionCell_ setActionButtonImage:[imagesCache imageNamed:MORE_INFORMATION_ICON_IMAGE_FILE_NAME]];
        
    }
    
    [mobileBankingActionCell_ addTarget:self action:@selector(mobileBankingCellAction)];
    
    [loginInfoCell_ release];
    loginInfoCell_ = [[LoginInfoCell loginInfoCell] retain];
    [loginInfoCell_ addLoginTarget:self action:@selector(loginAction)];
    [loginInfoCell_ addForgotPasswordTarget:self action:@selector(passwordHelpAction)];
    [loginInfoCell_ addSuscribeTarget:self action:@selector(suscribeAction)];
    
    //
    [loginInfoCell_ addFrequentCardCheckTarget:self action:@selector(infoAction)];
    [loginInfoCell_ addFrequentCardComboTarget:self action:@selector(frequentCardComboPressed)];
    
    [[loginInfoCell_ userTextField] setInputAccessoryView:popButtonsView_];
    [[loginInfoCell_ aliasTextField] setInputAccessoryView:popButtonsView_];
    [[loginInfoCell_ frequentCardCombo] setInputAccessoryView:popButtonsView_];
    [[loginInfoCell_ passwordTextField] setInputAccessoryView:popButtonsView_];
    
    [[loginInfoCell_ userTextField] setDelegate:self];
    [[loginInfoCell_ passwordTextField] setDelegate:self];
    [[loginInfoCell_ aliasTextField] setDelegate:self];
    
    [loginInfoCell_  frequentCardCombo].delegate = self;
    [loginInfoCell_  frequentCardCheck].delegate = self;
    
    [bankPOILocatorActionCell_ release];
    bankPOILocatorActionCell_ = [[IconActionCell iconActionCell] retain];
    [bankPOILocatorActionCell_ setActionText:NSLocalizedString(ATM_AND_BRANCHES_LOCATOR_TEXT_KEY, nil)];
    [bankPOILocatorActionCell_ setActionIcon:[imagesCache imageNamed:FIND_US_ICON_IMAGE_FILE_NAME]];
    [bankPOILocatorActionCell_ setActionButtonImage:[imagesCache imageNamed:ARROW_ICON_IMAGE_FILE_NAME]];
    [bankPOILocatorActionCell_ addTarget:self action:@selector(locateBankPOIsAction)];
    
    [contactUsActionCell_ release];
    contactUsActionCell_ = [[IconActionCell iconActionCell] retain];
    [contactUsActionCell_ setActionText:NSLocalizedString(CONTACT_US_TEXT_KEY, nil)];
    [contactUsActionCell_ setActionIcon:[imagesCache imageNamed:CUSTOMER_ICON_IMAGE_FILE_NAME]];
    [contactUsActionCell_ addTarget:self action:@selector(contactUsCellAction)];

    [callUsActionCell_ release];
    callUsActionCell_ = [[IconActionCell iconActionCell] retain];
    [callUsActionCell_ setActionText:NSLocalizedString(CALL_US_TEXT_KEY, nil)];
    [callUsActionCell_ setActionIcon:[imagesCache imageNamed:CUSTOMER_ICON_IMAGE_FILE_NAME]];
    [callUsActionCell_ setActionButtonImage:[imagesCache imageNamed:ARROW_ICON_IMAGE_FILE_NAME]];
    [callUsActionCell_ addTarget:self action:@selector(callCustomerCenterAction)];
    
    [yourOpinionActionCell_ release];
    yourOpinionActionCell_ = [[IconActionCell iconActionCell] retain];
    [yourOpinionActionCell_ setActionText:NSLocalizedString(YOUR_OPINION_TEXT_KEY, nil)];
    [yourOpinionActionCell_ setActionButtonImage:[imagesCache imageNamed:ARROW_ICON_IMAGE_FILE_NAME]];
    [yourOpinionActionCell_ addTarget:self action:@selector(yourOpinionCellAction)];
    yourOpinionActionCell_.displayImage = NO;
    [yourOpinionActionCell_ displayGrayBackground:YES];
    
    
    [radioBBVAActionCell_ release];
    radioBBVAActionCell_ = [[IconActionCell iconActionCell] retain];
    [radioBBVAActionCell_ setActionText:NSLocalizedString(RADIO_BBVA_OPTION_TEXT_KEY, nil)];
    radioBBVAActionCell_.actionLabel.textColor = [UIColor BBVABlueSpectrumToneOneColor];
    [radioBBVAActionCell_ setActionIcon:[imagesCache imageNamed:RADIO_BBVA_ICON_IMAGE_FILE_NAME]];
    [radioBBVAActionCell_ setActionButtonImage:[imagesCache imageNamed:ARROW_ICON_IMAGE_FILE_NAME]];


    
    
    //transparent scroll
    [self setScrollableView:actionsTableView_];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.origin.y = CGRectGetMinY(actionsTableView_.frame);
    scrollFrame.size.height = actionsTableView_.frame.size.height;
    [self setScrollNominalFrame:scrollFrame];
    
    CGRect containerFrame = actionsTableView_.frame;
    containerFrame.origin = CGPointZero;
    actionsTableView_.frame = containerFrame;
    
    [self.view bringSubviewToFront:brandingHeader_];
    
    transparentScroll_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:@"UITextFieldTextDidChangeNotification" object:[loginInfoCell_ userTextField]];
    
    
}

- (void)textFieldDidChange :(NSNotification *)notif
{
        
    if([frequentCards_ count]>0 && [[loginInfoCell_ frequentCardCombo] selectedIndex]>0){
            
        [[loginInfoCell_ frequentCardCombo] setSelectedIndex:0];
    }
}

/**
 * Notifies the view controller that its view is about to be become visible. The navigation bar is hidden
 *
 * @param animated If YES, the view is being added to the window using an animation
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginResponseReceived:) name:kNotificationLoginEnds object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginEncryptResponseReceived:) name:kNotificationLoginEncryptEnds object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDesencryptResponseReceived:) name:kNotificationLoginDesencryptEnds object:nil];

    //TODO: Uncomment to use last user name logged
//    NSString *maskedUsername = [Tools obfuscateUserName:[[self appDelegate] lastUsernameLogged]];
    NSString *maskedUsername = @"";
    [[loginInfoCell_ userTextField] setText:maskedUsername];
    [[loginInfoCell_ passwordTextField] setText:nil];
    
    
    frequentCards_ = [[TblFrequentCard fnGetInstanceFrequentCard] fnGetListFrequentCard];
    
    [loginInfoCell_ updateComponents:frequentCards_ stateAdd:NO];
    [self updateSelection];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];

    [super viewWillAppear:animated];
    
    [actionsTableView_ reloadData];
    
    if (loginInformationExpanded_) {
        
        [loginInfoCell_ setCellVisible:YES];
        
    } else {
        
        [loginInfoCell_ setCellVisible:NO];
        
    }
    
    [Tools checkTableScrolling:actionsTableView_];
    
    editableViews_ = [loginInfoCell_ updateEditableViews:editableViews_];
      
   
    if([[self appDelegate] deeplinkOption] == DEEPLINK_RADIO_BBVA){
        
        [[self appDelegate] setDeeplinkOption:DEEPLINK_NONE];
        
        [self radioBBVACellAction];
    }
}


/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view.
 *
 * @param animated: If YES, the disappearance of the view is being animated.
 */
- (void)viewWillDisappear:(BOOL)animated {

    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationLoginEnds object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationLoginEncryptEnds object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationLoginDesencryptEnds object:nil];
    
    [self.view endEditing:YES];
}
#pragma mark -
#pragma mark Cells actions

/*
 * Mobile banking cell action. Executed when user clicks the button inside the mobile banking cell. It shows or hides the
 * login information cell
 */
- (void)mobileBankingCellAction {
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    if (loginInformationExpanded_) {
        
        [loginInfoCell_ dismissKeyboard];
        
        loginInformationExpanded_ = NO;
        
        [actionsTableView_ beginUpdates];
        NSIndexPath *loginCellIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        NSArray *indexPathArray = [NSArray arrayWithObject:loginCellIndexPath];
        [actionsTableView_ deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationTop];
        [actionsTableView_ endUpdates];
        
        [loginInfoCell_ setCellVisible:NO];
        
        [mobileBankingActionCell_ setActionButtonImage:[imagesCache imageNamed:MORE_INFORMATION_ICON_IMAGE_FILE_NAME]];
        
    } else {
        
        loginInformationExpanded_ = YES;
        
        [actionsTableView_ beginUpdates];
        NSIndexPath *loginCellIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        NSArray *indexPathArray = [NSArray arrayWithObject:loginCellIndexPath];
        [actionsTableView_ insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationTop];
        [actionsTableView_ endUpdates];
        
        [loginInfoCell_ setCellVisible:YES];
        
        [mobileBankingActionCell_ setActionButtonImage:[imagesCache imageNamed:LESS_INFORMATION_ICON_IMAGE_FILE_NAME]];

    }
    
    [Tools checkTableScrolling:actionsTableView_];

}

/*
 * Login action. Executed when user touches up inside the login button in the login information cell. It starts the login process with
 * the information provided by the user. Some checks are performed before the login process starts (e.g. user and password must not
 * be empty)
 */
- (void)loginAction {
    
    NSString *user = [loginInfoCell_ userName];
    
	if (user != nil) {
        
		NSString *maskedUsername = [Tools obfuscateUserName:[[self appDelegate] lastUsernameLogged]];
        
		if ([user isEqualToString:maskedUsername]) {
            
            user = [[self appDelegate] lastUsernameLogged];
            
		}
        
	}

    
    NSString *password = [loginInfoCell_ password];
    
    BOOL isAliasHidden = [[loginInfoCell_ aliasTextField] isHidden];
    NSString *aliasCard = isAliasHidden ?
                            nil :
                            [[[loginInfoCell_ aliasTextField] text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    BOOL isNotSelectedFrequentCard = ([frequentCards_ count]==0 || [loginInfoCell_ frequentCardCombo].selectedIndex==0);
    BOOL isActiveSave = [loginInfoCell_ frequentCardCheck].checkActive;
    BOOL isNewFrequentCard = isNotSelectedFrequentCard;
    
    if ([self checkLoginSuitabilityForUserName:user password:password aliasCard:aliasCard]) {
        
        [loginInfoCell_ dismissKeyboard];
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        
        if(isNewFrequentCard){
            
            NSString *formattedUser = [Tools formatUsername:user];
            
            [[Updater getInstance] loginWithId:formattedUser andPassword:[loginInfoCell_ password]];
            
        }
        else{//reused
            
            NSString * token;
            int index = [[loginInfoCell_ frequentCardCombo] selectedIndex];
            if(index>0 && [frequentCards_ count]>0){
                
                FrequentCard * frequentCard = [frequentCards_ objectAtIndex:(index -1)];
                token = frequentCard.value;
                
            }
            
            [[Updater getInstance] loginDesencryptWithToken:token];
            
            
        }

    }
    
}

/**
 * Password help action. Executed when user touches up inside the pasword help button in the login information cell. It navigates to the
 * password helper view
 */
- (void)passwordHelpAction {
    
    /*if (loginInfoViewController_ == nil) {
     
     loginInfoViewController_ = [[LoginInfoViewController loginInfoViewController] retain];
     
     }
     
     [loginInfoViewController_ setType:lit_ForgotPassword];
     
     [[self navigationController] pushViewController:loginInfoViewController_ animated:YES];*/
    
    ChangePassStep1 *ChangePassStep1_ = [[ChangePassStep1 alloc] init];
    [self.navigationController pushViewController:ChangePassStep1_ animated:YES];
    
}

/*
 * Suscribe action. Executed when user touches up inside the suscribe button in the login information cell. It navigates to suscribe view.
 */
- (void)suscribeAction {
    //aqui
    //clic en incribete
    
    /*if (loginInfoViewController_ == nil) {
     
     loginInfoViewController_ = [[LoginInfoViewController loginInfoViewController] retain];
     
     }
     
     [loginInfoViewController_ setType:lit_Subscribe];
     
     [[self navigationController] pushViewController:loginInfoViewController_ animated:YES];*/
    
    
    
    SuscriptionStep1 *SuscriptionStep1_ = [[SuscriptionStep1 alloc] init];
    [self.navigationController pushViewController:SuscriptionStep1_ animated:YES];
    
    
    
    
}

/*
 * Info action. Executed when user touches up inside the info button in the login information cell. 
 */
- (void) infoAction {
    [Tools showAlertWithMessage:NSLocalizedString(FC_MESSAGE_INFO_TEXT_KEY, nil) title:@""];
}

/*
 * combo action. Executed when user touches up inside the combo button in the login information cell.
 */
- (void)frequentCardComboPressed {
    
    [self editableViewHasBeenClicked:[loginInfoCell_ frequentCardCombo]];
}

/*
 * ATM and branches location action. Executed when user clicks the button inside the bank POIs locator cell. It starts user location and once
 * located, shows the map with the nearest branches and ATMs
 */
- (void)locateBankPOIsAction {
    
    if (poiMapViewController_ == nil) {
        
        poiMapViewController_ = [[POIMapViewController poiMapViewController] retain];
        
    }
    
    [[self navigationController] pushViewController:poiMapViewController_ animated:YES];

}

/*
 * Contact us cell action. Executed when user clicks the button inside the contact us cell. It shows or hides contact us options cells
 */
- (void)contactUsCellAction {
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    NSUInteger rowCorrection = 0;
    
    if (loginInformationExpanded_) {
        
        rowCorrection ++;
        
    }
    
    [Tools checkTableScrolling:actionsTableView_];

}

/*
 * Call customer center action. Executed when user clicks the blue button inside the customer care call cell. On devices that can make a phone call (e.g. iPhone)
 * it shows the user a dialog to confirm the call. On devices that cannot make a phone call (e.g. iPod) shows a message with the phone number
 */
- (void)callCustomerCenterAction {
    
    [[self view] endEditing:YES];
    
    UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:nil
                                                     message:[NSString stringWithFormat:NSLocalizedString(CALL_TO_POI_PHONE_TEXT_KEY, nil), BBVA_LINE_PHONE] 
                                                    delegate:self 
                                           cancelButtonTitle:NSLocalizedString(CANCEL_TEXT_BUTTON_KEY, nil) 
                                           otherButtonTitles:NSLocalizedString(OK_TEXT_KEY, nil), nil] autorelease];
    alert.tag = CALL_ALERT;
    
    [alert show];
    
}

/*
 * Your Radio BBVA cell action. Executed when user clicks the button inside the your go to radio cell. It displays the view associated to your radio bbva action
 */
- (void)radioBBVACellAction {
    
    [[Updater getInstance] processMCBNotifications:MAT_LOG_RADIO_BBVA_OPERATION];
    
    if([self.appDelegate.radioStreamManager isPlayingRadio]){
        [self hideSplash];
    }
    else{
    
    
    if (splashRadioViewController_ == nil) {
        
        splashRadioViewController_ = [[SplashRadioViewController splashRadioViewController] retain];
        
    }
     splashRadioViewController_.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:splashRadioViewController_ animated:YES];
    [self performSelector:@selector(hideSplash) withObject:nil afterDelay:2.0];

    }
    
    
    
}


//hide splash screen

- (void)hideSplash{
 
    if (slideOutNavigationViewController_ == nil) {
        
        slideOutNavigationViewController_ = [[SlideOutNavigationViewController alloc] init];
        
    }
    
    [[self navigationController] pushViewController:slideOutNavigationViewController_ animated:YES];

    [[self modalViewController] dismissModalViewControllerAnimated:YES];

    
}

/*
 * Your opinion cell action. Executed when user clicks the button inside the your opinion cell. It displays the view associated to your opinion action
 */
- (void)yourOpinionCellAction {
    
    if (yourOpinionMattersViewController_ == nil) {
        
        yourOpinionMattersViewController_ = [[YourOpinionMattersViewController yourOpinionMattersViewController] retain];
        
    }
    
    [[self navigationController] pushViewController:yourOpinionMattersViewController_ animated:YES];
    
}

#pragma mark -
#pragma mark UIAlertViewDelegate methods

/**
 * Sent to the delegate when the user clicks a button on an alert view.
 *
 * @param alertView: The alert view containing the button.
 * @param buttonIndex: The index of the button that was clicked.
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
            
        case 1:
            
            if(alertView.tag == CALL_ALERT){
            
                [Tools startPhoneCallToNumber:BBVA_LINE_PHONE];
            }
            else if(alertView.tag == DELETE_FREQUENT_CARD_ALERT){
                
                [self deleteFrequentCard];
                
            }
            
            break;
            
        default:
            
            if(alertView.tag == DELETE_FREQUENT_CARD_ALERT){
                
                [loginInfoCell_ updateComponents:frequentCards_ stateAdd:NO];
            }
            
            break;
            
    }
    
}

#pragma mark -
#pragma mark Checking information

/*
 * Checks the user and password provided to perform the login operation. When user and/or password are not suitable for the login operation,
 * an error message is displayed
 */
- (BOOL)checkLoginSuitabilityForUserName:(NSString *)aUserName password:(NSString *)aPassword
            aliasCard:(NSString *) aliasCard{
    
    BOOL result = YES;
    
    NSString *errorMessage = nil;
    
    NSUInteger userNameLength = [aUserName length];
    NSUInteger passwordLength = [aPassword length];
    
    
    if ((userNameLength == 0) && (passwordLength == 0)) {
        
        result = NO;
        errorMessage = NSLocalizedString(LOGIN_ERROR_USER_AND_PASSWORD_EMPTY_KEY, nil);
        
    } else if (userNameLength == 0) {
        
        result = NO;
        errorMessage = NSLocalizedString(LOGIN_ERROR_USER_EMPTY_KEY, nil);
        
    } else if (userNameLength < USERNAME_LENGHT) {
        
        result = NO;
        errorMessage = NSLocalizedString(LOGIN_ERROR_INVALID_USER_KEY, nil);
    
    } else if (passwordLength == 0) {
        
        result = NO;
        errorMessage = NSLocalizedString(LOGIN_ERROR_PASSWORD_EMPTY_KEY, nil);
		
	} else if(aliasCard){
        
        NSUInteger aliasLength = [aliasCard length];
        if (aliasLength == 0) {
            
            result = NO;
            errorMessage = NSLocalizedString(LOGIN_ERROR_ALIAS_EMPTY_KEY, nil);
            
        }
    }
    
    if (result == NO) {
        
        [Tools showInfoWithMessage:errorMessage];
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Normalizes the cell row depending on the elements collapsed or expanded
 */
- (NSUInteger)normalizeCellRow:(NSUInteger)originalRow {
    
    NSUInteger result = originalRow;
    
    if ((!loginInformationExpanded_) && (result > 0)) {
        
        result ++;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Notifications management

/*
 * The login response has been received via NSNotification
 */
- (void)loginResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    GeneralLoginResponse *loginResponse = [notification object];
    
    if (![loginResponse isError]) {
        
        if (![[loginResponse status] isEqualToString:@"OK"]) {
            
            [[loginInfoCell_ passwordTextField] setText:@""];
            
            NSString *errorMessage = [loginResponse loginErrorCode];
            
            [Tools showErrorWithMessage:errorMessage];
            [[self appDelegate] hideActivityIndicator];

            
        } else {
            
            NSString *user = [loginInfoCell_ userName];
            
            if (user != nil) {
                
                NSString *maskedUsername = [Tools obfuscateUserName:[[self appDelegate] lastUsernameLogged]];
                
                if ([user isEqualToString:maskedUsername]) {
                    
                    user = [[self appDelegate] lastUsernameLogged];
                    
                }
            }
            
            [[self appDelegate] setLastUsernameLogged:user];
            [[self appDelegate] saveUserPreferences];
            
            
            
            BOOL isNotSelectedFrequentCard = ([frequentCards_ count]==0 || [loginInfoCell_ frequentCardCombo].selectedIndex==0);
            BOOL isActiveSave = [loginInfoCell_ frequentCardCheck].checkActive;
            BOOL isNewFrequentCard = isNotSelectedFrequentCard && isActiveSave;
            
            if(isNewFrequentCard){
                
                NSString *aliasCard = [[[loginInfoCell_ aliasTextField] text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                [[self appDelegate] showActivityIndicator:poai_Both];
                [[Updater getInstance] loginEncryptWithId:[loginInfoCell_ userName] andAlias:aliasCard];
            }
            else{
                
                [self updateLastFrequentCard];
                [[self appDelegate] loginPerformed];
            }
            
        }
        
    } else {
    
        [[loginInfoCell_ passwordTextField] setText:@""];
        
        [[self appDelegate] hideActivityIndicator];
    
    }
	
}

- (void)loginEncryptResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    GeneralLoginEncryptResponse *resp = [notification object];
    
    if(resp.response) {
        
        NSString * token = resp.response;
        [self saveFrequentCard:token];
        [[self appDelegate] loginPerformed];
        
	} else {
        
        [Tools showErrorWithMessage:resp.errorMessage];
        
    }
    
}

- (void)loginDesencryptResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    GeneralLoginEncryptResponse *resp = [notification object];
    
    if( resp.response ) {
        
        NSString * user = [resp.response substringToIndex:16];
        
        NSString *formattedUser = [Tools formatUsername:user];
        
		[[self appDelegate] showActivityIndicator:poai_Both];
        
        [[Updater getInstance] loginWithId:formattedUser andPassword:[loginInfoCell_ password]];
        
	} else {
        
        [Tools showErrorWithMessage:resp.errorMessage];
        
    }
    
}

#pragma mark -
#pragma mark Table view data source

/**
 * Asks the data source to return the number of sections in the table view. The action table only has one section
 *
 * @param tableView An object representing the table view requesting this information.
 * @return Always 1
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view. When no options are expanded, only 3 cells are available,
 * when the login information cell is expanded, then an additional cell is diplayed, and when contact us options are expanded, two additional
 * cells are displayed
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section in tableView
 * @return The number of visible action cells
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = 4;
    
    if (loginInformationExpanded_) {
        
        count ++;
        
    }
    
    return count;
    
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. Cell type depends on its
 * location inside the table, but all of them are precreated
 *
 * @param tableView A table-view object requesting the cell
 * @param indexPath An index path locating a row in tableView
 * @return The MoreTabViewCell instance to represent the tab element
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger row = indexPath.row;
    row = [self normalizeCellRow:row];
    
    UITableViewCell *cell = nil;
    
    switch (row) {
            
        case MOBILE_BANKING_ROW: {
            
            cell = mobileBankingActionCell_;
            break;
            
        }
            
        case LOGIN_ROW: {
            
            cell = loginInfoCell_;
            break;
            
        }
            
        case ATM_LOCATOR_ROW: {
            
            cell = bankPOILocatorActionCell_;
            break;
            
        }
            
        case CONTACT_US_ROW: {
            
            cell = callUsActionCell_;
            break;
            
        }
            
            
        default: {
            
            break;
            
        }
            
    }
    
    if(row == RADIO_BBVA_ROW) {
        
        cell = radioBBVAActionCell_;
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];

        
    }


    

    
    return cell;

}

#pragma mark -
#pragma mark UITableViewDelegate methods

/**
 * Asks the delegate for the height to use for a row in a specified location. Depending on the cell type it has a different size
 *
 * @param tableView The table-view object requesting this information
 * @param indexPath An index path that locates a row in tableView
 * @return A floating-point value that specifies the height (in points) that row should be
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat result = 0.0f;
    
    NSInteger row = indexPath.row;
    row = [self normalizeCellRow:row];
    
    switch (row) {
            
        case RADIO_BBVA_ROW:
        case MOBILE_BANKING_ROW:
        case ATM_LOCATOR_ROW:
        case CONTACT_US_ROW: {
            
            result = [IconActionCell cellHeight];
            break;
            
        }
            
        case LOGIN_ROW: {
            
            result = [loginInfoCell_ cellHeight];
            break;
            
        }            
        default: {
            
        }
            
    }
    
    return result;
    
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger row = [indexPath row];
    row = [self normalizeCellRow:row];
    
    switch (row) {
            
        case MOBILE_BANKING_ROW: {
            
            [self mobileBankingCellAction];
            break;
            
        }
            
        case ATM_LOCATOR_ROW: {
            
            [self locateBankPOIsAction];
            break;
            
        }
            
        case CONTACT_US_ROW: {

            [self callCustomerCenterAction];
            break;
            
        }
            
        case RADIO_BBVA_ROW:
             [self radioBBVACellAction];
            break;
            
        default: {
            
            break;
            
        }
            
    }
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [self customNavigationItem];
    
    return result;
    
}


#pragma - UITextfieldDelegate
/*
 **
 * the delegate specified the textField begin editing.
 *
 * @param textField: The text field containing the text.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField{

    [self editableViewHasBeenClicked:textField];
}

/*
**
* Asks the delegate if the specified text should be changed.
*
* @param textField: The text field containing the text.
* @param range: The range of characters to be replaced
* @param string: The replacement string.
* @return YES if the specified text range should be replaced; otherwise, NO to keep the old text.
*/
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return [loginInfoCell_ textField:textField shouldChangeCharactersInRange:range replacementString:string];
}

/**
 * Asks the delegate if the text field should process the pressing of the return button. Switches from the user to the password text field or
 * dismisses the keyboard, depending on the text field currently active
 *
 * @param textField The text field whose return button was pressed
 * @return Always NO, as the delegate manages the event
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    return [loginInfoCell_ textFieldShouldReturn:textField];
    
}


#pragma mark -
#pragma mark CheckViewDelegate methods

- (void)checkView:(CheckView *)checkView checkStateChanged:(BOOL)isSelected{
    
    BOOL isRegister = ([frequentCards_ count]==0 || [loginInfoCell_ frequentCardCombo].selectedIndex==0);
    if(isRegister){
        if(isSelected){
            [loginInfoCell_ aliasTextField].text = @"";
            [loginInfoCell_ aliasTextField].hidden = NO;
        }
        else{
            [loginInfoCell_ aliasTextField].hidden = YES;
        }
    }
    else{
        if(isSelected){
            
            [[self view] endEditing:YES];
            
            UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:nil
                                                             message:NSLocalizedString(CONFIRM_DELETE_FREQUENT_OPERATION_TEXT_KEY, nil)
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(CANCEL_TEXT_BUTTON_KEY, nil)
                                                   otherButtonTitles:NSLocalizedString(OK_TEXT_KEY, nil), nil] autorelease];
            alert.tag = DELETE_FREQUENT_CARD_ALERT;
            
            [alert show];
        }
    }
    
    editableViews_ = [loginInfoCell_ updateEditableViews:editableViews_];
}

#pragma mark -
#pragma mark NXTComboButtonDelegate methods
- (void)NXTComboButtonValueSelected:(NXTComboButtonLogin *)comboButton{
    
    //DLog(@"NXTComboButtonValueSelected");
    
    int32_t index = [comboButton selectedIndex];
    if(index == 0){
        [loginInfoCell_ updateComponents:frequentCards_ stateAdd:YES];
        [loginInfoCell_ userTextField].text = @"";
        [loginInfoCell_ passwordTextField].text = @"";
    }
    else{
        [loginInfoCell_ updateComponents:frequentCards_ stateAdd:NO];
        
        FrequentCard * frequentCard = [frequentCards_ objectAtIndex:(index-1)];
        [loginInfoCell_ userTextField].text = frequentCard.masked;
        [loginInfoCell_ passwordTextField].text = @"";
        
    }
    
    editableViews_ = [loginInfoCell_ updateEditableViews:editableViews_];
    
}

#pragma mark -
#pragma mark Frequent card
-(void)saveFrequentCard:(NSString*)cardNumber{
    BOOL isRegister = ([frequentCards_ count]==0 || [loginInfoCell_ frequentCardCombo].selectedIndex==0);
    BOOL isActiveSave = [loginInfoCell_ frequentCardCheck].checkActive;
    
    if(isRegister && isActiveSave){
        
        
        NSString * alias = [[[loginInfoCell_ aliasTextField] text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        FrequentCard * frequentCard = [[FrequentCard alloc] init];
        frequentCard.alias = alias;
        frequentCard.value = cardNumber;
        frequentCard.masked = [Tools obfuscateUserNameLarge:[loginInfoCell_ userName]];
        
        [[TblFrequentCard fnGetInstanceFrequentCard] fnInsertFrequentCard:frequentCard];
        
        frequentCards_ = [[TblFrequentCard fnGetInstanceFrequentCard] fnGetListFrequentCard];
        [loginInfoCell_ updateComponents:frequentCards_ stateAdd:NO];
            
            
        [self updateSelection];
            
        editableViews_ = [loginInfoCell_ updateEditableViews:editableViews_];
            
        [[loginInfoCell_ frequentCardCombo] reloadInputViews];
        
        [actionsTableView_ reloadData];

        
    }
    
}

-(void)updateLastFrequentCard{
    
    BOOL isReUsed = ([frequentCards_ count]>0 || [loginInfoCell_ frequentCardCombo].selectedIndex>0);
    if(isReUsed){
        int32_t selectedIndex = [loginInfoCell_ frequentCardCombo].selectedIndex;
    
        if(selectedIndex <= [frequentCards_ count] && selectedIndex > 0){
            FrequentCard * frequentCard = [frequentCards_ objectAtIndex:(selectedIndex-1)];
            [[TblFrequentCard fnGetInstanceFrequentCard] fnUpdateLastFrequentCard:frequentCard];
        }
    
        [self updateSelection];
        [[loginInfoCell_ frequentCardCombo] reloadInputViews];
    }
    
}

-(void)deleteFrequentCard{
    
    int32_t selectedIndex = [loginInfoCell_ frequentCardCombo].selectedIndex;
    FrequentCard * frequentCard = [frequentCards_ objectAtIndex:(selectedIndex-1)];
    
    [[TblFrequentCard fnGetInstanceFrequentCard] fnRemoveFrequentCard:frequentCard];
    frequentCards_ = [[TblFrequentCard fnGetInstanceFrequentCard] fnGetListFrequentCard];
    
    [loginInfoCell_ updateComponents:frequentCards_ stateAdd:NO];
    
    if([frequentCards_ count]>0){
        [[loginInfoCell_ frequentCardCombo] setSelectedIndex:0];
    }
    else{
        [actionsTableView_ reloadData];
    }
    
    [loginInfoCell_ userTextField].text = @"";
    [loginInfoCell_ passwordTextField].text = @"";
    
    editableViews_ = [loginInfoCell_ updateEditableViews:editableViews_];
    
    [[loginInfoCell_ frequentCardCombo] reloadInputViews];
    
    
}

-(void) updateSelection{
    
    if([frequentCards_ count]>0){
        
        int index = 1;
        BOOL seletecd = NO;
        for (FrequentCard * frequentCard in frequentCards_) {
            
            if([frequentCard.flag_last_user intValue]==1){
             
                [[loginInfoCell_ frequentCardCombo] setSelectedIndex:index];
                
                [loginInfoCell_ userTextField].text = frequentCard.masked;
                [loginInfoCell_ passwordTextField].text = @"";
                
                [[loginInfoCell_ frequentCardCombo] setTitle:frequentCard.alias];
                
                seletecd = YES;
            }
            
            index++;
        }
        
        if(!seletecd){
            
            if([frequentCards_ count]>0){
                [[loginInfoCell_ frequentCardCombo] setSelectedIndex:1];
                FrequentCard * frequentCard = [frequentCards_ objectAtIndex:0];
                
                [loginInfoCell_ userTextField].text = frequentCard.masked;
                [loginInfoCell_ passwordTextField].text = @"";
                [[loginInfoCell_ frequentCardCombo] setTitle:frequentCard.alias];
            }

        }
        
    }
    else{
        [loginInfoCell_ userTextField].text = @"";
        [loginInfoCell_ passwordTextField].text = @"";
    }
}

@end
