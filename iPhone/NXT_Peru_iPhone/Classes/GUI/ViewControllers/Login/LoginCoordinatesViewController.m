/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "LoginCoordinatesViewController.h"
#import "NXTEditableViewController+protected.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "StringKeys.h"
#import "Tools.h"
#import "NXTTextField.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "Updater.h"
#import "UIColor+BBVA_Colors.h"
#import "CoordinateResponse.h"
#import "SectionHeader.h"


#define NIB_FILE_NAME                                           @"LoginCoordinatesViewController"

/**
 * Defines the delay to made coordinates text field the first responder (this avoids a funny movement the first time the view controller is displayed)
 */
#define BECOME_FIRST_RESPONDER_DELAY                            0.3f


#pragma mark -

/**
 * LoginCoordinatesViewController private extension
 */
@interface LoginCoordinatesViewController()

/**
 * Releases graphic elements
 *
 * @private
 */
- (void)releasesLoginCoordinatesViewControllerGraphicElements;

/**
 * The login response has been received via NSNotification
 *
 * @param notification The notification
 * @private
 */
- (void)loginResponseReceived:(NSNotification *)notification;

/**
 * Ends the editing of the view
 *
 * @private
 */
- (void)endEditing;

@end


#pragma mark -

@implementation LoginCoordinatesViewController

#pragma mark -
#pragma mark Properties

@synthesize infoLabel = infoLabel_;
@synthesize requiredCoordinateLabel = requiredCoordinateLabel_;
@synthesize coordinateTextField = coordinateTextField_;
@synthesize separatorImageView = separatorImageView_;
@synthesize acceptButton = acceptButton_;
@synthesize brandingImageView = brandingImageView_;
@synthesize coordinate = coordinate_;

#pragma mark -
#pragma mark Memory management

/*
 * Releases memory
 */
- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationCoordinateLoginUpdated
                                                  object:nil];

	[self releasesLoginCoordinatesViewControllerGraphicElements];
    
    [doneButton_ release];
    doneButton_ = nil;
    
    [coordinate_ release];
    coordinate_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasesLoginCoordinatesViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/**
 * Releases graphic elements
 */
- (void)releasesLoginCoordinatesViewControllerGraphicElements {
    
    [sectionHeader_ release];
    sectionHeader_ = nil;
    
    [infoLabel_ release];
    infoLabel_ = nil;
    
    [requiredCoordinateLabel_ release];
    requiredCoordinateLabel_ = nil;
    
    [coordinateTextField_ release];
    coordinateTextField_ = nil;
    
    [separatorImageView_ release];
    separatorImageView_ = nil;
    
    [acceptButton_ release];
    acceptButton_ = nil;
    
    [brandingImageView_ release];
    brandingImageView_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased LoginCoordinatesViewController constructed from a NIB file
 */
+ (LoginCoordinatesViewController *)loginCoordinatesViewController {
    
    LoginCoordinatesViewController *result = [[[LoginCoordinatesViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark View management

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIView *view = self.view;
    
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    if (sectionHeader_ == nil) {
        
        sectionHeader_ = [[SectionHeader sectionHeader] retain];
        CGRect frame = sectionHeader_.frame;
        frame.origin.x = 0.0f;
        frame.origin.y = 0.0f;
        frame.size.width = view.frame.size.width;
        sectionHeader_.frame = frame;
        [view addSubview:sectionHeader_];
        
    }
    
    sectionHeader_.leftText = NSLocalizedString(COORDINATE_REQUEST_HEADER_TEXT_KEY, nil);
    
    [NXT_Peru_iPhoneStyler styleLabel:infoLabel_ withFontSize:14.0f color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:requiredCoordinateLabel_ withFontSize:25.0f color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleTextField:coordinateTextField_ withFontSize:14.0f andColor:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleBlueButton:acceptButton_];
    
    infoLabel_.text = NSLocalizedString( COORDINATE_REQUEST_INFO_TEXT_KEY, nil );
    
    separatorImageView_.image = [imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    [acceptButton_ setTitle:NSLocalizedString(LOGIN_TEXT_KEY, nil)
                   forState:UIControlStateNormal];
    
    coordinateTextField_.fillFormMessage = NSLocalizedString(COORDINATE_REQUEST_PASSWORD_EMPTY_ERROR_TEXT, nil);
    
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
    [brandingImageView_ removeFromSuperview];
    [self setSubviewsIntoScroller];
    [[self view] addSubview:brandingImageView_];
    
    if (editableViews_ == nil) {
        
        editableViews_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [editableViews_ removeAllObjects];
        
    }
    
    [editableViews_ addObject:coordinateTextField_];
    
}

/**
 * Called when the controller’s view is released from memory. Graphic elements are released
 */
- (void)viewDidUnload {
    
	[self releasesLoginCoordinatesViewControllerGraphicElements];
    [super viewDidUnload];
	
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    UINavigationController *navigationController = self.navigationController;
    [navigationController setNavigationBarHidden:NO animated:YES];
    
    requiredCoordinateLabel_.text = coordinate_;
    coordinateTextField_.text = nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginResponseReceived:)
                                                 name:kNotificationCoordinateLoginUpdated
                                               object:nil];
    
}

/**
 * Notifies the view controller that its view was added to a window. The text field is made the first responder
 *
 * @param animated If YES, the view was added to the window using an animation
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [coordinateTextField_ becomeFirstResponder];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. A blank web page is loaded
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationCoordinateLoginUpdated
                                                  object:nil];
    
    [self.view endEditing:YES];
    
    [[self navigationController] popToRootViewControllerAnimated:NO];
    
}

#pragma mark -
#pragma mark User interactions

/*
 * Invoked by framework when user taps on the accept button. The coordinate is checked agains the server
 */
- (IBAction)acceptButtonTapped {
    
    if ([self validateForm]) {
        
        [[self view] endEditing:YES];
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        
        [[Updater getInstance] loginWithCoordinate:coordinateTextField_.text];
        
    }
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Ends the editing of the view
 */
- (void)endEditing {

    [self.view endEditing:YES];
    
    self.navigationItem.rightBarButtonItem = nil;

}

#pragma mark -
#pragma mark Notifications management

/*
 * The login response has been received via NSNotification
 */
- (void)loginResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    CoordinateResponse *resp = [notification object];
    
    if( [resp.status isEqualToString:@"OK"] ) {
        
        [[self appDelegate] loginPerformed];
        
	} else {
        
        [Tools showErrorWithMessage:resp.errorMessage];
        
    }
    
}

#pragma mark -
#pragma mark UITextFieldDelegate methods

/**
 * Asks the delegate if editing should begin in the specified text field. The done button is set to the navigation bar right side
 *
 * @param textField The text field for which editing is about to begin
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    self.navigationItem.rightBarButtonItem = doneButton_;
    
    return YES;
    
}

/**
 * Asks the delegate if the text field should process the pressing of the return button. Switches from the user to the password text field or
 * dismisses the keyboard, depending on the text field currently active
 *
 * @param textField The text field whose return button was pressed
 * @return Always NO, as the delegate manages the event
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [coordinateTextField_ resignFirstResponder];
    
    [acceptButton_ sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    return NO;
    
}

/**
 * Check max length of text fields.
 *
 * @param textField The text field containing the text.
 * @param range The range of characters to be replaced
 * @param string The replacement string.
 * @return YES if the specified text range should be replaced; otherwise, NO to keep the old text.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = YES;
    
    if (textField == coordinateTextField_) {
        
        NSCharacterSet *numbersSet = [NSCharacterSet decimalDigitCharacterSet];
        
        NSInteger length = [string length];
        
        if (length > 0) {
            
            unichar numberExtracted = [string characterAtIndex:length-1];
            
            if (![numbersSet characterIsMember:numberExtracted]) {
                
                result = NO;
                
            }
            
        }
        
        if (result) {
            
            NSString *resultString = [coordinateTextField_.text stringByReplacingCharactersInRange:range
                                                                                        withString:string];
            
            if ([resultString length] > COORDINATES_MAXIMUM_LENGHT) {
                
                result = NO;
                
            }
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = NSLocalizedString(COORDINATE_REQUEST_TITLE_TEXT_KEY, nil);
    
    if (doneButton_ == nil) {
        
        doneButton_ = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                      target:self
                                                      action:@selector(endEditing)];
        
    }
    
    return result;
    
}

@end
