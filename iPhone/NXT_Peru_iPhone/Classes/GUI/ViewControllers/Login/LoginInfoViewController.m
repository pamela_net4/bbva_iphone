/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "LoginInfoViewController.h"

#import "Constants.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"LoginInfoViewController"

#define STEP_ONE                                                    @"1.-"
#define STEP_TWO                                                    @"2.-"
#define STEP_THREE                                                  @"3.-"
#define STEP_FOUR                                                   @"4.-"

@interface LoginInfoViewController (Private)

#pragma mark -
#pragma mark Private methods

/**
 * Method to show the view information.
 *
 * @private
 */
- (void)showViewInformation;

/**
 * Releases all graphic elements contained on this view controller
 *
 * @private
 */
- (void)releaseGraphicElements;

@end

@implementation LoginInfoViewController

#pragma mark -
#pragma mark Properties

@synthesize signupView = signupView_;
@synthesize forgotPasswordView = forgotPasswordView_;
@synthesize signupHeaderLabel = signupHeaderLabel_;
@synthesize singup1Label = singup1Label_;
@synthesize signupStep1Label = signupStep1Label_;
@synthesize signupURLButton = signupURLButton_;
@synthesize singup2Label = singup2Label_;
@synthesize signupStep2Label = signupStep2Label_;
@synthesize singup3Label = singup3Label_;
@synthesize signupStep3Label = signupStep3Label_;
@synthesize signupStep3BoldLabel = signupStep3BoldLabel_;
@synthesize singup4Label = singup4Label_;
@synthesize signupStep4Label = signupStep4Label_;
@synthesize signupStep4BoldLabel = signupStep4BoldLabel_;
@synthesize signupBackgroundImageView = signupBackgroundImageView_;
@synthesize forgotPasswordHeaderLabel = forgotPasswordHeaderLabel_;
@synthesize forgotPassword1Label = forgotPassword1Label_;
@synthesize forgotPasswordStep1Label = forgotPasswordStep1Label_;
@synthesize forgotPasswordURLButton = forgotPasswordURLButton_;
@synthesize forgotPassword2Label = forgotPassword2Label_;
@synthesize forgotPasswordStep2Label = forgotPasswordStep2Label_;
@synthesize forgotPasswordStep2BoldLabel = forgotPasswordStep2BoldLabel_;
@synthesize forgotPassword3Label = forgotPassword3Label_;
@synthesize forgotPasswordStep3Label = forgotPasswordStep3Label_;
@synthesize forgotPasswordStep3BoldLabel = forgotPasswordStep3BoldLabel_;
@synthesize forgotPasswordBackgroundImageView = forgotPasswordBackgroundImageView_;
@synthesize type = type_;
@synthesize extraDisclaimer = extraDisclaimer_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [signupView_ release];
    signupView_ = nil;
    
    [forgotPasswordView_ release];
    forgotPasswordView_ = nil;
    
    [signupHeaderLabel_ release];
    signupHeaderLabel_ = nil;
    
    [singup1Label_ release];
    singup1Label_ = nil;
    
    [signupStep1Label_ release];
    signupStep1Label_ = nil;
    
    [signupURLButton_ release];
    signupURLButton_ = nil;
    
    [singup2Label_ release];
    singup2Label_ = nil;
    
    [signupStep2Label_ release];
    signupStep2Label_ = nil;
    
    [singup3Label_ release];
    singup3Label_ = nil;
    
    [signupStep3Label_ release];
    signupStep3Label_ = nil;
    
    [signupStep3BoldLabel_ release];
    signupStep3BoldLabel_ = nil;
    
    [singup4Label_ release];
    singup4Label_ = nil;
    
    [signupStep4Label_ release];
    signupStep4Label_ = nil;
    
    [signupStep4BoldLabel_ release];
    signupStep4BoldLabel_ = nil;
    
    [signupBackgroundImageView_ release];
    signupBackgroundImageView_ = nil;
    
    [forgotPasswordHeaderLabel_ release];
    forgotPasswordHeaderLabel_ = nil;
    
    [forgotPassword1Label_ release];
    forgotPassword1Label_ = nil;
    
    [forgotPasswordStep1Label_ release];
    forgotPasswordStep1Label_ = nil;
    
    [forgotPasswordURLButton_ release];
    forgotPasswordURLButton_ = nil;
    
    [forgotPassword2Label_ release];
    forgotPassword2Label_ = nil;
    
    [forgotPasswordStep2Label_ release];
    forgotPasswordStep2Label_ = nil;
    
    [forgotPasswordStep2BoldLabel_ release];
    forgotPasswordStep2BoldLabel_ = nil;
    
    [forgotPassword3Label_ release];
    forgotPassword3Label_ = nil;
    
    [forgotPasswordStep3Label_ release];
    forgotPasswordStep3Label_ = nil;
    
    [forgotPasswordStep3BoldLabel_ release];
    forgotPasswordStep3BoldLabel_ = nil;
    
    [forgotPasswordBackgroundImageView_ release];
    forgotPasswordBackgroundImageView_ = nil;
    
    [extraDisclaimer_ release];
    extraDisclaimer_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases all graphic elements contained on this view controller
 */
- (void)releaseGraphicElements {
    
    [signupView_ release];
    signupView_ = nil;
    
    [forgotPasswordView_ release];
    forgotPasswordView_ = nil;
    
    [signupHeaderLabel_ release];
    signupHeaderLabel_ = nil;
    
    [singup1Label_ release];
    singup1Label_ = nil;
    
    [signupStep1Label_ release];
    signupStep1Label_ = nil;
    
    [signupURLButton_ release];
    signupURLButton_ = nil;
    
    [singup2Label_ release];
    singup2Label_ = nil;
    
    [signupStep2Label_ release];
    signupStep2Label_ = nil;
    
    [singup3Label_ release];
    singup3Label_ = nil;
    
    [signupStep3Label_ release];
    signupStep3Label_ = nil;
    
    [signupStep3BoldLabel_ release];
    signupStep3BoldLabel_ = nil;
    
    [singup4Label_ release];
    singup4Label_ = nil;
    
    [signupStep4Label_ release];
    signupStep4Label_ = nil;
    
    [signupStep4BoldLabel_ release];
    signupStep4BoldLabel_ = nil;
    
    [signupBackgroundImageView_ release];
    signupBackgroundImageView_ = nil;
    
    [forgotPasswordHeaderLabel_ release];
    forgotPasswordHeaderLabel_ = nil;
    
    [forgotPassword1Label_ release];
    forgotPassword1Label_ = nil;
    
    [forgotPasswordStep1Label_ release];
    forgotPasswordStep1Label_ = nil;
    
    [forgotPasswordURLButton_ release];
    forgotPasswordURLButton_ = nil;
    
    [forgotPassword2Label_ release];
    forgotPassword2Label_ = nil;
    
    [forgotPasswordStep2Label_ release];
    forgotPasswordStep2Label_ = nil;
    
    [forgotPasswordStep2BoldLabel_ release];
    forgotPasswordStep2BoldLabel_ = nil;
    
    [forgotPassword3Label_ release];
    forgotPassword3Label_ = nil;
    
    [forgotPasswordStep3Label_ release];
    forgotPasswordStep3Label_ = nil;
    
    [forgotPasswordStep3BoldLabel_ release];
    forgotPasswordStep3BoldLabel_ = nil;
    
    [forgotPasswordBackgroundImageView_ release];
    forgotPasswordBackgroundImageView_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
}

/*
 * Creates and returns an autoreleased LoginInfoViewController constructed from a NIB file.
 */
+ (LoginInfoViewController *)loginInfoViewController {
    
    LoginInfoViewController *result =  [[[LoginInfoViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    [NXT_Peru_iPhoneStyler styleLabel:signupHeaderLabel_ withBoldFontSize:14.0f color:[UIColor BBVACoolGreyColor]];
    //[NXT_Peru_iPhoneStyler styleLabel:signupStep1Label_ withFontSize:13.0f color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:signupStep2Label_ withFontSize:13.0f color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:signupStep3Label_ withFontSize:13.0f color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:signupStep3BoldLabel_ withBoldFontSize:13.0f color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:signupStep4Label_ withFontSize:13.0f color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:signupStep4BoldLabel_ withBoldFontSize:13.0f color:[UIColor BBVACoolGreyColor]];
    //[NXT_Peru_iPhoneStyler styleLabel:singup1Label_ withFontSize:14.0f color:[UIColor darkGrayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:singup2Label_ withFontSize:14.0f color:[UIColor darkGrayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:singup3Label_ withFontSize:14.0f color:[UIColor darkGrayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:singup4Label_ withFontSize:14.0f color:[UIColor darkGrayColor]];
     [NXT_Peru_iPhoneStyler styleLabel:extraDisclaimer_ withFontSize:13.0f color:[UIColor darkGrayColor]];
    [[signupURLButton_ titleLabel] setFont:[UIFont boldSystemFontOfSize:12.0f]];
    [signupURLButton_ setTitleColor:[UIColor blueColor]
                           forState:UIControlStateNormal];
    [signupBackgroundImageView_ setImage:[imagesCache imageNamed:ALERT_BOX_IMAGE_FILE_NAME]];
    
    [NXT_Peru_iPhoneStyler styleLabel:forgotPasswordHeaderLabel_ withBoldFontSize:14.0f color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:forgotPasswordStep1Label_ withFontSize:13.0f color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:forgotPasswordStep2Label_ withFontSize:13.0f color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:forgotPasswordStep2BoldLabel_ withBoldFontSize:13.0f color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:forgotPasswordStep3Label_ withFontSize:13.0f color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:forgotPasswordStep3BoldLabel_ withBoldFontSize:13.0f color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:forgotPassword1Label_ withFontSize:14.0f color:[UIColor darkGrayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:forgotPassword2Label_ withFontSize:14.0f color:[UIColor darkGrayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:forgotPassword3Label_ withFontSize:14.0f color:[UIColor darkGrayColor]];
    [[forgotPasswordURLButton_ titleLabel] setFont:[UIFont boldSystemFontOfSize:12.0f]];
    [forgotPasswordURLButton_ setTitleColor:[UIColor blueColor]
                                   forState:UIControlStateNormal];
    [forgotPasswordBackgroundImageView_ setImage:[imagesCache imageNamed:ALERT_BOX_IMAGE_FILE_NAME]];
    
    

    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated: If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    [signupView_ removeFromSuperview];
    [forgotPasswordView_ removeFromSuperview];
    
    if (type_ == lit_Subscribe) {
        
        [signupHeaderLabel_ setText:NSLocalizedString(SIGNUP_HEADER_TEXT_KEY, nil)];
        //[signupStep1Label_ setText:NSLocalizedString(SIGNUP_STEP1_TEXT_KEY, nil)];
        [signupStep2Label_ setText:NSLocalizedString(SIGNUP_STEP2_TEXT_KEY, nil)];
        [signupStep3Label_ setText:NSLocalizedString(SIGNUP_STEP3_TEXT_KEY, nil)];
        [signupStep3BoldLabel_ setText:NSLocalizedString(SIGNUP_STEP3_BOLD_TEXT_KEY, nil)];
        [signupStep4Label_ setText:NSLocalizedString(SIGNUP_STEP4_TEXT_KEY, nil)];
        [signupStep4BoldLabel_ setText:NSLocalizedString(SIGNUP_STEP4_BOLD_TEXT_KEY, nil)];
        [signupURLButton_ setTitle:NSLocalizedString(SIGNUP_URL_KEY, nil) forState:UIControlStateNormal];
        
        //[singup1Label_ setText:STEP_ONE];
        [singup2Label_ setText:STEP_ONE];
        [singup3Label_ setText:STEP_TWO];
        [singup4Label_ setText:STEP_THREE];
        
        [extraDisclaimer_ setText:@"Recuerda que para realizar tus operaciones deberás afiliarte a la Clave SMS en nuestra Red de Cajeros."];

        [[self view] addSubview:signupView_];
        
    } else if (type_ == lit_ForgotPassword) {
        
        [forgotPasswordHeaderLabel_ setText:NSLocalizedString(FORGOT_PASSWORD_HEADER_TEXT_KEY, nil)];
        [forgotPasswordStep1Label_ setText:NSLocalizedString(FORGOT_PASSWORD_STEP1_TEXT_KEY, nil)];
        [forgotPasswordStep2Label_ setText:NSLocalizedString(FORGOT_PASSWORD_STEP2_TEXT_KEY, nil)];
        [forgotPasswordStep2BoldLabel_ setText:NSLocalizedString(FORGOT_PASSWORD_STEP2_BOLD_TEXT_KEY, nil)];
        [forgotPasswordStep3Label_ setText:NSLocalizedString(FORGOT_PASSWORD_STEP3_TEXT_KEY, nil)];
        [forgotPasswordStep3BoldLabel_ setText:NSLocalizedString(FORGOT_PASSWORD_STEP3_BOLD_TEXT_KEY, nil)];
        [forgotPasswordURLButton_ setTitle:NSLocalizedString(FORGOT_PASSWORD_URL_KEY, nil) forState:UIControlStateNormal];
        
        [forgotPassword1Label_ setText:STEP_ONE];
        [forgotPassword2Label_ setText:STEP_TWO];
        [forgotPassword3Label_ setText:STEP_THREE];
        
        [[self view] addSubview:forgotPasswordView_];
        
    }
    
}

#pragma mark -
#pragma mark UIButton events

/*
 * Signup URL clicked.
 */
- (IBAction)signupURLClick {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:SIGNUP_URL]];
    
}

/*
 * Forgot password URL clicked.
 */
- (IBAction)forgotPasswordURLClick {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:FORGOT_PASSWORD_URL]];
    
}

#pragma mark - 
#pragma mark UINavigationBar

/**
 * The navigation item used to represent the view controller. (read-only)
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [self customNavigationItem];
    
    if (type_ == lit_Subscribe) {
        
        [[result customTitleView] setTopLabelText:NSLocalizedString(SIGNUP_TITLE_TEXT_KEY, nil)];
        
    } else if (type_ == lit_ForgotPassword) {
        
        [[result customTitleView] setTopLabelText:NSLocalizedString(FORGOT_PASSWORD_TITLE_TEXT_KEY, nil)];
        
    }
    
    return result;
    
}

@end
