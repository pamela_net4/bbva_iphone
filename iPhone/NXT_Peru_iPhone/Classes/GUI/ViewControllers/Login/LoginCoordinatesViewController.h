/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTEditableViewController.h"


//Forward declarations
@class NXTTextField;
@class SectionHeader;


/**
 * Displays a view to capture the user coordinate from the coordinates card
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface LoginCoordinatesViewController : NXTEditableViewController {
    
@private
    
    /**
     * Section header
     */
    SectionHeader *sectionHeader_;
    
	/**
	 * Information label
	 */
    UILabel *infoLabel_;
    
    /** 
	 * Required coordinate label
	 */
    UILabel *requiredCoordinateLabel_;
    
    /** 
	 * Coordinate text field
	 */
    NXTTextField *coordinateTextField_;
    
    /**
     * Separator image view
     */
    UIImageView *separatorImageView_;
    
    /** 
	 * Accept button
	 */
    UIButton *acceptButton_;
    
    /**
     * Branding image view
     */
    UIImageView *brandingImageView_;
    
    /**
     * Done navigation bar button
     */
    UIBarButtonItem *doneButton_;
    
    /** 
	 * Required coordinate
	 */
    NSString *coordinate_;

}

/**
 * Provides read-write access to the information label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *infoLabel;

/**
 * Provides read-write access to the required coordinate label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *requiredCoordinateLabel;

/**
 * Provides read-write access to the coordinate text field and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTTextField *coordinateTextField;

/**
 * Provides read-write access to the separator image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separatorImageView;

/**
 * Provides read-write access to the accept button and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *acceptButton;

/**
 * Provides read-write access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;

/**
 * Provides read-write access to the coordinate
 */
@property (nonatomic, readwrite, copy) NSString *coordinate;


/**
 * Creates and returns an autoreleased LoginCoordinatesViewController constructed from a NIB file
 *
 * @return The autoreleased LoginCoordinatesViewController constructed from a NIB file
 */
+ (LoginCoordinatesViewController *)loginCoordinatesViewController;


/**
 * Invoked by framework when user taps on the accept button. The coordinate is checked agains the server
 */
- (IBAction)acceptButtonTapped;

@end
