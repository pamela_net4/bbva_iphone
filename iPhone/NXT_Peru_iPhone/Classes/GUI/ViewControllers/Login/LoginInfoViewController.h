/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTViewController.h"

/**
 * Enum to select a type of LoginInfoType
 */
typedef enum {
    
    lit_Subscribe = 0, //<! The login info type subscribe.
    lit_ForgotPassword //<! The login info type forgon password.
    
} LoginInfoType;

/**
 * LoginInfo ViewController show data to sign up and forgot password information.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface LoginInfoViewController : NXTViewController {
    
@private
    
    /**
	 * Signup View.
	 */
    UIView *signupView_;
    
    /**
	 * Forgot Password View. 
	 */
    UIView *forgotPasswordView_;
    
    /**
	 * Signup label 
	 */
    UILabel *signupHeaderLabel_;
    
    /**
     * Sinup label, representing step 1 number
     */
    UILabel *singup1Label_;
    
	/**
	 * Signup label 
	 */
    UILabel *signupStep1Label_;
    
	/**
	 * Signup button 
	 */
    UIButton *signupURLButton_;
    
    /**
     * Sinup label, representing step 2 number
     */
    UILabel *singup2Label_;
    
	/**
	 * Signup label 
	 */
    UILabel *signupStep2Label_;
    
    /**
     * Sinup label, representing step 3 number
     */
    UILabel *singup3Label_;
    
	/**
	 * Signup label 
	 */
    UILabel *signupStep3Label_;
    
	/**
	 * Signup label 
	 */
    UILabel *signupStep3BoldLabel_;
    
    /**
     * Sinup label, representing step 4 number
     */
    UILabel *singup4Label_;
    
	/**
	 * Signup label 
	 */
    UILabel *signupStep4Label_;
    
	/**
	 * Signup label 
	 */
    UILabel *signupStep4BoldLabel_;
    
    /**
     * The image view that contains the background.
     */
    UIImageView *signupBackgroundImageView_;
    
    /**
	 * Forgot password label 
	 */
    UILabel *forgotPasswordHeaderLabel_;
    
    /**
     * Forgot password label, representing step 1 number
     */
    UILabel *forgotPassword1Label_;
    
	/**
	 * Forgot password label
	 */
    UILabel *forgotPasswordStep1Label_;
    
	/**
	 * Forgot password button 
	 */
    UIButton *forgotPasswordURLButton_;
    
    /**
     * Forgot password label, representing step 2 number
     */
    UILabel *forgotPassword2Label_;
    
	/**
	 * Forgot password label 
	 */
    UILabel *forgotPasswordStep2Label_;
    
	/**
	 * Forgot password step 2 bold label.
	 */
    UILabel *forgotPasswordStep2BoldLabel_;
    
    /**
     * Forgot password label, representing step 3 number
     */
    UILabel *forgotPassword3Label_;
    
	/**
	 * Forgot password label.
	 */
    UILabel *forgotPasswordStep3Label_;
    
	/**
	 * Forgot password step 3 bold label.
	 */
    UILabel *forgotPasswordStep3BoldLabel_;
            
    /**
     * The image view that contains the background.
     */
    UIImageView *forgotPasswordBackgroundImageView_;
    
    
    /**
     * Forgot password label.
     */
    UILabel *extraDisclaimer_;

    
    /**
     * The type to show information.
     */
    LoginInfoType type_;
    
}

/**
 * Provides read-write access to signupView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *signupView;

/**
 * Provides read-write access to forgotPasswordView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *forgotPasswordView;

/**
 * Provides read-write access to signupHeaderLabel. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *signupHeaderLabel;

/**
 * Provides read-write access to singup1Label. Connected to Interface Builder;
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *singup1Label;

/**
 * Provides read-retain access to signupStep1Label. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *signupStep1Label;

/**
 * Provides read-write access to signupURLButton. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *signupURLButton;

/**
 * Provides read-write access to signup2Label. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *singup2Label;

/**
 * Provides read-write access to signupStep2Label. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *signupStep2Label;

/**
 * Provides read-write access to signup3Label. Connected to Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *singup3Label;

/**
 * Provides read-write access to signupStep3Label. Connected to Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *signupStep3Label;

/**
 * Provides read-write access to the signup step 3 bold label and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *signupStep3BoldLabel;

/**
 * Provides read-write access to signup4Label. Connected to Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *singup4Label;

/**
 * Provides read-write access to signupStep4Label. Connected to Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *signupStep4Label;

/**
 * Provides read-write access to the signup step 4 bold label and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *signupStep4BoldLabel;

/**
 * Provides read-write access to signupBackgroundImageView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *signupBackgroundImageView;

/**
 * Provides read-write access to forgotPasswordHeaderLabel. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *forgotPasswordHeaderLabel;

/**
 * Provides read-write access to forgotPassword1Label. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *forgotPassword1Label;

/**
 * Provides read-write access to forgotPasswordStep1Label. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *forgotPasswordStep1Label;

/**
 * Provides read-write access to forgotPasswordURLButton. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *forgotPasswordURLButton;

/**
 * Provides read-write access to forgotPassword2Label. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *forgotPassword2Label;

/**
 * Provides read-write access to forgotPasswordStep2Label. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *forgotPasswordStep2Label;

/**
 * Provides read-write access to the forgot password step 2 bold label and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *forgotPasswordStep2BoldLabel;

/**
 * Provides read-write access to forgotPassword3Label. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *forgotPassword3Label;

/**
 * Provides read-write access to forgotPasswordStep3Label. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *forgotPasswordStep3Label;

/**
 * Provides read-write access to the forgot password step 3 bold label and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *forgotPasswordStep3BoldLabel;

/**
 * Provides read-write access to forgotPasswordBackgroundImageView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *forgotPasswordBackgroundImageView;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *extraDisclaimer;

/**
 * Provides read-write access to type.
 */
@property (nonatomic, readwrite, assign) LoginInfoType type;

/**
 * Creates and returns an autoreleased LoginInfoViewController constructed from a NIB file.
 *
 * @return The autoreleased LoginInfoViewController constructed from a NIB file.
 */
+ (LoginInfoViewController *)loginInfoViewController;

/**
 * Signup URL clicked.
 */
- (IBAction)signupURLClick;

/**
 * Forgot password URL clicked.
 */
- (IBAction)forgotPasswordURLClick;

@end
