/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTViewController.h"
#import "DownloadListener.h"
#import "NXTEditableViewController.h"



//Forward declarations
@class BlueButtonCell;
@class IconActionCell;
@class LoginInfoCell;
@class POIMapViewController;
@class PasswordChangeStepOneViewController;
@class MobileCashLoginViewController;
@class VideoListViewController;
@class MPMoviePlayerController;
@class QRCodeTransferLoginViewController;
@class RemoteManagerViewController;
@class LoginCoordinatesViewController;
@class LoginInfoViewController;
@class YourOpinionMattersViewController;
@class SplashRadioViewController;
@class SlideOutNavigationViewController;

/**
 * Controls the login view, which contains a table with options, a header and an ad section. Table cells are stored inside the view controller to ease
 * their management, as they are always visible and never reused
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface LoginViewController : NXTEditableViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, UITextFieldDelegate,CheckViewDelegate,NXTComboButtonDelegate> {

@private
    
    /**
     * Image header. Contains the BBVA branding header
     */
    UIImageView *brandingHeader_;
    
    /**
     * Table view to contain all possible actions
     */
    UITableView *actionsTableView_;

    /**
     * Mobile banking action cell
     */
    IconActionCell *mobileBankingActionCell_;
    
    /**
     * Login information cell
     */
    LoginInfoCell *loginInfoCell_;
    
    /**
     * ATM and branches locator action cell
     */
    IconActionCell *bankPOILocatorActionCell_;
    
    /**
     * Contact us action cell
     */
    IconActionCell *contactUsActionCell_;
    
    /**
     * Call us action cell
     */
    IconActionCell *callUsActionCell_;
    
    /**
     * Your opinion action cell
     */
    IconActionCell *yourOpinionActionCell_;
    
    /**
     * Your opinion action cell
     */
    IconActionCell *radioBBVAActionCell_;

    
    /**
     * Login coordinates view controller
     */
    LoginCoordinatesViewController *loginCoordinatesViewController_;
    
    /**
     * Poi map view controller.
     */
    POIMapViewController *poiMapViewController_;
    
    /**
     * Info text view controller.
     */
    LoginInfoViewController *loginInfoViewController_;
    
    /**
     * Your opinion matters view controller.
     */
    YourOpinionMattersViewController *yourOpinionMattersViewController_;
    
    
    SplashRadioViewController *splashRadioViewController_;
    
    SlideOutNavigationViewController *slideOutNavigationViewController_;
    
    /**
     * Login information expanded flag. YES when the login information is expanded, NO otherwise
     */
    BOOL loginInformationExpanded_;
    
    NSMutableArray * frequentCards_;

}

/**
 * Provides read-write access to the image header and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingHeader;

/**
 * Provides read-write access to the table view to contain all possible actions and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *actionsTableView;


@property (nonatomic, readwrite, retain) NSMutableArray * frequentCards;


/**
 * Creates and returns an autoreleased LoginViewController constructed from a NIB file
 *
 * @return The autoreleased LoginViewController constructed from a NIB file
 */
+ (LoginViewController *)loginViewController;

@end
