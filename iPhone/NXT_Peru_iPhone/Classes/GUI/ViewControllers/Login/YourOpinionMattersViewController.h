/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTViewController.h"
#import <MessageUI/MessageUI.h>

@class IconActionCell;

/**
 * View Controller to show table that contains your opinion matters.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface YourOpinionMattersViewController : NXTViewController <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate> {
    
@private
    
    /**
     * TableView that contains all cells.
     */
    UITableView *tableView_;
    
    /**
     * Branding image view.
     */
    UIImageView *brandImageView_;
    
    /**
     * The action cell of help us to improve.
     */
    IconActionCell *helpUsToImproveActionCell_;
    
    /**
     * The action cell of your comments.
     */
    IconActionCell *yourCommentsActionCell_;
    
    /**
     * The action cell of your opinion matters.
     */
    IconActionCell *yourOpinionMattersActionCell_;
    
}

/**
 * Provides read-write access to tableView.
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *tableView;

/**
 * Provides read-write access to brandImageView.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandImageView;

/**
 * Creates and returns an autoreleased YourOpinionMattersViewController constructed from a NIB file.
 *
 * @return The autoreleased YourOpinionMattersViewController constructed from a NIB file.
 */
+ (YourOpinionMattersViewController *)yourOpinionMattersViewController;

@end
