/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "YourOpinionMattersViewController.h"

#import "Constants.h"
#import "IconActionCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTNavigationItem.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "Tools.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"YourOpinionMattersViewController"

/**
 * Defines the cell of Help us to improve.
 */
#define CELL_HELP_US_TO_IMPROVE                                     0

/**
 * Defines the cell of Your comments.
 */
#define CELL_YOUR_COMMENTS                                          1

/**
 * Defines the cell of recommend to a friend.
 */
#define CELL_RECOMMEND_TO_A_FRIEND                                  2

@interface YourOpinionMattersViewController (Private)

#pragma mark -
#pragma mark Private methods

/**
 * Send mail with information.
 *
 * @param subject: The subject that specified mail.
 * @param recipients: All recipients to send mail.
 * @param bodyText: The body text that contains information of mail.
 * @private
 */
- (void)sendMailWithSubject:(NSString *)subject toRecipients:(NSArray *)recipients andBodyText:(NSString *)bodyText;

/**
 * The action to help us to improve cell.
 *
 * @private
 */
- (void)helpUsToImproveAction;

/**
 * The action to your comments cell.
 *
 * @private
 */
- (void)yourCommentsAction;

/**
 * The action to your opinion matters cell.
 *
 * @private
 */
- (void)yourOpinionMattersAction;

/**
 * Releases all graphic elements contained on this view controller
 *
 * @private
 */
- (void)releaseGraphicElements;

@end

#pragma mark -

@implementation YourOpinionMattersViewController

#pragma mark -
#pragma mark Properties

@synthesize tableView = tableView_;
@synthesize brandImageView = brandImageView_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [tableView_ release];
    tableView_ = nil;
    
    [brandImageView_ release];
    brandImageView_ = nil;
    
    [helpUsToImproveActionCell_ release];
    helpUsToImproveActionCell_ = nil;
    
    [yourCommentsActionCell_ release];
    yourCommentsActionCell_ = nil;
    
    [yourOpinionMattersActionCell_ release];
    yourOpinionMattersActionCell_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases all graphic elements contained on this view controller
 */
- (void)releaseGraphicElements {
    
    [tableView_ release];
    tableView_ = nil;
    
    [brandImageView_ release];
    brandImageView_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
}

/*
 * Creates and returns an autoreleased YourOpinionMattersViewController constructed from a NIB file.
 */
+ (YourOpinionMattersViewController *)yourOpinionMattersViewController {
    
    YourOpinionMattersViewController *result =  [[[YourOpinionMattersViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    [brandImageView_ setImage:[imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    [NXT_Peru_iPhoneStyler styleTableView:tableView_];
    
    [helpUsToImproveActionCell_ release];
    helpUsToImproveActionCell_ = [[IconActionCell iconActionCell] retain];
    [helpUsToImproveActionCell_ setActionText:NSLocalizedString(HELP_US_IMPROVE_TEXT_KEY, nil)];
    [helpUsToImproveActionCell_ setActionButtonImage:[imagesCache imageNamed:ARROW_ICON_IMAGE_FILE_NAME]];
    [helpUsToImproveActionCell_ setDisplayImage:NO];
    [helpUsToImproveActionCell_ displayGrayBackground:YES];
    
    [yourCommentsActionCell_ release];
    yourCommentsActionCell_ = [[IconActionCell iconActionCell] retain];
    [yourCommentsActionCell_ setActionText:NSLocalizedString(YOUR_COMMENTS_TEXT_KEY, nil)];
    [yourCommentsActionCell_ setActionButtonImage:[imagesCache imageNamed:ARROW_ICON_IMAGE_FILE_NAME]];
    [yourCommentsActionCell_ setDisplayImage:NO];
    [yourCommentsActionCell_ displayGrayBackground:YES];
    
    [yourOpinionMattersActionCell_ release];
    yourOpinionMattersActionCell_ = [[IconActionCell iconActionCell] retain];
    [yourOpinionMattersActionCell_ setActionText:NSLocalizedString(RECOMMEND_TO_A_FRIEND_TEXT_KEY, nil)];
    [yourOpinionMattersActionCell_ setActionButtonImage:[imagesCache imageNamed:ARROW_ICON_IMAGE_FILE_NAME]];
    [yourOpinionMattersActionCell_ setDisplayImage:NO];
    [yourOpinionMattersActionCell_ displayGrayBackground:YES];
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated: If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
}

#pragma mark -
#pragma mark YourOpinionMattersViewController methods

/*
 * Send mail with information.
 */
- (void)sendMailWithSubject:(NSString *)subject toRecipients:(NSArray *)recipients andBodyText:(NSString *)bodyText {
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mfMailViewController = [[MFMailComposeViewController alloc] init];
        [mfMailViewController setMailComposeDelegate:self];
        
        [[mfMailViewController navigationBar] setTintColor:[[[self navigationController] navigationBar] tintColor]];
        
        // Set subject
        [mfMailViewController setSubject:subject];
        
        // Set up recipients
        [mfMailViewController setToRecipients:recipients];
        
        // Set message body
        [mfMailViewController setMessageBody:bodyText isHTML:NO];
        
        [self presentModalViewController:mfMailViewController animated:YES];
        
        [mfMailViewController release];
        
    } else {
        
        [Tools showInfoWithMessage:NSLocalizedString(EMAIL_NOT_CONFIGURED_ERROR_TEXT_KEY, nil)];
        
    }
    
}

#pragma mark -
#pragma mark User interaction

/**
 * The action to help us to improve cell.
 *
 * @private
 */
- (void)helpUsToImproveAction {
    
    // Set up recipients
    NSArray *toRecipients = [NSArray arrayWithObjects:NSLocalizedString(CONTACT_US_EMAIL, nil), nil];
    
    [self sendMailWithSubject:NSLocalizedString(HELP_US_IMPROVE_MAIL_SUBJECT_TEXT_KEY, nil) toRecipients:toRecipients andBodyText:@""];
    
}

/**
 * The action to your comments cell.
 *
 * @private
 */
- (void)yourCommentsAction {
    
    // Set up recipients
    NSArray *toRecipients = [NSArray arrayWithObjects:NSLocalizedString(CONTACT_US_EMAIL, nil), nil];
    
    [self sendMailWithSubject:NSLocalizedString(YOUR_COMMENTS_MAIL_SUBJECT_TEXT_KEY, nil) toRecipients:toRecipients andBodyText:@""];    
}

/**
 * The action to your opinion matters cell.
 *
 * @private
 */
- (void)yourOpinionMattersAction {
    
    [self sendMailWithSubject:NSLocalizedString(RECOMMEND_TO_A_FRIEND_MAIL_SUBJECT_TEXT_KEY, nil) toRecipients:nil andBodyText:NSLocalizedString(RECOMMEND_TO_A_FRIEND_MAIL_BODY_TEXT_KEY, nil)];
    
}

#pragma mark -
#pragma mark UITableViewDataSource protocol selectors

/**
 * Tells the data source to return the number of rows in a given section of a table view. Returns the number of actions available,
 * three in this case.
 *
 * @param tableView: The table-view object requesting this information.
 * @param section: An index number identifying a section in tableView.
 * @return The number of actions available.
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 3;
    
}

/**
 * Asks the data source to return the number of sections in the table view.
 *
 * @param tableView: An object representing the table view requesting this information.
 * return The number of sections in tableView.
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

/**
 * Asks the data source for a cell to insert in a particular questions of the table view. A UITableViewCell is returned, containing
 * the action at the given question pressed.
 *
 * @param tableView: The table-view object requesting the cell.
 * @param indexPath: An index path locating a row in tableView.
 * @return An object inheriting from UITableViewCell that the table view can use for the specified row.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    UITableViewCell *result = nil;
    
    switch ([indexPath row]) {
            
        case CELL_HELP_US_TO_IMPROVE:
            
            result = helpUsToImproveActionCell_;
            
            break;
            
        case CELL_YOUR_COMMENTS:
            
            result = yourCommentsActionCell_;
            
            break;
            
        case CELL_RECOMMEND_TO_A_FRIEND:
            
            result = yourOpinionMattersActionCell_;
            
            break;
            
        default:
            
            break;
            
    }
    
    [result setSelectionStyle:UITableViewCellSelectionStyleGray];
    
    return result;
    
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 *
 * @param tableView: The table-view object requesting this information.
 * @param indexPath: An index path that locates a row in tableView.
 * @return A floating-point value that specifies the height (in points) that row should be.
 */ 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [IconActionCell cellHeight];
    
}

/**
 * Asks the delegate for a view object to display in the header of the specified section of the table view.
 *
 * @param tableView: The table-view object asking for the view object.
 * @param section: An index number identifying a section of tableView .
 * @return A view object to be displayed in the header of section .
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    SimpleHeaderView *view = [SimpleHeaderView simpleHeaderView];
    
    [view setTitle:NSLocalizedString(YOUR_OPINION_TEXT_KEY, nil)];
    
    return view;
    
}

/**
 * Asks the delegate for the height to use for the header of a particular section.
 *
 * @param tableView: The table-view object requesting this information.
 * @param section: An index number identifying a section of tableView .
 * @return A floating-point value that specifies the height (in points) of the header for section.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return [SimpleHeaderView height];
    
}

#pragma mark -
#pragma mark UITableViewDelegate protocol selectors

/**
 * Tells the delegate that the specified row is now selected.
 *
 * @param tableView: A table-view object informing the delegate about the new row selection.
 * @param indexPath: An index path locating the new selected row in tableView.
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
    
    switch ([indexPath row]) {
            
        case CELL_HELP_US_TO_IMPROVE:
            
            [self helpUsToImproveAction];
            
            break;
            
        case CELL_YOUR_COMMENTS:
            
            [self yourCommentsAction];
            
            break;
            
        case CELL_RECOMMEND_TO_A_FRIEND:
            
            [self yourOpinionMattersAction];
            
            break;
            
        default:
            
            break;
            
    }
    
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate methods

/**
 * Tells the delegate that the user wants to dismiss the mail composition view.
 *
 * @param controller: The view controller object managing the mail composition view.
 * @param result: The result of the user’s action.
 * @param error: If an error occurred, this parameter contains an error object with information about the type of failure.
 */
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    switch (result) {

        case MFMailComposeResultCancelled:
            
            // Message Canceled

            break;
            
        case MFMailComposeResultSaved:
            
            // Message Saved

            break;
            
        case MFMailComposeResultSent:
            
            // Message Sent
            
            break;
            
        case MFMailComposeResultFailed:
            
            // Message Failed
            
            break;
            
        default:
            
            // Message Not Sent
            
            break;
            
    }
    
    [self dismissModalViewControllerAnimated:YES];
    
}

#pragma mark - 
#pragma mark UINavigationBar

/**
 * The navigation item used to represent the view controller. (read-only)
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [super customNavigationItem];
    
    [[result customTitleView] setTopLabelText:NSLocalizedString(YOUR_OPINION_TEXT_KEY, nil)];
    
    return result;
    
}

@end
