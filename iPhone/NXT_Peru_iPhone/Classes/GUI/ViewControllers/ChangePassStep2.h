//
//  SuscriptionStep2.h
//  NXT_Peru_iPhone
//
//  Created by Cardenas Torres, B. on 31/03/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMLReader.h"

@interface ChangePassStep2 : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>
{
    BOOL checked;
    
    NSMutableString *textInProgress;
    
    NSMutableArray *dictionaryStack;
    
    NSString *mstrXMLString;
    
    XMLReader *reader;
    
    NSString *fetchedXML;
    
}

@end
