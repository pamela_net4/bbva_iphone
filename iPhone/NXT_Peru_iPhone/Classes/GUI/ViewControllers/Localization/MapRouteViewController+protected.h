/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MapRouteViewController.h"


/**
 * MapRouteViewController protected category
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MapRouteViewController(protected)

/**
 * Provides read-write access to the current route information being displayed
 */
@property (nonatomic, readwrite, retain) GoogleMapsDirectionsResponse *routeInformation;


/**
 * Displays the route copyright and warnings information
 */
- (void)displayRouteCopyrightAndWarnings;

/**
 * Checks whether the warnings button can be displayed.
 *
 * @protected
 */
- (void)checkWarningButtonMustBeDisplayed;

@end
