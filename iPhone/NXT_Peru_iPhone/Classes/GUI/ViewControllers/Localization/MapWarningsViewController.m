/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MapWarningsViewController.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "StringKeys.h"
#import "Tools.h"
#import "MapWarningCell.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "ModalManager.h"

/**
 * Defines the MapWarningsViewController NIB file name
 */
#define MAP_WARNINGS_VIEW_CONTROLLER_NIB_FILE                                       @"MapWarningsViewController"

/**
 * Defines the view controller font size
 */
#define VIEW_CONTROLLER_FONT_SIZE                                                   15.0f


#pragma mark -

/**
 * MapWarningsViewController private extension
 */
@interface MapWarningsViewController()

/**
 * Releases the graphic elements not needed when view is hidden
 *
 * @private
 */
- (void)releaseMapWarningsViewControllerGraphicElements;

/**
 * Updates the information displayed. The view size is also update to better fit the information
 *
 * @private
 */
- (void)updateDisplayedInformation;

@end


#pragma mark -

@implementation MapWarningsViewController

#pragma mark -
#pragma mark Properties

@synthesize backgroundView = backgroundView_;
@synthesize backgroundImageView = backgroundImageView_;
@synthesize titleLabel = titleLabel_;
@synthesize copyrightLabel = copyrightLabel_;
@synthesize warningsTable = warningsTable_;
@synthesize acceptButton = acceptButton_;
@synthesize copyrightText = copyrightText_;
@dynamic warningsArray;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [self releaseMapWarningsViewControllerGraphicElements];
    
    [copyrightText_ release];
    copyrightText_ = nil;
    
    [warningsArray_ release];
    warningsArray_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory. Visual elements are released
 */
- (void)viewDidUnload {
    
    [self releaseMapWarningsViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/*
 * Releases the graphic elements not needed when view is hidden
 */
- (void)releaseMapWarningsViewControllerGraphicElements {
    
    [backgroundView_ release];
    backgroundView_ = nil;
    
    [backgroundImageView_ release];
    backgroundImageView_ = nil;
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [copyrightLabel_ release];
    copyrightLabel_ = nil;
    
    [warningsTable_ release];
    warningsTable_ = nil;
    
    [acceptButton_ release];
    acceptButton_ = nil;
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseMapWarningsViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased MapWarningsViewController constructed from a NIB file
 */
+ (MapWarningsViewController *)mapWarningsViewController {
    
    MapWarningsViewController *result = [[[MapWarningsViewController alloc] initWithNibName:MAP_WARNINGS_VIEW_CONTROLLER_NIB_FILE
                                                                                     bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory. Style is applied to graphic elements
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIColor *clearColor = [UIColor clearColor];
    self.view.backgroundColor = clearColor;
    backgroundView_.backgroundColor = clearColor;
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ROUTE_MODAL_VIEW_CONTROLLER_BACKGROUND_IMAGE_FILE_NAME];
    
    UIColor *whiteColor = [UIColor whiteColor];
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_
                        withFontSize:VIEW_CONTROLLER_FONT_SIZE
                               color:whiteColor];
    titleLabel_.text = NSLocalizedString(ABOUT_ROUTE_TEXT_KEY, nil);
    
    [NXT_Peru_iPhoneStyler styleLabel:copyrightLabel_
                        withFontSize:VIEW_CONTROLLER_FONT_SIZE
                               color:whiteColor];
    [NXT_Peru_iPhoneStyler styleRouteModalViewButton:acceptButton_];
    [acceptButton_ setTitle:NSLocalizedString(OK_TEXT_KEY, nil) forState:UIControlStateNormal];
    warningsTable_.backgroundColor = clearColor;
    
    [self.view bringSubviewToFront:backgroundView_];

}

/**
 * Notifies the view controller that its view is about to be become visible. Information is displayed in the
 * corresponding controls
 *
 * @param animated If YES, the view is being added to the window using an animation
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self updateDisplayedInformation];
    
}

#pragma mark -
#pragma mark User interaction

/*
 * Invoked by framework when the accept button is tapped. The view controller is dismissed
 */
- (IBAction)acceptButtonTapped {

    [[ModalManager getInstance] dismissModalViewController:self];
    
}

#pragma mark -
#pragma mark Graphic information

/*
 * Updates the information displayed. The view size is also update to better fit the information
 */
- (void)updateDisplayedInformation {

    [copyrightLabel_ setText:copyrightText_];
    
    [warningsTable_ reloadData];
    
    CGRect viewFrame = self.view.frame;
    viewFrame.size.height = viewFrame.size.height - 40.0f;
    viewFrame.size.width = viewFrame.size.width - 40.0f;
    viewFrame.origin.x = viewFrame.origin.x - 10.0f;
    viewFrame.origin.y = viewFrame.origin.y - 10.0f;
    
    CGRect frame = titleLabel_.frame;
    CGFloat auxPos = CGRectGetMaxY(frame) + 15.0f;
    
    frame = copyrightLabel_.frame;
    frame.origin.y = auxPos;
    copyrightLabel_.frame = frame;
    auxPos = CGRectGetMaxY(frame) + 10.0f;
    frame.origin.y = auxPos;
    
    CGRect buttonFrame = acceptButton_.frame;
    CGFloat buttonHeight = CGRectGetHeight(buttonFrame);
    
    if ([warningsArray_ count] == 0) {
        
        warningsTable_.hidden = YES;
        
    } else {
        
        warningsTable_.hidden = NO;
        
        CGFloat availableHeight = CGRectGetHeight(viewFrame);
        
        CGFloat usedHeight = auxPos + buttonHeight + 20.0f;
        
        CGFloat tableMaxHeight = availableHeight - usedHeight - 10.0f;
        
        CGRect tableContentRect = [warningsTable_ rectForSection:0];
        CGFloat tableContentHeight = CGRectGetHeight(tableContentRect);
        
        CGFloat tableHeight = (tableMaxHeight < tableContentHeight) ? tableMaxHeight : tableContentHeight;
        
        frame.size.height = tableHeight;
        warningsTable_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + 10.0f;
        frame.origin.y = auxPos;
        
        if (tableHeight < tableContentHeight) {
            
            warningsTable_.scrollEnabled = YES;
            
        } else {
            
            warningsTable_.scrollEnabled = NO;
            
        }
        
    }
    
    buttonFrame.origin.y = auxPos;
    acceptButton_.frame = buttonFrame;
    frame.size.height = buttonHeight;
    auxPos = CGRectGetMaxY(frame) + 20.0f;
    
    CGPoint centerPoint = backgroundView_.center;
    viewFrame.size.height = auxPos;
    backgroundView_.frame = viewFrame;
    backgroundView_.center = centerPoint;
    
}

#pragma mark -
#pragma mark UITableViewDataSource protocol selectors

/**
 * Tells the data source to return the number of rows in a given section of a table view. Returns the number of warnings
 * stored in the warnings array
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section in tableView
 * @return The number of warning strings stored in the warnings array
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [warningsArray_ count];
    
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. A MapWarningCell is returned, containing the
 * warning text at the given position
 *
 * @param tableView The table-view object requesting the cell
 * @param indexPath An index path locating a row in tableView
 * @return A MapWarningCell containing the warning information at the given position
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellId = [MapWarningCell cellIdentifier];
    
    MapWarningCell *result = (MapWarningCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (result == nil) {
        
        result = [MapWarningCell mapWarningCell];
        
    }
    
    NSInteger row = indexPath.row;

    NSString *warning = [warningsArray_ objectAtIndex:row];
    
    result.warningText = warning;
    
    return result;
    
}

#pragma mark -
#pragma mark UITableViewDelegate protocol selectors

/**
 * Asks the delegate for the height to use for a row in a specified location. The cell height is calculated from the text and available
 * width
 *
 * @param tableView The table-view object requesting this information
 * @param indexPath An index path that locates a row in tableView
 * @return A floating-point value that specifies the height (in points) that row should be
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger row = indexPath.row;
    
    NSString *warning = [warningsArray_ objectAtIndex:row];
    CGRect tableFrame = warningsTable_.frame;
    CGFloat width = CGRectGetWidth(tableFrame);
    
    return [MapWarningCell cellHeightForText:warning
                                       width:width];
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the new copyright text. The string is stored localy and the associated label
 *
 * @param aCopyright The new copyright information to set
 */
- (void)setCopyrightText:(NSString *)aCopyright {
    
    if (![copyrightText_ isEqualToString:aCopyright]) {
        
        [copyrightText_ release];
        copyrightText_ = nil;
        copyrightText_ = [aCopyright copyWithZone:[self zone]];
        
        [self updateDisplayedInformation];
        
    }
    
}

/*
 * Returns the warnings array as a nonmutable array
 *
 * @return The warnings array
 */
- (NSArray *)warningsArray {
    
    return [NSArray arrayWithArray:warningsArray_];
    
}

/**
 * Sets the warnings array. Only NSString instances are stored
 *
 * @param aWarningsArray The new warnings array
 */
- (void)setWarningsArray:(NSArray *)aWarningsArray {
    
    if (warningsArray_ == nil) {
        
        warningsArray_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [warningsArray_ removeAllObjects];
        
    }
    
    for (NSObject *object in aWarningsArray) {
        
        if ([object isKindOfClass:[NSString class]]) {
            
            [warningsArray_ addObject:object];
            
        }
        
    }

    [self updateDisplayedInformation];
    
}

@end
