/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "RouteStepsViewController.h"
#import "POIListEnRouteViewController+protected.h"
#import "MapRouteViewController+protected.h"
#import "ATMList.h"
#import "GeocodedAddressList.h"
#import "RouteStepView.h"
#import "StepAnnotationView.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "Tools.h"
#import "StringKeys.h"
#import "Location.h"
#import "GoogleMapsRoute.h"
#import "GoogleMapsLeg.h"
#import "GoogleMapsStep.h"
#import "GoogleMapsDirectionsResponse.h"
#import "GoogleMapsLocation.h"
#import "GoogleMapsDistance.h"
#import "POIAnnotation.h"
#import "OriginAnnotation.h"
#import "DestinationAnnotation.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhone_AppDelegate.h"

/**
 * Defines the RouteStepsViewController NIB file name
 */
#define ROUTE_STEPS_VIEW_CONTROLLER_NIB_FILE                                            @"RouteStepsViewController"

/**
 * Defines the toolbar action button width measured in points
 */
#define ACTION_TOOLBAR_BUTTON_WIDTH                                                     100.0f

/**
 * Defines the toolbar items offset measured in points
 */
#define TOOLBAR_COMPONENTS_OFFSET                                                       10.0f


#pragma mark -

/**
 * RouteStepsViewController private extension
 */
@interface RouteStepsViewController()

/**
 * Releases the graphic elements not needed when view is hidden
 *
 * @private
 */
- (void)releaseRouteStepsViewControllerGraphicElements;

/**
 * Invoked by framework when the navigation bar action button is checked. It toggles the view controller state
 *
 * @private
 */
- (void)navigationButtonActionTapped;

@end


#pragma mark -

@implementation RouteStepsViewController

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self releaseRouteStepsViewControllerGraphicElements];
    
    [poi_ release];
    poi_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory. Visual elements are released
 */
- (void)viewDidUnload {
    
    [self releaseRouteStepsViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseRouteStepsViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases the graphic elements not needed when view is hidden
 */
- (void)releaseRouteStepsViewControllerGraphicElements {
    
    [actionBarButtonItem_ release];
    actionBarButtonItem_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Sets the show POI callout button flag to NO, so that button is not displayed
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
	[self setShowPOICalloutButton:NO];
    
}

/*
 * Creates and returns an autoreleased RouteStepsViewController constructed from a NIB file
 */
+ (RouteStepsViewController *)routeStepsViewController {
    
    RouteStepsViewController *result = [[[RouteStepsViewController alloc] initWithNibName:ROUTE_STEPS_VIEW_CONTROLLER_NIB_FILE
                                                                                   bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory. Style is applied to graphic elements
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
		
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated: If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
	
	[self displayRouteSteps];
    
    [self fitMapRegion];
    
}

/**
 * Invoked by framework when view is added to the window. Registers to notification center to receive keyboard notifications
 *
 * @param animated YES if view is going to be animated, NO otherwise
 */
- (void)viewDidAppear:(BOOL)animated {
	
    [super viewDidAppear:animated];
    
    [[self appDelegate] setTabBarVisibility:NO animated:YES];
    
    [self startDisplayingSteps:YES];
    
    [self fitMapRegion];
    
}

#pragma mark -
#pragma mark Setting the route

/*
 * Sets the route origin and destinaton, and start the route download
 */
- (void)setRouteOrigin:(Location *)routeOrigin
      routeDestination:(Location *)routeDestination
         associatedPOI:(ATMData *)poi 
            byRouteCar:(BOOL)carRoute {
    
    [super setRouteOrigin:routeOrigin
         routeDestination:routeDestination 
               byRouteCar:carRoute];
    
    [self setPoiList:nil];
    [self setAnnotationsArray:nil];
    
    if (poi != poi_) {
        
        [poi_ release];
        poi_ = nil;
        poi_ = [poi retain];
        NSArray *poiList = [NSArray arrayWithObject:poi_];
        [self setPoiList:poiList];
        
    }
    
    UINavigationItem *navigationItem = [self navigationItem];
    [navigationItem setTitle:[[[poi_ geocodedAddress] address] addressFirstLine]];
    
    [self fitMapRegion];
    
}

#pragma mark -
#pragma mark User interaction

/*
 * Invoked by framework when the navigation bar action button is checked. It toggles the view controller state
 */
- (void)navigationButtonActionTapped {
	
	[self toggleDisplayRouteSteps];
    
}

#pragma mark -
#pragma mark POIListEnRouteViewController selectors

/**
 * Child subclasses must return the route time and distance view superview. The route time and distance view is located
 * at the top region of its superview. View controller's view is returned
 *
 * @protected
 */
- (UIView *)routeTimeAndDistanceViewSuperview {
    
    return [self view];
    
}

/**
 * Child subclasses must return the route step view superview. The route step view is located at the top region of its superview.
 * View controller's view is returned
 */
- (UIView *)routeStepViewSuperview {
    
    return [self view];
    
}

#pragma mark -
#pragma mark MapRouteViewController selectors

/**
 * Sets the current route information and displays it on the map. The toolbar elements are checked
 *
 * @param aRouteInformation The new route information to display
 */
- (void)setRouteInformation:(GoogleMapsDirectionsResponse *)aRouteInformation {
    
    [super setRouteInformation:aRouteInformation];
    
    if (poi_ != nil) {
        
        POIAnnotation *poiAnnotation = [[[POIAnnotation allocWithZone:self.zone] initWithAssociatedPOI:poi_] autorelease];
        NSArray *annotationsArray = [NSArray arrayWithObject:poiAnnotation];
        
        [self setAnnotationsArray:annotationsArray];
        
    }
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Return the navigation item used to represent the view controller. Inserts the multifunction segmented selector at the right
 *
 * @return The navigation item used to represent the view controller
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [super customNavigationItem];
    
	[[result customTitleView] setTopLabelText:[[[poi_ geocodedAddress] address] addressFirstLine]];
    [result setRightBarButtonItem:actionBarButtonItem_];
    
    return result;
    
}

@end
