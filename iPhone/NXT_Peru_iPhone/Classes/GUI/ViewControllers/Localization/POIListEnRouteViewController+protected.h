/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "POIListEnRouteViewController.h"


//Forward declarations
@class ATMData;
@class GoogleMapsRoute;
@class GoogleMapsStep;


/**
 * POIListEnRouteViewController protected category
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface POIListEnRouteViewController(protected)


/**
 * Provides read-only access to the route steps view
 */
@property (nonatomic, readonly, retain) RouteStepView *routeStepsView;

/**
 * Provides read-only access to the time and distance header view to display route time and distance
 */
@property (nonatomic, readonly, retain) TimeAndDistanceHeaderView *timeAndDistanceHeaderView;

/**
 * Provides read-only access to the displaying setps view flag
 */
@property (nonatomic, readonly, assign) BOOL displayingStepsView;

/**
 * Provides read-write access to the poi list
 */
@property (nonatomic, readwrite, retain) NSArray *poiList;

/**
 * Provides read-only access to the current route
 */
@property (nonatomic, readonly, retain) GoogleMapsRoute *currentRoute;

/**
 * Provides read-write access to the annotation array
 */
@property (nonatomic, readwrite, retain) NSArray *annotationsArray;

/**
 * Provides read-write access to the how POI callout button flag
 */
@property (nonatomic, readwrite, assign) BOOL showPOICalloutButton;


/**
 * Starts process to the obtains a route from an origin location to a destination location. There must be only
 * one active route process
 *
 * @param originLocation The route location coordinate
 * @param destinationLocation The route location coordinate
 * @param byCarFlag YES when the route must be by car, NO when the route must be on foot
 * @return YES if obtain route process can be started, NO otherwise
 * @protected
 */
- (BOOL)obtainRouteFromOriginLocation:(Location *)originLocation
                toDestinationLocation:(Location *)destinationLocation
                                byCar:(BOOL)byCarFlag;

/**
 * Subclasses must implement this selector to respond when the POI list was downloaded correctly from the server.
 * Default implementation displays all downloaded POIs into the map
 *
 * @protected
 */
- (void)poiListDownloaded;

/**
 * Subclasses must implement this selector to responde when the POI list download finished in error. Default implementation
 * does nothing
 *
 * @protected
 */
- (void)poiListDownloadFinishedWithError;

/**
 * Subclasses must implement this selector to displays the provided POI information in another view controller.
 * Default implementation does nothing
 *
 * @param poi The POI to display the information
 * @protected
 */
- (void)displayPOI:(ATMData *)poi;

/**
 * Starts displaying the steps information
 *
 * @param animated YES to animate the step view appearance, NO to simply put the step view onto the view controller's view
 * @protected
 */
- (void)startDisplayingSteps:(BOOL)animated;

/**
 * Stops displaying the steps information
 *
 * @param animated YES to animate the step view disappearance, NO to simply put the step view onto the view controller's view
 * @protected
 */
- (void)stopDisplayingSteps:(BOOL)animated;

/**
 * Displays the current route step information
 *
 * @param animated YES to animate the step view resizing, NO otherwise
 * @protected
 */
- (void)displayCurrentRouteStep:(BOOL)animated;

/**
 * Calculates the step at the given position
 *
 * @param aPosition The step position to look for
 * @return The step at the given position, or nil if step is out of bounds
 * @protected
 */
- (GoogleMapsStep *)stepAtPosition:(NSUInteger)aPosition;

/**
 * Calculates the map region to fit all the annotations and the route and sets it
 *
 * @protected
 */
- (void)fitMapRegion;

/**
 * Child subclasses notifies the parent class whether the annotation callout button is located to the right or not. Default
 * implementation locates the annotation callout button to the right
 *
 * @return YES when the annotation callout button is located to the right, NO when it is located to the left
 * @protected
 */
- (BOOL)annotationCalloutButtonLocatedRight;

/**
 * Child subclasses must return the route time and distance view superview. The route time and distance view is located
 * at the top region of its superview. Default implementation returns nil
 *
 * @protected
 */
- (UIView *)routeTimeAndDistanceViewSuperview;

/**
 * Child subclasses must return the route step view superview. The route step view is located at the top region of its superview.
 * Default implementation returns nil
 *
 * @protected
 */
- (UIView *)routeStepViewSuperview;

/**
 * Child subclasses must implement this method to perform any operation when the route step view is going to appear. It can
 * be either animated or not. Default implementation does nothing
 *
 * @protected
 */
- (void)routeStepViewWillAppear;

/**
 * Child subclasses must implement this method to perform any operation when the route step view is going to disappear. It can
 * be either animated or not. Default implementation does nothing
 *
 * @protected
 */
- (void)routeStepViewWillDisppear;

/**
 * Updates the step annotation to a provided coordinate
 *
 * @param aNewCoordinate The new step annotation coordinate
 * @protected
 */
- (void)updateStepAnnotationToCoordinate:(CLLocationCoordinate2D)aNewCoordinate;

/**
 * Toggles the display route steps flag and updates the interface
 *
 * @return YES when the route steps are being displayed, no otherwise
 * @protected
 */
- (BOOL)toggleDisplayRouteSteps;

/**
 * Clears the map locations annotations and releases them
 *
 * @protected
 */
- (void)clearMapLocationsAnnotations;

/**
 * Checks the steps navigator segmented selector enabled state
 *
 * @protected
 */
- (void)checkStepNavigatorHeaderSelectorEnabledState;

/**
 * Sets the new user coordinates
 * 
 * @param coordinates The new user coordinates to display
 * @protected
 */
- (void)setUserCoordinates:(CLLocationCoordinate2D)coordinates;

/**
 * Arranges steps and information views to iPad screen
 */
- (void)arrangeInfoViewsToiPadScreen;

@end
