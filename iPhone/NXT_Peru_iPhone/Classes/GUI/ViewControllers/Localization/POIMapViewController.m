/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "POIMapViewController.h"

#import "BBVATabBarViewController.h"
#import "ATMList.h"
#import "BranchesList.h"
#import "POIListViewController.h"
#import "ExecutionResult.h"
#import "POIFilterViewController.h"
#import "GeocodedAddressList.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "Location.h"
#import "MapRouteViewController+protected.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTNavigationItem.h"
#import "MoreTabViewController.h"
#import "OperationResult.h"
#import "POIAnnotation.h"
#import "POIDetailsATMBranchViewController.h"
#import "POIListEnRouteViewController+protected.h"
#import "POIListMultipleAddressViewController.h"
#import "POILocationSession.h"
#import "POIMapViewController+protected.h"
#import "Route.h"
#import "RouteStepsViewController.h"
#import "Session.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Defines the POIListViewController NIB file name
 */
#define POI_LIST_VIEW_CONTROLLER_NIB_FILE                                           @"POIMapViewController"


/**
 * Defines the interval to look for the user location measured in milliseconds (set to 30sec.)
 */
#define USER_LOCATION_MAXIMUM_INTERVAL                                              30.0f

/**
 * Defines the work-around timer for iOS 3.x (mapView: didUpdateUserLocation: is an iOS 4.x method in MKMapViewDelegate, but iOS 3.x has no way to get that notification)
 */
#define USER_LOCATION_UPDATE_IOS_3_X_WORK_AROUND_TIMER                              1.0f

/**
 * Defines the maximum age a location is allow to be to be accepted as valid measured in millisecons (set to 5min.)
 */
#define LOCATION_MAX_AGE_TO_BE_VALID                                                300.0f

/**
 * Defines the segment branches.
 */
#define SEGMENT_BRANCHES                                            0

/**
 * Defines the segment ATM.
 */
#define SEGMENT_ATM                                                 1

/**
 * Defines the segment agents.
 */
#define SEGMENT_AGENTS                                              2

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
/**
 * Defines the locate bank POIs operation states
 */
typedef enum {
    
    LBPOSIdle = 0, //!<There is no ongoing operation
    LBPOSLocatingUser, //!<Obtaining the user location
    LBPOSDownloadingPOIs //!<Obtaining the list of ATMs and branches around the user location
    
} LocateBankingPOIsOperationStates;


#pragma mark -

/**
 * POIListViewController private extension
 */
@interface POIMapViewController()

/**
 * Releases the graphic elements not needed when view is hidden
 *
 * @private
 */
- (void)releasePOIMapViewControllerGraphicElements;

/**
 * Starts downloading the ATMs and branches nearby
 *
 * @param aLocation The location to obtain the address for
 */
- (void)downloadATMsAndBranchesForLocation:(CLLocationCoordinate2D)aLocation;

/**
 * Checks whether the ATMs and branches nearby download finished or not. If both has finished, the list is obtained and displayed on the map
 *
 * @private
 */
- (void)checkATMsExpressAgentsAndBranchesDownloadsFinished;

/**
 * Stops the POI download process
 *
 * @private
 */
- (void)stopPOIDownloadProcess;

/**
 * Cancels any activity
 *
 * @private
 */
- (void)stopProcess;

/**
 * Cancels the information download
 *
 * @private
 */
- (void)cancelDownloads;

/**
 * Cancels the ATMs and branches nearby download
 *
 * @private
 */
- (void)cancelATMsAndBranchesDownload;

/**
 * Cancels the ATMs nearby download
 *
 * @private
 */
- (void)cancelATMsDownload;

/**
 * Cancels the branches nearby download
 *
 * @private
 */
- (void)cancelBranchesDownload;

/**
 * Starts obtaining the user location
 *
 * @private
 */
- (void)startLocatingUser;

/**
 * Checks whether the user location was stablished or not. If not stablished, a message is displayed to the user
 *
 * @private
 */
- (void)checkUserLocationStablished;

/**
 * Checks the map user location and triggers the mapView:didUpdateUserLocation: in iOS 3.x
 *
 * @private
 */
- (void)checkMapViewUserLocation;

/**
 * Starts downloading the addresses like a given string
 *
 * @param searchString The search string that addresses must be like
 * @private
 */
- (void)downloadAddressesLikeString:(NSString *)searchString;

/**
 * Jumps to the selected geocoded address
 *
 * @param geocodedAddress The geocoded address to jump to
 * @private
 */
- (void)jumpToGeocodedAddress:(GeocodedAddressData *)geocodedAddress;

/**
 * Jumps to the selected coordinates
 *
 * @param coordinate The coordinates to jump to
 * @private
 */
- (void)jumpToCordinate:(CLLocationCoordinate2D)coordinate;

/**
 * Refreshes the POIs information displayed in the screen
 *
 * @private
 */
- (void)refreshPOIsRepresentation;

/**
 * Show filter map view controller to select like filter.
 *
 * @private
 */
- (void)showFilterMap;

@end


#pragma mark -

@implementation POIMapViewController

#pragma mark -
#pragma mark Properties

@synthesize locationSearchBar = locationSearchBar_;
@synthesize informationSupportView = informationSupportView_;
@synthesize mapViewSupportView = mapViewSupportView_;
@synthesize toolbarView = toolbarView_;
@synthesize listModeButton = listModeButton_;
@synthesize typesSegmentedControl = typesSegmentedControl_;
@synthesize localizedButton = localizedButton_;
@synthesize separatorImageView = separatorImageView_;
@synthesize isBackNavigation = isBackNavigation_;

#pragma mark -
#pragma mark Memory management

/**
 * Release the used memory
 */
- (void)dealloc {
    
    [self releasePOIMapViewControllerGraphicElements];
    
    [self cancelDownloads];
    
    [downloadingATMList_ release];
    downloadingATMList_ = nil;
    
    [atmDownloadIdentifier_ release];
    atmDownloadIdentifier_ = nil;
    
    [atmOperationResult_ release];
    atmOperationResult_ = nil;
    
    [downloadingBranchesList_ release];
    downloadingBranchesList_ = nil;
    
    [branchesDownloadIdentifier_ release];
    branchesDownloadIdentifier_ = nil;
    
    [downloadingExpressAgentList_ release];
    downloadingExpressAgentList_ = nil;
    
    [expressAgentsOperationResult_ release];
    expressAgentsOperationResult_ = nil;
    
    [expressAgentDownloadIdentifier_ release];
    expressAgentDownloadIdentifier_ = nil;
    
    [routeOriginOperationResult_ release];
    routeOriginOperationResult_ = nil;
    
    [routeOriginDownloadIdentifier_ release];
    routeOriginDownloadIdentifier_ = nil;
    
    [branchesOperationResult_ release];
    branchesOperationResult_ = nil;
    
    [similarAddressesList_ release];
    similarAddressesList_ = nil;
    
    [similarAddressesOperationResult_ release];
    similarAddressesOperationResult_ = nil;
    
    [multipleAddressViewController_ release];
    multipleAddressViewController_ = nil;
    
    [poiListViewController_ release];
    poiListViewController_ = nil;
    
    [detailATMBranchViewController_ release];
    detailATMBranchViewController_ = nil;
    
    [poiFilterViewController_ release];
    poiFilterViewController_ = nil;
    
    [similarAddressesDownloadIdentifier_ release];
    similarAddressesDownloadIdentifier_ = nil;
    
    [locationString_ release];
    locationString_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory. Visual elements are released
 */
- (void)viewDidUnload {
    
    [self releasePOIMapViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasePOIMapViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases the graphic elements not needed when view is hidden
 */
- (void)releasePOIMapViewControllerGraphicElements {
    
    [locationSearchBar_ release];
    locationSearchBar_ = nil;
    
    [informationSupportView_ release];
    informationSupportView_ = nil;
    
    [mapViewSupportView_ release];
    mapViewSupportView_ = nil;
    
    [toolbarView_ release];
    toolbarView_ = nil;
    
    [listModeButton_ release];
    listModeButton_ = nil;
    
    [typesSegmentedControl_ release];
    typesSegmentedControl_ = nil;
    
    [localizedButton_ release];
    localizedButton_ = nil;
	
	[separatorImageView_ release];
	separatorImageView_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Sets the display POI right annotation
 * view to YES so a blue detail disclosure button is displayed when the callout is shown
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self setShowPOICalloutButton:YES];
    
}

/*
 * Creates and returns an autoreleased POIListViewController constructed from a NIB file
 */
+ (POIMapViewController *)poiMapViewController {
    
    POIMapViewController *result = [[[POIMapViewController alloc] initWithNibName:POI_LIST_VIEW_CONTROLLER_NIB_FILE
                                                                             bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory. Style is applied to graphic elements
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if(IS_OS_8_OR_LATER) {
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
        [self.locationManager startUpdatingLocation];
    }
    
    UIColor *clearColor = [UIColor clearColor];
    [[self view] setBackgroundColor:clearColor];
    [informationSupportView_ setBackgroundColor:clearColor];
    [mapViewSupportView_ setBackgroundColor:clearColor];
    
    [locationSearchBar_ setTintColor:[UIColor BBVAGreyToneFourColor]];
    [locationSearchBar_ setPlaceholder:NSLocalizedString(MAP_SEARCH_PLACEHOLDER_TEXT_KEY, nil)];
    
	ImagesCache *imagesCache = [ImagesCache getInstance];
	
	[separatorImageView_ setImage:[imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
    
    [informationSupportView_ bringSubviewToFront:mapViewSupportView_];
    
    [typesSegmentedControl_ setTitle:NSLocalizedString(BRANCH_POI_MAP_KEY, nil) forSegmentAtIndex:SEGMENT_BRANCHES];
    [typesSegmentedControl_ setTitle:NSLocalizedString(ATM_POI_MAP_KEY, nil) forSegmentAtIndex:SEGMENT_ATM];
    [typesSegmentedControl_ setTitle:NSLocalizedString(AGENTS_POI_MAP_KEY, nil) forSegmentAtIndex:SEGMENT_AGENTS];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        [typesSegmentedControl_ setTintColor:[UIColor BBVABlueSpectrumColor]];
    }
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        self.navigationController.navigationBar.tintColor = [UIColor BBVABlueSpectrumColor];
        
    } else
    {
        self.navigationController.navigationBar.barTintColor = [UIColor BBVABlueSpectrumColor];
        self.navigationController.navigationBar.translucent = NO;
        
    }
    
    [toolbarView_ setBackgroundColor:[UIColor BBVAGreyToneFourColor]];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        [listModeButton_ setBackgroundImage:[imagesCache imageNamed:GRADIENT_BUTTON_IMAGE_FILE] forState:UIControlStateNormal];
        [localizedButton_ setBackgroundImage:[imagesCache imageNamed:GRADIENT_BUTTON_IMAGE_FILE] forState:UIControlStateNormal];
        
    } else
    {
        [listModeButton_ setBackgroundColor:[UIColor BBVABlueSpectrumColor]];
        [localizedButton_ setBackgroundColor:[UIColor BBVABlueSpectrumColor]];
        
    }
    
    
    [listModeButton_ setImage:[imagesCache imageNamed:MAP_POI_LIST_IMAGE_FILE] forState:UIControlStateNormal];
    [localizedButton_ setImage:[imagesCache imageNamed:MAP_LOCATION_IMAGE_FILE] forState:UIControlStateNormal];
	
	isBackNavigation_ = NO;
    
}

/**
 * Notifies the view controller that its view is about to be become visible. The available information is checked to perform an action
 *
 * @param animated If YES, the view is being added to the window using an animation
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [locationSearchBar_ setText:locationString_];
	
	[typesSegmentedControl_ setSelectedSegmentIndex:[[POILocationSession getInstance] segmentedSelectedIndex]];
	
	UINavigationController *navigationController = [self navigationController];
	
	if (navigationController != nil) {
		
		[navigationController setNavigationBarHidden:NO animated:YES];
		
		if (!isBackNavigation_) {
			
			enabledLocation_ = YES;
			
			[self startLocatingATMsAndBranches];
			
		}
		
	}
    
}

/**
 * Notifies the view controller that its view is visible
 *
 * @param animated If YES, the view was added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    
    NSArray *viewControllersArray = [[self navigationController] viewControllers];
    
    if ([viewControllersArray count] >= 1) {
        
        UIViewController *viewController = [viewControllersArray objectAtIndex:0];
        
        if (viewController != self) {
            
            if (![[self appDelegate] setTabBarVisibility:NO animated:YES]) {
                
                [[self brandImageView] setHidden:NO];
                CGRect frame = [informationSupportView_ frame];
                CGFloat frameBottom = CGRectGetMaxY(frame);
                CGFloat brandTop = CGRectGetMinY([[self brandImageView] frame]);
                frame.size.height = frame.size.height + brandTop - frameBottom;
                [informationSupportView_ setFrame:frame];
                
            }
            
        } else {
            
            if ([[self appDelegate] setTabBarVisibility:YES animated:YES]) {
                
                [[self brandImageView] setHidden:YES];
                CGRect frame = [informationSupportView_ frame];
                CGFloat frameBottom = CGRectGetMaxY(frame);
                CGFloat brandBottom = CGRectGetMaxY([[self brandImageView] frame]);
                frame.size.height = frame.size.height + brandBottom - frameBottom;
                [informationSupportView_ setFrame:frame];
                
            }
            
        }
        
    }
		
	if (!isBackNavigation_ && enabledLocation_) {
		
		[[self appDelegate] showActivityIndicator:poai_Both];
		
	}
	
	isBackNavigation_ = NO;
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Editing is finished
 * 
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [locationString_ release];
    locationString_ = [[locationSearchBar_ text] retain];
    
    [[self view] endEditing:YES];
    
    [self stopProcess];
    
}

/**
 * Notifies the view controller that its view was dismissed, covered, or otherwise hidden from view.
 *
 * @param animated: If YES, the disappearance of the view was animated.
 */
- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    
    UIViewController *lastViewController = [viewControllers objectAtIndex:([viewControllers count] - 1)];
    
    if (lastViewController == nil) {
        
        [locationString_ release];
        locationString_ = nil;
        
    }
    
}

#pragma mark -
#pragma mark User iteraction

/**
 * Show list of ATM and branches.
 */
- (IBAction)showATMAndBranchesList:(id)sender {
    
    isBackNavigation_ = YES;
    
    if (poiListViewController_ == nil) {
        
        poiListViewController_ = [[POIListViewController poiListViewController] retain];
        
    }
    
    [[self navigationController] pushViewController:poiListViewController_ animated:YES];
    
}

#pragma mark -
#pragma mark Filter map methods

/*
 * Show filter map view controller to select like filter.
 */
- (void)showFilterMap {
    
    if (poiFilterViewController_ == nil) {
        
        poiFilterViewController_ = [[POIFilterViewController poiFilterViewController] retain];
        
    }
    
    [poiFilterViewController_ setDelegate:self];
	
	isBackNavigation_ = YES;
    
    UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:poiFilterViewController_] autorelease];
	
	[[self appDelegate] presentModalViewController:navController animated:YES];
    
}


/*
 * User location in mapview.
 */
- (IBAction)jumpToUserLocation:(id)sender {
	
	POILocationSession *poiSession = [POILocationSession getInstance];
	
	[poiSession setBranchList:nil];
	[poiSession setATMList:nil];
	[poiSession setAgentsList:nil];
    
    [self startLocatingUser];
    
}

/**
 * The segmented state is changed.
 */
- (IBAction)segmentedChangedState:(id)sender {
    
    NSUInteger selectedIndex = [typesSegmentedControl_ selectedSegmentIndex];
	
	POILocationSession *poiSession = [POILocationSession getInstance];
	
	[poiSession setSegmentedSelectedIndex:selectedIndex];
    
    switch (selectedIndex) {
            
        case SEGMENT_BRANCHES:
            
            [self setPoiList:[poiSession branchList]];
            
            break;
            
        case SEGMENT_ATM:
            
            [self setPoiList:[poiSession atmFilteredList]];
            
            break;
            
        case SEGMENT_AGENTS:
            
            [self setPoiList:[poiSession agentsFilteredList]];
            
            break;
            
        default:
            
            break;
            
    }
    
    [self refreshPOIsRepresentation];
    
}

#pragma mark -
#pragma mark Location process cycle

/*
 * Starts obtaining the user location
 */
- (void)startLocatingUser {
    
    if (locateBankingPOIsOperationState_ == LBPOSIdle) {
		
		MKMapView *mapView = [self mapView];
		
		[[POILocationSession getInstance] setUserLocation:nil];
		
		[mapView removeAnnotations:[mapView annotations]];
		
		[self setAnnotationsArray:nil];
        
        locateBankingPOIsOperationState_ = LBPOSLocatingUser;
        
        userLocatingProcessStartTime_ = [NSDate timeIntervalSinceReferenceDate];
        
        UIDevice *device = [UIDevice currentDevice];
        NSString *osVersion = device.systemVersion;
        
        if ([osVersion compare:@"4.0" options:NSNumericSearch] == NSOrderedAscending) {
            
            [self performSelector:@selector(checkMapViewUserLocation) withObject:nil afterDelay:USER_LOCATION_UPDATE_IOS_3_X_WORK_AROUND_TIMER];
        }
        
        [self performSelector:@selector(checkUserLocationStablished) withObject:nil afterDelay:USER_LOCATION_MAXIMUM_INTERVAL];
        
		[mapView setShowsUserLocation:YES];
        
    }
    
}

/*
 * Checks whether the user location was stablished or not. If not stablished, a message is displayed to the user
 */
- (void)checkUserLocationStablished {
    
    if ([[POILocationSession getInstance] userLocation] == nil) {
        
        MKMapView *mapView = [self mapView];
        
        [mapView setShowsUserLocation:NO];
        locateBankingPOIsOperationState_ = LBPOSIdle;
        [[self appDelegate] hideActivityIndicator];
		enabledLocation_ = NO;
        [Tools showInfoWithMessage:NSLocalizedString(ERROR_LOCATING_USER_KEY, nil)];
        
    }
    
}

/*
 * Checks the map user location and triggers the mapView:didUpdateUserLocation: in iOS 3.x
 */
- (void)checkMapViewUserLocation {
    
    MKMapView *mapView = self.mapView;
    
    MKUserLocation *mapViewUserLocation = mapView.userLocation;
    
    if (mapViewUserLocation != nil) {
        
        [self mapView:mapView didUpdateUserLocation:mapViewUserLocation];
        
    }
    
    [self performSelector:@selector(checkMapViewUserLocation) withObject:nil afterDelay:USER_LOCATION_UPDATE_IOS_3_X_WORK_AROUND_TIMER];
    
}

#pragma mark -
#pragma mark User location management

/*
 * Jumps to the selected geocoded address
 */
- (void)jumpToGeocodedAddress:(GeocodedAddressData *)geocodedAddress {
    
    if (geocodedAddress != nil) {
        
        [[self mapView] setShowsUserLocation:NO];
        
        GeoPositionData *geoPosition = geocodedAddress.geoPosition;
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = geoPosition.latitude;
        coordinate.longitude = geoPosition.longitude;
        
        [self jumpToCordinate:coordinate];
        
    }
    
}

/*
 * Jumps to the selected coordinates
 */
- (void)jumpToCordinate:(CLLocationCoordinate2D)coordinate {
    
    CLLocationDegrees latitude = coordinate.latitude;
    CLLocationDegrees longitude = coordinate.longitude;
    
    if ((latitude >= -90.0f) && (latitude <= 90.0f) && (longitude >= -180.0f) && (longitude <= 180.0f)) {
        
        [self cancelDownloads];
        [self setPoiList:nil];
        [self setAnnotationsArray:nil];
        [self setUserCoordinates:coordinate];
        
        // Set user location
		[[POILocationSession getInstance] setUserLocation:[[[Location allocWithZone:[self zone]] initWithGeographicCoordinate:coordinate sensorCoordinate:YES formattedAddress:nil] autorelease]];
        
        locateBankingPOIsOperationState_ = LBPOSLocatingUser;
        [self downloadATMsAndBranchesForLocation:coordinate];
        
    }
    
}

#pragma mark -
#pragma mark Refreshing interface

/*
 * Refreshes the POIs information displayed in the screen
 */
- (void)refreshPOIsRepresentation {
    
    NSMutableArray *annotationsArray = [NSMutableArray array];
    BaseAnnotation *annotation = nil;
    NSArray *poiList = [self poiList];
    NSZone *zone = [self zone];
    
    for (ATMData *poiData in poiList) {
        
        annotation = [[[POIAnnotation allocWithZone:zone] initWithAssociatedPOI:poiData] autorelease];
        
        if (annotation != nil) {
            
            [annotationsArray addObject:annotation];
            
        }
        
    }
    
    [self setAnnotationsArray:annotationsArray];
    
    [self fitMapRegion];
    
}

#pragma mark -
#pragma mark FilterMapViewControllerDelegate protocol selectors

/**
 * This method is called when filter is finished.
 */
- (void)didFinishFilterWithOptions:(NSArray *)array {
	
	[[POILocationSession getInstance] setPOIFilter:array];
    
    [poiFilterViewController_ dismissModalViewControllerAnimated:YES];
    
    [self segmentedChangedState:nil];
    
}

#pragma mark -
#pragma mark POI management

/*
 * Starts downloading the ATMs and branches nearby
 */
- (void)downloadATMsAndBranchesForLocation:(CLLocationCoordinate2D)aLocation {
    
    if (locateBankingPOIsOperationState_ == LBPOSLocatingUser) {
        
        [self cancelATMsAndBranchesDownload];
        
        [downloadingATMList_ release];
        downloadingATMList_ = nil;
        downloadingATMList_ = [[ATMList alloc] init];
        
        [downloadingBranchesList_ release];
        downloadingBranchesList_ = nil;
        downloadingBranchesList_ = [[BranchesList alloc] init];
        
        [downloadingExpressAgentList_ release];
        downloadingExpressAgentList_ = nil;
        downloadingExpressAgentList_ = [[ATMList alloc] init];
        
        [atmDownloadIdentifier_ release];
        atmDownloadIdentifier_ = [[GeoServerManager atmListForLatitute:aLocation.latitude
                                                          andLongitude:aLocation.longitude
                                                           forListener:self
                                                   withOpertaionResult:atmOperationResult_
                                                     positionNewCharge:0
                                                            storedInto:downloadingATMList_] retain];
        
        [branchesDownloadIdentifier_ release];
        branchesDownloadIdentifier_ = [[GeoServerManager branchListForLatitute:aLocation.latitude
                                                                  andLongitude:aLocation.longitude
                                                                   forListener:self
                                                           withOpertaionResult:nil
                                                             positionNewCharge:0
                                                                    storedInto:downloadingBranchesList_] retain];
        
        [expressAgentDownloadIdentifier_ release];
        expressAgentDownloadIdentifier_ = [[GeoServerManager expressAgentListForLatitute:aLocation.latitude
                                                                            andLongitude:aLocation.longitude
                                                                             forListener:self
                                                                     withOpertaionResult:expressAgentsOperationResult_ positionNewCharge:0
                                                                              storedInto:downloadingExpressAgentList_] retain];
        
        locateBankingPOIsOperationState_ = LBPOSDownloadingPOIs;
        
        NSString *errorMessage = nil;
        
        if (([atmDownloadIdentifier_ length] == 0) && ([branchesDownloadIdentifier_ length] == 0)) {
            
            //            errorMessage = NSLocalizedString(ERROR_SENDING_BOTH_LISTS_REQUEST_KEY, nil);
            [self stopPOIDownloadProcess];
            
        } else if ([atmDownloadIdentifier_ length] == 0) {
            
            errorMessage = NSLocalizedString(ERROR_SENDING_ATMS_LIST_REQUEST_KEY, nil);
            [self cancelATMsDownload];
            [[self appDelegate] showActivityIndicator:poai_Both];
            
            
        } else if ([branchesDownloadIdentifier_ length] == 0) {
            
            errorMessage = NSLocalizedString(ERROR_SENDING_BANCHES_LIST_REQUEST_KEY, nil);
            [self cancelBranchesDownload];
            [[self appDelegate] showActivityIndicator:poai_Both];
            
            
        } else {
            
            [[self appDelegate] showActivityIndicator:poai_Both];
            
        }
        
        if ([errorMessage length] > 0) {
            
            [Tools showErrorWithMessage:errorMessage];
            
        }
        
    } else {
        
        //        [Tools showErrorWithMessage:NSLocalizedString(ERROR_SENDING_BOTH_LISTS_REQUEST_KEY, nil)];
        [self stopPOIDownloadProcess];
        
    }
    
}

/*
 * Checks whether the ATMs and branches nearby download finished or not. If both has finished, the list is obtained and displayed on the map
 */
- (void)checkATMsExpressAgentsAndBranchesDownloadsFinished {
    
    if ((atmDownloadIdentifier_ == nil) && (branchesDownloadIdentifier_ == nil) && (expressAgentDownloadIdentifier_ == nil)) {
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                 selector:@selector(checkMapViewUserLocation)
                                                   object:nil];
		
		POILocationSession *poiSession = [POILocationSession getInstance];
		
		[poiSession setATMList:downloadingATMList_];
        
		[poiSession setAgentsList:downloadingExpressAgentList_];
        
		[poiSession setBranchList:downloadingBranchesList_];
        
        [downloadingATMList_ release];
        downloadingATMList_ = nil;
        
        [downloadingExpressAgentList_ release];
        downloadingExpressAgentList_ = nil;
        
        [downloadingBranchesList_ release];
        downloadingBranchesList_ = nil;
		
		locateBankingPOIsOperationState_ = LBPOSIdle;
		
		[poiSession setPOIFilter:[poiSession poiFilter]];
        
        [typesSegmentedControl_ setSelectedSegmentIndex:[typesSegmentedControl_ selectedSegmentIndex]];
        
        [self segmentedChangedState:nil];
		
		[[self appDelegate] hideActivityIndicator];
        
        if ([[poiSession atmList] count] == 0 && [[poiSession branchList] count] == 0 && [[poiSession agentsList] count] == 0) {
            
            [Tools showInfoWithMessage:NSLocalizedString(NO_NEARBY_ATMS_OR_BRANCHES_KEY, nil)];
            
        } else if ([[poiSession atmList] count] == 0) {
            
            [Tools  showInfoWithMessage:NSLocalizedString(NO_NEARBY_ATMS_KEY, nil)];
            
        } else if ([[poiSession branchList] count] == 0) {
            
            [Tools  showInfoWithMessage:NSLocalizedString(NO_NEARBY_BRANCHES_KEY, nil)];
            
        } else if ([[poiSession agentsList] count] == 0) {
            
            [Tools showInfoWithMessage:NSLocalizedString(NO_NEARBY_EXPRESS_AGENTS_KEY, nil)];
            
        }
        
    }
    
}

/*
 * Starts the ATMs and branches locate process
 */
- (void)startLocatingATMsAndBranches {
    
    [self stopPOIDownloadProcess];
    
    if (locateBankingPOIsOperationState_ == LBPOSIdle) {
        
        [self startLocatingUser];
        
    }
    
}

/*
 * Stops the POI download process
 */
- (void)stopPOIDownloadProcess {
    
    [self stopProcess];
	[[POILocationSession getInstance] setUserLocation:nil];
    
}

/*
 * Cancels any activity
 */
- (void)stopProcess {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(checkMapViewUserLocation) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(checkUserLocationStablished) object:nil];
    
    [self cancelDownloads];
    
    [[self mapView] setShowsUserLocation:NO];
    
    locateBankingPOIsOperationState_ = LBPOSIdle;
    
    [[self appDelegate] hideActivityIndicator];
    
}

/*
 * Cancels the information download
 */
- (void)cancelDownloads {
    
    [self cancelATMsAndBranchesDownload];
    
}

/*
 * Cancels the ATMs and branches nearby download
 */
- (void)cancelATMsAndBranchesDownload {
    
    [self cancelATMsDownload];
    [self cancelBranchesDownload];
    
}

/*
 * Cancels the ATMs nearby download
 */
- (void)cancelATMsDownload {
    
    if ([atmDownloadIdentifier_ length] > 0) {
        
        [GeoServerManager cancelDownload:atmDownloadIdentifier_];
        
    }
    
    [atmDownloadIdentifier_ release];
    atmDownloadIdentifier_ = nil;
    
    [downloadingATMList_ release];
    downloadingATMList_ = nil;
    
}

/*
 * Cancels the branches nearby download
 */
- (void)cancelBranchesDownload {
    
    if ([branchesDownloadIdentifier_ length] > 0) {
        
        [GeoServerManager cancelDownload:branchesDownloadIdentifier_];
        
    }
    
    [branchesDownloadIdentifier_ release];
    branchesDownloadIdentifier_ = nil;
    
    [downloadingBranchesList_ release];
    downloadingBranchesList_ = nil;
    
}

#pragma mark -
#pragma mark DownloadListener methods

/**
 * Notifies the download has ju6st started. Do nothing
 *
 * @param aDownloadId The download identification
 * @param aSize The downloaded data size if known, 0 if unknown
 */
- (void)startDownload:(NSString *)aDownloadId withSize:(unsigned long long)aSize {
}

/**
 * Notifies the download has just finished. The spinning wheel is removed and the information received is analyzed
 *
 * @param aDownloadId The download identification
 * @param aSize The downloaded data size
 */
- (void)endDownload:(NSString *)aDownloadId withSize:(unsigned long long)aSize {
    
    numberOfConetions_--;
    
    if( numberOfConetions_ <= 0){
//        [[self appDelegate] hideActivityIndicator];
        numberOfConetions_ = 0;
        
    }
    
    if ([similarAddressesDownloadIdentifier_ isEqualToString:aDownloadId]) {
        
        NSInteger addressesCount = [similarAddressesList_ geocodedAddressCount];
        
        if (addressesCount == 0) {
			
			[[self appDelegate] hideActivityIndicator];
            
            [Tools showErrorWithMessage: NSLocalizedString(NO_SIMILAR_ADDRESSES_TO_STRING_KEY, nil)];
            
        } else if (addressesCount == 1) {
            			
//            [branchList_ removeAllObjects];
//            [atmList_ removeAllObjects];
            
            GeocodedAddressData *address = [similarAddressesList_ geocodedAddressAtPosition:0];
            [self jumpToGeocodedAddress:address];
//            [routeOriginAddress_ release];
//            routeOriginAddress_ = nil;
//            routeOriginAddress_ = [address.address retain];
//            //NSLog(@"Direccion %@",routeOriginAddress_.addressFirstLine);
            
        } else {
			
			[[self appDelegate] hideActivityIndicator];
            
            if (multipleAddressViewController_ == nil) {
                
                multipleAddressViewController_ = [[POIListMultipleAddressViewController poiListMultipleAddressViewController] retain];
                [multipleAddressViewController_ setDelegate:self];
                
            }
            
            [multipleAddressViewController_ setGeocoderAddressList:similarAddressesList_];
            
			[[self appDelegate] presentModalViewController:multipleAddressViewController_ animated:YES];
    
        }
        
        [similarAddressesDownloadIdentifier_ release];
        similarAddressesDownloadIdentifier_ = nil;
        [similarAddressesOperationResult_ release];
        similarAddressesOperationResult_ = nil;
        [similarAddressesList_ release];
        similarAddressesList_ = nil;
        
    } else if ([aDownloadId isEqualToString:atmDownloadIdentifier_]) {
        
        
        [atmDownloadIdentifier_ release];
        atmDownloadIdentifier_ = nil;
        
        NSInteger resultCode = [[atmOperationResult_ result] code];
        
        if ((resultCode  == RESULT_OK) || (resultCode == WARNING_NEW_VERSION)) {
            
            [self checkATMsExpressAgentsAndBranchesDownloadsFinished];
            
        } else {
            
            [self stopProcess];
            
            switch (resultCode) {
                    
                case CLIENT_ERROR:
                    
                    [Tools showErrorWithMessage:NSLocalizedString(CLIENT_ERROR_DOWNLOADING_ATM_LIST_KEY, nil)];
                    
                    break;
                    
                case ERROR_PARAMETERS_SENT:
                case ERROR_NEW_VERSION:
                    
                    [Tools showErrorWithMessage:NSLocalizedString(SERVER_ERROR_DOWNLOADING_ATM_LIST_KEY, nil)];
                    
                    break;
                    
                default:
                    
                    break;
                    
            }
            
        }
        
    } else if ([aDownloadId isEqualToString:expressAgentDownloadIdentifier_]) {
        
        [expressAgentDownloadIdentifier_ release];
        expressAgentDownloadIdentifier_ = nil;
        
        NSInteger resultCode = [[expressAgentsOperationResult_ result] code];
        
        if ((resultCode == RESULT_OK) || (resultCode == WARNING_NEW_VERSION)) {
            
            [self checkATMsExpressAgentsAndBranchesDownloadsFinished];
            
        } else {
            
            [self stopProcess];
            
            switch (resultCode) {
                    
                case CLIENT_ERROR:
                    
                    [Tools showErrorWithMessage:NSLocalizedString(CLIENT_ERROR_DOWNLOADING_EXPRESS_AGENTS_LIST_KEY, nil)];
                    break;
                    
                case ERROR_PARAMETERS_SENT:
                case ERROR_NEW_VERSION:
                    
                    [Tools showErrorWithMessage:NSLocalizedString(SERVER_ERROR_DOWNLOADING_EXPRESS_AGENTS_LIST_KEY, nil)];
                    break;
                    
                default:
                    
                    break;
                    
            }
            
        }
        
    } else if ([aDownloadId isEqualToString:branchesDownloadIdentifier_]) {
        
        [branchesDownloadIdentifier_ release];
        branchesDownloadIdentifier_ = nil;
        
        NSInteger resultCode = [[branchesOperationResult_ result] code];
        
        if ((resultCode == RESULT_OK) || (resultCode == WARNING_NEW_VERSION)) {
            
            [self checkATMsExpressAgentsAndBranchesDownloadsFinished];
            
        } else {
            
            [self stopProcess];
            
            switch (resultCode) {
                    
                case CLIENT_ERROR:
                    
                    [Tools showErrorWithMessage:NSLocalizedString(CLIENT_ERROR_DOWNLOADING_BRANCHES_LIST_KEY, nil)];
                    break;
                    
                case ERROR_PARAMETERS_SENT:
                case ERROR_NEW_VERSION:
                    
                    [Tools showErrorWithMessage:NSLocalizedString(SERVER_ERROR_DOWNLOADING_BRANCHES_LIST_KEY, nil)];
                    break;
                    
                default:
                    
                    break;
                    
            }
            
        }
        
    } else if ([routeOriginDownloadIdentifier_ isEqualToString:aDownloadId]) {
        
        [routeOriginDownloadIdentifier_ release];
        routeOriginDownloadIdentifier_ = nil;
        
        [routeOriginOperationResult_ release];
        routeOriginOperationResult_ = nil;
        
    }
    
}

/**
 * Notifies the download has finished in error. The spinning wheel is removed and the error message is displayed to the user
 *
 * @param aDownloadId The download identification
 * @param anError The download error
 * @param aXMLString parsed
 */
- (void)download:(NSString *)aDownloadId error:(NSError *)anError xmlString:(NSString *)aXMLString {
    
    numberOfConetions_--;
    
    if( numberOfConetions_ <= 0){
        
        [[self appDelegate] hideActivityIndicator];
        
        numberOfConetions_ = 0;
        
    }
    
    [Tools showErrorWithMessage:[anError localizedDescription]];
    NSString *errorMessage = NSLocalizedString(TECNICAL_ERROR_TEXT_KEY, nil);
    [Tools showErrorWithMessage:errorMessage];
    
}

/**
 * Notifies the download has finished with a status code different than OK_STATUS (200). The activity indicator is removed and the error is displayed to the user
 *
 * @param aDownloadId The download identification
 * @param aStatusCode The status code that finished the download
 */
- (void)download:(NSString *)aDownloadId finishedWithStatusCode:(NSInteger)aStatusCode {
    
    numberOfConetions_ --;
    
    if( numberOfConetions_ <= 0){
        
        [self.appDelegate hideActivityIndicator];
        numberOfConetions_=0;
        
    }   
    
    NSString *errorMessage = NSLocalizedString(TECNICAL_ERROR_TEXT_KEY, nil);//[NSString stringWithFormat:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil), aStatusCode];
    [Tools showErrorWithMessage:errorMessage];
    
}

/**
 * Notifies the download was cancelled. The activity indicator is removed from the view
 *
 * @param aDownloadId The download identification
 */
- (void)cancelledDownload:(NSString *)aDownloadId {
    
    numberOfConetions_ --;
    
    if( numberOfConetions_ <= 0){
        
        [[self appDelegate] hideActivityIndicator];
        numberOfConetions_ = 0;
        
    }   
    
} 

/**
 * Downloads the address for a given location
 *
 * @param aLocation The location which address is going to be retrieved
 */
- (void)downloadAddressForLocation:(CLLocation *)aLocation {
    
    numberOfConetions_ --;
    
    if( numberOfConetions_ <= 0){
        
        [[self appDelegate] hideActivityIndicator];
        numberOfConetions_ = 0;
        
    }   
    
    
//    if (aLocation != nil) {
        
        //        if ([routeOriginDownloadIdentifier_ length] > 0) {
        //            
        //            [GeoServerManager cancelDownload:routeOriginDownloadIdentifier_];
        //            
        //        }
        //        
        //        [routeOriginAddress_ release];
        //        routeOriginAddress_ = nil;
        //        routeOriginAddress_ = [[AddressData alloc] init];
        //        
        //        [routeOriginOperationResult_ release];
        //        routeOriginOperationResult_ = nil;
        //        routeOriginOperationResult_ = [[OperationResult alloc] initWithDelegate:routeOriginAddress_];
        //        
        //        [self.appDelegate showActivityIndicator:poai_Both onActiveWindow:NO];
        //        
        //        numberOfConetions_ ++;      
        //        
        //        CLLocationCoordinate2D coordinates = aLocation.coordinate;
        //        [routeOriginDownloadIdentifier_ release];
        //        routeOriginDownloadIdentifier_ = nil;
        //        routeOriginDownloadIdentifier_ = [[GeoServerManager addressForLatitute:coordinates.latitude longitude:coordinates.longitude
        //                                                                   forListener:self analyzedBy:routeOriginOperationResult_] retain];
        //        
        //        if ([routeOriginDownloadIdentifier_ length] == 0) {
        //            
        //            [routeOriginDownloadIdentifier_ release];
        //            routeOriginDownloadIdentifier_ = nil;
        //            
        //            [routeOriginOperationResult_ release];
        //            routeOriginOperationResult_ = nil;
        //            
        //            [routeOriginAddress_ release];
        //            routeOriginAddress_ = nil;
        //            
        //        }
        
//    }
    
}


#pragma mark -
#pragma mark Address like download management

/*
 * Starts downloading the addresses like a given string
 */
- (void)downloadAddressesLikeString:(NSString *)searchString {
    
    if (similarAddressesDownloadIdentifier_ != nil) {
        
        [GeoServerManager cancelDownload:similarAddressesDownloadIdentifier_];
        [similarAddressesDownloadIdentifier_ release];
        similarAddressesDownloadIdentifier_ = nil;
        
        [similarAddressesList_ release];
        similarAddressesList_ = nil;
        
        [similarAddressesOperationResult_ release];
        similarAddressesOperationResult_ = nil;
        
    }
    
    similarAddressesList_ = [[GeocodedAddressList alloc] init];
    similarAddressesOperationResult_ = [[OperationResult alloc] initWithDelegate:similarAddressesList_];
    
    [[self appDelegate] showActivityIndicator:poai_Both];
    
    numberOfConetions_ ++;
    
    similarAddressesDownloadIdentifier_ = [[GeoServerManager streetNamesLikeString:searchString
                                                                       forListener:self
                                                                        analyzedBy:similarAddressesOperationResult_] retain];
    
    if ([similarAddressesDownloadIdentifier_ length] == 0) {
        
        [similarAddressesDownloadIdentifier_ release];
        similarAddressesDownloadIdentifier_ = nil;
        
        [similarAddressesList_ release];
        similarAddressesList_ = nil;
        
        [similarAddressesOperationResult_ release];
        similarAddressesOperationResult_ = nil;
        
    } else {
        
        [locationSearchBar_ resignFirstResponder];
        
    }
    
}

#pragma mark -
#pragma mark MKMapViewDelegate methods

/**
 * Tells the delegate that the location of the user was updated. If distance to previous used location is greater than the used location, a new branches and
 * ATMs is started
 *
 * @param mapView The map view that is tracking the user’s location
 * @param userLocation The location object representing the user’s latest location
 */
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    if (locateBankingPOIsOperationState_ != LBPOSIdle) {
        
#if TARGET_IPHONE_SIMULATOR
        CLLocation *newLocation = [[[CLLocation alloc] initWithLatitude:-12.043333f longitude:-77.083954f] autorelease];
#else
        CLLocation *newLocation = userLocation.location;
#endif
        NSDate *locationDate = [newLocation timestamp];
        NSDate *currentDate = [NSDate date];
        
        if ([currentDate timeIntervalSinceDate:locationDate] < LOCATION_MAX_AGE_TO_BE_VALID) {
            
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(checkUserLocationStablished) object:nil];
            
            CLLocationDistance distanceToNewLocation = 1000.0f;
            
			POILocationSession *poiSession = [POILocationSession getInstance];
			
            CLLocation *location = nil;
            
            if ([poiSession userLocation] != nil) {
                
                CLLocationCoordinate2D coordinates = [[poiSession userLocation] geographicCoordinate];
                location = [[[CLLocation allocWithZone:[self zone]] initWithLatitude:coordinates.latitude
                                                                           longitude:coordinates.longitude] autorelease];
                
            }
            
            if ([newLocation respondsToSelector:@selector(distanceFromLocation:)]) {
                
                if (location != nil) {
                    
                    distanceToNewLocation = [newLocation distanceFromLocation:location];
                    
                }
                
            } else {
                
                if (location != nil) {
                    
#if defined(__IPHONE_3_2) && (__IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2)
                    distanceToNewLocation = [newLocation distanceFromLocation:location];
#else
                    distanceToNewLocation = [newLocation getDistanceFrom:location];
#endif
                    
                }
                
            }
            
            if (([poiSession userLocation] == nil) || (distanceToNewLocation > 500)) {
                
                if ([NSDate timeIntervalSinceReferenceDate] < (userLocatingProcessStartTime_ + USER_LOCATION_MAXIMUM_INTERVAL)) {
                    
                    //                    BOOL useLocation = YES;
                    //                    
                    //                    if (userLocation_ != nil) {
                    //                        
                    //                        CLLocationAccuracy currentAccuracy = userLocation_.accuracy;
                    //                        CLLocationAccuracy newAccuracy = [newLocation horizontalAccuracy];
                    //
                    //                        if ((currentAccuracy > newAccuracy) && ((currentAccuracy + newAccuracy) < distanceToNewLocation)) {
                    //                            
                    //                            useLocation = NO;
                    //                            
                    //                        }
                    //                        
                    //                    }
                    //                    
                    //                    if (useLocation) {
                    
					[poiSession setUserLocation:[[[Location allocWithZone:[self zone]] initWithGeographicCoordinate:[newLocation coordinate] sensorCoordinate:YES formattedAddress:nil] autorelease]];
                    CLLocationCoordinate2D coordinate = [[poiSession userLocation] geographicCoordinate];
                    [self setUserCoordinates:coordinate];
                    [self cancelDownloads];
                    locateBankingPOIsOperationState_ = LBPOSLocatingUser;
                    [self downloadATMsAndBranchesForLocation:coordinate];
                    
                    [self fitMapRegion];
                    
                    //                    }
                    
                }
                
            }
            
        }
        
    }
    
    if (locateBankingPOIsOperationState_ == LBPOSLocatingUser) {
        
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, 30000.0f, 30000.0f);
        [self.mapView setRegion:region animated:NO];
        
    }
    
}

/**
 * Tells the delegate that an attempt to locate the user’s position failed. Notifies the user that it was impossible to find his/her location
 *
 * @param mapView The map view that is tracking the user’s location
 * @param error An error object containing the reason why location tracking failed
 */
- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error {
    
    [self stopPOIDownloadProcess];
    
	[[self appDelegate] hideActivityIndicator];
	
	enabledLocation_ = NO;
	
    NSString *errorMessage = NSLocalizedString(ERROR_LOCATING_USER_KEY, nil);
    
    [Tools showErrorWithMessage:errorMessage];
    
}

#pragma mark -
#pragma mark UISearchBardDelegate selectors

/**
 * Asks the delegate if editing should begin in the specified search bar. The cancel button is displayed
 *
 * @param searchBar The search bar that is being edited
 * @return Always YES
 */
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    if (searchBar == locationSearchBar_) {
        
        [locationSearchBar_ setShowsCancelButton:YES
                                        animated:YES];
        
    }
    
    return YES;
    
}

/**
 * Asks the delegate if editing should stop in the specified search bar. The editing is allowed to stop
 *
 * @param searchBar The search bar that is being edited
 * @return Always YES
 */
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    
    if (searchBar == locationSearchBar_) {
        
        [locationSearchBar_ setShowsCancelButton:NO
                                        animated:YES];
        
    }
    
    return YES;
    
}

/**
 * Tells the delegate that the cancel button was tapped. Search editing is stopped
 *
 * @param searchBar The search bar that was tapped
 */
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    if (searchBar == locationSearchBar_) {
        
        [locationSearchBar_ endEditing:YES];
        
    }
    
}

/**
 * Tells the delegate that the search button was tapped. The search process is started invoking the child class selector
 *
 * @param searchBar The search bar that was tapped
 */
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    if (searchBar == locationSearchBar_) {
        
        [locationSearchBar_ resignFirstResponder];
        NSString *searchString = searchBar.text;
        
        if ([searchString length] > 0) {
            
            [self downloadAddressesLikeString:searchString];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark POIListEnRouteViewController selectors

/**
 * Subclasses must implement this selector to displays the provided POI information in another view controller.
 * A POIDetailViewController is pushed into the navigation stack to display the POI information
 *
 * @param poi The POI to display the information
 * @protected
 */
- (void)displayPOI:(ATMData *)poi {
    
    [[self view] endEditing:YES];
    
    isBackNavigation_ = YES;
    
    if (detailATMBranchViewController_ == nil) {
        
        detailATMBranchViewController_ = [[POIDetailsATMBranchViewController poiDetailsATMBranchViewController] retain];
        
    }
	
	switch ([typesSegmentedControl_ selectedSegmentIndex]) {
            
        case SEGMENT_BRANCHES:
            
			[detailATMBranchViewController_ setBranchData:poi];
            
            break;
            
        case SEGMENT_ATM:
            
			[detailATMBranchViewController_ setATMData:poi isAgent:NO];
            
            break;
            
        case SEGMENT_AGENTS:
            
			[detailATMBranchViewController_ setATMData:poi isAgent:YES];
            
            break;
            
        default:
            
            break;
            
    }
    
    [detailATMBranchViewController_ setShouldClearInterfaceData:YES];
    
    [[self navigationController] pushViewController:detailATMBranchViewController_ animated:YES];
    
}

#pragma mark -
#pragma mark POIListMultipleAddressViewControllerDelegate methods

/**
 * It informs that the POIListMultipleAddressViewController view is wanted close
 */
- (void)canceledListMultipleAddress {
    
	[[self appDelegate] dismissModalViewControllerAnimated:YES];
    
}

/**
 * It informs when the user selected a address
 */
- (void)selectedGeocodedAddress:(GeocodedAddressData *)geocodedAddressData {
    
	[[self appDelegate] dismissModalViewControllerAnimated:YES];
    
    [self jumpToGeocodedAddress:geocodedAddressData];
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Return the navigation item used to represent the view controller. Inserts the display mode segmented selector at the right
 *
 * @return The navigation item used to represent the view controller
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [super customNavigationItem];
    
    [[result customTitleView] setTopLabelText:NSLocalizedString(LOCATION_TEXT_KEY, nil)];
	
	[result setRightBarButtonItem:[[[UIBarButtonItem alloc] initWithImage:[[ImagesCache getInstance] imageNamed:MAP_FILTER_IMAGE_FILE] style:UIBarButtonItemStyleBordered target:self action:@selector(showFilterMap)] autorelease]];
    
    return result;
    
}

@end
