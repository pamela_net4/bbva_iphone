/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "POIListEnRouteViewController.h"
#import "POIListEnRouteViewController+protected.h"
#import "MapRouteViewController+protected.h"
#import "Tools.h"
#import "StringKeys.h"
#import "ATMList.h"
#import "GoogleMapsLeg.h"
#import "GoogleMapsRoute.h"
#import "GoogleMapsStep.h"
#import "GoogleMapsLocation.h"
#import "GoogleMapsPolyline.h"
#import "GoogleMapsDistance.h"
#import "GoogleMapsDuration.h"
#import "POIAnnotation.h"
#import "DestinationAnnotation.h"
#import "OriginAnnotation.h"
#import "RouteStepView.h"
#import "TimeAndDistanceHeaderView.h"
#import "XMLParserDataDownloadClient+protected.h"
#import "Location.h"
#import "GoogleMapsInstructions.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "GeocodedAddressList.h"
/**
 * Defines the steps view animation duration, measured in seconds
 */
#define STEPS_VIEW_ANIMATION_DURATION                                               0.5f

#pragma mark -

@implementation POIListEnRouteViewController(protected)

#pragma mark -
#pragma mark Route management

/*
 * Starts process to the obtains a route from an origin location to a destination location. There must be only
 * one active route process
 */
- (BOOL)obtainRouteFromOriginLocation:(Location *)anOriginLocation
                toDestinationLocation:(Location *)aDestinationLocation
                                byCar:(BOOL)byCarFlag {
    
    [self clearMapLocationsAnnotations];
    
    if (originLocation_ != anOriginLocation) {
        
        [originLocation_ release];
        originLocation_ = nil;
        originLocation_ = [anOriginLocation retain];
        
    }
    
    if (destinationLocation_ != aDestinationLocation) {
        
        [destinationLocation_ release];
        destinationLocation_ = nil;
        destinationLocation_ = [aDestinationLocation retain];
        
    }
    
    MKMapView *mapView = [self mapView];
    
    originAnnotation_ = [[OriginAnnotation alloc] init];
    
    if (originAnnotation_ != nil) {
        
        [originAnnotation_ setCoordinate:[originLocation_ geographicCoordinate]];
        [originAnnotation_ setTitle:NSLocalizedString(ORIGIN_LOCATION_TEXT_KEY, nil)];
        [mapView addAnnotation:originAnnotation_];

    }
    
    destinationAnnotation_ = [[DestinationAnnotation alloc] init];
    
    if (destinationAnnotation_ != nil) {
        
        [destinationAnnotation_ setCoordinate:[destinationLocation_ geographicCoordinate]];
        [destinationAnnotation_ setTitle:NSLocalizedString(DESTINATION_LOCATION_TEXT_KEY, nil)];
        [mapView addAnnotation:destinationAnnotation_];
        
    }
    
    [self setRouteInformation:nil];
    
    BOOL result = [self obtainRouteFromOrigin:[originLocation_ geographicCoordinate]
                                toDestination:[destinationLocation_ geographicCoordinate]
                                        byCar:byCarFlag
                           obtainedWithSensor:([originLocation_ sensorCoordinate] || [destinationLocation_ sensorCoordinate])];
    
    return result;
    
}

#pragma mark -
#pragma mark User location management

/*
 * Sets the new user coordinates
 */
- (void)setUserCoordinates:(CLLocationCoordinate2D)coordinates {
    
    MKMapView *mapView = [self mapView];
    
    if (userLocationAnnotation_ != nil) {
        
        [mapView removeAnnotation:userLocationAnnotation_];
        
    } else {
        
        userLocationAnnotation_ = [[BaseAnnotation allocWithZone:[self zone]] init];
        
    }
    
    [userLocationAnnotation_ setCoordinate:coordinates];
    [mapView addAnnotation:userLocationAnnotation_];
    
    [self fitMapRegion];
    
}

#pragma mark -
#pragma mark POI management

/*
 * Subclasses must implement this selector to respond when the POI list was downloaded correctly from the server.
 * Default implementation displays all downloaded POIs into the map
 */
- (void)poiListDownloaded {
    
    MKMapView *mapView = [self mapView];

    if ([annotationsArray_ count] > 0) {
        
        [mapView removeAnnotations:annotationsArray_];

    }
    
    [annotationsArray_ removeAllObjects];
    
    POIAnnotation *poiAnnotation = nil;
    
    for (ATMData *poi in poiList_) {
        
        poiAnnotation = [[[POIAnnotation alloc] initWithAssociatedPOI:poi] autorelease];
        
        if (poiAnnotation != nil) {
            
            [annotationsArray_ addObject:poiAnnotation];
            
        }
        
    }
    
    [mapView addAnnotations:annotationsArray_];
    
}

/*
 * Subclasses must implement this selector to responde when the POI list download finished in error. Default implementation
 * does nothing
 */
- (void)poiListDownloadFinishedWithError {
    
}

/*
 * Subclasses must implement this selctor to displays the provided POI information in another view controller.
 * Default implementation does nothing
 */
- (void)displayPOI:(ATMData *)aPOI {
    
}

#pragma mark -
#pragma mark Graphic interface

/*
 * Checks the steps navigator segmented selector enabled state
 */
- (void)checkStepNavigatorHeaderSelectorEnabledState {
    
    if (displayRouteSteps_) {
        
        if (stepIndex_ == 0) {
            
            [stepsNavigationSegmentedControl_ setEnabled:NO
                                       forSegmentAtIndex:0];
            
        } else {
            
            [stepsNavigationSegmentedControl_ setEnabled:YES
                                       forSegmentAtIndex:0];
            
        }
        
        if ((stepIndex_ + 1) >= routeStepsCount_) {
            
            [stepsNavigationSegmentedControl_ setEnabled:NO
                                       forSegmentAtIndex:1];
            
        } else {
            
            [stepsNavigationSegmentedControl_ setEnabled:YES
                                       forSegmentAtIndex:1];
            
        }
        
    }
    
}

/*
 * Child subclasses must return the route time and distance view superview. The route time and distance view is located
 * at the top region of its superview. Default implementation returns nil
 */
- (UIView *)routeTimeAndDistanceViewSuperview {
    
    return nil;
    
}

/*
 * Child subclasses must return the route step view superview. The route step view is located at the top region of its superview.
 * Default implementation returns nil
 */
- (UIView *)routeStepViewSuperview {
    
    return nil;
    
}

/*
 * Child subclasses must implement this method to perform any operation when the route step view is going to appear. It can
 * be either animated or not. Default implementation does nothing
 */
- (void)routeStepViewWillAppear {
    
}

/*
 * Child subclasses must implement this method to perform any operation when the route step view is going to disappear. It can
 * be either animated or not. Default implementation does nothing
 */
- (void)routeStepViewWillDisppear {
    
}

/*
 * Child subclasses notifies the parent class whether the annotation callout button is located to the right or not. Default
 * implementation locates the annotation callout button to the right
 */
- (BOOL)annotationCalloutButtonLocatedRight {
    
    return YES;
    
}

/*
 * Toggles the display route steps flag and updates the interface
 */
- (BOOL)toggleDisplayRouteSteps {
    
    BOOL result = NO;
    
    if (currentRoute_ != nil) {
        
        displayRouteSteps_ = !displayRouteSteps_;
        
        if (displayRouteSteps_) {
            
            [self startDisplayingSteps:YES];
            result = YES;
            
        } else {
            
            [self stopDisplayingSteps:YES];
            
        }
        
    } else {
        
        displayRouteSteps_ = NO;
        
        [self stopDisplayingSteps:YES];
        
    }
    
    return result;
    
}

/*
 * Clears the map locations annotations and releases them
 */
- (void)clearMapLocationsAnnotations {
    
    MKMapView *mapView = [self mapView];
    
    if (originAnnotation_ != nil) {
        
        [mapView removeAnnotation:originAnnotation_];
        [originAnnotation_ release];
        originAnnotation_ = nil;
        
    }
    
    if (destinationAnnotation_ != nil) {
        
        [mapView removeAnnotation:destinationAnnotation_];
        [destinationAnnotation_ release];
        destinationAnnotation_ = nil;
        
    }
    
    if (userLocationAnnotation_ != nil) {
        
        [mapView removeAnnotation:userLocationAnnotation_];
        [userLocationAnnotation_ release];
        userLocationAnnotation_ = nil;
        
    }
    
}

#pragma mark -
#pragma mark Map display

/*
 * Calculates the map region to fit all the annotations and the route
 */
- (void)fitMapRegion {
    
    CLLocationDegrees north = -90.0f;
    CLLocationDegrees south = 90.0f;
    CLLocationDegrees east = -180.0f;
    CLLocationDegrees west = 180.0f;
    
    CLLocationCoordinate2D coordinate;
    
    for (BaseAnnotation *annotation in annotationsArray_) {
        
        coordinate = annotation.coordinate;
        
        if (north < coordinate.latitude) {
            
            north = coordinate.latitude;
            
        }
        
        if (south > coordinate.latitude) {
            
            south = coordinate.latitude;
            
        }
        
        if (east < coordinate.longitude) {
            
            east = coordinate.longitude;
            
        }
        
        if (west > coordinate.longitude) {
            
            west = coordinate.longitude;
            
        }
        
    }
    
    if (originAnnotation_ != nil) {
        
        coordinate = originAnnotation_.coordinate;
        
        if (north < coordinate.latitude) {
            
            north = coordinate.latitude;
            
        }
        
        if (south > coordinate.latitude) {
            
            south = coordinate.latitude;
            
        }
        
        if (east < coordinate.longitude) {
            
            east = coordinate.longitude;
            
        }
        
        if (west > coordinate.longitude) {
            
            west = coordinate.longitude;
            
        }
        
    }

    if (destinationAnnotation_ != nil) {
        
        coordinate = destinationAnnotation_.coordinate;
        
        if (north < coordinate.latitude) {
            
            north = coordinate.latitude;
            
        }
        
        if (south > coordinate.latitude) {
            
            south = coordinate.latitude;
            
        }
        
        if (east < coordinate.longitude) {
            
            east = coordinate.longitude;
            
        }
        
        if (west > coordinate.longitude) {
            
            west = coordinate.longitude;
            
        }
        
    }
    
    if (userLocationAnnotation_ != nil) {
        
        coordinate = userLocationAnnotation_.coordinate;
        
        if (north < coordinate.latitude) {
            
            north = coordinate.latitude;
            
        }
        
        if (south > coordinate.latitude) {
            
            south = coordinate.latitude;
            
        }
        
        if (east < coordinate.longitude) {
            
            east = coordinate.longitude;
            
        }
        
        if (west > coordinate.longitude) {
            
            west = coordinate.longitude;
            
        }
        
    }
    
    if (self.routeInformation != nil) {
        
        CLLocationCoordinate2D northEast = self.northEastPoint;
        CLLocationCoordinate2D southWest = self.southWestPoint;
        
        if (north < northEast.latitude) {
            
            north = northEast.latitude;
            
        }
        
        if (east < northEast.longitude) {
            
            east = northEast.longitude;
            
        }
        
        if (south > southWest.latitude) {
            
            south = southWest.latitude;
            
        }
        
        if (west > southWest.longitude) {
            
            west = southWest.longitude;
            
        }
        
    }
    
    if ((north >= south) && (east >= west)) {
        
        MKCoordinateRegion coordinateRegion;
        coordinateRegion.center.latitude = (north + south) / 2.0f;
        coordinateRegion.center.longitude = (east + west) / 2.0f;
        
        coordinateRegion.span.latitudeDelta = fabs(north - south) * 1.3f;
        coordinateRegion.span.longitudeDelta = fabs(east - west) * 1.3f;
        
        if (coordinateRegion.span.latitudeDelta < 0.001f) {
            
            coordinateRegion.span.latitudeDelta = 0.001f;
            
        }
        
        if (coordinateRegion.span.longitudeDelta < 0.001f) {
            
            coordinateRegion.span.latitudeDelta = 0.001f;
            
        }
        
        MKMapView *mapView = [self mapView];
        if(coordinateRegion.center.latitude <= 90 && coordinateRegion.center.latitude >= -90 &&
           coordinateRegion.center.longitude >= -180 && coordinateRegion.center.longitude <= 180){
        [mapView setRegion:[mapView regionThatFits:coordinateRegion]
                  animated:YES];
        }
    }
    
}

#pragma mark -
#pragma mark Steps management

/*
 * Displays the current route step information
 */
- (void)displayCurrentRouteStep:(BOOL)animated {
    
    [routeStepsView_ setStepIndex:stepIndex_ + 1];
    
    GoogleMapsLocation *location;
    NSString *stepString = nil;
    
    if (stepIndex_ == 0) {
        
        GoogleMapsStep *step = [self stepAtPosition:stepIndex_];
        location = [step startLocation];
        stepString = [[step instructions] string];
        
    } else if ((stepIndex_ + 1) == routeStepsCount_) {
        
        GoogleMapsStep *step = [self stepAtPosition:(stepIndex_ - 1)];
        NSArray *legs = currentRoute_.legs;
        GoogleMapsLeg *leg = [legs lastObject];
        location = [step endLocation];
        stepString = [NSString stringWithFormat:@"%@ %@", [NSString stringWithFormat:NSLocalizedString(ADVANCE_STEP_TEXT_KEY, nil), [[step distance] string]],
                      [NSString stringWithFormat:NSLocalizedString(ARRIVAL_STEP_TEXT_KEY, nil), [leg endAddress]]];
        
    } else {
        
        GoogleMapsStep *currentStep = [self stepAtPosition:stepIndex_];
        GoogleMapsStep *previousStep = [self stepAtPosition:(stepIndex_ - 1)];
        location = currentStep.startLocation;
        stepString = [NSString stringWithFormat:@"%@ %@", [NSString stringWithFormat:NSLocalizedString(ADVANCE_STEP_TEXT_KEY, nil), [[previousStep distance] string]],
                      [[currentStep instructions] string]];
        
    }
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = location.latitude;
    coordinate.longitude = location.longitude;
    
    [routeStepsView_ setStepText:stepString
                        animated:YES];
        
    [self arrangeInfoViewsToiPadScreen];
    
    [self updateStepAnnotationToCoordinate:coordinate];
    
}

/*
 * Starts displaying the steps information
 */
- (void)startDisplayingSteps:(BOOL)animated {
    
    if ((currentRoute_ != nil)) {
        
        routeStepsCount_ = 0;
        
        NSArray *steps = nil;
        NSArray *legs = [currentRoute_ legs];
        NSUInteger stepsCount;
        GoogleMapsStep *firstStep = nil;
        
        for (GoogleMapsLeg *leg in legs) {
            
            steps = [leg steps];
            stepsCount = [steps count];
            
            if (stepsCount > 0) {
                
                routeStepsCount_ += stepsCount;
                
                if (firstStep == nil) {
                    
                    firstStep = [steps objectAtIndex:0];
                    
                }
                
            }
            
        }
        
        CLLocationCoordinate2D centerCoordinate;
        centerCoordinate.latitude = [firstStep startLocation].latitude;
        centerCoordinate.longitude = [firstStep startLocation].longitude;
        stepAnnotation_.coordinate = centerCoordinate;
        [[self mapView] addAnnotation:stepAnnotation_];
        
        if (routeStepsCount_ > 0) {
            
            routeStepsCount_++;
            
        }
        
        [routeStepsView_ setRouteStepsCount:routeStepsCount_];
        stepIndex_ = 0;
        [self displayCurrentRouteStep:NO];
        
        [self checkStepNavigatorHeaderSelectorEnabledState];
        
        CGRect routeStepsViewFrame = [routeStepsView_ frame];
        
        routeStepsViewFrame.origin.y = mapViewHeight_ - CGRectGetHeight(routeStepsViewFrame);
        [routeStepsView_ setFrame:routeStepsViewFrame];
        
        CGRect mapFrame = [[self mapView] frame];
        mapFrame.size.height = mapViewHeight_ - CGRectGetHeight(routeStepsViewFrame);
        
        CGRect wButtonFrame = [[self warningsButton] frame];
        wButtonFrame.origin.y = CGRectGetMaxY([[self mapView] frame]) - 30.0f;
        
        if ([routeStepsView_ superview] == nil) {
            
            [[self routeStepViewSuperview] addSubview:routeStepsView_];
            
        }
        
        if (animated) {
            
            if ([[UIView class] respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
                
                [UIView animateWithDuration:STEPS_VIEW_ANIMATION_DURATION
                                      delay:0.0f
                                    options:UIViewAnimationOptionCurveEaseInOut
                                 animations:^{
                                     
                                     routeStepsView_.frame = routeStepsViewFrame;
                                     [[self mapView] setFrame:mapFrame];
                                     [[self warningsButton] setFrame:wButtonFrame];
                                     routeStepsView_.alpha = 1.0f;
                                     timeAndDistanceHeaderView_.alpha = 1.0f;
                                     [self routeStepViewWillAppear];
                                     
                                 }
                                 completion:nil];
                
            } else {
                
                [UIView beginAnimations:nil
                                context:nil];
                [UIView setAnimationDuration:STEPS_VIEW_ANIMATION_DURATION];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                
                routeStepsView_.frame = routeStepsViewFrame;
                [[self mapView] setFrame:mapFrame];
                [[self warningsButton] setFrame:wButtonFrame];
                routeStepsView_.alpha = 1.0f;
                timeAndDistanceHeaderView_.alpha = 1.0f;
                [self routeStepViewWillAppear];
                
                [UIView commitAnimations];
                
            }
            
        } else {
            
            routeStepsView_.frame = routeStepsViewFrame;
            [[self mapView] setFrame:mapFrame];
            [[self warningsButton] setFrame:wButtonFrame];
            [self routeStepViewWillAppear];
            
        }
        
    }
    
}

/*
 * Stops displaying the steps information
 */
- (void)stopDisplayingSteps:(BOOL)animated {
    
    if (stepAnnotation_ != nil) {
        
        [[self mapView] removeAnnotation:stepAnnotation_];
        
    }
    
}

/*
 * Calculates the step at the given position
 */
- (GoogleMapsStep *)stepAtPosition:(NSUInteger)aPosition {
    
    GoogleMapsStep *result = nil;
    
    NSUInteger legPosition = aPosition;
    
    NSArray *legs = currentRoute_.legs;
    NSUInteger legsCount = [legs count];
    GoogleMapsLeg *leg = nil;
    NSArray *steps = nil;
    NSUInteger stepsCount = 0;
    
    for (NSUInteger i = 0; i < legsCount; i ++) {
        
        leg = [legs objectAtIndex:i];
        steps = leg.steps;
        stepsCount = [steps count];
        
        if (legPosition < stepsCount) {
            
            result = [steps objectAtIndex:legPosition];
            break;
            
        } else {
            
            legPosition -= stepsCount;
            
        }
        
    }
    
    return result;
    
}

/*
 * Updates the step annotation to a provided coordinate
 */
- (void)updateStepAnnotationToCoordinate:(CLLocationCoordinate2D)aNewCoordinate {
    
    CLLocationCoordinate2D oldCoordinate = stepAnnotation_.coordinate;
    CLLocationDegrees oldLatitude = oldCoordinate.latitude;
    CLLocationDegrees oldLongitude = oldCoordinate.longitude;
    CLLocationDegrees newLatitude = aNewCoordinate.latitude;
    CLLocationDegrees newLongitude = aNewCoordinate.longitude;
    CLLocationDegrees northMost = (oldLatitude > newLatitude) ? oldLatitude : newLatitude;
    CLLocationDegrees southMost = (oldLatitude < newLatitude) ? oldLatitude : newLatitude;
    CLLocationDegrees westMost = (oldLongitude < newLongitude) ? oldLongitude : newLongitude;
    CLLocationDegrees eastMost = (oldLongitude > newLongitude) ? oldLongitude : newLongitude;
    
    CLLocationCoordinate2D center;
    center.latitude = (northMost + southMost) / 2.0f;
    center.longitude = (eastMost + westMost) / 2.0f;
    
    MKCoordinateSpan coordinateSpan;
    coordinateSpan.latitudeDelta = fabs(northMost - southMost) * 1.3f;
    coordinateSpan.longitudeDelta = fabs(eastMost - westMost) * 1.3f;
    
    if (coordinateSpan.latitudeDelta < 0.001f) {
        
        coordinateSpan.latitudeDelta = 0.001f;
        
    }
    
    if (coordinateSpan.longitudeDelta < 0.001f) {
        
        coordinateSpan.longitudeDelta = 0.001f;
        
    }
    
    MKCoordinateRegion coordinateRegion;
    coordinateRegion.center = center;
    coordinateRegion.span = coordinateSpan;
    
    
    if(coordinateRegion.center.latitude <= 90 && coordinateRegion.center.latitude >= -90 &&
       coordinateRegion.center.longitude >= -180 && coordinateRegion.center.longitude <= 180){
    MKMapView *mapView = [self mapView];
    MKCoordinateRegion regionThatFits = [mapView regionThatFits:coordinateRegion];
    [mapView setRegion:regionThatFits
              animated:YES];}
    stepAnnotation_.coordinate = aNewCoordinate;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the route steps view
 *
 * @return The route steps view
 */
- (RouteStepView *)routeStepsView {
    
    return routeStepsView_;
    
}

/*
 * Returns the time and distance header view
 *
 * @return The time and distance header view
 */
- (TimeAndDistanceHeaderView *)timeAndDistanceHeaderView {
    
    return timeAndDistanceHeaderView_;
    
}

/*
 * Returns the displaying route steps view controller flag
 *
 * @return The displaying route steps view controller flag
 */
- (BOOL)displayingStepsView {
    
    CGRect routeStepsViewFrame = [routeStepsView_ frame];
    CGFloat viewBottom = CGRectGetMaxY(routeStepsViewFrame);
    
    return (!(viewBottom <= 0));
    
}

/*
 * Returns the POI list
 *
 * @return The POI list
 */
- (NSArray *)poiList {
    
    return [NSArray arrayWithArray:poiList_];
    
}

/*
 * Sets the new POI list. Only LocalGeneralPOI instances in the array are stored in the new array
 *
 * @param aPOIList the new POI list to set
 */
- (void)setPoiList:(NSArray *)aPOIList {
    
    [poiList_ removeAllObjects];
    
    NSObject *object = nil;
    
    for (object in aPOIList) {
        
        if ([object isKindOfClass:[ATMData class]]) {
            
            [poiList_ addObject:object];
            
        }
        
    }
    
}

/*
 * Returns the current route
 *
 * @return The current route
 */
- (GoogleMapsRoute *)currentRoute {
    
    return currentRoute_;
    
}

/*
 * Returns the annotations array
 *
 * @return The annotations array
 */
- (NSArray *)annotationsArray {
    
    return [NSArray arrayWithArray:annotationsArray_];
    
}

/*
 * Sets the new annotations array. Only POIAnnotation instances in the array are stored in the new array
 *
 * @param anAnnotationsArray The new annotations array to set
 */
- (void)setAnnotationsArray:(NSArray *)anAnnotationsArray {
    
    MKMapView *mapView = [self mapView];
    [mapView removeAnnotations:annotationsArray_];
    [annotationsArray_ removeAllObjects];
    
    NSObject *object = nil;
    
    for (object in anAnnotationsArray) {
        
        if ([object isKindOfClass:[POIAnnotation class]]) {
            
            [annotationsArray_ addObject:object];
            
        }
        
    }
    
    [mapView addAnnotations:annotationsArray_];
    
    [self startDisplayingSteps:YES];
    
    [self fitMapRegion];

}

/*
 * Returns the show POI callout button flag
 *
 * @return The show POI callout button flag
 */
- (BOOL)showPOICalloutButton {
    
    return showPOICalloutButton_;
    
}

/*
 * Sets the new show POI callout button flag
 *
 * @param showPOICalloutButton The new show POI callout button flag to set
 */
- (void)setShowPOICalloutButton:(BOOL)showPOICalloutButton {
    
    showPOICalloutButton_ = showPOICalloutButton;
    
}

#pragma mark -
#pragma mark iPad aux view methods

/*
 * Arranges steps and information views to iPad screen
 */
- (void)arrangeInfoViewsToiPadScreen {
    
    UIView *superView = [self routeTimeAndDistanceViewSuperview];
    
    if (superView != nil) {
        
        // Relocates steps view
        
        [routeStepsView_ setBackgroundImageView:nil];
        [routeStepsView_ setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:1.0f]];
        [routeStepsView_ setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth];
        
        CGRect routeStepsViewFrame = [routeStepsView_ frame];
        routeStepsViewFrame.origin.x = 0.0f;
        routeStepsViewFrame.origin.y = mapViewHeight_ - CGRectGetHeight(routeStepsViewFrame);
        routeStepsViewFrame.size.width = CGRectGetWidth([superView frame]);
        [routeStepsView_ setFrame:routeStepsViewFrame];
        
        CGRect mapFrame = [[self mapView] frame];
        mapFrame.size.height = mapViewHeight_ - CGRectGetHeight(routeStepsViewFrame);
        
        CGRect wButtonFrame = [[self warningsButton] frame];
        wButtonFrame.origin.y = CGRectGetMaxY(mapFrame) - 30.0f;
        
        if ([[UIView class] respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
            
            [UIView animateWithDuration:0.2f
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 
                                 [[self mapView] setFrame:mapFrame];
                                 [[self warningsButton] setFrame:wButtonFrame];
                                 
                             }
                             completion:nil];
            
        } else {
            
            [UIView beginAnimations:nil
                            context:nil];
            [UIView setAnimationDuration:0.2f];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            
            [[self mapView] setFrame:mapFrame];
            [[self warningsButton] setFrame:wButtonFrame];
            
            [UIView commitAnimations];
            
        }
        
        // Relocates information view
        
        [timeAndDistanceHeaderView_ setBackgroundImageView:nil];
        [timeAndDistanceHeaderView_ setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.75f]];
        [timeAndDistanceHeaderView_ setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth];
        
        routeStepsViewFrame = [timeAndDistanceHeaderView_ frame];
        routeStepsViewFrame.origin.x = 0.0f;
        routeStepsViewFrame.origin.y = 0.0f;
        routeStepsViewFrame.size.width = CGRectGetWidth([superView frame]);
        [timeAndDistanceHeaderView_ setFrame:routeStepsViewFrame];

    }
}

@end
