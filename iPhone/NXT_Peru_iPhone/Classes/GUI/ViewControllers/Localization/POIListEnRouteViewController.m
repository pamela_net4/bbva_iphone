/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "POIListEnRouteViewController.h"
#import "POIListEnRouteViewController+protected.h"
#import "MapRouteViewController+protected.h"
//#import "Constants.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "StringKeys.h"
#import "Tools.h"
#import "ATMList.h"
#import "BranchesList.h"
#import "ExecutionResult.h"
#import "GeocodedAddressList.h"
#import "GoogleMapsDirectionsResponse.h"
#import "GoogleMapsDistance.h"
#import "GoogleMapsDuration.h"
#import "GoogleMapsLeg.h"
#import "GoogleMapsLocation.h"
#import "GoogleMapsPolyline.h"
#import "GoogleMapsRoute.h"
#import "GoogleMapsStep.h"
#import "ATMAnnotationView.h"
#import "BaseAnnotation.h"
#import "BranchAnnotationView.h"
#import "DestinationAnnotation.h"
#import "OriginAnnotation.h"
#import "RouteDestinationAnnotationView.h"
#import "RouteOriginAnnotationView.h"
#import "POIAnnotation.h"
#import "StepAnnotationView.h"
#import "UserAnnotationView.h"
#import "RouteStepView.h"
#import "TimeAndDistanceHeaderView.h"
#import "XMLParserDataDownloadClient+protected.h"
#import "Location.h"
#import "Route.h"
#import "NXT_Peru_iPhone_AppDelegate.h"

/**
 * Defines the callout button width
 */
#define CALLOUT_BUTTON_WIDTH                                                        30.0f

/**
 * Defines the callout button height
 */
#define CALLOUT_BUTTON_HEIGHT                                                       30.0f


/**
 * Defines the route time and distance view animation duration, measured in seconds
 */
#define TIME_AND_DISTANCE_VIEW_ANIMATION_DURATION                                   0.5f


#pragma mark -

/**
 * POIListEnRouteViewController private extension
 */
@interface POIListEnRouteViewController()

/**
 * Releases the graphic elements not needed when view is hidden
 *
 * @private
 */
- (void)releasePOIListEnRouteViewControllerGraphicElements;

/**
 * Displays the next route step
 *
 * @private
 */
- (void)displayNextRouteStep;

/**
 * Displays the previous route step
 *
 * @private
 */
- (void)displayPreviousRouteStep;

/**
 * Route steps navigator segmented control selected index changed. Moves to the next or previous step
 *
 * @param aControl The segmented control triggering the event
 * @private
 */
- (void)stepsNavigatorSegmentedControlSelectedIndexChanged:(UIControl *)aControl;

/**
 * Travel mode toggle segmented control selected index changed. Downloads a new route for the selected travel mode
 *
 * @param aControl The segmented control triggering the event
 * @private
 */
- (void)travelModeSegmentedControlSelectedIndexChanged:(UIControl *)aControl;

/**
 * Downloads the route using the stored information
 *
 * @param byCarRoute YES to obtain the route by car, NO to obtain the route on foot
 * @private
 */
- (void)downloadRouteByCar:(BOOL)byCarRoute;

@end


#pragma mark -

@implementation POIListEnRouteViewController

#pragma mark -
#pragma mark Properties

@synthesize displayRouteSteps = displayRouteSteps_;
@synthesize originLocation = originLocation_;
@synthesize destinationLocation = destinationLocation_;
@synthesize brandImageView = brandImageView_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self releasePOIListEnRouteViewControllerGraphicElements];
    
    [poiList_ release];
    poiList_ = nil;
    
    [currentRoute_ release];
    currentRoute_ = nil;
    
    [annotationsArray_ release];
    annotationsArray_ = nil;
    
    [originLocation_ release];
    originLocation_ = nil;
    
    [destinationLocation_ release];
    destinationLocation_ = nil;
    
    [originAnnotation_ release];
    originAnnotation_ = nil;
    
    [destinationLocation_ release];
    destinationLocation_ = nil;
    
    [stepAnnotation_ release];
    stepAnnotation_ = nil;
    
    [userLocationAnnotation_ release];
    userLocationAnnotation_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory. Visual elements are released
 */
- (void)viewDidUnload {
    
    [self releasePOIListEnRouteViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasePOIListEnRouteViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases the graphic elements not needed when view is hidden
 */
- (void)releasePOIListEnRouteViewControllerGraphicElements {
    
    [routeStepsView_ release];
    routeStepsView_ = nil;
    
    [timeAndDistanceHeaderView_ release];
    timeAndDistanceHeaderView_ = nil;
    
    [stepsNavigationSegmentedControl_ release];
    stepsNavigationSegmentedControl_ = nil;
    
    [travelModeSegmentedControl_ release];
    travelModeSegmentedControl_ = nil;
    
    [annotationCalloutButton_ release];
    annotationCalloutButton_ = nil;
    
    [brandImageView_ release];
    brandImageView_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. The POI arrays are constructed
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    poiList_ = [[NSMutableArray alloc] init];
    
    annotationsArray_ = [[NSMutableArray alloc] init];
    
    showPOICalloutButton_ = YES;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory. Style is applied to graphic elements
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [brandImageView_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    mapViewHeight_ = CGRectGetHeight([[self mapView] frame]);
    
    if (routeStepsView_ == nil) {
        
        routeStepsView_ = [[RouteStepView routeStepView] retain];
        
        [routeStepsView_ setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
        
    }
    
    [stepsNavigationSegmentedControl_ release];
    stepsNavigationSegmentedControl_ = nil;
    stepsNavigationSegmentedControl_ = [[routeStepsView_ stepsSegmentedControl] retain];
    [stepsNavigationSegmentedControl_ addTarget:self
                                         action:@selector(stepsNavigatorSegmentedControlSelectedIndexChanged:)
                               forControlEvents:UIControlEventValueChanged];
    [stepsNavigationSegmentedControl_ setSelectedSegmentIndex:-1];
    
    if (timeAndDistanceHeaderView_ == nil) {
        
        timeAndDistanceHeaderView_ = [[TimeAndDistanceHeaderView timeAndDistanceHeaderView] retain];
        
        [timeAndDistanceHeaderView_ setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth];
        
    }
    
    [travelModeSegmentedControl_ release];
    travelModeSegmentedControl_ = nil;
    travelModeSegmentedControl_ = [[timeAndDistanceHeaderView_ routeTypeSelectorSegmentedControl] retain];
    [travelModeSegmentedControl_ addTarget:self
                                    action:@selector(travelModeSegmentedControlSelectedIndexChanged:)
                          forControlEvents:UIControlEventValueChanged];
    
    if (routeByCar_) {
        
        [travelModeSegmentedControl_ setSelectedSegmentIndex:1];
        
    } else {
        
        [travelModeSegmentedControl_ setSelectedSegmentIndex:0];
        
    }
    
    CGRect frame = [timeAndDistanceHeaderView_ frame];
    frame.origin.x = 0.0f;
    frame.origin.y = 0.0f;
    [timeAndDistanceHeaderView_ setFrame:frame];

    UIView *superView = [self routeTimeAndDistanceViewSuperview];   
    [superView addSubview:timeAndDistanceHeaderView_];
    [superView bringSubviewToFront:timeAndDistanceHeaderView_];
    [timeAndDistanceHeaderView_ setAlpha:0.0f];
    
}

/*
 * Notifies the view controller that its view is about to be become visible. The available information is checked to display it
 *
 * @param animated If YES, the view is being added to the window using an animation
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
        
    mapViewHeight_ = CGRectGetMinY([brandImageView_ frame]) - CGRectGetMinY([[self mapView] frame]);

    [self checkStepNavigatorHeaderSelectorEnabledState];
    
    if (displayRouteSteps_) {
        
        [self startDisplayingSteps:NO];
        
    } else {
        
        [self stopDisplayingSteps:NO];
        
    }
    
    MKMapView *map = self.mapView;
    
    if ((originAnnotation_ != nil) && (destinationAnnotation_ != nil)) {
        
        [map removeAnnotation:originAnnotation_];
        [map removeAnnotation:destinationAnnotation_];
        [map addAnnotation:originAnnotation_];
        [map addAnnotation:destinationAnnotation_];
        
    }
    
    if (userLocationAnnotation_ != nil) {
        
        [map removeAnnotation:userLocationAnnotation_];
        [map addAnnotation:userLocationAnnotation_];
        
    }
    
    [self arrangeInfoViewsToiPadScreen];
    
}

/**
 * Notifies the view controller that its view was added to a view hierarchy.
 *
 * @param animated: If YES, the view was added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
}

#pragma mark -
#pragma mark Setting the route information

/*
 * Sets the route origin and ;, and start the route download
 */
- (void)setRouteOrigin:(Location *)routeOrigin
      routeDestination:(Location *)routeDestination 
            byRouteCar:(BOOL)byCarRoute {
    
    routeByCar_ = byCarRoute;
    
    if (routeOrigin != originLocation_) {
        
        [originLocation_ release];
        originLocation_ = nil;
        originLocation_ = [routeOrigin retain];
        
    }
    
    if (routeDestination != destinationLocation_) {
        
        [destinationLocation_ release];
        destinationLocation_ = nil;
        destinationLocation_ = [routeDestination retain];
        
    }
    
    MKMapView *map = [self mapView];
    
    if (originAnnotation_ == nil) {
        
        originAnnotation_ = [[OriginAnnotation alloc] init];
        [originAnnotation_ setCoordinate:[originLocation_ geographicCoordinate]];
        [originAnnotation_ setTitle:NSLocalizedString(ORIGIN_LOCATION_TEXT_KEY, nil)];
        
    } else {
        
        [map removeAnnotation:originAnnotation_];
        [originAnnotation_ setCoordinate:[originLocation_ geographicCoordinate]];
        
    }
    
    if (destinationAnnotation_ == nil) {
        
        destinationAnnotation_ = [[DestinationAnnotation alloc] init];
        [destinationAnnotation_ setCoordinate:[destinationLocation_ geographicCoordinate]];
        [destinationAnnotation_ setTitle:NSLocalizedString(DESTINATION_LOCATION_TEXT_KEY, nil)];
        
    } else {
        
        [map removeAnnotation:destinationAnnotation_];
        [destinationAnnotation_ setCoordinate:[destinationLocation_ geographicCoordinate]];
        
    }
    
    if ((originLocation_ != nil) && (destinationLocation_ != nil) && (originAnnotation_ != nil) && (destinationAnnotation_ != nil)) {
        
        [map addAnnotation:originAnnotation_];
        [map addAnnotation:destinationAnnotation_];
        
    }
    
    displayRouteSteps_ = YES;
    
    stepIndex_ = 0;
    
    [self setPoiList:nil];
    [self setAnnotationsArray:nil];
    [self setRouteInformation:nil];
    
	[self downloadRouteByCar:routeByCar_];
	
	if (routeByCar_) {
		
		[travelModeSegmentedControl_ setSelectedSegmentIndex:1];
		
	} else {
		
		[travelModeSegmentedControl_ setSelectedSegmentIndex:0];
		
	}
    
}

#pragma mark -
#pragma mark User interaction

/*
 * Route steps navigator segmented control selected index changed. Moves to the next or previous step
 */
- (void)stepsNavigatorSegmentedControlSelectedIndexChanged:(UIControl *)aControl {
    
    if (aControl == stepsNavigationSegmentedControl_) {
        
        NSInteger selectedItem = [stepsNavigationSegmentedControl_ selectedSegmentIndex];
        [stepsNavigationSegmentedControl_ setSelectedSegmentIndex:-1];
        
        if (selectedItem == 0) {
            
            [self displayPreviousRouteStep];
            
        } else if (selectedItem == 1) {
            
            [self displayNextRouteStep];
            
        }
        
    }
    
}

/*
 * Travel mode toggle segmented control selected index changed. Downloads a new route for the selected travel mode
 */
- (void)travelModeSegmentedControlSelectedIndexChanged:(UIControl *)aControl {
    
    if (aControl == travelModeSegmentedControl_) {
        
        [self stopDisplayingSteps:YES];
        
        NSInteger selectedMode = [travelModeSegmentedControl_ selectedSegmentIndex];
        BOOL byCar = (selectedMode == 1);
        [self downloadRouteByCar:byCar];
        
    }
    
}

#pragma mark -
#pragma mark Route management

/*
 * Downloads the route using the stored information
 */
- (void)downloadRouteByCar:(BOOL)byCarRoute {
    
    Location *originLocation = self.originLocation;
    Location *destinationLocation = self.destinationLocation;
    
    routeByCar_ = byCarRoute;
    
    [self obtainRouteFromOriginLocation:originLocation
                  toDestinationLocation:destinationLocation
                                  byCar:byCarRoute];
    
}

#pragma mark -
#pragma mark Steps management

/*
 * Displays the next route step
 */
- (void)displayNextRouteStep {
    
    if ((stepIndex_ + 1) < routeStepsCount_) {
        
        stepIndex_++;
        [self displayCurrentRouteStep:YES];
        [self checkStepNavigatorHeaderSelectorEnabledState];
        
    }
    
}

/*
 * Displays the previous route step
 */
- (void)displayPreviousRouteStep {
    
    if (stepIndex_ > 0) {
        
        stepIndex_ --;
        [self displayCurrentRouteStep:YES];
        [self checkStepNavigatorHeaderSelectorEnabledState];
        
    }
    
}

#pragma mark -
#pragma mark MKMapViewDelegate methods

/**
 * Returns the view associated with the specified annotation object. When superclass does not return an annotation view
 * and the annotation object is a POI annotation, a POI annotation view is returned
 *
 * @param mapView The map view that requested the annotation view
 * @param annotation The object representing the annotation that is about to be displayed
 * @return The annotation view to display for the specified annotation or nil if you want to display a standard annotation view
 */
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    MKAnnotationView *result = [super mapView:mapView viewForAnnotation:annotation];
    
    if (result == nil) {
        
        NSObject *annotationObject = (NSObject *)annotation;
        static NSString *routeOriginViewIdentifier = @"routeOriginAnnotationView";
        static NSString *routeDestinationViewIdentifier = @"routeDestinationAnnotationView";
        static NSString *stepViewIdentifier = @"stepAnnotationView";
        static NSString *userLocationViewIdentifier = @"userLocationAnnotationView";
        static NSString *atmAnnotationViewIdentifier = @"atmAnnotationViewIdentifier";
        static NSString *branchAnnotationViewIdentifier = @"branchAnnotationViewIdentifier";
        
        if (annotationObject == originAnnotation_) {
            
            result = [mapView dequeueReusableAnnotationViewWithIdentifier:routeOriginViewIdentifier];
            
            if (result == nil) {
                
                result = [[[RouteOriginAnnotationView alloc] initWithAnnotation:annotation
                                                                reuseIdentifier:routeOriginViewIdentifier] autorelease];
                
            }
            
            [result setLeftCalloutAccessoryView:nil];
            [result setRightCalloutAccessoryView:nil];
            [result setCanShowCallout:YES];
            
        } else if (annotationObject == destinationAnnotation_) {
            
            result = [mapView dequeueReusableAnnotationViewWithIdentifier:routeDestinationViewIdentifier];
            
            if (result == nil) {
                
                result = [[[RouteDestinationAnnotationView alloc] initWithAnnotation:annotation
                                                                     reuseIdentifier:routeDestinationViewIdentifier] autorelease];
                
            }
            
            [result setLeftCalloutAccessoryView:nil];
            [result setRightCalloutAccessoryView:nil];
            [result setCanShowCallout:YES];
            
        } else if (annotationObject == stepAnnotation_) {
            
            StepAnnotationView *stepAnnotationView = (StepAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:stepViewIdentifier];
            
            if (stepAnnotationView == nil) {
                
                stepAnnotationView = [[[StepAnnotationView alloc] initWithAnnotation:annotation
                                                                     reuseIdentifier:stepViewIdentifier] autorelease];
                UIImage *image = [[ImagesCache getInstance] imageNamed:ROUTE_STEP_ANNOTATION_VIEW_IMAGE_FILE_NAME];
                stepAnnotationView.stepAnnotationImage = image;
                
            }
        
            result = stepAnnotationView;
            [result setCanShowCallout:NO];
            
        } else if (annotationObject == userLocationAnnotation_) {
            
            result = [mapView dequeueReusableAnnotationViewWithIdentifier:userLocationViewIdentifier];
            
            if (result == nil) {
                
                result = [[[UserAnnotationView alloc] initWithAnnotation:annotation
                                                         reuseIdentifier:userLocationViewIdentifier] autorelease];
                
            }
            
            [result setLeftCalloutAccessoryView:nil];
            [result setRightCalloutAccessoryView:nil];
            [result setCanShowCallout:NO];
            
        } else if ([annotationObject isKindOfClass:[POIAnnotation class]]) {
            
            POIAnnotation *poiAnnotation = (POIAnnotation *)annotationObject;
            ATMData *poi = poiAnnotation.poi;
            
            if ([poi isMemberOfClass:[BranchData class]]) {
                
                result = (BranchAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:branchAnnotationViewIdentifier];
                
                if (result == nil) {
                    
                    result = [[[BranchAnnotationView allocWithZone:[self zone]] initWithAnnotation:annotation
                                                                                 reuseIdentifier:branchAnnotationViewIdentifier] autorelease];
                    
                }
                
            } else if ([poi isMemberOfClass:[ATMData class]]) {
                
                ATMAnnotationView *res = (ATMAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:atmAnnotationViewIdentifier];
                
                if (result == nil) {
                    
                    res = [[[ATMAnnotationView allocWithZone:[self zone]] initWithAnnotation:annotation
                                                                           reuseIdentifier:atmAnnotationViewIdentifier] autorelease];
                    
                }
                
                [res setImageWithType:[poi category]];
                
                result = res;
                
            }
            
            if (([self showPOICalloutButton]) && ([result rightCalloutAccessoryView] == nil)) {
                
                [result setRightCalloutAccessoryView:[UIButton buttonWithType:UIButtonTypeDetailDisclosure]];
                
            }
            
            [result setCanShowCallout:YES];
            
        }
        
    }
    
    return result;
    
}

/**
 * Tells the delegate that the user tapped one of the annotation view’s accessory buttons. Hides the callout and displays the the POI information
 *
 * @param mapView The map view containing the specified annotation view
 * @param view The annotation view whose button was tapped
 * @param control The control that was tapped
 */
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
    id<MKAnnotation> annotationObject = view.annotation;
    [mapView deselectAnnotation:annotationObject
                       animated:YES];
    
    if ([(NSObject *)annotationObject isKindOfClass:[POIAnnotation class]]) {
        
        POIAnnotation *annotation = (POIAnnotation *)annotationObject;
        
        [self displayPOI:annotation.poi];
        
    }
    
}

#pragma mark -
#pragma mark MapRouteViewController selectors

/**
 * Sets the current route information and displays it on the map. The route information is analyzed
 *
 * @param aRouteInformation The new route information to display
 */
- (void)setRouteInformation:(GoogleMapsDirectionsResponse *)aRouteInformation {
    
    [super setRouteInformation:aRouteInformation];
    
    [currentRoute_ release];
    currentRoute_ = nil;
    
    GoogleMapsDirectionsResponse *directionsResponse = [self routeInformation];
    
    BOOL mustFitMapRegion = NO;
    
    if (directionsResponse != nil) {
        
        mustFitMapRegion = YES;
        
    }
    
    NSArray *routes = directionsResponse.routes;
    
    if ([routes count] > 0) {
        
        currentRoute_ = [[routes objectAtIndex:0] retain];
        
        if (stepAnnotation_ == nil) {
            
            stepAnnotation_ = [[BaseAnnotation alloc] init];
            
        }
        
        NSArray *legs = [currentRoute_ legs];
        
        if ([legs count] > 0) {
            
            GoogleMapsLeg *firstLeg = [legs objectAtIndex:0];
            GoogleMapsLeg *lastLeg = [legs lastObject];
            
            GoogleMapsLocation *location = firstLeg.startLocation;
            CLLocationCoordinate2D coordinate;
            coordinate.latitude = location.latitude;
            coordinate.longitude = location.longitude;
            originAnnotation_.coordinate = coordinate;
            
            location = [lastLeg endLocation];
            coordinate.latitude = location.latitude;
            coordinate.longitude = location.longitude;
            destinationAnnotation_.coordinate = coordinate;
            
            [timeAndDistanceHeaderView_ setDistanceText:[[firstLeg distance] string]];
            [timeAndDistanceHeaderView_ setTimeText:[[firstLeg duration] string]];
            
        }
        
        mustFitMapRegion = YES;
        
    }
    
    if (mustFitMapRegion) {
        
        [self fitMapRegion];
        
    }
    
}

@end
