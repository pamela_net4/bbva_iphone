/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PopoverBaseViewController.h"


//Forward declarations
@class GeocodedAddressList;
@class GeocodedAddressData;


/**
 * POIListMultipleAddressViewController delegate protocol. The delegate receives the view controller event
 * when user selects an address or cancels the selection
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol POIListMultipleAddressViewControllerDelegate

/**
 * It informs that the POIListMultipleAddressViewController view is wanted close
 */
- (void)canceledListMultipleAddress;

/**
 * It informs when the user selected a address
 *
 * @param geocodedAddressData Sent selected address
 */
- (void)selectedGeocodedAddress:(GeocodedAddressData *)geocodedAddressData;

@end


/**
 * View controller to display a multiple address when you search a location
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface POIListMultipleAddressViewController : PopoverBaseViewController <UITableViewDelegate, UITableViewDataSource> {
@private
    
    /**
     * Table to show data of similar address list
     */
    UITableView *similarTableView_;
    
    /**
     * Navigation top bar
     */
    UINavigationBar *topBar_;
    
    /**
     * Brand image view
     */
    UIImageView *brandImageView_;
    
    /**
     * Cancel button in UINavigationBar
     */
    UIBarButtonItem *cancelBarButtonItem_;
    
    /**
     * Address list similar to a given one
     */
    GeocodedAddressList *similarAddressesList_;
    
    /**
     * Delegates
     */
    id<POIListMultipleAddressViewControllerDelegate> delegate_;
}

/**
 * Provides read-write access to the table view to asign protocols in Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *similarTableView;

/**
 * Provides read-write access to the top bar to asign element in Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UINavigationBar *topBar;

/**
 * Provides read-write access to the brand image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandImageView;

/**
 * Provides read-write access to the delegate
 */
@property (nonatomic, readwrite, assign) id<POIListMultipleAddressViewControllerDelegate> delegate;

/**
 * Creates and return an autoreleased POIListMultipleAddressViewController contructed from a NIB file
 *
 * @return The autorreleased POIListMultipleAddressViewController contructed from a NIB file
 */
+ (POIListMultipleAddressViewController *)poiListMultipleAddressViewController;

/**
 * Sets GeocodedAddressList to show in ViewController
 *
 * @param geocodedAddressList List containing all similar address
 */
- (void)setGeocoderAddressList:(GeocodedAddressList *)geocodedAddressList;

@end
