/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "POIMapViewController.h"

@class POIListMultipleAddressViewController;

@interface POIMapViewController(protected)

/**
 * Shows addresses list on a popover anchored to search bar
 *
 * @param viewController Instance of POIListMultipleAddressViewController to be shown
 */
- (void)showInsidePopoverMultipleAddressesList:(POIListMultipleAddressViewController *)viewController;

/**
 * Dismisses the addresses list popover
 */
- (void)dismissPopoverOfMultipleAddressesList;

/**
 * Starts the ATMs and branches locate process
 */
- (void)startLocatingATMsAndBranches;

@end
