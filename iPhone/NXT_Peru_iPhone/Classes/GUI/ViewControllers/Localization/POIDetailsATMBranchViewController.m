/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "POIDetailsATMBranchViewController.h"
#import "StringKeys.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTNavigationItem.h"
#import "ATMList.h"
#import "BranchesList.h"
#import "Tools.h"
#import "GeoServerManager.h"
#import "GeocodedAddressList.h"
#import "GoogleMapsApiInvoker.h"
#import "GoogleMapsRoute.h"
#import "GoogleMapsLeg.h"
#import "GoogleMapsDirectionsDataDownloadClient.h"
#import "GoogleMapsDirectionsResponse.h"
#import "POILocationSession.h"
#import "PhoneCell.h"
#import "DirectionCell.h"
#import "RouteStepsViewController.h"
#import "RouteCell.h"
#import "ScheduleCell.h"
#import "Location.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"POIDetailsATMBranchViewController"

/**
 * Defines the font size of ATM or Branch name
 */
CGFloat const kOfficeNameFontSize = 14.0f;


#pragma mark -

/**
 * POIDetailsATMBranchViewControllers private extension
 */
@interface POIDetailsATMBranchViewController()

/**
 * Displays the information
 *
 * @private
 */
- (void)displaysInformation;

/**
 * Starts process to the obtains a route from an origin coordinate to a destination coordinate. There must be only
 * one active route process
 *
 * @param anOriginCoordinate The route origin coordinate
 * @param aDestinationCoordinate The route destination coordinate
 * @param byCarFlag YES when the route must be by car, NO when the route must be on foot
 * @param sensorFlag YES when origin or destination coordinates were obtained with the sensor device (such as GPS), NO otherwise
 * @return YES if obtain route process can be started, NO otherwise
 * @private
 */
- (BOOL)obtainRouteFromOrigin:(CLLocationCoordinate2D)anOriginCoordinate
                toDestination:(CLLocationCoordinate2D)aDestinationCoordinate
                        byCar:(BOOL)byCarFlag
           obtainedWithSensor:(BOOL)sensorFlag;

/**
 * Releases all graphic elements contained on this view controller
 *
 * @private
 */
- (void)releaseGraphicElements;

@end


#pragma mark -

@implementation POIDetailsATMBranchViewController

#pragma mark -
#pragma mark Properties

@synthesize brandImageView = brandImageView_;
@synthesize poiIconImageView = poiIconImageView_;
@synthesize streetLabel = streetLabel_;
@synthesize distanceLabel = distanceLabel_;
@synthesize detailsTableView = detailsTableView_;
@synthesize shouldClearInterfaceData = shouldClearInterfaceData_;

#pragma mrk -
#pragma mark Memory management

/**
 * Release the used memnory
 */
- (void)dealloc {
    
    [brandImageView_ release];
    brandImageView_ = nil;
    
    [poiIconImageView_ release];
    poiIconImageView_ = nil;
    
    [streetLabel_ release];
    streetLabel_ = nil;
    
    [distanceLabel_ release];
    distanceLabel_ = nil;
    
    [detailsTableView_ release];
    detailsTableView_ = nil;
    
    [routeFoodInformation_ release];
    routeFoodInformation_ = nil;
    
    [routeCarInformation_ release];
    routeCarInformation_ = nil;
    
    [routeStepsViewController_ release];
    routeStepsViewController_ = nil;
    
    [poiData_ release];
    poiData_ = nil;
    
    [destinationCoordinate_ release];
    destinationCoordinate_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller's view is released from memory. Visiual elements are released
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseGraphicElements];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases all graphic elements contained on this view controller
 */
- (void)releaseGraphicElements {
    
    [brandImageView_ release];
    brandImageView_ = nil;
    
    [poiIconImageView_ release];
    poiIconImageView_ = nil;
    
    [streetLabel_ release];
    streetLabel_ = nil;
    
    [distanceLabel_ release];
    distanceLabel_ = nil;
    
    [detailsTableView_ release];
    detailsTableView_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and return an autoreleased POIDetailsATMBranchViewController constructed from a NIB file
 */
+ (POIDetailsATMBranchViewController *)poiDetailsATMBranchViewController {
    
    return [[[POIDetailsATMBranchViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
}

#pragma mark -
#pragma mark View management

/**
 * Called after the controller's view is loaded into memory. Style is applied to graphic elements
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Stylized views.
    [NXT_Peru_iPhoneStyler styleLabel:streetLabel_
                         withFontSize:kOfficeNameFontSize
                                color:[UIColor BBVAGreyToneOneColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:distanceLabel_
                         withFontSize:kOfficeNameFontSize
                                color:[UIColor BBVAGreyToneOneColor]];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    [NXT_Peru_iPhoneStyler styleTableView:detailsTableView_];
    
    // Set branding image view
    [brandImageView_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    // The first time, the view should cleaned.
    shouldClearInterfaceData_ = YES;
	
	[detailsTableView_ setScrollEnabled:NO];
    
}

/**
 * Notifies the view contorller that its view about to be become visible. the available information is checked to perfomr an action
 *
 * @param animated if YES, the view is being added to the window using an animation
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // If should clear interface data, release DownloadClients and obtain a new route.
    if (shouldClearInterfaceData_) {
        
        shouldClearInterfaceData_ = NO;
		
		foodInfoDownloaded_ = NO;
		carInfoDownloaded_ = NO;
        
        [routeFoodDownloadClient_ release];
        routeFoodDownloadClient_ = nil;
        
        [routeFoodInformation_ release];
        routeFoodInformation_ = nil;
        
        [routeCarDownloadClient_ release];
        routeCarDownloadClient_ = nil;
        
        [routeCarInformation_ release];
        routeCarInformation_ = nil;
        
        [[self appDelegate] setTabBarVisibility:NO animated:YES];
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        
        [self obtainRouteFromOrigin:[[[POILocationSession getInstance] userLocation] geographicCoordinate] 
                      toDestination:[destinationCoordinate_ geographicCoordinate]
                              byCar:NO 
                 obtainedWithSensor:NO];
        
    }
    
    // Display information of POI
    [self displaysInformation];
    
}

#pragma mark -
#pragma mark Displaying information

/*
 * Displays the information
 */
- (void)displaysInformation {
    
    // Set text of street and distance label.
    [streetLabel_ setText:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:[[[poiData_ geocodedAddress] address] addressFirstLine]], [Tools notNilString:[[[poiData_ geocodedAddress] address] addressSecondLine]]]];
    
    [distanceLabel_ setText:[Tools distanceAsString:[[[poiData_ geocodedAddress] geoFeatures] addressDistance]]];
    
    // Set image by POI.
    UIImage *image = nil;
    
    if ([poiData_ isKindOfClass:[BranchData class]]) {
        
        image = [[ImagesCache getInstance] imageNamed:MAP_FILTER_BBVA_CONTINENTAL_BRACH_IMAGE_FILE];
        
    } else {
        
        switch ([poiData_ category]) {
                
            case atmType:
                
                image = [[ImagesCache getInstance] imageNamed:MAP_FILTER_BBVA_CONTINENTAL_IMAGE_FILE];
                
                break;
                
            case atmBCType:
                
                image = [[ImagesCache getInstance] imageNamed:MAP_FILTER_BCP_IMAGE_FILE];
                
                break;
                
            case atmInterbankType:
                
                image = [[ImagesCache getInstance] imageNamed:MAP_FILTER_INTERBANK_GLOBALNET_IMAGE_FILE];
                
                break;
                
            case atmScotiabankType:
                
                image = [[ImagesCache getInstance] imageNamed:MAP_FILTER_SCOTIABANK_IMAGE_FILE];
                
                break;
                
            case expressAgentType:
                
                image = [[ImagesCache getInstance] imageNamed:MAP_FILTER_EXPRESS_AGENT_IMAGE_FILE];
                
                break;
                
            case expressAgentPlusType:
                
                image = [[ImagesCache getInstance] imageNamed:MAP_FILTER_EXPRESS_AGENT_PLUS_IMAGE_FILE];
                
                break;
                
            case kasnetType:
                
                image = [[ImagesCache getInstance] imageNamed:MAP_FILTER_KASNET_IMAGE_FILE];
                
                break;
                
            case multifacilType:
                
                image = [[ImagesCache getInstance] imageNamed:MAP_FILTER_MULTIFACIL_IMAGE_FILE];
                
                break;
                
            default:
                
                break;
                
        }
        
    }
    
    [poiIconImageView_ setImage:image];
	
	[detailsTableView_ reloadData];
    
}

#pragma mark -
#pragma mark Obtain route process management

/*
 * Starts process to the obtains a route from an origin coordinate to a destination coordinate. There must be only
 * one active route process
 */
- (BOOL)obtainRouteFromOrigin:(CLLocationCoordinate2D)anOriginCoordinate
                toDestination:(CLLocationCoordinate2D)aDestinationCoordinate
                        byCar:(BOOL)byCarFlag
           obtainedWithSensor:(BOOL)sensorFlag {
    
    BOOL result = NO;
    
    if (routeFoodDownloadClient_ == nil) {
        
        NSLocale *locale = [NSLocale currentLocale];
        NSNumber *metricSystem = [locale objectForKey:NSLocaleUsesMetricSystem];
        NSString *languageCode = NSLocalizedString(GOOGLE_MAPS_DIRECTIONS_API_LANGUAGE_CODE_KEY, nil);
        
        routeFoodDownloadClient_ = [[MapRouteDirectionsDataDownloadClient alloc] initWithOriginPoint:anOriginCoordinate
                                                                                    destinationPoint:aDestinationCoordinate
                                                                                            language:languageCode
                                                                                          routeByCar:byCarFlag
                                                                                        metricSystem:[metricSystem boolValue]
                                                        mapRouteDirectionsDataDownloadClientDelegate:self];
        
        result = [GoogleMapsApiInvoker routeForDirectionClient:routeFoodDownloadClient_
                                            obtainedFromSensor:sensorFlag];
        
        if (!result) {
            
            [routeFoodDownloadClient_ cancelDownload];
            [routeFoodDownloadClient_ release];
            routeFoodDownloadClient_ = nil;
            
            [Tools showErrorWithMessage:NSLocalizedString(UNABLE_TO_OBTAIN_ROUTE_ERROR_KEY, nil)];
            
        } else {
            
            [[self appDelegate] showActivityIndicator:poai_Both];
            
        }
        
    } else if (routeCarDownloadClient_ == nil) {
        
        NSLocale *locale = [NSLocale currentLocale];
        NSNumber *metricSystem = [locale objectForKey:NSLocaleUsesMetricSystem];
        NSString *languageCode = NSLocalizedString(GOOGLE_MAPS_DIRECTIONS_API_LANGUAGE_CODE_KEY, nil);
        
        routeCarDownloadClient_ = [[MapRouteDirectionsDataDownloadClient alloc] initWithOriginPoint:anOriginCoordinate
                                                                                   destinationPoint:aDestinationCoordinate
                                                                                           language:languageCode
                                                                                         routeByCar:byCarFlag
                                                                                       metricSystem:[metricSystem boolValue]
                                                       mapRouteDirectionsDataDownloadClientDelegate:self];
        
        result = [GoogleMapsApiInvoker routeForDirectionClient:routeCarDownloadClient_
                                            obtainedFromSensor:sensorFlag];
        
        if (!result) {
            
            [routeCarDownloadClient_ cancelDownload];
            [routeCarDownloadClient_ release];
            routeCarDownloadClient_ = nil;
            
            [Tools showErrorWithMessage:NSLocalizedString(UNABLE_TO_OBTAIN_ROUTE_ERROR_KEY, nil)];
            
        } else {
            
            [[self appDelegate] showActivityIndicator:poai_Both];
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark UITableViewDataSource protocol selectors

/**
 * Tells the data source to return the number of rows in a given section of a table view. Returns the number of cells, which depends on
 * whether the information displayed is an ATM or a branch
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section in tableView
 * @return The number of information cells to display
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
	// Check if is ATM or if telephone is not exist.
    if ((isATM_) || (isAgent_) || ([[poiData_ telephone] isEqualToString:@""])) {
        
        return 3;
        
    } else {
        
        return 4;
        
    }
    
}

/**
 * Asks the data source for a cell to insert in a particular questions of the table view. A PhoneCell or DirecctionCell is returned, containing the
 * information at the given position
 *
 * @param tableView The table-view object requesting the cell
 * @param indexPath An index path locating a row in tableView
 * @return A PhoneCell or a DirectionCell containing information
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellID;
    
    UITableViewCell *result;
    
    NSInteger cell = [indexPath row];
    
    if (((isATM_) || (isAgent_) || ([[poiData_ telephone] isEqualToString:@""])) && cell > 0) {
        
        cell++;
        
    }
    
    switch (cell) {
            
        case 0: {
            
            cellID = [ScheduleCell cellIdentifier];
            
            ScheduleCell *scheduleCell = (ScheduleCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
            
            if (scheduleCell == nil) {
                
                scheduleCell = [ScheduleCell scheduleCell];
                
            }
            
            [scheduleCell setPoi:poiData_];
            
            result = scheduleCell;
            
            break;
            
        }
            
        case 1: {
            
            cellID = [PhoneCell cellIdentifier];
            
            PhoneCell *phoneCell = (PhoneCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
            
            if (phoneCell == nil) {
                
                phoneCell = [PhoneCell phoneCell];
                
            }
            
            [[phoneCell phoneLabel] setText:NSLocalizedString(TELEPHONE_TEXT_KEY, nil)];
            [phoneCell setPhoneNumber:[poiData_ telephone]];
            
            result = phoneCell;
            
            break;
            
        }
            
        case 2: {
            
            cellID = [RouteCell cellIdentifierWithRouteType:rt_food];
            
            RouteCell *routeFoodCell = (RouteCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
            
            if (routeFoodCell == nil) {
                
                routeFoodCell = [RouteCell routeCell];
                
            }
            
            NSArray *routes = [routeFoodInformation_ routes];
            
            if (routes != nil || [routes count] > 0) {
                
                GoogleMapsRoute *routeFood = [routes objectAtIndex:0];
                GoogleMapsLeg *legFood = [[routeFood legs] objectAtIndex:0];
                
                [routeFoodCell setRouteInformationDuration:[Tools notNilString:[[legFood duration] string]] distance:[Tools notNilString:[[legFood distance] string]]];
                
            } else {
                
                [routeFoodCell setRouteInformationDuration:@"" distance:@""];
                
            }
            
            [routeFoodCell setRouteType:rt_food];
			
			[routeFoodCell setShowDisclosureArrow:YES];
            
            result = routeFoodCell;
            
            break;
            
        }
            
        case 3: {
            
            cellID = [RouteCell cellIdentifierWithRouteType:rt_car];
            
            RouteCell *routeCarCell = (RouteCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
            
            if (routeCarCell == nil) {
                
                routeCarCell = [RouteCell routeCell];
                
            }
            
            NSArray *routes = [routeCarInformation_ routes];
            
            if (routes != nil || [routes count] > 0) {
                
                GoogleMapsRoute *routeCar = [routes objectAtIndex:0];
                GoogleMapsLeg *legCar = [[routeCar legs] objectAtIndex:0];
                
                [routeCarCell setRouteInformationDuration:[Tools notNilString:[[legCar duration] string]] distance:[Tools notNilString:[[legCar distance] string]]];
                
            } else {
                
                [routeCarCell setRouteInformationDuration:@"" distance:@""];
                
            }
            
            [routeCarCell setRouteType:rt_car];
			
			[routeCarCell setShowDisclosureArrow:YES];
            
            result = routeCarCell;
            
            break;
            
        }
            
        default: {
            
            result = [tableView dequeueReusableCellWithIdentifier:@""];
            
            if (result == nil) {
                
                result = [[[UITableViewCell alloc] init] autorelease];
                
            }
            
            break;
            
        }
            
    }
    
    return result;
    
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 *
 * @param tableView: The table-view object requesting this information.
 * @param indexPath: An index path that locates a row in tableView.
 * @return A floating-point value that specifies the height (in points) that row should be.
 */ 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger cell = [indexPath row];
    
    if (((isATM_) || (isAgent_) || ([[poiData_ telephone] isEqualToString:@""])) && cell > 0) {
        
        cell++;
        
    }
    
    switch (cell) {
            
        case 0:
            
            return [ScheduleCell cellHeight];
            
        case 1:
            
            return [PhoneCell cellHeight];
            
        case 2:
            
            return [RouteCell cellHeight];
            
        case 3:
            
            return [RouteCell cellHeight];
            
        default:
            
            return [DirectionCell cellHeight];
            
    }
    
}

#pragma mark -
#pragma mark UITableViewDelegate protocol selectors


/**
 * Tells the delegate that the specified row is now selected. Executes the action and show details in POIDetailsATMBranchViewController
 *
 * @param tableView A table-view object informing the delegate about the new row selection
 * @param indexPath An index path locating the new selected row in tableView
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
    
    NSInteger cell = [indexPath row];
    
    if (((isATM_) || (isAgent_) || ([[poiData_ telephone] isEqualToString:@""])) && cell > 0) {
        
        cell++;
        
    }
    
    switch (cell) {
            
        case 1:
				
			[Tools startPhoneCallToNumber:[poiData_ telephone]];
            
            break;
            
        case 2:
            
            if (routeStepsViewController_ == nil) {
                
                routeStepsViewController_ = [[RouteStepsViewController routeStepsViewController] retain];
                
            }
            
            [routeStepsViewController_ setRouteOrigin:[[POILocationSession getInstance] userLocation] routeDestination:destinationCoordinate_ associatedPOI:poiData_ byRouteCar:NO];
            
            [[self navigationController] pushViewController:routeStepsViewController_ animated:YES];
            
            break;
            
        case 3:
            
            if (routeStepsViewController_ == nil) {
                
                routeStepsViewController_ = [[RouteStepsViewController routeStepsViewController] retain];
                
            }
            
            [routeStepsViewController_ setRouteOrigin:[[POILocationSession getInstance] userLocation] routeDestination:destinationCoordinate_ associatedPOI:poiData_ byRouteCar:YES];
            
            [[self navigationController] pushViewController:routeStepsViewController_ animated:YES];
            
            break;
            
        default:
            
            break;
            
    }
    
}

#pragma -
#pragma mark POIDetailsATMBranchViewController selectors

/*
 * Set ATMData to show details in this view
 */
- (void)setATMData:(ATMData *)poiData isAgent:(BOOL)agent {
    
    if (poiData != poiData_) {
        
        [poiData retain];
        [poiData_ release];
        poiData_ = poiData;
        
    }
    
    [destinationCoordinate_ release];
    destinationCoordinate_ = [[Location alloc] initWithGeographicCoordinate:CLLocationCoordinate2DMake([[[poiData geocodedAddress] geoPosition] latitude], [[[poiData geocodedAddress] geoPosition] longitude]) sensorCoordinate:NO formattedAddress:nil];
    
    isATM_ = !agent;
	isAgent_ = agent;
    
    [self displaysInformation];
    
}

/*
 * Set BranchData to show details in this view
 */
- (void)setBranchData:(ATMData *)poiData {
    
    if (poiData != poiData_) {
        
        [poiData retain];
        [poiData_ release];
        poiData_ = poiData;
        
    }
    
    [destinationCoordinate_ release];
    destinationCoordinate_ = [[Location alloc] initWithGeographicCoordinate:CLLocationCoordinate2DMake([[[poiData geocodedAddress] geoPosition] latitude], [[[poiData geocodedAddress] geoPosition] longitude]) sensorCoordinate:NO formattedAddress:nil];
    
    isATM_ = NO;
	isAgent_ = NO;
    
    [self displaysInformation];
    
}

#pragma mark -
#pragma mark MapRouteDirectionsDataDownloadClientDelegate protocol selectors

/*
 * The GoogleMaps directions API response was parsed correcty. The response is analyze to obtain the route information information and display it
 */
- (void)analyzeRouteResponse:(GoogleMapsDirectionsResponse *)aGoogleMapsDirectionsResponse {
    
    if (routeFoodInformation_ == nil) {
        
        routeFoodInformation_ = [aGoogleMapsDirectionsResponse retain];
        
        [self obtainRouteFromOrigin:[[[POILocationSession getInstance] userLocation] geographicCoordinate] 
                      toDestination:[destinationCoordinate_ geographicCoordinate]
                              byCar:YES 
                 obtainedWithSensor:NO];
        
    } else {
        
        routeCarInformation_ = [aGoogleMapsDirectionsResponse retain];
        
    }
    
    [detailsTableView_ reloadData];
    
}

/*
 * Invoked when the GoogleMaps directions API information was downloaded and parsed successfully. The information is analyzed
 * to create the new route
 */
- (void)routeInformationParsed:(MapRouteDirectionsDataDownloadClient *)aRouteDirectionsDataDownloadClient {
    
    if (aRouteDirectionsDataDownloadClient == routeFoodDownloadClient_) {
        
        GoogleMapsDirectionsResponse *routeResponse = [routeFoodDownloadClient_ directionsResponse];
        NSString *responseCode = [routeResponse status];
        
        if ((![responseCode isEqualToString:GOOGLE_MAPS_OPERATION_OK_STATUS]) || ([[routeResponse routes] count] == 0)) {
            
            NSMutableString *errorString = [NSMutableString stringWithString:NSLocalizedString(UNABLE_TO_OBTAIN_ROUTE_ERROR_KEY, nil)];
            
            [Tools showErrorWithMessage:errorString];
            
        } else {
            
            [self analyzeRouteResponse:routeResponse];
            
        }
        
        [routeFoodDownloadClient_ cancelDownload];
        [routeFoodDownloadClient_ release];
        routeFoodDownloadClient_ = nil;
		
		foodInfoDownloaded_ = YES;
        
    } else if (aRouteDirectionsDataDownloadClient == routeCarDownloadClient_) {
        
        GoogleMapsDirectionsResponse *routeResponse = routeCarDownloadClient_.directionsResponse;
        NSString *responseCode = routeResponse.status;
        
        if ((![responseCode isEqualToString:GOOGLE_MAPS_OPERATION_OK_STATUS]) || ([[routeResponse routes] count] == 0)) {
            
            NSMutableString *errorString = [NSMutableString stringWithString:NSLocalizedString(UNABLE_TO_OBTAIN_ROUTE_ERROR_KEY, nil)];
            
            [Tools showErrorWithMessage:errorString];
            
        } else {
            
            [self analyzeRouteResponse:routeResponse];
            
        }
        
        [routeCarDownloadClient_ cancelDownload];
        [routeCarDownloadClient_ release];
        routeCarDownloadClient_ = nil;
		
		carInfoDownloaded_ = YES;
        
    }
    
	if (foodInfoDownloaded_ && carInfoDownloaded_) {
		
		[[self appDelegate] hideActivityIndicator];	
		
	}
    
}

/*
 * The route parsing process finished in error. The user is notified about the situation
 */
- (void)routeParsingProcessError:(NSError *)anError {
    
    [routeFoodDownloadClient_ cancelDownload];
    [routeFoodDownloadClient_ release];
    routeFoodDownloadClient_ = nil;
    
    [routeCarDownloadClient_ cancelDownload];
    [routeCarDownloadClient_ release];
    routeCarDownloadClient_ = nil;
    
    NSMutableString *errorString = [NSMutableString stringWithString:NSLocalizedString(UNABLE_TO_OBTAIN_ROUTE_ERROR_KEY, nil)];
    
    [Tools showErrorWithMessage:errorString];
    
    [self.appDelegate hideActivityIndicator];
    
}

/*
 * The route download process finished in error. The user is notified about the situation
 */
- (void)routeDownloadProcessError:(NSError *)anError {
    
    [self routeParsingProcessError:anError];
    
    [[self appDelegate] hideActivityIndicator];
    
}

/*
 * The route download process was cancelled
 */
- (void)routeDownloadCancelled {
    
    [routeFoodDownloadClient_ cancelDownload];
    [routeFoodDownloadClient_ release];
    routeFoodDownloadClient_ = nil;
    
    [routeCarDownloadClient_ cancelDownload];
    [routeCarDownloadClient_ release];
    routeCarDownloadClient_ = nil;
    
    [[self appDelegate] hideActivityIndicator];
    
}


#pragma mark - 
#pragma mark UINavigationBar

/**
 * The navigation item used to represent the view controller. (read-only)
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [super customNavigationItem];
    
//    NSString *data = @"";
//    
//    if (isATM_) {
//        
//        data = NSLocalizedString(SINGLE_ATM_POI_MAP_KEY, nil);
//        
//    } else {
//		
//		if (isAgent_) {
//			
//			data = NSLocalizedString(SINGLE_AGENT_POI_MAP_KEY, nil);
//			
//		} else {
//			
//			data = NSLocalizedString(SINGLE_BRANCH_POI_MAP_KEY, nil);
//			
//		}
//        
//    }
    
    [[result customTitleView] setTopLabelText:[[poiData_ bankName] uppercaseString]];
    
    return result;
    
}

@end
