/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MapRouteViewController.h"


//Forward declarations
@class RouteStepView;
@class TimeAndDistanceHeaderView;
@class LocalLayer;
@class GoogleMapsRoute;
@class OriginAnnotation;
@class DestinationAnnotation;
@class BaseAnnotation;
@class Location;


/**
 * Base view controller to download POIs information and display a route in the map
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface POIListEnRouteViewController : MapRouteViewController {
    
@private
    
    /**
     * Route steps view
     */
    RouteStepView *routeStepsView_;
    
    /**
     * Time and distance header view to display route time and distance
     */
    TimeAndDistanceHeaderView *timeAndDistanceHeaderView_;
    
    /**
     * Steps navigation selector segmented control
     */
    UISegmentedControl *stepsNavigationSegmentedControl_;
    
    /**
     * Travel mode selector segmented control
     */
    UISegmentedControl *travelModeSegmentedControl_;
    
    /**
     * Annotations shared button. All annotations callout views share the same button
     */
    UIButton *annotationCalloutButton_;
    
    /**
     * Brand image view
     */
    UIImageView *brandImageView_;
    
    /**
     * POI array downloaded. All objects are LocalGeneralPOI instances (or its subclass LocalPetrolStationPOI)
     */
    NSMutableArray *poiList_;
    
    /**
     * Displaying route steps flag. YES when the route steps are being displayed, NO otherwise
     */
    BOOL displayRouteSteps_;
    
    /**
     * Route by car flag. YES when the route displayed is by car, NO otherwise
     */
    BOOL routeByCar_;
    
    /**
     * Current route being displayed
     */
    GoogleMapsRoute *currentRoute_;
    
    /**
     * Current route steps count
     */
    NSUInteger routeStepsCount_;
    
    /**
     * Current step index, (for the complete route steps list)
     */
    NSUInteger stepIndex_;
    
    /**
     * Annotations array. Contains all annotations currently being displayed, but the origin, destination and step annotations.
     * All instances are BaseAnnotation instances
     */
    NSMutableArray *annotationsArray_;
    
    /**
     * Route origin location
     */
    Location *originLocation_;
    
    /**
     * Route destination location
     */
    Location *destinationLocation_;
    
    /**
     * Route origin annotation
     */
    OriginAnnotation *originAnnotation_;
    
    /**
     * Route destination annotation
     */
    DestinationAnnotation *destinationAnnotation_;
    
    /**
     * Step annotation
     */
    BaseAnnotation *stepAnnotation_;
    
    /**
     * User location annotation. This annotation location is obtained from the Session information
     */
    BaseAnnotation *userLocationAnnotation_;
    
    /**
     * The height of mapview.
     */
    CGFloat mapViewHeight_;
    
    /**
     * Show POI callout button flag. YES to show the POI callout button, NO to hide it
     */
    BOOL showPOICalloutButton_;
    
}


/**
 * Provides read only access to the displaying route steps flag
 */
@property (nonatomic, readonly, assign) BOOL displayRouteSteps;

/**
 * Provides read only access to the route origin location
 */
@property (nonatomic, readonly, retain) Location *originLocation;

/**
 * Provides read only access to the route destination location
 */
@property (nonatomic, readonly, retain) Location *destinationLocation;

/**
 * Provides read-write access to the brand image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandImageView;

/**
 * Sets the route origin and destinaton, and start the route download
 *
 * @param routeOrigin: The route origin location
 * @param routeDestination: The route destination location
 * @param byCarRoute: YES if is car route. NO if is food route.
 */
- (void)setRouteOrigin:(Location *)routeOrigin
      routeDestination:(Location *)routeDestination 
          byRouteCar:(BOOL)byCarRoute;

@end
