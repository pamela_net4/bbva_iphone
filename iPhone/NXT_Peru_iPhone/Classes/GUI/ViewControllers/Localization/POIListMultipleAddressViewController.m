/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "POIListMultipleAddressViewController.h"
#import "GeocodedAddressList.h"
#import "ImagesFileNames.h"
#import "ImagesCache.h"
#import "AddressCell.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTNavigationItem.h"
#import "StringKeys.h"
#import "Tools.h"
#import "NXT_Peru_iPhone_AppDelegate.h"

/**
 * Defines the POIListMultipleAddressViewController NIB file name
 */
#define POI_LIST_MULTIPLE_ADDRESS_VIEW_CONTROLLER_NIB_FILE                                              @"POIListMultipleAddressViewController"

/**
 * Defines the POIListMultipleAddressViewController NIB file name for iPad
 */
#define POI_LIST_MULTIPLE_ADDRESS_VIEW_CONTROLLER_NIB_FILE_IPAD                                         @"POIListMultipleAddressViewController_iPad"


#pragma  mark -

@interface POIListMultipleAddressViewController()

/**
 * Relesase the graphic elements not needed when view is hidden
 *
 * @private
 */
- (void)releasePOIListMultipleAddressViewControllerGraphicElements;

/**
 * Invoked by framework when cancel button is tapped. Delegate is notified that the user wants to cancel the selection
 *
 * @private
 */
- (void)cancelButtonTapped;

@end


#pragma mark -

@implementation POIListMultipleAddressViewController

#pragma mark -
#pragma mark Properties

@synthesize similarTableView = similarTableView_;
@synthesize topBar = topBar_;
@synthesize brandImageView = brandImageView_;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Release the used memory
 */
- (void)dealloc {
    
    [self releasePOIListMultipleAddressViewControllerGraphicElements];
    
    [cancelBarButtonItem_ release];
    cancelBarButtonItem_ = nil;
    
    [similarAddressesList_ release];
    similarAddressesList_ = nil;
    
    delegate_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releasePOIListMultipleAddressViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasePOIListMultipleAddressViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Relesase the graphic elements not needed when view is hidden
 */
- (void)releasePOIListMultipleAddressViewControllerGraphicElements {
    
    [similarTableView_ release];
    similarTableView_ = nil;
    
    [topBar_ release];
    topBar_ = nil;
    
    [brandImageView_ release];
    brandImageView_ = nil;
    
}

#pragma mark -
#pragma mark View managment

/**
 * Called after the controller’s view is loaded into memory. Styles graphic elements
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleNavigationBar:topBar_];
    
    brandImageView_.image = [[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
}

/**
 * Notifies the view controller that its view is about to be become visible. Checks whether the view controller is inside a navigation
 * view controller to hide or show the top bar
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    UIView *ownView = self.view;
    CGRect viewFrame = ownView.frame;
    
    if (self.navigationController == nil) {
        
        if (topBar_.superview == nil) {
            
            CGRect topBarFrame = topBar_.frame;
            topBarFrame.origin.y = 0.0f;
            topBarFrame.origin.x = 0.0f;
            topBarFrame.size.width = CGRectGetWidth(viewFrame);
            
            [ownView addSubview:topBar_];
            
        }
        
        UINavigationItem *navigationItem = self.navigationItem;
        
        if (navigationItem != nil) {
            
            [topBar_ setItems:[NSArray arrayWithObject:navigationItem] animated:NO];
            
        }
        
    } else {
        
        [topBar_ removeFromSuperview];
        
    }
    
    [similarTableView_ reloadData];
//    [Tools checkTableViewScrolling:similarTableView_];
    
}

/*
 * Creates and return an autoreleased POIListMultipleAddressViewController contructed from a NIB file
 */
+ (POIListMultipleAddressViewController *)poiListMultipleAddressViewController {
    
    NSString *nib = POI_LIST_MULTIPLE_ADDRESS_VIEW_CONTROLLER_NIB_FILE;
    
    return [[[POIListMultipleAddressViewController alloc] initWithNibName:nib bundle:nil] autorelease];
    
}

#pragma mark -
#pragma mark User interaction

/*
 * Invoked by framework when cancel button is tapped. Delegate is notified that the user wants to cancel the selection
 */
- (void)cancelButtonTapped {
    
    [delegate_ canceledListMultipleAddress];
    
}

#pragma mark -
#pragma mark Information management

/*
 * Sets GeocodedAddressList to show in ViewController
 */
- (void)setGeocoderAddressList:(GeocodedAddressList *)geocodedAddressList {
    
    if (geocodedAddressList != similarAddressesList_) {
        
        [geocodedAddressList retain];
        [similarAddressesList_ release];
        similarAddressesList_ = geocodedAddressList;
        
        [similarTableView_ reloadData];
//        [Tools checkTableViewScrolling:similarTableView_];
        
    }
    
}

#pragma mark -
#pragma mark UITableViewDataSource protocol selectors

/**
 * Tells the data source to return the number of rows in a given section of a table view. Returns the number of similar addresses to display
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section in tableView
 * @return The number of similar addresses to display
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [similarAddressesList_ geocodedAddressCount];
    
}

/**
 * Asks the data source for a cell to insert in a particular questions of the table view. An AddressCell is returned, containing the
 * addres at the given position
 *
 * @param tableView The table-view object requesting the cell
 * @param indexPath An index path locating a row in tableView
 * @return An AddressCell containing title from help
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSString *cellId = [AddressCell cellIdentifier];
    
    AddressCell *result = (AddressCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (result == nil) {
        
        result = [AddressCell addressCell];
        
    }
    
	if (indexPath.row < [similarAddressesList_ geocodedAddressCount]) {
        
        GeocodedAddressData *addressData = [similarAddressesList_ geocodedAddressAtPosition:indexPath.row];
        AddressData *addressInformation = [addressData address];
        
        result.addressFirstLineText = addressInformation.addressFirstLine;
        result.addressSecondLineText = addressInformation.addressSecondLine;

	}
    
    return result;
    
}

#pragma mark -
#pragma mark UITableViewDelegate protocol selectors

/**
 * Tells the delegate that the specified row is now selected. Notifies the delegate about the address selected
 *
 * @param tableView A table-view object informing the delegate about the new row selection
 * @param indexPath An index path locating the new selected row in tableView
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
    
    [delegate_ selectedGeocodedAddress:[similarAddressesList_ geocodedAddressAtPosition:indexPath.row]];
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Return the navigation item used to represent the view controller. Inserts the action buttons needed
 *
 * @return The navigation item used to represent the view controller
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [super customNavigationItem];
        
    NSZone *zone = self.zone;
    
    if (cancelBarButtonItem_ == nil) {
        
        cancelBarButtonItem_ = [[UIBarButtonItem allocWithZone:zone] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                          target:self
                                                                                          action:@selector(cancelButtonTapped)];
        
    }
    
	[[result customTitleView] setTopLabelText:NSLocalizedString(SELECTED_ONE_LOCATION_TEXT_KEY, nil)];
    result.rightBarButtonItem = cancelBarButtonItem_;
    
    return result;
    
}

@end
