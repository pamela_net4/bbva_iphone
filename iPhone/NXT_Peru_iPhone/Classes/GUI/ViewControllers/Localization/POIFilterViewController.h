/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTViewController.h"

#import "NXTSwitchCell.h"

@protocol POIFilterViewControllerDelegate

/**
 * This method is called when filter is finished.
 */
- (void)didFinishFilterWithOptions:(NSArray *)array;

@end

/**
 * This view controller is to filter map pois.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface POIFilterViewController : NXTViewController <NXTSwitchCellDelegate> {
    
@private
    
    /**
     * Table view to populate data.
     */
    UITableView *tableView_;
    
    /**
     * Button to accept filter.
     */
    UIButton *acceptButton_;
    
    /**
     * Branding to show image branding.
     */
    UIImageView *brandingImageView_;
    
    /**
     * The number of rows in tableView.
     */
    NSInteger numberOfRows_;
    
    /**
     * The dictionary to normalize the switch cells.
     */
    NSMutableDictionary *cellsStatusDictionary;
    
    /**
     * Flag to know if ATM's is expanded.
     */
    BOOL atmIsExpanded_;
    
    /**
     * Flag to know if Agents is expanded.
     */
    BOOL agentsIsExpanded_;
    
    /**
     * The delegate of FilterMapViewController.
     */
    id <POIFilterViewControllerDelegate> delegate_;
    
    
}

/**
 * Provides read-write access to tableView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *tableView;

/**
 * Provides read-write access to acceptButton. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *acceptButton;

/**
 * Provides read-write access to brandingImageView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;

/**
 * Provides read-write access to delegate.
 */
@property (nonatomic, readwrite, assign) id <POIFilterViewControllerDelegate> delegate;

/**
 * Creates and returns an autoreleased POIFilterViewController constructed from a NIB file.
 *
 * @return The autoreleased POIFilterViewController constructed from a NIB file.
 */
+ (POIFilterViewController *)poiFilterViewController;

/**
 * When tapped accept button launch this method.
 */
- (IBAction)acceptFilterMap:(id)sender;

/**
 * Set current status options with array.
 *
 * @param optionsArray: The array that contain all options.
 */
- (void)setFilterOptionsValue:(NSArray *)optionsArray;

@end
