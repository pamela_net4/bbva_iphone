/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "POIListEnRouteViewController.h"
#import "GeoServerManager.h"
#import "POIListMultipleAddressViewController.h"
#import "DownloadListener.h"
#import "POIFilterViewController.h"

//Forward declarations
@class POIListLocationNotificationReceiver;
@class POIListViewController;
@class Location;
@class RouteStepsViewController;
@class ATMList;
@class DownloadListener;
@class BranchesList;
@class GeocodedAddressList;
@class OperationResult;
@class POIListMultipleAddressViewController;
@class POIDetailsATMBranchViewController;
@class POIFilterViewController;

/**
 * View controller to display a POI list as a list or inside a map
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface POIMapViewController : POIListEnRouteViewController <POIListMultipleAddressViewControllerDelegate, UISearchBarDelegate, DownloadListener, POIFilterViewControllerDelegate,CLLocationManagerDelegate> {

@private
    
    /**
     * Search view to look for the location
     */
    UISearchBar *locationSearchBar_;
    
    /**
     * Information support view. It excludes the layer view
     */
    UIView *informationSupportView_;
    
    /**
     * View containing all visual elements associated to the map view
     */
    UIView *mapViewSupportView_;
    
    /**
     * The toolbar view to put all buttons.
     */
    UIView *toolbarView_;
    
    /**
     * The button to show list mode POIs.
     */
    UIButton *listModeButton_;
    
    /**
     * The segmented control to show types of POIs.
     */
    UISegmentedControl *typesSegmentedControl_;
    
    /**
     * Button to center in user localized.
     */
    UIButton *localizedButton_;
	
	/**
	 * The separator image view.
	 */
	UIImageView *separatorImageView_;
        
    /**
     * User locating process start time stamp. The locating process must not take more than a give amount
     */
    NSTimeInterval userLocatingProcessStartTime_;
    
    /**
     * Locate banking POIs operation state. The values are defined in the LocateBankingPOIsOperationStates enumerated
     */
    NSInteger locateBankingPOIsOperationState_;
    
    /**
     * ATM list being downloaded. Once the download is complete, it is pre-processed and relesed if an error was found. That way,
     * when the final response is analyzed (ATMs and branches download finishes), the error can be detected and make a difference
     * from the situation when no ATM were found
     */
    ATMList *downloadingATMList_;
    
    /**
     * ATM list download identifier
     */
    NSString *atmDownloadIdentifier_;
    
    /**
     * ATMs download operation result
     */
    OperationResult *atmOperationResult_;
    
    /**
     * Branches list being downloaded. Once the download is complete, it is pre-processed and relesed if an error was found. That way,
     * when the final response is analyzed (ATMs and branches download finishes), the error can be detected and make a difference
     * from the situation when no branches were found
     */
    BranchesList *downloadingBranchesList_;
    
    /**
     * Branches download identifier
     */
    NSString *branchesDownloadIdentifier_;
    
    /**
     * Express agents list being downloaded. Once the download is complete, it is pre-processed and relesed if an error was found. That way,
     * when the final response is analyzed (ATMs, express agents and branches download finishes), the error can be detected and make a difference
     * from the situation when no express agent were found
     */
    ATMList *downloadingExpressAgentList_;
    
    /**
     * Express agents download operation result
     */
    OperationResult *expressAgentsOperationResult_;
    
    /**
     * Express agents list download identifier
     */
    NSString *expressAgentDownloadIdentifier_;
    
    /**
     * Route origin address download operation result
     */
    OperationResult *routeOriginOperationResult_;
    
    /**
     * Route origin address download identifier
     */
    NSString *routeOriginDownloadIdentifier_;
    
    /**
     * Branches download operation result
     */
    OperationResult *branchesOperationResult_;
    
    /**
     * Address list similar to a given one
     */
    GeocodedAddressList *similarAddressesList_;
    
    /**
     * Address list similar to a given one operation result
     */
    OperationResult *similarAddressesOperationResult_;
    
    /**
     * This view contain multiple address for you can choose only one
     */
    POIListMultipleAddressViewController *multipleAddressViewController_;
    
    /**
     * The list of branches and ATM.
     */
    POIListViewController *poiListViewController_;
    
    /**
     * Show detail information to ATM or Branch
     */
    POIDetailsATMBranchViewController *detailATMBranchViewController_;
    
    /**
     * The view controller to filter maps.
     */
    POIFilterViewController *poiFilterViewController_;

    /**
     * Similar addresses operation download identifier
     */
    NSString *similarAddressesDownloadIdentifier_;
    
    /**
     * Text that user search.
     */
    NSString *locationString_;
    
    /**
     * counter for increment or decrement the number of conexions
     */ 
    NSInteger numberOfConetions_;
    
    /**
     * Back navigation flag. YES when the view is appearing from a back navigation in the navigation controller, NO otherwise
     */
    BOOL isBackNavigation_;
	
	/**
	 * Flag to know if location is enabled.
	 */
	BOOL enabledLocation_;

}


/**
 * Provides read-write access to the search view to look for the location and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UISearchBar *locationSearchBar;

/**
 * Provides read-write access to the information support view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *informationSupportView;

/**
 * Provides read-write access to the view containing all visual elements associated to the map view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *mapViewSupportView;

/**
 * Provides read-write access to toolbarView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *toolbarView;

/**
 * Provides read-write access to listModeButton_. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *listModeButton;

/**
 * Provides read-write access to typesSegmentedControl. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UISegmentedControl *typesSegmentedControl;

/**
 * Provides read-write access to localizedButton. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *localizedButton;

/**
 * Provides read-write access to separatorImageView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separatorImageView;

/**
 * Provides read-write access to the isBackNavigation_
 */
@property (nonatomic, readwrite, assign) BOOL isBackNavigation;

/**
 * Creates and returns an autoreleased POIListViewController constructed from a NIB file
 *
 * @return The autoreleased POIListViewController constructed from a NIB file
 */
+ (POIMapViewController *)poiMapViewController;

/**
 * Show list of ATM and branches.
 */
- (IBAction)showATMAndBranchesList:(id)sender;

/**
 * User location in mapview.
 */
- (IBAction)jumpToUserLocation:(id)sender;

/**
 * The segmented state is changed.
 */
- (IBAction)segmentedChangedState:(id)sender;

@end
