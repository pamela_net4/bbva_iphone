/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "POIListViewController.h"

#import "ATMList.h"
#import "BranchesList.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "Location.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "POIDetailsATMBranchViewController.h"
#import "POILocationSession.h"
#import "POILocationCell.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"POIListViewController"

/**
 * Defines the segment branches.
 */
#define SEGMENT_BRANCHES                                            0

/**
 * Defines the segment ATM.
 */
#define SEGMENT_ATM                                                 1

/**
 * Defines the segment agents.
 */
#define SEGMENT_AGENTS                                              2

@interface POIListViewController (Private)

/**
 * Show filter map view controller to select like filter.
 *
 * @private
 */
- (void)showFilterMap;

/**
 * Fills the poi list depending on selected type and search text
 *
 * @private
 */
- (void)fillPoiList;

/**
 * Filters the poi list depending on search text
 *
 * @private
 */
- (void)filterPoiList;

/**
 * Releases all graphic elements contained on this view controller
 *
 * @private
 */
- (void)releaseGraphicElements;

@end

@implementation POIListViewController

#pragma mark -
#pragma mark Properties

@synthesize tableView = tableView_;
@synthesize toolbarView = toolbarView_;
@synthesize segmentedControl = segmentedControl_;
@synthesize separatorImageView = separatorImageView_;
@synthesize brandingImageView = brandingImageView_;
@synthesize searchBar = searchBar_;
#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [tableView_ release];
    tableView_ = nil;
    
    [toolbarView_ release];
    toolbarView_ = nil;
    
    [segmentedControl_ release];
    segmentedControl_ = nil;
	
	[separatorImageView_ release];
	separatorImageView_ = nil;
    
    [brandingImageView_ release];
    brandingImageView_ = nil;
	
	[searchBar_ release];
	searchBar_ = nil;
    
    [detailATMBranchViewController_ release];
    detailATMBranchViewController_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases all graphic elements contained on this view controller
 */
- (void)releaseGraphicElements {
    
    [tableView_ release];
    tableView_ = nil;
    
    [toolbarView_ release];
    toolbarView_ = nil;
    
    [segmentedControl_ release];
    segmentedControl_ = nil;
	
	[separatorImageView_ release];
	separatorImageView_ = nil;
    
    [brandingImageView_ release];
    brandingImageView_ = nil;
	
	[searchBar_ release];
	searchBar_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
	
	poisArray_ = [[NSMutableArray alloc] init];
    
}

/*
 * Creates and returns an autoreleased BranchesAndATMViewController constructed from a NIB file.
 */
+ (POIListViewController *)poiListViewController {
    
    POIListViewController *result =  [[[POIListViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    [NXT_Peru_iPhoneStyler styleTableView:tableView_];
	
	[searchBar_ setText:searchText_];
	[searchBar_ setTintColor:[UIColor BBVAGreyToneFourColor]];
	[searchBar_ setPlaceholder:NSLocalizedString(POI_LIST_FILTER_PLACEHOLDER_TEXT_KEY, nil)];
	
	// Changes style of return key of search bar
	// Copied for NXT
    for (UIView *subview in searchBar_.subviews) {
        
        if ([subview isKindOfClass:[UITextField class]]) {
            
            UITextField *searchField = (UITextField *)subview;
            
            searchField.enablesReturnKeyAutomatically = NO;
            searchField.returnKeyType = UIReturnKeyDone;
        }
    }
	
	[toolbarView_ setBackgroundColor:[UIColor BBVAGreyToneFiveColor]];
	
	ImagesCache *imagesCache = [ImagesCache getInstance];
	
	[separatorImageView_ setImage:[imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];    
    [brandingImageView_ setImage:[imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    [segmentedControl_ setTitle:NSLocalizedString(BRANCH_POI_MAP_KEY, nil) forSegmentAtIndex:SEGMENT_BRANCHES];
    [segmentedControl_ setTitle:NSLocalizedString(ATM_POI_MAP_KEY, nil) forSegmentAtIndex:SEGMENT_ATM];
    [segmentedControl_ setTitle:NSLocalizedString(AGENTS_POI_MAP_KEY, nil) forSegmentAtIndex:SEGMENT_AGENTS];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        [segmentedControl_ setTintColor:[UIColor BBVABlueSpectrumColor]];
    }
    
    segmentedIndex_ = SEGMENT_BRANCHES;
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];

    [self releaseGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated: If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
	
	[[self appDelegate] setTabBarVisibility:NO animated:YES];
	
	[segmentedControl_ setSelectedSegmentIndex:[[POILocationSession getInstance] segmentedSelectedIndex]];
	
	[self segmentedValueChanged:nil];
    
    [tableView_ reloadData];
    
}

#pragma mark -
#pragma mark User interaction

/**
 * This event is throwed when the segmented value is changed.
 */
- (IBAction)segmentedValueChanged:(id)sender {
    
    segmentedIndex_ = [segmentedControl_ selectedSegmentIndex];
	
	POILocationSession *poiSession = [POILocationSession getInstance];
	
	[poiSession setSegmentedSelectedIndex:segmentedIndex_];
	
	[poisArray_ removeAllObjects];
	
    switch (segmentedIndex_) {
            
        case SEGMENT_BRANCHES:
            
            [poisArray_ addObjectsFromArray:[poiSession branchList]];
            
            break;
            
        case SEGMENT_ATM:
            
            [poisArray_ addObjectsFromArray:[poiSession atmFilteredList]];
			
            break;
            
        case SEGMENT_AGENTS:
            
            [poisArray_ addObjectsFromArray:[poiSession agentsFilteredList]];
            
            break;
            
        default:
			
            break;
    }
	
	[self fillPoiList];
    
    [tableView_ reloadData];
    
}

#pragma mark -
#pragma mark UISearchBarDelegate

/**
 * Tells the delegate that the user changed the search text.
 *
 * @param searchBar: The search bar that is being edited.
 * @param searchText: The current text in the search text field.
 */
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    [searchText_ release];
    searchText_ = [searchText copy];
    
	[self fillPoiList];
	
    [tableView_ reloadData];
}

/**
 * Tells the delegate that the search button was tapped.
 *
 * @param searchBar: The search bar that was tapped.
 */
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar_ resignFirstResponder];
}

#pragma mark -
#pragma mark Data

/*
 * Returns a list depending on selected type
 */
- (void)fillPoiList {
    
    NSMutableArray *result = [NSMutableArray array];
	POILocationSession *poiSession = [POILocationSession getInstance];
    
    switch (segmentedIndex_) {
            
        case SEGMENT_BRANCHES:
            
            [result addObjectsFromArray:[poiSession branchList]];
            
            [result sortUsingSelector:@selector(compareByDistance:)];
            
            break;
            
        case SEGMENT_ATM:
            
            [result addObjectsFromArray:[poiSession atmFilteredList]];
			
            break;
            
        case SEGMENT_AGENTS:
            
            [result addObjectsFromArray:[poiSession agentsFilteredList]];
            
            break;
            
        default:
            break;
    }
    
	[poisArray_ removeAllObjects];
	[poisArray_ addObjectsFromArray:result];
    
    [self filterPoiList];
	
}

/*
 * Filters the poi list depending on search text
 */
- (void)filterPoiList {
    
    NSString *text = [searchText_ stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	
    if (text.length > 0) {
        
        NSMutableString *predicateString = [NSMutableString stringWithString:@""];
        
        NSArray *searchWords = [text componentsSeparatedByString:@" "];
		
        for (int i = 0; i < searchWords.count; i++) {
            
            NSString *word = [searchWords objectAtIndex:i];
            
            NSString *containsString = [NSString stringWithFormat:@"CONTAINS[cd] '%@'", word];
			
            [predicateString appendString:@"("];
            [predicateString appendFormat:@"geocodedAddress.address.addressFirstLine %@", containsString];
            [predicateString appendString:@" OR "];
            [predicateString appendFormat:@"geocodedAddress.address.addressSecondLine %@", containsString];
            [predicateString appendString:@" OR "];
            [predicateString appendFormat:@"bankName %@", containsString];
            [predicateString appendString:@")"];
            
            if (i < searchWords.count - 1) {
                
                [predicateString appendString:@" AND "];
            }
        }
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
		
        NSArray *filteredArray = [poisArray_ filteredArrayUsingPredicate:predicate];
        
		[poisArray_ removeAllObjects];
		[poisArray_ addObjectsFromArray:filteredArray];
    
    }

}

#pragma mark -
#pragma mark UITableViewDataSource protocol selectors

/**
 * Tells the data source to return the number of rows in a given section of a table view. Returns the number of actions available,
 * three in this case.
 *
 * @param tableView: The table-view object requesting this information.
 * @param section: An index number identifying a section in tableView.
 * @return The number of actions available.
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [poisArray_ count];
    
}

/**
 * Asks the data source for a cell to insert in a particular questions of the table view. A POILocationCell is returned, containing
 * the action at the given question pressed.
 *
 * @param tableView: The table-view object requesting the cell.
 * @param indexPath: An index path locating a row in tableView.
 * @return An object inheriting from POICell that the table view can use for the specified row.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSString *cellId = [POILocationCell cellIdentifier];
    
    POILocationCell *result = (POILocationCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (result == nil) {
        
        result = [POILocationCell poiLocationCell];
        
    }
    
    NSUInteger row = [indexPath row];
    
    [result setPoiType:[segmentedControl_ selectedSegmentIndex]];
    
    [result setPoi:[poisArray_ objectAtIndex:row]];
    
    return result;
    
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 *
 * @param tableView: The table-view object requesting this information.
 * @param indexPath: An index path that locates a row in tableView.
 * @return A floating-point value that specifies the height (in points) that row should be.
 */ 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [POILocationCell cellHeight];
    
}

#pragma mark -
#pragma mark UITableViewDelegate protocol selectors

/**
 * Tells the delegate that the specified row is now selected.
 *
 * @param tableView: A table-view object informing the delegate about the new row selection.
 * @param indexPath: An index path locating the new selected row in tableView.
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
    
    if (detailATMBranchViewController_ == nil) {
        
        detailATMBranchViewController_ = [[POIDetailsATMBranchViewController poiDetailsATMBranchViewController] retain];
        
    }
    
    [detailATMBranchViewController_ setShouldClearInterfaceData:YES];
	POILocationSession *poiSession = [POILocationSession getInstance];
    
    switch ([segmentedControl_ selectedSegmentIndex]) {
            
        case SEGMENT_BRANCHES:
            
            [detailATMBranchViewController_ setBranchData:[[poiSession branchList] objectAtIndex:[indexPath row]]];
            
            break;
            
        case SEGMENT_ATM:
            
            [detailATMBranchViewController_ setATMData:[[poiSession atmFilteredList] objectAtIndex:[indexPath row]] isAgent:NO];
            
            break;
            
        case SEGMENT_AGENTS:
            
            [detailATMBranchViewController_ setATMData:[[poiSession agentsFilteredList] objectAtIndex:[indexPath row]] isAgent:YES];
            
            break;
            
        default:
            
            break;
            
    }
    
    [[self navigationController] pushViewController:detailATMBranchViewController_ animated:YES];
    
}

#pragma mark -
#pragma mark Filter map methods

/*
 * Show filter map view controller to select like filter.
 */
- (void)showFilterMap {
    
    if (poiFilterViewController_ == nil) {
        
        poiFilterViewController_ = [[POIFilterViewController poiFilterViewController] retain];
        
    }
    
    [poiFilterViewController_ setDelegate:self];
    
    UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController:poiFilterViewController_] autorelease];
	
	[[self appDelegate] presentModalViewController:navController animated:YES];
    
}

#pragma mark -
#pragma mark POIFilterViewControllerDelegate protocol selectors

/**
 * This method is called when filter is finished.
 */
- (void)didFinishFilterWithOptions:(NSArray *)array {
	
	[[POILocationSession getInstance] setPOIFilter:array];
    
    [poiFilterViewController_ dismissModalViewControllerAnimated:YES];
    
    [self segmentedValueChanged:nil];
	
}

#pragma mark - 
#pragma mark UINavigationBar

/**
 * The navigation item used to represent the view controller. (read-only)
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [super customNavigationItem];
    
    [[result customTitleView] setTopLabelText:NSLocalizedString(POI_LOCATION_MAP_NAVIGATION_BAR_TITLE_KEY, nil)];
	
	[result setRightBarButtonItem:[[[UIBarButtonItem alloc] initWithImage:[[ImagesCache getInstance] imageNamed:MAP_FILTER_IMAGE_FILE] style:UIBarButtonItemStyleBordered target:self action:@selector(showFilterMap)] autorelease]];
    
    return result;
    
}

@end
