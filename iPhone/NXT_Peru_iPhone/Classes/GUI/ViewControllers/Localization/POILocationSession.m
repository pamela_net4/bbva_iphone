/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "POILocationSession.h"

#import "ATMList.h"
#import "BranchesList.h"
#import "Constants.h"
#import "Location.h"

#pragma mark -

@interface POILocationSession (Private)

/**
 * Create nsmutablearray filter 
 *
 * @private
 */
- (void)createFilter;

@end

@implementation POILocationSession

#pragma mark -
#pragma mark Properties

@synthesize atmFilteredList = atmFilteredList_;
@synthesize agentsFilteredList = agentsFilteredList_;
@synthesize poiFilter = poiFilter_;
@synthesize segmentedSelectedIndex = segmentedSelectedIndex_;
@synthesize userLocation = userLocation_;

/**
 * POILocationSession only insntace
 */
static POILocationSession *managerInstance = nil;

#pragma mark -
#pragma mark Singleton selectors

/**
 * The allocation returns the singleton only instance.
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([POILocationSession class]) {
        
		if (managerInstance == nil) {
            
			managerInstance = [super allocWithZone:zone];
            
			return managerInstance;
            
		}
        
	}
	
	return nil;
    
}

/*
 * Returns the POILocationSession singleton only instance.
 */
+ (POILocationSession *)getInstance {
    
	if (managerInstance == nil) {
        
		@synchronized([POILocationSession class]) {
            
			if (managerInstance == nil) {
                
				managerInstance = [[POILocationSession alloc] init];
                
			}
            
		}
        
	}
	
	return managerInstance;
    
}

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
	
    [atmList_ release];
    atmList_ = nil;
    
    [atmFilteredList_ release];
    atmFilteredList_ = nil;
    
    [branchList_ release];
    branchList_ = nil;
    
    [poiFilter_ release];
    poiFilter_ = nil;
    
    [agentsList_ release];
    agentsList_ = nil;
    
    [agentsFilteredList_ release];
    agentsFilteredList_ = nil;
	
	segmentedSelectedIndex_ = 0;
	
	[userLocation_ release];
	userLocation_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer.
 * Implemented by subclasses to initialize a new object (the receiver) immediately after memory for it has been allocated.
 *
 * @return An initialized object.
 */
- (id)init {
    
    self = [super init];
    
    if (self) {
		
		branchList_ = [[NSMutableArray alloc] init];
		atmList_ = [[NSMutableArray alloc] init];
		agentsList_ = [[NSMutableArray alloc] init];
		
		atmFilteredList_ = [[NSMutableArray alloc] init];
		[agentsFilteredList_ = [NSMutableArray alloc] init];
        
	    atmFilteredList_ = [[NSMutableArray alloc] init];
		agentsFilteredList_ = [[NSMutableArray alloc] init];
		
		// Restore the POI filter from system preferences.
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		
		// If POI filter not initialized, this start with all types.
		if ([prefs objectForKey:@"PoiFilter"] == nil) {
			
			poiFilter_ = [[NSMutableArray alloc] init];
			
			[poiFilter_ addObject:[NSNumber numberWithInt:atmType]];
			[poiFilter_ addObject:[NSNumber numberWithInt:atmBCType]];
			[poiFilter_ addObject:[NSNumber numberWithInt:atmInterbankType]];
			[poiFilter_ addObject:[NSNumber numberWithInt:atmScotiabankType]];
			[poiFilter_ addObject:[NSNumber numberWithInt:expressAgentType]];
			[poiFilter_ addObject:[NSNumber numberWithInt:expressAgentPlusType]];
			[poiFilter_ addObject:[NSNumber numberWithInt:kasnetType]];
			[poiFilter_ addObject:[NSNumber numberWithInt:multifacilType]];
			
        }else{
            
            poiFilter_ = [[NSMutableArray alloc]initWithArray:[prefs objectForKey:@"PoiFilter"]];
            
		}
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Create annotation filter

/*
 * Create nsmutablearray filter
 */
- (void)createFilter {
    
    if (poiFilter_ != nil) {
        
        NSUInteger index;
        
        [atmFilteredList_ removeAllObjects];
        [agentsFilteredList_ removeAllObjects];
        
        for (ATMData *atmData in atmList_) {
            
            index = [poiFilter_ indexOfObject:[NSNumber numberWithInt:[atmData category]]];
            
            if ((index < [poiFilter_ count]) && index != NSNotFound) {
                
                [atmFilteredList_ addObject:atmData];
                
            }
            
        }
        
        for (ATMData *expressAgentData in agentsList_) {
            
            index = [poiFilter_ indexOfObject:[NSNumber numberWithInt:[expressAgentData category]]];
            
            if ((index < [poiFilter_ count]) && index != NSNotFound) {
                
                [agentsFilteredList_ addObject:expressAgentData];
                
            }
            
        }
        
    }
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the branchList_ value.
 */
- (NSArray *)branchList {
	
	return branchList_;
	
}

/*
 * Returns the agentsList_ value.
 */
- (NSArray *)agentsList {
	
	return agentsList_;
	
}

/*
 * Returns the atmList_ value.
 */
- (NSArray *)atmList {
	
	return atmList_;
	
}

/*
 * Set the branchList_ value to another value.
 */
- (void)setBranchList:(BranchesList *)branchList {
    
	NSUInteger branchesCount = [branchList branchCount];
	BranchData *branch = nil;
	
	[branchList_ removeAllObjects];
	
	for (NSUInteger i = 0; i < branchesCount; i ++) {
		
		branch = [branchList branchAtPostion:i];
		
		if (branch != nil) {
			
			[branchList_ addObject:branch];
			
		}
		
	}
    
}

/*
 * Set the atmList_ value to another value.
 */
- (void)setATMList:(ATMList *)atmList {
    
	NSUInteger atmCount = [atmList atmCount];
	ATMData *atm = nil;
	
	[atmList_ removeAllObjects];
	
	for (NSUInteger i = 0; i < atmCount; i ++) {
		
		atm = [atmList atmAtPostion:i];
		
		if (atm != nil) {
			
			[atmList_ addObject:atm];
			
		}
		
	}
	
}

/**
 * Set the agentsList_ value to another value.
 *
 * @param agentsList: Another value to agentsList_.
 */
- (void)setAgentsList:(ATMList *)agentsList {
    
	NSUInteger expressAgentsCount = [agentsList atmCount];
	ATMData *expressAgent = nil;
	
	[agentsList_ removeAllObjects];
	
	for (NSUInteger i = 0; i < expressAgentsCount; i++) {
		
		expressAgent = [agentsList atmAtPostion:i];
		
		if (expressAgent != nil) {
			
			[agentsList_ addObject:expressAgent];
			
		}
	}
    
}

/*
 * Set the enabledPOIFilter_ value to another value.
 */
- (void)setPOIFilter:(NSArray *)poiFilter {
	
	NSArray *filter = [NSArray arrayWithArray:poiFilter];
	
	[poiFilter_ removeAllObjects];
	[poiFilter_ addObjectsFromArray:filter];
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];   
	[prefs setObject:poiFilter_ forKey:@"PoiFilter"];
	[prefs synchronize];
	
	[self createFilter];
    
}

@end
