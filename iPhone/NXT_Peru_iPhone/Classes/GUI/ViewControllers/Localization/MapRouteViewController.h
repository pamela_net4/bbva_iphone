/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MapRouteDirectionsDataDownloadClient.h"

//Forward declarations
@class GoogleMapsDirectionsResponse;
@class MapRouteDirectionsDataDownloadClient;
@class MKRouteAnnotation;
@class MKRouteAnnotationView;


/**
 * View controller to display a route using GoogleMaps Directions API
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MapRouteViewController : NXTViewController <MKMapViewDelegate, CLLocationManagerDelegate, MapRouteDirectionsDataDownloadClientDelegate> {
    
@private
    
    /**
     * Map view. It displays the map and route information
     */
    MKMapView *mapView_;
    
    /**
     * Warnings button. Displays the warning for the route
     */
    UIButton *warningsButton_;
    
    /**
     * Current route information being displayed
     */
    GoogleMapsDirectionsResponse *routeInformation_;
    
    /**
     * Auxiliary element to download the route from GoogleMaps directions API
     */
    MapRouteDirectionsDataDownloadClient *routeDataDownloadClient_;

    /**
     * Annotation to display the route (for iOS 3.x)
     */
    MKRouteAnnotation *routeAnnotation_;
    
    /**
     * Annotation view to display the route (for iOS 3.x)
     */
    MKRouteAnnotationView *routeAnnotationView_;
    
    /**
     * Polyline to display the route (for iOS 4.x)
     */
    MKPolyline *routePolyline_;
    
    /**
     * Polyline view to display the route (for iOS 4.x)
     */
    MKPolylineView * routePolylineView_;
    
    /**
     * Auxiliary polygon to fix a MKMapView bug when displaying overlays
     */
    MKPolygon *auxPolygon_;
    
    /**
     * Auxiliary transparent polygon view to fix a MKMapView bug when displaying overlays
     */
    MKPolygonView *auxTransparentPolygonView_;
    
}

@property (strong, nonatomic) CLLocationManager *locationManager;
/**
 * Provides read-write access to the map view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet MKMapView *mapView;

/**
 * Provides read-write access to the warnings button and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *warningsButton;

/**
 * Provides read-only access to the route north-east point (the point with greater latitude and longitude)
 */
@property (nonatomic, readonly, assign) CLLocationCoordinate2D northEastPoint;

/**
 * Provides read-only access to the route south-west point (the point with lesser latitude and longitude)
 */
@property (nonatomic, readonly, assign) CLLocationCoordinate2D southWestPoint;

/**
 * Provides read-only access to the downloading route flag. YES when the route is being downloaded, NO otherwise
 */
@property (nonatomic, readonly, assign) BOOL downloadingRoute;


/**
 * Creates and returns an autoreleased MapRouteViewController constructed from a NIB file
 *
 * @return The autoreleased MapRouteViewController constructed from a NIB file
 */
+ (MapRouteViewController *)mapRouteViewController;


/**
 * Starts process to the obtains a route from an origin coordinate to a destination coordinate. There must be only
 * one active route process
 *
 * @param anOriginCoordinate The route origin coordinate
 * @param aDestinationCoordinate The route destination coordinate
 * @param byCarFlag YES when the route must be by car, NO when the route must be on foot
 * @param sensorFlag YES when origin or destination coordinates were obtained with the sensor device (such as GPS), NO otherwise
 * @return YES if obtain route process can be started, NO otherwise
 */
- (BOOL)obtainRouteFromOrigin:(CLLocationCoordinate2D)anOriginCoordinate
                toDestination:(CLLocationCoordinate2D)aDestinationCoordinate
                        byCar:(BOOL)byCarFlag
           obtainedWithSensor:(BOOL)sensorFlag;

/**
 * Checks whether there is an obtain route process active
 *
 * @return YES when any obtain route process is active, NO otherwise
 */
- (BOOL)isObtainRouteProcessActive;

/**
 * Cancels the current active obtain route process, if any
 */
- (void)cancelActiveObtainRouteProcess;

/**
 * Removes and releases the annotations and the route information
 */
- (void)removeAnnotationsAndRouteInformation;

/**
 * Sets route elements. In iOS 3.x, an MKRouteAnnotation is created, whereas in iOS 4.x, an MKPolyline instance is created. In both cases
 * the information stored in routeInformation_ is used to construct the elements
 */
- (void)setRouteElements;

@end
