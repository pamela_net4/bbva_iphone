/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "SingletonBase.h"

@class ATMList;
@class BranchesList;
@class Location;

/**
 * Singleton class to save poi arrays and filter pois.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface POILocationSession : SingletonBase {
    
@private
	
    /**
     * Array with atms POI
     */
    NSMutableArray *atmList_;
    
    /**
     * Array with atms filtered POI.
     */
    NSMutableArray *atmFilteredList_;
    
    /**
     * Array with agents POI
     */
    NSMutableArray *agentsList_;
    
    /**
     * Array agents filtered POI.
     */
    NSMutableArray *agentsFilteredList_;
    
    /**
     * Array with branches POI
     */
    NSMutableArray *branchList_;
    
    /**
     * Array with filter flag POI
     */
    NSMutableArray *poiFilter_;
	
	/**
	 * The session selected segmented.
	 */
	NSUInteger segmentedSelectedIndex_;
	
	/**
	 * User location.
	 */
	Location *userLocation_;
    
}

/**
 * Provides read-write access to atmFilteredList.
 */
@property (nonatomic, readonly, retain) NSArray *atmFilteredList;

/**
 * Provides read-write access to agentsFilteredList.
 */
@property (nonatomic, readonly, retain) NSArray *agentsFilteredList;

/**
 * Provides read-write access to enabledPOIFilter.
 */
@property (nonatomic, readonly, retain) NSArray *poiFilter;

/**
 * Provides read-write access to segmentedSelectedIndex.
 */
@property (nonatomic, readwrite, assign) NSUInteger segmentedSelectedIndex;

/**
 * Provides read-write access to location.
 */
@property (nonatomic, readwrite, retain) Location *userLocation;

/**
 * Returns the POILocationSession singleton only instance.
 *
 * @return The POILocationSession singleton only instance.
 */
+ (POILocationSession *)getInstance;

/**
 * Returns the branchList_ value.
 *
 * @return The branchList_ value.
 */
- (NSArray *)branchList;

/**
 * Returns the agentsList_ value.
 *
 * @return The agentsList_ value.
 */
- (NSArray *)agentsList;

/**
 * Returns the atmList_ value.
 *
 * @return The atmList_ value.
 */
- (NSArray *)atmList;

/**
 * Set the branchList_ value to another value.
 *
 * @param branchList: Another value to branchList_.
 */
- (void)setBranchList:(BranchesList *)branchList;

/**
 * Set the atmList_ value to another value.
 *
 * @param atmList: Another value to atmList_.
 */
- (void)setATMList:(ATMList *)atmList;

/**
 * Set the agentsList_ value to another value.
 *
 * @param agentsList: Another value to agentsList_.
 */
- (void)setAgentsList:(ATMList *)agentsList;

/**
 * Set the poiFilter_ value to another value.
 *
 * @param poiFilter: Another value to poiFilter_.
 */
- (void)setPOIFilter:(NSArray *)poiFilter;

@end
