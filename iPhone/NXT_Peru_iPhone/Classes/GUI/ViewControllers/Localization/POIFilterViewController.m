/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "POIFilterViewController.h"

#import "Constants.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "POILocationSession.h"
#import "NXTSwitchCell.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"POIFilterViewController"

@interface POIFilterViewController (Private)

#pragma mark -
#pragma mark Private methods

/**
 * Enable all ATM types and reload data.
 *
 * @param enable: YES to enable, NO otherwise.
 * @private
 */
- (void)enableAllATMTypes:(BOOL)enable;

/**
 * Enable all Agentes types and reload data.
 *
 * @param enable: YES to enable, NO otherwise.
 * @private
 */
- (void)enableAllAgentsType:(BOOL)enable;

/**
 * Dismiss current modal view controller.
 *
 * @private
 */
- (void)dismissModalViewController;

/**
 * Releases all graphic elements contained on this view controller
 *
 * @private
 */
- (void)releaseGraphicElements;

@end

@implementation POIFilterViewController

#pragma mark -
#pragma mark Properties

@synthesize tableView = tableView_;
@synthesize acceptButton = acceptButton_;
@synthesize brandingImageView = brandingImageView_;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [tableView_ release];
    tableView_ = nil;
    
    [acceptButton_ release];
    acceptButton_ = nil;
    
    [brandingImageView_ release];
    brandingImageView_ = nil;
    
    [cellsStatusDictionary release];
    cellsStatusDictionary = nil;
    
    delegate_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases all graphic elements contained on this view controller
 */
- (void)releaseGraphicElements {
    
    [tableView_ release];
    tableView_ = nil;
    
    [acceptButton_ release];
    acceptButton_ = nil;
    
    [brandingImageView_ release];
    brandingImageView_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    // Initialize dictionary with "NO" value to all elements.
    cellsStatusDictionary = [[NSMutableDictionary alloc] init];
    
    // When start FilterMapViewController and no filter applied, disable all cells.
    [self enableAllATMTypes:NO];
    [self enableAllAgentsType:NO];
    
}

/*
 * Creates and returns an autoreleased POIFilterViewController constructed from a NIB file.
 */
+ (POIFilterViewController *)poiFilterViewController {
    
    POIFilterViewController *result =  [[[POIFilterViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Stylized views.
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    [NXT_Peru_iPhoneStyler styleNavigationBar:[[self navigationController] navigationBar]];
    [NXT_Peru_iPhoneStyler styleBlueButton:acceptButton_];
    [NXT_Peru_iPhoneStyler styleTableView:tableView_];
    
    // Set titles, images and separators to views.
    [acceptButton_ setTitle:NSLocalizedString(OK_TEXT_KEY, nil) forState:UIControlStateNormal];
    [brandingImageView_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    [tableView_ setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    // Set right bar button and add close button.
    [[self navigationItem] setRightBarButtonItem:[[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(CLOSE_TEXT_KEY, nil) style:UIBarButtonItemStyleBordered target:self action:@selector(dismissModalViewController)] autorelease]];
    
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseGraphicElements];
        
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated: If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
	
	[self setFilterOptionsValue:[[POILocationSession getInstance] poiFilter]];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
    [self.navigationController navigationBar].barTintColor=[UIColor BBVABlueColor];
    }
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view.
 *
 * @param animated: If YES, the disappearance of the view is being animated.
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
}

/**
 * Notifies the view controller that its view was added to a view hierarchy.
 *
 * @param animated: If YES, the view was added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
	
	// Reset tableview to reload with saved data.
	numberOfRows_ = 2;
	[tableView_ reloadData];
	
	// If the status of switch parent is ON, expand the cells.
	if (atmIsExpanded_) {
		
		NXTSwitchCell *cell = (NXTSwitchCell *)[tableView_ cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
		
		// Expand ATM cells
		[[cell optionSwitch] setOn:YES];
		[self didSwitchCellWithIndexPath:-2 turned:YES];
		
	}
	
	if (agentsIsExpanded_) {
		
		NSInteger row = 1;
		
		if (atmIsExpanded_) {
			
			row = 5;
			
		}
		
		NXTSwitchCell *cell = (NXTSwitchCell *)[tableView_ cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
		
		// Expand ATM cells
		[[cell optionSwitch] setOn:YES];
		[self didSwitchCellWithIndexPath:-3 turned:YES];
		
	}
	
	[tableView_ reloadData];
    
}

/**
 * Notifies the view controller that its view was removed from a view hierarchy.
 *
 * @param animated: If YES, the disappearance of the view was animated.
 */
- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
}

#pragma mark -
#pragma mark FilterMapViewController selectors

/*
 * Set current status options with array.
 */
- (void)setFilterOptionsValue:(NSArray *)optionsArray {
	
	[self enableAllAgentsType:NO];
	[self enableAllATMTypes:NO];
	
	atmIsExpanded_ = NO;
	agentsIsExpanded_ = NO;
	
	[cellsStatusDictionary setObject:[NSNumber numberWithBool:NO] forKey:[NSNumber numberWithInt:-2]];
	[cellsStatusDictionary setObject:[NSNumber numberWithBool:NO] forKey:[NSNumber numberWithInt:-3]];
    
    // Through the array to check cells status previus filtered.
    for (NSNumber *number in optionsArray) {
		
		NSComparisonResult result = [number compare:[NSNumber numberWithInt:5]];
		
		if (result == NSOrderedAscending) {
			
			atmIsExpanded_ = YES;
			
			[cellsStatusDictionary setObject:[NSNumber numberWithBool:YES] forKey:[NSNumber numberWithInt:-2]];
			
		} else {
			
			agentsIsExpanded_ = YES;
			
			[cellsStatusDictionary setObject:[NSNumber numberWithBool:YES] forKey:[NSNumber numberWithInt:-3]];
			
		}
        
        [cellsStatusDictionary setObject:[NSNumber numberWithBool:YES] forKey:number];
        
    }
    
    // Reload information to show correctly switch value.
    [tableView_ reloadData];
    
}

/**
 * Dismiss current modal view controller.
 *
 * @private
 */
- (void)dismissModalViewController {
	
	[self dismissModalViewControllerAnimated:YES];
	
}

#pragma mark -
#pragma mark FilterMapViewController private selectors

/**
 * Enable all ATM types and reload data.
 *
 * @param enable: YES to enable, NO otherwise.
 * @private
 */
- (void)enableAllATMTypes:(BOOL)enable {
    
    // Set all ATM types to enable boolean value.
    [cellsStatusDictionary setObject:[NSNumber numberWithBool:enable] forKey:[NSNumber numberWithInt:atmType]];
    [cellsStatusDictionary setObject:[NSNumber numberWithBool:enable] forKey:[NSNumber numberWithInt:atmBCType]];
    [cellsStatusDictionary setObject:[NSNumber numberWithBool:enable] forKey:[NSNumber numberWithInt:atmInterbankType]];
    [cellsStatusDictionary setObject:[NSNumber numberWithBool:enable] forKey:[NSNumber numberWithInt:atmScotiabankType]];
    
}

/**
 * Enable all Agentes types and reload data.
 *
 * @param enable: YES to enable, NO otherwise.
 * @private
 */
- (void)enableAllAgentsType:(BOOL)enable {
    
    // Set all Agents types to enable boolena value.
    [cellsStatusDictionary setObject:[NSNumber numberWithBool:enable] forKey:[NSNumber numberWithInt:expressAgentType]];
    [cellsStatusDictionary setObject:[NSNumber numberWithBool:enable] forKey:[NSNumber numberWithInt:expressAgentPlusType]];
    [cellsStatusDictionary setObject:[NSNumber numberWithBool:enable] forKey:[NSNumber numberWithInt:kasnetType]];
    [cellsStatusDictionary setObject:[NSNumber numberWithBool:enable] forKey:[NSNumber numberWithInt:multifacilType]];
    
}

#pragma mark -
#pragma mark User iteraction

/*
 * When tapped accept button launch this method.
 */
- (IBAction)acceptFilterMap:(id)sender {
	
    NSArray *array = [cellsStatusDictionary allKeys];
    NSMutableArray *filteredOptions = [NSMutableArray array];
    
    // If any parent cell is not expanded, all child cells are selected.
    if (!atmIsExpanded_) {
        
        [self enableAllATMTypes:NO];
        
    }
    
    if (!agentsIsExpanded_) {
        
        [self enableAllAgentsType:NO];
        
    }
    
    // Find all true values from dictionary cells.
    for (int x = 0; x < [array count]; x++) {
        
        NSInteger key = [[array objectAtIndex:x] intValue];
        
        if (key > -1) {
            
            // If value is true, added in filtered options.
            if ([[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:key]] boolValue] == YES) {
                
                [filteredOptions addObject:[NSNumber numberWithInt:key]];
                
            }
            
        }
        
    }
	
	// When view is disappeared, save parent switch status.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSNumber numberWithBool:atmIsExpanded_] forKey:@"ATMIsExpanded"];
    [defaults setValue:[NSNumber numberWithBool:agentsIsExpanded_] forKey:@"AgentsIsExpanded"];
    [defaults synchronize];
    
    // When finish check true values, tells to delegate the filter array.
    [delegate_ didFinishFilterWithOptions:filteredOptions];
    
}

#pragma mark -
#pragma mark UITableViewDataSource protocol selectors

/**
 * Tells the data source to return the number of rows in a given section of a table view. Returns the number of actions available,
 * three in this case.
 *
 * @param tableView: The table-view object requesting this information.
 * @param section: An index number identifying a section in tableView.
 * @return The number of actions available.
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return numberOfRows_;
    
}

/**
 * Asks the data source for a cell to insert in a particular questions of the table view. A NXTSwitchCell is returned, containing
 * the action at the given question pressed.
 *
 * @param tableView: The table-view object requesting the cell.
 * @param indexPath: An index path locating a row in tableView.
 * @return An object inheriting from UITableViewCell that the table view can use for the specified row.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSString *cellId = [NXTSwitchCell cellIdentifier];
    
    NXTSwitchCell *result = (NXTSwitchCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (result == nil) {
        
        result = [NXTSwitchCell nxtSwitchCell];
        
    }
    
    NSInteger row = [indexPath row];
    
    // Check if ATM is expanded. If not expanded sum 4 rows to current row for
    // ignore ATM rows.
    if (!atmIsExpanded_ && [indexPath row] > 0) {
        
        row += 4;
        
    }
    
    NSString *textLabel = @"";
    NSInteger keyCell = -1;
    
    switch (row) {
            
        case 0:
            
            keyCell = -2;
            
            textLabel = NSLocalizedString(ATM_POI_MAP_KEY, nil);
            
            [[result imageView] setImage:nil];
            
            break;
            
        case 1:
            
            keyCell = atmType;
			
            textLabel = NSLocalizedString(BBVA_CONTINNENTAL_TEXT_KEY, nil);
            
            [[result imageView] setImage:[[ImagesCache getInstance] imageNamed:MAP_FILTER_BBVA_CONTINENTAL_IMAGE_FILE]];
            
            break;
            
        case 2:
            
            keyCell = atmBCType;
            
            textLabel = NSLocalizedString(BBVA_BCP_KEY, nil);
            
            [[result imageView] setImage:[[ImagesCache getInstance] imageNamed:MAP_FILTER_BCP_IMAGE_FILE]];
            
            break;
            
        case 3:
            
            keyCell = atmInterbankType;
            
            textLabel = NSLocalizedString(BBVA_INTERBANK_KEY, nil);
			
            [[result imageView] setImage:[[ImagesCache getInstance] imageNamed:MAP_FILTER_INTERBANK_GLOBALNET_IMAGE_FILE]];
            
            break;
            
        case 4:
            
            keyCell = atmScotiabankType;
            
            textLabel = NSLocalizedString(BBVA_SCOTIA_KEY, nil);
            
            [[result imageView] setImage:[[ImagesCache getInstance] imageNamed:MAP_FILTER_SCOTIABANK_IMAGE_FILE]];
            
            break;
            
        case 5:
            
            keyCell = -3;
            
            textLabel = NSLocalizedString(AGENTS_POI_MAP_KEY, nil);
            
            [[result imageView] setImage:nil];
            
            break;
            
        case 6:
            
            keyCell = expressAgentType;
            
            textLabel = NSLocalizedString(BBVA_AGENT_KEY, nil);
			
            [[result imageView] setImage:[[ImagesCache getInstance] imageNamed:MAP_FILTER_EXPRESS_AGENT_IMAGE_FILE]];
            
            break;
            
        case 7:
            
            keyCell = expressAgentPlusType;
            
            textLabel = NSLocalizedString(BBVA_AGENT_PLUS_KEY, nil);
            
            [[result imageView] setImage:[[ImagesCache getInstance] imageNamed:MAP_FILTER_EXPRESS_AGENT_PLUS_IMAGE_FILE]];
            
            break;
            
        case 8:
            
            keyCell = kasnetType;
            
            textLabel = NSLocalizedString(BBVA_KASNET_KEY, nil);
            
            [[result imageView] setImage:[[ImagesCache getInstance] imageNamed:MAP_FILTER_KASNET_IMAGE_FILE]];
            
            break;
            
        case 9:
            
            keyCell = multifacilType;
            
            textLabel = NSLocalizedString(BBVA_MULTIFACIL_KEY, nil);
			
            [[result imageView] setImage:[[ImagesCache getInstance] imageNamed:MAP_FILTER_MULTIFACIL_IMAGE_FILE]];
            
            break;
            
        default:
            
            break;
            
    }
	
    // Set data of cell by information of switch
    [result setDelegate:self withKey:keyCell];
    [[result textLabel] setText:textLabel];
    [[result optionSwitch] setOn:[[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:keyCell]] boolValue]];
    
    return result;
    
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 *
 * @param tableView: The table-view object requesting this information.
 * @param indexPath: An index path that locates a row in tableView.
 * @return A floating-point value that specifies the height (in points) that row should be.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [NXTSwitchCell cellHeight];
    
}

#pragma mark -
#pragma mark NXTSwitchCellDelegate protocol selectors

/*
 * Tells the delegate that the specified cell is change switch.
 */
- (void)didSwitchCellWithIndexPath:(NSInteger)keyCell turned:(BOOL)isTurnedOn {
    
    if (keyCell != -1) {
        
		// Set status of general switch selected.
        [cellsStatusDictionary setObject:[NSNumber numberWithBool:isTurnedOn] forKey:[NSNumber numberWithInt:keyCell]];
        
        BOOL expand = NO;
        
        // If switch selected is -2 (ATM cell) this needs to be expanded.
        // Else if -3 (Agents cell) this needs to be expanded.
        if (keyCell == -2) {
            
            atmIsExpanded_ = isTurnedOn;
            
            expand = YES;
            
            // When expand ATM, and all cells are disabled, should be enabled.
            if (![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:atmType]] boolValue] &&
                ![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:atmBCType]] boolValue] && 
                ![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:atmInterbankType]] boolValue] &&
                ![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:atmScotiabankType]] boolValue]) {
                
                [self enableAllATMTypes:YES];   
                
            }
            
        } else if (keyCell == -3) {
            
            agentsIsExpanded_ = isTurnedOn;
            
            expand = YES;
            
            // When expand ATM, and all cells are disabled, should be enabled.
            if (![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:expressAgentType]] boolValue] &&
                ![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:expressAgentPlusType]] boolValue] &&
                ![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:kasnetType]] boolValue] &&
                ![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:multifacilType]] boolValue]) {
                
                [self enableAllAgentsType:YES];
                
            }
            
        }
        
        // If cell is expanded, add new rows to table view.
        if (expand) {
            
            NSInteger row;
            
            if (keyCell == -2) {
                
                row = 0;
                
            } else {
                
                // If the atm cell is expanded the row is 1, row 5 otherwise.
                if (!atmIsExpanded_) {
                    
                    row = 1;
                    
                } else {
                    
                    row = 5;
                    
                }
                
            }
            
            // Sections to insert new rows in table view.
            NSArray *indexPathArray = [NSArray arrayWithObjects:[NSIndexPath indexPathForRow:row + 1 inSection:0],
                                       [NSIndexPath indexPathForRow:row + 2 inSection:0],
                                       [NSIndexPath indexPathForRow:row + 3 inSection:0],
                                       [NSIndexPath indexPathForRow:row + 4 inSection:0],
                                       nil];
            
            // Insert or delete rows depending if cell is enabled or no.
            if (isTurnedOn) {
                
                numberOfRows_ += 4;
                
                [tableView_ insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationBottom];
                
            } else {
                
                numberOfRows_ -= 4;
                
                [tableView_ deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationTop];
                
            }
            
        } else {
            
            // If is not expanded, check if is last switch enabled.
            if (keyCell < expressAgentType) {
                
                // If all ATM types are disabled, the parent ATM switch to off.
                if (![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:atmType]] boolValue] &&
                    ![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:atmBCType]] boolValue] && 
                    ![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:atmInterbankType]] boolValue] &&
                    ![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:atmScotiabankType]] boolValue]) {
                    
                    NXTSwitchCell *cell = (NXTSwitchCell *)[tableView_ cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    
                    // Set parent switch to disabled.
                    [[cell optionSwitch] setOn:NO];
                    [self didSwitchCellWithIndexPath:-2 turned:NO];
                    
                }
                
            } else {
                
                // If all Agents types are disabled, the parent switch to off.
                if (![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:expressAgentType]] boolValue] &&
                    ![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:expressAgentPlusType]] boolValue] &&
                    ![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:kasnetType]] boolValue] &&
                    ![[cellsStatusDictionary objectForKey:[NSNumber numberWithInt:multifacilType]] boolValue]) {
                    
                    NSInteger row;
                    
                    // If the atm cell is expanded the row is 1, row 5 otherwise.
                    if (!atmIsExpanded_) {
                        
                        row = 1;
                        
                    } else {
                        
                        row = 5;
                        
                    }
                    
                    NXTSwitchCell *cell = (NXTSwitchCell *)[tableView_ cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
                    
                    // Set parent switch to disabled.
                    [[cell optionSwitch] setOn:NO];
                    [self didSwitchCellWithIndexPath:-3 turned:NO];                    
                    
                }
                
            }
            
        }
        
    }
    
}

#pragma mark - 
#pragma mark UINavigationBar

/**
 * The navigation item used to represent the view controller. (read-only)
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [super customNavigationItem];
    
    [[result customTitleView] setTopLabelText:NSLocalizedString(MAP_FILTER_MAP_KEY, nil)];
    
    return result;
    
}

@end
