/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>
#import "NXTViewController.h"
#import "MapRouteDirectionsDataDownloadClient.h"

// Forward declarations
@class ATMData;
@class RouteStepsViewController;
@class Location;
@class GoogleMapsDirectionsResponse;

/**
 * View controller to display an ATM of branch detail
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface POIDetailsATMBranchViewController : NXTViewController <UITableViewDelegate, UITableViewDataSource, MapRouteDirectionsDataDownloadClientDelegate> {
@private
    
    /**
     * Brand image view
     */
    UIImageView *brandImageView_;
    
    /**
     * The poi icon.
     */
    UIImageView *poiIconImageView_;
    
    /**
     * ATM or branch name or title
     */
    UILabel *streetLabel_;
    
    /**
     * Label that describe distance to poi.
     */
    UILabel *distanceLabel_;
    
    /**
     * TableView to show details whith ATM or Branch
     */
    UITableView *detailsTableView_;
    
    /**
     * Current route information being displayed
     */
    GoogleMapsDirectionsResponse *routeFoodInformation_;
    
    /**
     * Current route information being displayed
     */
    GoogleMapsDirectionsResponse *routeCarInformation_;
    
    /**
     * Route steps view controller
     */
    RouteStepsViewController *routeStepsViewController_;
    
    /**
     * The download client to directions.
     */
    MapRouteDirectionsDataDownloadClient *routeFoodDownloadClient_;
    
    /**
     * The download client to directions.
     */
    MapRouteDirectionsDataDownloadClient *routeCarDownloadClient_;
    
    /**
     * ATM or Branch data
     */
    ATMData *poiData_;
    
    /**
     * ATM flag. YES when POI data is an ATM, NO when it is a branch or Agent.
     */
    BOOL isATM_;
	
	/**
	 * Agent flag. YES when POI data is an Agent, NO when it is branch or ATM.
	 */
	BOOL isAgent_;
    
    /**
     * Flag to know if is a first time and indicate if clear interface.
     */
    BOOL shouldClearInterfaceData_;
	
	/**
	 * Flag to hide food info indicator.
	 */
	BOOL foodInfoDownloaded_;
	
	/**
	 * Flag to hide car info indicator.
	 */
	BOOL carInfoDownloaded_;
    
    /**
     * The destination coordinate.
     */
    Location *destinationCoordinate_;
    
}

/**
 * Provides read-write access to the brand image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandImageView;

/**
 * Provides read-write access to poiIconImageView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *poiIconImageView;

/**
 * Provides read-write access to the ATM or Branch name or title and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *streetLabel;

/**
 * Provides read-write access to distanceLabel. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *distanceLabel;

/**
 * Provides read-write access to the show table view details and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *detailsTableView;

/**
 * Provides read-write access to shouldClearInterfaceData.
 */
@property (nonatomic, readwrite, assign) BOOL shouldClearInterfaceData;

/**
 * Creates and return an autoreleased POIDetailsATMBranchViewController constructed from a NIB file
 *
 * @return The autoreleased POIDetialsATMBranchViewController instance
 */
+ (POIDetailsATMBranchViewController *)poiDetailsATMBranchViewController;

/**
 * Set ATMData to show details in this view
 *
 * @param poiData ATMData that contains the ATM data
 * @param agent YES when ATM is an agent, NO otherwise
 */
- (void)setATMData:(ATMData *)poiData isAgent:(BOOL)agent;

/**
 * Set BranchData to show details in this view
 *
 * @param poiData ATMData that contains the branch data
 */
- (void)setBranchData:(ATMData *)poiData;

@end
