/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MapRouteViewController.h"
#import "MapRouteViewController+protected.h"
#import "GoogleMapsDirectionsResponse.h"
#import "GoogleMapsRoute.h"
#import "MapWarningsViewController.h"


#pragma mark -

@implementation MapRouteViewController(protected)

#pragma mark -
#pragma mark Route management

/*
 * Displays the route copyright and warnings information
 */
- (void)displayRouteCopyrightAndWarnings {
    
    if (routeInformation_ != nil) {

        NSArray *routes = routeInformation_.routes;
        
        if ([routes count] > 0) {
            
            GoogleMapsRoute *route = [routes objectAtIndex:0];
            MapWarningsViewController *mapWarningsViewController = [MapWarningsViewController mapWarningsViewController];
            mapWarningsViewController.copyrightText = route.copyrights;
            mapWarningsViewController.warningsArray = route.warnings;
            [mapWarningsViewController displayModal];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Checks whether the warnings button can be displayed.
 */
- (void)checkWarningButtonMustBeDisplayed {
    
    UIDevice *device = [UIDevice currentDevice];
    NSString *version = [device systemVersion];
    
    BOOL displayWarningButton = YES;
    
    if ([version compare:@"6.0"
                 options:NSNumericSearch] != NSOrderedAscending) {
        
        displayWarningButton = NO;
        
    }
    
    warningsButton_.hidden = ((routeInformation_ == nil) || (!displayWarningButton));

}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the current route information being displayed
 *
 * @return The current route information being displayed
 */
- (GoogleMapsDirectionsResponse *)routeInformation {
    
    return routeInformation_;
    
}

/*
 * Sets the current route information and displays it on the map
 *
 * @param aRouteInformation The new route information to display
 */
- (void)setRouteInformation:(GoogleMapsDirectionsResponse *)aRouteInformation {
    
    [self cancelActiveObtainRouteProcess];
    
    if (aRouteInformation != routeInformation_) {
        
        [routeInformation_ release];
        routeInformation_ = nil;
        routeInformation_ = [aRouteInformation retain];
        
        [self setRouteElements];
        
    }
    
    [self checkWarningButtonMustBeDisplayed];
    
}

@end
