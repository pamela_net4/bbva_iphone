/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "POIListEnRouteViewController.h"

//Forward declarations
@class RouteStepView;
@class GoogleMapsRoute;
@class Location;
@class BaseAnnotation;
@class OriginAnnotation;
@class DestinationAnnotation;
@class ATMData;
@class POIAnnotation;

/**
 * View controller to display a route and select between on foot and by car routes. It also displays the route steps (inherited from its superclass)
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface RouteStepsViewController : POIListEnRouteViewController {
    
@private
    
    /**
     * Toolbar action button. It allows to toggle from step view to change travel mode state
     */
    UIBarButtonItem *actionBarButtonItem_;
    
    /**
     * POI to obtain the route to
     */
    ATMData *poi_;
    
}

/**
 * Creates and returns an autoreleased RouteStepsViewController constructed from a NIB file
 *
 * @return The autoreleased RouteStepsViewController constructed from a NIB file
 */
+ (RouteStepsViewController *)routeStepsViewController;


/**
 * Sets the route origin and destinaton, and start the route download
 *
 * @param routeOrigin The route origin location
 * @param routeDestination The route destination location
 * @param poi The associated route POI (if any)
 * @param carRoute YES when the route is a car route, NO otherwise
 */
- (void)setRouteOrigin:(Location *)routeOrigin
      routeDestination:(Location *)routeDestination
         associatedPOI:(ATMData *)poi 
            byRouteCar:(BOOL)carRoute;

@end
