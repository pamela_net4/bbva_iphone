/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTViewController.h"

#import "POIFilterViewController.h"

@class ATMList;
@class BranchesList;
@class Location;
@class POIDetailsATMBranchViewController;

/**
 * View Controller to show all POI's in a table view.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface POIListViewController : NXTViewController <UITableViewDelegate, UITableViewDataSource, POIFilterViewControllerDelegate, UISearchBarDelegate> {
    
@private
    
    /**
     * Table view that contains all POI's
     */
    UITableView *tableView_;
    
    /**
     * A toolbar view to put segmented control.
     */
    UIView *toolbarView_;
    
    /**
     * A segmented control.
     */
    UISegmentedControl *segmentedControl_;
	
	/**
	 * The separator image view.
	 */
	UIImageView *separatorImageView_;
    
    /**
     * The branding image view.
     */
    UIImageView *brandingImageView_;
	
	/**
     * Search bar
     */
    UISearchBar *searchBar_;
    
    /**
     * The index by segment selected.
     */
    NSUInteger segmentedIndex_;
	
	/**
     * Search text
     */
    NSString *searchText_;
	
	/**
	 * The array that contain all pois of segment tab.
	 */
	NSMutableArray *poisArray_;
    
    /**
     * Show detail information to ATM or Branch
     */
    POIDetailsATMBranchViewController *detailATMBranchViewController_;
	
	/**
	 * Poi filter view controller.
	 */
	POIFilterViewController *poiFilterViewController_;
    
}

/**
 * Provides read-write access to tableView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *tableView;

/**
 * Provides read-write access to toolbarView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *toolbarView;

/**
 * Provides read-write access to segmentedControl. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UISegmentedControl *segmentedControl;

/**
 * Provides read-write access to separatorImageView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separatorImageView;

/**
 * Provides read-write access to brandingImageView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;

/**
 * Provides read-write access to searchBar. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UISearchBar *searchBar;

/**
 * Creates and returns an autoreleased BranchesAndATMViewController constructed from a NIB file.
 *
 * @return The autoreleased BranchesAndATMViewController constructed from a NIB file.
 */
+ (POIListViewController *)poiListViewController;

/**
 * This event is throwed when the segmented value is changed.
 */
- (IBAction)segmentedValueChanged:(id)sender;

@end
