/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ModalViewControllerBase.h"


/**
 * View controller to display the map route warnings and copyright information
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MapWarningsViewController : ModalViewControllerBase <UITableViewDelegate, UITableViewDataSource> {

@private
    
    /**
     * View to contain all visible elements
     */
    UIView *backgroundView_;
    
    /**
     * Background image view
     */
    UIImageView *backgroundImageView_;
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Copyright information label
     */
    UILabel *copyrightLabel_;
    
    /**
     * Warnings table
     */
    UITableView *warningsTable_;
    
    /**
     * Accept button
     */
    UIButton *acceptButton_;
    
    /**
     * Copyright information text
     */
    NSString *copyrightText_;
    
    /**
     * Warnings array to display
     */
    NSMutableArray *warningsArray_;
    
}


/**
 * Provides read-write access to the view to contain all visible elements and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *backgroundView;

/**
 * Provides read-write access to the background image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *backgroundImageView;

/**
 * Provides read-write access to the title label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides read-write access to the copyright information label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *copyrightLabel;

/**
 * Provides read-write access to the warnings table and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *warningsTable;

/**
 * Provides read-write access to the accept button and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *acceptButton;

/**
 * Provides read-write access to the copyright information text
 */
@property (nonatomic, readwrite, copy) NSString *copyrightText;

/**
 * Provides read-write access to the warnings array
 */
@property (nonatomic, readwrite, copy) NSArray *warningsArray;


/**
 * Creates and returns an autoreleased MapWarningsViewController constructed from a NIB file
 *
 * @return The autoreleased MapWarningsViewController constructed from a NIB file
 */
+ (MapWarningsViewController *)mapWarningsViewController;

/**
 * Invoked by framework when the accept button is tapped. The view controller is dismissed
 */
- (IBAction)acceptButtonTapped;

@end
