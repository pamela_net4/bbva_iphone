/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MapRouteViewController.h"
#import "MapRouteViewController+protected.h"
#import "StringKeys.h"
#import "Tools.h"
#import "GoogleMapsConstants.h"
#import "GoogleMapsApiInvoker.h"
#import "GoogleMapsDirectionsDataDownloadClient.h"
#import "GoogleMapsDirectionsResponse.h"
#import "GoogleMapsPolyline.h"
#import "GoogleMapsRoute.h"
#import "MapWarningsViewController.h"
#import "MKRouteAnnotation.h"
#import "MKRouteAnnotationView.h"
#import "NXT_Peru_iPhone_AppDelegate.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

/**
 * Defines the MapRouteViewController NIB file name
 */
#define MAP_ROUTE_VIEW_CONTROLLER_NIB_FILE                                      @"MapRouteViewController"

/**
 * Class name to test to use polylines or annotations for the route representation
 */
#define ROUTE_TYPE_CLASS_NAME_TEST                                              @"MKPolyline"

#pragma mark -

/**
 * MapRouteViewController private extension
 */
@interface MapRouteViewController()

/**
 * Releases the graphic elements not needed when view is hidden
 *
 * @private
 */
- (void)releaseMapRouteViewControllerGraphicElements;

/**
 * Calculates route annotation new position
 *
 * @private
 */
- (void)relocateRouteAnnotation;

/**
 * Calculates route annotation new position
 *
 * @private
 */
- (void)relocateRouteAnnotation;

@end


#pragma mark -

@implementation MapRouteViewController

#pragma mark -
#pragma mark Properties

@synthesize mapView = mapView_;
@synthesize warningsButton = warningsButton_;
@dynamic northEastPoint;
@dynamic southWestPoint;
@dynamic downloadingRoute;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [self releaseMapRouteViewControllerGraphicElements];
    
    [routeInformation_ release];
    routeInformation_ = nil;
    
    [routeDataDownloadClient_ cancelDownload];
    [routeDataDownloadClient_ release];
    routeDataDownloadClient_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory. Visual elements are released
 */
- (void)viewDidUnload {
    
    [self releaseMapRouteViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/*
 * Releases the graphic elements not needed when view is hidden
 */
- (void)releaseMapRouteViewControllerGraphicElements {
    
    [mapView_ release];
    mapView_ = nil;
    
    [warningsButton_ release];
    warningsButton_ = nil;
    
    [routeAnnotation_ release];
    routeAnnotation_ = nil;
    
    [routeAnnotationView_ release];
    routeAnnotationView_ = nil;
    
    [routePolyline_ release];
    routePolyline_ = nil;
    
    [routePolylineView_ release];
    routePolylineView_ = nil;
    
    [auxPolygon_ release];
    auxPolygon_ = nil;
    
    [auxTransparentPolygonView_ release];
    auxTransparentPolygonView_ = nil;
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseMapRouteViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased MapRouteViewController constructed from a NIB file
 */
+ (MapRouteViewController *)mapRouteViewController {

    MapRouteViewController *result = [[[MapRouteViewController alloc] initWithNibName:MAP_ROUTE_VIEW_CONTROLLER_NIB_FILE
                                                                               bundle:nil] autorelease];
    [result awakeFromNib];

    return result;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory
 */
- (void)viewDidLoad {
	    
    [super viewDidLoad];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    if(IS_OS_8_OR_LATER) {
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
        [self.locationManager startUpdatingLocation];
    }

    if (NSClassFromString(ROUTE_TYPE_CLASS_NAME_TEST) == nil) {
        
        if (routeAnnotation_ == nil) {
            
            routeAnnotation_ = [[MKRouteAnnotation alloc] init];
            CLLocationCoordinate2D routeCoordinate;
            routeCoordinate.latitude = 90.0f;
            routeCoordinate.longitude = 0.0f;
            routeAnnotation_.coordinate = routeCoordinate;
            
        }
        
    }
    
    [warningsButton_ addTarget:self
                        action:@selector(displayRouteCopyrightAndWarnings)
              forControlEvents:UIControlEventTouchUpInside];
    
    CLLocationDegrees north = 48.f;
    CLLocationDegrees west = -125.0f;
    CLLocationDegrees south = 24.0f;
    CLLocationDegrees east = -65.0f;
    
    MKCoordinateRegion coordinateRegion;
    coordinateRegion.center.latitude = (north + south) / 2.0f;
    coordinateRegion.center.longitude = (west + east) / 2.0f;
    coordinateRegion.span.latitudeDelta = fabs(north - south);
    coordinateRegion.span.longitudeDelta = fabs(east - west);

    if(coordinateRegion.center.latitude <= 90 && coordinateRegion.center.latitude >= -90 &&
       coordinateRegion.center.longitude >= -180 && coordinateRegion.center.longitude <= 180){
    
    MKCoordinateRegion fitRegion = [mapView_ regionThatFits:coordinateRegion];
    [mapView_ setRegion:fitRegion
               animated:NO];
    }
    [self checkWarningButtonMustBeDisplayed];

}

/**
 * Notifies the view controller that its view is about to be become visible. The route elements are displayed in the map
 *
 * @param animated If YES, the view is being added to the window using an animation
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self setRouteElements];
    
}

#pragma mark -
#pragma mark Route and annotations management

/*
 * Calculates route annotation new position
 */
- (void)relocateRouteAnnotation {
    
    [routeAnnotationView_ updateRoute];
    
}

/*
 * Sets route elements. In iOS 3.x, an MKRouteAnnotation is created, whereas in iOS 4.x, an MKPolyline instance is created. In both cases
 * the information stored in routeInformation_ is used to contruct the elements
 */
- (void)setRouteElements {
    
    NSArray *routes = routeInformation_.routes;
    GoogleMapsRoute *route = nil;
    GoogleMapsPolyline *polyline = nil;
    NSArray *polylinePoints = nil;
    
    if ([routes count] > 0) {
        
        route = [routes objectAtIndex:0];
        polyline = route.detailledPolyline;
        polylinePoints = polyline.polylineArray;
        
    }
    
    if (NSClassFromString(ROUTE_TYPE_CLASS_NAME_TEST) != nil) {
        
        if (routePolyline_ != nil) {
            
            [mapView_ removeOverlay:routePolyline_];
            [routePolyline_ release];
            routePolyline_ = nil;
            
        }
        
        if (routePolylineView_ != nil) {
            
            [routePolylineView_ release];
            routePolylineView_ = nil;
            
        }
        
        if (polylinePoints != nil) {
            
            NSUInteger pointsCount = [polylinePoints count];
            CLLocationCoordinate2D *routeCoordinates = (CLLocationCoordinate2D *)malloc(sizeof(CLLocationCoordinate2D) * pointsCount);
            NSValue *coordinateValue;
            CLLocationCoordinate2D coordinate;
            
            for (NSUInteger i = 0; i < pointsCount; i++) {
                
                coordinateValue = [polylinePoints objectAtIndex:i];
                [coordinateValue getValue:&coordinate];
                routeCoordinates[i] = coordinate;
                
            }
            
            routePolyline_ = [[MKPolyline polylineWithCoordinates:routeCoordinates count:pointsCount] retain];
            free(routeCoordinates);
            
            routePolylineView_ = [[MKPolylineView alloc] initWithPolyline:routePolyline_];
            routePolylineView_.strokeColor = [UIColor colorWithRed:0.0f green:0.0f blue:1.0f alpha:0.5f];
            routePolylineView_.lineWidth = 5.0f;
            
            [mapView_ addOverlay:routePolyline_];
            
        }
        
        if (auxPolygon_ == nil) {
            
            auxPolygon_ = [[MKPolygon alloc] init];
            [mapView_ addOverlay:auxPolygon_];
            
        }
        
    } else {
        
        routeAnnotation_.polyline = polyline;
        CLLocationCoordinate2D routeAnnotationCoordinate;
        routeAnnotationCoordinate.latitude = routeAnnotation_.northernMostCoordinate;
        routeAnnotationCoordinate.longitude = routeAnnotation_.westernMostCoordinate;
        routeAnnotation_.coordinate = routeAnnotationCoordinate;
        [mapView_ addAnnotation:routeAnnotation_];
        [self relocateRouteAnnotation];
        [routeAnnotationView_ updateRoute];
        routeAnnotationView_.hidden = NO;
        
    }
    
}

#pragma mark -
#pragma mark Obtain route process management

/*
 * Starts process to the obtains a route from an origin coordinate to a destination coordinate. There must be only
 * one active route process
 */
- (BOOL)obtainRouteFromOrigin:(CLLocationCoordinate2D)anOriginCoordinate
                toDestination:(CLLocationCoordinate2D)aDestinationCoordinate
                        byCar:(BOOL)byCarFlag
           obtainedWithSensor:(BOOL)sensorFlag {
    
    BOOL result = NO;
    
    if (routeDataDownloadClient_ == nil) {
        
        NSLocale *locale = [NSLocale currentLocale];
        NSNumber *metricSystem = [locale objectForKey:NSLocaleUsesMetricSystem];
        NSString *languageCode = NSLocalizedString(GOOGLE_MAPS_DIRECTIONS_API_LANGUAGE_CODE_KEY, nil);

        routeDataDownloadClient_ = [[MapRouteDirectionsDataDownloadClient alloc] initWithOriginPoint:anOriginCoordinate
                                                                                    destinationPoint:aDestinationCoordinate
                                                                                            language:languageCode
                                                                                          routeByCar:byCarFlag
                                                                                        metricSystem:[metricSystem boolValue]
                                                        mapRouteDirectionsDataDownloadClientDelegate:self];
        
        result = [GoogleMapsApiInvoker routeForDirectionClient:routeDataDownloadClient_
                                            obtainedFromSensor:sensorFlag];
        
        if (!result) {
        
            [routeDataDownloadClient_ cancelDownload];
            [routeDataDownloadClient_ release];
            routeDataDownloadClient_ = nil;
            
            [Tools showErrorWithMessage:NSLocalizedString(UNABLE_TO_OBTAIN_ROUTE_ERROR_KEY, nil)];
            
        } else {
            
            [self.appDelegate showActivityIndicator:poai_Both];
            
        }
        
    }
    
    return result;
    
}

/*
 * Checks whether there is an obtain route process active
 */
- (BOOL)isObtainRouteProcessActive {
    
    return (routeDataDownloadClient_ != nil);
    
}

/*
 * Cancels the current active obtain route process, if any
 */
- (void)cancelActiveObtainRouteProcess {
    
    [routeDataDownloadClient_ cancelDownload];
    [routeDataDownloadClient_ release];
    routeDataDownloadClient_ = nil;
    
}

#pragma mark -
#pragma mark MapRouteDirectionsDataDownloadClientDelegate protocol selectors

/*
 * The GoogleMaps directions API response was parsed correcty. The response is analyze to obtain the route information information and display it
 */
- (void)analyzeRouteResponse:(GoogleMapsDirectionsResponse *)aGoogleMapsDirectionsResponse {
    
    self.routeInformation = aGoogleMapsDirectionsResponse;
    
}

/*
 * Invoked when the GoogleMaps directions API information was downloaded and parsed successfully. The information is analyzed
 * to create the new route
 */
- (void)routeInformationParsed:(MapRouteDirectionsDataDownloadClient *)aRouteDirectionsDataDownloadClient {
    
    if (aRouteDirectionsDataDownloadClient == routeDataDownloadClient_) {
        
        GoogleMapsDirectionsResponse *routeResponse = routeDataDownloadClient_.directionsResponse;
        NSString *responseCode = routeResponse.status;
        
        if ((![responseCode isEqualToString:GOOGLE_MAPS_OPERATION_OK_STATUS]) || ([routeResponse.routes count] == 0)) {
            
            NSMutableString *errorString = [NSMutableString stringWithString:NSLocalizedString(UNABLE_TO_OBTAIN_ROUTE_ERROR_KEY, nil)];
            
            [Tools showErrorWithMessage:errorString];
            
        } else {
            
            [self analyzeRouteResponse:routeResponse];
            
        }
        
        [routeDataDownloadClient_ cancelDownload];
        [routeDataDownloadClient_ release];
        routeDataDownloadClient_ = nil;
        
        [self.appDelegate hideActivityIndicator];
        
    }
    
}

/*
 * The route parsing process finished in error. The user is notified about the situation
 */
- (void)routeParsingProcessError:(NSError *)anError {
    
    [routeDataDownloadClient_ cancelDownload];
    [routeDataDownloadClient_ release];
    routeDataDownloadClient_ = nil;
    
    NSMutableString *errorString = [NSMutableString stringWithString:NSLocalizedString(UNABLE_TO_OBTAIN_ROUTE_ERROR_KEY, nil)];
    
    [Tools showErrorWithMessage:errorString];
    
    [self.appDelegate hideActivityIndicator];

}

/*
 * The route download process finished in error. The user is notified about the situation
 */
- (void)routeDownloadProcessError:(NSError *)anError {
    
    [self routeParsingProcessError:anError];
    
    [self.appDelegate hideActivityIndicator];

}

/*
 * The route download process was cancelled
 */
- (void)routeDownloadCancelled {
    
    [routeDataDownloadClient_ cancelDownload];
    [routeDataDownloadClient_ release];
    routeDataDownloadClient_ = nil;
    
    [self.appDelegate hideActivityIndicator];

}

#pragma mark -
#pragma mark MKMapViewDelegate methods

/**
 * Tells the delegate that the region displayed by the map view just changed. Locates the route annotation
 *
 * @param mapView The map view whose visible region changed
 * @param animated If YES, the change to the new region was animated
 */
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    if (routeAnnotation_ != nil) {
        
        [mapView_ addAnnotation:routeAnnotation_];
        
    }
    
}

/**
 * Tells the delegate that the region displayed by the map view is about to change. The route view is hidden
 *
 * @param mapView The map view whose visible region is about to change
 * @param animated If YES, the change to the new region will be animated. If NO, the change will be made immediately
 */
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    
    if (routeAnnotation_ != nil) {
        
        [mapView_ removeAnnotation:routeAnnotation_];
        
    }
    
}

/**
 * Returns the view associated with the specified annotation object. When annotation is an MKRouteAnnotation, an MKRouteAnnotationView is returned
 *
 * @param mapView The map view that requested the annotation view
 * @param annotation The object representing the annotation that is about to be displayed
 * @return The annotation view to display for the specified annotation or nil if you want to display a standard annotation view
 */
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    MKAnnotationView *result = nil;
    NSObject *annotationObject = (NSObject *)annotation;
    
    if ([annotationObject isEqual:routeAnnotation_]) {
        
        static NSString *viewIdentifier = @"routeAnnotationView";
        
        if (routeAnnotationView_ == nil) {
            routeAnnotationView_ = [[MKRouteAnnotationView alloc] initWithAnnotation:routeAnnotation_ reuseIdentifier:viewIdentifier];
            routeAnnotationView_.mapView = mapView_;
            
        } else {
            
            [mapView_ dequeueReusableAnnotationViewWithIdentifier:viewIdentifier];
            
        }
        
        result = routeAnnotationView_;
        
    }
    
    return result;
    
}

/**
 * Asks the delegate for the overlay view to use when displaying the specified overlay object. Returns the polyline view for the route
 *
 * @param mapView The map view that requested the overlay view
 * @param overlay The object representing the overlay that is about to be displayed
 * @return The view to use when presenting the specified overlay on the map
 */
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    
    MKOverlayView *result = nil;
    
    if (overlay == routePolyline_) {
        
        result = routePolylineView_;
        
    } else if (overlay == auxPolygon_) {
        
        if (auxTransparentPolygonView_ == nil) {
            
            auxTransparentPolygonView_ = [[MKPolygonView alloc] initWithPolygon:overlay];
            auxTransparentPolygonView_.alpha = 0.0f;
            
        }
        
        result = auxTransparentPolygonView_;
        
    }
    
    return result;
    
}

/**
 * Tells the delegate that one or more annotation views were added to the map. The route annotation view location is updated
 *
 * @param mapView The map view that added the annotation views
 * @param views An array of MKAnnotationView objects representing the views that were added
 */
- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    
    if ([views containsObject:routeAnnotationView_]) {
        
        [self relocateRouteAnnotation];
        
    }
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the route north-east point
 *
 * @return The route north-east point
 */
- (CLLocationCoordinate2D)northEastPoint {
    
    CLLocationDegrees north = 0.0f;
    CLLocationDegrees east = 0.0f;
    
    NSArray *routes = routeInformation_.routes;
    GoogleMapsRoute *route = nil;
    GoogleMapsPolyline *polyline = nil;
    NSArray *polylinePoints = nil;
    
    if ([routes count] > 0) {
        
        route = [routes objectAtIndex:0];
        polyline = route.detailledPolyline;
        polylinePoints = polyline.polylineArray;
        
        if (polylinePoints != nil) {
            
            north = -90.0f;
            east = -180.0f;
            
            NSUInteger pointsCount = [polylinePoints count];
            NSValue *coordinateValue;
            CLLocationCoordinate2D coordinate;
            
            for (NSUInteger i = 0; i < pointsCount; i++) {
                
                coordinateValue = [polylinePoints objectAtIndex:i];
                [coordinateValue getValue:&coordinate];
                
                if (coordinate.latitude > north) {
                    
                    north = coordinate.latitude;
                    
                }
                
                if (coordinate.longitude > east) {
                    
                    east = coordinate.longitude;
                    
                }
                
            }
            
        }
        
    }
    
    CLLocationCoordinate2D result;
    result.latitude = north;
    result.longitude = east;
    
    return result;
    
}

/*
 * Returns the route south-west point
 *
 * @return The route south-west point
 */
- (CLLocationCoordinate2D)southWestPoint {
    
    CLLocationDegrees south = 0.0f;
    CLLocationDegrees west = 0.0f;
    
    NSArray *routes = routeInformation_.routes;
    GoogleMapsRoute *route = nil;
    GoogleMapsPolyline *polyline = nil;
    NSArray *polylinePoints = nil;
    
    if ([routes count] > 0) {
        
        route = [routes objectAtIndex:0];
        polyline = route.detailledPolyline;
        polylinePoints = polyline.polylineArray;
        
        if (polylinePoints != nil) {
            
            south = 90.0f;
            west = 180.0f;
            
            NSUInteger pointsCount = [polylinePoints count];
            NSValue *coordinateValue;
            CLLocationCoordinate2D coordinate;
            
            for (NSUInteger i = 0; i < pointsCount; i++) {
                
                coordinateValue = [polylinePoints objectAtIndex:i];
                [coordinateValue getValue:&coordinate];
                
                if (coordinate.latitude < south) {
                    
                    south = coordinate.latitude;
                    
                }
                
                if (coordinate.longitude < west) {
                    
                    west = coordinate.longitude;
                    
                }
                
            }
            
        }
        
    }
    
    CLLocationCoordinate2D result;
    result.latitude = south;
    result.longitude = west;
    
    return result;
    
}

/*
 * Returns the downloading route flag
 *
 * @return The downloading route flag
 */
- (BOOL)downloadingRoute {
    
    return (routeDataDownloadClient_ != nil);
    
}

#pragma mark -
#pragma mark Route displaying

/*
 * Removes and releases the annotations and the route information
 */
- (void)removeAnnotationsAndRouteInformation {
    
    if (routePolyline_ != nil) {
        
        [mapView_ removeOverlay:routePolyline_];
        [routePolyline_ release];
        routePolyline_ = nil;
        
    }
    
    if (routeAnnotation_ != nil) {
        
        [mapView_ removeAnnotation:routeAnnotation_];
        
    }
    
}

@end
