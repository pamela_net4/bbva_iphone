/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "SingletonBase.h"


//Forward declarations
@class ModalViewControllerBase;


/**
 * Singleton to manage the modal view controlers. It creates the modal window, animates appearance and
 * disappearance, and maintains a stack of pending view controllers to display
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ModalManager : SingletonBase {
    
@private
    
    /**
     * Modal view controllers window
     */
    UIWindow *modalWindow_;
    
    /**
     * Previous key window
     */
    //    UIWindow *previousKeyWindow_;
    
    /**
     * Modal view controllers pending to be displayed
     */
    NSMutableArray *pendingViewControllers_;
    
    /**
     * Current modal view controller being displayed
     */
    ModalViewControllerBase *currentModalViewController_;
    
    /**
     * Current modal view transformation
     */
    CGAffineTransform currentModalViewTransform_;
    
}

/**
 * Returns the singleton only instance
 *
 * @result The singleton only instance
 */
+ (ModalManager *)getInstance;

/**
 * Provides readonly access to the current modal view controller being displayed
 */
@property (nonatomic, readonly, retain) ModalViewControllerBase *currentModalViewController;

/**
 * Displays a view controller in the modal window. The view appearance is animated
 *
 * @param aModalViewController The view controller to display modally
 */
- (void)displayModalViewController:(ModalViewControllerBase *)aModalViewController;

/**
 * Dismisses the view controller
 *
 * @param aModalViewController The modal view controller. It must be equal to the current view
 * controller. When both view controllers are not equar, no action is performed
 */
- (void)dismissModalViewController:(ModalViewControllerBase *)aModalViewController;


/**
 * Dismisses the current controller and remove all pending.
 */
- (void)dismissAllModalViewControllers;


@end

