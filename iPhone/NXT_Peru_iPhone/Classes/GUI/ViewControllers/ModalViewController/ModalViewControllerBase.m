/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ModalViewControllerBase.h"
#import "ModalViewControllerBase+protected.h"
#import "ModalManager.h"


#pragma mark -

@implementation ModalViewControllerBase

#pragma mark -
#pragma mark Properties

@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    delegate_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Modal display management

/*
 * Displays the view controller in a modal window
 */
- (void)displayModal {
    
    ModalManager *modalManager = [ModalManager getInstance];
    
    [modalManager displayModalViewController:self];
    
}

@end


#pragma mark -

@implementation ModalViewControllerBase(protected)

#pragma mark -
#pragma mark Modal display managenet

/*
 * Dismises the modal vie controller
 */
- (void)dismissModalViewController {
    
    ModalManager *modalManager = [ModalManager getInstance];
    [modalManager dismissModalViewController:self];
    
}

@end
