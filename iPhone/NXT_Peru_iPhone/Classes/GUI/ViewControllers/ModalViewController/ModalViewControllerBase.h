/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "NXTEditableViewController.h"
#import "NXTEditableViewController+protected.h"


//Forward declarations
@class ModalViewControllerBase;


/**
 * Protocol for the ModalViewControllerDelegate. The delegate is informed when the modal view is dismissed. View controlles's view
 * background color is set to transparent when displayed, so child classes must provided another view as a background
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol ModalViewControllerDelegate

@required

/**
 * The modal view controller has been dismissed
 *
 * @param aModalViewController The modal view controller that was dismissed
 */
- (void)modalViewControllerDismissed:(ModalViewControllerBase *)aModalViewController;

@end



/**
 * Base view controller for al view controllers that must to be displayed in a modal way
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ModalViewControllerBase : NXTEditableViewController {
@protected    
    /**
     * Modal view controller delegate
     */
    id<ModalViewControllerDelegate> delegate_;
}

/**
 * Provides read-write access to the modal view controller delegate
 */
@property (nonatomic, readwrite, assign) id<ModalViewControllerDelegate> delegate;


/**
 * Displays the view controller in a modal window
 */
- (void)displayModal;

@end
