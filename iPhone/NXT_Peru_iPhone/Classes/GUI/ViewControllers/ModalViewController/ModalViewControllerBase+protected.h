/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ModalViewControllerBase.h"


/**
 * ModalViewControllerBase protected category
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ModalViewControllerBase(protected)

/**
 * Dismises the modal vie controller
 *
 * @protected
 */
- (void)dismissModalViewController;

@end
