/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ModalManager.h"
#import "ModalViewControllerBase.h"


/**
 * Defines the modal view controller appear animation name
 */
#define MODAL_VIEW_CONTROLLER_APPEAR_ANIMATION_NAME                                     @"net.movilok.modalManager.appearAnimation"

/**
 * Defines the modal view controller appear animation duration measured in seconds
 */
#define MODAL_VIEW_CONTROLLER_APPEAR_ANIMATION_DURATION                                 0.3f

/**
 * Defines the modal view controller disappear animation name
 */
#define MODAL_VIEW_CONTROLLER_DISAPPEAR_ANIMATION_NAME                                  @"net.movilok.modalManager.disappearAnimation"

/**
 * Defines the modal view controller disappear animation duration measured in seconds
 */
#define MODAL_VIEW_CONTROLLER_DISAPPEAR_ANIMATION_DURATION                              0.3f


#pragma mark -

/**
 * ModalManager private category
 */
@interface ModalManager(private)

/**
 * Displays the next modal view controller stored in the pending list
 *
 * @private
 */
- (void)displayNextModalViewController;

/**
 * Animates a view controller to be displayed. If a previous modal view controller is displayed, it
 * is removed without any animation
 *
 * @param aModalViewController The view controller to be animated
 * @private
 */
- (void)animateViewControllerToAppear:(ModalViewControllerBase *)aModalViewController;

/**
 * Invoked by framework when view controller disappear animation is stopped
 *
 * @param animationID An NSString containing an optional application-supplied identifier
 * @param finished An NSNumber object containing a Boolean value. The value is YES if the animation ran to completion before it stopped or NO if it did not
 * @param context An optional application-supplied context
 * @private
 */
- (void)viewControllerDisappearAnimationDidStop:(NSString *)animationID
                                       finished:(NSNumber *)finished
                                        context:(void *)context;

/**
 * Removes the view controller that disappeared from the window (intended to be used once the view controller disappear animation is finished)
 *
 * @private
 */
- (void)removeDisappearedViewController;

@end


#pragma mark -

@implementation ModalManager

#pragma mark -
#pragma mark Static attributes

/**
 * Singleton only instance
 */
static ModalManager *modalManagerInstance_ = nil;

#pragma mark -
#pragma mark Singleton selectors

/**
 * Allocates the singleton instance, in case it is not already allocated
 *
 * @param zone The memory zone to allocate the singleton in
 * @return The newly allocated singleton instance, or nil if singleton was already allocated
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([ModalManager class]) {
        
		if (modalManagerInstance_ == nil) {
            
			modalManagerInstance_ = [super allocWithZone:zone];
			return modalManagerInstance_;
            
		}
        
	}
	
	return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (ModalManager *)getInstance {
    
	if (modalManagerInstance_ == nil) {
        
		@synchronized ([ModalManager class]) {
            
			if (modalManagerInstance_ == nil) {
                
				modalManagerInstance_ = [[ModalManager alloc] init];
                
			}
            
		}
        
	}
	
	return modalManagerInstance_;
    
}


#pragma mark -
#pragma mark Properties

@synthesize currentModalViewController = currentModalViewController_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [modalWindow_ release];
    modalWindow_ = nil;
    
    //    [previousKeyWindow_ release];
    //    previousKeyWindow_ = nil;
    
    [pendingViewControllers_ release];
    pendingViewControllers_ = nil;
    
    [currentModalViewController_ release];
    currentModalViewController_ = nil;
    
    modalManagerInstance_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a ModalManager instance creating the necesary elements
 *
 * @return The initialized ModalManager instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        UIScreen *screen = [UIScreen mainScreen];
        CGRect frame = screen.bounds;
        frame.origin.x = 0.0f;
        frame.origin.y = 0.0f;
        modalWindow_ = [[UIWindow alloc] initWithFrame:frame];
        pendingViewControllers_ = [[NSMutableArray alloc] init];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Managing modal view controllers

/*
 * Displays a view controller in the modal window. The view appearance is animated
 */
- (void)displayModalViewController:(ModalViewControllerBase *)aModalViewController {
    
    if (currentModalViewController_ == nil) {
        
        //        [previousKeyWindow_ release];
        //        previousKeyWindow_ = nil;
        //        previousKeyWindow_ = [[[UIApplication sharedApplication] keyWindow] retain];
        [self animateViewControllerToAppear:aModalViewController];
        
    } else {
        
        [pendingViewControllers_ addObject:aModalViewController];
        
    }
    
}

/*
 * Dismisses the view controller
 */
- (void)dismissModalViewController:(ModalViewControllerBase *)aModalViewController {
    
    if (aModalViewController == currentModalViewController_) {
        
        UIView *currentView = currentModalViewController_.view;
        currentModalViewTransform_ = currentView.transform;
        CGAffineTransform finalTransform = CGAffineTransformScale(currentModalViewTransform_, 0.01f, 0.01f);
        
        if ([[UIView class] respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
            
            [UIView animateWithDuration:MODAL_VIEW_CONTROLLER_DISAPPEAR_ANIMATION_DURATION
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 
                                 currentView.transform = finalTransform;
                                 modalWindow_.backgroundColor = [UIColor clearColor];
                                 
                             }
                             completion:^(BOOL finished) {
                                 
                                 [self removeDisappearedViewController];
                                 
                             }];
            
        } else {
            
            [UIView beginAnimations:MODAL_VIEW_CONTROLLER_DISAPPEAR_ANIMATION_NAME
                            context:nil];
            
            [UIView setAnimationDuration:MODAL_VIEW_CONTROLLER_DISAPPEAR_ANIMATION_DURATION];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(viewControllerDisappearAnimationDidStop:finished:context:)];
            
            currentView.transform = finalTransform;
            modalWindow_.backgroundColor = [UIColor clearColor];
            
            [UIView commitAnimations];
            
        }
        
    }
    
}


/**
 * Dismisses the current controller and remove all pending.
 */
- (void)dismissAllModalViewControllers {
    [pendingViewControllers_ removeAllObjects];
    [self dismissModalViewController:currentModalViewController_];
}


@end


#pragma mark -

@implementation ModalManager(private)

#pragma mark -
#pragma mark Managind modal view controllers

/**
 * Displays the next modal view controller stored in the pending list
 */
- (void)displayNextModalViewController {
    
    if ([pendingViewControllers_ count] > 0) {
        
        ModalViewControllerBase *modalViewController = [[[pendingViewControllers_ objectAtIndex:0] retain] autorelease];
        [pendingViewControllers_ removeObjectAtIndex:0];
        [self animateViewControllerToAppear:modalViewController];
        
    } else {
        
        modalWindow_.hidden = YES;
        //        [previousKeyWindow_ makeKeyAndVisible];
        //        [previousKeyWindow_ release];
        //        previousKeyWindow_ = nil;
        
    }
    
}

/*
 * Animates a view controller to be displayed. If a previous modal view controller is displayed, it
 * is removed without any animation
 */
- (void)animateViewControllerToAppear:(ModalViewControllerBase *)aModalViewController {
    
    if (currentModalViewController_ != aModalViewController) {
        
		[aModalViewController retain];
        [currentModalViewController_.view removeFromSuperview];
        [currentModalViewController_ release];
        currentModalViewController_ = nil;
        currentModalViewController_ = aModalViewController;
        
        UIView *currentView = currentModalViewController_.view;
        [currentView removeFromSuperview];
        [modalWindow_ addSubview:currentView];
        
        UIScreen *screen = [UIScreen mainScreen];
        CGRect applicationFrame = [screen applicationFrame];
        currentView.frame = applicationFrame;
        currentView.backgroundColor = [UIColor clearColor];
        currentModalViewTransform_ = currentView.transform;
        CGAffineTransform initialTransform = CGAffineTransformScale(currentModalViewTransform_, 0.01f, 0.01f);
        CGAffineTransform finalTransform = CGAffineTransformScale(currentModalViewTransform_, 1.0f, 1.0f);
        currentView.transform = initialTransform;
        modalWindow_.backgroundColor = [UIColor clearColor];
        [modalWindow_ makeKeyAndVisible];
        modalWindow_.hidden = NO;
        
        if ([[UIView class] respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
            
            [UIView animateWithDuration:MODAL_VIEW_CONTROLLER_APPEAR_ANIMATION_DURATION
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 
                                 currentView.transform = finalTransform;
                                 modalWindow_.backgroundColor = [UIColor colorWithWhite:0.05f
                                                                                  alpha:0.5f];
                                 
                             }
                             completion:nil];
            
        } else {
            
            [UIView beginAnimations:MODAL_VIEW_CONTROLLER_APPEAR_ANIMATION_NAME
                            context:nil];
            
            [UIView setAnimationDuration:MODAL_VIEW_CONTROLLER_APPEAR_ANIMATION_DURATION];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            
            currentView.transform = finalTransform;
            modalWindow_.backgroundColor = [UIColor colorWithWhite:0.05f
                                                             alpha:0.5f];
            
            [UIView commitAnimations];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark Animations management

/*
 * Invoked by framework when view controller disappear animation is stopped
 */
- (void)viewControllerDisappearAnimationDidStop:(NSString *)animationID
                                       finished:(NSNumber *)finished
                                        context:(void *)context {
    
    if ([animationID isEqualToString:MODAL_VIEW_CONTROLLER_DISAPPEAR_ANIMATION_NAME]) {
        
        [self removeDisappearedViewController];
        
    }
    
}

/*
 * Removes the view controller that disappeared from the window (intended to be used once the view controller disappear animation is finished)
 */
- (void)removeDisappearedViewController {
    
    UIView *currentView = currentModalViewController_.view;
    [currentView removeFromSuperview];
    currentView.transform = currentModalViewTransform_;
    [currentModalViewController_.delegate modalViewControllerDismissed:currentModalViewController_];
    
    [currentModalViewController_ release];
    currentModalViewController_ = nil;
    
    [self displayNextModalViewController];
    
}

@end
