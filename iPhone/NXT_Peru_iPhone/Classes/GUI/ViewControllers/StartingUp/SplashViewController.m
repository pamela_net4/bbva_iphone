/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "SplashViewController.h"
#import "Constants.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MCBConfiguration.h"
#import "MCBFacade.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "Updater.h"
#import "UIView+Frame.h"

/**
 * Define the SplashViewController NIB file name
 */
NSString * const kSplashViewControllerNibFileName = @"SplashViewController";

/**
 * Defines the minimum time to display the splash view measured in seconds
 */
NSTimeInterval const kMinSplashDisplayTime = 0.0f;

#pragma mark -

/**
 * SplashViewController private extension
 */
@interface SplashViewController()

/**
 * Releases the graphic elements not needed when view is hidden
 *
 * @private
 */
- (void)releaseSplashViewControllerGraphicElements;

/**
 * Notifies the delegate that the splash view finished its presentation
 *
 * @private
 */
- (void)notifySplashPresentationFinished;

/**
 * Checks whether the view controller can disappear or not. If it is too early, a delay to notify the delegate is started
 *
 * @private
 */
- (void)checkViewControllerCanDisappearNow;

@end


#pragma mark -

@implementation SplashViewController

#pragma mark -
#pragma mark Properties

@synthesize splashImageView = splashImageView_;
@synthesize bbvaLogoImageView = bbvaLogoImageView_;
@synthesize activityIndicator = activityIndicator_;
@synthesize grayVersionLabel = grayVersionLabel_;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self releaseSplashViewControllerGraphicElements];
    
    delegate_ = nil;
	
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory. Visual elements are released
 */
- (void)viewDidUnload {
    
    [self releaseSplashViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseSplashViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases the graphic elements not needed when view is hidden
 */
- (void)releaseSplashViewControllerGraphicElements {
    
    [splashImageView_ release];
    splashImageView_ = nil;
    
    [bbvaLogoImageView_ release];
    bbvaLogoImageView_ = nil;
    
    [activityIndicator_ release];
    activityIndicator_ = nil;
    
    [grayVersionLabel_ release];
    grayVersionLabel_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased SplashViewController constructed from a NIB file
 */
+ (SplashViewController *)splashViewController {
    
    SplashViewController *result = [[[SplashViewController alloc] initWithNibName:kSplashViewControllerNibFileName
                                                                           bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
	
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory. Style is applied to graphic elements
 */
- (void)viewDidLoad {
	
    [super viewDidLoad];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
	
    splashImageView_.image = [imagesCache imageNamed:IMG_SPLASH_WINDOW_BACKGROUND];
    
    bbvaLogoImageView_.image = [imagesCache imageNamed:SPLASH_TEXT_IMAGE_FILE_NAME];
    bbvaLogoImageView_.width = bbvaLogoImageView_.image.size.width;
    
    CGFloat grayLabelTextSize = 14.0f;
	
    [NXT_Peru_iPhoneStyler styleLabel:grayVersionLabel_ withFontSize:grayLabelTextSize color:[UIColor grayColor]];
    
    [Tools setText:[NSString stringWithFormat:@"v%@", NSLocalizedString(APP_VERSION_STRING, nil)] toLabel:grayVersionLabel_];

	[activityIndicator_ setHidesWhenStopped:YES];
	
}

/**
 * Notifies the view controller that its view is about to be become visible. Starts the information download
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
	
	// Delay of splash screen
	NSTimeInterval delay = self.appDelegate.firstRun ? FIRST_SPLASH_DELAY : SPLASH_DELAY;
	[self performSelector:@selector(checkViewControllerCanDisappearNow) withObject:nil afterDelay:delay];
	
}

/**
 * Notifies the view controller that its view was added to a window. The current time is stored
 *
 * @param animated If YES, the view was added to the window using an animation
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    viewControllerAppearTime_ = [[NSDate date] timeIntervalSinceReferenceDate];
    
}

/**
 * Notifies the view controller that its view has been dismissed. Stops the activity indicator
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
	
    [activityIndicator_ stopAnimating];
    
}

#pragma mark -
#pragma mark View disappearing management

/*
 * Checks whether the view controller can disappear or not. If it is too early, a delay to notify the delegate is started
 */
- (void)checkViewControllerCanDisappearNow {
    
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSinceReferenceDate];
    NSTimeInterval lastTimeToDisplayViewController = viewControllerAppearTime_ + kMinSplashDisplayTime;
    
    if (currentTime >= lastTimeToDisplayViewController) {
        
        [self notifySplashPresentationFinished];
        
    } else {
        
        [self performSelector:@selector(notifySplashPresentationFinished)
                   withObject:nil
                   afterDelay:(lastTimeToDisplayViewController - currentTime)];
        
    }
    
}

#pragma mark -
#pragma mark Delegate notification

/*
 * Notifies the delegate that the splash view finished its presentation
 */
- (void)notifySplashPresentationFinished {
	
    [delegate_ splashViewControllerFinishedPresentation:self];
	
}

@end
