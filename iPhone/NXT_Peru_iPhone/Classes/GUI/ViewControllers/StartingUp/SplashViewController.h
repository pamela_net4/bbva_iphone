/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTViewController.h"


//Forward declarations
@class SplashViewController;
@class UpdaterOperation;


/**
 * SplashViewController delegate protocol. The delegate will receive splash view controll events to control
 * when to hide it
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol SplashViewControllerDelegate

@required

/**
 * The deleate is notified that the SplashViewController instance finished its presentation and it is OK to hide it
 *
 * @param splashViewController The SplashViewController instance triggering the event
 */
- (void)splashViewControllerFinishedPresentation:(SplashViewController *)splashViewController;

@end


/**
 * Simple view with a splash image
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface SplashViewController : NXTViewController {
    
@private
    
#pragma mark Visuals
    
    /**
     * Background image view
     */
    UIImageView *splashImageView_;
    
    /**
     * BBVA logo image view
     */
    UIImageView *bbvaLogoImageView_;
    
    /**
     * Activity indicator
     */
    UIActivityIndicatorView* activityIndicator_;
    
    /**
     * Gray version label
     */
    UILabel *grayVersionLabel_;
    
#pragma mark Logic
    
    /**
     * Stores the time the view controller appeared
     */
    NSTimeInterval viewControllerAppearTime_;
	
    /**
     * Splash view controller delegate
     */
    id<SplashViewControllerDelegate> delegate_;
}


/**
 * Provides read-write access to the background image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *splashImageView;

/**
 * Provides read-write access to the BBVA logo image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *bbvaLogoImageView;

/**
 * Provides read-write access to the activity indicator and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIActivityIndicatorView* activityIndicator;

/**
 * Provides read-write access to the gray version label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *grayVersionLabel;

/**
 * Provides read-write access to the splash view controller delegate
 */
@property (nonatomic, readwrite, assign) id<SplashViewControllerDelegate> delegate;


/**
 * Creates and returns an autoreleased SplashViewController constructed from a NIB file
 *
 * @return The autoreleased SplashViewController constructed from a NIB file
 */
+ (SplashViewController *)splashViewController;

@end
