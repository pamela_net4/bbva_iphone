//
//  FastLoanStepThreeViewController.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 5/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "NXTEditableViewController.h"
#import "FastLoanSummary.h"
@class NXTTextField;

@interface FastLoanStepThreeViewController : NXTEditableViewController<UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate>{
    
    @private
    
    /**
     * Container view
     */
    UIView *containerView_;
    
    UIImageView *headerImageView_;
    
    UILabel * dateOperationLabel_;
    
    UILabel * dateOperationValueLabel_;
    
    UILabel * numberOperationLabel_;
    
    UILabel * numberOperationValueLabel_;
    
    UILabel * accountNumberLabel_;
    
    UILabel * accountNumberValueLabel_;
    
    UILabel * amountLoanLabel_;
    
    UILabel * amountLoanValueLabel_;
    
    UILabel * contractLabel_;
    
    UILabel * contractValueLabel_;
    
    UILabel * quotaMonthLabel_;
    
    UILabel * quotaMonthValueLabel_;
    
    UILabel * termLabel_;
    
    UILabel * termValueLabel_;
    
    UILabel * quotaTypeLabel_;
    
    UILabel * quotaTypeValueLabel_;
    
    UILabel * dayOfPaytLabel_;
    
    UILabel * dayOfPaytValueLabel_;
    
    UILabel * teatLabel_;
    
    UILabel * teaValueLabel_;
    
    UILabel * tceaLabel_;
    
    UILabel * tceaValueLabel_;
    
    UIButton * quotaMonthButton_;
    
    UIButton * teaTipButton_;
    
    UIButton * tceaTipButton_;
    
    
    UIView * separatorTop_;
    
    /**
     * Accept button
     */
    UIButton *acceptButton_;
    
    /*
     * gretting label
     */
    UILabel *grettingLabel_;
    
    /*
     * branding ImageView
     */
    UIImageView *brandingImageView_;
    
    UIView * optCordView_;
    
    UILabel * optCordLabel_;
    
    FastLoanSummary *FastLoanSummary_;
    
    FastLoanStepThreeViewController * FastLoanStepThreeViewController_;
}

/**
 * Provides readwrite access to the containerView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *containerView;

/**
 * Provides readwrite access to the headerImageView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *headerImageView;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *grettingLabel;

/**
 * Provides readwrite access to the dateOperationLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dateOperationLabel;

/**
 * Provides readwrite access to the dateOperationValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dateOperationValueLabel;

/**
 * Provides readwrite access to the numberOperationLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * numberOperationLabel;

/**
 * Provides readwrite access to the numberOperationValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * numberOperationValueLabel;

/**
 * Provides readwrite access to the accountNumberLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * accountNumberLabel;

/**
 * Provides readwrite access to the accountNumberValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * accountNumberValueLabel;

/**
 * Provides readwrite access to the emailLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * amountLoanLabel;

/**
 * Provides readwrite access to the emailValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * amountLoanValueLabel;

/**
 * Provides readwrite access to the contractLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * contractLabel;

/**
 * Provides readwrite access to the contractValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * contractValueLabel;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * quotaMonthLabel;

/**
 * Provides readwrite access to the quotaMonthValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * quotaMonthValueLabel;

/**
 * Provides readwrite access to the termLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * termLabel;

/**
 * Provides readwrite access to the termValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * termValueLabel;

/**
 * Provides readwrite access to the quotaTypeLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * quotaTypeLabel;

/**
 * Provides readwrite access to the quotaTypeValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * quotaTypeValueLabel;

/**
 * Provides readwrite access to the dayOfPaytLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dayOfPaytLabel;

/**
 * Provides readwrite access to the dayOfPaytValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dayOfPaytValueLabel;

/**
 * Provides readwrite access to the teatLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * teatLabel;

/**
 * Provides readwrite access to the teaValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * teaValueLabel;

/**
 * Provides readwrite access to the tceaLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * tceaLabel;

/**
 * Provides readwrite access to the tceaValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * tceaValueLabel;

@property (nonatomic, readwrite, retain) IBOutlet UIButton * quotaMonthButton;

@property (nonatomic, readwrite, retain) IBOutlet UIButton * teaTipButton;

@property (nonatomic, readwrite, retain) IBOutlet UIButton * tceaTipButton;

/**
 * Provides readwrite access to the separatorTop. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView * separatorTop;

/**
 * Provides readwrite access to the acceptButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *acceptButton;
/**
 * Provides readwrite access to the brandingImageView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;


@property (nonatomic, readwrite, retain) IBOutlet UIView * optCordView;

@property (nonatomic, readwrite, retain) IBOutlet UILabel * optCordLabel;

/**
 * Provides read-write access to the helper element
 */
@property (nonatomic, readwrite, retain) FastLoanSummary *FastLoanSummary;

/**
 * Creates and returns an autoreleased FastLoanStepThreeViewController constructed from a NIB file
 *
 * @return The autoreleased FastLoanStepThreeViewController constructed from a NIB file
 */
+ (FastLoanStepThreeViewController *)FastLoanStepThreeViewController;

@end
