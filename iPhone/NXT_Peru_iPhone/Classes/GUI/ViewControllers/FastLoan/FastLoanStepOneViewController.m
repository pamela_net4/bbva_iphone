//
//  FastLoanStepOneViewController.m
//  NXT_Peru_iPhone
//
//  Created by Estefany on 2/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "FastLoanStepOneViewController.h"
#import "SimpleHeaderView.h"
#import "NXTNavigationItem.h"
#import "ImagesCache.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "ImagesFileNames.h"
#import "NXTEditableViewController+protected.h"
#import "Tools.h"
#import "NXTCurrencyTextField.h"
#import "NXTComboButton.h"
#import "NXTTextField.h"
#import "UIColor+BBVA_Colors.h"
#import "AccountFastLoan.h"
#import "AccountFastLoanList.h"
#import "EmailInfo.h"
#import "EmailInfoList.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "FastLoanConfirm.h"
#import "Updater.h"
#import "FastLoanTerm.h"
#import "FastLoanTermList.h"
#import "SelectionItemsPopView.h"
#import "NPopup.h"
#import "FastLoanSimulation.h"
#import "SimulationView.h"
#import "FastLoanStepTwoViewController.h"
#import "FastLoanBenefitsViewController.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"FastLoanStepOneViewController"
/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_VERY_NEAR_ELEMENT                      1.0f
/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_NEAR_ELEMENT                           5.0f

/**
 * Defines the vertical gap between two far away elements
 */
#define VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT                       10.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              16.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        14.0f


@interface FastLoanStepOneViewController ()

@end

@implementation FastLoanStepOneViewController

#pragma mark -
#pragma mark Properties

@synthesize cardCombo = cardCombo_;
@synthesize cardLabel = cardLabel_;
@synthesize emailCombo = emailCombo_;
@synthesize emailLabel = emailLabel_;
@synthesize amountIndicationLabel = amountIndicationLabel_;
@synthesize amountTextField = amountTextField_;
@synthesize emailTipButton = emailTipButton_;
@synthesize transferButton = transferButton_;
@synthesize brandingImageView = brandingImageView_;
@synthesize containerView = containerView_;
@synthesize FastLoanStartup = FastLoanStartup_;
@synthesize emailTextField = emailTextField_;
@synthesize editEmailButton = editEmailButton_;
@synthesize editAmountButton = editAmountButton_;
@synthesize separatorEmailBottom = separatorEmailBottom_;
@synthesize separatorEmailTop = separatorEmailTop_;
@synthesize amountValueIndicationLabel = amountValueIndicationLabel_;
@synthesize termLabel = termLabel_;
@synthesize termCombo = termCombo_;
@synthesize typeOfQuotaLabel = typeOfQuotaLabel_;
@synthesize typeOfQuotaCombo = typeOfQuotaCombo_;
@synthesize dayOfPayLabel = dayOfPayLabel_;
@synthesize dayOfPayTextField = dayOfPayTextField_;
@synthesize editDayOfPay = editDayOfPay_;
@synthesize moreInformationView = moreInformationView_;
@synthesize changeTypeInformationView = changeTypeInformationView_;
@synthesize moreInformationButton = moreInformationButton_;
@synthesize separatorMoreInformationBottom = separatorMoreInformationBottom_;
@synthesize editMonthsButton = editMonthsButton_;

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransferWithCashMobileViewControllerGraphicElements];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationFastLoanTermResponse
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationFastLoanSimulationResponse
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationFastLoanStepTwoResponse
                                                  object:nil];
    
    [FastLoanStartup_ release];
    FastLoanStartup_ = nil;
    
    [selectionsMonths_ release];
    selectionsMonths_ = nil;
    
    [months_ release];
    months_ = nil;
    
    [editableViews_ release];
    editableViews_ = nil;
    
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransferWithCashMobileViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransferWithCashMobileViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseTransferWithCashMobileViewControllerGraphicElements {
    
    [cardCombo_ release];
    cardCombo_ = nil;
    
    [cardLabel_ release];
    cardLabel_ = nil;
    
    [emailCombo_ release];
    emailCombo_ = nil;
    
    [emailLabel_ release];
    emailLabel_ = nil;
    
    [amountIndicationLabel_ release];
    amountIndicationLabel_ = nil;
    
    [amountValueIndicationLabel_ release];
    amountValueIndicationLabel_ = nil;
    
    [amountTextField_ release];
    amountTextField_ = nil;
    
    [emailTipButton_ release];
    emailTipButton_ = nil;
    
    [transferButton_ release];
    transferButton_ = nil;
    
    [brandingImageView_ release];
    brandingImageView_ = nil;
    
    [emailTextField_ release];
    emailTextField_ = nil;
    
    [editEmailButton_ release];
    editEmailButton_ = nil;
    
    [editAmountButton_ release];
    editAmountButton_ = nil;
    
    [separatorEmailBottom_ release];
    separatorEmailBottom_ = nil;
    
    [separatorEmailTop_ release];
    separatorEmailTop_ = nil;
    
    [termLabel_ release];
    termLabel_ = nil;
    
    [termCombo_ release];
    termCombo_ = nil;
    
    [typeOfQuotaCombo_ release];
    typeOfQuotaCombo_ = nil;
    
    [typeOfQuotaLabel_ release];
    typeOfQuotaLabel_ = nil;
    
    [editDayOfPay_ release];
    editDayOfPay_ = nil;
    
    [dayOfPayLabel_ release];
    dayOfPayLabel_ = nil;
    
    [dayOfPayTextField_ release];
    dayOfPayTextField_ = nil;
    
    [moreInformationView_ release];
    moreInformationView_ = nil;
    
    [changeTypeInformationView_ release];
    changeTypeInformationView_ = nil;
    
    [separatorMoreInformationBottom_ release];
    separatorMoreInformationBottom_ = nil;
    
    [moreInformationButton_ release];
    moreInformationButton_ = nil;
    
    [editMonthsButton_ release];
    editMonthsButton_ = nil;
    
    [editableViews_ removeAllObjects];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self view] setFrame:[[UIScreen mainScreen] bounds]];
    
    UIView *view = self.view;
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    view.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
    containerView_.backgroundColor = [UIColor clearColor];
    
    /**/
    
    selectionsMonths_ = [[NSArray arrayWithObjects:[NSNumber numberWithInt:6],[NSNumber numberWithInt:11], nil] retain];
    months_ = [[[NSMutableArray alloc] init] retain];
    [months_ addObject:@"Enero"];
    [months_ addObject:@"Febrero"];
    [months_ addObject:@"Marzo"];
    [months_ addObject:@"Abril"];
    [months_ addObject:@"Mayo"];
    [months_ addObject:@"Junio"];
    [months_ addObject:@"Julio"];
    [months_ addObject:@"Agosto"];
    [months_ addObject:@"Setiembre"];
    [months_ addObject:@"Octubre"];
    [months_ addObject:@"Noviembre"];
    [months_ addObject:@"Diciembre"];
    
    [NXT_Peru_iPhoneStyler styleLabel:cardLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [cardLabel_ setText:@"Cuenta de Abono"];
    
    [cardCombo_ setTitle:@"Selecciona cuenta"];
    [cardCombo_ setDelegate:self];
    [cardCombo_ setInputAccessoryView:popButtonsView_];
    [cardCombo_ setIsNewDesign:YES];
    
    [NXT_Peru_iPhoneStyler styleLabel:amountIndicationLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [amountIndicationLabel_ setText:@"Monto"];
    
    [NXT_Peru_iPhoneStyler styleLabel:amountValueIndicationLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVABlueColor]];
    [amountValueIndicationLabel_ setText:@"(mín.  y máx.  )"];
    
    [NXT_Peru_iPhoneStyler styleTextField:amountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    [amountTextField_ setCanContainCents:NO];
    [amountTextField_ setCurrencyShowAlways:YES];
    [amountTextField_ setDelegate:self];
    [amountTextField_ setInputAccessoryView:popButtonsView_];
    
    
    [NXT_Peru_iPhoneStyler styleLabel:termLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [termLabel_ setText:@"Plazo"];
    
    [termCombo_ setTitle:@"Selecciona plazo"];
    [termCombo_ setIsNewDesign:YES];
    [termCombo_ setDelegate:self];
    [termCombo_ setInputAccessoryView:popButtonsView_];
    
    [NXT_Peru_iPhoneStyler styleLabel:dayOfPayLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    
    [dayOfPayLabel_ setText:@"Día de Pago"];
    
    [NXT_Peru_iPhoneStyler styleTextField:dayOfPayTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    dayOfPayTextField_.keyboardType = UIKeyboardTypeNumberPad;
    dayOfPayTextField_.enabled = YES;
    [dayOfPayTextField_ setDelegate:self];
    [dayOfPayTextField_ setInputAccessoryView:popButtonsView_];
    
     [NXT_Peru_iPhoneStyler styleLabel:typeOfQuotaLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    
    [typeOfQuotaLabel_ setText:@"Tipo de Cuota"];
    
    [typeOfQuotaCombo_ setTitle:@"Selecciona Tipo de Cuota"];
    [typeOfQuotaCombo_ setIsNewDesign:YES];
    [typeOfQuotaCombo_ setDelegate:self];
    [typeOfQuotaCombo_ setInputAccessoryView:popButtonsView_];
    
    
    [NXT_Peru_iPhoneStyler styleLabel:emailLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [emailLabel_ setText:@"Correo Electrónico"];
    
    [emailCombo_ setTitle:@"Selecciona Correo Electrónico"];
    [emailCombo_ setIsNewDesign:YES];
    [emailCombo_ setDelegate:self];
    [emailCombo_ setInputAccessoryView:popButtonsView_];
    
    [NXT_Peru_iPhoneStyler styleTextField:emailTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    emailTextField_.keyboardType = UIKeyboardTypeEmailAddress;
    emailTextField_.enabled = YES;
    [emailTextField_ setPlaceholder:@"Escribe tu correo"];
    [emailTextField_ setDelegate:self];
    [emailTextField_ setInputAccessoryView:popButtonsView_];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:transferButton_];
    [transferButton_ setTitle:@"Calcular" forState:UIControlStateNormal];
    [transferButton_ addTarget:self
                        action:@selector(continueButton:)
              forControlEvents:UIControlEventTouchUpInside];
    
    [moreInformationButton_ addTarget:self action:@selector(moreInformation:) forControlEvents:UIControlEventTouchUpInside];
    
    [emailTipButton_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    [editEmailButton_ addTarget:self action:@selector(editTextField:) forControlEvents:UIControlEventTouchUpInside];
    [editAmountButton_ addTarget:self action:@selector(editTextField:) forControlEvents:UIControlEventTouchUpInside];
    [editDayOfPay_ addTarget:self action:@selector(editTextField:) forControlEvents:UIControlEventTouchUpInside];
    
    [editMonthsButton_ addTarget:self action:@selector(showSelectionMonths:) forControlEvents:UIControlEventTouchUpInside];
    
    changeTypeInformationView_.hidden = YES;
    editMonthsButton_.hidden = YES;
    
    /**/
    
    CGRect containerFrame = containerView_.frame;
    containerFrame.origin.x = 0.0f;
    containerFrame.origin.y = 0.0f;
    containerView_.frame = containerFrame;
    
    [self setScrollableView:containerView_];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.origin.y = 0;
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
    [self layoutViews];
    
    [view bringSubviewToFront:brandingImageView_];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
    
    transparentScroll_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
   
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(FastLoanTermResponseReceived:)
                                                 name:kNotificationFastLoanTermResponse
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(FastLoanResponseReceived:)
                                                 name:kNotificationFastLoanSimulationResponse
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(FastLoanStepTwoResponseReceived:)
                                                 name:kNotificationFastLoanStepTwoResponse
                                               object:nil];
    
    if (shouldClearInterfaceData_) {
        
        [transparentScroll_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f) animated:NO];
        
        shouldClearInterfaceData_ = NO;
        
    }
    
    if (editableViews_ == nil) {
        editableViews_ = [[NSMutableArray alloc] init];
    } else {
        [editableViews_ removeAllObjects];
    }
    [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:cardCombo_,
                                         amountTextField_,
                                         termCombo_,
                                         dayOfPayTextField_,
                                         typeOfQuotaCombo_,
                                         emailCombo_,
                                         nil]];
    
    
    
    [self layoutViews];
    [self displayStoredInformation];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the confirmation response and stores the information
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationFastLoanSimulationResponse
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationFastLoanTermResponse
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationFastLoanStepTwoResponse
                                                  object:nil];
    
    [self okButtonClickedInPopButtonsView:popButtonsView_];
    
    [super viewWillDisappear:animated];
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
}

/**
 * Lays out the views
 */
- (void)layoutViews {
    
   // if ([self isViewLoaded]) {
        
        CGRect frame = cardLabel_.frame;
        frame.origin.y = VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        CGFloat height = [Tools labelHeight:cardLabel_ forText:cardLabel_.text];
        frame.size.height = height;
        cardLabel_.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = cardCombo_.frame;
        frame.origin.y = auxPos;
        cardCombo_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        if(!changeTypeInformationView_.hidden){
            
            frame = changeTypeInformationView_.frame;
            frame.origin.y = auxPos;
            changeTypeInformationView_.frame = frame;
            auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
            
        }
        else{
            auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        }
        
        frame = amountValueIndicationLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:amountIndicationLabel_ forText:amountIndicationLabel_.text];
        frame.size.height = height;
        amountValueIndicationLabel_.frame = frame;
        
        frame = amountIndicationLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:amountIndicationLabel_ forText:amountIndicationLabel_.text];
        frame.size.height = height;
        amountIndicationLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        
        frame = amountTextField_.frame;
        frame.origin.y = auxPos;
        amountTextField_.frame = frame;
        
        frame = editAmountButton_.frame;
        frame.origin.y = auxPos - 5;
        editAmountButton_.frame = frame;
        
        auxPos = CGRectGetMaxY(amountTextField_.frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = termLabel_.frame;
        frame.origin.y = auxPos;
        termLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = termCombo_.frame;
        frame.origin.y = auxPos;
        termCombo_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = dayOfPayLabel_.frame;
        frame.origin.y = auxPos;
        dayOfPayLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = dayOfPayTextField_.frame;
        frame.origin.y = auxPos;
        dayOfPayTextField_.frame = frame;
        
        frame = editDayOfPay_.frame;
        frame.origin.y = auxPos - 5;
        editDayOfPay_.frame = frame;
        
        auxPos = CGRectGetMaxY(dayOfPayTextField_.frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        frame = typeOfQuotaLabel_.frame;
        frame.origin.y = auxPos;
        typeOfQuotaLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = editMonthsButton_.frame;
        frame.origin.y = auxPos - 5;
        editMonthsButton_.frame = frame;
        
        frame = typeOfQuotaCombo_.frame;
        frame.origin.y = auxPos;
        frame.size.width = editMonthsButton_.hidden ? 300 : 254;
        typeOfQuotaCombo_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = separatorEmailTop_.frame;
        frame.origin.y = auxPos;
        separatorEmailTop_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = emailLabel_.frame;
        frame.origin.y = auxPos;
        emailLabel_.frame = frame;
        
        frame = emailTipButton_.frame;
        frame.origin.y = auxPos - 8;
        emailTipButton_.frame = frame;
        
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        
        frame = emailCombo_.frame;
        frame.origin.y = auxPos;
        emailCombo_.frame = frame;
        
        frame = emailTextField_.frame;
        frame.origin.y = auxPos;
        emailTextField_.frame = frame;
        
        frame = editEmailButton_.frame;
        frame.origin.y = auxPos - 5;
        editEmailButton_.frame = frame;
        
        auxPos = MAX(CGRectGetMaxY(emailTextField_.frame),
                     CGRectGetMaxY(emailCombo_.frame)) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        //NSLog(@"posy separator email bottom %f", auxPos);
        
        frame = separatorEmailBottom_.frame;
        frame.origin.y = auxPos;
        separatorEmailBottom_.frame = frame;
        auxPos = CGRectGetMaxY(frame);
        
        //NSLog(@"posy moreInformationView %f", auxPos);
        
        frame = moreInformationView_.frame;
        frame.origin.y = auxPos;
        moreInformationView_.frame = frame;
        auxPos = CGRectGetMaxY(frame);
        
        frame = separatorMoreInformationBottom_.frame;
        frame.origin.y = auxPos;
        separatorMoreInformationBottom_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        UIButton *transferButton = self.transferButton;
        frame = transferButton.frame;
        frame.origin.y = auxPos;
        transferButton.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.size.height = auxPos;
        containerView.frame = frame;
        
        [self recalculateScroll];
        
   // }
}

- (UINavigationItem *)navigationItem {
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = @"Préstamo al Toque";
    return result;
}


#pragma mark -
#pragma mark FastLoan selectors

/**
 * Displays the stored information. Sets the information into the views
 */
- (void)displayStoredInformation {
    
    if ([self isViewLoaded]) {
        
        //cards
        
        NSMutableArray *array = [NSMutableArray array];
        NSArray *cardList = [FastLoanStartup_.accountFastLoanList accountfastloanList];
        
        for(AccountFastLoan *cardForIncrement in cardList) {
            [array addObject:[NSString stringWithFormat:@"%@", cardForIncrement.account]];
        }
        cardCombo_.stringsList = [NSArray arrayWithArray:array];
        
        if ([array count] > 0) {
            [cardCombo_ setSelectedIndex:0];
        }
        
        if([array count] == 1){
            [cardCombo_ setEnabled:NO];
        }
        else{
            [cardCombo_ setEnabled:YES];
        }
        
        [array removeAllObjects];
        
        //term
        
        array = [NSMutableArray array];
        NSArray *fastloantermList = [FastLoanStartup_.fastLoanTermList fastloantermList];
        
        int indexMaxTerm = -1, maxTerm = -1;
        for(int i=0; i<[fastloantermList count]; i++) {
            FastLoanTerm *fastLoanTerm = [fastloantermList objectAtIndex:i];
            [array addObject:[NSString stringWithFormat:@"%@", fastLoanTerm.term]];
            
            if([fastLoanTerm.term intValue] > maxTerm){
                maxTerm = [fastLoanTerm.term intValue];
                indexMaxTerm = i;
            }
            
        }
        termCombo_.stringsList = [NSArray arrayWithArray:array];
        
        if ([array count] > 0 && indexMaxTerm>-1) {
            [termCombo_ setSelectedIndex:indexMaxTerm];
        }
        if([array count] == 1){
            [termCombo_ setEnabled:NO];
        }
        else{
            [termCombo_ setEnabled:YES];
        }
        [array removeAllObjects];
        
        //typeOfQUota
        array = [NSMutableArray array];
        [array addObject:@"Simple"];
        [array addObject:@"Doble (Julio y Diciembre)"];
        typeOfQuotaCombo_.stringsList = [NSArray arrayWithArray:array];
        
        if ([array count] > 0) {
            [typeOfQuotaCombo_ setSelectedIndex:0];
        }
        if([array count] == 1){
            [typeOfQuotaCombo_ setEnabled:NO];
        }
        else{
            [typeOfQuotaCombo_ setEnabled:YES];
        }
        [array removeAllObjects];
        
        
        //emails
        
        array = [NSMutableArray array];
        NSArray *emailList = [FastLoanStartup_.emailInfoList emailinfoList];
        
        for(EmailInfo *email in emailList) {
            [array addObject:email.email];
        }
        emailCombo_.stringsList = [NSArray arrayWithArray:array];
        
        if ([array count] > 0) {
            [emailCombo_ setSelectedIndex:0];
            [emailCombo_ setHidden:NO];
            [emailTextField_ setHidden:YES];
            
            
            if (editableViews_ == nil) {
                editableViews_ = [[NSMutableArray alloc] init];
            } else {
                [editableViews_ removeAllObjects];
            }
            [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:cardCombo_,
                                                 amountTextField_,
                                                 termCombo_,
                                                 dayOfPayTextField_,
                                                 typeOfQuotaCombo_,
                                                 emailCombo_,
                                                 nil]];
        }
        else{
            [emailCombo_ setHidden:YES];
            [emailTextField_ setHidden:NO];
            
            
            if (editableViews_ == nil) {
                editableViews_ = [[NSMutableArray alloc] init];
            } else {
                [editableViews_ removeAllObjects];
            }
            [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:cardCombo_,
                                                 amountTextField_,
                                                 termCombo_,
                                                 dayOfPayTextField_,
                                                 typeOfQuotaCombo_,
                                                 emailTextField_,
                                                 nil]];
        }
        
        if([array count] == 1){
            [emailCombo_ setEnabled:NO];
        }
        else{
            [emailCombo_ setEnabled:YES];
        }
        
        [array removeAllObjects];
        
        
        [amountValueIndicationLabel_ setText:[NSString stringWithFormat:@"(mín. %@ %@ y máx %@ %@)",
                                            FastLoanStartup_.currencySymbol,
                                            FastLoanStartup_.amountMinFormated,
                                            FastLoanStartup_.currencySymbol,
                                              FastLoanStartup_.amountMaxFormated]];

        [amountTextField_ setCurrencySymbol:FastLoanStartup_.currencySymbol];
        [amountTextField_ setText:FastLoanStartup_.amountFormated];
        
        [dayOfPayTextField_ setText:FastLoanStartup_.dayOfPay];
        
       
        
    }
}

/**
 * Stores the information into the transfer operation helper instance
 */
- (void)storeInformationIntoHelper {
    

    
}

#pragma mark -
#pragma mark User interaction

- (IBAction)comboPressed:(UIView*)view {
    
    [self editableViewHasBeenClicked:view];
    
    
}

- (IBAction)editTextField:(id)view {
    
    if(view == editAmountButton_){
        [amountTextField_ setText:@""];
        [amountTextField_ becomeFirstResponder];
    }
    else if(view == editDayOfPay_){
        [dayOfPayTextField_ setText:@""];
        [dayOfPayTextField_ becomeFirstResponder];
    }
    else if(view == editEmailButton_){
        [emailCombo_ setHidden:YES];
        [emailTextField_ setHidden:NO];
        
        if (editableViews_ == nil) {
            editableViews_ = [[NSMutableArray alloc] init];
        } else {
            [editableViews_ removeAllObjects];
        }
        [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:cardCombo_,
                                             amountTextField_,
                                             termCombo_,
                                             dayOfPayTextField_,
                                             typeOfQuotaCombo_,
                                             emailTextField_,
                                             nil]];
    }
    
   
}

-(IBAction)showSelectionMonths:(id)view{
    
    SelectionItemsPopView * selectionItemsPopView = [[SelectionItemsPopView alloc] init];
    selectionItemsPopView.delegate = self;
    [selectionItemsPopView showInWindow:self.view.window];

    NSArray * months = [[NSArray arrayWithArray:months_] retain];
    [selectionItemsPopView setItemsString:months];
    [selectionItemsPopView setValuesSelected:selectionsMonths_];
}

- (IBAction)showTooltip:(UIView*)view {
    [self showInfoTooltipFromView:view
                          message:@"Para enviarte los documentos del préstamo y estados de cuenta"];
}


-(IBAction)moreInformation:(id)sender{
    FastLoanBenefitsViewController * fastLoanBenefitsViewController = [[FastLoanBenefitsViewController fastLoanBenefitsViewController] retain];
    [[self navigationController] pushViewController:fastLoanBenefitsViewController animated:YES];
}

- (IBAction)continueButton:(UIView*)view {
    
    BOOL isValid = YES;
    
   [self storeInformationIntoHelper];
    
    [amountTextField_ resignFirstResponder];
    [dayOfPayTextField_ resignFirstResponder];
    [emailTextField_ resignFirstResponder];
    
    int selectedCardIndex = (int)cardCombo_.selectedIndex;
    AccountFastLoan * card = nil;
    
    int selectedTermIndex = (int)termCombo_.selectedIndex;
    
    NSString * amountString = amountTextField_.text;
    NSString *amountOnlyNumbers = [amountString stringByReplacingOccurrencesOfString:@"." withString:@""];
    amountOnlyNumbers = [amountOnlyNumbers stringByReplacingOccurrencesOfString:@"," withString:@""];
    long int amount = [amountOnlyNumbers intValue];
    
    NSString * email = emailCombo_.isHidden ? emailTextField_.text : @"";
    NSString * emailSelection = @"";
    NSString * emailEditable = email;
    
    
    NSArray *emailList = [FastLoanStartup_.emailInfoList emailinfoList];
    NSString * accion = [emailList count] == 0 ? @"agregar" : @"editar";
    if(!emailCombo_.isHidden && [emailList count]>0){
        EmailInfo * emailInfo = [ emailList objectAtIndex:emailCombo_.selectedIndex];
        email = emailInfo.email;
        emailSelection = emailInfo.email;
        accion = @"";
    }
    
    NSString * dayOfPayString = dayOfPayTextField_.text;
    long int dayOfPay = [dayOfPayString length] > 0 ? [dayOfPayString intValue] : -1;
    
    if (selectedCardIndex == NSNotFound || selectedCardIndex < 0 ||
        selectedCardIndex >=
        [[[[self FastLoanStartup] accountFastLoanList] accountfastloanList] count]) {
        
        [Tools showAlertWithMessage:@"Selecciona la cuenta de abono." title:nil];
        isValid = NO;
        
    }
    else if (selectedTermIndex == NSNotFound || selectedTermIndex < 0 ||
             selectedTermIndex >=
             [[[[self FastLoanStartup] fastLoanTermList] fastloantermList] count]) {
        
        [Tools showAlertWithMessage:@"Selecciona plazo." title:nil];
        isValid = NO;
        
    }
    else{
        
        card = [[[[self FastLoanStartup] accountFastLoanList] accountfastloanList] objectAtIndex:selectedCardIndex];
        
        if(![Tools isValidEmail:email]){
            
            [self showErrorTooltipFromView:emailTextField_ message:@"Ingresa un correo válido"];
            
            [NXT_Peru_iPhoneStyler styleLabel:emailLabel_ withFontSize:TEXT_FONT_SIZE
                                        color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            [NXT_Peru_iPhoneStyler styleTextFieldError:emailTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                         andColor:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            isValid = NO;
        }
        
        if([dayOfPayString length]==0){
            [self showErrorTooltipFromView:dayOfPayTextField_ message:@"Ingresa un día de pago"];
            
            [NXT_Peru_iPhoneStyler styleLabel:dayOfPayLabel_ withFontSize:TEXT_FONT_SIZE
                                        color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            [NXT_Peru_iPhoneStyler styleTextFieldError:dayOfPayTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                              andColor:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            isValid = NO;
        }
        else if(dayOfPay<1 || dayOfPay>31){
            
            [self showErrorTooltipFromView:dayOfPayTextField_ message:@"Ingresa un día de pago entre 1 y 31"];
            
            [NXT_Peru_iPhoneStyler styleLabel:dayOfPayLabel_ withFontSize:TEXT_FONT_SIZE
                                        color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            [NXT_Peru_iPhoneStyler styleTextFieldError:dayOfPayTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                              andColor:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            isValid = NO;
        }
        
        if (!([amountOnlyNumbers length] > 0) || ([amountString isEqualToString:@"0.00"]) ||
            ([amountString floatValue] == 0.0f)) {
            
            NSString * message = [NSString stringWithFormat:@"Ingresa un monto"];
            
            [NXT_Peru_iPhoneStyler styleLabel:amountIndicationLabel_ withFontSize:TEXT_FONT_SIZE
                                color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            [NXT_Peru_iPhoneStyler styleLabel:amountValueIndicationLabel_ withFontSize:TEXT_FONT_SIZE
                                        color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            [NXT_Peru_iPhoneStyler styleTextFieldError:amountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                              andColor:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            [self showErrorTooltipFromView:amountTextField_ message:message];
            isValid = NO;
            
        }
        else if (amount > [[FastLoanStartup_ amountMax] floatValue] ||
                 amount < [[FastLoanStartup_ amountMin] floatValue]) {
            
            NSString * message = [NSString stringWithFormat:@"Ingresa un monto entre %@ %@ y %@ %@",
                                  [FastLoanStartup_ currencySymbol],
                                  [FastLoanStartup_ amountMinFormated],
                                  [FastLoanStartup_ currencySymbol],
                                  [FastLoanStartup_ amountMaxFormated]];
            
            [NXT_Peru_iPhoneStyler styleLabel:amountIndicationLabel_ withFontSize:TEXT_FONT_SIZE
                                         color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
             [NXT_Peru_iPhoneStyler styleLabel:amountValueIndicationLabel_ withFontSize:TEXT_FONT_SIZE
                                         color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
             [NXT_Peru_iPhoneStyler styleTextFieldError:amountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                               andColor:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
             [self showErrorTooltipFromView:amountTextField_ message:message];
             isValid = NO;
            
        }
    }
    
    if(isValid){
        NSString * position = [card id];
        FastLoanTerm * fastLoanTerm = [[[[self FastLoanStartup] fastLoanTermList] fastloantermList] objectAtIndex:selectedTermIndex];
        
        NSString * quotaType = [typeOfQuotaCombo_ selectedIndex] == 0 ? @"Simples" : @"Dobles";
        NSString * monthDobles = nil;
        
        if([typeOfQuotaCombo_ selectedIndex] == 1){
            int firstMonth = (int)[[selectionsMonths_ objectAtIndex:0] integerValue];
            int secondMonth = (int)[[selectionsMonths_ objectAtIndex:1] integerValue];
            monthDobles = [NSString stringWithFormat:@"%i$%i",
                       (firstMonth + 1),
                       (secondMonth + 1)
                       ];
        }
            
        [self.appDelegate showActivityIndicator:poai_Both];
        
        [[Updater getInstance] fastLoanOperationSimulation:amountOnlyNumbers
                                                      term:fastLoanTerm.term
                                                  dayOfPay:dayOfPayString
                                                 quotaType:quotaType
                                               monthDobles:monthDobles
                                              accountIndex:position
                                            emailSelection:emailSelection emailEditable:emailEditable action:accion];
    }
}

#pragma SimulationViewDelegate

-(void)onNextScreenFromSimulation{
    
    [self.appDelegate showActivityIndicator:poai_Both];
    [[Updater getInstance] fastLoanOperationConfirm];
}

#pragma SelectionItemsPopViewDelegate

-(void)onDismissSelectionItems:(NSArray*)selecteds{
    
    selectionsMonths_ = selecteds;
    int fisrtMonth = [[selectionsMonths_ objectAtIndex:0] intValue];
    int secondMonth = [[selectionsMonths_ objectAtIndex:1] intValue];
    
    NSString * stringMonths = [NSString stringWithFormat:@"Doble (%@ y %@)",
                               [months_ objectAtIndex:fisrtMonth],
                               [months_ objectAtIndex:secondMonth]
                               ];
    
    NSMutableArray * array = [NSMutableArray array];
    [array addObject:@"Simple"];
    [array addObject:stringMonths];
    typeOfQuotaCombo_.stringsList = [NSArray arrayWithArray:array];
    
    [typeOfQuotaCombo_ setSelectedIndex:1];
    [typeOfQuotaCombo_ setTitle:[array objectAtIndex:typeOfQuotaCombo_.selectedIndex]];
    
    [typeOfQuotaCombo_.picker reloadInputViews];
}


#pragma mark -
#pragma mark NXTComboButtonDelegate selectors

/*
 * Informs the delegate that a value has been selected
 */
- (void)NXTComboButtonValueSelected:(NXTComboButton *)comboButton {
    
    if (comboButton == cardCombo_) {

        NSArray *accountfastloanList = [FastLoanStartup_.accountFastLoanList accountfastloanList];
        NSString * currency = FastLoanStartup_.currency;
        int accountSelected = (int)[cardCombo_ selectedIndex];
        BOOL reloadView = NO;
        
        if(accountSelected>=0 && accountSelected<[accountfastloanList count]) {
            
            AccountFastLoan * accountFastLoan = [accountfastloanList objectAtIndex:accountSelected];
            if ([accountFastLoan.currency isEqualToString:currency]) {
                reloadView = changeTypeInformationView_.hidden == NO;
            } else {
                reloadView = changeTypeInformationView_.hidden == YES;
            }
        }
        
        if(reloadView){
            [self okButtonClickedInPopButtonsView:popButtonsView_];
            
            changeTypeInformationView_.hidden = !changeTypeInformationView_.hidden;
            [self layoutViews];//reload
            [self editableViewHasBeenClicked:cardCombo_];//edit
        }
        
    }
    else if (comboButton == typeOfQuotaCombo_) {
        
        if([typeOfQuotaCombo_ selectedIndex] == 0){
            editMonthsButton_.hidden = YES;
        }
        else{
            editMonthsButton_.hidden = NO;
        }
        [self layoutViews];
    }
    else if (comboButton == emailCombo_) {
    }
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        if (popButtonsView_.superview == nil) {
            popButtonsState_ = pbse_Show;
        } else {
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
    if(textField == amountTextField_){
        [NXT_Peru_iPhoneStyler styleLabel:amountIndicationLabel_ withFontSize:TEXT_FONT_SIZE
                                    color:[UIColor BBVABlueColor]];
        [NXT_Peru_iPhoneStyler styleLabel:amountValueIndicationLabel_ withFontSize:TEXT_FONT_SMALL_SIZE
                                    color:[UIColor BBVABlueColor]];
        [NXT_Peru_iPhoneStyler styleTextField:amountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                          andColor:[UIColor BBVAGreyColor]];
        [NXT_Peru_iPhoneStyler removeError:amountTextField_];
    }
    else if(textField == dayOfPayTextField_){
        [NXT_Peru_iPhoneStyler styleLabel:dayOfPayLabel_ withFontSize:TEXT_FONT_SIZE
                                    color:[UIColor BBVABlueColor]];
        [NXT_Peru_iPhoneStyler styleTextField:dayOfPayTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                     andColor:[UIColor BBVAGreyColor]];
        [NXT_Peru_iPhoneStyler removeError:dayOfPayTextField_];
    }
    else if(textField == emailTextField_){
        [NXT_Peru_iPhoneStyler styleLabel:emailLabel_ withFontSize:TEXT_FONT_SIZE
                                    color:[UIColor BBVABlueColor]];
        [NXT_Peru_iPhoneStyler styleTextField:emailTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                     andColor:[UIColor BBVAGreyColor]];
        [NXT_Peru_iPhoneStyler removeError:emailTextField_];
    }
    
    return YES;
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self editableViewHasBeenClicked:textField];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == amountTextField_){
        
        if([[amountTextField_ text] length] > 0){
            
            NSString * amountString = amountTextField_.text;
            NSString *amountOnlyNumbers = [amountString stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountOnlyNumbers = [amountOnlyNumbers stringByReplacingOccurrencesOfString:@"," withString:@""];
        
            [self.appDelegate showActivityIndicator:poai_Both];
            [[Updater getInstance] fastLoanOperationTerm:amountOnlyNumbers];
        }
        
    }
}

/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    NSUInteger finalTotalChars = [textField.text length] + [string length] - range.length;

    if(textField == amountTextField_) {
        
        if (resultLength == 0) {
            result = YES;
        } else {
            NSScanner *scanner = [NSScanner scannerWithString:resultString];
            NSInteger resultInteger = 0;
            
            if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                if (resultInteger > 0) {
                    result = YES;
                }
            }
        }
        
    }
    else if(textField == dayOfPayTextField_){
        
        result = (finalTotalChars <= 2 &&
                  [Tools isValidText:string forCharacterString:DAY_OF_MONTH_VALID_CHARACTERS]);
        return result;
    }
    else if(textField == emailTextField_){
        
        result = (finalTotalChars <= 50 && [Tools isValidText:string forCharacterString:EMAIL_TEXT_VALID_CHARACTERS]);
        return result;
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased constructed from a NIB file
 */
+ (FastLoanStepOneViewController *)FastLoanStepOneViewController {
    
    return [[[FastLoanStepOneViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
}

- (BOOL)FastLoanTermResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    BOOL result = NO;
    
    if (!response.isError) {
        
         if ([response isKindOfClass:[FastLoanTermList class]]) {
             
             FastLoanTermList *fastLoanTermList = (FastLoanTermList *)response;
             
             FastLoanStartup_.fastLoanTermList = fastLoanTermList;
             
             NSMutableArray * array = [NSMutableArray array];
             NSArray *fastloantermList = [FastLoanStartup_.fastLoanTermList fastloantermList];
             
             int indexMaxTerm = -1, maxTerm = -1;
             for(int i=0; i<[fastloantermList count]; i++) {
                 FastLoanTerm *fastLoanTerm = [fastloantermList objectAtIndex:i];
                 [array addObject:[NSString stringWithFormat:@"%@", fastLoanTerm.term]];
                 
                 if([fastLoanTerm.term intValue] > maxTerm){
                     maxTerm = [fastLoanTerm.term intValue];
                     indexMaxTerm = i;
                 }
                 
             }
             termCombo_.stringsList = [NSArray arrayWithArray:array];
             
             if ([array count] > 0 && indexMaxTerm>-1) {
                 [termCombo_ setSelectedIndex:indexMaxTerm];
             }
             if([array count] == 1){
                 [termCombo_ setEnabled:NO];
             }
             else{
                 [termCombo_ setEnabled:YES];
             }
             [array removeAllObjects];
             
             result = YES;
         }
    }
    return result;
}


/*
 * Notifies the transfer operation helper the tranfer response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)FastLoanResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    BOOL result = NO;
    
    if (!response.isError) {
        
        if ([response isKindOfClass:[FastLoanSimulation class]]) {
            
            //call step two
            
            FastLoanSimulation *fastLoanSimulation = (FastLoanSimulation *)response;
            
            SimulationView * simulationView = [[SimulationView alloc] init];
            
            simulationView.delegate = self;
            [simulationView showInWindow:self.view.window];
            [simulationView setData:fastLoanSimulation];
            
            result = YES;
        
        }
    }
    
    return result;
 
}

-(BOOL)FastLoanStepTwoResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    BOOL result = NO;
    
    if (!response.isError) {
        
        if ([response isKindOfClass:[FastLoanConfirm class]]) {
            
            //call step two
            FastLoanConfirm *fastLoanConfirm = (FastLoanConfirm *)response;
            
             FastLoanStepTwoViewController * fastLoanStepTwoViewController = [[FastLoanStepTwoViewController FastLoanStepTwoViewController] retain];
             fastLoanStepTwoViewController.fastLoanConfirm = fastLoanConfirm;
             [[self navigationController] pushViewController:fastLoanStepTwoViewController animated:YES];
            
            
            result = YES;
            
        }
    }
    
    return result;
    
}


@end
