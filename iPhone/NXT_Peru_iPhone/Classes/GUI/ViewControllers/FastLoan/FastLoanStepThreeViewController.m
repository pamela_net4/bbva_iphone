//
//  FastLoanStepThreeViewController.m
//  NXT_Peru_iPhone
//
//  Created by Estefany on 5/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "FastLoanStepThreeViewController.h"
#import "Constants.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "NXTEditableViewController+protected.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "SealCell.h"
#import "TransferDetailCell.h"
#import "Tools.h"
#import "GlobalAdditionalInformation.h"
#import "SimpleHeaderView.h"
#import "UIColor+BBVA_Colors.h"
#import "Session.H"
#import "NXTNavigationItem.h"
#import "TitleAndAttributes.h"
#import "NXTTextField.h"
#import "Updater.h"
#import "FastLoanSummary.h"


/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"FastLoanStepThreeViewController"
/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_NEAR_ELEMENT                           5.0f

/**
 * Defines the vertical gap between two far away elements
 */
#define VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT                       10.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_BIG_SIZE                                          17.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              15.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                        13.0f

@interface FastLoanStepThreeViewController ()

@end

@implementation FastLoanStepThreeViewController

@synthesize containerView = containerView_;
@synthesize headerImageView = headerImageView_;
@synthesize acceptButton = acceptButton_;
@synthesize grettingLabel = grettingLabel_;
@synthesize brandingImageView = brandingImageView_;
@synthesize FastLoanSummary = FastLoanSummary_;
@synthesize dateOperationLabel = dateOperationLabel_;
@synthesize dateOperationValueLabel = dateOperationValueLabel_;
@synthesize numberOperationLabel = numberOperationLabel_;
@synthesize numberOperationValueLabel = numberOperationValueLabel_;
@synthesize accountNumberLabel = accountNumberLabel_;
@synthesize accountNumberValueLabel = accountNumberValueLabel_;
@synthesize amountLoanLabel = amountLoanLabel_;
@synthesize amountLoanValueLabel = amountLoanValueLabel_;
@synthesize contractLabel = contractLabel_;
@synthesize contractValueLabel = contractValueLabel_;
@synthesize quotaMonthLabel = quotaMonthLabel_;
@synthesize quotaMonthValueLabel = quotaMonthValueLabel_;
@synthesize separatorTop = separatorTop_;
@synthesize optCordView = optCordView_;
@synthesize optCordLabel = optCordLabel_;
@synthesize quotaTypeLabel = quotaTypeLabel_;
@synthesize quotaTypeValueLabel = quotaTypeValueLabel_;
@synthesize termLabel = termLabel_;
@synthesize termValueLabel = termValueLabel_;
@synthesize dayOfPaytLabel = dayOfPaytLabel_;
@synthesize dayOfPaytValueLabel = dayOfPaytValueLabel_;
@synthesize teatLabel = teatLabel_;
@synthesize teaValueLabel = teaValueLabel_;
@synthesize tceaLabel = tceaLabel_;
@synthesize tceaValueLabel = tceaValueLabel_;
@synthesize quotaMonthButton = quotaMonthButton_;
@synthesize teaTipButton = teaTipButton_;
@synthesize tceaTipButton = tceaTipButton_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransfersStepTwoViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/**
 * Releases graphic elements
 */
- (void)releaseTransfersStepTwoViewControllerGraphicElements {
    
    [containerView_ release];
    containerView_ = nil;
    
    [headerImageView_ release];
    headerImageView_ = nil;
    
    [acceptButton_ release];
    acceptButton_ = nil;
    
    [dateOperationLabel_ release];
    dateOperationLabel_ = nil;
    
    [dateOperationValueLabel_ release];
    dateOperationValueLabel_ = nil;
    
    [numberOperationLabel_ release];
    numberOperationLabel_ = nil;
    
    [numberOperationValueLabel_ release];
    numberOperationValueLabel_ = nil;
    
    [accountNumberLabel_ release];
    accountNumberLabel_ = nil;
    
    [accountNumberValueLabel_ release];
    accountNumberValueLabel_ = nil;
    
    [amountLoanLabel_ release];
    amountLoanLabel_ = nil;
    
    [amountLoanValueLabel_ release];
    amountLoanValueLabel_ = nil;
    
    [contractLabel_ release];
    contractLabel_ = nil;
    
    [contractValueLabel_ release];
    contractValueLabel_ = nil;
    
    [quotaMonthLabel_ release];
    quotaMonthLabel_ = nil;
    
    [quotaMonthValueLabel_ release];
    quotaMonthValueLabel_ = nil;
    
    [termLabel_ release];
    termLabel_ = nil;
    
    [termValueLabel_ release];
    termValueLabel_ = nil;
    
    [quotaTypeLabel_ release];
    quotaTypeLabel_ = nil;
    
    [quotaTypeValueLabel_ release];
    quotaTypeValueLabel_ = nil;
    
    [dayOfPaytLabel_ release];
    dayOfPaytLabel_ = nil;
    
    [dayOfPaytValueLabel_ release];
    dayOfPaytValueLabel_ = nil;
    
    [teatLabel_ release];
    teatLabel_ = nil;
    
    [teaValueLabel_ release];
    teaValueLabel_ = nil;
    
    [tceaLabel_ release];
    tceaLabel_ = nil;
    
    [tceaValueLabel_ release];
    tceaValueLabel_ = nil;
    
    [separatorTop_ release];
    separatorTop_ = nil;
    
    [optCordView_ release];
    optCordView_ = nil;
    
    [optCordLabel_ release];
    optCordLabel_ = nil;
    
    [teaTipButton_ release];
    teaTipButton_ = nil;
    
    [tceaTipButton_ release];
    tceaTipButton_ = nil;
    
    [quotaMonthButton_ release];
    quotaMonthButton_ = nil;
    
    [editableViews_ removeAllObjects];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIView *view = self.view;
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    view.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
    containerView_.backgroundColor = [UIColor clearColor];
    
    /**/
    
    [NXT_Peru_iPhoneStyler styleLabel:grettingLabel_ withFontSize:TEXT_FONT_BIG_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:dateOperationLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:dateOperationValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:numberOperationLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:numberOperationValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:accountNumberLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:accountNumberValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:amountLoanLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:amountLoanValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:contractLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:contractValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:quotaMonthLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:quotaMonthValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:termLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:termValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:quotaTypeLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:quotaTypeValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:dayOfPaytLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:dayOfPaytValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:teatLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:teaValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:tceaLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:tceaValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:optCordLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    
    dateOperationLabel_.text = @"Fecha y Hora de Operación";
    numberOperationLabel_.text = @"Nro. Operación";
    accountNumberLabel_.text = @"Nro. Cuenta";
    amountLoanLabel_.text = @"Monto Préstamo";
    contractLabel_.text = @"Nro. Contrato";
    termLabel_.text = @"Plazo";
    quotaTypeLabel_.text = @"Tipo de Cuota";
    dayOfPaytLabel_.text = @"Día de Pago";
    teatLabel_.text = @"TEA";
    tceaLabel_.text = @"TCEA";
    quotaMonthLabel_.text = @"Cuota Mensual";
    
    [quotaMonthButton_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    [teaTipButton_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    [tceaTipButton_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    
    /**/
    
    CGRect containerFrame = containerView_.frame;
    containerFrame.origin.x = 0.0f;
    containerFrame.origin.y = 0.0f;
    containerView_.frame = containerFrame;
    
    [self setScrollableView:containerView_];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.origin.y = 0;
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
    [self layoutViews];
    
    [view bringSubviewToFront:brandingImageView_];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:acceptButton_];
    [acceptButton_ setTitle:@"Volver a Mis Cuentas" forState:UIControlStateNormal];
    [acceptButton_ addTarget:self action:@selector(acceptButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
    
    transparentScroll_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
}



/*
 * Creates and returns an autoreleased FastLoanStepThreeViewController constructed from a NIB file
 */
+ (FastLoanStepThreeViewController *)FastLoanStepThreeViewController {
    
    FastLoanStepThreeViewController *result = [[[FastLoanStepThreeViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}


/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self displayStoredInformation];
    
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the
 * transfer last step confirmation notification
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [Session getInstance].globalPositionRequestServerData = YES;
    [[self navigationController] popToRootViewControllerAnimated:NO];
    
  // [self storeInformationIntoHelper];
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingImageView] frame]);
    [self setScrollNominalFrame:scrollFrame];
    
    
    
}

/*
 * Lays out the views
 */
- (void)layoutViews {
    
    if ([self isViewLoaded]) {
        
        CGRect frame = headerImageView_.frame;
        frame.origin.y = VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        headerImageView_.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame);
        
        frame = grettingLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:grettingLabel_ forText:grettingLabel_.text];
        grettingLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = separatorTop_.frame;
        frame.origin.y = auxPos;
        separatorTop_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        frame = numberOperationLabel_.frame;
        frame.origin.y = auxPos;
        numberOperationLabel_.frame = frame;
        
        frame = numberOperationValueLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:numberOperationValueLabel_ forText:numberOperationValueLabel_.text];
        numberOperationValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = dateOperationLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:dateOperationLabel_ forText:dateOperationLabel_.text];
        dateOperationLabel_.frame = frame;
        
        frame = dateOperationValueLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:dateOperationValueLabel_ forText:dateOperationValueLabel_.text];
        dateOperationValueLabel_.frame = frame;
        auxPos = MAX(CGRectGetMaxY(dateOperationValueLabel_.frame ),CGRectGetMaxY(dateOperationLabel_.frame)) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = accountNumberLabel_.frame;
        frame.origin.y = auxPos;
        accountNumberLabel_.frame = frame;
        
        frame = accountNumberValueLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:accountNumberValueLabel_ forText:accountNumberValueLabel_.text];
        accountNumberValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = amountLoanLabel_.frame;
        frame.origin.y = auxPos;
        amountLoanLabel_.frame = frame;
        
        frame = amountLoanValueLabel_.frame;
        frame.size.height = [Tools labelHeight:amountLoanValueLabel_ forText:amountLoanValueLabel_.text];
        frame.origin.y = auxPos;
        amountLoanValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = contractLabel_.frame;
        frame.origin.y = auxPos;
        contractLabel_.frame = frame;
        
        frame = contractValueLabel_.frame;
        frame.size.height = [Tools labelHeight:contractValueLabel_ forText:contractValueLabel_.text];
        frame.origin.y = auxPos;
        contractValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = quotaMonthLabel_.frame;
        frame.origin.y = auxPos;
        quotaMonthLabel_.frame = frame;
        
        frame = quotaMonthButton_.frame;
        frame.origin.y = auxPos - 8;
        quotaMonthButton_.frame = frame;
        
        frame = quotaMonthValueLabel_.frame;
        frame.size.height = [Tools labelHeight:quotaMonthValueLabel_ forText:quotaMonthValueLabel_.text];
        frame.origin.y = auxPos;
        quotaMonthValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = termLabel_.frame;
        frame.origin.y = auxPos;
        termLabel_.frame = frame;
        
        frame = termValueLabel_.frame;
        frame.size.height = [Tools labelHeight:termValueLabel_ forText:termValueLabel_.text];
        frame.origin.y = auxPos;
        termValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = quotaTypeLabel_.frame;
        frame.origin.y = auxPos;
        quotaTypeLabel_.frame = frame;
        
        frame = quotaTypeValueLabel_.frame;
        frame.size.height = [Tools labelHeight:quotaTypeValueLabel_ forText:quotaTypeValueLabel_.text];
        frame.origin.y = auxPos;
        quotaTypeValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = dayOfPaytLabel_.frame;
        frame.origin.y = auxPos;
        dayOfPaytLabel_.frame = frame;
        
        frame = dayOfPaytValueLabel_.frame;
        frame.size.height = [Tools labelHeight:dayOfPaytValueLabel_ forText:dayOfPaytValueLabel_.text];
        frame.origin.y = auxPos;
        dayOfPaytValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = teatLabel_.frame;
        frame.origin.y = auxPos;
        teatLabel_.frame = frame;
        
        frame = teaTipButton_.frame;
        frame.origin.y = auxPos - 8;
        teaTipButton_.frame = frame;
        
        frame = teaValueLabel_.frame;
        frame.size.height = [Tools labelHeight:teaValueLabel_ forText:teaValueLabel_.text];
        frame.origin.y = auxPos;
        teaValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = tceaLabel_.frame;
        frame.origin.y = auxPos;
        tceaLabel_.frame = frame;
        
        frame = tceaTipButton_.frame;
        frame.origin.y = auxPos - 8;
        tceaTipButton_.frame = frame;
        
        frame = tceaValueLabel_.frame;
        frame.size.height = [Tools labelHeight:tceaValueLabel_ forText:tceaValueLabel_.text];
        frame.origin.y = auxPos;
        tceaValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = optCordLabel_.frame;
        frame.origin.y = 0;
        frame.size.height = [Tools labelHeight:optCordLabel_ forText:optCordLabel_.text] + 16;
        optCordLabel_.frame = frame;
        
        frame = optCordView_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:optCordLabel_ forText:optCordLabel_.text] + 16;
        optCordView_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = acceptButton_.frame;
        frame.origin.y = auxPos;
        acceptButton_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
    
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.origin.y = 0;
        frame.size.height = auxPos;
        containerView.frame = frame;
    
        [self recalculateScroll];
    }
    
}

-(void)displayStoredInformation{
    
    grettingLabel_.text = [NSString stringWithFormat:@"¡%@, el dinero ya está en tu cuenta!",
                           FastLoanSummary_.customer];
    
    dateOperationValueLabel_.text = [NSString stringWithFormat:@"%@ - %@",
                                     FastLoanSummary_.date,
                                     FastLoanSummary_.hour];
    numberOperationValueLabel_.text = FastLoanSummary_.numberOperation;
    
    accountNumberValueLabel_.text = [NSString stringWithFormat:@"%@ (%@)",
                                     FastLoanSummary_.account,
                                     [Tools getCurrencyLiteral:FastLoanSummary_.currencyAccount]];
    
    amountLoanValueLabel_.text = [NSString stringWithFormat:@"%@ %@",
                                     FastLoanSummary_.currencyAmount,
                                     FastLoanSummary_.amount];
    
    contractValueLabel_.text = FastLoanSummary_.contract;
    
    quotaMonthValueLabel_.text = [NSString stringWithFormat:@"%@ %@",
                                  FastLoanSummary_.currencyQuota,
                                  FastLoanSummary_.amountQuota];
    
    termValueLabel_.text = [NSString stringWithFormat:@"%@ meses", FastLoanSummary_.term];
    
    quotaTypeValueLabel_.text = FastLoanSummary_.quotaType;
    
    dayOfPaytValueLabel_.text = FastLoanSummary_.dayOfPay;
    teaValueLabel_.text = [NSString stringWithFormat:@"%@%%", FastLoanSummary_.tea];
    tceaValueLabel_.text = [NSString stringWithFormat:@"%@%%", FastLoanSummary_.tcea];
    
    optCordLabel_.text = FastLoanSummary_.messageNotification;
    
    [self layoutViews];

}


- (IBAction)showTooltip:(UIView*)view {
    if(view == quotaMonthButton_){
        [self showInfoTooltipFromView:view
                              message:@"No incluye seguro de desgravamen, el cual puede variar entre S/ 0.35 y S/16.50 (ver cuota total en cronograma de pagos)."];
    }
    else if(view == teaTipButton_){
        [self showInfoTooltipFromView:view
                              message:@"La Tasa de Interés Efectiva Anual calcula el costo o valor de interés esperado en 360 días."];
    }
    else if(view == tceaTipButton_){
        [self showInfoTooltipFromView:view
                              message:@"La Tasa de Costo Efectiva Anual representa el costo total que se pagará en 360 días, incluyendo TEA, comisiones, gastos y seguros."];
    }
}


#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initial configuration is set.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
}





#pragma mark -
#pragma mark User Interaction

/*
 * Performs accept buttom action
 */
- (IBAction)acceptButtonTapped {
    
    [Session getInstance].globalPositionRequestServerData = YES;
    [[self navigationController] popToRootViewControllerAnimated:YES];
    
}



#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    result.hidesBackButton = YES;
    result.customTitleView.topLabelText = @"Préstamo al Toque";
    return result;
    
}


@end
