//
//  FastLoanStepTwoViewController.m
//  NXT_Peru_iPhone
//
//  Created by Estefany on 5/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "FastLoanStepTwoViewController.h"
#import "Constants.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "NXTEditableViewController+protected.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "SealCell.h"
#import "TransferDetailCell.h"
#import "Tools.h"
#import "GlobalAdditionalInformation.h"
#import "SimpleHeaderView.h"
#import "UIColor+BBVA_Colors.h"
#import "Session.H"
#import "NXTNavigationItem.h"
#import "TitleAndAttributes.h"
#import "NXTTextField.h"
#import "Updater.h"
#import "FastLoanConfirm.h"
#import "FastLoanSummary.h"
#import "FastLoanSchedule.h"
#import "ScheduleView.h"
#import "FastLoanStepThreeViewController.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"FastLoanStepTwoViewController"
/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_NEAR_ELEMENT                           5.0f

/**
 * Defines the vertical gap between two far away elements
 */
#define VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT                       10.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_BIG_SIZE                                          17.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              15.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                        13.0f

@interface FastLoanStepTwoViewController ()

@end

@implementation FastLoanStepTwoViewController

@synthesize containerView = containerView_;
@synthesize acceptButton = acceptButton_;
@synthesize grettingLabel = grettingLabel_;
@synthesize grettingInfoLabel = grettingInfoLabel_;
@synthesize brandingImageView = brandingImageView_;
@synthesize fastLoanConfirm = fastLoanConfirm_;
@synthesize quotaMonthLabel = quotaMonthLabel_;
@synthesize quotaMonthValueLabel = quotaMonthValueLabel_;
@synthesize termLabel = termLabel_;
@synthesize termValueLabel = termValueLabel_;
@synthesize quotaTypeLabel = quotaTypeLabel_;
@synthesize quotaTypeValueLabel = quotaTypeValueLabel_;
@synthesize dayOfPayLabel = dayOfPayLabel_;
@synthesize dayOfPayValueLabel = dayOfPayValueLabel_;
@synthesize teaLabel = teaLabel_;
@synthesize teaValueLabel = teaValueLabel_;
@synthesize tceaLabel = tceaLabel_;
@synthesize tceaValueLabel = tceaValueLabel_;
@synthesize separatorTop = separatorTop_;
@synthesize optCordView = optCordView_;
@synthesize sealView = sealView_;
@synthesize optCordLabel = optCordLabel_;
@synthesize optCordTipButton = optCordTipButton_;
@synthesize optCordTextField = optCordTextField_;
@synthesize sealImageView = sealImageView_;
@synthesize sealLabel = sealLabel_;
@synthesize quotaMonthTipButton = quotaMonthTipButton_;
@synthesize optCordTipImageView = optCordTipImageView_;
@synthesize teaTipButton = teaTipButton_;
@synthesize tceaTipButton = tceaTipButton_;
@synthesize documentsView = documentsView_;
@synthesize documentContractLabel = documentContractLabel_;
@synthesize documentSureLabel = documentSureLabel_;
@synthesize documentScheduleLabel = documentScheduleLabel_;
@synthesize documentContractButton = documentContractButton_;
@synthesize documentSureButton = documentSureButton_;
@synthesize documentScheduleButton = documentScheduleButton_;
@synthesize legalTermsView = legalTermsView_;
@synthesize acceptLabel = acceptLabel_;
@synthesize emailLabel = emailLabel_;
@synthesize emailTipButton = emailTipButton_;
@synthesize legalTermsSwitch = legalTermsSwitch_;
@synthesize legalTermsAlertImageView = legalTermsAlertImageView_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [fastLoanConfirm_ release];
    fastLoanConfirm_ = nil;
    
    [termsAndConsiderationsViewController_ release];
    termsAndConsiderationsViewController_ = nil;
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationFastLoanStepThreeResponse
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationFastLoanStepTwoResponse
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationFastLoanScheduleResponse
                                                  object:nil];
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransfersStepTwoViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/**
 * Releases graphic elements
 */
- (void)releaseTransfersStepTwoViewControllerGraphicElements {
    
    [containerView_ release];
    containerView_ = nil;
    
    [grettingLabel_ release];
    grettingLabel_ = nil;
    
    [grettingInfoLabel_ release];
    grettingInfoLabel_ = nil;
    
    [acceptButton_ release];
    acceptButton_ = nil;
    
    [quotaMonthLabel_ release];
    quotaMonthLabel_ = nil;
    
    [quotaMonthValueLabel_ release];
    quotaMonthValueLabel_ = nil;
    
    [termLabel_ release];
    termLabel_ = nil;
    
    [termValueLabel_ release];
    termValueLabel_ = nil;
    
    [quotaTypeLabel_ release];
    quotaTypeLabel_ = nil;
    
    [quotaTypeValueLabel_ release];
    quotaTypeValueLabel_ = nil;
    
    [dayOfPayLabel_ release];
    dayOfPayLabel_ = nil;
    
    [dayOfPayValueLabel_ release];
    dayOfPayValueLabel_ = nil;
    
    [teaValueLabel_ release];
    teaValueLabel_ = nil;
    
    [teaLabel_ release];
    teaLabel_ = nil;
    
    [tceaValueLabel_ release];
    tceaValueLabel_ = nil;
    
    [tceaLabel_ release];
    tceaLabel_ = nil;
    
    [teaTipButton_ release];
    teaTipButton_ = nil;
    
    [tceaTipButton_ release];
    tceaTipButton_ = nil;
    
    [separatorTop_ release];
    separatorTop_ = nil;
    
    [optCordView_ release];
    optCordView_ = nil;
    
    [sealView_ release];
    sealView_ = nil;
    
    [optCordLabel_ release];
    optCordLabel_ = nil;
    
    [optCordTipButton_ release];
    optCordTipButton_ = nil;
    
    [optCordTextField_ release];
    optCordTextField_ = nil;
    
    [sealImageView_ release];
    sealImageView_ = nil;
    
    [sealLabel_ release];
    sealLabel_ = nil;
    
    [quotaMonthTipButton_ release];
    quotaMonthTipButton_ = nil;
    
    [optCordTipImageView_ release];
    optCordTipImageView_ = nil;
    
    [documentsView_ release];
    documentsView_ = nil;
    
    [documentContractLabel_ release];
    documentContractLabel_ = nil;
    
    [documentSureLabel_ release];
    documentSureLabel_ = nil;
    
    [documentScheduleLabel_ release];
    documentScheduleLabel_ = nil;
    
    [documentContractButton_ release];
    documentContractButton_ = nil;
    
    [documentSureButton_ release];
    documentSureButton_ = nil;
    
    [documentScheduleButton_ release];
    documentScheduleButton_ = nil;
    
    [legalTermsView_ release];
    legalTermsView_ = nil;
    
    [acceptLabel_ release];
    acceptLabel_ = nil;
    
    [emailLabel_ release];
    emailLabel_ = nil;
    
    [emailTipButton_ release];
    emailTipButton_ = nil;
    
    [legalTermsSwitch_ release];
    legalTermsSwitch_ = nil;
    
    [legalTermsAlertImageView_ release];
    legalTermsAlertImageView_ = nil;
    
    
    [editableViews_ removeAllObjects];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIView *view = self.view;
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    view.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
    containerView_.backgroundColor = [UIColor clearColor];
    documentsView_.backgroundColor = [UIColor clearColor];
    legalTermsView_.backgroundColor = [UIColor clearColor];
    
    /**/
    
    [NXT_Peru_iPhoneStyler styleLabel:grettingLabel_ withFontSize:TEXT_FONT_BIG_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:grettingInfoLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:quotaMonthLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:quotaMonthValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:termLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:termValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:quotaTypeLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:quotaTypeValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:dayOfPayLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:dayOfPayValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:teaLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:teaValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:tceaLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:tceaValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:documentContractLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:documentSureLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:documentScheduleLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:optCordLabel_ withFontSize:TEXT_FONT_BIG_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:sealLabel_ withFontSize:TEXT_FONT_BIG_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleTextField:optCordTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:emailLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:acceptLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    
    quotaMonthLabel_.text = @"Cuota Mensual";
    termLabel_.text = @"Plazo";
    quotaTypeLabel_.text = @"Tipo de Cuota";
    dayOfPayLabel_.text = @"Día de Pago";
    teaLabel_.text = @"TEA";
    tceaLabel_.text = @"TCEA";
    documentContractLabel_.text = @"Contrato y Hoja de Resumen Informativa";
    documentSureLabel_.text = @"Seguro de Desgravamen";
    documentScheduleLabel_.text = @"Cronograma Resumen de Pagos";
    sealLabel_.text = @"Sello de Operaciones";
    acceptLabel_.text = @"He leído y acepto los documentos";
    legalTermsSwitch_.on = NO;
    
    legalTermsAlertImageView_.image = [[ImagesCache getInstance] imageNamed:BLUE_ICON_ALERT_NO_BORDER_IMAGE_FILE_NAME];
    
    [quotaMonthTipButton_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    [teaTipButton_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    [tceaTipButton_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    [optCordTipButton_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    [emailTipButton_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    
    [documentContractButton_ addTarget:self action:@selector(actionDocument:) forControlEvents:UIControlEventTouchUpInside];
    [documentSureButton_ addTarget:self action:@selector(actionDocument:) forControlEvents:UIControlEventTouchUpInside];
    [documentScheduleButton_ addTarget:self action:@selector(actionDocument:) forControlEvents:UIControlEventTouchUpInside];
    
    optCordTextField_.keyboardType = UIKeyboardTypeNumberPad;
    optCordTextField_.secureTextEntry = YES;
    optCordTextField_.delegate = self;
    
    
    [NXT_Peru_iPhoneStyler styleBlueButton:acceptButton_];
    [acceptButton_ setTitle:NSLocalizedString(CONFIRM_TEXT_KEY, nil) forState:UIControlStateNormal];
    [acceptButton_ addTarget:self action:@selector(acceptButtonTapped) forControlEvents:UIControlEventTouchUpInside];

    /**/
    
    CGRect containerFrame = containerView_.frame;
    containerFrame.origin.x = 0.0f;
    containerFrame.origin.y = 0.0f;
    containerView_.frame = containerFrame;
    
    [self setScrollableView:containerView_];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.origin.y = 0;
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
    [self layoutViews];
    
    [view bringSubviewToFront:brandingImageView_];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
    
    transparentScroll_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    
}



/*
 * Creates and returns an autoreleased FastLoanStepTwoViewController constructed from a NIB file
 */
+ (FastLoanStepTwoViewController *)FastLoanStepTwoViewController {
    
    FastLoanStepTwoViewController *result = [[[FastLoanStepTwoViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}


/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(FastLoanResponseReceived:)
                                                 name:kNotificationFastLoanStepTwoResponse
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(confirmationResponseReceived:)
                                                 name:kNotificationFastLoanStepThreeResponse
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(scheduleResponseReceived:)
                                                 name:kNotificationFastLoanScheduleResponse
                                               object:nil];
    
    [self displayStoredInformation];
    
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];

}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the
 * transfer last step confirmation notification
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationFastLoanStepThreeResponse
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationFastLoanStepTwoResponse
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationFastLoanScheduleResponse
                                                  object:nil];
    
  // [self storeInformationIntoHelper];
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingImageView] frame]);
    [self setScrollNominalFrame:scrollFrame];
    
    
    
}

/*
 * Lays out the views
 */
- (void)layoutViews {
    
    if ([self isViewLoaded]) {
        
        CGRect frame = grettingLabel_.frame;
        frame.origin.y = VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        grettingLabel_.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = grettingInfoLabel_.frame;
        frame.origin.y = auxPos;
        grettingInfoLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = separatorTop_.frame;
        frame.origin.y = auxPos;
        separatorTop_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        frame = quotaMonthLabel_.frame;
        frame.origin.y = auxPos;
        quotaMonthLabel_.frame = frame;
        
        frame = quotaMonthTipButton_.frame;
        frame.origin.y = auxPos - 8;
        quotaMonthTipButton_.frame = frame;
        
        frame = quotaMonthValueLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:quotaMonthValueLabel_ forText:quotaMonthValueLabel_.text];
        quotaMonthValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = termLabel_.frame;
        frame.origin.y = auxPos;
        termLabel_.frame = frame;
        
        frame = termValueLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:termValueLabel_ forText:termValueLabel_.text];
        termValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = quotaTypeLabel_.frame;
        frame.origin.y = auxPos;
        quotaTypeLabel_.frame = frame;
        
        frame = quotaTypeValueLabel_.frame;
        frame.size.height = [Tools labelHeight:quotaTypeValueLabel_ forText:quotaTypeValueLabel_.text];
        frame.origin.y = auxPos;
        quotaTypeValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = dayOfPayLabel_.frame;
        frame.origin.y = auxPos;
        dayOfPayLabel_.frame = frame;
        
        frame = dayOfPayValueLabel_.frame;
        frame.size.height = [Tools labelHeight:dayOfPayValueLabel_ forText:dayOfPayValueLabel_.text];
        frame.origin.y = auxPos;
        dayOfPayValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = teaLabel_.frame;
        frame.origin.y = auxPos;
        teaLabel_.frame = frame;
        
        frame = teaTipButton_.frame;
        frame.origin.y = auxPos - 8;
        teaTipButton_.frame = frame;
        
        frame = teaValueLabel_.frame;
        frame.size.height = [Tools labelHeight:teaValueLabel_ forText:teaValueLabel_.text];
        frame.origin.y = auxPos;
        teaValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = tceaLabel_.frame;
        frame.origin.y = auxPos;
        tceaLabel_.frame = frame;
        
        frame = tceaTipButton_.frame;
        frame.origin.y = auxPos - 8;
        tceaTipButton_.frame = frame;
        
        frame = tceaValueLabel_.frame;
        frame.size.height = [Tools labelHeight:tceaValueLabel_ forText:tceaValueLabel_.text];
        frame.origin.y = auxPos;
        tceaValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = documentsView_.frame;
        frame.origin.y = auxPos;
        documentsView_.frame = frame;
        auxPos = CGRectGetMaxY(frame);
        
        frame = legalTermsView_.frame;
        frame.origin.y = auxPos;
        legalTermsView_.frame = frame;
        auxPos = CGRectGetMaxY(frame);
        
        frame = optCordView_.frame;
        frame.origin.y = auxPos;
        optCordView_.frame = frame;
        auxPos = CGRectGetMaxY(frame);
        
        if(fastLoanConfirm_.seal && [fastLoanConfirm_.seal length]>0){
        
            frame = sealView_.frame;
            frame.origin.y = auxPos;
            sealView_.frame = frame;
            auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
            
            sealView_.hidden = NO;
            
        }
        else{
            auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
            
            sealView_.hidden = YES;
        }
        
        
        frame = acceptButton_.frame;
        frame.origin.y = auxPos;
        acceptButton_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
    
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.origin.y = 0;
        frame.size.height = auxPos;
        containerView.frame = frame;
    
        [self recalculateScroll];
    }
    
}

-(void)displayStoredInformation{
    
    grettingLabel_.text = [NSString stringWithFormat:@"%@, tu préstamo sería de %@ %@",
                           fastLoanConfirm_.customer,
                          [Tools clientCurrencySymbolForServerCurrency:fastLoanConfirm_.currencyAmount],
                           fastLoanConfirm_.amountFormated];
    grettingInfoLabel_.text = [NSString stringWithFormat:@"en la cuenta de ahorros **%@ (%@)",
                               [Tools obfuscateAccountNumber:fastLoanConfirm_.account],
                               [Tools getCurrencyLiteral:fastLoanConfirm_.currencyAccount]];
    
    quotaMonthValueLabel_.text = [NSString stringWithFormat:@"%@ %@",
                                  [Tools clientCurrencySymbolForServerCurrency:fastLoanConfirm_.currencyAmount],
                                  fastLoanConfirm_.quotaFormated];
    termValueLabel_.text = [NSString stringWithFormat:@"%@ meses", fastLoanConfirm_.term];
    
    NSString * months = fastLoanConfirm_.monthDobles!=nil ?
                        [NSString stringWithFormat:@" %@",fastLoanConfirm_.monthDobles] : @"";
    quotaTypeValueLabel_.text = [NSString stringWithFormat:@"%@%@",
                                 (fastLoanConfirm_.quotaType !=nil ? fastLoanConfirm_.quotaType : @""),
                                 months];
    dayOfPayValueLabel_.text = fastLoanConfirm_.dayOfPay;
    teaValueLabel_.text = [NSString stringWithFormat:@"%@%%", fastLoanConfirm_.teaFormated];
    tceaValueLabel_.text = [NSString stringWithFormat:@"%@%%", fastLoanConfirm_.tceaFormated];
    
    emailLabel_.text = [NSString stringWithFormat:@"(Se enviarán a %@)", fastLoanConfirm_.email];
    
    CGFloat posX = [emailLabel_.text sizeWithFont:emailLabel_.font].width + CGRectGetMinX(emailLabel_.frame);
    CGRect frame = emailTipButton_.frame;
    frame.origin.x = posX;
    emailTipButton_.frame = frame;
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];;
    NSString *title = @"";
    
    if (otpUsage == otp_UsageTC) {
        title = @"Tarjeta de Coordenadas";
        optCordTextField_.placeholder = [NSString stringWithFormat:@"Ingrese la coordenada %@ de tu tarjeta",
                                         fastLoanConfirm_.coordinate];
    } else if (otpUsage == otp_UsageOTP) {
        title = @"Clave SMS (enviada a tu celular)";
        optCordTextField_.placeholder = @"Ingrese los 6 dígitos de tu clave";
    }
    optCordLabel_.text = title;
    
    if(fastLoanConfirm_.seal && [fastLoanConfirm_.seal length]>0){
        
        NSData *data = [Tools base64DataFromString:fastLoanConfirm_.seal];
        UIImage *image = [[[UIImage alloc] initWithData:data] autorelease];
        [sealImageView_ setImage:image];
    }
    
    UIView *popButtonsView = [self popButtonsView];
    [optCordTextField_ setInputAccessoryView:popButtonsView];
    
    if (editableViews_ == nil) {
        editableViews_ = [[NSMutableArray alloc] init];
    } else {
        [editableViews_ removeAllObjects];
    }
    
    [editableViews_ addObject:optCordTextField_];
    
    [self layoutViews];

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initial configuration is set.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
    [self makeViewVisible:textField];
    
    if(textField == optCordTextField_){
        [NXT_Peru_iPhoneStyler styleLabel:optCordLabel_
                                    withFontSize:TEXT_FONT_BIG_SIZE
                                    color:[UIColor BBVABlueSpectrumToneTwoColor]];
        [NXT_Peru_iPhoneStyler styleTextField:optCordTextField_
                                 withFontSize:TEXT_FONT_SMALL_SIZE
                                     andColor:[UIColor BBVAGreyColor]];
        [NXT_Peru_iPhoneStyler removeError:optCordTextField_];
    }
    
    return YES;
    
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self editableViewHasBeenClicked:textField];
    
}

/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    if (otpUsage == otp_UsageTC) {
        
        if (resultLength <= COORDINATES_MAXIMUM_LENGHT) {
            
            result = YES;
            
        }
    }
    else if (otpUsage == otp_UsageOTP) {
        
        if(resultLength <= OTP_MAXIMUM_LENGHT) {
            
            if ([Tools isValidText:resultString forCharacterString:OTP_VALID_CHARACTERS]) {
                
                result = YES;
            }
        }
    }
    else {
        
        result = YES;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark UITextViewDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text view.
 *
 * @param textView The text view for which editing is about to begin.
 */
- (BOOL)textViewShouldBeginEditing:(UITextView*)textView {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
    }
    return YES;
}

/**
 * Tells the delegate that editing began for the specified text view.
 *
 * @param textView The text view for which an editing session began.
 */
- (void)textViewDidBeginEditing:(UITextView *)textView {

    [self editableViewHasBeenClicked:textView];
    
}



#pragma mark -
#pragma mark User Interaction

/*
 * Performs accept buttom action
 */
- (IBAction)acceptButtonTapped {
    
    [optCordTextField_ endEditing:YES];
    
    BOOL canStartProcess = YES;
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
    if(!legalTermsSwitch_.isOn){
        [NXT_Peru_iPhoneStyler styleLabel:acceptLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE
                                    color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
        [NXT_Peru_iPhoneStyler styleLabel:emailLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE
                                    color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];

        legalTermsAlertImageView_.image = [[ImagesCache getInstance] imageNamed:RED_ICON_ALERT_NO_BORDER_IMAGE_FILE_NAME];
        [self showErrorTooltipFromView:emailLabel_ message:@"Confirma que leíste y aceptas los documentos" delegate:self];
        canStartProcess = NO;
    }
    else if ((otpUsage == otp_UsageOTP) && ([[self secondFactorKey] length] == 0)) {
        [Tools showAlertWithMessage:@"Ingresa los 6 dígitos de tu Clave SMS." title:@""];
        canStartProcess = NO;
        
    } else if ((otpUsage == otp_UsageTC) && (([[self secondFactorKey] length] == 0) || ([[self secondFactorKey] length] < 3))) {
        [Tools showAlertWithMessage:@"Ingresa los 3 dígitos de la coordenada solicitada." title:@""];
        canStartProcess = NO;
        
    }
    
    if(canStartProcess){
        [self.appDelegate showActivityIndicator:poai_Both];
        [[Updater getInstance] fastLoanOperationSucess:[self secondFactorKey]];
    }
    
}

/*
 * Returns the second factor key
 */
- (NSString *)secondFactorKey {
    NSString *secondFactor = @"";
    secondFactor = optCordTextField_.text;
    return secondFactor;
    
}

- (IBAction)showTooltip:(UIView*)view {
    if(view == optCordTipButton_){
        OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
        if (otpUsage == otp_UsageTC) {
            [self showInfoTooltipFromView:optCordTipImageView_
                                  message:@"Al ingresar tu coordenada aceptas el préstamo y al presionar \"Confirmar\" podrás disponer de él."];
        }
        else if (otpUsage == otp_UsageOTP) {
            [self showInfoTooltipFromView:optCordTipImageView_
                                  message:@"Al ingresar tu Clave SMS aceptas el préstamo y al presionar \"Confirmar\" podrás disponer de él."];
        }
    }
    else if(view == quotaMonthTipButton_){
        [self showInfoTooltipFromView:view
                              message:@"No incluye seguro de desgravamen, el cual puede variar entre S/ 0.35 y S/16.50 (ver cuota total en cronograma de pagos)."];
    }
    else if(view == teaTipButton_){
        [self showInfoTooltipFromView:view
                              message:@"La Tasa de Interés Efectiva Anual calcula el costo o valor de interés esperado en 360 días."];
    }
    else if(view == tceaTipButton_){
        [self showInfoTooltipFromView:view
                              message:@"La Tasa de Costo Efectiva Anual representa el costo total que se pagará en 360 días, incluyendo TEA, comisiones, gastos y seguros."];
    }
    else if(view == emailTipButton_){
        [self showInfoTooltipFromView:view
                              message:@"En un máximo de 48 horas"];
    }
}

-(IBAction)actionDocument:(id)sender{
    if(sender==documentContractButton_){
        if (termsAndConsiderationsViewController_ == nil) {
            
            termsAndConsiderationsViewController_ = [[TermsAndConsiderationsViewController TermsAndConsiderationsViewController] retain];
            
        }
        [termsAndConsiderationsViewController_ setURLText:fastLoanConfirm_.disclaimerContract
                                                titleText:@"Préstamo al Toque"
                                               headerText:@"Contrato y Hoja de Resumen Informativa"];
        [self.navigationController pushViewController:termsAndConsiderationsViewController_ animated:YES];
    }
    else if(sender==documentSureButton_){
        if (termsAndConsiderationsViewController_ == nil) {
            
            termsAndConsiderationsViewController_ = [[TermsAndConsiderationsViewController TermsAndConsiderationsViewController] retain];
            
        }
        [termsAndConsiderationsViewController_ setURLText:fastLoanConfirm_.disclaimerASecured
                                                titleText:@"Préstamo al Toque"
                                               headerText:@"Seguro de Desgravamen"];
        [self.navigationController pushViewController:termsAndConsiderationsViewController_ animated:YES];
    }
    else if(sender==documentScheduleButton_){
        [self.appDelegate showActivityIndicator:poai_Both];
        [[Updater getInstance] fastLoanOperatioSchedule:@"0" quotaNumber:@"6"];//opAlternative siempre 0 , quotaNumber numero cuotas que deseo obtener n-1 primeras  + last
    }
    
}

#pragma TooltipDelegate


-(void)onDismiss:(UIView*)view{
    [NXT_Peru_iPhoneStyler styleLabel:emailLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:acceptLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    legalTermsAlertImageView_.image = [[ImagesCache getInstance] imageNamed:BLUE_ICON_ALERT_NO_BORDER_IMAGE_FILE_NAME];
}

#pragma mark -
#pragma mark Notifications management

/*
 * Invoked by framework when the response to the transfer operation is received. The information is analized
 */
- (void)confirmationResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    NSString * physicalError = @"Se presentaron inconvenientes al contratar tu Préstamo al Toque. Por favor, inténtalo más tarde.";
    
    if (![response isError]) {
        
        if ([response isKindOfClass:[FastLoanSummary class]]) {
            
            //Sucess
            FastLoanSummary *fastLoanSummary = (FastLoanSummary *)response;
            
             FastLoanStepThreeViewController * fastLoanStepThreeViewController = [[FastLoanStepThreeViewController FastLoanStepThreeViewController] retain];
             fastLoanStepThreeViewController.FastLoanSummary = fastLoanSummary;
             [[self navigationController] pushViewController:fastLoanStepThreeViewController animated:YES];
            
            
        }
        else{
            optCordTextField_.text = @"";
            
            [Tools showAlertWithMessage:physicalError
                                  title:@"" andDelegate:self];
        }
        
    } else {
        
        
        if([response errorCode] != nil  &&
           (    [[response errorCode] isEqualToString:@WRONG_CONFIRM_CODE_ERROR]
            || [[response errorCode] isEqualToString:@REMAIN_ONE_TRY_CONFIRM_CODE_ERROR]
            || [[response errorCode] isEqualToString:@SMS_CODE_EXPIRED_ERROR]
            || [[response errorCode] isEqualToString:@WRONG_COORDINATE_CONFIRM_CODE_ERROR]
            )){
               
               optCordTextField_.text = @"";
               
               NSString * message = [response errorMessage];
               if (message == nil || [@"" isEqualToString:message]) {
                   message = physicalError;// physical error
               }
               
               NSString *buttonText = NSLocalizedString(OK_TEXT_KEY, nil);
               UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"" message:message
                                                                   delegate:self cancelButtonTitle:buttonText otherButtonTitles:nil] autorelease];
               alertView.tag = 333;
               [alertView show];
           }
        else{
            
            optCordTextField_.text = @"";
            
            NSString * message = [response errorMessage];
            if (message == nil || [@"" isEqualToString:message]) {
                message = physicalError;// physical error
            }
            [Tools showAlertWithMessage:message title:@"" andDelegate:self];
        }
        
        
    }
    
}

- (BOOL)FastLoanResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    optCordTextField_.text = @"";
    
    StatusEnabledResponse *response = [notification object];
    
    BOOL result = NO;
    
    if (response && !response.isError) {
        
        if ([response isKindOfClass:[FastLoanConfirm class]]) {
            
            FastLoanConfirm *fastLoanConfirm = (FastLoanConfirm *)response;
            
            OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];;
            if (otpUsage == otp_UsageTC) {
                optCordTextField_.placeholder = [NSString stringWithFormat:@"Ingrese la coordenada %@ de tu tarjeta",
                                                 fastLoanConfirm.coordinate];
            }
            result = YES;
            
        }
    }
    else if(response && [response errorCode] != nil  &&
           [[response errorCode] isEqualToString:@MAX_TRY_CONFIRM_CODE_ERROR]){
        NSString * message =  response ? [response errorMessage] : nil;
        if (message == nil || [@"" isEqualToString:message]) {
            message = NSLocalizedString(COMMUNICATION_ERROR_KEY, nil);// physical error
        }
        [Tools showAlertWithMessage:message title:@"" andDelegate:self];
    }
    else{
        NSString * message =  response ? [response errorMessage] : nil;
        if (message == nil || [@"" isEqualToString:message]) {
            message = NSLocalizedString(COMMUNICATION_ERROR_KEY, nil);// physical error
        }
        [Tools showAlertWithMessage:message title:@"" andDelegate:self];
    }
    
    return result;
    
}

- (BOOL)scheduleResponseReceived:(NSNotification *)notification {
    [self.appDelegate hideActivityIndicator];
    StatusEnabledResponse *response = [notification object];
    
    BOOL result = NO;
    
    if (response && !response.isError) {
        if ([response isKindOfClass:[FastLoanSchedule class]]) {
            
            FastLoanSchedule *fastLoanSchedule = (FastLoanSchedule *)response;
            ScheduleView * scheduleView = [[ScheduleView alloc] init];
            [scheduleView setFastLoanSchedule:fastLoanSchedule];
            [scheduleView showInWindow:self.view.window];
            
            
        }
    }
    
    return result;
}

#pragma mark -
#pragma mark UIAlertViewDelegate methods

/**
 * Sent to the delegate when the user clicks a button on an alert view.
 *
 * @param alertView: The alert view containing the button.
 * @param buttonIndex: The index of the button that was clicked.
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 333){
        
        [self.appDelegate showActivityIndicator:poai_Both];
        [[Updater getInstance] fastLoanOperationConfirm];
    }
    else{
        [[self navigationController] popToRootViewControllerAnimated:YES];
    }
}


#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = @"Préstamo al Toque";
    return result;
    
}


@end
