//
//  FastLoanBenefitsViewController.h
//  NXT_Peru_iPhone
//
//  Created by Flavio Franco Tunqui on 2/23/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "NXTViewController.h"
#import "DownloadListener.h"
#import "NXTEditableViewController.h"

@class FastLoanBenefitCell;
@class FastLoanBenefitDetailCell;
@class FastLoanBenefitHelpCell;

/**
 * Controls the login view, which contains a table with options, a header and an ad section. Table cells are stored inside the view controller to ease
 * their management, as they are always visible and never reused
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface FastLoanBenefitsViewController : NXTEditableViewController <UITableViewDelegate, UITableViewDataSource> {
    
@private
    
    /**
     * Table view to contain all possible actions
     */
    UITableView *actionsTableView_;
    
    /**
     * Benefit information cell
     */
    FastLoanBenefitCell *fastLoanBenefitCell_;
    
    /**
     * BenefitDetail and branches locator action cell
     */
    FastLoanBenefitDetailCell *fastLoanBenefitDetailCell_;
    
    /**
     * Row selected
     */
    NSInteger rowSelected;
    
    /**
     * NSMutableArray Titles selected
     */
    NSMutableArray *arrTitles;
    
    /**
     * NSMutableArray Icons selected
     */
    NSMutableArray *arrIcons;
    
    /**
     * NSMutableArray Details selected
     */
    NSMutableArray *arrDetails;
}

/**
 * Provides read-write access to the table view to contain all possible actions and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *actionsTableView;

/**
 * Creates and returns an autoreleased FastLoanBenefitsViewController constructed from a NIB file
 *
 * @return The autoreleased FastLoanBenefitsViewController constructed from a NIB file
 */
+ (FastLoanBenefitsViewController *)fastLoanBenefitsViewController;

@end