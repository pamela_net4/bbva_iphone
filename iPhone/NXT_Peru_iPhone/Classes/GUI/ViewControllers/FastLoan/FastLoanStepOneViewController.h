//
//  FastLoanStepOneViewController.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 2/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "NXTEditableViewController.h"
#import "NXTComboButton.h"
#import "FastLoanStartup.h"
#import "SelectionItemsPopView.h"
#import "SimulationView.h"

@class SimpleHeaderView;
@class NXTComboButton;
@class NXTCurrencyTextField;
@class NXTTextField;

@interface FastLoanStepOneViewController : NXTEditableViewController<NXTComboButtonDelegate, UITextFieldDelegate, SelectionItemsPopViewDelegate, SimulationViewDelegate>{
    
@private
    
    /*
     * card label
     */
    UILabel *cardLabel_;
    
    /*
     * card combo
     */
    NXTComboButton *cardCombo_;
    
    UILabel *amountIndicationLabel_;
    
    UILabel *amountValueIndicationLabel_;
    
    NXTCurrencyTextField *amountTextField_;
    
    /*
     * email label
     */
    UILabel *emailLabel_;
    
    /*
     * email combo
     */
    NXTComboButton *emailCombo_;
    
    NXTTextField * emailTextField_;
    
    UIImageView *brandingImageView_;
    
    UIButton *emailTipButton_;
    
    UIButton *transferButton_;
    
    UIButton *editAmountButton_;
    
    UIButton *editEmailButton_;
    
    UIView * separatorEmailTop_;
    
    UIView * separatorEmailBottom_;

    UILabel *typeOfQuotaLabel_;
    
    NXTComboButton *typeOfQuotaCombo_;
    
    UILabel *termLabel_;
    
    NXTComboButton *termCombo_;
    
    UILabel *dayOfPayLabel_;
    
    NXTTextField * dayOfPayTextField_;
    
    UIButton *editDayOfPay_;
    
    UIView * moreInformationView_;
    
    UIView * changeTypeInformationView_;
    
    UIView * separatorMoreInformationBottom_;
    
    UIButton * moreInformationButton_;
    
    UIButton *editMonthsButton_;
    
    /**
     * Container view to contain the editable components
     */
    UIView *containerView_;
    
    FastLoanStartup *FastLoanStartup_;
    
    NSArray * selectionsMonths_;
    
    NSMutableArray * months_;

}

/**
 * Provides readwrite access to the cardLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *cardLabel;

/**
 * Provides readwrite access to the cardCombo button. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *cardCombo;

/**
 * Provides readwrite access to the amountIndicationLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *amountIndicationLabel;

/**
 * Provides readwrite access to the amountValueIndicationLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *amountValueIndicationLabel;

/**
 * Provides readwrite access to the amountTextField. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTCurrencyTextField *amountTextField;

/**
 * Provides readwrite access to the typeOfQuotaLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *typeOfQuotaLabel;

/**
 * Provides readwrite access to the typeOfQuotaCombo. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *typeOfQuotaCombo;

/**
 * Provides readwrite access to the dayOfPayLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *dayOfPayLabel;

/**
 * Provides readwrite access to the dayOfPayTextField. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTTextField * dayOfPayTextField;

/**
 * Provides readwrite access to the dayOfPayTipButton. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIButton *editDayOfPay;

/**
 * Provides readwrite access to the termLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *termLabel;

/**
 * Provides readwrite access to the termCombo. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *termCombo;

/**
 * Provides readwrite access to the emailLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *emailLabel;

/**
 * Provides readwrite access to the emailCombo button. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *emailCombo;
/**
 * Provides readwrite access to the email tip button. Exported to IB
 */
@property (nonatomic, retain) IBOutlet UIButton *emailTipButton;

/**
 * Provides readwrite access to the emailTextField. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTTextField * emailTextField;

/**
 * Provides readwrite access to the editAmountButton_. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIButton *editAmountButton;

/**
 * Provides readwrite access to the editEmailButton_. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIButton *editEmailButton;

/**
 * Provides readwrite access to the separatorEmailTop. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIView * separatorEmailTop;

/**
 * Provides readwrite access to the separatorEmailTop. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIView * separatorEmailBottom;

/**
 * Provides readwrite access to the moreInformationView. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIView * moreInformationView;

/**
 * Provides readwrite access to the changeTypeInformationView. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIView * changeTypeInformationView;

/**
 * Provides readwrite access to the separatorMoreInformationBottom. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIView * separatorMoreInformationBottom;

/**
 * Provides readwrite access to the moreInformationButton. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIButton * moreInformationButton;

/**
 * Provides readwrite access to the editMonths button. Exported to IB
 */
@property (nonatomic, retain) IBOutlet UIButton *editMonthsButton;

/**
 * Provides readwrite access to the transfer button. Exported to IB
 */
@property (nonatomic, retain) IBOutlet UIButton *transferButton;
/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, retain) IBOutlet UIImageView *brandingImageView;
/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, retain) IBOutlet UIView *containerView;

/**
 * Provides read-write access to the helper element
 */
@property (nonatomic, readwrite, retain) FastLoanStartup *FastLoanStartup;


+ (FastLoanStepOneViewController *)FastLoanStepOneViewController ;

@end
