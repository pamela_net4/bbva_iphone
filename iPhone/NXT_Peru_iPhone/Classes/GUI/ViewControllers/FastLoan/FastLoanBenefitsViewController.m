//
//  FastLoanBenefitsViewController.m
//  NXT_Peru_iPhone
//
//  Created by Flavio Franco Tunqui on 2/23/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "FastLoanBenefitsViewController.h"
#import "FastLoanBenefitCell.h"
#import "FastLoanBenefitDetailCell.h"
#import "BlueButtonCell.h"
#import "Constants.h"
#import "GeneralLoginResponse.h"
#import "IconActionCell.h"
#import "LoginInfoCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "LoginInfoCell.h"
#import "LoginViewController.h"
#import "Tools.h"
#import "NXTNavigationItem.h"
#import "Updater.h"
#import "MCBFacade.h"
#import "MOKUILimitedLengthTextField.h"
#import "LoginCoordinatesViewController.h"
#import "LoginInfoViewController.h"
#import "POIMapViewController.h"
#import "Session.h"
#import "UIColor+BBVA_Colors.h"
#import "YourOpinionMattersViewController.h"
#import "NXTEditableViewController+protected.h"
#import "TblFrequentCard.h"
#import "FrequentCard.h"
#import "GeneralLoginEncryptResponse.h"
#import "SplashRadioViewController.h"
#import "SlideOutNavigationViewController.h"


/**
 * Define the LoginViewController NIB file name
 */
#define NIB_FILE_NAME @"FastLoanBenefitsViewController"

/**
 * Minimum distance from pop buttons view top to edited view bottom
 */
#define MIN_VERTICAL_DISTANCE_TO_EDITING_VIEW                               80.0f

/**
 * Defines the advertising text size
 */
#define ADVERTISING_TEXT_SIZE                                                   13.0f

#define FONT_SIZE 14.0f
#define CELL_CONTENT_MARGIN 15.0f

#pragma mark -

/**
 * FastLoanBenefitsViewController private extension
 */

@interface FastLoanBenefitsViewController ()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releaseLoginViewControllerGraphicElements;

@end

@implementation FastLoanBenefitsViewController

#pragma mark -
#pragma mark Properties

@synthesize actionsTableView = actionsTableView_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc{
    
    [self releaseLoginViewControllerGraphicElements];
    
    // nothing
    
    [super dealloc];
}

/*
 * Releases elements not needed when controller view is unloaded
 */
- (void)releaseLoginViewControllerGraphicElements {

    [actionsTableView_ release];
    actionsTableView_ = nil;
    
    [editableViews_ removeAllObjects];
}

/**
 * Called when the controller’s view is released from memory. Unneeded memory is released
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseLoginViewControllerGraphicElements];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseLoginViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Sets the show login information cell flag its initial value
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
}

/*
 * Creates and returns an autoreleased LoginViewController constructed from a NIB file
 */
+ (FastLoanBenefitsViewController *)fastLoanBenefitsViewController {
    
    FastLoanBenefitsViewController *result = [[[FastLoanBenefitsViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory. View elements are initialized
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    actionsTableView_.frame = CGRectMake(0, 0, screenRect.size.width, screenRect.size.height - 66);
    
    UIView *view = self.view;
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    view.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    
    arrTitles = [[NSMutableArray alloc] init];
    [arrTitles addObject:@"Flexibilidad"];
    [arrTitles addObject:@"Cambio de Fecha de Pago"];
    [arrTitles addObject:@"Cuota Comodín"];
    [arrTitles addObject:@"Eliges tus cuotas"];
    [arrTitles addObject:@"Pago anticipado"];
    [arrTitles addObject:@"Seguros adicionales"];
    
    arrIcons = [[NSMutableArray alloc] init];
    [arrIcons addObject:@"icono-primera-cuota"];
    [arrIcons addObject:@"icono-fecha-pago"];
    [arrIcons addObject:@"icono-cuota-comodin"];
    [arrIcons addObject:@"icono-elige-cuotas"];
    [arrIcons addObject:@"icono-pago-anticipado"];
    [arrIcons addObject:@"icono-seguros-adicionales"];
    
    arrDetails = [[NSMutableArray alloc] init];
    [arrDetails addObject:@"Te financiamos a partir de S/.1, 000. Dispones de hasta 60 meses para pagar (sujeto a evaluación crediticia)."];
    [arrDetails addObject:@"Escoge la fecha que más te conviene para pagar tus cuotas. Puedes cambiar la fecha de pago desde el día siguiente del desembolso del préstamo, por un máximo de dos (02) veces al año y hasta diez veces (10) durante el plazo del préstamo.\nPara solicitar el Cambio de Fecha de Pago puedes llamar de Lunes a Viernes a Banca por Teléfono (01) 595-0000 opción 2, 2, 9 entre las 9 am y 6 pm. La solicitud solo se podrá hacer 48 horas antes a la fecha de vencimiento de la cuota.\nDeberás encontrarte al día en los pagos, no podrás solicitar “Cambio de Fecha de Pago” y “Cuota Comodín” a la vez, ni uno a continuación del otro salvo que entre las solicitudes se haya producido, por lo menos, el vencimiento y pago de una cuota. El uso de los beneficios, generará un nuevo Cronograma de Pagos que formará parte del contrato y podría generar incremento del monto total de intereses pagados."];
    [arrDetails addObject:@"Elige qué cuota quieres dejar de pagar en el mes que lo desees y ésta será adicionada al final de la vida del préstamo. Puedes usar la Cuota Comodín a partir de la 4ta cuota, por un máximo de dos (02) veces al año y hasta diez veces (10) durante el plazo del préstamo.\nPara solicitar la Cuota Comodín puedes llamar de Lunes a Viernes a Banca por Teléfono (01) 595-0000 opción 2, 2, 9 entre las 9 am y 6 pm. La solicitud solo se podrá hacer 48 horas antes a la fecha de vencimiento de la cuota.\nDeberás encontrarte al día en los pagos, no podrás solicitar “Cuota Comodín” y “Cambio de Fecha de Pago” a la vez, ni uno a continuación del otro salvo que entre las solicitudes se haya producido, por lo menos, el vencimiento y pago de una cuota. El uso de los beneficios, generará un nuevo Cronograma de Pagos que formará parte del contrato y podría generar incremento del monto total de intereses pagados."];
    [arrDetails addObject:@"Puedes hacer los pagos en cuotas fijas por todo el plazo del préstamo o en cuotas especiales de doble pago en los meses que previamente elijas."];
    [arrDetails addObject:@"Puedes cancelar anticipadamente el total de la deuda en cualquier momento, hacer amortizaciones parciales o adelantar el pago de cuotas (prepagos)."];
    [arrDetails addObject:@"Cuentas con un seguro de desgravamen que cancela la deuda con el banco en caso de fallecimiento, lo que permite reembolsar a los beneficiarios la parte del capital ya pagado."];
    
    rowSelected = -1;
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        self.navigationController.navigationBar.tintColor = [UIColor BBVABlueSpectrumColor];
        
    } else
    {
        self.navigationController.navigationBar.barTintColor = [UIColor BBVABlueSpectrumColor];
        self.navigationController.navigationBar.translucent = NO;
        
    }
    
    [self reloadScrollTableView];
}

- (void) reloadScrollTableView {
    [self setScrollableView:actionsTableView_];
    actionsTableView_.scrollEnabled = true;
//    
//    CGRect scrollFrame = [transparentScroll_ frame];
//    scrollFrame.origin.y = CGRectGetMinY(actionsTableView_.frame);
//    scrollFrame.size.height = actionsTableView_.frame.size.height;
//    [self setScrollNominalFrame:scrollFrame];
//    
//    CGRect containerFrame = actionsTableView_.frame;
//    containerFrame.origin = CGPointZero;
//    actionsTableView_.frame = containerFrame;
//    
//    transparentScroll_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
}

- (UINavigationItem *)navigationItem {
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = @"Beneficios de Préstamo al Toque";
    return result;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [actionsTableView_ reloadData];
    
    [Tools checkTableScrolling:actionsTableView_];
}


/**
 * Tells the data source to return the number of rows in a given section of a table view. When no options are expanded, only 3 cells are available,
 * when the login information cell is expanded, then an additional cell is diplayed, and when contact us options are expanded, two additional
 * cells are displayed
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section in tableView
 * @return The number of visible action cells
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. Cell type depends on its
 * location inside the table, but all of them are precreated
 *
 * @param tableView A table-view object requesting the cell
 * @param indexPath An index path locating a row in tableView
 * @return The MoreTabViewCell instance to represent the tab element
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ((indexPath.row % 2) == 0) {
        FastLoanBenefitCell *cell = (FastLoanBenefitCell *)[tableView dequeueReusableCellWithIdentifier:[FastLoanBenefitCell cellIdentifier]];
        
        if (cell == nil) {
            cell = [FastLoanBenefitCell fastLoanBenefitCell];
        }
        
        if (rowSelected == indexPath.row) {
            [cell setArrow:@"▲"];
        } else {
            [cell setArrow:@"▼"];
        }
        
        [cell setTitle:arrTitles[indexPath.row/2]];
        
        UIImage *image = [UIImage imageNamed:arrIcons[indexPath.row/2]];
        [cell setIcon:image];
        
        return cell;
    } else {
        FastLoanBenefitDetailCell *cell = (FastLoanBenefitDetailCell *)[tableView dequeueReusableCellWithIdentifier:[FastLoanBenefitDetailCell cellIdentifier]];
        
        if (cell == nil) {
            cell = [FastLoanBenefitDetailCell fastLoanBenefitDetailCell];
        }
        
        [cell setDetail:arrDetails[(indexPath.row - 1)/2]];
        
        [cell.detailTextView sizeToFit];
        [cell.detailTextView layoutIfNeeded];
        
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 106.0)];
    [footerView setBackgroundColor:[UIColor whiteColor]];
    
    UIView *line1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, footerView.frame.size.width, 1)];
    UIView *line2 = [[UIView alloc]initWithFrame:CGRectMake(0, 105, footerView.frame.size.width, 1)];
    
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(8, 8, footerView.frame.size.width - 16, 22)];
    fromLabel.text = @"¿Necesitas mayor información?";
    [NXT_Peru_iPhoneStyler styleLabel:fromLabel withFontSize:14.0 color:[UIColor BBVABlueColor]];
    
    UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(4, 30, footerView.frame.size.width - 8, 75)];
    textView.text = @"Comunícate con nuestra Banca por Teléfono al (01) 595-0000 o visita cualquiera de las oficinas del BBVA a nivel nacional.";
    [NXT_Peru_iPhoneStyler styleTextView:textView withFontSize:14.0 andColor:[UIColor BBVAGrayFourToneColor]];
    textView.textAlignment = NSTextAlignmentJustified;
    textView.editable = false;
    textView.selectable = true;
    textView.scrollEnabled = false;
    textView.dataDetectorTypes = UIDataDetectorTypePhoneNumber;
    
    [footerView addSubview:line1];
    [footerView addSubview:line2];
    [footerView addSubview:fromLabel];
    [footerView addSubview:textView];
    
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 106.0;
}

#pragma mark -
#pragma mark UITableViewDelegate methods

/**
 * Asks the delegate for the height to use for a row in a specified location. Depending on the cell type it has a different size
 *
 * @param tableView The table-view object requesting this information
 * @param indexPath An index path that locates a row in tableView
 * @return A floating-point value that specifies the height (in points) that row should be
 */

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ((indexPath.row % 2) == 0) {
        return 44.0f;
    } else {
        if (rowSelected != -1) {
            if ((rowSelected + 1) == indexPath.row) {

                NSString *text = arrDetails[(indexPath.row - 1)/2];
                
                CGSize constraint = CGSizeMake(self.view.frame.size.width - (CELL_CONTENT_MARGIN * 2), 20000.0f);
                
                CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
                
                CGFloat height = MAX(size.height, 44.0f);
                
                return height + (CELL_CONTENT_MARGIN * 2);
            } else {
                return 0.0f;
            }
        } else {
            return 0.0f;
        }
    }
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ((indexPath.row % 2) == 0)  {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        NSIndexPath *tempIndexPathPast = [[NSIndexPath indexPathForRow:rowSelected inSection:indexPath.section] retain];
        
        rowSelected = indexPath.row;
    
        NSIndexPath *tempIndexPath = [[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:indexPath.section] retain];
        
        [actionsTableView_ beginUpdates];
        [actionsTableView_ reloadRowsAtIndexPaths:[NSArray arrayWithObject:tempIndexPathPast] withRowAnimation:UITableViewRowAnimationAutomatic];
        [actionsTableView_ reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [actionsTableView_ reloadRowsAtIndexPaths:[NSArray arrayWithObject:tempIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [actionsTableView_ endUpdates];
    }
    
    [self reloadScrollTableView];
}

@end