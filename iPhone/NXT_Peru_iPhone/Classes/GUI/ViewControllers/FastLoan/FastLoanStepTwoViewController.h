//
//  FastLoanStepTwoViewController.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 5/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "NXTEditableViewController.h"
#import "FastLoanConfirm.h"
#import "TermsAndConsiderationsViewController.h"
@class NXTTextField;

@interface FastLoanStepTwoViewController : NXTEditableViewController<UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate>{
    
    @private
    
    /**
     * Container view
     */
    UIView *containerView_;
    
    UILabel * quotaMonthLabel_;
    
    UILabel * quotaMonthValueLabel_;
    
    UILabel * termLabel_;
    
    UILabel * termValueLabel_;
    
    UILabel * quotaTypeLabel_;
    
    UILabel * quotaTypeValueLabel_;
    
    UILabel * dayOfPayLabel_;
    
    UILabel * dayOfPayValueLabel_;
    
    UILabel * teaLabel_;
    
    UILabel * teaValueLabel_;
    
    UILabel * tceaLabel_;
    
    UILabel * tceaValueLabel_;
    
    UIButton * teaTipButton_;
    
    UIButton * tceaTipButton_;
    
    
    UIView * separatorTop_;
    
    UIButton * quotaMonthTipButton_;
    
    /**
     * Accept button
     */
    UIButton *acceptButton_;
    
    /*
     * gretting label
     */
    UILabel *grettingLabel_;
    
    UILabel *grettingInfoLabel_;
    
    /*
     * branding ImageView
     */
    UIImageView *brandingImageView_;
    
    UIView * legalTermsView_;
    
    UILabel * acceptLabel_;
    
    UILabel * emailLabel_;
    
    UIButton * emailTipButton_;
    
    UISwitch *legalTermsSwitch_;
    
    UIImageView * legalTermsAlertImageView_;
    
    
    UIView * documentsView_;
    
    UILabel * documentContractLabel_;
    
    UILabel * documentSureLabel_;
    
    UILabel * documentScheduleLabel_;
    
    UIButton * documentContractButton_;
    
    UIButton * documentSureButton_;
    
    UIButton * documentScheduleButton_;
    
    UIView * optCordView_;
    
    UIView * sealView_;
    
    UILabel * optCordLabel_;
    
    UIButton * optCordTipButton_;

    UIImageView *optCordTipImageView_;
    
    NXTTextField * optCordTextField_;
    
    UIImageView * sealImageView_;
    
     UILabel *sealLabel_;
    
    FastLoanConfirm *fastLoanConfirm_;
    
    FastLoanStepTwoViewController * FastLoanStepTwoViewController_;
    
    TermsAndConsiderationsViewController * termsAndConsiderationsViewController_;
}

/**
 * Provides readwrite access to the containerView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *containerView;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *grettingLabel;

/**
 * Provides readwrite access to the grettingInfoLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *grettingInfoLabel;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * quotaMonthLabel;

/**
 * Provides readwrite access to the quotaMonthValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * quotaMonthValueLabel;

/**
 * Provides readwrite access to the termLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * termLabel;

/**
 * Provides readwrite access to the termValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * termValueLabel;

/**
 * Provides readwrite access to the quotaTypeLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * quotaTypeLabel;

/**
 * Provides readwrite access to the quotaTypeValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * quotaTypeValueLabel;
/**
 * Provides readwrite access to the quotaTypeLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dayOfPayLabel;

/**
 * Provides readwrite access to the quotaTypeValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dayOfPayValueLabel;
/**
 * Provides readwrite access to the quotaTypeLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * teaLabel;

/**
 * Provides readwrite access to the quotaTypeValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * teaValueLabel;
/**
 * Provides readwrite access to the quotaTypeLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * tceaLabel;

/**
 * Provides readwrite access to the quotaTypeValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * tceaValueLabel;

/**
 * Provides readwrite access to the documentContractLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * documentContractLabel;

/**
 * Provides readwrite access to the documentSureLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * documentSureLabel;

/**
 * Provides readwrite access to the documentScheduleLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * documentScheduleLabel;

/**
 * Provides readwrite access to the documentContractButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton * documentContractButton;

/**
 * Provides readwrite access to the documentSureButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton * documentSureButton;

/**
 * Provides readwrite access to the documentScheduleButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton * documentScheduleButton;

/**
 * Provides readwrite access to the separatorTop. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView * separatorTop;

/**
 * Provides readwrite access to the acceptButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *acceptButton;

/**
 * Provides readwrite access to the brandingImageView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView *optCordTipImageView;

@property (nonatomic, readwrite, retain) IBOutlet UIView * optCordView;

@property (nonatomic, readwrite, retain) IBOutlet UIView * sealView;

@property (nonatomic, readwrite, retain) IBOutlet UIView * documentsView;

@property (nonatomic, readwrite, retain) IBOutlet UILabel * optCordLabel;

@property (nonatomic, readwrite, retain) IBOutlet UIButton * optCordTipButton;

@property (nonatomic, readwrite, retain) IBOutlet UIButton * teaTipButton;

@property (nonatomic, readwrite, retain) IBOutlet UIButton * tceaTipButton;

@property (nonatomic, readwrite, retain) IBOutlet NXTTextField * optCordTextField;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView * sealImageView;

@property (nonatomic, readwrite, retain) IBOutlet UIButton * quotaMonthTipButton;

@property (nonatomic, readwrite, retain) IBOutlet UILabel * sealLabel;

@property (nonatomic, readwrite, retain) IBOutlet UIView * legalTermsView;

@property (nonatomic, readwrite, retain) IBOutlet UILabel * acceptLabel;

@property (nonatomic, readwrite, retain) IBOutlet UILabel * emailLabel;

@property (nonatomic, readwrite, retain) IBOutlet UIButton * emailTipButton;

@property (nonatomic, readwrite, retain) IBOutlet UISwitch *legalTermsSwitch;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView * legalTermsAlertImageView;

/**
 * Provides read-write access to the helper element
 */
@property (nonatomic, readwrite, retain) FastLoanConfirm *fastLoanConfirm;

/**
 * Creates and returns an autoreleased FastLoanStepTwoViewController constructed from a NIB file
 *
 * @return The autoreleased FastLoanStepTwoViewController constructed from a NIB file
 */
+ (FastLoanStepTwoViewController *)FastLoanStepTwoViewController;

@end
