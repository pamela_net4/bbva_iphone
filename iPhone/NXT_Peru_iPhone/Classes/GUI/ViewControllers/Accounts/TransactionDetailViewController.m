/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TransactionDetailViewController.h"
#import "AccountTransaction.h"
#import "AccountTransactionDetail.h"
#import "BankAccount.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTNavigationItem.h"
#import "NXTSwipeGestureRecognizer.h"
#import "OptionCell.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"

#define ANIMATION_DURATION                              0.5f

#define DETAILS_NUMBER                                  8

#define NIB_NAME                                        @"TransactionDetailViewController"

@interface TransactionDetailViewController()

/**
 * Releases graphic element
 */
- (void)releaseTransactionDetailViewControllerGraphicElements;

/**
 * Chnages the state of the buttons of the movements navigation
 */
- (void)changeStateOfMovementsNavigationButtons;

/**
 * Determines the options to show
 */
- (void)determineOptions;

/**
 * Shows the option of custom payment
 */
- (void)optionCustomPayment;

/**
 * Shows the option of send
 */
- (void)optionSend;

/**
 * Shows the option of duplicate
 */
- (void)optionCopy;

/**
 * Shows the option of share payment
 */
- (void)optionSharePayment;

/**
 * Returns an array formed with the selected transactions
 *
 * @param aTransction The selected transaction
 */
- (NSArray *)informationArrayForTransaction:(AccountTransaction *)aTransaction;

@end

@implementation TransactionDetailViewController

@synthesize optionsTable = optionsTable_;
@synthesize previousMovementButton = previousMovementButton_;
@synthesize nextMovementButton = nextMovementButton_;
@synthesize separatorLine = separatorLine_;
@synthesize bottomBrandingLine = bottomBrandingLine_;
@synthesize movementsList = movementsList_;
@synthesize selectedMovement = selectedMovement_;
@synthesize account = account_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self releaseTransactionDetailViewControllerGraphicElements];
    
    [movementsList_ release];
    movementsList_ = nil;
    
    [selectedMovement_ release];
    selectedMovement_ = nil;
    
    [auxTransaction_ release];
    auxTransaction_ = nil;
    
    [optionsArray_ release];
    optionsArray_ = nil;
    
    [allOptionsArray_ release];
    allOptionsArray_ = nil;
    
    [account_ release];
    account_ = nil;
    
    [super dealloc];
}

/**
 * Releases graphic element
 */
- (void)releaseTransactionDetailViewControllerGraphicElements {

    [optionsTable_ release];
    optionsTable_ = nil;
    
    [previousMovementButton_ release];
    previousMovementButton_ = nil;
    
    [nextMovementButton_ release];
    nextMovementButton_ = nil;
    
    [detailView_ release];
    detailView_ = nil;
    
    [auxDetailView_ release];
    auxDetailView_ = nil;
    
    [separatorLine_ release];
    separatorLine_ = nil;
    
    [bottomBrandingLine_ release];
    bottomBrandingLine_ = nil;

}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransactionDetailViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}


#pragma mark -
#pragma mark Instance initialization

/*
 * Creates a new autoreleased instance from the associated NIB file
 */
+ (TransactionDetailViewController *)transactionDetailViewController {
	return [[[TransactionDetailViewController alloc] initWithNibName:NIB_NAME bundle:nil] autorelease];
}

#pragma mark -
#pragma mark View managment

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
    [NXT_Peru_iPhoneStyler styleTableView:optionsTable_];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    separatorLine_.image = [imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    bottomBrandingLine_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    

    
    optionsTable_.scrollEnabled = NO;
    
    [detailView_ release];
    detailView_ = [[TransactionDetailView transactionDetailView] retain];
    detailView_.frame = CGRectMake(31.0f, 0.0f, [TransactionDetailView width], [TransactionDetailView heightForDetailsNumber:DETAILS_NUMBER]);
    detailView_.delegate = self;
    [self.view addSubview:detailView_];
    
    //layout
    
    nextMovementButton_.frame = CGRectMake(nextMovementButton_.frame.origin.x, nextMovementButton_.frame.origin.y, nextMovementButton_.frame.size.width, detailView_.frame.size.height);
    previousMovementButton_.frame = CGRectMake(previousMovementButton_.frame.origin.x, previousMovementButton_.frame.origin.y, previousMovementButton_.frame.size.width, detailView_.frame.size.height);
    
    [nextMovementButton_ setImage:[imagesCache imageNamed:ARROW_ICON_IMAGE_FILE_NAME] forState:UIControlStateNormal];
    [nextMovementButton_ setBackgroundColor:[UIColor whiteColor]];
    [previousMovementButton_ setImage:[imagesCache imageNamed:ARROW_ICON_LEFT_IMAGE_FILE_NAME] forState:UIControlStateNormal];
    [previousMovementButton_ setBackgroundColor:[UIColor whiteColor]];
    
    separatorLine_.frame = CGRectMake(separatorLine_.frame.origin.x, detailView_.frame.size.height, separatorLine_.frame.size.width, separatorLine_.frame.size.height);
    
    optionsTable_.frame = CGRectMake(optionsTable_.frame.origin.x, separatorLine_.frame.origin.y + separatorLine_.frame.size.height, optionsTable_.frame.size.width, optionsTable_.frame.size.height);
    
    //
    
    [optionsArray_ release];
    optionsArray_ = [[NSMutableArray alloc] init];
    
    [allOptionsArray_ release];
    allOptionsArray_ = [[NSMutableArray alloc] init];
 
    OptionCell *option = [OptionCell optionCell];
    option.leftText = NSLocalizedString(TRANSACTION_ACCOUNT_DETAIL_SEND_KEY, nil);
    option.rightText = NSLocalizedString(TRANSACTION_ACCOUNT_DETAIL_SEND_SUBTITLE_KEY, nil);
    option.icon = [imagesCache imageNamed:ICON_SHARE_PAYMENT_FILE_NAME];
    [allOptionsArray_ addObject:option];
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
   
    [super viewDidUnload];
    
    [self releaseTransactionDetailViewControllerGraphicElements];
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    NSArray *informationArray = [self informationArrayForTransaction:selectedMovement_];
    detailView_.information = informationArray;
    [self changeStateOfMovementsNavigationButtons];
    [self determineOptions];

    [[self view] bringSubviewToFront:separatorLine_];
    [[self view] bringSubviewToFront:optionsTable_];

}

/*
 * Chnages the state of the buttons of the movements navigation
 */
- (void)changeStateOfMovementsNavigationButtons {
    if ([movementsList_ indexOfObject:selectedMovement_] == 0) {
        previousMovementButton_.hidden = YES;
    } else {
        previousMovementButton_.hidden = NO;
    }
    
    if ([movementsList_ indexOfObject:selectedMovement_] == [movementsList_ count] - 1) {
        nextMovementButton_.hidden = YES;
    } else {
        nextMovementButton_.hidden = NO;
    }
}

#pragma mark -
#pragma mark UITableView methods

/**	
 * Asks the data source to return the number of sections in the table view
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OptionCell *result = [optionsArray_ objectAtIndex:indexPath.row];
    
    result.leftText = NSLocalizedString(TRANSACTION_ACCOUNT_DETAIL_SEND_KEY, nil);
    result.rightText = NSLocalizedString(TRANSACTION_ACCOUNT_DETAIL_SEND_SUBTITLE_KEY, nil);
    result.icon = [[ImagesCache getInstance] imageNamed:ICON_SHARE_PAYMENT_FILE_NAME];
    
    return result;
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES]; 

    if (indexPath.row == 0) {
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *mailController = [[[MFMailComposeViewController alloc] init] autorelease];
            
            mailController.mailComposeDelegate = self;
            [mailController setSubject:[selectedMovement_ literal]];
            
            NSString *amount = [NSString stringWithFormat:@"%@ %@",
                                [Tools clientCurrencySymbolForServerCurrency:[selectedMovement_ currency]],
                                [selectedMovement_ amountString]];
            NSString *emailBosy = [NSString stringWithFormat:@"BBVA Continental - *%@ - %@ - %@", 
                                   [Tools notNilString:[selectedMovement_ literal]], 
                                   [Tools notNilString:amount], 
                                   [Tools notNilString:[selectedMovement_ valueDateString]]];
            
            [mailController setMessageBody:emailBosy
                                    isHTML:NO];
            mailController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
            
            [self presentViewController:mailController animated:TRUE completion:^{
                [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
            }];
            
        } else {
			
			[Tools showErrorWithMessage:NSLocalizedString(ERROR_SENDING_MAIL_TEXT_KEY, nil)];
			
		}
    }
    
//    OptionCell *selectedCell = (OptionCell *)[tableView cellForRowAtIndexPath:indexPath];
//    
//    if (selectedCell.optionSelector != @selector(optionCustomPayment)) {
//        [self performSelector:selectedCell.optionSelector];
//    }
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [optionsArray_ count];
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [OptionCell cellHeight];
}

/*
 * Determines the options to show
 */
- (void)determineOptions {
    if (optionsArray_ != nil) {
        [optionsArray_ release];
        optionsArray_ = nil;
    }
    optionsArray_ = [allOptionsArray_ mutableCopy];
    
    if (selectedMovement_ != nil) {
        
        
    }
    
    [optionsTable_ reloadData];
}

#pragma mark -
#pragma mark Information management

/**
 * Returns an array formed with the selected transactions
 *
 * @param aTransaction The selected transaction
 * @return A information array for transaction.
 */
- (NSArray *)informationArrayForTransaction:(AccountTransaction *)aTransaction {

    NSMutableArray *informationArray = [NSMutableArray array];
    
    NSString *detail = @"";
    
    // Amount
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    [titleAndAttributes setTitleString:[NSString stringWithFormat:@"%@ %@",
                                        [Tools clientCurrencySymbolForServerCurrency:[aTransaction currency]],
                                        [aTransaction amountString]]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    // Name
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    if ([aTransaction literal] != nil) {
        detail = [aTransaction literal];
    } else {
        detail = @"";
    }
    
    [titleAndAttributes setTitleString:[Tools notNilString:detail]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    
    // Line 1
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    [titleAndAttributes setTitleString:NSLocalizedString(TRANSACTION_ACCOUNT_DETAIL_OP_DESCRIPTION_KEY, nil)];
    
    if ([aTransaction deatilDescription] != nil) {
        detail = [aTransaction deatilDescription];
    } else {
        detail = @"";
    }
    
    [titleAndAttributes addAttribute:[Tools notNilString:detail]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    // Line 2
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    [titleAndAttributes setTitleString:NSLocalizedString(TRANSACTION_ACCOUNT_DETAIL_MOV_NUMBER_KEY, nil)];
    
    if ([aTransaction number] != nil) {
        detail = [aTransaction number];
    } else {
        detail = @"";
    }
    
    [titleAndAttributes addAttribute:[Tools notNilString:detail]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    // Line 3
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    [titleAndAttributes setTitleString:NSLocalizedString(TRANSACTION_ACCOUNT_DETAIL_CHECK_NUMBER_KEY, nil)];
    
    if ([aTransaction checkNumber] != nil) {
        detail = [aTransaction checkNumber];
    } else {
        detail = @"";
    }
    
    [titleAndAttributes addAttribute:[Tools notNilString:detail]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    // Line 4
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    [titleAndAttributes setTitleString:NSLocalizedString(TRANSACTION_ACCOUNT_DETAIL_TYPE_KEY, nil)];
    
    if ([aTransaction type] != nil) {
        detail = [aTransaction type];
    } else {
        detail = @"";
    }
    
    [titleAndAttributes addAttribute:[Tools notNilString:detail]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    // Line 5
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    [titleAndAttributes setTitleString:NSLocalizedString(TRANSACTION_ACCOUNT_DETAIL_VALUE_DATE_KEY, nil)];

    if ([aTransaction number] != nil) {
        detail = [aTransaction valueDateString];
    } else {
        detail = @"";
    }
    
    [titleAndAttributes addAttribute:[Tools notNilString:detail]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    // Line 6
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    [titleAndAttributes setTitleString:NSLocalizedString(TRANSACTION_ACCOUNT_DETAIL_CONTABLE_DATE_KEY, nil)];
    
    if ([aTransaction accountingDateString] != nil) {
        detail = [aTransaction accountingDateString];
    } else {
        detail = @"";
    }
    
    [titleAndAttributes addAttribute:[Tools notNilString:detail]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    // Line 7
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    [titleAndAttributes setTitleString:NSLocalizedString(TRANSACTION_ACCOUNT_DETAIL_DATE_TIME_KEY, nil)];
    
    if ([aTransaction operationHour] != nil) {
        detail = [aTransaction operationHour];
    } else {
        detail = @"";
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:[aTransaction valueDateString]],detail]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    // Line 8
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    [titleAndAttributes setTitleString:NSLocalizedString(TRANSACTION_ACCOUNT_DETAIL_CENTER_KEY, nil)];    
    
    if ([aTransaction center] != nil) {
        detail = [aTransaction center];
    } else {
        detail = @"";
    }
    
    [titleAndAttributes addAttribute:[Tools notNilString:detail]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }    
    
    return informationArray;

} 


#pragma mark -
#pragma mark User interaction

/*
 * Shows the option of custom payment
 */
- (void)optionCustomPayment {
    

    
}

/*
 * Shows the option of send
 */
- (void)optionSend {
}

/*
 * Shows the option of duplicate
 */
- (void)optionCopy {
}

/*
 * Shows the option of share payment
 */
- (void)optionSharePayment {
    
}

/*
 * The previous movement button has been pressed
 */
- (IBAction)previousMovementButtonPressed {
    self.view.userInteractionEnabled = NO;
    
    [auxDetailView_ release];
    auxDetailView_ = [[TransactionDetailView transactionDetailView] retain];    
    
    if (auxTransaction_ != nil) {
        [auxTransaction_ release];
        auxTransaction_ = nil;
    }
    
    auxTransaction_ = [[movementsList_ objectAtIndex:[movementsList_ indexOfObject:selectedMovement_] - 1] retain];
    
    auxDetailView_.alpha = 0.0f;
    auxDetailView_.frame = CGRectMake(0.0f - [TransactionDetailView width], 0.0f, [TransactionDetailView width], [TransactionDetailView heightForDetailsNumber:DETAILS_NUMBER]);
    
    previousMovementButton_.frame = CGRectMake(previousMovementButton_.frame.origin.x, previousMovementButton_.frame.origin.y, previousMovementButton_.frame.size.width, auxDetailView_.frame.size.height);
    nextMovementButton_.frame = CGRectMake(nextMovementButton_.frame.origin.x, nextMovementButton_.frame.origin.y, nextMovementButton_.frame.size.width, auxDetailView_.frame.size.height);

    NSArray *informationArray = [self informationArrayForTransaction:auxTransaction_ ];
    auxDetailView_.information = informationArray;
    [self.view addSubview:auxDetailView_];
    
    [self.view bringSubviewToFront:previousMovementButton_];
    [self.view bringSubviewToFront:nextMovementButton_];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:ANIMATION_DURATION];
        
    detailView_.frame = CGRectMake(self.view.frame.size.width + [TransactionDetailView width], 0.0f, [TransactionDetailView width], [TransactionDetailView heightForDetailsNumber:DETAILS_NUMBER]);
    detailView_.alpha = 0.0f;
    auxDetailView_.frame = CGRectMake(31.0f, 0.0f, [TransactionDetailView width], [TransactionDetailView heightForDetailsNumber:DETAILS_NUMBER]);
    auxDetailView_.alpha = 1.0f;
    
    [UIView commitAnimations];
}

/*
 * The next movement button has been pressed
 */
- (IBAction)nextMovementButtonPressed {
    self.view.userInteractionEnabled = NO;
    
    [auxDetailView_ release];
    auxDetailView_ = [[TransactionDetailView transactionDetailView] retain];
    
    if (auxTransaction_ != nil) {
        [auxTransaction_ release];
        auxTransaction_ = nil;
    }
    
    auxTransaction_ = [[movementsList_ objectAtIndex:[movementsList_ indexOfObject:selectedMovement_] + 1] retain];

    auxDetailView_.alpha = 0.0f;
    auxDetailView_.frame = CGRectMake(self.view.frame.size.width + [TransactionDetailView width], 0.0f, [TransactionDetailView width], [TransactionDetailView heightForDetailsNumber:DETAILS_NUMBER]);
    
    NSArray *informationArray = [self informationArrayForTransaction:auxTransaction_];
    
    auxDetailView_.information = informationArray;
    [self.view addSubview:auxDetailView_];
    
    [self.view bringSubviewToFront:previousMovementButton_];
    [self.view bringSubviewToFront:nextMovementButton_];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:ANIMATION_DURATION];
    
    detailView_.frame = CGRectMake(0.0f - [TransactionDetailView width], 0.0f, [TransactionDetailView width], [TransactionDetailView heightForDetailsNumber:DETAILS_NUMBER]);
    detailView_.alpha = 0.0f;
    auxDetailView_.frame = CGRectMake(31.0f, 0.0f, [TransactionDetailView width], [TransactionDetailView heightForDetailsNumber:DETAILS_NUMBER]);
    auxDetailView_.alpha = 1.0f;
    
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark Animations

/*
 * The animation has ended
 */
- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    detailView_.alpha = 1.0f;
    detailView_.information = auxDetailView_.information;
    detailView_.frame = CGRectMake(31.0f, 0.0f, [TransactionDetailView width], [TransactionDetailView heightForDetailsNumber:DETAILS_NUMBER]);
    
    self.selectedMovement = auxTransaction_;
    
    [auxDetailView_ removeFromSuperview];
    [auxDetailView_ release];
    auxDetailView_ = nil;
    
    self.view.userInteractionEnabled = YES;
    [self changeStateOfMovementsNavigationButtons];
    [self determineOptions];
}

#pragma mark -
#pragma mark TransactionDetailViewDelegate methods

/**
 * The swipe has been done on the given direction
 *
 * @param direction of the swipe
 */
- (void)swipeGestureDoneOnDirection:(SwipeDirection)direction {
    if (direction == swipe_Left) {
        
        if ([movementsList_ indexOfObject:selectedMovement_] < [movementsList_ count] - 1){
            [self nextMovementButtonPressed];
        }
        
    } else if (direction == swipe_Right) {
        
        if ([movementsList_ indexOfObject:selectedMovement_] > 0){
            [self previousMovementButtonPressed];
        }
        
    }
}


#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate protocol selectors

/**
 * Tells the delegate that the user wants to dismiss the mail composition view. The message composer is dismissed from view
 *
 * @param controller The view controller object managing the mail composition view
 * @param result The result of the user’s action
 * @param error If an error occurred, this parameter contains an error object with information about the type of failure
 */
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    [self dismissModalViewControllerAnimated:YES];
    
}

#pragma mark -
#pragma mark Properties methods


/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    
    result.customTitleView.topLabelText = [account_ accountType];
    result.customTitleView.bottomLabelText = [account_ number];
    
    return result;
    
}

/*
 * Sets the bank account
 */
- (void)setAccount:(BankAccount *)account {
    
    if (account_ != account) {
        [account_ release];
        account_ = [account retain];
    }
    
}

@end
