/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTViewController.h"
#import "NXTAccountGraphic.h"


//Forward declarations
@class NXTAccountGraphicInformation;
@class BankAccount;


/**
 * View controller to display an account balance graphic
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountGraphicViewController : NXTViewController <NXTAccountGraphicDelegate, UIAlertViewDelegate> {

@private
    
    /**
     * Graphic view that renders the account balance graphic
     */
    NXTAccountGraphic *accountGraphicView_;
    
    /**
     * Alert view to display a helper text to the user
     */
    UIAlertView *alertView_;
    
    /**
     * Account being displayed
     */
    BankAccount *account_;
    
    /**
     * Account graphic information to display
     */
    NXTAccountGraphicInformation *accountGraphicInformation_;
    
    /**
     * Currency symbol
     */
    NSString *currencySymbol_;
    
    /**
     * Currency follows the amount flag. If YES, the currency symbol is displayed after the amount (i.e. 10€), otherwise, the currency symbol is displayed before the amount (i.e. $10)
     */
    BOOL currencyFollows_;
    
    /**
     * Navigation bar was hidden before the view was displayed
     */
    BOOL navigationBarHidden_;
    
    /**
     * Status bar was hidden before the view was displayed
     */
    BOOL statusBarHidden_;
    
    /**
     * Animating alert view display flag. YES when the alert view is animating to show, NO otherwise
     */
    BOOL animatingAlertView_;
    
    /**
     * Must return back once the alert view has been animated
     */
    BOOL mustReturnOnceAlertViewDisplayer_;
    
    /**
     * Original status bar orientation
     */
    UIInterfaceOrientation originalStatusBarOrientation_;
    
    /**
     * Original device orientation
     */
    UIDeviceOrientation originalDeviceOrientation_;
    
    /**
     * From graphic button flag. YES when the view was displayed from the graphic button, NO when it wasdisplayed by rotating the device.
     */
    BOOL fromGraphicButton_;
    


}

/**
 * Provides read-write access to the graphic view that renders the account balance graphic and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTAccountGraphic *accountGraphicView;

/**
 * Provides read-write access to the rom graphic button flag.
 */
@property (nonatomic, readwrite, assign) BOOL fromGraphicButton;


/**
 * Creates and returns an autoreleased AccountGraphicViewController constructed from a NIB file
 *
 * @return The autoreleased AccountGraphicViewController constructed from a NIB file
 */
+ (AccountGraphicViewController *)accountGraphicViewController;

/**
 * Displays the graphic for the given graphic information. The information instance is retained so it must not be altered once set
 *
 * @param anAccount The account to display
 * @param anAccountGraphicInformation The account graphic information to display
 * @param aCurrencySymbol The currency symbol to display with all the amounts
 * @param currencyFollows YES to display the currency to the amount right (i.e. 10€), NO to display the currency to the amount left (i.e. $10)
 */
- (void)displayGraphicForAccount:(BankAccount *)anAccount
                     information:(NXTAccountGraphicInformation *)anAccountGraphicInformation
                    withCurrency:(NSString *)aCurrencySymbol
                     currencyFollows:(BOOL)currencyFollows;

@end
