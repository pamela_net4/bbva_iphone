/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "AccountGraphicViewController.h"

#import "AccountList.h"
#import "AccountTransaction.h"
#import "AccountTransactionList.h"
#import "AccountTransactionsListViewController.h"
#import "AccountTransactionsResponse.h"
#import "BankAccount.h"
#import "NXTAccountGraphic.h"
#import "NXTAccountGraphicInformation.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Tools.h"
#import "TransactionsFilterModalViewController.h"
#import "Session.h"
#import "StringKeys.h"
#import "Updater.h"
#import "UIColor+BBVA_Colors.h"



/**
 * Define the AccountGraphicViewController NIB file name
 */
#define NIB_FILE_NAME                                                           @"AccountGraphicViewController"


#pragma mark -

/**
 * AccountGraphicViewController private category
 */
@interface AccountGraphicViewController(private)

/**
 * Releases visual elements not needed when view controller view is unloaded
 */
- (void)releaseVisualElements;

/**
 * Receives the notification of the orientation changes
 */
- (void)orientationChanged:(NSNotification *)notification;

/**
 * Downloads more graphic information
 */
- (void)downloadGraphicInformation;

/**
 * The account has been updated with the transactions list. The graphic information is updated
 *
 * @param notification The notification instance containing notification informtion
 */
-(void)accountUpdated:(NSNotification *)notification;

/**
 * Returns to the previous view controller
 */
- (void)backToPreviousViewController;

@end


#pragma mark -

@implementation AccountGraphicViewController

#pragma mark -
#pragma mark Properties

@synthesize accountGraphicView = accountGraphicView_;
@synthesize fromGraphicButton = fromGraphicButton_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
 - (void)dealloc {
     
     [self releaseVisualElements];
     
     [account_ release];
     account_ = nil;
     
     [accountGraphicInformation_ release];
     accountGraphicInformation_ = nil;
     
     [currencySymbol_ release];
     currencySymbol_ = nil;
     
     [super dealloc];
     
 }

/**
 * Called when the controller’s view is released from memory. Visual elements are released
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseVisualElements];
    
}

/*
 * Releases visual elements not needed when view controller view is unloaded
 */
- (void)releaseVisualElements {
    
    [accountGraphicView_ release];
    accountGraphicView_ = nil;
    
    [alertView_ release];
    alertView_ = nil;
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseVisualElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased AccountGraphicViewController constructed from a NIB file
 */
+ (AccountGraphicViewController *)accountGraphicViewController {
    
    AccountGraphicViewController *result = [[[AccountGraphicViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];

    return result;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory. The information is set to the graphic
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [accountGraphicView_ displayGraphicForInformation:accountGraphicInformation_ withCurrency:currencySymbol_ currencyFollows:currencyFollows_];
    self.view.transform = CGAffineTransformMakeRotation(M_PI / 2.0f);
    
    [alertView_ release];
    alertView_ = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(ACCOUNT_GRAPHIC_HELPER_TEXT_KEY, nil)
                                           delegate:self cancelButtonTitle:NSLocalizedString(CLOSE_TEXT_KEY, nil) otherButtonTitles:nil];
    
}

#pragma mark -
#pragma mark View events

/**
 * Notifies the view controller that its view is about to be become visible. The navigation and the status bar are hidden
 *
 * @param animated If YES, the view is being added to the window using an animation
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if ([UIDevice currentDevice].orientation == UIInterfaceOrientationLandscapeRight) {
        self.view.transform = CGAffineTransformMakeRotation(M_PI_2);
    } else if ([UIDevice currentDevice].orientation == UIInterfaceOrientationLandscapeLeft) {
        self.view.transform = CGAffineTransformMakeRotation(-M_PI_2);
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    UIApplication *application = [UIApplication sharedApplication];
    
    if (!application.statusBarHidden) {
        
#if defined(__IPHONE_3_2) && (__IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2)
        [application setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
#else
        if ([application respondsToSelector:@selector(setStatusBarHidden:withAnimation:)]) {
            
            [application setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
            
        } else {
            
            [application setStatusBarHidden:YES animated:NO];
            
        }
#endif
        
    }
    
    UINavigationController *navigationController = self.navigationController;
    if (!navigationController.navigationBarHidden) {
        
        [navigationController setNavigationBarHidden:YES animated:NO];
        
    }
    
    [self.appDelegate.connectedTabBarViewController setTabBarVisibility:NO animated:NO];
    
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    originalDeviceOrientation_ = deviceOrientation;
    
    if (deviceOrientation == UIDeviceOrientationLandscapeRight) {
        
        originalStatusBarOrientation_ = UIInterfaceOrientationLandscapeLeft;
        
    } else {
        
        originalStatusBarOrientation_ = UIInterfaceOrientationLandscapeRight;
        
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:originalDeviceOrientation_] forKey:@"orientation"];
        
        CGRect frame = [[UIScreen mainScreen] applicationFrame];
        NXT_Peru_iPhone_AppDelegate *appDelegate = (NXT_Peru_iPhone_AppDelegate *)[UIApplication sharedApplication].delegate;
        appDelegate.connectedTabBarViewController.view.frame = frame;
        self.view.frame = frame;
    }
    else{
        [application setStatusBarOrientation:originalStatusBarOrientation_];
    }
    
    
    
}

/**
 * Notifies the view controller that its view was added to a window. The accountGraphicView_ frame is set as a work-around for iOS 3.x (when
 * this work-around is not applied, the graphic appears elongated and with reduced height)
 *
 * @param animated If YES, the view was added to the window using an animation
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
//    CGRect frame = self.view.frame;
//    CGFloat width = CGRectGetMaxX(frame) - CGRectGetMinX(frame);
//    CGFloat height = CGRectGetMaxY(frame) - CGRectGetMinY(frame);
    
    NXT_Peru_iPhone_AppDelegate *appDelegate = (NXT_Peru_iPhone_AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
       CGRect frame = [[UIScreen mainScreen] applicationFrame];
       appDelegate.connectedTabBarViewController.view.frame = frame;
       self.view.frame = frame;
    }
    
    accountGraphicView_.frame = CGRectMake(0.0f, 0.0f, [[UIApplication sharedApplication].delegate window].frame.size.height, [[UIApplication sharedApplication].delegate window].frame.size.width);
    
    [accountGraphicView_ displayGraphicForInformation:accountGraphicInformation_ withCurrency:currencySymbol_ currencyFollows:currencyFollows_];
    [accountGraphicView_ enableScroll];
    
    if (!appDelegate.accountGraphicDisplayed) {
        
        [alertView_ show];
        animatingAlertView_ = YES;
        mustReturnOnceAlertViewDisplayer_ = NO;
        
        appDelegate.accountGraphicDisplayed = YES;
        [appDelegate saveUserPreferences];
        
    }
    
    UIDevice *device = [UIDevice currentDevice];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accountUpdated:) name:kNotificationAccountUpdated object:nil];
    [device beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        [self prefersStatusBarHidden];
    }
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Shows again the navigation bar and status bar
 * if they where visible when this view was displayed
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationPortrait] forKey:@"orientation"];
    }
    else{
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationAccountUpdated object:nil];
    
    if (alertView_.visible) {
        
        [alertView_ dismissWithClickedButtonIndex:0 animated:YES];
        
    }
    
    [accountGraphicView_ disableScroll];
    
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:TRUE];
    
    
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark -
#pragma mark Public interface

/*
 * Displays the graphic for the given graphic information. The information instance is retained so it must not be altered once set
 */
- (void)displayGraphicForAccount:(BankAccount *)anAccount
                     information:(NXTAccountGraphicInformation *)anAccountGraphicInformation
                    withCurrency:(NSString *)aCurrencySymbol
                 currencyFollows:(BOOL)currencyFollows {
    
	if (account_ != anAccount) {
		[account_ release];
		account_ = [anAccount retain];
	}
    
    [accountGraphicInformation_ release];
    accountGraphicInformation_ = [anAccountGraphicInformation retain];
    
    [currencySymbol_ release];
    currencySymbol_ = [[Tools translateServerCurrencySymbolIntoClientCurrencySymbol:aCurrencySymbol] copy];
    
    currencyFollows_ = currencyFollows;
    
    [accountGraphicView_ displayGraphicForInformation:accountGraphicInformation_ withCurrency:currencySymbol_ currencyFollows:currencyFollows_];
    
}

#pragma mark -
#pragma mark Updating notifications

/*
 * The account has been updated with the transactions list. The graphic information is updated
 */
-(void)accountUpdated:(NSNotification *)notification {

	[accountGraphicView_ hideActivityIndicator];
	
    AccountTransactionsResponse *accountTransactionsResponse = [notification object];
    AccountTransactionList *transactionList = [accountTransactionsResponse accountTransactionList];

    if ([transactionList accountTransactionCount] > 0) {
        
//        AccountTransactionList *transactionList = [auxAccount accountTransactionsList];

        NSTimeInterval lastDateToRetrieve = accountGraphicView_.firstDayDisplayed;
        NSTimeInterval currentDate = lastDateToRetrieve - A_DAY_IN_SECONDS;
        NSTimeInterval firstDateToRetrieve = currentDate - (A_DAY_IN_SECONDS * 30);
        NSDecimalNumber *balance = accountGraphicView_.initialBalance;
        
        NXTAccountGraphicInformation *accountGraphicInformation = [[[NXTAccountGraphicInformation alloc] init] autorelease];
        NSDecimalNumber *zero = [NSDecimalNumber zero];
        NSDecimalNumber *transactionsGrandTotal = nil;
        
        while (currentDate >= firstDateToRetrieve) {
            
            NSArray *dayTransactions = [transactionList transactionListForDate:currentDate];
            
            if ([dayTransactions count] == 0) {
                
                [accountGraphicInformation addInformationForDayTimeInterval:currentDate
                                                                withBalance:balance
                                                     transactionsGrandTotal:zero
                                                            hasTransactions:NO];
                
            } else {
                
                transactionsGrandTotal = zero;
                
                for (AccountTransaction *transaction in dayTransactions) {
                    
                    transactionsGrandTotal = [transactionsGrandTotal decimalNumberByAdding:transaction.amount];
                    
                }
                
                balance = [balance decimalNumberBySubtracting:transactionsGrandTotal];
                
                [accountGraphicInformation addInformationForDayTimeInterval:currentDate
                                                                withBalance:balance
                                                     transactionsGrandTotal:transactionsGrandTotal
                                                            hasTransactions:YES];
                
            }
            
            currentDate -= A_DAY_IN_SECONDS;
            
        }
        
        [accountGraphicView_ addGraphicInformationToGraphic:accountGraphicInformation];
        
    } else {
        
        [accountGraphicView_ addGraphicInformationToGraphic:nil];
        
	}
    
}

#pragma mark -
#pragma mark Downloading additional information

/*
 * Downloads more graphic information
 */
- (void)downloadGraphicInformation {
    
//    NSTimeInterval firstDayDisplayed = accountGraphicView_.firstDayDisplayed;
//    NSDate *lastDateToRetrieve = [NSDate dateWithTimeIntervalSince1970:(firstDayDisplayed - A_DAY_IN_SECONDS)];
//    NSDate *firstDateToRetrieve = [lastDateToRetrieve dateByAddingTimeInterval:-A_DAY_IN_SECONDS * 30];
        
//	if (account_.mode == batxeas_Normal) {
//
//	    [[Updater getInstance] obtainAccountTransactionsWithAccount:account_
//											  andFilterWithInitDate:firstDateToRetrieve
//														 finishDate:lastDateToRetrieve
//														showIncomes:YES
//													   showPayments:YES];
//	
//	} else if (account_.mode == batxeas_CreditLine) {
//
//	    [[Updater getInstance] obtainCreditLineTransactionsWithAccount:account_
//												 andFilterWithInitDate:firstDateToRetrieve
//															finishDate:lastDateToRetrieve
//														   showIncomes:YES
//														  showPayments:YES];
//		
//	}
    
}

#pragma mark -
#pragma mark NXTAccountGraphicDelegate methods

/**
 * Notifies the delegate that the transactions callout was tapped for the given date. The transactions
 * associated to this day are displayed
 *
 * @param anAccountGraphic The NXTAccountGraphic that triggered the event
 * @param aDate The date where the callout was over
 */
- (void)accountGraphic:(NXTAccountGraphic *)anAccountGraphic transactionCalloutTappedOnDate:(NSDate *)aDate {
    UINavigationController *navController = self.navigationController;
    NSArray *viewControllers = [navController viewControllers];
    NSInteger indexOfTransactionsList = [viewControllers indexOfObject:[navController visibleViewController]] - 1;
    
    TransactionsFilterObject *filterObject = [[[TransactionsFilterObject alloc] init] autorelease];
    filterObject.toDate = [aDate dateByAddingTimeInterval:(A_DAY_IN_SECONDS - 1)];
    filterObject.fromDate =  aDate;
    
    AccountTransactionsListViewController *transactionsListViewController = (AccountTransactionsListViewController *) [viewControllers objectAtIndex:indexOfTransactionsList];
    [transactionsListViewController setTransactionsFilter:filterObject];
    
    
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationPortrait] forKey:@"orientation"];
    }
    else{
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
    }
    
    [navController popViewControllerAnimated:YES];
    
}

/*
 * Notifies the delegate that an additional download was requested
 */
- (void)startAdditionalDownloadForAccountGraphic:(NXTAccountGraphic *)anAccountGraphic {
    
    // do nothing
    
}

#pragma mark -
#pragma mark Navigation methods

/*
 * Returns to the previous view controller
 */
- (void)backToPreviousViewController {
    
    if (accountGraphicView_.canRotate) {
        
        UINavigationController *navigationController = self.navigationController;
        
        if (([[navigationController viewControllers] count] > 1) &&
            (navigationController.topViewController == self) &&
            ([UIDevice currentDevice].orientation == UIInterfaceOrientationPortrait)) {
            
            [navigationController popViewControllerAnimated:YES];
            
        } else {
            
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
                [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:originalDeviceOrientation_] forKey:@"orientation"];
            }
            else{
                [[UIApplication sharedApplication] setStatusBarOrientation:originalStatusBarOrientation_];
            }
            
        }
        
    }

}

#pragma mark -
#pragma mark UIAlertViewDelegate selectors

/**
 * Sent to the delegate after an alert view is presented to the user.
*/
- (void)didPresentAlertView:(UIAlertView *)alertView {
    
    animatingAlertView_ = NO;
    
    if (mustReturnOnceAlertViewDisplayer_) {
        
        [self performSelector:@selector(backToPreviousViewController)
                   withObject:nil
                   afterDelay:0.5f];
        mustReturnOnceAlertViewDisplayer_ = NO;
        
    }

}

#pragma mark -
#pragma mark UIDeviceOrientationDidChangeNotification selector

/*
 * Receives the notification of the orientation changes
 */
- (void)orientationChanged:(NSNotification *)notification {
    
    if (originalStatusBarOrientation_ != 0) {
        
        UINavigationController *navigationController = self.navigationController;
        
        if (([[navigationController viewControllers] count] > 1) &&
            (navigationController.topViewController == self) &&
            ([UIDevice currentDevice].orientation == UIInterfaceOrientationPortrait)) {
            
            [navigationController popViewControllerAnimated:YES];
            
        } else {

            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
                [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:originalDeviceOrientation_] forKey:@"orientation"];
            }
            else{
                [[UIApplication sharedApplication] setStatusBarOrientation:originalStatusBarOrientation_];
            }
            
            
        }
        
    }
    
//    if (fromGraphicButton_) {
//        
//        UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
//        
//        if (((deviceOrientation == UIDeviceOrientationLandscapeRight) && (originalStatusBarOrientation_ == UIInterfaceOrientationLandscapeLeft)) ||
//            ((deviceOrientation == UIDeviceOrientationLandscapeLeft) && (originalStatusBarOrientation_ == UIInterfaceOrientationLandscapeRight))) {
//            
//            fromGraphicButton_ = NO;
//            
//        }
//        
//        [[UIApplication sharedApplication] setStatusBarOrientation:originalStatusBarOrientation_];
//        
//    } else {
//        
//        if (animatingAlertView_) {
//            
//            mustReturnOnceAlertViewDisplayer_ = YES;
//            [[UIApplication sharedApplication] setStatusBarOrientation:originalStatusBarOrientation_];
//            
//        } else {
//            
//            [self backToPreviousViewController];
//            
//        }
//        
//    }

}

@end
