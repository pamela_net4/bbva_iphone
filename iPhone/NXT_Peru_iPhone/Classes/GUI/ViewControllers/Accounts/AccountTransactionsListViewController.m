/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "AccountTransactionsListViewController.h"

#import "AccountGraphicViewController.h"
#import "AccountList.h"
#import "AccountTransactionCell.h"
#import "AccountTransaction.h"
#import "AccountTransactionDetail.h"
#import "AccountTransactionDetailResponse.h"
#import "AccountTransactionList.h"
#import "BankAccount.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXTNavigationItem.h"
#import "NXTAccountGraphicInformation.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "AccountHeaderView.h"
#import "RetentionList.h"
#import "RetentionListResponse.h"
#import "RetentionsAdditionalInformation.h"
#import "RetentionsCell.h"
#import "RetentionsViewController.h"
#import "Session.h"
#import "StringKeys.h"
#import "Tools.h"
#import "TransactionDetailViewController.h"
#import "UIColor+BBVA_Colors.h"
#import "Updater.h"

//#import "OperationResultResponse.h"

/**
 * Defines the Nib file name
 */
#define NIB_NAME                        @"AccountTransactionsListViewController"

#pragma mark -

/**
 * AccountTransactionsListViewController private extension
 */
@interface AccountTransactionsListViewController()

/**
 * Provides read-write access to the flag for control if view it´s dowloading information
 */
@property (nonatomic, readwrite, assign) BOOL downloading;


/**
 * Releases the graphic elements
 * @private
 */
- (void)releaseAccountTransactionsListViewControllerGraphicElements;

/**
 * Shows the account balance graphic
 *
 * @param fromGraphicButton YES when the view is displayed from the graphic button, NO otherwise
 */
- (void)showBalanceGraphic:(BOOL)fromGraphicButton;

/**
 * Receives the notification of the orientation changes
 * @private
 */
- (void)orientationChanged:(NSNotification *)notification;

/**
 * Returns the total of transactions amount of a given date
 *
 * @param aDate to get the total
 * @private
 */
- (NSDecimalNumber *)getTransactionsTotalForDate:(NSDate *)aDate;

/**
 * The account has been updated with the transactions list
 * @private
 */
- (void)accountUpdated:(NSNotification *)notification;

/**
 * Returns whether there are transactions for the given date
 *
 * @param aDate The date to check
 * @return YES when there are transactions for the given date, no otherwise
 */
- (BOOL)anyTransactionForDate:(NSDate *)aDate;

/**
 * Displays the retentions view controller
 * @private
 */
- (void)displayRetentions;

/**
 * Displays the selected transaction detail
 *
 * @param accountTransaction The account selected
 * @private
 */
- (void)displayTransaction:(AccountTransaction *)accountTransaction;
//      andTransactionDetail:(AccountTransactionDetail *)transactionDetail;

/**
 * Returns the transaction list for the given list, extracted from the general transactions list
 *
 * @param aTransactionList The array containing all the transactions to analyze. All objects must be accountTransaction instances
 * @param aDate The date to obtaion the transaction list for
 * @return The array containing only the transactions for the given date
 * @private
 */
- (NSArray *)transactionsFromTransactionList:(NSArray *)aTransactionList
                                     forDate:(NSDate *)aDate;

/**
 * Returns the total of transactions amount of a given transactions list
 *
 * @param aTransactionList The array containing the account transactions. Only AccountTransaction instances are used to calculate the grand total
 * @return The transactions total for the given transaction list
 * @private
 */
- (NSDecimalNumber *)transactionsTotalForTransactionList:(NSArray *)aTransactionList;
    
/**
 * Inits the account transacation details update
 */
- (void)initAccountTransactionDetailsUpdate;

/**
 * Update subviews. If the transaction list is empty, chartButton is not showed and noTransactionLabel is showed
 *
 * @param filter If Yes, no transaction label must show a filter message. If No, must show a default message
 * @private
 */
- (void)updateSubviewsFromFilter:(BOOL)filter;

/**
 * Registers to device rotations event.
 *
 * @private
 */
- (void)registerToDeviceRotationsEvent;

/**
 * Unregisters from device rotations event.
 *
 * @private
 */
- (void)unregisterFromDeviceRotationsEvent;

@end


#pragma mark -

@implementation AccountTransactionsListViewController

#pragma mark -
#pragma mark Properties

@synthesize table = table_;
@dynamic account;
@synthesize buttonBarBackground = buttonBarBackground_;
@synthesize chartButton = chartButton_;
@synthesize filterButton = filterButton_;
@synthesize noTransactionLabel = noTransactionsLabel_;
@synthesize barButtonDivisionLine = barButtonDivisionLine_;
@synthesize downloading = downloading_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self releaseAccountTransactionsListViewControllerGraphicElements];
        
    [account_ release];
    account_ = nil;
    
    [retentionsViewController_ release];
    retentionsViewController_ = nil;
    
    [transactionDetailViewController_ release];
    transactionDetailViewController_ = nil;
    
    [transactionsFilterModalViewController_ release];
    transactionsFilterModalViewController_ = nil;
    
    [filterOptions_ release];
    filterOptions_ = nil;
    
    [transactionsList_ release];
    transactionsList_ = nil;
    
    [graphicTransactionsList_ release];
    graphicTransactionsList_ = nil;
    
    [graphicViewController_ release];
    graphicViewController_ = nil;
    
    [updatingTransaction_ release];
    updatingTransaction_ = nil;
    
    [detailsList_ release];
    detailsList_ = nil;
    
    [self setDownloading:NO];
    
    [super dealloc];
}

/**
 * Releases the graphic elements
 */
- (void)releaseAccountTransactionsListViewControllerGraphicElements {

    [table_ release];
    table_ = nil;
    
    [buttonBarBackground_ release];
    buttonBarBackground_ = nil;
    
    [chartButton_ release];
    chartButton_ = nil;
    
    [filterButton_ release];
    filterButton_ = nil;
    
    [noTransactionsLabel_ release];
    noTransactionsLabel_ = nil;
    
    [barButtonDivisionLine_ release];
    barButtonDivisionLine_ = nil;

}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseAccountTransactionsListViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Creates the internal structures
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    transactionsList_ = [[NSMutableArray alloc] init];
    detailsList_ = [[NSMutableArray alloc] init];
    totalAmountRetained_ = @"";
}


/*
 * Creates a new autoreleased instance from the associated NIB file
 */
+ (AccountTransactionsListViewController *)accountTransactionsListViewController {
    
    AccountTransactionsListViewController *result = [[[AccountTransactionsListViewController alloc] initWithNibName:NIB_NAME bundle:nil] autorelease];
    [result awakeFromNib];
    
	return result;
}

#pragma mark -
#pragma mark View managment

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
    [NXT_Peru_iPhoneStyler styleTableView:table_];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    buttonBarBackground_.image = [imagesCache imageNamed:BOTTOM_BUTTON_BAR_BACKGROUND_IMAGE_FILE_NAME];

    UIImage *divisionLine = [imagesCache imageNamed:BUTTON_BAR_DIVISION_LINE_IMAGE_FILE_NAME];
    
    [chartButton_ setImage:[imagesCache imageNamed:ICON_CHART_FILE_NAME] forState:UIControlStateNormal];
    
    [NXT_Peru_iPhoneStyler styleBottomBarButton:chartButton_ withTitle:NSLocalizedString(ACCOUNT_TRANSACTION_LIST_TAB_2_TEXT_KEY, nil)];
    
    barButtonDivisionLine_ = [[UIImageView alloc] initWithFrame:CGRectMake(chartButton_.frame.size.width - 1.0f, chartButton_.frame.origin.y + 2.0f, 2.0f, chartButton_.frame.size.height)];
    barButtonDivisionLine_.image = divisionLine;
    [barButtonDivisionLine_ setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [self.view addSubview:barButtonDivisionLine_];
    [self.view bringSubviewToFront:barButtonDivisionLine_];
    
    [filterButton_ setImage:[imagesCache imageNamed:ICON_FILTER_FILE_NAME] forState:UIControlStateNormal];
    [NXT_Peru_iPhoneStyler styleBottomBarButton:filterButton_ withTitle:NSLocalizedString(ACCOUNT_TRANSACTION_LIST_TAB_1_TEXT_KEY, nil)];
    
    [filterOptions_ release];
    filterOptions_ = [[TransactionsFilterObject alloc] init];
    filterOptions_.toDate = [NSDate date];
    filterOptions_.fromDate = [[NSDate date] dateByAddingTimeInterval:35*(-A_DAY_IN_SECONDS)];
    
    [NXT_Peru_iPhoneStyler styleLabel:noTransactionsLabel_ withBoldFontSize:16.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [noTransactionsLabel_ setText:NSLocalizedString(ACCOUNT_DOES_NOT_HAVE_TRANSACTIONS_TEXT_KEY, nil)];
    [noTransactionsLabel_ setHidden:YES];

    isGoToGraphic_ = NO;
   
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseAccountTransactionsListViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
#if defined(__IPHONE_3_2) && (__IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2)
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
#else
    [[UIApplication sharedApplication] setStatusBarHidden:NO animated:NO];
#endif
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self
                           selector:@selector(retentionsUpdated:)
                               name:kNotificationRetrieveRetainsEnds
                             object:nil];    
    [notificationCenter addObserver:self
                           selector:@selector(accountUpdated:)
                               name:kNotificationRetrieveAccountTransactionListEnds
                             object:nil];
        
    if ((!downloading_) && (account_ != nil) && ((account_.transactionsAdditionalInformation == nil) || (account_.accountTransactionsList == nil))) {
        
        [self setDownloading:YES];
        [self.appDelegate showActivityIndicator:poai_Both];

        [[Updater getInstance] obtainAccountTransactionsForAccountNumber:account_.number];

        
    } 
    
    [table_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f) animated:NO];
    
    
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")){
        
        if(isGoToGraphic_){//viene de grafico
            isGoToGraphic_ = NO;
            
            CGSize size = [[UIScreen mainScreen] bounds].size;
            NXT_Peru_iPhone_AppDelegate *appDelegate = (NXT_Peru_iPhone_AppDelegate *)[UIApplication sharedApplication].delegate;
            appDelegate.connectedTabBarViewController.view.frame = CGRectMake(0, 18, size.width, size.height - 16);

        }
    }
    
}

/**
 * Notifies the view controller that its view was added to a window.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (!downloading_) {
        
        [self registerToDeviceRotationsEvent];
        
    }
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        if (isFromGraphic_) {
            CGRect viewFrame = self.view.frame;
            viewFrame.origin.y = 44.0f;
            [self.view setFrame:viewFrame];
            isFromGraphic_ = FALSE;
        }
    }
    
    [self.appDelegate.connectedTabBarViewController setTabBarVisibility:NO animated:YES];

    self.navigationController.navigationBar.translucent = NO;

}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self setDownloading:NO];
	
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self
                                  name:kNotificationRetrieveRetainsEnds
                                object:nil];    
    [notificationCenter removeObserver:self
                                  name:kNotificationRetrieveAccountTransactionListEnds
                                object:nil];
    
    [self unregisterFromDeviceRotationsEvent];
    
}

/**
 * Update subviews. If the transaction list is empty, chartButton is not showed and noTransactionLable is showed
*/
- (void)updateSubviewsFromFilter:(BOOL)filter{
    
    CGRect filterButtonFrame = [filterButton_ frame];
    
    if ([transactionsList_ count] > 0){
        
        filterButtonFrame.origin.x = 0.0f;
        
        CGRect chartButtonFrame = [chartButton_ frame];
        chartButtonFrame.origin.y = filterButtonFrame.origin.y;
        [chartButton_ setFrame:chartButtonFrame];
        
        CGRect divisionLineFrame = [barButtonDivisionLine_ frame];
        divisionLineFrame.origin.y = filterButtonFrame.origin.y;
        [barButtonDivisionLine_ setFrame:divisionLineFrame];
        
        [self.view addSubview:chartButton_];
        [self.view bringSubviewToFront:chartButton_];
        
        [self.view addSubview:barButtonDivisionLine_];
        [self.view bringSubviewToFront:barButtonDivisionLine_];
        
        [noTransactionsLabel_ setHidden:YES];
        
    } else {
        
        filterButtonFrame.origin.x = 80;
        
        [chartButton_ removeFromSuperview];
        [barButtonDivisionLine_ removeFromSuperview];
        
        if (!downloading_) {
            
            [noTransactionsLabel_ setHidden:NO];
            
            if (filter){
                
                [noTransactionsLabel_ setText:NSLocalizedString(ACCOUNT_DOES_NOT_HAVE_MOVEMENTS_TEXT_KEY, nil)];
                
            } else {
                
                [noTransactionsLabel_ setText:NSLocalizedString(ACCOUNT_DOES_NOT_HAVE_TRANSACTIONS_TEXT_KEY, nil)];
                
            }
            
        }
        
    }
    
    [filterButton_ setFrame:filterButtonFrame];
    
}

#pragma mark -
#pragma mark UITableView methods

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSInteger index = [indexPath row];
	
	if (!hasRetentions_) {
		
		index++;
		
	}
    
    if ([indexPath row] == 0 && hasRetentions_) {
    
        RetentionsCell *result = (RetentionsCell *)[tableView dequeueReusableCellWithIdentifier: [RetentionsCell cellIdentifier]];
        
        if (result == nil) {
            result = [RetentionsCell retentionsCell];
        }
        
        [result.leftTextLabel setText:NSLocalizedString(RETENTIONS_TEXT_KEY, nil)];
        [result.rightTextLabel setText:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(AMOUNT_RETENTION_TEXT_2_KEY, nil), [Tools notNilString:totalAmountRetained_]]];

        return result;
    
    } else {
    
        AccountTransactionCell *result = (AccountTransactionCell*)[tableView dequeueReusableCellWithIdentifier: [AccountTransactionCell cellIdentifier]];
        
        if (result == nil) {
            result = [AccountTransactionCell accountTransactionCell];
        }
        
        result.accountTransaction = [transactionsList_ objectAtIndex:index - 1];
        
        return result;
    
    }

}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	NSInteger index = [indexPath row];
	
	if (!hasRetentions_) {
		
		index++;
		
	}

    if (index == 0) {
        
        [self displayRetentions];
        
    } else {
    
        [self displayTransaction:[transactionsList_ objectAtIndex:index - 1]];
//            andTransactionDetail:[detailsList_ objectAtIndex:indexPath.row - 1]];
    
    }
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	NSInteger result = [transactionsList_ count];
	
	if (hasRetentions_) {
		
		result++;
		
	}
	
    return result;
	
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0 && hasRetentions_) {
        
        return [RetentionsCell cellHeight];

    } else {
        
        return [AccountTransactionCell cellHeight];

    }
    
}

/**
 * Asks the delegate for a view object to display in the header of the specified section of the table view.
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    AccountHeaderView *result = [AccountHeaderView accountHeaderView];
        
    result.topTitle = NSLocalizedString(CURRENT_BALANCE_ACCOUNT_KEY , nil);
    
    NSString *notNilCurrencySymbol = [Tools notNilString:[Tools clientCurrencySymbolForServerCurrency:account_.currency]];
    
    result.topDetail = [NSString stringWithFormat:@"%@ %@", notNilCurrencySymbol, [Tools notNilString:[account_ currentBalanceString]]];
    
    result.bottomTitle = NSLocalizedString(AVAILABLE_BALANCE_ACCOUNT_KEY , nil);
    result.bottomDetail = [NSString stringWithFormat:@"%@ %@", notNilCurrencySymbol, [Tools notNilString:[account_ availableBalanceString]]];
    
    return result;
}

/**
 * Asks the delegate for the height to use for the header of a particular section.
 *
 * @param tableView The table-view object requesting this information.
 * @param section An index number identifying a section of tableView 
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [AccountHeaderView height];
}

#pragma mark -
#pragma mark User interaction

/*
 * The chartButton has been pressed
 */
- (IBAction)chartButtonPressed {
    
    [self showBalanceGraphic:YES];
    
}

/*
 * The filterButton has been pressed
 */
- (IBAction)filterButtonPressed {
    
    if (transactionsFilterModalViewController_ == nil) {
        transactionsFilterModalViewController_ = [[TransactionsFilterModalViewController transactionsFilterModalViewController] retain];
        transactionsFilterModalViewController_.delegate = self;
    }
    
    transactionsFilterModalViewController_.filterObject = filterOptions_;
    transactionsFilterModalViewController_.account = account_;
    [self.navigationController pushViewController:transactionsFilterModalViewController_ animated:YES];
}

/*
 * Displays the retentions view controller
 */
- (void)displayRetentions {

    if (retentionsViewController_ == nil) {
        
        retentionsViewController_ = [[RetentionsViewController retentionsViewController] retain];
        
    }
    
    retentionsViewController_.retentionList = retentionList_;
    retentionsViewController_.total = totalAmountRetained_;
    
    [self.navigationController pushViewController:retentionsViewController_ animated:YES];
    
}

/*
 * Displays the selected transaction detail
 */
- (void)displayTransaction:(AccountTransaction *)accountTransaction {
//      andTransactionDetail:(AccountTransactionDetail *)transactionDetail {

    if (transactionDetailViewController_ == nil) {
        transactionDetailViewController_ = [[TransactionDetailViewController transactionDetailViewController] retain];
    }
    
    transactionDetailViewController_.movementsList = transactionsList_;
    transactionDetailViewController_.selectedMovement = accountTransaction;
//    transactionDetailViewController_.transactionDetail = transactionDetail;
    transactionDetailViewController_.account = account_;
//    transactionDetailViewController_.detailsList = detailsList_;
    [self.navigationController pushViewController:transactionDetailViewController_ animated:YES];
    
}

#pragma mark -
#pragma mark Filter management

/*
 * Sets the transactions filter.
 */
- (void)setTransactionsFilter:(TransactionsFilterObject *)filter {
    
	// Filter the transactionList and parent method popToRootViewController.
    if (filter != nil) {
        
        if (filterOptions_ != filter) {
            [filterOptions_ release];
            filterOptions_ = [filter retain];
        }
        
        [transactionsList_ removeAllObjects];
        [transactionsList_ addObjectsFromArray:[Tools filterTrasactions:account_.accountTransactionsList.accountTransactionList withFilter:filterOptions_]];
        
        [self updateSubviewsFromFilter:YES];
                
        [table_ reloadData];
        [Tools checkTableScrolling:table_];
        
    }

}

#pragma mark -
#pragma mark  TransactionsFilterModalViewControllerDelegate selectors

/*
 * The filter has been closed with the given options
 */
- (void)filterClosedWithOptions:(TransactionsFilterObject *)filterOptions {
    
    if (filterOptions != nil) {
        
        [self setTransactionsFilter:filterOptions];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }

}

#pragma mark -
#pragma mark Balance gaphic

/*
 * Returns the total of transactions amount of a given date
 */
- (NSDecimalNumber *)getTransactionsTotalForDate:(NSDate *)aDate {
    NSDecimalNumber *total = [[[NSDecimalNumber alloc] initWithMantissa:0 exponent:0 isNegative:NO] autorelease];
    
    NSString *filteringDateExpression = [[NSExpression expressionForConstantValue:aDate] description];
    NSString *predicateString = [NSString stringWithFormat:@"operationDate == %@", filteringDateExpression];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
    NSArray *transactionsOfDate = [graphicTransactionsList_ filteredArrayUsingPredicate:predicate];
    
    for (AccountTransaction *transaction in transactionsOfDate) {
        total = [total decimalNumberByAdding:transaction.amount];
    }
    
    return total;
    return [NSDecimalNumber zero];
}

/*
 * Returns whether there are transactions for the given date
 */
- (BOOL)anyTransactionForDate:(NSDate *)aDate {

    NSString *filteringDateExpression = [[NSExpression expressionForConstantValue:aDate] description];
    NSString *predicateString = [NSString stringWithFormat:@"operationDate == %@", filteringDateExpression];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
    NSArray *transactionsOfDate = [graphicTransactionsList_ filteredArrayUsingPredicate:predicate];

    return ([transactionsOfDate count] > 0);
    return YES;
}

/*
 * Shows the account balance graphic
 */
- (void)showBalanceGraphic:(BOOL)fromGraphicButton {
    
    UINavigationController *navigationController = self.navigationController;
    
    if (navigationController != nil) {
        if (graphicViewController_ == nil) {
            graphicViewController_ = [[AccountGraphicViewController accountGraphicViewController] retain];
        }
                              
        BankAccount *account = account_;
        
        NSArray * graphicTransactionsList = account.accountTransactionsList.accountTransactionList;
        
        NXTAccountGraphicInformation *info = [[[NXTAccountGraphicInformation alloc] init] autorelease];
        
        NSTimeInterval time = [[Tools dateWithTimeToZero:[NSDate date]] timeIntervalSince1970];
        time += A_DAY_IN_SECONDS; // To show current day on the right edge of the graphic
        
        NSTimeInterval lastTransactionInterval = time;
        
        if (graphicTransactionsList != nil) {
            
            lastTransactionInterval = [((AccountTransaction *)[graphicTransactionsList lastObject]).operationDate timeIntervalSince1970];
            
        }
        
        lastTransactionInterval -= A_DAY_IN_SECONDS; // Use to show the last transaction day on the left edge of the graphic
        NSDate *lastTransactionDate = [NSDate dateWithTimeIntervalSince1970:lastTransactionInterval];
        
        NSInteger numberOfDays = [[Tools dateWithTimeToZero:[NSDate date]] timeIntervalSinceDate:lastTransactionDate] / A_DAY_IN_SECONDS;
        NSString *currency = account.currency;
        NSDecimalNumber *accountBalance = account.currentBalance;
        NSDate *currentDate = nil;
        NSArray *transactionsForDate = nil;
        NSDecimalNumber *transactionsTotal = nil;
        NSDecimalNumber *zero = [NSDecimalNumber zero];
        BOOL hasTransactions = NO;
        
        for (NSInteger day = 0; day <= numberOfDays; day++) {
            
            currentDate = [Tools dateWithTimeToZero:[NSDate dateWithTimeIntervalSince1970:(time - A_DAY_IN_SECONDS)]];
            transactionsForDate = [self transactionsFromTransactionList:graphicTransactionsList
                                                                forDate:currentDate];
            
            if ([transactionsForDate count] > 0) {
                
                hasTransactions = YES;
                transactionsTotal = [self transactionsTotalForTransactionList:transactionsForDate];
                
            } else {
                
                hasTransactions = NO;
                transactionsTotal = zero;
                
            }
            
            accountBalance = [accountBalance decimalNumberBySubtracting:transactionsTotal];
            
            [info addInformationForDayTimeInterval:time
                                       withBalance:accountBalance
                            transactionsGrandTotal:transactionsTotal
                                   hasTransactions:hasTransactions];
            time -= A_DAY_IN_SECONDS;
            
        }
        
        
        [graphicViewController_ displayGraphicForAccount:account
                                             information:info
                                            withCurrency:[Tools translateServerCurrencySymbolIntoClientCurrencySymbol:currency]
                                         currencyFollows:NO];
        graphicViewController_.fromGraphicButton = fromGraphicButton;
        
        isFromGraphic_ = TRUE;
        
        
        isGoToGraphic_ = YES;
        [navigationController pushViewController:graphicViewController_ animated:YES];
        
    }
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Returns the transaction list for the given list, extracted from the general transactions list
 */
- (NSArray *)transactionsFromTransactionList:(NSArray *)aTransactionList
                                     forDate:(NSDate *)aDate {
    
    NSString *filteringDateExpression = [[NSExpression expressionForConstantValue:aDate] description];
    NSString *predicateString = [NSString stringWithFormat:@"operationDate == %@", filteringDateExpression];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
    return [aTransactionList filteredArrayUsingPredicate:predicate];
    
}

/*
 * Returns the total of transactions amount of a given transactions list
 */
- (NSDecimalNumber *)transactionsTotalForTransactionList:(NSArray *)aTransactionList {
    
    NSDecimalNumber *result = [NSDecimalNumber zero];
    AccountTransaction *transaction = nil;
    
    for (NSObject *object in aTransactionList) {
        
        if ([object isKindOfClass:[AccountTransaction class]]) {
            
            transaction = (AccountTransaction *)object;
            result = [result decimalNumberByAdding:transaction.amount];
            
        }
        
    }
    
    return result;
    
}

/*
 * Registers to device rotations event.
 */
- (void)registerToDeviceRotationsEvent {
    
    if (!registeredToRotationsFlag_) {
        
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
        registeredToRotationsFlag_ = YES;
        
    }

}

/*
 * Unregisters from device rotations event.
 */
- (void)unregisterFromDeviceRotationsEvent {
    
    if (registeredToRotationsFlag_) {
        
        [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
        
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter removeObserver:self
                                      name:UIDeviceOrientationDidChangeNotification
                                    object:nil];

        registeredToRotationsFlag_ = NO;
        
    }
    
}

#pragma mark -
#pragma mark UIDeviceOrientationDidChangeNotification selector

/*
 * Receives the notification of the orientation changes
 */
- (void)orientationChanged:(NSNotification *)notification {
    
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    
    if ([transactionsList_ count] > 0){
        
        if ((deviceOrientation == UIInterfaceOrientationLandscapeRight) || (deviceOrientation == UIInterfaceOrientationLandscapeLeft)) {
            
            [self showBalanceGraphic:NO];
            
        }
        
    }

}


#pragma mark
#pragma mark Information management

/*
 * Inits the account transacation details update
 */
- (void)initAccountTransactionDetailsUpdate {

//    updatingTransaction_ = [transactionsList_ objectAtIndex:0];
            
    [[Session getInstance] updateTransactionsForAccount:account_];
    
//    for (AccountTransaction *transaction in transactionsList_) {
        
//        [[Updater getInstance] obtainAccountTransactionDetailForAccountNumber:[account_ number]
//                                                            transactionNumber:[updatingTransaction_ number]];
        
//    }
        
}

#pragma mark -
#pragma mark Notifications

/*
 * The account has been updated with the transactions list
 */
- (void)accountUpdated:(NSNotification *)notification {
    [self.appDelegate hideActivityIndicator];
    BankAccount *theAccount = [notification object];
    
    [self setDownloading:NO];
    
    if ([theAccount.errorCode length] == 0) {
        
        [transactionsList_ removeAllObjects];
        [transactionsList_ addObjectsFromArray:[[account_ accountTransactionsList] accountTransactionList]];
        
        [self updateSubviewsFromFilter:NO];
        [table_ reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.appDelegate showActivityIndicator:poai_Both];
    [[Updater getInstance] retrieveRetainsForAccountNumber:account_.number 
                                               accountType:account_.accountType 
                                                   balance:account_.currentBalanceString 
                                          availableBalance:account_.availableBalanceString 
                                                  currency:account_.currency 
                                                      bank:account_.bank 
                                                    office:account_.branch 
                                              controlDigit:account_.controlDigit 
                                                   account:account_.branchAccount 
                                                      type:account_.type 
                                                  andIndex:account_.subject];
    
}

/*
 * Product has been updated.
 */
- (void)retentionsUpdated:(NSNotification *)notification {
    //DLog( @"productUpdated" );
    
    [self.appDelegate hideActivityIndicator];
    RetentionListResponse *response = [notification object];
    RetentionList *retentionList = response.retentionList;
    
    if (retentionList != nil) {
        
        
        if (retentionList != retentionList_) {
            
            [retentionList_ release];
            retentionList_ = nil;
            retentionList_ = [retentionList retain];
            
        }
        
        [totalAmountRetained_ release];
        totalAmountRetained_ = nil;
        
        RetentionsAdditionalInformation *additionalInformation = response.additionalInformation;
        
        totalAmountRetained_ = [[Tools formatAmount:additionalInformation.totalRetention
                                       withCurrency:[Tools clientCurrencySymbolForServerCurrency:additionalInformation.currency]
                            currenctyPrecedesAmount:YES
                                       decimalCount:2] retain];
		
		hasRetentions_ = YES;
        
        [table_ reloadData];
        
    }
    
    if ([transactionsList_ count] > 0) {
        
        [self initAccountTransactionDetailsUpdate];
        
    }
    
}




#pragma mark -
#pragma mark Properties methods

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    
    result.customTitleView.topLabelText = [account_ accountType];
    result.customTitleView.bottomLabelText = [account_ number];

    return result;    
}

/**
 * Returns the bank account
 */
- (BankAccount *)account {
    return account_;
}

/*
 * Sets the bank account
 */
- (void)setAccount:(BankAccount *)account {
    
    if (account_ != account) {
        
        [account_ release];
        account_ = [account retain];
        [self setDownloading:NO];
		hasRetentions_ = NO;

    }
    
    [filterOptions_ release];
    filterOptions_ = [[TransactionsFilterObject alloc] init];
    
    if (transactionsList_ == nil) {
        transactionsList_ = [[NSMutableArray alloc] init];
    }
    
    [transactionsList_ removeAllObjects];
    [transactionsList_ addObjectsFromArray:[[account_ accountTransactionsList] accountTransactionList]];
     
    [detailsList_ removeAllObjects];
    
	NSArray *graphicTransactionsList = [[account_ accountTransactionsList] accountTransactionList];
	if (graphicTransactionsList_ != graphicTransactionsList) {
		[graphicTransactionsList_ release];
		graphicTransactionsList_ = [[NSArray alloc] initWithArray:graphicTransactionsList];
	}

    [self updateSubviewsFromFilter:NO];
    [table_ reloadData];
    
}

/**
 * Sets the downloading flag.
 *
 * @param downloading The new downloading flag.
 */
- (void)setDownloading:(BOOL)downloading {
    
    if (downloading != downloading_) {
        
        downloading_ = downloading;
        
        if (!downloading) {
            
            if (([self isViewLoaded]) && ([[self view] window] != nil)) {
                
                [self registerToDeviceRotationsEvent];
                
            }
            
        } else {
            
            [noTransactionsLabel_ setHidden:YES];
            
        }
        
    }
    
}

@end
