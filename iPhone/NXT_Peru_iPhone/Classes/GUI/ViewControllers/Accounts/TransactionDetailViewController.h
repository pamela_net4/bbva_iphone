/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NXTViewController.h"
#import "TransactionDetailView.h"
#import <MessageUI/MessageUI.h>

@class BankAccount;

/**
 * Movement detail view with a detail view and a table with options we can do with this movement.
 * On the above there are two arrows to navigate throught the list of movements of the previous list
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransactionDetailViewController : NXTViewController <UITableViewDelegate, UITableViewDataSource, TransactionDetailViewDelegate, MFMailComposeViewControllerDelegate> {
    
@private
    
    /**
     * Options table
     */
    UITableView *optionsTable_;
    
    /**
     * Previous movement button
     */
    UIButton *previousMovementButton_;
    
    /**
     * Next movement button
     */
    UIButton *nextMovementButton_;
    
    /**
     * Movement detail
     */
    TransactionDetailView *detailView_;
    
    /**
     * Movement detail auxiliar view for animation purposes
     */
    TransactionDetailView *auxDetailView_;
    
    /**
     * Separator line
     */
    UIImageView *separatorLine_;
    
    /**
     * Bottom branding line
     */
    UIImageView *bottomBrandingLine_;
    
    /**
     * List of movements
     */
    NSArray *movementsList_;
    
    /**
     * Selected movement
     */
    AccountTransaction *selectedMovement_;
    
    /**
     * Auxiliar movement
     */
    AccountTransaction *auxTransaction_;    
    
    /**
     * List with the options to show
     */
    NSMutableArray *optionsArray_;
    
    /**
     * Array with all the possible options related to a transaction
     */
    NSMutableArray *allOptionsArray_;
    
    /**
     * Account
     */
    BankAccount *account_;

}

/**
 * Provides readwrite access to the optionsTable. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *optionsTable;

/**
 * Provides readwrite access to the previousMovementButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *previousMovementButton;

/**
 * Provides readwrite access to the nextMovementButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *nextMovementButton;

/**
 * Provides readwrite access to the separatorLine. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separatorLine;

/**
 * Provides readwrite access to the bottomBrandingLine. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *bottomBrandingLine;

/**
 * Provides readwrite access to the movementsList
 */
@property (nonatomic, readwrite, retain) NSArray *movementsList;

/**
 * Provides readwrite access to the selectedMovement
 */
@property (nonatomic, readwrite, retain) AccountTransaction *selectedMovement;

/**
 * Provides readwrite access to the account
 */
@property (nonatomic, readwrite, retain) BankAccount *account;

/**
 * Creates a new autoreleased instance from the associated NIB file
 */
+ (TransactionDetailViewController *)transactionDetailViewController;

/**
 * The previous movement button has been pressed
 */
- (IBAction)previousMovementButtonPressed;

/**
 * The next movement button has been pressed
 */
- (IBAction)nextMovementButtonPressed;

@end
