/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>
#import "NXTEditableViewController.h"
#import "MOKDateComboButton.h"

@class BankAccount;

/**
 * Object passed to the delegate with the properties of the filter
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransactionsFilterObject : NSObject {
    
@private
    /**
     * From date
     */
    NSDate *fromDate_;
    
    /**
     * To date
     */
    NSDate *toDate_;
    
    /**
     * Show payments
     */
    BOOL showPayments_;
    
    /**
     * Show incomes
     */
    BOOL showIncomes_;
    
}

/**
 * Provides readwrite access to the showIncomes property
 */
@property (nonatomic, readwrite, assign) BOOL showIncomes;

/**
 * Provides readwrite access to the showBills property
 */
@property (nonatomic, readwrite, assign) BOOL showPayments;

/**
 * Provides readwrite access to the fromDate property
 */
@property (nonatomic, readwrite, retain) NSDate *fromDate;

/**
 * Provides readwrite access to the toDate property
 */
@property (nonatomic, readwrite, retain) NSDate *toDate;

@end

#pragma mark -

/**
 * Protocol that informs about filter modal VC
 */ 
@protocol TransactionsFilterModalViewControllerDelegate

/**
 * The filter has been closed with the given options
 */
- (void)filterClosedWithOptions:(TransactionsFilterObject *)filterOptions;

@end

/**
 * Modal view controller that shows some options to filter transactions
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransactionsFilterModalViewController : NXTEditableViewController <UITableViewDelegate, UITableViewDataSource, MOKDateComboButtonDelegate> {
    
@private
    
    /**
     * Scrollable view
     */
    UIView *scrollableElementsContainerView_;
    
    /**
     * Table
     */
    UITableView *table_;
    
    /**
     * Separator
     */
    UIImageView *separator_;

    /**
     * From combo button
     */
    MOKDateComboButton *fromCombo_;
    
    /**
     * To combo button
     */
    MOKDateComboButton *toCombo_;
    
    /**
     * Incomes switch
     */
    UISwitch *incomesSwitch_;
    
    /**
     * Payments switch
     */
    UISwitch *paymentsSwitch_;
    
    /**
     * Accept button
     */
    UIButton *acceptButton_;
    
    /**
     * Filter object
     */
    TransactionsFilterObject *filterObject_;
    
    /**
     * Delegate of the modal view
     */
    id <TransactionsFilterModalViewControllerDelegate> delegate_;
    
    /**
     * Array of transaction type tags
     */
    NSArray *transactionsType_;
    
    /**
     * From date
     */
    NSDate *fromDate_;
    
    /**
     * To date
     */
    NSDate *toDate_;
    
    /**
     * Account
     */
    BankAccount *account_;

}

/**
 * Provides readwrite access to the delegate
 */
@property (nonatomic, readwrite, assign) id <TransactionsFilterModalViewControllerDelegate> delegate;

/**
 * Provides readwrite access to the scrollable view
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *scrollableElementsContainerView;

/**
 * Provides readwrite access to the table
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *table;

/**
 * Provides readwrite access to the navigation bar right button and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

/**
 * Provides readwrite access to the accept button and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *acceptButton;

/**
 * Provides readwrite access to the filterObject
 */
@property (nonatomic, readwrite, retain) TransactionsFilterObject *filterObject;

/**
 * Provides readwrite access to the filterObject
 */
@property (nonatomic, readwrite, retain) BankAccount *account;

/**
 * Creates a new autoreleased instance from the associated NIB file
 */
+ (TransactionsFilterModalViewController *)transactionsFilterModalViewController;

/**
 * The to combo has been pressed
 */
- (void)toComboTapped;


/**
 * The from combo has been pressed
 */
- (void)fromComboTapped;

/**
 * Accept button pressed
 */
- (IBAction)acceptButtonPressed;


@end
