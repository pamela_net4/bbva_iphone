/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "TransactionsFilterModalViewController.h"

#import "BankAccount.h"
#import "Constants.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKDateComboButton.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTComboButton.h"
#import "NXTEditableViewController+protected.h"
#import "NXTNavigationItem.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Defines the header height
 */
#define HEADER_HEIGHT                           50.0f

/**
 * Defines the header height
 */
#define FOOTER_HEIGHT                           20.0f

/**
 * Defines a day in seconds
 */
#define DAY_IN_SECONDS                          86400

/**
 * Defines the cells text size
 */
#define CELLS_TEXT_SIZE                         15.0f

/**
 * Defines the nib file name
 */
#define NIB_NAME                                @"TransactionsFilterModalViewController"

@implementation TransactionsFilterObject

@synthesize fromDate = fromDate_;
@synthesize toDate = toDate_;
//@synthesize taggedAs = taggedAs_;
@synthesize showIncomes = showIncomes_;
@synthesize showPayments = showPayments_;


#pragma mark -
#pragma mark Instance initialization

/**
 * Initialization
 */
- (id)init {
	
    if ((self = [super init])) {
		
		fromDate_ = [[NSDate alloc] initWithTimeIntervalSinceNow:35*(-A_DAY_IN_SECONDS)];
		toDate_ = [[NSDate alloc] init];
        showIncomes_ = YES;
        showPayments_ = YES;
		
    }
    
    return self;
}

#pragma mark -
#pragma mark Memory management

/**
 * Dealloc
 */
- (void)dealloc {
	
    [toDate_ release];
    toDate_ = nil;
	
    [fromDate_ release];
    fromDate_ = nil;
    
    [super dealloc];

}

#pragma mark -
#pragma mark Getters and setters

/**
 * Returns the toDate
 */
- (NSDate *)toDate {
    return toDate_;
}

/**
 * Sets the toDate
 */
- (void)setToDate:(NSDate *)aDate {
    if (toDate_ != aDate) {
        [toDate_ release];
        toDate_ = [aDate retain];
    }
}

/**
 * Returns the fromDate
 */
- (NSDate *)fromDate {
    return fromDate_;
}

/**
 * Sets the fromDate
 */
- (void)setFromDate:(NSDate *)aDate {
    if (fromDate_ != aDate) {
        [fromDate_ release];
        fromDate_ = [aDate retain];
    }
}


@end

#pragma mark -

@interface TransactionsFilterModalViewController()

/**
 * Releases the graphic elements
 */
- (void)releaseTransactionsFilterModalViewControllerGraphicElements;

/**
 * A switch has been edited
 */
- (void)switchValueChanged:(UISwitch *)aSwitch;

@end


#pragma mark -

@implementation TransactionsFilterModalViewController

#pragma mark -
#pragma mark Properties

@synthesize scrollableElementsContainerView = scrollableElementsContainerView_;
@synthesize table = table_;
@synthesize filterObject = filterObject_;
@synthesize delegate = delegate_;
@synthesize separator = separator_;
@synthesize acceptButton = acceptButton_;
@synthesize account = account_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self releaseTransactionsFilterModalViewControllerGraphicElements];
    
    [filterObject_ release];
    filterObject_ = nil;
    
    [transactionsType_ release];
    transactionsType_ = nil;
    
    [account_ release];
    account_ = nil;
    
    [fromDate_ release];
    fromDate_ = nil;
    
    [toDate_ release];
    toDate_ = nil;
    
    [super dealloc];
    
}

/**
 * Releases the graphic elements
 */
- (void)releaseTransactionsFilterModalViewControllerGraphicElements {

    [scrollableElementsContainerView_ release];
    scrollableElementsContainerView_ = nil;
    
    [table_ release];
    table_ = nil;
    
    [incomesSwitch_ release];
    incomesSwitch_ = nil;
    
    [paymentsSwitch_ release];
    paymentsSwitch_ = nil;
    
    [separator_ release];
    separator_ = nil;
    
    [fromCombo_ release];
    fromCombo_ = nil;
	
	[toCombo_ release];
    toCombo_ = nil;
    
    [acceptButton_ release];
    acceptButton_ = nil;

}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransactionsFilterModalViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransactionsFilterModalViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates a new autoreleased instance from the associated NIB file
 */
+ (TransactionsFilterModalViewController *)transactionsFilterModalViewController {
	return [[[TransactionsFilterModalViewController alloc] initWithNibName:NIB_NAME bundle:nil] autorelease];
}

#pragma mark -
#pragma mark View managment

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
    [NXT_Peru_iPhoneStyler styleTableView:table_];
    
    table_.scrollEnabled = NO;
    
    separator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 200.0f, 30.0f)] autorelease];
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel withBoldFontSize:15.0f color:[UIColor whiteColor]];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment = UITextAlignmentCenter;
    titleLabel.text = NSLocalizedString(TRANSACTION_FILTER_TITLE_TEXT_KEY, nil);
        
    [toCombo_ release];
    toCombo_ = [[MOKDateComboButton alloc] initWithFrame:CGRectMake(0.0f, 7.0f, 160.0f, 30.0f)];
    [NXT_Peru_iPhoneStyler styleComboButton:toCombo_];
    toCombo_.delegate = self;
    toCombo_.title = NSLocalizedString(TRANSACTION_FILTER_TO_TEXT_KEY, nil);
    [toCombo_ addTarget:self action:@selector(toComboTapped) forControlEvents:UIControlEventTouchUpInside];
    toCombo_.inputAccessoryView = popButtonsView_;

//    toCombo_.minimumDate = [[NSDate alloc] initWithTimeIntervalSinceNow:45*(-A_DAY_IN_SECONDS)];
    toCombo_.maximumDate = [NSDate date];
    
    [fromCombo_ release];
    fromCombo_ = [[MOKDateComboButton alloc] initWithFrame:CGRectMake(0.0f, 7.0f, 160.0f, 30.0f)];
    [NXT_Peru_iPhoneStyler styleComboButton:toCombo_];
    fromCombo_.delegate = self;
    fromCombo_.title = NSLocalizedString(TRANSACTION_FILTER_FROM_TEXT_KEY, nil);
    [fromCombo_ addTarget:self action:@selector(fromComboTapped) forControlEvents:UIControlEventTouchUpInside];
    fromCombo_.inputAccessoryView = popButtonsView_;
    
//    fromCombo_.minimumDate = [[NSDate alloc] initWithTimeIntervalSinceNow:45*(-A_DAY_IN_SECONDS)];
    fromCombo_.maximumDate = [NSDate date];
    
    incomesSwitch_ = [[UISwitch alloc] initWithFrame:CGRectMake(25.0f, 9.0f , 0.0f, table_.rowHeight - 10.0f)];
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1){
        [incomesSwitch_ setOnTintColor:[UIColor BBVABlueSpectrumColor]];
        [incomesSwitch_ setTintColor: [UIColor BBVABlueSpectrumColor]];
    }
    
    [incomesSwitch_ addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    paymentsSwitch_ = [[UISwitch alloc] initWithFrame:CGRectMake(25.0f, 9.0f, 0.0f, table_.rowHeight - 10.0f)];
    [paymentsSwitch_ addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1){
        [paymentsSwitch_ setTintColor: [UIColor BBVABlueSpectrumColor]];
        [paymentsSwitch_ setOnTintColor:[UIColor BBVABlueSpectrumColor]];
    }
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] initWithObjects:fromCombo_, toCombo_, nil];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:acceptButton_];
    [acceptButton_ setTitle:NSLocalizedString(TRANSACTION_FILTER_OK_TEXT_KEY, nil) 
                   forState:UIControlStateNormal];
    
    CGRect frame = separator_.frame;
    frame.origin.y = table_.frame.origin.y + table_.frame.size.height;
    separator_.frame = frame;
    
    scrollableElementsContainerView_.backgroundColor = [UIColor clearColor];
    
    [self setScrollableView:scrollableElementsContainerView_];
    [self setScrollNominalFrame:table_.frame];

}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [table_ reloadData];

//    toCombo_.minimumDate = [[NSDate alloc] initWithTimeIntervalSinceNow:45*(-A_DAY_IN_SECONDS)];
    toCombo_.maximumDate = [NSDate date];
    
//    fromCombo_.minimumDate = [[NSDate alloc] initWithTimeIntervalSinceNow:45*(-A_DAY_IN_SECONDS)];
    fromCombo_.maximumDate = [NSDate date];

    [fromDate_ release];
    fromDate_ = [fromCombo_.selectedDate retain];
    
    [toDate_ release];
    toDate_ = [toCombo_.selectedDate retain];
    
}

#pragma mark -
#pragma mark UITableView methods

/**	
 * Asks the data source to return the number of sections in the table view
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    NSString *identifier = @"TransactionsFilterCell";
    UITableViewCell* result = [tableView dequeueReusableCellWithIdentifier:identifier];
	
	if (result == nil) {
		result = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
	}
    
    UIView *accesoryView = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 170.0f, 44)] autorelease];
	
    switch (indexPath.section) {
		case 0:
			switch (indexPath.row) {
				case 0:
                    
                    result.textLabel.text = NSLocalizedString(TRANSACTION_FILTER_FROM_TEXT_KEY, nil);
                    fromCombo_.selectedDate = filterObject_.fromDate;
                    [accesoryView addSubview:fromCombo_];
                    result.accessoryView = accesoryView;
                    break;

                case 1:
                    
                    result.textLabel.text  = NSLocalizedString(TRANSACTION_FILTER_TO_TEXT_KEY, nil);
                    toCombo_.selectedDate = filterObject_.toDate;
                    [accesoryView addSubview:toCombo_];
                    result.accessoryView = accesoryView;
                    
					break;
					
				default:
					break;
			}
            
            [NXT_Peru_iPhoneStyler styleLabel:result.textLabel withBoldFontSize:14.0f color:[UIColor blackColor]];
            
			break;
		case 1:
            
            accesoryView = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 130.0f, 44)] autorelease];
            
			switch (indexPath.row) {
				case 0:
					
					result.textLabel.text = NSLocalizedString(TRANSACTION_FILTER_SHOW_INCOMES_TEXT_KEY, nil);
					[incomesSwitch_ setOn:filterObject_.showIncomes];
					[accesoryView addSubview:incomesSwitch_];
					
                    result.accessoryView = accesoryView;
                    
					break;
					
				case 1:
					
					result.textLabel.text = NSLocalizedString(TRANSACTION_FILTER_SHOW_PAYMENTS_TEXT_KEY, nil);
					[paymentsSwitch_ setOn:filterObject_.showPayments];
					[accesoryView addSubview:paymentsSwitch_];
                    
                    result.accessoryView = accesoryView;
					
					break;
					
				default:
					break;
			}
			break;
	}
    
    result.textLabel.font = [NXT_Peru_iPhoneStyler boldFontWithSize:CELLS_TEXT_SIZE];

    result.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return result;
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
    NSInteger rows = 0;
	switch (section) {
		case 0:
			rows = 2;
			break;
		case 1:
			rows = 2;
			break;
	}
    return rows;
    
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

/**
 * Asks the delegate for a view object to display in the header of the specified section of the table view.
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *headerLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10.0f, 10.0f, 280.0f, 30.0f)] autorelease];
    [NXT_Peru_iPhoneStyler styleLabel:headerLabel withFontSize:18.0f color:[UIColor BBVABlueColor]];
    headerLabel.backgroundColor = [UIColor clearColor];
	
	switch (section) {
		case 0:
			headerLabel.text = NSLocalizedString(TRANSACTION_FILTER_HEADER_TEXT_KEY, nil);
			break;
		case 1:
			headerLabel.text = NSLocalizedString(TRANSACTION_FILTER_HEADER2_TEXT_KEY, nil);
			break;
	}
	
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tableView.frame.size.width, HEADER_HEIGHT)] autorelease];
    headerView.backgroundColor = [UIColor clearColor];
    [headerView addSubview:headerLabel];
    
    return headerView;
}

/**
 * Asks the delegate for the height to use for the header of a particular section.
 *
 * @param tableView The table-view object requesting this information.
 * @param section An index number identifying a section of tableView 
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return HEADER_HEIGHT;
}

/**
 * Asks the delegate for the height to use for the footer of a particular section.
 *
 * @param tableView The table-view object requesting this information.
 * @param section An index number identifying a section of tableView .
 */
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return FOOTER_HEIGHT;
}

#pragma mark -
#pragma mark MOKDateComboButton Delegate selectors

/**
 * Informs the delegate that a value has been selected
 *
 * @param combo that informs
 */
- (void)MOKDateComboButtonDateChanged:(MOKDateComboButton *)combo {
         
    if (combo == toCombo_) {
        
        [toDate_ release];
        NSDate *auxDate = [Tools dateWithTimeToZero:combo.selectedDate];
#if defined(__IPHONE_4_0) && (__IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_4_0)
        auxDate = [auxDate dateByAddingTimeInterval:(A_DAY_IN_SECONDS-1)];
#else
        auxDate = [auxDate addTimeInterval:(A_DAY_IN_SECONDS-1)];
#endif
        toDate_ = [auxDate retain];
        
#if defined(__IPHONE_4_0) && (__IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_4_0)
        auxDate = [toDate_ dateByAddingTimeInterval:-45*(A_DAY_IN_SECONDS)];
#else
        auxDate = [toDate_ addTimeInterval:-45*(A_DAY_IN_SECONDS)];
#endif
        
        if ([fromCombo_.selectedDate compare:auxDate] == NSOrderedAscending) {
            
            [fromDate_ release];
            fromDate_ = [auxDate retain];
            fromCombo_.selectedDate = [Tools dateWithTimeToZero:fromDate_];
            
        }
    
    } else if (combo == fromCombo_) {
        
        [fromDate_ release];
        fromDate_ = [[Tools dateWithTimeToZero:combo.selectedDate] retain];
        
#if defined(__IPHONE_4_0) && (__IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_4_0)
        NSDate *auxDate = [fromDate_ dateByAddingTimeInterval:45*(A_DAY_IN_SECONDS)];
#else
        NSDate *auxDate = [fromDate_ addTimeInterval:45*(A_DAY_IN_SECONDS)];
#endif
        
        if ([toCombo_.selectedDate compare:auxDate] == NSOrderedDescending) {
            
            [toDate_ release];
#if defined(__IPHONE_4_0) && (__IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_4_0)
            auxDate = [auxDate dateByAddingTimeInterval:(A_DAY_IN_SECONDS-1)];
#else
            auxDate = [auxDate addTimeInterval:(A_DAY_IN_SECONDS-1)];
#endif
            toDate_ = [auxDate retain];
            
            toCombo_.selectedDate = toDate_;
            
        }
        
    }
    
    toCombo_.minimumDate = fromCombo_.selectedDate;
    fromCombo_.maximumDate = toCombo_.selectedDate;

    
}

#pragma mark -
#pragma mark User interaction

/**
 * The to combo has been pressed
 */
- (void)toComboTapped {
    [self editableViewHasBeenClicked:toCombo_];
}

/**
 * The from combo has been pressed
 */
- (void)fromComboTapped {
    [self editableViewHasBeenClicked:fromCombo_];
}

/*
 * A switch has been edited
 */
- (void)switchValueChanged:(UISwitch *)aSwitch {
    if (aSwitch == incomesSwitch_) {
        
        filterObject_.showIncomes = aSwitch.on;
        
        if (!incomesSwitch_.on && !paymentsSwitch_.on) {
            [paymentsSwitch_ setOn:YES animated:YES];
            filterObject_.showPayments = YES;
        }
        
    } else if (aSwitch == paymentsSwitch_) {
        
        filterObject_.showPayments = aSwitch.on;
        
        if (!incomesSwitch_.on && !paymentsSwitch_.on) {
            [incomesSwitch_ setOn:YES animated:YES];
            filterObject_.showIncomes = YES;
        }
    }
}


/*
 * Accept button pressed
 */
- (void)acceptButtonPressed {
    
    filterObject_.toDate = toDate_;
    filterObject_.fromDate = fromDate_;
    
    [delegate_ filterClosedWithOptions:filterObject_];
}

#pragma mark -
#pragma mark Properties methods

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    
    result.customTitleView.topLabelText = [account_ accountType];
    result.customTitleView.bottomLabelText = [account_ number];
    
    return result;
    
}


/*
 * Sets the bank account
 */
- (void)setAccount:(BankAccount *)account {
    
    if (account_ != account) {
        [account_ release];
        account_ = [account retain];
    }
        
    self.customNavigationItem.customTitleView.topLabelText = [account_ accountType];
    self.customNavigationItem.customTitleView.bottomLabelText = [account_ number];
    
}

@end
