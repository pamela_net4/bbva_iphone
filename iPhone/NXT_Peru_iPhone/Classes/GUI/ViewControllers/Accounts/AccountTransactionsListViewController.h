/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "NXTViewController.h"
#import "TransactionDetailViewController.h"
#import "TransactionsFilterModalViewController.h"


//Forward delcataions
@class BankAccount;
@class AccountGraphicViewController;
@class RetentionList;
@class RetentionsViewController;

/**
 * Movements list view. It only has a table view and a bottom bar with options. Allow the filtering of movements
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountTransactionsListViewController : NXTViewController <UITableViewDelegate, UITableViewDataSource, TransactionsFilterModalViewControllerDelegate> {
@private
    /**
     * Table
     */
    UITableView *table_;
    
    /**
     * Button bar background
     */
    UIImageView *buttonBarBackground_;
    
    /**
     * Chart button
     */
    UIButton *chartButton_;
    
    /**
     * Filter button
     */
    UIButton *filterButton_;
    
    /**
     * No transactions Label
     */
    UILabel *noTransactionsLabel_;
    
    /**
     * Bar button division line
     */
    UIImageView *barButtonDivisionLine_;
    
    /**
     * Account 
     */
    BankAccount *account_;
    
    /**
     * Retentions view controller
     */
    RetentionsViewController *retentionsViewController_;
    
    /**
     * Movement detail view controller
     */
    TransactionDetailViewController *transactionDetailViewController_;
    
    /**
     * Modal view controller for the transactions filter
     */
    TransactionsFilterModalViewController *transactionsFilterModalViewController_;
    
    /**
     * Account graphic view controller to display the account balance graphic
     */
    AccountGraphicViewController *graphicViewController_;
    
    /**
     * Filter object
     */
    TransactionsFilterObject *filterOptions_;
    
    /**
     * Retention list being displayed
     */
    RetentionList *retentionList_;
    
    /**
     * Total amount retained including the currency;
     */
    NSString *totalAmountRetained_;
    
    /**
     * List of transactions
     */
    NSMutableArray *transactionsList_;
    
    /**
     * List of transactions to pass to the graphic
     */
    NSArray *graphicTransactionsList_;

    /**
     * Flag for control if view it´s dowloading information
     */ 
    BOOL downloading_;   
	
	/**
	 * Bool to know if has retentions.
	 */
	BOOL hasRetentions_;
    
    /**
	 * Bool to know if comes from graphic.
	 */
	BOOL isFromGraphic_;
    
    
    BOOL isGoToGraphic_;
        
    /**
     * Transaction that is being updated with the detail
     */
    AccountTransaction *updatingTransaction_;
    
    /**
     * Accoun transaction Details List
     */
    NSMutableArray *detailsList_;
    
    /**
     * Retistered to rotations flag. YES when the view controller is registered to device rotations events, NO othwerwise.
     */
    BOOL registeredToRotationsFlag_;
}

/**
 * Provides readwrite access to the table. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *table;

/**
 * Provides readwrite access to the buttonBarBackground. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *buttonBarBackground;

/**
 * Provides readwrite access to the chartButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *chartButton;

/**
 * Provides readwrite access to the filterButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *filterButton;

/**
 * Provides readwrite access to the noTtransactionLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *noTransactionLabel;

/**
 * Provides readwrite access to the barButtonDivisionLine. Exported to IB
 */
@property (nonatomic, readwrite, retain) UIImageView *barButtonDivisionLine;

/**
 * Provides readwrite access to the account
 */
@property (nonatomic, readwrite, retain) BankAccount *account;

/**
 * Creates a new autoreleased instance from the associated NIB file
 */
+ (AccountTransactionsListViewController *)accountTransactionsListViewController;

/**
 * The chartButton has been pressed
 */
- (IBAction)chartButtonPressed;

/**
 * The filterButton has been pressed
 */
- (IBAction)filterButtonPressed;

/**
 * Sets the transactions filter.
 *
 * @param filter The new transactions filter.
 */
- (void)setTransactionsFilter:(TransactionsFilterObject *)filter;

@end
