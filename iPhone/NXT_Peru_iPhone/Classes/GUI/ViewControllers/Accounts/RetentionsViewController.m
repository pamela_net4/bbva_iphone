/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "RetentionsViewController.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "ProductHeaderView.h"
#import "Retention.h"
#import "RetentionsDetailCell.h"
#import "RetentionList.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Defines the Nib file name
 */
#define NIB_NAME                        @"RetentionsViewController"

#pragma mark -

/**
 * AccountTransactionsListViewController private extension
 */
@interface RetentionsViewController()

/**
 * Releases the graphic elements
 */
- (void)releaseRetentionsViewControllerGraphicElements;
    

@end


#pragma mark -

@implementation RetentionsViewController

#pragma mark -
#pragma mark Properties

@synthesize table = table_;
@synthesize brandingLine = brandingLine_;
@synthesize total = total_;
@synthesize retentionList = retentionList_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self releaseRetentionsViewControllerGraphicElements];
    
    [total_ release];
    total_ = nil;
    
    [retentionList_ release];
    retentionList_ = nil;
    
    [super dealloc];
}

/**
 * Releases the graphic elements
 */
- (void)releaseRetentionsViewControllerGraphicElements {

    [table_ release];
    table_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseRetentionsViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates a new autoreleased instance from the associated NIB file
 */
+ (RetentionsViewController *)retentionsViewController {
	return [[[RetentionsViewController alloc] initWithNibName:NIB_NAME
                                                       bundle:nil] autorelease];
}

#pragma mark -
#pragma mark View managment

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
    self.view.backgroundColor = [UIColor BBVAWhiteColor];
    brandingLine_.image = [[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseRetentionsViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [table_ reloadData];
    [Tools checkTableScrolling:table_];

}

/**
 * Notifies the view controller that its view was added to a window.
 */
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.appDelegate setTabBarVisibility:NO animated:YES];
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
}

#pragma mark -
#pragma mark UITableView methods

/**	
 * Asks the data source to return the number of sections in the table view
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    RetentionsDetailCell *result = (RetentionsDetailCell *)[tableView dequeueReusableCellWithIdentifier:[RetentionsDetailCell cellIdentifier]];
    
    if (result == nil) {
        result = [RetentionsDetailCell retentionsDetailCell];
    }
    
    result.retention = [[retentionList_ retentionsList] objectAtIndex:indexPath.row];
    
    return result;
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES]; 
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[retentionList_ retentionsList] count];
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [RetentionsDetailCell cellHeight];
}

/**
 * Asks the delegate for a view object to display in the header of the specified section of the table view.
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    ProductHeaderView *result = [ProductHeaderView productHeaderView];
    
    result.title = NSLocalizedString(RETENTIONS_TOTAL_RETAINED_TEXT_KEY, nil);
    result.amountLabel.text = total_;
    
    return result;
}

/**
 * Asks the delegate for the height to use for a section.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return [ProductHeaderView height];

}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    
    [[result customTitleView] setTopLabelText:NSLocalizedString(RETENTIONS_TEXT_KEY, nil)];
    
    return result;
    
}

@end
