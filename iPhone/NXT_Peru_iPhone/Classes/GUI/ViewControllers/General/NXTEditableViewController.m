/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "NXTEditableViewController.h"
#import "NXTEditableViewController+protected.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "PopButtonsView.h"
#import "NXTTextField.h"
#import "NXTTextView.h"
#import "NXTComboButton.h"
#import "Tools.h"
#import "NXTCurrencyTextField.h"
#import "NXTSearchBar.h"


#pragma mark -

/**
 * NXTEditableViewController private category
 */
@interface NXTEditableViewController(private)

/**
 * Releases the graphic elements
 */
- (void)releaseNXTEditableViewControllerGraphicElements;

/**
 * Invoked by framework when a text field is about to make the keyboard appear. Used to animate the
 * pop buttons view
 *
 * @param aNotification The notification information
 */
- (void) keyboardWillAppear: (NSNotification*) aNotification;

/**
 * Invoked by framework when a text field is about to make the keyboard disappear. Used to animate the
 * pop buttons view
 *
 * @param aNotification The notification information
 */
- (void) keyboardWillDisappear: (NSNotification*) aNotification;

@end


#pragma mark -

@implementation NXTEditableViewController

#pragma mark -
#pragma mark Properties

@synthesize popButtonsView = popButtonsView_;
@synthesize shouldClearInterfaceData = shouldClearInterfaceData_;
@dynamic appDelegate;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void) dealloc {
    
	NSNotificationCenter *notifCenter = [NSNotificationCenter defaultCenter];
	[notifCenter removeObserver:self
                           name:UIKeyboardWillShowNotification
                         object:nil];
	[notifCenter removeObserver:self
                           name:UIKeyboardWillHideNotification
                         object:nil];

    [self releaseNXTEditableViewControllerGraphicElements];
	
	[predictiveText_ release];
	predictiveText_ = nil;
	
	[currentTextViewText_ release];
	currentTextViewText_ = nil;
    
    [editableViews_ release];
    editableViews_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory. Releases graphic elements
 */
- (void)viewDidUnload {
    
    [self releaseNXTEditableViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/*
 * Releases the graphic elements
 */
- (void)releaseNXTEditableViewControllerGraphicElements {
    
    [transparentScroll_ release];
    transparentScroll_ = nil;
    
    [scrollableView_ release];
    scrollableView_ = nil;
    
    [editedView_ release];
    editedView_ = nil;
    
    [topHeaderView_ release];
    topHeaderView_ = nil;
    
	[popButtonsView_ release];
	popButtonsView_ = nil;

    [currentTextView_ release];
    currentTextView_ = nil;
    
}

#pragma mark -
#pragma mark View management

/**
 * Invoked by framework when view has loaded from NIB file. Sets the subviews, if any, in the scroll view
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
	popButtonsState_ = pbse_Idle;

    if (popButtonsView_ == nil) {
        
        popButtonsView_ = [[PopButtonsView popButtonsView] retain];
        [popButtonsView_ showShowAvailableTextsButton: NO];
        [popButtonsView_ showPreviousAndNextResponderButtons: YES];
        popButtonsView_.delegate = self;
        
    }
    
    if (transparentScroll_ == nil) {
        
        scrollNominalFrame_ = self.view.frame;
        scrollNominalFrame_.origin.x = 0.0f;
        scrollNominalFrame_.origin.y = 0.0f;
        
        transparentScroll_ = [[UIScrollView alloc] initWithFrame:scrollNominalFrame_];
        scrollNominalFrame_ = transparentScroll_.frame;
        [transparentScroll_ setBackgroundColor:[UIColor clearColor]];
        
        if (topHeaderView_ != nil) {
            
            CGFloat headerFrameHeigh = CGRectGetHeight(topHeaderView_.frame);
            
            scrollNominalFrame_.size.height = CGRectGetHeight(scrollNominalFrame_) - headerFrameHeigh;
            scrollNominalFrame_.origin.y = headerFrameHeigh;
            transparentScroll_.frame = scrollNominalFrame_;
            
        }
        
    }
    
    [transparentScroll_ removeFromSuperview];
    [self.view addSubview:transparentScroll_];

}

/**
 * Invoked by framework when view is added to the window. Registers to notification center to receive keyboard notifications
 *
 * @param animated YES if view is going to be animated, NO otherwise
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
	NSNotificationCenter *notifCenter = [NSNotificationCenter defaultCenter];
	[notifCenter addObserver:self
                    selector:@selector(keyboardWillAppear:)
                        name:UIKeyboardWillShowNotification
                      object:nil];
	[notifCenter addObserver:self
                    selector:@selector(keyboardWillDisappear:)
                        name:UIKeyboardWillHideNotification
                      object:nil];
    
}

/**
 * Invoked by frameword when view is about to be dismissed. Unregisters to notification center as keyboard
 * notifications are not needed anymore
 *
 * @param animated YES if view is going to be animated, NO otherwise
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [self okButtonClickedInPopButtonsView:popButtonsView_];
	
	NSNotificationCenter *notifCenter = [NSNotificationCenter defaultCenter];
	[notifCenter removeObserver:self
                           name:UIKeyboardWillShowNotification
                         object:nil];
	[notifCenter removeObserver:self
                           name:UIKeyboardWillHideNotification
                         object:nil];
	
	[super viewWillDisappear: animated];
    
}

#pragma mark -
#pragma mark Interface management

/*
 * Recalculates the scroll
 */
- (void)recalculateScroll {
    
    if (internalScrollableView_) {
        
        NSArray *subviews = [NSArray arrayWithArray:[scrollableView_ subviews]];
        CGFloat maxY = 0;
        CGRect viewFrame;
        
        for (UIView *subview in subviews) {
            
            viewFrame = subview.frame;
            CGFloat auxVal = CGRectGetMaxY(viewFrame);
            
            if ((maxY < auxVal) && (subview.hidden == NO)) {
                
                maxY = auxVal;
                
            }
            
        }
        
        maxY += [self distanceFromLastControlToViewBottom];
        
        scrollableView_.frame = CGRectMake(0.0f, scrollableView_.frame.origin.y, scrollableView_.frame.size.width, maxY);
        transparentScroll_.contentSize = CGSizeMake(self.view.frame.size.width, maxY);
        
    } else {
        
        if (scrollableView_ != nil) {
            
            transparentScroll_.contentSize = scrollableView_.frame.size;
            
        } else {
            
            transparentScroll_.contentSize = CGSizeZero;
            
        }
        
    }
    
}

/*
 * Resets the scroll position to the top left
 */
- (void)resetScrollToTopLeftAnimated:(BOOL)animated {
    
    [transparentScroll_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f) animated:animated];
    
}

#pragma mark -
#pragma mark Keyboard notifications

/**
 * Invoked by framework when a text field is about to make the keyboard appear. Used to animate the
 * pop buttons view
 *
 * @param aNotification The notification information
 */
- (void) keyboardWillAppear: (NSNotification*) aNotification {
    
    NSDictionary* userInfo = aNotification.userInfo;
    
#if defined(__IPHONE_3_2) && (__IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2)
    
    NSValue* value = [userInfo objectForKey: UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardEndFrame = [value CGRectValue];
    
    CGRect scrollFrame = [self scrollerFrameForEditingViewFrame:keyboardEndFrame];
    
    if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
        
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationCurveLinear
                         animations:^ {
                             
                             transparentScroll_.frame = scrollFrame;
                             
                         }
                         completion:nil];
        
    } else {
        
        [UIView beginAnimations:nil context: nil];
        [UIView setAnimationCurve: UIViewAnimationCurveLinear];
        [UIView setAnimationDuration: 0.3f];
        
        transparentScroll_.frame = scrollFrame;
        
        [UIView  commitAnimations];
        
    }
    
    if (![editedView_ isKindOfClass:[UITextField class]] || ([editedView_ isKindOfClass:[UITextField class]] && !isKeyboardShowing_))  {
        [self makeViewVisible:editedView_];
    }
    
    isKeyboardShowing_ = YES;
    isPopBarShowing_ = YES;

#else
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];

    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] != NSOrderedAscending) {
        
        NSValue* value = [userInfo objectForKey: UIKeyboardFrameEndUserInfoKey];
        CGRect keyboardEndFrame = [value CGRectValue];
        
        CGRect scrollFrame = [self scrollerFrameForEditingViewFrame:keyboardEndFrame];
        
        if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
            
            [UIView animateWithDuration:0.3f
                                  delay:0.0f
                                options:UIViewAnimationCurveLinear
                             animations:^ {
                                 
                                 transparentScroll_.frame = scrollFrame;
                                 
                             }
                             completion:nil];
            
        } else {
            
            [UIView beginAnimations:nil context: nil];
            [UIView setAnimationCurve: UIViewAnimationCurveLinear];
            [UIView setAnimationDuration: 0.3f];
            
            transparentScroll_.frame = scrollFrame;
            
            [UIView  commitAnimations];
            
        }
        
        if (![editedView_ isKindOfClass:[UITextField class]] || ([editedView_ isKindOfClass:[UITextField class]] && !isKeyboardShowing_))  {
            [self makeViewVisible:editedView_];
        }
        
        isKeyboardShowing_ = YES;
        isPopBarShowing_ = YES;
        
    } else {
        
        currentTextView_ = nil;
        
        if ((popButtonsState_ != pbse_Idle) && (popButtonsState_ != pbse_Hide)) {
            [popButtonsView_ showListText: YES];
            
            UIWindow* window = self.view.window;
            CGRect windowFrame = window.frame;
            CGFloat initYHeight;
            CGFloat finalYHeight;
            
            NSValue* value = [userInfo objectForKey: UIKeyboardBoundsUserInfoKey];
            CGRect keyboardBounds = [value CGRectValue];
            
            
            if (popButtonsState_ == pbse_Show) {
                value = [userInfo objectForKey: UIKeyboardCenterBeginUserInfoKey];
                CGPoint beginCenter = [value CGPointValue];
                
                initYHeight = windowFrame.size.height - (beginCenter.y - (keyboardBounds.size.height / 2.0f));
            }
            
            value = [userInfo objectForKey: UIKeyboardCenterEndUserInfoKey];
            CGPoint endCenter = [value CGPointValue];
            finalYHeight = windowFrame.size.height - (endCenter.y - (keyboardBounds.size.height / 2.0f));
            
            BOOL animationDefined = NO;
            UIViewAnimationCurve animationCurve;
            double animationDuration;
            
            value = [userInfo objectForKey: UIKeyboardAnimationCurveUserInfoKey];
            animationDefined = YES;
            
            if (value != nil) {
                [value getValue: &animationCurve];
                
                value = [userInfo objectForKey: UIKeyboardAnimationDurationUserInfoKey];
                
                if (value != nil) {
                    [value getValue: &animationDuration];
                } else {
                    animationDefined = NO;
                }
            } else {
                animationDefined = NO;
            }
            
            if (animationDefined == YES) {
                if (popButtonsState_ == pbse_Show) {
                    [self movePopToShowFromHeight: initYHeight toHeight: finalYHeight withAnimationCurve: animationCurve andDuration: animationDuration];
                } else if (popButtonsState_ == pbse_Relocate) {
                    [self movePopToHeight: finalYHeight withAnimationCurve: animationCurve andDuration: animationDuration];
                }
            } else {
                if (popButtonsState_ == pbse_Show) {
                    [self fadeInPopToShowAtHeight:finalYHeight withAnimationCurve:UIViewAnimationCurveLinear andDuration: 0.3f];
                } else {
                    [self movePopToHeight: finalYHeight withAnimationCurve: UIViewAnimationCurveLinear andDuration: 0.3f];
                }
            }
        }
    }

#endif

}

/**
 * Invoked by framework when a text field is about to make the keyboard disappear. Used to animate the
 * pop buttons view
 *
 * @param aNotification The notification information
 */
- (void) keyboardWillDisappear: (NSNotification*) aNotification {
    
#if defined(__IPHONE_3_2) && (__IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2)
    
    CGRect scrollFrame = scrollNominalFrame_;
    
    if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
        
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationCurveLinear
                         animations:^ {
                             
                             transparentScroll_.center = CGPointMake((CGRectGetMinX(scrollFrame) + CGRectGetMaxX(scrollFrame)) / 2.0f,
                                                                     (CGRectGetMinY(scrollFrame) + CGRectGetMaxY(scrollFrame)) / 2.0f);
                             transparentScroll_.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(scrollFrame), CGRectGetHeight(scrollFrame));
                             
                         }
                         completion:nil];
        
    } else {
        
        [UIView beginAnimations:nil context: nil];
        [UIView setAnimationCurve: UIViewAnimationCurveLinear];
        [UIView setAnimationDuration: 0.3f];
        
        transparentScroll_.center = CGPointMake((CGRectGetMinX(scrollFrame) + CGRectGetMaxX(scrollFrame)) / 2.0f,
                                                (CGRectGetMinY(scrollFrame) + CGRectGetMaxY(scrollFrame)) / 2.0f);
        transparentScroll_.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(scrollFrame), CGRectGetHeight(scrollFrame));
        
        [UIView commitAnimations];
        
    }
    
    [self makeViewVisible:editedView_];
    isKeyboardShowing_ = NO;
    isPopBarShowing_ = NO;
    
#else
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] != NSOrderedAscending) {
        
        CGRect scrollFrame = scrollNominalFrame_;
        
        if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
            
            [UIView animateWithDuration:0.3f
                                  delay:0.0f
                                options:UIViewAnimationCurveLinear
                             animations:^ {
                                 
                                 transparentScroll_.center = CGPointMake((CGRectGetMinX(scrollFrame) + CGRectGetMaxX(scrollFrame)) / 2.0f,
                                                                         (CGRectGetMinY(scrollFrame) + CGRectGetMaxY(scrollFrame)) / 2.0f);
                                 transparentScroll_.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(scrollFrame), CGRectGetHeight(scrollFrame));
                                 
                             }
                             completion:nil];
            
        } else {
            
            [UIView beginAnimations:nil context: nil];
            [UIView setAnimationCurve: UIViewAnimationCurveLinear];
            [UIView setAnimationDuration: 0.3f];
            
            transparentScroll_.center = CGPointMake((CGRectGetMinX(scrollFrame) + CGRectGetMaxX(scrollFrame)) / 2.0f,
                                                    (CGRectGetMinY(scrollFrame) + CGRectGetMaxY(scrollFrame)) / 2.0f);
            transparentScroll_.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(scrollFrame), CGRectGetHeight(scrollFrame));
            
            [UIView commitAnimations];
            
        }
        [self makeViewVisible:editedView_];
        isKeyboardShowing_ = NO;
        isPopBarShowing_ = NO;
        
    } else {
        
        if (popButtonsState_ == pbse_Hide) {
            NSDictionary* userInfo = aNotification.userInfo;
            NSValue* value;// = [userInfo objectForKey: UIKeyboardCenterBeginUserInfoKey];
            
            value = [userInfo objectForKey: UIKeyboardCenterEndUserInfoKey];
            CGPoint endCenter = [value CGPointValue];
            
            value = [userInfo objectForKey: UIKeyboardBoundsUserInfoKey];
            CGRect keyboardBounds = [value CGRectValue];
            
            BOOL animationDefined = NO;
            UIViewAnimationCurve animationCurve;
            double animationDuration;
            
            value = [userInfo objectForKey: UIKeyboardAnimationCurveUserInfoKey];
            animationDefined = YES;
            
            if (value != nil) {
                [value getValue: &animationCurve];
                
                value = [userInfo objectForKey: UIKeyboardAnimationDurationUserInfoKey];
                
                if (value != nil) {
                    [value getValue: &animationDuration];
                } else {
                    animationDefined = NO;
                }
            } else {
                animationDefined = NO;
            }
            
            if (animationDefined == YES) {
                UIWindow* window = self.view.window;
                CGRect windowFrame = window.frame;
                
                CGFloat finalYHeight = windowFrame.size.height - (endCenter.y - (keyboardBounds.size.height / 2.0f));
                [self movePopToHideAtHeight: finalYHeight withAnimationCurve: animationCurve andDuration: animationDuration];
            } else {
                [self fadeOutPopToHideWithAnimationCurve: UIViewAnimationCurveLinear andDuration: 0.3f];
            }
        }
    }

#endif

}

#pragma mark -
#pragma mark PopButtonsViewDelegate selectors

/**
 * Informs the pop buttons view delegate that OK button was clicked. Picker is dismissed
 * when showing predictive information
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void) okButtonClickedInPopButtonsView: (PopButtonsView*) aPopButtonsView {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] != NSOrderedAscending) {
        
        [editedView_ resignFirstResponder];
        [editedView_ release];
        editedView_ = nil;    
        
    } else {
        
        popButtonsState_ = pbse_Hide;
        
        if (showingPredictiveInfo_ == YES) {
            [self movePickerToHideAndPopUp: YES];
            showingPredictiveInfo_ = NO;
        }

        [editedView_ resignFirstResponder];
        
        if (![editedView_ isKindOfClass:[UITextField class]] && ![editedView_ isKindOfClass:[NXTSearchBar class]]) {
            [self movePickerToHideAndPopUp:YES];
        }
        
        [editedView_ release];
        editedView_ = nil;

    }
    
}

/*
 * Informs the delegate that previous responder button was clicked
 */
- (void)previousButtonClickedInPopButtonsView:(PopButtonsView *)aPopButtonsView {
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] != NSOrderedAscending) {
        
        if ([editableViews_ indexOfObject:editedView_] > 0) {
            
            UIView *auxView = [editableViews_ objectAtIndex:[editableViews_ indexOfObject:editedView_] - 1];
            
            if (auxView != editedView_) {
                
                [auxView retain];
                [editedView_ release];
                editedView_ = auxView;
                
            }
            
            [self editableViewHasBeenClicked:editedView_];
            
        }
        
    } else {
        
        if ([editableViews_ indexOfObject:editedView_] > 0) {
            
            UIView *previousView = [editableViews_ objectAtIndex:[editableViews_ indexOfObject:editedView_] - 1];
            
            [editedView_ resignFirstResponder];
            
            if (![editedView_ isKindOfClass:[UITextField class]]) {
                [self movePickerToHideAndPopUp:NO];
            }
            
            if (editedView_ != previousView) {
                
                [previousView retain];
                [editedView_ release];
                editedView_ = previousView;

            }
                
            [self editableViewHasBeenClicked:editedView_];
            
        }
    }
}

/*
 * Informs the delegate that next responder button was clicked
 */
- (void)nextButtonClickedInPopButtonsView:(PopButtonsView *)aPopButtonsView {
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] != NSOrderedAscending) {
        
        if ([editableViews_ indexOfObject:editedView_] < [editableViews_ count] - 1) {
            
            UIView *auxView = [editableViews_ objectAtIndex:[editableViews_ indexOfObject:editedView_] + 1];
            
            if (auxView != editedView_) {
                
                [auxView retain];
                [editedView_ release];
                editedView_ = auxView;
                
            }
            
            [self editableViewHasBeenClicked:editedView_];
            
        }
        
    } else {
     
        if ([editableViews_ indexOfObject:editedView_] < [editableViews_ count] - 1) {
            
            UIView *nextView = [editableViews_ objectAtIndex:[editableViews_ indexOfObject:editedView_] + 1];

            [editedView_ resignFirstResponder];
            
            if (![editedView_ isKindOfClass:[UITextField class]]) {
                [self movePickerToHideAndPopUp:NO];
            }
            
            if (editedView_ != nextView) {
                
                [nextView retain];
                [editedView_ release];
                editedView_ = nextView;

            }
            
            [self editableViewHasBeenClicked:editedView_];
                
        }
    }
}


#pragma mark -
#pragma mark Properties methods

/*
 * Returns the NXT application delegate
 */
- (NXT_Peru_iPhone_AppDelegate *)appDelegate {
    return (NXT_Peru_iPhone_AppDelegate *)[UIApplication sharedApplication].delegate;
}

/**
 * Sets the BBVA logo as the navigation item title
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem * result = [super navigationItem];
    
    if (result.titleView == nil) {
        
        result.titleView = [[[UIImageView alloc] initWithImage:[[ImagesCache getInstance] imageNamed:BBVA_LOGO_IMAGE_FILE_NAME]] autorelease];
        
    }
    
    return result;
    
}

@end
