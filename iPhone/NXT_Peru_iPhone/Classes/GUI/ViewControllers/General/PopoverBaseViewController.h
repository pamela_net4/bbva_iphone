/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTViewController.h"

/**
 * Base view controller for all the view controllers that will be shown inside a 
 * UIPopoverController; but this view can also be used as base class of regular iPhone view controller.
 *
 * Children must access to the navigation item via properties of self instance
 * because self.navigationItem and self.title are overriden to do it transparent, as a regular navigationItem.
 *
 * Because of this behavior, this class can be used as base of a regular iPhone view controller (children of CompassViewController).
 * iPhone views never will be shown inside a UIPOpoverController but when accessing to navigation item it 
 * returns the super one and not the fake one
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PopoverBaseViewController : NXTViewController {
@private
    /**
     * Navigation bar. Remember to link it via XIB
     */
    UINavigationBar *fakeNavigationBar_;
}

/**
 * Provides readwrite access to the fakeNavigationBar_. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UINavigationBar *fakeNavigationBar;

@end
