/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PopoverBaseViewController.h"

@implementation PopoverBaseViewController

@synthesize fakeNavigationBar = fakeNavigationBar_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    [fakeNavigationBar_ release];
    fakeNavigationBar_ = nil;
    
    [super dealloc];
}

#pragma mark - 
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    [super viewDidLoad];
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    [super viewDidUnload];
    
    self.fakeNavigationBar = nil;
}

#pragma mark -
#pragma mark Overriden properties

/**
 * Override this to set title to the fake navigation bar, if not nil, or to super navigation item
 *
 * @param title The title of the view
 */
- (void)setTitle:(NSString *)title {
    
    if (fakeNavigationBar_ != nil) { // In case fake bar is nil we set super as regular
    
        fakeNavigationBar_.topItem.title = title;
        
    } else {
        
        super.title = title;
    }
}

/**
 * Override this to return the top navigation item of our fake navigation bar, if not nil, or the super one
 *
 * @return The correct navigation bar  item
 */
- (UINavigationItem *)navigationItem {
    
    if (fakeNavigationBar_ != nil) { // In case fake bar is nil we return super as regular
    
        return fakeNavigationBar_.topItem;
        
    } else {
     
        return super.navigationItem;
    }        
}

@end
