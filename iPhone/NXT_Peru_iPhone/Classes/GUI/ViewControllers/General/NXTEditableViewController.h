/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "NXTViewController.h"
#import "PopButtonsView.h"


/**
 * Defines the requred version of the iOS to perform certain operations
 */
#define MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION                          @"3.2"

/**
 * Maximum number of characters on certain fields
 */
#define MAX_NUMBER_OF_CHARACTERS                                            20


//Forward declarations



/**
 * Protocol implemented by UIDatePicker observers when the value of the date picker changes.
 */
@protocol UIDatePickerProtocol
@optional

/**
 * Responds to the event of changing the value of the date picker
 */
- (void) changeDateInPicker;

@end


/**
 * NXT editable view controller. Base view controller for all NXT application editable view controllers. Implements
 * basic behaviour common to all editable views
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTEditableViewController : NXTViewController <PopButtonsViewDelegate> {

@protected
    
    /**
     * Transparent scroll to provide scrolling capabilities
     */
    UIScrollView *transparentScroll_;
    
    /**
     * Transparent view to hold all subviews created from NIB file
     */
    UIView *scrollableView_;
	
	/**
	 * View currently being edited. Child view must set this attribute before animation starts to
	 * allow the subview to automatically move the view up or down
	 */
	UIView* editedView_;
    
    /**
     * Header view to show on top of controller's view
     */
    UIView *topHeaderView_;
    
	/**
	 * Pop buttons view to complement the keyboard
	 */
	PopButtonsView *popButtonsView_;
	
    /**
	 * Current text field being edited. Used when switching from keyboard to picker on predictive text events
	 */
	UITextField* currentTextView_;
    
	/**
	 * Pop buttons view state
	 */
	PopButtonsStateEnum popButtonsState_;
	
	/**
	 * Predictive text array to show
	 */
	NSArray* predictiveText_;
	
	/**
	 * Current text from text view
	 */
	NSString *currentTextViewText_;
    
    /**
     * Scrollable top position
     */
    CGFloat scrollableTop_;
	
    /**
     * Scroll nominal frame
     */
    CGRect scrollNominalFrame_;
    
	/**
	 * Showing predictive information flag. YES when picker containing predictive information is shown
	 */
	BOOL showingPredictiveInfo_;
	
	/**
	 * If true, the data in the UI elements of ViewController should be cleared in the viewWillAppear method. The classes
	 * that inherits NXTEditableViewController are responsible of doing this clearing. 
	 */
	BOOL shouldClearInterfaceData_;
    
    /**
     * Array with the editable views on the view. We use this array to navigate from one view to anoother when we
     * click on the Next and Previous buttons of the PopButtonsView. On load of the subclassed view controller we must
     * fill this array with the views <b>in the order we want to navigate as we click the buttons</b>.
     */
    NSMutableArray *editableViews_;
    
    /**
     * Flag that indicates that the keyboard is showing
     */
    BOOL isKeyboardShowing_;
    
    /**
     * Flag that indicates that the buttons pop bar is showing
     */
    BOOL isPopBarShowing_;
    
    /**
     * Internal scrollable view flag. YES when the scrollable view was created internally, NO when it was provided by a subclass
     */
    BOOL internalScrollableView_;
    
}


/**
 * Provides read-only access to the pop buttons view to complement the keyboard
 */
@property (nonatomic, readonly, retain) PopButtonsView *popButtonsView;

/**
 * Provides readwrite access to the shouldClearInterfaceData
 */
@property (nonatomic, readwrite, assign) BOOL shouldClearInterfaceData;


/**
 * Recalculates the scroll
 */
- (void)recalculateScroll;

/**
 * Resets the scroll position to the top left
 *
 * @param animated YES to animate the scroll re-position, NO to re-position without animation
 */
- (void)resetScrollToTopLeftAnimated:(BOOL)animated;

@end
