/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "Tooltip.h"

//Forward declarations
@class NXT_Peru_iPhone_AppDelegate;
@class NXTNavigationItem;


/**
 * NXT view controller. Base view controller for all NXT application view controllers. Implements
 * basic behaviour common to all views
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTViewController : UIViewController <TooltipDelegate>{
    
@private
    
    /**
     * Private custom nativation item
     */
    NXTNavigationItem *customNavigationItem_;
    
    NSMutableArray * tooltips_;
    
}

/**
 * Provides read-only access to the NXT application delegate
 */
@property (nonatomic, readonly, retain) NXT_Peru_iPhone_AppDelegate *appDelegate;

/**
 * Provides read-only access to the custom natigation item
 */
@property (nonatomic, readonly, retain) NXTNavigationItem *customNavigationItem;

-(void)showInfoTooltipFromView:(UIView*)fromView message:(NSString*) message;
-(void)showErrorTooltipFromView:(UIView*)fromView message:(NSString*) message;
-(void)showErrorTooltipFromView:(UIView*)fromView message:(NSString*) message delegate:(id)delegate;

@end
