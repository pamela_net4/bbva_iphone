/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "NXTEditableViewController.h"

/**
 * Defines the delay of the workaround use on 3.X to make visible a view different from editedView
 */
#define MAKE_VIEW_VISIBLE_3X_WORKAROUND_DELAY                 0.03

/**
 * NXTEditableViewController protected category
 */
@interface NXTEditableViewController(protected)

/**
 * Once all the necesary subviews has been laid out, child view controllers ask their parent to put them in the scroller via this selector
 *
 * @protected
 */
- (void)setSubviewsIntoScroller;

/**
 * Child view controllers provide the parent view controller with a header view
 *
 * @param aHeaderView The header view to show on top of the controller's view
 * @protected
 */
- (void)setHeaderView:(UIView *)aHeaderView;

/**
 * Child classes must return the offset from the last control in the view to the
 * view bottom. Default is 0
 *
 * @return The offset between the last control in the view and the view bottom
 * @protected
 */
- (CGFloat)distanceFromLastControlToViewBottom;

/**
 * Sets the scrollable view
 *
 * @param aScrollableView The scrollable view to set inside the scroll
 * @protected
 */
- (void)setScrollableView:(UIView *)aScrollableView;

/**
 * Sets the scrollable view top position measured in points from the view top border. The scrollable view will expand
 * from this top position to the view bottom position
 *
 * @param aTopPosition The new scrollable top position
 * @protected
 */
- (void)setScrollableTopPosition:(CGFloat)aTopPosition;

/**
 * Sets the scroll nominal frame. This nominal frame is used when no keyboard or picker is displayed. Default value occupies the whole view
 *
 * @param scrollNominalFrame The new scroll nominal frame to set
 * @protected
 */
- (void)setScrollNominalFrame:(CGRect)scrollNominalFrame;

/**
 * Fades in the pop buttons view to show it at a given height. If pop buttons view is already in view, only
 * transparent view position is calculated
 *
 * @param aFinalHeight The pop buttons view bottom final height, measured from window bottom
 * @param anAnimationCurve The animation curve to use
 * @param anAnimationDuration The animation duration
 * @protected
 */
- (void) fadeInPopToShowAtHeight: (CGFloat) aFinalHeight withAnimationCurve: (UIViewAnimationCurve) anAnimationCurve
					 andDuration: (double) anAnimationDuration;

/**
 * Fades out the pop buttons view to hide it. If pop buttons view is not in view, no operation is performed
 *
 * @param anAnimationCurve The animation curve to use
 * @param anAnimationDuration The animation duration
 * @protected
 */
- (void) fadeOutPopToHideWithAnimationCurve: (UIViewAnimationCurve) anAnimationCurve
								andDuration: (double) anAnimationDuration;

/**
 * Moves the pop buttons view to show from an initial height to a final height position. If pop buttons view is already
 * in view, only transparent view position is calculated
 *
 * @param anInitHeight The pop buttons view bottom initial height, measured from window bottom
 * @param aFinalHeight The pop buttons view bottom final height, measured from  window bottom
 * @param anAnimationCurve The animation curve to use
 * @param anAnimationDuration The animation duration
 * @protected
 */
- (void) movePopToShowFromHeight: (CGFloat) anInitHeight toHeight: (CGFloat) aFinalHeight
			  withAnimationCurve: (UIViewAnimationCurve) anAnimationCurve andDuration: (double) anAnimationDuration;

/**
 * Moves the pop buttons view to hide at a final height position. If pop buttons view is not in view no operation is performed
 *
 * @param aFinalHeight The pop buttons view bottom final height, measured from window bottom
 * @param anAnimationCurve The animation curve to use
 * @param anAnimationDuration The animation duration
 * @protected
 */
- (void) movePopToHideAtHeight: (CGFloat) aFinalHeight withAnimationCurve: (UIViewAnimationCurve) anAnimationCurve
				   andDuration: (double) anAnimationDuration;

/**
 * Moves the pop buttons view to a final height position. If pop buttons view is not in view no operation is performed
 *
 * @param aFinalHeight The pop buttons view bottom final height, measured from window bottom
 * @param anAnimationCurve The animation curve to use
 * @param anAnimationDuration The animation duration
 * @protected
 */
- (void) movePopToHeight: (CGFloat) aFinalHeight withAnimationCurve: (UIViewAnimationCurve) anAnimationCurve
			 andDuration: (double) anAnimationDuration;

/**
 * Shows the picker in case it is hidden, The picker is animated from the window bottom to the final position
 *
 * @protected
 */
- (void) movePickerToShow;

/**
 * Hides the picker in case it is visible, The picker is animated to the window bottom. In case it is necesary,
 * the pop up is also hiden
 *
 * @param aHidePopUp YES to hide also the pop up, NO otherwise
 * @protected
 */
- (void) movePickerToHideAndPopUp: (BOOL) aHidePopUp;

/**
 * Makes the given view visible inside the scroller
 *
 * @param aView The view to make visible
 * @protected
 */
- (void) makeViewVisible: (UIView*) aView;

/**
 * Invoked by framework when animation finishes. If animation is a pop buttons view hide animation,
 * pop buttons view is removed from view
 *
 * @param animationID An NSString containing an optional application-supplied identifier
 * @param finished An NSNumber object containing a Boolean value. The value is YES if the animation ran to completion before it stopped or NO if it did not
 * @param context An optional application-supplied context
 * @protected
 */
- (void) animationDidStop: (NSString*) animationID finished: (NSNumber*) finished context: (void*) context;

/**
 * An editable view has been clicked and has the focus
 *
 * @protected
 */
- (void)editedViewHasFocus;

/**
 * A view has been clicked
 *
 * @param theView clicked
 * @protected
 */
- (void)editableViewHasBeenClicked:(UIView *)theView;

/**
 * Validates the formulary
 *
 * @return YES if ok
 * @protected
 */
- (BOOL)validateForm;

/**
 * Calculates the scroller frame when the editing view is displayed
 *
 * @param editingViewFrame The editing view frame in windows coordinates
 * @return The scroller frame when the editing frame is displayed
 * @protected
 */
- (CGRect)scrollerFrameForEditingViewFrame:(CGRect)editingViewFrame;

/**
 * set the position of TransparentScrollView
 *
 * @param change the position of the view.
 * @protected
 */
- (void)SetContentOffSetScrollView:(CGPoint)contentOffset;

@end
