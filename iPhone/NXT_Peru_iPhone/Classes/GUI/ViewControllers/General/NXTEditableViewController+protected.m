/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "NXTEditableViewController.h"
#import "NXTEditableViewController+protected.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "PopButtonsView.h"
#import "NXTTextField.h"
#import "NXTTextView.h"
#import "NXTComboButton.h"
#import "Tools.h"
#import "NXTCurrencyTextField.h"
#import "NXTSearchBar.h"

/**
 * Defines the pop buttons view show animation
 */
#define POP_BUTTONS_VIEW_SHOW_ANIMATION										@"PopButtonsViewShowAnimation"

/**
 * Defines the pop buttons view hide animation
 */
#define POP_BUTTONS_VIEW_HIDE_ANIMATION										@"PopButtonsViewHideAnimation"

/**
 * Defines the pop buttons view relocate animation
 */
#define POP_BUTTONS_VIEW_RELOCATE_ANIMATION									@"PopButtonsViewRelocateAnimation"

/**
 * Defines the picker view show animation
 */
#define PICKER_VIEW_SHOW_ANIMATION											@"PickerViewShowAnimation"

/**
 * Defines the picker view hide animation
 */
#define PICKER_VIEW_HIDE_ANIMATION											@"PickerViewHideAnimation"

/**
 * Picker animation duration, in seconds
 */
#define PICKER_ANIMATION_DURATION											0.3f

/**
 * Minimum distance from pop buttons view top to edited view bottom
 */
#define MIN_VERTICAL_DISTANCE_TO_EDITING_VIEW								20.0f


#pragma mark -

@implementation NXTEditableViewController(protected)

#pragma mark -
#pragma mark Internal scrollable view management

/*
 * Once all the necesary subviews has been laid out, child view controllers ask their parent to put them in the scroller via this selector
 */
- (void)setSubviewsIntoScroller {
    
    CGRect ownViewFrame = self.view.frame;
    
    scrollNominalFrame_ = ownViewFrame;
    scrollNominalFrame_.origin.x = 0.0f;
    scrollNominalFrame_.origin.y = 0.0f;
    
    if (topHeaderView_ != nil) {
        
        CGRect topHeaderFrame = topHeaderView_.frame;
        CGFloat topHeaderMaxY = CGRectGetMaxY(topHeaderFrame);
        scrollNominalFrame_.size.height = CGRectGetHeight(scrollNominalFrame_) - topHeaderMaxY;
        scrollNominalFrame_.origin.y = topHeaderMaxY;
        
    }
    
    transparentScroll_.frame = scrollNominalFrame_;
    
    [scrollableView_ removeFromSuperview];
    [scrollableView_ release];
    scrollableView_ = nil;
        
    scrollableView_ = [[UIView alloc] initWithFrame:ownViewFrame];
    [scrollableView_ setBackgroundColor:[UIColor clearColor]];;
    [transparentScroll_ addSubview:scrollableView_];
    
    internalScrollableView_ = YES;
    
    NSArray *subviews = [NSArray arrayWithArray:[self.view subviews]];
    CGFloat maxY = 0;
    CGRect viewFrame;
    
    for (UIView *subview in subviews) {
        
        if ((subview != topHeaderView_) && (subview != transparentScroll_)) {
            
            [subview removeFromSuperview];
            [scrollableView_ addSubview:subview];
            
            viewFrame = subview.frame;
            CGFloat auxVal = CGRectGetMaxY(viewFrame);
            
            if ((maxY < auxVal) && (subview.hidden == NO)) {
                
                maxY = auxVal;
                
            }
            
        }
        
    }
    
    maxY += [self distanceFromLastControlToViewBottom];
    
    CGRect transparentViewFrame = scrollableView_.frame;
    scrollableView_.frame = CGRectMake(0.0f, CGRectGetMinY(transparentViewFrame), CGRectGetWidth(transparentViewFrame), maxY);
    transparentScroll_.contentSize = CGSizeMake(self.view.frame.size.width, maxY);
    transparentScroll_.scrollEnabled = YES;
    
}

/*
 * Child view controllers provide the parent view controller with a header view
 */
- (void)setHeaderView:(UIView *)aHeaderView {
    
    if (internalScrollableView_) {
        
        if (aHeaderView != topHeaderView_) {
            
            [topHeaderView_ removeFromSuperview];
            [topHeaderView_ release];
            topHeaderView_ = [aHeaderView retain];
            CGRect headerFrame = topHeaderView_.frame;
            headerFrame.origin.y = 0.0f;
            topHeaderView_.frame = headerFrame;
            [self.view addSubview:topHeaderView_];
            
            if (transparentScroll_ != nil) {
                
                CGFloat headerMaxY = CGRectGetMaxY(headerFrame);
                scrollNominalFrame_ = self.view.frame;
                scrollNominalFrame_.size.height = CGRectGetHeight(scrollNominalFrame_) - headerMaxY;
                scrollNominalFrame_.origin.y = headerMaxY;
                transparentScroll_.frame = scrollNominalFrame_;
                
            }
            
        }
        
    }
    
}

/*
 * Child classes must return the offset from the last control in the view to the
 * view bottom. Default is 0
 */
- (CGFloat)distanceFromLastControlToViewBottom {
    
    return 0.0f;
    
}

#pragma mark -
#pragma mark External scrollable view

/*
 * Sets the scrollable view
 */
- (void)setScrollableView:(UIView *)aScrollableView {
    
    if (topHeaderView_ != nil) {
        
        [topHeaderView_ release];
        topHeaderView_ = nil;
        
    }
    
    if (scrollableView_ != aScrollableView) {
        
        [scrollableView_ removeFromSuperview];
        
        [scrollableView_ release];
        scrollableView_ = nil;
        scrollableView_ = [aScrollableView retain];
        
        scrollableView_.autoresizingMask = UIViewAutoresizingNone;
        
        [transparentScroll_ addSubview:scrollableView_];
        [transparentScroll_ setContentSize:scrollableView_.frame.size];
        
        if (scrollableView_ != nil) {
            
            transparentScroll_.userInteractionEnabled = YES;
            
        } else {
            
            transparentScroll_.userInteractionEnabled = NO;
            
        }
        
    }
    
    internalScrollableView_ = NO;

}

/*
 * Sets the scrollable view top position measured in points from the view top border. The scrollable view will expand
 * from this top position to the view bottom position
 */
- (void)setScrollableTopPosition:(CGFloat)aTopPosition {
    
    if (!internalScrollableView_) {
        
        if (aTopPosition != scrollableTop_) {
            
            scrollableTop_ = (aTopPosition > 0.0f) ? aTopPosition : 0.0f;
            
            CGRect frame = self.view.frame;
            CGRect scrollFrame = CGRectMake(0.0f, scrollableTop_, frame.size.width, frame.size.height - scrollableTop_);
            transparentScroll_.frame = scrollFrame;
            
        }

    }
    
}

/*
 * Sets the scroll nominal frame. This nominal frame is used when no keyboard or picker is displayed. Default value occupies the whole view
 */
- (void)setScrollNominalFrame:(CGRect)scrollNominalFrame {
    
    if (!internalScrollableView_) {
        
        scrollNominalFrame_ = scrollNominalFrame;
        
        if (!isPopBarShowing_) {
            
            transparentScroll_.frame = scrollNominalFrame;
            
        }

    }
    
}

#pragma mark -
#pragma mark Pop buttons view showing and hiding

/*
 * Fades in the pop buttons view to show it at a given height. If pop buttons view is already in view, only
 * transparent view position is calculated
 */
- (void) fadeInPopToShowAtHeight: (CGFloat) aFinalHeight withAnimationCurve: (UIViewAnimationCurve) anAnimationCurve
					 andDuration: (double) anAnimationDuration {
    
    CGSize popSize = popButtonsView_.frame.size;
	CGRect viewFrame = self.view.frame;
	
	UIWindow* window = [UIApplication sharedApplication].keyWindow;
	CGRect windowFrame = window.frame;
	CGFloat windowYCoord = windowFrame.size.height - aFinalHeight;
	CGPoint popTopLeftPoint = [window convertPoint: CGPointMake(0.0f, windowYCoord - popSize.height) toView: self.view];
	
	CGRect scrollFrame = transparentScroll_.frame;
	scrollFrame.size.height = popTopLeftPoint.y - CGRectGetMinY(scrollFrame);
	
	BOOL animatePopButtonsView = NO;
	
	if (popButtonsView_.superview == nil) {
		[window addSubview: popButtonsView_];
		popButtonsView_.alpha = 0.0f;
		popButtonsView_.frame = CGRectMake(0.0f, CGRectGetHeight(windowFrame) - popSize.height - aFinalHeight, viewFrame.size.width, popSize.height);
		animatePopButtonsView = YES;
	}
	
	[popButtonsView_ enableEventForwarding:YES];
	
    if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
        
        [UIView animateWithDuration:anAnimationDuration
                              delay:0.0f
                            options:anAnimationCurve
                         animations:^ {
                             
                             if (animatePopButtonsView == YES)
                             {
                                 popButtonsView_.alpha = 1.0f;
                             }
                             
                             transparentScroll_.center = CGPointMake((CGRectGetMinX(scrollFrame) + CGRectGetMaxX(scrollFrame)) / 2.0f,
                                                                     (CGRectGetMinY(scrollFrame) + CGRectGetMaxY(scrollFrame)) / 2.0f);
                             transparentScroll_.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(scrollFrame), CGRectGetHeight(scrollFrame));

                         }
                         completion:^ (BOOL finised) {
                             
                             [self animationDidStop:POP_BUTTONS_VIEW_SHOW_ANIMATION
                                           finished:[NSNumber numberWithBool:finised]
                                            context:nil];
                             
                         }];
        
    } else {
        
        [UIView beginAnimations: POP_BUTTONS_VIEW_SHOW_ANIMATION context: nil];
        [UIView setAnimationCurve: anAnimationCurve];
        [UIView setAnimationDuration: anAnimationDuration];
        [UIView setAnimationDelegate: self];
        [UIView setAnimationDidStopSelector: @selector(animationDidStop: finished: context:)];
        
        if (animatePopButtonsView == YES)
        {
            popButtonsView_.alpha = 1.0f;
        }
        
        transparentScroll_.center = CGPointMake((CGRectGetMinX(scrollFrame) + CGRectGetMaxX(scrollFrame)) / 2.0f,
                                                (CGRectGetMinY(scrollFrame) + CGRectGetMaxY(scrollFrame)) / 2.0f);
        transparentScroll_.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(scrollFrame), CGRectGetHeight(scrollFrame));
        
        [UIView commitAnimations];
        
    }
	
	[self makeViewVisible: editedView_];
    
    isPopBarShowing_ = YES;
    
}

/*
 * Fades out the pop buttons view to hide it. If pop buttons view is not in view, no operation is performed
 */
- (void) fadeOutPopToHideWithAnimationCurve: (UIViewAnimationCurve) anAnimationCurve
								andDuration: (double) anAnimationDuration {
    
	if (popButtonsView_.superview != nil) {
        
		[popButtonsView_ enableEventForwarding:YES];
        
        if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
            
            [UIView animateWithDuration:anAnimationDuration
                                  delay:0.0f
                                options:anAnimationCurve
                             animations:^ {
                                 
                                 popButtonsView_.alpha = 0.0f;
                                 CGRect scrollFrame = scrollNominalFrame_;
                                 transparentScroll_.center = CGPointMake((CGRectGetMinX(scrollFrame) + CGRectGetMaxX(scrollFrame)) / 2.0f,
                                                                         (CGRectGetMinY(scrollFrame) + CGRectGetMaxY(scrollFrame)) / 2.0f);
                                 transparentScroll_.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(scrollFrame), CGRectGetHeight(scrollFrame));
                                 
                             }
                             completion:^ (BOOL finised) {
                                 
                                 [self animationDidStop:POP_BUTTONS_VIEW_HIDE_ANIMATION
                                               finished:[NSNumber numberWithBool:finised]
                                                context:nil];
                                 
                             }];
            
        } else {
                
            [UIView beginAnimations: POP_BUTTONS_VIEW_HIDE_ANIMATION context: nil];
            [UIView setAnimationCurve: anAnimationCurve];
            [UIView setAnimationDuration: anAnimationDuration];
            [UIView setAnimationDelegate: self];
            [UIView setAnimationDidStopSelector: @selector(animationDidStop: finished: context:)];
            
            popButtonsView_.alpha = 0.0f;
            CGRect scrollFrame = scrollNominalFrame_;
            transparentScroll_.center = CGPointMake((CGRectGetMinX(scrollFrame) + CGRectGetMaxX(scrollFrame)) / 2.0f,
                                                    (CGRectGetMinY(scrollFrame) + CGRectGetMaxY(scrollFrame)) / 2.0f);
            transparentScroll_.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(scrollFrame), CGRectGetHeight(scrollFrame));
            
            [UIView commitAnimations];
            
        }
        
	}
    
    isPopBarShowing_ = NO;
    
}

/*
 * Moves the pop buttons view to a final height position. If pop buttons view is not in view no operation is performed
 */
- (void) movePopToHeight: (CGFloat) aFinalHeight withAnimationCurve: (UIViewAnimationCurve) anAnimationCurve
			 andDuration: (double) anAnimationDuration {
    
	if (popButtonsView_.superview != nil) {
        
		CGSize popSize = popButtonsView_.frame.size;
		CGRect viewFrame = self.view.frame;
		
		UIWindow* window = [UIApplication sharedApplication].keyWindow;
		CGRect windowFrame = window.frame;
		CGFloat finalWindowYCoord = windowFrame.size.height - aFinalHeight;
		CGPoint finalPopTopLeftPoint = [window convertPoint: CGPointMake(0.0f, finalWindowYCoord - popSize.height) toView: self.view]; 
		
		CGRect scrollFrame = transparentScroll_.frame;
		scrollFrame.size.height = finalPopTopLeftPoint.y - CGRectGetMinY(scrollFrame);
		
		[popButtonsView_ enableEventForwarding:YES];
		
        if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
            
            [UIView animateWithDuration:anAnimationDuration
                                  delay:0.0f
                                options:anAnimationCurve
                             animations:^ {
                                 
                                 popButtonsView_.frame = CGRectMake(0.0f, CGRectGetHeight(windowFrame) - popSize.height - aFinalHeight , viewFrame.size.width, popSize.height);
                                 
                                 transparentScroll_.center = CGPointMake((CGRectGetMinX(scrollFrame) + CGRectGetMaxX(scrollFrame)) / 2.0f,
                                                                         (CGRectGetMinY(scrollFrame) + CGRectGetMaxY(scrollFrame)) / 2.0f);
                                 transparentScroll_.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(scrollFrame), CGRectGetHeight(scrollFrame));
                                 
                             }
                             completion:^ (BOOL finised) {
                                 
                                 [self animationDidStop:POP_BUTTONS_VIEW_RELOCATE_ANIMATION
                                               finished:[NSNumber numberWithBool:finised]
                                                context:nil];
                                 
                             }];
            
        } else {
            
            [UIView beginAnimations: POP_BUTTONS_VIEW_RELOCATE_ANIMATION context: nil];
            [UIView setAnimationCurve: anAnimationCurve];
            [UIView setAnimationDuration: anAnimationDuration];
            [UIView setAnimationDelegate: self];
            [UIView setAnimationDidStopSelector: @selector(animationDidStop: finished: context:)];
            
            popButtonsView_.frame = CGRectMake(0.0f, CGRectGetHeight(windowFrame) - popSize.height - aFinalHeight , viewFrame.size.width, popSize.height);
            
            transparentScroll_.center = CGPointMake((CGRectGetMinX(scrollFrame) + CGRectGetMaxX(scrollFrame)) / 2.0f,
                                                    (CGRectGetMinY(scrollFrame) + CGRectGetMaxY(scrollFrame)) / 2.0f);
            transparentScroll_.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(scrollFrame), CGRectGetHeight(scrollFrame));
            
            [UIView commitAnimations];
            
        }
		
		[self makeViewVisible: editedView_];
        [self.view bringSubviewToFront:popButtonsView_];
        
        isPopBarShowing_ = YES;
        
	}
    
}

/*
 * Moves the pop buttons view to hide at a final height position. If pop buttons view is not in view no operation is performed
 */
- (void) movePopToHideAtHeight: (CGFloat) aFinalHeight withAnimationCurve: (UIViewAnimationCurve) anAnimationCurve
				   andDuration: (double) anAnimationDuration {
    
	if (popButtonsView_.superview != nil) {
        
		CGSize popSize = popButtonsView_.frame.size;
		CGRect viewFrame = self.view.frame;
		
		UIWindow* window = [UIApplication sharedApplication].keyWindow;
		CGRect windowFrame = window.frame;
		
		[popButtonsView_ enableEventForwarding:YES];
		
		[UIView beginAnimations: POP_BUTTONS_VIEW_HIDE_ANIMATION context: nil];
		[UIView setAnimationCurve: anAnimationCurve];
		[UIView setAnimationDuration: anAnimationDuration];
		
		[UIView setAnimationDelegate: self];
		[UIView setAnimationDidStopSelector: @selector(animationDidStop: finished: context:)];
		
		popButtonsView_.frame = CGRectMake(0.0f, CGRectGetHeight(windowFrame), viewFrame.size.width, popSize.height);
		CGRect scrollFrame = scrollNominalFrame_;
		transparentScroll_.center = CGPointMake((CGRectGetMinX(scrollFrame) + CGRectGetMaxX(scrollFrame)) / 2.0f,
												(CGRectGetMinY(scrollFrame) + CGRectGetMaxY(scrollFrame)) / 2.0f);
		transparentScroll_.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(scrollFrame), CGRectGetHeight(scrollFrame));
        
		[UIView commitAnimations];
        
        CGRect frameToScroll = editedView_.frame;
        frameToScroll.size.height += 20.0f;
        [transparentScroll_ scrollRectToVisible:frameToScroll animated:NO];
	
        [self.view bringSubviewToFront:popButtonsView_];
        
        isPopBarShowing_ = NO;
        
    }
    
}

/*
 * Moves the pop buttons view to show from an initial height to a final height position. If pop buttons view is already
 * in view, only transparent view position is calculated
 */
- (void) movePopToShowFromHeight: (CGFloat) anInitHeight toHeight: (CGFloat) aFinalHeight
			  withAnimationCurve: (UIViewAnimationCurve) anAnimationCurve andDuration: (double) anAnimationDuration {
    
	CGSize popSize = popButtonsView_.frame.size;
	CGRect viewFrame = self.view.frame;
	
	UIWindow* window = [UIApplication sharedApplication].keyWindow;
	CGRect windowFrame = window.frame;
	CGFloat finalWindowYCoord = windowFrame.size.height - aFinalHeight;
	CGPoint finalPopTopLeftPoint = [window convertPoint: CGPointMake(0.0f, finalWindowYCoord - popSize.height) toView: self.view]; 
	
	CGRect scrollFrame = transparentScroll_.frame;
	scrollFrame.size.height = finalPopTopLeftPoint.y - CGRectGetMinY(scrollFrame);
	
	BOOL animatePopButtonsView = NO;
	
	if (popButtonsView_.superview == nil) {
        
		[window addSubview: popButtonsView_];
		popButtonsView_.alpha = 1.0f;
		popButtonsView_.frame = CGRectMake(0.0f, CGRectGetHeight(windowFrame) - popSize.height, viewFrame.size.width, popSize.height);
		animatePopButtonsView = YES;
        
	}
	
	[popButtonsView_ enableEventForwarding:YES];
	
	[UIView beginAnimations: POP_BUTTONS_VIEW_SHOW_ANIMATION context: nil];
	[UIView setAnimationCurve: anAnimationCurve];
	[UIView setAnimationDuration: anAnimationDuration];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDidStopSelector: @selector(animationDidStop: finished: context:)];
	
	if (animatePopButtonsView == YES) {
		popButtonsView_.frame = CGRectMake(0.0f, CGRectGetHeight(windowFrame) - popSize.height - aFinalHeight , viewFrame.size.width, popSize.height);
	}
	
	transparentScroll_.center = CGPointMake((CGRectGetMinX(scrollFrame) + CGRectGetMaxX(scrollFrame)) / 2.0f,
											(CGRectGetMinY(scrollFrame) + CGRectGetMaxY(scrollFrame)) / 2.0f);
	transparentScroll_.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(scrollFrame), CGRectGetHeight(scrollFrame));
	
	[UIView commitAnimations];
	
	[self makeViewVisible: editedView_];
    [self.view bringSubviewToFront:popButtonsView_];
    
    isPopBarShowing_ = YES;
    
}


/*
 * Invoked by framework when animation finishes. If animation is a pop buttons view hide animation,
 * pop buttons view is removed from view
 */
- (void) animationDidStop: (NSString*) animationID finished: (NSNumber*) finished context: (void*) context {
    
    if ([animationID isEqualToString:POP_BUTTONS_VIEW_HIDE_ANIMATION]) {
        
        [popButtonsView_ removeFromSuperview];
        
    }
    
}

#pragma mark -
#pragma mark Picker showing and hiding

/*
 * Hides the picker in case it is visible, The picker is animated to the window bottom. In case it is necesary,
 * the pop up is also hiden
 */
- (void) movePickerToHideAndPopUp: (BOOL) aHidePopUp {
    
	if (editedView_.inputView.superview != nil) {
        
        CGRect pickerFrame = editedView_.inputView.frame;
		pickerFrame.origin.y = CGRectGetHeight(self.view.window.bounds);
		
		if (aHidePopUp == YES) {
            
			NSString* requiredVersion = @"3.0";
			NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
            
			if ([currentVersion compare: requiredVersion options: NSNumericSearch] != NSOrderedAscending) {
                
				[self movePopToHideAtHeight: 0.0f withAnimationCurve: UIViewAnimationCurveLinear andDuration: PICKER_ANIMATION_DURATION];
                
			} else {
                
				[self fadeOutPopToHideWithAnimationCurve: UIViewAnimationCurveLinear andDuration: PICKER_ANIMATION_DURATION];
                
			}
            
		}
		
		[UIView beginAnimations: PICKER_VIEW_HIDE_ANIMATION context: nil];
		[UIView setAnimationCurve: UIViewAnimationCurveLinear];
		[UIView setAnimationDuration: PICKER_ANIMATION_DURATION];
		[UIView setAnimationDelegate: self];
		[UIView setAnimationDidStopSelector: @selector(animationDidStop: finished: context:)];
		
		editedView_.inputView.frame = pickerFrame;
		
		[UIView commitAnimations];
		
		showingPredictiveInfo_ = NO;
        
	}
    
}

/*
 * Shows the picker in case it is hidden, The picker is animated from the window bottom to the final position
 */
- (void) movePickerToShow {
	
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    CGRect pickerFrame = editedView_.inputView.frame;
    CGFloat popUpBottomHeight = CGRectGetHeight(pickerFrame);
    pickerFrame.origin.y = CGRectGetHeight(window.bounds);
    editedView_.inputView.frame = pickerFrame;
    
    if (popButtonsState_ == pbse_Show) {
        
        NSString* requiredVersion = @"3.0";
        NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
        
        if ([currentVersion compare: requiredVersion options: NSNumericSearch] != NSOrderedAscending) {
            
            [self movePopToShowFromHeight: 0.0f toHeight: popUpBottomHeight withAnimationCurve: UIViewAnimationCurveLinear andDuration: PICKER_ANIMATION_DURATION];
            
        } else {
            
            [self fadeInPopToShowAtHeight: popUpBottomHeight withAnimationCurve: UIViewAnimationCurveLinear andDuration: PICKER_ANIMATION_DURATION];
            
        }
        
    } else {
        
        [self movePopToHeight: popUpBottomHeight withAnimationCurve: UIViewAnimationCurveLinear andDuration: PICKER_ANIMATION_DURATION];
        
    }
    
    if (editedView_.inputView.superview == nil) {
        
        [window addSubview: editedView_.inputView];
        
    }
    
    [window bringSubviewToFront: editedView_.inputView];
    
    pickerFrame.origin.y = pickerFrame.origin.y - popUpBottomHeight;
    
    [UIView beginAnimations: PICKER_VIEW_SHOW_ANIMATION context: nil];
    [UIView setAnimationCurve: UIViewAnimationCurveLinear];
    [UIView setAnimationDuration: PICKER_ANIMATION_DURATION];
    [UIView setAnimationDelegate: self];
    [UIView setAnimationDidStopSelector: @selector(animationDidStop:finished:context:)];
    
    editedView_.inputView.frame = pickerFrame;
    
    [UIView commitAnimations];
    
    [self makeViewVisible: editedView_];
    [popButtonsView_ showListText: NO];
    
}

#pragma mark -
#pragma mark Utility methods

/*
 * A view has been clicked
 */
- (void)editableViewHasBeenClicked:(UIView *)theView {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        if ([editedView_ isKindOfClass:[NXTComboButton class]]) { // Hides previous picker, if the previous editedView is a NXTCombo
            [self movePickerToHideAndPopUp:NO];
        }
    }    
    
    if (editedView_ != theView) {
        
        [theView retain];
        [editedView_ release];
        editedView_ = theView;
        
    }

    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (![editedView_ isKindOfClass:[NXTTextView class]]) {
            if (popButtonsView_.superview == nil) {
                popButtonsState_ = pbse_Show;
            } else {
                popButtonsState_ = pbse_Relocate;
            }
        }
    }
    
    [popButtonsView_ showShowAvailableTextsButton: NO];
    
    if ([editableViews_ count] == 1) {
        [popButtonsView_ enableNextResponderButton: NO];
        [popButtonsView_ enablePreviousResponderButton: NO];
    } else if ([editableViews_ indexOfObject:editedView_] == 0) {
        [popButtonsView_ enableNextResponderButton: YES];
        [popButtonsView_ enablePreviousResponderButton:NO];
    } else if ([editableViews_ indexOfObject:editedView_] == [editableViews_ count] - 1) {
        [popButtonsView_ enableNextResponderButton: NO];
        [popButtonsView_ enablePreviousResponderButton:YES];
    } else {
        [popButtonsView_ enableNextResponderButton: YES];
        [popButtonsView_ enablePreviousResponderButton:YES];
    }
    
    if (![editedView_ isFirstResponder]) {
        [editedView_ becomeFirstResponder];
    }

    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] != NSOrderedAscending) {
                
        if ((![editedView_ isKindOfClass:[UITextField class]]) || (editedView_.superview != scrollableView_)){
        
            [self makeViewVisible:editedView_];
        
        } 

    } else {

        if ([editedView_ isKindOfClass:[UITextField class]] || [editedView_ isKindOfClass:[NXTSearchBar class]]) {
            [self movePickerToHideAndPopUp:NO];
        } else {
            [self movePickerToShow];
        }
    
    }
    
    if ([self respondsToSelector:@selector(editedViewHasFocus)]) {
        [self editedViewHasFocus];
    }
}

/*
 * Makes the given view visible inside the scroller
 */
- (void) makeViewVisible: (UIView*) aView {
    
	if (aView != nil) {
        
		CGRect viewFrame = aView.frame;
		viewFrame.origin.x = 0.0f;
		viewFrame.origin.y = 0.0f;
        
        NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
        
        UIView *baseView = aView;
        UIView *nextParent = baseView.superview;
        
        while ((nextParent != nil) && (nextParent != transparentScroll_)) {
            
            baseView = nextParent;
            nextParent = baseView.superview;
            
        }
        
		if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] != NSOrderedAscending) {
            
            CGRect auxFrame = [aView convertRect:viewFrame
                                          toView:baseView];
            CGPoint currentOffset = transparentScroll_.contentOffset;
            
            // If the scroll is down to up (and is not a UITextField) we add a top margin. If not we add a bottom margin
            if (currentOffset.y > auxFrame.origin.y) {
                
                viewFrame.origin.y = ([aView isKindOfClass:[UITextField class]] ? 0.0f : -MIN_VERTICAL_DISTANCE_TO_EDITING_VIEW);
                
            } else {
                
                viewFrame.size.height += MIN_VERTICAL_DISTANCE_TO_EDITING_VIEW;
                
            }

            viewFrame = [aView convertRect:viewFrame
                                    toView:baseView];
            [transparentScroll_ scrollRectToVisible:viewFrame
                                           animated:YES];
            
        } else {
            
            viewFrame = [aView convertRect:viewFrame
                                    toView:baseView];
            viewFrame.size.height += MIN_VERTICAL_DISTANCE_TO_EDITING_VIEW;
            [transparentScroll_ scrollRectToVisible:viewFrame
                                           animated:([aView isKindOfClass:[UITextField class]] ? NO : YES)];
            
        }
        
	}
    
}

/*
 * An editable view has been clicked and has the focus
 */
- (void)editedViewHasFocus {
    
}

/*
 * Calculates the scroller frame when the editing view is displayed
 */
- (CGRect)scrollerFrameForEditingViewFrame:(CGRect)editingViewFrame {
    
    CGRect result = scrollNominalFrame_;
    
    CGRect editingViewLocalFrame = [self.view convertRect:editingViewFrame
                                                 fromView:nil];
    
    result.size.height = CGRectGetMinY(editingViewLocalFrame) - CGRectGetMinY(result);
    
    return result;
    
}

#pragma mark -
#pragma mark Validation of formulary

/*
 * Validates the formulary
 */
- (BOOL)validateForm {
    BOOL result = YES;
    
    [self okButtonClickedInPopButtonsView:popButtonsView_];
    
    for (UIResponder *responder in editableViews_) {
        
        if ([responder isKindOfClass:[NXTComboButton class]]) {
            
            NXTComboButton *combo = (NXTComboButton *)responder;
            
            if (!combo.valid && combo.fillFormMessage != nil) {
                result = NO;
                [Tools showAlertWithMessage:combo.fillFormMessage];
                break;
            }
            
        } else if ([responder isKindOfClass:[NXTCurrencyTextField class]]) {
            
            NXTCurrencyTextField *currencyField = (NXTCurrencyTextField *)responder;
            
            if (!currencyField.valid && currencyField.fillFormMessage != nil) {
                result = NO;
                [Tools showAlertWithMessage:currencyField.fillFormMessage];
                break;
                
            }
            
        } else if ([responder isKindOfClass:[NXTTextField class]] && ![responder isKindOfClass:[NXTCurrencyTextField class]]) {
            
            NXTTextField *textField = (NXTTextField *)responder;
            
            if (!textField.valid && textField.fillFormMessage != nil) {
                result = NO;
                [Tools showAlertWithMessage:textField.fillFormMessage];
                break;
                
            }
            
        } else if ([responder isKindOfClass:[NXTTextView class]]) {
            
            NXTTextView *textView = (NXTTextView *)responder;
            
            if (!textView.valid && textView.fillFormMessage != nil) {
                result = NO;
                [Tools showAlertWithMessage:textView.fillFormMessage];
                break;
                
            }
            
        } 
        
    }
    
    return result;
}
- (void)SetContentOffSetScrollView:(CGPoint)contentOffset{
    [transparentScroll_ setContentOffset:contentOffset];
    
    
}

@end
