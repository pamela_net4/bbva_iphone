/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "NXTViewController.h"


/**
 * Defines the accepted button tapped notification
 */
#define BROWSER_VIEW_CONTROLLER_ACCEPT_BUTTON_TAPPED                                            @"net.movilok.nxt.browserViewControllerAcceptButtonTapped"


/**
 * Simple view with a web view. It has two properties to set the URL of the web view 
 * and a title to put on the navigation bar
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BrowserViewController : NXTViewController <UIWebViewDelegate, MFMailComposeViewControllerDelegate> {
@protected
    /**
     * Web view
     */
    UIWebView *webView_;
    
    /**
     * URL to load
     */
    NSString *url_;
	
@private
    /**
     * Navigatin bar title
     */
    NSString *title_;
    
    /**
     * Footer image view
     */
    UIImageView *footerImage_;
    
    /**
     * Flag to determine if the footer image must be shown
     */
    BOOL showFooterImage_;
    
    /**
     * Flag to determine if the action button must be shown
     */
    BOOL showActionButton_;
    
    /**
     * Show accept button flag. YES to display an accept button on the navigation bar right side, NO otherwise
     */
    BOOL showAcceptButton_;
    
    /**
     * Reload view
     */
    BOOL reloadView_;
    
    /**
     * Mail subject string
     */
    NSString *mailSubject_;
    
    /**
     * Mail body initial text
     */
    NSString *mailBodyInitialText_;
    
    /**
     * Flag to check if view is loaded
     */
    BOOL viewDidEnter_;
    
    /**
     * Action button
     */
    UIBarButtonItem *actionBarButton_;
    
    /**
     * Accept button
     */
    UIBarButtonItem *acceptBarButton_;
}

/**
 * Provides readwrite access to the webView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIWebView *webView;

/**
 * Provides readwrite access to the footerImage. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *footerImage;

/**
 * Provides readwrite access to the showFooterImage
 */
@property (nonatomic, readwrite, assign) BOOL showFooterImage;

/**
 * Provides readwrite access to the showActionButton
 */
@property (nonatomic, readwrite, assign) BOOL showActionButton;

/**
 * Provides readwrite access to the show accept button flag
 */
@property (nonatomic, readwrite, assign) BOOL showAcceptButton;

/**
 * Provides readwrite access to the title
 */
@property (nonatomic, readwrite, copy) NSString *title;

/**
 * Provides readwrite access to the mailSubject
 */
@property (nonatomic, readwrite, copy) NSString *mailSubject;

/**
 * Provides readwrite access to the mail body initial text
 */
@property (nonatomic, readwrite, copy) NSString *mailBodyInitialText;

/**
 * Provides readwrite access to the url
 */
@property (nonatomic, readwrite, copy) NSString *url;

/**
 * Creates and returns an autoreleased BrowserViewController constructed from a NIB file
 *
 * @return The autoreleased BrowserViewController constructed from a NIB file
 */
+ (BrowserViewController *)browserViewController;

@end
