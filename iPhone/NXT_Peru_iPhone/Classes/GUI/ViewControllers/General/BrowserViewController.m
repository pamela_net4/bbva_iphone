/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BrowserViewController.h"
#import "NXTNavigationItem.h"
#import "Tools.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "StringKeys.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"BrowserViewController"


#pragma mark -

/**
 * BrowserViewController private category
 */
@interface BrowserViewController(private)

/**
 * The action button of the navigation bar has been pressed
 */
- (void)actionButtonPressed;

/**
 * The accept button in the navigation bar has been tapped
 */
- (void)acceptButtonTapped;

/**
 * Releases all graphic elements contained on this view controller
 *
 * @private
 */
- (void)releaseGraphicElements;

@end


#pragma mark -

@implementation BrowserViewController

#pragma mark -
#pragma mark Properties

@synthesize webView = webView_;
@synthesize title = title_;
@synthesize url = url_;
@synthesize footerImage = footerImage_;
@synthesize showFooterImage = showFooterImage_;
@synthesize showActionButton = showActionButton_;
@synthesize showAcceptButton = showAcceptButton_;
@synthesize mailSubject = mailSubject_;
@synthesize mailBodyInitialText = mailBodyInitialText_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [webView_ release];
    webView_ = nil;
    
    [title_ release];
    title_ = nil;
    
    [url_ release];
    url_ = nil;
    
    [footerImage_ release];
    footerImage_ = nil;
    
    [mailSubject_ release];
    mailSubject_ = nil;
    
    [mailBodyInitialText_ release];
    mailBodyInitialText_ = nil;
    
    [actionBarButton_ release];
    actionBarButton_ = nil;
    
    [acceptBarButton_ release];
    acceptBarButton_ = nil;
    
    [super dealloc];

}

/*
 * Releases all graphic elements contained on this view controller
 */
- (void)releaseGraphicElements {
    
    [webView_ release];
    webView_ = nil;
    
    [footerImage_ release];
    footerImage_ = nil;
    
    [actionBarButton_ release];
    actionBarButton_ = nil;
    
    [acceptBarButton_ release];
    acceptBarButton_ = nil;

}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseGraphicElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark View managment

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    
    webView_.scalesPageToFit = YES;
    reloadView_ = YES;
    
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
    
    footerImage_.image = [[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
    viewDidEnter_ = NO;
    
    actionBarButton_ = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionButtonPressed)];
    
    acceptBarButton_ = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(OK_TEXT_KEY, nil) style:UIBarButtonItemStyleBordered target:self action:@selector(acceptButtonTapped)];
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
   
    [super viewDidUnload];
    
    [self releaseGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated: If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (!viewDidEnter_) {
        
        viewDidEnter_ = YES;
        
        [self.appDelegate showActivityIndicator:poai_Both];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url_]];
        [webView_ loadRequest:request];
    }
}

/**
 * Notifies the view controller that its view was added to a window.
 *
 * @param animated: If YES, the view was added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    UINavigationItem *navigationItem = self.navigationItem;
    navigationItem.rightBarButtonItem = nil;
    
    if (showAcceptButton_) {
        
        acceptBarButton_.enabled = YES;
        
        navigationItem.rightBarButtonItem = acceptBarButton_;        
    }
    
    if (showActionButton_) {
        
        if (showAcceptButton_) {
            
            navigationItem.leftBarButtonItem = actionBarButton_;
            
        } else {
            
            navigationItem.rightBarButtonItem = actionBarButton_;            
        }        
    }
    
    // Check if parent is MoreTabViewController to hide tabBar
    NSArray *viewControllersArray = [[self navigationController] viewControllers];
    
    if ([viewControllersArray count] >= 1) {
        
        UIViewController *viewController = [viewControllersArray objectAtIndex:0];
        
        if (viewController != self) {
            
            if ([[self appDelegate] setTabBarVisibility:NO animated:YES]) {
                
                if ([footerImage_ isHidden]) {
                    
                    CGRect frame = [webView_ frame];
                    frame.size.height = frame.size.height - footerImage_.frame.size.height;
                    [webView_ setFrame:frame];
                    
                }
                
                [footerImage_ setHidden:NO];
                
            }
            
        } else {
            
            if ([[self appDelegate] setTabBarVisibility:YES animated:YES]) {
                
                if (![footerImage_ isHidden]) {
                    
                    CGRect frame = [webView_ frame];
                    frame.size.height = frame.size.height + footerImage_.frame.size.height;
                    [webView_ setFrame:frame];
                    
                }
                
                [footerImage_ setHidden:YES];
                
            }
            
        }
        
    }

}

/**
 * Notifies the view controller that its view has been dismissed
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (reloadView_) {
        [webView_ loadHTMLString:@"" baseURL:nil];
    }
    
    viewDidEnter_ = NO;
}

/*
 * Creates and returns an autoreleased NavigatorViewController constructed from a NIB file
 */
+ (BrowserViewController *)browserViewController {
    return [[[BrowserViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
}

#pragma mark -
#pragma mark UIWebView methods

/**
 * Sent if a web view failed to load content
 */
- (void) webView: (UIWebView*) webView didFailLoadWithError: (NSError*) error {
    [self.appDelegate hideActivityIndicator];
    [Tools showErrorWithMessage:[error localizedDescription]];
    
    acceptBarButton_.enabled = NO;
}

/**
 * Sent after a web view finishes loading content
 */
- (void) webViewDidFinishLoad: (UIWebView*) webView {
    [self.appDelegate hideActivityIndicator];
    
    acceptBarButton_.enabled = YES;
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate selector

/**
 * Tells the delegate that the user wants to dismiss the mail composition view.
 *
 * @param controller The view controller object managing the mail composition view.
 * @param result The result of the user’s action.
 * @param error If an error occurred, this parameter contains an error object with information about the type of failure.
 */
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self.appDelegate dismissModalViewControllerAnimated:YES];
    
    if (result == MFMailComposeResultSent) {
        [Tools showAlertWithMessage:NSLocalizedString(DOCUMENT_SENT_SUCCESSFULLY_KEY, nil) title:nil];
    }
}

#pragma mark -
#pragma mark User interaction

/*
 * The action button of the navigation bar has been pressed
 */
- (void)actionButtonPressed {
    if ([MFMailComposeViewController canSendMail]) {
        
        reloadView_ = NO;
        
        MFMailComposeViewController *mailController = [[[MFMailComposeViewController alloc] init] autorelease];
        mailController.mailComposeDelegate = self;
        
        [mailController setSubject:mailSubject_];
        
        NSString *emailBody = [NSString stringWithFormat:@"%@\n\n%@\n\n%@ %@\n\n%@", mailBodyInitialText_, url_,
                               NSLocalizedString(EMAIL_DOCUMENT_VERSION_KEY, nil), [Tools getStringWithHourWithCurrentLocaleFromDate:[NSDate date]],
                               NSLocalizedString(EMAIL_BODY_END_KEY, nil)];
//        NSString *emailBody = [NSString stringWithFormat:@"Versión del documento %@\n\n%@", [Tools getStringWithHourFromDate:[NSDate date]] , self.url];
        [mailController setMessageBody:emailBody isHTML:NO]; 
        
        [[mailController navigationBar] setTintColor:[[[self navigationController] navigationBar] tintColor]];
        
        [self.appDelegate presentModalViewController:mailController animated:YES];
        
    } else {
		
        [Tools showAlertWithMessage:NSLocalizedString(ERROR_SENDING_MAIL_TEXT_KEY, nil)];
		
    }
}


/*
 * The accept button in the navigation bar has been tapped
 */
- (void)acceptButtonTapped {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:BROWSER_VIEW_CONTROLLER_ACCEPT_BUTTON_TAPPED object:self];
    
}

#pragma mark -
#pragma mark Properties methods

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = title_;
    
    return result;
    
}

@end
