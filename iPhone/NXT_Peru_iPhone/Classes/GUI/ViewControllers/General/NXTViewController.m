/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTViewController.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "StringKeys.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"
#import "Tooltip.h"

#pragma mark -

@implementation NXTViewController

#pragma mark -
#pragma mark Properties

@dynamic appDelegate;
@dynamic customNavigationItem;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [customNavigationItem_ release];
    customNavigationItem_ = nil;
    
    [tooltips_ removeAllObjects];
    [tooltips_ release];
    tooltips_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark View management

/**
 * Called when the controller’s view is loaded. The autoresizing mask and autoresizes subviews flag are set. Navigation bat tint is customized
 */
- (void)viewDidLoad {

    [super viewDidLoad];
    
    tooltips_ = [[NSMutableArray alloc] init];
    
    UIView *view = self.view;
    view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |
                            UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    view.autoresizesSubviews = YES;
    
}

/**
 * Notifies the view controller that its view is about to be become visible. Sets the navigation bar color
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    [NXT_Peru_iPhoneStyler styleNavigationBar:self.navigationController.navigationBar];
    [NXT_Peru_iPhoneStyler styleToolbar:self.navigationController.toolbar];

    
    if([self.appDelegate.radioStreamManager isPlayingRadio]){
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        [self becomeFirstResponder];
    }

 

}
-(BOOL)canBecomeFirstResponder{
    return YES;
}
-(void)remoteControlReceivedWithEvent:(UIEvent *)event
{
    
    if(event.type==UIEventTypeRemoteControl){
        switch(event.subtype){
            case UIEventSubtypeRemoteControlTogglePlayPause:
                if (floor(NSFoundationVersionNumber) < NSFoundationVersionNumber_iOS_7_1){
                    [self.appDelegate.radioStreamManager toggleStreamAction];
                }
                break;
            default:
                break;
        }
        
    }
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if([self.appDelegate.radioStreamManager isPlayingRadio]){
        [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
        [self becomeFirstResponder];
    }
}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the NXT application delegate
 */
- (NXT_Peru_iPhone_AppDelegate *)appDelegate {
    
    return (NXT_Peru_iPhone_AppDelegate *)[UIApplication sharedApplication].delegate;
}

/**
 * Returns the custom navigation item, and sets its title to "Atrás"
 */
- (NXTNavigationItem *)customNavigationItem {
    
    if (customNavigationItem_ == nil) {
        
        customNavigationItem_ = [[NXTNavigationItem alloc] initWithTitle:NSLocalizedString(BACK_TEXT_KEY, nil)];
    }
    
    return customNavigationItem_;
    
}

-(void)showInfoTooltipFromView:(UIView*)fromView message:(NSString*) message{
    Tooltip * tooltipnew = [[Tooltip alloc] initWithMessage:message
                                                     source:fromView parentWindow:self.view.window];
    tooltipnew.hideShadow = YES;
    tooltipnew.arrowColor = [UIColor colorWithRed:0.0/255.0 green:110.0/255.0 blue:193.0/255.0 alpha:1];
    tooltipnew.borderColor =[UIColor colorWithRed:9.0/255.0 green:79.0/255.0 blue:164.0/255.0 alpha:1];
    tooltipnew.textColor = [UIColor whiteColor];
    [tooltipnew show];
    
   // [tooltips_ addObject:tooltipnew];
}

-(void)showErrorTooltipFromView:(UIView*)fromView message:(NSString*) message{
    Tooltip * tooltipnew = [[Tooltip alloc] initWithMessage:message
                                                     source:fromView parentWindow:self.view.window];
    tooltipnew.delegate = self;
    tooltipnew.hideShadow = YES;
    tooltipnew.arrowColor = [UIColor colorWithRed:245.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1];
    tooltipnew.borderColor =[UIColor colorWithRed:241.0/255.0 green:210.0/255.0 blue:223.0/255.0 alpha:1];
    tooltipnew.textColor = [UIColor colorWithRed:184.0/255.0 green:0.0/255.0 blue:97.0/255.0 alpha:1];
    tooltipnew.label.textAlignment = NSTextAlignmentCenter;
    [tooltipnew show];
    
    [tooltips_ addObject:tooltipnew];
}

-(void)showErrorTooltipFromView:(UIView*)fromView message:(NSString*) message delegate:(id)delegate{
    Tooltip * tooltipnew = [[Tooltip alloc] initWithMessage:message
                                                     source:fromView parentWindow:self.view.window];
    tooltipnew.delegate = delegate;
    tooltipnew.hideShadow = YES;
    tooltipnew.arrowColor = [UIColor colorWithRed:245.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1];
    tooltipnew.borderColor =[UIColor colorWithRed:241.0/255.0 green:210.0/255.0 blue:223.0/255.0 alpha:1];
    tooltipnew.textColor = [UIColor colorWithRed:184.0/255.0 green:0.0/255.0 blue:97.0/255.0 alpha:1];
    tooltipnew.label.textAlignment = NSTextAlignmentCenter;
    [tooltipnew show];
    
    [tooltips_ addObject:tooltipnew];
}

#pragma TooltipDelegate


-(void)onDismiss:(UIView*)view{
    for (int i=0; i<[tooltips_ count]; i++) {
        Tooltip * tooltip = [tooltips_ objectAtIndex:i];
        tooltip.delegate = nil;
        [tooltip dismiss];
    }
    [view becomeFirstResponder];
}

@end
