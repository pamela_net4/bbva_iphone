//
//  SalaryAdvanceAffiliationViewController.m
//  NXT_Peru_iPhone
//
//  Created by Flavio Franco Tunqui on 3/17/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "SalaryAdvanceAffiliationViewController.h"
#import "Constants.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "NXTEditableViewController+protected.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "SealCell.h"
#import "TransferDetailCell.h"
#import "Tools.h"
#import "GlobalAdditionalInformation.h"
#import "SimpleHeaderView.h"
#import "UIColor+BBVA_Colors.h"
#import "Session.H"
#import "NXTNavigationItem.h"
#import "TitleAndAttributes.h"
#import "NXTTextField.h"
#import "Updater.h"
#import "FastLoanConfirm.h"
#import "FastLoanSummary.h"
#import "FastLoanSchedule.h"
#import "ScheduleView.h"
#import "FastLoanStepThreeViewController.h"
#import "EmailInfo.h"
#import "EmailInfoList.h"
#import "SalaryAdvanceReceive.h"
#import "SalaryAdvanceReceiveViewController.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"SalaryAdvanceAffiliationViewController"
/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_NEAR_ELEMENT                           5.0f

/**
 * Defines the vertical gap between two far away elements
 */
#define VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT                       10.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_BIG_SIZE                                          17.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              15.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                        13.0f

@interface SalaryAdvanceAffiliationViewController ()

@end

@implementation SalaryAdvanceAffiliationViewController

@synthesize containerView = containerView_;

@synthesize viewHeader = viewHeader_;
@synthesize lblHeadertext = lblHeadertext_;

@synthesize viewAsk1 = viewAsk1_;
@synthesize imgAsk1 = imgAsk1_;
@synthesize lblAsk1 = lblAsk1_;
@synthesize lblAnswer1 = lblAnswer1_;

@synthesize viewAsk2 = viewAsk2_;
@synthesize imgAsk2 = imgAsk2_;
@synthesize lblAsk2 = lblAsk2_;
@synthesize lblAnswer2 = lblAnswer2_;

@synthesize viewInformation = viewInformation_;
@synthesize imgInformation = imgInformation_;
@synthesize lblInformation = lblInformation_;

@synthesize viewAsk3 = viewAsk3_;
@synthesize imgAsk3 = imgAsk3_;
@synthesize lblAsk3 = lblAsk3_;
@synthesize lblAnswer3 = lblAnswer3_;

@synthesize line1 = line1_;

@synthesize viewEmail = viewEmail_;
@synthesize lblLabelEmail = lblLabelEmail_;
@synthesize btnTTEmail = btnTTEmail_;
@synthesize btnEditEmail = btnEditEmail_;
@synthesize emailCombo = emailCombo_;
@synthesize emailTextField = emailTextField_;

@synthesize line2 = line2_;

@synthesize viewContract = viewContract_;
@synthesize lblLabelContract = lblLabelContract_;
@synthesize btnTTContract = btnTTContract_;
@synthesize btnContract = btnContract_;

@synthesize line3 = line3_;

@synthesize viewContractAcceptContract = viewContractAcceptContract_;
@synthesize lblLabelAcceptContract = lblLabelAcceptContract_;
@synthesize imgAlertAcceptContract = imgAlertAcceptContract_;
@synthesize switchAcceptContract = switchAcceptContract_;

@synthesize viewOperation = viewOperation_;
@synthesize lblLabelOperation = lblLabelOperation_;
@synthesize btnTTOperation = btnTTOperation_;
@synthesize viewEdittext = viewEdittext_;
@synthesize opTextField = opTextField_;

@synthesize viewSeal = viewSeal_;
@synthesize viewLabel = viewLabel_;
@synthesize lblLabelSeal = lblLabelSeal_;
@synthesize imgSeal = imgSeal_;

@synthesize buttonConfirm = buttonConfirm_;
@synthesize brandingImageView = brandingImageView_;

@synthesize salaryAdvanceAffiliation = salaryAdvanceAffiliation_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [salaryAdvanceAffiliation_ release];
    salaryAdvanceAffiliation_ = nil;
    
    [termsAndConsiderationsViewController_ release];
    termsAndConsiderationsViewController_ = nil;
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationSalaryAdvanceAffiliationConfirmResponse
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationSalaryAdvanceAffiliationResponse
                                                  object:nil];
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransfersStepTwoViewControllerGraphicElements];
        [self setView:nil];
        
    }
}

/**
 * Releases graphic elements
 */
- (void)releaseTransfersStepTwoViewControllerGraphicElements {
    
    [containerView_ release];
    containerView_ = nil;
    
    [viewHeader_ release];
    viewHeader_ = nil;
    [lblHeadertext_ release];
    lblHeadertext_ = nil;
    
    [viewAsk1_ release];
    viewAsk1_ = nil;
    [imgAsk1_ release];
    imgAsk1_ = nil;
    [lblAsk1_ release];
    lblAsk1_ = nil;
    [lblAnswer1_ release];
    lblAnswer1_ = nil;
    
    [viewAsk2_ release];
    viewAsk2_ = nil;
    [imgAsk2_ release];
    imgAsk2_ = nil;
    [lblAsk2_ release];
    lblAsk2_ = nil;
    [lblAnswer2_ release];
    lblAnswer2_ = nil;
    
    [viewInformation_ release];
    viewInformation_ = nil;
    [imgInformation_ release];
    imgInformation_ = nil;
    [lblInformation_ release];
    lblInformation_ = nil;
    
    [viewAsk3_ release];
    viewAsk3_ = nil;
    [imgAsk3_ release];
    imgAsk3_ = nil;
    [lblAsk3_ release];
    lblAsk3_ = nil;
    [lblAnswer3_ release];
    lblAnswer3_ = nil;
    
    [line1_ release];
    line1_ = nil;
    
    [viewEmail_ release];
    viewEmail_ = nil;
    [lblLabelEmail_ release];
    lblLabelEmail_ = nil;
    [btnTTEmail_ release];
    btnTTEmail_ = nil;
    [btnEditEmail_ release];
    btnEditEmail_ = nil;
    [emailCombo_ release];
    emailCombo_ = nil;
    [emailTextField_ release];
    emailTextField_ = nil;
    
    [line2_ release];
    line2_ = nil;
    
    [viewContract_ release];
    viewContract_ = nil;
    [lblLabelContract_ release];
    lblLabelContract_ = nil;
    [btnTTContract_ release];
    btnTTContract_ = nil;
    [btnContract_ release];
    btnContract_ = nil;
    
    [line3_ release];
    line3_ = nil;
    
    [viewContractAcceptContract_ release];
    viewContractAcceptContract_ = nil;
    [lblLabelAcceptContract_ release];
    lblLabelAcceptContract_ = nil;
    [imgAlertAcceptContract_ release];
    imgAlertAcceptContract_ = nil;
    [switchAcceptContract_ release];
    switchAcceptContract_ = nil;
    
    [viewOperation_ release];
    viewOperation_ = nil;
    [lblLabelOperation_ release];
    lblLabelOperation_ = nil;
    [btnTTOperation_ release];
    btnTTOperation_ = nil;
    [viewEdittext_ release];
    viewEdittext_ = nil;
    [opTextField_ release];
    opTextField_ = nil;
    
    [viewSeal_ release];
    viewSeal_ = nil;
    [viewLabel_ release];
    viewLabel_ = nil;
    [lblLabelSeal_ release];
    lblLabelSeal_ = nil;
    [imgSeal_ release];
    imgSeal_ = nil;
    
    [buttonConfirm_ release];
    buttonConfirm_ = nil;
    
    [brandingImageView_ release];
    brandingImageView_ = nil;
    
    [editableViews_ removeAllObjects];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIView *view = self.view;
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    view.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
    containerView_.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:lblHeadertext_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVABlueSpectrumToneOneColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:lblAsk1_ withBoldFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVABlackColor]];
    [NXT_Peru_iPhoneStyler styleLabel:lblAsk2_ withBoldFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVABlackColor]];
    [NXT_Peru_iPhoneStyler styleLabel:lblAsk3_ withBoldFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVABlackColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:lblAnswer1_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:lblAnswer2_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:lblAnswer3_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:lblInformation_ withFontSize:11.0 color:[UIColor BBVABlueColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:lblLabelEmail_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVABlueColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:lblLabelOperation_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueSpectrumToneOneColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:lblLabelSeal_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueSpectrumToneOneColor]];
    
    
    [NXT_Peru_iPhoneStyler styleLabel:lblLabelContract_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:lblLabelAcceptContract_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleTextField:opTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
  
    [btnTTEmail_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    [btnTTContract_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    [btnTTOperation_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    
    switchAcceptContract_.on = NO;
    [NXT_Peru_iPhoneStyler styleSwitchForiOS7:switchAcceptContract_];

    [btnContract_ addTarget:self action:@selector(actionDocument:) forControlEvents:UIControlEventTouchUpInside];

    opTextField_.keyboardType = UIKeyboardTypeNumberPad;
    opTextField_.secureTextEntry = YES;
    opTextField_.delegate = self;

    [NXT_Peru_iPhoneStyler styleLabel:lblLabelEmail_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [lblLabelEmail_ setText:@"Correo Electrónico"];
    
    [emailCombo_ setTitle:@"Selecciona Correo Electrónico"];
    [emailCombo_ setDelegate:self];
    [emailCombo_ setIsNewDesign:YES];
    [emailCombo_ setInputAccessoryView:popButtonsView_];
    
    [NXT_Peru_iPhoneStyler styleTextField:emailTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    emailTextField_.keyboardType = UIKeyboardTypeEmailAddress;
    emailTextField_.enabled = YES;
    [emailTextField_ setPlaceholder:@"Escribe tu correo"];
    [emailTextField_ setDelegate:self];
    [emailTextField_ setInputAccessoryView:popButtonsView_];
    
    [btnEditEmail_ addTarget:self action:@selector(editTextField:) forControlEvents:UIControlEventTouchUpInside];
    [btnEditEmail_ addTarget:self action:@selector(editTextField:) forControlEvents:UIControlEventTouchUpInside];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:buttonConfirm_];
    [buttonConfirm_ setTitle:@"Afiliar" forState:UIControlStateNormal];
    [buttonConfirm_ addTarget:self action:@selector(acceptButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect containerFrame = containerView_.frame;
    containerFrame.origin.x = 0.0f;
    containerFrame.origin.y = 0.0f;
    containerView_.frame = containerFrame;
    
    [self setScrollableView:containerView_];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.origin.y = 0;
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
    [self layoutViews];
    
    [view bringSubviewToFront:brandingImageView_];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
    
    transparentScroll_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
}

/*
 * Creates and returns an autoreleased SalaryAdvanceAffiliationViewController constructed from a NIB file
 */
+ (SalaryAdvanceAffiliationViewController *)salaryAdvanceAffiliationViewController{
    
    SalaryAdvanceAffiliationViewController *result = [[[SalaryAdvanceAffiliationViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
}


/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(afiliationConfirmResponseReceived:)
                                                 name:kNotificationSalaryAdvanceAffiliationConfirmResponse
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(afiliationResponseReceived:)
                                                 name:kNotificationSalaryAdvanceAffiliationResponse
                                               object:nil];
    
    if (shouldClearInterfaceData_) {
        
        [transparentScroll_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f) animated:NO];
        
        shouldClearInterfaceData_ = NO;
        
    }
    
    if (editableViews_ == nil) {
        editableViews_ = [[NSMutableArray alloc] init];
    } else {
        [editableViews_ removeAllObjects];
    }
    [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:
                                         emailCombo_,
                                         nil]];
    
    [self displayStoredInformation];
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the
 * transfer last step confirmation notification
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationSalaryAdvanceAffiliationConfirmResponse
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationSalaryAdvanceAffiliationResponse
                                                  object:nil];
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingImageView] frame]);
    [self setScrollNominalFrame:scrollFrame];
    
}

/*
 * Lays out the views
 */
- (void)layoutViews {

    if ([self isViewLoaded]) {
        if(salaryAdvanceAffiliation_.seal && [salaryAdvanceAffiliation_.seal length]>0){
            viewSeal_.hidden = NO;

        } else {
            viewSeal_.hidden = YES;
            
            buttonConfirm_.frame = CGRectMake(CGRectGetMinX(buttonConfirm_.frame), CGRectGetMaxY(viewOperation_.frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT, CGRectGetWidth(buttonConfirm_.frame), CGRectGetHeight(buttonConfirm_.frame));
        }
        
        self.containerView.frame = CGRectMake(CGRectGetMinX(self.containerView.frame), CGRectGetMinY(self.containerView.frame), CGRectGetWidth(self.containerView.frame), CGRectGetMaxY(buttonConfirm_.frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT);

        [self recalculateScroll];
    }

}

-(void)displayStoredInformation{

    lblAnswer1_.text = salaryAdvanceAffiliation_.messageAmount;
    lblAnswer2_.text = salaryAdvanceAffiliation_.messageCommission;
    lblAnswer3_.text = salaryAdvanceAffiliation_.messageDayPay;
    
    //emails
    NSMutableArray *array = [NSMutableArray array];
    array = [NSMutableArray array];
    NSArray *emailList = [salaryAdvanceAffiliation_.emailInfoList emailinfoList];
    
    for(EmailInfo *email in emailList) {
        [array addObject:email.email];
    }
    emailCombo_.stringsList = [NSArray arrayWithArray:array];
    
    if ([array count] > 0) {
        [emailCombo_ setSelectedIndex:0];
        [emailCombo_ setHidden:NO];
        [emailTextField_ setHidden:YES];
        
        
        if (editableViews_ == nil) {
            editableViews_ = [[NSMutableArray alloc] init];
        } else {
            [editableViews_ removeAllObjects];
        }
        [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:
                                             emailCombo_,
                                             nil]];
    }
    else{
        [emailCombo_ setHidden:YES];
        [emailTextField_ setHidden:NO];
        
        
        if (editableViews_ == nil) {
            editableViews_ = [[NSMutableArray alloc] init];
        } else {
            [editableViews_ removeAllObjects];
        }
        [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:
                                             emailTextField_,
                                             nil]];
    }
    
    if([array count] == 1){
        [emailCombo_ setEnabled:NO];
    }
    else{
        [emailCombo_ setEnabled:YES];
    }
    
    [array removeAllObjects];

    CGFloat posXTTEmail = [lblLabelEmail_.text sizeWithFont:lblLabelEmail_.font].width + CGRectGetMinX(lblLabelEmail_.frame);
    CGRect frameTTEmail = btnTTEmail_.frame;
    frameTTEmail.origin.x = posXTTEmail;
    btnTTEmail_.frame = frameTTEmail;
    
    CGFloat posXTTContract = [lblLabelContract_.text sizeWithFont:lblLabelContract_.font].width + CGRectGetMinX(lblLabelContract_.frame);
    CGRect frameTTContract = btnTTContract_.frame;
    frameTTContract.origin.x = posXTTContract;
    btnTTContract_.frame = frameTTContract;

    CGFloat posXTTOperation = [lblLabelOperation_.text sizeWithFont:lblLabelOperation_.font].width + CGRectGetMinX(lblLabelOperation_.frame);
    CGRect frameTTOperation = btnTTOperation_.frame;
    frameTTOperation.origin.x = posXTTOperation;
    btnTTOperation_.frame = frameTTOperation;
    
    NSString * htmlInformation = @"<html><head><style>body {font-family:-apple-system, 'Helvetica', 'Arial', sans-serif;font-size:11px;text-align: left;color:#094FA4;}</style></head><body>El Adelanto de Sueldo <b>no incluye intereses</b>. Sólo se te cobrará una comisión por el abono a tu cuenta.</body></html>";
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlInformation dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    lblInformation_.attributedText = attrStr;
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];;
    NSString *title = @"";

    if (otpUsage == otp_UsageTC) {
        title = @"Tarjeta de Coordenadas";
        opTextField_.placeholder = [NSString stringWithFormat:@"Ingrese la coordenada %@ de tu tarjeta", salaryAdvanceAffiliation_.coordinate];
    } else if (otpUsage == otp_UsageOTP) {
        title = @"Clave SMS (enviada a tu celular)";
        opTextField_.placeholder = @"Ingrese los 6 dígitos de tu clave";
    }
    lblLabelOperation_.text = title;

    if(salaryAdvanceAffiliation_.seal && [salaryAdvanceAffiliation_.seal length]>0){

        NSData *data = [Tools base64DataFromString:salaryAdvanceAffiliation_.seal];
        UIImage *image = [[[UIImage alloc] initWithData:data] autorelease];
        [imgSeal_ setImage:image];
    }

    UIView *popButtonsView = [self popButtonsView];
    [opTextField_ setInputAccessoryView:popButtonsView];

    if (editableViews_ == nil) {
        editableViews_ = [[NSMutableArray alloc] init];
    } else {
        [editableViews_ removeAllObjects];
    }

    [editableViews_ addObject:opTextField_];

    [self layoutViews];
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initial configuration is set.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
    [self makeViewVisible:textField];
    
    if(textField == opTextField_){
        [NXT_Peru_iPhoneStyler styleLabel:lblLabelOperation_
                             withFontSize:TEXT_FONT_SIZE
                                    color:[UIColor BBVABlueSpectrumToneOneColor]];
        [NXT_Peru_iPhoneStyler styleTextField:opTextField_
                                 withFontSize:TEXT_FONT_SMALL_SIZE
                                     andColor:[UIColor BBVAGreyColor]];
        [NXT_Peru_iPhoneStyler removeError:opTextField_];
    }
    
    else if(textField == emailTextField_){
        [NXT_Peru_iPhoneStyler styleLabel:lblLabelEmail_ withFontSize:TEXT_FONT_SIZE
                                    color:[UIColor BBVABlueColor]];
        [NXT_Peru_iPhoneStyler styleTextField:emailTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                     andColor:[UIColor BBVAGreyColor]];
        [NXT_Peru_iPhoneStyler removeError:emailTextField_];
    }
    
    return YES;
    
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self editableViewHasBeenClicked:textField];
    
}

/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    NSUInteger finalTotalChars = [textField.text length] + [string length] - range.length;
    
    if (textField == opTextField_) {
        OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
        if (otpUsage == otp_UsageTC) {
            
            if (resultLength <= COORDINATES_MAXIMUM_LENGHT) {
                
                result = YES;
                
            }
        }
        else if (otpUsage == otp_UsageOTP) {
            
            if(resultLength <= OTP_MAXIMUM_LENGHT) {
                
                if ([Tools isValidText:resultString forCharacterString:OTP_VALID_CHARACTERS]) {
                    
                    result = YES;
                }
            }
        }
    } else if(textField == emailTextField_){
        
        result = (finalTotalChars <= 50 && [Tools isValidText:string forCharacterString:EMAIL_TEXT_VALID_CHARACTERS]);
        return result;
    }
    
    return result;
    
}

#pragma mark -
#pragma mark UITextViewDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text view.
 *
 * @param textView The text view for which editing is about to begin.
 */
- (BOOL)textViewShouldBeginEditing:(UITextView*)textView {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
    }
    return YES;
}

/**
 * Tells the delegate that editing began for the specified text view.
 *
 * @param textView The text view for which an editing session began.
 */
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    [self editableViewHasBeenClicked:textView];
    
}



#pragma mark -
#pragma mark User Interaction

/*
 * Performs accept buttom action
 */
- (void) acceptButtonTapped {
    
    [opTextField_ endEditing:YES];
    [emailTextField_ endEditing:YES];
    
    BOOL canStartProcess = YES;
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
    
    NSString * email = emailCombo_.isHidden ? emailTextField_.text : @"";
    NSArray *emailList = [salaryAdvanceAffiliation_.emailInfoList emailinfoList];
    if(!emailCombo_.isHidden && [emailList count]>0){
        EmailInfo * emailInfo = [ emailList objectAtIndex:emailCombo_.selectedIndex];
        email = emailInfo.email;
    }
    
    if (![Tools isValidEmail:email]) {
        [self showErrorTooltipFromView:emailTextField_ message:@"Escribe un correo válido"];
        
        [NXT_Peru_iPhoneStyler styleLabel:lblLabelEmail_ withFontSize:TEXT_FONT_SIZE
                                    color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
        [NXT_Peru_iPhoneStyler styleTextFieldError:emailTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                          andColor:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
        canStartProcess = NO;
    } else if(!switchAcceptContract_.isOn){
        [NXT_Peru_iPhoneStyler styleLabel:lblLabelAcceptContract_ withFontSize:TEXT_FONT_SMALLER_SIZE
                                    color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
        
        imgAlertAcceptContract_.image = [[ImagesCache getInstance] imageNamed:RED_ICON_ALERT_NO_BORDER_IMAGE_FILE_NAME];
        [self showErrorTooltipFromView:lblLabelAcceptContract_ message:@"Confirma que leíste y aceptas el documento" delegate:self];
        canStartProcess = NO;
    } else if ((otpUsage == otp_UsageOTP) && ([[self secondFactorKey] length] == 0)) {
        [Tools showAlertWithMessage:@"Ingresa los 6 dígitos de tu Clave SMS." title:@""];
        canStartProcess = NO;
        
    } else if ((otpUsage == otp_UsageTC) && (([[self secondFactorKey] length] == 0) || ([[self secondFactorKey] length] < 3))) {
        [Tools showAlertWithMessage:@"Ingresa los 3 dígitos de la coordenada solicitada." title:@""];
        canStartProcess = NO;
        
    }
    
    if(canStartProcess){
        [self.appDelegate showActivityIndicator:poai_Both];
        [[Updater getInstance] salaryAdvanceAffiliationConfirmOperation:[self secondFactorKey] :email];
    }
}

/*
 * Returns the second factor key
 */
- (NSString *)secondFactorKey {
    NSString *secondFactor = @"";
    secondFactor = opTextField_.text;
    return secondFactor;
}

#pragma mark -
#pragma mark User interaction

- (IBAction)comboPressed:(UIView*)view {
    
    [self editableViewHasBeenClicked:view];
}

- (void)editTextField:(id)view {
    
    if(view == btnEditEmail_){
        [emailCombo_ setHidden:YES];
        [emailTextField_ setHidden:NO];
        
        if (editableViews_ == nil) {
            editableViews_ = [[NSMutableArray alloc] init];
        } else {
            [editableViews_ removeAllObjects];
        }
        [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:
                                             emailTextField_,
                                             nil]];
    }
    
    
}

- (void) showTooltip:(UIView*)view {
    if(view == btnTTEmail_){
        [self showInfoTooltipFromView:view
                              message:@"Para enviarte tu contrato de adelanto de sueldo"];
    }
    else if(view == btnTTContract_){
        [self showInfoTooltipFromView:view
                              message:@"El contrato establece las condiciones, obligaciones y beneficios del Adelanto de Sueldo"];
    }
    else if(view == btnTTOperation_){
        OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
        if (otpUsage == otp_UsageTC) {
            [self showInfoTooltipFromView:btnTTOperation_
                                  message:@"Al ingresar tu coordenada te afiliarás al Adelanto de Sueldo y luego podrás recibirlo"];
        }
        else if (otpUsage == otp_UsageOTP) {
            [self showInfoTooltipFromView:btnTTOperation_
                                  message:@"Al ingresar tu Clave SMS te afiliarás al Adelanto de Sueldo y luego podrás recibirlo"];
        }
    }
}

-(void) actionDocument:(id)sender{
        if(sender==btnContract_){
            if (termsAndConsiderationsViewController_ == nil) {
    
                termsAndConsiderationsViewController_ = [[TermsAndConsiderationsViewController TermsAndConsiderationsViewController] retain];
    
            }
            [termsAndConsiderationsViewController_ setURLText:salaryAdvanceAffiliation_.disclaimerContract
                                                    titleText:@"Adelando de Sueldo"
                                                   headerText:@"Contrato de Adelanto de Sueldo"];
            [self.navigationController pushViewController:termsAndConsiderationsViewController_ animated:YES];
        }
}

#pragma TooltipDelegate

-(void)onDismiss:(UIView*)view{
    [NXT_Peru_iPhoneStyler styleLabel:lblLabelAcceptContract_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    imgAlertAcceptContract_.image = [[ImagesCache getInstance] imageNamed:BLUE_ICON_ALERT_NO_BORDER_IMAGE_FILE_NAME];
}

#pragma mark -
#pragma mark Notifications management

- (void)afiliationConfirmResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    NSString * physicalError = @"Se presentaron inconvenientes al solicitar tu Adelanto de Sueldo. Por favor, intentálo más tarde.";
    
    if (![response isError]) {
        
        if ([response isKindOfClass:[SalaryAdvanceReceive class]]) {
            
            //Sucess
            SalaryAdvanceReceive *salaryAdvanceReceive = (SalaryAdvanceReceive *)response;
            
            SalaryAdvanceReceiveViewController * salaryAdvanceReceiveViewController = [[SalaryAdvanceReceiveViewController SalaryAdvanceReceiveViewController] retain];
            salaryAdvanceReceiveViewController.SalaryAdvanceReceive = salaryAdvanceReceive;
            [[self navigationController] pushViewController:salaryAdvanceReceiveViewController animated:YES];
        }
        else{
            opTextField_.text = @"";
            
            [Tools showAlertWithMessage:physicalError
                                  title:@"" andDelegate:self];
        }
        
    } else {
        if([response errorCode] != nil  &&
           (    [[response errorCode] isEqualToString:@WRONG_CONFIRM_CODE_ERROR]
            || [[response errorCode] isEqualToString:@REMAIN_ONE_TRY_CONFIRM_CODE_ERROR]
            || [[response errorCode] isEqualToString:@SMS_CODE_EXPIRED_ERROR]
            || [[response errorCode] isEqualToString:@WRONG_COORDINATE_CONFIRM_CODE_ERROR]
            )){
               
               opTextField_.text = @"";
               
               NSString * message = [response errorMessage];
               if (message == nil || [@"" isEqualToString:message]) {
                   message = physicalError;// physical error
               }
               
               NSString *buttonText = NSLocalizedString(OK_TEXT_KEY, nil);
               UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"" message:message
                                                                   delegate:self cancelButtonTitle:buttonText otherButtonTitles:nil] autorelease];
               alertView.tag = 666;
               [alertView show];
           }
        else{
            
            opTextField_.text = @"";
            
            NSString * message = [response errorMessage];
            if (message == nil || [@"" isEqualToString:message]) {
                message = physicalError;// physical error
            }
            
            NSString *buttonText = NSLocalizedString(OK_TEXT_KEY, nil);
            UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"" message:message
                                                                delegate:self cancelButtonTitle:buttonText otherButtonTitles:nil] autorelease];
            [alertView show];
        }
        
        
    }
}

/*
 * Invoked by framework when the response to the transfer operation is received. The information is analized
 */
- (BOOL)afiliationResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    opTextField_.text = @"";
    
    StatusEnabledResponse *response = [notification object];
    
    BOOL result = NO;
    
    if (response && !response.isError) {
        
        if ([response isKindOfClass:[SalaryAdvanceAffiliation class]]) {
            
            SalaryAdvanceAffiliation *salaryAdvanceAffiliation = (SalaryAdvanceAffiliation *)response;
            
            OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];;
            if (otpUsage == otp_UsageTC) {
                opTextField_.placeholder = [NSString stringWithFormat:@"Ingrese la coordenada %@ de tu tarjeta",
                                            salaryAdvanceAffiliation.coordinate];
            }
            result = YES;
            
        }
    }
    else if(response && [response errorCode] != nil  &&
            [[response errorCode] isEqualToString:@MAX_TRY_CONFIRM_CODE_ERROR]){
        NSString * message =  response ? [response errorMessage] : nil;
        if (message == nil || [@"" isEqualToString:message]) {
            message = NSLocalizedString(COMMUNICATION_ERROR_KEY, nil);// physical error
        }
        [Tools showAlertWithMessage:message title:@"" andDelegate:self];
    }
    else{
        NSString * message =  response ? [response errorMessage] : nil;
        if (message == nil || [@"" isEqualToString:message]) {
            message = NSLocalizedString(COMMUNICATION_ERROR_KEY, nil);// physical error
        }
        [Tools showAlertWithMessage:message title:@"" andDelegate:self];
    }
    
    return result;
}


#pragma mark -
#pragma mark UIAlertViewDelegate methods

/**
 * Sent to the delegate when the user clicks a button on an alert view.
 *
 * @param alertView: The alert view containing the button.
 * @param buttonIndex: The index of the button that was clicked.
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 666){
        
        [self.appDelegate showActivityIndicator:poai_Both];
        [[Updater getInstance] salaryAdvanceAffiliationOperation];
    }
    else{
        [[self navigationController] popToRootViewControllerAnimated:YES];
    }
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = @"Adelando de Sueldo";
    return result;
}

@end