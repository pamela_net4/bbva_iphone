//
//  SalaryAdvanceSummaryViewController.m
//  NXT_Peru_iPhone
//
//  Created by Estefany on 5/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "SalaryAdvanceSummaryViewController.h"
#import "Constants.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "NXTEditableViewController+protected.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "SealCell.h"
#import "TransferDetailCell.h"
#import "Tools.h"
#import "GlobalAdditionalInformation.h"
#import "SimpleHeaderView.h"
#import "UIColor+BBVA_Colors.h"
#import "Session.H"
#import "NXTNavigationItem.h"
#import "TitleAndAttributes.h"
#import "NXTTextField.h"
#import "Updater.h"
#import "SalaryAdvanceSummary.h"


/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"SalaryAdvanceSummaryViewController"
/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_NEAR_ELEMENT                           5.0f

/**
 * Defines the vertical gap between two far away elements
 */
#define VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT                       10.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_BIG_SIZE                                          17.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              15.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                        13.0f

@interface SalaryAdvanceSummaryViewController ()

@end

@implementation SalaryAdvanceSummaryViewController

@synthesize containerView = containerView_;
@synthesize headerImageView = headerImageView_;
@synthesize acceptButton = acceptButton_;
@synthesize grettingLabel = grettingLabel_;
@synthesize brandingImageView = brandingImageView_;
@synthesize SalaryAdvanceSummary = SalaryAdvanceSummary_;
@synthesize dateOperationLabel = dateOperationLabel_;
@synthesize dateOperationValueLabel = dateOperationValueLabel_;
@synthesize numberOperationLabel = numberOperationLabel_;
@synthesize numberOperationValueLabel = numberOperationValueLabel_;
@synthesize numberAccountLabel = numberAccountLabel_;
@synthesize numberAccountValueLabel = numberAccountValueLabel_;
@synthesize amountAdvanceLabel = amountAdvanceLabel_;
@synthesize amountAdvanceValueLabel = amountAdvanceValueLabel_;
@synthesize commissionLabel = commissionLabel_;
@synthesize commissionValueLabel = commissionValueLabel_;
@synthesize dayOfPayLabel_ = dayOfPayLabel_;
@synthesize dayOfPayValueLabel_ = dayOfPayValueLabel_;
@synthesize separatorTop = separatorTop_;
@synthesize optCordView = optCordView_;
@synthesize optCordLabel = optCordLabel_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransfersStepTwoViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/**
 * Releases graphic elements
 */
- (void)releaseTransfersStepTwoViewControllerGraphicElements {
    
    [containerView_ release];
    containerView_ = nil;
    
    [headerImageView_ release];
    headerImageView_ = nil;
    
    [acceptButton_ release];
    acceptButton_ = nil;
    
    [dateOperationLabel_ release];
    dateOperationLabel_ = nil;
    
    [dateOperationValueLabel_ release];
    dateOperationValueLabel_ = nil;
    
    [numberOperationLabel_ release];
    numberOperationLabel_ = nil;
    
    [numberOperationValueLabel_ release];
    numberOperationValueLabel_ = nil;
    
    [numberAccountLabel_ release];
    numberAccountLabel_ = nil;
    
    [numberAccountValueLabel_ release];
    numberAccountValueLabel_ = nil;
    
    [amountAdvanceLabel_ release];
    amountAdvanceLabel_ = nil;
    
    [amountAdvanceValueLabel_ release];
    amountAdvanceValueLabel_ = nil;
    
    [commissionLabel_ release];
    commissionLabel_ = nil;
    
    [commissionValueLabel_ release];
    commissionValueLabel_ = nil;
    
    [dayOfPayLabel_ release];
    dayOfPayLabel_ = nil;
    
    [dayOfPayValueLabel_ release];
    dayOfPayValueLabel_ = nil;
    
    [separatorTop_ release];
    separatorTop_ = nil;
    
    [optCordView_ release];
    optCordView_ = nil;
    
    [optCordLabel_ release];
    optCordLabel_ = nil;
    
    [editableViews_ removeAllObjects];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIView *view = self.view;
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    view.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
    containerView_.backgroundColor = [UIColor clearColor];
    
    /**/
    
    [NXT_Peru_iPhoneStyler styleLabel:grettingLabel_ withFontSize:TEXT_FONT_BIG_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:dateOperationLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:dateOperationValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:numberOperationLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:numberOperationValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:numberAccountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:numberAccountValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:amountAdvanceLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:amountAdvanceValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:commissionLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:commissionValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:dayOfPayLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:dayOfPayValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:optCordLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    
    dateOperationLabel_.text = @"Fecha y Hora de Operación";
    numberOperationLabel_.text = @"Nro. Operación";
    numberAccountLabel_.text = @"Nro. Cuenta";
    amountAdvanceLabel_.text = @"Monto del Adelanto";
    commissionLabel_.text = @"Comisión";
    dayOfPayLabel_.text = @"Día de Pago";
    
    /**/
    
    CGRect containerFrame = containerView_.frame;
    containerFrame.origin.x = 0.0f;
    containerFrame.origin.y = 0.0f;
    containerView_.frame = containerFrame;
    
    [self setScrollableView:containerView_];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.origin.y = 0;
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
    [self layoutViews];
    
    [view bringSubviewToFront:brandingImageView_];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:acceptButton_];
    [acceptButton_ setTitle:@"Volver a Mis Cuentas" forState:UIControlStateNormal];
    [acceptButton_ addTarget:self action:@selector(acceptButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
    
    transparentScroll_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
}



/*
 * Creates and returns an autoreleased SalaryAdvanceSummaryViewController constructed from a NIB file
 */
+ (SalaryAdvanceSummaryViewController *)SalaryAdvanceSummaryViewController {
    
    SalaryAdvanceSummaryViewController *result = [[[SalaryAdvanceSummaryViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}


/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self displayStoredInformation];
    
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the
 * transfer last step confirmation notification
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [Session getInstance].globalPositionRequestServerData = YES;
    [[self navigationController] popToRootViewControllerAnimated:NO];
    
  // [self storeInformationIntoHelper];
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingImageView] frame]);
    [self setScrollNominalFrame:scrollFrame];
    
    
    
}

/*
 * Lays out the views
 */
- (void)layoutViews {
    
    if ([self isViewLoaded]) {
        
        CGRect frame = headerImageView_.frame;
        frame.origin.y = VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        headerImageView_.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame);
        
        frame = grettingLabel_.frame;
        frame.origin.y = auxPos;
        grettingLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = separatorTop_.frame;
        frame.origin.y = auxPos;
        separatorTop_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        
        frame = numberOperationLabel_.frame;
        frame.origin.y = auxPos;
        numberOperationLabel_.frame = frame;
        
        frame = numberOperationValueLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:numberOperationValueLabel_ forText:numberOperationValueLabel_.text];
        numberOperationValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        frame = numberAccountLabel_.frame;
        frame.origin.y = auxPos;
        numberAccountLabel_.frame = frame;
        
        frame = numberAccountValueLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:numberAccountValueLabel_ forText:numberAccountValueLabel_.text];
        numberAccountValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        frame = amountAdvanceLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:amountAdvanceLabel_ forText:amountAdvanceLabel_.text];
        amountAdvanceLabel_.frame = frame;
        
        frame = amountAdvanceValueLabel_.frame;
        frame.size.height = [Tools labelHeight:amountAdvanceValueLabel_ forText:amountAdvanceValueLabel_.text];
        frame.origin.y = auxPos;
        amountAdvanceValueLabel_.frame = frame;
        auxPos = MAX(CGRectGetMaxY(amountAdvanceValueLabel_.frame ),CGRectGetMaxY(amountAdvanceLabel_.frame)) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        frame = commissionLabel_.frame;
        frame.origin.y = auxPos;
        commissionLabel_.frame = frame;
        
        frame = commissionValueLabel_.frame;
        frame.size.height = [Tools labelHeight:commissionValueLabel_ forText:commissionValueLabel_.text];
        frame.origin.y = auxPos;
        commissionValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = dayOfPayLabel_.frame;
        frame.origin.y = auxPos;
        dayOfPayLabel_.frame = frame;
        
        frame = dayOfPayValueLabel_.frame;
        frame.size.height = [Tools labelHeight:dayOfPayValueLabel_ forText:dayOfPayValueLabel_.text];
        frame.origin.y = auxPos;
        dayOfPayValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        frame = dateOperationLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:dateOperationLabel_ forText:dateOperationLabel_.text];
        dateOperationLabel_.frame = frame;
        
        frame = dateOperationValueLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:dateOperationValueLabel_ forText:dateOperationValueLabel_.text];
        dateOperationValueLabel_.frame = frame;
        auxPos = MAX(CGRectGetMaxY(dateOperationValueLabel_.frame ),CGRectGetMaxY(dateOperationLabel_.frame)) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        frame = optCordLabel_.frame;
        frame.origin.y = 0;
        frame.size.height = [Tools labelHeight:optCordLabel_ forText:optCordLabel_.text] + 16;
        optCordLabel_.frame = frame;
        
        frame = optCordView_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:optCordLabel_ forText:optCordLabel_.text] + 16;
        optCordView_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = acceptButton_.frame;
        frame.origin.y = auxPos;
        acceptButton_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
    
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.origin.y = 0;
        frame.size.height = auxPos;
        containerView.frame = frame;
    
        [self recalculateScroll];
    }
    
}

-(void)displayStoredInformation{
    
    grettingLabel_.text = [NSString stringWithFormat:@"¡%@, ya puedes usar tu adelanto de sueldo!",
                           SalaryAdvanceSummary_.customer];
    
    dateOperationValueLabel_.text = [NSString stringWithFormat:@"%@ - %@",
                                     SalaryAdvanceSummary_.date,
                                     SalaryAdvanceSummary_.hour];
    numberOperationValueLabel_.text = SalaryAdvanceSummary_.numberOperation;
    numberAccountValueLabel_.text = SalaryAdvanceSummary_.account;
    amountAdvanceValueLabel_.text = SalaryAdvanceSummary_.amountAdvance;
    commissionValueLabel_.text = SalaryAdvanceSummary_.commission;
    dayOfPayValueLabel_.text = SalaryAdvanceSummary_.dayOfPay;
    
    optCordLabel_.text = SalaryAdvanceSummary_.messageNotification;
    
    [self layoutViews];

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initial configuration is set.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
}





#pragma mark -
#pragma mark User Interaction

/*
 * Performs accept buttom action
 */
- (IBAction)acceptButtonTapped {
    
    [Session getInstance].globalPositionRequestServerData = YES;
    [[self navigationController] popToRootViewControllerAnimated:YES];
    
}



#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    result.hidesBackButton = YES;
    result.customTitleView.topLabelText = @"Adelanto de Sueldo";
    return result;
    
}


@end
