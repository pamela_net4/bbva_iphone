//
//  SalaryAdvanceAffiliationViewController.h
//  NXT_Peru_iPhone
//
//  Created by Flavio Franco Tunqui on 3/17/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "NXTEditableViewController.h"
#import "NXTComboButton.h"
#import "SalaryAdvanceAffiliation.h"
#import "TermsAndConsiderationsViewController.h"

@class NXTTextField;
@class SimpleHeaderView;
@class NXTComboButton;
@class NXTCurrencyTextField;

@interface SalaryAdvanceAffiliationViewController : NXTEditableViewController<NXTComboButtonDelegate, UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate>{
    
@private
    
    /**
     * Container view
     */
    UIView *containerView_;
    
    UIView * viewHeader_;
    UILabel * lblHeadertext_;
    
    UIView * viewAsk1_;
    UIImageView * imgAsk1_;
    UILabel * lblAsk1_;
    UILabel * lblAnswer1_;
    
    UIView * viewAsk2_;
    UIImageView * imgAsk2_;
    UILabel * lblAsk2_;
    UILabel * lblAnswer2_;
    
    UIView * viewInformation_;
    UIImageView * imgInformation_;
    UILabel * lblInformation_;
    
    UIView * viewAsk3_;
    UIImageView * imgAsk3_;
    UILabel * lblAsk3_;
    UILabel * lblAnswer3_;
    
    UIView * line1_;
    
    UIView * viewEmail_;
    UILabel * lblLabelEmail_;
    UIButton * btnTTEmail_;
    UIButton * btnEditEmail_;
    NXTComboButton *emailCombo_;
    NXTTextField * emailTextField_;
    
    UIView * line2_;
    
    UIView * viewContract_;
    UILabel * lblLabelContract_;
    UIButton * btnTTContract_;
    UIButton * btnContract_;
    
    UIView * line3_;
    
    UIView * viewContractAcceptContract_;
    UILabel * lblLabelAcceptContract_;
    UIImageView * imgAlertAcceptContract_;
    UISwitch * switchAcceptContract_;
    
    UIView * viewOperation_;
    UILabel * lblLabelOperation_;
    UIButton * btnTTOperation_;
    UIView * viewEdittext_;
    NXTTextField * opTextField_;
    
    UIView * viewSeal_;
    UIView * viewLabel_;
    UILabel * lblLabelSeal_;
    UIImageView * imgSeal_;
    

    UIImageView *brandingImageView_;
    
    SalaryAdvanceAffiliation *salaryAdvanceAffiliation_;
    
    SalaryAdvanceAffiliationViewController * salaryAdvanceAffiliationViewController_;
    
    TermsAndConsiderationsViewController * termsAndConsiderationsViewController_;
}

/**
 * Provides readwrite access to the containerView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *containerView;

/**
 * Provides readwrite access to the containerView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *viewHeader;



/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *lblHeadertext;

/**
 * Provides readwrite access to the grettingInfoLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *viewAsk1;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView * imgAsk1;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *lblAsk1;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *lblAnswer1;



/**
 * Provides readwrite access to the grettingInfoLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *viewAsk2;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView * imgAsk2;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *lblAsk2;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *lblAnswer2;




/**
 * Provides readwrite access to the grettingInfoLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *viewInformation;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView * imgInformation;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *lblInformation;



/**
 * Provides readwrite access to the grettingInfoLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *viewAsk3;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView * imgAsk3;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *lblAsk3;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *lblAnswer3;



/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *line1;



/**
 * Provides readwrite access to the grettingInfoLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *viewEmail;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *lblLabelEmail;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton * btnTTEmail;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton * btnEditEmail;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTComboButton *emailCombo;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTTextField *emailTextField;




/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *line2;




/**
 * Provides readwrite access to the grettingInfoLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *viewContract;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *lblLabelContract;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton * btnTTContract;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton * btnContract;



/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *line3;


/**
 * Provides readwrite access to the grettingInfoLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *viewContractAcceptContract;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *lblLabelAcceptContract;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView * imgAlertAcceptContract;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UISwitch * switchAcceptContract;




/**
 * Provides readwrite access to the grettingInfoLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *viewOperation;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *lblLabelOperation;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton * btnTTOperation;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView * viewEdittext;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTTextField * opTextField;



/**
 * Provides readwrite access to the grettingInfoLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *viewSeal;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *viewLabel;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * lblLabelSeal;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView * imgSeal;




/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton * buttonConfirm;

/**
 * Provides readwrite access to the quotaMonthLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView * brandingImageView;




/**
 * Provides read-write access to the helper element
 */
@property (nonatomic, readwrite, retain) SalaryAdvanceAffiliation *salaryAdvanceAffiliation;

/**
 * Creates and returns an autoreleased SalaryAdvanceAffiliationViewController constructed from a NIB file
 *
 * @return The autoreleased SalaryAdvanceAffiliationViewController constructed from a NIB file
 */
+ (SalaryAdvanceAffiliationViewController *)salaryAdvanceAffiliationViewController;

@end