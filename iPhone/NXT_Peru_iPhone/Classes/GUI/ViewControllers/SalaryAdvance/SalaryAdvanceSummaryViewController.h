//
//  SalaryAdvanceSummaryViewController.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 5/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "NXTEditableViewController.h"
#import "SalaryAdvanceSummary.h"
@class NXTTextField;

@interface SalaryAdvanceSummaryViewController : NXTEditableViewController<UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate>{
    
    @private
    
    /**
     * Container view
     */
    UIView *containerView_;
    
    UIImageView *headerImageView_;
    
    UILabel * dateOperationLabel_;
    
    UILabel * dateOperationValueLabel_;
    
    UILabel * numberOperationLabel_;
    
    UILabel * numberOperationValueLabel_;
    
    UILabel * numberAccountLabel_;
    
    UILabel * numberAccountValueLabel_;
    
    UILabel * amountAdvanceLabel_;
    
    UILabel * amountAdvanceValueLabel_;
    
    UILabel * commissionLabel_;
    
    UILabel * commissionValueLabel_;
    
    UILabel * dayOfPayLabel_;
    
    UILabel * dayOfPayValueLabel_;
    
    UIView * separatorTop_;
    
    /**
     * Accept button
     */
    UIButton *acceptButton_;
    
    /*
     * gretting label
     */
    UILabel *grettingLabel_;
    
    /*
     * branding ImageView
     */
    UIImageView *brandingImageView_;
    
    UIView * optCordView_;
    
    UILabel * optCordLabel_;
    
    SalaryAdvanceSummary *SalaryAdvanceSummary_;
    
    SalaryAdvanceSummaryViewController * SalaryAdvanceSummaryViewController_;
}

/**
 * Provides readwrite access to the containerView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *containerView;

/**
 * Provides readwrite access to the headerImageView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *headerImageView;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *grettingLabel;

/**
 * Provides readwrite access to the dateOperationLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dateOperationLabel;

/**
 * Provides readwrite access to the dateOperationValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dateOperationValueLabel;

/**
 * Provides readwrite access to the numberOperationLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * numberOperationLabel;

/**
 * Provides readwrite access to the numberOperationValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * numberOperationValueLabel;

/**
 * Provides readwrite access to the numberAccountLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * numberAccountLabel;

/**
 * Provides readwrite access to the numberAccountValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * numberAccountValueLabel;

/**
 * Provides readwrite access to the emailLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * amountAdvanceLabel;

/**
 * Provides readwrite access to the emailValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * amountAdvanceValueLabel;

/**
 * Provides readwrite access to the commissionLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * commissionLabel;

/**
 * Provides readwrite access to the commissionValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * commissionValueLabel;

/**
 * Provides readwrite access to the dayOfPayLabel_. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dayOfPayLabel_;

/**
 * Provides readwrite access to the dayOfPayValueLabel_. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dayOfPayValueLabel_;

/**
 * Provides readwrite access to the separatorTop. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView * separatorTop;

/**
 * Provides readwrite access to the acceptButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *acceptButton;
/**
 * Provides readwrite access to the brandingImageView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;


@property (nonatomic, readwrite, retain) IBOutlet UIView * optCordView;

@property (nonatomic, readwrite, retain) IBOutlet UILabel * optCordLabel;

/**
 * Provides read-write access to the helper element
 */
@property (nonatomic, readwrite, retain) SalaryAdvanceSummary *SalaryAdvanceSummary;

/**
 * Creates and returns an autoreleased SalaryAdvanceSummaryViewController constructed from a NIB file
 *
 * @return The autoreleased SalaryAdvanceSummaryViewController constructed from a NIB file
 */
+ (SalaryAdvanceSummaryViewController *)SalaryAdvanceSummaryViewController;

@end
