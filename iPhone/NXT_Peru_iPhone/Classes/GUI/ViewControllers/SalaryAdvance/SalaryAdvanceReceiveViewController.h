//
//  SalaryAdvanceReceiveViewController.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 5/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "NXTEditableViewController.h"
#import "SalaryAdvanceReceive.h"
@class NXTTextField;

@interface SalaryAdvanceReceiveViewController : NXTEditableViewController<UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate>{
    
    @private
    
    /**
     * Container view
     */
    UIView *containerView_;
    
    UILabel * commissionLabel_;
    
    UILabel * commissionValueLabel_;
    
    UILabel * totalToPayLabel_;
    
    UILabel * totalToPayValueLabel_;
    
    UILabel * dayOfPayLabel_;
    
    UILabel * dayOfPayValueLabel_;
    
    UIView * separatorTop_;
    
    /**
     * Accept button
     */
    UIButton *acceptButton_;
    
    UIButton *modifyButton_;
    
    /*
     * gretting label
     */
    UILabel *grettingLabel_;
    
    /*
     * branding ImageView
     */
    UIImageView *brandingImageView_;
    
    UIView * optCordView_;
    
    UIView * sealView_;
    
    UILabel * optCordLabel_;
    
    UIButton * optCordTipButton_;

    UIImageView *optCordTipImageView_;
    
    NXTTextField * optCordTextField_;
    
    UIImageView * sealImageView_;
    
     UILabel *sealLabel_;
    
    SalaryAdvanceReceive *SalaryAdvanceReceive_;
    
    SalaryAdvanceReceiveViewController * SalaryAdvanceReceiveViewController_;
}

/**
 * Provides readwrite access to the containerView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *containerView;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *grettingLabel;

/**
 * Provides readwrite access to the commissionLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * commissionLabel;

/**
 * Provides readwrite access to the commissionValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * commissionValueLabel;

/**
 * Provides readwrite access to the totalToPayLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * totalToPayLabel;

/**
 * Provides readwrite access to the totalToPayValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * totalToPayValueLabel;

/**
 * Provides readwrite access to the dayOfPayLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dayOfPayLabel;

/**
 * Provides readwrite access to the dayOfPayValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dayOfPayValueLabel;

/**
 * Provides readwrite access to the separatorTop. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView * separatorTop;

/**
 * Provides readwrite access to the acceptButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *acceptButton;

/**
 * Provides readwrite access to the acceptButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *modifyButton;
/**
 * Provides readwrite access to the brandingImageView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView *optCordTipImageView;

@property (nonatomic, readwrite, retain) IBOutlet UIView * optCordView;

@property (nonatomic, readwrite, retain) IBOutlet UIView * sealView;

@property (nonatomic, readwrite, retain) IBOutlet UILabel * optCordLabel;

@property (nonatomic, readwrite, retain) IBOutlet UIButton * optCordTipButton;

@property (nonatomic, readwrite, retain) IBOutlet NXTTextField * optCordTextField;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView * sealImageView;

@property (nonatomic, readwrite, retain) IBOutlet UILabel * sealLabel;

/**
 * Provides read-write access to the helper element
 */
@property (nonatomic, readwrite, retain) SalaryAdvanceReceive *SalaryAdvanceReceive;

@property (nonatomic, readwrite, retain) NSString * SPOposition;

@property (nonatomic, readwrite, retain) NSString * SPOamountOnlyNumbers;

@property (nonatomic, readwrite, retain) NSString * SPOdayOfPay;

/**
 * Creates and returns an autoreleased SalaryAdvanceReceiveViewController constructed from a NIB file
 *
 * @return The autoreleased SalaryAdvanceReceiveViewController constructed from a NIB file
 */
+ (SalaryAdvanceReceiveViewController *)SalaryAdvanceReceiveViewController;

@end
