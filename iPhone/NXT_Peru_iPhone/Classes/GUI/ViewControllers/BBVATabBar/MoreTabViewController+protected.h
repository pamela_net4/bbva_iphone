/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MoreTabViewController.h"


//Forward declarations
@class BBVATabBarTabInformation;


/**
 * MoreTabViewController protected category
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MoreTabViewController(protected)

/**
 * Displays the provided view controller as the initial view controller. Default implementation does nothing
 *
 * @param viewController The view controller to display
 * @param tabBar The tab bar the view controller is located in
 * @protected
 */
- (void)displayViewControllerAsBase:(UIViewController *)viewController
                         fromTabBar:(BBVATabBarTabInformation *)tabBar;

/**
 * Checks whether a given tab bar can be displayed or not
 *
 * @param tabBar The tab bar to check
 * @return YES when the given tab can be displayed, NO otherwise
 * @protected
 */
- (BOOL)checkTabBarCanBeDisplayed:(BBVATabBarTabInformation *)tabBar;

/**
 * Reloads the table view and enables or disables scrolling
 *
 * @protected
 */
- (void)loadTableInformation;

@end
