/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


//Forward declarations
@class BBVATabBarTabInformation;


/**
 * View to represent a tab view with the icon and the text (not including the background) at the given state
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BBVATabView : UIView {
    
@private
    
    /**
     * Tab bar information to display
     */
    BBVATabBarTabInformation *tabInformation_;
    
    /**
     * Tab selected flag. YES when tab is selected, NO otherwise
     */
    BOOL tabSelected_;
    
    /**
     * Tab disabled flag. YES when the tab is disabled, NO otherwise. This flag has only meaning when the
     * tab base selected flag is NO
     */
    BOOL tabDisabled_;
    
    /**
     * iPhone device flag. YES when the device is an iPhone, NO when it is an iPad
     */
    BOOL iPhoneDevice_;
    
    /**
     * Draw text flag. YES to display the tab text, NO otherwise
     */
    BOOL drawText_;

}


/**
 * Provides read-write access to the tab bar information to display
 */
@property (nonatomic, readwrite, retain) BBVATabBarTabInformation *tabInformation;

/**
 * Provides read-write access to the tab selected flag
 */
@property (nonatomic, readwrite, assign) BOOL tabSelected;

/**
 * Provides read-write access to the tab disabled flag
 */
@property (nonatomic, readwrite, assign) BOOL tabDisabled;

/**
 * Provides read-write access to the draw text flag
 */
@property (nonatomic, readwrite, assign) BOOL drawText;

@end
