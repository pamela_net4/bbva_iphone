/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BBVATabView.h"
#import "BBVATabBarDrawingUtils.h"
#import "BBVATabBarTabInformation.h"


#pragma mark -

/**
 * BBVATabView private extension
 */
@interface BBVATabView()

/**
 * Initializes the view providing a clear background color
 *
 * @private
 */
- (void)initializeBBVATabView;

@end


#pragma mark -

@implementation BBVATabView

#pragma mark -
#pragma mark Properties

@synthesize tabInformation = tabInformation_;
@synthesize tabSelected = tabSelected_;
@synthesize tabDisabled = tabDisabled_;
@synthesize drawText = drawText_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [tabInformation_ release];
    tabInformation_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initializes the view
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initializeBBVATabView];
    
}

/**
 * Superclass designated initializer. Initializes and returns a newly allocated BBVATabView object with the specified frame rectangle.
 *
 * @param frame The frame rectangle for the view, measured in points
 * @return The initialized BBVATabView instance
 */
- (id)initWithFrame:(CGRect)frame {
    
    if ((self = [super initWithFrame:frame])) {
        
        [self initializeBBVATabView];
        
    }
    
    return self;
    
}

/*
 * Initializes the view providing a clear background color
 */
- (void)initializeBBVATabView {
    
    [super setBackgroundColor:[UIColor clearColor]];
    
    iPhoneDevice_ = YES;
    
#ifdef UI_USER_INTERFACE_IDIOM
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        iPhoneDevice_ = NO;
        
    }
    
#endif
    
    drawText_ = YES;

}

#pragma mark -
#pragma mark Drawing methods

/**
 * Draws the receiver’s image within the passed-in rectangle
 *
 * @param rect A rectangle defining the area to restrict drawing to
 */
- (void)drawRect:(CGRect)rect {
    
    CGRect drawingRect = self.bounds;
    drawingRect.origin.x = 0.0f;
    drawingRect.origin.y = 0.0f;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    CGContextSetAllowsAntialiasing(context, true);
    
    if (tabSelected_) {
        
        [BBVATabBarDrawingUtils drawSelectedTabElement:tabInformation_
                                           intoContext:context
                                           withinFrame:drawingRect
                                      foriPhoneTabType:iPhoneDevice_
                                              drawText:drawText_];
        
    } else if (tabDisabled_) {
        
        [BBVATabBarDrawingUtils drawDisabledTabElement:tabInformation_
                                           intoContext:context
                                             withinFrame:drawingRect
                                      foriPhoneTabType:iPhoneDevice_
                                              drawText:drawText_];
        
    } else {
        
        [BBVATabBarDrawingUtils drawUnselectedTabElement:tabInformation_
                                             intoContext:context
                                             withinFrame:drawingRect
                                        foriPhoneTabType:iPhoneDevice_
                                                drawText:drawText_];
        
    }

    CGContextRestoreGState(context);

}

#pragma mark -
#pragma mark UIView selectors

/**
 * Sets the view background color. Overriden to avoid changing the background color, so it does nothing
 *
 * @param backgroundColor The new background color
 */
- (void)setBackgroundColor:(UIColor *)backgroundColor {
    
}

/**
 * Sets the new frame view. The view is marked for drawing again
 *
 * @param frame The new view frame
 */
- (void)setFrame:(CGRect)frame {
    
    [super setFrame:frame];
    
    [self setNeedsDisplay];
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the new tab bar information and sets the view to redraw
 *
 * @param tabInformation The new tab information to set
 */
- (void)setTabInformation:(BBVATabBarTabInformation *)tabInformation {
    
    if (tabInformation_ != tabInformation) {
        
        [tabInformation retain];
        [tabInformation_ release];
        tabInformation_ = tabInformation;
        
        [self setNeedsDisplay];
        
    }
    
}

/*
 * Sets the new tab bar selected flag and sets the view to redraw
 *
 * @param tabBarSelected The new tab bar selected flag
 */
- (void)setTabSelected:(BOOL)tabBarSelected {
    
    if (tabSelected_ != tabBarSelected) {
        
        tabSelected_ = tabBarSelected;
        
        [self setNeedsDisplay];
        
    }
    
}

/*
 * Sets the new tab bar disabled flag and sets the view to redraw
 *
 * @param tabBarDisabled The new tab bar disabled flag
 */
- (void)setTabDisabled:(BOOL)tabBarDisabled {
    
    if (tabDisabled_ != tabBarDisabled) {
        
        tabDisabled_ = tabBarDisabled;
        
        [self setNeedsDisplay];
        
    }
    
}

/*
 * Sets the new draw text flag and redraws the view
 *
 * @param drawText The new draw text flag to set
 */
- (void)setDrawText:(BOOL)drawText {
    
    if (drawText_ != drawText) {
        
        drawText_ = drawText;
        
        [self setNeedsDisplay];
        
    }
    
}

@end
