/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BBVATabBarRelocateTabsViewController.h"
#import "BBVATabBarBackground.h"
#import "BBVATabBarConstants.h"
#import "BBVATabBarDrawingUtils.h"
#import "BBVATabBarTabInformation.h"
#import "BBVATabRelocateView.h"
#import "BBVATabView.h"


/**
 * Local dark blue color
 */
#define LOCAL_DARK_BLUE_COLOR                                       [UIColor colorWithRed:0.0f green:0.2471f blue:0.5529f alpha:1.0f]

/**
 * Local light blue color
 */
#define LOCAL_LIGHT_BLUE_COLOR                                      [UIColor colorWithRed:0.0f green:0.5765f blue:0.8784f alpha:1.0f]

/**
 * General tabs vertical offset measured in points
 */
#define GENERAL_TABS_VERTICAL_OFFSET                                5.0f

/**
 * General tabs horizontal offset measured in points
 */
#define GENERAL_TABS_HORIZONTAL_OFFSET                              12.0f

/**
 * Number of icons columns
 */
#define ICONS_COLUMNS_NUMBER                                        4

/**
 * Defines the animations durations measured in seconds
 */
#define ANIMATIONS_DURATIONS                                        0.1f

/**
 * Defines the header background image view height
 */
#define HEADER_BACKGROND_IMAGE_VIEW_HEIGHT                          49.0f

/**
 * Defines the iPad header background image view height
 */
#define I_PAD_HEADER_BACKGROND_IMAGE_VIEW_HEIGHT                    76.0f

/**
 * Defines the title label left and right offset from header borders measured in points
 */
#define HEADER_LABEL_LEFT_AND_RIGHT_OFFSET                          20.0f

/**
 * Defines the tab bar height measured in points
 */
#define TAB_BAR_HEIGHT                                              44.0f


#pragma mark -

/**
 * BBVATabBarRelocateTabsViewController private extension
 */
@interface BBVATabBarRelocateTabsViewController()

/**
 * Releases the graphic elements not needed when view is hidden
 *
 * @private
 */
- (void)releaseBBVATabBarRelocateTabsViewControllerGraphicElements;

/**
 * Creates the graphic elements in case they are not created and adds them to the view controller's view
 *
 * @private
 */
- (void)createBBVATabBarRelocateTabsViewControllerGraphicElements;

/**
 * Invoked by framework when user taps on the done button. The delegate is notified that the tabs has been reordered
 *
 * @private
 */
- (void)doneButtonTapped;

/**
 * Invoked by framework when user taps on the reset button. The tabs are reseted to their default order
 *
 * @private
 */
- (void)resetButtonTapped;

/**
 * Resest the tabs order to the default
 */
- (void)resetOrderToDefault;

/**
 * Returns the tabs array in the new selected order
 *
 * @return The tabs array in the new selected order. All objects are BBVATabBarTabInformation instances
 */
- (NSArray *)orderedTabsArray;

/**
 * Calculates the tab width for iPhone version
 *
 * @return The tab width
 * @private
 */
- (CGFloat)iPhoneTabWidth;

/**
 * Populates the tab views arrays and displays them inside the view
 *
 * @private
 */
- (void)populateTabViewsArrays;

/**
 * Checks whether the tab bar tab being moved invades another editable tab bar tab location. When it is the case, tabs are swapped both in the
 * tab bar and the general section. Their tab information is also swapped both in tab bar array and general array. The moving view
 * original top left corner and the view touch points are updated
 *
 * @param point The touch point location to test
 * @private
 */
- (void)checkTabBarInvadingOtherEditableTabBarForPoint:(CGPoint)point;

/**
 * Finishes a general tab edition. When the touch is not inside the tab bar or over an editable tab in the tab bar, the view is just
 * returned to its rest state. When the touch is over an editable tab bar, their positions in the general section are switched
 *
 * @param touchViewLocation The touch location inside the view
 * @private
 */
- (void)finishGeneralTabEditionAtViewLocation:(CGPoint)touchViewLocation;

/**
 * Finishes a general tab edition when a tab in the tab bar was invaded. When the touch is over an editable tab bar, their positions in the general
 * section are switched and all animations are performed
 *
 * @param touchViewLocation The touch location inside the view
 * @return YES when the general tab invaded an editable tab bar, NO otherwise
 * @private
 */
- (BOOL)finishGeneralTabEditionCheckingTabInvadingAtViewLocation:(CGPoint)touchViewLocation;

/**
 * Finishes a general tab edition when a tab is added to the tab bar. This option has only meaning in iPad. When the tab is located between the "more"
 * tab and the following in the tab (in an empty location), the tab bar is added to the tab bar. All animations are performed
 *
 * @param touchViewLocation The touch location inside the view
 * @return YES when the general tab is added, NO otherwise
 * @private
 */
- (BOOL)finishGeneralTabEditionCheckingTabAdditionAtViewLocation:(CGPoint)touchViewLocation;

/**
 * Finishes a tab bar tab edition. iPhone version just returns to th rest state, but iPad version checks whether the view is too far away from
 * the bar to remove it from the tab bar
 *
 * @param touchViewLocation The touch location inside the view
 * @private
 */
- (void)finishTabBarTabEditionAtViewLocation:(CGPoint)touchViewLocation;

/**
 * Finishes a tab bar tab edition when the tab bar is too far away from the tab bar so it must be removed from it. All animations are performed
 *
 * @param touchViewLocation The touch location inside the view
 * @return YES when the tab bar tab is removed, NO otherwise
 * @private
 */
- (BOOL)finishTabBarTabEditionCheckingTabRemovalFromTabBarAtViewLocation:(CGPoint)touchViewLocation;

/**
 * Sets the view to a "rest" state where no edition is performed. Any view that must return to its location is animated
 *
 * @private
 */
- (void)returnToRestState;

/**
 * Removes the auxiliary moving view
 *
 * @private
 */
- (void)removeAuxiliaryMovingView;

/**
 * Invoked by framework when the reset state animation comes to an end. The auxiliary moving view is removed
 *
 * @param animationID An NSString containing an optional application-supplied identifier
 * @param finished An NSNumber object containing a Boolean value
 * @param context An optional application-supplied context
 * @private
 */
- (void)resetStateAnimationDidStop:(NSString *)animationID
                          finished:(NSNumber *)finished
                           context:(void *)context;

/**
 * Calculates the rectangle where general tabs can be displayed
 *
 * @return The rectangle where general tabs can be displayed
 * @private
 */
- (CGRect)generalTabsRectangle;

/**
 * Arranges the tabs stored in the general section array
 *
 * @private
 */
- (void)arrangeGeneralTabs;

/**
 * Arranges the tabs stored in the tab bar section array
 *
 * @private
 */
- (void)arrangeTabBarTabs;

/**
 * Calculates the equivalent frame once applied the auxiliary moving view transform. The result is equal to applying the
 * transform to the view, so the frame is "centered" in the same point (if no translation is applied)
 *
 * @param frame The frame to transform;
 * @return The calculated equivalent frame once applied the auxiliary moving view transform
 * @private
 */
- (CGRect)applyAuxiliaryMovingViewTransform:(CGRect)frame;

@end


#pragma mark -

@implementation BBVATabBarRelocateTabsViewController

#pragma mark -
#pragma mark Properties

@synthesize logoImage = logoImage_;
@synthesize navigationBarClass = navigationBarClass_;
@synthesize navigationBarColor = navigationBarColor_;
@synthesize viewBackgroundColor = viewBackgroundColor_;
@synthesize titleTextColor = titleTextColor_;
@synthesize relocateHeaderBackgroundImage = relocateHeaderBackgroundImage_;
@synthesize relocateTabNavigationTitle = relocateTabNavigationTitle_;
@synthesize relocateTabHeaderTitle = relocateTabHeaderTitle_;
@synthesize relocateTabResetText = relocateTabResetText_;
@synthesize relocateTabDoneText = relocateTabDoneText_;
@synthesize relocateTabsViewControllerDelegate = relocateTabsViewControllerDelegate_;

#pragma mark -
#pragma mark Memory managenet


/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self releaseBBVATabBarRelocateTabsViewControllerGraphicElements];
    
    [doneButton_ release];
    doneButton_ = nil;
    
    [resetButton_ release];
    resetButton_ = nil;
    
    [logoImage_ release];
    logoImage_ = nil;
    
    [defaultTabsOrderArray_ release];
    defaultTabsOrderArray_ = nil;
    
    [currentTabsOrderArray_ release];
    currentTabsOrderArray_ = nil;
    
    [currentEditableTabsOrderArray_ release];
    currentEditableTabsOrderArray_ = nil;
    
    [moreTabInformation_ release];
    moreTabInformation_ = nil;

    [navigationBarClass_ release];
    navigationBarClass_ = nil;
    
    [navigationBarColor_ release];
    navigationBarColor_ = nil;
    
    [viewBackgroundColor_ release];
    viewBackgroundColor_ = nil;
    
    [titleTextColor_ release];
    titleTextColor_ = nil;

    [relocateHeaderBackgroundImage_ release];
    relocateHeaderBackgroundImage_ = nil;
    
    [relocateTabNavigationTitle_ release];
    relocateTabNavigationTitle_ = nil;
    
    [relocateTabHeaderTitle_ release];
    relocateTabHeaderTitle_ = nil;
    
    [relocateTabResetText_ release];
    relocateTabResetText_ = nil;
    
    [relocateTabDoneText_ release];
    relocateTabDoneText_ = nil;
    
    [generalTabViewsArray_ release];
    generalTabViewsArray_ = nil;
    
    [tabBarViewsArray_ release];
    tabBarViewsArray_ = nil;
    
    [currentEditableTabsDisplayedArray_ release];
    currentEditableTabsDisplayedArray_ = nil;
    
    [currentTabsInTabBarArray_ release];
    currentTabsInTabBarArray_ = nil;
    
    [moreTabInformation_ release];
    moreTabInformation_ = nil;
    
    [selectedView_ release];
    selectedView_ = nil;
    
    [auxiliaryMovingView_ release];
    auxiliaryMovingView_ = nil;
    
    [selectedInformation_ release];
    selectedInformation_ = nil;

    relocateTabsViewControllerDelegate_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory. The tab bar is released
 */
- (void)viewDidUnload {
    
    [self releaseBBVATabBarRelocateTabsViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/*
 * Releases the graphic elements not needed when view is hidden
 */
- (void)releaseBBVATabBarRelocateTabsViewControllerGraphicElements {
    
    [topBar_ release];
    topBar_ = nil;
    
    [graphicContainerView_ release];
    graphicContainerView_ = nil;
    
    [titleBackgroundImageView_ release];
    titleBackgroundImageView_ = nil;
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [tabBarBackgroundView_ release];
    tabBarBackgroundView_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Creates the management arrays
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    iPhoneDevice_ = YES;
    
#ifdef UI_USER_INTERFACE_IDIOM
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        iPhoneDevice_ = NO;
        
    }
    
#endif

    NSZone *zone = self.zone;
    
    if (defaultTabsOrderArray_ == nil) {
        
        defaultTabsOrderArray_ = [[NSMutableArray allocWithZone:zone] init];
        
    }
    
    if (currentTabsOrderArray_ == nil) {
        
        currentTabsOrderArray_ = [[NSMutableArray allocWithZone:zone] init];
        
    }
    
    if (currentEditableTabsOrderArray_ == nil) {
        
        currentEditableTabsOrderArray_ = [[NSMutableArray allocWithZone:zone] init];
        
    }
    
    if (generalTabViewsArray_ == nil) {
        
        generalTabViewsArray_ = [[NSMutableArray allocWithZone:zone] init];
        
    }
    
    if (tabBarViewsArray_ == nil) {
        
        tabBarViewsArray_ = [[NSMutableArray allocWithZone:zone] init];
        
    }
    
    if (defaultTabsOrderArray_ == nil) {
        
        defaultTabsOrderArray_ = [[NSMutableArray allocWithZone:zone] init];
        
    }
    
    if (currentEditableTabsOrderArray_ == nil) {
        
        currentEditableTabsOrderArray_ = [[NSMutableArray allocWithZone:zone] init];
        
    }
    
    if (currentEditableTabsDisplayedArray_ == nil) {
        
        currentEditableTabsDisplayedArray_ = [[NSMutableArray allocWithZone:zone] init];
        
    }
    
    if (currentTabsInTabBarArray_ == nil) {
        
        currentTabsInTabBarArray_ = [[NSMutableArray allocWithZone:zone] init];
        
    }
    
    viewBackgroundColor_ = [LOCAL_DARK_BLUE_COLOR retain];
    titleTextColor_ = [LOCAL_LIGHT_BLUE_COLOR retain];

}

/*
 * Creates and returns an autoreleased BBVATabBarRelocateTabsViewController constructed from a NIB file
 */
+ (BBVATabBarRelocateTabsViewController *)bbvaTabBarRelocateTabsViewController {
    
    BBVATabBarRelocateTabsViewController *result = [[[BBVATabBarRelocateTabsViewController alloc] init] autorelease];
    [result awakeFromNib];
    
    return result;

}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory. Styles graphic elements
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self createBBVATabBarRelocateTabsViewControllerGraphicElements];
    
    titleBackgroundImageView_.image = relocateHeaderBackgroundImage_;
    
    UIFont *font = nil;
    
    if (iPhoneDevice_) {

        font = [UIFont boldSystemFontOfSize:17.0f];
        
    } else {
        
        font = [UIFont systemFontOfSize:17.0f];
        
    }
    
    titleLabel_.font = font;
    titleLabel_.textColor = titleTextColor_;
    titleLabel_.highlightedTextColor = titleTextColor_;
    titleLabel_.numberOfLines = 2.0f;
    titleLabel_.text = relocateTabHeaderTitle_;
    
    if (navigationBarColor_ != nil) {
        
        topBar_.tintColor = navigationBarColor_;
        
    }
    
    graphicContainerView_.backgroundColor = viewBackgroundColor_;
    self.view.backgroundColor = [UIColor blackColor];

    tabBarBackgroundView_.logoImage = logoImage_;

}

/**
 * Notifies the view controller that its view is about to be become visible. Checks whether the view controller is inside a navigation
 * view controller to hide or show the top bar
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    UINavigationItem *navigationItem = self.navigationItem;
    
    if (navigationItem != nil) {
        
        [topBar_ setItems:[NSArray arrayWithObject:navigationItem] animated:NO];
        
    }
    
    tabBarBackgroundView_.tabsCount = maximumTabsInTabBarCount_;

}

/**
 * Returns a Boolean value indicating whether the view controller supports the specified orientation. It returns YES for any orientation
 * on an iPad, and vertical orientation on an iPhone, NO otherwise
 *
 * @param toInterfaceOrientation The orientation of the application’s user interface after the rotation
 * @return YES if the view controller auto-rotates its view to the specified orientation; otherwise, NO
 */
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    
    BOOL result = YES;
    
    if ((iPhoneDevice_) && (toInterfaceOrientation != UIInterfaceOrientationPortrait)) {
        
        result = NO;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark User interaction

/*
 * Invoked by framework when user taps on the done button. The delegate is notified that the tabs has been reordered
 */
- (void)doneButtonTapped {
    
    if (relocateTabsViewControllerDelegate_ != nil) {
        
        NSArray *tabOrder = [self orderedTabsArray];
        [relocateTabsViewControllerDelegate_ bbvaTabBarRelocateTabsViewController:self
                                                                      orderedTabs:tabOrder
                                                                      tabBarCount:currentTabsInTabBarCount_];
        
    }
    
}

/*
 * Invoked by framework when user taps on the reset button. The tabs are reseted to their default order
 */
- (void)resetButtonTapped {

    [self resetOrderToDefault];
    
}

#pragma mark -
#pragma mark Visual elements management

/*
 * Creates the graphic elements in case they are not created and adds them to the view controller's view
 */
- (void)createBBVATabBarRelocateTabsViewControllerGraphicElements {
    
    UIView *view = self.view;
    
    CGRect frame = view.frame;
    CGFloat frameHeight = CGRectGetHeight(frame);
    CGFloat frameWidth = CGRectGetWidth(frame);
    NSZone *zone = self.zone;
    CGRect topBarFrame = frame;
    CGRect titleBackgroundImageViewFrame;
    CGRect titleLabelFrame;
    CGRect graphicContainerViewFrame;
    CGRect tabBarViewFrame;

    if (topBar_ == nil) {
        
        topBarFrame.origin.y = 0.0f;
        
        if (iPhoneDevice_) {
            
            topBarFrame.origin.x = 0.0f;
            
        } else {
            
            CGFloat iPadTabBarWidth = [BBVATabBarBackground iPadTabBarWidth];
            topBarFrame.origin.x = iPadTabBarWidth;
            topBarFrame.size.width = frameWidth - iPadTabBarWidth;
            
        }
        
        topBarFrame.size.height = TAB_BAR_HEIGHT;
        
        if (navigationBarClass_ == nil) {
            
            topBar_ = [[UINavigationBar allocWithZone:zone] initWithFrame:topBarFrame];
            
        } else {
            
            topBar_ = [[navigationBarClass_ allocWithZone:zone] init];
            topBar_.frame = topBarFrame;
            
        }
        
        [view addSubview:topBar_];
        topBar_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        
    }
    
    topBarFrame = topBar_.frame;
    CGFloat topBarX = topBarFrame.origin.x;
    CGFloat topBarWidth = topBarFrame.size.width;
    
    if (titleBackgroundImageView_ == nil) {
        
        titleBackgroundImageViewFrame.origin.x = topBarX;
        titleBackgroundImageViewFrame.origin.y = CGRectGetMaxY(topBarFrame);
        titleBackgroundImageViewFrame.size.width = topBarWidth;
        
        if (iPhoneDevice_) {
            
            titleBackgroundImageViewFrame.size.height = HEADER_BACKGROND_IMAGE_VIEW_HEIGHT;
            
        } else {
            
            titleBackgroundImageViewFrame.size.height = I_PAD_HEADER_BACKGROND_IMAGE_VIEW_HEIGHT;
            
        }
        
        titleBackgroundImageView_ = [[UIImageView allocWithZone:zone] initWithFrame:titleBackgroundImageViewFrame];
        [view addSubview:titleBackgroundImageView_];
        
        titleBackgroundImageView_.backgroundColor = [UIColor clearColor];
        titleBackgroundImageView_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        
    }
    
    titleBackgroundImageViewFrame = titleBackgroundImageView_.frame;
    CGFloat titleImageViewBottom = CGRectGetMaxY(titleBackgroundImageViewFrame);
    
    if (titleLabel_ == nil) {
        
        titleLabelFrame = titleBackgroundImageViewFrame;
        titleLabelFrame.origin.x = titleLabelFrame.origin.x + HEADER_LABEL_LEFT_AND_RIGHT_OFFSET;
        titleLabelFrame.size.width = CGRectGetWidth(titleBackgroundImageViewFrame) - (2.0f * HEADER_LABEL_LEFT_AND_RIGHT_OFFSET);
        
        titleLabel_ = [[UILabel allocWithZone:zone] initWithFrame:titleLabelFrame];
        [view addSubview:titleLabel_];
        
        titleLabel_.backgroundColor = [UIColor clearColor];
        titleLabel_.autoresizingMask =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        
    }
    
    if (tabBarBackgroundView_ == nil) {
        
        CGFloat tabBarHeight = CGRectGetHeight(frame);
        
        if (iPhoneDevice_) {
            
            tabBarHeight = [BBVATabBarBackground iPhoneTabBarHeight];
            
        }
        tabBarViewFrame.origin.x = 0.0f;
        tabBarViewFrame.origin.y = 0.0f;
        
        if (iPhoneDevice_) {
            
            tabBarViewFrame.origin.y = frameHeight - tabBarHeight;
            tabBarViewFrame.size.width = frameWidth;
            
        } else {
            
            tabBarViewFrame.size.width = [BBVATabBarBackground iPadTabBarWidth];
            
        }
        
        tabBarViewFrame.size.height = tabBarHeight;
        
        tabBarBackgroundView_ = [[BBVATabBarBackground allocWithZone:zone] initWithFrame:tabBarViewFrame];
        [view addSubview:tabBarBackgroundView_];
        
        if (iPhoneDevice_) {
            
            tabBarBackgroundView_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;

        } else {
            
            tabBarBackgroundView_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
            
        }
        
    }
    
    tabBarViewFrame = tabBarBackgroundView_.frame;

    if (graphicContainerView_ == nil) {
        
        CGFloat graphicContainerViewBottom = frameHeight;
        
        if (iPhoneDevice_) {
            
            graphicContainerViewBottom = CGRectGetMinY(tabBarViewFrame);
            
        }
        
        graphicContainerViewFrame.origin.x = topBarX;
        graphicContainerViewFrame.origin.y = titleImageViewBottom;
        graphicContainerViewFrame.size.width = topBarWidth;
        graphicContainerViewFrame.size.height = graphicContainerViewBottom - CGRectGetMaxY(titleBackgroundImageViewFrame);
        
        graphicContainerView_ = [[BBVATabBarViewControllerView allocWithZone:zone] initWithFrame:graphicContainerViewFrame];
        [view addSubview:graphicContainerView_];
        
        graphicContainerView_.bbvaTabBarViewControllerViewDelegate = self;
        graphicContainerView_.backgroundColor = [UIColor clearColor];
        graphicContainerView_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
    }
    
    [view bringSubviewToFront:titleLabel_];
    [view bringSubviewToFront:tabBarBackgroundView_];
    
}

/*
 * Populates the tab views arrays and displays them inside the view
 */
- (void)populateTabViewsArrays {
    
    UIView *view = self.view;
    NSZone *zone = self.zone;
    
    if (tabBarViewsArray_ != nil) {
        
        for (UIView *view in tabBarViewsArray_) {
            
            [view removeFromSuperview];
            
        }
        
        [tabBarViewsArray_ removeAllObjects];
        
        BBVATabView *tabBarView = nil;
        CGRect viewFrame = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
        NSNull *null = [NSNull null];
        
        for (BBVATabBarTabInformation *tabBarInformation in currentTabsInTabBarArray_) {
            
            tabBarView = [[[BBVATabView allocWithZone:zone] initWithFrame:viewFrame] autorelease];
            
            if (tabBarView != nil) {
                
                tabBarView.tabInformation = tabBarInformation;
                
                [tabBarViewsArray_ addObject:tabBarView];
                [view addSubview:tabBarView];
                
            } else {
                
                [tabBarViewsArray_ addObject:null];
                
            }
            
        }
        
        [self arrangeTabBarTabs];
        
    }
    
    if (generalTabViewsArray_ != nil) {
        
        for (UIView *view in generalTabViewsArray_) {
            
            [view removeFromSuperview];
            
        }
        
        [generalTabViewsArray_ removeAllObjects];
        
        BBVATabView *tabView = nil;
        CGRect viewFrame = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
        NSNull *null = [NSNull null];
        
        for (BBVATabBarTabInformation *tabInformation in currentEditableTabsDisplayedArray_) {
            
            tabView = [[[BBVATabView allocWithZone:zone] initWithFrame:viewFrame] autorelease];
            
            if (tabView != nil) {
                
                tabView.tabInformation = tabInformation;
                [generalTabViewsArray_ addObject:tabView];
                [view addSubview:tabView];
                
            } else {
                
                [generalTabViewsArray_ addObject:null];
                
            }
            
        }
        
        [self arrangeGeneralTabs];
        
    }
    
}

/*
 * Arranges the tabs stored in the general section array
 */
- (void)arrangeGeneralTabs {
    
    CGRect relocateFrame = [self generalTabsRectangle];
    CGFloat left = relocateFrame.origin.x;
    CGFloat top = relocateFrame.origin.y;
    CGFloat width = relocateFrame.size.width;

    CGFloat horizontalGapsCount = (CGFloat)(ICONS_COLUMNS_NUMBER + 1);
    CGFloat tabWidth = 0.0f;
    
    if (iPhoneDevice_) {
        
        tabWidth = round((width - (horizontalGapsCount * GENERAL_TABS_HORIZONTAL_OFFSET)) / (CGFloat)ICONS_COLUMNS_NUMBER);
        
    } else {
        
        tabWidth = [BBVATabBarBackground iPadTabBarWidth];
    }
    
    NSUInteger column = 0;
    NSUInteger row = 0;
    CGFloat tabHeight = BBVA_I_PHONE_TAB_BAR_VIEW_HEIGHT;
    
    if (!iPhoneDevice_) {
        
        tabHeight = BBVA_I_PAD_TABS_HEIGHT;
        
    }
    
    CGFloat iconToIconWidth = round(width / ICONS_COLUMNS_NUMBER);
    CGFloat iconToIconHeight = GENERAL_TABS_VERTICAL_OFFSET;
    
    if (iPhoneDevice_) {
        
        iconToIconHeight += BBVA_I_PHONE_TAB_BAR_VIEW_HEIGHT;
        
    } else {
        
        iconToIconHeight += BBVA_I_PAD_TABS_HEIGHT;
        
    }
    
    CGRect viewFrame = CGRectMake(left + GENERAL_TABS_HORIZONTAL_OFFSET,
                                  top + round((iconToIconHeight - tabHeight) / 2.0f) + GENERAL_TABS_VERTICAL_OFFSET,
                                  tabWidth,
                                  tabHeight);
    BBVATabView *tabView = nil;
    Class bbvaTabView = [BBVATabView class];

    for (NSObject *storedObject in generalTabViewsArray_) {
        
        if ([storedObject isKindOfClass:bbvaTabView]) {
            
            tabView = (BBVATabView *)storedObject;
            viewFrame.origin.x = left + ((CGFloat)column * iconToIconWidth) + round((iconToIconWidth - tabWidth) / 2.0f) + GENERAL_TABS_HORIZONTAL_OFFSET;
            tabView.frame = viewFrame;
            tabView.drawText = YES;
            tabView.tabDisabled = (!iPhoneDevice_);
            
        }
        
        column++;
        
        if (column == ICONS_COLUMNS_NUMBER) {
            
            column = 0;
            row++;
            
            viewFrame.origin.y = top + ((CGFloat)row * iconToIconHeight) + round((iconToIconHeight - tabHeight) / 2.0f) + GENERAL_TABS_VERTICAL_OFFSET;
            
        }
        
    }

}

/**
 * Arranges the tabs stored in the tab bar section array
 */
- (void)arrangeTabBarTabs {
    
    UIView *view = self.view;
    CGRect tabBarFrame = tabBarBackgroundView_.frame;
    tabBarFrame = [tabBarBackgroundView_.superview convertRect:tabBarFrame
                                                        toView:view];
    CGFloat barLeft = CGRectGetMinX(tabBarFrame);
    CGFloat barTop = CGRectGetMinY(tabBarFrame);
    Class bbvaTabViewClass = [BBVATabView class];

    BBVATabView *tabBarView = nil;
    CGFloat tabWidth = BBVA_I_PAD_TAB_BAR_VIEW_WIDTH;
    CGFloat tabHeight = BBVA_I_PAD_TABS_HEIGHT;
    NSUInteger position = 0;
    
    if (iPhoneDevice_) {
        
        tabWidth = [self iPhoneTabWidth];
        tabHeight = BBVA_I_PHONE_TAB_BAR_VIEW_HEIGHT;
        
    }
    
    CGRect viewFrame = CGRectMake(barLeft, barTop, tabWidth, tabHeight);
    position = 0;

    for (NSObject *storedObject in tabBarViewsArray_) {
        
        if ([storedObject isKindOfClass:bbvaTabViewClass]) {
            
            tabBarView = (BBVATabView *)storedObject;
            
            if (iPhoneDevice_) {
                
                tabBarView.drawText = YES;
                tabBarView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
                
                viewFrame.origin.x = barLeft + (tabWidth * (CGFloat)position);

            } else {
                
                tabBarView.drawText = NO;
                tabBarView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
                
                if (tabBarView.tabInformation == moreTabInformation_) {
                    
                    viewFrame.origin.y = barTop + (tabHeight * (CGFloat)(maximumTabsInTabBarCount_ - 1));
                    
                } else {
                    
                    viewFrame.origin.y = barTop + (tabHeight * (CGFloat)position);
                    
                }

            }
            
            if (![currentEditableTabsOrderArray_ containsObject:tabBarView.tabInformation]) {
                
                tabBarView.tabDisabled = YES;
                
            } else {
                
                tabBarView.tabDisabled = NO;
                
            }

            tabBarView.frame = viewFrame;

        }
        
        position++;
        
    }

}

/*
 * Checks whether the tab bar tab being moved invades another editable tab bar tab location. When it is the case, tabs are swapped both in the
 * tab bar and the general section. Their tab information is also swapped both in tab bar array and general array. The moving view
 * original top left corner and the view touch points are updated
 */
- (void)checkTabBarInvadingOtherEditableTabBarForPoint:(CGPoint)point {
    
    NSUInteger barTabsCount = [tabBarViewsArray_ count];
    BOOL tabBarInvaded = NO;
    NSObject *storedObject = nil;
    BBVATabView *invadedTabBarView = nil;
    Class bbvaTabViewClass = [BBVATabView class];
    CGRect tabViewFrame;
    NSUInteger invadedTabIndex = NSNotFound;
    
    for (NSUInteger i = 0; i < barTabsCount; i++) {
        
        storedObject = [tabBarViewsArray_ objectAtIndex:i];
        
        if ([storedObject isKindOfClass:bbvaTabViewClass]) {
            
            invadedTabBarView = (BBVATabView *)storedObject;
            
            if ((!invadedTabBarView.tabDisabled) && (invadedTabBarView.tabInformation != auxiliaryMovingView_.tabInformation)) {
                
                tabViewFrame = invadedTabBarView.frame;
                
                if (iPhoneDevice_) {
                    
                    CGFloat xLocation = point.x;
                    
                    if ((xLocation >= CGRectGetMinX(tabViewFrame)) && (xLocation <= CGRectGetMaxX(tabViewFrame))) {
                        
                        tabBarInvaded = YES;
                        invadedTabIndex = i;
                        break;
                        
                    }
                    
                } else {
                    
                    CGFloat yLocation = point.y;
                    
                    if ((yLocation >= CGRectGetMinY(tabViewFrame)) && (yLocation <= CGRectGetMaxY(tabViewFrame))) {
                        
                        tabBarInvaded = YES;
                        invadedTabIndex = i;
                        break;
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    if (tabBarInvaded) {
        
        if (invadedTabIndex < [currentTabsInTabBarArray_ count]) {
            
            BBVATabBarTabInformation *invadedInformation = [currentTabsInTabBarArray_ objectAtIndex:invadedTabIndex];
            NSUInteger generalInvadedInformationIndex = [currentEditableTabsOrderArray_ indexOfObject:invadedInformation];
            NSUInteger originalInformationIndex = [currentEditableTabsDisplayedArray_ indexOfObject:selectedInformation_];
            NSUInteger generalInvadedInformationVisualIndex = generalInvadedInformationIndex;
            
            if (moreTabElementsOrdered_) {
                
                generalInvadedInformationVisualIndex = [currentEditableTabsDisplayedArray_ indexOfObject:invadedInformation];
                
            }
            
            BBVATabView *invadedGeneralView = nil;
            BBVATabView *originalGeneralView = nil;
            
            if (generalInvadedInformationIndex < [generalTabViewsArray_ count]) {
                
                storedObject = [generalTabViewsArray_ objectAtIndex:generalInvadedInformationVisualIndex];
                
                if ([storedObject isKindOfClass:bbvaTabViewClass]) {
                    
                    invadedGeneralView = (BBVATabView *)storedObject;
                    
                }
                
            }
            
            if (originalInformationIndex < [generalTabViewsArray_ count]) {
                
                storedObject = [generalTabViewsArray_ objectAtIndex:originalInformationIndex];
                
                if ([storedObject isKindOfClass:bbvaTabViewClass]) {
                    
                    originalGeneralView = (BBVATabView *)storedObject;
                    
                }
                
            }
            
            if ((invadedGeneralView != nil) && (originalGeneralView != nil) && (auxiliaryMovingView_ != nil) && (invadedTabBarView != nil)) {
                
                [currentTabsInTabBarArray_ exchangeObjectAtIndex:initialTabLocation_
                                               withObjectAtIndex:invadedTabIndex];
                [currentEditableTabsOrderArray_ exchangeObjectAtIndex:initialGeneralLocation_
                                                    withObjectAtIndex:generalInvadedInformationIndex];
                [tabBarViewsArray_ exchangeObjectAtIndex:initialTabLocation_
                                       withObjectAtIndex:invadedTabIndex];
                
                initialTabLocation_ = invadedTabIndex;
                initialGeneralLocation_ = generalInvadedInformationIndex;
                
                if (!moreTabElementsOrdered_) {
                    
                    [generalTabViewsArray_ exchangeObjectAtIndex:initialGeneralVisualLocation_
                                               withObjectAtIndex:generalInvadedInformationVisualIndex];
                    [currentEditableTabsDisplayedArray_ exchangeObjectAtIndex:initialGeneralVisualLocation_
                                                            withObjectAtIndex:generalInvadedInformationVisualIndex];
                    initialGeneralVisualLocation_ = generalInvadedInformationVisualIndex;
                    
                }
                
                CGRect generalInvadedViewFrame = invadedGeneralView.frame;
                CGRect generalOriginalViewFrame = originalGeneralView.frame;
                
                CGRect tabBarInvadedViewFrame = invadedTabBarView.frame;
                
                if (iPhoneDevice_) {
                    
                    CGFloat invadedTabBarLeftPosition = tabBarInvadedViewFrame.origin.x;
                    tabBarInvadedViewFrame.origin.x = originalViewTopLeftPoint_.x;
                    
                    CGFloat originTabBarXDelta = originalViewTopLeftPoint_.x - invadedTabBarLeftPosition;
                    originalViewTopLeftPoint_.x = invadedTabBarLeftPosition;
                    originalAuxiliaryViewTopLeftPoint_.x = invadedTabBarLeftPosition;
                    originalTouchPoint_.x = originalTouchPoint_.x - originTabBarXDelta;
                    
                } else {
                    
                    CGFloat invadedTabBarTopPosition = tabBarInvadedViewFrame.origin.y;
                    tabBarInvadedViewFrame.origin.y = originalViewTopLeftPoint_.y;
                    
                    CGFloat originTabBarYDelta = originalViewTopLeftPoint_.y - invadedTabBarTopPosition;
                    originalViewTopLeftPoint_.y = invadedTabBarTopPosition;
                    originalAuxiliaryViewTopLeftPoint_.y = invadedTabBarTopPosition;
                    originalTouchPoint_.y = originalTouchPoint_.y - originTabBarYDelta;
                    
                }
                
                if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
                    
                    [UIView animateWithDuration:ANIMATIONS_DURATIONS
                                          delay:0.0f
                                        options:(UIViewAnimationCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState)
                                     animations:^{
                                         
                                         if (!moreTabElementsOrdered_) {
                                             
                                             invadedGeneralView.frame = generalOriginalViewFrame;
                                             originalGeneralView.frame = generalInvadedViewFrame;
                                             
                                         }
                                         
                                         invadedTabBarView.frame = tabBarInvadedViewFrame;
                                         
                                     }
                                     completion:nil];
                    
                } else {
                    
                    [UIView beginAnimations:nil
                                    context:nil];
                    
                    [UIView setAnimationDuration:ANIMATIONS_DURATIONS];
                    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                    [UIView setAnimationBeginsFromCurrentState:YES];
                    
                    if (!moreTabElementsOrdered_) {
                        
                        invadedGeneralView.frame = generalOriginalViewFrame;
                        originalGeneralView.frame = generalInvadedViewFrame;
                        
                    }
                    
                    invadedTabBarView.frame = tabBarInvadedViewFrame;
                    
                    [UIView commitAnimations];
                    
                }
                
            }
            
        }
        
    }
    
}

/*
 * Finishes a general tab edition. When the touch is not inside the tab bar or over an editable tab in the tab bar, the view is just
 * returned to its rest state. When the touch is over an editable tab bar, their positions in the general section are switched
 */
- (void)finishGeneralTabEditionAtViewLocation:(CGPoint)touchViewLocation {
    
    if (![self finishGeneralTabEditionCheckingTabInvadingAtViewLocation:touchViewLocation]) {
        
        if (!iPhoneDevice_) {
            
            if (![self finishGeneralTabEditionCheckingTabAdditionAtViewLocation:touchViewLocation]) {
                
                [self returnToRestState];
                
            }
            
        } else {
            
            [self returnToRestState];
            
        }
        
    }
        
}

/**
 * Finishes a general tab edition when a tab in the tab bar was invaded. When the touch is over an editable tab bar, their positions in the general
 * section are switched and all animations are performed
 *
 * @param touchViewLocation The touch location inside the view
 * @return YES when the general tab invaded an editable tab bar, NO otherwise
 * @private
 */
- (BOOL)finishGeneralTabEditionCheckingTabInvadingAtViewLocation:(CGPoint)touchViewLocation {
    
    BOOL result = NO;
    
    if (editingGeneralTab_) {
        
        CGFloat tabBarTop = tabBarBackgroundView_.frame.origin.y;
        CGFloat tabBarRight = CGRectGetMaxX(tabBarBackgroundView_.frame);
        
        if (((iPhoneDevice_) && (touchViewLocation.y >= tabBarTop)) ||
            ((!iPhoneDevice_) && (touchViewLocation.x <= tabBarRight))) {
            
            NSUInteger barTabsCount = [tabBarViewsArray_ count];
            NSObject *storedObject = nil;
            BBVATabView *invadedTabBarView = nil;
            Class bbvaTabViewClass = [BBVATabView class];
            CGRect tabViewFrame;
            NSUInteger invadedTabIndex = NSNotFound;
            CGFloat xLocation = touchViewLocation.x;
            CGFloat yLocation = touchViewLocation.y;
            
            for (NSUInteger i = 0; i < barTabsCount; i++) {
                
                storedObject = [tabBarViewsArray_ objectAtIndex:i];
                
                if ([storedObject isKindOfClass:bbvaTabViewClass]) {
                    
                    invadedTabBarView = (BBVATabView *)storedObject;
                    
                    if ((!invadedTabBarView.tabDisabled) && (invadedTabBarView.tabInformation != auxiliaryMovingView_.tabInformation)) {
                        
                        tabViewFrame = invadedTabBarView.frame;
                        
                        if (iPhoneDevice_) {
                            
                            if ((xLocation >= CGRectGetMinX(tabViewFrame)) && (xLocation <= CGRectGetMaxX(tabViewFrame))) {
                                
                                invadedTabIndex = i;
                                break;
                                
                            }
                            
                        } else {
                            
                            if ((yLocation >= CGRectGetMinY(tabViewFrame)) && (yLocation <= CGRectGetMaxY(tabViewFrame))) {
                                
                                invadedTabIndex = i;
                                break;
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
            if (invadedTabIndex != NSNotFound) {
                
                BBVATabBarTabInformation *invadedTabInformation = invadedTabBarView.tabInformation;
                
                NSUInteger generalTabInvadedIndex = [currentEditableTabsOrderArray_ indexOfObject:invadedTabInformation];
                NSUInteger generalTabInvadedVisualIndex = [currentEditableTabsDisplayedArray_ indexOfObject:invadedTabInformation];
                
                if ((generalTabInvadedIndex != NSNotFound) && (generalTabInvadedVisualIndex != NSNotFound)) {
                    
                    if (generalTabInvadedVisualIndex < [generalTabViewsArray_ count]) {
                        
                        NSObject *storedObject = [generalTabViewsArray_ objectAtIndex:generalTabInvadedVisualIndex];
                        
                        if ([storedObject isKindOfClass:bbvaTabViewClass]) {
                            
                            BBVATabView *generalTabInvadedView = (BBVATabView *)storedObject;
                            CGRect generalTabInvadedFrame = generalTabInvadedView.frame;
                            CGRect originalTabFrame = selectedView_.frame;
                            BBVATabView *originalTabBarView = nil;
                            CGRect tabBarInvadedFrame = invadedTabBarView.frame;
                            CGRect originalTabBarFrame = CGRectZero;
                            
                            [currentEditableTabsOrderArray_ exchangeObjectAtIndex:generalTabInvadedIndex
                                                                withObjectAtIndex:initialGeneralLocation_];
                            
                            if (!moreTabElementsOrdered_) {
                                
                                [currentEditableTabsDisplayedArray_ exchangeObjectAtIndex:generalTabInvadedVisualIndex
                                                                        withObjectAtIndex:initialGeneralVisualLocation_];
                                
                                [generalTabViewsArray_ exchangeObjectAtIndex:generalTabInvadedVisualIndex
                                                           withObjectAtIndex:initialGeneralVisualLocation_];
                                CGRect transformedTabFrame = [self applyAuxiliaryMovingViewTransform:originalTabFrame];
                                originalAuxiliaryViewTopLeftPoint_ = transformedTabFrame.origin;
                                
                            } else {
                                
                                CGRect transformedTabFrame = [self applyAuxiliaryMovingViewTransform:generalTabInvadedFrame];
                                originalAuxiliaryViewTopLeftPoint_ = transformedTabFrame.origin;
                                
                            }
                            
                            if (initialTabLocation_ != NSNotFound) {
                                
                                storedObject = [tabBarViewsArray_ objectAtIndex:initialTabLocation_];
                                
                                if ([storedObject isKindOfClass:bbvaTabViewClass]) {
                                    
                                    originalTabBarView = (BBVATabView *)storedObject;
                                    originalTabBarFrame = originalTabBarView.frame;
                                    
                                }
                                
                                [currentTabsInTabBarArray_ exchangeObjectAtIndex:invadedTabIndex
                                                               withObjectAtIndex:initialTabLocation_];
                                [tabBarViewsArray_ exchangeObjectAtIndex:invadedTabIndex
                                                       withObjectAtIndex:initialTabLocation_];
                                
                            } else {
                                
                                [currentTabsInTabBarArray_ replaceObjectAtIndex:invadedTabIndex
                                                                     withObject:selectedInformation_];
                                
                            }
                            
                            if (originalTabBarView == nil) {
                                
                                invadedTabBarView.tabInformation = selectedInformation_;
                                
                            }
                            
                            auxiliaryMovingView_.tabInformation = invadedTabInformation;
                            
                            if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
                                
                                [UIView animateWithDuration:ANIMATIONS_DURATIONS
                                                      delay:0.0f
                                                    options:(UIViewAnimationCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState)
                                                 animations:^{
                                                     
                                                     if (!moreTabElementsOrdered_) {
                                                         
                                                         generalTabInvadedView.frame = originalTabFrame;
                                                         selectedView_.frame = generalTabInvadedFrame;
                                                         
                                                     }
                                                     
                                                     if (originalTabBarView != nil) {
                                                         
                                                         originalTabBarView.frame = tabBarInvadedFrame;
                                                         invadedTabBarView.frame = originalTabBarFrame;
                                                         
                                                     }
                                                     
                                                 }
                                                 completion:nil];
                                
                            } else {
                                
                                [UIView beginAnimations:nil
                                                context:nil];
                                
                                [UIView setAnimationDuration:ANIMATIONS_DURATIONS];
                                [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                                [UIView setAnimationBeginsFromCurrentState:YES];
                                
                                if (!moreTabElementsOrdered_) {
                                    
                                    generalTabInvadedView.frame = originalTabFrame;
                                    selectedView_.frame = generalTabInvadedFrame;
                                    
                                }
                                
                                if (originalTabBarView != nil) {
                                    
                                    originalTabBarView.frame = tabBarInvadedFrame;
                                    invadedTabBarView.frame = originalTabBarFrame;
                                    
                                }
                                
                                [UIView commitAnimations];
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
                [self returnToRestState];
                
                result = YES;

            }

        }
        
    }

    return result;
    
}

/*
 * Finishes a general tab edition when a tab is added to the tab bar. This option has only meaning in iPad. When the tab is located between the "more"
 * tab and the following in the tab (in an empty location), the tab bar is added to the tab bar. All animations are performed
 */
- (BOOL)finishGeneralTabEditionCheckingTabAdditionAtViewLocation:(CGPoint)touchViewLocation {
    
    BOOL result = NO;
    
    if ((initialTabLocation_ == NSNotFound) && (currentTabsInTabBarCount_ < (maximumTabsInTabBarCount_ - 1)) && (!iPhoneDevice_) && (editingGeneralTab_)) {
        
        CGFloat tabBarRight = CGRectGetMaxX(tabBarBackgroundView_.frame);
        
        if (touchViewLocation.x <= tabBarRight) {
            
            NSUInteger moreTabIndex = [currentTabsInTabBarArray_ indexOfObject:moreTabInformation_];
            
            if (moreTabIndex != NSNotFound) {
                
                if (moreTabIndex < [tabBarViewsArray_ count]) {
                    
                    BBVATabView *moreTabView = [tabBarViewsArray_ objectAtIndex:moreTabIndex];
                    CGFloat bottomPosition = CGRectGetMinY(moreTabView.frame);
                    CGFloat topPosition = 0.0f;
                    
                    if (moreTabIndex > 0) {
                        
                        BBVATabView *nextTabView = [tabBarViewsArray_ objectAtIndex:(moreTabIndex - 1)];
                        topPosition = CGRectGetMaxY(nextTabView.frame);
                        
                    }
                    
                    CGFloat yPosition = touchViewLocation.y;
                    
                    if ((yPosition > topPosition) && (yPosition < bottomPosition)) {
                        
                        NSUInteger tabNewIndex = moreTabIndex;
                        BBVATabBarTabInformation *tabInformation = nil;
                        
                        for (BBVATabView *tabView in tabBarViewsArray_) {
                            
                            tabInformation = tabView.tabInformation;
                            
                            if ((tabInformation != moreTabInformation_) && (![currentEditableTabsOrderArray_ containsObject:tabInformation])) {
                                
                                tabNewIndex --;
                                
                            }
                            
                        }
                        
                        NSUInteger tabsCount = [currentEditableTabsOrderArray_ count];
                        
                        if (tabNewIndex >= tabsCount) {
                            
                            if (tabsCount > 0) {
                                
                                tabNewIndex = tabsCount - 1;
                                
                            } else {
                                
                                tabNewIndex = 0;
                                
                            }
                            
                        }
                        
                        BBVATabView *tabViewToInsert = [[[BBVATabView allocWithZone:self.zone] init] autorelease];
                        
                        if (tabViewToInsert != nil) {
                            
                            tabViewToInsert.tabInformation = selectedInformation_;
                            
                            [self.view addSubview:tabViewToInsert];
                            
                            if (iPhoneDevice_) {
                                
                                tabViewToInsert.drawText = YES;
                                
                            } else {
                                
                                tabViewToInsert.drawText = NO;
                                
                            }
                            
                            [tabBarViewsArray_ insertObject:tabViewToInsert
                                                    atIndex:moreTabIndex];
                            [currentTabsInTabBarArray_ insertObject:selectedInformation_
                                                            atIndex:moreTabIndex];
                            currentTabsInTabBarCount_++;
                            
                            [self arrangeTabBarTabs];
                            
                            CGRect transformedTabFrame = [self applyAuxiliaryMovingViewTransform:tabViewToInsert.frame];
                            originalAuxiliaryViewTopLeftPoint_ = transformedTabFrame.origin;

                            [currentEditableTabsOrderArray_ removeObject:selectedInformation_];
                            [currentEditableTabsOrderArray_ insertObject:selectedInformation_
                                                                 atIndex:tabNewIndex];
                            
                            if (!moreTabElementsOrdered_) {
                                
                                [currentEditableTabsDisplayedArray_ removeObject:selectedInformation_];
                                [currentEditableTabsDisplayedArray_ insertObject:selectedInformation_
                                                                         atIndex:tabNewIndex];
                                
                                [generalTabViewsArray_ removeObject:selectedView_];
                                [generalTabViewsArray_ insertObject:selectedView_
                                                            atIndex:tabNewIndex];
                                
                                if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
                                    
                                    [UIView animateWithDuration:ANIMATIONS_DURATIONS
                                                          delay:0.0f
                                                        options:(UIViewAnimationCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState)
                                                     animations:^{
                                                         
                                                         [self arrangeGeneralTabs];
                                                         
                                                     }
                                                     completion:nil];
                                    
                                } else {
                                    
                                    [UIView beginAnimations:nil
                                                    context:nil];
                                    
                                    [UIView setAnimationDuration:ANIMATIONS_DURATIONS];
                                    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                                    [UIView setAnimationBeginsFromCurrentState:YES];
                                    
                                    [self arrangeGeneralTabs];
                                    
                                    [UIView commitAnimations];
                                    
                                }
                                
                            }
                            
                            [self returnToRestState];
                            
                            result = YES;

                        }
                        
                    }
                    
                }
                
            }

        }
        
    }
    
    return result;
    
}

/*
 * Finishes a tab bar tab edition. iPhone version just returns to th rest state, but iPad version checks whether the view is too far away from
 * the bar to remove it from the tab bar
 */
- (void)finishTabBarTabEditionAtViewLocation:(CGPoint)touchViewLocation {
    
    if (iPhoneDevice_) {
        
        [self returnToRestState];
        
    } else if (![self finishTabBarTabEditionCheckingTabRemovalFromTabBarAtViewLocation:touchViewLocation]) {
        
        [self returnToRestState];
    }
    
}

/**
 * Finishes a tab bar tab edition when the tab bar is too far away from the tab bar so it must be removed from it. All animations are performed
 *
 * @param touchViewLocation The touch location inside the view
 * @return YES when the tab bar tab is removed, NO otherwise
 * @private
 */
- (BOOL)finishTabBarTabEditionCheckingTabRemovalFromTabBarAtViewLocation:(CGPoint)touchViewLocation {
    
    BOOL result = NO;
    
    CGRect tabBarFrame = tabBarBackgroundView_.frame;
    CGFloat limitRight = CGRectGetMaxX(tabBarFrame) + CGRectGetWidth(tabBarFrame);
    
    if (touchViewLocation.x > limitRight) {
        
        NSUInteger newPosition = ([currentTabsInTabBarArray_ count] - 1);
        
        if ((moreTabInformation_ != nil) && ([currentTabsInTabBarArray_ containsObject:moreTabInformation_])) {
            
            newPosition --;
            
        }
        
        BBVATabBarTabInformation *tabInformation = nil;
        
        for (BBVATabView *tabView in tabBarViewsArray_) {
            
            tabInformation = tabView.tabInformation;
            
            if ((tabInformation != moreTabInformation_) && (![currentEditableTabsOrderArray_ containsObject:tabInformation])) {
                
                newPosition --;
                
            }
            
        }
        
        NSUInteger tabsCount = [currentEditableTabsOrderArray_ count];
        
        if (newPosition >= tabsCount) {
            
            if (tabsCount > 0) {
                
                newPosition = tabsCount - 1;
                
            } else {
                
                newPosition = 0;
                
            }
            
        }
        
        [currentEditableTabsOrderArray_ removeObject:selectedInformation_];
        [currentEditableTabsOrderArray_ insertObject:selectedInformation_
                                             atIndex:newPosition];
        
        if (!moreTabElementsOrdered_) {
            
            NSUInteger viewIndex = [currentEditableTabsDisplayedArray_ indexOfObject:selectedInformation_];
            
            [currentEditableTabsDisplayedArray_ removeObject:selectedInformation_];
            [currentEditableTabsDisplayedArray_ insertObject:selectedInformation_
                                                     atIndex:newPosition];
            
            NSObject *storedObject = [generalTabViewsArray_ objectAtIndex:viewIndex];
            [generalTabViewsArray_ removeObject:storedObject];
            [generalTabViewsArray_ insertObject:storedObject
                                        atIndex:newPosition];
            
        }
        
        [currentTabsInTabBarArray_ removeObject:selectedInformation_];
        [tabBarViewsArray_ removeObject:selectedView_];
        
        currentTabsInTabBarCount_ --;
        
        NSUInteger tabViewVisualIndex = [currentEditableTabsDisplayedArray_ indexOfObject:selectedInformation_];
        
        if (tabViewVisualIndex < [generalTabViewsArray_ count]) {
            
            BBVATabView *generalTabView = [generalTabViewsArray_ objectAtIndex:tabViewVisualIndex];
            CGRect transformedTabFrame = CGRectApplyAffineTransform(generalTabView.frame,
                                                                    CGAffineTransformInvert(auxiliaryMovingView_.transform));
            originalAuxiliaryViewTopLeftPoint_ = transformedTabFrame.origin;
            
        }
        
        if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
            
            [UIView animateWithDuration:ANIMATIONS_DURATIONS
                                  delay:0.0f
                                options:(UIViewAnimationCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState)
                             animations:^{
                                 
                                 [self arrangeTabBarTabs];
                                 
                                 if (!moreTabElementsOrdered_) {
                                     
                                     [self arrangeGeneralTabs];
                                     
                                 }
                                 
                             }
                             completion:nil];
            
        } else {
            
            [UIView beginAnimations:nil
                            context:nil];
            
            [UIView setAnimationDuration:ANIMATIONS_DURATIONS];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            [UIView setAnimationBeginsFromCurrentState:YES];
            
            [self arrangeTabBarTabs];
            
            if (!moreTabElementsOrdered_) {
                
                [self arrangeGeneralTabs];
                
            }
            
            [UIView commitAnimations];
            
        }

        removeAuxiliaryViewFromSuperview_ = YES;
        
        [self returnToRestState];
        
        result = YES;

    }
    
    return result;
    
}

/*
 * Sets the view to a "rest" state where no edition is performed. Any view that must return to its location is animated
 */
- (void)returnToRestState {
    
    if (editingGeneralTab_) {
        
        removeAuxiliaryViewFromSuperview_ = YES;
        
    }
    
    CGRect auxiliaryViewFrame = auxiliaryMovingView_.frame;
    auxiliaryViewFrame.origin.x = originalAuxiliaryViewTopLeftPoint_.x;
    auxiliaryViewFrame.origin.y = originalAuxiliaryViewTopLeftPoint_.y;
    CGAffineTransform transform = selectedView_.transform;
    
    if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
        
        [UIView animateWithDuration:ANIMATIONS_DURATIONS
                              delay:0.0f
                            options:(UIViewAnimationCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState)
                         animations:^{
                             
                             auxiliaryMovingView_.frame = auxiliaryViewFrame;
                             auxiliaryMovingView_.transform = transform;
                             
                         }
                         completion:^(BOOL finished) {
                             
                             [self removeAuxiliaryMovingView];
                             
                         }];
        
    } else {
        
        [UIView beginAnimations:nil
                        context:nil];
        
        [UIView setAnimationDuration:ANIMATIONS_DURATIONS];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(resetStateAnimationDidStop:finished:context:)];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        auxiliaryMovingView_.transform = transform;
        auxiliaryMovingView_.frame = auxiliaryViewFrame;
        
        [UIView commitAnimations];
        
    }
    
    editingGeneralTab_ = NO;
    editingBarTab_ = NO;
    selectedView_ = nil;
    
    [selectedInformation_ release];
    selectedInformation_ = nil;
    
    initialGeneralLocation_ = NSNotFound;
    initialTabLocation_ = NSNotFound;
    originalViewTopLeftPoint_ = CGPointZero;
    originalAuxiliaryViewTopLeftPoint_ = CGPointZero;
    originalTouchPoint_ = CGPointZero;
    
}

/**
 * Removes the auxiliary moving view
 */
- (void)removeAuxiliaryMovingView {
    
    if (removeAuxiliaryViewFromSuperview_) {
        
        [auxiliaryMovingView_ removeFromSuperview];

    }
    
    [auxiliaryMovingView_ release];
    auxiliaryMovingView_ = nil;
    
    removeAuxiliaryViewFromSuperview_ = NO;
    
}

#pragma mark -
#pragma mark Accessing information

/*
 * Returns the tabs array in the new selected order
 */
- (NSArray *)orderedTabsArray {
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    NSMutableArray *pendingTabsToMoreTab = [NSMutableArray array];
    
    NSUInteger defaultOrderTabsCount = [defaultTabsOrderArray_ count];
    NSUInteger currentOrderedTabsCount = [currentEditableTabsOrderArray_ count];
    BBVATabBarTabInformation *defaultOrderTabBarInformation = nil;
    BBVATabBarTabInformation *currentOrderTabBarInformaiton = nil;
    NSUInteger currentOrderArrayIndex = 0;
    
    for (NSUInteger i = 0; i < defaultOrderTabsCount; i++) {
        
        if ([mutableResult count] >= currentTabsInTabBarCount_) {
            
            [mutableResult addObjectsFromArray:pendingTabsToMoreTab];
            [pendingTabsToMoreTab removeAllObjects];
            
        }
        
        defaultOrderTabBarInformation = [defaultTabsOrderArray_ objectAtIndex:i];
        
        if ([currentEditableTabsOrderArray_ containsObject:defaultOrderTabBarInformation]) {
            
            if (currentOrderArrayIndex < currentOrderedTabsCount) {
                
                currentOrderTabBarInformaiton = [currentEditableTabsOrderArray_ objectAtIndex:currentOrderArrayIndex];
                [mutableResult addObject:currentOrderTabBarInformaiton];
                currentOrderArrayIndex++;
                
            }
            
        } else {
            
            if (defaultOrderTabBarInformation.canShowOnTabBar) {
                
                [mutableResult addObject:defaultOrderTabBarInformation];
                
            } else if (i < currentTabsInTabBarCount_) {
                
                [pendingTabsToMoreTab addObject:defaultOrderTabBarInformation];
                
            } else {
                
                [mutableResult addObject:defaultOrderTabBarInformation];
                
            }
            
        }
        
    }
    
    return [NSArray arrayWithArray:mutableResult];
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Calculates the tab width
 */
- (CGFloat)iPhoneTabWidth {
    
    CGFloat result = 0.0f;
    
    if (tabBarBackgroundView_ != nil) {
        
        tabBarBackgroundView_.tabsCount = maximumTabsInTabBarCount_;
        result = [tabBarBackgroundView_ tabWidth];
        
    } else {
        
        CGRect frame = self.view.frame;
        CGFloat width = frame.size.width;
        NSUInteger tabsCount = [tabBarViewsArray_ count];
        
        if (tabsCount > 0) {
            
            result = round(width / (CGFloat)tabsCount);
            
        } else {
            
            result = width;
            
        }
        
    }
    
    return result;
    
}

/*
 * Invoked by framework when the reset state animation comes to an end. The auxiliary moving view is removed
 */
- (void)resetStateAnimationDidStop:(NSString *)animationID
                          finished:(NSNumber *)finished
                           context:(void *)context {
    
    [self removeAuxiliaryMovingView];
    
}

/*
 * Calculates the rectangle where general tabs can be displayed
 */
- (CGRect)generalTabsRectangle {
    
    CGRect result = graphicContainerView_.frame;
    
    return result;
    
}

/*
 * Calculates the equivalent frame once applied the auxiliary moving view transform. The result is equal to applying the
 * transform to the view, so the frame is "centered" in the same point (if no translation is applied)
 */
- (CGRect)applyAuxiliaryMovingViewTransform:(CGRect)frame {
    
    CGRect result = CGRectZero;
    
    CGRect frameToTransform = frame;
    CGFloat halfWidth = round(frame.size.width / 2.0f);
    CGFloat halfHeight = round(frame.size.height / 2.0f);
    frameToTransform.origin.x = -halfWidth;
    frameToTransform.origin.y = -halfHeight;
    
    CGRect transformedFrame = CGRectApplyAffineTransform(frameToTransform, auxiliaryMovingView_.transform);
    
    result = transformedFrame;
    result.origin.x = round(frame.origin.x + halfWidth + transformedFrame.origin.x);
    result.origin.y = round(frame.origin.y + halfHeight + transformedFrame.origin.y);
    
    return result;
    
}

#pragma mark -
#pragma mark Configuration management

/*
 * Sets the arrays needed to relocate the tabs. Arrays objects are BBVATabBarInformation instances
 */
- (void)setDefaultTabsOrderArray:(NSArray *)defaultTabsOrderArray
           currentTabsOrderArray:(NSArray *)currentTabsOrderArray
   currentEditableTabsOrderArray:(NSArray *)currentEditableTabsOrderArray
               tabsInTabBarCount:(NSUInteger)tabsInTabBarCount
        defaultTabsInTabBarCount:(NSUInteger)defaultTabsInTabBarCount
        maximumTabsInTabBarCount:(NSUInteger)maximumTabsInTabBarCount
              moreTabInformation:(BBVATabBarTabInformation *)moreTabInformation
          moreTabElementsOrdered:(BOOL)moreTabElementsOrdered {
    
    if (([defaultTabsOrderArray count] > 0) &&
        ([currentEditableTabsOrderArray count] > 0) &&
        (moreTabInformation != nil)) {
        
        moreTabElementsOrdered_ = moreTabElementsOrdered;
        
        [currentTabsInTabBarArray_ removeAllObjects];
        
        if (moreTabInformation_ != moreTabInformation) {
            
            [moreTabInformation retain];
            [moreTabInformation_ release];
            moreTabInformation_ = moreTabInformation;
            
        }
        
        Class bbvaTabBarTabInformation = [BBVATabBarTabInformation class];
        if (defaultTabsOrderArray_ != defaultTabsOrderArray) {
            
            [defaultTabsOrderArray_ removeAllObjects];
            
            for (NSObject *object in defaultTabsOrderArray) {
                
                if ([object isKindOfClass:bbvaTabBarTabInformation]) {
                    
                    [defaultTabsOrderArray_ addObject:object];
                    
                }
                
            }
            
        }
        
        if (currentTabsOrderArray_ != currentTabsOrderArray) {
            
            [currentTabsOrderArray_ removeAllObjects];
            
            for (NSObject *object in currentTabsOrderArray) {
                
                if ([object isKindOfClass:bbvaTabBarTabInformation]) {
                    
                    [currentTabsOrderArray_ addObject:object];
                    
                }
                
            }
            
        }
        
        if (currentEditableTabsOrderArray_ != currentEditableTabsOrderArray) {
            
            [currentEditableTabsOrderArray_ removeAllObjects];
            
            for (NSObject *object in currentEditableTabsOrderArray) {
                
                if ([object isKindOfClass:bbvaTabBarTabInformation]) {
                    
                    [currentEditableTabsOrderArray_ addObject:object];
                    
                }
                
            }
            
        }
        
        [currentEditableTabsDisplayedArray_ removeAllObjects];
        [currentEditableTabsDisplayedArray_ addObjectsFromArray:currentEditableTabsOrderArray_];
        
        if (moreTabElementsOrdered_) {
            
            [currentEditableTabsDisplayedArray_ sortUsingSelector:@selector(compareForMoreTabLocation:)];
            
        }
        
        currentTabsInTabBarCount_ = tabsInTabBarCount;
        defaultTabsInTabBarCount_ = defaultTabsInTabBarCount;
        maximumTabsInTabBarCount_ = maximumTabsInTabBarCount;
        
        NSUInteger totalTabs = [defaultTabsOrderArray count];
        BOOL needsMoreTabInformation = NO;
        
        if (totalTabs > currentTabsInTabBarCount_) {
            
            needsMoreTabInformation = YES;
            
        }
        
        NSUInteger tabsInTabBar = (needsMoreTabInformation) ? currentTabsInTabBarCount_ : totalTabs;
        BBVATabBarTabInformation *information = nil;
        
        for (NSUInteger i = 0; i < tabsInTabBar; i++) {
            
            information = [currentTabsOrderArray_ objectAtIndex:i];
            
            [currentTabsInTabBarArray_ addObject:information];
            
        }
        
        if (needsMoreTabInformation) {
            
            [currentTabsInTabBarArray_ addObject:moreTabInformation_];
            
        }
        
        tabBarBackgroundView_.tabsCount = maximumTabsInTabBarCount_;
        
        [self populateTabViewsArrays];
        
    }
    
}

/*
 * Resest the tabs order to the default
 */
- (void)resetOrderToDefault {
    
    if ([defaultTabsOrderArray_ count] > 0) {
        
        NSMutableArray *auxArray = [NSMutableArray array];
        
        for (BBVATabBarTabInformation *tabInformation in defaultTabsOrderArray_) {
            
            if ([currentEditableTabsOrderArray_ containsObject:tabInformation]) {
                
                [auxArray addObject:tabInformation];
                
            }
            
        }
        
        [self setDefaultTabsOrderArray:defaultTabsOrderArray_
                 currentTabsOrderArray:defaultTabsOrderArray_
         currentEditableTabsOrderArray:auxArray
                     tabsInTabBarCount:defaultTabsInTabBarCount_
              defaultTabsInTabBarCount:defaultTabsInTabBarCount_
              maximumTabsInTabBarCount:maximumTabsInTabBarCount_
                    moreTabInformation:moreTabInformation_
                moreTabElementsOrdered:moreTabElementsOrdered_];
        
    }
    
}

#pragma mark -
#pragma mark BBVATabBarViewControllerViewDelegate protocol selectors

/**
 * Notifies the delegate that the view frame has changed. The general tab views are relocated
 *
 * @param bbvaTabBarViewControllerView The BBVATabBarViewControllerView instance triggering the event
 */
- (void)tabBarViewControllerViewFrameChanged:(BBVATabBarViewControllerView *)bbvaTabBarViewControllerView {
    
    if (bbvaTabBarViewControllerView == graphicContainerView_) {
        
        [self arrangeGeneralTabs];
        
    }
    
}

#pragma mark -
#pragma mark UIResponder selectors

/**
 * Tells the receiver when one or more fingers touch down in a view or window. The icon where the touch is started is detected
 *
 * @param touches A set of UITouch instances that represent the touches for the starting phase of the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (((!editingBarTab_) && (!editingGeneralTab_)) && (auxiliaryMovingView_ == nil)) {
        
        if ([touches count] == 1) {
            
            UIView *view = self.view;
            
            UITouch *touch = [touches anyObject];
            
            CGPoint viewLocation = [touch locationInView:view];
            
            editingGeneralTab_ = NO;
            editingBarTab_ = NO;
            [selectedView_ release];
            selectedView_ = nil;
            [selectedInformation_ release];
            selectedInformation_ = nil;
            initialGeneralLocation_ = NSNotFound;
            initialTabLocation_ = NSNotFound;
            removeAuxiliaryViewFromSuperview_ = NO;
            
            CGRect viewFrame;
            BBVATabView *tabView = nil;
            NSObject *object = nil;
            Class bbvaTabViewClass = [BBVATabView class];
            NSUInteger maxCount = [generalTabViewsArray_ count];
            NSUInteger generalInformationCounts = [currentEditableTabsOrderArray_ count];
            maxCount = (maxCount < generalInformationCounts) ? maxCount : generalInformationCounts;
            
            for (NSUInteger i = 0; i < maxCount; i ++) {
                
                object = [generalTabViewsArray_ objectAtIndex:i];
                
                if ([object isKindOfClass:bbvaTabViewClass]) {
                    
                    tabView = (BBVATabView *)object;
                    viewFrame = tabView.frame;
                    
                    if (CGRectContainsPoint(viewFrame, viewLocation)) {
                        
                        editingGeneralTab_ = YES;
                        selectedView_ = [tabView retain];
                        selectedInformation_ = [[currentEditableTabsDisplayedArray_ objectAtIndex:i] retain];
                        initialGeneralLocation_ = [currentEditableTabsOrderArray_ indexOfObject:selectedInformation_];
                        initialGeneralVisualLocation_ = i;
                        initialTabLocation_ = [currentTabsInTabBarArray_ indexOfObject:selectedInformation_];
                        break;
                        
                    }
                    
                }
                
            }
            
            if (!editingGeneralTab_) {
                
                NSUInteger tabViewsCount = [tabBarViewsArray_ count];
                NSUInteger tabInformationCount = [currentTabsInTabBarArray_ count];
                maxCount = (tabViewsCount < tabInformationCount) ? tabViewsCount : tabInformationCount;
                
                for (NSUInteger i = 0; i < maxCount; i ++) {
                    
                    object = [tabBarViewsArray_ objectAtIndex:i];
                    
                    if ([object isKindOfClass:bbvaTabViewClass]) {
                        
                        tabView = (BBVATabView *)object;
                        
                        if (!tabView.tabDisabled) {
                            
                            viewFrame = tabView.frame;
                            
                            if (CGRectContainsPoint(viewFrame, viewLocation)) {
                                
                                editingBarTab_ = YES;
                                selectedView_ = [tabView retain];
                                selectedInformation_ = [[currentTabsInTabBarArray_ objectAtIndex:i] retain];
                                initialGeneralLocation_ = [currentEditableTabsOrderArray_ indexOfObject:selectedInformation_];
                                initialGeneralVisualLocation_ = [currentEditableTabsDisplayedArray_ indexOfObject:selectedInformation_];
                                initialTabLocation_ = i;
                                break;
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
            if ((editingBarTab_) || (editingGeneralTab_)) {
                
                originalTouchPoint_ = viewLocation;
                originalViewTopLeftPoint_ = selectedView_.frame.origin;
                touchBeingFollowed_ = touch;
                
                [self removeAuxiliaryMovingView];
                
                if (editingBarTab_) {
                    
                    auxiliaryMovingView_ = [selectedView_ retain];

                } else {
                    
                    CGRect viewFrame = selectedView_.frame;
                    auxiliaryMovingView_ = [[BBVATabView allocWithZone:self.zone] initWithFrame:viewFrame];
                    [self.view addSubview:auxiliaryMovingView_];
                    auxiliaryMovingView_.tabInformation = selectedInformation_;
                    auxiliaryMovingView_.tabDisabled = (!iPhoneDevice_);
                    auxiliaryMovingView_.tabSelected = NO;

                    CGAffineTransform transform = auxiliaryMovingView_.transform;
                    transform = CGAffineTransformScale(transform, 1.5f, 1.5f);
                    transform = CGAffineTransformTranslate(transform, 0.0f, -round(viewFrame.size.height / 4.0f));
                    
                    if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
                        
                        [UIView animateWithDuration:ANIMATIONS_DURATIONS
                                              delay:0.0f
                                            options:(UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState)
                                         animations:^{
                                             
                                             auxiliaryMovingView_.transform = transform;
                                             
                                         }
                                         completion:nil];
                        
                    } else {
                        
                        [UIView beginAnimations:nil
                                        context:nil];
                        [UIView setAnimationDuration:ANIMATIONS_DURATIONS];
                        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                        [UIView setAnimationBeginsFromCurrentState:YES];
                        
                        auxiliaryMovingView_.transform = transform;
                        
                        [UIView commitAnimations];
                        
                    }
                    
                }
                
                originalAuxiliaryViewTopLeftPoint_ = auxiliaryMovingView_.frame.origin;

                if (auxiliaryMovingView_ != nil) {
                    
                    [view bringSubviewToFront:auxiliaryMovingView_];
                    
                }
                
            }
            
        } else {
            
            [self returnToRestState];
            
        }
        
    }
    
}

/**
 * Tells the receiver when one or more fingers associated with an event move within a view or window. Auxiliary view location is updated
 *
 * @param touches A set of UITouch instances that represent the touches that are moving during the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if ((editingBarTab_) || (editingGeneralTab_)) {
        
        if ([touches count] == 1) {
            
            UIView *view = self.view;
            
            UITouch *touch = [touches anyObject];
            
            if (touch == touchBeingFollowed_) {
                
                CGPoint viewLocation = [touch locationInView:view];
                
                if (editingGeneralTab_) {
                    
                    CGFloat xDelta = viewLocation.x - originalTouchPoint_.x;
                    CGFloat yDelta = viewLocation.y - originalTouchPoint_.y;
                    
                    CGRect auxiliaryViewFrame = auxiliaryMovingView_.frame;
                    auxiliaryViewFrame.origin.x = originalAuxiliaryViewTopLeftPoint_.x + xDelta;
                    auxiliaryViewFrame.origin.y = originalAuxiliaryViewTopLeftPoint_.y + yDelta;
                    auxiliaryMovingView_.frame = auxiliaryViewFrame;
                    
                } else if (editingBarTab_) {
                    
                    CGRect auxiliaryViewFrame = auxiliaryMovingView_.frame;

                    if (iPhoneDevice_) {
                        
                        CGFloat xDelta = viewLocation.x - originalTouchPoint_.x;
                        auxiliaryViewFrame.origin.x = originalAuxiliaryViewTopLeftPoint_.x + xDelta;
                        
                    } else {
                        
                        CGFloat xDelta = viewLocation.x - originalTouchPoint_.x;
                        CGFloat yDelta = viewLocation.y - originalTouchPoint_.y;
                        auxiliaryViewFrame.origin.x = originalAuxiliaryViewTopLeftPoint_.x + xDelta;
                        auxiliaryViewFrame.origin.y = originalAuxiliaryViewTopLeftPoint_.y + yDelta;
                        
                    }
                    
                    auxiliaryMovingView_.frame = auxiliaryViewFrame;
                    
                    if (iPhoneDevice_) {
                        
                        CGFloat tabBarTop = CGRectGetMinY(tabBarBackgroundView_.frame);
                        
                        if (viewLocation.y >= tabBarTop) {
                            
                            [self checkTabBarInvadingOtherEditableTabBarForPoint:viewLocation];
                            
                        }
                        
                    } else {
                        
                        CGFloat tabBarRight = CGRectGetMaxX(tabBarBackgroundView_.frame);
                        
                        if (viewLocation.x <= tabBarRight) {
                            
                            [self checkTabBarInvadingOtherEditableTabBarForPoint:viewLocation];

                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}

/**
 * Tells the receiver when one or more fingers are raised from a view or window. The last operation for a general image editing is checked. In case the
 * editing was between tab bars, the view is returned to its rest state
 *
 * @param touches A set of UITouch instances that represent the touches for the ending phase of the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if ((editingBarTab_) || (editingGeneralTab_)) {
        
        if ([touches count] == 1) {
            
            UIView *view = self.view;
            
            UITouch *touch = [touches anyObject];
            
            if (touch == touchBeingFollowed_) {
                
                CGPoint viewLocation = [touch locationInView:view];
                
                if (editingGeneralTab_) {
                    
                    [self finishGeneralTabEditionAtViewLocation:viewLocation];
                    
                } else if (editingBarTab_) {
                    
                    [self finishTabBarTabEditionAtViewLocation:viewLocation];
                    
                }
                
                touchBeingFollowed_ = nil;
                
            }
            
        }
        
    }
    
}

/**
 * Sent to the receiver when a system event (such as a low-memory warning) cancels a touch event. The view is set to its original state
 * 
 * @param touches A set of UITouch instances that represent the touches for the ending phase of the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if ((editingBarTab_) || (editingGeneralTab_)) {
        
        if ([touches count] == 1) {
            
            UITouch *touch = [touches anyObject];
            
            if (touch == touchBeingFollowed_) {
                
                [self returnToRestState];
                touchBeingFollowed_ = nil;
                
            }
            
        }
        
    }
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Return the navigation item used to represent the view controller. Sets the view title and navigation buttons
 *
 * @return The navigation item used to represent the view controller
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    result.title = relocateTabNavigationTitle_;
    
    NSZone *zone = self.zone;
    
    if (self.navigationController == nil) {
        
        if (doneButton_ == nil) {
            
            doneButton_ = [[UIBarButtonItem allocWithZone:zone] initWithTitle:relocateTabDoneText_
                                                                        style:UIBarButtonItemStyleBordered
                                                                       target:self
                                                                       action:@selector(doneButtonTapped)];
            
        }
        
        result.leftBarButtonItem = doneButton_;
        
    } else {
        
        result.leftBarButtonItem = nil;
        
    }
    
    if (resetButton_ == nil) {
        
        resetButton_ = [[UIBarButtonItem allocWithZone:zone] initWithTitle:relocateTabResetText_
                                                                     style:UIBarButtonItemStyleBordered
                                                                    target:self
                                                                    action:@selector(resetButtonTapped)];
        
    }
    
    result.rightBarButtonItem = resetButton_;
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the BBVA logo image
 *
 * @param logoImage The BBVA logo image to set
 */
- (void)setLogoImage:(UIImage *)logoImage {
    
    if (logoImage != logoImage_) {
        
        [logoImage retain];
        [logoImage_ release];
        logoImage_ = logoImage;
        
        tabBarBackgroundView_.logoImage = logoImage_;
        
    }
    
}

/*
 * Sets the navigation bar class. It is used when the navigation bar is created, not inmediatelly
 *
 * @param navigationBarClass The navigation bar class to set
 */
- (void)setNavigationBarClass:(Class)navigationBarClass {
    
    if (navigationBarClass != navigationBarClass_) {
        
        if ((navigationBarClass == nil) || ([navigationBarClass isSubclassOfClass:[UINavigationBar class]])) {
            
            [navigationBarClass retain];
            [navigationBarClass_ release];
            navigationBarClass_ = navigationBarClass;
            
        }
        
    }
    
}

/*
 * Sets the new navigation bar color
 *
 * @param navigationBarColor The new navigation bar color to set
 */
- (void)setNavigationBarColor:(UIColor *)navigationBarColor {
    
    if ((navigationBarColor != nil) && (navigationBarColor != navigationBarColor_)) {
        
        [navigationBarColor retain];
        [navigationBarColor_ release];
        navigationBarColor_ = navigationBarColor;
        
        topBar_.tintColor = navigationBarColor_;
        
    }
    
}

/*
 * Sets the view background color
 *
 * @param viewBackgroundColor The view background color to set
 */
- (void)setViewBackgroundColor:(UIColor *)viewBackgroundColor {
    
    if (viewBackgroundColor != viewBackgroundColor_) {
        
        if ((viewBackgroundColor == nil) || ([viewBackgroundColor isEqual:[UIColor clearColor]])) {
            
            viewBackgroundColor = LOCAL_DARK_BLUE_COLOR;
            
        }
        
        [viewBackgroundColor retain];
        [viewBackgroundColor_ release];
        viewBackgroundColor_ = viewBackgroundColor;
        
        graphicContainerView_.backgroundColor = viewBackgroundColor_;
        
    }
    
}

/*
 * Sets the title text color
 *
 * @param titleTextColor The new title text color to set
 */
- (void)setTitleTextColor:(UIColor *)titleTextColor {
    
    if (titleTextColor != titleTextColor_) {
        
        if ((titleTextColor == nil) || ([titleTextColor isEqual:[UIColor clearColor]])) {
            
            titleTextColor = LOCAL_LIGHT_BLUE_COLOR;
            
        }
        
        [titleTextColor retain];
        [titleTextColor_ release];
        titleTextColor_ = titleTextColor;
        
        titleLabel_.textColor = titleTextColor;
        titleLabel_.highlightedTextColor = titleTextColor;
        
    }
    
}

/*
 * Sets the relocate header background image
 *
 * @param relocateHeaderBackgroundImage The new realocate header background image
 */
- (void)setRelocateHeaderBackgroundImage:(UIImage *)relocateHeaderBackgroundImage {
    
    if (relocateHeaderBackgroundImage != relocateHeaderBackgroundImage_) {
        
        [relocateHeaderBackgroundImage retain];
        [relocateHeaderBackgroundImage_ release];
        relocateHeaderBackgroundImage_ = relocateHeaderBackgroundImage;
        
        titleBackgroundImageView_.image = relocateHeaderBackgroundImage_;
        
    }
    
}

/*
 * Set the new relocate tab navigation bar title
 *
 * @param relocateTabNavigationTitle The new relocate tab navigation bar title to set
 */
- (void)setRelocateTabNavigationTitle:(NSString *)relocateTabNavigationTitle {
    
    if (relocateTabNavigationTitle != relocateTabNavigationTitle_) {
        
        [relocateTabNavigationTitle retain];
        [relocateTabNavigationTitle_ release];
        relocateTabNavigationTitle_ = relocateTabNavigationTitle;
        
        self.navigationItem.title = relocateTabNavigationTitle_;
        
    }
    
}

/*
 * Set the new relocate tab header title
 *
 * @param relocateTabHeaderTitle The new relocate tab header title to set
 */
- (void)setRelocateTabHeaderTitle:(NSString *)relocateTabHeaderTitle {
    
    if (relocateTabHeaderTitle != relocateTabHeaderTitle_) {
        
        [relocateTabHeaderTitle retain];
        [relocateTabHeaderTitle_ release];
        relocateTabHeaderTitle_ = relocateTabHeaderTitle;
        
        titleLabel_.text = relocateTabHeaderTitle_;
        
    }
    
}

/*
 * Set the new relocate tab reset text
 *
 * @param relocateTabResetText The new relocate tab reset text to set
 */
- (void)setRelocateTabResetText:(NSString *)relocateTabResetText {
    
    if (relocateTabResetText != relocateTabResetText_) {
        
        [relocateTabResetText retain];
        [relocateTabResetText_ release];
        relocateTabResetText_ = relocateTabResetText;
        
        resetButton_.title = relocateTabResetText_;
        
    }
    
}

/*
 * Set the new relocate tab done text
 *
 * @param relocateTabDoneText The new relocate tab done text to set
 */
- (void)setRelocateTabDoneText:(NSString *)relocateTabDoneText {
    
    if (relocateTabDoneText != relocateTabDoneText_) {
        
        [relocateTabDoneText retain];
        [relocateTabDoneText_ release];
        relocateTabDoneText_ = relocateTabDoneText;
        
        doneButton_.title = relocateTabDoneText_;
        
    }
    
}

@end
