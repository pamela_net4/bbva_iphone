/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BBVATabBarContainerView.h"
#import <QuartzCore/QuartzCore.h>
#import "BBVATabBarDrawingUtils.h"


/**
 * Defines the iPad additional width with respect to the tab bar width, measured in points
 */
#define I_PAD_ADDITIONAL_WIDTH                                  25.0f

/**
 * Defines the iPad collapse view and button width measured in points
 */
#define I_PAD_COLLAPSE_VIEW_WIDTH                               34.0f

/**
 * Defines the iPad collapse view and button height measured in points
 */
#define I_PAD_COLLAPSE_VIEW_HEIGHT                              88.0f


#pragma mark -

/**
 * BBVATabBarContainerView private extension
 */
@interface BBVATabBarContainerView()

/**
 * Initializes the tab bar container view creating the graphic elements
 *
 * @private
 */
- (void)initializeTabBarContainerView;

/**
 * Invoked by framework when user taps on the collapse button. The delegate is notified
 *
 * @private
 */
- (void)collapseButtonTapped;

/**
 * Set the correct icon for the collapse button
 *
 * @private
 */
- (void)updateCollapseButtonImage;

@end


#pragma mark -

@implementation BBVATabBarContainerView

#pragma mark -
#pragma mark properties

@dynamic tabsList;
@synthesize rightArrowImage = rightArrowImage_;
@synthesize leftArrowImage = leftArrowImage_;
@dynamic informationText;
@dynamic logoImage;
@dynamic selectedTabBackgroundColor;
@synthesize tabBarOnRightHandSide = tabBarOnRightHandSide_;
@synthesize tabBarCollapsed = tabBarCollapsed_;
@synthesize displayCollapseButtonFlag = displayCollapseButtonFlag_;
@synthesize bbvaTabBarcontainerViewDelegate = bbvaTabBarcontainerViewDelegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [tabBarView_ release];
    tabBarView_ = nil;
    
    [collapseButtonView_ release];
    collapseButtonView_ = nil;
    
    [collapseButton_ release];
    collapseButton_ = nil;
    
    [rightArrowImage_ release];
    rightArrowImage_ = nil;
    
    [leftArrowImage_ release];
    leftArrowImage_ = nil;
    
    bbvaTabBarcontainerViewDelegate_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. View is initialized
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initializeTabBarContainerView];
    
}

/**
 * Superclass designated initializer. Initializes the BBVATabBarContainerView instance by creating the graphic elements
 *
 * @param frame View initial frame
 */
- (id)initWithFrame:(CGRect)frame {
    
    if ((self = [super initWithFrame:frame])) {
        
        [self initializeTabBarContainerView];
        
    }
    
    return self;
    
}

/*
 * Initializes the tab bar container view creating the graphic elements
 */
- (void)initializeTabBarContainerView {
    
    iPhoneDevice_ = YES;
    
#ifdef UI_USER_INTERFACE_IDIOM
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        iPhoneDevice_ = NO;
        
    }
    
#endif
    
    [super setBackgroundColor:[UIColor clearColor]];
    
    CGRect frame = self.frame;
    CGRect auxFrame = frame;
    auxFrame.origin.x = 0.0f;
    auxFrame.origin.y = 0.0f;
    
    NSZone *zone = self.zone;
    
    if (tabBarView_ == nil) {
        
        tabBarView_ = [[BBVATabBarView allocWithZone:zone] initWithFrame:auxFrame];
        tabBarView_.delegate = bbvaTabBarcontainerViewDelegate_;
        [self addSubview:tabBarView_];

        auxFrame = tabBarView_.frame;
        frame.size.height = auxFrame.size.height;

        if (iPhoneDevice_) {
            
            frame.size.width = auxFrame.size.width;
            
        } else {
            
            frame.size.width = auxFrame.size.width + I_PAD_ADDITIONAL_WIDTH;
            tabBarView_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
            
        }

        self.frame = frame;

    }
    
    if (!iPhoneDevice_) {
        
        auxFrame = frame;
        auxFrame.size.width = I_PAD_COLLAPSE_VIEW_WIDTH;
        auxFrame.size.height = I_PAD_COLLAPSE_VIEW_HEIGHT;
        auxFrame.origin.x = CGRectGetMaxX(frame) - I_PAD_COLLAPSE_VIEW_WIDTH;
        auxFrame.origin.y = round((CGRectGetMaxY(frame) - I_PAD_COLLAPSE_VIEW_HEIGHT) / 2.0f);

        if (collapseButtonView_ == nil) {
            
            collapseButtonView_ = [[UIView allocWithZone:zone] initWithFrame:auxFrame];
            [self addSubview:collapseButtonView_];
            [self bringSubviewToFront:tabBarView_];
            collapseButtonView_.layer.cornerRadius = 10.0f;
            collapseButtonView_.backgroundColor = I_PAD_TAB_BACKGROUND_COLOR;
            collapseButtonView_.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
            
        }
        
        if (collapseButton_ == nil) {
            
            auxFrame.origin.x = 0.0f;
            auxFrame.origin.y = 0.0f;
            
            collapseButton_ = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
            collapseButton_.frame = auxFrame;
            [collapseButtonView_ addSubview:collapseButton_];
            [collapseButton_ addTarget:self
                                action:@selector(collapseButtonTapped)
                      forControlEvents:UIControlEventTouchUpInside];
            
        }
        
    }
	
	tabBarOnRightHandSide_ = NO;
    tabBarCollapsed_ = NO;
    displayCollapseButtonFlag_ = NO;
    collapseButtonView_.hidden = YES;

}

#pragma mark -
#pragma mark User interaction

/*
 * Invoked by framework when user taps on the collapse button. The delegate is notified
 */
- (void)collapseButtonTapped {
    
    if ([(NSObject *)bbvaTabBarcontainerViewDelegate_ respondsToSelector:@selector(bbvaTabBarContainerViewCollapseButtonTapped:)]) {
        
        [bbvaTabBarcontainerViewDelegate_ bbvaTabBarContainerViewCollapseButtonTapped:self];
        
    }
    
}

#pragma mark -
#pragma mark Information access

/*
 * Returns the iPhone tab bar container view height
 */
+ (CGFloat)iPhoneTabBarContainerHeight {
    
    return [BBVATabBarView iPhoneTabBarHeight];
    
}

/*
 * Returns the iPad tab bar container view width
 */
+ (CGFloat)iPadTabBarContainerWidth {
    
    return [BBVATabBarView iPadTabBarWidth] + I_PAD_ADDITIONAL_WIDTH;
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Set the correct icon for the collapse button
 */
- (void)updateCollapseButtonImage {
    
    if (!iPhoneDevice_) {
        
        if (((tabBarOnRightHandSide_) && (tabBarCollapsed_)) ||
            ((!tabBarOnRightHandSide_) && (!tabBarCollapsed_))) {
            
            UIImage *showButtonImage = leftArrowImage_;
            [collapseButton_ setImage:showButtonImage
                             forState:UIControlStateNormal];
            
        } else {
            
            UIImage *showButtonImage = rightArrowImage_;
            [collapseButton_ setImage:showButtonImage
                             forState:UIControlStateNormal];
            
        }

    }
    
}

#pragma mark -
#pragma mark BBVATabBarView forwarded selectors

/*
 * Selects a given view controller, if found among the tab information
 */
- (BBVATabBarTabInformation *)selectViewController:(UIViewController *)viewController {
    
    return [tabBarView_ selectViewController:viewController];
    
}

/*
 * Returns the tab bar index for a given view controller
 */
- (NSUInteger)tabBarIndexForViewController:(UIViewController *)viewController {
    
    return [tabBarView_ tabBarIndexForViewController:viewController];
    
}

/*
 * Selects the given tab
 */
- (void)selectTab:(BBVATabBarTabInformation *)tab {
    
    [tabBarView_ selectTab:tab];
    
}

#pragma mark -
#pragma mark UIView selectors

/**
 * Returns the farthest descendant of the receiver in the view hierarchy (including itself) that contains a specified point. In iPad version, when
 * the view is tapped and not the child views, nil is returned so this view is transparent to touches
 *
 * @param point A point specified in the receiver’s local coordinate system (bounds)
 * @param event The event that warranted a call to this method
 * @return The view object that is the farthest descendant of the current view and contains point
 */
- (UIView *)hitTest:(CGPoint)point
          withEvent:(UIEvent *)event {
    
    UIView *result = [super hitTest:point
                          withEvent:event];
    
    if ((!iPhoneDevice_) && (result == self)) {
        
        result = nil;
        
    }

    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the tab bar information array
 *
 * @return The tab bar information array
 */
- (NSArray *)tabsList {
    
    return tabBarView_.tabsList;
    
}

/*
 * Sets the new tab bar information array
 *
 * @param tabsList The new tab bar information array
 */
- (void)setTabsList:(NSArray *)tabsList {
    
    tabBarView_.tabsList = tabsList;
    
}

/*
 * Sets the new right arrow image
 *
 * @param rightArrowImage The new right arrow image
 */
- (void)setRightArrowImage:(UIImage *)rightArrowImage {
    
    if (!iPhoneDevice_) {
        
        if (rightArrowImage != rightArrowImage_) {
            
            [rightArrowImage retain];
            [rightArrowImage_ release];
            rightArrowImage_ = rightArrowImage;
            
            [self updateCollapseButtonImage];
            
        }
        
    }
    
}

/*
 * Sets the new left arrow image
 *
 * @param leftArrowImage The new left arrow image
 */
- (void)setLeftArrowImage:(UIImage *)leftArrowImage {
    
    if (!iPhoneDevice_) {
        
        if (leftArrowImage != leftArrowImage_) {
            
            [leftArrowImage retain];
            [leftArrowImage_ release];
            leftArrowImage_ = leftArrowImage;
            
            [self updateCollapseButtonImage];
            
        }
        
    }
    
}

/*
 * Returns the tab bar information text
 *
 * @return The tab bar information text
 */
- (NSString *)informationText {
    
    return tabBarView_.informationText;
    
}

/*
 * Sets the new tab bar information text
 *
 * @return The tab bar information text
 */
- (void)setInformationText:(NSString *)informationText {
    
    tabBarView_.informationText = informationText;
    
}

/*
 * Returns the tab bar logo image
 *
 * @return The tab bar logo image
 */
- (UIImage *)logoImage {
    
    return tabBarView_.logoImage;
}

/*
 * Sets the new tab bar logo image
 *
 * @param logoImage The new tab bar logo image
 */
- (void)setLogoImage:(UIImage *)logoImage {
    
    tabBarView_.logoImage = logoImage;
    
}

/*
 * Returns the selected tab background color
 *
 * @return The selected tab background color
 */
- (UIColor *)selectedTabBackgroundColor {
    
    return tabBarView_.selectedTabBackgroundColor;
    
}

/*
 * Sets the selected tab background color
 *
 * @param selectedTabBackgroundColor The new selected tab background color to set
 */
- (void)setSelectedTabBackgroundColor:(UIColor *)selectedTabBackgroundColor {
    
    tabBarView_.selectedTabBackgroundColor = selectedTabBackgroundColor;
    
}

/*
 * Sets the new delegate. Tab bar view is set the same delegate
 *
 * @param bbvaTabBarContainerViewDelegate The new delegate to set
 */
- (void)bbvaTabBarcontainerViewDelegate:(id<BBVATabBarContainerViewDelegate>)bbvaTabBarcontainerViewDelegate {
    
    bbvaTabBarcontainerViewDelegate_ = bbvaTabBarcontainerViewDelegate;
    tabBarView_.delegate = bbvaTabBarcontainerViewDelegate;
    
}

/*
 * Set the new tab bar on the right side flag and modifies the view layout
 *
 * @param tabBarOnRightHandSide The new tab bar on the right side flag
 */
- (void)setTabBarOnRightHandSide:(BOOL)tabBarOnRightHandSide {
    
    if (!iPhoneDevice_) {
        
        if (tabBarOnRightHandSide_ != tabBarOnRightHandSide) {
            
            tabBarOnRightHandSide_ = tabBarOnRightHandSide;
            
            CGRect frame = self.frame;
            CGRect mainTabViewFrame = tabBarView_.frame;
            CGRect showTabButtonFrame = collapseButtonView_.frame;
            
            if (tabBarOnRightHandSide_) {
                
                mainTabViewFrame.origin.x = frame.size.width - mainTabViewFrame.size.width;
                showTabButtonFrame.origin.x = 0.0f;
                
            } else {
                
                mainTabViewFrame.origin.x = 0.0f;
                showTabButtonFrame.origin.x = frame.size.width - showTabButtonFrame.size.width;
                
            }
            
            tabBarView_.frame = mainTabViewFrame;
            collapseButtonView_.frame = showTabButtonFrame;
            
            [self updateCollapseButtonImage];
            
        }
        
    }
    
}

/*
 * Sets the new tab bar collapsed flag. The collapse/expand button image is updated
 *
 * @param tabBarCollapsed The new tab bar collapsed flag
 */
- (void)setTabBarCollapsed:(BOOL)tabBarCollapsed {
    
    if (!iPhoneDevice_) {
        
        if (tabBarCollapsed_ != tabBarCollapsed) {
            
            tabBarCollapsed_ = tabBarCollapsed;
            
            [self updateCollapseButtonImage];
            
        }
        
    }
    
}

/*
 * Sets the new display collapsed/expand button flag and updates its state
 *
 * @param displayCollapseButtonFlag The new display collapsed/expand button flag
 */
- (void)setDisplayCollapseButtonFlag:(BOOL)displayCollapseButtonFlag {
    
    if (!iPhoneDevice_) {
        
        if (displayCollapseButtonFlag_ != displayCollapseButtonFlag) {
            
            displayCollapseButtonFlag_ = displayCollapseButtonFlag;
            
            if (displayCollapseButtonFlag_) {
                
                collapseButtonView_.hidden = NO;
                
            } else {
                
                collapseButtonView_.hidden = YES;
                
            }
            
        }
        
    }
    
}

/**
 * Sets the new delegate and forwards it to the tab bar
 *
 * @param bbvaTabBarcontainerViewDelegate The new delegate to set
 */
- (void)setBbvaTabBarcontainerViewDelegate:(id<BBVATabBarContainerViewDelegate>)bbvaTabBarcontainerViewDelegate {
    
    bbvaTabBarcontainerViewDelegate_ = bbvaTabBarcontainerViewDelegate;
    
    tabBarView_.delegate = bbvaTabBarcontainerViewDelegate_;
    
}

@end
