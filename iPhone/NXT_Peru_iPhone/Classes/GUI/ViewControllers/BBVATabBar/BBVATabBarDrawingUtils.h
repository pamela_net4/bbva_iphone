/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>


/**
 * Defines the iPhone tab view height measured in points
 */
#define BBVA_I_PHONE_TAB_BAR_VIEW_HEIGHT                                        56.0f

/**
 * Defines the iPad tab view width measured in points
 */
#define BBVA_I_PAD_TAB_BAR_VIEW_WIDTH                                           96.0f

/**
 * Defines the iPad tabs height measured in points
 */
#define BBVA_I_PAD_TABS_HEIGHT                                                  86.0f

/**
 * Defines the iPad tab background rounded corners radius measured in points
 */
#define BBVA_I_PAD_TAB_ROUNDED_CORNERS_RADIUS                                   5.0f

/**
 * iPad tab background color
 */
#define I_PAD_TAB_BACKGROUND_COLOR                                              [UIColor colorWithRed:0.0f green:0.2706f blue:0.5882f alpha:1.0f]


//Forward declarations
@class BBVATabBarTabInformation;


/**
 * Utility class to draw all the graphic elements associated to the tabs. Only class selectors are exported
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BBVATabBarDrawingUtils : NSObject

/**
 * Creates the iPhone tab blue gradient (from top to bottom). It assumes the gradient is NULL when invoked, so no check is performed
 */
+ (void)createiPhoneViewGradient;

/**
 * Draws the iPhone gradient background in the given context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 *
 * @param context The context to draw to
 * @param frame The frame to draw the gradient to
 * @param iPhoneTab YES when the tab is an iPhone tab, NO when it is an iPad tab
 */
+ (void)drawBackgroundGradientIntoContext:(CGContextRef)context
                              withinFrame:(CGRect)frame
                         foriPhoneTabType:(BOOL)iPhoneTab;

/**
 * Draws the iPhone tab separator at the given position
 *
 * @param context The context to draw to
 * @param fromMin The separator minimum position (top for iPhone, left for iPad)
 * @param toMax The separator maximum position (bottom for iPhone, right for iPad)
 * @param position The separator position (horizontal for iPhone, vertical for iPad)
 * @param iPhoneTab YES when the tab is an iPhone tab, NO when it is an iPad tab
 */
+ (void)drawTabSeparatorIntoContext:(CGContextRef)context
                            fromMin:(CGFloat)fromMin
                              toMax:(CGFloat)toMax
                         atPosition:(CGFloat)position
                   foriPhoneTabType:(BOOL)iPhoneTab;

/**
 * Draws a selected tab element (not including the selected background) in the given context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 *
 * @param tabElement The tab element to draw
 * @param context The context to draw to
 * @param tabElementFrame The tab element frame to draw in
 * @param iPhoneTab YES when the tab is an iPhone tab, NO when it is an iPad tab
 * @param drawText YES when the tab text must be displayed, NO otherwise
 */
+ (void)drawSelectedTabElement:(BBVATabBarTabInformation *)tabElement
                   intoContext:(CGContextRef)context
                   withinFrame:(CGRect)tabElementFrame
              foriPhoneTabType:(BOOL)iPhoneTab
                      drawText:(BOOL)drawText;

/**
 * Draws an unselected tab element in the given context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 *
 * @param tabElement The tab element to draw
 * @param context The context to draw to
 * @param tabElementFrame The tab element frame to draw in
 * @param iPhoneTab YES when the tab is an iPhone tab, NO when it is an iPad tab
 * @param drawText YES when the tab text must be displayed, NO otherwise
 */
+ (void)drawUnselectedTabElement:(BBVATabBarTabInformation *)tabElement
                     intoContext:(CGContextRef)context
                     withinFrame:(CGRect)tabElementFrame
                foriPhoneTabType:(BOOL)iPhoneTab
                        drawText:(BOOL)drawText;

/**
 * Draws a disabled tab element in the given context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 *
 * @param tabElement The tab element to draw
 * @param context The context to draw to
 * @param tabElementFrame The tab element frame to draw in
 * @param iPhoneTab YES when the tab is an iPhone tab, NO when it is an iPad tab
 * @param drawText YES when the tab text must be displayed, NO otherwise
 */
+ (void)drawDisabledTabElement:(BBVATabBarTabInformation *)tabElement
                   intoContext:(CGContextRef)context
                   withinFrame:(CGRect)tabElementFrame
              foriPhoneTabType:(BOOL)iPhoneTab
                      drawText:(BOOL)drawText;

/**
 * Draws an image as the tab element image into the provided context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 *
 * @param tabImage The tab image to draw
 * @param context The context to draw to
 * @param tabElementFrame The tab element frame to draw in
 * @param iPhoneTab YES when the tab is an iPhone tab, NO when it is an iPad tab
 */
+ (void)drawTabImage:(UIImage *)tabImage
         intoContext:(CGContextRef)context
         withinFrame:(CGRect)tabElementFrame
    foriPhoneTabType:(BOOL)iPhoneTab;

/**
 * Draws the element text into the provided context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 *
 * @param text The element text to draw
 * @param context The context to draw to
 * @param tabElementFrame The tab element frame to draw in
 * @param textColor The element text color
 * @param iPhoneTab YES when the tab is an iPhone tab, NO when it is an iPad tab
 */
+ (void)drawTabText:(NSString *)text
        intoContext:(CGContextRef)context
        withinFrame:(CGRect)tabElementFrame
              color:(UIColor *)textColor
   foriPhoneTabType:(BOOL)iPhoneTab;

/**
 * Draws the selected element background in the given context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 *
 * @param context The context to draw to
 * @param tabElementFrame The tab element frame to draw in
 * @param backgroundColor The selected tab background color
 * @param iPhoneTab YES when the tab is an iPhone tab, NO when it is an iPad tab
 */
+ (void)drawSelectedTabBackgroundIntoContext:(CGContextRef)context
                                 withinFrame:(CGRect)tabElementFrame
                             backgroundColor:(UIColor *)backgroundColor
                            foriPhoneTabType:(BOOL)iPhoneTab;

@end
