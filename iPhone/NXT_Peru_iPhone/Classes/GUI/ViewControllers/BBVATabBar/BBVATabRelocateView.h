/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


//Forward declarations
@class BBVATabBarTabInformation;
@class BBVATabView;


/**
 * View used to relocate tabs in the tab bar. Tabs are relocated by dragging and dropping on the simulated tab bar
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BBVATabRelocateView : UIView {
    
@private

}

@end
