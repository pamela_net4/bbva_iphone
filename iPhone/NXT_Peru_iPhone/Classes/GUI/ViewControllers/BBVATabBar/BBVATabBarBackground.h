/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * View to display as the custom BBVA tab bar background. It draws a top to bottom blue gradient
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BBVATabBarBackground : UIView {
    
@private
    
    /**
     * Information label (iPad only)
     */
    UILabel *informationLabel_;
    
    /**
     * BBVA logo image view (iPad only)
     */
    UIImageView *bbvaLogoImageView_;
    
    /**
     * iPhone device flag. YES when the device is an iPhone, NO when it is an iPad
     */
    BOOL iPhoneDevice_;
    
    /**
     * Number of tabs in the tab bar
     */
    NSUInteger tabsCount_;

}

/**
 * Provides read-write access to the information text.
 */
@property (nonatomic, readwrite, copy) NSString *informationText;

/**
 * Provides read-only access to the BBVA logo image view (iPad only)
 */
@property (nonatomic, readwrite, retain) UIImageView *bbvaLogoImageView;

/**
 * Provides read-write access to the BBVA logo image
 */
@property (nonatomic, readwrite, retain) UIImage *logoImage;

/**
 * Provides read-write access to the iPhone device flag
 */
@property (nonatomic, readonly, assign) BOOL iPhoneDevice;

/**
 * Provides read-write access to the number of tabs in the tab bar
 */
@property (nonatomic, readwrite, assign) NSUInteger tabsCount;


/**
 * Returns the iPhone tab bar view height
 *
 * @return The iPhone tab bar view height
 */
+ (CGFloat)iPhoneTabBarHeight;

/**
 * Returns the iPad tab bar view width
 *
 * @return The iPad tab bar view width
 */
+ (CGFloat)iPadTabBarWidth;

/**
 * Calculates the tab width
 *
 * @return The tab width
 * @private
 */
- (CGFloat)tabWidth;

/**
 * Calculates the tab height
 *
 * @return The tab height
 * @private
 */
- (CGFloat)tabHeight;

/**
 * Clips the given context to the tab shape
 *
 * @param context The context to clip
 * @param width The screen width
 * @param height The screen height
 */
- (void)clipContext:(CGContextRef)context
     forScreenWidth:(CGFloat)width
             height:(CGFloat)height;

@end
