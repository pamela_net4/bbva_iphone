/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MoreTabViewController.h"
#import "MoreTabViewController+protected.h"
#import "BBVATabBarTabInformation.h"
#import "BBVATabBarViewController.h"
#import "MoreTabViewCell.h"


/**
 * Defines the NIB file associated to the MoreTabViewController
 */
#define MORE_TAB_VIEW_CONTROLLER_NIB_FILE                                           @"MoreTabViewController"


#pragma mark -

/**
 * MoreTabViewController private extension
 */
@interface MoreTabViewController()

/**
 * Releases the graphic elements not needed when view is hidden
 *
 * @private
 */
- (void)releaseMoreTabViewControllerGraphicElements;

/**
 * Selects the given tab
 *
 * @param tabInformation The tab to select
 * @result YES when the tab is selected, NO otherwise
 * @private
 */
- (BOOL)selectTab:(BBVATabBarTabInformation *)tabInformation;

@end


#pragma mark -

@implementation MoreTabViewController

#pragma mark -
#pragma mark Properties

@synthesize moreTabsTableView = moreTabsTableView_;
@synthesize leftNavigationButton = leftNavigationButton_;
@synthesize rightNavigationButton = rightNavigationButton_;
@synthesize containerViewController = containerViewController_;
@synthesize iPhoneDevice = iPhoneDevice_;
@synthesize tabsList = tabsList_;
@synthesize navigationBarColor = navigationBarColor_;
@synthesize moreTabBackgroundColor = moreTabBackgroundColor_;
@synthesize moreTabTableBackgroundColor = moreTabTableBackgroundColor_;
@synthesize moreTabText = moreTabText_;
@synthesize moreTabElementsOrdered = moreTabElementsOrdered_;
@synthesize cellArrowImage = cellArrowImage_;
@synthesize cellSeparatorImage = cellSeparatorImage_;
@synthesize cellIsNarrow = cellIsNarrow_;
@dynamic selectedTabBar;
@dynamic firstTabBar;
@synthesize moreTabViewControllerDelegate = moreTabViewControllerDelegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self releaseMoreTabViewControllerGraphicElements];

    [leftNavigationButton_ release];
    leftNavigationButton_ = nil;
    
    [rightNavigationButton_ release];
    rightNavigationButton_ = nil;

    containerViewController_ = nil;
    
    [selectedIndexPath_ release];
    selectedIndexPath_ = nil;
    
    [tabsList_ release];
    tabsList_ = nil;
    
    [originalTabsList_ release];
    originalTabsList_ = nil;

    [navigationBarColor_ release];
    navigationBarColor_ = nil;
    
    [moreTabBackgroundColor_ release];
    moreTabBackgroundColor_ = nil;
    
    [moreTabTableBackgroundColor_ release];
    moreTabTableBackgroundColor_ = nil;
    
    [moreTabText_ release];
    moreTabText_ = nil;
    
    [cellArrowImage_ release];
    cellArrowImage_ = nil;
    
    [cellSeparatorImage_ release];
    cellSeparatorImage_ = nil;
    
    moreTabViewControllerDelegate_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory. The navigation controller array is stored to restore it in case it is necessary
 */
- (void)viewDidUnload {
    
    [self releaseMoreTabViewControllerGraphicElements];

    [super viewDidUnload];
    
}

/*
 * Releases the graphic elements not needed when view is hidden
 */
- (void)releaseMoreTabViewControllerGraphicElements {
    
    [moreTabsTableView_ release];
    moreTabsTableView_ = nil;
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseMoreTabViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. The tab list is created
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    if (tabsList_ == nil) {
        
        tabsList_ = [[NSMutableArray alloc] init];
        
    }
    
    if (originalTabsList_ == nil) {
        
        originalTabsList_ = [[NSMutableArray alloc] init];
        
    }
    
    iPhoneDevice_ = YES;
    
#ifdef UI_USER_INTERFACE_IDIOM
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        iPhoneDevice_ = NO;
        
    }
    
#endif

}

#pragma mark -
#pragma mark View management

/**
 * Called after the controller’s view is loaded into memory. View background color is set to default aswell as navigation bar tint color
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if (moreTabBackgroundColor_ == nil) {
        
        self.view.backgroundColor = [UIColor whiteColor];
        
    } else {
        
        self.view.backgroundColor = moreTabBackgroundColor_;
        
    }
    
    if (iPhoneDevice_) {
        
        if (moreTabTableBackgroundColor_ == nil) {
            
            moreTabsTableView_.backgroundColor = [UIColor whiteColor];

        } else {
            
            moreTabsTableView_.backgroundColor = moreTabTableBackgroundColor_;

        }
        
    } else {
        
        moreTabsTableView_.backgroundColor = [UIColor clearColor];
        
    }

    moreTabsTableView_.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (navigationBarColor_ != nil) {
        
        self.navigationController.navigationBar.tintColor = navigationBarColor_;
        
    }
    
}

/**
 * Notifies the view controller that its view is about to be become visible. The table information is realoaded
 *
 * @param animated If YES, the view is being added to the window using an animation
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (navigationBarColor_ != nil) {
        
        self.navigationController.navigationBar.tintColor = navigationBarColor_;
        
    }

    moreTabsTableView_.rowHeight = [MoreTabViewCell cellHeightForNarrowCell:cellIsNarrow_];
    [self loadTableInformation];
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if ([containerViewController_ isKindOfClass:[BBVATabBarViewController class]]) {
        
        BBVATabBarViewController *tabBarViewController = (BBVATabBarViewController *)containerViewController_;
        [tabBarViewController setTabBarVisibility:YES animated:YES];
        
    }
    
}

#pragma mark -
#pragma mark Tab selection

/*
 * Selects the given element
 */
- (BOOL)selectTab:(BBVATabBarTabInformation *)tabInformation {
    
    BOOL result = NO;
    
    if ([tabsList_ containsObject:tabInformation]) {
        
        if ((moreTabViewControllerDelegate_ == nil) ||
            ([moreTabViewControllerDelegate_ moreTabViewController:self shouldSelectTab:tabInformation])) {
            
            UIViewController *viewController = tabInformation.moreTabViewController;
            
            if (viewController != nil) {
                
                [moreTabViewControllerDelegate_ moreTabViewController:self
                                                        willSelectTab:tabInformation];
                
                [self displayViewControllerAsBase:viewController
                                       fromTabBar:tabInformation];
                
                [moreTabViewControllerDelegate_ moreTabViewController:self
                                                          selectedTab:tabInformation];
                
                result = YES;
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Selects a given view controller, if found among the tab information
 */
- (BBVATabBarTabInformation *)selectViewController:(UIViewController *)viewController {
    
    BBVATabBarTabInformation *result = [self tabInformationForViewController:viewController];
    
    if (result != nil) {
        
        [self selectTab:result];
        
    }
    
    return result;
    
}

/*
 * Checks whether the more tab  bar contains a view controller or not
 */
- (BOOL)containsViewController:(UIViewController *)viewController {
    
    BOOL result = NO;
    
    if (viewController != nil) {
        
        for (BBVATabBarTabInformation *storedTab in tabsList_) {
            
            if (storedTab.viewController == viewController) {
                
                result = YES;
                break;
                
            }
            
        }
        
    }
    
    return result;

}

#pragma mark -
#pragma mark Utility selectors

/*
 * Returns the tab information for the given view controller
 */
- (BBVATabBarTabInformation *)tabInformationForViewController:(UIViewController *)viewController {
    
    BBVATabBarTabInformation *result = nil;
    
    for (result in tabsList_) {
        
        if (result.viewController == viewController) {
            
            break;
            
        } else {
            
            result = nil;
            
        }
        
    }

    return result;
    
}

/*
 * Resets the more tab bar to the first tab. Only available on iPad. Default implementation does nothing
 */
- (void)resetMoreTabToFirstTab {
    
    //Do nothing.
    
}

#pragma mark -
#pragma mark Table view data source

/**
 * Tells the data source to return the number of rows in a given section of a table view. The more tabs table only section has as many cells
 * as tab information instances inside the tabs list
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section in tableView
 * @return The number of instances inside the tabs list
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [tabsList_ count];
    
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. The cell returned is a MoreTabViewCell instance
 *
 * @param tableView A table-view object requesting the cell
 * @param indexPath An index path locating a row in tableView
 * @return The MoreTabViewCell instance to represent the tab element
 */
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     MoreTabViewCell *cell = (MoreTabViewCell *)[tableView dequeueReusableCellWithIdentifier:[MoreTabViewCell cellIdentifier]];
     
     if (cell == nil) {
        
         cell = [MoreTabViewCell moreTabViewCell];
        
     }

     cell.cellIsNarrow = cellIsNarrow_;
     cell.cellArrowImage = cellArrowImage_;
     cell.cellSeparatorImage = cellSeparatorImage_;
     
     if (cellSeparatorImage_ == nil) {
         
         tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
         
     } else {
         
         tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
         
     }
    
    if (indexPath.section == 0) {
        
        NSUInteger row = indexPath.row;
        
        if (row < [tabsList_ count]) {
            
            BBVATabBarTabInformation *tabInformation = [tabsList_ objectAtIndex:row];
            cell.tabInformation = tabInformation;
            
            if (tabInformation.moreTabViewController == nil) {
                
                cell.displayArrow = YES;
                
            } else {
                
                cell.displayArrow = YES;
                
            }
            
            if (!iPhoneDevice_) {
                
                if ((selectedIndexPath_ != nil) && ([selectedIndexPath_ compare:indexPath] == NSOrderedSame)) {
                    
                    [tableView selectRowAtIndexPath:selectedIndexPath_
                                           animated:YES
                                     scrollPosition:UITableViewScrollPositionNone];
                    cell.displayArrow = NO;
                    
                }
                
            }
            
        } else {
            
            cell.tabInformation = nil;
            
        }
        
    } else {
        
        cell.tabInformation = nil;
        
    }
    
    return cell;
     
}

#pragma mark -
#pragma mark Table view delegate

/**
 * Tells the delegate that the specified row is now selected. The view controller associated to the tab information is displayed
 *
 * @param tableView A table-view object informing the delegate about the new row selection
 * @param indexPath An index path locating the new selected row in tableView
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (iPhoneDevice_) {
        
        [tableView deselectRowAtIndexPath:indexPath
                                 animated:YES];
        
    }
    
    UINavigationController *navigationController = self.navigationController;
    
    if ((indexPath.section == 0) && (navigationController != nil)) {
        
        NSUInteger row = indexPath.row;
        
        if (row < [tabsList_ count]) {
            
            BBVATabBarTabInformation *tabInformation = [tabsList_ objectAtIndex:row];

            if ([self selectTab:tabInformation]) {

                if ((selectedIndexPath_ == nil) || ([selectedIndexPath_ compare:indexPath] != NSOrderedSame)) {
                    
                    NSMutableArray *updatingCell = [NSMutableArray array];
                    
                    if (selectedIndexPath_ != nil) {
                        
                        [updatingCell addObject:selectedIndexPath_];
                        
                    }
                    
                    [indexPath retain];
                    [selectedIndexPath_ release];
                    selectedIndexPath_ = indexPath;
                    
                    
                    if (selectedIndexPath_ != nil) {
                        
                        [updatingCell addObject:selectedIndexPath_];
                        
                    }
                    
                    if (!iPhoneDevice_) {
                        
                        [moreTabsTableView_ reloadRowsAtIndexPaths:updatingCell
                                                  withRowAnimation:UITableViewRowAnimationNone];
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the navigation item used to represent the view controller. (read-only)
 *
 * @return the navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *item = [super navigationItem];
    
    item.leftBarButtonItem = leftNavigationButton_;
    item.title = moreTabText_;
    item.rightBarButtonItem = rightNavigationButton_;
    
    return item;
    
}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the tab list
 *
 * @return The tab list
 */
- (NSArray *)tabsList {
    
    return [NSArray arrayWithArray:tabsList_];
    
}

/*
 * Sest the tabs list, checking the array elements type
 *
 * @param tabsList The new tabs list to store and display
 */
- (void)setTabsList:(NSArray *)tabsList {
    
    if ([BBVATabBarTabInformation fillArray:originalTabsList_ withBBVATabBarTabInformationInstancesFromOrigin:tabsList]) {
        
        [tabsList_ removeAllObjects];
        [tabsList_ addObjectsFromArray:originalTabsList_];
        
        if (moreTabElementsOrdered_) {
            
            [tabsList_ sortUsingSelector:@selector(compareForMoreTabLocation:)];
            
        }
        
        [self loadTableInformation];
        
    }
    
}

/*
 * Sets the new navigation bar color
 *
 * @param navigationBarColor The new navigation bar color to set
 */
- (void)setNavigationBarColor:(UIColor *)navigationBarColor {
    
    if ((navigationBarColor != nil) && (navigationBarColor != navigationBarColor_)) {
        
        [navigationBarColor retain];
        [navigationBarColor_ release];
        navigationBarColor_ = navigationBarColor;
        
        self.navigationController.navigationBar.tintColor = navigationBarColor_;
        
    }
    
}

/*
 * Sets the new more tab background color
 *
 * @param moreTabBackgroundColor The new more tab background color to set
 */
- (void)setMoreTabBackgroundColor:(UIColor *)moreTabBackgroundColor {
    
    if ((moreTabBackgroundColor != nil) && (moreTabBackgroundColor != moreTabBackgroundColor_)) {
        
        [moreTabBackgroundColor retain];
        [moreTabBackgroundColor_ release];
        moreTabBackgroundColor_ = moreTabBackgroundColor;
        
        if ([self isViewLoaded]) {
            
            self.view.backgroundColor = moreTabBackgroundColor_;
            
        }
        
    }
    
}

/*
 * Sets the new more tab table background color
 *
 * @param moreTabTableBackgroundColor The new more tab table background color to set
 */
- (void)setMoreTabTableBackgroundColor:(UIColor *)moreTabTableBackgroundColor {
    
    if ((moreTabTableBackgroundColor != nil) && (moreTabTableBackgroundColor != moreTabTableBackgroundColor_)) {
        
        [moreTabTableBackgroundColor retain];
        [moreTabTableBackgroundColor_ release];
        moreTabTableBackgroundColor_ = moreTabTableBackgroundColor;
        
        if (iPhoneDevice_) {
            
            moreTabsTableView_.backgroundColor = moreTabTableBackgroundColor_;
            
        } else {
            
            moreTabsTableView_.backgroundColor = [UIColor clearColor];
            
        }
        
    }
    
}

/*
 * Sets the new more tab text
 *
 * @param moreTabText The new more tab text to set
 */
- (void)setMoreTabText:(NSString *)moreTabText {
    
    if (moreTabText != moreTabText_) {
        
        NSString *auxString = [moreTabText copyWithZone:self.zone];
        [moreTabText_ release];
        moreTabText_ = auxString;
        
        self.navigationItem.title = moreTabText_;
        
    }
    
}

/*
 * Sets the more tab elements ordered alphabetically flag
 *
 * @param moreTabElementsOrdered The new more tab elements ordered alphabetically flag
 */
- (void)setMoreTabElementsOrdered:(BOOL)moreTabElementsOrdered {
    
    if (moreTabElementsOrdered != moreTabElementsOrdered_) {
        
        moreTabElementsOrdered_ = moreTabElementsOrdered;
        
        BOOL updateSelected = NO;
        BBVATabBarTabInformation *selectedTabBar = nil;
        NSUInteger originalSelectedOrder = NSNotFound;
        
        if ((!iPhoneDevice_) && (selectedIndexPath_ != nil)) {
            
            updateSelected = YES;
            originalSelectedOrder = selectedIndexPath_.row;
            selectedTabBar = [tabsList_ objectAtIndex:originalSelectedOrder];
            
        }
        
        self.tabsList = originalTabsList_;
        
        if (updateSelected) {
            
            NSUInteger newSelectedOrder = [tabsList_ indexOfObject:selectedTabBar];
            
            if ((newSelectedOrder != NSNotFound) && (newSelectedOrder != originalSelectedOrder)) {
                
                NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:newSelectedOrder
                                                               inSection:0];
                [moreTabsTableView_ selectRowAtIndexPath:newIndexPath
                                                animated:NO
                                          scrollPosition:UITableViewScrollPositionNone];
                [self tableView:moreTabsTableView_ didSelectRowAtIndexPath:newIndexPath];
                
            }
            
        }
        
    }
    
}

/*
 * Sets the new cell arrow image
 *
 * @param cellArrowImage The new cell arrow image to set
 */
- (void)setCellArrowImage:(UIImage *)cellArrowImage {
    
    if (cellArrowImage != cellArrowImage_) {
        
        [cellArrowImage retain];
        [cellArrowImage_ release];
        cellArrowImage_ = cellArrowImage;
        
        [self loadTableInformation];
        
    }
    
}

/*
 * Sets the new cell separator image
 *
 * @param cellSeparatorImage The new cell separator image to set
 */
- (void)setCellSeparatorImage:(UIImage *)cellSeparatorImage {
    
    if (cellSeparatorImage != cellSeparatorImage_) {
        
        [cellSeparatorImage retain];
        [cellSeparatorImage_ release];
        cellSeparatorImage_ = cellSeparatorImage;
        
        [self loadTableInformation];
        
    }
    
}

/*
 * Sets the new narrow cell layout flag and reloads the table information
 *
 * @param cellIsNarrow The new narrow cell layout flag to set
 */
- (void)setCellIsNarrow:(BOOL)cellIsNarrow {
    
    if (cellIsNarrow != cellIsNarrow_) {
        
        cellIsNarrow_ = cellIsNarrow;
        
        if ([self isViewLoaded]) {
            
            moreTabsTableView_.rowHeight = [MoreTabViewCell cellHeightForNarrowCell:cellIsNarrow_];
            [self loadTableInformation];
            
        }
        
    }
    
}

/*
 * Returns the selected tab bar
 *
 * @return The selected tab bar
 */
- (BBVATabBarTabInformation *)selectedTabBar {
    
    return nil;
    
}

/*
 * Returns the first tab bar
 *
 * @return The first tab bar
 */
- (BBVATabBarTabInformation *)firstTabBar {
    
    BBVATabBarTabInformation *result = nil;
    
    if (tabsList_ > 0) {
        
        result = [tabsList_ objectAtIndex:0];
        
    }
    
    return result;
    
}

@end

