/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "BBVATabBarRelocateTabsViewController.h"
#import "BBVATabBarContainerView.h"
#import "BBVATabBarViewControllerView.h"
#import "MoreTabViewController.h"


//Forward declarations
@class BBVATabBarViewController;
@class BBVATabBarTabInformation;


/**
 * BBVATabBarViewController delegate protocol. Delegate is notified when an event happens or is requested to decide whether to display
 * a view controller or not
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol BBVATabBarViewControllerDelegate

@required

/**
 * Asks the delegate confirmation to select or not a given view controller
 *
 * @param bbvaTabBarViewController The BBVA tab bar view controller asking for confirmation
 * @param viewController The view controller that is going to be selected
 * @return YES if the view controller can be selected, NO otherwise
 */
- (BOOL)tabBarViewController:(BBVATabBarViewController *)bbvaTabBarViewController
  shouldSelectViewController:(UIViewController *)viewController;

/**
 * Notifies a new view controller will be selected
 *
 * @param bbvaTabBarViewController The BBVA tab bar view controller triggering the event
 * @param viewController The view controller being selected
 */
- (void)tabBarViewController:(BBVATabBarViewController *)bbvaTabBarViewController
    willSelectViewController:(UIViewController *)viewController;

/**
 * Notifies a new view controller has been selected
 *
 * @param bbvaTabBarViewController The BBVA tab bar view controller triggering the event
 * @param viewController The view controller being selected
 */
- (void)tabBarViewController:(BBVATabBarViewController *)bbvaTabBarViewController
      viewControllerSelected:(UIViewController *)viewController;

/**
 * Notifies the delegate that the tab bar view did disappear
 *
 * @param bbvaTabBarViewController The BBVA tab bar view controller triggering the event
 */
- (void)tabBarViewControllerTabBarDidDisappear:(BBVATabBarViewController *)bbvaTabBarViewController;

/**
 * Notifies the delegate that the tab bar view did appear
 *
 * @param bbvaTabBarViewController The BBVA tab bar view controller triggering the event
 */
- (void)tabBarViewControllerTabBarDidAppear:(BBVATabBarViewController *)bbvaTabBarViewController;

/**
 * Asks the delegate whether a given view controller can be relocated
 *
 * @param bbvaTabBarViewController The BBVA tab bar view controller asking for confirmation
 * @param tabBarInformation The information object representing the BBVA tab bar to check about
 * @return YES when the view controller can be selected, NO otherwise
 */
- (BOOL)tabBarViewController:(BBVATabBarViewController *)bbvaTabBarViewController
              canRelocateTab:(BBVATabBarTabInformation *)tabBarInformation;

/**
 * Notifies the delegate that the tab bars where reordered
 *
 * @param bbvaTabBarViewController The BBVA tab bar view controller triggering the event
 * @param tabsList The new tabs order. All objects are BBVATabBarTabInformation instances
 * @param tabBarCount The number of tabs inside the tab bar (not including the "more" tab). This parameter has meaning in iPad version
 */
- (void)tabBarViewcontroller:(BBVATabBarViewController *)bbvaTabBarViewController
               tabsReordered:(NSArray *)tabsList
                 tabBarCount:(NSUInteger)tabBarCount;

/**
 * Notifies the delegate that the logo image was tapped.
 *
 * @param bbvaTabBarViewController The BBVA tab bar view controller triggering the event
 */
- (void)tabBarViewControllerLogoImageTapped:(BBVATabBarViewController *)bbvaTabBarViewController;

@end


/**
 * View controller to manage a customized BBVA tab bar
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BBVATabBarViewController : UIViewController <BBVATabBarContainerViewDelegate, MoreTabViewControllerDelegate, BBVATabBarRelocateTabsViewControllerDelegate, BBVATabBarViewControllerViewDelegate> {

@private
    
    /**
     * Tab bar to select the active content view
     */
    BBVATabBarContainerView *tabBarView_;
    
    /**
     * Navigation controller for the more tabs view controller. When application doesn't provide any, a default one is created
     */
    UINavigationController *moreTabNavigationController_;

    /**
     * More tabs view controller
     */
    MoreTabViewController *moreTabsViewController_;
    
    /**
     * Relocate tabs view controller
     */
    BBVATabBarRelocateTabsViewController *relocateTabsViewController_;
    
    /**
     * Controller currently being displayed
     */
    UIViewController *selectedViewController_;
    
    /**
     * More view controller left navigation button
     */
    UIBarButtonItem *moreLeftNavigationButton_;
    
    /**
     * More view controller right navigation button
     */
    UIBarButtonItem *moreRightNavigationButton_;
    
    /**
     * Selected tab information
     */
    BBVATabBarTabInformation *selectedTabInformation_;

    /**
     * Complete list of tabs. Tab bar view can only display as much as five tabs. When more than the maximum number of tabs are present, a "more" tab is displayed as the last tab to
     * access the remaining tabs. All objects are BBVATabBarTabInformation instances
     */
    NSMutableArray *tabsList_;
    
    /**
     * Tabs default order. All objects are BBVATabBarTabInformation instances
     */
    NSMutableArray *tabsDefaultOrderList_;
    
    /**
     * Number of tabs inside the tab bar, not including the "more" tab. This attribute has more meaning in iPad version (iPhone version has a fixed amount)
     */
    NSUInteger tabsInTabBarCount_;
    
    /**
     * Default number of tabs inside the tab bar, not including the "more" tab. This attribute has more meaning in iPad version (iPhone version has a fixed amount)
     */
    NSUInteger defaultTabsInTabBarCount_;
    
    /**
     * Maximum number of tabs inside the tab bar, including the "more" tab. This attribute has only meaing in iPad version (iPhone version has a fixed amount)
     */
    NSUInteger maximumTabsInTabBarCount_;
    
    /**
     * More tabs available tab information
     */
    BBVATabBarTabInformation *moreTabInformation_;
    
    /**
     * Current tab is more tab flag. YES when the tab selected in the tab bar is the more tab, NO otherwise
     */
    BOOL currentTabIsMoreTab_;
    
    /**
     * iPhone device flag. YES when the device is an iPhone, NO when it is an iPad
     */
    BOOL iPhoneDevice_;
    
    /**
     * Tab bar hidden flag. YES when the tab bar is completely hidden (even the collapse button in case it is displayed), NO otherwise
     */
    BOOL tabBarDisplayed_;
    
    /**
     * Tab bar displayed on the right hand side flag. YES when the tab bar is displayed on the right hand side of screen, NO when it is displayed
     * on the left hand side. (iPad only)
     */
    BOOL tabBarOnRightHandSide_;
    
    /**
     * Display collapse/expand button flag. YES when the tab bar collapse/expand button must be displayed, NO otherwise. (iPad only)
     */
    BOOL displayCollapseButtonFlag_;
    
    /**
     * Collapse tab bar on portrait. YES when the tab bar must be collapsed on portrait orientation, NO otherwise. (iPad only)
     */
    BOOL collapseTabBarOnPortrait_;
    
    /**
     * Tab bar floating flag. YES when tab bar seems to float over content (the content expands to all the view size), NO otherwise. (iPad only)
     */
    BOOL floatingTabBar_;

    /**
     * View visible flag. YES when the view is being displayed, NO otherwise
     */
    BOOL viewVisible_;
    
    /**
     * Navigation bar color
     */
    UIColor *navigationBarColor_;
    
    /**
     * More tab background color
     */
    UIColor *moreTabBackgroundColor_;
    
    /**
     * More tab table background color
     */
    UIColor *moreTabTableBackgroundColor_;

    /**
     * More tab text
     */
    NSString *moreTabText_;
    
    /**
     * More tab reference image to synthesize not provided images image
     */
    UIImage *moreTabReferenceImage_;
    
    /**
     * More tab selected image
     */
    UIImage *moreTabSelectedImage_;
    
    /**
     * More tab selected color, in case the image must be synthesized
     */
    UIColor *moreTabSelectedColor_;
    
    /**
     * More tab unselected image
     */
    UIImage *moreTabUnselectedImage_;
    
    /**
     * More tab unselected color, in case the image must be synthesized
     */
    UIColor *moreTabUnselectedColor_;
    
    /**
     * More tab disabled image
     */
    UIImage *moreTabDisabledImage_;
    
    /**
     * More tab disabled color, in case the image must be synthesized
     */
    UIColor *moreTabDisabledColor_;
    
    /**
     * More tab elements ordered alphabetically flag. YES when the more tab elements must be ordered alphabetically, NO when the order is not set
     */
    BOOL moreTabElementsOrdered_;
    
    /**
     * Reset more tab on selection flag. YES when the more tab selection must he reseted to the firs element each time it is selectedo, NO otherwise. Only available in iPad
     */
    BOOL moreTabResetOnSelection_;

    /**
     * Cell arrow image
     */
    UIImage *cellArrowImage_;
    
    /**
     * Cell separator image
     */
    UIImage *cellSeparatorImage_;
    
    /**
     * Relocate navigation bar class used to create a custom navigation bar if needed. Only UINavigationBar subclasses are stored
     */
    Class relocateNavigationBarClass_;

    /**
     * Relocate view background color
     */
    UIColor *relocateViewBackgroundColor_;
    
    /**
     * Relocate title text color
     */
    UIColor *relocateTitleTextColor_;

    /**
     * Header background image
     */
    UIImage *relocateHeaderBackgroundImage_;
    
    /**
     * Relocate tab navigation title
     */
    NSString *relocateTabNavigationTitle_;
    
    /**
     * Relocate tab header title
     */
    NSString *relocateTabHeaderTitle_;
    
    /**
     * Relocate tab reset text
     */
    NSString *relocateTabResetText_;
    
    /**
     * Relocate tab done text
     */
    NSString *relocateTabDoneText_;
    
    /**
     * Collapse button right arrow image (iPad only)
     */
    UIImage *collapseTabRightArrowImage_;
    
    /**
     * Collapse button left arrow image (iPad only)
     */
    UIImage *collapseTabLeftArrowImage_;
    
    /**
     * Tab bar information text (iPad only)
     */
    NSString *tabBarInformationText_;
    
    /**
     * Tab bar logo image (iPad only)
     */
    UIImage *tabBarLogoImage_;
    
    /**
     * Selected tab background color
     */
    UIColor *selectedTabBackgroundColor_;

    /**
     * More tab narrow cell layout flag. YES when the cell is a narrow cell with only one text field and the image, NO otherwise
     */
    BOOL moreTabCellIsNarrow_;
    
    /**
     * New version flag. NO when iOS version is older than 5.0, YES otherwise
     */
    BOOL newOSVersion_;

    /**
     * BBVA tab bar view controller delegate
     */
    id<BBVATabBarViewControllerDelegate> delegate_;
    
}


/**
 * Provides read-only access to the navigation controller for the more tabs view controller. It can only be set when no previous navigation controller for the more tabs view controller exists
 */
@property (nonatomic, readwrite, retain) UINavigationController *moreTabNavigationController;

/**
 * Provides read-only access to the selected view controller
 */
@property (nonatomic, readonly, retain) UIViewController *selectedViewController;

/**
 * Provides read-only access to the selected view controller tab index
 */
@property (nonatomic, readonly, assign) NSUInteger selectedViewControllerTabIndex;

/**
 * Provides read-write access to the tab on the right hand side flag
 */
@property (nonatomic, readwrite, assign) BOOL tabBarOnRightHandSide;

/**
 * Provides read-write access to the collapse tab bar on portrait.
 */
@property (nonatomic, readwrite, assign) BOOL collapseTabBarOnPortrait;

/**
 * Provides read-only access to the visibility
 */
@property (nonatomic, readonly, assign) BOOL hidden;

/**
 *  Provides read-write access to the navigation bar color
 */
@property (nonatomic, readwrite, retain) UIColor *navigationBarColor;

/**
 * Provides read-write access to the more tab background color
 */
@property (nonatomic, readwrite, retain) UIColor *moreTabBackgroundColor;

/**
 * Provides read-write access to the More tab table background color
 */
@property (nonatomic, readwrite, retain) UIColor *moreTabTableBackgroundColor;

/**
 * Provides read-write access to the more tab text
 */
@property (nonatomic, readwrite, copy) NSString *moreTabText;

/**
 * Provides read-write access to the more tab reference image to synthesize not provided images image
 */
@property (nonatomic, readwrite, retain) UIImage *moreTabReferenceImage;

/**
 * Provides read-write access to the more tab selected image
 */
@property (nonatomic, readwrite, retain) UIImage *moreTabSelectedImage;

/**
 * Provides read-write access to the more tab selected color, in case the image must be synthesized
 */
@property (nonatomic, readwrite, retain) UIColor *moreTabSelectedColor;

/**
 * Provides read-write access to the more tab unselected image
 */
@property (nonatomic, readwrite, retain) UIImage *moreTabUnselectedImage;

/**
 * Provides read-write access to the more tab unselected color, in case the image must be synthesized
 */
@property (nonatomic, readwrite, retain) UIColor *moreTabUnselectedColor;

/**
 * Provides read-write access to the more tab disabled image
 */
@property (nonatomic, readwrite, retain) UIImage *moreTabDisabledImage;

/**
 * Provides read-write access to the more tab disabled color, in case the image must be synthesized
 */
@property (nonatomic, readwrite, retain) UIColor *moreTabDisabledColor;

/**
 * Provides read-write access to the more tab elements ordered alphabetically
 */
@property (nonatomic, readwrite, assign) BOOL moreTabElementsOrdered;

/**
 * Provides read-write access to the reset more tab on selection flag.
 */
@property (nonatomic, readwrite, assign) BOOL moreTabResetOnSelection;

/**
 * Provides read-write access to the cell arrow image
 */
@property (nonatomic, readwrite, retain) UIImage *cellArrowImage;

/**
 * Provides read-write access to the cell separator image
 */
@property (nonatomic, readwrite, retain) UIImage *cellSeparatorImage;

/**
 * Provides read-write access to the relocate navigation bar class used to create a custom navigation bar if needed
 */
@property (nonatomic, readwrite, retain) Class relocateNavigationBarClass;

/**
 * Provides read-write access to the relocate view background color
 */
@property (nonatomic, readwrite, retain) UIColor *relocateViewBackgroundColor;

/**
 * Provides read-write access to the relocate title text color
 */
@property (nonatomic, readwrite, retain) UIColor *relocateTitleTextColor;

/**
 * Provides read-write access to the header background image
 */
@property (nonatomic, readwrite, retain) UIImage *relocateHeaderBackgroundImage;

/**
 * Provides read-write access to the relocate tab navigation title
 */
@property (nonatomic, readwrite, copy) NSString *relocateTabNavigationTitle;

/**
 * Provides read-write access to the relocate tab header title
 */
@property (nonatomic, readwrite, copy) NSString *relocateTabHeaderTitle;

/**
 * Provides read-write access to the relocate tab reset text
 */
@property (nonatomic, readwrite, copy) NSString *relocateTabResetText;

/**
 * Provides read-write access to the relocate tab done text
 */
@property (nonatomic, readwrite, copy) NSString *relocateTabDoneText;

/**
 * Provides read-write access to the collapse button right arrow image (iPad only)
 */
@property (nonatomic, readwrite, retain) UIImage *collapseTabRightArrowImage;

/**
 * Provides read-write access to the collapse button left arrow image (iPad only)
 */
@property (nonatomic, readwrite, retain) UIImage *collapseTabLeftArrowImage;

/**
 * Provides read-write access to the tTab bar information text (iPad only)
 */
@property (nonatomic, readwrite, copy) NSString *tabBarInformationText;

/**
 * Provides read-write access to the tab bar logo image (iPad only)
 */
@property (nonatomic, readwrite, retain) UIImage *tabBarLogoImage;

/**
 * Provides read-write access to the selected tab background color
 */
@property (nonatomic, readwrite, retain) UIColor *selectedTabBackgroundColor;

/**
 * Provides read-write access to the more tab narrow cell layout flag
 */
@property (nonatomic, readwrite, assign) BOOL moreTabCellIsNarrow;

/**
 * Provides read-write access to the BBVA tab bar view controller delegate
 */
@property (nonatomic, readwrite, assign) id<BBVATabBarViewControllerDelegate> delegate;


/**
 * Creates and returns an autoreleased BBVATabBarViewController constructed from a NIB file
 *
 * @return The autoreleased BBVATabBarViewController constructed from a NIB file
 */
+ (BBVATabBarViewController *)bbvaTabBarViewController;

/**
 * Sets the default tabs order array and the current tabs list order array
 *
 * @param defaultTabsOrderArray The array containing the default tabs ordering. Only BBVATabBarTabInformation instances are used
 * @param currentTabsOrderArray The array containing the current tabs ordering. Only BBVATabBarTabInformation instances are used and must
 * be the same found in defaulTabsOrderArray
 */
- (void)setDefaultTabsOrderArray:(NSArray *)defaultTabsOrderArray
           currentTabsOrderArray:(NSArray *)currentTabsOrderArray;

/**
 * Sets the default tabs order array, the current tabs list order array and the tabs in tab bar count elements. This selector has more meaing in iPad version. In iPhone version
 * tabs count elements are set to fixed values
 *
 * @param defaultTabsOrderArray The array containing the default tabs ordering. Only BBVATabBarTabInformation instances are used
 * @param currentTabsOrderArray The array containing the current tabs ordering. Only BBVATabBarTabInformation instances are used and must
 * be the same found in defaulTabsOrderArray
 * @param tabsInTabBarCount The number of tabs inside the tab bar, not including the "more" tab
 * @param defaultTabsInTabBarCount The default number of tabs inside the tab bar, not including the "more" tab
 * @param maximumTabsInTabBarCount The maximum number of tabs inside the tab bar, including the "more" tab
 */
- (void)setDefaultTabsOrderArray:(NSArray *)defaultTabsOrderArray
           currentTabsOrderArray:(NSArray *)currentTabsOrderArray
               tabsInTabBarCount:(NSUInteger)tabsInTabBarCount
        defaultTabsInTabBarCount:(NSUInteger)defaultTabsInTabBarCount
        maximumTabsInTabBarCount:(NSUInteger)maximumTabsInTabBarCount;

/**
 * Selects a given view controller, if found among the tab information. It forwards the operation to its tab bar view
 *
 * @param viewController The view controller to select
 * @return YES when the view controller can be displayed, NO otherwise
 */
- (BOOL)selectViewController:(UIViewController *)viewController;

/**
 * Sets the tab bar view visibility animated or not. When animated, the tab var is moved down.
 *
 * @param visible The visitility flag
 * @param animated YES to animate the tab bar
 */
- (void)setTabBarVisibility:(BOOL)visible
                   animated:(BOOL)animated;

/**
 * Starts relocating the tabs. It displays a modal view controller to allow the user move the tabs
 */
- (void)relocateTabs;

/**
 * Stops relocating the tabs
 */
- (void)stopRelocateTabs;

/**
 * Sets the more tab naviagation bar elements
 *
 * @param moreTabLeftBarButton The more tab left navigation bar button
 * @param moreTabRightBarButton The more tab right navigation bar button
 */
- (void)setMoreTabLeftBarButton:(UIBarButtonItem *)moreTabLeftBarButton
                 rightBarButton:(UIBarButtonItem *)moreTabRightBarButton;

/**
 * Pushes the provided view controller into the more view controller, in case it is active
 *
 * @param viewController The view controller to push
 * @param animated YES when the view controller must be displayed with an animation, NO otherwise
 */
- (void)pushViewControllerIntoMoreTab:(UIViewController *)viewController
                             animated:(BOOL)animated;

@end
