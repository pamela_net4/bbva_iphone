/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


//Forward declarations
@class BBVATabBarTabInformation;


/**
 * Cell to display a tab element inside the more tabs view table
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MoreTabViewCell : UITableViewCell {

@private
    
    /**
     * Tab icon image view
     */
    UIImageView *tabIconImageView_;
    
    /**
     * Tab text label
     */
    UILabel *tabTextLabel_;
    
    /**
     * Secondary text label
     */
    UILabel *tabSecondaryTextLabel_;
    
    /**
     * Arrow image view
     */
    UIImageView *cellArrowImageView_;
    
    /**
     * Cell separator image view
     */
    UIImageView *cellSeparatorImageView_;

    /**
     * Tab bar invormation associated
     */
    BBVATabBarTabInformation *tabInformation_;
    
    /**
     * Narrow cell layout flag. YES when the cell is a narrow cell with only one text field and the image, NO otherwise
     */
    BOOL cellIsNarrow_;
    
}

/**
 * Provides read-write access to the tab icon image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *tabIconImageView;

/**
 * Provides read-write access to the tab text label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *tabTextLabel;

/**
 * Provides read-write access to the secondary text label
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *tabSecondaryTextLabel;

/**
 * Provides read-write access to the arrow image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *cellArrowImageView;

/**
 * Provides read-write access to the cell separator image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *cellSeparatorImageView;

/**
 * Provides read-write access to the cell arrow image
 */
@property (nonatomic, readwrite, retain) UIImage *cellArrowImage;

/**
 * Provides read-write access to the cell separator image
 */
@property (nonatomic, readwrite, retain) UIImage *cellSeparatorImage;

/**
 * Provides read-write access to the tab bar invormation associated
 */
@property (nonatomic, readwrite, retain) BBVATabBarTabInformation *tabInformation;

/**
 * Provides read-write access to the narrow cell layout flag
 */
@property (nonatomic, readwrite, assign) BOOL cellIsNarrow;

/**
 * Provides read-write access to the display arrow flag
 */
@property (nonatomic, readwrite, assign) BOOL displayArrow;


/**
 * Creates and returns an autoreleased MoreTabViewCell constructed from a NIB file
 *
 * @return The autoreleased MoreTabViewCell constructed from a NIB file
 */
+ (MoreTabViewCell *)moreTabViewCell;

/**
 * Returns the cell height
 *
 * @param isNarrowCell YES when the cell to calculate is a narrow one, NO when it is a wide one
 * @return The cell height
 */
+ (CGFloat)cellHeightForNarrowCell:(BOOL)isNarrowCell;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end
