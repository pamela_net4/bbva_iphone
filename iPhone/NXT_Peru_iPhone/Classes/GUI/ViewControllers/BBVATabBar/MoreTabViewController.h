/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


//Forward declarations
@class MoreTabViewController;
@class BBVATabBarTabInformation;
@class BBVATabBarViewController;



/**
 * MoreTabViewController delegate protocol. Delegate receives notifications and must decide about displaying view controllers or not
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol MoreTabViewControllerDelegate

/**
 * Asks the delegate confirmation to select or not a given tab
 *
 * @param moreTabViewController The more tab view controller asking for confirmation
 * @param tabInformation The information that defines the tab that is going to be selected
 * @return YES if the tab can be selected, NO otherwise
 */
- (BOOL)moreTabViewController:(MoreTabViewController *)moreTabViewController
              shouldSelectTab:(BBVATabBarTabInformation *)tabInformation;

/**
 * Notifies a new view controller will be selected. The view controller appear and disappear selectors must not be triggered by the tab bar view controller
 * itself
 *
 * @param moreTabViewController The more tab view controller asking for confirmation
 * @param tabInformation The tab being selected information
 */
- (void)moreTabViewController:(MoreTabViewController *)moreTabViewController
                willSelectTab:(BBVATabBarTabInformation *)tabInformation;

/**
 * Notifies a new tab was selected. The view controller appear and disappear selectors must not be triggered by the tab bar view controller
 * itself
 *
 * @param moreTabViewController The more tab view controller asking for confirmation
 * @param tabInformation The tab being selected information
 */
- (void)moreTabViewController:(MoreTabViewController *)moreTabViewController
                  selectedTab:(BBVATabBarTabInformation *)tabInformation;

@end


/**
 * View controller associated to the more tab inside the BBVATabBarViewController. It displays a table with the tabs that couldn't fit
 * inside the tab bar view
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MoreTabViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {

@private
    
    /**
     * Table view to display the tabs that didn't made it to the tab bar
     */
    UITableView *moreTabsTableView_;

    /**
     * Left navigation button
     */
    UIBarButtonItem *leftNavigationButton_;
    
    /**
     * Right navigation button
     */
    UIBarButtonItem *rightNavigationButton_;
    
    /**
     * Container view controller
     */
    BBVATabBarViewController *containerViewController_;
    
    /**
     * Selected table index
     */
    NSIndexPath *selectedIndexPath_;
    
    /**
     * iPhone device flag. YES when the device is an iPhone, NO when it is an iPad
     */
    BOOL iPhoneDevice_;

    /**
     * The list of tabs that couldn't be displayed in the tab bar view
     */
    NSMutableArray *tabsList_;
    
    /**
     * The list of tabs that couldn't be displayed in the tab bar view in their original order (as they where provided to the more tab)
     */
    NSMutableArray *originalTabsList_;
    
    /**
     * Navigation bar color
     */
    UIColor *navigationBarColor_;
    
    /**
     * More tab background color
     */
    UIColor *moreTabBackgroundColor_;
    
    /**
     * More tab table background color
     */
    UIColor *moreTabTableBackgroundColor_;

    /**
     * More tab text
     */
    NSString *moreTabText_;
    
    /**
     * More tab elements ordered alphabetically flag. YES when the more tab elements must be ordered alphabetically, NO when the order is not set
     */
    BOOL moreTabElementsOrdered_;
    
    /**
     * Cell arrow image
     */
    UIImage *cellArrowImage_;
    
    /**
     * Cell separator image
     */
    UIImage *cellSeparatorImage_;
    
    /**
     * Narrow cell layout flag. YES when the cell is a narrow cell with only one text field and the image, NO otherwise
     */
    BOOL cellIsNarrow_;

    /**
     * More tab view controller delegate
     */
    id<MoreTabViewControllerDelegate> moreTabViewControllerDelegate_;
    
}

/**
 * Provides read-write access to the table view to display the tabs that didn't made it to the tab bar and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *moreTabsTableView;

/**
 * Provides read-write access to the left navigation button
 */
@property (nonatomic, readwrite, retain) UIBarButtonItem *leftNavigationButton;

/**
 * Provides read-write access to the right navigation button
 */
@property (nonatomic, readwrite, retain) UIBarButtonItem *rightNavigationButton;

/**
 * Provides read-write access to the container view controller
 */
@property (nonatomic, readwrite, assign) BBVATabBarViewController *containerViewController;

/**
 * Provides read-only access to the iPhone device flag
 */
@property (nonatomic, readwrite, assign) BOOL iPhoneDevice;

/**
 * Provides read-write access to the list of tabs that couldn't be displayed in the tab bar view
 */
@property (nonatomic, readwrite, retain) NSArray *tabsList;

/**
 * Provides read-write access to the navigation bar color
 */
@property (nonatomic, readwrite, retain) UIColor *navigationBarColor;

/**
 * Provides read-write access to the more tab background color
 */
@property (nonatomic, readwrite, retain) UIColor *moreTabBackgroundColor;

/**
 * Provides read-write access to the More tab table background color
 */
@property (nonatomic, readwrite, retain) UIColor *moreTabTableBackgroundColor;

/**
 * Provides read-write access to the more tab text
 */
@property (nonatomic, readwrite, copy) NSString *moreTabText;

/**
 * Provides read-write access to the more tab elements ordered alphabetically
 */
@property (nonatomic, readwrite, assign) BOOL moreTabElementsOrdered;

/**
 * Provides read-write access to the cell arrow image
 */
@property (nonatomic, readwrite, retain) UIImage *cellArrowImage;

/**
 * Provides read-write access to the cell separator image
 */
@property (nonatomic, readwrite, retain) UIImage *cellSeparatorImage;

/**
 * Provides read-write access to the narrow cell layout flag
 */
@property (nonatomic, readwrite, assign) BOOL cellIsNarrow;

/**
 * Provides read-only access to the seleced tab bar information. Only available on iPad
 */
@property (nonatomic, readonly, retain) BBVATabBarTabInformation *selectedTabBar;

/**
 * Provides read-only access to the first tab bar information
 */
@property (nonatomic, readonly, retain) BBVATabBarTabInformation *firstTabBar;

/**
 * Provides read-write access to the more tab view controller delegate
 */
@property (nonatomic, readwrite, assign) id<MoreTabViewControllerDelegate> moreTabViewControllerDelegate;


/**
 * Returns the tab information for the given view controller
 *
 * @param viewController The view controller to look for
 * @return The tab information associated to the view controller, or nil if no suitable view controller is found
 */
- (BBVATabBarTabInformation *)tabInformationForViewController:(UIViewController *)viewController;

/**
 * Selects a given view controller, if found among the tab information
 *
 * @param viewController The view controller to select
 * @return The selected tab information
 */
- (BBVATabBarTabInformation *)selectViewController:(UIViewController *)viewController;

/**
 * Checks whether the more tab  bar contains a view controller or not
 *
 * @param viewController The view controller to check
 * @return The selected tab information
 */
- (BOOL)containsViewController:(UIViewController *)viewController;

/**
 * Resets the more tab bar to the first tab. Only available on iPad. Default implementation does nothing
 */
- (void)resetMoreTabToFirstTab;

@end
