/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BBVATabRelocateView.h"
#import "BBVATabBarConstants.h"
#import "BBVATabBarDrawingUtils.h"
#import "BBVATabBarTabInformation.h"
#import "BBVATabView.h"

#pragma mark -

/**
 * BBVATabRelocateView private extension
 */
@interface BBVATabRelocateView()

/**
 * Calculates the tab width
 *
 * @return The tab width
 * @private
 */
- (CGFloat)tabWidth;

@end


#pragma mark -

@implementation BBVATabRelocateView

#pragma mark -
#pragma mark Drawing methods

/**
 * Draws the receiver’s image within the passed-in rectangle
 *
 * @param rect A rectangle defining the area to restrict drawing to
 */
- (void)drawRect:(CGRect)rect {
    
    CGRect frame = self.frame;
    CGFloat width = frame.size.width;
    CGFloat height = frame.size.height;
    CGFloat barTop = height - BBVA_I_PHONE_TAB_BAR_VIEW_HEIGHT;
    
    CGRect drawingRect = CGRectMake(0.0f, barTop, width, BBVA_I_PHONE_TAB_BAR_VIEW_HEIGHT);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    CGContextSetAllowsAntialiasing(context, true);
    
    [BBVATabBarDrawingUtils drawBackgroundGradientIntoContext:context
                                                  withinFrame:drawingRect
                                             foriPhoneTabType:YES];
    
    NSUInteger tabsCount = 5;//[tabBarViewsArray_ count];
    
    if (tabsCount > 0) {
        
        CGFloat tabWidth = [self tabWidth];
        
        if (tabsCount > 1) {
            
            for (NSUInteger i = 1; i < tabsCount; i++) {
                
                CGFloat separatorPosition = ((CGFloat) i) * tabWidth;
                
                [BBVATabBarDrawingUtils drawTabSeparatorIntoContext:context
                                                            fromMin:barTop
                                                              toMax:height
                                                         atPosition:separatorPosition
                                                   foriPhoneTabType:YES];
                
            }
            
        }
        
    }
    
    CGContextRestoreGState(context);
    

}

#pragma mark -
#pragma mark Utility selectors

/*
 * Calculates the tab width
 */
- (CGFloat)tabWidth {
    
    CGFloat result = 0.0f;
    
    CGRect frame = self.frame;
    CGFloat width = frame.size.width;
    NSUInteger tabsCount = 5;//[tabBarViewsArray_ count];
    
    if (tabsCount > 0) {
        
        result = round(width / (CGFloat)tabsCount);
        
    } else {
        
        result = width;
        
    }
    
    return result;
    
}

@end
