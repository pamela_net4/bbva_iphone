/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MoreTabViewController.h"
#import "BBVATabBarViewControllerView.h"
#import "MoreTab_iPadProtocols.h"


//Forward declarations
@class BBVATabBarTabInformation;


/**
 * View controller associated to the more tab inside the BBVATabBarViewController in an iPad application. It displays a table with the tabs that couldn't fit
 * inside the tab bar view, and the initial view for the selected tab
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MoreTab_iPadViewController : MoreTabViewController <MoreTab_iPadManager, BBVATabBarViewControllerViewDelegate> {
    
@private
    
    /**
     * Table container view
     */
    UIView *tableContainerView_;
    
    /**
     * Table background image view
     */
    UIImageView *tableBackgroundImageView_;
    
    /**
     * Current displayed view controller
     */
    UIViewController *currentViewController_;
    
    /**
     * Table background image
     */
    UIImage *tableBackgroundImage_;
    
    /**
     * More tab table collapsed flag. YES when the more tab table is collapsed, NO otherwise
     */
    BOOL tableCollapsed_;
    
    /**
     * View visible flag. YES when the view is being displayed, NO otherwise
     */
    BOOL viewVisible_;

    /**
     * New version flag. NO when iOS version is older than 5.0, YES otherwise
     */
    BOOL newOSVersion_;
    
    /**
     * Must propagate viewWillAppear for new version flag. YES when the current view controller viewWillAppear is invoked when no superview is available, so the automatic
     * viewWillAppear when adding the view is not propagated, so it must be forced inside the viewDidAppear. NO otherwise
     */
    BOOL mustPropagateViewWillAppearForNewVersion_;
    
    /**
     * Selected tab bar
     */
    BBVATabBarTabInformation *selectedTabBar_;

}


/**
 * Provides read-write access to the table container view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *tableContainerView;

/**
 * Provides read-write access to the table background image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *tableBackgroundImageView;


/**
 * Returns a new autoreleased MoreTab_iPadViewController created from its NIB file
 *
 * @return The new autoreleased MoreTab_iPadViewController
 */
+ (MoreTab_iPadViewController *)moreTab_iPadViewController;

@end
