/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BBVATabBarViewController.h"
#import "BBVATabBarConstants.h"
#import "BBVATabBarTabInformation.h"
#import "MoreTab_iPadViewController.h"
#import "MoreTab_iPhoneViewController.h"
#import "MoreTabViewController.h"


/**
 * Defines the hide/show tab bar animation duration (measured in seconds)
 */
#define TAB_BAR_ANIMATION_DURATION                                          0.15f

/**
 * Defines the hide tab bar animation identification
 */
#define HIDE_TAB_BAR_ANIMATION_ID                                           @"animation.BBVATabBarViewController.hideTabBar"

/**
 * Defines the show tab bar animation identification
 */
#define SHOW_TAB_BAR_ANIMATION_ID                                           @"animation.BBVATabBarViewController.showTabBar"


#pragma mark -
/**
 * BBVATabBarViewController private extension
 */
@interface BBVATabBarViewController()

/**
 * Releases the graphic elements not needed when view is hidden
 *
 * @private
 */
- (void)releaseBBVATabBarViewControllerGraphicElements;

/**
 * Creates the more tab bar tab information element. It assumes it is not allocated, so no check is performed
 *
 * @private
 */
- (void)createMoreTabBarTab;

/**
 * Invoked by framework when application will change the status bar frame. The view layout is recalculater
 *
 * @param notification The NSNotification instance containing the notification information
 * @private
 */
- (void)statusBarFrameWillChange:(NSNotification *)notification;

/**
 * Initializes the tab bar and the more tab
 *
 * @private
 */
- (void)initializeTabBarAndMoreTab;

/**
 * Checks the application frame and sets the view size
 *
 * @private
 */
- (void)checkViewFrame;

/**
 * Shows the tab bar
 *
 * @private
 */
- (void)showTabBar;

/**
 * Hides the tab bar
 *
 * @private
 */
- (void)hideTabBar;

/**
 * Collapses the tab bar
 *
 * @private
 */
- (void)collapseTabBar;

/**
 * Expands the tab bar
 *
 * @private
 */
- (void)expandTabBar;

/**
 * The tab bar animation did stop event selector. When hidding, the tab bar hidden propertie is updated
 *
 * @param animationID An NSString containing an optional application-supplied identifier
 * @param finished An NSNumber object containing a Boolean value. The value is YES if the animation ran to completion before it stopped or NO if it did not
 * @param context An optional application-supplied context
 * @private
 */
- (void)tabBarAnimationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context;

/**
 * Invoked when the hide tab bar animation is finished. The delegate is notified and user interaction is restored
 *
 * @private
 */
- (void)hideTabBarAnimationFinished;

/**
 * Invoked when the show tab bar animation is finished. The delegate is notified and user interaction is restored
 *
 * @private
 */
- (void)showTabBarAnimationFinished;

/**
 * Recalculates the tab bar and current view sizes and location
 *
 * @private
 */
- (void)recalculateLayout;

/**
 * Calculates the child view frame
 *
 * @return The calculated child view frame
 * @private
 */
- (CGRect)calculateChildFrame;

/**
 * Returns the tab bar selected view controller
 *
 * @return The tab bar selected view controller
 * @private
 */
- (UIViewController *)tabBarSelectedViewController;

/**
 * Adapts the graphic interface to the provided interface orientation. It has only meaning on iPad
 *
 * @param interfaceOrientation The interface orientation
 * @private
 */
- (void)adaptGraphicInterfaceToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;

/**
 * Pops the provided view controller to its root view controller in case it is a navigation controller
 *
 * @param viewController View controller to check and pop if necessary
 * @private
 */
- (void)popViewControllerToRoot:(UIViewController *)viewController;

/**
 * Pushes the provided view controller into the more tab.
 *
 * @param viewController The view controller to push into the more tab.
 * @private
 */
- (void)doPushViewControllerIntoMoreTab:(UIViewController *)viewController;

@end


#pragma mark -

@implementation BBVATabBarViewController

#pragma mark -
#pragma mark Properties

@synthesize moreTabNavigationController = moreTabNavigationController_;
@synthesize selectedViewController = selectedViewController_;
@synthesize tabBarOnRightHandSide = tabBarOnRightHandSide_;
@synthesize collapseTabBarOnPortrait = collapseTabBarOnPortrait_;
@dynamic hidden;
@synthesize navigationBarColor = navigationBarColor_;
@synthesize moreTabBackgroundColor = moreTabBackgroundColor_;
@synthesize moreTabTableBackgroundColor = moreTabTableBackgroundColor_;
@synthesize moreTabText = moreTabText_;
@synthesize moreTabReferenceImage = moreTabReferenceImage_;
@synthesize moreTabSelectedImage = moreTabSelectedImage_;
@synthesize moreTabSelectedColor = moreTabSelectedColor_;
@synthesize moreTabUnselectedImage = moreTabUnselectedImage_;
@synthesize moreTabUnselectedColor = moreTabUnselectedColor_;
@synthesize moreTabDisabledImage = moreTabDisabledImage_;
@synthesize moreTabDisabledColor = moreTabDisabledColor_;
@synthesize moreTabElementsOrdered = moreTabElementsOrdered_;
@synthesize moreTabResetOnSelection = moreTabResetOnSelection_;
@synthesize cellArrowImage = cellArrowImage_;
@synthesize cellSeparatorImage = cellSeparatorImage_;
@synthesize relocateNavigationBarClass = relocateNavigationBarClass_;
@synthesize relocateViewBackgroundColor = relocateViewBackgroundColor_;
@synthesize relocateTitleTextColor = relocateTitleTextColor_;
@synthesize relocateHeaderBackgroundImage = relocateHeaderBackgroundImage_;
@synthesize relocateTabNavigationTitle = relocateTabNavigationTitle_;
@synthesize relocateTabHeaderTitle = relocateTabHeaderTitle_;
@synthesize relocateTabResetText = relocateTabResetText_;
@synthesize relocateTabDoneText = relocateTabDoneText_;
@synthesize collapseTabRightArrowImage = collapseTabRightArrowImage_;
@synthesize collapseTabLeftArrowImage = collapseTabLeftArrowImage_;
@synthesize tabBarInformationText = tabBarInformationText_;
@synthesize tabBarLogoImage = tabBarLogoImage_;
@synthesize selectedTabBackgroundColor = selectedTabBackgroundColor_;
@synthesize moreTabCellIsNarrow = moreTabCellIsNarrow_;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Static attributes

/**
 * Defines the BBVATabBarViewController NIB file name
 */
NSString * const kNibFileName = @"BBVATabBarViewController";

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self
                                  name:UIApplicationWillChangeStatusBarFrameNotification
                                object:nil];
    
    [self releaseBBVATabBarViewControllerGraphicElements];
    
    [moreTabNavigationController_ release];
    moreTabNavigationController_ = nil;
    
    [moreTabsViewController_ release];
    moreTabsViewController_ = nil;
    
    [relocateTabsViewController_ release];
    relocateTabsViewController_ = nil;
    
    [selectedViewController_ release];
    selectedViewController_ = nil;
    
    [moreLeftNavigationButton_ release];
    moreLeftNavigationButton_ = nil;
    
    [moreRightNavigationButton_ release];
    moreRightNavigationButton_ = nil;
    
    [selectedTabInformation_ release];
    selectedTabInformation_ = nil;
    
    [tabsList_ release];
    tabsList_ = nil;
    
    [tabsDefaultOrderList_ release];
    tabsDefaultOrderList_ = nil;
    
    [moreTabInformation_ release];
    moreTabInformation_ = nil;
    
    [navigationBarColor_ release];
    navigationBarColor_ = nil;
    
    [moreTabBackgroundColor_ release];
    moreTabBackgroundColor_ = nil;
    
    [moreTabTableBackgroundColor_ release];
    moreTabTableBackgroundColor_ = nil;
    
    [moreTabText_ release];
    moreTabText_ = nil;
    
    [moreTabReferenceImage_ release];
    moreTabReferenceImage_ = nil;
    
    [moreTabSelectedImage_ release];
    moreTabSelectedImage_ = nil;
    
    [moreTabSelectedColor_ release];
    moreTabSelectedColor_ = nil;
    
    [moreTabUnselectedImage_ release];
    moreTabUnselectedImage_ = nil;
    
    [moreTabUnselectedColor_ release];
    moreTabUnselectedColor_ = nil;
    
    [moreTabDisabledImage_ release];
    moreTabDisabledImage_ = nil;
    
    [moreTabDisabledColor_ release];
    moreTabDisabledColor_ = nil;
    
    [cellArrowImage_ release];
    cellArrowImage_ = nil;
    
    [cellSeparatorImage_ release];
    cellSeparatorImage_ = nil;
    
    [relocateNavigationBarClass_ release];
    relocateNavigationBarClass_ = nil;
    
    [relocateViewBackgroundColor_ release];
    relocateViewBackgroundColor_ = nil;
    
    [relocateTitleTextColor_ release];
    relocateTitleTextColor_ = nil;
    
    [relocateHeaderBackgroundImage_ release];
    relocateHeaderBackgroundImage_ = nil;
    
    [relocateTabNavigationTitle_ release];
    relocateTabNavigationTitle_ = nil;
    
    [relocateTabHeaderTitle_ release];
    relocateTabHeaderTitle_ = nil;
    
    [relocateTabResetText_ release];
    relocateTabResetText_ = nil;
    
    [relocateTabDoneText_ release];
    relocateTabDoneText_ = nil;
    
    [collapseTabRightArrowImage_ release];
    collapseTabRightArrowImage_ = nil;
    
    [collapseTabLeftArrowImage_ release];
    collapseTabLeftArrowImage_ = nil;
    
    [tabBarInformationText_ release];
    tabBarInformationText_ = nil;
    
    [tabBarLogoImage_ release];
    tabBarLogoImage_ = nil;
    
    [selectedTabBackgroundColor_ release];
    selectedTabBackgroundColor_ = nil;
    
    delegate_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory. The tab bar is released
 */
- (void)viewDidUnload {
    
    [self releaseBBVATabBarViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/*
 * Releases the graphic elements not needed when view is hidden
 */
- (void)releaseBBVATabBarViewControllerGraphicElements {
    
    tabBarView_.bbvaTabBarcontainerViewDelegate = nil;
    [tabBarView_ release];
    tabBarView_ = nil;
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseBBVATabBarViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. The tab list is created
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    iPhoneDevice_ = YES;
    
#ifdef UI_USER_INTERFACE_IDIOM
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        iPhoneDevice_ = NO;
        
    }
    
#endif
    
    collapseTabBarOnPortrait_ = YES;
    
    if (tabsList_ == nil) {
        
        tabsList_ = [[NSMutableArray allocWithZone:self.zone] init];
        
    }
    
    if (tabsDefaultOrderList_ == nil) {
        
        tabsDefaultOrderList_ = [[NSMutableArray allocWithZone:self.zone] init];
        
    }
    
    tabBarDisplayed_ = YES;
    
    
    NSString *iOSVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([iOSVersion compare:@"5.0" options:NSNumericSearch] != NSOrderedAscending) {
        
        newOSVersion_ = YES;
        
    } else {
        
        newOSVersion_ = NO;
        
    }
    
}

/**
 * Creates and returns an autoreleased BBVATabBarViewController constructed from a NIB file
 *
 * @return The autoreleased BBVATabBarViewController constructed from a NIB file
 */
+ (BBVATabBarViewController *)bbvaTabBarViewController {
    
    BBVATabBarViewController *result = [[[BBVATabBarViewController alloc] initWithNibName:kNibFileName
                                                                                   bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark View management

/**
 * Called after the controller’s view is loaded into memory. The view controller is set as the tab bar delegate
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIView *ownView = self.view;
    
    ownView.clipsToBounds = YES;
    
    CGRect frame = ownView.frame;
    CGRect auxFrame = frame;
    
    NSZone *zone = self.zone;
    
    if (tabBarView_ == nil) {
        
        auxFrame.origin.x = 0.0f;
        
        if (iPhoneDevice_) {
            
            CGFloat tabHeight = [BBVATabBarContainerView iPhoneTabBarContainerHeight];
            auxFrame.origin.y = CGRectGetMaxY(frame) - tabHeight;
            auxFrame.size.height = tabHeight;
            
        } else {
            
            CGFloat tabWidth = [BBVATabBarContainerView iPadTabBarContainerWidth];
            auxFrame.origin.y = 0.0f;
            auxFrame.size.width = tabWidth;
            
        }
        
        tabBarView_ = [[BBVATabBarContainerView allocWithZone:zone] initWithFrame:auxFrame];
        tabBarView_.bbvaTabBarcontainerViewDelegate = self;
        tabBarView_.rightArrowImage = collapseTabRightArrowImage_;
        tabBarView_.leftArrowImage = collapseTabLeftArrowImage_;
        tabBarView_.informationText = tabBarInformationText_;
        tabBarView_.logoImage = tabBarLogoImage_;
        tabBarView_.selectedTabBackgroundColor = selectedTabBackgroundColor_;
        tabBarView_.tabBarOnRightHandSide = tabBarOnRightHandSide_;
        [ownView addSubview:tabBarView_];
        
    }
    
    [ownView bringSubviewToFront:tabBarView_];
    
    tabBarView_.bbvaTabBarcontainerViewDelegate = self;
    UIViewController *selectedViewController = [[selectedViewController_ retain] autorelease];
    BOOL currentTabIsMoreTab = currentTabIsMoreTab_;
    [self initializeTabBarAndMoreTab];
    
    if (!currentTabIsMoreTab) {
        
        [self selectViewController:selectedViewController];
        
    } else {
        
        BBVATabBarTabInformation *selectedTabInformation = [moreTabsViewController_ tabInformationForViewController:selectedViewController];
        [tabBarView_ selectViewController:moreTabsViewController_.navigationController];
        
        if (selectedTabInformation != selectedTabInformation_) {
            
            [selectedTabInformation retain];
            [selectedTabInformation_ release];
            selectedTabInformation_ = selectedTabInformation;
            
        }
        
        if (selectedViewController != selectedViewController_) {
            
            [selectedViewController retain];
            [selectedViewController_ release];
            selectedViewController_ = selectedViewController;
            
        }
        
    }
    
    [[self view] setBackgroundColor:[UIColor clearColor]];
    
    [tabBarView_ setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [tabBarView_ setAutoresizesSubviews:YES];
    
    if (!iPhoneDevice_) {
        
        [self adaptGraphicInterfaceToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
        
    }
    
    [self recalculateLayout];
    
}

/**
 * Notifies the view controller that its view is about to be become visible. The selected view controller is notified and registers to
 * the status bar frame is about to change notification
 *
 * @param animated If YES, the view is being added to the window using an animation
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    viewVisible_ = YES;
    
    
    if (!newOSVersion_) {
        
        [[self tabBarSelectedViewController] viewWillAppear:animated];
        
    }
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self
                           selector:@selector(statusBarFrameWillChange:)
                               name:UIApplicationWillChangeStatusBarFrameNotification
                             object:nil];
    
    if (!iPhoneDevice_) {
        
        self.view.backgroundColor = [UIColor blackColor];
        [self adaptGraphicInterfaceToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
        
    }
    
}

/**
 * Notifies the view controller that its view was added to a window. The selected view controller is notified
 *
 * @param animated If YES, the view was added to the window using an animation
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (!newOSVersion_) {
        
        [[self tabBarSelectedViewController] viewDidAppear:animated];
        
    }
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. The selected view controller
 * is notified
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    if (!newOSVersion_) {
        
        [[self tabBarSelectedViewController] viewWillDisappear:animated];
        
    }
    
}

/**
 * Notifies the view controller that its view was dismissed, covered, or otherwise hidden from view. The selected view controller is notified.
 * Unregisters from the status bar frame is about to change notification
 *
 * @param animated If YES, the disappearance of the view was animated
 */
- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    viewVisible_ = NO;
    
    if (!newOSVersion_) {
        
        [[self tabBarSelectedViewController] viewDidDisappear:animated];
        
    }
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self
                                  name:UIApplicationWillChangeStatusBarFrameNotification
                                object:nil];
    
}

/**
 * Returns a Boolean value indicating whether the view controller supports the specified orientation. The selected view controller is
 * requested to decide the result
 *
 * @param interfaceOrientation The orientation of the application’s user interface after the rotation
 * @return YES when the embedded navigation controller allows the interface orientation, NO otherwise
 */
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    UIViewController *viewController = [self tabBarSelectedViewController];
    
    if (viewController == nil) {
        
        if ([tabsList_ count] > 0) {
            
            BBVATabBarTabInformation *firstTab = [tabsList_ objectAtIndex:0];
            viewController = firstTab.viewController;
            
        }
        
    }
    
    return [viewController shouldAutorotateToInterfaceOrientation:interfaceOrientation];
    
}

/**
 * Sent to the view controller just before the user interface begins rotating. The event is propagated to the selected tab view controller and the interface is adapted
 *
 * @param toInterfaceOrientation The new orientation for the user interface
 * @param duration The duration of the pending rotation, measured in seconds
 */
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation
                                   duration:duration];
    
    [[self tabBarSelectedViewController] willRotateToInterfaceOrientation:toInterfaceOrientation
                                                                 duration:duration];
    
    if (!iPhoneDevice_) {
        
        [self adaptGraphicInterfaceToInterfaceOrientation:toInterfaceOrientation];
        
    }
    
}

/**
 * Sent to the view controller after the user interface rotates. The event is propagated to the selected tab view controller
 *
 * @param fromInterfaceOrientation The old orientation of the user interface
 */
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    [[self tabBarSelectedViewController] didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
}

/**
 * Sent to the view controller before performing a one-step user interface rotation. The event is propagated to the selected tab view controller
 *
 * @param interfaceOrientation The new orientation for the user interface
 * @param duration The duration of the pending rotation, measured in seconds
 */
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
                                         duration:(NSTimeInterval)duration {
    
    [super willAnimateRotationToInterfaceOrientation:interfaceOrientation
                                            duration:duration];
    
    [[self tabBarSelectedViewController] willAnimateRotationToInterfaceOrientation:interfaceOrientation
                                                                          duration:duration];
    
}

/**
 * Sent to the view controller before performing the first half of a user interface rotation. The event is propagated to the selected tab view controller
 *
 * @param toInterfaceOrientation The state of the application’s user interface orientation before the rotation
 * @param duration The duration of the first half of the pending rotation, measured in seconds
 */
- (void)willAnimateFirstHalfOfRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                                    duration:(NSTimeInterval)duration {
    
    [super willAnimateFirstHalfOfRotationToInterfaceOrientation:toInterfaceOrientation
                                                       duration:duration];
    
    [[self tabBarSelectedViewController] willAnimateFirstHalfOfRotationToInterfaceOrientation:toInterfaceOrientation
                                                                                     duration:duration];
    
}

/**
 * Sent to the view controller before the second half of the user interface rotates. The event is propagated to the selected tab view controller
 *
 * @param fromInterfaceOrientation The state of the application’s user interface orientation before the rotation
 * @param duration The duration of the second half of the pending rotation, measured in seconds
 */
- (void)willAnimateSecondHalfOfRotationFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
                                                       duration:(NSTimeInterval)duration {
    
    [super willAnimateSecondHalfOfRotationFromInterfaceOrientation:fromInterfaceOrientation
                                                          duration:duration];
    
    [[self tabBarSelectedViewController] willAnimateSecondHalfOfRotationFromInterfaceOrientation:fromInterfaceOrientation
                                                                                        duration:duration];
    
}

/**
 * Sent to the view controller after the completion of the first half of the user interface rotation. The event is propagated to the selected tab view controller
 *
 * @param toInterfaceOrientation The state of the application’s user interface orientation after the rotation
 */
- (void)didAnimateFirstHalfOfRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    
    [super didAnimateFirstHalfOfRotationToInterfaceOrientation:toInterfaceOrientation];
    
    [[self tabBarSelectedViewController] didAnimateFirstHalfOfRotationToInterfaceOrientation:toInterfaceOrientation];
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Recalculates the tab bar and current view sizes and location
 */
- (void)recalculateLayout {
    
    if ([self isViewLoaded]) {
        
        UIView *ownView = self.view;
        CGRect bounds = ownView.bounds;
        CGFloat boundsWidth = CGRectGetMaxX(bounds) - CGRectGetMinX(bounds);
        CGFloat boundsHeight = CGRectGetMaxY(bounds) - CGRectGetMinY(bounds);
        
        UIViewController *selectedViewController = [self tabBarSelectedViewController];
        
        UIView *currentView = selectedViewController.view;
        
        CGRect tabBarFrame = tabBarView_.frame;
        CGRect childFrame = bounds;
        childFrame.origin.x = 0.0f;
        childFrame.origin.y = 0.0f;
        
        if (iPhoneDevice_) {
            
            CGFloat tabBarHeight = [BBVATabBarContainerView iPhoneTabBarContainerHeight];
            tabBarFrame.size.width = boundsWidth;
            tabBarFrame.origin.x = 0.0f;
            tabBarFrame.size.height = tabBarHeight;
            
            if (tabBarDisplayed_) {
                
                tabBarFrame.origin.y = boundsHeight - tabBarHeight;
                childFrame.size.height = CGRectGetMinY(tabBarFrame);
                
            } else {
                
                tabBarFrame.origin.y = boundsHeight;
                childFrame.size.height = boundsHeight;
                
            }
            
        } else {
            
            tabBarFrame.size.height = boundsHeight;
            CGFloat tabBarWidth = tabBarFrame.size.width;
            tabBarFrame.origin.y = 0.0f;
            tabBarFrame.origin.x = 0.0f;
            
            CGFloat tabButtonsWidth = [BBVATabBarView iPadTabBarWidth];
            childFrame.origin.x = tabButtonsWidth;
            childFrame.size.width = boundsWidth - tabButtonsWidth;
            
            BOOL tabBarOnRightHandSide = tabBarView_.tabBarOnRightHandSide;
            BOOL collapsed = tabBarView_.tabBarCollapsed;
            
            if (((!tabBarDisplayed_) || (collapsed)) && (!tabBarOnRightHandSide)) {
                
                tabBarFrame.origin.x = -tabButtonsWidth;
                childFrame.origin.x = 0.0f;
                childFrame.size.width = boundsWidth;
                
            } else if ((tabBarDisplayed_) && (!collapsed) && (tabBarOnRightHandSide)) {
                
                tabBarFrame.origin.x = boundsWidth - tabBarWidth;
                childFrame.origin.x = 0.0f;
                
            } else if (((!tabBarDisplayed_) || (collapsed)) && (tabBarOnRightHandSide)) {
                
                tabBarFrame.origin.x = boundsWidth - tabBarWidth + tabButtonsWidth;
                childFrame.origin.x = 0.0f;
                childFrame.size.width = boundsWidth;
                
            }
            
            if (floatingTabBar_) {
                
                childFrame.origin.x = 0.0f;
                childFrame.size.width = boundsWidth;
                
            }
            
        }
        
        tabBarView_.frame = tabBarFrame;
        currentView.frame = childFrame;
        
    }
    
}

/*
 * Calculates the child view frame
 */
- (CGRect)calculateChildFrame {
    
    CGRect result = CGRectZero;
    
    UIView *ownView = self.view;
    CGRect frame = ownView.bounds;
    CGFloat frameWidth = frame.size.width;
    CGFloat frameHeight = frame.size.height;
    
    result = frame;
    result.origin.x = 0.0f;
    result.origin.y = 0.0f;
    
    if (iPhoneDevice_) {
        
        if (tabBarDisplayed_) {
            
            CGFloat tabBarHeight = [BBVATabBarContainerView iPhoneTabBarContainerHeight];
            result.size.height = frameHeight - tabBarHeight;
            
        } else {
            
            result.size.height = frameHeight;
            
        }
        
    } else {
        
        CGFloat tabButtonsWidth = [BBVATabBarView iPadTabBarWidth];
        result.origin.x = tabButtonsWidth;
        result.size.width = frameWidth - tabButtonsWidth;
        
        BOOL tabBarOnRightHandSide = tabBarView_.tabBarOnRightHandSide;
        BOOL collapsed = tabBarView_.tabBarCollapsed;
        
        if (((!tabBarDisplayed_) || (collapsed)) && (!tabBarOnRightHandSide)) {
            
            result.origin.x = 0.0f;
            result.size.width = frameWidth;
            
        } else if ((tabBarDisplayed_) && (!collapsed) && (tabBarOnRightHandSide)) {
            
            result.origin.x = 0.0f;
            
        } else if (((!tabBarDisplayed_) || (collapsed)) && (tabBarOnRightHandSide)) {
            
            result.origin.x = 0.0f;
            result.size.width = frameWidth;
            
        }
        
        if (floatingTabBar_) {
            
            result.origin.x = 0.0f;
            result.size.width = frameWidth;
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the tab bar selected view controller
 */
- (UIViewController *)tabBarSelectedViewController {
    
    UIViewController *result = selectedViewController_;
    
    if (currentTabIsMoreTab_) {
        
        UINavigationController *moreNavigationController = moreTabsViewController_.navigationController;
        
        if (moreNavigationController != nil) {
            
            result = moreNavigationController;
            
        } else {
            
            result = moreTabsViewController_;
            
        }
        
    }
    
    return result;
    
}

/*
 * Pops the provided view controller to its root view controller in case it is a navigation controller
 */
- (void)popViewControllerToRoot:(UIViewController *)viewController {
    
    if ([viewController isKindOfClass:[UINavigationController class]]) {
        
        UINavigationController *navigationController = (UINavigationController *)viewController;
        
        NSArray *viewControllersArray = [navigationController viewControllers];
        
        if ([viewControllersArray count] > 0) {
            
            UIViewController *rootViewController = [viewControllersArray objectAtIndex:0];
            [navigationController setViewControllers:[NSArray arrayWithObject:rootViewController]];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark Tabs initialization

/*
 * Sets the default tabs order array and the current tabs list order array
 */
- (void)setDefaultTabsOrderArray:(NSArray *)defaultTabsOrderArray
           currentTabsOrderArray:(NSArray *)currentTabsOrderArray {
    
    if (iPhoneDevice_) {
        
        maximumTabsInTabBarCount_ = BBVA_TAB_BAR_MAX_TABS_ON_IPHONE_BAR;
        
    } else {
        
        maximumTabsInTabBarCount_ = BBVA_TAB_BAR_MAX_TABS_ON_IPAD_BAR;
        
    }
    
    tabsInTabBarCount_ = maximumTabsInTabBarCount_ - 1;
    defaultTabsInTabBarCount_ = maximumTabsInTabBarCount_ - 1;
    
    [self setDefaultTabsOrderArray:defaultTabsOrderArray
             currentTabsOrderArray:currentTabsOrderArray
                 tabsInTabBarCount:tabsInTabBarCount_
          defaultTabsInTabBarCount:defaultTabsInTabBarCount_
          maximumTabsInTabBarCount:maximumTabsInTabBarCount_];
    
}

/*
 * Sets the default tabs order array, the current tabs list order array and the tabs in tab bar count elements. This selector has more meaing in iPad version. In iPhone version
 * tabs count elements are set to fixed values
 */
- (void)setDefaultTabsOrderArray:(NSArray *)defaultTabsOrderArray
           currentTabsOrderArray:(NSArray *)currentTabsOrderArray
               tabsInTabBarCount:(NSUInteger)tabsInTabBarCount
        defaultTabsInTabBarCount:(NSUInteger)defaultTabsInTabBarCount
        maximumTabsInTabBarCount:(NSUInteger)maximumTabsInTabBarCount {
    
    if ([BBVATabBarTabInformation fillArray:tabsList_ withBBVATabBarTabInformationInstancesFromOrigin:currentTabsOrderArray]) {
        
        if (defaultTabsOrderArray != tabsDefaultOrderList_) {
            
            [tabsDefaultOrderList_ removeAllObjects];
            
            Class bbvaTabBarTabInformation = [BBVATabBarTabInformation class];
            
            for (NSObject *storedObject in defaultTabsOrderArray) {
                
                if ([storedObject isKindOfClass:bbvaTabBarTabInformation]) {
                    
                    [tabsDefaultOrderList_ addObject:storedObject];
                    
                }
                
            }
            
        }
        
        if (iPhoneDevice_) {
            
            maximumTabsInTabBarCount_ = BBVA_TAB_BAR_MAX_TABS_ON_IPHONE_BAR;
            tabsInTabBarCount_ = maximumTabsInTabBarCount_;
            defaultTabsInTabBarCount_ = maximumTabsInTabBarCount_;
            
        } else {
            
            maximumTabsInTabBarCount_ = BBVA_TAB_BAR_MAX_TABS_ON_IPAD_BAR;
            tabsInTabBarCount_ = tabsInTabBarCount;
            defaultTabsInTabBarCount_ = defaultTabsInTabBarCount;
            
        }
        
        [self initializeTabBarAndMoreTab];
        
    }
    
}

/*
 * Initializes the tab bar and the more tab
 */
- (void)initializeTabBarAndMoreTab {
    
    NSUInteger tabsListCount = [tabsList_ count];
    
    if (tabsListCount <= tabsInTabBarCount_) {
        
        [tabBarView_ setTabsList:tabsList_];
        
        if (tabsListCount < tabsInTabBarCount_) {
            
            tabsInTabBarCount_ = tabsListCount;
            
        }
        
        if (tabsListCount < defaultTabsInTabBarCount_) {
            
            defaultTabsInTabBarCount_ = tabsListCount;
            
        }
        
    } else {
        
        NSRange range;
        
        if (tabsInTabBarCount_ >= maximumTabsInTabBarCount_) {
            
            tabsInTabBarCount_ = maximumTabsInTabBarCount_ - 1;
            
        }
        
        if (defaultTabsInTabBarCount_ >= maximumTabsInTabBarCount_) {
            
            defaultTabsInTabBarCount_ = maximumTabsInTabBarCount_ - 1;
            
        }
        
        range.length = tabsInTabBarCount_;
        range.location = 0;
        
        NSMutableArray *visibleTabs = [NSMutableArray arrayWithArray:[tabsList_ subarrayWithRange:range]];
        
        range.location = tabsInTabBarCount_;
        range.length = tabsListCount - tabsInTabBarCount_;
        
        NSArray *moreTabTabs = [tabsList_ subarrayWithRange:range];
        NSMutableArray *finalMoreTabTabs = [NSMutableArray array];
        Class bbvaTabBarTabInformationClass = [BBVATabBarTabInformation class];
        BBVATabBarTabInformation *tabBarInformation = nil;
        
        for (NSObject *object in moreTabTabs) {
            
            if ([object isKindOfClass:bbvaTabBarTabInformationClass]) {
                
                tabBarInformation = (BBVATabBarTabInformation *)object;
                
                if (tabBarInformation.canShowOnMoreTab) {
                    
                    [finalMoreTabTabs addObject:tabBarInformation];
                    
                }
                
            }
            
        }
        
        if ([finalMoreTabTabs count] > 0) {
            
            if (moreTabInformation_ == nil) {
                
                [self createMoreTabBarTab];
                
            }
            
            moreTabsViewController_.tabsList = finalMoreTabTabs;
            [visibleTabs addObject:moreTabInformation_];
            
        }
        
        [tabBarView_ setTabsList:visibleTabs];
        
    }
    
}

#pragma mark -
#pragma mark Tab selection

/*
 * Selects a given view controller, if found among the tab information. It forwards the operation to its tab bar view
 */
- (BOOL)selectViewController:(UIViewController *)viewController {
    
    BOOL result = NO;
    
    BBVATabBarTabInformation *selectedTabInformation = [tabBarView_ selectViewController:viewController];
    
    if (selectedTabInformation == nil) {
        
        if ([moreTabsViewController_ containsViewController:viewController]) {
            
            [tabBarView_ selectViewController:moreTabInformation_.viewController];
            selectedTabInformation = [moreTabsViewController_ selectViewController:viewController];
            
        }
        
    }
    
    if (selectedTabInformation != nil) {
        
        if (selectedTabInformation != selectedTabInformation_) {
            
            [selectedTabInformation retain];
            [selectedTabInformation_ release];
            selectedTabInformation_ = selectedTabInformation;
            
        }
        
        result = YES;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Tab bar view management

/*
 * Sets the tab bar view visibility animated or not. When animated, the tab var is moved down.
 */
- (void)setTabBarVisibility:(BOOL)visible
                   animated:(BOOL)animated {
    
    if (tabBarDisplayed_ != visible) {
        
        UIViewController *displayedViewController = selectedViewController_;
        
        if ([moreTabsViewController_.tabsList containsObject:selectedTabInformation_]) {
            
            displayedViewController = moreTabsViewController_.navigationController;
            
        }
        
        if (animated) {
            
            tabBarView_.userInteractionEnabled = NO;
            displayedViewController.view.userInteractionEnabled = NO;
            
            if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
                
                [UIView animateWithDuration:TAB_BAR_ANIMATION_DURATION
                                      delay:0.0f
                                    options:(UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState)
                                 animations:^{
                                     
                                     if (visible) {
                                         
                                         [self showTabBar];
                                         
                                     } else {
                                         
                                         [self hideTabBar];
                                         
                                     }
                                     
                                 }
                                 completion:^(BOOL finished) {
                                     
                                     if (visible) {
                                         
                                         [self showTabBarAnimationFinished];
                                         
                                     } else {
                                         
                                         [self hideTabBarAnimationFinished];
                                         
                                     }
                                     
                                 }];
                
            } else {
                
                NSString *animationId = SHOW_TAB_BAR_ANIMATION_ID;
                
                if (!visible) {
                    
                    animationId = HIDE_TAB_BAR_ANIMATION_ID;
                    
                }
                
                [UIView beginAnimations:animationId context:nil];
                [UIView setAnimationDuration:TAB_BAR_ANIMATION_DURATION];
                [UIView setAnimationBeginsFromCurrentState:YES];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                [UIView setAnimationDelegate:self];
                [UIView setAnimationDidStopSelector:@selector(tabBarAnimationDidStop:finished:context:)];
                
                if (visible) {
                    
                    [self showTabBar];
                    
                } else {
                    
                    [self hideTabBar];
                    
                }
                
                [UIView commitAnimations];
                
            }
            
        } else {
            
            if (visible) {
                
                [self showTabBar];
                [self showTabBarAnimationFinished];
                [delegate_ tabBarViewControllerTabBarDidAppear:self];
                
            } else {
                
                [self hideTabBar];
                [self hideTabBarAnimationFinished];
                [delegate_ tabBarViewControllerTabBarDidDisappear:self];
                
            }
            
        }
        
        tabBarDisplayed_ = visible;
        
    }
    
}

/*
 * Shows the tab bar
 */
- (void)showTabBar {
    
    if ([tabBarView_ superview] == nil) {
        
        UIView *ownView = self.view;
        [ownView addSubview:tabBarView_];
        [ownView bringSubviewToFront:tabBarView_];
        
    }
    
    tabBarDisplayed_ = YES;
    tabBarView_.displayCollapseButtonFlag = displayCollapseButtonFlag_;
    tabBarView_.hidden = NO;
    [self recalculateLayout];
    
}

/*
 * Hides the tab bar
 */
- (void)hideTabBar {
    
    tabBarDisplayed_ = NO;
    tabBarView_.displayCollapseButtonFlag = NO;
    [self recalculateLayout];
    
}

/*
 * The tab bar animation did stop event selector. When hidding, the tab bar hidden propertie is updated
 */
- (void)tabBarAnimationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    
    if ([animationID isEqualToString:HIDE_TAB_BAR_ANIMATION_ID]) {
        
        [self hideTabBarAnimationFinished];
        
    } else if ([animationID isEqualToString:SHOW_TAB_BAR_ANIMATION_ID]) {
        
        [self showTabBarAnimationFinished];
        
    }
    
}

/*
 * Invoked when the hide tab bar animation is finished. The delegate is notified and user interaction is restored
 */
- (void)hideTabBarAnimationFinished {
    
    tabBarView_.userInteractionEnabled = YES;
    
    UIViewController *displayedViewController = selectedViewController_;
    
    if ([moreTabsViewController_.tabsList containsObject:selectedTabInformation_]) {
        
        displayedViewController = moreTabsViewController_.navigationController;
        
    }
    
    displayedViewController.view.userInteractionEnabled = YES;
    
    if (!tabBarDisplayed_) {
        
        tabBarView_.hidden = YES;
        [tabBarView_ removeFromSuperview];
        [delegate_ tabBarViewControllerTabBarDidDisappear:self];
        
    }
    
}

/*
 * Invoked when the show tab bar animation is finished. The delegate is notified and user interaction is restored
 */
- (void)showTabBarAnimationFinished {
    
    tabBarView_.userInteractionEnabled = YES;
    
    UIViewController *displayedViewController = selectedViewController_;
    
    if ([moreTabsViewController_.tabsList containsObject:selectedTabInformation_]) {
        
        displayedViewController = moreTabsViewController_.navigationController;
        
    }
    
    displayedViewController.view.userInteractionEnabled = YES;
    
    if (tabBarDisplayed_) {
        
        [delegate_ tabBarViewControllerTabBarDidAppear:self];
        
    }
    
}

/*
 * Collapses the tab bar. (iPad only)
 */
- (void)collapseTabBarWithAnimation:(BOOL)animated {
    
    if (!iPhoneDevice_) {
        
        if (animated) {
            
            if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
                
                [UIView animateWithDuration:TAB_BAR_ANIMATION_DURATION
                                      delay:0.0f
                                    options:(UIViewAnimationCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState)
                                 animations:^{
                                     
                                     [self collapseTabBar];
                                     
                                 }
                                 completion:nil];
                
            } else {
                
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:TAB_BAR_ANIMATION_DURATION];
                [UIView setAnimationBeginsFromCurrentState:YES];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
                
                [self collapseTabBar];
                
                [UIView commitAnimations];
                
            }
            
        } else {
            
            [self collapseTabBar];
            
        }
        
    }
    
}

/*
 * Expands the tab bar. (iPad only)
 */
- (void)expandTabBarWithAnimation:(BOOL)animated {
    
    if (!iPhoneDevice_) {
        
        if (animated) {
            
            if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
                
                [UIView animateWithDuration:TAB_BAR_ANIMATION_DURATION
                                      delay:0.0f
                                    options:(UIViewAnimationCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState)
                                 animations:^{
                                     
                                     [self expandTabBar];
                                     
                                 }
                                 completion:nil];
                
            } else {
                
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:TAB_BAR_ANIMATION_DURATION];
                [UIView setAnimationBeginsFromCurrentState:YES];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
                
                [self expandTabBar];
                
                [UIView commitAnimations];
                
            }
            
        } else {
            
            [self expandTabBar];
            
        }
        
    }
    
}

/*
 * Collapses the tab bar. (iPad only)
 */
- (void)collapseTabBar {
    
    if (!iPhoneDevice_) {
        
        tabBarView_.tabBarCollapsed = YES;
        [self recalculateLayout];
        
    }
    
}

/*
 * Expands the tab bar. (iPad only)
 */
- (void)expandTabBar {
    
    if (!iPhoneDevice_) {
        
        tabBarView_.tabBarCollapsed = NO;
        [self recalculateLayout];
        
    }
    
}

#pragma mark -
#pragma mark Tabs relocation

/*
 * Starts relocating the tabs. It displays a modal view controller to allow the user move the tabs
 */
- (void)relocateTabs {
    
    NSMutableArray *currentEditableTabsOrder = [NSMutableArray array];
    
    for (BBVATabBarTabInformation *tabBarInformation in tabsList_) {
        
        if ((delegate_ == nil) ||
            ([delegate_ tabBarViewController:self canRelocateTab:tabBarInformation])) {
            
            [currentEditableTabsOrder addObject:tabBarInformation];
            
        }
        
    }
    
    if ([currentEditableTabsOrder count] > 0) {
        
        if (relocateTabsViewController_ == nil) {
            
            relocateTabsViewController_ = [[BBVATabBarRelocateTabsViewController bbvaTabBarRelocateTabsViewController] retain];
            relocateTabsViewController_.logoImage = tabBarLogoImage_;
            relocateTabsViewController_.relocateTabsViewControllerDelegate = self;
            relocateTabsViewController_.viewBackgroundColor = relocateViewBackgroundColor_;
            relocateTabsViewController_.titleTextColor = relocateTitleTextColor_;
            relocateTabsViewController_.navigationBarClass = relocateNavigationBarClass_;
            relocateTabsViewController_.navigationBarColor = navigationBarColor_;
            relocateTabsViewController_.relocateHeaderBackgroundImage = relocateHeaderBackgroundImage_;
            relocateTabsViewController_.relocateTabNavigationTitle = relocateTabNavigationTitle_;
            relocateTabsViewController_.relocateTabHeaderTitle = relocateTabHeaderTitle_;
            relocateTabsViewController_.relocateTabResetText = relocateTabResetText_;
            relocateTabsViewController_.relocateTabDoneText = relocateTabDoneText_;
            
        }
        
        [relocateTabsViewController_ setDefaultTabsOrderArray:tabsDefaultOrderList_
                                        currentTabsOrderArray:tabsList_
                                currentEditableTabsOrderArray:currentEditableTabsOrder
                                            tabsInTabBarCount:tabsInTabBarCount_
                                     defaultTabsInTabBarCount:defaultTabsInTabBarCount_
                                     maximumTabsInTabBarCount:maximumTabsInTabBarCount_
                                           moreTabInformation:moreTabInformation_
                                       moreTabElementsOrdered:moreTabElementsOrdered_];
        
        [self presentModalViewController:relocateTabsViewController_
                                animated:YES];
        
    }
    
}

/*
 * Stops relocating the tabs
 */
- (void)stopRelocateTabs {
    
    [self dismissModalViewControllerAnimated:YES];
    
}

#pragma mark -
#pragma mark Graphic elements management

/**
 * Checks the application frame and sets the view size
 *
 * @private
 */
- (void)checkViewFrame {
    
    CGRect newOwnViewFrame = [[UIScreen mainScreen] applicationFrame];
    
    UIView *ownView = self.view;
    UIView *superview = ownView.superview;
    
    if ([superview isKindOfClass:[UIWindow class]]) {
        
        if (!CGRectEqualToRect(ownView.frame, newOwnViewFrame)) {
            
            ownView.frame = newOwnViewFrame;
            
        }
        
    }
    
}

/*
 * Adapts the graphic interface to the provided interface orientation. It has only meaning on iPad
 */
- (void)adaptGraphicInterfaceToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    if (!iPhoneDevice_) {
        
        if ((UIInterfaceOrientationIsLandscape(interfaceOrientation)) || (!collapseTabBarOnPortrait_)) {
            
            displayCollapseButtonFlag_ = NO;
            floatingTabBar_ = NO;
            [self expandTabBarWithAnimation:NO];
            
        } else {
            
            displayCollapseButtonFlag_ = YES;
            floatingTabBar_ = YES;
            [self collapseTabBarWithAnimation:NO];
            
        }
        
        tabBarView_.displayCollapseButtonFlag = displayCollapseButtonFlag_;
        
    }
    
}

#pragma mark -
#pragma mark More tab management

/*
 * Creates the more tab bar tab information element. It assumes it is not allocated, so no check is performed
 */
- (void)createMoreTabBarTab {
    
    UIImage *moreTabUnselectedIcon = moreTabUnselectedImage_;
    NSString *moreTabText = moreTabText_;
    
    [moreTabsViewController_ release];
    
    if (iPhoneDevice_) {
        
        moreTabsViewController_ = [[MoreTab_iPhoneViewController moreTab_iPhoneViewController] retain];
        
    } else {
        
        moreTabsViewController_ = [[MoreTab_iPadViewController moreTab_iPadViewController] retain];
        
    }
    
    moreTabsViewController_.moreTabViewControllerDelegate = self;
    
    moreTabsViewController_.leftNavigationButton = moreLeftNavigationButton_;
    moreTabsViewController_.rightNavigationButton = moreRightNavigationButton_;
    moreTabsViewController_.moreTabText = moreTabText;
    moreTabsViewController_.moreTabElementsOrdered = moreTabElementsOrdered_;
    moreTabsViewController_.containerViewController = self;
    moreTabsViewController_.navigationBarColor = navigationBarColor_;
    moreTabsViewController_.moreTabBackgroundColor = moreTabBackgroundColor_;
    moreTabsViewController_.moreTabTableBackgroundColor = moreTabTableBackgroundColor_;
    moreTabsViewController_.cellArrowImage = cellArrowImage_;
    moreTabsViewController_.cellSeparatorImage = cellSeparatorImage_;
    moreTabsViewController_.cellIsNarrow = moreTabCellIsNarrow_;
    
    if (moreTabNavigationController_ == nil) {
        
        moreTabNavigationController_ = [[UINavigationController alloc] initWithRootViewController:moreTabsViewController_];
        
    } else {
        
        [moreTabNavigationController_ setViewControllers:[NSArray arrayWithObjects:moreTabsViewController_,
                                                          nil]
                                                animated:NO];
        
    }
    
    [moreTabInformation_ release];
    moreTabInformation_ = [[BBVATabBarTabInformation alloc] initWithTabIdentifier:nil
                                                                   viewController:moreTabNavigationController_
                                                                     actionTarget:nil
                                                                   actionSelector:nil
                                                               referenceImageIcon:moreTabReferenceImage_
                                                                selectedImageIcon:moreTabSelectedImage_
                                                               selectedImageColor:moreTabSelectedColor_
                                                              unselectedImageIcon:moreTabUnselectedIcon
                                                             unselectedImageColor:moreTabUnselectedColor_
                                                                    moreImageIcon:nil
                                                                   moreImageColor:nil
                                                                disabledImageIcon:moreTabDisabledImage_
                                                               disabledImageColor:moreTabDisabledColor_
                                                                          tabText:moreTabText
                                                                 moreCellMainText:nil
                                                            moreCellSecondaryText:nil
                                                                 canShowOnMoreTab:NO
                                                                  canShowOnTabBar:YES];
    
}

/*
 * Sets the more tab naviagation bar elements
 */
- (void)setMoreTabLeftBarButton:(UIBarButtonItem *)moreTabLeftBarButton
                 rightBarButton:(UIBarButtonItem *)moreTabRightBarButton {
    
    if (moreTabLeftBarButton != moreLeftNavigationButton_) {
        
        [moreTabLeftBarButton retain];
        [moreLeftNavigationButton_ release];
        moreLeftNavigationButton_ = moreTabLeftBarButton;
        
    }
    
    if (moreTabRightBarButton != moreRightNavigationButton_) {
        
        [moreTabRightBarButton retain];
        [moreRightNavigationButton_ release];
        moreRightNavigationButton_ = moreTabRightBarButton;
        
    }
    
    moreTabsViewController_.leftNavigationButton = moreLeftNavigationButton_;
    moreTabsViewController_.rightNavigationButton = moreRightNavigationButton_;
    
}

/*
 * Pushes the provided view controller into the more view controller, in case it is active
 */
- (void)pushViewControllerIntoMoreTab:(UIViewController *)viewController
                             animated:(BOOL)animated {
    
    if (!currentTabIsMoreTab_) {
        
        [tabBarView_ selectTab:moreTabInformation_];
        
    }
    
    [self performSelector:@selector(doPushViewControllerIntoMoreTab:)
               withObject:viewController
               afterDelay:0.2f];
    
}

/*
 * Pushes the provided view controller into the more tab.
 */
- (void)doPushViewControllerIntoMoreTab:(UIViewController *)viewController {
    
    NSArray *viewControllers = moreTabNavigationController_.viewControllers;
    
    if ([viewControllers count] > 0) {
        
        UIViewController *rootViewController = [viewControllers objectAtIndex:0];
        
        [moreTabNavigationController_ setViewControllers:[NSArray arrayWithObjects:rootViewController,
                                                          viewController,
                                                          nil]
                                                animated:YES];
        
    }
    
}

#pragma mark -
#pragma mark Notifications management

/*
 * Invoked by framework when application will change the status bar frame. The view layout is recalculater
 */
- (void)statusBarFrameWillChange:(NSNotification *)notification {
    
    [self performSelector:@selector(checkViewFrame)
               withObject:nil
               afterDelay:0.01f];
    
    if (!iPhoneDevice_) {
        
        [self recalculateLayout];
        
    }
    
}

#pragma mark -
#pragma mark BBVATabBarViewControllerViewDelegate selectors

/**
 * Notifies the delegate that the view frame has changed. View layout is recalculated
 *
 * @param bbvaTabBarViewControllerView The BBVATabBarViewControllerView instance triggering the event
 */
- (void)tabBarViewControllerViewFrameChanged:(BBVATabBarViewControllerView *)bbvaTabBarViewControllerView {
    
    if ([self isViewLoaded]) {
        
        if (bbvaTabBarViewControllerView == self.view) {
            
            [self recalculateLayout];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark BBVATabBarContainerViewDelegate selectors

/**
 * BBVATabBarContainerView notifies the delegate that the collapse/expand button has been tapped. The tab bar collapsed state is updated
 *
 * @param bbvaTabBarContainerView The BBVATabBarContainerView instance triggering the event
 */
- (void)bbvaTabBarContainerViewCollapseButtonTapped:(BBVATabBarContainerView *)bbvaTabBarContainerView {
    
    if (!iPhoneDevice_) {
        
        if (bbvaTabBarContainerView == tabBarView_) {
            
            if (tabBarView_.tabBarCollapsed) {
                
                [self expandTabBarWithAnimation:YES];
                
            } else {
                
                [self collapseTabBarWithAnimation:YES];
                
            }
            
        }
        
    }
    
}

/**
 * Asks the delegate confirmation to select or not a given tab. It asks its delegate whether it can selected the associated view controller or not
 *
 * @param bbvaTabBarView The BBVA tab bar view asking for confirmation
 * @param tabInformation The information that defines the tab that is going to be selected
 * @return YES if the tab can be selected, NO otherwise
 */
- (BOOL)tabBarView:(BBVATabBarView *)bbvaTabBarView shouldSelectTab:(BBVATabBarTabInformation *)tabInformation {
    
    BOOL result = NO;
    
    if (tabInformation.viewController == nil) {
        
        [tabInformation executeAction];
        
    } else {
        
        if (delegate_ == nil) {
            
            result = YES;
            
        } else if ((!iPhoneDevice_) && (tabInformation == moreTabInformation_)) {
            
            BBVATabBarTabInformation *selectedTabBar = moreTabsViewController_.selectedTabBar;
            
            if (selectedTabBar != nil) {
                
                result = [delegate_ tabBarViewController:self
                              shouldSelectViewController:selectedTabBar.viewController];
                
            } else {
                
                result = [delegate_ tabBarViewController:self shouldSelectViewController:tabInformation.viewController];
                
            }
            
        } else {
            
            result = [delegate_ tabBarViewController:self shouldSelectViewController:tabInformation.viewController];
            
        }
        //        result = ((delegate_ == nil) || ([delegate_ tabBarViewController:self shouldSelectViewController:tabInformation.viewController]));
        
    }
    
    return result;
    
}


/**
 * Notifies a  new tab has been selected. Associated view controller is displayed
 *
 * @param bbvaTabBarView The BBVA tab bar view triggering the event
 * @param tabInformation The tab being selected information
 */
- (void)tabBarView:(BBVATabBarView *)bbvaTabBarView tabSelected:(BBVATabBarTabInformation *)tabInformation {
    
    UIViewController *tabViewController = tabInformation.tabViewController;
    
    if ((!iPhoneDevice_) && ([moreTabsViewController_ containsViewController:selectedTabInformation_.viewController])) {
        
        if (moreTabResetOnSelection_) {
            
            [moreTabsViewController_ resetMoreTabToFirstTab];
            
        }
        
    }
    
    if (tabInformation != selectedTabInformation_) {
        
        [tabInformation retain];
        [selectedTabInformation_ release];
        selectedTabInformation_ = tabInformation;
        
    }
    
    UIView *childView = selectedViewController_.view;
    
    if ((viewVisible_) && (!newOSVersion_)) {
        
        [selectedViewController_ viewWillDisappear:NO];
        
    }
    
    [childView removeFromSuperview];
    
    if ((viewVisible_) && (!newOSVersion_)) {
        
        [selectedViewController_ viewDidDisappear:NO];
        
    }
    
    [tabViewController retain];
    [selectedViewController_ release];
    selectedViewController_ = tabViewController;
    
    UIViewController *notificationViewController = tabViewController;
    
    if ((!iPhoneDevice_) && (tabInformation == moreTabInformation_)) {
        
        BBVATabBarTabInformation *selectedTabBar = moreTabsViewController_.selectedTabBar;
        
        if (selectedTabBar != nil) {
            
            notificationViewController = selectedTabBar.viewController;
            
            if (notificationViewController == nil) {
                
                notificationViewController = tabViewController;
                
            }
            
        }
        
    }
    
    if (selectedTabInformation_ == moreTabInformation_) {
        
        currentTabIsMoreTab_ = YES;
        
    } else {
        
        currentTabIsMoreTab_ = NO;
        
    }
    
    if (viewVisible_) {
        
        if (newOSVersion_) {
            
            //Workaround for a strage iOS 5.0 view appear/disappear behaviour
            [self performSelector:@selector(popViewControllerToRoot:)
                       withObject:selectedViewController_
                       afterDelay:0.1f];
            
        } else {
            
            [self popViewControllerToRoot:selectedViewController_];
            
        }
        
    }
    
    UIView *view = self.view;
    childView = selectedViewController_.view;
    
    if (viewVisible_) {
        
        [delegate_ tabBarViewController:self
               willSelectViewController:notificationViewController];
        
    }
    
    if (childView != nil) {
        
        childView.autoresizingMask = UIViewAutoresizingNone;
        
        childView.autoresizesSubviews = YES;
        
        CGRect childFrame = [self calculateChildFrame];
        childView.frame = childFrame;
        
        
        if ((viewVisible_) && (!newOSVersion_)) {
            
            [selectedViewController_ viewWillAppear:NO];
            
        }
        
        [view addSubview:childView];
        
        
        if ((viewVisible_) && (!newOSVersion_)) {
            
            [selectedViewController_ viewDidAppear:NO];
            
        }
        
        if (!iPhoneDevice_) {
            
            if ((UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) && (collapseTabBarOnPortrait_)) {
                
                [self collapseTabBarWithAnimation:YES];
                
            }
            
        }
        
        [self recalculateLayout];
        
    }
    
    [view bringSubviewToFront:tabBarView_];
    
    if (viewVisible_) {
        
        [delegate_ tabBarViewController:self viewControllerSelected:notificationViewController];
        
    }
    
}


/**
 * Notifies the logo image was tapped. The tab bar view controller delegate is notified.
 *
 * @param bbvaTabBarView The BBVA tab bar view triggering the event.
 */
- (void)tabBarViewLogoImageTapped:(BBVATabBarView *)bbvaTabBarView {
    
    [delegate_ tabBarViewControllerLogoImageTapped:self];
    
}

#pragma mark -
#pragma mark MoreTabBarViewControllerDelegate protocol selectors

/**
 * Asks the delegate confirmation to select or not a given tab. It asks its delegate whether it can selected the associated view controller or not
 *
 * @param moreTabViewController The more tab view controller asking for confirmation
 * @param tabInformation The information that defines the tab that is going to be selected
 * @return YES if the tab can be selected, NO otherwise
 */
- (BOOL)moreTabViewController:(MoreTabViewController *)moreTabViewController
              shouldSelectTab:(BBVATabBarTabInformation *)tabInformation {
    
    BOOL result = NO;
    
    if (tabInformation.viewController == nil) {
        
        [tabInformation executeAction];
        
    } else {
        
        result = ((delegate_ == nil) || ([delegate_ tabBarViewController:self shouldSelectViewController:tabInformation.viewController]));
        
    }
    
    return result;
    
}

/**
 * Notifies a new view controller will be selected. The view controller appear and disappear selectors must not be triggered by the tab bar view controller
 * itself. The delegate is notified
 *
 * @param moreTabViewController The more tab view controller asking for confirmation
 * @param tabInformation The tab being selected information
 */
- (void)moreTabViewController:(MoreTabViewController *)moreTabViewController
                willSelectTab:(BBVATabBarTabInformation *)tabInformation {
    
    if (viewVisible_) {
        
        UIViewController *viewController = tabInformation.viewController;
        [delegate_ tabBarViewController:self
               willSelectViewController:viewController];
        
    }
    
}

/**
 * Notifies a new tab was selected. The view controller appear and disappear selectors must not be triggered by the tab bar view controller
 * itself. The new selected view controller is stored
 *
 * @param moreTabViewController The more tab view controller asking for confirmation
 * @param tabInformation The tab being selected information
 */
- (void)moreTabViewController:(MoreTabViewController *)moreTabViewController
                  selectedTab:(BBVATabBarTabInformation *)tabInformation {
    
    UIViewController *viewController = tabInformation.viewController;
    
    if (tabInformation != selectedTabInformation_) {
        
        [tabInformation retain];
        [selectedTabInformation_ release];
        selectedTabInformation_ = tabInformation;
        
    }
    
    if (viewController != selectedViewController_) {
        
        [viewController retain];
        [selectedViewController_ release];
        selectedViewController_ = viewController;
        
    }
    
    if (viewVisible_) {
        
        [delegate_ tabBarViewController:self viewControllerSelected:viewController];
        
    }
    
}

#pragma mark -
#pragma mark BBVATabBarRelocateTabsViewControllerDelegate protocol selectors

/**
 * Delegate is notified when the user accepts the new ordering. The tabs order is updated and the delegate is notified that
 * the array order has been updated
 *
 * @param relocateTabsViewController The BBVATabBarRelocateTabsViewController triggering the event
 * @param tabsOrderedArray The array containing the new tabs ordering. All objects are BBVATabBarTabInformation insntances
 * @param tabBarCount The number of tabs inside the tab bar (not including the "more" tab). This parameter has meaning in iPad version
 */
- (void)bbvaTabBarRelocateTabsViewController:(BBVATabBarRelocateTabsViewController *)relocateTabsViewController
                                 orderedTabs:(NSArray *)tabsOrderedArray
                                 tabBarCount:(NSUInteger)tabBarCount {
    
    if (relocateTabsViewController_ == relocateTabsViewController) {
        
        [self setDefaultTabsOrderArray:tabsDefaultOrderList_
                 currentTabsOrderArray:tabsOrderedArray
                     tabsInTabBarCount:tabBarCount
              defaultTabsInTabBarCount:defaultTabsInTabBarCount_
              maximumTabsInTabBarCount:maximumTabsInTabBarCount_];
        
        [self selectViewController:selectedViewController_];
        
        [delegate_ tabBarViewcontroller:self
                          tabsReordered:tabsOrderedArray
                            tabBarCount:tabBarCount];
        
        [self dismissModalViewControllerAnimated:YES];
        
    }
    
}

#pragma mark -
#pragma mark Properties methods

/**
 * Returns the selected view controller tab index asking the tab bar view for the information
 *
 * @return The selected view controller tab index
 */
- (NSUInteger)selectedViewControllerTabIndex {
    
    return [tabBarView_ tabBarIndexForViewController:selectedViewController_];
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the new more tabs navigation controller
 *
 * @param moreTabNavigationController The more tabs navigation controller
 */
- (void)setMoreTabNavigationController:(UINavigationController *)moreTabNavigationController {
    
    if ((moreTabNavigationController != nil) && (moreTabNavigationController_ == nil)) {
        
        [moreTabNavigationController retain];
        [moreTabNavigationController_ release];
        moreTabNavigationController_ = moreTabNavigationController;
        
    }
    
}

/*
 * Sets the tab bar on right hand side flag
 *
 * @param tabBarOnRightHandSide
 */
- (void)setTabBarOnRightHandSide:(BOOL)tabBarOnRightHandSide {
    
    if (tabBarOnRightHandSide_ != tabBarOnRightHandSide) {
        
        tabBarOnRightHandSide_ = tabBarOnRightHandSide;
        
        if (tabBarView_ != nil) {
            
            tabBarView_.tabBarOnRightHandSide = tabBarOnRightHandSide_;
            
            if ([self isViewLoaded]) {
                
                [self recalculateLayout];
                
            }
            
        }
        
    }
    
}

/*
 * Sets the new collapse tab bar on portrait orientation flag.
 *
 * @param collapseTabBarOnPortrait The new collapse tab bar portrait orientation flag to set.
 */
- (void)setCollapseTabBarOnPortrait:(BOOL)collapseTabBarOnPortrait {
    
    if (collapseTabBarOnPortrait_ != collapseTabBarOnPortrait) {
        
        collapseTabBarOnPortrait_ = collapseTabBarOnPortrait;
        
        [self adaptGraphicInterfaceToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
        
    }
    
}

/*
 * Returns the visibility of the tab bar view
 */
- (BOOL)hidden {
    
    BOOL flag = YES;
    
    CGRect frame = self.view.frame;
    CGFloat maxY = frame.size.height;
    
    CGFloat tabBarYCoord = tabBarView_.frame.origin.y;
    
    if (([[self.view subviews] containsObject:tabBarView_]) && (tabBarView_.hidden == NO) && (tabBarYCoord < maxY)) {
        
        flag = NO;
        
    }
    
    return flag;
    
}

/*
 * Sets the new navigation bar color
 *
 * @param navigationBarColor The new navigation bar color to set
 */
- (void)setNavigationBarColor:(UIColor *)navigationBarColor {
    
    if ((navigationBarColor != nil) && (navigationBarColor != navigationBarColor_)) {
        
        [navigationBarColor retain];
        [navigationBarColor_ release];
        navigationBarColor_ = navigationBarColor;
        
        moreTabsViewController_.navigationBarColor = navigationBarColor_;
        relocateTabsViewController_.navigationBarColor = navigationBarColor_;
        
    }
    
}

/*
 * Sets the new more tab background color
 *
 * @param moreTabBackgroundColor The new more tab background color to set
 */
- (void)setMoreTabBackgroundColor:(UIColor *)moreTabBackgroundColor {
    
    if ((moreTabBackgroundColor != nil) && (moreTabBackgroundColor != moreTabBackgroundColor_)) {
        
        [moreTabBackgroundColor retain];
        [moreTabBackgroundColor_ release];
        moreTabBackgroundColor_ = moreTabBackgroundColor;
        
        moreTabsViewController_.moreTabBackgroundColor = moreTabBackgroundColor_;
        
    }
    
}

/*
 * Sets the new more tab table background color
 *
 * @param moreTabTableBackgroundColor The new more tab table background color to set
 */
- (void)setMoreTabTableBackgroundColor:(UIColor *)moreTabTableBackgroundColor {
    
    if ((moreTabTableBackgroundColor != nil) && (moreTabTableBackgroundColor != moreTabTableBackgroundColor_)) {
        
        [moreTabTableBackgroundColor retain];
        [moreTabTableBackgroundColor_ release];
        moreTabTableBackgroundColor_ = moreTabTableBackgroundColor;
        
        moreTabsViewController_.moreTabTableBackgroundColor = moreTabTableBackgroundColor_;
        
    }
    
}

/*
 * Sets the new more tab text
 *
 * @param moreTabText The new more tab text to set
 */
- (void)setMoreTabText:(NSString *)moreTabText {
    
    if (moreTabText != moreTabText_) {
        
        NSString *auxString = [moreTabText copyWithZone:self.zone];
        [moreTabText_ release];
        moreTabText_ = auxString;
        
        moreTabsViewController_.moreTabText = moreTabText_;
        
    }
    
}

/*
 * Sets the more tab elements ordered alphabetically flag
 *
 * @param moreTabElementsOrdered The new more tab elements ordered alphabetically flag
 */
- (void)setMoreTabElementsOrdered:(BOOL)moreTabElementsOrdered {
    
    if (moreTabElementsOrdered != moreTabElementsOrdered_) {
        
        moreTabElementsOrdered_ = moreTabElementsOrdered;
        moreTabsViewController_.moreTabElementsOrdered = moreTabElementsOrdered_;
        
    }
    
}

/*
 * Sets the new cell arrow image
 *
 * @param cellArrowImage The new cell arrow image to set
 */
- (void)setCellArrowImage:(UIImage *)cellArrowImage {
    
    if (cellArrowImage != cellArrowImage_) {
        
        [cellArrowImage retain];
        [cellArrowImage_ release];
        cellArrowImage_ = cellArrowImage;
        
        moreTabsViewController_.cellArrowImage = cellArrowImage_;
        
    }
    
}

/*
 * Sets the new cell separator image
 *
 * @param cellSeparatorImage The new cell separator image to set
 */
- (void)setCellSeparatorImage:(UIImage *)cellSeparatorImage {
    
    if (cellSeparatorImage != cellSeparatorImage_) {
        
        [cellSeparatorImage retain];
        [cellSeparatorImage_ release];
        cellSeparatorImage_ = cellSeparatorImage;
        
        moreTabsViewController_.cellSeparatorImage = cellSeparatorImage_;
        
    }
    
}

/*
 * Sets the relocate navigation bar class
 *
 * @param relocateNavigationBarClass The new relocate navigation bar class to set
 */
- (void)setRelocateNavigationBarClass:(Class)relocateNavigationBarClass {
    
    if (relocateNavigationBarClass != relocateNavigationBarClass_) {
        
        if ((relocateNavigationBarClass == nil) || ([relocateNavigationBarClass isSubclassOfClass:[UINavigationBar class]])) {
            
            [relocateNavigationBarClass retain];
            [relocateNavigationBarClass_ release];
            relocateNavigationBarClass_ = relocateNavigationBarClass;
            
            relocateTabsViewController_.navigationBarClass = relocateNavigationBarClass_;
            
        }
        
    }
    
}

/*
 * Sets the relocate view background color
 *
 * @param relocateViewBackgroundColor The new relocate view background color to set
 */
- (void)setRelocateViewBackgroundColor:(UIColor *)relocateViewBackgroundColor {
    
    if (relocateViewBackgroundColor != relocateViewBackgroundColor_) {
        
        [relocateViewBackgroundColor retain];
        [relocateViewBackgroundColor_ release];
        relocateViewBackgroundColor_ = relocateViewBackgroundColor;
        
        relocateTabsViewController_.viewBackgroundColor = relocateViewBackgroundColor_;
        
    }
    
}

/*
 * Sets the relocate view title text color
 *
 * @param relocateTitleTextColor The new relocate view title text color to set
 */
- (void)setRelocateTitleTextColor:(UIColor *)relocateTitleTextColor {
    
    if (relocateTitleTextColor != relocateTitleTextColor_) {
        
        [relocateTitleTextColor retain];
        [relocateTitleTextColor_ release];
        relocateTitleTextColor_ = relocateTitleTextColor;
        
        relocateTabsViewController_.titleTextColor = relocateTitleTextColor_;
        
    }
    
}

/*
 * Sets the relocate header background image
 *
 * @param relocateHeaderBackgroundImage The new realocate header background image
 */
- (void)setRelocateHeaderBackgroundImage:(UIImage *)relocateHeaderBackgroundImage {
    
    if (relocateHeaderBackgroundImage != relocateHeaderBackgroundImage_) {
        
        [relocateHeaderBackgroundImage retain];
        [relocateHeaderBackgroundImage_ release];
        relocateHeaderBackgroundImage_ = relocateHeaderBackgroundImage;
        
        relocateTabsViewController_.relocateHeaderBackgroundImage = relocateHeaderBackgroundImage_;
        
    }
    
}

/*
 * Set the new relocate tab navigation bar title
 *
 * @param relocateTabNavigationTitle The new relocate tab navigation bar title to set
 */
- (void)setRelocateTabNavigationTitle:(NSString *)relocateTabNavigationTitle {
    
    if (relocateTabNavigationTitle != relocateTabNavigationTitle_) {
        
        [relocateTabNavigationTitle retain];
        [relocateTabNavigationTitle_ release];
        relocateTabNavigationTitle_ = relocateTabNavigationTitle;
        
        relocateTabsViewController_.relocateTabNavigationTitle = relocateTabNavigationTitle_;
        
    }
    
}

/*
 * Set the new relocate tab header title
 *
 * @param relocateTabHeaderTitle The new relocate tab header title to set
 */
- (void)setRelocateTabHeaderTitle:(NSString *)relocateTabHeaderTitle {
    
    if (relocateTabHeaderTitle != relocateTabHeaderTitle_) {
        
        [relocateTabHeaderTitle retain];
        [relocateTabHeaderTitle_ release];
        relocateTabHeaderTitle_ = relocateTabHeaderTitle;
        
        relocateTabsViewController_.relocateTabHeaderTitle = relocateTabHeaderTitle_;
        
    }
    
}

/*
 * Set the new relocate tab reset text
 *
 * @param relocateTabResetText The new relocate tab reset text to set
 */
- (void)setRelocateTabResetText:(NSString *)relocateTabResetText {
    
    if (relocateTabResetText != relocateTabResetText_) {
        
        [relocateTabResetText retain];
        [relocateTabResetText_ release];
        relocateTabResetText_ = relocateTabResetText;
        
        relocateTabsViewController_.relocateTabResetText = relocateTabResetText_;
        
    }
    
}

/*
 * Set the new relocate tab done text
 *
 * @param relocateTabDoneText The new relocate tab done text to set
 */
- (void)setRelocateTabDoneText:(NSString *)relocateTabDoneText {
    
    if (relocateTabDoneText != relocateTabDoneText_) {
        
        [relocateTabDoneText retain];
        [relocateTabDoneText_ release];
        relocateTabDoneText_ = relocateTabDoneText;
        
        relocateTabsViewController_.relocateTabDoneText = relocateTabDoneText_;
        
    }
    
}

/*
 * Sets the collapse button right arrow image
 *
 * @param collapseTabRightArrowImage The new collapse button right arrow image
 */
- (void)setCollapseTabRightArrowImage:(UIImage *)collapseTabRightArrowImage {
    
    if (!iPhoneDevice_) {
        
        if (collapseTabRightArrowImage != collapseTabRightArrowImage_) {
            
            [collapseTabRightArrowImage retain];
            [collapseTabRightArrowImage_ release];
            collapseTabRightArrowImage_ = collapseTabRightArrowImage;
            
            tabBarView_.rightArrowImage = collapseTabRightArrowImage_;
            
        }
        
    }
    
}

/*
 * Sets the collapse button left arrow image
 *
 * @param collapseTabLeftArrowImage The new collapse button left arrow image
 */
- (void)setCollapseTabLeftArrowImage:(UIImage *)collapseTabLeftArrowImage {
    
    if (!iPhoneDevice_) {
        
        if (collapseTabLeftArrowImage != collapseTabLeftArrowImage_) {
            
            [collapseTabLeftArrowImage retain];
            [collapseTabLeftArrowImage_ release];
            collapseTabLeftArrowImage_ = collapseTabLeftArrowImage;
            
            tabBarView_.leftArrowImage = collapseTabLeftArrowImage_;
            
        }
        
    }
    
}

/*
 * Sets the tab bar information text
 *
 * @param tabBarInformationText The new tab bar information text to set
 */
- (void)setTabBarInformationText:(NSString *)tabBarInformationText {
    
    if (!iPhoneDevice_) {
        
        if (tabBarInformationText != tabBarInformationText_) {
            
            [tabBarInformationText_ release];
            tabBarInformationText_ = nil;
            tabBarInformationText_ = [tabBarInformationText copy];
            
            tabBarView_.informationText = tabBarInformationText;
            
        }
        
    }
    
}

/*
 * Sets the tab bar logo image
 *
 * @param tabBarLogoImage The new tab bar logo image to set
 */
- (void)setTabBarLogoImage:(UIImage *)tabBarLogoImage {
    
    if (!iPhoneDevice_) {
        
        if (tabBarLogoImage != tabBarLogoImage_) {
            
            [tabBarLogoImage retain];
            [tabBarLogoImage_ release];
            tabBarLogoImage_ = tabBarLogoImage;
            
            tabBarView_.logoImage = tabBarLogoImage;
            relocateTabsViewController_.logoImage = tabBarLogoImage;
            
        }
        
    }
    
}

/*
 * Sets the selected tab background color
 *
 * @param selectedTabBackgroundColor The new selected tab background color to set
 */
- (void)setSelectedTabBackgroundColor:(UIColor *)selectedTabBackgroundColor {
    
    if (selectedTabBackgroundColor != selectedTabBackgroundColor_) {
        
        [selectedTabBackgroundColor retain];
        [selectedTabBackgroundColor_ release];
        selectedTabBackgroundColor_ = selectedTabBackgroundColor;
        
        tabBarView_.selectedTabBackgroundColor = selectedTabBackgroundColor_;
        
    }
    
}

/*
 * Sets the new more tab cell is narrow flag
 *
 * @param moreTabCellIsNarrow The new more tab cell is narrow flag
 */
- (void)setMoreTabCellIsNarrow:(BOOL)moreTabCellIsNarrow {
    
    moreTabCellIsNarrow_ = moreTabCellIsNarrow;
    
    moreTabsViewController_.cellIsNarrow = moreTabCellIsNarrow_;
    
}

@end