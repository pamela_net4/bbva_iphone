/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BBVATabBarTabInformation.h"
#import "UIColor+BBVA_Colors.h"


#pragma mark -


/**
 * Defines the default selected image color
 */
#define DEFAULT_SELECTED_IMAGE_COLOR                                [UIColor colorWithRed:0.0f green:0.3961f blue:0.7176f alpha:1.0f]
/**
 * Defines the default selected image color iOS7
 */
#define DEFAULT_SELECTED_IMAGE_COLOR_IOS7                                [UIColor BBVABlueSpectrumColor]

/**
 * Defines the default unselected image color
 */
#define DEFAULT_UNSELECTED_IMAGE_COLOR                              [UIColor colorWithWhite:1.0f alpha:1.0f]

/**
 * Defines the default disabled image color
 */
#define DEFAULT_DISABLED_IMAGE_COLOR                                [UIColor colorWithWhite:0.5f alpha:1.0f]

/**
 * Defines the default more image color
 */
#define DEFAULT_MORE_IMAGE_COLOR                                    [UIColor colorWithRed:0.1961f green:0.6667f blue:0.8902f alpha:1.0f]


/**
 * BBVATabBarTabInformation private extension
 */
@interface BBVATabBarTabInformation()

/**
 * Creates and returns a selected image from the provided image, applying the alpha channel to given color
 *
 * @param originalImage The original image to mask with the blue background
 * @param color The color to use to synthesize the image
 * @return The image created from the provided image
 * @private
 */
+ (UIImage *)imageFromImage:(UIImage *)originalImage
                  withColor:(UIColor *)color;

/**
 * Restores the view controller hierarchy in case it was modified due to view controller veing displayed inside the more tab
 *
 * @private
 */
- (void)restoreViewControllerHierarchy;

@end

#pragma mark -

@implementation BBVATabBarTabInformation

#pragma mark -
#pragma mark Properties

@synthesize tabIdentifier = tabIdentifier_;
@dynamic tabViewController;
@dynamic moreTabViewController;
@synthesize viewController = viewController_;
@synthesize selectedIconImage = selectedIconImage_;
@synthesize selectedColor = selectedColor_;
@synthesize unselectedIconImage = unselectedIconImage_;
@synthesize unselectedColor = unselectedColor_;
@synthesize moreIconImage = moreIconImage_;
@synthesize moreColor = moreColor_;
@synthesize disabledIconImage = disabledIconImage_;
@synthesize disabledColor = disabledColor_;
@synthesize tabText = tabText_;
@dynamic moreCellMainText;
@synthesize moreCellSecondaryText = moreCellSecondaryText_;
@synthesize canShowOnMoreTab = canShowOnMoreTab_;
@synthesize canShowOnTabBar = canShowOnTabBar_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self restoreViewControllerHierarchy];
    
    [tabIdentifier_ release];
    tabIdentifier_ = nil;
    
    [viewController_ release];
    viewController_ = nil;
    
    [internalViewController_ release];
    internalViewController_ = nil;
    
    [selectedIconImage_ release];
    selectedIconImage_ = nil;
    
    [selectedColor_ release];
    selectedColor_ = nil;
    
    [unselectedIconImage_ release];
    unselectedIconImage_ = nil;
    
    [unselectedColor_ release];
    unselectedColor_ = nil;
    
    [moreIconImage_ release];
    moreIconImage_ = nil;
    
    [moreColor_ release];
    moreColor_ = nil;
    
    [disabledIconImage_ release];
    disabledIconImage_ = nil;
    
    [disabledColor_ release];
    disabledColor_ = nil;
    
    [tabText_ release];
    tabText_ = nil;
    
    [moreCellMainText_ release];
    moreCellMainText_ = nil;
    
    [moreCellSecondaryText_ release];
    moreCellSecondaryText_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It invokes the designated initializer to return a nil instance, as no information is defined
 *
 * @return Always nil, as this initialization is not allowed
 */
- (id)init {
    
    return [self initWithTabIdentifier:nil
                        viewController:nil
                          actionTarget:nil
                        actionSelector:nil
                    referenceImageIcon:nil
                     selectedImageIcon:nil
                    selectedImageColor:nil
                   unselectedImageIcon:nil
                  unselectedImageColor:nil
                         moreImageIcon:nil
                        moreImageColor:nil
                     disabledImageIcon:nil
                    disabledImageColor:nil
                               tabText:nil
                      moreCellMainText:nil
                 moreCellSecondaryText:nil
                      canShowOnMoreTab:NO
                       canShowOnTabBar:NO];
    
}

/*
 * Designated initializer. It initializes a BBVATabBarTabInformation instance with the provided information. All information
 * provided must be non-nil, or the instance will be released and a nil reference will be returned
 */
- (id)initWithTabIdentifier:(NSString *)tabIdentifier
             viewController:(UIViewController *)viewController
               actionTarget:(id)actionTarget
             actionSelector:(SEL)actionSelector
         referenceImageIcon:(UIImage *)referenceIconImage
          selectedImageIcon:(UIImage *)selectedIconImage
         selectedImageColor:(UIColor *)selectedImageColor
        unselectedImageIcon:(UIImage *)unselectedIconImage
       unselectedImageColor:(UIColor *)unselectedImageColor
              moreImageIcon:(UIImage *)moreIconImage
             moreImageColor:(UIColor *)moreImageColor
          disabledImageIcon:(UIImage *)disabledIconImage
         disabledImageColor:(UIColor *)disabledImageColor
                    tabText:(NSString *)tabText
           moreCellMainText:(NSString *)moreCellMainText
      moreCellSecondaryText:(NSString *)moreCellSecondaryText
           canShowOnMoreTab:(BOOL)canShowOnMoreTab
            canShowOnTabBar:(BOOL)canShowOnTabBar {
    
    BBVATabBarTabInformation *result = nil;
    
    if (((viewController == nil) && ((actionTarget == nil) || (actionSelector == nil))) ||
        (referenceIconImage == nil) || ([tabText length] == 0)) {
        
        [self release];
        
    } else if (self = [super init]) {
        
        NSZone *zone = self.zone;
        
        tabIdentifier_ = [tabIdentifier copyWithZone:zone];
        
        viewController_ = [viewController retain];
        
        actionTarget_ = actionTarget;
        actionSelector_ = actionSelector;
        
        if (selectedIconImage != nil) {
            
            selectedIconImage_ = [selectedIconImage retain];
            
        } else if (selectedImageColor != nil) {
            
            selectedIconImage_ = [[BBVATabBarTabInformation imageFromImage:referenceIconImage
                                                                 withColor:selectedImageColor] retain];
            
        } else {
            
            selectedIconImage_ = [[BBVATabBarTabInformation imageFromImage:referenceIconImage
                                                                 withColor:DEFAULT_SELECTED_IMAGE_COLOR] retain];
            
        }
        
        if (selectedImageColor != nil) {
            
            selectedColor_ = [selectedImageColor retain];
            
        } else {
            
            selectedColor_ = [DEFAULT_SELECTED_IMAGE_COLOR retain];
            
        }
        
        if (unselectedIconImage != nil) {
            
            unselectedIconImage_ = [unselectedIconImage retain];
            
        } else if (unselectedImageColor != nil) {
            
            unselectedIconImage_ = [[BBVATabBarTabInformation imageFromImage:referenceIconImage
                                                                   withColor:unselectedImageColor] retain];
            
        } else {
            
            unselectedIconImage_ = [[BBVATabBarTabInformation imageFromImage:referenceIconImage
                                                                   withColor:DEFAULT_UNSELECTED_IMAGE_COLOR] retain];
            
        }
        
        if (unselectedImageColor != nil) {
            
            unselectedColor_ = [unselectedImageColor retain];
            
        } else {
            
            unselectedColor_ = [DEFAULT_UNSELECTED_IMAGE_COLOR retain];
            
        }
        
        if (moreIconImage != nil) {
            
            moreIconImage_ = [moreIconImage retain];
            
        } else if (moreImageColor != nil) {
            
            moreIconImage_ = [[BBVATabBarTabInformation imageFromImage:referenceIconImage
                                                             withColor:moreImageColor] retain];
            
        } else {
            
            moreIconImage_ = [[BBVATabBarTabInformation imageFromImage:referenceIconImage
                                                             withColor:DEFAULT_MORE_IMAGE_COLOR] retain];
            
        }
        
        if (moreImageColor != nil) {
            
            moreColor_ = [moreImageColor retain];
            
        } else {
            
            moreColor_ = [DEFAULT_MORE_IMAGE_COLOR retain];
            
        }
        
        if (disabledIconImage != nil) {
            
            disabledIconImage_ = [disabledIconImage retain];
            
        } else if (disabledImageColor != nil) {
            
            disabledIconImage_ = [[BBVATabBarTabInformation imageFromImage:referenceIconImage
                                                                 withColor:disabledImageColor] retain];
            
        } else {
            
            disabledIconImage_ = [[BBVATabBarTabInformation imageFromImage:referenceIconImage
                                                                 withColor:DEFAULT_DISABLED_IMAGE_COLOR] retain];

        }
        
        if (disabledImageColor != nil) {
            
            disabledColor_ = [disabledImageColor retain];
            
        } else {
            
            disabledColor_ = [DEFAULT_DISABLED_IMAGE_COLOR retain];
            
        }
        
        tabText_ = [tabText copyWithZone:zone];
        
        moreCellMainText_ = [moreCellMainText copyWithZone:zone];
        
        moreCellSecondaryText_ = [moreCellSecondaryText copyWithZone:zone];
        
        canShowOnMoreTab_ = canShowOnMoreTab;
        
        canShowOnTabBar_ = canShowOnTabBar;
        
        result = self;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Action execution

/*
 * Executes the action associated to the tab when no tab view controller is available
 */
- (void)executeAction {
    
    if ((viewController_ == nil) && (actionTarget_ != nil) && (actionSelector_ != nil)) {
        
        if ([(NSObject *)actionTarget_ respondsToSelector:actionSelector_]) {
            
            [actionTarget_ performSelector:actionSelector_];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark Utility methods

/*
 * Sets a list of BBVATabBarTabInformation instances from one array to a mutable array. Instances in the source array which are not
 * a BBVATabBarTabInformation are discarded
 */
+ (BOOL)fillArray:(NSMutableArray *)destinationArray withBBVATabBarTabInformationInstancesFromOrigin:(NSArray *)sourceArray {
    
    BOOL result = NO;
    
    if ([sourceArray count] > 0) {
        
        NSMutableArray *auxArray = [NSMutableArray array];
        
        for (NSObject *object in sourceArray) {
            
            if ([object isKindOfClass:[BBVATabBarTabInformation class]]) {
                
                [auxArray addObject:object];
                
            }
            
        }
        
        if ([auxArray count] > 0) {
            
            [destinationArray removeAllObjects];
            [destinationArray addObjectsFromArray:auxArray];
            result = YES;
            
        }
        
    }
    
    return result;

}

/*
 * Restores the view controller hierarchy in case it was modified due to view controller veing displayed inside the more tab
 */
- (void)restoreViewControllerHierarchy {
    
    if (([viewController_ isKindOfClass:[UINavigationController class]]) && (internalViewController_ != nil)) {
        
        UINavigationController *navigationController = (UINavigationController *)viewController_;
        
        UINavigationController *internalViewControllerNavigationController = internalViewController_.navigationController;
        
        if (internalViewControllerNavigationController != nil) {
            
            NSMutableArray *mutableViewControllerList = [NSMutableArray arrayWithArray:internalViewControllerNavigationController.viewControllers];
            [mutableViewControllerList removeObject:internalViewController_];
            [internalViewControllerNavigationController setViewControllers:mutableViewControllerList
                                                                  animated:NO];
            
        }
        
        [navigationController setViewControllers:[NSArray arrayWithObject:internalViewController_]
                                        animated:NO];
        
    }
    
    [internalViewController_ release];
    internalViewController_ = nil;

}

#pragma mark -
#pragma mark Image utility selectors

/**
 * Creates and returns a selected image from the provided image, applying the alpha channel to given color
 *
 * @param originalImage The original image to mask with the blue background
 * @param color The color to use to synthesize the image
 * @return The image created from the provided image
 * @private
 */
+ (UIImage *)imageFromImage:(UIImage *)originalImage
                  withColor:(UIColor *)color {
    
    UIImage *result = nil;
    
    if ((originalImage != nil) && (color != nil)) {
        
        CGSize imageSize = originalImage.size;
        CGFloat originalWidth = imageSize.width;
        CGFloat originalHeight = imageSize.height;
        CGFloat scale = 1.0f;
        
        if (([originalImage respondsToSelector:@selector(scale)]) &&
            ([UIImage respondsToSelector:@selector(imageWithCGImage:scale:orientation:)])) {
            
            scale = originalImage.scale;
            
        }
        
        CGFloat width = originalWidth * scale;
        CGFloat height = originalHeight * scale;
        
        CGRect imageFrame = CGRectMake(0.0f, 0.0f, width, height);
        
        CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
        
        CGContextRef bitmapContext = CGBitmapContextCreate(NULL,
                                                           width,
                                                           height,
                                                           8,
                                                           (4 * width),
                                                           colorSpaceRef,
                                                           kCGImageAlphaPremultipliedFirst);
        
        
        //cambio de los bordes de las iamgens del tabbar
        
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
        CGContextSetFillColorWithColor(bitmapContext, color.CGColor);
        }
        else
        {
        CGContextSetFillColorWithColor(bitmapContext, DEFAULT_SELECTED_IMAGE_COLOR_IOS7.CGColor);
        }
        CGContextFillRect(bitmapContext, imageFrame);
        CGContextSetBlendMode(bitmapContext, kCGBlendModeDestinationIn);
        CGContextDrawImage(bitmapContext,imageFrame, originalImage.CGImage);
        
        
        CGImageRef image = CGBitmapContextCreateImage(bitmapContext);
        
        if ([UIImage respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
            
            result = [UIImage imageWithCGImage:image
                                         scale:scale
                                   orientation:UIImageOrientationUp];
            
        } else {
            
            result = [UIImage imageWithCGImage:image];
            
        }
        
        CGImageRelease(image);
        CGContextRelease(bitmapContext);
        CGColorSpaceRelease(colorSpaceRef);
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Comparison selectors

/*
 * Compares a tab bar information object with another using the more tab tex as reference
 */
- (NSComparisonResult)compareForMoreTabLocation:(BBVATabBarTabInformation *)tabBarTabInformation {
    
    NSComparisonResult result = NSOrderedAscending;
    
    if (tabBarTabInformation != nil) {
        
        NSString *selfMoreTabText = self.moreCellMainText;
        NSString *otherMoreTabText = tabBarTabInformation.moreCellMainText;
        NSRange range;
        range.location = 0;
        range.length = [selfMoreTabText length];
        
        result = [selfMoreTabText compare:otherMoreTabText
                                  options:NSCaseInsensitiveSearch
                                    range:range
                                   locale:[NSLocale currentLocale]];
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the tab bar view controller. When stored tab bar is a UINavigationController and the internal view controller is not nil,
 * the internal view controller is inserted as the root
 *
 * @return The tab bar view controller
 */
- (UIViewController *)tabViewController {
    
    UIViewController *result = viewController_;
    
    [self restoreViewControllerHierarchy];
    
    return result;
    
}

/**
 * Returns the view controller to use in the more tab bar. When the stored view controller is a UINavigationController, its root is extracted and
 * stored as the internal view controller, which is returned. Otherwise, the stored view controller is returned
 *
 * @return The view controller to use in the more tab bar
 */
- (UIViewController *)moreTabViewController {
    
    UIViewController *result = internalViewController_;
    
    if (result == nil) {
        
        if ([viewController_ isKindOfClass:[UINavigationController class]]) {
            
            UINavigationController *navigationController = (UINavigationController *)viewController_;
            
            NSMutableArray *viewControllersList = [NSMutableArray arrayWithArray:navigationController.viewControllers];
            
            if ([viewControllersList count] > 0) {
                
                internalViewController_ = [[viewControllersList objectAtIndex:0] retain];
                result = internalViewController_;
                [viewControllersList removeObject:internalViewController_];
                [navigationController setViewControllers:viewControllersList
                                                animated:NO];
                
            }
            
        } else {
            
            result = viewController_;
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the more cell main text. When no text is present in the moreCellMainText_ attribute, the tab text is returned instead
 *
 * @return The more cell main text
 */
- (NSString *)moreCellMainText {
    
    NSString *result = moreCellMainText_;
    
    if ([result length] == 0) {
        
        result = tabText_;
        
    }
    
    return result;
    
}

@end
