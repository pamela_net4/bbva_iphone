/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MoreTabViewController+protected.h"
#import "BBVATabBarTabInformation.h"


#pragma mark -

@implementation MoreTabViewController(protected)

#pragma mark -
#pragma mark Selecting view controllers

/*
 * Displays the provided view controller as the initial view controller. Default implementation does nothing
 */
- (void)displayViewControllerAsBase:(UIViewController *)viewController
                         fromTabBar:(BBVATabBarTabInformation *)tabBar {
    
    //Do nothing
    
}

/*
 * Checks whether a given tab bar can be displayed or not
 */
- (BOOL)checkTabBarCanBeDisplayed:(BBVATabBarTabInformation *)tabBar {
    
    BOOL result = YES;
    
    if (tabBar == nil) {
        
        result = NO;
        
    } else {
        
        if (moreTabViewControllerDelegate_ != nil) {
            
            result = [moreTabViewControllerDelegate_ moreTabViewController:self
                                                           shouldSelectTab:tabBar];
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Visual elements management

/*
 * Reloads the table view and enables or disables scrolling
 */
- (void)loadTableInformation {
    
    [moreTabsTableView_ reloadData];
    
    CGSize contentSize = moreTabsTableView_.contentSize;
    CGFloat contentHeight = contentSize.height;
    CGFloat tableHeight = moreTabsTableView_.frame.size.height;
    
    if (contentHeight <= tableHeight) {
        
        moreTabsTableView_.scrollEnabled = NO;
        
    } else {
        
        moreTabsTableView_.scrollEnabled = YES;
        
    }
    
}

@end

