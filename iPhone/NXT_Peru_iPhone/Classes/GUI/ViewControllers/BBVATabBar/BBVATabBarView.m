/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BBVATabBarView.h"
#import "BBVATabBarDrawingUtils.h"
#import "BBVATabBarTabInformation.h"
#import "BBVATabView.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Default selected tab background color
 */
#define SELECTED_TAB_BACKGROUND_COLOR                               [UIColor colorWithRed:0.949f green:0.949f blue:0.949f alpha:1.0f]

/**
 * Default selected tab background color in IOS7
 */
#define SELECTED_TAB_BACKGROUND_COLOR_IOS7                               [UIColor BBVAGreyToneFourColor]

/**
 * Default selected iPad tab background color
 */
#define I_PAD_SELECTED_TAB_BACKGROUND_COLOR                         [UIColor colorWithRed:0.8431f green:0.8980f blue:0.9490f alpha:1.0f]


#pragma mark -

/**
 * BBVATabBarView private extension
 */
@interface BBVATabBarView()

/**
 * Initializes the view
 *
 * @private
 */
- (void)initializeView;

@end


#pragma mark -

@implementation BBVATabBarView

#pragma mark -
#pragma mark Properties

@synthesize tabsList = tabsList_;
@synthesize selectedTabBackgroundColor = selectedTabBackgroundColor_;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [tabViewsList_ release];
    tabViewsList_ = nil;
    
    [tabsList_ release];
    tabsList_ = nil;
    
    [selectedTab_ release];
    selectedTab_ = nil;
    
    [selectedTabBackgroundColor_ release];
    selectedTabBackgroundColor_ = nil;
    
    delegate_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initializes the view
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initializeView];
    
}

/**
 * Designated initializer. It stablishes the view height to a fixed value
 *
 * @param frame The view initial frame which height is fixed
 * @return The initialized BBVATabBarView insntace
 */
- (id)initWithFrame:(CGRect)frame {
    
    BOOL iPhoneDevice = YES;
    
#ifdef UI_USER_INTERFACE_IDIOM
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        iPhoneDevice = NO;
        
    }
    
#endif
    
    if (iPhoneDevice) {

        frame.size.height = BBVA_I_PHONE_TAB_BAR_VIEW_HEIGHT;
        
    } else {
        
        frame.size.width = BBVA_I_PAD_TAB_BAR_VIEW_WIDTH;
        
    }
    
    if ((self = [super initWithFrame:frame])) {
        
        [self initializeView];

    }
    
    return self;
    
}

/*
 * Initializes the view
 */
- (void)initializeView {
    
    if (tabViewsList_ == nil) {
        
        tabViewsList_ = [[NSMutableArray allocWithZone:self.zone] init];
        
    }
    
    if (tabsList_ == nil) {
     
        tabsList_ = [[NSMutableArray allocWithZone:self.zone] init];
        
    }
    
    if (selectedTabBackgroundColor_ == nil) {
        
        if (self.iPhoneDevice) {
            
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
            selectedTabBackgroundColor_ = [SELECTED_TAB_BACKGROUND_COLOR retain];
            }
            else{
            selectedTabBackgroundColor_ = [SELECTED_TAB_BACKGROUND_COLOR_IOS7 retain];
            }
            
        } else {
            
            selectedTabBackgroundColor_ = [I_PAD_SELECTED_TAB_BACKGROUND_COLOR retain];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark Drawing methods

/**
 * Draws the receiver’s image within the passed-in rectangle
 *
 * @param rect A rectangle defining the area to restrict drawing to
 */
- (void)drawRect:(CGRect)rect {
    
    [super drawRect:rect];

    CGRect drawingRect = self.frame;
    drawingRect.origin.x = 0.0f;
    drawingRect.origin.y = 0.0f;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    CGContextSetAllowsAntialiasing(context, true);
    
    CGFloat viewWidth = CGRectGetWidth(drawingRect);
    CGFloat viewHeight = CGRectGetHeight(drawingRect);
    
    [self clipContext:context
       forScreenWidth:viewWidth
               height:viewHeight];

    NSUInteger tabsCount = self.tabsCount;
    
    if (tabsCount > 0) {
        
        CGFloat tabWidth = [self tabWidth];
        CGFloat tabHeight = [self tabHeight];
        
        NSUInteger index = [tabsList_ indexOfObject:selectedTab_];
        
        if (index != NSNotFound) {
            
            CGRect tabElementFrame = CGRectZero;
            
            if (self.iPhoneDevice) {
                
                tabElementFrame = CGRectMake(((CGFloat)index) * tabWidth, 0.0f, tabWidth, tabHeight);
                
            } else {
                
                tabElementFrame = CGRectMake(0.0f, ((CGFloat)index) * tabHeight, tabWidth, tabHeight);
                
            }
            
            [BBVATabBarDrawingUtils drawSelectedTabBackgroundIntoContext:context
                                                             withinFrame:tabElementFrame
                                                         backgroundColor:selectedTabBackgroundColor_
                                                        foriPhoneTabType:self.iPhoneDevice];
            
        }
        
    }
    
    CGContextRestoreGState(context);
    
}

#pragma mark -
#pragma mark Tab selection

/*
 * Selects the given element
 */
- (void)selectTab:(BBVATabBarTabInformation *)tab {
    
    if ([tabsList_ containsObject:tab]) {
        
        if ((delegate_ == nil) || ([delegate_ tabBarView:self shouldSelectTab:tab])) {
        
            NSUInteger index = [tabsList_ indexOfObject:selectedTab_];
            
            if (index != NSNotFound) {
                
                NSObject *storedObject = [tabViewsList_ objectAtIndex:index];
                
                if ([storedObject isKindOfClass:[BBVATabView class]]) {
                    
                    BBVATabView *tabView = (BBVATabView *)storedObject;
                    tabView.tabSelected = NO;
                    
                }
                
            }
            
            [tab retain];
            [selectedTab_ release];
            selectedTab_ = tab;
            
            index = [tabsList_ indexOfObject:selectedTab_];
            
            if (index != NSNotFound) {
                
                NSObject *storedObject = [tabViewsList_ objectAtIndex:index];
                
                if ([storedObject isKindOfClass:[BBVATabView class]]) {
                    
                    BBVATabView *tabView = (BBVATabView *)storedObject;
                    tabView.tabSelected = YES;
                    
                }
                
            }
            
            [self setNeedsDisplay];
            
            [delegate_ tabBarView:self
                      tabSelected:selectedTab_];
                
        }
        
    }
    
}

/*
 * Selects a given view controller, if found among the tab information
 */
- (BBVATabBarTabInformation *)selectViewController:(UIViewController *)viewController {
    
    BBVATabBarTabInformation * result = nil;
    
    if (viewController != nil) {
        
        for (result in tabsList_) {
            
            if (result.tabViewController == viewController) {
                
                break;
                
            } else {
                
                result = nil;
                
            }
            
        }
        
    }
    
    if (result != nil) {
        
        [self selectTab:result];
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Information access

/*
 * Returns the tab bar index for a given view controller
 */
- (NSUInteger)tabBarIndexForViewController:(UIViewController *)viewController {
    
    NSUInteger result = NSNotFound;
    
    NSUInteger tabsCount = [tabsList_ count];
    
    for (NSUInteger i = 0; i < tabsCount; i++) {
        
        BBVATabBarTabInformation *tabInformation = [tabsList_ objectAtIndex:i];
        
        if (tabInformation.tabViewController == viewController) {
            
            result = i;
            break;
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark BBVATabBarBackground selectors

/**
 * Returns the number of tabs in the tab bar, which is equal to the number of tabs in the list
 *
 * @return The number of tabs in the list
 */
- (NSUInteger)tabsCount {
    
    return [tabsList_ count];
    
}

#pragma mark -
#pragma mark UIResponder selectors

/**
 * Tells the receiver when one or more fingers touch down in a view or window. The tab under the touch is selected
 *
 * @param touches A set of UITouch instances that represent the touches for the starting phase of the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    NSUInteger tabsCount = [tabsList_ count];
    
    if ((tabsCount > 0) && ([touches count] == 1)) {
        
        UITouch *touch = [touches anyObject];
        
        CGPoint viewPoint = [touch locationInView:self];
        
        CGFloat y = viewPoint.y;
        CGFloat x = viewPoint.x;
        
        if ((y >= 0.0f) && (x >= 0.0f) &&
            (((self.iPhoneDevice) && (y < BBVA_I_PHONE_TAB_BAR_VIEW_HEIGHT)) ||
             ((!self.iPhoneDevice) && (x < BBVA_I_PAD_TAB_BAR_VIEW_WIDTH)))) {
            
            BOOL iPhoneDevice = self.iPhoneDevice;
            NSInteger selectedTabIndex = -1;
            
            if (iPhoneDevice) {
                
                CGRect frame = self.frame;
                CGFloat viewWidth = CGRectGetWidth(frame);
                CGFloat tabWidth = viewWidth / (CGFloat)tabsCount;
                selectedTabIndex = (NSInteger)floor(x / tabWidth);
                
            } else {
                
                CGFloat tabHeight = BBVA_I_PAD_TABS_HEIGHT;
                selectedTabIndex = (NSInteger)floor(y / tabHeight);
                
            }
            
            
            if ((selectedTabIndex >= 0) && (selectedTabIndex < tabsCount)) {
                
                BBVATabBarTabInformation *selectedTab = [tabsList_ objectAtIndex:selectedTabIndex];
                [self selectTab:selectedTab];
                
            } else if (!iPhoneDevice) {
                
                UIImageView *logoImageView = self.bbvaLogoImageView;
                
                if (logoImageView != nil) {
                    
                    CGRect logoImageFrame = logoImageView.frame;
                    
                    if ((x >= CGRectGetMinX(logoImageFrame)) && (x <= CGRectGetMaxX(logoImageFrame)) &&
                        (y >= CGRectGetMinY(logoImageFrame)) && (y <= CGRectGetMaxY(logoImageFrame))) {
                        
                        [delegate_ tabBarViewLogoImageTapped:self];
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}

#pragma mark -
#pragma mark Properties methods

/**
 * Sets the tabs list, removing from the list the objects which are not BBVATabBarTabInformation instances. If no BBVATabBarTabInformation instances
 * are found, the previous elements list is not released
 *
 * @param aTabsList The array containing the new tab list
 */
- (void)setTabsList:(NSArray *)aTabsList {
    
    if ([BBVATabBarTabInformation fillArray:tabsList_ withBBVATabBarTabInformationInstancesFromOrigin:aTabsList]) {
        
        for (UIView *tabView in tabViewsList_) {
            
            [tabView removeFromSuperview];
            
        }
        
        [tabViewsList_ removeAllObjects];
        
        CGFloat viewWidth = [self tabWidth];
        CGFloat viewHeight = [self tabHeight];
        BBVATabBarTabInformation *tabInformation;
        NSUInteger tabsCount = [tabsList_ count];
        BBVATabView *tabView = nil;
        CGRect viewFrame = CGRectMake(0.0f, 0.0f, viewWidth, viewHeight);
        NSZone *zone = self.zone;
        NSNull *null = [NSNull null];
        
        for (NSUInteger i = 0; i < tabsCount; i++) {
            
            tabInformation = [tabsList_ objectAtIndex:i];
            
            if (self.iPhoneDevice) {
                
                viewFrame.origin.x = ((CGFloat)i * viewWidth);
                
            } else {
                
                viewFrame.origin.y = ((CGFloat)i * viewHeight);
                
            }
            
            tabView = [[[BBVATabView allocWithZone:zone] initWithFrame:viewFrame] autorelease];
            
            if (tabView != nil) {
                
                tabView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
                tabView.tabInformation = tabInformation;
                tabView.drawText = self.iPhoneDevice;
                
                [tabViewsList_ addObject:tabView];
                [self addSubview:tabView];
                
            } else {
                
                [tabViewsList_ addObject:null];
                
            }
            
        }

        if ([tabsList_ containsObject:selectedTab_]) {
            
            [self selectTab:selectedTab_];
            
        } else {
            
            [self selectTab:[tabsList_ objectAtIndex:0]];
            
        }
        
        [self setNeedsDisplay];
        
    }
    
}

/*
 * Sets the selected tab background color
 *
 * @param selectedTabBackgroundColor The selected tab background color
 */
- (void)setSelectedTabBackgroundColor:(UIColor *)selectedTabBackgroundColor {
    
    if (selectedTabBackgroundColor != selectedTabBackgroundColor_) {
        
        if ((selectedTabBackgroundColor != nil) && (selectedTabBackgroundColor != [UIColor clearColor])) {
            
            [selectedTabBackgroundColor retain];
            [selectedTabBackgroundColor_ release];
            selectedTabBackgroundColor_ = selectedTabBackgroundColor;
            
        } else {
            
            if (self.iPhoneDevice) {
                
                if (SELECTED_TAB_BACKGROUND_COLOR != selectedTabBackgroundColor_ || SELECTED_TAB_BACKGROUND_COLOR_IOS7 != selectedTabBackgroundColor_) {
                    
                    [selectedTabBackgroundColor_ release];
                    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
                    {
                        selectedTabBackgroundColor_ = [SELECTED_TAB_BACKGROUND_COLOR retain];
                    }
                    else{
                        selectedTabBackgroundColor_ = [SELECTED_TAB_BACKGROUND_COLOR_IOS7 retain];
                    }
                    
                   
                    
                }
                
            } else {
                
                if (I_PAD_SELECTED_TAB_BACKGROUND_COLOR != selectedTabBackgroundColor_) {
                    
                    [selectedTabBackgroundColor_ release];
                    selectedTabBackgroundColor_ = [I_PAD_SELECTED_TAB_BACKGROUND_COLOR retain];
                    
                }
                
            }
            
        }
        
        [self setNeedsDisplay];
        
    }
    
}

@end
