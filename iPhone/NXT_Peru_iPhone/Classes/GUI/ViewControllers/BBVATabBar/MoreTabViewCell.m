/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MoreTabViewCell.h"
#import "NibLoader.h"
#import "BBVATabBarTabInformation.h"


/**
 * Define the MoreTabViewCell NIB file name
 */
#define NIB_FILE_NAME                                               @"MoreTabViewCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"MoreTabViewCell"

/**
 * Defines the wide cell height
 */
#define WIDE_CELL_HEIGHT                                            60.0f

/**
 * Defines the narrow cell height
 */
#define NARROW_CELL_HEIGHT                                          44.0f

/**
 * Defines the iPad narrow cell height
 */
#define IPAD_NARROW_CELL_HEIGHT                                     50.0f

/**
 * Defines the main and secondary text vertical distance measured in points
 */
#define MAIN_AND_SECONDARY_TEXT_VERTICAL_DISTANCE                   2.0f

/**
 * Defines the distance from the labels to the images borders measured in points
 */
#define LABELS_TO_IMAGES_HORIZONTAL_DISTANCE                        5.0f

/**
 * Defines the local blue color
 */
#define LOCAL_BLUE_COLOR                                            [UIColor colorWithRed:0.0f green:0.2471f blue:0.5529f alpha:1.0f]

/**
 * Defines the tab image size (both height and width) on a narrow cell
 */
#define NARROW_CELL_IMAGE_SIZE                                      20.0f

/**
 * Defines the tab image size (both height and width) on a wide cell
 */
#define WIDE_CELL_IMAGE_SIZE                                        32.0f


#pragma mark -

@implementation MoreTabViewCell

#pragma mark -
#pragma mark Properties

@synthesize tabIconImageView = tabIconImageView_;
@synthesize tabTextLabel = tabTextLabel_;
@synthesize tabSecondaryTextLabel = tabSecondaryTextLabel_;
@synthesize cellArrowImageView = cellArrowImageView_;
@synthesize cellSeparatorImageView = cellSeparatorImageView_;
@dynamic cellArrowImage;
@dynamic cellSeparatorImage;
@synthesize tabInformation = tabInformation_;
@synthesize cellIsNarrow = cellIsNarrow_;
@synthesize displayArrow = displayArrow_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [tabIconImageView_ release];
    tabIconImageView_ = nil;
    
    [tabTextLabel_ release];
    tabTextLabel_ = nil;
    
    [tabSecondaryTextLabel_ release];
    tabSecondaryTextLabel_ = nil;
    
    [cellArrowImageView_ release];
    cellArrowImageView_ = nil;
    
    [cellSeparatorImageView_ release];
    cellSeparatorImageView_ = nil;
    
    [tabInformation_ release];
    tabInformation_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    UIFont *font = [UIFont systemFontOfSize:17.0f];
    tabTextLabel_.font = font;
    tabTextLabel_.textColor = LOCAL_BLUE_COLOR;
    tabTextLabel_.highlightedTextColor = LOCAL_BLUE_COLOR;

    font = [UIFont systemFontOfSize:15.0f];
    tabSecondaryTextLabel_.font = font;
    tabSecondaryTextLabel_.textColor = LOCAL_BLUE_COLOR;
    tabSecondaryTextLabel_.highlightedTextColor = [UIColor lightGrayColor];

    BOOL iPhoneDevice = YES;
    
#ifdef UI_USER_INTERFACE_IDIOM
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        iPhoneDevice = NO;
        
    }
    
#endif
    
    if (iPhoneDevice) {

        self.selectionStyle = UITableViewCellSelectionStyleGray;
        
    } else {
        
        self.selectionStyle = UITableViewCellSelectionStyleGray;
        UIView *selectedBackgroundView = [[[UIView allocWithZone:self.zone] initWithFrame:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f)] autorelease];
        selectedBackgroundView.backgroundColor = [UIColor whiteColor];
        self.selectedBackgroundView = selectedBackgroundView;
        
    }
    
}

/*
 * Creates and returns an autoreleased MoreTabViewCell constructed from a NIB file
 */
+ (MoreTabViewCell *)moreTabViewCell {

    return (MoreTabViewCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeightForNarrowCell:(BOOL)isNarrowCell {
    
    CGFloat result = WIDE_CELL_HEIGHT;
    
    if (isNarrowCell) {
        
        result = NARROW_CELL_HEIGHT;
        
#ifdef UI_USER_INTERFACE_IDIOM
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            
            result = IPAD_NARROW_CELL_HEIGHT;
            
        }
        
#endif
   
    }
    
    return result;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return CELL_IDENTIFIER;
    
}

#pragma mark -
#pragma mark UIView selectors

/**
 * Layouts the view subviews
 */
- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    CGFloat cellHeight = self.frame.size.height;
    CGRect imageFrame = tabIconImageView_.frame;
    
    if (cellIsNarrow_) {
        
        imageFrame.size.width = NARROW_CELL_IMAGE_SIZE;
        imageFrame.size.height = NARROW_CELL_IMAGE_SIZE;
        
    } else {
        
        imageFrame.size.width = WIDE_CELL_IMAGE_SIZE;
        imageFrame.size.height = WIDE_CELL_IMAGE_SIZE;
        
    }
    
    imageFrame.origin.y = round((cellHeight - imageFrame.size.height) / 2.0f);
    tabIconImageView_.frame = imageFrame;

    CGFloat labelsLeft = CGRectGetMaxX(imageFrame) + LABELS_TO_IMAGES_HORIZONTAL_DISTANCE;
    
    CGFloat labelsRight = 0.0f;
    CGRect arrowImageFrame = cellArrowImageView_.frame;
    
    if (cellArrowImageView_.hidden) {
        
        labelsRight = CGRectGetMaxX(arrowImageFrame);
        
    } else {
        
        labelsRight = CGRectGetMinX(arrowImageFrame) - LABELS_TO_IMAGES_HORIZONTAL_DISTANCE;
        
    }
    
    if (tabSecondaryTextLabel_.hidden) {
        
        CGRect mainLabelFrame = tabTextLabel_.frame;
        mainLabelFrame.origin.x = labelsLeft;
        mainLabelFrame.origin.y = 0.0f;
        mainLabelFrame.size.height = cellHeight;
        mainLabelFrame.size.width = labelsRight - labelsLeft;
        tabTextLabel_.frame = mainLabelFrame;
        tabTextLabel_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
    } else {
        
        CGRect mainLabelFrame = tabTextLabel_.frame;
        CGRect secondaryLabelFrame = tabSecondaryTextLabel_.frame;
        CGSize mainTextSize = [tabTextLabel_.text sizeWithFont:tabTextLabel_.font];
        CGFloat mainTextHeight = mainTextSize.height;
        CGSize secondaryTextSize = [tabSecondaryTextLabel_.text sizeWithFont:tabSecondaryTextLabel_.font];
        CGFloat secondaryTextHeigh = secondaryTextSize.height;
        
        CGFloat mainTextBottom = round((cellHeight - MAIN_AND_SECONDARY_TEXT_VERTICAL_DISTANCE) / 2.0f);
        CGFloat mainTextTop = mainTextBottom - mainTextHeight;
        mainLabelFrame.origin.x = labelsLeft;
        mainLabelFrame.origin.y = mainTextTop;
        mainLabelFrame.size.height = mainTextHeight;
        mainLabelFrame.size.width = labelsRight - labelsLeft;
        tabTextLabel_.frame = mainLabelFrame;
        
        CGFloat secondaryTextTop = mainTextBottom + MAIN_AND_SECONDARY_TEXT_VERTICAL_DISTANCE;
        secondaryLabelFrame.origin.x = labelsLeft;
        secondaryLabelFrame.origin.y = secondaryTextTop;
        secondaryLabelFrame.size.height = secondaryTextHeigh;
        secondaryLabelFrame.size.width = labelsRight - labelsLeft;
        tabSecondaryTextLabel_.frame = secondaryLabelFrame;
        
        tabTextLabel_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;

    }
    
}

#pragma mark -
#pragma mark Properties methods

/**
 * Sets the tab information associated to the cell and updates the graphic interface
 *
 * @param tabInformation The new tab information to store
 */
- (void)setTabInformation:(BBVATabBarTabInformation *)tabInformation {
    
    if (tabInformation_ != tabInformation) {
        
        [tabInformation_ release];
        tabInformation_ = nil;
        tabInformation_ = [tabInformation retain];
        
        tabIconImageView_.image = tabInformation_.moreIconImage;
        
        NSString *mainText = tabInformation_.moreCellMainText;
        
        if ([mainText length] == 0) {
            
            mainText = tabInformation_.tabText;
            
        }
        
        tabTextLabel_.text = mainText;
        
        NSString *secondaryText = tabInformation_.moreCellSecondaryText;
        tabSecondaryTextLabel_.text = secondaryText;
        
        if (([secondaryText length] == 0) || (cellIsNarrow_)) {
            
            tabSecondaryTextLabel_.hidden = YES;
            
        } else {
            
            tabSecondaryTextLabel_.hidden = NO;
            
        }
        
        [self setNeedsLayout];
        
    }
    
}

/*
 * Returns the cell arrow image
 *
 * @return The cell arrow image
 */
- (UIImage *)cellArrowImage {
    
    return cellArrowImageView_.image;
    
}

/*
 * Sets the new cell arrow image
 *
 * @param cellArrowImage The new cell arrow image
 */
- (void)setCellArrowImage:(UIImage *)cellArrowImage {
    
    cellArrowImageView_.image = cellArrowImage;
    
}

/*
 * Returns the cell separator image
 *
 * @return The cell separator image
 */
- (UIImage *)cellSeparatorImage {
    
    return cellSeparatorImageView_.image;
    
}

/*
 * Sets the new cell separator image
 *
 * @param cellSeparatorImage The new cell separator image
 */
- (void)setCellSeparatorImage:(UIImage *)cellSeparatorImage {
    
    cellSeparatorImageView_.image = cellSeparatorImage;
    
}

/*
 * Sets the new narrow cell layout flag and lays-out the cell
 *
 * @param cellIsNarrow The new narrow cell layout flag to set
 */
- (void)setCellIsNarrow:(BOOL)cellIsNarrow {
    
    if (cellIsNarrow != cellIsNarrow_) {
        
        cellIsNarrow_ = cellIsNarrow;
        
        if (cellIsNarrow_) {
            
            tabSecondaryTextLabel_.hidden = YES;
            
        } else {
            
            tabSecondaryTextLabel_.hidden = ([tabSecondaryTextLabel_.text length] > 0);
            
        }
        
        [self setNeedsLayout];
        
    }
    
}

/*
 * Returns the display arrow flag
 *
 * @return The display arrow flag
 */
- (BOOL)displayArrow {
    
    return !cellArrowImageView_.hidden;
    
}

/*
 * Sets the new display arrow flag
 *
 * param displayArrow The new display arrow flag
 */
- (void)setDisplayArrow:(BOOL)displayArrow {
    
    cellArrowImageView_.hidden = !displayArrow;
    
    [self setNeedsLayout];
    
}

@end
