/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BBVATabBarDrawingUtils.h"
#import "BBVATabBarTabInformation.h"
#import "UIColor+BBVA_Colors.h"


/**
 * BBVA brand gradient first color
 */
#define BBVA_BRAND_GRADIENT_COLOR_1                                 [UIColor colorWithRed:0.0039f green:0.2549f blue:0.5451f alpha:1.0f]

/**
 * BBVA brand gradient second color
 */
#define BBVA_BRAND_GRADIENT_COLOR_2                                 [UIColor colorWithRed:0.0f green:0.4f blue:0.7294f alpha:1.0f]

/**
 * BBVA brand gradient third color
 */
#define BBVA_BRAND_GRADIENT_COLOR_3                                 [UIColor colorWithRed:0.0f green:0.5804f blue:0.8863f alpha:1.0f]

/**
 * BBVA brand gradient forth color
 */
#define BBVA_BRAND_GRADIENT_COLOR_4                                 [UIColor colorWithRed:0.2275f green:0.6863f blue:0.9294f alpha:1.0f]

/**
 * BBVA brand gradient fifth color
 */
#define BBVA_BRAND_GRADIENT_COLOR_5                                 [UIColor colorWithRed:0.4902f green:0.8118f blue:0.9804f alpha:1.0f]

/**
 * BBVA brand gradient sixth color
 */
#define BBVA_BRAND_GRADIENT_COLOR_6                                 [UIColor colorWithRed:0.6941f green:0.8824f blue:0.9882f alpha:1.0f]

/**
 * iPhone BBVA brand gradient height measured in points
 */
#define I_PHONE_BBVA_BRAND_GRADIENT_HEIGHT                          5.0f

/**
 * iPad BBVA brand gradient height measured in points
 */
#define I_PAD_BBVA_BRAND_GRADIENT_HEIGHT                            12.0f


/**
 * Defines the tab image top position
 */
#define TAB_IMAGE_TOP                                               6.0f

/**
 * Defines the iPad tab image top position
 */
#define I_PAD_TAB_IMAGE_TOP                                         25.0f

/**
 * Defines the iPhone tab image width
 */
#define I_PHONE_TAB_IMAGE_WIDTH                                     30.0f

/**
 * Defines the iPhone tab image HEIGHT
 */
#define I_PHONE_TAB_IMAGE_HEIGHT                                    30.0f

/**
 * Defines the iPad tab image width
 */
#define I_PAD_TAB_IMAGE_WIDTH                                       32.0f

/**
 * Defines the iPad tab image HEIGHT
 */
#define I_PAD_TAB_IMAGE_HEIGHT	                                    32.0f

/**
 * Defines the tab text size
 */
#define TAB_TEXT_SIZE                                               9.0f

/**
 * Defines the tab text top position
 */
#define TAB_TEXT_TOP                                                37.0f

/**
 * Defines the iPad tab text top position
 */
#define I_PAD_TAB_TEXT_TOP                                          57.0f

/**
 * Defines the tab text distance to the element border (left and right) measured in points
 */
#define TAB_TEXT_HORIZONTAL_INSET                                   5.0f


/**
 * Defines the view gradient top color as an array
 */
#define VIEW_GRADIENT_TOP_COLOR_AS_ARRAY                            0.1294f, 0.4784f, 0.7608f, 1.0f

/**
 * Defines the view gradient bottom color as an array
 */
#define VIEW_GRADIENT_BOTTOM_COLOR_AS_ARRAY                         0.0117f, 0.2902f, 0.6118f, 1.0f

/**
 * Defines the view gradient top color as an array in iOS7
 */
#define VIEW_GRADIENT_TOP_COLOR_AS_ARRAY_IOS7                            0.0f, 0.4313f, 0.7568f, 1.0f

/**
 * Defines the view gradient bottom color as an array in iOS7
 */
#define VIEW_GRADIENT_BOTTOM_COLOR_AS_ARRAY_IOS7                         0.0f, 0.4313f, 0.7568f, 1.0f

/**
 * Selected tab background color
 */
#define SELECTED_TAB_BACKGROUND_COLOR                               [UIColor colorWithRed:0.949f green:0.949f blue:0.949f alpha:1.0f]

/**
 * Selected iPad tab background color
 */
#define I_PAD_SELECTED_TAB_BACKGROUND_COLOR                         [UIColor colorWithRed:0.8431f green:0.8980f blue:0.9490f alpha:1.0f]

/**
 * Defines the tab separator color
 */
#define TAB_SEPARATOR_COLOR                                         [UIColor colorWithRed:1.0f green:1.0f blue:0.9922f alpha:0.15f]

/**
 * Defines the tab separator color
 */
#define TAB_SEPARATOR_COLOR_IOS7                                         [UIColor BBVABlueSpectrumColor]

/**
 * Defines the iPhone tab separator top color
 */
#define I_PAD_TAB_SEPARATOR_TOP_COLOR                               [UIColor colorWithRed:0.0824f green:0.1843f blue:0.3922f alpha:1.0f]

/**
 * Defines the iPhone tab separator bottom color
 */
#define I_PAD_TAB_SEPARATOR_BOTTOM_COLOR                            [UIColor colorWithRed:0.1020f green:0.3451f blue:0.6314f alpha:1.0f]

/**
 * Defines the unselected tab text color
 */
#define TAB_UNSELECTED_TEXT_COLOR                                   [UIColor colorWithRed:0.8157f green:0.8863f blue:0.9647f alpha:1.0f]

/**
 * Defines the selected tab text color
 */
#define TAB_SELECTED_TEXT_COLOR                                     [UIColor colorWithRed:0.0f green:0.4f blue:0.7294f alpha:1.0f]

/**
 * Defines the disabled tab text color
 */
#define TAB_DISABLED_TEXT_COLOR                                     [UIColor colorWithWhite:0.5f alpha:1.0f]


#pragma mark -

@implementation BBVATabBarDrawingUtils

#pragma mark -
#pragma mark Static attributes

/**
 * View blue gradient
 */
static CGGradientRef viewGradient_ = NULL;

#pragma mark -
#pragma mark Drawing selectors

/*
 * Creates the tab blue gradient (from top to bottom). It assumes the gradient is NULL when invoked, so no check is performed
 */
+ (void)createiPhoneViewGradient {
    
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0f, 1.0f };
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
    CGFloat components[8] = { VIEW_GRADIENT_TOP_COLOR_AS_ARRAY, VIEW_GRADIENT_BOTTOM_COLOR_AS_ARRAY };
        CGColorSpaceRef rgbColorspace = CGColorSpaceCreateDeviceRGB();
        CGGradientRelease(viewGradient_);
        viewGradient_ = CGGradientCreateWithColorComponents(rgbColorspace, components, locations, num_locations);
        CGColorSpaceRelease(rgbColorspace);
    }
    else{
    CGFloat components[8] = { VIEW_GRADIENT_TOP_COLOR_AS_ARRAY_IOS7, VIEW_GRADIENT_BOTTOM_COLOR_AS_ARRAY_IOS7 };
        CGColorSpaceRef rgbColorspace = CGColorSpaceCreateDeviceRGB();
        CGGradientRelease(viewGradient_);
        viewGradient_ = CGGradientCreateWithColorComponents(rgbColorspace, components, locations, num_locations);
        CGColorSpaceRelease(rgbColorspace);
    }
    

}

/*
 * Draws the gradient background in the given context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 */
+ (void)drawBackgroundGradientIntoContext:(CGContextRef)context
                              withinFrame:(CGRect)frame
                         foriPhoneTabType:(BOOL)iPhoneTab {
    
    if ((iPhoneTab) && (viewGradient_ == nil)) {
        
        [BBVATabBarDrawingUtils createiPhoneViewGradient];
        
    }
    
    CGFloat maxY = CGRectGetMaxY(frame);
    
    CGContextSaveGState(context);
    
    CGContextClipToRect(context, frame);
    
    if (iPhoneTab) {
        
        CGContextDrawLinearGradient(context, viewGradient_, CGPointMake(0.0f, 0.0f), CGPointMake(0.0f, maxY), 0);
        
    } else {
        
        CGContextSetFillColorWithColor(context, I_PAD_TAB_BACKGROUND_COLOR.CGColor);
        CGContextFillRect(context, frame);
        
    }
    
    CGContextRestoreGState(context);

}

/*
 * Draws the tab separator at the given position
 */
+ (void)drawTabSeparatorIntoContext:(CGContextRef)context
                            fromMin:(CGFloat)fromMin
                              toMax:(CGFloat)toMax
                         atPosition:(CGFloat)position
                   foriPhoneTabType:(BOOL)iPhoneTab {
    
    CGContextSaveGState(context);
    CGContextSetLineWidth(context, 1.0f);
    
    if (iPhoneTab) {
        
        CGContextBeginPath(context);
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
        CGContextSetStrokeColorWithColor(context, TAB_SEPARATOR_COLOR.CGColor);
        }
        else
        {
        CGContextSetStrokeColorWithColor(context, TAB_SEPARATOR_COLOR_IOS7.CGColor);
        }
        CGContextMoveToPoint(context, position, fromMin);
        CGContextAddLineToPoint(context, position, toMax);
        CGContextStrokePath(context);
        
    } else {
        
        CGContextSetStrokeColorWithColor(context, I_PAD_TAB_SEPARATOR_TOP_COLOR.CGColor);
        CGContextMoveToPoint(context, fromMin, position);
        CGContextAddLineToPoint(context, toMax, position);
        CGContextStrokePath(context);
        CGContextSetStrokeColorWithColor(context, I_PAD_TAB_SEPARATOR_BOTTOM_COLOR.CGColor);
        CGContextMoveToPoint(context, fromMin, position + 1.0f);
        CGContextAddLineToPoint(context, toMax, position + 1.0f);
        CGContextStrokePath(context);
        
    }
    
    CGContextRestoreGState(context);

}

/*
 * Draws a selected tab element (not including the selected background) in the given context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 */
+ (void)drawSelectedTabElement:(BBVATabBarTabInformation *)tabElement
                   intoContext:(CGContextRef)context
                   withinFrame:(CGRect)tabElementFrame
              foriPhoneTabType:(BOOL)iPhoneTab
                      drawText:(BOOL)drawText {
    
    CGContextSaveGState(context);
    
    CGContextClipToRect(context, tabElementFrame);
    
    CGContextTranslateCTM(context, CGRectGetMinX(tabElementFrame), 0.0f);
    tabElementFrame.origin.x = 0.0f;
    
    [BBVATabBarDrawingUtils drawTabImage:tabElement.selectedIconImage
                             intoContext:context
                             withinFrame:tabElementFrame
                        foriPhoneTabType:iPhoneTab];
    
    if (drawText) {
        
        [BBVATabBarDrawingUtils drawTabText:tabElement.tabText
                                intoContext:context
                                withinFrame:tabElementFrame
                                      color:tabElement.selectedColor
                           foriPhoneTabType:iPhoneTab];
        
    }
    
    CGContextRestoreGState(context);
    
}

/*
 * Draws an unselected tab element in the given context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 */
+ (void)drawUnselectedTabElement:(BBVATabBarTabInformation *)tabElement
                     intoContext:(CGContextRef)context
                     withinFrame:(CGRect)tabElementFrame
                foriPhoneTabType:(BOOL)iPhoneTab
                        drawText:(BOOL)drawText {
    
    CGContextSaveGState(context);
    
    CGContextClipToRect(context, tabElementFrame);
    CGContextTranslateCTM(context, CGRectGetMinX(tabElementFrame), 0.0f);
    tabElementFrame.origin.x = 0.0f;
    
    [BBVATabBarDrawingUtils drawTabImage:tabElement.unselectedIconImage
                             intoContext:context
                             withinFrame:tabElementFrame
                        foriPhoneTabType:iPhoneTab];
    
    if (drawText) {
        
        [BBVATabBarDrawingUtils drawTabText:tabElement.tabText
                                intoContext:context
                                withinFrame:tabElementFrame
                                      color:tabElement.unselectedColor
                           foriPhoneTabType:iPhoneTab];
        
    }
    
    CGContextRestoreGState(context);

}

/*
 * Draws a disabled tab element in the given context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 */
+ (void)drawDisabledTabElement:(BBVATabBarTabInformation *)tabElement
                   intoContext:(CGContextRef)context
                   withinFrame:(CGRect)tabElementFrame
              foriPhoneTabType:(BOOL)iPhoneTab
                      drawText:(BOOL)drawText {
    
    CGContextSaveGState(context);
    
    CGContextClipToRect(context, tabElementFrame);
    CGContextTranslateCTM(context, CGRectGetMinX(tabElementFrame), 0.0f);
    tabElementFrame.origin.x = 0.0f;
    
    [BBVATabBarDrawingUtils drawTabImage:tabElement.disabledIconImage
                             intoContext:context
                             withinFrame:tabElementFrame
                        foriPhoneTabType:iPhoneTab];
    
    if (drawText) {
        
        [BBVATabBarDrawingUtils drawTabText:tabElement.tabText
                                intoContext:context
                                withinFrame:tabElementFrame
                                      color:tabElement.disabledColor
                           foriPhoneTabType:iPhoneTab];
        
    }
    
    CGContextRestoreGState(context);

}

/*
 * Draws an image as the tab element image into the provided context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 */
+ (void)drawTabImage:(UIImage *)tabImage
         intoContext:(CGContextRef)context
         withinFrame:(CGRect)tabElementFrame
    foriPhoneTabType:(BOOL)iPhoneTab {
    
    CGFloat tabWidth = CGRectGetWidth(tabElementFrame);
    
    CGSize imageSize = tabImage.size;
    CGFloat imageWidth = imageSize.width;
    CGFloat imageHeight = imageSize.height;
    
    CGFloat availableImageWidth = I_PHONE_TAB_IMAGE_WIDTH;
    CGFloat availableImageHeight = I_PHONE_TAB_IMAGE_HEIGHT;
    
    if (!iPhoneTab) {
        
        availableImageWidth = I_PAD_TAB_IMAGE_WIDTH;
        availableImageHeight = I_PAD_TAB_IMAGE_HEIGHT;
        
    }
    
    if ((imageWidth > 0) && (imageHeight > 0)) {
        
        CGFloat aspectRatio = imageWidth / imageHeight;
        
        CGRect imageRect = CGRectZero;
        
        if (aspectRatio >= 1.0f) {
            
            CGFloat finalHeight = availableImageWidth / aspectRatio;
            imageRect = CGRectMake(round(-availableImageWidth / 2.0f), round(-finalHeight / 2.0f), availableImageWidth, finalHeight);
            
        } else {
            
            CGFloat finalWidth = availableImageHeight * aspectRatio;
            imageRect = CGRectMake(round(-finalWidth / 2.0f), round(-availableImageHeight / 2.0f), finalWidth, availableImageHeight);
            
        }
        
        CGContextSaveGState(context);
        
        if (iPhoneTab) {
            
            CGContextTranslateCTM(context, round(tabWidth / 2.0f), TAB_IMAGE_TOP + round(availableImageHeight / 2.0f));
            
        } else {
            
            CGContextTranslateCTM(context, round(tabWidth / 2.0f), I_PAD_TAB_IMAGE_TOP + round(availableImageHeight / 2.0f));
            
        }
        
        CGContextScaleCTM(context, 1.0f, -1.0f);
        
        CGContextDrawImage(context, imageRect, tabImage.CGImage);
        
        CGContextRestoreGState(context);
        
    }

}

/*
 * Draws the element text into the provided context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 */
+ (void)drawTabText:(NSString *)text
        intoContext:(CGContextRef)context
        withinFrame:(CGRect)tabElementFrame
              color:(UIColor *)textColor
   foriPhoneTabType:(BOOL)iPhoneTab {
    
    CGFloat tabWidth = CGRectGetWidth(tabElementFrame);
    CGFloat maxTextWidth = tabWidth - (2.0f * TAB_TEXT_HORIZONTAL_INSET);
    
    if (maxTextWidth > 10.0f) {
        
        UIFont *textFont = [UIFont boldSystemFontOfSize:TAB_TEXT_SIZE];
        NSString *finalText = text;
        
        CGSize textSize = [finalText sizeWithFont:textFont];
        
        if (textSize.width > maxTextWidth) {
            
            NSUInteger textLength = [finalText length];
            
            if (textLength > 2) {
                
                for (NSUInteger i = textLength - 2; i > 0; i--) {
                    
                    finalText = [NSString stringWithFormat:@"%@%@", [text substringToIndex:i], @"..."];
                    
                    textSize = [finalText sizeWithFont:textFont];
                    
                    if (textSize.width > maxTextWidth) {
                        
                        finalText = nil;
                        
                    } else {
                        
                        break;
                        
                    }
                    
                }
                
            } else {
                
                finalText = nil;
                
            }
            
        }
        
        if ([finalText length] > 0) {
            
            CGContextSaveGState(context);
            
            CGContextSetFillColorWithColor(context, textColor.CGColor);
            
            CGRect textRect = CGRectZero;
            
            if (iPhoneTab) {
            
                textRect = CGRectMake(round((tabWidth - textSize.width) / 2.0f), TAB_TEXT_TOP, textSize.width, TAB_TEXT_SIZE);
                
            } else {
                
                textRect = CGRectMake(round((tabWidth - textSize.width) / 2.0f), I_PAD_TAB_TEXT_TOP, textSize.width, TAB_TEXT_SIZE);
                
            }
            
            [finalText drawInRect:textRect withFont:textFont];
            
            CGContextRestoreGState(context);
            
        }
        
    }

}

/*
 * Draws the selected element background in the given context and frame. Frame origin is assumed to be (0.0f, 0.0f)
 */
+ (void)drawSelectedTabBackgroundIntoContext:(CGContextRef)context
                                 withinFrame:(CGRect)tabElementFrame
                             backgroundColor:(UIColor *)backgroundColor
                            foriPhoneTabType:(BOOL)iPhoneTab {
    
    CGContextSaveGState(context);
    CGContextClipToRect(context, tabElementFrame);

    CGContextTranslateCTM(context, CGRectGetMinX(tabElementFrame), CGRectGetMinY(tabElementFrame));
    tabElementFrame.origin.x = 0.0f;
    tabElementFrame.origin.y = 0.0f;

    CGContextSetFillColorWithColor(context, backgroundColor.CGColor);
    
    CGContextFillRect(context, tabElementFrame);
    
    CGFloat tabWidth = CGRectGetWidth(tabElementFrame);
    CGFloat tabHeight = CGRectGetHeight(tabElementFrame);
    
    CGFloat gradientElementWidth = tabWidth / 6.0f;
    CGFloat gradientHeight = I_PAD_BBVA_BRAND_GRADIENT_HEIGHT;
    
    if (iPhoneTab) {
        
        gradientHeight = I_PHONE_BBVA_BRAND_GRADIENT_HEIGHT;
        
    }
    
    CGFloat gradientTop = tabHeight - gradientHeight;
    
    for (NSUInteger i = 0; i < 6; i++) {
        
        CGRect gradientSectionFrame = CGRectMake(floor(((CGFloat) i) * gradientElementWidth), gradientTop, ceil(gradientElementWidth), gradientHeight);
        
        UIColor *sectionColor = BBVA_BRAND_GRADIENT_COLOR_1;
        
        switch (i) {
                
            case 1: {
                
                sectionColor = BBVA_BRAND_GRADIENT_COLOR_2;
                break;
                
            }
                
            case 2: {
                
                sectionColor = BBVA_BRAND_GRADIENT_COLOR_3;
                break;
                
            }
                
            case 3: {
                
                sectionColor = BBVA_BRAND_GRADIENT_COLOR_4;
                break;
                
            }
                
            case 4: {
                
                sectionColor = BBVA_BRAND_GRADIENT_COLOR_5;
                break;
                
            }
                
            case 5: {
                
                sectionColor = BBVA_BRAND_GRADIENT_COLOR_6;
                break;
                
            }
                
            default: {
                
                break;
                
            }
                
        }
        
        CGContextSetFillColorWithColor(context, sectionColor.CGColor);
        CGContextFillRect(context, gradientSectionFrame);
        
    }
    
    CGContextRestoreGState(context);

}

@end
