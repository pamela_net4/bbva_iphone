/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "BBVATabBarViewControllerView.h"


//Forward declarations
@class BBVATabBarRelocateTabsViewController;
@class BBVATabBarBackground;
@class BBVATabBarTabInformation;
@class BBVATabView;


/**
 * BBVATabBarRelocateTabsViewController delegate protocol. Delegate is notified when the user accepts the new ordering
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol BBVATabBarRelocateTabsViewControllerDelegate

@required

/**
 * Delegate is notified when the user accepts the new ordering
 *
 * @param relocateTabsViewController The BBVATabBarRelocateTabsViewController triggering the event
 * @param tabsOrderedArray The array containing the new tabs ordering. All objects are BBVATabBarTabInformation instances
 * @param tabBarCount The number of tabs inside the tab bar (not including the "more" tab). This parameter has meaning in iPad version
 */
- (void)bbvaTabBarRelocateTabsViewController:(BBVATabBarRelocateTabsViewController *)relocateTabsViewController
                                 orderedTabs:(NSArray *)tabsOrderedArray
                                 tabBarCount:(NSUInteger)tabBarCount;

@end


/**
 * View controller to manage BBVA tab bar relocation
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BBVATabBarRelocateTabsViewController : UIViewController <BBVATabBarViewControllerViewDelegate> {
    
@private
    
    /**
     * Top bar. Displayed when view controller is not inside a navigation controller
     */
    UINavigationBar *topBar_;
    
    /**
     * Graphic container view. The general tabs are contained in this view
     */
    BBVATabBarViewControllerView *graphicContainerView_;
    
    /**
     * Title background image view
     */
    UIImageView *titleBackgroundImageView_;
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Tab bar background view
     */
    BBVATabBarBackground *tabBarBackgroundView_;
    
    /**
     * Done navigation button
     */
    UIBarButtonItem *doneButton_;
    
    /**
     * Reset navigation button
     */
    UIBarButtonItem *resetButton_;
    
    /**
     * BBVA logo image
     */
    UIImage *logoImage_;

    /**
     * Default tabs order array. All objects are BBVATabBarTabInformation instances
     */
    NSMutableArray *defaultTabsOrderArray_;
    
    /**
     * Current tabs order array, including all the tabs, even the ones that cannot be edited. All objects are BBVATabBarTabInformation instances
     */
    NSMutableArray *currentTabsOrderArray_;
    
    /**
     * Current editable tabs order array. Tabs not present in this array will not be relocated. All objects are BBVATabBarTabInformation instances
     */
    NSMutableArray *currentEditableTabsOrderArray_;
    
    /**
     * More tab information
     */
    BBVATabBarTabInformation *moreTabInformation_;
    
    /**
     * More tab elements ordered alphabetically flag. When the more tab elements are ordered alphabetically, all tab views are always displayed
     * that way in the general section, when they are not, tabs in general view represent their real order
     */
    BOOL moreTabElementsOrdered_;
    
    /**
     * Navigation bar class used to create a custom navigation bar if needed. Only UINavigationBar subclasses are stored
     */
    Class navigationBarClass_;

    /**
     * Navigation bar color
     */
    UIColor *navigationBarColor_;
    
    /**
     * View background color
     */
    UIColor *viewBackgroundColor_;
    
    /**
     * Title text color
     */
    UIColor *titleTextColor_;

    /**
     * Header background image
     */
    UIImage *relocateHeaderBackgroundImage_;
    
    /**
     * Relocate tab navigation title
     */
    NSString *relocateTabNavigationTitle_;
    
    /**
     * Relocate tab header title
     */
    NSString *relocateTabHeaderTitle_;
    
    /**
     * Relocate tab reset text
     */
    NSString *relocateTabResetText_;
    
    /**
     * Relocate tab done text
     */
    NSString *relocateTabDoneText_;
    
    /**
     * General tab views array. Contains the tab views located in the view body (not the ones in the simulated tab bar). All objects
     * are BBVATabView instances
     */
    NSMutableArray *generalTabViewsArray_;
    
    /**
     * Simulated tab views array. Contais the tab view located in the simulated tab bar. All objects are BBVATabView instances
     */
    NSMutableArray *tabBarViewsArray_;
    
    /**
     * Current tabs array stored in the order they are displayed in the view. All objects are BBVATabBarTabInformation instances
     */
    NSMutableArray *currentEditableTabsDisplayedArray_;
    
    /**
     * Current tabs inside the tab bar array. All objects are BBVATabBarTabInformation instances
     */
    NSMutableArray *currentTabsInTabBarArray_;
    
    /**
     * Current tabs in tab bar, not including the "more" tab. This attribute has more meaning in iPad version (iPhone version has a fixed amount)
     */
    NSUInteger currentTabsInTabBarCount_;
    
    /**
     * Default number of tabs inside the tab bar, not including the "more" tab. This attribute has more meaning in iPad version (iPhone version has a fixed amount)
     */
    NSUInteger defaultTabsInTabBarCount_;
    
    /**
     * Maximum number of tabs inside the tab bar, including the "more" tab. This attribute has only meaing in iPad version (iPhone version has a fixed amount)
     */
    NSUInteger maximumTabsInTabBarCount_;

    /**
     * Editing a general tab flag. YES when the view is editing a general tab position, NO otherwise
     */
    BOOL editingGeneralTab_;
    
    /**
     * Editing a bar tab flag. YES when the view is editing a bar tab position, NO otherwise
     */
    BOOL editingBarTab_;
    
    /**
     * Selected view that started the edition
     */
    BBVATabView *selectedView_;
    
    /**
     * Current view being moved. It can be a general tab (which will be displayed at a 125% scale) or a tab bar view
     */
    BBVATabView *auxiliaryMovingView_;
    
    /**
     * Selected tab information
     */
    BBVATabBarTabInformation *selectedInformation_;
    
    /**
     * General tabs position being moved
     */
    NSUInteger initialGeneralLocation_;
    
    /**
     * General visual tabs position being moved
     */
    NSUInteger initialGeneralVisualLocation_;
    
    /**
     * Tab bar position being moved
     */
    NSUInteger initialTabLocation_;
    
    /**
     * Original view top left point
     */
    CGPoint originalViewTopLeftPoint_;
    
    /**
     * Original auxiliary view top left point
     */
    CGPoint originalAuxiliaryViewTopLeftPoint_;
    
    /**
     * Original touch point inside the view
     */
    CGPoint originalTouchPoint_;
    
    /**
     * Touch being followed
     */
    UITouch *touchBeingFollowed_;
    
    /**
     * Remove auxiliary view from superview flag. YES when the auxiliary view must be removed from the superview, NO otherwise
     */
    BOOL removeAuxiliaryViewFromSuperview_;
    
    /**
     * iPhone device flag. YES when the device is an iPhone, NO when it is an iPad
     */
    BOOL iPhoneDevice_;

    /**
     * Relocate tabs view controller delegate
     */
    id<BBVATabBarRelocateTabsViewControllerDelegate> relocateTabsViewControllerDelegate_;
    
}


/**
 * Provides read-write access to the BBVA logo image
 */
@property (nonatomic, readwrite, retain) UIImage *logoImage;

/**
 * Provides read-write access to the navigation bar class
 */
@property (nonatomic, readwrite, retain) Class navigationBarClass;

/**
 *  Provides read-write access to the navigation bar color
 */
@property (nonatomic, readwrite, retain) UIColor *navigationBarColor;

/**
 * Provides read-write access to the view background color
 */
@property (nonatomic, readwrite, retain) UIColor *viewBackgroundColor;

/**
 * Provides read-write access to the title text color
 */
@property (nonatomic, readwrite, retain) UIColor *titleTextColor;

/**
 * Provides read-write access to the header background image
 */
@property (nonatomic, readwrite, retain) UIImage *relocateHeaderBackgroundImage;

/**
 * Provides read-write access to the relocate tab navigation title
 */
@property (nonatomic, readwrite, copy) NSString *relocateTabNavigationTitle;

/**
 * Provides read-write access to the relocate tab header title
 */
@property (nonatomic, readwrite, copy) NSString *relocateTabHeaderTitle;

/**
 * Provides read-write access to the relocate tab reset text
 */
@property (nonatomic, readwrite, copy) NSString *relocateTabResetText;

/**
 * Provides read-write access to the relocate tab done text
 */
@property (nonatomic, readwrite, copy) NSString *relocateTabDoneText;

/**
 * Provides readwrite access to the relocate tabs view controller delegate
 */
@property (nonatomic, readwrite, assign) id<BBVATabBarRelocateTabsViewControllerDelegate> relocateTabsViewControllerDelegate;


/**
 * Creates and returns an autoreleased BBVATabBarRelocateTabsViewController constructed from a NIB file
 *
 * @return The autoreleased BBVATabBarRelocateTabsViewController constructed from a NIB file
 */
+ (BBVATabBarRelocateTabsViewController *)bbvaTabBarRelocateTabsViewController;


/**
 * Sets the arrays needed to relocate the tabs. Arrays objects are BBVATabBarInformation instances
 *
 * @param defaultTabsOrderArray The default tabs order array (including both editable and not editable tabs)
 * @param currentTabsOrderArray The current tabs order array (including both editable and not editable tabs)
 * @param currentEditableTabsOrderArray The current tabs order array (including both editable and not editable tabs)
 * @param tabsInTabBarCount The number of tabs inside the tab bar, not including the "more" tab
 * @param defaultTabsInTabBarCount The default number of tabs inside the tab bar, not including the "more" tab
 * @param maximumTabsInTabBarCount The maximum number of tabs inside the tab bar, including the "more" tab
 * @param moreTabInformation The more tab information in case it is needed
 * @param moreTabElementsOrdered YES when more tabs elements are ordered, and so are the general section tabs, NO otherwise
 */
- (void)setDefaultTabsOrderArray:(NSArray *)defaultTabsOrderArray
           currentTabsOrderArray:(NSArray *)currentTabsOrderArray
   currentEditableTabsOrderArray:(NSArray *)currentEditableTabsOrderArray
               tabsInTabBarCount:(NSUInteger)tabsInTabBarCount
        defaultTabsInTabBarCount:(NSUInteger)defaultTabsInTabBarCount
        maximumTabsInTabBarCount:(NSUInteger)maximumTabsInTabBarCount
              moreTabInformation:(BBVATabBarTabInformation *)moreTabInformation
          moreTabElementsOrdered:(BOOL)moreTabElementsOrdered;

@end
