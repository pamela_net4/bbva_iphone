/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "BBVATabBarView.h"


//Forward declarations
@class BBVATabBarContainerView;


/**
 * BBVATabBarContainerView delegate protocol. Delegate is notified when the user taps on the collapse button and on any tab bar notification
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol BBVATabBarContainerViewDelegate <BBVATabBarViewDelegate>

@optional

/**
 * BBVATabBarContainerView notifies the delegate that the collapse/expand button has been tapped
 *
 * @param bbvaTabBarContainerView The BBVATabBarContainerView instance triggering the event
 */
- (void)bbvaTabBarContainerViewCollapseButtonTapped:(BBVATabBarContainerView *)bbvaTabBarContainerView;

@end
 

/**
 * View to contain the tab bar view. In iPhone version it just contains the tab bar, but in iPad version it contains the tab bar and the
 * collapse/expand button
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BBVATabBarContainerView : UIView {
    
@private
    
    /**
     * Tab bar view
     */
    BBVATabBarView *tabBarView_;
    
    /**
     * Collapse button view (iPad only)
     */
    UIView *collapseButtonView_;
    
    /**
     * Collapse button (iPad only)
     */
    UIButton *collapseButton_;
    
    /**
     * Collapse button right arrow image (iPad only)
     */
    UIImage *rightArrowImage_;
    
    /**
     * Collapse button left arrow image (iPad only)
     */
    UIImage *leftArrowImage_;
    
    /**
     * iPhone device flag. YES when the device is an iPhone, NO when it is an iPad
     */
    BOOL iPhoneDevice_;
    
    /**
     * Tab bar displayed on the right hand side flag. YES when the tab bar is displayed on the right hand side of screen, NO when it is displayed
     * on the left hand side. (iPad only)
     */
    BOOL tabBarOnRightHandSide_;
    
    /**
     * Tab bar collapsed flag. YES when the tab bar is collapsed and only the show/hide button can be displayed, NO when it is expanded.
     *  (iPad only)
     */
    BOOL tabBarCollapsed_;
    
    /**
     * Display collapse/expand button flag. YES to display the collapse/expand button, NO to hide it. (iPad only)
     */
    BOOL displayCollapseButtonFlag_;

    /**
     * BBVA tab bar container view delegate
     */
    id<BBVATabBarContainerViewDelegate> bbvaTabBarcontainerViewDelegate_;
    
}


/**
 * Provides read-write access to the array containing the tabs to display
 */
@property (nonatomic, readwrite, retain) NSArray *tabsList;

/**
 * Provides read-write access to the collapse button right arrow image
 */
@property (nonatomic, readwrite, retain) UIImage *rightArrowImage;

/**
 * Provides read-write access to the collapse button left arrow image
 */
@property (nonatomic, readwrite, assign) UIImage *leftArrowImage;

/**
 * Provides read-write access to the information text
 */
@property (nonatomic, readwrite, copy) NSString *informationText;

/**
 * Provides read-write access to the BBVA logo image
 */
@property (nonatomic, readwrite, retain) UIImage *logoImage;

/**
 * Provides read-write access to the selected tab background color
 */
@property (nonatomic, readwrite, retain) UIColor *selectedTabBackgroundColor;

/**
 * Provides read-write access to the tab on the right hand side flag
 */
@property (nonatomic, readwrite, assign) BOOL tabBarOnRightHandSide;

/**
 * Provides readwrite access to the tab bar collapsed flag
 */
@property (nonatomic, readwrite, assign) BOOL tabBarCollapsed;

/**
 * Provides read-write access to the display collapse/expand button flag
 */
@property (nonatomic, readwrite, assign) BOOL displayCollapseButtonFlag;

/**
 * BBVA tab bar container view delegate
 */
@property (nonatomic, readwrite, assign) id<BBVATabBarContainerViewDelegate> bbvaTabBarcontainerViewDelegate;


/**
 * Returns the iPhone tab bar container view height
 *
 * @return The iPhone tab bar container view height
 */
+ (CGFloat)iPhoneTabBarContainerHeight;

/**
 * Returns the iPad tab bar container view width
 *
 * @return The iPad tab bar container view width
 */
+ (CGFloat)iPadTabBarContainerWidth;


/**
 * Selects a given view controller, if found among the tab information
 *
 * @param viewController The view controller to select
 * @return The selected tab information
 */
- (BBVATabBarTabInformation *)selectViewController:(UIViewController *)viewController;

/**
 * Returns the tab bar index for a given view controller
 *
 * @return The tab bar index for a given view controller o NSNotFound if the view controller is not managed by the tab bar view
 */
- (NSUInteger)tabBarIndexForViewController:(UIViewController *)viewController;

/**
 * Selects the given tab
 *
 * @param tab The tab to select
 */
- (void)selectTab:(BBVATabBarTabInformation *)tab;

@end
