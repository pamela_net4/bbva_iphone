/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BBVATabBarBackground.h"
#import "BBVATabBarConstants.h"
#import "BBVATabBarDrawingUtils.h"


/**
 * Defines the BBVA logo image height measured in points
 */
#define I_PAD_LOGO_IMAGE_HEIGHT                     25.0f

/**
 * Defines the BBVA logo image bottom offset measured in points
 */
#define I_PAD_LOGO_IMAGE_BOTTOM_OFFSET              8.0f

/**
 * Defines the information label horizontal offset measured in points
 */
#define I_PAD_INFORMATION_LABEL_HORIZONTAL_OFFSET   5.0f

/**
 * Defines the information label bottom offset measured in points
 */
#define I_PAD_INFORMATION_LABEL_BOTTOM_OFFSET       8.0f


#pragma mark -

/**
 * BBVATabBarBackground private extension
 */
@interface BBVATabBarBackground()

/**
 * Initializes the view
 *
 * @private
 */
- (void)initializeBBVATabBarBackground;

/**
 * Sets the logo image frame for the given height
 *
 * @param logoImageHeight The logo image height
 * @private
 */
- (void)setLogoImageHeightForHeight:(CGFloat)logoImageHeight;

/**
 * Lays out the information label
 *
 * @private
 */
- (void)layoutInformationLabel;

@end


#pragma mark -

@implementation BBVATabBarBackground

#pragma mark -
#pragma mark Properties

@dynamic informationText;
@synthesize bbvaLogoImageView = bbvaLogoImageView_;
@dynamic logoImage;
@synthesize iPhoneDevice = iPhoneDevice_;
@synthesize tabsCount = tabsCount_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [informationLabel_ release];
    informationLabel_ = nil;
    
    [bbvaLogoImageView_ release];
    bbvaLogoImageView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initializes the view
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initializeBBVATabBarBackground];
    
}

/**
 * Designated initializer. It stablishes the view height to a fixed value
 *
 * @param frame The view initial frame which height is fixed
 * @return The initialized BBVATabBarView insntace
 */
- (id)initWithFrame:(CGRect)frame {
    
    BOOL iPhoneDevice = YES;
    
#ifdef UI_USER_INTERFACE_IDIOM
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        iPhoneDevice = NO;
        
    }
    
#endif
    
    if (iPhoneDevice) {
        
        frame.size.height = BBVA_I_PHONE_TAB_BAR_VIEW_HEIGHT;
        
    } else {
        
        frame.size.width = BBVA_I_PAD_TAB_BAR_VIEW_WIDTH;
        
    }
    
    if ((self = [super initWithFrame:frame])) {
        
        [self initializeBBVATabBarBackground];
        
    }
    
    return self;
    
}

/*
 * Initializes the view
 */
- (void)initializeBBVATabBarBackground {
    
    iPhoneDevice_ = YES;
    
#ifdef UI_USER_INTERFACE_IDIOM
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        iPhoneDevice_ = NO;
        
    }
    
#endif
    
    if (!iPhoneDevice_) {
        
        bbvaLogoImageView_ = [[UIImageView allocWithZone:self.zone] initWithFrame:CGRectZero];
        [self setLogoImageHeightForHeight:I_PAD_LOGO_IMAGE_HEIGHT];
        [self addSubview:bbvaLogoImageView_];
        
        informationLabel_ = [[UILabel alloc] initWithFrame:CGRectZero];
        informationLabel_.backgroundColor = [UIColor clearColor];
        informationLabel_.font = [UIFont systemFontOfSize:14.0f];
        informationLabel_.minimumFontSize = 14.0f;
        informationLabel_.numberOfLines = 0;
        informationLabel_.textColor = [UIColor colorWithWhite:0.9f
                                                        alpha:1.0f];
        [self addSubview:informationLabel_];
        
    }
    
    self.backgroundColor = [UIColor clearColor];
    
}

#pragma mark -
#pragma mark Drawing methods

/**
 * Draws the receiver’s image within the passed-in rectangle
 *
 * @param rect A rectangle defining the area to restrict drawing to
 */
- (void)drawRect:(CGRect)rect {
    
    CGRect drawingRect = self.frame;
    drawingRect.origin.x = 0.0f;
    drawingRect.origin.y = 0.0f;
    CGFloat maxY = BBVA_I_PHONE_TAB_BAR_VIEW_HEIGHT;
    CGFloat maxX = BBVA_I_PAD_TAB_BAR_VIEW_WIDTH;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    CGContextSetAllowsAntialiasing(context, true);
    
    CGFloat viewWidth = CGRectGetWidth(drawingRect);
    CGFloat viewHeight = CGRectGetHeight(drawingRect);
    
    [self clipContext:context
       forScreenWidth:viewWidth
               height:viewHeight];
    
    [BBVATabBarDrawingUtils drawBackgroundGradientIntoContext:context
                                                  withinFrame:drawingRect
                                             foriPhoneTabType:iPhoneDevice_];
    
    NSUInteger tabsCount = self.tabsCount;
    
    if (tabsCount > 0) {
        
        CGFloat tabWidth = [self tabWidth];
        CGFloat tabHeight = [self tabHeight];
        
        if (tabsCount > 1) {
            
            CGFloat separatorPosition = 0.0f;
            
            NSUInteger linesToDraw = tabsCount;
            
            if (!iPhoneDevice_) {
                
                linesToDraw ++;
                
            }
            
            for (NSUInteger i = 1; i < linesToDraw; i++) {
                
                if (iPhoneDevice_) {
                    
                    separatorPosition = ((CGFloat) i) * tabWidth;
                    
                    [BBVATabBarDrawingUtils drawTabSeparatorIntoContext:context
                                                                fromMin:0.0f
                                                                  toMax:maxY
                                                             atPosition:separatorPosition
                                                       foriPhoneTabType:iPhoneDevice_];
                    
                } else {
                    
                    separatorPosition = ((CGFloat) i) * tabHeight;
                    
                    [BBVATabBarDrawingUtils drawTabSeparatorIntoContext:context
                                                                fromMin:0.0f
                                                                  toMax:maxX
                                                             atPosition:separatorPosition
                                                       foriPhoneTabType:iPhoneDevice_];
                    
                }
                
            }
            
        }
        
    }
    
    CGContextRestoreGState(context);
    
}

/*
 * Clips the given context to the tab shape
 */
- (void)clipContext:(CGContextRef)context
     forScreenWidth:(CGFloat)width
             height:(CGFloat)height {
    
    if (!iPhoneDevice_) {
        
        CGContextMoveToPoint(context, 0.0f, BBVA_I_PAD_TAB_ROUNDED_CORNERS_RADIUS);
        CGContextAddArcToPoint(context, 0.0f, 0.0f,
                               BBVA_I_PAD_TAB_ROUNDED_CORNERS_RADIUS, 0.0f,
                               BBVA_I_PAD_TAB_ROUNDED_CORNERS_RADIUS);
        CGContextAddArcToPoint(context, width, 0.0f,
                               width, BBVA_I_PAD_TAB_ROUNDED_CORNERS_RADIUS,
                               BBVA_I_PAD_TAB_ROUNDED_CORNERS_RADIUS);
        CGContextAddLineToPoint(context, width, height);
        CGContextAddLineToPoint(context, 0.0f, height);
        CGContextClosePath(context);
        CGContextClip(context);
        
    }

}

#pragma mark -
#pragma mark Utility selectors

/*
 * Calculates the tab width
 */
- (CGFloat)tabWidth {
    
    CGFloat result = BBVA_I_PAD_TAB_BAR_VIEW_WIDTH;
    
    if (iPhoneDevice_) {
        
        CGRect frame = self.frame;
        CGFloat width = frame.size.width;
        NSUInteger tabsCount = self.tabsCount;
        
        if (tabsCount > 0) {
            
            result = round(width / (CGFloat)tabsCount);
            
        } else {
            
            result = width;
            
        }
        
    }
    
    return result;
    
}

/*
 * Calculates the tab height
 */
- (CGFloat)tabHeight {
    
    CGFloat result = BBVA_I_PAD_TABS_HEIGHT;
    
    if (iPhoneDevice_) {
        
        result = BBVA_I_PHONE_TAB_BAR_VIEW_HEIGHT;
        
    }
    
    return result;
    
}

/*
 * Sets the logo image frame for the given height
 */
- (void)setLogoImageHeightForHeight:(CGFloat)logoImageHeight {
    
    if (logoImageHeight < 0.0f) {
        
        logoImageHeight = 0.0f;
        
    }

    CGRect logoImageFrame = CGRectMake(0.0f,
                                       self.frame.size.height - logoImageHeight - I_PAD_LOGO_IMAGE_BOTTOM_OFFSET,
                                       [BBVATabBarBackground iPadTabBarWidth],
                                       logoImageHeight);
    bbvaLogoImageView_.frame = logoImageFrame;
    bbvaLogoImageView_.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    bbvaLogoImageView_.contentMode = UIViewContentModeScaleAspectFit;
    
    [self layoutInformationLabel];

}

/*
 * Lays out the information label
 */
- (void)layoutInformationLabel {
    
    CGFloat labelWidth = self.frame.size.width - (2.0f * I_PAD_INFORMATION_LABEL_HORIZONTAL_OFFSET);
    
    CGSize labelSize = [informationLabel_.text sizeWithFont:informationLabel_.font
                                          constrainedToSize:CGSizeMake(labelWidth, 1000.0f)
                                              lineBreakMode:UILineBreakModeWordWrap];
    
    CGRect imageFrame = bbvaLogoImageView_.frame;
    
    CGFloat labelHeight = labelSize.height;
    CGFloat labelTop = CGRectGetMinY(imageFrame) - labelHeight - I_PAD_INFORMATION_LABEL_BOTTOM_OFFSET;
    
    if (labelTop <= 0) {
        
        labelTop = self.frame.size.height - labelHeight - I_PAD_INFORMATION_LABEL_BOTTOM_OFFSET;
        
    }
    
    CGRect labelFrame = CGRectMake(I_PAD_INFORMATION_LABEL_HORIZONTAL_OFFSET,
                                   labelTop,
                                   labelWidth,
                                   labelHeight);
    informationLabel_.frame = labelFrame;
    informationLabel_.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;

}

#pragma mark -
#pragma mark UIView methods

/**
 * Sets the new view frame. Its height is fixed to a given amount
 *
 * @param frame The view new frame which height is going to be fixed
 */
- (void)setFrame:(CGRect)frame {
    
    iPhoneDevice_ = YES;
    
#ifdef UI_USER_INTERFACE_IDIOM
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        iPhoneDevice_ = NO;
        
    }
    
#endif
    
    if (iPhoneDevice_) {
        
        frame.size.height = BBVA_I_PHONE_TAB_BAR_VIEW_HEIGHT;
        
    } else {
        
        frame.size.width = BBVA_I_PAD_TAB_BAR_VIEW_WIDTH;
        
    }
    
    [super setFrame:frame];
    
    [self setNeedsDisplay];
    
}

#pragma mark -
#pragma mark Information access

/*
 * Returns the iPhone tab bar view height
 */
+ (CGFloat)iPhoneTabBarHeight {
    
    return BBVA_I_PHONE_TAB_BAR_VIEW_HEIGHT;
    
}

/*
 * Returns the iPad tab bar view width
 */
+ (CGFloat)iPadTabBarWidth {
    
    return BBVA_I_PAD_TAB_BAR_VIEW_WIDTH;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the information text
 *
 * @return The information text
 */
- (NSString *)informationText {
    
    return informationLabel_.text;
    
}

/*
 * Sets the information text
 *
 * @param informationText The information text to set
 */
- (void)setInformationText:(NSString *)informationText {
    
    if (!iPhoneDevice_) {
        
        informationLabel_.text = informationText;
        [self layoutInformationLabel];
        
    }
    
}

/*
 * Returns the logo image
 *
 * @return The logo image
 */
- (UIImage *)logoImage {
    
    return bbvaLogoImageView_.image;
    
}

/*
 * Sets the logo image
 *
 * @param logoImage The new logo image to display
 */
- (void)setLogoImage:(UIImage *)logoImage {
    
    if (!iPhoneDevice_) {
        
        bbvaLogoImageView_.image = logoImage;
        [self setLogoImageHeightForHeight:logoImage.size.height];
        
    }
    
}

@end
