/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MoreTab_iPadViewController.h"
#import "MoreTabViewController+protected.h"
#import "BBVATabBarTabInformation.h"


/**
 * Defines the NIB file associated to the MoreTab_iPadViewController
 */
#define MORE_TAB_IPAD_VIEW_CONTROLLER_NIB_FILE                                              @"MoreTab_iPadViewController"

/**
 * Defines the table width in landscape orientation
 */
#define TABLE_WIDTH_IN_LANDSCAPE_ORIENTATION                                                300.0f

/**
 * Defines the table width in portrait orientation
 */
#define TABLE_WIDTH_IN_PORTRAIT_ORIENTATION                                                 248.0f


/**
 * Defines the hide/show more tab table animation duration (measured in seconds)
 */
#define MORE_TAB_TABLE_ANIMATION_DURATION                                                   0.15f

/**
 * Defines the hide more tab table animation identification
 */
#define HIDE_MORE_TAB_TABLE_ANIMATION_ID                                                    @"animation.MoreTab_iPadViewController.hideMoreTabTable"

/**
 * Defines the show more tab table animation identification
 */
#define SHOW_MORE_TAB_TABLE_ANIMATION_ID                                                    @"animation.MoreTab_iPadViewController.showMoreTabTable"


#pragma mark -

/**
 * MoreTab_iPadViewController private extension
 */
@interface MoreTab_iPadViewController()

/**
 * Releases the graphic elements not needed when view is hidden
 *
 * @private
 */
- (void)releaseMoreTab_iPadViewControllerGraphicElements;

/**
 * Creates a new background image with the provided background color
 *
 * @param backgroundColor The background color to use with the gradient
 * @private
 */
- (void)createBackgroundImageWithBackgroundColor:(UIColor *)backgroundColor;

/**
 * Shows the more tab table
 *
 * @private
 */
- (void)showMoreTabTable;

/**
 * Hides the more tab table
 *
 * @private
 */
- (void)hideMoreTabTable;

/**
 * Recalculates the more tab table and current view sizes and location
 *
 * @private
 */
- (void)recalculateLayout;

/**
 * Sets the more tab table visibility animated or not. When animated, the tab var is moved down.
 *
 * @param visible The visitility flag
 * @param animated YES to animate the tab bar
 * @private
 */
- (void)setMoreTabTableVisibility:(BOOL)visible
                         animated:(BOOL)animated;

/**
 * The more tab table animation did stop event selector. When hidding, the more tab table hidden propertie is updated
 *
 * @param animationID An NSString containing an optional application-supplied identifier
 * @param finished An NSNumber object containing a Boolean value. The value is YES if the animation ran to completion before it stopped or NO if it did not
 * @param context An optional application-supplied context
 * @private
 */
- (void)moreTabTableAnimationDidStop:(NSString *)animationID
                            finished:(NSNumber *)finished
                             context:(void *)context;

/**
 * Invoked when the hide more tab table animation is finished. User interaction is restored
 *
 * @private
 */
- (void)hideMoreTabTableAnimationFinished;

/**
 * Invoked when the show more tab table animation is finished. User interaction is restored
 *
 * @private
 */
- (void)showMoreTabTableAnimationFinished;

/**
 * Calculates the child view frame
 *
 * @return The calculated child view frame
 * @private
 */
- (CGRect)calculateChildFrame;

/**
 * Selects the given tab bar into the more table view and the child view
 *
 * @param tabBar The tab bar to select
 * @private
 */
- (void)selectTabIntoTableView:(BBVATabBarTabInformation *)tabBar;

@end


#pragma mark -

@implementation MoreTab_iPadViewController

#pragma mark -
#pragma marl Properties

@synthesize tableContainerView = tableContainerView_;
@synthesize tableBackgroundImageView = tableBackgroundImageView_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self releaseMoreTab_iPadViewControllerGraphicElements];
    
    [currentViewController_ release];
    currentViewController_ = nil;
    
    [tableBackgroundImage_ release];
    tableBackgroundImage_ = nil;
    
    [selectedTabBar_ release];
    selectedTabBar_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory. The navigation controller array is stored to restore it in case it is necessary
 */
- (void)viewDidUnload {
    
    [self releaseMoreTab_iPadViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/*
 * Releases the graphic elements not needed when view is hidden
 */
- (void)releaseMoreTab_iPadViewControllerGraphicElements {
    
    [tableContainerView_ release];
    tableContainerView_ = nil;
    
    [tableBackgroundImageView_ release];
    tableBackgroundImageView_ = nil;
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseMoreTab_iPadViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Creates a background image for the table
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self createBackgroundImageWithBackgroundColor:[UIColor whiteColor]];
    
    NSString *iOSVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([iOSVersion compare:@"5.0" options:NSNumericSearch] != NSOrderedAscending) {
        
        newOSVersion_ = YES;
        
    } else {
        
        newOSVersion_ = NO;
        
    }

}

/*
 * Returns a new autoreleased MoreTab_iPadViewController created from its NIB file
 */
+ (MoreTab_iPadViewController *)moreTab_iPadViewController {
    
    MoreTab_iPadViewController *result = [[[MoreTab_iPadViewController alloc] initWithNibName:MORE_TAB_IPAD_VIEW_CONTROLLER_NIB_FILE bundle:nil] autorelease];
    [result awakeFromNib];
    return result;
    
}

#pragma mark -
#pragma mark View management

/**
 * Called after the controller’s view is loaded into memory. The table background image is set
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    tableBackgroundImageView_.image = tableBackgroundImage_;

    if (selectedTabBar_ != nil) {
        
        NSArray *tabList = self.tabsList;

        if (![tabList containsObject:selectedTabBar_]) {
            
            [selectedTabBar_ release];
            selectedTabBar_ = nil;
            
        } else {
            
            if ([self checkTabBarCanBeDisplayed:selectedTabBar_]) {
                
                [self.moreTabsTableView reloadData];
                
                [self selectTabIntoTableView:selectedTabBar_];
                
                CGRect childFrame = [self calculateChildFrame];
                UIView *childView = currentViewController_.view;
                childView.frame = childFrame;
                UIView *view = self.view;
                [view addSubview:childView];
                [view bringSubviewToFront:tableContainerView_];

            } else {
                
                [selectedTabBar_ release];
                selectedTabBar_ = nil;
                
                [currentViewController_ release];
                currentViewController_ = nil;
                
            }

        }
        
    }
    
    if (newOSVersion_) {
        
        mustPropagateViewWillAppearForNewVersion_ = YES;
        
    }

}

/**
 * Notifies the view controller that its view is about to be become visible. The selected view controller is notified
 *
 * @param animated If YES, the view is being added to the window using an animation
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    viewVisible_ = YES;
    
    NSArray *tabList = self.tabsList;
    
    if (selectedTabBar_ != nil) {
        
        if (![tabList containsObject:selectedTabBar_]) {
            
            [selectedTabBar_ release];
            selectedTabBar_ = nil;
            
        }
            
    }
    
    BOOL newViewControllerLoaded = NO;
    
    if (selectedTabBar_ == nil) {
        
        [self setMoreTabTableVisibility:YES
                               animated:NO];
        
        BBVATabBarTabInformation *tabBarInformation;
        NSUInteger tabsCount = [tabList count];
        
        for (NSUInteger i = 0; i < tabsCount; i++) {
            
            tabBarInformation = [tabList objectAtIndex:i];
            
            if ((tabBarInformation.viewController != nil) && ([self checkTabBarCanBeDisplayed:tabBarInformation])) {
                
                [self selectTabIntoTableView:tabBarInformation];
                newViewControllerLoaded = YES;
                break;
                
            }
            
        }
        
    }

    [self loadTableInformation];
    
    if ([currentViewController_ conformsToProtocol:@protocol(MoreTab_iPadClient)]) {
        
        id<MoreTab_iPadClient> moreTabClient = (id<MoreTab_iPadClient>)currentViewController_;
        [moreTabClient setManager:self];
        
    }

    if (!newViewControllerLoaded) {
        
        [currentViewController_ viewWillAppear:animated];
        
    }
    
}

/**
 * Notifies the view controller that its view was added to a window. The selected view controller is notified
 *
 * @param animated If YES, the view was added to the window using an animation
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];

    if ((newOSVersion_) && (mustPropagateViewWillAppearForNewVersion_)) {
        
        [currentViewController_ viewWillAppear:NO];
        
    }
    
    [currentViewController_ viewDidAppear:animated];
    
    mustPropagateViewWillAppearForNewVersion_ = NO;
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. The selected view controller
 * is notified
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];

    if ([currentViewController_ conformsToProtocol:@protocol(MoreTab_iPadClient)]) {
        
        id<MoreTab_iPadClient> moreTabClient = (id<MoreTab_iPadClient>)currentViewController_;
        [moreTabClient setManager:nil];
        
    }

    [currentViewController_ viewWillDisappear:animated];
    
}

/**
 * Notifies the view controller that its view was dismissed, covered, or otherwise hidden from view. The selected view controller is notified
 *
 * @param animated If YES, the disappearance of the view was animated
 */
- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    viewVisible_ = NO;

    [currentViewController_ viewDidDisappear:animated];
    
}

/**
 * Returns a Boolean value indicating whether the view controller supports the specified orientation. The selected view controller is
 * requested to decide the result
 *
 * @param interfaceOrientation The orientation of the application’s user interface after the rotation
 * @return YES when the embedded navigation controller allows the interface orientation, NO otherwise
 */
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    BOOL result = YES;
    
    if (currentViewController_ != nil) {
        
        result = [currentViewController_ shouldAutorotateToInterfaceOrientation:interfaceOrientation];
        
    }
    
    return result;
    
}

/**
 * Sent to the view controller just before the user interface begins rotating. The event is propagated to the selected view controller and the interface is adapted
 *
 * @param toInterfaceOrientation The new orientation for the user interface
 * @param duration The duration of the pending rotation, measured in seconds
 */
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation
                                   duration:duration];
    
    [currentViewController_ willRotateToInterfaceOrientation:toInterfaceOrientation
                                                    duration:duration];
    
}

/**
 * Sent to the view controller after the user interface rotates. The event is propagated to the selected view controller
 *
 * @param fromInterfaceOrientation The old orientation of the user interface
 */
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    [currentViewController_ didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
}

/**
 * Sent to the view controller before performing a one-step user interface rotation. The event is propagated to the selected view controller
 * 
 * @param interfaceOrientation The new orientation for the user interface
 * @param duration The duration of the pending rotation, measured in seconds
 */
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
                                         duration:(NSTimeInterval)duration {
    
    [super willAnimateRotationToInterfaceOrientation:interfaceOrientation
                                            duration:duration];
    
    [self recalculateLayout];
    
    [currentViewController_ willAnimateRotationToInterfaceOrientation:interfaceOrientation
                                                             duration:duration];
    
}

/**
 * Sent to the view controller before performing the first half of a user interface rotation. The event is propagated to the selected view controller
 *
 * @param toInterfaceOrientation The state of the application’s user interface orientation before the rotation
 * @param duration The duration of the first half of the pending rotation, measured in seconds
 */
- (void)willAnimateFirstHalfOfRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                                    duration:(NSTimeInterval)duration {
    
    [super willAnimateFirstHalfOfRotationToInterfaceOrientation:toInterfaceOrientation
                                                       duration:duration];
    
    [currentViewController_ willAnimateFirstHalfOfRotationToInterfaceOrientation:toInterfaceOrientation
                                                                        duration:duration];
    
}

/**
 * Sent to the view controller before the second half of the user interface rotates. The event is propagated to the selected view controller
 *
 * @param fromInterfaceOrientation The state of the application’s user interface orientation before the rotation
 * @param duration The duration of the second half of the pending rotation, measured in seconds
 */
- (void)willAnimateSecondHalfOfRotationFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
                                                       duration:(NSTimeInterval)duration {
    
    [super willAnimateSecondHalfOfRotationFromInterfaceOrientation:fromInterfaceOrientation
                                                          duration:duration];
    
    [self recalculateLayout];
    
    [currentViewController_ willAnimateSecondHalfOfRotationFromInterfaceOrientation:fromInterfaceOrientation
                                                                           duration:duration];
    
}

/**
 * Sent to the view controller after the completion of the first half of the user interface rotation. The event is propagated to the selected view controller
 *
 * @param toInterfaceOrientation The state of the application’s user interface orientation after the rotation
 */
- (void)didAnimateFirstHalfOfRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    
    [super didAnimateFirstHalfOfRotationToInterfaceOrientation:toInterfaceOrientation];
    
    [currentViewController_ didAnimateFirstHalfOfRotationToInterfaceOrientation:toInterfaceOrientation];
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Creates a new background image with the provided background color
 */
- (void)createBackgroundImageWithBackgroundColor:(UIColor *)backgroundColor {
    
    if ((backgroundColor == nil) || ([backgroundColor isEqual:[UIColor clearColor]])) {
        
        backgroundColor = [UIColor whiteColor];
        
    }
    
    [tableBackgroundImage_ release];
    tableBackgroundImage_ = nil;
    
    CGFloat scale = 1.0f;
    UIScreen *screen = [UIScreen mainScreen];
    
    if (([screen respondsToSelector:@selector(scale)]) &&
        ([UIImage respondsToSelector:@selector(imageWithCGImage:scale:orientation:)])) {
        
        scale = screen.scale;
        
    }
    
    CGFloat imageWidth = 20.0f * scale;
    CGFloat imageHeight = 4.0f * scale;

    CGRect imageFrame = CGRectMake(0.0f, 0.0f, imageWidth, imageWidth);

    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef bitmapContext = CGBitmapContextCreate(NULL,
                                                       imageWidth,
                                                       imageHeight,
                                                       8,
                                                       (4 * imageWidth),
                                                       colorSpaceRef,
                                                       kCGImageAlphaPremultipliedFirst);
    
    CGContextSetFillColorWithColor(bitmapContext, backgroundColor.CGColor);
    CGContextFillRect(bitmapContext, imageFrame);

	CGFloat colors[8] = { 
		0.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 0.25f
	};
    
	CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpaceRef, colors, NULL, 2);
    
    CGContextDrawLinearGradient(bitmapContext, gradient, CGPointMake(4.0f * scale, 0.0f), CGPointMake(imageWidth - scale, 0.0f), 0);
    
    CGContextSaveGState(bitmapContext);
    CGContextSetStrokeColorWithColor(bitmapContext, [UIColor darkGrayColor].CGColor);
    CGContextSetLineWidth(bitmapContext, scale);
    CGContextBeginPath(bitmapContext);
    CGContextMoveToPoint(bitmapContext, imageWidth, 0.0f);
    CGContextAddLineToPoint(bitmapContext, imageWidth, imageHeight);
    CGContextStrokePath(bitmapContext);
    CGContextRestoreGState(bitmapContext);
    
    CGImageRef image = CGBitmapContextCreateImage(bitmapContext);
    
    if ([UIImage respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        
        tableBackgroundImage_ = [UIImage imageWithCGImage:image
                                     scale:scale
                               orientation:UIImageOrientationUp];
        
    } else {
        
        tableBackgroundImage_ = [UIImage imageWithCGImage:image];
        
    }

    if ([tableBackgroundImage_ respondsToSelector:@selector(resizableImageWithCapInsets:)]) {
        
        tableBackgroundImage_ = [[tableBackgroundImage_ resizableImageWithCapInsets:UIEdgeInsetsMake(0.0f, scale, 0.0f, imageWidth - (2.0f * scale))] retain];
        
    } else {

        tableBackgroundImage_ = [[tableBackgroundImage_ stretchableImageWithLeftCapWidth:scale
                                                                            topCapHeight:0.0f] retain];
        
    }
    
    CGImageRelease(image);
    CGGradientRelease(gradient);
    CGContextRelease(bitmapContext);
    CGColorSpaceRelease(colorSpaceRef);
    
    tableBackgroundImageView_.image = tableBackgroundImage_;

}

/*
 * Recalculates the more tab table and current view sizes and location
 */
- (void)recalculateLayout {
    
    if ([self isViewLoaded]) {
        
        UIView *ownView = self.view;
        CGRect bounds = ownView.bounds;
        CGFloat boundsWidth = CGRectGetMaxX(bounds) - CGRectGetMinX(bounds);
        CGFloat boundsHeight = CGRectGetMaxY(bounds) - CGRectGetMinY(bounds);
        
        UIView *currentView = currentViewController_.view;
        
        CGRect moreTableFrame = tableContainerView_.frame;
        CGRect childFrame = bounds;
        childFrame.origin.x = 0.0f;
        childFrame.origin.y = 0.0f;
        
        UIInterfaceOrientation interfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
        
        CGFloat tableWidth = TABLE_WIDTH_IN_PORTRAIT_ORIENTATION;
        
        if (UIInterfaceOrientationIsLandscape(interfaceOrientation)) {
            
            tableWidth = TABLE_WIDTH_IN_LANDSCAPE_ORIENTATION;
            
        }

        moreTableFrame.size.height = boundsHeight;
        moreTableFrame.size.width = tableWidth;
        
        if (tableCollapsed_) {
            
            moreTableFrame.origin.x = -tableWidth;
            
        } else {
            
            moreTableFrame.origin.x = 0.0f;

        }
        
        CGFloat childViewWidth = boundsWidth;
        CGFloat childViewLeft = 0.0f;
        
        if (!tableCollapsed_) {
            
            childViewWidth -= tableWidth;
            childViewLeft = tableWidth;
            
        }
        
        childFrame.origin.x = childViewLeft;
        childFrame.size.width = childViewWidth;

        tableContainerView_.frame = moreTableFrame;
        currentView.frame = childFrame;
        
    }

    [self loadTableInformation];

}

/*
 * Calculates the child view frame
 */
- (CGRect)calculateChildFrame {
    
    CGRect result = CGRectZero;
    
    if ([self isViewLoaded]) {
        
        UIView *ownView = self.view;
        CGRect bounds = ownView.bounds;
        CGFloat boundsWidth = CGRectGetMaxX(bounds) - CGRectGetMinX(bounds);
        
        result = bounds;
        result.origin.x = 0.0f;
        result.origin.y = 0.0f;
        
        UIInterfaceOrientation interfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
        
        CGFloat tableWidth = TABLE_WIDTH_IN_PORTRAIT_ORIENTATION;
        
        if (UIInterfaceOrientationIsLandscape(interfaceOrientation)) {
            
            tableWidth = TABLE_WIDTH_IN_LANDSCAPE_ORIENTATION;
            
        }
        
        CGFloat childViewWidth = boundsWidth;
        CGFloat childViewLeft = 0.0f;
        
        if (!tableCollapsed_) {
            
            childViewWidth -= tableWidth;
            childViewLeft = tableWidth;
            
        }
        
        result.origin.x = childViewLeft;
        result.size.width = childViewWidth;
        
    }
    
    return result;
    
}

/**
 * Resets the more tab bar to the first tab. Only available on iPad
 */
- (void)resetMoreTabToFirstTab {
    
    [self selectTabIntoTableView:self.firstTabBar];
    
}

#pragma mark -
#pragma mark More tab table management

/*
 * Sets the more tab table visibility animated or not. When animated, the tab var is moved down.
 */
- (void)setMoreTabTableVisibility:(BOOL)visible
                         animated:(BOOL)animated {
    
    if (tableCollapsed_ == visible) {
        
        if (animated) {
            
            tableContainerView_.userInteractionEnabled = NO;
            currentViewController_.view.userInteractionEnabled = NO;
            
            if ([UIView respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
                
                [UIView animateWithDuration:MORE_TAB_TABLE_ANIMATION_DURATION
                                      delay:0.0f
                                    options:(UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState)
                                 animations:^{
                                     
                                     if (visible) {
                                         
                                         [self showMoreTabTable];
                                         
                                     } else {
                                         
                                         [self hideMoreTabTable];
                                         
                                     }
                                     
                                 }
                                 completion:^(BOOL finished) {
                                     
                                     if (visible) {
                                         
                                         [self showMoreTabTableAnimationFinished];
                                         
                                     } else {
                                         
                                         [self hideMoreTabTableAnimationFinished];
                                         
                                     }
                                     
                                 }];
                
            } else {
                
                NSString *animationId = SHOW_MORE_TAB_TABLE_ANIMATION_ID;
                
                if (!visible) {
                    
                    animationId = HIDE_MORE_TAB_TABLE_ANIMATION_ID;
                    
                }
                
                [UIView beginAnimations:animationId context:nil];
                [UIView setAnimationDuration:MORE_TAB_TABLE_ANIMATION_DURATION];
                [UIView setAnimationBeginsFromCurrentState:YES];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                [UIView setAnimationDelegate:self];
                [UIView setAnimationDidStopSelector:@selector(moreTabTableAnimationDidStop:finished:context:)];
                
                if (visible) {
                    
                    [self showMoreTabTable];
                    
                } else {
                    
                    [self hideMoreTabTable];
                    
                }
                
                [UIView commitAnimations];
                
            }
            
        } else {
            
            if (visible) {
                
                [self showMoreTabTable];
                [self showMoreTabTableAnimationFinished];
                
            } else {
                
                [self hideMoreTabTable];
                [self hideMoreTabTableAnimationFinished];
                
            }
            
        }
        
        tableCollapsed_ = !visible;
        
    }

}

/*
 * Shows the more tab table
 */
- (void)showMoreTabTable {
    
    if ([tableContainerView_ superview] == nil) {
        
        UIView *ownView = self.view;
        [ownView addSubview:tableContainerView_];
        [ownView bringSubviewToFront:tableContainerView_];
        
    }
    
    if (tableCollapsed_) {
        
        tableCollapsed_ = NO;
        tableContainerView_.hidden = NO;
        [self recalculateLayout];
        
    }

}

/*
 * Hides the more tab table
 */
- (void)hideMoreTabTable {
    
    if (!tableCollapsed_) {
        
        tableCollapsed_ = YES;
        [self recalculateLayout];
        
    }

}

/*
 * The more tab table animation did stop event selector. When hidding, the more tab table hidden propertie is updated
 */
- (void)moreTabTableAnimationDidStop:(NSString *)animationID
                            finished:(NSNumber *)finished
                             context:(void *)context {
    
    if ([animationID isEqualToString:HIDE_MORE_TAB_TABLE_ANIMATION_ID]) {
        
        [self hideMoreTabTableAnimationFinished];
        
    } else if ([animationID isEqualToString:SHOW_MORE_TAB_TABLE_ANIMATION_ID]) {
        
        [self showMoreTabTableAnimationFinished];
        
    }

}

/*
 * Invoked when the hide more tab table animation is finished. User interaction is restored
 */
- (void)hideMoreTabTableAnimationFinished {
    
    tableContainerView_.userInteractionEnabled = YES;
    currentViewController_.view.userInteractionEnabled = YES;
    
    if (!tableCollapsed_) {
        
        tableContainerView_.hidden = YES;
        [tableContainerView_ removeFromSuperview];
        
    }

}

/*
 * Invoked when the show more tab table animation is finished. User interaction is restored
 */
- (void)showMoreTabTableAnimationFinished {
    
    tableContainerView_.userInteractionEnabled = YES;
    currentViewController_.view.userInteractionEnabled = YES;
    
}

#pragma mark -
#pragma mark Tabs selection

/*
 * Selects the given tab bar into the more table view and the child view
 */
- (void)selectTabIntoTableView:(BBVATabBarTabInformation *)tabBar {
    
    if (([self isViewLoaded]) && (tabBar != nil)) {
        
        NSArray *tabList = self.tabsList;

        if ([tabList containsObject:tabBar]) {
            
            NSUInteger selectedIndex = [tabList indexOfObject:tabBar];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedIndex
                                                        inSection:0];

            UITableView *tableView = self.moreTabsTableView;
            
            [tableView selectRowAtIndexPath:indexPath
                                   animated:NO
                             scrollPosition:UITableViewScrollPositionTop];
            
            [self tableView:tableView didSelectRowAtIndexPath:indexPath];

        }
        
    }
    
}

#pragma mark -
#pragma mark MoreTabViewController selectors

/**
 * Displays the provided view controller as the initial view controller. Displays the new view controller inside the own view
 *
 * @param viewController The view controller to display
 * @param tabBar The tab bar the view controller is located in
 */
- (void)displayViewControllerAsBase:(UIViewController *)viewController
                         fromTabBar:(BBVATabBarTabInformation *)tabBar {
    
    if ((viewController != nil) && (viewController != currentViewController_)) {
        
        [self showMoreTabTable];
        
        UIView *childView = currentViewController_.view;
        
        if (selectedTabBar_ != tabBar) {
            
            [tabBar retain];
            [selectedTabBar_ release];
            selectedTabBar_ = tabBar;
            
        }
        
        if ((viewVisible_) && (!newOSVersion_)) {
            
            [currentViewController_ viewWillDisappear:NO];
            
        }
        
        [childView removeFromSuperview];
        
        if ((viewVisible_) && (!newOSVersion_)) {
            
            [currentViewController_ viewDidDisappear:NO];
            
        }
        
        if ([currentViewController_ conformsToProtocol:@protocol(MoreTab_iPadClient)]) {
            
            id<MoreTab_iPadClient> moreTabClient = (id<MoreTab_iPadClient>)currentViewController_;
            [moreTabClient setManager:nil];
            
        }
        
        [viewController retain];
        [currentViewController_ release];
        currentViewController_ = viewController;
        
        UIView *view = self.view;
        childView = currentViewController_.view;
        
        if (childView != nil) {
            
            childView.autoresizingMask = UIViewAutoresizingNone;
            
            childView.autoresizesSubviews = YES;
            
            CGRect childFrame = [self calculateChildFrame];
            childView.frame = childFrame;
            
            if ([currentViewController_ conformsToProtocol:@protocol(MoreTab_iPadClient)]) {
                
                id<MoreTab_iPadClient> moreTabClient = (id<MoreTab_iPadClient>)currentViewController_;
                [moreTabClient setManager:self];
                
            }
            
            if ((viewVisible_) && (!newOSVersion_)) {
                
                [currentViewController_ viewWillAppear:NO];
                
            }
            
            [view addSubview:childView];
            
            if ((viewVisible_) && (!newOSVersion_)) {
                
                [currentViewController_ viewDidAppear:NO];
                
            }
            
            [self recalculateLayout];
            
        }
        
        [view bringSubviewToFront:tableContainerView_];
        
        UINavigationItem *currentViewControllerNavigationItem = currentViewController_.navigationItem;
        UINavigationItem *selfNavigationItem = self.navigationItem;
        selfNavigationItem.rightBarButtonItem = currentViewControllerNavigationItem.rightBarButtonItem;
        selfNavigationItem.leftBarButtonItem = currentViewControllerNavigationItem.leftBarButtonItem;
        
    }
    
}

#pragma mark -
#pragma mark BBVATabBarViewControllerViewDelegate selectors

/**
 * Notifies the delegate that the view frame has changed. View layout is recalculated
 *
 * @param bbvaTabBarViewControllerView The BBVATabBarViewControllerView instance triggering the event
 */
- (void)tabBarViewControllerViewFrameChanged:(BBVATabBarViewControllerView *)bbvaTabBarViewControllerView {
    
    if ([self isViewLoaded]) {
        
        if (bbvaTabBarViewControllerView == self.view) {
            
            [self recalculateLayout];
            
        }

    }
    
}

#pragma mark -
#pragma mark MoreTab_iPadManager protocol selectors

/**
 * The client asks the manager to navigate to a new view controller. The view controller is added to the more tab navigator controller
 *
 * @param client The MoreTab_iPadClient instance requesting the operation. When client is not the same as the current view controller, no operation is performed
 * @param viewController The view controller to navigate to
 * @return YES when the operation is performed, NO otherwise
 */
- (BOOL)moreTabClient:(id<MoreTab_iPadClient>)client navigateToViewController:(UIViewController *)viewController {
    
    BOOL result = NO;
    
    if (viewController != nil) {
        
        if ((NSObject *)client == currentViewController_) {
            
            [self.navigationController pushViewController:viewController
                                                 animated:YES];
            
        }
        
    }
    
    return result;
    
}

/**
 * The client asks the manager to collapse the more table. Collapses the more tab table when it is not already collapsed
 *
 * @param client The MoreTab_iPadClient instance requesting the operation. When client is not the same as the current view controller, no operation is performed
 * @return YES when the operation is performed, or the more table is already collapsed, NO otherwise
 */
- (BOOL)moreTabClientCollapseMoreTable:(id<MoreTab_iPadClient>)client {
    
    BOOL result = NO;
    
    if (!tableCollapsed_) {
        
        if (viewVisible_) {
            
            [self setMoreTabTableVisibility:NO
                                   animated:YES];
            
            result = YES;
            
        }
        
    } else {
        
        result = YES;
        
    }
    
    return result;
    
}

/**
 * The client asks the manager to expand the more table. Expands the more tab table when it is not already expanded
 *
 * @param client The MoreTab_iPadClient instance requesting the operation. When client is not the same as the current view controller, no operation is performed
 * @return YES when the operation is performed or the more table is already expanded, NO otherwise
 */
- (BOOL)moreTabClientExpandMoreTable:(id<MoreTab_iPadClient>)client {
    
    BOOL result = NO;
    
    if (tableCollapsed_) {
        
        if (viewVisible_) {
            
            [self setMoreTabTableVisibility:YES
                                   animated:YES];
            
            result = YES;
            
        }
        
    } else {
        
        result = YES;
        
    }
    
    return result;

}


/**
 * The client asks the manager to switch the more table collapsed state. When the table is collapsed, it is expanded and vice versa
 *
 * @param client The MoreTab_iPadClient instance requesting the operation. When client is not the same as the current view controller, no operation is performed
 * @return YES when the operation is performed or the more table is already expanded, NO otherwise
 */
- (BOOL)moreTabClientSwitchMoreTableCollapsedState:(id<MoreTab_iPadClient>)client {
    
    BOOL result = NO;
    
    if (tableCollapsed_) {
        
        result = [self moreTabClientExpandMoreTable:client];
        
    } else {
        
        result = [self moreTabClientCollapseMoreTable:client];
        
    }
    
    return result;
    
}

/**
 * The client asks the manager whether the more table is collapsed or not. Returns the table collapsed flag
 *
 * @param client The MoreTab_iPadClient instance requesting the operation
 * @return YES when the more table is collapsed, NO when it is expanded
 */
- (BOOL)moreTabClientIsMoreTableCollapsed:(id<MoreTab_iPadClient>)client {
    
    return tableCollapsed_;
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the navigation item used to represent the view controller. The right and left buttons in the current view controller are set inside
 * the navigation var
 *
 * @return The navigation item used to represent the view controller
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    UINavigationItem *currentViewControllerNavigationItem = currentViewController_.navigationItem;
    
    result.leftBarButtonItem = currentViewControllerNavigationItem.leftBarButtonItem;
    result.rightBarButtonItem = currentViewControllerNavigationItem.rightBarButtonItem;
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/**
 * Sets the new more tab table background color, creating the new table background image
 *
 * @param moreTabTableBackgroundColor The new more tab table background color to set
 */
- (void)setMoreTabTableBackgroundColor:(UIColor *)moreTabTableBackgroundColor {
    
    [super setMoreTabTableBackgroundColor:moreTabTableBackgroundColor];
    
    UIColor *tableBackgroundColor = self.moreTabTableBackgroundColor;
    
    [self createBackgroundImageWithBackgroundColor:tableBackgroundColor];
    
}

/*
 * Returns the selected tab bar
 *
 * @return The selected tab bar
 */
- (BBVATabBarTabInformation *)selectedTabBar {
    
    return selectedTabBar_;
    
}

@end
