/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BBVATabBarBackground.h"


//Forward declarations
@class BBVATabBarTabInformation;
@class BBVATabBarView;
@class BBVATabView;


/**
 * BBVATabBarView delegate protocol. Delegate receives notifications and must decide about displaying view controllers or not
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol BBVATabBarViewDelegate

/**
 * Asks the delegate confirmation to select or not a given tab
 *
 * @param bbvaTabBarView The BBVA tab bar view asking for confirmation
 * @param tabInformation The information that defines the tab that is going to be selected
 * @return YES if the tab can be selected, NO otherwise
 */
- (BOOL)tabBarView:(BBVATabBarView *)bbvaTabBarView shouldSelectTab:(BBVATabBarTabInformation *)tabInformation;

/**
 * Notifies a new tab has been selected
 *
 * @param bbvaTabBarView The BBVA tab bar view triggering the event
 * @param tabInformation The tab being selected information
 */
- (void)tabBarView:(BBVATabBarView *)bbvaTabBarView tabSelected:(BBVATabBarTabInformation *)tabInformation;

/**
 * Notifies the logo image was tapped.
 *
 * @param bbvaTabBarView The BBVA tab bar view triggering the event.
 */
- (void)tabBarViewLogoImageTapped:(BBVATabBarView *)bbvaTabBarView;

@end


/**
 * View to display as the custom BBVA tab bar. It draws a top to bottom blue gradient on unselected elements, and
 * a gray background with a left to right blue gradient at the bottom on selected elements
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BBVATabBarView : BBVATabBarBackground {

@private
    
    /**
     * Tab views list. All objects are BBVATabView instances
     */
    NSMutableArray *tabViewsList_;
    
    /**
     * Array containing the tabs to display. All objects are BBVATabBarTabInformation instances
     */
    NSMutableArray *tabsList_;
    
    /**
     * Currently selected tab
     */
    BBVATabBarTabInformation *selectedTab_;
    
    /**
     * Selected tab background color
     */
    UIColor *selectedTabBackgroundColor_;

    /**
     * BBVA tab bar view delegate, usually the BBVATabBarViewController
     */
    id<BBVATabBarViewDelegate> delegate_;
    
}


/**
 * Provides read-write access to the array containing the tabs to display
 */
@property (nonatomic, readwrite, retain) NSArray *tabsList;

/**
 * Provides read-write access to the selected tab background color
 */
@property (nonatomic, readwrite, retain) UIColor *selectedTabBackgroundColor;

/**
 * Provides read-write access to the BBVA tab bar view delegate
 */
@property (nonatomic, readwrite, assign) id<BBVATabBarViewDelegate> delegate;


/**
 * Selects a given view controller, if found among the tab information
 *
 * @param viewController The view controller to select
 * @return The selected tab information
 */
- (BBVATabBarTabInformation *)selectViewController:(UIViewController *)viewController;

/**
 * Returns the tab bar index for a given view controller
 *
 * @return The tab bar index for a given view controller o NSNotFound if the view controller is not managed by the tab bar view
 */
- (NSUInteger)tabBarIndexForViewController:(UIViewController *)viewController;

/**
 * Selects the given tab
 *
 * @param tab The tab to select
 */
- (void)selectTab:(BBVATabBarTabInformation *)tab;

@end
