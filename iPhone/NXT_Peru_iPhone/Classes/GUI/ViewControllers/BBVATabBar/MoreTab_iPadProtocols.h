/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


//Forward declarations
@protocol MoreTab_iPadClient;


/**
 * Protocol to export several of the more tab view controller functionality in iPad version so the view controllers can invoke it
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol MoreTab_iPadManager

@required

/**
 * The client asks the manager to navigate to a new view controller
 *
 * @param client The MoreTab_iPadClient instance requesting the operation. When client is not the same as the current view controller, no operation is performed
 * @param viewController The view controller to navigate to
 * @return YES when the operation is performed, NO otherwise
 */
- (BOOL)moreTabClient:(id<MoreTab_iPadClient>)client navigateToViewController:(UIViewController *)viewController;

/**
 * The client asks the manager to collapse the more table
 *
 * @param client The MoreTab_iPadClient instance requesting the operation. When client is not the same as the current view controller, no operation is performed
 * @return YES when the operation is performed, or the more table is already collapsed, NO otherwise
 */
- (BOOL)moreTabClientCollapseMoreTable:(id<MoreTab_iPadClient>)client;

/**
 * The client asks the manager to expand the more table
 *
 * @param client The MoreTab_iPadClient instance requesting the operation. When client is not the same as the current view controller, no operation is performed
 * @return YES when the operation is performed or the more table is already expanded, NO otherwise
 */
- (BOOL)moreTabClientExpandMoreTable:(id<MoreTab_iPadClient>)client;

/**
 * The client asks the manager to switch the more table collapsed state. When the table is collapsed, it is expanded and vice versa
 *
 * @param client The MoreTab_iPadClient instance requesting the operation. When client is not the same as the current view controller, no operation is performed
 * @return YES when the operation is performed or the more table is already expanded, NO otherwise
 */
- (BOOL)moreTabClientSwitchMoreTableCollapsedState:(id<MoreTab_iPadClient>)client;

/**
 * The client asks the manager whether the more table is collapsed or not
 *
 * @param client The MoreTab_iPadClient instance requesting the operation
 * @return YES when the more table is collapsed, NO when it is expanded
 */
- (BOOL)moreTabClientIsMoreTableCollapsed:(id<MoreTab_iPadClient>)client;

@end


/**
 * Protocol implemented by tabs view controllers that wants to take advantage of the manager services
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol MoreTab_iPadClient

/**
 * Sets the more tab manager in iPad version
 *
 * @param manager The MoreTab_iPadManager that will serve the client
 */
- (void)setManager:(id<MoreTab_iPadManager>)manager;

@end
