/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


//Forward declarations
@class BBVATabBarViewControllerView;


/**
 * BBVATabBarViewControllerView delegate protocol. It notifies the delegate when the view frame changes
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol BBVATabBarViewControllerViewDelegate

/**
 * Notifies the delegate that the view frame has changed
 *
 * @param bbvaTabBarViewControllerView The BBVATabBarViewControllerView instance triggering the event
 */
- (void)tabBarViewControllerViewFrameChanged:(BBVATabBarViewControllerView *)bbvaTabBarViewControllerView;

@end


/**
 * View to manage the tab bar and the selected view inside the BBVATabBarViewController
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BBVATabBarViewControllerView : UIView {
    
@private

    /**
     * BBVATabBarViewControllerView delegate
     */
    id<BBVATabBarViewControllerViewDelegate> bbvaTabBarViewControllerViewDelegate_;
    
}


/**
 * Provides read-write access to the BBVATabBarViewControllerView delegate and exports it for Interface Builder
 */
@property (nonatomic, readwrite, assign) IBOutlet id<BBVATabBarViewControllerViewDelegate> bbvaTabBarViewControllerViewDelegate;

@end
