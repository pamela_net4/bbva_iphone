/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BBVATabBarViewControllerView.h"


#pragma mark -

@implementation BBVATabBarViewControllerView

#pragma mark -
#pragma mark Properties

@synthesize bbvaTabBarViewControllerViewDelegate = bbvaTabBarViewControllerViewDelegate_;

#pragma mark -
#pragma mark UIView selectors

/**
 * Sest the new view bounds. The delegate is notfied
 *
 * @param bounds The new view bounds
 */
- (void)setBounds:(CGRect)bounds {

    [super setBounds:bounds];
    
    [bbvaTabBarViewControllerViewDelegate_ tabBarViewControllerViewFrameChanged:self];
    
}

/**
 * Sets the new view frame. The delegate is notified
 *
 * @param frame The new view frame
 */
- (void)setFrame:(CGRect)frame {
    
    [super setFrame:frame];
    
    [bbvaTabBarViewControllerViewDelegate_ tabBarViewControllerViewFrameChanged:self];
    
}

/**
 * Sets the new view transform. The delegate is notified
 *
 * @param transform The new view transform
 */
- (void)setTransform:(CGAffineTransform)transform {
    
    [super setTransform:transform];
    
    [bbvaTabBarViewControllerViewDelegate_ tabBarViewControllerViewFrameChanged:self];
    
    
}

@end
