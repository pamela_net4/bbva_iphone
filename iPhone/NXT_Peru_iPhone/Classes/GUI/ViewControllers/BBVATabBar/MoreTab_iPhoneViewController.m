/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MoreTab_iPhoneViewController.h"
#import "MoreTabViewController+protected.h"
#import "BBVATabBarTabInformation.h"


/**
 * Defines the NIB file associated to the MoreTab_iPhoneViewController
 */
#define MORE_TAB_IPHONE_VIEW_CONTROLLER_NIB_FILE                                            @"MoreTab_iPhoneViewController"


#pragma mark -

@implementation MoreTab_iPhoneViewController

#pragma mark -
#pragma mark Instance initialization

/*
 * Returns a new autoreleased MoreTab_iPhoneViewController created from its NIB file
 */
+ (MoreTab_iPhoneViewController *)moreTab_iPhoneViewController {
    
    MoreTab_iPhoneViewController *result = [[[MoreTab_iPhoneViewController alloc] initWithNibName:MORE_TAB_IPHONE_VIEW_CONTROLLER_NIB_FILE bundle:nil] autorelease];
    [result awakeFromNib];
    return result;

}

#pragma mark -
#pragma mark MoreTabViewController selectors

/**
 * Displays the provided view controller as the initial view controller. Pushes the view controller into the navigation controller
 *
 * @param viewController The view controller to display
 * @param tabBar The tab bar the view controller is located in
 */
- (void)displayViewControllerAsBase:(UIViewController *)viewController
                         fromTabBar:(BBVATabBarTabInformation *)tabBar {
    
    if (viewController != nil) {
        
        UINavigationController *navigationController = self.navigationController;
        
        NSArray *viewControllersArray = navigationController.viewControllers;
        
        if ([viewControllersArray lastObject] != viewController) {
            
            [navigationController setViewControllers:[NSArray arrayWithObjects:self,
                                                      viewController,
                                                      nil]
                                            animated:YES];
            
        }
        
    }
    
}

@end
