/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MoreTabViewController.h"


/**
 * View controller associated to the more tab inside the BBVATabBarViewController in an iPhone application. It displays a table with the tabs that couldn't fit
 * inside the tab bar view
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MoreTab_iPhoneViewController : MoreTabViewController

/**
 * Returns a new autoreleased MoreTab_iPhoneViewController created from its NIB file
 *
 * @return The new autoreleased MoreTab_iPhoneViewController
 */
+ (MoreTab_iPhoneViewController *)moreTab_iPhoneViewController;

@end
