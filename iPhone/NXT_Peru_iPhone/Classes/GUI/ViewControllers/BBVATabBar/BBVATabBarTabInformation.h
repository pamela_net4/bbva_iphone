/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * Contains the information that defines a BBVA tab bar tab. It includes the selected and unselected icons
 * and the view controller that contains the view to display
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BBVATabBarTabInformation : NSObject {

@private
    
    /**
     * Tab identifier
     */
    NSString *tabIdentifier_;
    
    /**
     * View controller that contains the view to display when the tab is selected
     */
    UIViewController *viewController_;
    
    /**
     * Action target, used when the tab view controller is nil
     */
    id actionTarget_;
    
    /**
     * Action selector invoked in target when the user tabs on the view controller and no ta view controller is defined
     */
    SEL actionSelector_;
    
    /**
     * Internal view controller. This view controller is the one inside tabViewController_ when it is a UINavigationController and the
     * more tab view controller was requested
     */
    UIViewController *internalViewController_;
    
    /**
     * Selected icon image
     */
    UIImage *selectedIconImage_;
    
    /**
     * Selected state color
     */
    UIColor *selectedColor_;
    
    /**
     * Unselected icon image
     */
    UIImage *unselectedIconImage_;
    
    /**
     * Unselected state color
     */
    UIColor *unselectedColor_;
    
    /**
     * More icon image
     */
    UIImage *moreIconImage_;
    
    /**
     * More state color
     */
    UIColor *moreColor_;
    
    /**
     * Disabled icon image
     */
    UIImage *disabledIconImage_;
    
    /**
     * Disabled state color
     */
    UIColor *disabledColor_;
 
    /**
     * Tab element text
     */
    NSString *tabText_;
    
    /**
     * More cell main text
     */
    NSString *moreCellMainText_;
    
    /**
     * More cell secondary text
     */
    NSString *moreCellSecondaryText_;
    
    /**
     * Can show on more tab flag. YES when the tab can be displayed inside the more tab, NO otherwise
     */
    BOOL canShowOnMoreTab_;
    
    /**
     * Can show on tab bar flag. YES when the tab can be displayed inside the tab bar, NO otherwise
     */
    BOOL canShowOnTabBar_;
    
}


/**
 * Provides read-only access to the tab identifier
 */
@property (nonatomic, readonly, copy) NSString *tabIdentifier;

/**
 * Provides read-only access to the view controller that contains the view to display when the tab is selected
 */
@property (nonatomic, readonly, retain) UIViewController *tabViewController;

/**
 * Provides read-only access to the more tab view controller
 */
@property (nonatomic, readonly, retain) UIViewController *moreTabViewController;

/**
 * Provides read-only access to the view controller stored
 */
@property (nonatomic, readonly, retain) UIViewController *viewController;

/**
 * Provides read-only access to the selected icon image
 */
@property (nonatomic, readonly, retain) UIImage *selectedIconImage;

/**
 * Provides read-only access to the selected state color
 */
@property (nonatomic, readonly, retain) UIColor *selectedColor;

/**
 * Provides read-only access to the unselected icon image
 */
@property (nonatomic, readonly, retain) UIImage *unselectedIconImage;

/**
 * Provides read-only access to the unselected state color
 */
@property (nonatomic, readonly, retain) UIColor *unselectedColor;

/**
 * Provides read-only access to the more icon image
 */
@property (nonatomic, readonly, retain) UIImage *moreIconImage;

/**
 * Provides read-only access to the more state color
 */
@property (nonatomic, readonly, retain) UIColor *moreColor;

/**
 * Provides read-only access to the pisabled icon image
 */
@property (nonatomic, readonly, retain) UIImage *disabledIconImage;

/**
 * Provides read-only access to the disabled state color
 */
@property (nonatomic, readonly, retain) UIColor *disabledColor;

/**
 * Provides read-only access to the tab element text
 */
@property (nonatomic, readonly, copy) NSString *tabText;

/**
 * Provides read-only access to the more cell main text
 */
@property (nonatomic, readonly, copy) NSString *moreCellMainText;

/**
 * Provides read-only access to the more cell secondary text
 */
@property (nonatomic, readonly, copy) NSString *moreCellSecondaryText;

/**
 * Provides read-only access to the can show on more tab flag
 */
@property (nonatomic, readonly, assign) BOOL canShowOnMoreTab;

/**
 * Provides read-only access to the can show on tab bar flag
 */
@property (nonatomic, readonly, assign) BOOL canShowOnTabBar;


/**
 * Designated initializer. It initializes a BBVATabBarTabInformation instance with the provided information. All information
 * provided must be non-nil (expect for the icons information, excluding the reference image), or the instance will be released
 * and a nil reference will be returned
 *
 * @param tabIdentifier The tab identifier to store. It is the user job to provide unique identifiers for each tab
 * in the tab bar if it were necesary to distinguish one from another
 * @param viewController The view controller with the view to display when the element is selected
 * @param actionTarget The instance to execute the action selector when the tab view controller is nil. User class is responsible to keep
 * this instance alive while the tab bar information object is alive
 * @param actionSelector The selector to invoke in the action target to execute the associated action
 * @param referenceIconImage The reference icon image to generate the other ones when the are nil. It must not be nil or the tab bar information
 * will not be created.The alpha channel from this image is the only part used to create those images
 * @param selectedIconImage The selected icon image
 * @param selectedImageColor the selected icon image color in case it must be synthesized from the reference icon image
 * @param unselectedIconImage The unselected icon image
 * @param unselectedImageColor The unselected icon image color in case it must be synthesized from the reference icon image
 * @param moreIconImage The more icon image
 * @param moreImageColor the more icon image color in case it must be synthesized from the reference icon image
 * @param disabledIconImage The disabled icon image
 * @param disabledImageColor the disabled icon image color in case it must be synthesized from the reference icon image
 * @param tabText The tab text to store
 * @param moreCellMainText The main text to show in the more cell
 * @param moreCellSecondaryText The secodnary text to show in the more cell
 * @param canShowOnMoreTab YES when the tab can be displayed inside the more tab, NO otherwise
 * @param canShowOnTabBar YES when the tab can be dsplayed inside the tab bar, NO otherwise
 * @return The initialized BBVTabBarTabInformation instance or nil if any of the provided information is nil
 */
- (id)initWithTabIdentifier:(NSString *)tabIdentifier
             viewController:(UIViewController *)viewController
               actionTarget:(id)actionTarget
             actionSelector:(SEL)actionSelector
         referenceImageIcon:(UIImage *)referenceIconImage
          selectedImageIcon:(UIImage *)selectedIconImage
         selectedImageColor:(UIColor *)selectedImageColor
        unselectedImageIcon:(UIImage *)unselectedIconImage
       unselectedImageColor:(UIColor *)unselectedImageColor
              moreImageIcon:(UIImage *)moreIconImage
             moreImageColor:(UIColor *)moreImageColor
          disabledImageIcon:(UIImage *)disabledIconImage
         disabledImageColor:(UIColor *)disabledImageColor
                    tabText:(NSString *)tabText
           moreCellMainText:(NSString *)moreCellMainText
      moreCellSecondaryText:(NSString *)moreCellSecondaryText
           canShowOnMoreTab:(BOOL)canShowOnMoreTab
            canShowOnTabBar:(BOOL)canShowOnTabBar;

/**
 * Sets a list of BBVATabBarTabInformation instances from one array to a mutable array. Instances in the source array which are not
 * a BBVATabBarTabInformation are discarded
 *
 * @param destinationArray The destination mutable array where BBVATabBarTabInformation instances will be stored
 * @param sourceArray The source array containing the BBVATabBarTabInformation instances
 * @return YES when the destination array is updated, NO otherwise
 */
+ (BOOL)fillArray:(NSMutableArray *)destinationArray withBBVATabBarTabInformationInstancesFromOrigin:(NSArray *)sourceArray;

/**
 * Executes the action associated to the tab when no tab view controller is available
 */
- (void)executeAction;

/**
 * Compares a tab bar information object with another using the more tab tex as reference
 *
 * @param tabBarTabInformation The tab bar information object to compare with
 * @return NSOrderedAscending if the more tab text of the receiver precedes the more tab text of tabBarTabInformation in lexical ordering,
 * NSOrderedSame if the more tab text of the receiver and tabBarTabInformation are equivalent in lexical value, and
 * NSOrderedDescending if the more tab text of the receiver follows the more tab text of tabBarTabInformation
 */
- (NSComparisonResult)compareForMoreTabLocation:(BBVATabBarTabInformation *)tabBarTabInformation;

@end
