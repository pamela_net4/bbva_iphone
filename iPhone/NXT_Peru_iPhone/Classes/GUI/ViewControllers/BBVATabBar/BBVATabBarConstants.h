/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#ifndef Compass_BBVATabBarConstants_h
#define Compass_BBVATabBarConstants_h


/**
 * Defines the maximum number of tabs to display in the tab bar in iPhone
 */
#define BBVA_TAB_BAR_MAX_TABS_ON_IPHONE_BAR                             5

/**
 * Defines the maximum number of tabs to display in the tab bar in iPad
 */
#define BBVA_TAB_BAR_MAX_TABS_ON_IPAD_BAR                               7

#endif
