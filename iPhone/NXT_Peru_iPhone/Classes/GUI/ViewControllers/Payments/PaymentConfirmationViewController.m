
/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PaymentConfirmationViewController.h"

#import "Constants.h"
#import "FrequentOperationHeaderView.h"
#import "FrequentOperationStepThreeViewController.h"
#import "GlobalAdditionalInformation.h"
#import "InformationCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKDoubleLabel.h"
#import "MOKEditableViewController.h"
#import "MOKEditableViewController+protected.h"
#import "MOKLimitedLengthTextField.h"
#import "MOKNavigationItem.h"
#import "MOKTextField.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentBaseProcess.h"
#import "PaymentCell.h"
#import "PaymentsConstants.h"
#import "PaymentSuccessViewController.h"
#import "Session.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "TermsAndConsiderationsViewController.h"
#import "Tools.h"
#import "TitleAndAttributes.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "Updater.h"

/**
 * Defines the interior view distance
 */
#define INTERIOR_VIEW_DISTANCE                      5.0f

/**
 * Defines the exterior view distance
 */
#define EXTERIOR_VIEW_DISTANCE                      10.0f

/**
 * Defines the text font size
 */
#define TEXT_FONT_SIZE                              14.0f

/**
 * Defines the small text font size
 */
#define SMALL_TEXT_FONT_SIZE                        12.0f

/**
 * Defines the big text font size
 */
#define BIG_TEXT_FONT_SIZE                          17.0f

/**
 * Defines the Nib file name
 */
#define NIB_FILE_NAME                               @"PaymentConfirmationViewController"

/**
 * PaymentConfirmationViewController private extension
 */
@interface PaymentConfirmationViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releasePaymentConfirmationViewControllerGraphicElements;

/**
 * Relocate views
 */
- (void)relocateViews;

/**
 * Update information
 */
- (void)updateInformation;

@end

#pragma mark -


@implementation PaymentConfirmationViewController


#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;
@synthesize tableBgImageView = tableBgImageView_;
@synthesize infoTable = infoTable_;
@synthesize otpKeyView = otpKeyView_;
@synthesize otpKeyLabel = otpKeyLabel_;
@synthesize otpKeyTextField = otpKeyTextField_;
@synthesize coordKeyView = coordKeyView_;
@synthesize coordKeyTitleLabel = coordKeyTitleLabel_;
@synthesize coordKeyBgImageView = coordKeyBgImageView_;
@synthesize coordKeySubtitleLabel = coordKeySubtitleLabel_;
@synthesize coordKeyabel = coordKeyabel_;
@synthesize coordKeyTextField = coordKeyTextField_;
@synthesize sealView = sealView_;
@synthesize sealTitleLabel = sealTitleLabel_;
@synthesize sealBgImageView = sealBgImageView_;
@synthesize sealSubtitleLabel = sealSubtitleLabel_;
@synthesize sealImageView = sealImageView_;
@synthesize continueButton = continueButton_;
@synthesize brandingLine = brandingLine_;
@synthesize process = process_;
@synthesize extraInfoView = extraInfoView_;
@synthesize topSeparator = topSeparator_;
@synthesize bottomSeparator = bottomSeparator_;
@synthesize extraInfoLabel = extraInfoLabel_;
@synthesize legalTermsView = legalTermsView_;
@synthesize legalTermsTitleLabel = legalTermsTitleLabel_;
@synthesize legalTermsBgImageView = legalTermsBgImageView_;
@synthesize legalTermsDetailLabel = legalTermsDetailLabel_;
@synthesize warningImageView = warningImageView_;
@synthesize legalTermsSwitch = legalTermsSwitch_;
@synthesize legalTermsButton = legalTermsButton_;


#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releasePaymentConfirmationViewControllerGraphicElements];
    
    [process_ release];
    process_ = nil;
    
    [termsAndConsiderationsViewController_ release];
    termsAndConsiderationsViewController_ = nil;
    
    [paymentSuccessViewController_ release];
    paymentSuccessViewController_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasePaymentConfirmationViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releasePaymentConfirmationViewControllerGraphicElements {
    
	[headerView_ release];
    headerView_ = nil;

	[titleLabel_ release];
    titleLabel_ = nil;
    
    [tableBgImageView_ release];
    tableBgImageView_ = nil;
    
    [infoTable_ release];
    infoTable_ = nil;
    
    [otpKeyView_ release];
    otpKeyView_ = nil;
    
    [otpKeyLabel_ release];
    otpKeyLabel_ = nil;
    
    [otpKeyTextField_ release];
    otpKeyTextField_ = nil;
    
    [coordKeyView_ release];
    coordKeyView_ = nil;
    
    [coordKeyTitleLabel_ release];
    coordKeyTitleLabel_ = nil;
    
    [coordKeyBgImageView_ release];
    coordKeyBgImageView_ = nil;
    
    [coordKeySubtitleLabel_ release];
    coordKeySubtitleLabel_ = nil;
    
    [coordKeyabel_ release];
    coordKeyabel_ = nil;

    [coordKeyTextField_ release];
    coordKeyTextField_ = nil;
    
    [sealView_ release];
    sealView_ = nil;

    [sealTitleLabel_ release];
    sealTitleLabel_ = nil;
    
    [sealBgImageView_ release];
    sealBgImageView_ = nil;
    
    [sealSubtitleLabel_ release];
    sealSubtitleLabel_ = nil;
    
    [sealImageView_ release];
    sealImageView_ = nil;
    
    [continueButton_ release];
    continueButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
    [extraInfoView_ release];
    extraInfoView_ = nil;
    
    [topSeparator_ release];
    topSeparator_ = nil;
    
    [bottomSeparator_ release];
    bottomSeparator_ = nil;
    
    [extraInfoLabel_ release];
    extraInfoLabel_ = nil;
    
    [legalTermsView_ release];
    legalTermsView_ = nil;

    [legalTermsTitleLabel_ release];
    legalTermsTitleLabel_ = nil;
    
    [legalTermsBgImageView_ release];
    legalTermsBgImageView_ = nil;
    
    [legalTermsDetailLabel_ release];
    legalTermsDetailLabel_ = nil;
    
    [warningImageView_ release];
    warningImageView_ = nil;
    
    [legalTermsSwitch_ release];
    legalTermsSwitch_ = nil;
    
    [legalTermsButton_ release];
    legalTermsButton_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
    [sealView_ setBackgroundColor:[UIColor clearColor]];

    headerView_ = [[SimpleHeaderView simpleHeaderView] retain];
    [self.scrollableView addSubview:headerView_];
    
    informationHeader_ = [[FrequentOperationHeaderView frequentOperationHeaderView] retain];
    [self.scrollableView addSubview:informationHeader_];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:continueButton_];
    [continueButton_ setTitle:NSLocalizedString(CONFIRM_TEXT_KEY, nil)
                     forState:UIControlStateNormal];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    [tableBgImageView_ setImage:[[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME]];
    [sealBgImageView_ setImage:[[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME]];
    [coordKeyBgImageView_ setImage:[[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME]];
    [legalTermsBgImageView_ setImage:[[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME]];

    [warningImageView_ setImage:[[ImagesCache getInstance] imageNamed:BLUE_ICON_ALERT_IMAGE_FILE_NAME]];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:otpKeyLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:coordKeyTitleLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:sealTitleLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:legalTermsTitleLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:coordKeySubtitleLabel_ withFontSize:SMALL_TEXT_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:sealSubtitleLabel_ withFontSize:SMALL_TEXT_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:legalTermsDetailLabel_ withFontSize:SMALL_TEXT_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];

    [NXT_Peru_iPhoneStyler styleLabel:coordKeyabel_ withFontSize:BIG_TEXT_FONT_SIZE color:[UIColor blackColor]];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:coordKeyTextField_ withFontSize:TEXT_FONT_SIZE andColor:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleMokTextField:otpKeyTextField_ withFontSize:TEXT_FONT_SIZE andColor:[UIColor grayColor]];
    [otpKeyTextField_ setTextAlignment:UITextAlignmentLeft];
    
    
    [coordKeySubtitleLabel_ setNumberOfLines:2];
    [legalTermsDetailLabel_ setNumberOfLines:3];
    
    [otpKeyLabel_ setText:NSLocalizedString(PUBLIC_SERVICE_OTPKEY_TEXT_KEY, nil)];
    [otpKeyTextField_ setPlaceholder:@""];
    
    [coordKeyTitleLabel_ setText:NSLocalizedString(COORDINATE_REQUEST_HEADER_TEXT_KEY, nil)];
    [coordKeySubtitleLabel_ setText:NSLocalizedString(COORDINATE_REQUEST_INFO_TEXT_KEY, nil)];
    
    [sealTitleLabel_ setText:NSLocalizedString(TRANSFER_STEP_TWO_SEAL_TEXT_KEY, nil)];
    [sealSubtitleLabel_ setText:NSLocalizedString(SHARE_SEAL_TEXT_KEY, nil)];
    
    [legalTermsTitleLabel_ setText:NSLocalizedString(PUBLIC_SERVICE_LEGAL_TERMS_TEXT_KEY, nil)];
    [legalTermsDetailLabel_ setText:NSLocalizedString(LEGAL_TERMS_TEXT_KEY, nil)];

    [otpKeyView_ setBackgroundColor:[UIColor clearColor]];
    [coordKeyView_ setBackgroundColor:[UIColor clearColor]];
    [sealView_ setBackgroundColor:[UIColor clearColor]];
    [infoTable_ setBackgroundColor:[UIColor clearColor]];
    [extraInfoView_ setBackgroundColor:[UIColor clearColor]];
    [legalTermsView_ setBackgroundColor:[UIColor clearColor]];
    
    [extraInfoLabel_ setNumberOfLines:4];
    [extraInfoLabel_ setText:NSLocalizedString(CARD_PAYMENT_CONFIRMATION_BOTTOM_TEXT_KEY, nil)];
    [NXT_Peru_iPhoneStyler styleLabel:extraInfoLabel_ withFontSize:11.0f color:[UIColor grayColor]];
    [extraInfoLabel_ setTextAlignment:UITextAlignmentCenter];
        
    [otpKeyTextField_ setMaxLength:OTP_MAXIMUM_LENGHT];
    [otpKeyTextField_ setKeyboardType:UIKeyboardTypeNumberPad];
    [coordKeyTextField_ setMaxLength:COORDINATES_MAXIMUM_LENGHT];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1){
        
        [legalTermsSwitch_ setOnTintColor:[UIColor BBVABlueSpectrumColor]];
        [legalTermsSwitch_ setTintColor: [UIColor BBVABlueSpectrumColor]];
    }    
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releasePaymentConfirmationViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    [self resetScrollToTopLeftAnimated:NO];
    
    [legalTermsSwitch_ setOn:NO];
    
    [self updateInformation];
    [self relocateViews];
    [self lookForEditViews];
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    [self relocateViews];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]);
    [self setScrollFrame:frame];
    
}

/**
 * Creates and returns an autoreleased PaymentConfirmationViewController constructed from a NIB file.
 *
 * @return The autoreleased PaymentConfirmationViewController constructed from a NIB file.
 */
+ (PaymentConfirmationViewController *)paymentConfirmationViewController {

    PaymentConfirmationViewController *result =  [[[PaymentConfirmationViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;

}

#pragma mark -
#pragma mark View format

/**
 * Update information
 */
- (void)updateInformation {

    NSString *title = @"";
    NSString *headerTitle = @"";
    
    switch ([process_ paymentOperationType]) {
            
        case PTEPaymentPSElectricServices:
            
            title = [NSString stringWithFormat:
                     NSLocalizedString(PAYMENT_CONFIRMATION_OF_TEXT_KEY, nil), 
                     NSLocalizedString(PUBLIC_SERVICE_ELECTRICITY_TEXT_LOWER_KEY, nil)];
            
            headerTitle = NSLocalizedString(PUBLIC_SERVICE_ELECTRICITY_TEXT_KEY, nil);
            
            break;
            
        case PTEPaymentPSWaterServices:
            
            title = [NSString stringWithFormat:
                     NSLocalizedString(PAYMENT_CONFIRMATION_OF_TEXT_KEY, nil),  
                     NSLocalizedString(PUBLIC_SERVICE_WATER_TEXT_KEY, nil)];
            
            headerTitle = NSLocalizedString(PUBLIC_SERVICE_WATER_HEADER_TEXT_KEY, nil);
            
            break;
            
        case PTEPaymentPSCellular:
            
            title = [NSString stringWithFormat:
                     NSLocalizedString(PAYMENT_CONFIRMATION_OF_TEXT_KEY, nil), 
                     NSLocalizedString(PUBLIC_SERVICE_CELLULAR_TEXT_LOWER_KEY, nil)];
            
            headerTitle = [NSString stringWithFormat:NSLocalizedString(PUBLIC_SERVICE_SERVICES_OF_TEXT_KEY, nil), NSLocalizedString(PUBLIC_SERVICE_CELLULAR_TEXT_LOWER_KEY, nil)];
            
            break;
            
        case PTEPaymentPSPhone:
            
            title = [NSString stringWithFormat:
                     NSLocalizedString(PAYMENT_CONFIRMATION_OF_TEXT_KEY, nil), 
                     NSLocalizedString(PUBLIC_SERVICE_NOT_CAP_LANDLINE_TEXT_KEY, nil)];
            
            headerTitle = [NSString stringWithFormat:NSLocalizedString(PUBLIC_SERVICE_SERVICES_OF_TEXT_KEY, nil), NSLocalizedString(PUBLIC_SERVICE_NOT_CAP_LANDLINE_TEXT_KEY, nil)];
            
            break;
            
        case PTEPaymentRecharge:
            
            title = [NSString stringWithFormat:
                     NSLocalizedString(PUBLIC_SERVICE_STEP_THREE_CONFIRM_OPERATION_TEXT_KEY, nil), 
                     [NSLocalizedString(PAYMENT_RECHARGE_RECHARGE_TITLE_KEY, nil) lowercaseString]];
            
            headerTitle = NSLocalizedString(PAYMENT_RECHARGE_TITLE_TEXT_KEY, nil);
            
            break;
            
        case PTEPaymentContOwnCard:
            
            title = [NSString stringWithFormat:
                     NSLocalizedString(PAYMENT_CONFIRMATION_OF_TEXT_KEY, nil), 
                     NSLocalizedString(CARD_PAYMENT_OWN_CARD_TEXT_LOWER_KEY, nil)];
            
            headerTitle = NSLocalizedString(CARD_PAYMENT_OWN_CARD_TEXT_KEY, nil);
            
            break;
            
        case PTEPaymentContThirdCard:
            
            title = [NSString stringWithFormat:
                     NSLocalizedString(PAYMENT_CONFIRMATION_OF_TEXT_KEY, nil), 
                     NSLocalizedString(CARD_PAYMENT_THIRD_CARD_TEXT_LOWER_KEY, nil)];
            
            headerTitle = NSLocalizedString(CARD_PAYMENT_THIRD_CARD_TEXT_KEY, nil);
            
            break;
            
        case PTEPaymentOtherBank:
            
            title = [NSString stringWithFormat:
                     NSLocalizedString(PAYMENT_CONFIRMATION_OF_TEXT_KEY, nil), 
                     NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TITLE_TEXT_LOWER_KEY, nil)];
            
            headerTitle = NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TITLE_TEXT_KEY, nil);
            
            break;
            
        case PTEPaymentISService :
            
            title = @"Confirmar compras por internet";
            
            headerTitle = NSLocalizedString(INTERNET_SHOPPING_STEP_TWO_TITLE_TEXT_KEY, nil);
            
            break;
            
        default:
            break;
    }
    
    if ([process_ isFO]) {
        title = NSLocalizedString(FO_CONFIRM_HEADER_TITLE_KEY, nil);
        headerTitle = NSLocalizedString(FO_CONFIRM_HEADER_TITLE_KEY, nil);
        
        [informationHeader_ setTitlesAndAttributeArray:[process_ confirmationInfoArray]];
        [informationHeader_ updateInformationView];
    }
    
    [titleLabel_ setText:title];
    [headerView_ setTitle:headerTitle];

    [coordKeyTextField_ setText:@""];
    [otpKeyTextField_ setText:@""];
    
    [coordKeyabel_ setText:[process_ coordHint]];
    
    [infoTable_ reloadData];
    
	NSData *data = [Tools base64DataFromString:[process_ seal]];
	UIImage *image = [[[UIImage alloc] initWithData:data] autorelease];
	[sealImageView_ setImage:image];
    
}

/**
 * Relocate views
 */
- (void)relocateViews {

    CGFloat yPosition = 0.0f;
    
    CGRect frame = headerView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    headerView_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
    
    if ([process_ isFO]) {
        
        [titleLabel_ setHidden:TRUE];
        [infoTable_ setDelegate:nil];
        [infoTable_ setDataSource:nil];
        [infoTable_ setHidden:TRUE];
        [tableBgImageView_ setHidden:TRUE];
        
        [informationHeader_ setTitlesAndAttributeArray:[process_ confirmationInfoArray]];
        [informationHeader_ updateInformationView];
        
        frame = [informationHeader_ frame];
        frame.origin.y = yPosition;
        frame.origin.x = 10.0f;
        frame.size.width = 300.0f;
        [informationHeader_ setFrame:frame];
        
        yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
        
    }else{
        
        [informationHeader_ setHidden:TRUE];
        
        [titleLabel_ setHidden:NO];
        [infoTable_ setDelegate:self];
        [infoTable_ setDataSource:self];
        [infoTable_ setHidden:NO];
        [tableBgImageView_ setHidden:NO];
        
        frame = titleLabel_.frame;
        frame.origin.y = yPosition;
        titleLabel_.frame = frame;
        
        yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
        
        [infoTable_ reloadData];
        
        frame = infoTable_.frame;
        frame.origin.y = yPosition;
        frame.size.height = [infoTable_ contentSize].height;
        infoTable_.frame = frame;
        
        yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
        
        frame = tableBgImageView_.frame;
        frame.origin.y = infoTable_.frame.origin.y + 3.0f;
        frame.size.height = [infoTable_ contentSize].height - 3.0f;
        tableBgImageView_.frame = frame;
    }
    
    if ([process_ paymentOperationType] == PTEPaymentOtherBank) {
        
        [legalTermsView_ setHidden:NO];
        frame = legalTermsView_.frame;
        frame.origin.y = yPosition;
        legalTermsView_.frame = frame;
        
        yPosition = CGRectGetMaxY(frame) + INTERIOR_VIEW_DISTANCE;
        
    } else {
        
        [legalTermsView_ setHidden:YES];
        
    }
    
    if ([process_ paymentOperationType] == PTEPaymentContOwnCard || [process_ isFO]) {
        
        [coordKeyView_ setHidden:YES];
        [otpKeyLabel_ setHidden:YES];
        [otpKeyTextField_ setHidden:YES];
        
    } else{
        
        if ([process_ typeOTPKey]) {
            
            [coordKeyView_ setHidden:YES];
            [otpKeyView_ setHidden:NO];

            frame = [otpKeyView_ frame];
            frame.origin.y = yPosition;
            [otpKeyView_ setFrame:frame];
            
            yPosition = CGRectGetMaxY(frame) + INTERIOR_VIEW_DISTANCE;
                        
        } else {
            
            [otpKeyView_ setHidden:YES];
            [coordKeyView_ setHidden:NO];
            
            frame = coordKeyView_.frame;
            frame.origin.y = yPosition;
            coordKeyView_.frame = frame;
            
            yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
            
        } 
    }
    
            
    if (([process_ seal] == nil) || ([[process_ seal] isEqualToString:@""])) {
        
        [sealView_ setHidden:YES];
        
    } else {
        
        [sealView_ setHidden:NO];
        
        frame = sealView_.frame;
        frame.origin.y = yPosition;
        sealView_.frame = frame;
        
        yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
    
    }
    
    frame = continueButton_.frame;
    frame.origin.y = yPosition;
    continueButton_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
    
    if (([process_ paymentOperationType] == PTEPaymentContOwnCard) || ([process_ paymentOperationType] == PTEPaymentContThirdCard)) {
        
        [extraInfoView_ setHidden:NO];
        frame = extraInfoView_.frame;
        frame.origin.y = yPosition;
        extraInfoView_.frame = frame;
        
        yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
        
    } else {
        
        [extraInfoView_ setHidden:YES];
        
    }
    
    [self setScrollContentSize:CGSizeMake(320.0f, yPosition)];
    
}

#pragma mark -
#pragma mark User interaction

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped {
    
    if ([process_ paymentOperationType] == PTEPaymentOtherBank) {
        
        [process_ setLegalTermsAccepted:[legalTermsSwitch_ isOn]];
        
    }
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    NSString *secondFactorKey = @"";
    
    if (![process_ isFO]) {
        if (otpUsage == otp_UsageOTP) {
            
            secondFactorKey = [otpKeyTextField_ text];
            
        } else if (otpUsage == otp_UsageTC) {
            
            secondFactorKey = [coordKeyTextField_ text];
            
        }
    }
    
    [process_ setSecondFactorKey:secondFactorKey];
    
    [process_ startPaymentSuccessRequest];
    

}

/**
 * The legal terms button has been tapped
 */
- (IBAction)legalTermsButtonTapped {

    if (termsAndConsiderationsViewController_ == nil) {
        
        termsAndConsiderationsViewController_ = [[TermsAndConsiderationsViewController TermsAndConsiderationsViewController] retain];
        
    }
    
    [termsAndConsiderationsViewController_ setURLText:[process_ legalTermsURL] 
                                           headerText:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TITLE_TEXT_KEY, nil)];
    
    [self.navigationController pushViewController:termsAndConsiderationsViewController_ animated:YES];        

}


#pragma mark -
#pragma mark Setters

/*
 * Sets the process
 */
- (void)setProcess:(PaymentBaseProcess *)process {

    if (process_ != process) {
        
        [process retain];
        [process_ release];
        process_ = process;
        
    }

    [process_ setDelegate:self];
    
}

#pragma mark -
#pragma mark UITableView methods

/**
 * Tells the data source to return the number of rows in a given section of a table view. (required)
 *
 * @param tableView: The table-view object requesting this information.
 * @param section: An index number identifying a section in tableView.
 * @return The number of rows in section.
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        
    return [[process_ confirmationInfoArray] count];
    
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. (required)
 *
 * @param tableView: A table-view object requesting the cell.
 * @param indexPath: An index path locating a row in tableView.
 * @return An object inheriting from UITableViewCellthat the table view can use for the specified row.
 */
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
            
    InformationCell *result = (InformationCell *)[tableView dequeueReusableCellWithIdentifier:[InformationCell cellIdentifier]];
    
    if (result == nil) {
            
        result = [[[InformationCell informationCell] retain] autorelease];
            
    }
        
    TitleAndAttributes *titleAndAttribute = [[process_ confirmationInfoArray] objectAtIndex:[indexPath row]];
    [result setInfo:titleAndAttribute];
    
    return result;
                
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 *
 * @param tableView: The table-view object requesting this information.
 * @param indexPath: An index path that locates a row in tableView.
 * @return A floating-point value that specifies the height (in points) that row should be.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    InformationCell * result = [[[InformationCell informationCell] retain] autorelease];
    TitleAndAttributes *titleAndAttribute = [[process_ confirmationInfoArray] objectAtIndex:[indexPath row]];
    [result setInfo:titleAndAttribute];
    return [result dynamicCellHeight];
    
}

#pragma mark -
#pragma mark PaymentBaseProcessDelegate

/**
 * Delegate is notified when the success analysis has finished 
 */
- (void)successAnalysisHasFinished {

    if ([process_ isFO]) {
        
        FrequentOperationStepThreeViewController *foStepThree = [[FrequentOperationStepThreeViewController frequentOperationStepThreeViewController] retain];
        
        [foStepThree setFoOperationHelper:(FOOperationHelper *)process_];
        [[self navigationController] pushViewController:foStepThree
                                               animated:YES];
        
    }else{
        if (paymentSuccessViewController_ == nil) {
            
            paymentSuccessViewController_ = [[PaymentSuccessViewController paymentSuccessViewController] retain];
            
        }
        
        [paymentSuccessViewController_ setIsFoFinnished:FALSE];
        [paymentSuccessViewController_ setProcess:process_];
        [paymentSuccessViewController_ setIsFoFinished:FALSE];
        [[self navigationController] pushViewController:paymentSuccessViewController_
                                               animated:YES];
    }
    
}

/**
 * Delegate is notified when the success analysis has finished with error
 */
- (void)successAnalysisHasFinishedWithError{
    
//    [[self navigationController] popViewControllerAnimated:YES];
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    NSString *title = @"";
    
    switch ([process_ paymentOperationType]) {
        
        case PTEPaymentPSElectricServices:
        case PTEPaymentPSWaterServices:
        case PTEPaymentPSCellular:
        case PTEPaymentPSPhone:
            
            title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY, nil)];
            
            break;
            
        case PTEPaymentRecharge:
            
            title = NSLocalizedString(PAYMENT_RECHARGES_TITLE_TEXT_KEY, nil);
            
            break;
            
        case PTEPaymentContOwnCard:
        case PTEPaymentContThirdCard:
        case PTEPaymentOtherBank:
            
            title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_CARDS_TITLE_LOWER_TEXT_KEY, nil)];
            
            break;
        
        case PTEPaymentISService:
            
            title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(INTERNET_SHOPPING_TITLE_TEXT_KEY, nil)];
        
        default:
            break;
    }
    
    if ([process_ isFO]) {
        title = NSLocalizedString(FO_TITLE_TEXT_REACTIVE_KEY, nil);
    }
    
    result.mainTitle = title;
    
    return result;
    
}

@end
