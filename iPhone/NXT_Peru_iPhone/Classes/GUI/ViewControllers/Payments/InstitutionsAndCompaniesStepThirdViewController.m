//
//  PaymentDetailViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 27/11/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "InstitutionsAndCompaniesStepThirdViewController.h"

#import "accountPay.h"
#import "accountPayList.h"
#import "CardType.h"
#import "CardTypeList.h"
#import "Carrier.h"
#import "CarrierList.h"
#import "CheckComboCell.h"
#import "CheckPendingDocCell.h"
#import "InstitutionsAndCompaniesStepFourViewController.h"
#import "MOKStringListSelectionButton.h"
#import "MOKEditableViewController.h"
#import "MOKEditableViewController+protected.h"
#import "MOKCurrencyTextField.h"
#import "MOKTextView.h"
#import "NXTEditableViewController+protected.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTTextField.h"
#import "NXTTextView.h"
#import "NXTCurrencyTextField.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Ocurrency.h"
#import "OcurrencyList.h"
#import "PaymentInstitutionAndCompaniesConfirmationResponse.h"
#import "PaymentInstitutionAndCompaniesConfirmationInformationResponse.h"
#import "pendingDocumentList.h"
#import "PendingDocument.h"
#import "SimpleHeaderView.h"
#import "UINavigationItem+DoubleLabel.h"
#import "UIColor+BBVA_Colors.h"

#import "SMSCell.h"
#import "EmailCell.h"
#import "StringKeys.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "Updater.h"
#import "Tools.h"

/**
 * Defines the Nib file name
 */
#define NIB_FILE_NAME                                       @"InstitutionsAndCompaniesStepThirdViewController"

/**
 * Defines the interior view distance
 */
#define INTERIOR_VIEW_DISTANCE                      5.0f

/**
 * Defines the exterior view distance
 */
#define EXTERIOR_VIEW_DISTANCE                      10.0f

/**
 * Defines the text font size
 */
#define TEXT_FONT_SIZE_PAY                              13.0f

/**
 * Defines the text font size
 */
#define TEXT_PARTIAL_FONT_SIZE                              10.0f

/**
 * Defines the small text font size
 */
#define SMALL_TEXT_FONT_SIZE                        12.0f

/**
 * Defines the big text font size
 */
#define BIG_TEXT_FONT_SIZE                          17.0f

/**
 * Defines de max text lenght for the mail
 */
#define MAX_TEXT_MAIL_LENGHT                    80


@interface InstitutionsAndCompaniesStepThirdViewController ()


/*
 * Save contact information into the helper
 */
- (void)saveContactInfoIntoHelper;

/**
 * Display the contact selection
 */
- (void)displayContactSelection;

/**
 * Fixes the add contact button images
 */
- (void)fixAddContactButtonImages;

/**
 * Return an array with carriers
 */
-(NSArray*) carrierListString;

/**
 * Re-Accomodate the views
 */
- (void)layoutViews;

@end



@implementation InstitutionsAndCompaniesStepThirdViewController


@synthesize paymentDetailEntity;
@synthesize accountsCell = accountsCell_;
@synthesize cardsCell = cardsCell_;
@synthesize destinationEmail1 = destinationEmail1_;
@synthesize destinationEmail2 = destinationEmail2_;
@synthesize emailMessage = emailMessage_;
@synthesize destinationSMS1 = destinationSMS1_;
@synthesize destinationSMS2 = destinationSMS2_;
@synthesize selectedCarrier1Index = selectedCarrier1Index_;
@synthesize selectedCarrier2Index = selectedCarrier2Index_;
@synthesize serviceType = serviceType_;
@synthesize separator = separator_;
@synthesize hasNavigateForward = hasNavigateForward_;



#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    
}

/**
 * Creates and returns an autoreleased RechargeViewController constructed from a NIB file.
 *
 * @return The autoreleased RechargeViewController constructed from a NIB file.
 */
+ (InstitutionsAndCompaniesStepThirdViewController *)institutionsAndCompaniesStepThirdViewController {
    
    InstitutionsAndCompaniesStepThirdViewController *result =  [[[InstitutionsAndCompaniesStepThirdViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[self view] setFrame:[[UIScreen mainScreen] bounds]];
   
    pendingDocumentsArray = [[NSArray alloc] initWithArray:paymentDetailEntity.pendingDocumentList.pendingDocumentList];
    
    brand = [[NSMutableArray alloc] initWithCapacity:[pendingDocumentsArray count]];
    
    for (int i = 0 ; i < [pendingDocumentsArray count]; i++) {
        [brand addObject:@"0"];
    }
    
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
    
    cardSelectedIndex = -1;
    
    separator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    emailMessage_ = @"";
    [self loadControls];
    
    [[self scrollableView] bringSubviewToFront:separator_];
    
    [NXT_Peru_iPhoneStyler styleAccountSelectionButton:cellAccountCombo_];
    [NXT_Peru_iPhoneStyler styleAccountSelectionButton:cellCardCombo_];
    
    [self lookForEditViews];
   
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:TRUE];
    
    [self lookForEditViews];
    [self layoutViews];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:FALSE];
    
    if (isContactListShowed_) {
        isContactListShowed_ = FALSE;
        if (showEmail1_) {
            [emailCell_.firstTextField becomeFirstResponder];
            [emailCell_.firstTextField resignFirstResponder];
        }else if (showSMS1_){
            [smsCell_.firstTextField becomeFirstResponder];
            [smsCell_.firstTextField resignFirstResponder];
        }
    }
    
}

-(void) loadControls
{
    
    for (UIView *label in [[self scrollableView] subviews]) {
        
        if ([label tag] == 777) {
            
            if (label != nil) {
                [label release];
                label = nil;
            }
            
            [label removeFromSuperview];
        }
        
    }
    
    UIView *view = self.view;
    CGRect frame = view.frame;
    CGFloat width = CGRectGetWidth(frame);
    cardSelectedIndex= -1;
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    
    OcurrencyList *ocurrencyList = [paymentDetailEntity ocurrencyList];
    pendingDocumentList =[paymentDetailEntity pendingDocumentList];
    
  
    isDB =!([[pendingDocumentList pendingDocumentList] count] ==0);
    
    isPartial = [paymentDetailEntity partialVal];
    
    NSArray *ocurrencyArray = [ocurrencyList ocurrencyList];
    
    int amountOcurrency = [ocurrencyArray count];
    
    
    SimpleHeaderView *simpleHeaderView = [SimpleHeaderView simpleHeaderView];
    
    if ([@"SERV" isEqualToString:paymentDetailEntity.payTypeCode] || [[@"Pago de servicios" lowercaseString] isEqualToString:[paymentDetailEntity.payType lowercaseString]]){
        [simpleHeaderView setTitle:serviceType_];
    }else{
        [simpleHeaderView setTitle:NSLocalizedString(PAYMENT_INSTITUTIONS_AND_COMPANIES_TITLE_TEXT_KEY, nil)];
    }
    
    [[self scrollableView] addSubview:simpleHeaderView];
    
    int heightReferential = CGRectGetHeight([simpleHeaderView frame]);
    
    //Empresa
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake( 20, heightReferential, 300,20)];
    
    [descriptionLabel setTag:777];
    [descriptionLabel setBackgroundColor:[UIColor clearColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:descriptionLabel withBoldFontSize:TEXT_FONT_SIZE_PAY color:[UIColor grayColor]];
    
    [descriptionLabel setText:NSLocalizedString(PAYMENT_RECHARGE_BUSINESS_TEXT_KEY, nil)];
    heightReferential += 20;
    
    UILabel *valueLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, heightReferential+5, 300, 20)];
    
    [valueLabel setTag:777];
    [valueLabel setText:paymentDetailEntity.companieName];
    
    [valueLabel setBackgroundColor:[UIColor clearColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:valueLabel withFontSize:TEXT_FONT_SIZE_PAY color:[UIColor BBVAGreyToneTwoColor]];
    
    [[self scrollableView] addSubview:descriptionLabel];
    
    [[self scrollableView] addSubview:valueLabel];
    
    [descriptionLabel release];
    [valueLabel release];
    
    
    heightReferential += 25;
    
    for (int i=0; i<amountOcurrency; i++) {
        
        UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake( 20, heightReferential, 300,20)];
        
        [descriptionLabel setTag:777];
        [descriptionLabel setBackgroundColor:[UIColor clearColor]];
        
        [NXT_Peru_iPhoneStyler styleLabel:descriptionLabel withBoldFontSize:TEXT_FONT_SIZE_PAY color:[UIColor grayColor]];
        
        [descriptionLabel setText:[[ocurrencyArray objectAtIndex:i ] description]];
        heightReferential += 18;
        
        UILabel *valueLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, heightReferential+4, 300, 20)];
        
        [valueLabel setTag:777];
        Ocurrency *ocurrency = [ocurrencyArray objectAtIndex:i ];
        
        if (ocurrency.valueCurrency == nil || [@"" isEqualToString:ocurrency.valueCurrency]) {
            [valueLabel setText:[NSString stringWithFormat:@"%@", ocurrency.value]];
        }else{
            [valueLabel setText:[NSString stringWithFormat:@"%@ %@", ocurrency.valueCurrency, ocurrency.value]];
        }
        
        [valueLabel setBackgroundColor:[UIColor clearColor]];
        
        [NXT_Peru_iPhoneStyler styleLabel:valueLabel withFontSize:TEXT_FONT_SIZE_PAY color:[UIColor BBVAGreyToneTwoColor]];
        
        [[self scrollableView] addSubview:descriptionLabel];
        
        [[self scrollableView] addSubview:valueLabel];
        
        [descriptionLabel release];
        [valueLabel release];
        
        
        heightReferential += 24;

        
    }    
    
    if (isDB) {
        
        //Detail section
        UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake( 20, heightReferential, 300,20)];
        
        [descriptionLabel setTag:777];
        [descriptionLabel setBackgroundColor:[UIColor clearColor]];
        
        [NXT_Peru_iPhoneStyler styleLabel:descriptionLabel withBoldFontSize:TEXT_FONT_SIZE_PAY color:[UIColor grayColor]];
        
        [descriptionLabel setText:NSLocalizedString(INSTITUTIONS_COMPANIES_HOLDER_NAME_KEY, nil)];
        heightReferential += 20;
        
        UILabel *valueLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, heightReferential, 300, 20)];
        
        [valueLabel setTag:777];
        [valueLabel setText:paymentDetailEntity.holderName];
        
        [valueLabel setBackgroundColor:[UIColor clearColor]];
        
        [NXT_Peru_iPhoneStyler styleLabel:valueLabel withFontSize:TEXT_FONT_SIZE_PAY color:[UIColor BBVAGreyToneTwoColor]];
        
        [[self scrollableView] addSubview:descriptionLabel];
        
        [[self scrollableView] addSubview:valueLabel];
        
        [descriptionLabel release];
        [valueLabel release];
        
        heightReferential += 25;
        
        if ([paymentDetailEntity scheduleAmpl] != nil && ![@"" isEqualToString:[paymentDetailEntity scheduleAmpl]]) {
            
            UILabel *labelSchedule = [[[UILabel alloc] init] autorelease];
            labelSchedule.frame = CGRectMake(0.0, heightReferential -5, 320.0, 30.0);
            [labelSchedule setTextAlignment:UITextAlignmentCenter];
            labelSchedule.backgroundColor = [UIColor clearColor];
            [NXT_Peru_iPhoneStyler styleLabel:labelSchedule
                                 withFontSize:12.0f
                                        color:[UIColor BBVABlackColor]];
            
            [labelSchedule setTag:777];
            labelSchedule.text = [paymentDetailEntity scheduleAmpl];
            
            [[self scrollableView] addSubview:labelSchedule];
            
            heightReferential = CGRectGetMaxY([labelSchedule frame]);
            
            heightReferential += 5;
        }
        
        [separator_ removeFromSuperview];
        
        CGRect separatorFrame = [separator_ frame];
        separatorFrame.origin.y = heightReferential;
        [separator_ setFrame:separatorFrame];
        [[self scrollableView] addSubview:separator_];
        
        //next section
        SimpleHeaderView *simpleSecondHeaderView = [SimpleHeaderView simpleHeaderView] ;
        [simpleSecondHeaderView setTitle:NSLocalizedString(PAYMENT_SELECT_RECEIPT_TO_CANCEL_TEXT_KEY, nil)];
        
        [simpleSecondHeaderView setFrame:CGRectMake(0, heightReferential, width, CGRectGetHeight([simpleSecondHeaderView frame]) )];
        
        [[self scrollableView] addSubview:simpleSecondHeaderView];
        
        heightReferential+=CGRectGetHeight([simpleSecondHeaderView frame]);
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            selectionTableView_ = [[UITableView alloc]initWithFrame:CGRectMake(0,heightReferential , width, 100) style:UITableViewStylePlain];
        }else{
            selectionTableView_ = [[UITableView alloc]initWithFrame:CGRectMake(0,heightReferential , width, 100) style:UITableViewStyleGrouped];
        }
        
        [selectionTableView_ setTag:1];
        
        [NXT_Peru_iPhoneStyler styleTableView:selectionTableView_];
        
        
        [selectionTableView_ setDataSource:self];
        [selectionTableView_ setDelegate:self];
        [selectionTableView_ setAllowsMultipleSelection:!isPartial];
        
        [selectionTableView_ reloadData];
        
        CGSize sizeTable =  [selectionTableView_ contentSize];
        
        [selectionTableView_ setFrame:CGRectMake([selectionTableView_ frame].origin.x,
                                                [selectionTableView_ frame].origin.y,
                                                [selectionTableView_ frame].size.width,
                                                sizeTable.height)];
        [selectionTableView_ setScrollEnabled: NO];
        
        [[self scrollableView] addSubview:selectionTableView_];
        
        heightReferential += CGRectGetHeight([selectionTableView_ frame]);
        
        if (isPartial) {
            
            SimpleHeaderView *amountTextHeader = [SimpleHeaderView simpleHeaderView] ;
            
            [amountTextHeader setTitle:NSLocalizedString(INSTITUTIONS_COMPANIES_PARTIAL_AMOUNT_KEY, nil)];
            
            [amountTextHeader setFrame:CGRectMake(0, heightReferential, width, CGRectGetHeight([amountTextHeader frame]) )];
            
            CGRect frame = [amountTextHeader frame];
            frame.size.height *= 2;
            [amountTextHeader setFrame:frame];
            [amountTextHeader.titleLabel setNumberOfLines:2];
            [amountTextHeader.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
            [amountTextHeader setBackgroundColor:[UIColor clearColor]];
            
            [NXT_Peru_iPhoneStyler styleLabel:amountTextHeader.titleLabel withFontSize:14.0f color:[UIColor BBVABlueSpectrumColor]];            
            [[self scrollableView] addSubview:amountTextHeader];
            
            heightReferential += CGRectGetHeight([amountTextHeader frame])/2;
            
            currencyText = [[MOKCurrencyTextField alloc] init];
            [currencyText setFrame:CGRectMake(10, heightReferential+10, self.view.frame.size.width - 20, 30)];
            [NXT_Peru_iPhoneStyler styleMokTextField:currencyText withFontSize:14.0f andColor:[UIColor BBVAGreyColor]];
            [currencyText setPlaceholder:NSLocalizedString(INSTITUTIONS_COMPANIES_CONFIRMATION_AMOUNT_KEY, nil)];
            [currencyText setText:@""];
            [currencyText setBorderStyle:UITextBorderStyleRoundedRect];
            currencyText.canContainCents = YES;
            currencyText.maxDecimalNumbers = 2;
            currencyText.currencySymbol = CURRENCY_SOLES_SYMBOL;
            [currencyText setKeyboardType:UIKeyboardTypeNumberPad];
            [currencyText setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
            [[self scrollableView] addSubview:currencyText];
            
            heightReferential += CGRectGetHeight([currencyText frame]) + 20;
            
        }
    }else{
        
        if ([paymentDetailEntity scheduleAmpl] != nil && ![@"" isEqualToString:[paymentDetailEntity scheduleAmpl]]) {
            
            UILabel *labelSchedule = [[[UILabel alloc] init] autorelease];
            labelSchedule.frame = CGRectMake(0.0, heightReferential -5, 320.0, 30.0);
            [labelSchedule setTextAlignment:UITextAlignmentCenter];
            labelSchedule.backgroundColor = [UIColor clearColor];
            [NXT_Peru_iPhoneStyler styleLabel:labelSchedule
                                 withFontSize:12.0f
                                        color:[UIColor BBVABlackColor]];
            
            [labelSchedule setTag:777];
            labelSchedule.text = [paymentDetailEntity scheduleAmpl];
            
            [[self scrollableView] addSubview:labelSchedule];
            
            heightReferential = CGRectGetMaxY([labelSchedule frame]);
            
            heightReferential += 25;
        }
        
        [separator_ removeFromSuperview];
        
        CGRect separatorFrame = [separator_ frame];
        separatorFrame.origin.y = heightReferential;
        [separator_ setFrame:separatorFrame];
        [[self scrollableView] addSubview:separator_];
    }
    
    SimpleHeaderView *simpleThirdHeaderView = [SimpleHeaderView simpleHeaderView] ;
    
    [simpleThirdHeaderView setTitle:NSLocalizedString(PAYMENT_SELECT_PAYMENT_MODE_KEY, nil)];
    
    [simpleThirdHeaderView setFrame:CGRectMake(0, heightReferential+5, width, [SimpleHeaderView height] )];
    
    
    [[self scrollableView] addSubview:simpleThirdHeaderView];
    
    heightReferential = CGRectGetMaxY([simpleThirdHeaderView frame]);
    
    CGFloat accountTableHeight = 150.0f;

    if ([[[paymentDetailEntity cardList] cardTypeList] count] > 0) {
        UITableView *accountTypeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,heightReferential , width, accountTableHeight) style:UITableViewStyleGrouped];
        
        [accountTypeTableView setTag:2];
        
        [NXT_Peru_iPhoneStyler styleTableView:accountTypeTableView];
        
        [accountTypeTableView setDataSource:self];
        [accountTypeTableView setDelegate:self];
        [accountTypeTableView setScrollEnabled:FALSE];

        [accountTypeTableView setScrollEnabled:FALSE];
        
        [[self scrollableView] addSubview:accountTypeTableView];
        
        [scrollView setContentSize:CGSizeMake(width, heightReferential+ CGRectGetHeight([accountTypeTableView frame]))];
        
        heightReferential += [accountTypeTableView frame].size.height;
    } else {
        
        accountTableHeight = 110;
        
        if (accountsCombo == nil) {
            accountsCombo = [[MOKStringListSelectionButton alloc] initWithFrame:CGRectMake(20, heightReferential + 10, 280, 44)];
        }
        [NXT_Peru_iPhoneStyler styleStringListButton:accountsCombo];
        [NXT_Peru_iPhoneStyler styleAccountSelectionButton:accountsCombo];
        [accountsCombo setOptionStringList:[self accountListString]];
        [accountsCombo setStringListSelectionButtonDelegate:self];
        [accountsCombo setSelectedIndex:0];
        [accountsCombo setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECT_ACCOUNT_TEXT_KEY, nil)];
        
        [scrollView setContentSize:CGSizeMake(width, heightReferential+ CGRectGetHeight([accountsCombo frame]))];
        
        [[self scrollableView] addSubview:accountsCombo];
        
        heightReferential = CGRectGetMaxY([accountsCombo frame]) + 10;
        
    }
    
    simpleNotificationHeaderView = [[SimpleHeaderView simpleHeaderView] retain];
    
    [simpleNotificationHeaderView setTitle:NSLocalizedString(PAYMENT_REPORT_TO_BENEFICIARY_TEXT_KEY, nil)];
    
    [simpleNotificationHeaderView setFrame:CGRectMake(0, heightReferential, width, CGRectGetHeight([simpleThirdHeaderView frame]) )];
    
    
    [[self scrollableView] addSubview:simpleNotificationHeaderView];
    heightReferential+= [simpleNotificationHeaderView frame].size.height;
    
    notificationTable = [[UITableView alloc] initWithFrame:CGRectMake(0,heightReferential , width, 200) style:UITableViewStylePlain];
    
    [NXT_Peru_iPhoneStyler styleTableView:notificationTable];
    notificationTable.backgroundColor = [UIColor clearColor];
    
    [notificationTable setTag:3];
    
    [notificationTable setDataSource:self];
    [notificationTable setDelegate:self];
    
    [[self scrollableView] addSubview:notificationTable];
    
    heightReferential+= [notificationTable frame].size.height;
    
    btnConfirmation = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:btnConfirmation];
    [btnConfirmation setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil)
                     forState:UIControlStateNormal];
    
    [btnConfirmation setFrame:CGRectMake(10, heightReferential, 300.0f, 37.0f )];//self.view.frame.size.width - 20, 50)];
    
    [btnConfirmation addTarget:self action:@selector(onTapConfirmButton) forControlEvents:UIControlEventTouchUpInside];
    
    [[self scrollableView] addSubview:btnConfirmation];
    
    [scrollView setContentSize:CGSizeMake(width, heightReferential + CGRectGetHeight([btnConfirmation frame]) + 100)];
    
    [self setScrollContentSize:CGSizeMake(320.0f, heightReferential)];
    [self lookForEditViews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)layoutViews
{
    if ([self isViewLoaded]) {
        
        CGRect frame = simpleNotificationHeaderView.frame;
        CGFloat height = [SimpleHeaderView height];
        frame.size.height = height;
        simpleNotificationHeaderView.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        UITableView *selectionTableView = notificationTable;
        [selectionTableView reloadData];
        CGSize contentSize = selectionTableView.contentSize;
        frame = selectionTableView.frame;
        frame.origin.y = auxPos;
        height = contentSize.height;
        frame.size.height = height;
        selectionTableView.frame = frame;
        selectionTableView.scrollEnabled = NO;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT2;
        
        UIButton *button = btnConfirmation;
        frame = button.frame;
        frame.origin.y = auxPos;
        button.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT2;
    
        [self setScrollContentSize:CGSizeMake(320.0f, auxPos + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT2)];
    }
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]);
    [self setScrollFrame:frame];
    
}

#pragma mark -
#pragma mark UITableDelegate / Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([tableView tag] == 1)
    {
    return [[pendingDocumentList pendingDocumentList] count];
    }
    else if ([tableView tag] == 2 )
    {
        if ([[[paymentDetailEntity cardList] cardTypeList] count] > 0 ) {
            return 2;
        }else{
            return 1;
        }
    }else if ([tableView tag] == 3 )
    {
        return 2;
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == selectionTableView_)
    {
        return 95;
    }
    else if( [tableView tag] == 2)
    {
        return  [CheckComboCell cellHeightCheckOn:(cardSelectedIndex == indexPath.row)];
    }else{
       
        if (indexPath.row == 0) {
            
            CGFloat result = [SMSCell cellHeightForFirstAddOn:showSMS1_
                                                  secondAddOn:showSMS2_];
            
            
            return result;
            
        } else {
            
            CGFloat result = [EmailCell cellHeightForFirstAddOn:showEmail1_
                                          secondAddOn:showEmail2_];
            
            return result;
            
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if([tableView tag] == 1)
    {
        CheckPendingDocCell *tableviewCell  = [tableView dequeueReusableCellWithIdentifier:[CheckPendingDocCell cellIdentifier]];
        [tableviewCell setTag:[indexPath row]];
        
        PendingDocument *pendingDocument = [[pendingDocumentList pendingDocumentList] objectAtIndex:indexPath.row];

        if(tableviewCell == nil)
        {
        
            tableviewCell = [[CheckPendingDocCell checkPendingDocCell] retain];
       
            [tableviewCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
            [tableviewCell accomodateForDocumentType:isPartial];
            
            if (isPartial) {
                
                [tableviewCell.mainLabel setText: [Tools capitalizeFirstWordOnly: [pendingDocument description]]];
                [tableviewCell.minAmount setText:[NSString stringWithFormat:@"%@%@", pendingDocument.minAmountCurrency, pendingDocument.minAmount]];
                [tableviewCell.maxAmount setText:[NSString stringWithFormat:@"%@%@", pendingDocument.maxAmountCurrency, pendingDocument.maxAmount]];
                
            }else{
                
                [tableviewCell.firstTitle setText: [Tools capitalizeFirstWordOnly: [pendingDocument description]]];
                [tableviewCell.mainDetail setText:[NSString stringWithFormat:@"%@%@", pendingDocument.totalAmountCurrency, pendingDocument.totalAmount]];
            }
            
            [tableviewCell.expirationDate setText:[pendingDocument expirationDate]];
            [tableviewCell setBackgroundColor:[UIColor whiteColor]];
            

            
            return tableviewCell;
        }
    }
    else if([tableView tag] == 2)
    {
        CheckComboCell *resultAux = (CheckComboCell *)[tableView dequeueReusableCellWithIdentifier:[CheckComboCell cellIdentifier]];
        
        if (resultAux == nil) {
            resultAux = [CheckComboCell checkComboCell];
            [[resultAux combo] setStringListSelectionButtonDelegate:self];
        }
        //cuentas
        if (indexPath.row == 0) {
            
            [resultAux applyAccountSelectionStyle:YES];
            
            [[resultAux topTextLabel] setText:NSLocalizedString(IAC_ACCOUNT_TITLE_TEXT_KEY, nil)];
            
            NSMutableArray *stringsArray = [[NSMutableArray alloc] initWithArray:[self accountListString]];
            
            [cellAccountCombo_ release];
            cellAccountCombo_ = nil;
            
            cellAccountCombo_ = [[resultAux combo] retain];
            
            if (cellAccountCombo_ == cellCardCombo_) {
                
                [cellCardCombo_ release];
                cellCardCombo_ = nil;
                
            }
            
            accountsCell_ = resultAux;
            [cellAccountCombo_ setOptionStringList:stringsArray];
            [cellAccountCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECT_ACCOUNT_TEXT_KEY, nil)];
            [cellAccountCombo_ setStringListSelectionButtonDelegate:self];
            [cellAccountCombo_ setSelectedIndex:0];
            [resultAux setCheckActive:(cardSelectedIndex == indexPath.row)];
            
        } else {
            
            [resultAux applyAccountSelectionStyle:NO];
            
            [[resultAux topTextLabel] setText:NSLocalizedString(PAYMENT_CARDS_TITLE_TEXT_KEY, nil)];
            
            NSMutableArray *stringsArray = [[NSMutableArray alloc] initWithArray:[self cardListString]];
            
            [cellCardCombo_ release];
            cellCardCombo_ = nil;
            
            cellCardCombo_ = [[resultAux combo] retain];
            
            if (cellCardCombo_ == cellAccountCombo_) {
                
                [cellAccountCombo_ release];
                cellAccountCombo_ = nil;
                
            }
            
            cardsCell_ = resultAux;
            [cellCardCombo_ setOptionStringList:stringsArray];
            [cellCardCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECT_CARD_TEXT_KEY, nil)];
            [cellCardCombo_ setStringListSelectionButtonDelegate:self];
            [cellCardCombo_ setSelectedIndex:0];
            [resultAux setCheckActive:(cardSelectedIndex == indexPath.row)];
            
        }
        
        [self lookForEditViews];
        
        return resultAux;
        
    }else{
        if (indexPath.row == 0) {
            
            SMSMokCell *result = (SMSMokCell *)[tableView dequeueReusableCellWithIdentifier:[SMSMokCell cellIdentifier]];
            
            if (result == nil) {
                
                if (smsCell_ == nil) {
                    
                    smsCell_ = [[SMSMokCell smsMokCell] retain];
                    
                }
                
                result = smsCell_;
                
            }
            
            result.delegate = self;
            result.showSeparator = NO;
            
            [result.titleLabel setText:NSLocalizedString(NOTIFICATIONS_TABLE_SEND_PHONE_TEXT_KEY, nil)];
            result.smsSwitch.on = sendSMS_;
            
           
            result.firstTextField.text = destinationSMS1_;
            result.firstTextField.delegate = self;
            
            result.selectedFirstOperatorIndex = selectedCarrier1Index_;
            [result.firstComboButton setStringListSelectionButtonDelegate:self];
            
            result.secondTextField.text = destinationSMS2_;
            result.secondTextField.delegate = self;
            
            result.selectedSecondOperatorIndex = selectedCarrier2Index_;
            [[result secondComboButton] setStringListSelectionButtonDelegate:self];
            
            [result setOperatorArray:[self carrierListString]
                          firstAddOn:showSMS1_
                         secondAddOn:showSMS2_];
            
            [result.firstComboButton setSelectedIndex:selectedCarrier1Index_];
            
            [result.secondComboButton setSelectedIndex:selectedCarrier2Index_];
            
            [result.firstTextField setDelegate:self];
            [result.secondTextField setDelegate:self];
            [result.firstComboButton setStringListSelectionButtonDelegate:self];
            [result.secondComboButton setStringListSelectionButtonDelegate:self];
            
            result.backgroundColor = [UIColor clearColor];
            
            [self lookForEditViews];
            
            return result;
            
        }else{
            EmailMokCell *result = (EmailMokCell *)[tableView dequeueReusableCellWithIdentifier:[EmailMokCell cellIdentifier]];
            
            if (result == nil) {
                
                if(emailCell_ == nil) {
                    
                    emailCell_ = [[EmailMokCell emailMokCell] retain];
                }
                result = emailCell_;
                
            }
            
            result.delegate = self;
            result.showSeparator = YES;
            
            [result.titleLabel setText:NSLocalizedString(NOTIFICATIONS_TABLE_SEND_EMAIL_TEXT_KEY, nil)];
            result.emailSwitch.on = sendEmail_;
            
            result.firstTextField.text = destinationEmail1_;
            result.secondTextField.text = destinationEmail2_;
            result.emailTextView.text = emailMessage_;
            
            [result.firstTextField setDelegate:self];
            [result.secondTextField setDelegate:self];
            [result.emailTextView setDelegate:self];
            
            [self fixAddContactButtonImages];
            
            [result setConfigurationForFirstAddOn:showEmail1_
                                      secondAddOn:showEmail2_];
            
            result.backgroundColor = [UIColor clearColor];
            
            [self lookForEditViews];
            
            return result;
        }
        
    }
    
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView tag] == 1) {

        CheckPendingDocCell *cell = (CheckPendingDocCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        if (isPartial) {
            
            if (![cell isSelectedDoc]) {
                    [brand replaceObjectAtIndex:indexPath.row withObject:@"0"];
            }else{
                [brand replaceObjectAtIndex:indexPath.row withObject:@"1"];
               
                
                PendingDocument *pendDoc = [pendingDocumentsArray objectAtIndex:indexPath.row];
                
                currencyText.currencySymbol = pendDoc.maxAmountCurrency;
                
            }
        }else{
            [brand replaceObjectAtIndex:indexPath.row withObject:@"1"];
          
        }
        
        if (currencyText != nil) {
            [currencyText resignFirstResponder];
        }
        
    }else if(tableView.tag == 2)
    {
        
        if (cardSelectedIndex == indexPath.row) {
            return;
        }
        if (indexPath.row == 0) {
            selectedAccount = [[[paymentDetailEntity accountList] accountPayList] objectAtIndex:0];

            selectedCard = nil;
      
            
        }else{
            NSArray *cardArray = [[paymentDetailEntity cardList] cardTypeList];
            
            if ([cardArray count] > 0) {
               selectedCard = [cardArray objectAtIndex:0];
            }
           
            selectedAccount = nil;
            
        }
        cardSelectedIndex = indexPath.row;
        [tableView reloadData];
    }
    
    [self lookForEditViews];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView tag] == 1) {
        
        [brand replaceObjectAtIndex:indexPath.row withObject:@"0"];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && [tableView tag] != 3)
        return 5.00f;
    
    return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && [tableView tag] != 3)
        return 5.00f;
    
    return 0.0f;
}

#pragma mark -
#pragma mark utility methods
-(NSArray*) carrierListString
{
    CarrierList *carrierList = [paymentDetailEntity carrierList];
    NSArray *carrierArray = [carrierList carrierList];
    NSMutableArray *carrierStringArray  = [[NSMutableArray alloc]init];
    
    for (Carrier *aCarrier in carrierArray)
    {
        [carrierStringArray addObject: aCarrier.description];
    }
    
    return  carrierStringArray;
}

-(NSMutableArray*) cardListString
{
    CardTypeList *cardTypeList = [paymentDetailEntity cardList];
    NSArray *cardTypeArray = [cardTypeList cardTypeList];
    NSMutableArray *cardStringArray  = [[[NSMutableArray alloc]init] autorelease];
    for(int i=0;i<[cardTypeArray count];i++)
    {
        CardType *card = [cardTypeArray objectAtIndex:i];
        NSString *cardType = card.type;
        [cardStringArray addObject: [NSString stringWithFormat:@"%@ | %@", [Tools obfuscateCardNumber:card.num],cardType]];
    }
    
    return  cardStringArray;
}
-(NSMutableArray *)accountListString
{
    accountPayList *accountPayList = [paymentDetailEntity accountList];
    NSArray *accountPayArray = [accountPayList accountPayList];
    NSMutableArray *accountStringArray  = [[[NSMutableArray alloc]init] autorelease];
    for(int i=0;i<[accountPayArray count];i++)
    {
        [accountStringArray addObject: ((accountPay*)[accountPayArray objectAtIndex:i]).accountIdAndDescription];
    }
    
    return accountStringArray;
}

#pragma mark -
#pragma mark  MOKStringListSelectionButtonDelegate

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton
{
    if (cardSelectedIndex == 0 && stringListSelectionButton == cellAccountCombo_) {
        
        selectedAccount = [[[paymentDetailEntity accountList] accountPayList] objectAtIndex:stringListSelectionButton.selectedIndex];
        selectedCard = nil;
        
    }else if (cardSelectedIndex == 1 && stringListSelectionButton == cellCardCombo_){
        selectedCard = [[[paymentDetailEntity cardList] cardTypeList] objectAtIndex:stringListSelectionButton.selectedIndex];
        selectedAccount = nil;
        
    } else if (stringListSelectionButton == smsCell_.firstComboButton) {
        
        selectedCarrier1Index_ = stringListSelectionButton.selectedIndex;
        
    } else if (stringListSelectionButton == smsCell_.secondComboButton) {
        
        selectedCarrier2Index_ = stringListSelectionButton.selectedIndex;
        
    }else if (stringListSelectionButton == accountsCombo){
        selectedAccount = [[[paymentDetailEntity accountList] accountPayList] objectAtIndex:stringListSelectionButton.selectedIndex];
    }

}

#pragma mark -
#pragma mark user interaction
-(void)selectionButton:(id)sender
{
    UIButton *buttonSelected = ((UIButton*)sender);
    [buttonSelected setSelected: ![buttonSelected isSelected]];
}

- (void)onTapConfirmButton
{
    [self saveContactInfoIntoHelper];
    
    BOOL isValid= TRUE;
        
    NSString *newBrand = @"";
    NSString *amount = @"";
    NSString *otherSeparator = @",";
    
    float total = 0.0f;
    int docIndex = 0;
    BOOL hasOlder = NO;
    if (isDB) {
        
        isValid = FALSE;
        
        for (int i = 0; i < [brand count]; i++) {
            
            if ([@"1" isEqualToString:[brand objectAtIndex:i]])
            {
                isValid = TRUE;
                
                docIndex = i;
                
                if(docIndex ==0)
                {
                    hasOlder =YES;
                }
                if (!isPartial) {
                    PendingDocument *doc = [pendingDocumentsArray objectAtIndex:i];
                    NSString *aux = [doc totalAmount];
                    aux = [aux stringByReplacingOccurrencesOfString:otherSeparator withString:@""];
                    total += [aux floatValue];
                }
            }
            newBrand = [newBrand stringByAppendingString:[brand objectAtIndex:i]];
            
        }
        
        if (!isValid) {
            [Tools showInfoWithMessage:NSLocalizedString(PAYMENT_ERROR_SELECT_RECEIPT_TO_CANCEL_TEXT_KEY, nil)];
            return;   
        }
        
        if(!hasOlder)
        {
            [Tools showInfoWithMessage:NSLocalizedString(PAYMENT_ERROR_RECEIPTS_NOT_OLDER_RECEPIPS_TEXT_KEY, nil)];
            isValid = FALSE;
            return;
        }

        if (currencyText != nil && currencyText.text != nil) {
            amount = currencyText.text;
        }
        
        amount = [Tools formatAmountWithDotDecimalSeparator:amount];
        
        if (isPartial && ([amount floatValue] <= 0 || currencyText == nil || currencyText.text == nil)) {
            
            [Tools showInfoWithMessage:NSLocalizedString(PAYMENT_ERROR_VALID_AMOUNT_KEY, nil)];
            isValid = FALSE;
            return;
        }else if (isPartial){
            
            PendingDocument *doc = [pendingDocumentsArray objectAtIndex:docIndex];
            
            if(docIndex!=0)
            {
                [Tools showInfoWithMessage:NSLocalizedString(PAYMENT_ERROR_RECEIPTS_NOT_OLDER_RECEPIPS_TEXT_KEY, nil)];
                isValid = FALSE;
                return;
            }
            
            total = [amount floatValue];
            
            NSString *minAmount = [[doc minAmount] stringByReplacingOccurrencesOfString:otherSeparator withString:@""];
            NSString *maxAmount = [[doc maxAmount] stringByReplacingOccurrencesOfString:otherSeparator withString:@""];
            
            if ([amount floatValue] < [minAmount floatValue] || [amount floatValue] > [maxAmount floatValue]) {
                [Tools showInfoWithMessage:NSLocalizedString(PAYMENT_ERROR_AMOUNT_MUST_IN_RANGE_KEY, nil)];
                isValid = FALSE;
                return;
            }
            
        }
        
    }
    
    NSString *form = @"CT";
    
    if (selectedAccount) {
        form = @"CT";
    }else if(selectedCard){
        form = @"TC";
    }else{
        [Tools showInfoWithMessage:NSLocalizedString(PAYMENT_ERROR_ACCOUNT_PAYMENT_TEXT_KEY, nil)];
        isValid = FALSE;
        return;
    }
    
    if (!isValid) {
        return;
    }
    
    NSString *accountNumber = @"";
    NSString *cardNumber = @"";
    
    if (selectedAccount != nil) {
        accountNumber = selectedAccount.accountParam;
        NSString *aux = [selectedAccount availableBalanceString];
        aux = [aux stringByReplacingOccurrencesOfString:otherSeparator withString:@""];
        if (total > [aux floatValue]) {
            [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_AMOUNT_EXCEEDED_KEY, nil)];
            return;
        }
        
    } else if (selectedCard != nil) {
        cardNumber = selectedCard.param;
    }
    
    if (!isValid) {
        return;
    }
    
    NSString *carrier1 = @"";
    if (selectedCarrier1Index_ >= 0) {
        
        Carrier *carrier = [[[paymentDetailEntity carrierList] carrierList] objectAtIndex:selectedCarrier1Index_];
        
        carrier1 = carrier.carrierCode;
    }
    
    NSString *carrier2 = @"";
    if (selectedCarrier2Index_ >= 0) {
        
        Carrier *carrier = [[[paymentDetailEntity carrierList] carrierList] objectAtIndex:selectedCarrier2Index_];
        
        carrier2 = carrier.carrierCode;
    }
    
    if (![self validateNotificationData]){
        return;
    }
    
    [self.appDelegate showActivityIndicator:poai_Both];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(confirmationPaymentResponse:) name:kNotificationPaymentInstitutionsConfirmationPayResult object:nil];
    
    [[Updater getInstance] obtainInstitutionsAndCompaniesPayConfirmationWithForm:form
                                                               ownSubjectAccount:accountNumber
                                                                  ownSubjectCard:cardNumber
                                                                       payImport:amount
                                                                    phonenumber1:destinationSMS1_
                                                                    phonenumber2:destinationSMS2_
                                                                          email1:destinationEmail1_
                                                                          email2:destinationEmail2_
                                                                        carrier1:carrier1
                                                                        carrier2:carrier2
                                                                     mailMessage:emailMessage_
                                                                           brand:newBrand
                                                                            type:0];
}

- (BOOL)validateNotificationData
{
    NSString *message = @"";
    
    if (showSMS1_) {
        
        if ([@"" isEqualToString:destinationSMS1_]) {
            message = NSLocalizedString(PAYMNET_ERROR_PHONE_TEXT_KEY, nil);
        }else if(![Tools isValidMobilePhoneNumberString:destinationSMS1_]){
            message = NSLocalizedString(TRANSFER_ERROR_PHONE_NUMBER_TEXT_KEY, nil);
        }else if(selectedCarrier1Index_ < 0){
            message = NSLocalizedString(PAYMENT_ERROR_CARRIER_TEXT_KEY, nil);
        }
    }
    
    if ([message length] == 0 && showSMS2_) {
        if ([@"" isEqualToString:destinationSMS2_]) {
            message = NSLocalizedString(PAYMNET_ERROR_PHONE_TEXT_KEY, nil);
        }else if(![Tools isValidMobilePhoneNumberString:destinationSMS2_]){
            message = NSLocalizedString(TRANSFER_ERROR_PHONE_NUMBER_TEXT_KEY, nil);
        }else if(selectedCarrier2Index_ < 0){
            message = NSLocalizedString(PAYMENT_ERROR_CARRIER_TEXT_KEY, nil);
        }
    }
    
    if (([message length] == 0) && ([self sendEmail])) {
        
        if (showEmail1_) {
            
            if ([[self destinationEmail1] length] == 0)  {
                
                message = NSLocalizedString(PAYMENT_ERROR_EMAIL_TEXT_KEY, nil);
                
            } else {
                
                NSArray *atSeparetedStrings = [[self destinationEmail1] componentsSeparatedByString:@"@"];
                NSUInteger atSeparetedStringsCount = [atSeparetedStrings count];
                
                if (atSeparetedStringsCount == 2) {
                    
                    NSString *firstString = [atSeparetedStrings objectAtIndex:0];
                    
                    if ([firstString length] > 0) {
                        
                        NSString *otherString = [atSeparetedStrings objectAtIndex:1];
                        NSRange dotRange = [otherString rangeOfString:@"."];
                        NSUInteger dotLocation = dotRange.location;
                        
                        if( !(((dotLocation > 0) && (dotLocation != NSNotFound)) && (dotLocation < ([otherString length] - 1))) ) {
                            
                            message = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                            
                        }
                        
                    } else {
                        
                        message = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                        
                    }
                    
                } else {
                    
                    message = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                    
                }
                
            }
            
        }
        
        if (([message length] == 0) && showEmail2_) {
            
            if ([[self destinationEmail2] length] == 0) {
                
                message = NSLocalizedString(PAYMENT_ERROR_EMAIL_TEXT_KEY, nil);
                
            } else {
                
                NSArray *atSeparetedStrings = [self.destinationEmail2 componentsSeparatedByString:@"@"];
                NSUInteger atSeparetedStringsCount = [atSeparetedStrings count];
                
                if (atSeparetedStringsCount == 2) {
                    
                    NSString *firstString = [atSeparetedStrings objectAtIndex:0];
                    
                    if ([firstString length] > 0) {
                        
                        NSString *otherString = [atSeparetedStrings objectAtIndex:1];
                        NSRange dotRange = [otherString rangeOfString:@"."];
                        NSUInteger dotLocation = dotRange.location;
                        
                        if( !(((dotLocation > 0) && (dotLocation != NSNotFound)) && (dotLocation < ([otherString length] - 1))) ) {
                            
                            message = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                            
                        }
                        
                    } else {
                        
                        message = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                        
                    }
                    
                } else {
                    
                    message = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                    
                }
                
            }
            
        }
        
        if (([message length] == 0) && ([[self emailMessage] length] == 0)) {
            
            message = NSLocalizedString(TRANSFER_ERROR_EMAIL_MESSAGE_TEXT_KEY, nil);
            
        }
    }
    
    if ([message length] > 0) {
        [Tools showInfoWithMessage:message];
        
        return FALSE;
    }
    
    return TRUE;
    
}

/*
 * Display the contact selection
 */
- (void)displayContactSelection {
    
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    
    [self setHasNavigateForward:TRUE];
    
    [[picker navigationBar] setTintColor:[[[self navigationController] navigationBar] tintColor]];
    
    [self.appDelegate presentModalViewController:picker animated:YES];
    
    isContactListShowed_ = TRUE;
    
    [picker release];
    
}

/**
 * Fixes the add contact button images
 */
- (void)fixAddContactButtonImages {
    
    if ((destinationSMS1_ == nil) || [destinationSMS1_ isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                           forState:UIControlStateNormal];
        }
        else{
            [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
        }
    } else {
        
        [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                           forState:UIControlStateNormal];
    }
    
    if ((destinationSMS2_ == nil) || [destinationSMS2_ isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                            forState:UIControlStateNormal];
        }else{
            [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
        }
    } else {
        
        [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                            forState:UIControlStateNormal];
    }
    
    if ((destinationEmail1_ == nil) || [destinationEmail1_ isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                             forState:UIControlStateNormal];
        }else{
            [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
        }
    } else {
        
        [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                             forState:UIControlStateNormal];
    }
    
    if ((destinationEmail2_ == nil) || [destinationEmail2_ isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                              forState:UIControlStateNormal];
        }else{
            [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
        }
    } else {
        
        [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                              forState:UIControlStateNormal];
    }
    
    
}

#pragma mark -
#pragma mark Notification
- (void)confirmationPaymentResponse:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationPaymentInstitutionsConfirmationPayResult object:nil];
    
    [self.appDelegate hideActivityIndicator];
    
    responseConfirmation_ = [notification object];
    
    if (responseConfirmation_ !=nil && [responseConfirmation_ isKindOfClass:[PaymentInstitutionAndCompaniesConfirmationInformationResponse class]]
        && !responseConfirmation_.isError) {
        
        NSMutableArray *keys = [NSMutableArray arrayWithObjects:
                                NSLocalizedString(INSTITUTIONS_COMPANIES_CONFIRMATION_AMOUNT_KEY, nil),
                                NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil),
                                NSLocalizedString(CASH_MOBILE_ACCOUNT_CHARGED_TEXT_KEY, nil),
                                NSLocalizedString(INSTITUTIONS_COMPANIES_TITLE_TEXT_KEY, nil), nil];
        
        NSString *account = [NSString stringWithFormat:@"%@ | %@ \n%@",responseConfirmation_.accountType,responseConfirmation_.currency,responseConfirmation_.subject];
     
        NSMutableArray *values = [NSMutableArray arrayWithObjects:
                                  [NSString stringWithFormat:@"%@%@", responseConfirmation_.payAmountCurrency,
                                   responseConfirmation_.payAmount],
                                  responseConfirmation_.operation,
                                  account,
                                  responseConfirmation_.companieName,
                                  nil];
        NSArray *ocurrency = [[responseConfirmation_ ocurrencyList] ocurrencyList];
        
        if (isDB) {
            
            //Total
            
            for (Ocurrency *aOccurrency in ocurrency) {
                [keys addObject:aOccurrency.description];
                [values addObject:aOccurrency.value];
            }
            
            [keys addObject:NSLocalizedString(INSTITUTIONS_COMPANIES_HOLDER_NAME_KEY, nil)];
            [values addObject:responseConfirmation_.holderName];
            
            if (isPartial) {
                
                PendingDocument *doc = [[responseConfirmation_.docList pendingDocumentList] objectAtIndex:0];
                
                [keys addObject: NSLocalizedString(DOCUMENT_TITLE_KEY, nil)];
                [values addObject:doc.description];
                
                [keys addObject:NSLocalizedString(PUBLIC_SERVICE_STEP_TWO_SELECTED_DUE_DATE_TEXT_KEY, nil)];
                [values addObject:doc.expirationDate];
                
                NSString *minAmount= [NSString stringWithFormat:@"%@%@", doc.minAmountCurrency, doc.minAmount];
                
                [keys addObject:NSLocalizedString(INSTITUTIONS_COMPANIES_MIN_AMOUNT_KEY, nil)];
                [values addObject:minAmount];
                
                NSString *maxAmount = [NSString stringWithFormat:@"%@%@", doc.maxAmountCurrency, doc.maxAmount];
                
                [keys addObject:NSLocalizedString(INSTITUTIONS_COMPANIES_MAX_AMOUNT_KEY, nil)];
                [values addObject:maxAmount];
                
            }else{
                [keys addObject: NSLocalizedString(INSTITUTIONS_COMPANIES_DOCUMENTS_NUMBER_KEY, nil)];
                [values addObject:responseConfirmation_.docNumber];
                
                NSArray *pendingDocs = [responseConfirmation_.docList pendingDocumentList];
                
                for (PendingDocument *doc in pendingDocs) {
                   [keys addObject: [Tools notNilString:[NSString stringWithFormat:@"Recibo %@", doc.description]]];
                    
                    NSString *totalAmount = [NSString stringWithFormat:@"%@%@", doc.totalAmountCurrency, doc.totalAmount];
                    
                    [values addObject:totalAmount];
                }
            }
        }else{
            for (Ocurrency *aOccurrency in ocurrency) {
                [keys addObject:aOccurrency.description];
                [values addObject:aOccurrency.value];
            }
        }
        
        
        if (transferConfirmationModalViewController_ == nil) {
            
            transferConfirmationModalViewController_ = [[InstitutionsAndCompaniesStepFourViewController institutionsAndCompaniesStepFourViewController] retain];
        }
        
        CGRect aFrame = self.view.frame;
        aFrame.origin.x = 0.0f;
        aFrame.origin.y = 0.0f;
        transferConfirmationModalViewController_.isWithBD = isDB;
        transferConfirmationModalViewController_.isPartial = isPartial;
        transferConfirmationModalViewController_.view.frame = aFrame;
        transferConfirmationModalViewController_.keyMutableArray = keys;
        transferConfirmationModalViewController_.valueMutableArray = values;
        transferConfirmationModalViewController_.responseInformation = responseConfirmation_;
        transferConfirmationModalViewController_.serviceType = serviceType_;
        
        [self.navigationController pushViewController:transferConfirmationModalViewController_ animated:TRUE];
    }
    
}

#pragma mark -
#pragma mark Information Methods
/*
 * Save contact information into the helper
 */
- (void)saveContactInfoIntoHelper {
    
    [self setSendSMS:smsCell_.smsSwitch.on];
    [self setDestinationSMS1:smsCell_.firstTextField.text];
    [self setDestinationSMS2:smsCell_.secondTextField.text];
    [self setSelectedCarrier1Index:smsCell_.firstComboButton.selectedIndex];
    [self setSelectedCarrier2Index:smsCell_.secondComboButton.selectedIndex];
    
    [self setSendEmail:emailCell_.emailSwitch.on];
    [self setDestinationEmail1:emailCell_.firstTextField.text];
    [self setDestinationEmail2:emailCell_.secondTextField.text];
    [self setEmailMessage:emailCell_.emailTextView.text];
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Resets the internal information.
 */
- (void)resetInformation {
    

    sendSMS_ =NO;
    showSMS1_ = NO;
    showSMS2_ = NO;
    selectedCarrier1Index_ = -1;
    destinationSMS1_ = @"";
    selectedCarrier2Index_ = -1;
    destinationSMS2_ = @"";
    
    sendEmail_ = NO;
    showEmail1_ = NO;
    showEmail2_ = NO;
    destinationEmail1_ = @"";
    destinationEmail2_ = @"";
    emailMessage_ = @"";
    
}

/**
 * Update the currency symbol on amount text field when user changes currency
 *
 * @param currency The currency name to set
 * @private
 */
- (void)updateAmountTextFieldWithCurreny:(NSString *)currency{
    
    if ([[currency lowercaseString] isEqualToString:[NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil) lowercaseString]]){
        
        currencyText.currencySymbol = CURRENCY_SOLES_SYMBOL;
        
    } else {
        
        currencyText.currencySymbol = CURRENCY_DOLARES_SYMBOL;
        
    }
    
}

#pragma mark -
#pragma mark SMSCell Delegate

/**
 * Switch has been tapped.
 */
- (void)switchButtonHasBeenTapped:(BOOL)on {
    
    [self saveContactInfoIntoHelper];
    
    sendSMS_ = on;
    showSMS1_ = on;
    
    if (!on) {
        
        showSMS2_ = on;
        
        addingFirstSMS_ = NO;
        addingSecondSMS_ = NO;
        
        smsCell_.firstComboButton.selectedIndex = -1;
        smsCell_.firstTextField.text = @"";
        destinationSMS1_ = @"";
        
        smsCell_.secondComboButton.selectedIndex = -1;
        smsCell_.secondTextField.text = @"";
        destinationSMS2_ = @"";
        
        selectedCarrier1Index_ = -1;
        selectedCarrier2Index_ = -1;
        
    } else {
        
        showSMS2_ = NO;
        destinationSMS1_ = smsCell_.firstTextField.text;
        destinationSMS2_ = smsCell_.secondTextField.text;
        
        smsCell_.firstComboButton.selectedIndex = -1;
        selectedCarrier1Index_ = -1;
    }
    
    [self fixAddContactButtonImages];
    
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
    
    [notificationTable reloadRowsAtIndexPaths:array
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    [array release];
    
    [self layoutViews];
    
}

/**
 * More button has been tapped
 */
- (void)moreButtonHasBeenTapped {
    
    [self saveContactInfoIntoHelper];
    showSMS2_ = YES;
    
    smsCell_.secondComboButton.selectedIndex = -1;
    selectedCarrier2Index_ = -1;
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
    
    [notificationTable reloadRowsAtIndexPaths:array
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [array release];
    
    [self layoutViews];
    [self lookForEditViews];
    
}

/**
 * Add First Contact Button Tapped
 */
- (void)addFirstContactHasBeenTapped {
    
    if ((smsCell_.firstTextField.text == nil) || [smsCell_.firstTextField.text isEqualToString:@""]) {
        
        addingFirstSMS_ = YES;
        [self displayContactSelection];
        
    } else {
        
        smsCell_.firstTextField.text = @"";
        destinationSMS1_ = @"";
        
    }
    
    [self fixAddContactButtonImages];
    
}

/**
 * Add Second Contact Button Tapped
 */
- (void)addSecondContactHasBeenTapped {
    
    if ((smsCell_.secondTextField.text == nil) || [smsCell_.secondTextField.text isEqualToString:@""]) {
        
        addingSecondSMS_ = YES;
        [self displayContactSelection];
        
    } else {
        
        smsCell_.secondTextField.text = @"";
        destinationSMS2_ = @"";
        
    }
    
    [self fixAddContactButtonImages];
    
}

/*
 *called when the user tapped in first combo button
 */
- (void)firstComboButtonTapped {
    
//    [self editableViewHasBeenClicked:smsCell_.firstComboButton];
    
}

/*
 *called when the user tapped in second combo button
 */
-(void)secondComboButtonTapped {
    
//    [self editableViewHasBeenClicked:smsCell_.secondComboButton];
}

#pragma mark -
#pragma mark EmailCell Delegate

/**
 * Switch has been tapped.
 *
 * @param on The flag
 */
- (void)emailSwitchButtonHasBeenTapped:(BOOL)on {
    
    [self saveContactInfoIntoHelper];
    
    sendEmail_ = on;
    showEmail1_ = on;
    
    if (!on) {
        
        showEmail2_ = on;
        
        addingFirstEmail_ = NO;
        addingSecondEmail_ = NO;
        
        emailCell_.firstTextField.text = @"";
        destinationEmail1_ = @"";
        
        emailCell_.secondTextField.text = @"";
        destinationEmail2_ = @"";
        
        emailCell_.emailTextView.text = @"";
        emailMessage_ = @"";
        
        
    } else {
        
        showEmail2_ = NO;
        destinationEmail1_ = emailCell_.firstTextField.text;
        destinationEmail2_ = emailCell_.secondTextField.text;
        emailMessage_ = [emailCell_.emailTextView.text retain];
        
    }
    
    [self fixAddContactButtonImages];
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
    
    [notificationTable reloadRowsAtIndexPaths:array
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [array release];
    
    [self layoutViews];
    
    
}

/**
 * Add First Contact Button Tapped
 */
- (void)emailAddFirstContactHasBeenTapped {
    
    if ((emailCell_.firstTextField.text == nil) || [emailCell_.firstTextField.text isEqualToString:@""]) {
        
        addingFirstEmail_ = YES;
        [self displayContactSelection];
        
    } else {
        
        emailCell_.firstTextField.text = @"";
        destinationEmail1_ = @"";
        
    }
    
    [self fixAddContactButtonImages];
    
}

/**
 * Add Second Contact Button Tapped
 */
- (void)emailAddSecondContactHasBeenTapped {
    
    if ((emailCell_.secondTextField.text == nil) || [emailCell_.secondTextField.text isEqualToString:@""]) {
        
        addingSecondEmail_ = YES;
        [self displayContactSelection];
        
    } else {
        
        emailCell_.secondTextField.text = @"";
        destinationEmail2_ = @"";
        
    }
    
    [self fixAddContactButtonImages];
    [self lookForEditViews];
    
}

/**
 * More button has been tapped
 */
- (void)emailMoreButtonHasBeenTapped {
    
    [self saveContactInfoIntoHelper];
    showEmail2_ = YES;
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
    
    [notificationTable reloadRowsAtIndexPaths:array
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [array release];
    
    [self layoutViews];
    
}

#pragma mark -
#pragma mark ABPeoplePickerNavigationControllerDelegate methods
/**
 * Called in ios 8 redirection to shouldContinueAfterSelectingPerson used in ios 7 or previous
 */
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    
    [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person property:property identifier:identifier];
}

/**
 * Called after the user has pressed cancel. The delegate is responsible for dismissing the peoplePicker
 */
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    
	[self.appDelegate dismissModalViewControllerAnimated:YES];
    
    [self fixAddContactButtonImages];
    
    [self setHasNavigateForward:FALSE];
    addingFirstSMS_ = NO;
    addingSecondSMS_ = NO;
    addingFirstEmail_ = NO;
    addingSecondEmail_ = NO;
}

/**
 * Called after a person has been selected by the user.
 *
 * Return YES if you want the person to be displayed.
 * Return NO  to do nothing (the delegate is responsible for dismissing the peoplePicker).
 */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    
	[self.appDelegate dismissModalViewControllerAnimated:YES];
    [self setHasNavigateForward:FALSE];
    
    if (addingFirstSMS_ || addingSecondSMS_) {
        
        BOOL isValidMobilePhoneNumber = NO;
        
        ABMultiValueRef phoneProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
        
        if (ABMultiValueGetCount(phoneProperty) > 0) {
            
            ABMultiValueRef phoneValue;
            NSString *phoneNumber;
            
            for (int i = 0; ABMultiValueGetCount(phoneProperty) > i; i++) {
                
                phoneValue = ABMultiValueCopyValueAtIndex(phoneProperty,i);
                
                phoneNumber = (NSString *)phoneValue;
                phoneNumber = [Tools mobileNumberLikeFromString:phoneNumber];
                
                isValidMobilePhoneNumber = [Tools isValidMobilePhoneNumberString:phoneNumber];
                
                if (isValidMobilePhoneNumber) {
                    break;
                }
            }
            
            if (isValidMobilePhoneNumber) {
                
                if (addingFirstSMS_) {
                    
                    destinationSMS1_ = [phoneNumber copyWithZone:self.zone];
                    smsCell_.firstTextField.text = destinationSMS1_;
                    addingFirstSMS_ = NO;
                    
                } else if (addingSecondSMS_) {
                    
                    destinationSMS2_ = [phoneNumber copyWithZone:self.zone  ];
                    smsCell_.secondTextField.text = destinationSMS2_;
                    addingSecondSMS_ = NO;
                    
                }
                
            }
            
            if (phoneValue != nil) {
                
                CFRelease(phoneValue);
                
            }
            
        }
        
        
        if (!isValidMobilePhoneNumber) {
            
            [Tools showInfoWithMessage:NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_NO_EXIST_TEXT_KEY, nil)];
            
        }
        
        addingFirstSMS_ = NO;
        addingSecondSMS_ = NO;
        
        if (phoneProperty != nil) {
            
            CFRelease(phoneProperty);
            
        }
        
    } else if (addingFirstEmail_ || addingSecondEmail_) {
        
        
        ABMultiValueRef emailProperty = ABRecordCopyValue(person, kABPersonEmailProperty);
        NSInteger emailsNumber = ABMultiValueGetCount(emailProperty);
        
        if (emailsNumber > 0) {
            
            //if (emailsNumber == 1) {
                
                ABMultiValueRef emailValue = ABMultiValueCopyValueAtIndex(emailProperty, 0);
                
                if (addingFirstEmail_) {
                    
                    destinationEmail1_= [(NSString *)emailValue copyWithZone:self.zone];
                    emailCell_.firstTextField.text = destinationEmail1_;
                    addingFirstEmail_ = NO;
                    
                } else if (addingSecondEmail_) {
                    
                    destinationEmail2_= [(NSString *)emailValue copyWithZone:self.zone];
                    emailCell_.secondTextField.text = destinationEmail2_;
                    addingSecondEmail_ = NO;
                    
                }
                
                if (emailValue != nil) {
                    
                    CFRelease(emailValue);
                    
                }
                
            //}
            
        } else {
            
            [Tools showInfoWithMessage:NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_NO_EXIST_TEXT_KEY, nil)];
            
            addingFirstEmail_ = NO;
            addingSecondEmail_ = NO;
            
        }
        
        if (emailProperty != nil) {
            
            CFRelease(emailProperty);
            
        }
        
    }
    
    [self fixAddContactButtonImages];
    
  	return NO;
}

/**
 * Called after a value has been selected by the user.
 *
 * Return YES if you want default action to be performed.
 * Return NO to do nothing (the delegate is responsible for dismissing the peoplePicker).
 */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property
                              identifier:(ABMultiValueIdentifier)identifier {
    
    return [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person];
    
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        //TODO:
        
    }
    
	return YES;
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    if( (textField == [smsCell_ firstTextField]) || (textField == [smsCell_ secondTextField]) ) {
        
        if(resultLength == 0) {
            
            result = YES;
        }
        else if(resultLength <= 9) {
            
            NSScanner *scanner = [NSScanner scannerWithString:resultString];
            NSInteger resultInteger = 0;
            
            if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                if (resultInteger >= 0) {
                    
                    result = YES;
                }
            }
        }
    }
    else if ( (textField == [emailCell_ firstTextField]) || (textField == [emailCell_ secondTextField]) ) {
        
        if (resultLength <= 50) {
            
            result = YES;
            
        }
    }else{
        
        BOOL result = NO;
        
        NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                         withString:string];
        NSUInteger resultLength = [resultString length];
        
        if(textField == currencyText) {
            
            if (resultLength == 0) {
                
                result = YES;
                
            } else {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger > 0) {
                        
                        result = YES;
                    }                
                }               
            }
        }
    }
    
    
    if (resultLength == 0) {
        
        if (textField == smsCell_.firstTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
            }else{
                [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                   forState:UIControlStateNormal];
            }
        } else if (textField == smsCell_.secondTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                forState:UIControlStateNormal];
            }else{
                [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                   forState:UIControlStateNormal];
            }
        } else if (textField == emailCell_.firstTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                 forState:UIControlStateNormal];
            }else{
                [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                   forState:UIControlStateNormal];
            }
        } else if (textField == emailCell_.secondTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                  forState:UIControlStateNormal];
            }else{
                [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME] 
                                                   forState:UIControlStateNormal];
            }
            
        }
        
    } else {
        
        if (textField == smsCell_.firstTextField) {
            
            [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
            
        } else if (textField == smsCell_.secondTextField) {
            
            [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                forState:UIControlStateNormal];
            
        } else if (textField == emailCell_.firstTextField) {
            
            [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                 forState:UIControlStateNormal];
            
        } else if (textField == emailCell_.secondTextField) {
            
            [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                  forState:UIControlStateNormal];
            
        }
        
    }
    
    return result;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    //    [self fixAddContactButtonImages];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [currencyText resignFirstResponder];
    return FALSE;
}


#pragma mark -
#pragma mark UITextView Delegate

/**
 * Asks the delegate whether the specified text should be replaced in the text view.
 *
 * @param textView The text view containing the changes.
 * @param range The current selection range.
 * @param text The text to insert.
 */
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    BOOL result = YES;
    
    NSString *resultString = [textView.text stringByReplacingCharactersInRange:range
                                                                    withString:text];
    
    if (textView == [emailCell_ emailTextView]) {
        
        if ([resultString length] < MAX_TEXT_MAIL_LENGHT) {
            
            result = ([Tools isValidText:text forCharacterSet:[NSCharacterSet alphanumericCharacterSet]] || [Tools isValidText:text forCharacterSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);
            
        }
        else {
            result = NO;
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    NSString *title = @"";
    
    if ([@"SERV" isEqualToString:paymentDetailEntity.payTypeCode] || [[@"Pago de servicios" lowercaseString] isEqualToString:[paymentDetailEntity.payType lowercaseString]])
    {
       title=  [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil),
         NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY, nil)];
    }
    else
    {

        title= [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT_LOWER_KEY, nil)];
    }
    
    result.mainTitle = title;
    
    return result;
    
}
#pragma mark -
#pragma mark Memory management

- (void)dealloc
{
    [simpleNotificationHeaderView release];
    simpleNotificationHeaderView = nil;
    
    [notificationTable release];
    notificationTable = nil;
    
    if (accountsCell_) {
        [accountsCell_ release];
        accountsCell_ = nil;
    }
    
    if (cardsCell_) {
        [cardsCell_ release];
        cardsCell_ = nil;
    }
    
    [scrollView release];
    scrollView = nil;
    
    
    if (transferConfirmationModalViewController_) {
        [transferConfirmationModalViewController_ release];
        transferConfirmationModalViewController_ = nil;
    }
    
    if (selectedAccount) {
        [selectedAccount release];
        selectedAccount = nil;
    }
    
    if (selectedCard) {
        [selectedCard release];
        selectedCard = nil;
    }
    
    [separator_ release];
    separator_ = nil;
    
    serviceType_ = nil;
    
    [super dealloc];
}

@end
