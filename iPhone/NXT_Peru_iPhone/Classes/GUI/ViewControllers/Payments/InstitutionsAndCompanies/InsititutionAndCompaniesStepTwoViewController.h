//
//  InsititutionAndCompaniesStepTwoViewController.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 27/11/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "NXTTableCell.h"
#import "NXTEditableViewController.h"
#import "MOKStringListSelectionButton.h"
#import "SimpleHeaderView.h"

@class PaymentInstitutionsAndCompaniesDetailResponse;
@class PaymentInstitutionAndCompaniesConfirmationResponse;
@class InstitutionsAndCompaniesStepThirdViewController;
@class PublicServiceHeaderView;

@interface InsititutionAndCompaniesStepTwoViewController : NXTEditableViewController <MOKStringListSelectionButtonDelegate, UITextFieldDelegate>
{
@private

    /**
     * Header
     */
    PublicServiceHeaderView *header_;
    

    /**
     * Continue button
     */
    UIButton *continueButton_;
    
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    
    /**
     * Label Companie line
     */
    UILabel *labelCompanie_;
    
    /**
     * Label nam
     */
    UILabel *labelNameCompanie_;
    
    /**
     * array with text fields
     */
    NSMutableArray *textFieldArray_;
    
    /**
     * Service name
     */
    NSString *serviceType_;
    
#pragma logic
    
    /*
     * Detail information for the dynamic view and to procced with the payment
     */
    PaymentInstitutionsAndCompaniesDetailResponse *responseInformation_;
    
    /*
     * Detail information for the Service and to procced with the payment
     */
    PaymentInstitutionAndCompaniesConfirmationResponse *paymentInstitutionAndCompaniesConfirmationResponse_;
    
    /**
     *
     */
    InstitutionsAndCompaniesStepThirdViewController *institutionsAndCompaniesPaymentViewController_;
    
    /**
     * Container view to contain the editable components
     */
    UIView *containerView_;
}


/**
 * Provides readwrite access to the response information
 */
@property (retain, readwrite, nonatomic) InstitutionsAndCompaniesStepThirdViewController *institutionsAndCompaniesPaymentViewController;


/**
 * Provides readwrite access to the response information
 */
@property (retain, readwrite, nonatomic) PaymentInstitutionAndCompaniesConfirmationResponse *paymentInstitutionAndCompaniesConfirmationResponse;

/**
 * Provides readwrite access to the response information
 */
@property (retain, readwrite, nonatomic) PaymentInstitutionsAndCompaniesDetailResponse *responseInformation;

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIButton *continueButton;

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIImageView *brandingLine;

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UILabel *labelCompanie;

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UILabel *labelNameCompanie;

/**
 * Provides readwrite access to container view to contain the editable components and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *containerView;

/**
 * Provides readwrite access to the service type name
 */
@property (nonatomic, readwrite, retain) NSString *serviceType;

/**
 * Creates and returns an autoreleased CardContStepOneViewController constructed from a NIB file.
 *
 * @return The autoreleased CardContStepOneViewController constructed from a NIB file.
 */
+ (InsititutionAndCompaniesStepTwoViewController *)insititutionAndCompaniesStepTwoViewController;

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped;


@end
