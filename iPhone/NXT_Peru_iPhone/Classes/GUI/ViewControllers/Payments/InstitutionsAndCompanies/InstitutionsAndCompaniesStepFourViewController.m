//
//  InstitutionsAndCompaniesStepFourViewController.m
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/11/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "InstitutionsAndCompaniesStepFourViewController.h"

#import "Constants.h"
#import "CoordKeyCell.h"
#import "GlobalAdditionalInformation.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "InstitutionAndCompaniesFinalViewController.h"
#import "LegalTermsTopCell.h"
#import "NXTEditableViewController+protected.h"
#import "NXTNavigationItem.h"
#import "NXTTableCell.h"
#import "NXTTextField.h"
#import "NXTTransfersViewController+protected.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Ocurrency.h"
#import "OcurrencyList.h"
#import "OpKeyCell.h"
#import "PendingDocument.h"
#import "PendingDocumentList.h"
#import "SealCell.h"
#import "Session.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "TermsAndConsiderationsViewController.h"
#import "TextCell.h"
#import "Tools.h"
#import "TransferConfirmationAdditionalInformation.h"
#import "TransferDetailCell.h"
#import "TransferOperationHelper.h"
#import "TransferSummaryCell.h"
#import "TransfersStepThreeViewController.h"
#import "PaymentInstitutionAndCompaniesConfirmationInformationResponse.h"
#import "PaymentInstitutionAndCompaniesSuccessConfirmationResponse.h"
#import "StringKeys.h"
#import "Updater.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"InstitutionsAndCompaniesStepFourViewController"

#pragma mark -


@interface InstitutionsAndCompaniesStepFourViewController ()

/**
 * Releases graphic elements
 *
 * @private
 */
- (void)releaseTransfersStepTwoViewControllerGraphicElements;

/**
 * Configures the second factor cell. It shows the OTP key or Coordinate key, depending on the otp service usage
 *
 * @return The cell configured
 * @private
 */
- (UITableViewCell *)configureSecondFactorCell;

/**
 * Invoked by framework when the response to the transfer operation is received. The information is analized
 *
 * @param notification The NSNotification instance containing the notification information
 * @private
 */
- (void)confirmTransferResult:(NSNotification *)notification;


@end

#pragma mark -


@implementation InstitutionsAndCompaniesStepFourViewController
#pragma mark -
#pragma mark Properties

@synthesize acceptButton = acceptButton_;
@synthesize keyMutableArray = keyMutableArray_;
@synthesize valueMutableArray = valueMutableArray_;
@synthesize responseInformation = responseInformation_;
@synthesize succesResponseInformation = succesResponseInformation_;
@synthesize isPartial = isPartial_;
@synthesize isWithBD = isWithBD_;
@synthesize serviceType = serviceType_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [transfersStepThreeViewController_ release];
    transfersStepThreeViewController_ = nil;
    
    serviceType_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransfersStepTwoViewControllerGraphicElements];
        
    }
    
}

/**
 * Releases graphic elements
 */
- (void)releaseTransfersStepTwoViewControllerGraphicElements {
    
    [acceptButton_ release];
    acceptButton_ = nil;
    
    [coordKeyCell_ release];
    coordKeyCell_ = nil;
    
    [otpKeyCell_ release];
    otpKeyCell_ = nil;

    
    [editableViews_ removeAllObjects];
    [editableViews_ release];
    editableViews_ = nil;
    
}

#pragma mark -
#pragma mark View managment

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.informationTableView.backgroundColor = [UIColor clearColor];
    self.informationTableView.scrollsToTop = YES;
    
    [self setScrollableView:[self containerView]];
    
    editableViews_ = [[NSMutableArray alloc] init];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[otpKeyCell_ opKeyTextField] setText:@""];
    [[coordKeyCell_ keyTextField] setText:@""];
    
    [self.informationTableView reloadData];
    
    CGRect frame = [self containerView].frame;
    frame.size.height = self.view.frame.size.height - CGRectGetMaxY([self operationTypeHeader].frame);
    [self containerView].frame = frame;
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    
    
    
    if ([@"SERV" isEqualToString:responseInformation_.payTypeCode] || [[@"Pago de servicios" lowercaseString] isEqualToString:[responseInformation_.payType lowercaseString]]) {
        
        [[self operationTypeHeader] setTitle:serviceType_];
    }else{
        [[self operationTypeHeader] setTitle:NSLocalizedString(PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT_KEY, nil)];
    }
    
    [self layoutViews];
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the
 * transfer last step confirmation notification
 *
 * @param animated If YES, the disappearance of the view is being animated
 */

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingImageView] frame]) - CGRectGetHeight([[self operationTypeHeader] frame]);
    [self setScrollNominalFrame:scrollFrame];
    
}

/*
 * Lays out the views
 */
- (void)layoutViews {
    
    if ([self isViewLoaded]) {
        UITableView *tableView = self.informationTableView;
        
        [tableView reloadData];
        CGFloat tableHeight = tableView.contentSize.height;
        CGRect tableFrame = tableView.frame;
        tableFrame.size.height = tableHeight;
        tableView.frame = tableFrame;
        
        if (acceptButton_ == nil) {
            acceptButton_ = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        }
        
        acceptButton_.frame = CGRectMake(acceptButton_.frame.origin.x, tableHeight + 10.0f, acceptButton_.frame.size.width, acceptButton_.frame.size.height);
        
        self.containerView.frame = CGRectMake(self.containerView.frame.origin.x, self.containerView.frame.origin.y, self.containerView.frame.size.width, tableHeight + 10.0f + acceptButton_.frame.size.height + 10.0f);
        
        [NXT_Peru_iPhoneStyler styleBlueButton:acceptButton_];
        [acceptButton_ setTitle:NSLocalizedString(CONFIRM_TEXT_KEY, nil) forState:UIControlStateNormal];
        
        [acceptButton_ addTarget:self action:@selector(acceptButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        
        
        [self recalculateScroll];
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initial configuration is set.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];

    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
}

/*
 * Creates and returns an autoreleased TransferToAnotherAccountViewController constructed from a NIB file
 */
+ (InstitutionsAndCompaniesStepFourViewController *)institutionsAndCompaniesStepFourViewController {
    
    InstitutionsAndCompaniesStepFourViewController *result = [[[InstitutionsAndCompaniesStepFourViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Checks whether the warnings button can be displayed.
 */
- (void)checkWarningButtonMustBeDisplayed {
    
    UIDevice *device = [UIDevice currentDevice];
    NSString *version = [device systemVersion];
    
    BOOL displayWarningButton = YES;
    
    if ([version compare:@"6.0"
                 options:NSNumericSearch] != NSOrderedAscending) {
        
        displayWarningButton = NO;
        
    }
    
//    warningsButton_.hidden = ((routeInformation_ == nil) || (!displayWarningButton));
    
}


#pragma mark -
#pragma mark UITableViewDatasource protocol selectors

/**
 * Asks the data source to return the number of sections in the table view. When no coordinate key is needed, 2 is returned, when
 * it is needed to display the coordinate key, 3 is returned
 *
 * @param tableView An object representing the table view requesting this information
 * @return The number of sections in tableView
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {  
    return ([[Tools notNilString:responseInformation_.seal] length]>0)?3:2;
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view. Each section has a calculated number of rows
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section in tableView
 * @return The number of rows in section
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger result = 1;
    
    if (section == 0) {
        
        result = [keyMutableArray_ count];
        
    }

    return result;
    
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. Returns the cell that fit the index path provided
 *
 * @param tableView The table-view object requesting this information
 * @param indexPath An index number identifying a section in tableView
 * @return The number of rows in section
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *result = nil;
    
    if(indexPath.section==0)
    {
        NSString *cellIdentifier = @"cell";
        
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if(cell == nil)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
        }
    
        if ( [NSLocalizedString(DOCUMENT_TITLE_KEY, nil) isEqual:[keyMutableArray_ objectAtIndex:indexPath.row]]) {
            
            NSString *documentNumber = [valueMutableArray_ objectAtIndex:indexPath.row ];
            if ([documentNumber length] > 30) {
                
                [cell.detailTextLabel setLineBreakMode:NSLineBreakByWordWrapping];
                [cell.detailTextLabel setNumberOfLines:2];
                
            }
        }else if ( [NSLocalizedString(CASH_MOBILE_ACCOUNT_CHARGED_TEXT_KEY, nil) isEqual:[keyMutableArray_ objectAtIndex:indexPath.row]]) {
            
                [cell.detailTextLabel setLineBreakMode:NSLineBreakByWordWrapping];
                [cell.detailTextLabel setNumberOfLines:2];
                
            }
        
        
        cell.textLabel.text = [keyMutableArray_ objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [valueMutableArray_ objectAtIndex:indexPath.row];
        
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel withBoldFontSize:14.0f color:[UIColor grayColor]];
        
        if (indexPath.row == 0) {
            [NXT_Peru_iPhoneStyler styleLabel:cell.detailTextLabel withFontSize:20.0f color:[UIColor BBVAMagentaColor]];
        }else{
            [NXT_Peru_iPhoneStyler styleLabel:cell.detailTextLabel withFontSize:13.0f color:[UIColor BBVAGreyToneTwoColor]];
        }
        
        UIView *auxViw = [[UIView alloc] initWithFrame:[cell frame]];
        [auxViw setBackgroundColor:[UIColor whiteColor]];
        
        [cell setBackgroundView:auxViw];
        cell.backgroundView.backgroundColor = [UIColor BBVAWhiteColor];
        cell.backgroundColor = [UIColor whiteColor];
        result = cell;
        
        [auxViw release];
        
    }
    else if (indexPath.section == 1) {
        
        result = [self configureSecondFactorCell];
        
        result.backgroundColor = [UIColor clearColor];
        result.backgroundView.backgroundColor = [UIColor clearColor];
        
    } else if (indexPath.section == 2) {
        
        SealCell *sealCell = (SealCell *)[tableView dequeueReusableCellWithIdentifier:[SealCell cellIdentifier]];
        
        if (sealCell == nil) {
            sealCell = [SealCell sealCell];
        }
        
        sealCell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSData *data = [Tools base64DataFromString:responseInformation_.seal];
        UIImage *image = [[[UIImage alloc] initWithData:data] autorelease];
        [[sealCell sealImageView] setImage:image];
        result = sealCell;
        
        result.backgroundColor = [UIColor clearColor];
        result.backgroundView.backgroundColor = [UIColor clearColor];
        
    }
    
    [result setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return result;
    
}

#pragma mark -
#pragma mark UITableViewDelegate selectors

/**
 * Asks the delegate for the height to use for the footer of a particular section.
 *
 * @param tableView The table-view object requesting this information.
 * @param section An index number identifying a section of tableView .
 */
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0;
    
}

///**
// * Asks the delegate for a view object to display in the footer of the specified section of the table view.
// */
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//
//    UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 0.0f)] autorelease];
//    view.backgroundColor = [UIColor whiteColor];
//    return view;
//    
//}


#pragma mark -
#pragma mark UITableViewDelegate protocol selectors

/**
 * Tells the delegate that the specified row is now selected
 *
 * @param tableView A table-view object informing the delegate about the new row selection
 * @param indexPath An index path locating the new selected row in tableView
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 *
 * @param tableView The table-view object requesting this information
 * @param indexPath An index path that locates a row in tableView
 * @return A floating-point value that specifies the height (in points) that row should be
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat result = 0.0f;
    
    NSUInteger normalizedSection = indexPath.section;
    
    if (normalizedSection == 0) {
        
        result = 50.0;
        
        if ( [NSLocalizedString(DOCUMENT_TITLE_KEY, nil) isEqual:[keyMutableArray_ objectAtIndex:indexPath.row]]) {
            
            NSString *documentNumber = [valueMutableArray_ objectAtIndex:indexPath.row ];
            if ([documentNumber length] > 30) {
                result = 60.0f;
            }
            
        }
        
    } else if (normalizedSection == 1) {
        
        OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
        
        if (otpUsage == otp_UsageOTP) {
            
            result = [OpKeyCell cellHeight];
            
        } else if (otpUsage == otp_UsageTC) {
            
            result = [CoordKeyCell cellHeight];
            
        }
    
    } else if (normalizedSection == 2) {
        
        result = [SealCell cellHeight];
        
    }
    
    return result;
    
}

/**
 * Asks the delegate for the height to use for the header of a particular section.
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section of tableView
 * @return A floating-point value that specifies the height (in points) of the header for section
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    CGFloat result = 0.0f;
    
    if (section == 0) {
        result = 30.0f;
    }else
        result = [SimpleHeaderView height];

	
    return result;
    
}

/**
 * Asks the delegate for a view object to display in the header of the specified section of the table view. The appropriate section
 * header is displayed
 *
 * @param tableView The table-view object asking for the view object
 * @param section An index number identifying a section of tableView
 * @return A view object to be displayed in the header of section
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *result = nil;
    
    NSUInteger normalizedSection = section;
    
    if (normalizedSection == 0) {
                
        UIView *header = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth([tableView frame]), 30.0f)] autorelease];
		[header setBackgroundColor:[UIColor whiteColor]];
		
		UILabel *title = [[[UILabel alloc] initWithFrame:CGRectMake(15.0, 0.0f, CGRectGetWidth([tableView frame]) - 10.0f, 30.0f)] autorelease];
		[NXT_Peru_iPhoneStyler styleLabel:title withBoldFontSize:13.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
        
        
        if ([@"SERV" isEqualToString:responseInformation_.payTypeCode]) {
            [title setText:[NSString stringWithFormat:NSLocalizedString(PAYMENT_CONFIRMATION_OF_TEXT_KEY, nil), [serviceType_ lowercaseString]]];
        }else{
            [title setText:[NSString stringWithFormat:NSLocalizedString(PAYMENT_CONFIRMATION_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT_LOWER_KEY, nil)]];
        }
        
        [header addSubview:title];
		result = header;
        
    } else if (normalizedSection == 1) {
        
        SimpleHeaderView *simpleHeaderView = [SimpleHeaderView simpleHeaderView];
        OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
        
        UIImageView *separatorImage = [[UIImageView alloc] initWithImage:[[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
        
        CGRect frame = [separatorImage frame];
        frame.origin.y = 0;
        frame.size.width = 320.0f;
        [separatorImage setFrame:frame];
        [simpleHeaderView addSubview:separatorImage];
        
        if ((otpUsage == otp_UsageOTP) || (otpUsage == otp_UsageUnknown)) {
            [simpleHeaderView setTitle:NSLocalizedString(TRANSFER_STEP_TWO_OTP_KEY_TEXT_KEY, nil)];
        }else{
            [simpleHeaderView setTitle:NSLocalizedString(TRANSFER_STEP_TWO_COORD_KEY_TEXT_KEY, nil)];
        }
        result = simpleHeaderView;
        
        [separatorImage release];
        
    } else if (normalizedSection == 2) {
        SimpleHeaderView *simpleHeaderView = [SimpleHeaderView simpleHeaderView];
        [simpleHeaderView setTitle:NSLocalizedString(TRANSFER_STEP_TWO_SEAL_TEXT_KEY, nil)];
        result = simpleHeaderView;
        
    }
    
//    [result setBackgroundColor:[UIColor clearColor]];
    
    return result;
    
}

#pragma mark -
#pragma mark Cells management

/*
 * Configures the second factor cell. It shows the OTP key or Coordinate key, depending on the otp service usage
 */
- (UITableViewCell *)configureSecondFactorCell {
    
    UITableViewCell *cell = nil;
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
    if ((otpUsage == otp_UsageOTP) || (otpUsage == otp_UsageUnknown)) {
        
        OpKeyCell *otpKeyCell = (OpKeyCell *)[[self informationTableView] dequeueReusableCellWithIdentifier:[OpKeyCell cellIdentifier]];
        
        if (otpKeyCell == nil) {
            
            if (otpKeyCell_ == nil) {
                
                otpKeyCell_ = [[OpKeyCell opKeyCell] retain];
                
            }
            
            [otpKeyCell_ setSelectionStyle:UITableViewCellSelectionStyleNone];
            [[otpKeyCell_ opKeyTextField] setDelegate:self];
            
            UIView *popButtonsView = [self popButtonsView];
            [[otpKeyCell_ opKeyTextField] setInputAccessoryView:popButtonsView];
            
            otpKeyCell = otpKeyCell_;
            
            [[otpKeyCell_ opKeyTextField] setDelegate:self];
            
            [editableViews_ release];
            editableViews_ = [[NSMutableArray alloc] initWithObjects:[otpKeyCell_ opKeyTextField], nil];
            
        }
        
        cell = otpKeyCell;
        
    } else if (otpUsage == otp_UsageTC) {
    
        CoordKeyCell *coordKeyCell = (CoordKeyCell *)[[self informationTableView] dequeueReusableCellWithIdentifier:[CoordKeyCell cellIdentifier]];
        
        if (coordKeyCell == nil) {
            
            if (coordKeyCell_ == nil) {
                
                coordKeyCell_ = [[CoordKeyCell coordKeyCell] retain];
                
            }
            
            coordKeyCell = coordKeyCell_;
            
            [[coordKeyCell_ keyTextField] setDelegate:self];
            
        }
        
        [coordKeyCell setCoordinate:[responseInformation_ coordinate]];
        [[coordKeyCell keyTextField] setDelegate:self];
        
        UIView *popButtonsView = [self popButtonsView];
        [[coordKeyCell keyTextField] setInputAccessoryView:popButtonsView];
        
        [coordKeyCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        cell = coordKeyCell;
        
        [editableViews_ release];
        editableViews_ = [[NSMutableArray alloc] initWithObjects:[coordKeyCell_ keyTextField], nil];
    }
    
    return cell;
    
}

/*
 * Returns the table cell height for the cell that represents the operation step information stored at teh given index.
 * The cell is a TransferDetailCell instance
 */
- (CGFloat)transferDetailCellHeightAtIndex:(NSInteger)index {
    
    CGFloat result = 0.0f;
    
    TitleAndAttributes *titleAndAttributes = nil;
    
    NSArray *informationArray = self.transferOperationHelper.transferSecondStepInformation;
    NSInteger informationArrayCount = [informationArray count];
    
    if (index < informationArrayCount) {
        
        titleAndAttributes = [informationArray objectAtIndex:index];
        
    }
    
    result = [TransferDetailCell cellHeightForTitleAndAttributes:titleAndAttributes];
    
    return result;
    
}



#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors



/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    if (textField == [coordKeyCell_ keyTextField]) {
        
        if (resultLength <= COORDINATES_MAXIMUM_LENGHT) {
            
            result = YES;
            
        }
        
    } else if (textField == [otpKeyCell_ opKeyTextField]) {
        
        if(resultLength <= OTP_MAXIMUM_LENGHT) {
            
            if ([Tools isValidText:resultString forCharacterString:OTP_VALID_CHARACTERS]) {
                
                result = YES;
            }
        }
        
    } else {
        
        result = YES;
        
    }
    
    return result;
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self editableViewHasBeenClicked:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == [otpKeyCell_ opKeyTextField]) {
        [[otpKeyCell_ opKeyTextField] resignFirstResponder];
    }else if (textField == [coordKeyCell_ keyTextField]){
        [[coordKeyCell_ keyTextField] resignFirstResponder];
    }
    
    return FALSE;
}

#pragma mark -
#pragma mark UIAlertViewDelegate methods

/**
 * Sent to the delegate when the user clicks a button on an alert view.
 *
 * @param alertView: The alert view containing the button.
 * @param buttonIndex: The index of the button that was clicked.
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    [[self navigationController] popViewControllerAnimated:YES];    
}


#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    
    NSString *title = @"";
    
    if ([@"SERV" isEqualToString:responseInformation_.payTypeCode] || [[@"Pago de servicios" lowercaseString] isEqualToString:[responseInformation_.payType lowercaseString]])
    {
        title=  [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil),
                 NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY, nil)];
    }
    else
    {
        
        title= [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT_LOWER_KEY, nil)];
    }
    
    
    result.customTitleView.topLabelText = title;
    
    return result;
    
}

#pragma mark -
#pragma mark user interaction
- (IBAction)acceptButtonTapped{
    
    BOOL canStartProcess = YES;
    
//    Session *session = [Session getInstance];
    NSString *secondFactorKey = @"";
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
	if ((otpUsage == otp_UsageOTP ) && ([[otpKeyCell_ opKeyTextField].text length] == 0)) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_OTP_KEY_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    } else if ((otpUsage == otp_UsageTC) && ([[coordKeyCell_ keyTextField].text length] == 0)) {
        
        [Tools showInfoWithMessage:NSLocalizedString(PAYMENT_ERROR_COORDINATES_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    }
    
    switch (otpUsage) {
            
        case otp_UsageOTP:
            
            secondFactorKey = [otpKeyCell_ opKeyTextField].text;
            
            break;
            
        case otp_UsageTC:
            
            secondFactorKey = [coordKeyCell_ keyTextField].text;
            
            break;
            
        default:
            
            break;
            
    }
    
    if (canStartProcess) {
        
        [self.view endEditing:YES];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(confirmTransferResult:)
                                                     name:kNotificationPaymentInstitutionsSuccessPayResult
                                                   object:nil];
        
        [self.appDelegate showActivityIndicator:poai_Both];
        
        [[Updater getInstance] institutionsAndcompaniesConfirmResultFromSecondKeyFactor:secondFactorKey];
        
    }
}

#pragma mark -
#pragma mark Notifications

/*
 * Account has been updated.
 */
- (void)confirmTransferResult:(NSNotification *)notification {
    
    //DLog( @"confirmTransferResult" );
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationPaymentInstitutionsSuccessPayResult
                                                  object:nil];
    
    [self.appDelegate hideActivityIndicator];
    
    succesResponseInformation_ = [notification object];
    
    if( succesResponseInformation_ != nil &&
       [succesResponseInformation_ isKindOfClass:[PaymentInstitutionAndCompaniesSuccessConfirmationResponse class]] &&
       !succesResponseInformation_.isError){
        
        NSMutableArray *keys = [[NSMutableArray alloc] init];
        NSMutableArray *values = [[NSMutableArray alloc] init];;
        
        keys = [NSMutableArray arrayWithObjects:NSLocalizedString(CASH_MOBILE_NUMBER_OPERATION_TITLE_TEXT_KEY, nil),
                NSLocalizedString(TRANSFER_DATE_HOUR_SHORT_TEXT_KEY, nil),
                NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil),
                NSLocalizedString(CASH_MOBILE_ACCOUNT_CHARGED_TEXT_KEY, nil),
                nil];
        
        NSString *fecha = [NSString stringWithFormat:@"%@ | %@", succesResponseInformation_.date, succesResponseInformation_.time];
        
        NSString *account = [NSString stringWithFormat:@"%@ | %@ \n%@", succesResponseInformation_.accountType, succesResponseInformation_.currency, succesResponseInformation_.subject];
        
        values = [NSMutableArray arrayWithObjects:succesResponseInformation_.operationNumber, fecha, succesResponseInformation_.operation, account,nil];
        
        NSArray *ocurrency = [[succesResponseInformation_ ocurrencyList] ocurrencyList];
        
        if (isWithBD_) {
            
            [keys addObject:NSLocalizedString(INSTITUTIONS_COMPANIES_TITLE_TEXT_KEY, nil)];
            [values addObject:succesResponseInformation_.companieName];
            
            for (Ocurrency *aOccurrency in ocurrency) {
                [keys addObject:aOccurrency.description];
                [values addObject:aOccurrency.value];
            }
            
            [keys addObject:NSLocalizedString(INSTITUTIONS_COMPANIES_HOLDER_NAME_KEY, nil)];
            [values addObject:succesResponseInformation_.holderName];
            
            if (isPartial_) {
                NSArray *pendingDocuments = [succesResponseInformation_.docList pendingDocumentList];
                PendingDocument *doc = [pendingDocuments objectAtIndex:0];
                
                [keys addObject: NSLocalizedString(DOCUMENT_TITLE_KEY, nil)];
                [values addObject:doc.description];
                
                [keys addObject:NSLocalizedString(PUBLIC_SERVICE_STEP_TWO_SELECTED_DUE_DATE_TEXT_KEY, nil)];
                [values addObject:doc.expirationDate];
                
                NSString *minAmount= [NSString stringWithFormat:@"%@%@", doc.minAmountCurrency, doc.minAmount];
                
                [keys addObject:NSLocalizedString(INSTITUTIONS_COMPANIES_MIN_AMOUNT_KEY, nil)];
                [values addObject:minAmount];
                
                NSString *maxAmount = [NSString stringWithFormat:@"%@%@", doc.maxAmountCurrency, doc.maxAmount];
                
                [keys addObject:NSLocalizedString(INSTITUTIONS_COMPANIES_MAX_AMOUNT_KEY, nil)];
                [values addObject:maxAmount];
                //Cambio de la lectura de texto moneda a simbolo moneda
                /*
                 NSString *minAmount= [NSString stringWithFormat:@"%@%@", [Tools getCurrencySimbol:doc.minAmountCurrency], doc.minAmount];
                 ;
                 
                 NSString *maxAmount = [NSString stringWithFormat:@"%@%@", [Tools getCurrencySimbol:doc.maxAmountCurrency ], doc.maxAmount];

                 */
                
                NSString *payedAmount = [NSString stringWithFormat:@"%@%@", succesResponseInformation_.payedAmountCurrency, succesResponseInformation_.payedAmount];
                
                [keys addObject:NSLocalizedString(INSTITUTIONS_COMPANIES_PAYED_AMOUNT_KEY, nil)];
                [values addObject:payedAmount];
                
             
                NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                NSNumber * payedamount = [formatter numberFromString:succesResponseInformation_.changeType];
                if(![payedamount isEqualToNumber:@0]){
                    
                NSString *changeType = [NSString stringWithFormat:@"%@%@", succesResponseInformation_.changeTypeCurrency, succesResponseInformation_.changeType];
                
                [keys addObject:NSLocalizedString(CHANGE_TYPE_CAPITILIZE_TITLE_KEY, nil)];
                [values addObject:changeType];
            }
                NSString *chargedAmount = [NSString stringWithFormat:@"%@%@", succesResponseInformation_.chargedAmountCurrency, succesResponseInformation_.chargedAmount];
                
                [keys addObject:NSLocalizedString(INSTITUTIONS_COMPANIES_CHARGED_AMOUNT_KEY, nil)];
                [values addObject:chargedAmount];
            }else{
                
                [keys addObject: NSLocalizedString(INSTITUTIONS_COMPANIES_DOCUMENTS_NUMBER_KEY, nil)];
                [values addObject:succesResponseInformation_.docNumber];
                
                NSArray *pendingDocs = [succesResponseInformation_.docList pendingDocumentList];
                
                for (PendingDocument *doc in pendingDocs) {
                   [keys addObject: [Tools notNilString:[NSString stringWithFormat:@"Recibo %@", doc.description]]];
                    
                    NSString *totalAmount = [NSString stringWithFormat:@"%@%@", doc.totalAmountCurrency, doc.totalAmount];
                    
                    [values addObject:totalAmount];
                }
                
                NSString *chargedAmount = [NSString stringWithFormat:@"%@%@", succesResponseInformation_.chargedAmountCurrency, succesResponseInformation_.chargedAmount];
                
                [keys addObject:NSLocalizedString(INSTITUTIONS_COMPANIES_CHARGED_AMOUNT_KEY, nil)];
                [values addObject:chargedAmount];
             
                NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                NSNumber * payedamount = [formatter numberFromString:succesResponseInformation_.changeType];
                if(![payedamount isEqualToNumber:@0]){
                    
                
                NSString *changeType = [NSString stringWithFormat:@"%@%@", succesResponseInformation_.changeTypeCurrency, succesResponseInformation_.changeType];
                
                [keys addObject:NSLocalizedString(CHANGE_TYPE_CAPITILIZE_TITLE_KEY, nil)];
                [values addObject:changeType];
            }
                NSString *payedAmount = [NSString stringWithFormat:@"%@%@", succesResponseInformation_.payedAmountCurrency, succesResponseInformation_.payedAmount];
                
                [keys addObject:NSLocalizedString(INSTITUTIONS_COMPANIES_PAYED_AMOUNT_KEY, nil)];
                [values addObject:payedAmount];
                
            }
        }else{
            for (Ocurrency *aOccurrency in ocurrency) {
                [keys addObject:aOccurrency.description];
                [values addObject:aOccurrency.value];
            }
            
            NSString *payedAmount = [NSString stringWithFormat:@"%@%@", succesResponseInformation_.payedAmountCurrency, succesResponseInformation_.payedAmount];
            
            [keys addObject:NSLocalizedString(INSTITUTIONS_COMPANIES_CONFIRMATION_AMOUNT_KEY, nil)];
            [values addObject:payedAmount];
            
            if(![succesResponseInformation_.payedAmountCurrency isEqualToString:succesResponseInformation_.chargedAmountCurrency])
            {
                NSString *changeType = [NSString stringWithFormat:@"%@%@", succesResponseInformation_.changeTypeCurrency, succesResponseInformation_.changeType];
            
                [keys addObject:NSLocalizedString(CHANGE_TYPE_CAPITILIZE_TITLE_KEY, nil)];
                [values addObject:changeType];
            }
            
            NSString *chargedAmount = [NSString stringWithFormat:@"%@%@", succesResponseInformation_.chargedAmountCurrency, succesResponseInformation_.chargedAmount];
            
            [keys addObject:NSLocalizedString(INSTITUTIONS_COMPANIES_CHARGED_AMOUNT_KEY, nil)];
            [values addObject:chargedAmount];
        }
        
        if (transfersFinalViewController_ == nil) {
            transfersFinalViewController_ = [[InstitutionAndCompaniesFinalViewController institutionAndCompaniesFinalViewController] retain];
        }
        
        // transferConfirmAdditionalModalViewController_.view.frame = self.view.frame;
        CGRect aFrame = self.view.frame;
        aFrame.origin.x = 0.0f;
        aFrame.origin.y = 0.0f;
        
        if (succesResponseInformation_.message != nil && ![@"" isEqualToString:succesResponseInformation_.message]) {
            transfersFinalViewController_.message = succesResponseInformation_.message;
        }

        transfersFinalViewController_.keyMutableArray = keys;
        transfersFinalViewController_.valueMutableArray = values;
        transfersFinalViewController_.serviceType = serviceType_;
        transfersFinalViewController_.isFOfinished = FALSE;
        transfersFinalViewController_.responseInformation = succesResponseInformation_;
        
        [self.navigationController pushViewController:transfersFinalViewController_ animated:TRUE];
        
    }
}

@end

