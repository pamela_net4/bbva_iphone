//
//  InstitutionsAndCompaniesViewController.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 10/29/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "PaymentDataBaseViewController.h"
#import "CheckCell.h"
#import "CardContStepOneViewController.h"
#import "CardContOwnCardViewController.h"
#import "CardContThirdCardViewController.h"
#import "InsititutionAndCompaniesStepTwoViewController.h"
#import "NXTEditableViewController.h"
#import "PaymentCardProcess.h"
#import "NXTComboButton.h"
#import "TransfersStepOneViewController.h"
#import "NXTTextField.h"
#import "TransfersStepOneViewController.h"

@class PaymentInstitutionsAndCompaniesInitialResponse;

@interface InstitutionsAndCompaniesViewController : TransfersStepOneViewController <UITableViewDelegate, UITableViewDataSource, MOKStringListSelectionButtonDelegate, UITextFieldDelegate, NXTComboButtonDelegate>
{
@private
    
    UIButton *buttonSearch_;
    
    /*
     * group combo
     */
    NXTComboButton *groupCombo_;
    
    /**
     * Transfer amount text field
     */
    NXTTextField *searchByNameTextField_;
    /**
     * Header
     */
    SimpleHeaderView *header_;
    
    /**
     * Table view
     */
    UITableView *table_;
    
    /**
     * Table view
     */
    UITableView *tableSearch_;
    
    /**
     * Own check cell
     */
    CheckCell *checkCell_;
    
    /**
     * Continue button
     */
    UIButton *continueButton_;
    
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    
    /**
     * Card Cont Own Card View Controller
     */
    CardContOwnCardViewController *cardContOwnCardViewController_;
    
    /**
     * Card Cont Third Card View Controller
     */
    CardContThirdCardViewController *cardContThirdCardViewController_;
    
#pragma logic
    
    /**
     * Active flag
     */
    NSInteger activeCellIndex_;
    
    /**
     * Cards array
     */
    NSMutableArray *cardsArray_;
    
    /**
     * Card names array
     */
    NSMutableArray *cardNamesArray_;
    
    /**
     * Card selected index
     */
    NSInteger cardSelectedIndex_;
    
    /**
     * Payment card process
     */
    PaymentCardProcess *process_;
    
    /**
     * First part of the card
     */
    NSString *firstPartCard_;
    
    /**
     * Second part of the card
     */
    NSString *secondPartCard_;
    
    /**
     * Third part of the card
     */
    NSString *thirdPartCard_;
    
    /**
     * Fourth part of the card
     */
    NSString *fourthPartCard_;
    
    /**
     * Response information
     */
    PaymentInstitutionsAndCompaniesInitialResponse *responseInformation_;
    
    /**
     * Next View for institutions
     */
    InsititutionAndCompaniesStepTwoViewController *institutionsAndCompaniesDetailViewController_;
    
    /**
     * Resoult of searching 
     */
    PaymentInstitutionsAndCompaniesInitialResponse *paymentInstitutionsAndCompaniesSearchResponse_;
    
    /**
     * Groups array
     */
    NSArray *groupsArray;
    
    /**
     * Institutions array
     */
    NSMutableArray *institutionsArray_;
    
    UILabel *noResultLabel_;
    
    /**
     * flag for the search kind
     */
    BOOL isSearchField;
    
    NSInteger currentPage;
    
    /**
     * Distance to move the view
     */
    float distanceToMoveView;
}

/**
 * Provides read-write access to the response information
 */
@property (nonatomic, readwrite, retain) PaymentInstitutionsAndCompaniesInitialResponse *responseInformation;

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UITableView *tableSearch;

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UITableView *table;

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIButton *continueButton;

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIImageView *brandingLine;

/**
 * Provides readwrite access to the firstComboButton and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTComboButton *groupCombo;

/**
 * Provides readwrite access to the search name  and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet  NXTTextField *searchByNameTextField;

/**
 * Provides readwrite access to the buttonSearch to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIButton *buttonSearch;

/**
 * Provides read-write access to the cardsArray
 */
@property (retain, readwrite, nonatomic) NSArray *cardsArray;

/**
 * Provides read-write access to the intitutionsArray
 */
@property (retain, readwrite, nonatomic) NSMutableArray *institutionsArray;

/**
 * Provides read-write access to the hasNavigateForward
 */
@property (readwrite, nonatomic, assign) BOOL hasNavigateForward;

@property (retain, readwrite, nonatomic)  PaymentInstitutionsAndCompaniesInitialResponse *paymentInstitutionsAndCompaniesSearchResponse;

/**
 * Creates and returns an autoreleased CardContStepOneViewController constructed from a NIB file.
 *
 * @return The autoreleased CardContStepOneViewController constructed from a NIB file.
 */
+ (InstitutionsAndCompaniesViewController *)institutionsAndCompaniesViewController;

/**
 * Reset the information in the view
 */
- (void)resetInformationPayment;

/**
 * The search button has been tapped
 */
- (IBAction)searchButtonTapped;

/**
 * Invoked by framework when the group combo is tapped. Sets it as the editing control
 */
- (IBAction)groupComboPressed;

@property (retain, nonatomic) IBOutlet UILabel *noResultLabel;

@end