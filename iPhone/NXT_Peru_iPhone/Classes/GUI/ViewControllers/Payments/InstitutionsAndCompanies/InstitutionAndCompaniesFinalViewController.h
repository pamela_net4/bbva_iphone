//
//  InstitutionAndCompaniesFinalViewController.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/11/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "NXTTransfersViewController.h"
#import "FOInscriptionBaseProcess.h"
#import "FrequentOperationAlterStepOneViewControllerViewController.h"

@class PaymentInstitutionAndCompaniesSuccessConfirmationResponse;
@interface InstitutionAndCompaniesFinalViewController : NXTTransfersViewController<UITableViewDelegate, UITableViewDataSource,FOInscriptionBaseProcessDelegate> {
    
@private
    
	/**
     * Global position button
     */
    UIButton *gpButton_;
	
    /**
     * Transfers button
     */
    UIButton *transfersButton_;
    
    /**
     * fo button
     */
    UIButton *foButton_;
    
#pragma Logic
    /**
     * mutable Array key labels first section
     */
    NSArray *keyMutableArray_;
    
    /**
     * mutable Array value labels first section
     */
    NSArray *valueMutableArray_;
    
    /**
     * Additional information from the services
     */
    PaymentInstitutionAndCompaniesSuccessConfirmationResponse *responseInformation_;
    
    FrequentOperationAlterStepOneViewControllerViewController *frequentOperationReactiveStepOneViewController_;
    
    /**
     * Service name
     */
    NSString *serviceType_;
    
    /**
     * Service name
     */
    BOOL hasScheduleMessage_;
    
    /**
     * Service name
     */
    BOOL isFOfinished_;
    
    /**
     * process FO
     **/
    FOInscriptionBaseProcess *foInscriptionBaseProcess_;
    
    /**
     *  Name of the holder of the operation
     **/
    NSString *holderName_;
}
/**
 * Provides readwrite access to the titleView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet FrequentOperationAlterStepOneViewControllerViewController *frequentOperationReactiveStepOneViewController;

///**
// * Provides readwrite access to the inscription process
// */
//@property (nonatomic, readwrite, retain) FOInscriptionBaseProcess *foInscriptionBaseProcess_;

/**
 * Provides readwrite access to the service type name
 */
@property (nonatomic, readwrite, retain) NSString *serviceType;

/**
 * Provides readwrite access to the gpButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *gpButton;

/**
 * Provides readwrite access to the gpButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *foButton;

/**
 * Provides readwrite access to the transfersButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *transfersButton;

/**
 * Boolean to know if is the resend button should be display
 */
@property (nonatomic) BOOL canResend;

/**
 * Mutable array with keys for the table
 */
@property (nonatomic,retain) NSArray *keyMutableArray;

/**
 * Mutable array with values for the table
 */
@property (nonatomic,retain) NSArray *valueMutableArray;

/**
 * Information message from the service
 */
@property(nonatomic, readwrite, retain) NSString *message;

/**
 * Additional information from the services
 */
@property(nonatomic, readwrite, retain) PaymentInstitutionAndCompaniesSuccessConfirmationResponse *responseInformation;

/**
 * Creates and returns an autoreleased TransfersStepThreeViewController constructed from a NIB file
 *
 * @return The autoreleased TransfersStepThreeViewController constructed from a NIB file
 */
+ (InstitutionAndCompaniesFinalViewController *)institutionAndCompaniesFinalViewController;


/**
 * Flag fo finished
 */
@property(nonatomic, readwrite, assign) BOOL isFOfinished;

/**
 * The acceptButton has been pressed
 */
- (IBAction)gpButtonPressed;

/**
 * The saveButton has been pressed
 */
- (IBAction)transfersButtonPressed;

/**
 * The foButton has been pressed
 */
- (IBAction)foButtonPressed;


@end
