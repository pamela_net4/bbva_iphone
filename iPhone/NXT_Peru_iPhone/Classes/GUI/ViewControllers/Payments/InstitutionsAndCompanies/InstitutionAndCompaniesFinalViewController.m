//
//  InstitutionAndCompaniesFinalViewController.m
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/11/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "InstitutionAndCompaniesFinalViewController.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTEditableViewController+protected.h"
#import "NXTNavigationItem.h"
#import "NXTTableCell.h"
#import "NXTTransfersViewController+protected.h"
#import "SimpleHeaderView.h"
#import "Session.h"
#import "TitleAndAttributes.h"
#import "PaymentInstitutionAndCompaniesSuccessConfirmationResponse.h"
#import "StringKeys.h"
#import "Tools.h"
#import "TransferDetailCell.h"
#import "TransferDoneCell.h"
#import "TransferOperationHelper.h"
#import "TransferSuccessResponse.h"
#import "TransferSuccessAdditionalInformation.h"
#import "Constants.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"
#import "Updater.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"InstitutionAndCompaniesFinalViewController"


#pragma mark -


@interface InstitutionAndCompaniesFinalViewController ()
/**
 * Releases graphic elements
 */
- (void)releaseTransfersStepThreeViewControllerGraphicElements;

/**
 * Returns the NXTTableCell
 *
 * @return The cell
 */
- (UITableViewCell *)nxtTableCell;

/**
 * Returns the TransferDoneCell
 *
 * @return The cell
 */
- (UITableViewCell *)transferDoneCell;

@end


@implementation InstitutionAndCompaniesFinalViewController
#pragma mark -
#pragma mark Properties

@synthesize gpButton = gpButton_;
@synthesize transfersButton = transfersButton_;
@synthesize message;
@synthesize keyMutableArray = keyMutableArray_;
@synthesize valueMutableArray = valueMutableArray_;
@synthesize responseInformation = responseInformation_;
@synthesize serviceType = serviceType_;
@synthesize foButton = foButton_;
@synthesize isFOfinished = isFOfinished_;
@synthesize frequentOperationReactiveStepOneViewController=frequentOperationReactiveStepOneViewController_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self releaseTransfersStepThreeViewControllerGraphicElements];
    
    serviceType_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransfersStepThreeViewControllerGraphicElements];
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransfersStepThreeViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseTransfersStepThreeViewControllerGraphicElements {
    
	[gpButton_ release];
	gpButton_ = nil;
    
    [transfersButton_ release];
	transfersButton_ = nil;
    
    [foButton_ release];
	foButton_ = nil;
    
    message = nil;
    
} 

#pragma mark -
#pragma mark View managment

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
	[NXT_Peru_iPhoneStyler styleBlueButton:gpButton_];
    [gpButton_ setTitle:NSLocalizedString(GLOBAL_POSITION_TITLE_TEXT_KEY, nil) forState:UIControlStateNormal];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:transfersButton_];
    [transfersButton_ setTitle:NSLocalizedString(PAYMENT_TITLE_TEXT_KEY, nil) forState:UIControlStateNormal];
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"IndicatorPF"] isEqualToString:@"S"])
    {
        [NXT_Peru_iPhoneStyler styleBlueButton:foButton_];
        [foButton_ setTitle:@"Inscribir a operación frecuente" forState:UIControlStateNormal];
        
    }
    
    if ([responseInformation_ scheduleAmpl] != nil && ![@"" isEqualToString:[responseInformation_ scheduleAmpl]]) {
        hasScheduleMessage_ = TRUE;
    }else{
        hasScheduleMessage_ = FALSE;
    }
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingImageView] frame]) - CGRectGetHeight([[self operationTypeHeader] frame]);
    
    [self setScrollNominalFrame:scrollFrame];
    
}

-(void) viewWillAppear:(BOOL)animated {
    
    if ([@"SERV" isEqualToString:responseInformation_.payTypeCode] || [[@"Pago de servicios" lowercaseString] isEqualToString:[responseInformation_.payType lowercaseString]]){
        [[[self operationTypeHeader] titleLabel] setText:serviceType_];
    }else{
        [[[self operationTypeHeader] titleLabel] setText:NSLocalizedString(PAYMENT_INSTITUTIONS_AND_COMPANIES_TITLE_TEXT_KEY, nil)];
    }
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased TransfersStepThreeViewController constructed from a NIB file
 */
+ (InstitutionAndCompaniesFinalViewController *)institutionAndCompaniesFinalViewController {
    
    return [[[InstitutionAndCompaniesFinalViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
}

#pragma mark -
#pragma mark User interaction
- (IBAction)foButtonPressed
{
    [[self appDelegate] showActivityIndicator:poai_Both];

    
    foInscriptionBaseProcess_ = [[FOInscriptionBaseProcess alloc] init];
    [foInscriptionBaseProcess_ setDelegate:self];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *parameters = [userDefault objectForKey:@"Parametros"];
    NSArray *array = [parameters componentsSeparatedByString:@"$"];
    NSString *CodMatri = [userDefault objectForKey:@"CodMatri"];
    NSString *arreglo = [userDefault objectForKey:@"arreglo"];
    
    
    [[Updater getInstance] obtainPaymentToInstitutionAndCompaniesFrequentOperationReactiveStepOneWithOperation:
     responseInformation_.operation
                                                                                                   andCodMatri:CodMatri
                                                                                                     andsClase:array[3]
                                                                                                   andsEntidad:array[1]
                                                                                                andsCodEntidad:array[0]
                                                                                                    andArreglo:arreglo
                                                                                               andsDescripcion:responseInformation_.companieName
                                                                                                andCuentaCargo:responseInformation_.subject
                                                                                                 andHolderName:holderName_];
    
}

/*
 * The gpButtonPressed has been pressed
 */
- (IBAction)gpButtonPressed {
	
    [self.appDelegate displayGlobalPositionTab];
    
}

/*
 * The transfersButtonPressed has been pressed
 */
- (IBAction)transfersButtonPressed {
    
    [[self navigationController] popToRootViewControllerAnimated:YES];
    
}

#pragma mark -
#pragma mark Table Cell configuration

/*
 * Returns the NXTTableCell
 */
- (UITableViewCell *)nxtTableCell {
    
    NSString *cellId = [NXTTableCell cellIdentifier];
    
    NXTTableCell *result = (NXTTableCell *)[self.informationTableView dequeueReusableCellWithIdentifier:cellId];
    
    if (result == nil) {
        
        result = [NXTTableCell NXTTableCell];
        
    }
    
    result.leftText = NSLocalizedString(TRANSFER_STEP_3_TEXT_KEY, nil);
    result.rightText = @"";
    result.showDisclosureArrow = NO;
    result.showSeparator = NO;
    
    return result;
    
}


/**
 * Returns the TransferDoneCell
 *
 * @return The cell
 */
- (UITableViewCell *)transferDoneCell {
    
    NSString *cellId = [TransferDoneCell cellIdentifier];
    
    TransferDoneCell *result = (TransferDoneCell *)[self.informationTableView dequeueReusableCellWithIdentifier:cellId];
    
    if (result == nil) {
        
        result = [TransferDoneCell transferDoneCell];
        
    }
    
    result.leftText = NSLocalizedString(PUBLIC_SERVICE_STEP_FOUR_CORRECT_PAYMENT_TITLE_TEXT_KEY, nil);
        
    
    result.rightText = @"";
    result.showDisclosureArrow = NO;
    result.showSeparator = NO;
    
    [[result backgroundView] setBackgroundColor:[UIColor clearColor]];
    CGRect frame = result.frame;
    frame.size.width = 300;
    [result setFrame:frame];
    
    frame = [result.tickImageView frame];
    frame.size.width = 300;
    [result.tickImageView setFrame:frame];
    
    return result;
    
}

#pragma mark -
#pragma mark TransfersViewController selectors

/**
 * Returns the step string to display in the step label. Returns the transfer third step string
 *
 * @return The transfer third step string
 */
- (NSString *)stepLabelString {
    
    return NSLocalizedString(TRANSFER_STEP_3_TEXT_KEY, nil);
    
}

/**
 * Displays the stored information
 */
- (void)displayStoredInformation {
    
    [super displayStoredInformation];
    
    if ([self isViewLoaded]) {
        
        [self layoutViews];
        
    }
    
}

/**
 * Lays out the views
 */
- (void)layoutViews {
    
    [super layoutViews];
    
    if ([self isViewLoaded]) {
        
        UITableView *informationTableView = self.informationTableView;
        [informationTableView reloadData];
        CGSize contentSize = informationTableView.contentSize;
        CGFloat height = contentSize.height;
        CGRect frame = informationTableView.frame;
        frame.origin.y = 0.0f;
        frame.size.height = height;
        informationTableView.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT2;

        if([[[NSUserDefaults standardUserDefaults] objectForKey:@"IndicatorPF"] isEqualToString:@"S"])
        {
            if (!isFOfinished_) {
                
                frame = foButton_.frame;
                frame.origin.y = auxPos;
                foButton_.frame = frame;
                auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
            }
        }
        frame = gpButton_.frame;
        frame.origin.y = auxPos;
        gpButton_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = transfersButton_.frame;
        frame.origin.y = auxPos;
        transfersButton_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        UILabel *bottomInfoLabe = self.bottomInfoLabel;
        frame = bottomInfoLabe.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:bottomInfoLabe
                            forText:bottomInfoLabe.text];
        frame.size.height = height;
        bottomInfoLabe.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.size.height = auxPos;
        containerView.frame = frame;
        
        [self recalculateScroll];
        
    }
    
}

/**
 * Returns the variable information for the view controller. All objects must be TitleAndAttributes instances (even though the array if
 * filtered). The transfer second step information is returned
 *
 * @return The variable information for the view controller
 * @protected
 */
- (NSArray *)variableInformation {
    
    return [self.transferOperationHelper transferThirdStepInformation];
    
}

#pragma mark -
#pragma mark UITableViewDatasource protocol selector

/**
 * Tells the data source to return the number of rows in a given section of a table view. The number of cells is equal to the number of
 * information elements plus one
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section in tableView
 * @return The number of rows in section
 */
- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section {

    //result += [self variableInformationEntriesCount];
    if (section == 0) {
        return 1;
    }else
        return [keyMutableArray_ count];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. Cells can be a blue transfer done cell or
 * an information cell
 *
 * @param tableView The table-view object requesting this information
 * @param indexPath An index number identifying a section in tableView
 * @return The number of rows in section
 */
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    UITableViewCell *result;
    
    if (indexPath.section == 0 ) {
        
        result = [self transferDoneCell];
        [result setBackgroundColor:[UIColor clearColor]];
    }
    else
    {
    
    NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
    }
    
        cell.textLabel.text = [keyMutableArray_ objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [valueMutableArray_ objectAtIndex:indexPath.row ];
        
        if ([NSLocalizedString(INSTITUTIONS_COMPANIES_HOLDER_NAME_KEY, nil) isEqual:[keyMutableArray_ objectAtIndex:indexPath.row]]) {
            holderName_ = [valueMutableArray_ objectAtIndex:indexPath.row ];
        }
        
        NSString *documentNumber = [valueMutableArray_ objectAtIndex:indexPath.row ];
        if ([documentNumber length] > 30) {
            
            [cell.detailTextLabel setLineBreakMode:NSLineBreakByWordWrapping];
            [cell.detailTextLabel setNumberOfLines:2];
            
        }
        
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel withBoldFontSize:14.0f color:[UIColor grayColor]];
        
        if ([[keyMutableArray_ objectAtIndex:indexPath.row] isEqualToString:NSLocalizedString(INSTITUTIONS_COMPANIES_CHARGED_AMOUNT_KEY, nil)]) {
            [NXT_Peru_iPhoneStyler styleLabel:cell.detailTextLabel withFontSize:13.0f color:[UIColor BBVAMagentaColor]];
        }else{
            [NXT_Peru_iPhoneStyler styleLabel:cell.detailTextLabel withFontSize:13.0f color:[UIColor BBVAGreyToneTwoColor]];
        }
    
        result = cell;
    }
    
    [result setSelectionStyle:UITableViewCellSelectionStyleNone];
    return result;
    
}

#pragma mark -
#pragma mark UITableViewDelegate protocol selectors

/**
 * Asks the delegate for the height to use for a row in a specified location.
 *
 * @param tableView The table-view object requesting this information
 * @param indexPath An index path that locates a row in tableView
 * @return A floating-point value that specifies the height (in points) that row should be
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat result = 00.0f;
    
    NSUInteger row = indexPath.row;
    
    if (row == 0) {
        
        result = [TransferDoneCell cellHeight];
        
    } else {
        
        NSString *valueText = [valueMutableArray_ objectAtIndex:indexPath.row ];
        
        if ([valueText length] > 30) {
            result =  60.0f;
        }else {
            result = 55.0f;
            
        }
    }
    
    return result;
    
}

/**
 * Asks the delegate for the height to use for the footer of a particular section.
 *
 * @param tableView The table-view object requesting this information.
 * @param section An index number identifying a section of tableView .
 */
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (section == 0) {
        return 10.0f;
    }
    
    CGFloat margin = 5.0f;
    CGFloat labelHeight = 60.0f;
    CGFloat result = 0.0f;
    
    result = result + margin + labelHeight;
    
    result = result + margin;
    
    if (hasScheduleMessage_) {
        result += margin + labelHeight + margin;
    }
    return result;
    
}

/**
 * Asks the delegate for a view object to display in the footer of the specified section of the table view.
 */
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if (section == 0) {
        
        UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 300.0f, 10.0f)] autorelease];
        view.backgroundColor = [UIColor clearColor];
        
        return view;
    }
    
    UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 300.0f, 0.0f)] autorelease];
    view.backgroundColor = [UIColor clearColor];
    
    CGFloat yPosition;
    CGFloat margin = 5.0f;
    CGFloat labelHeight = 60.0f;
    
        yPosition = margin;
    
    if (hasScheduleMessage_) {
        
        UILabel *labelSchedule = [[[UILabel alloc] init] autorelease];
        labelSchedule.frame = CGRectMake(5.0, yPosition, 290.0, labelHeight);
        [labelSchedule setTextAlignment:UITextAlignmentCenter];
        labelSchedule.backgroundColor = [UIColor clearColor];
        [NXT_Peru_iPhoneStyler styleLabel:labelSchedule
                             withFontSize:12.0f
                                    color:[UIColor grayColor]];
        
        labelSchedule.text = [responseInformation_ scheduleAmpl];
        
        [labelSchedule setNumberOfLines:0];
        [view addSubview:labelSchedule];
        
        yPosition = CGRectGetMaxY([labelSchedule frame]) + 5.0f;
        
    }
    
    UILabel *confLabel = [[[UILabel alloc] initWithFrame:CGRectMake(5.0, yPosition, 290.0f, labelHeight)] autorelease];
    yPosition = yPosition + labelHeight + margin;
    
    [NXT_Peru_iPhoneStyler styleLabel:confLabel withFontSize:11.0f color:[UIColor grayColor]];
    confLabel.numberOfLines = 2;
    confLabel.textAlignment = UITextAlignmentCenter;
    confLabel.backgroundColor = [UIColor clearColor];
    
    confLabel.text = message;
    
    view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, yPosition);
    [view addSubview:confLabel];
    
    
    return view;
    
}


- (void)dataAnalysisHasFinished
{
    frequentOperationReactiveStepOneViewController_ = [FrequentOperationAlterStepOneViewControllerViewController frequentOperationAlterStepOneViewController];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(frequentOperationHasFinished:)  name: @"frequentOperationHasFinished" object: nil];
    
    [foInscriptionBaseProcess_ setRootViewController: self];
    [frequentOperationReactiveStepOneViewController_ setProcess: foInscriptionBaseProcess_];
    [frequentOperationReactiveStepOneViewController_ awakeFromNib];
    [self.navigationController pushViewController:frequentOperationReactiveStepOneViewController_ animated:YES];
    
}

-(void)frequentOperationHasFinished : (NSNotification *) notification {
    
    foButton_.hidden = TRUE;
    isFOfinished_ = TRUE;
    [self layoutViews];
    
}


#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    
    NSString *title = @"";
    
    if ([@"SERV" isEqualToString:responseInformation_.payTypeCode] || [[@"Pago de servicios" lowercaseString] isEqualToString:[responseInformation_.payType lowercaseString]])
    {
        title=  [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil),
                 NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY, nil)];
    }
    else
    {
        
        title= [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT_LOWER_KEY, nil)];
    }
    
    result.customTitleView.topLabelText = title;
    
    result.hidesBackButton = TRUE;
    
    return result;
    
}

@end
