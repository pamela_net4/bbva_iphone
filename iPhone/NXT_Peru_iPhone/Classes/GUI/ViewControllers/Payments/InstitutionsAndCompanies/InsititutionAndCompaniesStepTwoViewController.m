//
//  InsititutionAndCompaniesStepTwoViewController.m
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 27/11/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "InsititutionAndCompaniesStepTwoViewController.h"

#import "NXTEditableViewController+protected.h"
#import "Constants.h"
#import "Field.h"
#import "FieldList.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXTCurrencyTextField.h"
#import "NXTTextField.h"
#import "NXTTransfersViewController.h"
#import "NXTNavigationItem.h"
#import "InstitutionsAndCompaniesStepThirdViewController.h"
#import "PaymentInstitutionsAndCompaniesDetailResponse.h"
#import "PaymentInstitutionAndCompaniesConfirmationResponse.h"
#import "PublicServiceHeaderView.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UINavigationItem+DoubleLabel.h"
#import "UIColor+BBVA_Colors.h"
#import "Updater.h"
#import "TransfersStepOneViewController.h"
#import "OcurrencyList.h"
#import "Ocurrency.h"
/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"InsititutionAndCompaniesStepTwoViewController"

#define START_LABEL 135


@implementation InsititutionAndCompaniesStepTwoViewController

@synthesize continueButton=continueButton_;
@synthesize brandingLine=brandingLine_;
@synthesize labelCompanie=labelCompanie_;
@synthesize labelNameCompanie=labelNameCompanie_;
@synthesize responseInformation = responseInformation_;
@synthesize paymentInstitutionAndCompaniesConfirmationResponse = paymentInstitutionAndCompaniesConfirmationResponse_;
@synthesize institutionsAndCompaniesPaymentViewController = institutionsAndCompaniesPaymentViewController_;
@synthesize containerView = containerView_;
@synthesize serviceType = serviceType_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseCardContStepOneViewControllerGraphicElements];
    
    [continueButton_ release];
    continueButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
    responseInformation_ = nil;
    
    [containerView_ release];
    containerView_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseCardContStepOneViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseCardContStepOneViewControllerGraphicElements {
    
    [header_ release];
    header_ = nil;
    
    [continueButton_ release];
    continueButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
}

#pragma mark -
#pragma mark - list selection delegate
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton
{
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];

    [NXT_Peru_iPhoneStyler styleLabel:labelCompanie_ withFontSize:14.0 color:[UIColor BBVABlackColor]];
    
    
    [NXT_Peru_iPhoneStyler styleLabel:labelNameCompanie_ withFontSize:12.0 color:[UIColor BBVAGreyColor]];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:continueButton_];
    [continueButton_ setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil)
                     forState:UIControlStateNormal];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    header_ = [[PublicServiceHeaderView publicServiceHeaderView] retain];
    
    if ([@"SERV" isEqualToString:responseInformation_.payTypeCode] || [[@"Pago de servicios" lowercaseString] isEqualToString:[responseInformation_.payType lowercaseString]]) {
        [header_ setTitle:serviceType_];
    }else{
        [header_ setTitle:NSLocalizedString(PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT_KEY, nil)];
    }
    
    [[self containerView] addSubview:header_];
    
    textFieldArray_ = [[NSMutableArray alloc] init];
    [labelNameCompanie_ setText:[[responseInformation_.institutionName lowercaseString]capitalizedString]];
    
    // add textfield and label from service
    [self addTextFieldAndLabelFromServer];
    
    [[self containerView] addSubview:continueButton_];
    [[self containerView] bringSubviewToFront:continueButton_];
    
    [containerView_ setBackgroundColor:[UIColor clearColor]];
    [self setScrollableView:containerView_];
    
    
}

-(void)addTextFieldAndLabelFromServer
{
    NSArray *fieldArray = [[responseInformation_ fieldList] fieldList];
    
    CGFloat lastHeight = 0.0;
    
    for(int i =0; i< [fieldArray count]; i++)
    {
        Field *field = [fieldArray objectAtIndex:i];
        
        //TextField label title
        UILabel *labelServer = [[UILabel alloc] init];
        labelServer.frame = CGRectMake(15.0, 105.0 + 65*i, 290.0, 20.0);
        labelServer.backgroundColor = [UIColor clearColor];
        [NXT_Peru_iPhoneStyler styleLabel:labelServer
                             withFontSize:14.0f
                                    color:[UIColor BBVABlueSpectrumColor]];
        labelServer.text = field.description;
        
        [[self containerView] addSubview:labelServer];
        
        
        //Texfield
        UITextField *textFieldServer;
        
        if ([@"m" isEqualToString:[[field validationType] lowercaseString]]){
            
            textFieldServer = [[NXTCurrencyTextField alloc] init];
            
            [(NXTCurrencyTextField *)textFieldServer setCanContainCents:YES];
            [(NXTCurrencyTextField *)textFieldServer setCurrencySymbol:[Tools getCurrencySimbol:[field auxLabel]]];
            [(NXTCurrencyTextField *)textFieldServer setDelegate:self];
            [textFieldServer setPlaceholder:NSLocalizedString(TRANSFER_AMOUNT_HINT_TEXT_KEY, nil)];

        }else{
            
            textFieldServer = [[NXTTextField alloc] init];
            NSString *placeholder = [[field.description lowercaseString] stringByReplacingOccurrencesOfString:@"ingresa " withString:@""] ;
            
            [textFieldServer setPlaceholder:[[placeholder lowercaseString] stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[[placeholder lowercaseString] substringToIndex:1] capitalizedString]]];
            
            [textFieldServer setDelegate:self];
            
            if ([@"n" isEqualToString:[[field validationType] lowercaseString]]){
                [textFieldServer setKeyboardType:UIKeyboardTypeNumberPad];
            }
        }
        
        [NXT_Peru_iPhoneStyler styleTextField:(NXTTextField *)textFieldServer withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
        
        textFieldServer.frame = CGRectMake(15.0, 130.0 + 65*i, 290.0, 30.0);
        [textFieldServer setInputAccessoryView:popButtonsView_];        
        [textFieldServer setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];        
        [textFieldServer setTag:i];
        [textFieldServer setAutocorrectionType:UITextAutocorrectionTypeNo];
        [textFieldArray_ addObject:textFieldServer];        
        
        [[self containerView] addSubview:textFieldServer];
        
        lastHeight = CGRectGetMaxY([textFieldServer frame]) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
    }
    
    if ([responseInformation_ scheduleAmpl] != nil && ![@"" isEqualToString:[responseInformation_ scheduleAmpl]]) {
        
        UILabel *labelSchedule = [[[UILabel alloc] init] autorelease];
        labelSchedule.frame = CGRectMake(0.0, lastHeight, 320.0, 30.0);
        [labelSchedule setTextAlignment:UITextAlignmentCenter];
        labelSchedule.backgroundColor = [UIColor clearColor];
        [NXT_Peru_iPhoneStyler styleLabel:labelSchedule
                             withFontSize:12.0f
                                    color:[UIColor BBVABlackColor]];
        
        labelSchedule.text = [responseInformation_ scheduleAmpl];
        
        [[self containerView] addSubview:labelSchedule];
        
        lastHeight = CGRectGetMaxY([labelSchedule frame]);
    }
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] initWithArray:textFieldArray_];
    
    [continueButton_ setEnabled:TRUE];
    
    CGRect frame = continueButton_.frame;
    frame.origin.y = lastHeight + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
    continueButton_.frame = frame;
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseCardContStepOneViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
    [header_ setCompanyName:[[responseInformation_.institutionName lowercaseString] capitalizedString]];
    //[self relocateViews];
	
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
	
}

/*
 * Creates and returns an autoreleased InstitutionsAndCompaniesViewController constructed from a NIB file.
 */

+ (InsititutionAndCompaniesStepTwoViewController *)insititutionAndCompaniesStepTwoViewController
{
    InsititutionAndCompaniesStepTwoViewController *result =  [[[InsititutionAndCompaniesStepTwoViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark TextField Delegate

/**
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
	return YES;
}

/**
 * Asks the delegate if the specified text should be changed.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
	BOOL result = YES;
    
    NSArray *fieldArray = [[responseInformation_ fieldList] fieldList];
    Field *field = [fieldArray objectAtIndex:[textField tag]];
	
    if ([textField.text length] + [string length] > [[field logiField] intValue] && range.length == 0)
    {
        return NO;
    }
    
    if ([@"a" isEqualToString:[[field validationType] lowercaseString]]) {
        result = [Tools isValidText:string forCharacterString:REFERENCE_VALID_CHARACTERS];
        
    } else if ([@"n" isEqualToString:[[field validationType] lowercaseString]]){
        NSCharacterSet *numbersSet = [NSCharacterSet decimalDigitCharacterSet];
		result = [Tools isValidText:string forCharacterSet:numbersSet];
    } else{
        
        NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                         withString:string];
        NSUInteger resultLength = [resultString length];
        
        if([@"m" isEqualToString:[[field validationType] lowercaseString]] ) {
            
            if (resultLength == 0) {
                
                result = YES;
                
            } else {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger > 0) {
                        
                        result = YES;
                    }
                }               
            }
        }
    }
    
	return result;
	
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self editableViewHasBeenClicked:textField];
}


#pragma mark -
#pragma mark User Interaction

- (IBAction)continueButtonTapped
{
    NXTTextField *codeMatTxt = (NXTTextField *)[textFieldArray_ objectAtIndex:0];
    
    NSString *codeMat = codeMatTxt.text;
    
    NSString *arrayCodes = @"";
    
    BOOL isEmptyFild = FALSE;
    
    NSString *emptyFieldMessage = @"";
    
    int i = 1;
    
    for (UITextField *textFieldCode in textFieldArray_) {
        
        NSArray *fieldArray = [[responseInformation_ fieldList] fieldList];
        Field *field = [fieldArray objectAtIndex:i-1];
        
        if (textFieldCode.text == nil || [@"" isEqualToString:textFieldCode.text])
        {

            NSString *auxMessage = [field.description lowercaseString];
            
            emptyFieldMessage = [auxMessage stringByReplacingOccurrencesOfString:@"ingresa" withString:@"Ingresar"];
            
            isEmptyFild = TRUE;
            break;
        }
        
        NSString *textfieldText = [textFieldCode text];
        
        if ([@"m" isEqualToString:[[field validationType] lowercaseString]]) {
            textfieldText = [Tools formatAmountWithDotDecimalSeparator:textfieldText];
        }
        
        arrayCodes = [arrayCodes stringByAppendingString:textfieldText];
        
        if (i != [textFieldArray_ count]) {
            
            arrayCodes = [arrayCodes stringByAppendingString:@"$"];
        }
        
        i++;
        
    }

    if([emptyFieldMessage length]>0)
    {
        emptyFieldMessage = [emptyFieldMessage stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[emptyFieldMessage substringToIndex:1] uppercaseString]];
    }
    
    if (isEmptyFild)
    {
        if ([textFieldArray_ count] > 1) {
            [Tools showInfoWithMessage:NSLocalizedString(@"Error en llenar los datos", nil)];
        }else
            [Tools showInfoWithMessage:NSLocalizedString(emptyFieldMessage, nil)];
        
        return;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pendingPaysInformationResponse:) name:kNotificationPaymentInstitutionsPendingPaysDetailResult object:nil];
    
    [[self appDelegate] showActivityIndicator:poai_Both];
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setObject:codeMat forKey:@"CodMatri"];
    [user setObject:arrayCodes forKey:@"arreglo"];
    [user synchronize];
    
    [[Updater getInstance] obtainInstitutionsAndCompaniesPendingPaysForCode:codeMat
                                                                  arrayLong:[NSString stringWithFormat:@"%d", [textFieldArray_ count]]
                                                                    array:arrayCodes
                                                                titlesArray:responseInformation_.arrayTitle
                                                             validationData:responseInformation_.dataValidation
                                                                 flagModule:responseInformation_.moduleFlag];
}

#pragma mark -
#pragma mark notification response
- (void)pendingPaysInformationResponse:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationPaymentInstitutionsPendingPaysDetailResult object:nil];
    
    [[self appDelegate] hideActivityIndicator];
    
    paymentInstitutionAndCompaniesConfirmationResponse_ = [notification object] ;
    
    if (!paymentInstitutionAndCompaniesConfirmationResponse_.isError)
    {
        institutionsAndCompaniesPaymentViewController_ = [[InstitutionsAndCompaniesStepThirdViewController institutionsAndCompaniesStepThirdViewController] retain];
        
        [institutionsAndCompaniesPaymentViewController_ setPaymentDetailEntity:paymentInstitutionAndCompaniesConfirmationResponse_];
        Ocurrency *auxOcurrency=(Ocurrency*)[[paymentInstitutionAndCompaniesConfirmationResponse_.ocurrencyList ocurrencyList] objectAtIndex:0];
        //NSLog(@"%@",auxOcurrency.value);
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        [user setObject:auxOcurrency.value forKey:@"CodMatri"];
        [user synchronize];
        
        [institutionsAndCompaniesPaymentViewController_ setServiceType:serviceType_];
        [institutionsAndCompaniesPaymentViewController_ resetInformation];
        
        [self.navigationController pushViewController:institutionsAndCompaniesPaymentViewController_ animated:TRUE];
      
    }
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    
    if ([@"SERV" isEqualToString:responseInformation_.payTypeCode] || [[@"Pago de servicios" lowercaseString] isEqualToString:[responseInformation_.payType lowercaseString]]) {
        result.customTitleView.topLabelText = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil),
                                               NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY, nil)];
    }else{
        result.customTitleView.topLabelText = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil),[responseInformation_.payType lowercaseString]];
    }
    
    return result;
    
}





@end
