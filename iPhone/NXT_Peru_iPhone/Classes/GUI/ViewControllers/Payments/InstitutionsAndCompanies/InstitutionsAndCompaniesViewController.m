//
//  InstitutionsAndCompaniesViewController.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 10/29/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "InstitutionsAndCompaniesViewController.h"

#import "Card.h"
#import "CardContOwnCardViewController.h"
#import "CardContThirdCardViewController.h"
#import "CheckCardCell.h"
#import "CheckComboCell.h"
#import "GlobalAdditionalInformation.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKDoubleLabel.h"
#import "MOKEditableViewController+protected.h"
#import "MOKLimitedLengthTextField.h"
#import "MOKNavigationItem.h"
#import "MOKStringListSelectionButton.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentCell.h"
#import "PaymentBaseProcess.h"
#import "PaymentCardProcess.h"
#import "PaymentCCOwnCardProcess.h"
#import "PaymentCCThirdCardProcess.h"
#import "Session.h"
#import "SimpleHeaderView.h"
#import "StatusEnabledResponse.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "Updater.h"
#import "NXTEditableViewController+protected.h"
#import "MOKEditableViewController.h"

#import "EmailCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "TransfersStepOneViewController+protected.h"
#import "NXTComboButton.h"
#import "NXTEditableViewController+protected.h"
#import "NXTSwitchCell.h"
#import "NXTTextField.h"
#import "NXTTextView.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "SMSCell.h"
#import "Tools.h"
#import "TermsAndConsiderationsViewController.h"
#import "UIColor+BBVA_Colors.h"

#import "GroupList.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "InstitutionList.h"
#import "Institution.h"
#import "InsititutionAndCompaniesStepTwoViewController.h"
#import "Group.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Updater.h"
#import "PaymentInstitutionsAndCompaniesInitialResponse.h"
#import "PaymentInstitutionsAndCompaniesDetailResponse.h"

#pragma mark -
#pragma mark Static attributes

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"InstitutionsAndCompaniesViewController"

/**
 * Defines the max number of chars in each text field
 */
#define MAX_CHARS                                                   4



@interface InstitutionsAndCompaniesViewController ()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releaseCardContStepOneViewControllerGraphicElements;

/**
 * Place the graphic elements
 */
- (void)relocateViews;

@end


@implementation InstitutionsAndCompaniesViewController

#pragma mark -
#pragma mark Properties

@synthesize table = table_;
@synthesize continueButton = continueButton_;
@synthesize brandingLine = brandingLine_;
@synthesize cardsArray = cardsArray_;
@synthesize groupCombo = groupCombo_;
@synthesize searchByNameTextField=searchByNameTextField_;
@synthesize buttonSearch=buttonSearch_;
@synthesize tableSearch=tableSearch_;
@synthesize responseInformation = responseInformation_;
@synthesize institutionsArray = institutionsArray_;
@synthesize paymentInstitutionsAndCompaniesSearchResponse = paymentInstitutionsAndCompaniesSearchResponse_;
@synthesize noResultLabel=noResultLabel;

- (IBAction)groupComboPressed
{
    [self editableViewHasBeenClicked:groupCombo_];
}


#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseCardContStepOneViewControllerGraphicElements];
    
    [cardsArray_ release];
    cardsArray_ = nil;
    
    [cardNamesArray_ release];
    cardNamesArray_ = nil;
    
    [cardContOwnCardViewController_ release];
    cardContOwnCardViewController_ = nil;
    
    [cardContThirdCardViewController_ release];
    cardContThirdCardViewController_ = nil;
    
    [searchByNameTextField_ release];
    searchByNameTextField_=nil;
    
    [groupCombo_ release];
    groupCombo_ = nil;
    
    [process_ release];
    process_ = nil;
    
    [institutionsArray_ release];
    institutionsArray_ = nil;
    
    [tableSearch_ release];
    tableSearch_ = nil;
    
    [noResultLabel_ release];
    noResultLabel_=nil;
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseCardContStepOneViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseCardContStepOneViewControllerGraphicElements {
    
    [header_ release];
    header_ = nil;
    
	[table_ release];
    table_ = nil;
    
    [checkCell_ release];
    checkCell_ = nil;
    
    [continueButton_ release];
    continueButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    [NXT_Peru_iPhoneStyler styleBlueButton:buttonSearch_];
    cardsArray_ = [[NSMutableArray alloc] init];
    cardNamesArray_ = [[NSMutableArray alloc] init];
    
}

- (void)resetInformationPayment
{
    
    [institutionsArray_ removeAllObjects];
    [tableSearch_ reloadData];
    
    [searchByNameTextField_ setText:@""];
    [searchByNameTextField_ resignFirstResponder];
    
    [groupCombo_ setSelectedIndex:-1];
    
}
#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleTableView:table_];
    
    [NXT_Peru_iPhoneStyler styleTableView:tableSearch_];
    
    [tableSearch_ setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:buttonSearch_];
    ImagesCache *imagesCache = [ImagesCache getInstance];
    UIImage *image = [imagesCache imageNamed:TEXT_FIELD_ICON_SEARCH_WHITE];
    
    [buttonSearch_ setImage:image forState:UIControlStateNormal];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:continueButton_];
    [continueButton_ setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil)
                     forState:UIControlStateNormal];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    [NXT_Peru_iPhoneStyler styleTextField:searchByNameTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    [searchByNameTextField_ setDelegate:self];
    [searchByNameTextField_ setReturnKeyType:UIReturnKeyDone];
    [searchByNameTextField_ setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    [popButtonsView_ enableNextResponderButton:FALSE];
    [popButtonsView_ enablePreviousResponderButton:FALSE];
    
    header_ = [[SimpleHeaderView simpleHeaderView] retain];
    [header_ setTitle:NSLocalizedString(PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT_KEY, nil)];
    [self.view addSubview:header_];
    
    [groupCombo_ setTitle:NSLocalizedString(INSTITUTIONS_SELECT_GROUP_KEY, nil)];
    groupsArray = [[NSArray alloc] initWithArray:[[responseInformation_ groupList] groupList]];
    
    NSMutableArray *stringArray = [[NSMutableArray alloc] init];
    
    for (Group *aGroup in groupsArray) {
        
        if(aGroup.description)
            [stringArray addObject:aGroup.description];
    }
    
    groupCombo_.stringsList = stringArray;
    [groupCombo_ setDelegate:self];
    [groupCombo_ setInputAccessoryView:popButtonsView_];
    [groupCombo_ setHidden:YES];
    table_.scrollEnabled = NO;
    
    isSearchField = TRUE;
    currentPage = 0;
    editableViews_ = [[NSMutableArray alloc] initWithObjects: nil];
    
    [editableViews_ addObject:groupCombo_];

    [[popButtonsView_ nextButton] setEnabled:FALSE];
    [[popButtonsView_ previousButton] setEnabled:FALSE];
    [popButtonsView_ enableNextResponderButton:FALSE];
    [popButtonsView_ enablePreviousResponderButton:FALSE];
    [[popButtonsView_ nextButton] removeFromSuperview];
    [[popButtonsView_ previousButton] removeFromSuperview];
    
    self.navigationController.title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil),
                                       NSLocalizedString(PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT_LOWER_KEY, nil)];
    
//    [self setScrollableView:self.view];
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [self setNoResultLabel:nil];
    [super viewDidUnload];
    
    [self releaseCardContStepOneViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];

    [self relocateViews];
	
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
	
}

/*
 * Creates and returns an autoreleased InstitutionsAndCompaniesViewController constructed from a NIB file.
 */
+ (InstitutionsAndCompaniesViewController *) institutionsAndCompaniesViewController {
    
    InstitutionsAndCompaniesViewController *result =  [[[InstitutionsAndCompaniesViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

/**
 * Place the graphic elements
 */
- (void)relocateViews {
    
    if(activeCellIndex_==0)
    {
        [searchByNameTextField_ setHidden:NO];
        [groupCombo_ setHidden:YES];
        isSearchField = TRUE;
    }
    if(activeCellIndex_==1)
    {
        [searchByNameTextField_ setHidden:YES];
        [searchByNameTextField_ setText:@""];
        isSearchField = FALSE;
        [groupCombo_ setHidden:NO];
    }
        
    
    CGFloat yPosition = 0.0f;
    
    CGRect frame = header_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    header_.frame = frame;

    
    yPosition = CGRectGetMaxY(frame);
    
    [table_ reloadData];
    
    frame = table_.frame;
    frame.origin.y = yPosition;
    frame.size.height = table_.contentSize.height;
    table_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 20.0f;
    
    frame = continueButton_.frame;
    frame.origin.y = yPosition;
    continueButton_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    //[self setScrollContentSize:CGSizeMake(320.0f, yPosition)];
    
}

#pragma mark -
#pragma mark UITableView methods

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView==table_){
        checkCell_ = [[CheckCell checkCell] retain];
        if(indexPath.row==0)
            [[checkCell_ topTextLabel] setText:NSLocalizedString(PAYMENT_SEARCH_BY_NAME_KEY, nil)];
        else
            [[checkCell_ topTextLabel] setText:NSLocalizedString(PAYMENT_SEARCH_BY_GROUP_KEY, nil)];
		
        [NXT_Peru_iPhoneStyler styleLabel:[checkCell_ topTextLabel] withFontSize:15.0f color:[UIColor BBVABlueColor]];
        
        [checkCell_ setCheckActive:(activeCellIndex_ == indexPath.row)];
        [checkCell_ setShowSeparator:YES];
        
        return checkCell_;
        
    } //institutions
    
    NSString *cellIdentifier = @"institutions";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        
        
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel  withBoldFontSize:14.0f color:[UIColor BBVABlueColor]];
        
    }
    
    Institution *aInstitution = [institutionsArray_ objectAtIndex:indexPath.row];
    
    cell.textLabel.text = aInstitution.institutionDescription;
    
    return cell;

        

}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == tableSearch_)
    {
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, 290, 20)];
           [NXT_Peru_iPhoneStyler styleLabel:titleLabel withBoldFontSize:14.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
    
        [titleLabel setText:NSLocalizedString(INSTITUTIONS_COMPANIES_LIST_KEY, nil)];
        UIView *viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 30)];
        [viewHeader setBackgroundColor:[UIColor whiteColor]];
        [viewHeader addSubview:titleLabel];
        
        return viewHeader;
    }
    else
    {
         return nil;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(tableView == tableSearch_)
    {
      
        return 30;
    }
    else
    {
    return 0;
    }
}
/**
 * Tells the delegate that the specified row is now selected
 */
- (void) tableView: (UITableView*) tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == table_) {
        activeCellIndex_ = indexPath.row;
        
        [self relocateViews];
    }else if (tableView == tableSearch_){
        
        Institution *institution = [institutionsArray_ objectAtIndex:indexPath.row];
        
        if (institution != nil) {
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(institutionDetailResponse:) name:kNotificationPaymentInstitutionsDetailResult object:nil];
            
            [[self appDelegate] showActivityIndicator:poai_Both];
            
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:institution.institutionParameter forKey:@"Parametros"];
            [userDefault setObject:institution.institutionIndicatorPF forKey:@"IndicatorPF"];
            [userDefault synchronize];
            
            
            [[Updater getInstance] obtainPaymentInstitutionsDetailForCod:institution.institutionParameter optionType:@"INST"];
        }
    }
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section {
    if (tableView == table_) {
        return 2;
    }else
        return [institutionsArray_ count];
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat result = 44.0f;
    
    return result;
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    
    if ((currentOffset > 0) && (maximumOffset - currentOffset) <= 10)
    {
        if(paymentInstitutionsAndCompaniesSearchResponse_ != nil)
        {
            [self loadNextInstitutionSearchPaged];
        }

    }
    else if(currentOffset<=0)
    {
        [self loadBackInstitutionSearchPaged];
    }
    
}

#pragma mark -
#pragma mark UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL result = TRUE;
    
    result = [Tools isValidText:string forCharacterString:REFERENCE_VALID_CHARACTERS];
    
    if(!result)
        return result;
    
    if ([textField.text length] + [string length] > 10 && range.length == 0)
    {
        result = NO;
    }
    
    return result;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

    [self.view setUserInteractionEnabled:TRUE];
    [buttonSearch_ setEnabled:TRUE];
    [self.view bringSubviewToFront:buttonSearch_];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    [self.view setCenter:CGPointMake(self.view.center.x, self.view.center.y + distance)];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [searchByNameTextField_ resignFirstResponder];
    return TRUE;
}

#pragma mark -
#pragma mark User interaction

/**
 * The search button has been tapped
 */
- (IBAction)searchButtonTapped {
    
    if (isSearchField) {
        
        if ([searchByNameTextField_.text length] < 4) {
            
            [Tools showInfoWithMessage:NSLocalizedString(PAYMENT_ERROR_MIN_CHARACTERS_TO_SEARCH_KEY, nil) ];
            return;
        }
        currentPage =0;
        [searchByNameTextField_ resignFirstResponder];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchListResponse:) name:kNotificationPaymentInstitutionsSearchListResult object:nil];
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        
        [[Updater getInstance] obtainInstitutionsAndCompaniesForEntity:searchByNameTextField_.text searchType:@"C" nextMovement:@"" indPag:@"" searchArg:searchByNameTextField_.text LastDescr:@"" button:@"siguiente" action:@"consultar"];
        
    }else{
        
         if(groupCombo_.selectedIndex>=[groupsArray count] || groupCombo_.selectedIndex<0)
            return;
        
        Group *aGroup;

        currentPage =0;
        for (Group *group in groupsArray) {
            if ([group.description isEqualToString:[groupCombo_.stringsList objectAtIndex:groupCombo_.selectedIndex]]) {
                aGroup = group;
            }
        }
        
        if (aGroup != nil) {
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchListResponse:) name:kNotificationPaymentInstitutionsSearchListResult object:nil];
            
            [[self appDelegate] showActivityIndicator:poai_Both];
            
            [[Updater getInstance] obtainInstitutionsAndCompaniesForEntity:aGroup.groupCod searchType:@"G" nextMovement:@"" indPag:@"" searchArg:aGroup.description LastDescr:@"" button:@"siguiente" action:@"consultar"];
            
        }
    }
    
}

#pragma mark -
#pragma mark MOKStringListSelectionButtonDelegate

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton {
    
    cardSelectedIndex_ = [stringListSelectionButton selectedIndex];
    
}

#pragma mark -
#pragma mark PaymentBaseProcessDelegate

/**
 * Delegate is notified when the data analysis has finished
 */
- (void)dataAnalysisHasFinished {
    
    if (activeCellIndex_ == 0) {
        
        if (cardContOwnCardViewController_ == nil) {
            
            cardContOwnCardViewController_ = [[CardContOwnCardViewController cardContOwnCardViewController] retain];
            
        }
        
//        hasNavigateForward_ = YES;
        
        [cardContOwnCardViewController_ setProcess:process_];
        [cardContOwnCardViewController_ setHasNavigateForward:NO];
        
        [[self navigationController] pushViewController:cardContOwnCardViewController_
                                               animated:YES];
        
    } else if (activeCellIndex_ == 1) {
        
        if (cardContThirdCardViewController_ == nil) {
            
            cardContThirdCardViewController_ = [[CardContThirdCardViewController cardContThirdCardViewController] retain];
            
        }
        
//        hasNavigateForward_ = YES;
        [cardContThirdCardViewController_ setProcess:process_];
        [cardContThirdCardViewController_ setHasNavigateForward:NO];
        
        [[self navigationController] pushViewController:cardContThirdCardViewController_
                                               animated:YES];
        
    }
    
}

#pragma mark -
#pragma mark Notification
/**
 * Search list response.
 *
 * @param notification: data send by the notification
 * @private
 */
- (void)searchListResponse:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationPaymentInstitutionsSearchListResult object:nil];
    
    PaymentInstitutionsAndCompaniesInitialResponse *response = [notification object];
    
    [[self appDelegate] hideActivityIndicator];
    

    self.paymentInstitutionsAndCompaniesSearchResponse = response;
    
    
    if (response != nil && [response isKindOfClass:[PaymentInstitutionsAndCompaniesInitialResponse class] ]
        && !response.isError) {
     
        InstitutionList *institutionList = [response institutionList];
        
        if (institutionsArray_ == nil) {
            institutionsArray_ = [[NSMutableArray alloc] init];
        }
        
        [institutionsArray_ removeAllObjects];
        [institutionsArray_ addObjectsFromArray:[institutionList institutionList]];
        
        [tableSearch_ reloadData];
        if ([institutionsArray_ count]==0){
            noResultLabel.hidden=FALSE;
        }else{  
            noResultLabel.hidden=TRUE;
        }
    }
    
    [tableSearch_ scrollRectToVisible:CGRectMake(0, 0, 320, CGRectGetHeight([tableSearch_ frame])) animated:FALSE];
}

-(void)loadBackInstitutionSearchPaged
{
   
    if(![self.paymentInstitutionsAndCompaniesSearchResponse backNextMovement])
        return;
    
    if (isSearchField) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchListResponse:) name:kNotificationPaymentInstitutionsSearchListResult object:nil];
        [[self appDelegate] showActivityIndicator:poai_Both];
        
      
        
        if(currentPage<=0)
        {
            return;
        }
        
        currentPage--;
        
        [[Updater getInstance] obtainInstitutionsAndCompaniesForEntity:searchByNameTextField_.text searchType:@"C" nextMovement:[self.paymentInstitutionsAndCompaniesSearchResponse backNextMovement]indPag:[NSString stringWithFormat:@"%i",currentPage] searchArg:searchByNameTextField_.text LastDescr:[self.paymentInstitutionsAndCompaniesSearchResponse backLastDescription] button:@"atras" action:@"masDatos"];
        
        
    }else{
        

        
        Group *aGroup;
        
        for (Group *group in groupsArray) {
            if ([group.description isEqualToString:[groupCombo_.stringsList objectAtIndex:groupCombo_.selectedIndex]]) {
                aGroup = group;
            }
        }
        
        if (aGroup != nil) {
            if(currentPage<=0)
            {
                return;
            }
            
            currentPage--;
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchListResponse:) name:kNotificationPaymentInstitutionsSearchListResult object:nil];
            
            [[self appDelegate] showActivityIndicator:poai_Both];
            
            [[Updater getInstance] obtainInstitutionsAndCompaniesForEntity:aGroup.groupCod searchType:@"G" nextMovement:[self.paymentInstitutionsAndCompaniesSearchResponse backNextMovement] indPag:[NSString stringWithFormat:@"%i",currentPage] searchArg:aGroup.description LastDescr:[self.paymentInstitutionsAndCompaniesSearchResponse backLastDescription] button:@"atras" action:@"masDatos"];
        }
        
        
    }
}

-(void)loadNextInstitutionSearchPaged
{
    if(![self.paymentInstitutionsAndCompaniesSearchResponse nextMovement])
        return;
    
    if (isSearchField) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchListResponse:) name:kNotificationPaymentInstitutionsSearchListResult object:nil];
        [[self appDelegate] showActivityIndicator:poai_Both];

        currentPage++;
        
        [[Updater getInstance] obtainInstitutionsAndCompaniesForEntity:searchByNameTextField_.text searchType:@"C" nextMovement:[self.paymentInstitutionsAndCompaniesSearchResponse nextMovement]indPag:[NSString stringWithFormat:@"%i",currentPage] searchArg:searchByNameTextField_.text LastDescr:[self.paymentInstitutionsAndCompaniesSearchResponse lastDescription] button:@"siguiente" action:@"masDatos"];
        
    }else{
        Group *aGroup;
        
        for (Group *group in groupsArray) {
            if ([group.description isEqualToString:[groupCombo_.stringsList objectAtIndex:groupCombo_.selectedIndex]]) {
                aGroup = group;
            }
        }
        
        if (aGroup != nil) {
            currentPage++;
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchListResponse:) name:kNotificationPaymentInstitutionsSearchListResult object:nil];
            
            [[self appDelegate] showActivityIndicator:poai_Both];
     
            [[Updater getInstance] obtainInstitutionsAndCompaniesForEntity:aGroup.groupCod searchType:@"G" nextMovement:[self.paymentInstitutionsAndCompaniesSearchResponse nextMovement] indPag:[NSString stringWithFormat:@"%i",currentPage] searchArg:aGroup.description LastDescr:[self.paymentInstitutionsAndCompaniesSearchResponse lastDescription] button:@"siguiente" action:@"masDatos"];
        }
        
     
    }
}


/**
 * Search list response.
 *
 * @param notification: data send by the notification
 * @private
 */
- (void)institutionDetailResponse:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationPaymentInstitutionsDetailResult object:nil];
    
    PaymentInstitutionsAndCompaniesDetailResponse *response = [notification object];
    
    [[self appDelegate] hideActivityIndicator];
    
    if (response !=nil && [response isKindOfClass:[PaymentInstitutionsAndCompaniesDetailResponse class]] && !response.isError) {
        
        if (institutionsAndCompaniesDetailViewController_ != nil) {
            [institutionsAndCompaniesDetailViewController_ release];
            institutionsAndCompaniesDetailViewController_ = nil;
        }
        institutionsAndCompaniesDetailViewController_ = [[InsititutionAndCompaniesStepTwoViewController insititutionAndCompaniesStepTwoViewController] retain];
        
        institutionsAndCompaniesDetailViewController_.responseInformation = response;
        
        [self.navigationController pushViewController:institutionsAndCompaniesDetailViewController_ animated:TRUE];
    }

}

#pragma mark -
#pragma mark Setters

/**
 * Sets the cards array
 */
- (void)setCardsArray:(NSArray *)cardsArray {
    
    [cardsArray_ removeAllObjects];
    [cardsArray_ addObjectsFromArray:cardsArray];
    
    [cardNamesArray_ removeAllObjects];
    NSString *cardName;
    
    for (Card *card in cardsArray_) {
        
        cardName = [card cardListName];
        [cardNamesArray_ addObject:cardName];
        
    }
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    
    result.customTitleView.topLabelText = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil),
                        NSLocalizedString(PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT_LOWER_KEY, nil)];
    
    return result;
    
}

@end
