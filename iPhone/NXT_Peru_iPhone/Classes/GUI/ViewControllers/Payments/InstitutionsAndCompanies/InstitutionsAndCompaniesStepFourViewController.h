//
//  InstitutionsAndCompaniesStepFourViewController.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 28/11/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "MOKEditableViewController.h"
#import "NXTTransfersViewController.h"
#import "LegalTermsTopCell.h"

@class TermsAndConsiderationsViewController;
@class CoordKeyCell;
@class OpKeyCell;
@class LegalTermsTopCell;
@class TransfersStepThreeViewController;
@class PaymentInstitutionAndCompaniesConfirmationInformationResponse;
@class PaymentInstitutionAndCompaniesSuccessConfirmationResponse;
@class InstitutionAndCompaniesFinalViewController;

@interface InstitutionsAndCompaniesStepFourViewController : NXTTransfersViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate>{
    
@private
    
    /**
     * Accept button
     */
    UIButton *acceptButton_;
    
    /**
     * Coordinates key cell
     */
    CoordKeyCell *coordKeyCell_;
    
    /**
     * OTP key cell
     */
    OpKeyCell *otpKeyCell_;
    
    /**
     * Transfers Step Three View Controller
     */
    TransfersStepThreeViewController *transfersStepThreeViewController_;
    
    /**
     * Transfers Step Final View Controller
     */
    InstitutionAndCompaniesFinalViewController *transfersFinalViewController_;
    
#pragma Logic
    /**
     * mutable Array key labels first section
     */
    NSArray *keyMutableArray_;
    
    /**
     * mutable Array value labels first section
     */
    NSArray *valueMutableArray_;
    
    /**
     * information from the response service
     */
    PaymentInstitutionAndCompaniesConfirmationInformationResponse *responseInformation_;
    
    /**
     * information from the success response service
     */
    PaymentInstitutionAndCompaniesSuccessConfirmationResponse *succesResponseInformation_;
    
    BOOL isWithBD_;
    
    BOOL isPartial_;
    
    /**
     * Service name
     */
    NSString *serviceType_;
}

/**
 * Provides readwrite access to the service type name
 */
@property (nonatomic, readwrite, retain) NSString *serviceType;

/**
 * Provides readwrite access to the acceptButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *acceptButton;

/**
 * Information to fill the current view
 */
@property (nonatomic, readwrite, retain) PaymentInstitutionAndCompaniesConfirmationInformationResponse *responseInformation;

/**
 * Information to fill the last view
 */
@property (nonatomic, readwrite, retain) PaymentInstitutionAndCompaniesSuccessConfirmationResponse *succesResponseInformation;

@property (nonatomic, readwrite, assign) BOOL isWithBD;

@property (nonatomic, readwrite, assign) BOOL isPartial;

/**
 * titles for the table view
 */
@property (nonatomic,retain) NSArray *keyMutableArray;

/**
 * detail for each row in the table
 */
@property (nonatomic,retain) NSArray *valueMutableArray;

/**
 * Creates and returns an autoreleased TransfersStepTwoViewController constructed from a NIB file
 *
 * @return The autoreleased TransfersStepTwoViewController constructed from a NIB file
 */
+ (InstitutionsAndCompaniesStepFourViewController *)institutionsAndCompaniesStepFourViewController;


/**
 * The acceptButton has been tapped
 */
- (IBAction)acceptButtonTapped;


@end
