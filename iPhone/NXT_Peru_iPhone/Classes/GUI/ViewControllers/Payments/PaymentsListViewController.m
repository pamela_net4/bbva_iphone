/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PaymentsListViewController.h"

#import "AccountList.h"
#import "BankAccount.h"
#import "Card.h"
#import "CardList.h"
#import "CardPaymentListViewController.h"
#import "GlobalAdditionalInformation.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "InstitutionsAndCompaniesViewController.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentCell.h"
#import "PaymentRechargeProcess.h"
#import "PaymentInstitutionsAndCompaniesInitialResponse.h"
#import "PublicServListViewController.h"
#import "RechargeViewController.h"
#import "ServiceList.h"
#import "ServiceResponse.h"
#import "Session.h"
#import "StatusEnabledResponse.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "Updater.h"
#import "UIColor+BBVA_Colors.h"
#import "InstitutionsAndCompaniesViewController.h"
#import "InternetShoppingStepOneViewController.h"
#import "PaymentISSafetyPayProcess.h"
#import "FrequentOperationConstants.h"

#pragma mark -
#pragma mark Static attributes
/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"PaymentsListViewController"

/**
 * GlobalPositionViewController private extension
 */
@interface PaymentsListViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releasePaymentsListViewControllerGraphicElements;

/**
 * Receives the response of the public services startup
 */
- (void)publicServicesStartupReceived:(NSNotification *)notification;

/**
 * Receives the response of the institutions and companies startup
 */
- (void)institutionsAndCompaniesStartupReceived:(NSNotification *)notification;

@end

#pragma mark -


@implementation PaymentsListViewController


#pragma mark -
#pragma mark Properties

@synthesize table = table_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releasePaymentsListViewControllerGraphicElements];
    
    [publicServListViewController_ release];
    publicServListViewController_ = nil;
    
    [cardPaymentListViewController_ release];
    cardPaymentListViewController_ = nil;
    
    [rechargeViewController_ release];
    rechargeViewController_ = nil;
    
    [institutionsAndCompaniesViewController_ release];
    institutionsAndCompaniesViewController_ = nil;
    
    [process_ release];
    process_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasePaymentsListViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releasePaymentsListViewControllerGraphicElements {
    
	[table_ release];
    table_ = nil;
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        self.navigationController.navigationBar.tintColor = [UIColor BBVABlueSpectrumColor];
        
    } else
    {
        self.navigationController.navigationBar.barTintColor = [UIColor BBVABlueSpectrumColor];
        self.navigationController.navigationBar.translucent = NO;
        
    }
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
    [NXT_Peru_iPhoneStyler styleTableView:table_];
    
    table_.scrollEnabled = NO;    
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releasePaymentsListViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
        
    [table_ reloadData];
    [NXT_Peru_iPhoneStyler styleNXTView:table_];
    [Tools checkTableScrolling:table_];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self selector:@selector(publicServicesStartupReceived:) name:kNotificationPaymentServicesListResultEnds object:nil];
	
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:YES animated:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    NSNotificationCenter *notifCenter = [NSNotificationCenter defaultCenter];
    
    [notifCenter removeObserver:self name:kNotificationPaymentServicesListResultEnds object:nil];
    [notifCenter removeObserver:self name:kNotificationPaymentRechargesInitialResultEnds object:nil];
	[notifCenter removeObserver:self name:kNotificationTransferToAccountFromOtherBanksStartupEnds object:nil];
	
}

/*
 * Creates and returns an autoreleased PaymentsListViewController constructed from a NIB file.
 */
+ (PaymentsListViewController *)paymentsListViewController {
    
    PaymentsListViewController *result =  [[[PaymentsListViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark UITableView methods

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PaymentCell *result = (PaymentCell *)[tableView dequeueReusableCellWithIdentifier:[PaymentCell cellIdentifier]];
    
    if (result == nil) {
        result = [PaymentCell paymentCell];
    }
        
    switch (indexPath.row) {
            
        case 0:
            [result.titleLabel setText:NSLocalizedString(INTERNET_SHOPPING_GROUP_KEY, nil)];
            break;
            
        case 1:
            [result.titleLabel setText:NSLocalizedString(PAYMENT_INSTITUTIONS_AND_COMPANIES_TITLE_TEXT_KEY, nil)];
            break;
            
        case 2:
            [result.titleLabel setText:NSLocalizedString(PAYMENT_RECHARGES_TITLE_TEXT_KEY, nil)];
            break;
            
        case 3:
            [result.titleLabel setText:NSLocalizedString(PAYMENT_PUBLIC_SERVICES_HEADER_TITLE_TEXT_KEY, nil)];
            break;
            
        case 4:
            [result.titleLabel setText:NSLocalizedString(CARDS_PRODUCT_GROUP_KEY, nil)];
            break;
        
        default:
            break;                  
    }
    
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    [result.imageView setImage:[imagesCache imageNamed:PAYMENT_LIST_ICON_IMAGE_FILE_NAME]];
    
    result.selectionStyle = UITableViewCellSelectionStyleGray;
    [result setShowDisclosureArrow:YES];
    [result setShowSeparator:YES];
    result.backgroundColor = [UIColor clearColor];
    return result;
    
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void) tableView: (UITableView*) tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSArray *accountsList = [[[Session getInstance] accountList] accountList];
    
    BOOL isValid = NO;
    BOOL otpActive = [[[Session getInstance] additionalInformation] otpActive];
    
    for (BankAccount *account in accountsList) {
                
        if (([CHECKING_ACCOUNT_TYPE isEqualToString:[account type]] || [SAVINGS_ACCOUNT_TYPE isEqualToString:[account type]])) {
            
            isValid = YES;
            
            break;
            
        }
        
    }
    
    if (isValid) {
        
        switch (indexPath.row) {
                
            case 0: {
                
                if(internetShoppingViewController_ == nil){
                    
                    [internetShoppingViewController_ release];
                    internetShoppingViewController_ = nil;
                    
                    internetShoppingViewController_ = [[InternetShoppingStepOneViewController internetShoppingStepOneViewController] retain];
                }
                
                PaymentISProcess *paymentISProcess = [[[PaymentISSafetyPayProcess alloc] init] autorelease];
                
                [internetShoppingViewController_ setProcess:paymentISProcess];
                
                [self.navigationController pushViewController:internetShoppingViewController_ animated:YES];
                
                break;
            }
            
            case 1: {
                if(institutionsAndCompaniesViewController_ != nil)
                {
                    [institutionsAndCompaniesViewController_ release];
                    institutionsAndCompaniesViewController_ = nil;             
                }

                institutionsAndCompaniesViewController_ = [[InstitutionsAndCompaniesViewController institutionsAndCompaniesViewController] retain];
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(institutionsAndCompaniesStartupReceived:) name:kNotificationPaymentInstitutionsListResultEnds object:nil];
                
                [[self appDelegate] showActivityIndicator:poai_Both];
                [[Updater getInstance] obtainPaymentInstitutionsAndCompanies];
                

                
                break;
                
            }
                
            case 2: {
                
                if (otpActive) {
                    
                    if (rechargeViewController_ == nil) {
                        
                        rechargeViewController_ = [[RechargeViewController rechargeViewController] retain];
                        
                    }
                    
                    [rechargeViewController_ setHasNavigateForward:NO];
                    
                    process_ = [[[PaymentRechargeProcess alloc] init] autorelease];
                    
                    [rechargeViewController_ setProcess:process_];
                    
                    [self.navigationController pushViewController:rechargeViewController_ animated:YES];
                    
                } else {
                    
                    [Tools showAlertWithMessage:NSLocalizedString(OTP_SERVICE_NO_ACTIVE_TEXT_KEY, nil)];
                    
                }
                
                break;
                
            }
            case 3: {
                
                if (otpActive) {
                    
                    [[self appDelegate] showActivityIndicator:poai_Both];
                    [[Updater getInstance] obtainPaymentPublicServices];
                    
                } else {
                    
                    [Tools showAlertWithMessage:NSLocalizedString(OTP_SERVICE_NO_ACTIVE_TEXT_KEY, nil)];
                    
                }
                
                break;
                
            }
            case 4: {
                
                if (cardPaymentListViewController_ == nil) {
                    
                    cardPaymentListViewController_ = [[CardPaymentListViewController cardPaymentListViewController] retain];
                }
                
                [self.navigationController pushViewController:cardPaymentListViewController_ animated:YES];
                break;
                
            }
                
            default: {
                
                break;
                
            }
                
        }
        
    } else {
        
        switch ([indexPath row]) {
                
            case 0:
                
                [Tools showErrorWithMessage:NSLocalizedString(PAYMENT_ERROR_CANT_ACCESS_INTERNET_SHOPPING_TEXT_KEY, nil)];
                
                break;
                
            case 1:
                
                [Tools showErrorWithMessage:NSLocalizedString(PAYMENT_ERROR_CANT_ACCESS_RECHARGES_TEXT_KEY, nil)];
                
                break;
                
            case 2:
                
                [Tools showErrorWithMessage:NSLocalizedString(PAYMENT_ERROR_CANT_ACCESS_PUBLIC_SERVICES_TEXT_KEY, nil)];
                
                break;
                
            case 3:
                
                [Tools showErrorWithMessage:NSLocalizedString(PAYMENT_ERROR_CANT_ACCESS_CARD_PAYMENTS_TEXT_KEY, nil)];
                
                break;
                
            default:
                break;
        }
        
    }
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section {
    return 5;
}

/**
 
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [PaymentCell cellHeight];
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [self customNavigationItem];
    
    [[result customTitleView] setTopLabelText:NSLocalizedString(PAYMENT_TITLE_TEXT_KEY, nil)];
    
    return result;
    
}


#pragma mark -
#pragma mark Notifications

/**
 * Receives the response of the public services startup
 */
- (void)publicServicesStartupReceived:(NSNotification *)notification {
    [self.appDelegate hideActivityIndicator];
    ServiceResponse *response = [notification object];
    
    if (!response.isError) {
        
        NSArray *serviceList = [[response serviceList] serviceList];
        
        if (publicServListViewController_ == nil) {
            
            publicServListViewController_ = [[PublicServListViewController publicServListViewController] retain];
        }
        
        [publicServListViewController_ setServicesList:serviceList];
                
        [self.navigationController pushViewController:publicServListViewController_ animated:YES];
    }
    
}

/**
 * Receives the response of the institutions and companies startup
 */
- (void)institutionsAndCompaniesStartupReceived:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationPaymentInstitutionsListResultEnds object:nil];
    
    PaymentInstitutionsAndCompaniesInitialResponse *response = [notification object];
    
    [self.appDelegate hideActivityIndicator];
    
    if (response !=nil &&
        ([response errorMessage] == nil || [@"" isEqualToString:[response errorMessage]]) &&
        [response isKindOfClass:[PaymentInstitutionsAndCompaniesInitialResponse class]] && ![response isError]) {
        
        if (institutionsAndCompaniesViewController_ == nil) {
            
            institutionsAndCompaniesViewController_ = [[InstitutionsAndCompaniesViewController institutionsAndCompaniesViewController] retain];
            
        }
        
        [institutionsAndCompaniesViewController_ resetInformationPayment];
        
        [institutionsAndCompaniesViewController_ setHasNavigateForward:NO];
        
        [institutionsAndCompaniesViewController_ setResponseInformation:response];
        
        [self.navigationController pushViewController:institutionsAndCompaniesViewController_ animated:YES];
    }
    
}


#pragma mark -
#pragma mark PaymentBaseProcessDelegate

/*
 * Delegate is notified when the data analysis has finished
 */
- (void)dataAnalysisHasFinished {
    
    if (rechargeViewController_ == nil) {
        
        rechargeViewController_ = [[RechargeViewController rechargeViewController] retain];
        
    }
    
    [rechargeViewController_ setHasNavigateForward:NO];
    [rechargeViewController_ setProcess:process_];
    
    [self.navigationController pushViewController:rechargeViewController_ animated:YES];
}


@end
