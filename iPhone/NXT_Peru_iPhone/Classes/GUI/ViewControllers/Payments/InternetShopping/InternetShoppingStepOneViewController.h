//
//  InternetShoppingViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOKEditableViewController.h"
#import "MOKStringListSelectionButton.h"
#import "PaymentISProcess.h"

@class InternetShoppingStepTwoViewController;
@class SimpleHeaderView;
@class Service;

@interface InternetShoppingStepOneViewController : MOKEditableViewController   <MOKStringListSelectionButtonDelegate, PaymentISProcessDelegate>{

    @private
    
    SimpleHeaderView *header_;
    
    UILabel *titleLabel_;
    
    MOKStringListSelectionButton *companyComboButton_;
    
    UIButton *continueButton_;
    
    UIImageView *brandingLine_;
    
    NSString *companySelected_;
    
    NSMutableArray *companiesArray_;
    
    PaymentISProcess *process_;
    
    InternetShoppingStepTwoViewController *internetShoppingStepTwoViewController_;
    
}


@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *companyComboButton;

@property (nonatomic, readwrite, retain) IBOutlet UIButton *continueButton;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingLine;

@property (nonatomic, readwrite, retain) PaymentISProcess *process;

+ (InternetShoppingStepOneViewController *) internetShoppingStepOneViewController;

- (IBAction)continueButtonTapped;


@end
