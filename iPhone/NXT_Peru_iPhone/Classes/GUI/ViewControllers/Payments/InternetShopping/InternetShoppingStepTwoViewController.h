//
//  InternetShoppingStepTwoViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 3/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentBaseProcess.h"
#import "MOKEditableViewController.h"

@class MOKStringListSelectionButton;
@class MOKTextField;
@class PaymentISProcess;
@class PublicServiceHeaderView;
@class Service;
@class InternetShoppingStepThreeViewController;

@interface InternetShoppingStepTwoViewController : MOKEditableViewController <UITextFieldDelegate, PaymentBaseProcessDelegate>{
@private
    
    PublicServiceHeaderView *header_;
    
    UILabel *enteringDataDescriptionLabel_;
    
    UILabel *transactionTitleLabel_;
    
    MOKTextField *transactionTextField_;
    
    UILabel *amountTitleLabel_;
    
    MOKTextField *amountTextField_;
    
    UIButton *continueButton_;
    
    UIImageView *brandingLine_;
    
    Service *service_;
    
    NSString *serviceType_;
    
    BOOL hasNavigateForward_;
    
    PaymentISProcess *process_;
    
    InternetShoppingStepThreeViewController *internetShoppingStepThreeViewController_;
}

@property (nonatomic, readwrite, retain) IBOutlet UILabel *enteringDataDescriptionLabel;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *transactionTitleLabel;

@property (nonatomic, readwrite, retain) IBOutlet MOKTextField *transactionTextField;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *amountTitleLabel;

@property (nonatomic, readwrite, retain) IBOutlet MOKTextField *amountTextField;

@property (nonatomic, readwrite, retain) IBOutlet UIButton *continueButton;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingLine;

@property (nonatomic, readwrite, retain) Service *service;

@property (nonatomic, readwrite, copy) NSString *serviceType;

@property (nonatomic, readwrite, assign) BOOL hasNavigateForward;

@property (nonatomic, readwrite, retain) PaymentISProcess *process;

+ (InternetShoppingStepTwoViewController *) internetShoppingStepTwoViewController;

- (IBAction)continueButtonTapped;

@end
