//
//  InternetShoppingStepThreeViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 4/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentDataBaseViewController.h"
#import "MOKStringListSelectionButton.h"
#import "InternetShoppingHeaderView.h"
#import "MOKCurrencyTextField.h"
#import "PaymentISSafetyPayProcess.h"

@interface InternetShoppingStepThreeViewController : PaymentDataBaseViewController<MOKStringListSelectionButtonDelegate>{

@private
    
    SimpleHeaderView *firstViewHeader_;
    
    UIImageView *firstViewSeparator_;
    
    UIImageView *secondViewSeparator_;
    
    SimpleHeaderView *secondViewHeader_;
    
    MOKStringListSelectionButton *accountCombo_;
    
    NSMutableArray *labelsArray_;
    
    CGFloat firstViewHeight_;
    
    CGFloat secondViewHeight_;
    
    PaymentISProcess *processIS_;
    
}

@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *accountCombo;

+ (InternetShoppingStepThreeViewController *)internetShoppigStepThreeViewController;

@end
