//
//  InternetShoppingStepTwoViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 3/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "InternetShoppingStepTwoViewController.h"
#import "MOKEditableViewController+protected.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PublicServiceHeaderView.h"
#import "UIColor+BBVA_Colors.h"
#import "StringKeys.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "PaymentISProcess.h"
#import "Tools.h"
#import "UIView+Frame.h"
#import "MOKTextField.h"
#import "UINavigationItem+DoubleLabel.h"
#import "InternetShoppingStepThreeViewController.h"
#import "PaymentISSafetyPayProcess.h"

#define NIB_FILE_NAME  @"InternetShoppingStepTwoViewController"

@interface InternetShoppingStepTwoViewController ()

- (void) releaseInternetShoppingStepTwoViewControllerGraphicElements;

- (void) relocateView;

@end

@implementation InternetShoppingStepTwoViewController

@synthesize enteringDataDescriptionLabel = enteringDataDescriptionLabel_;
@synthesize transactionTitleLabel = transactionTitleLabel_;
@synthesize transactionTextField = transactionTextField_;
@synthesize amountTitleLabel = amountTitleLabel_;
@synthesize amountTextField = amountTextField_;
@synthesize continueButton = continueButton_;
@synthesize service = service_;
@synthesize serviceType = serviceType_;
@synthesize brandingLine = brandingLine_;
@synthesize hasNavigateForward = hasNavigateForward_;
@synthesize process = process_;

- (void) dealloc {
    
    [self releaseInternetShoppingStepTwoViewControllerGraphicElements];
    
    [service_ release];
    service_ = nil;
    
    [serviceType_ release];
    serviceType_ = nil;
    
    [process_ release];
    process_ = nil;
    
    [internetShoppingStepThreeViewController_ release];
    internetShoppingStepThreeViewController_ = nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseInternetShoppingStepTwoViewControllerGraphicElements];
        [self setView:nil];
        
    }
}

- (void) releaseInternetShoppingStepTwoViewControllerGraphicElements {
    
    [header_ release];
    header_ = nil;
    
    [transactionTitleLabel_ release];
    transactionTitleLabel_ = nil;
    
    [transactionTextField_ release];
    transactionTextField_ = nil;
    
    [amountTitleLabel_ release];
    amountTitleLabel_ = nil;
    
    [amountTextField_ release];
    amountTextField_ = nil;
    
    [continueButton_ release];
    continueButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
}

- (void) awakeFromNib {
    
    [super awakeFromNib];
    
}

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    CGRect mainFrame = [[UIScreen mainScreen] bounds];
    
    [[self view] setFrame:mainFrame];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
    header_ = [[PublicServiceHeaderView publicServiceHeaderView] retain];
    
    [[self scrollableView] addSubview:header_];
    
    [NXT_Peru_iPhoneStyler styleLabel:enteringDataDescriptionLabel_ withFontSize:12.0f color:[UIColor grayColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:transactionTitleLabel_ withFontSize: 14.0f color:[UIColor BBVABlueSpectrumColor]];
    
    [transactionTextField_ setDelegate : self];
    [transactionTextField_ setTextAlignment : UITextAlignmentLeft];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:transactionTextField_ withFontSize:14.0f andColor:[UIColor grayColor]];
    

    [NXT_Peru_iPhoneStyler styleLabel:amountTitleLabel_ withFontSize: 14.0f color:[UIColor BBVABlueSpectrumColor]];
    
    [amountTextField_  setDelegate : self];
    [amountTextField_ setTextAlignment : UITextAlignmentLeft];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:amountTextField_ withFontSize:14.0f andColor:[UIColor grayColor]];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:continueButton_];
    [continueButton_ setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil)
                     forState:UIControlStateNormal];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    CGRect frame = [[self view] frame];
    
    CGRect scrollFrame = [[self scrollableView] frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingLine] frame]) - CGRectGetMinY(frame);
    [self setScrollFrame:scrollFrame];
}

- (void) viewDidUnload {

    [super viewDidUnload];
    
    [self releaseInternetShoppingStepTwoViewControllerGraphicElements];
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.appDelegate setTabBarVisibility: NO animated:YES];
    
    NSString *title = @"";
    
    if([process_ paymentOperationType] == PTEPaymentISService){
        title = NSLocalizedString(INTERNET_SHOPPING_STEP_TWO_TITLE_TEXT_KEY, nil);
    }
    
    [header_ setTitle:title];
    [header_ setCompanyName: [process_ selectedCompany]];
    
    switch ([process_ paymentOperationType]) {
        case PTEPaymentISService:
            
            [transactionTitleLabel_ setText:[NSString stringWithFormat:@"%@",NSLocalizedString(INTERNET_SHOPPING_STEP_TWO_ENTER_TRANSACTION_NUMBER_KEY, nil)]];
    
            [amountTitleLabel_ setText:[NSString stringWithFormat:@"%@",NSLocalizedString(INTERNET_SHOPPING_STEP_TWO_ENTER_AMOUNT_KEY, nil)]];
    
            [enteringDataDescriptionLabel_ setText:NSLocalizedString(INTERNET_SHOPPING_STEP_TWO_ENTER_DATA_DESCRIP_KEY, nil)];
    
            break;
            
        default:
            break;
    }
    
    [self relocateView];
    
    [self lookForEditViews];
    
    if(process_ != nil){
        [process_ setDelegate:self];
    }
    
}

- (void) viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated: YES];
    
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [process_ setDelegate:nil];
    
    [transactionTextField_ setText:@""];
    [amountTextField_ setText:@""];
    
}

+ (InternetShoppingStepTwoViewController *) internetShoppingStepTwoViewController {
    
    InternetShoppingStepTwoViewController *result = [[[InternetShoppingStepTwoViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

- (void) dataAnalysisHasFinished {
    
    if (internetShoppingStepThreeViewController_ == nil) {
        
        internetShoppingStepThreeViewController_ = [[InternetShoppingStepThreeViewController internetShoppigStepThreeViewController] retain];
        
    }
    
    hasNavigateForward_ = YES;
    
    [internetShoppingStepThreeViewController_ setProcess: process_];
    [internetShoppingStepThreeViewController_ setHasNavigateForward:NO];
    
    [[self navigationController] pushViewController:internetShoppingStepThreeViewController_ animated:YES];
    
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
   
    return true;
}

- (IBAction)continueButtonTapped {

    [process_ setTransactionNumber: [transactionTextField_ text]];
    [process_ setAmountToPay: [amountTextField_ text]];
    [process_ startPaymentDataRequest];
    
}

- (void) relocateView {

    CGFloat yPosition = header_.frame.origin.y + header_.frame.size.height;
    
    CGRect frame = enteringDataDescriptionLabel_.frame;
    frame.origin.y = yPosition + 10.0f;
    enteringDataDescriptionLabel_.frame = frame;
    yPosition += 20.0 + frame.size.height;
    
    frame = transactionTitleLabel_.frame;
    frame.origin.y = yPosition + 15.0f;
    transactionTitleLabel_.frame = frame;
    yPosition += 15.0f + frame.size.height;
    
    frame = transactionTextField_.frame;
    frame.origin.y = yPosition + 10.0f;
    transactionTextField_.frame = frame;
    yPosition += 10.0f + frame.size.height;
    
    frame = amountTitleLabel_.frame;
    frame.origin.y = yPosition + 15.0f;
    amountTitleLabel_.frame = frame;
    yPosition += 15.0f + frame.size.height;
    
    frame = amountTextField_.frame;
    frame.origin.y = yPosition + 10.0f;
    amountTextField_.frame = frame;
    yPosition += 10.0f + frame.size.height;
    
    frame = continueButton_.frame;
    frame.origin.y = yPosition + 20.0f;
    continueButton_.frame = frame;
    
}

- (void) setService:(Service *)service {

    if (service_ != service) {
        [service retain];
        [service_ release];
        service_ = service;
    }
}

- (void) setProcess:(PaymentISProcess *)process {
    
    if (process_ != process) {
        
        [process_ setDelegate:nil];
        [process retain];
        [process_ release];
        process_ = process;
        
    }
    
    [process_ setDelegate:self];
    
}

- (UINavigationItem *) navigationItem {
    
    UINavigationItem *result  = [super navigationItem];
    
    result.mainTitle = NSLocalizedString(INTERNET_SHOPPING_STEP_ONE_TITLE_TEXT_KEY, nil);
    
    return result;
    
}

- (NXT_Peru_iPhone_AppDelegate *)appDelegate {
    
    return (NXT_Peru_iPhone_AppDelegate *)([UIApplication sharedApplication]).delegate;
    
}

@end
