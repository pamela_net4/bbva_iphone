//
//  InternetShoppingStepThreeViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 4/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "InternetShoppingStepThreeViewController.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "SimpleHeaderView.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "UIButton+Util.h"
#import "MOKEditableViewController+protected.h"
#import "TitleAndAttributes.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "BankAccount.h"

/* test */
#import "PaymentConfirmationViewController.h"

/**
 * Short vertical distance
 */
#define SHORT_VERTICAL_DISTANCE                             5.0f

/**
 * Long vertical distance
 */
#define LONG_VERTICAL_DISTANCE                              10.0f


#define NIB_FILE_NAME @"InternetShoppingStepThreeViewController"

@interface InternetShoppingStepThreeViewController ()

- (void) releaseInternetShoppingStepThreeViewControllerGraphicsElements;

@end

@implementation InternetShoppingStepThreeViewController

@synthesize accountCombo = accountCombo_;

- (void) dealloc {

    [self releaseInternetShoppingStepThreeViewControllerGraphicsElements];
    
    [processIS_ release];
    processIS_ = nil;
    
    [labelsArray_ release];
    labelsArray_ = nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseInternetShoppingStepThreeViewControllerGraphicsElements];
        [self setView:nil];
        
    }
}

- (void) releaseInternetShoppingStepThreeViewControllerGraphicsElements {

    
    for (UILabel *label in labelsArray_) {
        
        [label removeFromSuperview];
        [label release];
        label = nil;
        
    }
    
    [labelsArray_ removeAllObjects];
    
    [firstViewHeader_ release];
    firstViewHeader_ = nil;
    
    [firstViewSeparator_ release];
    firstViewSeparator_ = nil;
    
    [secondViewHeader_ release];
    secondViewHeader_ = nil;
    
    [firstViewSeparator_ release];
    firstViewSeparator_ = nil;
    
    [secondViewSeparator_ release];
    secondViewSeparator_ = nil;
    
    [accountCombo_ release];
    accountCombo_ = nil;
    
}

- (void) awakeFromNib {

    [super awakeFromNib];
    
    labelsArray_ = [[NSMutableArray alloc] init];
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];

}

- (void) viewDidLoad {

    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleAccountSelectionButton:accountCombo_];
    
    [accountCombo_ setStringListSelectionButtonDelegate:self];
    
    [[self firstView] setBackgroundColor:[UIColor whiteColor]];
    [NXT_Peru_iPhoneStyler styleNXTView:[self firstView]];
    [NXT_Peru_iPhoneStyler styleNXTView:[self secondView]];
    
    firstViewHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [[self firstView] addSubview:firstViewHeader_];
    firstViewSeparator_ = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 2.0f)];
    
    [firstViewSeparator_ setImage:[[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
    [[self firstView] addSubview:firstViewSeparator_];
    
    
    [firstViewHeader_ setTitle:NSLocalizedString(INTERNET_SHOPPING_STEP_ONE_TITLE_TEXT_KEY, nil)];
    
    
    secondViewHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [secondViewHeader_ setTitle:NSLocalizedString(INTERNET_SHOPPING_CHOOSE_PAYMENT_MODE_TEXT_KEY, nil)];
    [[self secondView] addSubview:secondViewHeader_];
    
    secondViewSeparator_ = [[UIImageView alloc] initWithFrame: CGRectMake( 0.0f,0.0f,320.0f,2.0f)];
    [secondViewSeparator_ setImage:[[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
    [[self secondView] addSubview:secondViewSeparator_];
    
}

- (void) viewDidUnload {

    [super viewDidUnload];
    
    [self releaseInternetShoppingStepThreeViewControllerGraphicsElements];

}

- (void) viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    [self resetScrollToTopLeftAnimated:YES];
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    if([self hasNavigateForward]){
    
        [processIS_ setSelectedMethodIndex:-1];
        
    } else {
    
        [self setHasNavigateForward: NO];
        
    }
    
    NSMutableArray *accountNames = [[[NSMutableArray alloc] init] autorelease];
    
    NSString *account = @"";
    
    NSArray *accountArray = [processIS_ accountsArray];
    
    for (BankAccount *bankAccount in accountArray) {
        
        account = [bankAccount accountIdAndDescription];
        [accountNames addObject:account];
        
    }
    
    [accountCombo_ setOptionStringList: accountNames];
    [accountCombo_ setNoSelectionText:NSLocalizedString(ACCOUND_CHARGED_TEXT_KEY, nil)];
    [accountCombo_ setSelectedIndex: [processIS_ selectedAccountIndex]];
    
    
    [self viewForFirstView];
    [self viewForSecondView];
    [self relocateViews];
    [self lookForEditViews];
    [self scrollableView];
    
    [self resetScrollToTopLeftAnimated:YES];
    
}

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]);
    [self setScrollFrame:frame];
    
}


- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    for (UILabel *label in labelsArray_) {
        
        [label removeFromSuperview];
        
    }
    [labelsArray_ removeAllObjects];
}


- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}


+ (InternetShoppingStepThreeViewController *) internetShoppigStepThreeViewController {
    
    InternetShoppingStepThreeViewController *result = [[[InternetShoppingStepThreeViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;

}

- (void) viewForFirstView {

    
    CGFloat yPosition = 0.0f;
    
    CGRect frame = firstViewHeader_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    firstViewHeader_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
    
    NSArray *infoArray = [NSArray arrayWithArray:[[self process] dataInfoArray]];
    [labelsArray_ removeAllObjects];
    
    for (TitleAndAttributes *titleAndAttributes in infoArray) {
        
        if ([[titleAndAttributes attributesArray] count] == 0)
        {
            continue;
        }
        else if([[[titleAndAttributes attributesArray] objectAtIndex:0] length]==0)
        {
            continue;
        }
        
        UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(20.0f, yPosition, 280.0f, 15.0f)] autorelease];
        
        yPosition += 15.0f;
        
        UILabel *detailLabel = [[[UILabel alloc] initWithFrame:CGRectMake(20.0f, yPosition, 280.0f, 15.0f)] autorelease];
        
        yPosition += 20.0f;
        
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [detailLabel setBackgroundColor:[UIColor clearColor]];
        
        [NXT_Peru_iPhoneStyler styleLabel:titleLabel withFontSize:14.0f color:[UIColor blackColor]];
        
        [NXT_Peru_iPhoneStyler styleLabel:detailLabel withFontSize:14.0f color:[UIColor BBVAGreyToneTwoColor]];
        
        if ([[titleAndAttributes titleString] isEqualToString: NSLocalizedString(INTERNET_SHOPPING_AMOUNT_PAYED_TEXT_KEY, nil)]) {
            
            [NXT_Peru_iPhoneStyler styleLabel:detailLabel withFontSize:14.0f color:[UIColor BBVAMagentaColor]];
            
        } else if ([[titleAndAttributes titleString] isEqualToString: NSLocalizedString(INTERNET_SHOPPING_AMOUNT_CHARGED_TEXT_KEY, nil)]) {
            
            [NXT_Peru_iPhoneStyler styleLabel:detailLabel withFontSize:14.0f color:[UIColor BBVAMagentaColor]];
            
        }              
        
        [titleLabel setText:[titleAndAttributes titleString]];
        
        if ([[titleAndAttributes attributesArray] count] == 0) {
            
            [detailLabel setText:@""];
            
        } else {
            
            [detailLabel setText:[[titleAndAttributes attributesArray] objectAtIndex:0]];
            
        }
        
        [labelsArray_ addObject:titleLabel];
        [labelsArray_ addObject:detailLabel];
        
        [[self firstView] addSubview:titleLabel];
        [[self firstView] addSubview:detailLabel];
        
    }
    
    //yPosition += 10.0f;
    
    frame = firstViewSeparator_.frame;
    frame.origin.y = yPosition;
    firstViewSeparator_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    firstViewHeight_ = yPosition;
    
}

- (void)viewForSecondView {
    
    CGFloat yPosition = 0.0f;
    
    CGRect frame = secondViewHeader_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    secondViewHeader_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + SHORT_VERTICAL_DISTANCE;
    
    frame = accountCombo_.frame;
    frame.origin.y = yPosition;
    accountCombo_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + LONG_VERTICAL_DISTANCE;
    
    frame = secondViewSeparator_.frame;
    frame.origin.y = yPosition;
    secondViewSeparator_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    secondViewHeight_ = yPosition;
    
}

- (CGFloat) heightForFirstView {
    
    return firstViewHeight_;

}

- (CGFloat) heightForSecondView {

    return secondViewHeight_;
    
}

- (void) stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton {

    BOOL invokeSuperImplementation = YES;
    
    if (accountCombo_ == stringListSelectionButton) {
        
        [processIS_ setSelectedAccountIndex:[stringListSelectionButton selectedIndex]];
        invokeSuperImplementation = NO;
        
    }
    
    if (invokeSuperImplementation) {
        
        [super stringListSelectionButtonSelectedIndexChanged: stringListSelectionButton];
        
    }

}

- (IBAction)continueButtonTapped {
    
    
    [self saveProcessInformation];
    
    [super continueButtonTapped];
    
}

- (void) saveProcessInformation {

    [processIS_ setDelegate:self];
    
}

- (UINavigationItem *) navigationItem {
    
    UINavigationItem *result  = [super navigationItem];
    
    NSString *title = @"";
    
    switch ([[self process] paymentOperationType]) {
        case PTEPaymentISService:
            
            title = [NSString stringWithFormat:NSLocalizedString(INTERNET_SHOPPING_STEP_ONE_TITLE_TEXT_KEY, nil)];
            
            break;
            
        default:
            break;
    }
    
    result.mainTitle = title;
    
    return result;
    
}


- (void) setProcess:(PaymentBaseProcess *)process {

    [super setProcess: process];
    
    if(processIS_ != process){
    
        [process retain];
        [processIS_ release];
        processIS_ = (PaymentISProcess *) process;
    }
}

@end
