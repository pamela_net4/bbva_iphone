//
//  InternetShoppingViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 28/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "InternetShoppingStepOneViewController.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "SimpleHeaderView.h"
#import "UIColor+BBVA_Colors.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKEditableViewController+protected.h"
#import "UINavigationItem+DoubleLabel.h"
#import "Service.h"
#import "InternetShoppingStepTwoViewController.h"
#import "ServiceDetail.h"
#import "Tools.h"

/**
 * Defines the Nib file name
 */
#define NIB_FILE_NAME @"InternetShoppingStepOneViewController"

@interface InternetShoppingStepOneViewController ()

- (void) releaseInternetShoppingViewControllerGraphicElements;

@end

@implementation InternetShoppingStepOneViewController

@synthesize titleLabel = titleLabel_;
@synthesize companyComboButton = companyComboButton_;
@synthesize continueButton = continueButton_;
@synthesize brandingLine = brandingLine_;
@synthesize process = process_;


- (void)dealloc {
    
    [self releaseInternetShoppingViewControllerGraphicElements];
    
    [companiesArray_ release];
    companiesArray_ = nil;
    
    [companySelected_ release];
    companySelected_ = nil;
    
    [process_ release];
    process_ = nil;
    
    [internetShoppingStepTwoViewController_ release];
    internetShoppingStepTwoViewController_ = nil;
    
    [super dealloc];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if((![self isViewLoaded]) || ([[self view] window] == nil)){
        
        [self releaseInternetShoppingViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}


- (void) releaseInternetShoppingViewControllerGraphicElements {
    
    [header_ release];
    header_ = nil;
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [companyComboButton_ release];
    companyComboButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
}

- (void) awakeFromNib {
    
    [super awakeFromNib];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect mainFrame = [[UIScreen mainScreen] bounds];
    
    [[self view] setFrame:mainFrame];
    
    header_ = [[SimpleHeaderView simpleHeaderView] retain];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
    [[self view] addSubview:header_];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:14.0f color:[UIColor BBVABlueSpectrumColor]];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:continueButton_];
    
    [continueButton_ setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil) forState:UIControlStateNormal];
    
    [NXT_Peru_iPhoneStyler styleStringListButton:companyComboButton_];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed: BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    CGRect frame = [[self view] frame];
    
    CGRect scrollFrame = [[self scrollableView] frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingLine] frame]) - CGRectGetMinY(frame);
    [self setScrollFrame:scrollFrame];
    
    
    if(companiesArray_ == nil){
        
        companiesArray_ = [[NSMutableArray alloc] init];
        
    }
    
    [companiesArray_ removeAllObjects];
    
    [companiesArray_ addObject: @"Safetypay"];
    
    [companyComboButton_ setOptionStringList:[NSArray arrayWithArray:companiesArray_]];
    
    [companyComboButton_ setStringListSelectionButtonDelegate:self];
    
    [companyComboButton_ setSelectedIndex: 0];
    [process_ setSelectedCompanyIndex: 0];
    
}

- (void) viewDidUnload {
    
    [super viewDidUnload];
    [self releaseInternetShoppingViewControllerGraphicElements];

}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    [header_ setTitle:[NSString stringWithFormat:NSLocalizedString(INTERNET_SHOPPING_STEP_ONE_SELECT_BUSINESS_KEY, nil)]];
    
    [titleLabel_ setText:[NSString stringWithFormat:@"%@",NSLocalizedString(INTERNET_SHOPPING_COMPANY_TEXT_KEY, nil)]];
    
    if (process_ != nil) {
        
        [process_ setInternetShoppingDelegate:self];
        
    }
    
    [self lookForEditViews];
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

- (void) viewWillDisappear:(BOOL)animated {

    [super viewWillDisappear:animated];
    
}

+ (InternetShoppingStepOneViewController *) internetShoppingStepOneViewController {
    
    InternetShoppingStepOneViewController *result = [[[InternetShoppingStepOneViewController alloc] initWithNibName:NIB_FILE_NAME bundle: nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
}


- (void) stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton {

    NSInteger selectedIndex = [companyComboButton_ selectedIndex];
    
    if((selectedIndex >= 0) && (selectedIndex < [companiesArray_ count])){
        
        companySelected_ = [companiesArray_ objectAtIndex:selectedIndex];
        [process_ setSelectedCompany:companySelected_];
        [process_ setSelectedCompanyIndex:[companyComboButton_ selectedIndex]];

    } else {
    
        [process_ setSelectedCompany: @""];
        [process_ setSelectedCompanyIndex:-1];
        [process_ setSelectedCompanyCode:@""];
    
    }
    
}


- (IBAction)continueButtonTapped {

    NSInteger selectedIndex = [companyComboButton_ selectedIndex];
    
    if((selectedIndex >= 0) && (selectedIndex < [companiesArray_ count])){
        
        companySelected_ = [companiesArray_ objectAtIndex:selectedIndex];
        [process_ setSelectedCompany:companySelected_];
        [process_ setSelectedCompanyIndex:[companyComboButton_ selectedIndex]];
        
    } else {
        
        [process_ setSelectedCompany: @""];
        [process_ setSelectedCompanyIndex:-1];
        [process_ setSelectedCompanyCode:@""];
        
    }

    
    [process_ startPaymentInitialRequest];
    
}

- (void) setProcess:(PaymentISProcess *)process {

    if (process_ != process) {
        
        [process retain];
        [process_ release];
        process_ = process;
        
    }
    [process_ setInternetShoppingDelegate : self];
}

- (void) initilizationHasFinished {

    if([process_.internetShoppingPaymentStatus.additionalInformation.activeStatus isEqualToString: @"N"]){
        
    [Tools showInfoWithMessage: [process_.internetShoppingPaymentStatus.additionalInformation message]];
        
    } else {
        
        if(internetShoppingStepTwoViewController_ != nil) {
            
            [internetShoppingStepTwoViewController_ release];
            internetShoppingStepTwoViewController_  = nil;
            
        }
        
        internetShoppingStepTwoViewController_ = [[InternetShoppingStepTwoViewController internetShoppingStepTwoViewController] retain];
        
        [internetShoppingStepTwoViewController_ setProcess:process_];
        
        [self.navigationController pushViewController:internetShoppingStepTwoViewController_ animated:YES];
        
    }
    
    
}

- (NXT_Peru_iPhone_AppDelegate *)appDelegate {
    
    return (NXT_Peru_iPhone_AppDelegate *)([UIApplication sharedApplication]).delegate;
    
}

- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result  = [super navigationItem];
    
    result.mainTitle = NSLocalizedString(INTERNET_SHOPPING_STEP_ONE_TITLE_TEXT_KEY, nil);
    
    return result;
}

@end
