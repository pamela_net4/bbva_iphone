//
//  PublicServDirectTVStepTwoViewController.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 12/10/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "MOKEditableViewController.h"
#import "MOKStringListSelectionButton.h"

@class InsititutionAndCompaniesStepTwoViewController;
@class SimpleHeaderView;

@interface PublicServDirectTVStepTwoViewController : MOKEditableViewController<MOKStringListSelectionButtonDelegate>{
    
@private
    
#pragma Graphic
    
    /**
     * Header
     */
    SimpleHeaderView *header_;
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Company combo button
     */
    MOKStringListSelectionButton *companyComboButton_;
    
    /**
     * Public Service Step Two View Controller
     */
    InsititutionAndCompaniesStepTwoViewController *publicServInstCompStepTwoViewController_;
    
    /**
     * Continue button
     */
    UIButton *continueButton_;
    
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    
#pragma Logic
    
    /**
     * Service type
     */
    NSString *serviceType_;
    
    /**
     * Selected company
     */
    NSString *companySelected_;
    
    /**
     * Navigation flag
     */
    BOOL hasNavigateForward_;
    
    /**
     *  To know if is cable company and show the table
     */
    BOOL isCable_;
    
    /**
     *  array with institutions information
     */
    NSMutableArray *institutionsArray_;
    
    /**
     *  position of the selected institution
     */
    NSInteger selectedIndex_;
}

/**
 * Provides readwrite access to the titleLabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides readwrite access to the companyButton and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet MOKStringListSelectionButton *companyComboButton;

/**
 * Provides readwrite access to the continueButton and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIButton *continueButton;

/**
 * Provides readwrite access to the brandingLine and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *brandingLine;

/**
 * Provides readwrite access to the service
 */
@property (nonatomic,readwrite, copy) NSString *serviceType;

/**
 * Provides read-write access to the intitutionsArray
 */
@property (retain, readwrite, nonatomic) NSMutableArray *institutionsArray;

/**
 * Provides readwrite access to the hasNavigateForward
 */
@property (nonatomic,readwrite, assign) BOOL hasNavigateForward;

/**
 * Creates and returns an autoreleased PublicServDirectTVStepTwoViewController constructed from a NIB file.
 *
 * @return The autoreleased PublicServDirectTVStepTwoViewController constructed from a NIB file.
 */
+ (PublicServDirectTVStepTwoViewController *)publicServDirectTVStepTwoViewController;

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped;

@end
