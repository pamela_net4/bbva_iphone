/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PublicServListViewController.h"

#import "Card.h"
#import "CardList.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentCell.h"
#import "PaymentPSCellServProcess.h"
#import "PaymentPSElectServProcess.h"
#import "PaymentPSLandlineServProcess.h"
#import "PaymentPSWaterServProcess.h"
#import "PaymentPSGasServProcess.h"
#import "PaymentPSCableServProcess.h"
#import "PublicServStepOneViewController.h"
#import "Service.h"
#import "Session.h"
#import "StatusEnabledResponse.h"
#import "StringKeys.h"
#import "Tools.h"
#import "Updater.h"


#pragma mark -
#pragma mark Static attributes

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"PublicServListViewController"


#pragma mark -

/**
 * PublicServListViewController private extension
 */
@interface PublicServListViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releasePublicServListViewControllerGraphicElements;

@end


#pragma mark -

@implementation PublicServListViewController

#pragma mark -
#pragma mark Properties

@synthesize table = table_;
@synthesize servicesList = servicesList_;
@synthesize brandingLine = brandingLine_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releasePublicServListViewControllerGraphicElements];
    
    [servicesList_ release];
    servicesList_ = nil;
    
    [publicServStepOneViewController_ release];
    publicServStepOneViewController_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasePublicServListViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releasePublicServListViewControllerGraphicElements {
    
	[table_ release];
    table_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];

    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
    [NXT_Peru_iPhoneStyler styleTableView:table_];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    table_.scrollEnabled = NO;    
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releasePublicServListViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    table_.backgroundColor = [UIColor clearColor];
    
    [table_ reloadData];

}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
	
}

/*
 * Creates and returns an autoreleased PublicServListViewController constructed from a NIB file.
 */
+ (PublicServListViewController *)publicServListViewController {
    
    PublicServListViewController *result =  [[[PublicServListViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark UITableView methods

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PaymentCell *result = (PaymentCell *)[tableView dequeueReusableCellWithIdentifier:[PaymentCell cellIdentifier]];
    
    if (result == nil) {
        result = [PaymentCell paymentCell];
    }
    
    NSUInteger row = [indexPath row];
    
    if (row < [servicesList_ count]) {
        
        Service *service = [servicesList_ objectAtIndex:[indexPath row]];
        
        [[result titleLabel] setText:[service service]];
        
    } else {
        
        [[result titleLabel] setText:@""];
        
    }
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    [result.imageView setImage:[imagesCache imageNamed:PAYMENT_LIST_ICON_IMAGE_FILE_NAME]];
    
    result.selectionStyle = UITableViewCellSelectionStyleGray;
    [result setShowDisclosureArrow:YES];
    [result setShowSeparator:YES];
    result.backgroundColor = [UIColor clearColor];
    return result;
    
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void) tableView: (UITableView*) tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUInteger row = [indexPath row];
    
    NSString *serviceTitle = @"";
    PaymentPSProcess *process = nil;
    Service *service = nil;
    BOOL isCable = FALSE;
    
    if (row < [servicesList_ count]) {
        
        service = [servicesList_ objectAtIndex:row];
        NSString *serviceCode = [service serviceCode];
        
        if ([serviceCode isEqualToString:PAYMENT_WATER_SERVICE_CODE]) {
            
            serviceTitle = NSLocalizedString(PUBLIC_SERVICE_WATER_HEADER_TEXT_KEY, nil);
            process = [[[PaymentPSWaterServProcess alloc] init] autorelease];

        } else if ([serviceCode isEqualToString:PAYMENT_ELECTRICAL_SERVICE_CODE]) {
            
            serviceTitle = NSLocalizedString(PUBLIC_SERVICE_ELECTRICITY_TEXT_KEY, nil);
            process = [[[PaymentPSElectServProcess alloc] init] autorelease];

        } else if ([serviceCode isEqualToString:PAYMENT_LANDLINE_SERVICE_CODE]) {
            
            serviceTitle = NSLocalizedString(PUBLIC_SERVICE_LANDLINE_TEXT_KEY, nil);
            process = [[[PaymentPSLandlineServProcess alloc] init] autorelease];

        } else if ([serviceCode isEqualToString:PAYMENT_MOBILE_PHONE_SERVICE_CODE]) {
            
            serviceTitle = NSLocalizedString(PUBLIC_SERVICE_CELLULAR_TEXT_KEY, nil);
            process = [[[PaymentPSCellServProcess alloc] init] autorelease];
            
        } else if ([serviceCode isEqualToString:PAYMENT_GAS_SERVICE_CODE]) {
            
            serviceTitle = NSLocalizedString(PUBLIC_SERVICE_GAS_TEXT_KEY, nil);
            process = [[[PaymentPSGasServProcess alloc] init] autorelease];
            
        } else if ([serviceCode isEqualToString:PAYMENT_CABLE_SERVICE_CODE]) {
            
            serviceTitle = NSLocalizedString(PUBLIC_SERVICE_CABLE_TEXT_KEY, nil);
            process = [[[PaymentPSCableServProcess alloc] init] autorelease];
            isCable = TRUE;
            
        }
        
    }
    
    if (service != nil) {
        
        if (publicServStepOneViewController_ == nil) {
            
            publicServStepOneViewController_ = [[PublicServStepOneViewController publicServStepOneViewController] retain];
            
        }
        
        [publicServStepOneViewController_ setService:service];
        [publicServStepOneViewController_ setServiceType:serviceTitle];
        [publicServStepOneViewController_ setHasNavigateForward:NO];
        [publicServStepOneViewController_ setIsCable:isCable];
        [publicServStepOneViewController_ setProcess:process];

        [self.navigationController pushViewController:publicServStepOneViewController_ 
                                             animated:YES];
        
    } else {
    
        [Tools showInfoWithMessage:NSLocalizedString(PUBLIC_SERVICE_SERVICE_NOT_FOUND_KEY, nil)];
    
    }
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section {
    
    return [servicesList_ count];
    
}

/**
 
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [PaymentCell cellHeight];
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Set the services list
 */
- (void)setServicesList:(NSArray *)servicesList {
    
    if (servicesList != servicesList_) {
        
        if (servicesList_ == nil) {
            
            servicesList_ = [[NSMutableArray alloc] init];
            
        } else {
            
            [servicesList_ removeAllObjects];
            
        }
        
        Class serviceClass = [Service class];
        Service *service = nil;
        NSString *serviceCode = nil;

        for (NSObject *object in servicesList) {
    
            if ([object isKindOfClass:serviceClass]) {
        
                service = (Service *)object;
                serviceCode = [service serviceCode];
        
                if (([serviceCode isEqualToString:PAYMENT_WATER_SERVICE_CODE]) || ([serviceCode isEqualToString:PAYMENT_ELECTRICAL_SERVICE_CODE]) ||
                    ([serviceCode isEqualToString:PAYMENT_LANDLINE_SERVICE_CODE])
                    || ([serviceCode isEqualToString:PAYMENT_MOBILE_PHONE_SERVICE_CODE])
                    || ([serviceCode isEqualToString:PAYMENT_GAS_SERVICE_CODE])
                    || ([serviceCode isEqualToString:PAYMENT_CABLE_SERVICE_CODE])) {
            
                    [servicesList_ addObject:service];
            
                }
                
            }
        
        }
    
    }

    [table_ reloadData];

}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [self customNavigationItem];
    
    [[result customTitleView] setTopLabelText:[NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY, nil)]];
    
    return result;
    
}


@end
