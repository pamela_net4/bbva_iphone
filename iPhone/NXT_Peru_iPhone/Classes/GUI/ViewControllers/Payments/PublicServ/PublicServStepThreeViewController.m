/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PublicServStepThreeViewController.h"

#import "BankAccount.h"
#import "Card.h"
#import "CheckComboCell.h"
#import "FrequentOperationHeaderView.h"
#import "FOOperationHelper.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKDoubleLabel.h"
#import "MOKEditableViewController.h"
#import "MOKEditableViewController+protected.h"
#import "MOKNavigationItem.h"
#import "MOKStringListSelectionButton.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Payment.h"
#import "PaymentBaseProcess.h"
#import "PaymentCell.h"
#import "PaymentsConstants.h"
#import "PaymentPSElectServProcess.h"
#import "PaymentPSWaterServProcess.h"
#import "ReceiptCell.h" 
#import "Session.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "Updater.h"

/**
 * Defines the Nib file name
 */
#define NIB_FILE_NAME                                       @"PublicServStepThreeViewController"

/**
 * PaymentDataBaseViewController private extension
 */
@interface PublicServStepThreeViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releasePublicServStepThreeViewControllerGraphicElements;

@end

#pragma mark -

@implementation PublicServStepThreeViewController

#pragma mark -
#pragma mark Properties

@synthesize paymentsTable = paymentsTable_;
@synthesize paymentMethodsTable = paymentMethodsTable_;
@synthesize accontCombo = accontCombo_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releasePublicServStepThreeViewControllerGraphicElements];
    
    [labelsArray_ release];
    labelsArray_ = nil;
    
    [selectedPaymentsArray_ release];
    selectedPaymentsArray_ = nil;
    
    [processPS_ release];
    processPS_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasePublicServStepThreeViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releasePublicServStepThreeViewControllerGraphicElements {
    
    for (UILabel *label in labelsArray_) {
        
        [label removeFromSuperview];
        [label release];
        label = nil;
        
    }
    [labelsArray_ removeAllObjects];
    
    [paymentsTable_ release];
    paymentsTable_ = nil;
    
    [paymentMethodsTable_ release];
    paymentMethodsTable_ = nil;
    
    [firstViewHeader_ release];
    firstViewHeader_ = nil;

    [secondViewHeader_ release];
    secondViewHeader_ = nil;
    
    [thirdViewHeader_ release];
    thirdViewHeader_ = nil;
    
    [firstViewSeparator_ release];
    firstViewSeparator_ = nil;
    
    [secondViewSeparator_ release];
    secondViewSeparator_ = nil;
    
    [thirdViewSeparator_ release];
    thirdViewSeparator_ = nil;
    
    [accontCombo_ release];
    accontCombo_ = nil;
    
    [cellAccountCombo_ release];
    cellAccountCombo_ = nil;
    
    [cellCardCombo_ release];
    cellCardCombo_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    labelsArray_ = [[NSMutableArray alloc] init];
    selectedPaymentsArray_ = [[NSMutableArray alloc] init];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleAccountSelectionButton:accontCombo_];
    [NXT_Peru_iPhoneStyler styleTableView:paymentsTable_];
    [NXT_Peru_iPhoneStyler styleTableView:paymentMethodsTable_];
    
    [accontCombo_ setStringListSelectionButtonDelegate:self];
    
    [[self firstView] setBackgroundColor:[UIColor whiteColor]];
    [NXT_Peru_iPhoneStyler styleNXTView:[self secondView]];
    [NXT_Peru_iPhoneStyler styleNXTView:[self thirdView]];
    
    firstViewHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [[self firstView] addSubview:firstViewHeader_];
    firstViewSeparator_ = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 2.0f)];
    [firstViewSeparator_ setImage:[[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
    [[self firstView] addSubview:firstViewSeparator_];
    
    informationHeaderView_ = [[FrequentOperationHeaderView frequentOperationHeaderView] retain];
    
    CGRect frame = [informationHeaderView_ frame];
    frame.origin.y = 70.0f;
    frame.origin.x = 10.0f;
    frame.size.width = 300.0f;
    [informationHeaderView_ setFrame:frame];
    
    [[self firstView] addSubview:informationHeaderView_];
    
    secondViewHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [[self secondView] addSubview:secondViewHeader_];
    secondViewSeparator_ = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 2.0f)];
    [secondViewSeparator_ setImage:[[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
    [[self secondView] addSubview:secondViewSeparator_];
    
    thirdViewHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [[self thirdView] addSubview:thirdViewHeader_];
    thirdViewSeparator_ = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 2.0f)];
    [thirdViewSeparator_ setImage:[[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
    [[self thirdView] addSubview:thirdViewSeparator_];
     
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releasePublicServStepThreeViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    [selectedPaymentsArray_ removeAllObjects];

    if (![self hasNavigateForward]) {
        
        [processPS_ setSelectedMethodIndex:-1];
        
        if ([[processPS_ cardsArray] count] > 0) {
            
            [processPS_ setSelectedCardIndex:0];
            
        } else {
            
            [processPS_ setSelectedCardIndex:-1];
            
        }
        [self resetScrollToTopLeftAnimated:YES];
        
    } else {
        
        [selectedPaymentsArray_ addObjectsFromArray:[processPS_ selectedPaymentsArray]];
        [self setHasNavigateForward:NO];
    }
    
    [self viewForFirstView];
    [self viewForSecondView];
    [self viewForThirdView];
    [self relocateViews];
    
    if (![processPS_ hasCards]) {
        
        [accontCombo_ setSelectedIndex:[processPS_ selectedAccountIndex]];
        
    }
    
    [self lookForEditViews];
    [self scrollableView];

}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]);
    [self setScrollFrame:frame];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view.
 *
 * @param animated: If YES, the disappearance of the view is being animated.
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];

    for (UILabel *label in labelsArray_) {
        
        [label removeFromSuperview];
        
    }
    [labelsArray_ removeAllObjects];
}


/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    [self viewForFirstView];
    [self viewForSecondView];
    [self viewForThirdView];
    [self relocateViews];
    
}

/**
 * Creates and returns an autoreleased PublicServStepThreeViewController constructed from a NIB file.
 *
 * @return The autoreleased PublicServStepThreeViewController constructed from a NIB file.
 */
+ (PublicServStepThreeViewController *)publicServStepThreeViewController {

    PublicServStepThreeViewController *result =  [[[PublicServStepThreeViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;

}

#pragma mark -
#pragma mark Views methods

/**
 * Returns the view needed in the first view. NIL if it is not needed
 */
- (void)viewForFirstView {
    
    CGFloat yPosition = 0.0f;
    
    NSString *title = @"";
    switch ([[self process] paymentOperationType]) {
            
        case PTEPaymentPSElectricServices:
            title = NSLocalizedString(PUBLIC_SERVICE_ELECTRICITY_TEXT_KEY, nil);
            break;
        case PTEPaymentPSWaterServices:
            title = NSLocalizedString(PUBLIC_SERVICE_WATER_HEADER_TEXT_KEY, nil);
            break;
        case PTEPaymentPSCellular:
            title = [NSString stringWithFormat:NSLocalizedString(PUBLIC_SERVICE_SERVICES_OF_TEXT_KEY,nil), NSLocalizedString(PUBLIC_SERVICE_CELLULAR_TEXT_LOWER_KEY, nil)];
            break;
        case PTEPaymentPSPhone:
            title = [NSString stringWithFormat:NSLocalizedString(PUBLIC_SERVICE_SERVICES_OF_TEXT_KEY,nil), NSLocalizedString(PUBLIC_SERVICE_NOT_CAP_LANDLINE_TEXT_KEY, nil)];
            break;
        default:
            break;
    }
    
    [firstViewHeader_ setTitle:title];
    
    CGRect frame = firstViewHeader_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    firstViewHeader_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
    
    if ([processPS_ isFO]) {
        
        [firstViewHeader_ setTitle:NSLocalizedString(FO_HEADER_TITLE_KEY, nil)];
        
        [informationHeaderView_ removeFromSuperview];
        [[self firstView] addSubview:informationHeaderView_];
        
        [informationHeaderView_ setTitlesAndAttributeArray:[processPS_ dataInfoArray]];
        [informationHeaderView_ updateInformationView];
        
        frame = [informationHeaderView_ frame];
        frame.origin.y = yPosition;
        [informationHeaderView_ setFrame:frame];
        
        yPosition = CGRectGetMaxY(frame);
        
    }else{
        
        if (informationHeaderView_ != nil) {
            [informationHeaderView_ removeFromSuperview];
            [informationHeaderView_ release];
            informationHeaderView_ = nil;
        }
        
        NSArray *infoArray = [NSArray arrayWithArray:[[self process] dataInfoArray]];
        [labelsArray_ removeAllObjects];
        
        for (UIView *label in [[self firstView] subviews]) {
            if ([label tag] == 777) {

                [label removeFromSuperview];
            }
        }
        
        for (TitleAndAttributes *titleAndAttributes in infoArray) {
            
            UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(20.0f, yPosition, 280.0f, 15.0f)] autorelease];
            
            yPosition += 15.0f;
            
            UILabel *detailLabel = [[[UILabel alloc] initWithFrame:CGRectMake(20.0f, yPosition, 280.0f, 15.0f)] autorelease];
            
            yPosition += 20.0f;
            
            [titleLabel setBackgroundColor:[UIColor clearColor]];
            [detailLabel setBackgroundColor:[UIColor clearColor]];
            
            [NXT_Peru_iPhoneStyler styleLabel:titleLabel withFontSize:14.0f color:[UIColor blackColor]];
            
            [NXT_Peru_iPhoneStyler styleLabel:detailLabel withFontSize:14.0f color:[UIColor BBVAGreyToneTwoColor]];
            
            [titleLabel setText:[titleAndAttributes titleString]];
            
            if ([[titleAndAttributes attributesArray] count] == 0) {
                
                [detailLabel setText:@""];
                
            } else {
                
                [detailLabel setText:[[titleAndAttributes attributesArray] objectAtIndex:0]];
                
            }
            
            [titleLabel setTag:777];
            [detailLabel setTag:777];
            
            [labelsArray_ addObject:titleLabel];
            [labelsArray_ addObject:detailLabel];
            
            [[self firstView] addSubview:titleLabel];
            [[self firstView] addSubview:detailLabel];
            
        }
    }
    
    yPosition += 10.0f;
    
    frame = firstViewSeparator_.frame;
    frame.origin.y = yPosition;
    firstViewSeparator_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    firstViewHeight_ = yPosition;
    
}

/**
 * Returns the view needed in the second view. NIL if it is not needed
 */
- (void)viewForSecondView {
    
    CGFloat yPosition = 0.0f;
    
    NSString *title = NSLocalizedString(PAYMENT_SELECT_RECEIPT_TO_CANCEL_TEXT_KEY, nil);
    [secondViewHeader_ setTitle:title];
    
    CGRect frame = secondViewHeader_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    secondViewHeader_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
        
    [paymentsTable_ reloadData];
    
    frame = paymentsTable_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [paymentsTable_ contentSize].height;
    paymentsTable_.frame = frame;
    
    [paymentsTable_ setScrollEnabled:NO];
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
    
    frame = secondViewSeparator_.frame;
    frame.origin.y = yPosition;
    secondViewSeparator_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    secondViewHeight_ = yPosition;

}

/**
 * Returns the view needed in the third view. NIL if it is not needed
 */
- (void)viewForThirdView {

    CGFloat yPosition = 0.0f;
    
    NSString *title = @"";
    
    [paymentMethodsTable_ setHidden:YES];
    [accontCombo_ setHidden:NO];

    if ([processPS_ hasCards]) {
        
        [paymentMethodsTable_ setHidden:NO];
        [accontCombo_ setHidden:YES];
        title = NSLocalizedString(PUBLIC_SERVICE_STEP_ONE_SELECT_PAYMENT_OPTION_TEXT_KEY, nil);
        
    } else {
        
        [paymentMethodsTable_ setHidden:YES];
        [accontCombo_ setHidden:NO];
        
    }
    
    if (![accontCombo_ isHidden]) {
        
        [paymentMethodsTable_ setHidden:YES];
        [accontCombo_ setHidden:NO];
        
        NSMutableArray *accountNames = [[[NSMutableArray alloc] init] autorelease];
        NSString *account = @"";
        
        NSArray *accountArray = [processPS_ accountsArray]; 

               
        for (BankAccount *bankAccount in accountArray) {
            
            account = [bankAccount accountIdAndDescription];
            [accountNames addObject:account];
            
        }
        
        [accontCombo_ setOptionStringList:accountNames];
        [accontCombo_ setNoSelectionText:NSLocalizedString(ACCOUND_CHARGED_TEXT_KEY, nil)];
        [accontCombo_ setSelectedIndex:[processPS_ selectedAccountIndex]];
        
        title = NSLocalizedString(PAYMENT_SELECT_PAYMENT_MODE_KEY, nil);
        
    }
    
    [thirdViewHeader_ setTitle:title];
    
    CGRect frame = thirdViewHeader_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    thirdViewHeader_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
       
    if (![accontCombo_ isHidden]) {
        
        frame = accontCombo_.frame;
        frame.origin.y = yPosition;
        accontCombo_.frame = frame;
        yPosition = CGRectGetMaxY(frame) + 10.0f;
       
    } else {
        
        [paymentMethodsTable_ reloadData];
    
        frame = paymentMethodsTable_.frame;
        frame.origin.y = yPosition;
        frame.size.height = paymentMethodsTable_.contentSize.height;
        paymentMethodsTable_.frame = frame;
    
        yPosition = CGRectGetMaxY(frame) + 10.0f;

    }
        
    frame = thirdViewSeparator_.frame;
    frame.origin.y = yPosition;
    thirdViewSeparator_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);  
        
    thirdViewHeight_ = yPosition;

}

/**
 * Returns the height for the first view. 0 if it is NIL
 */
- (CGFloat)heightForFirstView {

    return firstViewHeight_;

}

/**
 * Returns the height for the second view. 0 if it is NIL
 */
- (CGFloat)heightForSecondView {

    return secondViewHeight_;

}

/**
 * Returns the height for the third view. 0 if it is NIL
 */
- (CGFloat)heightForThirdView {
    
    return thirdViewHeight_;
    
}

#pragma mark -
#pragma mark UITableView methods

/**
 * Tells the data source to return the number of rows in a given section of a table view. (required)
 *
 * @param tableView: The table-view object requesting this information.
 * @param section: An index number identifying a section in tableView.
 * @return The number of rows in section.
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger result = 0;
    
    if (tableView == paymentMethodsTable_) {
                
        result = 2;
        
    } else if (tableView == paymentsTable_) {
        
        result = [[processPS_ paymentsArray] count];
                    
    } else {
    
        result = [super tableView:tableView numberOfRowsInSection:section];
        
    }
    
    return result;
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. (required)
 *
 * @param tableView: A table-view object requesting the cell.
 * @param indexPath: An index path locating a row in tableView.
 * @return An object inheriting from UITableViewCellthat the table view can use for the specified row.
 */
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == paymentMethodsTable_) {
        
        CheckComboCell *resultAux = (CheckComboCell *)[tableView dequeueReusableCellWithIdentifier:[CheckComboCell cellIdentifier]];
        
        if (resultAux == nil) {
            resultAux = [CheckComboCell checkComboCell];
            [[resultAux combo] setStringListSelectionButtonDelegate:self];
        }
        
        if (indexPath.row == 0) {
            
            [resultAux applyAccountSelectionStyle:YES];
            
            [[resultAux topTextLabel] setText:NSLocalizedString(IAC_ACCOUNT_TITLE_TEXT_KEY, nil)];
            
            NSArray *array = [NSArray arrayWithArray:[processPS_ accountsArray]];
            NSMutableArray *stringsArray = [[[NSMutableArray alloc] init] autorelease];
            
            for (BankAccount *account in array) {
                
                [stringsArray addObject:[account bankAccountListName]];
                
            }
            
            [cellAccountCombo_ release];
            cellAccountCombo_ = nil;
            
            cellAccountCombo_ = [[resultAux combo] retain];
            
            if (cellAccountCombo_ == cellCardCombo_) {
                
                [cellCardCombo_ release];
                cellCardCombo_ = nil;
                
            }
            
            [cellAccountCombo_ setOptionStringList:stringsArray];
            [cellAccountCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECT_ACCOUNT_TEXT_KEY, nil)];
            [cellAccountCombo_ setStringListSelectionButtonDelegate:self];
            [cellAccountCombo_ setSelectedIndex:[processPS_ selectedAccountIndex]];
            [resultAux setCheckActive:([processPS_ selectedMethodIndex] == indexPath.row)];
            
        } else {
            
            [resultAux applyAccountSelectionStyle:NO];

            [[resultAux topTextLabel] setText:NSLocalizedString(PAYMENT_CARDS_TITLE_TEXT_KEY, nil)];
            
            NSArray *array = [NSArray arrayWithArray:[processPS_ cardsArray]];
            NSMutableArray *stringsArray = [[[NSMutableArray alloc] init] autorelease];
            
            for (Card *card in array) {
                
                [stringsArray addObject:[card cardListName]];
                
            }
            
            [cellCardCombo_ release];
            cellCardCombo_ = nil;
            
            cellCardCombo_ = [[resultAux combo] retain];
            
            if (cellCardCombo_ == cellAccountCombo_) {
                
                [cellAccountCombo_ release];
                cellAccountCombo_ = nil;
                
            }
            
            [cellCardCombo_ setOptionStringList:stringsArray];
            [cellCardCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECT_CARD_TEXT_KEY, nil)];
            [cellCardCombo_ setStringListSelectionButtonDelegate:self];
            [cellCardCombo_ setSelectedIndex:[processPS_ selectedCardIndex]];
            [resultAux setCheckActive:([processPS_ selectedMethodIndex] == indexPath.row)];

        }
        
        return resultAux;
        
    } else if (tableView == paymentsTable_) {
                
        ReceiptCell *resultAux = (ReceiptCell *)[tableView dequeueReusableCellWithIdentifier:[ReceiptCell cellIdentifier]];
        
        if (resultAux == nil) {
            resultAux = [ReceiptCell receiptCell];
        }
        resultAux.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if ([processPS_ paymentOperationType] == PTEPaymentPSWaterServices) {
            
            PaymentPSWaterServProcess *psWater = (PaymentPSWaterServProcess *)processPS_;
            [resultAux setWaterAmount:[psWater waterAmount]];
            
        }
        
        Payment *payment = [[processPS_ paymentsArray] objectAtIndex:[indexPath row]];
        
        [resultAux setPayment:payment 
                  paymentType:[processPS_ paymentOperationType]];
        
        NSInteger position = [selectedPaymentsArray_ indexOfObject:payment];
        
        [resultAux setCheckActive:(position != NSNotFound)];
        
        return resultAux;
        
    } else {
    
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    }
}



/**
 * Asks the delegate for the height to use for a row in a specified location.
 *
 * @param tableView: The table-view object requesting this information.
 * @param indexPath: An index path that locates a row in tableView.
 * @return A floating-point value that specifies the height (in points) that row should be.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    CGFloat result = 0.0f;
    
    if (tableView == paymentMethodsTable_) {
    
        result = [CheckComboCell cellHeightCheckOn:([processPS_ selectedMethodIndex] == indexPath.row)];
        
    } else if (tableView == paymentsTable_) {
    
        result = [ReceiptCell cellHeightForReceiptType:[processPS_ paymentOperationType]];
    
    } else {
    
        result = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
        
    return result;
}

/**
 * Tells the delegate that a specified row is about to be selected. When the table is the receipts table, it is checked whether the selected receipts are the order ones.
 *
 * @param tableView A table-view object informing the delegate about the impending selection.
 * @param indexPath An index path locating the row in tableView.
 * @return An index-path object that confirms or alters the selected row.
 */
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSIndexPath *result = indexPath;
    
    if (tableView == paymentsTable_) {
        
        NSUInteger row = [indexPath row];
        
        NSArray *paymentsArray = [processPS_ paymentsArray];
        
        if (row < [paymentsArray count]) {
            
            Payment *payment = [paymentsArray objectAtIndex:row];
            
            if (![selectedPaymentsArray_ containsObject:payment]) {
                
                if (![processPS_ canSelectReceipt:payment
                                 forSelectedArray:selectedPaymentsArray_]) {
                    
                    result = nil;
                    [Tools showInfoWithMessage:NSLocalizedString(PAYMENT_ERROR_RECEIPTS_NOT_OLDER_RECEPIPS_TEXT_KEY, nil)];
                    
                }
                
            }
            
        } else {
            
            result = nil;
            
        }
        
    }
    
    return result;
    
}

/**
 * Tells the delegate that the specified row is now selected.
 *
 * @param tableView: A table-view object informing the delegate about the new row selection.
 * @param indexPath: An index path locating the new selected row in tableView.
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (tableView == paymentMethodsTable_) {
        
        [processPS_ setSelectedMethodIndex:indexPath.row];
                
        [self viewForThirdView];
        [self relocateViews];
        [self lookForEditViews];
        
        [tableView reloadData];
        
    } else if (tableView == paymentsTable_) {
        
        Payment *payment = [[processPS_ paymentsArray] objectAtIndex:[indexPath row]];
                    
        NSInteger position = [selectedPaymentsArray_ indexOfObject:payment];
        
        if (([processPS_ paymentOperationType] == PTEPaymentPSCellular) ||
            ([processPS_ paymentOperationType] == PTEPaymentPSPhone)) {
            
            [selectedPaymentsArray_ removeAllObjects];
            [selectedPaymentsArray_ addObject:payment];
            [paymentsTable_ reloadData];
            
        } else {
            
            if (position == NSNotFound) {
                
                if ([selectedPaymentsArray_ count] == 3) {
                    
                    [Tools showInfoWithMessage:NSLocalizedString(PAYMENT_ERROR_RECEIPTS_MAX_TEXT_KEY, nil)];
                    
                } else {
                    
                    [selectedPaymentsArray_ addObject:payment];
                    
                }
                
            } else {
                
                [selectedPaymentsArray_ removeObjectAtIndex:position];
                
            }
            
            NSArray *correctedReceiptsList = [processPS_ correctSelectedReceiptsList:selectedPaymentsArray_];
            
            if (correctedReceiptsList != selectedPaymentsArray_) {
                
                [selectedPaymentsArray_ removeAllObjects];
                [selectedPaymentsArray_ addObjectsFromArray:correctedReceiptsList];
                
            }
            
            [paymentsTable_ reloadData];

        }
        
    } else {
    
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
        
    }
    
}

#pragma mark -
#pragma mark  MOKStringListSelectionButtonDelegate

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton {
    
    BOOL invokeSuperImplementation = YES;
    
    if (accontCombo_ == stringListSelectionButton) {
        
        [processPS_ setSelectedAccountIndex:[stringListSelectionButton selectedIndex]];
        invokeSuperImplementation = NO;
        
    } else if ([processPS_ hasCards]) {
        
        if (([processPS_ selectedMethodIndex] == 0) && (stringListSelectionButton == cellAccountCombo_)) {
            
            [processPS_ setSelectedAccountIndex:[stringListSelectionButton selectedIndex]];
            invokeSuperImplementation = NO;
            
        } else if (([processPS_ selectedMethodIndex] == 1) && (stringListSelectionButton == cellCardCombo_)) {
            
            [processPS_ setSelectedCardIndex:[stringListSelectionButton selectedIndex]];
            invokeSuperImplementation = NO;
            
        }
    
    }
    
    if (invokeSuperImplementation) {
        
        [super stringListSelectionButtonSelectedIndexChanged:stringListSelectionButton];
        
    }
    
}

#pragma mark -
#pragma mark User interaction

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped {
    
    [self saveProcessInformation];
    
    [super continueButtonTapped];
    
}

/*
 * Save process information. 
 */
- (void)saveProcessInformation {
        
    [processPS_ setDelegate:self];
    
    [processPS_ setSelectedPaymentsArray:selectedPaymentsArray_];
    [processPS_ setSelectingCard:([processPS_ selectedMethodIndex] == 1)];
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    NSString *title = @"";
    
    switch ([[self process] paymentOperationType]) {
        
        case PTEPaymentPSElectricServices:
        case PTEPaymentPSWaterServices:
        case PTEPaymentPSCellular:
        case PTEPaymentPSPhone:
            
            title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY, nil)];
            
            break;
            
        case PTEPaymentRecharge:
            
            title = NSLocalizedString(PAYMENT_RECHARGE_OPERATION_TEXT_KEY, nil);
            
            break;
            
        case PTEPaymentContOwnCard:
        case PTEPaymentContThirdCard:
        case PTEPaymentOtherBank:
            
            title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_CARDS_TITLE_LOWER_TEXT_KEY, nil)];
            
            break;

        default:
            break;
    }
    
    if ([processPS_ isFO]) {
        title = NSLocalizedString(FO_TITLE_TEXT_REACTIVE_KEY, nil);
    }
    
    result.mainTitle = title;
    
    return result;
    
}

/*
 * Set the process
 */
- (void)setProcess:(PaymentBaseProcess *)process {
    
    [super setProcess:process];
    
    if (processPS_ != process) {

        [process retain];
        [processPS_ release];
        processPS_ = (PaymentPSProcess *)process;

    }

}


@end
