/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PublicServStepOneViewController.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "InsititutionAndCompaniesStepTwoViewController.h"
#import "InstitutionList.h"
#import "Institution.h"
#import "MOKDoubleLabel.h"
#import "MOKEditableViewController.h"
#import "MOKEditableViewController+protected.h"
#import "MOKStringListSelectionButton.h"
#import "MOKNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentInstitutionsAndCompaniesInitialResponse.h"
#import "PaymentInstitutionsAndCompaniesDetailResponse.h"
#import "PaymentBaseProcess.h"
#import "PaymentPSElectServProcess.h"
#import "PaymentPSProcess.h"
#import "PaymentCell.h"
#import "PaymentsConstants.h"
#import "PublicServStepTwoViewController.h"
#import "PublicServDirectTVStepTwoViewController.h"
#import "Session.h"
#import "Service.h"
#import "ServiceDetail.h"
#import "SimpleHeaderView.h"
#import "StatusEnabledResponse.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "Updater.h"


#pragma mark -
#pragma mark Static attributes
/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"PublicServStepOneViewController"

/**
 * GlobalPositionViewController private extension
 */
@interface PublicServStepOneViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releasePublicServStepOneViewControllerGraphicElements;

@end

#pragma mark -


@implementation PublicServStepOneViewController


#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;
@synthesize companyComboButton = companyComboButton_;
@synthesize continueButton = continueButton_;
@synthesize isCable = isCable_;
@synthesize institutionsArray = institutionsArray_;
@synthesize service = service_;
@synthesize serviceType = serviceType_;
@synthesize brandingLine = brandingLine_;
@synthesize hasNavigateForward = hasNavigateForward_;
@synthesize process = process_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releasePublicServStepOneViewControllerGraphicElements];
    
    [companiesArray_ release];
    companiesArray_ = nil;
    
    [service_ release];
    service_ = nil;
    
    [serviceType_ release];
    serviceType_ = nil;
    
    [companySelected_ release];
    companySelected_ = nil;
    
    [process_ release];
    process_ = nil;
    
    [institutionsArray_ release];
    institutionsArray_ = nil;
    
    [publicServStepTwoViewController_ release];
    publicServStepTwoViewController_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasePublicServStepOneViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releasePublicServStepOneViewControllerGraphicElements {
    
	[header_ release];
    header_ = nil;

	[titleLabel_ release];
    titleLabel_ = nil;
    
    [companyComboButton_ release];
    companyComboButton_ = nil;
    
    [continueButton_ release];
    continueButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    CGRect mainFrame = [[UIScreen mainScreen] bounds];
    
    [[self view] setFrame:mainFrame];
    
    header_ = [[SimpleHeaderView simpleHeaderView] retain];

    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];

    [[self view] addSubview:header_];
        
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ 
                         withFontSize:14.0f 
                                color:[UIColor BBVABlueSpectrumColor]];
    
    
    [NXT_Peru_iPhoneStyler styleBlueButton:continueButton_];
    [continueButton_ setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil) 
                     forState:UIControlStateNormal];
    
    
    [NXT_Peru_iPhoneStyler styleStringListButton:companyComboButton_];
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    CGRect frame = [[self view] frame];
    
    CGRect scrollFrame = [[self scrollableView] frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingLine] frame]) - CGRectGetMinY(frame);
    [self setScrollFrame:scrollFrame];
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releasePublicServStepOneViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    [self.appDelegate setTabBarVisibility:NO animated:YES];

    [companyComboButton_ setStringListSelectionButtonDelegate:self];    
        
    [header_ setTitle:[Tools capitalizeFirstWordOnly:[NSString stringWithFormat:NSLocalizedString(PUBLIC_SERVICE_STEP_ONE_SELECT_BUSINESS_KEY, nil),[serviceType_ lowercaseString]]]];

    [titleLabel_ setText:[NSString stringWithFormat:@"%@", NSLocalizedString(PAYMENT_RECHARGE_BUSINESS_TEXT_KEY, nil)]];

    if (companiesArray_ == nil) {

        companiesArray_ = [[NSMutableArray alloc] init];

    }
    
    [companiesArray_ removeAllObjects];
    
    if (institutionsArray_ == nil) {
        institutionsArray_ = [[NSMutableArray alloc] init];
    }
    [institutionsArray_ removeAllObjects];
    
    for (ServiceDetail *serviceDetail in [service_ serviceDetailList]) {
        
        [companiesArray_ addObject:[serviceDetail company]];
        
    }
    
    [companyComboButton_ setOptionStringList:[NSArray arrayWithArray:companiesArray_]];    
    [companyComboButton_ setNoSelectionText:NSLocalizedString(PUBLIC_SERVICE_STEP_ONE_SELECT_BUSINESS_TITLE_TEXT_KEY, nil)];

    
    if (!hasNavigateForward_) {
            
        [companyComboButton_ setSelectedIndex:-1]; //makes the combo refresh
        
        if ([companiesArray_ count] == 1) {
            
            [process_ setSelectedCompanyIndex:0];
        
        } else {
        
            [process_ setSelectedCompanyIndex:-1];
        
        }
                
    } else {
    
        if (process_ != nil) {
            
            [process_ setPsDelegate:self];

        }
        
        hasNavigateForward_ = NO;
        
    }
    
    [companyComboButton_ setSelectedIndex:[process_ selectedCompanyIndex]];
    
    [self lookForEditViews];

}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
}

/*
 * Creates and returns an autoreleased PublicServStepOneViewController constructed from a NIB file.
 */
+ (PublicServStepOneViewController *)publicServStepOneViewController {
    
    PublicServStepOneViewController *result =  [[[PublicServStepOneViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}
//[[Updater getInstance] obtainPaymentInstitutionsDetailForCod:institution.institutionParameter optionType:@"INST"];


#pragma mark -
#pragma mark MOKStringListSelectionButtonDelegate selectors

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton {
    
    NSInteger selectedIndex = [companyComboButton_ selectedIndex];
    NSArray *serviceDetailList = [service_ serviceDetailList];

    if ((selectedIndex >= 0) && (selectedIndex < [companiesArray_ count]) && (selectedIndex < [serviceDetailList count])) {
        
        companySelected_ = [companiesArray_ objectAtIndex:selectedIndex];
        [process_ setSelectedCompany:companySelected_];
        [process_ setSelectedCompanyIndex:[companyComboButton_ selectedIndex]];
        
        ServiceDetail *serviceDetail = [serviceDetailList objectAtIndex:selectedIndex];
        
        [process_ setSelectedCompanyCode:[serviceDetail code]];
        [process_ setSelectedCompanyParameter:[serviceDetail param]];
        
    } else {
        
        [process_ setSelectedCompany:@""];
        [process_ setSelectedCompanyIndex:-1];
        [process_ setSelectedCompanyCode:@""];
        
    }
    
}

#pragma mark -
#pragma mark User interaction

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped {
    
    [process_ startPaymentInitialRequest];

}

#pragma mark -
#pragma mark Setters

/*
 * Sets the selected service
 */
- (void)setService:(Service *)service {

    if (service_ != service) {
        
        [service retain];
        [service_ release];
        service_ = service;
        
    }

}

/*
 * Sets the process
 */
- (void)setProcess:(PaymentPSProcess *)process {

    if (process_ != process) {
        
        [process retain];
        [process_ release];
        process_ = process;
        
    }

    [process_ setPsDelegate:self];
    
}

#pragma mark -
#pragma mark PaymentPSProcessDelegate

/*
 * Delegate is notified when the initialization has finished 
 */
- (void)initilizationHasFinished:(BOOL)isNextDynamicView{
    
    if (!isCable_) {
        hasNavigateForward_ = YES;
        
        if (isNextDynamicView) {
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            
            NSString *parameters = process_.selectedCompanyParameter;
            NSString *indicadorPF = @"N";
            NSArray *informationArray = [NSArray array];
            
            [userDefault setObject:parameters forKey:@"Parametros"];
            
            informationArray = [parameters componentsSeparatedByString:@"$"];
            
            for (int i = 0; i < [informationArray count]; i++) {
                
                NSString *data = [informationArray objectAtIndex:i];
                
                if ([@"*" isEqualToString:data]) {
                    if ( i+1 < [informationArray count]) {
                        indicadorPF = [informationArray objectAtIndex:i+1];
                        break;
                    }
                }
            }
            
            [userDefault setObject:indicadorPF forKey:@"IndicatorPF"];
            [userDefault synchronize];            
            
            if (publicServInstCompStepTwoViewController_ != nil) {
                [publicServInstCompStepTwoViewController_ release];
                publicServInstCompStepTwoViewController_ = nil;
            }
            
            publicServInstCompStepTwoViewController_ = [[InsititutionAndCompaniesStepTwoViewController insititutionAndCompaniesStepTwoViewController] retain];
            
            [publicServInstCompStepTwoViewController_ setServiceType:serviceType_];
            [publicServInstCompStepTwoViewController_ setResponseInformation:process_.responseInformation];
            [self.navigationController pushViewController:publicServInstCompStepTwoViewController_
                                                 animated:YES];
            
        }else{
            if (publicServStepTwoViewController_ == nil) {
                
                publicServStepTwoViewController_ = [[PublicServStepTwoViewController publicServStepTwoViewController] retain];
                
            }
            
            [publicServStepTwoViewController_ setProcess:process_];
            [self.navigationController pushViewController:publicServStepTwoViewController_
                                                 animated:YES];
        }
    }else{

        InstitutionList *institutionList = [[process_ responseInitialResponse] institutionList];
        
        if (institutionsArray_ == nil) {
            institutionsArray_ = [[NSMutableArray alloc] init];
        }
        
        [institutionsArray_ removeAllObjects];
        [institutionsArray_ addObjectsFromArray:[institutionList institutionList]];
        
        if ([institutionsArray_ count] == 0) {
            [Tools showInfoWithMessage:@"No existen convenios para listar"];
        }
        
        if (publicServDirectTVStepTwoViewController_ != nil) {
            [publicServDirectTVStepTwoViewController_ release];
            publicServDirectTVStepTwoViewController_ = nil;
        }
        
        publicServDirectTVStepTwoViewController_ = [[PublicServDirectTVStepTwoViewController publicServDirectTVStepTwoViewController] retain];
        
        [publicServDirectTVStepTwoViewController_ setServiceType:companySelected_];
        [publicServDirectTVStepTwoViewController_ setInstitutionsArray:institutionsArray_];
        [self.navigationController pushViewController:publicServDirectTVStepTwoViewController_
                                             animated:YES];
        
    }
    
}

/**
 * Search list response.
 *
 * @param notification: data send by the notification
 * @private
 */
- (void)institutionDetailResponse:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationPaymentInstitutionsDetailResult object:nil];
    
    PaymentInstitutionsAndCompaniesDetailResponse *response = [notification object];
    
    [[self appDelegate] hideActivityIndicator];
    
    if (response !=nil && [response isKindOfClass:[PaymentInstitutionsAndCompaniesDetailResponse class]] && !response.isError) {
        
        hasNavigateForward_ = YES;
        
        if (publicServInstCompStepTwoViewController_ != nil) {
            [publicServInstCompStepTwoViewController_ release];
            publicServInstCompStepTwoViewController_ = nil;
        }
        publicServInstCompStepTwoViewController_ = [[InsititutionAndCompaniesStepTwoViewController insititutionAndCompaniesStepTwoViewController] retain];
        
        publicServInstCompStepTwoViewController_.responseInformation = response;
        
        [self.navigationController pushViewController:publicServInstCompStepTwoViewController_ animated:TRUE];
    }
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    result.mainTitle = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY,nil)];
    
    return result;
    
}

@end
