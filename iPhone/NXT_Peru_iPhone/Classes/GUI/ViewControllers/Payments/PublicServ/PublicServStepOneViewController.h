/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKEditableViewController.h"
#import "MOKStringListSelectionButton.h"
#import "PaymentPSProcess.h"

@class MOKStringListSelectionButton;
@class PaymentPSProcess;
@class InsititutionAndCompaniesStepTwoViewController;
@class PublicServDirectTVStepTwoViewController;
@class PublicServStepTwoViewController;
@class SimpleHeaderView;
@class Service;

/**
 * View controller to show public services payment step one.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PublicServStepOneViewController : MOKEditableViewController <MOKStringListSelectionButtonDelegate, PaymentPSProcessDelegate>{
    
@private
    
#pragma Graphic
    
    /**
     * Header
     */
    SimpleHeaderView *header_;
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Company combo button
     */
    MOKStringListSelectionButton *companyComboButton_;
    
    /**
     * Continue button
     */
    UIButton *continueButton_;
    
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    
#pragma Logic
    /**
     * Service
     */
    Service *service_;
    
    /**
     * Service type
     */
    NSString *serviceType_;
    
    /**
     * Selected company
     */
    NSString *companySelected_;
    
    /**
     * Navigation flag
     */
    BOOL hasNavigateForward_;
    
    /**
     * Companies array
     */
    NSMutableArray *companiesArray_;
    
    /**
     * Process for PS Electric Services
     */
    PaymentPSProcess *process_;
    
    /**
     * Public Service Step Two View Controller
     */
    PublicServStepTwoViewController *publicServStepTwoViewController_;
    
    /**
     * Public Service Step Two View Controller for direct tv
     */
    PublicServDirectTVStepTwoViewController *publicServDirectTVStepTwoViewController_;
    
    /**
     * Public Service Step Two View Controller
     */
    InsititutionAndCompaniesStepTwoViewController *publicServInstCompStepTwoViewController_;
    
    /**
     *  To know if is cable company and show the table
     */
    BOOL isCable_;
    
    /**
     *  array with institutions information
     */
    NSMutableArray *institutionsArray_;
}

/**
 * Provides readwrite access to the titleLabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides readwrite access to the companyButton and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet MOKStringListSelectionButton *companyComboButton;

/**
 * Provides readwrite access to the continueButton and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIButton *continueButton;

/**
 * Provides readwrite access to the brandingLine and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *brandingLine;

/**
 * Provides readwrite access to the service
 */
@property (nonatomic,readwrite, retain) Service *service;

/**
 * Provides readwrite access to the service
 */
@property (nonatomic,readwrite, copy) NSString *serviceType;

/**
 * Provides read-write access to the intitutionsArray
 */
@property (retain, readwrite, nonatomic) NSMutableArray *institutionsArray;

/**
 * Provides readwrite access to the hasNavigateForward
 */
@property (nonatomic,readwrite, assign) BOOL hasNavigateForward;

/**
 *  To know if is cable company and show the table
 */
@property (nonatomic,readwrite, assign) BOOL isCable;

/**
 * Provides readwrite access to the processPSEletricServices
 */
@property (nonatomic,readwrite, retain) PaymentPSProcess *process;

/**
 * Creates and returns an autoreleased PublicServStepOneViewController constructed from a NIB file.
 *
 * @return The autoreleased PublicServStepOneViewController constructed from a NIB file.
 */
+ (PublicServStepOneViewController *)publicServStepOneViewController;

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped;

@end
