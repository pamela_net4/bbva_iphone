/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "PublicServStepTwoViewController.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKDoubleLabel.h"
#import "MOKEditableViewController.h"
#import "MOKEditableViewController+protected.h"
#import "MOKStringListSelectionButton.h"
#import "MOKTextField.h"
#import "MOKNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentBaseProcess.h"
#import "PaymentCell.h"
#import "PaymentPSProcess.h"
#import "PublicServiceHeaderView.h"
#import "PublicServiceResponse.h"
#import "PublicServStepThreeViewController.h"
#import "Session.h"
#import "Service.h"
#import "ServiceDetail.h"
#import "StatusEnabledResponse.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "Updater.h"


/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"PublicServStepTwoViewController"


#pragma mark -

/**
 * PublicServStepTwoViewController private extension
 */
@interface PublicServStepTwoViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releasePublicServStepTwoViewControllerGraphicElements;

/**
 * Relocate the view components depending on the process type
 */
- (void)relocateView;

@end


#pragma mark -

@implementation PublicServStepTwoViewController

#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;
@synthesize continueButton = continueButton_;
@synthesize supplyTextField = supplyTextField_;
@synthesize service = service_;
@synthesize serviceType = serviceType_;
@synthesize brandingLine = brandingLine_;
@synthesize hasNavigateForward = hasNavigateForward_;
@synthesize process = process_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releasePublicServStepTwoViewControllerGraphicElements];
    
    [service_ release];
    service_ = nil;
    
    [serviceType_ release];
    serviceType_ = nil;
    
    [companySelected_ release];
    companySelected_ = nil;
    
    [process_ release];
    process_ = nil;
    
    [publicServStepThreeViewController_ release];
    publicServStepThreeViewController_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasePublicServStepTwoViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releasePublicServStepTwoViewControllerGraphicElements {
    
	[header_ release];
    header_ = nil;

	[titleLabel_ release];
    titleLabel_ = nil;
    
    [supplyTextField_ release];
    supplyTextField_ = nil;
    
    [continueButton_ release];
    continueButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    CGRect mainFrame = [[UIScreen mainScreen] bounds];
    
    [[self view] setFrame:mainFrame];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
    header_ = [[PublicServiceHeaderView publicServiceHeaderView] retain];
    
    [[self scrollableView] addSubview:header_];
        
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ 
                         withFontSize:14.0f 
                                color:[UIColor BBVABlueSpectrumColor]];

    [supplyTextField_ setDelegate:self];
    [supplyTextField_ setTextAlignment:UITextAlignmentLeft];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:supplyTextField_ withFontSize:14.0f andColor:[UIColor grayColor]];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:continueButton_];
    [continueButton_ setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil) 
                     forState:UIControlStateNormal];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    CGRect frame = [[self view] frame];
    
    CGRect scrollFrame = [[self scrollableView] frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingLine] frame]) - CGRectGetMinY(frame);
    [self setScrollFrame:scrollFrame];
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releasePublicServStepTwoViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    [self.appDelegate setTabBarVisibility:NO animated:YES];

    NSString *title = @"";
    
    if ([process_ paymentOperationType] == PTEPaymentPSElectricServices) {
        title = NSLocalizedString(PUBLIC_SERVICE_ELECTRICITY_TEXT_KEY, nil);
    } else if ([process_ paymentOperationType] == PTEPaymentPSWaterServices) {
        title = NSLocalizedString(PUBLIC_SERVICE_WATER_HEADER_TEXT_KEY, nil);
    } else if ([process_ paymentOperationType] == PTEPaymentPSPhone) {
        title = [NSString stringWithFormat:NSLocalizedString(PUBLIC_SERVICE_SERVICES_OF_TEXT_KEY, nil), NSLocalizedString(PUBLIC_SERVICE_NOT_CAP_LANDLINE_TEXT_KEY, nil)];
    } else if ([process_ paymentOperationType] == PTEPaymentPSCellular) {
        title = [NSString stringWithFormat:NSLocalizedString(PUBLIC_SERVICE_SERVICES_OF_TEXT_KEY, nil), NSLocalizedString(PUBLIC_SERVICE_CELLULAR_TEXT_LOWER_KEY, nil)];
    }
    
    [header_ setTitle:title];
    [header_ setCompanyName:[process_ selectedCompany]];
    
    switch ([process_ paymentOperationType]) {
        case PTEPaymentPSElectricServices:
        case PTEPaymentPSWaterServices:
            
            [titleLabel_ setText:[NSString stringWithFormat:@"%@", NSLocalizedString(PUBLIC_SERVICE_STEP_ONE_ENTRY_NUMBER_OF_SUPPLY_KEY, nil)]];

            break;
            
        default:
            
            [titleLabel_ setText:[NSString stringWithFormat:@"%@", NSLocalizedString(PUBLIC_SERVICE_STEP_ONE_ENTRY_PHONE_NUMBER_OR_CLIENT_CODE_KEY, nil)]];

            break;
    }
    
    [self relocateView];
        
    if (!hasNavigateForward_) {
        
        [supplyTextField_ setText:@""];
        [process_ setSelectedLocalityIndex:-1];
        
    } else {
    
        hasNavigateForward_ = NO;
        [supplyTextField_ setText:[process_ supplies]];
        
    }
    
    [self lookForEditViews];
    
    if (process_ != nil) {
        [process_ setDelegate:self];
    }
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [process_ setDelegate:nil];
    [supplyTextField_ resignFirstResponder];
    
}

/*
 * Creates and returns an autoreleased PublicServStepTwoViewController constructed from a NIB file.
 */
+ (PublicServStepTwoViewController *)publicServStepTwoViewController {
    
    PublicServStepTwoViewController *result = [[[PublicServStepTwoViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark PaymentBaseProcessDelegate selectors

/**
 * Delegate is notified when the data analysis has finished 
 */
- (void)dataAnalysisHasFinished {

    if (publicServStepThreeViewController_ == nil) {
    
        publicServStepThreeViewController_ = [[PublicServStepThreeViewController publicServStepThreeViewController] retain];
        
    }
    
    hasNavigateForward_ = YES;
    
    [publicServStepThreeViewController_ setProcess:process_];
    [publicServStepThreeViewController_ setHasNavigateForward:NO];
    
    [[self navigationController] pushViewController:publicServStepThreeViewController_ 
                                           animated:YES];
    
}

#pragma mark -
#pragma mark TextField Delegate

/**
 * Asks the delegate if the specified text should be changed.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
	BOOL result = YES;
	
	NSUInteger newLength = [textField.text length] + [string length] - range.length;
	NSCharacterSet *numbersSet = [NSCharacterSet decimalDigitCharacterSet];
    
	result = [Tools isValidText:string forCharacterSet:numbersSet];
    
	if (result) {
        
		NSInteger maxCharacters = [[process_ companyConfiguration] maxLong];
			
        if (newLength > maxCharacters) {
            result = NO;
        }

	}
	
	return result;
	
}

#pragma mark -
#pragma mark User interaction

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped {

    [process_ setSupplies:[supplyTextField_ text]];
    [process_ startPaymentDataRequest];

}

#pragma mark -
#pragma mark View format

/**
 * Relocate the view components depending on the process type
 */
- (void)relocateView {

    CGFloat yPosition = header_.frame.origin.y + header_.frame.size.height;
    
    CGRect frame = titleLabel_.frame;
    frame.origin.y = yPosition + 15.0f;
    titleLabel_.frame = frame;
    yPosition += 15.0f + frame.size.height;
    
    frame = supplyTextField_.frame;
    frame.origin.y = yPosition + 10.0f;
    supplyTextField_.frame = frame;
    yPosition += 10.0f + frame.size.height;
    
    frame = continueButton_.frame;
    frame.origin.y = yPosition + 20.0f;
    continueButton_.frame = frame;
    
}

#pragma mark -
#pragma mark Setters

/*
 * Sets the selected service
 */
- (void)setService:(Service *)service {

    if (service_ != service) {
        
        [service retain];
        [service_ release];
        service_ = service;
        
    }

}

/*
 * Sets the process
 */
- (void)setProcess:(PaymentPSProcess *)process {
    
    if (process_ != process) {
        
        [process_ setDelegate:nil];
        [process retain];
        [process_ release];
        process_ = process;

    }
    
    [process_ setDelegate:self];
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    result.mainTitle = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY, nil)];
    
    return result;
    
}

@end
