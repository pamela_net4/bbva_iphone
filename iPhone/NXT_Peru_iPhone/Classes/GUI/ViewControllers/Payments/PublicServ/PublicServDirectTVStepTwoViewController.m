//
//  PublicServDirectTVStepTwoViewController.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 12/10/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "PublicServDirectTVStepTwoViewController.h"

#import "Institution.h"
#import "InsititutionAndCompaniesStepTwoViewController.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKEditableViewController+protected.h"
#import "UINavigationItem+DoubleLabel.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentInstitutionsAndCompaniesDetailResponse.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"
#import "Updater.h"


#pragma mark -
#pragma mark Static attributes
/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"PublicServDirectTVStepTwoViewController"

@interface PublicServDirectTVStepTwoViewController ()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releasePublicServDirectTVStepTwoViewControllerGraphicElements;

/**
 * Search list response.
 *
 * @param notification: data send by the notification
 * @private
 */
- (void)institutionDetailResponse:(NSNotification *)notification;

@end

@implementation PublicServDirectTVStepTwoViewController
#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;
@synthesize companyComboButton = companyComboButton_;
@synthesize continueButton = continueButton_;
@synthesize institutionsArray = institutionsArray_;
@synthesize serviceType = serviceType_;
@synthesize brandingLine = brandingLine_;
@synthesize hasNavigateForward = hasNavigateForward_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releasePublicServDirectTVStepTwoViewControllerGraphicElements];

    [serviceType_ release];
    serviceType_ = nil;
    
    [companySelected_ release];
    companySelected_ = nil;

    [institutionsArray_ release];
    institutionsArray_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasePublicServDirectTVStepTwoViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releasePublicServDirectTVStepTwoViewControllerGraphicElements {
    
	[header_ release];
    header_ = nil;
    
	[titleLabel_ release];
    titleLabel_ = nil;
    
    [companyComboButton_ release];
    companyComboButton_ = nil;
    
    [continueButton_ release];
    continueButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    if (institutionsArray_ == nil) {
        institutionsArray_ = [[NSMutableArray alloc] init];
    }
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    CGRect mainFrame = [[UIScreen mainScreen] bounds];
    
    [[self view] setFrame:mainFrame];
    
    header_ = [[SimpleHeaderView simpleHeaderView] retain];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
    [[self view] addSubview:header_];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_
                         withFontSize:14.0f
                                color:[UIColor BBVABlueSpectrumColor]];
    
    
    [NXT_Peru_iPhoneStyler styleBlueButton:continueButton_];
    [continueButton_ setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil)
                     forState:UIControlStateNormal];
    
    
    [NXT_Peru_iPhoneStyler styleStringListButton:companyComboButton_];
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    CGRect frame = [[self view] frame];
    
    CGRect scrollFrame = [[self scrollableView] frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingLine] frame]) - CGRectGetMinY(frame);
    [self setScrollFrame:scrollFrame];
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releasePublicServDirectTVStepTwoViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    [companyComboButton_ setStringListSelectionButtonDelegate:self];
    
    [header_ setTitle:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(PAYMENT_RECHARGE_BUSINESS_TEXT_KEY, nil), serviceType_]] ;
    
    [titleLabel_ setText:[NSString stringWithFormat:@"%@", NSLocalizedString(@"Servicio", nil)]];
    
    NSMutableArray *companiesArray_ = [[NSMutableArray alloc] init];
    
    for (Institution *institution in institutionsArray_) {
        
        [companiesArray_ addObject:institution.institutionDescription];
        
    }
    
    [companyComboButton_ setOptionStringList:[NSArray arrayWithArray:companiesArray_]];
    [companyComboButton_ setNoSelectionText:NSLocalizedString(PUBLIC_SERVICE_STEP_ONE_SELECT_BUSINESS_TITLE_TEXT_KEY, nil)];
    
    
    if (!hasNavigateForward_) {
        
        [companyComboButton_ setSelectedIndex:-1]; //makes the combo refresh
        
        if ([institutionsArray_ count] == 1) {
            
            selectedIndex_ = 0;
            
        } else {
            
            selectedIndex_ = -1;
            
        }
        
    } else {
        
        hasNavigateForward_ = NO;
        
    }
    
    [companyComboButton_ setSelectedIndex:selectedIndex_];
    
    [self lookForEditViews];
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

/*
 * Creates and returns an autoreleased PublicServStepOneViewController constructed from a NIB file.
 */
+ (PublicServDirectTVStepTwoViewController *)publicServDirectTVStepTwoViewController {
    
    PublicServDirectTVStepTwoViewController *result =  [[[PublicServDirectTVStepTwoViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}



#pragma mark -
#pragma mark MOKStringListSelectionButtonDelegate selectors

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton {
    
    selectedIndex_ = [stringListSelectionButton selectedIndex];
    
}

#pragma mark -
#pragma mark User interaction

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped {
    
    if (selectedIndex_ >= 0) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];
    
        Institution *institution = [institutionsArray_ objectAtIndex:selectedIndex_];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(institutionDetailResponse:) name:kNotificationPaymentInstitutionsDetailResult object:nil];
        
        [[Updater getInstance] obtainPaymentInstitutionsDetailForCod:institution.institutionParameter optionType:@"SERV"];
    }
    
}

#pragma mark -
#pragma mark Institutions response notification
/**
 * Search list response.
 *
 * @param notification: data send by the notification
 * @private
 */
- (void)institutionDetailResponse:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationPaymentInstitutionsDetailResult object:nil];
    
    PaymentInstitutionsAndCompaniesDetailResponse *response = [notification object];
    
    [[self appDelegate] hideActivityIndicator];
    
    if (response != nil && [response isKindOfClass:[PaymentInstitutionsAndCompaniesDetailResponse class]] && !response.isError) {
        
        Institution *institution = [institutionsArray_ objectAtIndex:selectedIndex_];
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        
        NSString *parameters = institution.institutionParameter;
        
        NSString *indicadorPF = @"N";
        NSArray *informationArray = [NSArray array];
        
        [userDefault setObject:parameters forKey:@"Parametros"];
        
        informationArray = [parameters componentsSeparatedByString:@"$"];
        
        for (int i = 0; i < [informationArray count]; i++) {
            
            NSString *data = [informationArray objectAtIndex:i];
            
            if ([@"*" isEqualToString:data]) {
                if ( i+1 < [informationArray count]) {
                    indicadorPF = [informationArray objectAtIndex:i+1];
                    break;
                }
            }
        }
        
        [userDefault setObject:indicadorPF forKey:@"IndicatorPF"];
        [userDefault synchronize];
        
        hasNavigateForward_ = YES;
        
        if (publicServInstCompStepTwoViewController_ != nil) {
            [publicServInstCompStepTwoViewController_ release];
            publicServInstCompStepTwoViewController_ = nil;
        }
        publicServInstCompStepTwoViewController_ = [[InsititutionAndCompaniesStepTwoViewController insititutionAndCompaniesStepTwoViewController] retain];
        
        publicServInstCompStepTwoViewController_.responseInformation = response;
        
        [publicServInstCompStepTwoViewController_ setServiceType:serviceType_];
        
        [self.navigationController pushViewController:publicServInstCompStepTwoViewController_ animated:TRUE];
    }

}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    result.mainTitle = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY,nil)];
    
    return result;
    
}

@end
