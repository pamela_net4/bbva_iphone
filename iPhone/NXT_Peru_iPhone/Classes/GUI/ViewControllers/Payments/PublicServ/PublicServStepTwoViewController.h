/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKEditableViewController.h"

#import "PaymentBaseProcess.h"


//Forward declarations
@class MOKStringListSelectionButton;
@class MOKTextField;
@class PaymentPSProcess;
@class PublicServiceHeaderView;
@class PublicServStepThreeViewController;
@class Service;


/**
 * View controller to show public services payment step two.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PublicServStepTwoViewController : MOKEditableViewController <UITextFieldDelegate, PaymentBaseProcessDelegate> {			
    
@private
    
#pragma Graphic
    
    /**
     * Header
     */
    PublicServiceHeaderView *header_;
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Supply text field
     */
    MOKTextField *supplyTextField_;
    
    /**
     * Continue button
     */
    UIButton *continueButton_;
    
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    
#pragma Logic
    /**
     * Service
     */
    Service *service_;
    
    /**
     * Service type
     */
    NSString *serviceType_;
    
    /**
     * Selected company
     */
    NSString *companySelected_;
    
    /**
     * Navigation flag
     */
    BOOL hasNavigateForward_;
    
    /**
     * Process
     */
    PaymentPSProcess *process_;
    
    /**
     * Public Service Step Three View Controller
     */
    PublicServStepThreeViewController *publicServStepThreeViewController_;
}

/**
 * Provides readwrite access to the titleLabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides readwrite access to the supplyTextField and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet MOKTextField *supplyTextField;

/**
 * Provides readwrite access to the continueButton and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIButton *continueButton;

/**
 * Provides readwrite access to the brandingLine and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *brandingLine;

/**
 * Provides readwrite access to the service
 */
@property (nonatomic,readwrite, retain) Service *service;

/**
 * Provides readwrite access to the service
 */
@property (nonatomic,readwrite, copy) NSString *serviceType;

/**
 * Provides readwrite access to the hasNavigateForward
 */
@property (nonatomic,readwrite, assign) BOOL hasNavigateForward;

/**
 * Provides readwrite access to the process
 */
@property (nonatomic,readwrite, retain) PaymentPSProcess *process;

/**
 * Creates and returns an autoreleased PublicServStepTwoViewController constructed from a NIB file.
 *
 * @return The autoreleased PublicServStepTwoViewController constructed from a NIB file.
 */
+ (PublicServStepTwoViewController *)publicServStepTwoViewController;

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped;

@end
