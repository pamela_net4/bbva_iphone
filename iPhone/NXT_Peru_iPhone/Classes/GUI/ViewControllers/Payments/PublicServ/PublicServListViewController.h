/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTViewController.h"

@class PublicServStepOneViewController;

/**
 * View controller to show all public services options.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PublicServListViewController : NXTViewController<UITableViewDelegate, UITableViewDataSource> {			
    
@private
    
    /**
     * Table view
     */
    UITableView *table_;
    
    /**
     * Services list
     */
    NSMutableArray *servicesList_;
    
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    
    /**
     * Public Service Step One View Controller
     */
    PublicServStepOneViewController *publicServStepOneViewController_;
    
}

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *table;

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingLine;

/**
 * Provides read-write access to service list
 */
@property (nonatomic, readwrite, retain) NSArray *servicesList;

/**
 * Creates and returns an autoreleased PublicServListViewController constructed from a NIB file.
 *
 * @return The autoreleased PublicServListViewController constructed from a NIB file.
 */
+ (PublicServListViewController *)publicServListViewController;



@end
