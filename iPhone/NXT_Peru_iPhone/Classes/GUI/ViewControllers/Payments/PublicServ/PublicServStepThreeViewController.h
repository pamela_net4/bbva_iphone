/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "PaymentDataBaseViewController.h"
#import "PaymentBaseProcess.h"
#import "MOKStringListSelectionButton.h"

@class FOOperationHelper;
@class FrequentOperationHeaderView;
@class MOKStringListSelectionButton;
@class PaymentBaseProcess;
@class PaymentPSElectServProcess;
@class PaymentPSProcess;
@class SimpleHeaderView;

/**
 * View controller to show public services payment step one.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PublicServStepThreeViewController : PaymentDataBaseViewController <UITableViewDataSource, UITableViewDelegate, MOKStringListSelectionButtonDelegate>{			
    
@private
    
#pragma Graphic
    
    /**
     * Payments table
     */
    UITableView *paymentsTable_;
    
    /**
     * Payment methods table
     */
    UITableView *paymentMethodsTable_;
    
    /**
     * Frequent operation header information
     */
    FrequentOperationHeaderView *informationHeaderView_;
    
    /**
     * First view header
     */
    SimpleHeaderView *frequenOperationViewHeader_;
    
    /**
     * First view header
     */
    SimpleHeaderView *firstViewHeader_;
    
    /**
     * Second view header
     */
    SimpleHeaderView *secondViewHeader_;
    
    /**
     * Third view header
     */
    SimpleHeaderView *thirdViewHeader_;
    
    /**
     * First view separator
     */
    UIImageView *firstViewSeparator_;
    
    /**
     * Second view separator
     */
    UIImageView *secondViewSeparator_;
    
    /**
     * Third view separator
     */
    UIImageView *thirdViewSeparator_;
    
    /**
     * Account combo
     */
    MOKStringListSelectionButton *accontCombo_;
    
    /**
     * Cell account combo.
     */
    MOKStringListSelectionButton *cellAccountCombo_;
    
    /**
     * Cell card combo.
     */
    MOKStringListSelectionButton *cellCardCombo_;
    
    
#pragma Logic
    
    /**
     * Labels array
     */
    NSMutableArray *labelsArray_;
    
    /**
     * Payments array
     */
    NSMutableArray *selectedPaymentsArray_;
    
    /**
     * First view height
     */
    CGFloat firstViewHeight_;
    
    /**
     * Second view height
     */
    CGFloat secondViewHeight_;
    
    /**
     * Third view height
     */
    CGFloat thirdViewHeight_;
    
    /**
     * Process
     */
    PaymentPSProcess *processPS_;
    
}

/**
 * Provides read-write access to the paymentsTable and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *paymentsTable;

/**
 * Provides read-write access to the paymentsTable and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *paymentMethodsTable;

/**
 * Provides read-write access to the accontCombo and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *accontCombo;

/**
 * Creates and returns an autoreleased PublicServStepThreeViewController constructed from a NIB file.
 *
 * @return The autoreleased PublicServStepThreeViewController constructed from a NIB file.
 */
+ (PublicServStepThreeViewController *)publicServStepThreeViewController;


@end
