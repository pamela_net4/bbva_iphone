//
//  InstitutionsAndCompaniesStepThirdViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 27/11/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//


#import <AddressBookUI/AddressBookUI.h>
#import "NXTComboButton.h"
#import "MOKEditableViewController.h"
#import "NXTEditableViewController.h"
#import "MOKStringListSelectionButton.h"
#import "SMSMokCell.h"
#import "EmailMokCell.h"

@class accountPay;
@class CardType;
@class CheckComboCell;
@class InstitutionsAndCompaniesStepFourViewController;
@class MOKCurrencyTextField;
@class PaymentInstitutionAndCompaniesConfirmationResponse;
@class PaymentInstitutionAndCompaniesConfirmationInformationResponse;
@class PendingDocumentList;
@class SimpleHeaderView;

@interface InstitutionsAndCompaniesStepThirdViewController : MOKEditableViewController <SMSMokCellDelegate, EmailMokCellDelegate,UITableViewDataSource,UITableViewDelegate,MOKStringListSelectionButtonDelegate, ABPeoplePickerNavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate, NXTComboButtonDelegate>
{
    PendingDocumentList *pendingDocumentList;
    
    /**
     *  index of the card selected
     */
    int cardSelectedIndex;
    
    /**
     *  When the payment is partial or not
     */
    BOOL isPartial;
    
    /**
     *  When the payment is with BD or no
     */
    BOOL isDB;
    
    /**
     * Accounts Cell
     */
    CheckComboCell *accountsCell_;
    
    /**
     * Cards Cell
     */
    CheckComboCell *cardsCell_;
    
    /**
     * SMS Cell
     */
    SMSMokCell *smsCell_;
    
    /**
     * Email Cell
     */
    EmailMokCell *emailCell_;
    
    /**
     * Amount field
     */
    MOKCurrencyTextField *currencyText;
    
    /**
     * Combo with accounts
     */
    MOKStringListSelectionButton *accountsCombo;
    
    /**
     *  Array to know the selcteds documents
     */
    NSMutableArray *brand;
    
    /**
     *  array with the pending documents 
     */
    NSArray *pendingDocumentsArray;
    
    /**
     *  response
     */
    PaymentInstitutionAndCompaniesConfirmationInformationResponse *responseConfirmation_;
    
    /**
     * Send email flag
     */
    BOOL sendSMS_;
    
    /**
     * ITF selected
     */
    BOOL itfSelected_;
    
    /**
     * Adding first contact flag
     */
    BOOL addingFirstSMS_;
    
    /**
     * Adding second contact flag
     */
    BOOL addingSecondSMS_;
    
    /**
     * Adding first email flag
     */
    BOOL addingFirstEmail_;
    
    /**
     * Adding first email flag
     */
    BOOL addingSecondEmail_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationSMS1_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationSMS2_;
    
    /**
     * Show SMS 1 flag
     */
    BOOL showSMS1_;
    
    /**
     * Show SMS 2 flag
     */
    BOOL showSMS2_;
    
    /**
     * Send email flag
     */
    BOOL sendEmail_;
    
    /*
     *selected carrier one index
     */
    NSInteger selectedCarrier1Index_;
    
    /*
     *selected carrier two index
     */
    NSInteger selectedCarrier2Index_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationEmail1_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationEmail2_;
    
    /**
     * Show email 1 flag
     */
    BOOL showEmail1_;
    
    /**
     * Show email 2 flag
     */
    BOOL showEmail2_;
    
    /**
     * User selected email message
     */
    NSString *emailMessage_;
    
    /**
     *  Card to pay
     */
    CardType *selectedCard;
    
    /**
     *  Account to pay
     */
    accountPay *selectedAccount;
    
    /**
     *  next view to display the confirm info
     */
    InstitutionsAndCompaniesStepFourViewController *transferConfirmationModalViewController_;
    
    /**
     *  table for sms and email
     */
    UITableView *notificationTable;
    
    /**
     * Adding first email flag
     */
    BOOL hasNavigateForward_;
    
    MOKStringListSelectionButton *cellAccountCombo_;
    
    MOKStringListSelectionButton *cellCardCombo_;
    
    SimpleHeaderView *simpleNotificationHeaderView;
    
    UIScrollView *scrollView;
    
    UIButton *btnConfirmation;
    
    /**
     * Service name
     */
    NSString *serviceType_;
    
    /**
     * Separator line
     */
    UIImageView *separator_;

    /**
     *
     */
    UITableView *selectionTableView_;
    
    /**
     * Frame to show when back from contacts list
     */
    CGRect lastActiveFrame_;
    
    /**
     *  Flag to know if has forward to contact list
     */
    BOOL isContactListShowed_;
}

/**
 *  Cell with accounts list
 */
@property(nonatomic, readwrite, retain) CheckComboCell *accountsCell;

/**
 *  Cell with cards list
 */
@property(nonatomic, readwrite, retain) CheckComboCell *cardsCell;

/**
 *  Entity with the response information
 */
@property (nonatomic, readwrite, retain)  PaymentInstitutionAndCompaniesConfirmationResponse *paymentDetailEntity;

/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, assign) BOOL hasNavigateForward;

/**
 * Provides read-write access to the user selected destinationSMS1 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationSMS1;

/**
 * Provides read-write access to the user selected destinationSMS2 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationSMS2;

/**
 * Provides read-write access to the user selected showSMS1
 */
@property (nonatomic, readwrite, assign) BOOL showSMS1;

/**
 * Provides read-write access to the user selected showSMS2
 */
@property (nonatomic, readwrite, assign) BOOL showSMS2;

/**
 * Provides read-write access to the send email flag
 */
@property (nonatomic, readwrite, assign) BOOL sendEmail;

/**
 * Provides read-write access to the send sms flag
 */
@property (nonatomic, readwrite, assign) BOOL sendSMS;

/**
 * Provides read-write access to the selected carrier one index
 */
@property (nonatomic, readwrite, assign) NSInteger selectedCarrier1Index;

/**
 * Provides read-write access to the selected carrier two index
 */
@property (nonatomic, readwrite,assign) NSInteger selectedCarrier2Index;

/**
 * Provides read-write access to the user selected destinationEmail1 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationEmail1;

/**
 * Provides read-write access to the user selected destinationEmail2 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationEmail2;

/**
 * Provides read-write access to the user selected showEmail1
 */
@property (nonatomic, readwrite, assign) BOOL showEmail1;

/**
 * Provides read-write access to the user selected showEmail2
 */
@property (nonatomic, readwrite, assign) BOOL showEmail2;

/**
 * Provides read-write access to the user selected email message
 */
@property (nonatomic, readwrite, retain) NSString *emailMessage;

/**
 * Provides readwrite Bottom border IBOutlet
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingLine;

/**
 * Provides readwrite access to the service type name
 */
@property (nonatomic, readwrite, retain) NSString *serviceType;

/**
 * Provides readwrite access to the separator1. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

/**
 * Creates and returns an autoreleased InstitutionsAndCompaniesStepThirdViewController constructed from a NIB file.
 *
 * @return The autoreleased InstitutionsAndCompaniesStepThirdViewController constructed from a NIB file.
 */
+ (InstitutionsAndCompaniesStepThirdViewController *)institutionsAndCompaniesStepThirdViewController;

/**
 *  When the user tap the confirm button to complete the operation
 */
- (void)onTapConfirmButton;

/*
 * Resets the internal information.
 */
- (void)resetInformation;

@end
