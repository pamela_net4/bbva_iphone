/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "MOKEditableViewController.h"

#import "MOKStringListSelectionButton.h"
#import "PaymentBaseProcess.h"

@class CardContOwnCardViewController;
@class CardContThirdCardViewController;
@class CheckCardCell;
@class CheckComboCell;
@class PaymentCardProcess;
@class SimpleHeaderView;

/**
 * View controller to show all card payment options.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardContStepOneViewController : MOKEditableViewController <UITableViewDelegate, UITableViewDataSource, MOKStringListSelectionButtonDelegate, UITextFieldDelegate, PaymentBaseProcessDelegate> {			
    
@private
    
    /**
     * Header
     */
    SimpleHeaderView *header_;
    
    /**
     * Table view
     */
    UITableView *table_;
    
    /**
     * Own card cell
     */
    CheckComboCell *checkComboCell_;
    
    /**
     * Third card cell
     */
    CheckCardCell *checkCardCell_;
    
    /**
     * Continue button
     */
    UIButton *continueButton_;
    
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    
    /**
     * Card Cont Own Card View Controller
     */
    CardContOwnCardViewController *cardContOwnCardViewController_; 
    
    /**
     * Card Cont Third Card View Controller
     */
    CardContThirdCardViewController *cardContThirdCardViewController_;
    
#pragma logic
    
    /**
     * Active flag
     */
    NSInteger activeCellIndex_;
    
    /**
     * Cards array
     */
    NSMutableArray *cardsArray_;
    
    /**
     * Card names array
     */
    NSMutableArray *cardNamesArray_;
    
    /**
     * Card selected index
     */
    NSInteger cardSelectedIndex_;
    
    /**
     * Payment card process
     */
    PaymentCardProcess *process_;
    
    /**
     * Navigation flag
     */
    BOOL hasNavigateForward_;
    
    /**
     * First part of the card
     */
    NSString *firstPartCard_;
    
    /**
     * Second part of the card
     */
    NSString *secondPartCard_;
    
    /**
     * Third part of the card
     */
    NSString *thirdPartCard_;
    
    /**
     * Fourth part of the card
     */
    NSString *fourthPartCard_;
    
}

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UITableView *table;

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIButton *continueButton;

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIImageView *brandingLine;

/**
 * Provides read-write access to the cardsArray
 */
@property (retain, readwrite, nonatomic) NSArray *cardsArray;

/**
 * Provides read-write access to the hasNavigateForward
 */
@property (readwrite, nonatomic, assign) BOOL hasNavigateForward;

/**
 * Creates and returns an autoreleased CardContStepOneViewController constructed from a NIB file.
 *
 * @return The autoreleased CardContStepOneViewController constructed from a NIB file.
 */
+ (CardContStepOneViewController *)cardContStepOneViewController;

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped;

@end