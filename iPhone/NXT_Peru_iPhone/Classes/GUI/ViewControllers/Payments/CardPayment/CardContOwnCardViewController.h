/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "PaymentDataBaseViewController.h"
#import "PaymentBaseProcess.h"
#import "MOKStringListSelectionButton.h"

@class MOKStringListSelectionButton;
@class PaymentCCOwnCardProcess;
@class SimpleHeaderView;
@class MOKCurrencyTextField;

/**
 * View controller to show card continental own card payment.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardContOwnCardViewController : PaymentDataBaseViewController <MOKStringListSelectionButtonDelegate>{			
    
@private
    
#pragma Graphic
    
    /**
     * First view header
     */
    SimpleHeaderView *firstViewHeader_;
    
    /**
     * Card label
     */
    UILabel *cardLabel_;
    
    /**
     * Card label
     */
    UILabel *cardDetailLabel_;
    
    /**
     * Account status label
     */
    UILabel *accountStatusLabel_;
    
    /**
     * Account status bg image view
     */
    UIImageView *accountStatusBg_;
    
    /**
     * Summary label
     */
    UILabel *summaryLabel_;
    
    /**
     * Summary bg image view
     */
    UIImageView *summaryBg_;
    
    /**
     * First view separator
     */
    UIImageView *firstViewSeparator_;
    
    /**
     * Second view separator
     */
    UIImageView *secondViewSeparator_;
    
    /**
     * Second view header
     */
    SimpleHeaderView *secondViewHeader_;
    
    /**
     * Account combo
     */
    MOKStringListSelectionButton *accontCombo_;
    
    /**
     * Amount label
     */
    UILabel *amountLabel_;
    
    /**
     * Amount text field
     */
    MOKCurrencyTextField *amountTextField_;
    
    /**
     * Currency label
     */
    UILabel *currencyLabel_;
    
    /**
     * Currency combo
     */
    MOKStringListSelectionButton *currencyCombo_;
    
#pragma Logic
    
    /**
     * Labels array
     */
    NSMutableArray *labelsArray_;
    
    /**
     * First view height
     */
    CGFloat firstViewHeight_;
    
    /**
     * Second view height
     */
    CGFloat secondViewHeight_;
    
    /**
     * Process
     */
    PaymentCCOwnCardProcess *processCard_;
    
}

/**
 * Provides read-write access to the cardLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *cardLabel;

/**
 * Provides read-write access to the cardDetailLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *cardDetailLabel;

/**
 * Provides read-write access to the accountStatusLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *accountStatusLabel;

/**
 * Provides read-write access to the accountStatusBg and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *accountStatusBg;

/**
 * Provides read-write access to the summaryLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *summaryLabel;

/**
 * Provides read-write access to the summaryBg and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *summaryBg;

/**
 * Provides read-write access to the accontCombo and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *accontCombo;

/**
 * Provides read-write access to the amountLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *amountLabel;

/**
 * Provides read-write access to the amountTextField and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKCurrencyTextField *amountTextField;

/**
 * Provides read-write access to the currencyLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *currencyLabel;

/**
 * Provides read-write access to the currencyCombo and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *currencyCombo;


/**
 * Creates and returns an autoreleased CardContOwnCardViewController constructed from a NIB file.
 *
 * @return The autoreleased CardContOwnCardViewController constructed from a NIB file.
 */
+ (CardContOwnCardViewController *)cardContOwnCardViewController;


@end
