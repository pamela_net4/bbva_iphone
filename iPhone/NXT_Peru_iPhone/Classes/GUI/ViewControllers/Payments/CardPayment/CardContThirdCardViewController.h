/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "PaymentDataBaseViewController.h"
#import "PaymentBaseProcess.h"
#import "MOKStringListSelectionButton.h"

@class MOKStringListSelectionButton;
@class PaymentCCThirdCardProcess;
@class SimpleHeaderView;
@class MOKCurrencyTextField;

/**
 * View controller to show card continental third card payment.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardContThirdCardViewController : PaymentDataBaseViewController <MOKStringListSelectionButtonDelegate>{			
    
@private
    
#pragma Graphic
    
    /**
     * First view header
     */
    SimpleHeaderView *firstViewHeader_;
    
    /**
     * First view separator
     */
    UIImageView *firstViewSeparator_;
    
    /**
     * Second view separator
     */
    UIImageView *secondViewSeparator_;
    
    /**
     * Second view header
     */
    SimpleHeaderView *secondViewHeader_;
    
    /**
     * Account combo
     */
    MOKStringListSelectionButton *accontCombo_;
    
    /**
     * Amount label
     */
    UILabel *amountLabel_;
    
    /**
     * Amount text field
     */
    MOKCurrencyTextField *amountTextField_;
    
    /**
     * Currency label
     */
    UILabel *currencyLabel_;
    
    /**
     * Currency combo
     */
    MOKStringListSelectionButton *currencyCombo_;
    
#pragma Logic
    
    /**
     * Labels array
     */
    NSMutableArray *labelsArray_;
    
    /**
     * First view height
     */
    CGFloat firstViewHeight_;
    
    /**
     * Second view height
     */
    CGFloat secondViewHeight_;
    
    /**
     * Process
     */
    PaymentCCThirdCardProcess *processCard_;
    
}

/**
 * Provides read-write access to the accontCombo and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *accontCombo;

/**
 * Provides read-write access to the amountLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *amountLabel;

/**
 * Provides read-write access to the amountTextField and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKCurrencyTextField *amountTextField;

/**
 * Provides read-write access to the currencyLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *currencyLabel;

/**
 * Provides read-write access to the currencyCombo and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *currencyCombo;

/**
 * Creates and returns an autoreleased CardContThirdCardViewController constructed from a NIB file.
 *
 * @return The autoreleased CardContThirdCardViewController constructed from a NIB file.
 */
+ (CardContThirdCardViewController *)cardContThirdCardViewController;


@end
