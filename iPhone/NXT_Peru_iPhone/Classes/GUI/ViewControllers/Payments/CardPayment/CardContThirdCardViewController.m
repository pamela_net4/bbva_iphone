/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "CardContThirdCardViewController.h"

#import "BankAccount.h"
#import "Card.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKDoubleLabel.h"
#import "MOKEditableViewController.h"
#import "MOKEditableViewController+protected.h"
#import "MOKNavigationItem.h"
#import "MOKStringListSelectionButton.h"
#import "MOKCurrencyTextField.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentBaseProcess.h"
#import "PaymentCCThirdCardProcess.h"
#import "PaymentsConstants.h"
#import "Session.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "Updater.h"

/**
 * Defines the Nib file name
 */
#define NIB_FILE_NAME                                       @"CardContThirdCardViewController"

/**
 * Short vertical distance
 */
#define SHORT_VERTICAL_DISTANCE                             5.0f

/**
 * Long vertical distance
 */
#define LONG_VERTICAL_DISTANCE                              10.0f


/**
 * PaymentDataBaseViewController private extension
 */
@interface CardContThirdCardViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releaseCardContThirdCardViewControllerGraphicElements;

/**
 * Update the currency symbol on amount text field when user changes currency
 *
 * @param currency The currency name to set
 * @private
 */
- (void)updateAmountTextFieldWithCurreny:(NSString *)currency;

@end

#pragma mark -

@implementation CardContThirdCardViewController

#pragma mark -
#pragma mark Properties

@synthesize accontCombo = accontCombo_;
@synthesize amountLabel = amountLabel_;
@synthesize amountTextField = amountTextField_;
@synthesize currencyLabel = currencyLabel_;
@synthesize currencyCombo = currencyCombo_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseCardContThirdCardViewControllerGraphicElements];
    
    [labelsArray_ release];
    labelsArray_ = nil;
    
    [processCard_ release];
    processCard_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseCardContThirdCardViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseCardContThirdCardViewControllerGraphicElements {
    
    for (UILabel *label in labelsArray_) {
        
        [label removeFromSuperview];
        [label release];
        label = nil;
        
    }
    [labelsArray_ removeAllObjects];
    
    [firstViewHeader_ release];
    firstViewHeader_ = nil;
    
    [firstViewSeparator_ release];
    firstViewSeparator_ = nil;
    
    [secondViewSeparator_ release];
    secondViewSeparator_ = nil;

    [secondViewHeader_ release];
    secondViewHeader_ = nil;
    
    [accontCombo_ release];
    accontCombo_ = nil;
    
    [amountLabel_ release];
    amountLabel_ = nil;
    
    [amountTextField_ release];
    amountTextField_ = nil;
    
    [currencyLabel_ release];
    currencyLabel_ = nil;
    
    [currencyCombo_ release];
    currencyCombo_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    labelsArray_ = [[NSMutableArray alloc] init];    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleAccountSelectionButton:accontCombo_];

    [accontCombo_ setStringListSelectionButtonDelegate:self];
    [currencyCombo_ setStringListSelectionButtonDelegate:self];

    [NXT_Peru_iPhoneStyler styleMokTextField:amountTextField_ withFontSize:14.0f andColor:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleStringListButton:currencyCombo_];

    [NXT_Peru_iPhoneStyler styleNXTView:[self firstView]];
    [NXT_Peru_iPhoneStyler styleNXTView:[self secondView]];
    
    firstViewHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [firstViewHeader_ setTitle:NSLocalizedString(CARD_PAYMENT_THIRD_CARD_TEXT_KEY, nil)];
    [[self firstView] addSubview:firstViewHeader_];
    firstViewSeparator_ = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 2.0f)];
    [firstViewSeparator_ setImage:[[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
    [[self firstView] addSubview:firstViewSeparator_];

    
    secondViewHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [secondViewHeader_ setTitle:NSLocalizedString(CARD_PAYMENT_CHOOSE_PAYMENT_MODE_TEXT_KEY, nil)];
    [[self secondView] addSubview:secondViewHeader_];
    secondViewSeparator_ = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 2.0f)];
    [secondViewSeparator_ setImage:[[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
    [[self secondView] addSubview:secondViewSeparator_];
    
    [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withFontSize:14.0f color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:currencyLabel_ withFontSize:14.0f color:[UIColor BBVABlueColor]];
    
    [amountLabel_ setText:NSLocalizedString(CARD_PAYMEN_AMOUNT_TO_PAY_TEXT_KEY, nil)];
    [currencyLabel_ setText:NSLocalizedString(CARD_PAYMENT_PAYMENT_CURRENCY_TEXT_KEY, nil)];

	amountTextField_.canContainCents = YES;
    amountTextField_.maxDecimalNumbers = 2;
    amountTextField_.currencySymbol = CURRENCY_SOLES_SYMBOL;
    amountTextField_.placeholder = NSLocalizedString(CARD_PAYMEN_AMOUNT_TO_PAY_TEXT_KEY, nil);

    [amountTextField_ setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseCardContThirdCardViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    [self resetScrollToTopLeftAnimated:YES];
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
	if ([self hasNavigateForward]) {
		
		[processCard_ setDelegate:self];
        [amountTextField_ setText:[processCard_ amount]];
        [self setHasNavigateForward:NO];

	} else {
        
        [amountTextField_ setText:@""];
        
    }
        
    NSMutableArray *accountNames = [[[NSMutableArray alloc] init] autorelease];
    NSString *account = @"";
    
    NSArray *accountArray = [processCard_ accountsArray]; 
    
    
    for (BankAccount *bankAccount in accountArray) {
        
        account = [bankAccount accountIdAndDescription];
        [accountNames addObject:account];
        
    }
    
    [accontCombo_ setOptionStringList:accountNames];
    [accontCombo_ setNoSelectionText:NSLocalizedString(ACCOUND_CHARGED_TEXT_KEY, nil)];
    [accontCombo_ setSelectedIndex:[processCard_ selectedAccountIndex]];
        
    [currencyCombo_ setOptionStringList:[processCard_ currencyTextArray]];
    [currencyCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_THIRD_ACCOUTS_CURRENCY_TEXT_KEY, nil)];
    [currencyCombo_ setSelectedIndex:[processCard_ selectedCurrencyIndex]];

    [self viewForFirstView];
    [self viewForSecondView];
    [self relocateViews];
    [self lookForEditViews];

}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]);
    [self setScrollFrame:frame];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view.
 *
 * @param animated: If YES, the disappearance of the view is being animated.
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];

    for (UILabel *label in labelsArray_) {
        
        [label removeFromSuperview];
        
    }
    [labelsArray_ removeAllObjects];
}


/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Creates and returns an autoreleased CardContThirdCardViewController constructed from a NIB file.
 *
 * @return The autoreleased CardContThirdCardViewController constructed from a NIB file.
 */
+ (CardContThirdCardViewController *)cardContThirdCardViewController {

    CardContThirdCardViewController *result =  [[[CardContThirdCardViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;

}

#pragma mark -
#pragma mark Views methods

/**
 * Returns the view needed in the first view. NIL if it is not needed
 */
- (void)viewForFirstView {
    
    CGFloat yPosition = 0.0f;
    
    CGRect frame = firstViewHeader_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    firstViewHeader_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
    
    NSArray *infoArray = [NSArray arrayWithArray:[[self process] dataInfoArray]];
    [labelsArray_ removeAllObjects];
    
    for (TitleAndAttributes *titleAndAttributes in infoArray) {
        
        UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(20.0f, yPosition, 280.0f, 15.0f)] autorelease];
        
        yPosition += 15.0f;
        
        UILabel *detailLabel = [[[UILabel alloc] initWithFrame:CGRectMake(20.0f, yPosition, 280.0f, 15.0f)] autorelease];
        
        yPosition += 20.0f;
        
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [detailLabel setBackgroundColor:[UIColor clearColor]];
        
        [NXT_Peru_iPhoneStyler styleLabel:titleLabel withFontSize:14.0f color:[UIColor blackColor]];
        
        [NXT_Peru_iPhoneStyler styleLabel:detailLabel withFontSize:14.0f color:[UIColor BBVAGreyToneTwoColor]];
        
        [titleLabel setText:[titleAndAttributes titleString]];
        
        if ([[titleAndAttributes attributesArray] count] == 0) {
            
            [detailLabel setText:@""];
            
        } else {
            
            [detailLabel setText:[[titleAndAttributes attributesArray] objectAtIndex:0]];
            
        }
        
        [labelsArray_ addObject:titleLabel];
        [labelsArray_ addObject:detailLabel];
        
        [[self firstView] addSubview:titleLabel];
        [[self firstView] addSubview:detailLabel];
        
    }
    
    yPosition += 10.0f;
    
    frame = firstViewSeparator_.frame;
    frame.origin.y = yPosition;
    firstViewSeparator_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    firstViewHeight_ = yPosition;
    
}

/**
 * Returns the view needed in the second view. NIL if it is not needed
 */
- (void)viewForSecondView {
    
    CGFloat yPosition = 0.0f;
    
    CGRect frame = secondViewHeader_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    secondViewHeader_.frame = frame;

    yPosition = CGRectGetMaxY(frame) + SHORT_VERTICAL_DISTANCE;

    frame = accontCombo_.frame;
    frame.origin.y = yPosition;
    accontCombo_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + LONG_VERTICAL_DISTANCE;
    
    frame = amountLabel_.frame;
    frame.origin.y = yPosition;
    amountLabel_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + SHORT_VERTICAL_DISTANCE;
    
    frame = amountTextField_.frame;
    frame.origin.y = yPosition;
    amountTextField_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + LONG_VERTICAL_DISTANCE;
    
    frame = currencyLabel_.frame;
    frame.origin.y = yPosition;
    currencyLabel_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + SHORT_VERTICAL_DISTANCE;
    
    frame = currencyCombo_.frame;
    frame.origin.y = yPosition;
    currencyCombo_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + LONG_VERTICAL_DISTANCE;
    
    frame = secondViewSeparator_.frame;
    frame.origin.y = yPosition;
    secondViewSeparator_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
        
    secondViewHeight_ = yPosition;

}


/**
 * Returns the height for the first view. 0 if it is NIL
 */
- (CGFloat)heightForFirstView {

    return firstViewHeight_;

}

/**
 * Returns the height for the second view. 0 if it is NIL
 */
- (CGFloat)heightForSecondView {

    return secondViewHeight_;

}

#pragma mark -
#pragma mark  MOKStringListSelectionButtonDelegate

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton {
    
    BOOL invokeSuperImplementation = YES;

    if (accontCombo_ == stringListSelectionButton) {
        
        [processCard_ setSelectedAccountIndex:[stringListSelectionButton selectedIndex]];
        invokeSuperImplementation = NO;
        
    } else if (currencyCombo_ == stringListSelectionButton) {
    
        [processCard_ setSelectedCurrencyIndex:[stringListSelectionButton selectedIndex]];
        
        if ([stringListSelectionButton selectedIndex] < 0) {
            
            amountTextField_.currencySymbol = CURRENCY_SOLES_SYMBOL;
            
        } else  {
        
            [self updateAmountTextFieldWithCurreny:[[currencyCombo_ optionStringList] objectAtIndex:[stringListSelectionButton selectedIndex]]];
        
        }
        
        invokeSuperImplementation = NO;

    }
    
    if (invokeSuperImplementation) {
        
        [super stringListSelectionButtonSelectedIndexChanged:stringListSelectionButton];
        
    }
    
}

#pragma mark -
#pragma mark User interaction

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped {
    
    [self saveProcessInformation];
    
    [super continueButtonTapped];
    
}

/**
 * Save process information
 */
- (void)saveProcessInformation {
    
    [processCard_ setAmount:[amountTextField_ text]];
    
}

/**
 * Update the currency symbol on amount text field when user changes currency
 *
 * @param currency The currency name to set
 * @private
 */
- (void)updateAmountTextFieldWithCurreny:(NSString *)currency{
    
    if ([[currency lowercaseString] isEqualToString:[NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil) lowercaseString]]){
        
        amountTextField_.currencySymbol = CURRENCY_SOLES_SYMBOL;
        
    } else {
        
        amountTextField_.currencySymbol = CURRENCY_DOLARES_SYMBOL;
        
    }
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    result.mainTitle = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_CARDS_TITLE_LOWER_TEXT_KEY, nil)];
    
    return result;
    
}

/*
 * Set the process
 */
- (void)setProcess:(PaymentBaseProcess *)process {
    
    [super setProcess:process];
    
    if (processCard_ != process) {

        [process retain];
        [processCard_ release];
        processCard_ = (PaymentCCThirdCardProcess *)process;

    }

}


@end
