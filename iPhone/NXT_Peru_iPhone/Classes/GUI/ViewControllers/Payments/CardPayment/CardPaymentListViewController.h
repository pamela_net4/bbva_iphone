/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTViewController.h"

@class CardContStepOneViewController;
@class CardOtherBanksStepOneViewController;

/**
 * View controller to show all card payment options.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardPaymentListViewController : NXTViewController<UITableViewDelegate, UITableViewDataSource> {			
    
@private
    
    /**
     * Table view
     */
    UITableView *table_;
    
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    
    /**
     * Card cont step view controller
     */
    CardContStepOneViewController *cardContStepOneViewController_;
    
    /**
     * Card Other Banks Step One View Controller
     */
    CardOtherBanksStepOneViewController *cardOtherBanksStepOneViewController_;
    
    
    
}

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UITableView *table;

/**
 * Provides read-write access to the brandingLine and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIImageView *brandingLine;

/**
 * Creates and returns an autoreleased CardPaymentListViewController constructed from a NIB file.
 *
 * @return The autoreleased CardPaymentListViewController constructed from a NIB file.
 */
+ (CardPaymentListViewController *)cardPaymentListViewController;


@end
