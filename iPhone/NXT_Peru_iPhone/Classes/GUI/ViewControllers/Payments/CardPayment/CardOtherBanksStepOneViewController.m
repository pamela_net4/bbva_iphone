/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "CardOtherBanksStepOneViewController.h"

#import "Bank.h"
#import "BankList.h"
#import "Card.h"
#import "CardOtherBanksCardViewController.h"
#import "CardType.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKDoubleLabel.h"
#import "MOKEditableViewController+protected.h"
#import "MOKLimitedLengthTextField.h"
#import "MOKNavigationItem.h"
#import "MOKStringListSelectionButton.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentCell.h"
#import "PaymentBaseProcess.h"
#import "PaymentCardProcess.h"
#import "PaymentCOtherBankProcess.h"
#import "Session.h"
#import "SimpleHeaderView.h"
#import "StatusEnabledResponse.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "Updater.h"


#pragma mark -
#pragma mark Static attributes

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"CardOtherBanksStepOneViewController"

/**
 * Defines the max number of chars in each text field
 */
#define MAX_CHARS                                                   4

/**
 * GlobalPositionViewController private extension
 */
@interface CardOtherBanksStepOneViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releaseCardOtherBanksStepOneViewControllerGraphicElements;

/**
 * Place the graphic elements
 */
- (void)relocateViews;

/**
 * Update banks array by class
 *
 * @param classIndex The class index
 */
- (void)updateBanksArrayByClassIndex:(NSInteger)classIndex;

@end

#pragma mark -


@implementation CardOtherBanksStepOneViewController


#pragma mark -
#pragma mark Properties

@synthesize topLabel = topLabel_;
@synthesize classLabel = classLabel_;
@synthesize classCombo = classCombo_;
@synthesize destinationBankLabel = destinationBankLabel_;
@synthesize destinationBankCombo = destinationBankCombo_;
@synthesize cardLabel = cardLabel_;
@synthesize cardView = cardView_;
@synthesize firstTextField = firstTextField_;
@synthesize secondTextField = secondTextField_;
@synthesize thirdTextField = thirdTextField_;
@synthesize fourthTextField = fourthTextField_;
@synthesize continueButton = continueButton_;
@synthesize brandingLine = brandingLine_;
@synthesize cardTypeList = cardTypeList_;
@synthesize hasNavigateForward = hasNavigateForward_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseCardOtherBanksStepOneViewControllerGraphicElements];
    
    [cardOtherBanksCardViewController_ release];
    cardOtherBanksCardViewController_ = nil;
    
    [cardTypeList_ release];
    cardTypeList_ = nil;
    
    [classArray_ release];
    classArray_ = nil;
    
    [destinationBankArray_ release];
    destinationBankArray_ = nil;    
    
    [process_ release];
    process_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseCardOtherBanksStepOneViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseCardOtherBanksStepOneViewControllerGraphicElements {
    
    [header_ release];
    header_ = nil;
    
	[topLabel_ release];
    topLabel_ = nil;
    
    [classLabel_ release];
    classLabel_ = nil;
    
    [classCombo_ release];
    classCombo_ = nil;

    [destinationBankLabel_ release];
    destinationBankLabel_ = nil;
    
    [destinationBankCombo_ release];
    destinationBankCombo_ = nil;

    [cardLabel_ release];
    cardLabel_ = nil;
    
    [cardView_ release];
    cardView_ = nil;
    
    [firstTextField_ release];
    firstTextField_ = nil;

    [secondTextField_ release];
    secondTextField_ = nil;
    
    [thirdTextField_ release];
    thirdTextField_ = nil;
    
    [fourthTextField_ release];
    fourthTextField_ = nil;
    
    [continueButton_ release];
    continueButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    classArray_ = [[NSMutableArray alloc] init];
    destinationBankArray_ = [[NSMutableArray alloc] init];
    
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
        
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];

    [NXT_Peru_iPhoneStyler styleBlueButton:continueButton_];
    [continueButton_ setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil) 
                     forState:UIControlStateNormal];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    header_ = [[SimpleHeaderView simpleHeaderView] retain];
    [header_ setTitle:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TITLE_TEXT_KEY, nil)];
    
    [[self scrollableView] addSubview:header_];
    
    [NXT_Peru_iPhoneStyler styleLabel:topLabel_ withFontSize:14.0f color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:classLabel_ withFontSize:14.0f color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:destinationBankLabel_ withFontSize:14.0f color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:cardLabel_ withFontSize:14.0f color:[UIColor BBVABlueColor]];

    [NXT_Peru_iPhoneStyler styleMokTextField:firstTextField_ withFontSize:13.0f andColor:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleMokTextField:secondTextField_ withFontSize:13.0f andColor:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleMokTextField:thirdTextField_ withFontSize:13.0f andColor:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleMokTextField:fourthTextField_ withFontSize:13.0f andColor:[UIColor grayColor]];

    [NXT_Peru_iPhoneStyler styleStringListButton:classCombo_];
    [NXT_Peru_iPhoneStyler styleStringListButton:destinationBankCombo_];
    
    [topLabel_ setText:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_CARD_DATA_TEXT_KEY, nil)];
    [classLabel_ setText:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_CLASS_TEXT_KEY, nil)];
    [destinationBankLabel_ setText:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_DEST_BANK_TEXT_KEY, nil)];
    [cardLabel_ setText:NSLocalizedString(CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil)];

    [classCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_CLASS_SELECTION_TEXT_KEY, nil)];
    [destinationBankCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_BANK_SELECTION_TEXT_KEY, nil)];
    
    [classCombo_ setStringListSelectionButtonDelegate:self];
    [destinationBankCombo_ setStringListSelectionButtonDelegate:self];

    [firstTextField_ setDelegate:self];
    [secondTextField_ setDelegate:self];
    [thirdTextField_ setDelegate:self];
    [fourthTextField_ setDelegate:self];
    
    [cardView_ setBackgroundColor:[UIColor clearColor]];
    
    [self lookForEditViews];
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseCardOtherBanksStepOneViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
        
    if (!hasNavigateForward_) {
    
        [firstTextField_ setText:@""];
        [secondTextField_ setText:@""];
        [thirdTextField_ setText:@""];
        [fourthTextField_ setText:@""];
        [classCombo_ setSelectedIndex:-1];
        [destinationBankCombo_ setSelectedIndex:-1];
        
        [classArray_ removeAllObjects];
        
        for (CardType *cardType in cardTypeList_) {
            
            [classArray_ addObject:[cardType category]];
            
        }
        
        [classCombo_ setOptionStringList:classArray_];
        [classCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_CLASS_SELECTION_TEXT_KEY, nil)];
        
        [destinationBankCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_BANK_SELECTION_TEXT_KEY, nil)];
        
        
        if (classSelectedIndex_ <= 0){
            
            if ([classArray_ count] == 1) {
                classSelectedIndex_ = 0;
                [classCombo_ setSelectedIndex:classSelectedIndex_];
            } else {
                classSelectedIndex_ = -1;
            }
            
        }
        
        if (destinationBankSelectedIndex_ <= 0){
            
            if ([destinationBankArray_ count] == 1) {
                destinationBankSelectedIndex_ = 0;
                [destinationBankCombo_ setSelectedIndex:destinationBankSelectedIndex_];
            } else {
                destinationBankSelectedIndex_ = -1;
            }
            
        }
    
    } else {
    
        hasNavigateForward_ = NO;
    
    }
        
    [self relocateViews];
	
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]);
    [self setScrollFrame:frame];
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
	
}

/*
 * Creates and returns an autoreleased CardOtherBanksStepOneViewController constructed from a NIB file.
 */
+ (CardOtherBanksStepOneViewController *)cardOtherBanksStepOneViewController {
    
    CardOtherBanksStepOneViewController *result =  [[[CardOtherBanksStepOneViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

/**
 * Place the graphic elements
 */
- (void)relocateViews {

    CGFloat yPosition = 0.0f;
    
    CGRect frame = header_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    header_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
    
    frame = topLabel_.frame;
    frame.origin.y = yPosition;
    topLabel_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
    
    frame = classLabel_.frame;
    frame.origin.y = yPosition;
    classLabel_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 5.0f;
    
    frame = classCombo_.frame;
    frame.origin.y = yPosition;
    classCombo_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
    
    frame = destinationBankLabel_.frame;
    frame.origin.y = yPosition;
    destinationBankLabel_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 5.0f;
    
    frame = destinationBankCombo_.frame;
    frame.origin.y = yPosition;
    destinationBankCombo_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
    
    frame = cardLabel_.frame;
    frame.origin.y = yPosition;
    cardLabel_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 5.0f;
    
    frame = cardView_.frame;
    frame.origin.y = yPosition;
    cardView_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
    
    frame = continueButton_.frame;
    frame.origin.y = yPosition;
    continueButton_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);

    [self setScrollContentSize:CGSizeMake(320.0f, yPosition)];

}

#pragma mark -
#pragma mark User interaction

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped {

    NSString *error = @"";
    
    if (classSelectedIndex_ < 0) {
        
        error = NSLocalizedString(CARD_PAYMENT_OTHER_BANK_CLASS_SELECTION_ERROR_TEXT_KEY, nil);
        
    } else if (destinationBankSelectedIndex_ < 0) {
        
        error = NSLocalizedString(CARD_PAYMENT_OTHER_BANK_BANK_SELECTION_ERROR_TEXT_KEY, nil);
        
    } else if (([[firstTextField_ text] length] < MAX_CHARS) ||
                ([[secondTextField_ text] length] < MAX_CHARS) ||
                ([[thirdTextField_ text] length] < MAX_CHARS) ||
                ([[fourthTextField_ text] length] < MAX_CHARS)) {
        
            error = NSLocalizedString(CARD_PAYMENT_MUST_INTRODUCE_A_CARD_NUMBER_ERROR_TEXT_KEY, nil);

    }

    if ([error isEqualToString:@""]) {
                    
        process_ = [[PaymentCOtherBankProcess alloc] init];
            
        [process_ setSelectedClass:[classArray_ objectAtIndex:classSelectedIndex_]];      
        
        
        CardType *cardType = [cardTypeList_ objectAtIndex:classSelectedIndex_];
        Bank *bank = [[[cardType bankList] bankList] objectAtIndex:destinationBankSelectedIndex_];
        
        if (bank != nil) {
            
            [process_ setDestinationBank:[bank identification]];
            [process_ setDestinationBankName:[bank bank]];
            [process_ setCardNumber:[NSString stringWithFormat:@"%@%@%@%@", [firstTextField_ text],[secondTextField_ text], [thirdTextField_ text], [fourthTextField_ text]] ];
            [process_ setDelegate:self];
            [process_ startPaymentDataRequest];
  
		} else {
			
			[Tools showErrorWithMessage:NSLocalizedString(PAYMENT_ERROR_NOT_FOUND_BANK_TEXT_KEY, nil)];
			
		}

    
    } else {
        
        [Tools showInfoWithMessage:error];    
    
    }

}

#pragma mark -
#pragma mark MOKStringListSelectionButtonDelegate

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton {

    if (stringListSelectionButton == classCombo_) {
        
        classSelectedIndex_ = [stringListSelectionButton selectedIndex];
        
        [self updateBanksArrayByClassIndex:classSelectedIndex_];
        
        [destinationBankCombo_ setSelectedIndex:-1];
        
    } else if (stringListSelectionButton == destinationBankCombo_) {
        
        destinationBankSelectedIndex_ = [stringListSelectionButton selectedIndex];
    
    }     
    
}

/**
 * Update banks array by class
 *
 * @param classIndex The class
 */
- (void)updateBanksArrayByClassIndex:(NSInteger)classIndex {
    
    [destinationBankArray_ removeAllObjects];
    
    if ((classIndex >= 0) && (classIndex < [cardTypeList_ count])) {
        
        CardType *cardType = [cardTypeList_ objectAtIndex:classIndex];
        NSArray *bankListArray = [[cardType bankList] bankList];
        
        for (Bank *bank in bankListArray) {
            
            [destinationBankArray_ addObject:[bank bank]];
            
        }
        
    }
    
    [destinationBankCombo_ setOptionStringList:destinationBankArray_];

}

#pragma mark - 
#pragma mark PaymentBaseProcessDelegate

/**
 * Delegate is notified when the data analysis has finished 
 */
- (void)dataAnalysisHasFinished {

    if (cardOtherBanksCardViewController_ == nil) {
        
        cardOtherBanksCardViewController_ = [[CardOtherBanksCardViewController cardOtherBanksCardViewController] retain];
        
    } 
    
    hasNavigateForward_ = YES;
    
    [cardOtherBanksCardViewController_ setHasNavigateForward:NO];
    [cardOtherBanksCardViewController_ setProcess:process_];
    
    [[self navigationController] pushViewController:cardOtherBanksCardViewController_ 
                                           animated:YES];

}

#pragma mark -
#pragma mark UITextFieldDelegate

/**
 * Asks the delegate if the specified text should be changed.
 * 
 * @param textField: The text field containing the text.
 * @param range: The range of characters to be replaced
 * @param string: The replacement string.
 * @return YES if the specified text range should be replaced; otherwise, NO to keep the old text.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = YES;
	
	NSUInteger newLength = [textField.text length] + [string length] - range.length;
	NSCharacterSet *numbersSet = [NSCharacterSet decimalDigitCharacterSet];
    
	result = [Tools isValidText:string forCharacterSet:numbersSet];
    
	if (result) {
        
		if (textField == firstTextField_) {
            
			if (newLength == MAX_CHARS && ![string isEqualToString:@""]) {
				textField.text = [textField.text stringByAppendingString:string];
				[secondTextField_ becomeFirstResponder];
				result = NO;
			} else if (newLength > MAX_CHARS) {
                
                [secondTextField_ becomeFirstResponder];
				result = NO;
                
            }
			
		} else if (textField == secondTextField_) {
            
			if (newLength == MAX_CHARS && ![string isEqualToString:@""]) {
				textField.text = [textField.text stringByAppendingString:string];
				[thirdTextField_ becomeFirstResponder];
				result = NO;
			} else if (newLength > MAX_CHARS) {
               
                [thirdTextField_ becomeFirstResponder];
				result = NO;

            }

			
		} else if (textField == thirdTextField_) {
            
			if (newLength == MAX_CHARS && ![string isEqualToString:@""]) {
				textField.text = [textField.text stringByAppendingString:string];
				[fourthTextField_ becomeFirstResponder];
				result = NO;
			}else if (newLength > MAX_CHARS) {
                
                [fourthTextField_ becomeFirstResponder];
				result = NO;
                
            }
     			
		} else if (textField == fourthTextField_) {
            
			if (newLength == MAX_CHARS && ![string isEqualToString:@""]) {
				textField.text = [textField.text stringByAppendingString:string];
				result = NO;
			} else if (newLength > MAX_CHARS) {
            
                result = NO;
            
            }
        
        }

		if (textField == fourthTextField_) {
			if (textField.text.length + string.length > MAX_CHARS) {
				result = NO;
			}
		}
		
	}
	
	return result;
    
}


#pragma mark -
#pragma mark Setters

/*
 * Sets the card type list
 */
- (void)setCardTypeList:(NSArray *)cardTypeList {
    
    if (cardTypeList_ != cardTypeList) {
        
        if (cardTypeList_ == nil) {
            
            cardTypeList_ = [[NSMutableArray alloc] init];
            
        } else {
        
            [cardTypeList_ removeAllObjects];
        }
        
        for (CardType *cardType in cardTypeList) {
            
            if ([cardType isKindOfClass:[CardType class]]) {
                
                [cardTypeList_ addObject:cardType];
                
            }
            
        }
        
    }
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    result.mainTitle = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil),
                        NSLocalizedString(PAYMENT_CARDS_TITLE_LOWER_TEXT_KEY, nil)];
    return result;
    
}

@end
