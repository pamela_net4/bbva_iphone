/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "PaymentDataBaseViewController.h"
#import "PaymentBaseProcess.h"
#import "MOKStringListSelectionButton.h"
#import "TransferTINOnlineStepViewController.h"

@class MOKStringListSelectionButton;
@class PaymentCOtherBankProcess;
@class SimpleHeaderView;
@class MOKCurrencyTextField;
@class MOKTextField;

/**
 * View controller to show card other banks card payment.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardOtherBanksCardViewController : PaymentDataBaseViewController <MOKStringListSelectionButtonDelegate>{			
    
@private
    
#pragma Graphic
    
    /**
     * First view header
     */
    SimpleHeaderView *firstViewHeader_;
    
    /**
     * First view separator
     */
    UIImageView *firstViewSeparator_;
    
    /**
     * Second view separator
     */
    UIImageView *secondViewSeparator_;
    
    /**
     * Location label
     */
    UILabel *locationLabel_;
    
    /**
     * Location combo
     */
    MOKStringListSelectionButton *locationCombo_;
    
    /**
     * Account label
     */
    UILabel *accountLabel_;
    
    /**
     * Account combo
     */
    MOKStringListSelectionButton *accontCombo_;
    
    /**
     * Amount label
     */
    UILabel *amountLabel_;
    
    /**
     * Amount text field
     */
    MOKCurrencyTextField *amountTextField_;
    
    /**
     * Currency label
     */
    UILabel *currencyLabel_;
    
    /**
     * Currency combo
     */
    MOKStringListSelectionButton *currencyCombo_;
    
    
    TransferTINOnlineStepViewController *transferTINOnlineStepViewController_;
    
#pragma Logic
    
    /**
     * Labels array
     */
    NSMutableArray *labelsArray_;
    
    /**
     * First view height
     */
    CGFloat firstViewHeight_;
    
    /**
     * Second view height
     */
    CGFloat secondViewHeight_;
    
    /**
     * Process
     */
    PaymentCOtherBankProcess *processCard_;
    
}

/**
 * Provides read-write access to the locationLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *locationLabel;

/**
 * Provides read-write access to the locationCombo and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *locationCombo;

/**
 * Provides read-write access to the accountLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *accountLabel;

/**
 * Provides read-write access to the accontCombo and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *accontCombo;

/**
 * Provides read-write access to the amountLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *amountLabel;

/**
 * Provides read-write access to the amountTextField and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKCurrencyTextField *amountTextField;

/**
 * Provides read-write access to the currencyLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *currencyLabel;

/**
 * Provides read-write access to the currencyCombo and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *currencyCombo;

/**
 * Provides read-write access to the beneficiaryLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *beneficiaryLabel;

/**
 * Provides read-write access to the beneficiaryTextField and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKTextField *beneficiaryTextField;

/**
 * Creates and returns an autoreleased CardOtherBanksCardViewController constructed from a NIB file.
 *
 * @return The autoreleased CardOtherBanksCardViewController constructed from a NIB file.
 */
+ (CardOtherBanksCardViewController *)cardOtherBanksCardViewController;


@end
