/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "CardPaymentListViewController.h"

#import "Card.h"
#import "CardContStepOneViewController.h"
#import "CardList.h"
#import "CardOtherBanksStepOneViewController.h"
#import "GlobalAdditionalInformation.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentCCOtherBanksInitialResponse.h"
#import "PaymentCCardInitialResponse.h"
#import "PaymentCell.h"
#import "Session.h"
#import "StatusEnabledResponse.h"
#import "StringKeys.h"
#import "StringList.h"
#import "Tools.h"
#import "Updater.h"

#pragma mark -
#pragma mark Static attributes
/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"CardPaymentListViewController"

/**
 * GlobalPositionViewController private extension
 */
@interface CardPaymentListViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releaseCardPaymentListViewControllerGraphicElements;

/**
 * The notification received from server.
 *
 * @param notification The notification
 */
- (void)paymentContinentalResponseReceived:(NSNotification *)notification;

/**
 * The notification received from server.
 *
 * @param notification The notification
 */
- (void)paymentOtherBankResponseReceived:(NSNotification *)notification;

@end

#pragma mark -


@implementation CardPaymentListViewController


#pragma mark -
#pragma mark Properties

@synthesize table = table_;
@synthesize brandingLine = brandingLine_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseCardPaymentListViewControllerGraphicElements];
    
    [cardContStepOneViewController_ release];
    cardContStepOneViewController_ = nil;

    [cardOtherBanksStepOneViewController_ release];
    cardOtherBanksStepOneViewController_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseCardPaymentListViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseCardPaymentListViewControllerGraphicElements {
    
	[table_ release];
    table_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleTableView:table_];
    
    table_.scrollEnabled = NO; 
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseCardPaymentListViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
   
    [super viewWillAppear:animated];
    
    
    [table_ reloadData];
   
    [Tools checkTableScrolling:table_];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(paymentContinentalResponseReceived:) 
                                                 name:kNotificationPaymentContinentalInicilizationResultEnds 
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(paymentOtherBankResponseReceived:) 
                                                 name:kNotificationPaymentOtherBankInicilizationResultEnds 
                                               object:nil];
	
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentContinentalInicilizationResultEnds 
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentOtherBankInicilizationResultEnds 
                                                  object:nil];
	
}

/*
 * Creates and returns an autoreleased CardPaymentListViewController constructed from a NIB file.
 */
+ (CardPaymentListViewController *)cardPaymentListViewController {
    
    CardPaymentListViewController *result =  [[[CardPaymentListViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}


#pragma mark -
#pragma mark UITableView methods

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PaymentCell *result = (PaymentCell *)[tableView dequeueReusableCellWithIdentifier:[PaymentCell cellIdentifier]];
    
    if (result == nil) {
        result = [PaymentCell paymentCell];
    }
        
    switch (indexPath.row) {
        case 0:
            [result.titleLabel setText:NSLocalizedString(CARD_PAYMENT_BBVA_CARDS_TEXT_KEY, nil)];
            break;
            
        case 1:
            [result.titleLabel setText:NSLocalizedString(CARD_PAYMENT_OTHER_BANKS_CARDS_TEXT_KEY, nil)];
            break;
            
        default:
            break;                  
    }
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    [result.imageView setImage:[imagesCache imageNamed:PAYMENT_LIST_ICON_IMAGE_FILE_NAME]];
    
    result.selectionStyle = UITableViewCellSelectionStyleGray;
    [result setShowDisclosureArrow:YES];
    [result setShowSeparator:YES];
    result.backgroundColor = [UIColor clearColor];
    return result;
    
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void) tableView: (UITableView*) tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BOOL otpActive = [[[Session getInstance] additionalInformation] otpActive];
        
    switch (indexPath.row) {
        
        case 0: 

            [self.appDelegate showActivityIndicator:poai_Both];
            [[Updater getInstance] obtainPaymentCardContinentalInitialization];
            
            break;
            
        case 1: 
            
            if (otpActive) {
            
                [self.appDelegate showActivityIndicator:poai_Both];   
                [[Updater getInstance] obtainPaymentCardOtherBanksInitialization];
           
            } else {
                
                [Tools showAlertWithMessage:NSLocalizedString(OTP_SERVICE_NO_ACTIVE_TEXT_KEY, nil)];
                
            }
            
            break;  

        default:
            break;
            
    }
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section {
    return 2;
}

/**
 
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [PaymentCell cellHeight];
}

#pragma mark -
#pragma mark Notification received

/*
 * The notification received from server.
 */
- (void)paymentContinentalResponseReceived:(NSNotification *)notification {
	
	[[self appDelegate] hideActivityIndicator];
	
	PaymentCCardInitialResponse *response = (PaymentCCardInitialResponse *)[notification object];
	
	if (![response isError]) {
        
        NSArray *cardNumberArray = [NSArray arrayWithArray:[[response numberCardList] stringList]];
        Card *card = nil;
        CardList *cardList = [[Session getInstance] cardList];
        NSMutableArray *cardsArray = [[[NSMutableArray alloc] init] autorelease];
        
        
        for (NSString *cardNumber in cardNumberArray) {
            
            card = [cardList cardFromCardTerminateNumber:cardNumber];
            
            if (card != nil) {
                
                [cardsArray addObject:card];
            }
            
        }
        
        
        if (cardContStepOneViewController_ == nil) {
            
            cardContStepOneViewController_ = [[CardContStepOneViewController cardContStepOneViewController] retain];
            
        }
        
        [cardContStepOneViewController_ setHasNavigateForward:NO];
        [cardContStepOneViewController_ setCardsArray:cardsArray];
        [[self navigationController] pushViewController:cardContStepOneViewController_ 
                                               animated:YES];
        
	}
	
}

/**
 * The notification received from server.
 *
 * @param notification The notification
 */
- (void)paymentOtherBankResponseReceived:(NSNotification *)notification {
    
    [[self appDelegate] hideActivityIndicator];
	
	PaymentCCOtherBanksInitialResponse *response = (PaymentCCOtherBanksInitialResponse *)[notification object];
	
	if (![response isError]) {
        
        if (cardOtherBanksStepOneViewController_ == nil) {
            
            cardOtherBanksStepOneViewController_ = [[CardOtherBanksStepOneViewController cardOtherBanksStepOneViewController] retain];
            
        }
        
        [cardOtherBanksStepOneViewController_ setHasNavigateForward:NO];
        [cardOtherBanksStepOneViewController_ setCardTypeList:[[response cardTypeList] cardTypeList]];
        
        [self.navigationController pushViewController:cardOtherBanksStepOneViewController_ animated:YES];
        
	}
    
}


#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [self customNavigationItem];
    
    [[result customTitleView] setTopLabelText:[NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_CARDS_TITLE_LOWER_TEXT_KEY, nil)]];
    
    return result;
    
}


@end
