/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */



#import "MOKEditableViewController.h"

#import "MOKStringListSelectionButton.h"
#import "PaymentBaseProcess.h"

@class CardOtherBanksCardViewController;
@class PaymentCOtherBankProcess;
@class SimpleHeaderView;
@class MOKLimitedLengthTextField;
@class MOKStringListSelectionButton;

/**
 * View controller to show all card payment options.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardOtherBanksStepOneViewController : MOKEditableViewController <MOKStringListSelectionButtonDelegate, UITextFieldDelegate, PaymentBaseProcessDelegate> {			
    
@private
    
    /**
     * Header
     */
    SimpleHeaderView *header_;
    
    /**
     * Top label
     */
    UILabel *topLabel_;

    /**
     * Class label
     */
    UILabel *classLabel_;
    
    /**
     * Class combo
     */
    MOKStringListSelectionButton *classCombo_;
    
    /**
     * Destination bank label
     */
    UILabel *destinationBankLabel_;
    
    /**
     * Destination bank combo
     */
    MOKStringListSelectionButton *destinationBankCombo_;
    
    /**
     * Card label
     */
    UILabel *cardLabel_;
    
    /**
     * Card View
     */
    UIView *cardView_;
    
    /**
     * First text field
     */
    MOKLimitedLengthTextField *firstTextField_;
    
    /**
     * Second text field
     */
    MOKLimitedLengthTextField *secondTextField_;
    
    /**
     * Third text field
     */
    MOKLimitedLengthTextField *thirdTextField_;
    
    /**
     * Fourth text field
     */
    MOKLimitedLengthTextField *fourthTextField_;
    
    /**
     * Continue button
     */
    UIButton *continueButton_;
    
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    
    /**
     * Card Other Banks Card View Controller
     */
    CardOtherBanksCardViewController *cardOtherBanksCardViewController_; 
    
    
#pragma logic
    
    /**
     * Class selected index
     */
    NSInteger classSelectedIndex_;
    
    /**
     * Destination bank selected index
     */
    NSInteger destinationBankSelectedIndex_;
    
    /**
     * Card type List
     */
    NSMutableArray *cardTypeList_;
    
    /**
     * Class array
     */
    NSMutableArray *classArray_;
    
    /**
     * Destination bank array
     */
    NSMutableArray *destinationBankArray_;
    
    /**
     * Payment card process
     */
    PaymentCOtherBankProcess *process_;
    
    /**
     * Navigation flag
     */
    BOOL hasNavigateForward_;
}

/**
 * Provides read-write access to the topLabel and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UILabel *topLabel;

/**
 * Provides read-write access to the classLabel and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UILabel *classLabel;

/**
 * Provides read-write access to the classCombo and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet MOKStringListSelectionButton *classCombo;

/**
 * Provides read-write access to the destinationBankLabel and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UILabel *destinationBankLabel;

/**
 * Provides read-write access to the destinationBankCombo and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet MOKStringListSelectionButton *destinationBankCombo;

/**
 * Provides read-write access to the cardLabel and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UILabel *cardLabel;

/**
 * Provides read-write access to the cardView and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIView *cardView;

/**
 * Provides read-write access to the firstTextField and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet MOKLimitedLengthTextField *firstTextField;

/**
 * Provides read-write access to the secondTextField and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet MOKLimitedLengthTextField *secondTextField;

/**
 * Provides read-write access to the thirdTextField and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet MOKLimitedLengthTextField *thirdTextField;

/**
 * Provides read-write access to the fourthTextField and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet MOKLimitedLengthTextField *fourthTextField;

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIButton *continueButton;

/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIImageView *brandingLine;

/**
 * Provides read-write access to the cardTypeList
 */
@property (nonatomic, readwrite, retain) NSArray *cardTypeList;

/**
 * Provides read-write access to the hasNavigateForward
 */
@property (nonatomic, readwrite, assign) BOOL hasNavigateForward;

/**
 * Creates and returns an autoreleased CardOtherBanksStepOneViewController constructed from a NIB file.
 *
 * @return The autoreleased CardOtherBanksStepOneViewController constructed from a NIB file.
 */
+ (CardOtherBanksStepOneViewController *)cardOtherBanksStepOneViewController;

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped;

@end