/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "CardContStepOneViewController.h"

#import "Card.h"
#import "CardContOwnCardViewController.h"
#import "CardContThirdCardViewController.h"
#import "CheckCardCell.h"
#import "CheckComboCell.h"
#import "GlobalAdditionalInformation.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKDoubleLabel.h"
#import "MOKEditableViewController+protected.h"
#import "MOKLimitedLengthTextField.h"
#import "MOKNavigationItem.h"
#import "MOKStringListSelectionButton.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentCell.h"
#import "PaymentBaseProcess.h"
#import "PaymentCardProcess.h"
#import "PaymentCCOwnCardProcess.h"
#import "PaymentCCThirdCardProcess.h"
#import "Session.h"
#import "SimpleHeaderView.h"
#import "StatusEnabledResponse.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "Updater.h"

#pragma mark -
#pragma mark Static attributes

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"CardContStepOneViewController"

/**
 * Defines the max number of chars in each text field
 */
#define MAX_CHARS                                                   4

/**
 * GlobalPositionViewController private extension
 */
@interface CardContStepOneViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releaseCardContStepOneViewControllerGraphicElements;

/**
 * Place the graphic elements
 */
- (void)relocateViews;

@end

#pragma mark -


@implementation CardContStepOneViewController


#pragma mark -
#pragma mark Properties

@synthesize table = table_;
@synthesize continueButton = continueButton_;
@synthesize brandingLine = brandingLine_;
@synthesize cardsArray = cardsArray_;
@synthesize hasNavigateForward = hasNavigateForward_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseCardContStepOneViewControllerGraphicElements];
    
    [cardsArray_ release];
    cardsArray_ = nil;
    
    [cardNamesArray_ release];
    cardNamesArray_ = nil;
    
    [cardContOwnCardViewController_ release];
    cardContOwnCardViewController_ = nil;
    
    [cardContThirdCardViewController_ release];
    cardContThirdCardViewController_ = nil;
    
    [process_ release];
    process_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseCardContStepOneViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseCardContStepOneViewControllerGraphicElements {
    
    [header_ release];
    header_ = nil;
    
	[table_ release];
    table_ = nil;
    
    [checkComboCell_ release];
    checkComboCell_ = nil;
    
    [checkCardCell_ release];
    checkCardCell_ = nil;
    
    [continueButton_ release];
    continueButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    cardsArray_ = [[NSMutableArray alloc] init];
    cardNamesArray_ = [[NSMutableArray alloc] init];
    
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleTableView:table_];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:continueButton_];
    [continueButton_ setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil) 
                     forState:UIControlStateNormal];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    header_ = [[SimpleHeaderView simpleHeaderView] retain];
    [header_ setTitle:NSLocalizedString(CARD_PAYMENT_CARD_SELECTION_TEXT_KEY, nil)];
    
    [[self scrollableView] addSubview:header_];
    
    table_.scrollEnabled = NO;    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseCardContStepOneViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
   
    [super viewWillAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
    if (!hasNavigateForward_) {
        
        activeCellIndex_ = -1;

        if ([cardsArray_ count] > 0) {
            
            cardSelectedIndex_ = 0;
            
        } else {
            
            cardSelectedIndex_ = -1;
            
        }
        
        firstPartCard_ = @"";
        secondPartCard_ = @"";
        thirdPartCard_ = @"";
        fourthPartCard_ = @"";
        
        if (checkCardCell_ != nil) {
            
            [[checkCardCell_ firstTextField] setText:firstPartCard_];
            [[checkCardCell_ secondTextField] setText:secondPartCard_];
            [[checkCardCell_ thirdTextField] setText:thirdPartCard_];
            [[checkCardCell_ fourthTextField] setText:fourthPartCard_];
        
        }
        
    } else {
    
        hasNavigateForward_ = NO;
        
        NSString *cardNumber = [process_ cardNumber];
        firstPartCard_ = [cardNumber substringToIndex:4];
        secondPartCard_ = [[cardNumber substringToIndex:8] substringFromIndex:4];
        thirdPartCard_ = [[cardNumber substringToIndex:12] substringFromIndex:8];
        fourthPartCard_ = [cardNumber substringFromIndex:12];
        
        [[checkCardCell_ firstTextField] setText:firstPartCard_];
        [[checkCardCell_ secondTextField] setText:secondPartCard_];
        [[checkCardCell_ thirdTextField] setText:thirdPartCard_];
        [[checkCardCell_ fourthTextField] setText:fourthPartCard_];
        
    }
    

    
    [self relocateViews];
	
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
        
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
	
}

/*
 * Creates and returns an autoreleased CardContStepOneViewController constructed from a NIB file.
 */
+ (CardContStepOneViewController *)cardContStepOneViewController {
    
    CardContStepOneViewController *result =  [[[CardContStepOneViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

/**
 * Place the graphic elements
 */
- (void)relocateViews {

    CGFloat yPosition = 0.0f;
    
    CGRect frame = header_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    header_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    [table_ reloadData];
    
    frame = table_.frame;
    frame.origin.y = yPosition;
    frame.size.height = table_.contentSize.height;
    table_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 20.0f;
    
    frame = continueButton_.frame;
    frame.origin.y = yPosition;
    continueButton_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);

    [self setScrollContentSize:CGSizeMake(320.0f, yPosition)];

}

#pragma mark -
#pragma mark UITableView methods

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        
        if (checkComboCell_ == nil) {
            checkComboCell_ = [[CheckComboCell checkComboCell] retain];
            [[checkComboCell_ combo] setStringListSelectionButtonDelegate:self];
        }
        
        [[checkComboCell_ topTextLabel] setText:NSLocalizedString(CARD_PAYMENT_OWN_CARD_TEXT_KEY, nil)];
		
        [NXT_Peru_iPhoneStyler styleLabel:[checkComboCell_ topTextLabel] withFontSize:15.0f color:[UIColor BBVABlueColor]];
        
		[[checkComboCell_ combo] setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECTION_TEXT_KEY, nil)];
        
        [[checkComboCell_ combo] setOptionStringList:cardNamesArray_];
        [checkComboCell_ setCheckActive:(activeCellIndex_ == indexPath.row)];
        [checkComboCell_ setShowSeparator:YES];
        
        [[checkComboCell_ combo] setSelectedIndex:cardSelectedIndex_];
        
        [self lookForEditViews];
        
        return checkComboCell_;
        
    } else if (indexPath.row == 1) {
    
        if (checkCardCell_ == nil) {
            checkCardCell_ = [[CheckCardCell checkCardCell] retain];
            
            [[checkCardCell_ firstTextField] setDelegate:self];
            [[checkCardCell_ secondTextField] setDelegate:self];
            [[checkCardCell_ thirdTextField] setDelegate:self];
            [[checkCardCell_ fourthTextField] setDelegate:self];

        }
        
        [NXT_Peru_iPhoneStyler styleLabel:[checkCardCell_ topTextLabel] withFontSize:15.0f color:[UIColor BBVABlueColor]];

        [[checkCardCell_ topTextLabel] setText:NSLocalizedString(CARD_PAYMENT_THIRD_CARD_TEXT_KEY, nil)];
        [checkCardCell_ setCheckActive:(activeCellIndex_ == indexPath.row)];
        [checkCardCell_ setShowSeparator:YES];

        [self lookForEditViews];

        return checkCardCell_;
    
    } else { 
            
        return nil;
            
    }
        
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void) tableView: (UITableView*) tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger selectedCell = -1;
    BOOL otpActive = [[[Session getInstance] additionalInformation] otpActive];
    
    switch (indexPath.row) {
        
        case 0:
            
            selectedCell = 0;                
            break;
        
        case 1: 
        
            if (otpActive) {
                
                selectedCell = 1;
                
            } else {
                
                [Tools showAlertWithMessage:NSLocalizedString(OTP_SERVICE_NO_ACTIVE_TEXT_KEY, nil)];
                
            }
            
            break;
            
        default: 
            break;
    }
    
    activeCellIndex_ = selectedCell;

    [self relocateViews];
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section {
    return 2;
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat result = 0.0f;
    
    if (indexPath.row == 0) {
        
        result = [CheckComboCell cellHeightCheckOn:(activeCellIndex_ == indexPath.row)];
        
    } else if (indexPath.row == 1) {
        
        result = [CheckCardCell cellHeightCheckOn:(activeCellIndex_ == indexPath.row)];
        
    } 
    
    return result;
}

#pragma mark -
#pragma mark User interaction

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped {

    NSString *error = @"";
    
    if (activeCellIndex_ < 0) {
        
        error = NSLocalizedString(CARD_PAYMENT_MUST_SELECT_A_CHOICE_ERROR_TEXT_KEY, nil);
        
    } else {
    
        if ((activeCellIndex_ == 0) && (cardSelectedIndex_ < 0)) {
            
            error = NSLocalizedString(CARD_PAYMENT_MUST_SELECT_A_CARD_ERROR_TEXT_KEY, nil);
            
        } else if ((activeCellIndex_ == 1) && 
                   (([[[checkCardCell_ firstTextField] text] length] < MAX_CHARS) ||
                    ([[[checkCardCell_ secondTextField] text] length] < MAX_CHARS) ||
                    ([[[checkCardCell_ thirdTextField] text] length] < MAX_CHARS) ||
                    ([[[checkCardCell_ fourthTextField] text] length] < MAX_CHARS))) {
        
            error = NSLocalizedString(CARD_PAYMENT_MUST_INTRODUCE_A_CARD_NUMBER_ERROR_TEXT_KEY, nil);

        }    
    }

    if ([error isEqualToString:@""]) {

        if (process_ != nil) {
            [process_ release];
            process_ = nil;
            
        }
        
        if (activeCellIndex_ == 0) {

            process_ = [[PaymentCCOwnCardProcess alloc] init];
            [process_ setOwnCardSelected:[cardsArray_ objectAtIndex:cardSelectedIndex_]]; 
            
        } else if (activeCellIndex_ == 1) {
        
            process_ = [[PaymentCCThirdCardProcess alloc] init];
            [process_ setCardNumber:[NSString stringWithFormat:@"%@%@%@%@", 
                                     [[checkCardCell_ firstTextField] text], 
                                     [[checkCardCell_ secondTextField] text], 
                                     [[checkCardCell_ thirdTextField] text], 
                                     [[checkCardCell_ fourthTextField] text]]];
            
            firstPartCard_ = [[checkCardCell_ firstTextField] text];
            secondPartCard_ = [[checkCardCell_ secondTextField] text];
            thirdPartCard_ = [[checkCardCell_ thirdTextField] text];
            fourthPartCard_ = [[checkCardCell_ fourthTextField] text];
            
        }
        
        [process_ setDelegate:self];
        [process_ startPaymentDataRequest];
    
    } else {
        
        [Tools showInfoWithMessage:error];    
    
    }

}

#pragma mark -
#pragma mark MOKStringListSelectionButtonDelegate

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton {

    cardSelectedIndex_ = [stringListSelectionButton selectedIndex];

}

#pragma mark - 
#pragma mark PaymentBaseProcessDelegate

/**
 * Delegate is notified when the data analysis has finished 
 */
- (void)dataAnalysisHasFinished {

    if (activeCellIndex_ == 0) {
        
        if (cardContOwnCardViewController_ == nil) {
             
            cardContOwnCardViewController_ = [[CardContOwnCardViewController cardContOwnCardViewController] retain];
            
        } 
        
        hasNavigateForward_ = YES;
        
        [cardContOwnCardViewController_ setProcess:process_];
        [cardContOwnCardViewController_ setHasNavigateForward:NO];
        
        [[self navigationController] pushViewController:cardContOwnCardViewController_ 
                                               animated:YES];
        
    } else if (activeCellIndex_ == 1) {
        
        if (cardContThirdCardViewController_ == nil) {
            
            cardContThirdCardViewController_ = [[CardContThirdCardViewController cardContThirdCardViewController] retain];
            
        } 
        
        hasNavigateForward_ = YES;
        [cardContThirdCardViewController_ setProcess:process_];
        [cardContThirdCardViewController_ setHasNavigateForward:NO];

        [[self navigationController] pushViewController:cardContThirdCardViewController_ 
                                               animated:YES];
        
    }

}

#pragma mark -
#pragma mark UITextFieldDelegate

/**
 * Asks the delegate if the specified text should be changed.
 * 
 * @param textField: The text field containing the text.
 * @param range: The range of characters to be replaced
 * @param string: The replacement string.
 * @return YES if the specified text range should be replaced; otherwise, NO to keep the old text.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = YES;
	
	NSUInteger newLength = [textField.text length] + [string length] - range.length;
	NSCharacterSet *numbersSet = [NSCharacterSet decimalDigitCharacterSet];
    
	result = [Tools isValidText:string forCharacterSet:numbersSet];
    
	if (result) {
        
		if (textField == checkCardCell_.firstTextField) {
            
			if (newLength == MAX_CHARS && ![string isEqualToString:@""]) {
				textField.text = [textField.text stringByAppendingString:string];
				[checkCardCell_.secondTextField becomeFirstResponder];
				result = NO;
			} else if (newLength > MAX_CHARS) {
                
                [checkCardCell_.secondTextField becomeFirstResponder];
				result = NO;
                
            }
			
		} else if (textField == checkCardCell_.secondTextField) {
            
			if (newLength == MAX_CHARS && ![string isEqualToString:@""]) {
				textField.text = [textField.text stringByAppendingString:string];
				[checkCardCell_.thirdTextField becomeFirstResponder];
				result = NO;
			} else if (newLength > MAX_CHARS) {
                
                [checkCardCell_.thirdTextField becomeFirstResponder];
				result = NO;
                
            }
			
		} else if (textField == checkCardCell_.thirdTextField) {
            
			if (newLength == MAX_CHARS && ![string isEqualToString:@""]) {
				textField.text = [textField.text stringByAppendingString:string];
				[checkCardCell_.fourthTextField becomeFirstResponder];
				result = NO;
			} else if (newLength > MAX_CHARS) {
                
                [checkCardCell_.fourthTextField becomeFirstResponder];
				result = NO;
                
            }
			
		} else if (textField == checkCardCell_.fourthTextField) {
            
			if (newLength == MAX_CHARS && ![string isEqualToString:@""]) {
				textField.text = [textField.text stringByAppendingString:string];
				result = NO;
			} else if (newLength > MAX_CHARS) {
                
                result = NO;
                
            }
			
		}
		
		if (textField == checkCardCell_.fourthTextField) {
			if (textField.text.length + string.length > MAX_CHARS) {
				result = NO;
			}
		}
		
	}
	
	return result;
    
}


#pragma mark -
#pragma mark Setters

/**
 * Sets the cards array
 */
- (void)setCardsArray:(NSArray *)cardsArray {

    [cardsArray_ removeAllObjects];
    [cardsArray_ addObjectsFromArray:cardsArray];
    
    [cardNamesArray_ removeAllObjects];
    NSString *cardName;
        
    for (Card *card in cardsArray_) {
        
        cardName = [card cardListName];
        [cardNamesArray_ addObject:cardName];
        
    }
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    result.mainTitle = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil),
                        NSLocalizedString(PAYMENT_CARDS_TITLE_LOWER_TEXT_KEY, nil)];
    
    return result;
    
}

@end
