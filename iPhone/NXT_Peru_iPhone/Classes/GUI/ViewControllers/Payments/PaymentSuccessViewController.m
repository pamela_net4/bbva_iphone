/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PaymentSuccessViewController.h"

#import "InformationCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKDoubleLabel.h"
#import "MOKEditableViewController+protected.h"
#import "MOKNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentBaseProcess.h"
#import "PaymentCell.h"
#import "PaymentsConstants.h"
#import "Session.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "Tools.h"
#import "TitleAndAttributes.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "Updater.h"

/**
 * Defines the interior view distance
 */
#define INTERIOR_VIEW_DISTANCE                      5.0f

/**
 * Defines the exterior view distance
 */
#define EXTERIOR_VIEW_DISTANCE                      10.0f

/**
 * Defines the text font size
 */
#define TEXT_FONT_SIZE                              14.0f

/**
 * Defines the small text font size
 */
#define SMALL_TEXT_FONT_SIZE                        12.0f

/**
 * Defines the big text font size
 */
#define BIG_TEXT_FONT_SIZE                          17.0f

/**
 * Defines the Nib file name
 */
#define NIB_FILE_NAME                               @"PaymentSuccessViewController"

/**
 * PaymentSuccessViewController private extension
 */
@interface PaymentSuccessViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releasePaymentSuccessViewControllerGraphicElements;

/**
 * Relocate views
 */
- (void)relocateViews;

/**
 * Update information
 */
- (void)updateInformation;

@end

#pragma mark -


@implementation PaymentSuccessViewController


#pragma mark -
#pragma mark Properties

@synthesize titleView = titleView_;
@synthesize titleLabel = titleLabel_;
@synthesize additionalLabel = additionalLabel_;
@synthesize transferTickBg = transferTickBg_;
@synthesize transferTickImageView = transferTickImageView_;
@synthesize tableBgImageView = tableBgImageView_;
@synthesize infoTable = infoTable_;
@synthesize infoLabel = infoLabel_;
@synthesize paymentButton = paymentButton_;
@synthesize globalPositionButton = globalPositionButton_;
@synthesize foButton=foButton_;
@synthesize bottomInfoView = bottomInfoView_;
@synthesize bottomInfoTextView = bottomInfoTextView_;
@synthesize bottomInfoBgImageView = bottomInfoBgImageView_;
@synthesize brandingLine = brandingLine_;
@synthesize process = process_;
@synthesize isFoFinished = isFoFinished_;
@synthesize frequentOperationReactiveStepOneViewController=frequentOperationReactiveStepOneViewController_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releasePaymentSuccessViewControllerGraphicElements];
    
    [process_ release];
    process_ = nil;
    
    [super dealloc];
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasePaymentSuccessViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releasePaymentSuccessViewControllerGraphicElements {
    
	[headerView_ release];
    headerView_ = nil;

    [titleView_ release];
    titleView_ = nil;
    
	[titleLabel_ release];
    titleLabel_ = nil;
    
    [additionalLabel_ release];
    additionalLabel_ = nil;
    
    [transferTickImageView_ release];
    transferTickImageView_ = nil;
    
    [transferTickBg_ release];
    transferTickBg_ = nil;
    
    [tableBgImageView_ release];
    tableBgImageView_ = nil;
    
    [infoTable_ release];
    infoTable_ = nil;
    
    [infoLabel_ release];
    infoLabel_ = nil;
    
    [globalPositionButton_ release];
    globalPositionButton_ = nil;
    
    [paymentButton_ release];
    paymentButton_ = nil;
    
    [bottomInfoView_ release];
    bottomInfoView_ = nil;
    
    [bottomInfoTextView_ release];
    bottomInfoTextView_ = nil;
    
    [bottomInfoBgImageView_ release];
    bottomInfoBgImageView_ = nil;

    [brandingLine_ release];
    brandingLine_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
    [tableBgImageView_ setImage:[[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME]];
    [transferTickBg_ setImage:[[ImagesCache getInstance] imageNamed:TRANSFER_DONE_IMAGE_FILE_NAME]];
    [transferTickImageView_ setImage:[[ImagesCache getInstance] imageNamed:TRANSFER_TICK_IMAGE_FILE_NAME]];
    
    [titleView_ setBackgroundColor:[UIColor clearColor]];
    [bottomInfoView_ setBackgroundColor:[UIColor clearColor]];
    [bottomInfoBgImageView_ setImage:[[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME]];

    headerView_ = [[SimpleHeaderView simpleHeaderView] retain];
    [[self view ] addSubview:headerView_];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:paymentButton_];
    [paymentButton_ setTitle:NSLocalizedString(PAYMENT_TITLE_TEXT_KEY, nil) 
                    forState:UIControlStateNormal];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:globalPositionButton_];
    [globalPositionButton_ setTitle:NSLocalizedString(GLOBAL_POSITION_TITLE_TEXT_KEY, nil)
                           forState:UIControlStateNormal];
    
    if([process_ paymentOperationType]!=PTEPaymentISService)
    {
    
    [NXT_Peru_iPhoneStyler styleBlueButton:foButton_];
    [foButton_ setTitle:@"Inscribir a operación frecuente"
                           forState:UIControlStateNormal];
    }
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueSpectrumColor]];
    [NXT_Peru_iPhoneStyler styleLabel:infoLabel_ withFontSize:SMALL_TEXT_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:additionalLabel_ withFontSize:SMALL_TEXT_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];

    
    [titleLabel_ setText:NSLocalizedString(PUBLIC_SERVICE_STEP_FOUR_CORRECT_PAYMENT_TITLE_TEXT_KEY, nil)];
    [infoLabel_ setText:NSLocalizedString(CARD_PAYMENT_SUCCESS_BOTTOM_TEXT_KEY, nil)];
    [infoLabel_ setNumberOfLines:2];
    [infoLabel_ setTextAlignment:UITextAlignmentCenter];
    
    [additionalLabel_ setNumberOfLines:2];
    [additionalLabel_ setTextAlignment:UITextAlignmentCenter];
    
    bottomInfoTextView_.textColor = [UIColor BBVAGreyToneTwoColor];
    bottomInfoTextView_.userInteractionEnabled = NO;
    bottomInfoTextView_.font = [NXT_Peru_iPhoneStyler normalFontWithSize:SMALL_TEXT_FONT_SIZE];
    
    [infoTable_ setUserInteractionEnabled:NO];
        
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releasePaymentSuccessViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    [self resetScrollToTopLeftAnimated:NO];
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    if([process_ paymentOperationType]== PTEPaymentOtherBank){
        [infoLabel_ setText:[process_ message]];
        [additionalLabel_ setText:[process_ notificationMessage]];
    }else{
    [additionalLabel_ setText:[process_ message]];
    }
    
    [self updateInformation];
    [self relocateViews];
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //[[NSNotificationCenter defaultCenter] removeObserver: self name: @"frequentOperationHasFinished" object: nil];
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.origin.y = CGRectGetHeight([headerView_ frame]);
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]) - CGRectGetHeight([headerView_ frame]);
    [self setScrollFrame:frame];
    
}

/**
 * Creates and returns an autoreleased PaymentSuccessViewController constructed from a NIB file.
 *
 * @return The autoreleased PaymentSuccessViewController constructed from a NIB file.
 */
+ (PaymentSuccessViewController *)paymentSuccessViewController {

    PaymentSuccessViewController *result =  [[[PaymentSuccessViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;

}

#pragma mark -
#pragma mark View format

/**
 * Update information
 */
- (void)updateInformation {

    NSString *headerTitle = @"";
    
    if (!isFoFinished_) {
        foButton_.hidden = FALSE;
    }
    
    switch ([process_ paymentOperationType]) {
            
        case PTEPaymentPSElectricServices:
                        
            headerTitle = NSLocalizedString(PUBLIC_SERVICE_ELECTRICITY_TEXT_KEY, nil);
            break;
            
        case PTEPaymentPSWaterServices:

            headerTitle = NSLocalizedString(PUBLIC_SERVICE_WATER_HEADER_TEXT_KEY, nil);
            break;
            
        case PTEPaymentPSCellular:
            
            headerTitle = [NSString stringWithFormat:NSLocalizedString(PUBLIC_SERVICE_SERVICES_OF_TEXT_KEY, nil),
                           NSLocalizedString(PUBLIC_SERVICE_CELLULAR_TEXT_LOWER_KEY, nil)];
            break;
            
        case PTEPaymentPSPhone:
            
            headerTitle = [NSString stringWithFormat:NSLocalizedString(PUBLIC_SERVICE_SERVICES_OF_TEXT_KEY, nil),
                           NSLocalizedString(PUBLIC_SERVICE_NOT_CAP_LANDLINE_TEXT_KEY, nil)];
            break;
            
        case PTEPaymentRecharge:
            
            [titleLabel_ setText:NSLocalizedString(RECHARGE_STEP_FOUR_CORRECT_PAYMENT_TITLE_TEXT_LEY, nil)];
            headerTitle = NSLocalizedString(PAYMENT_RECHARGE_TITLE_TEXT_KEY, nil);
            break;
            
        case PTEPaymentContOwnCard:
            
            headerTitle = NSLocalizedString(CARD_PAYMENT_OWN_CARD_TEXT_KEY, nil);
            foButton_.hidden = YES;
            break;
            
        case PTEPaymentContThirdCard:
            
            headerTitle = NSLocalizedString(CARD_PAYMENT_THIRD_CARD_TEXT_KEY, nil);
            break;
            
        case PTEPaymentOtherBank:
            
            headerTitle = NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TITLE_TEXT_KEY, nil);
            break;
        
        case PTEPaymentISService:
            
            headerTitle = NSLocalizedString(INTERNET_SHOPPING_STEP_TWO_TITLE_TEXT_KEY, nil);
            foButton_.hidden = YES;
            break;
            
            
        default:
            break;
    }
    
    [headerView_ setTitle:headerTitle];

}

/**
 * Relocate views
 */
- (void)relocateViews {

    CGFloat yPosition = EXTERIOR_VIEW_DISTANCE;
    
    CGRect frame = titleView_.frame;
    frame.origin.y = yPosition;
    titleView_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
    
    [infoTable_ reloadData];
    
    frame = infoTable_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [infoTable_ contentSize].height;
    infoTable_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
        
    frame = tableBgImageView_.frame;
    frame.origin.y = infoTable_.frame.origin.y + 3.0f;
    frame.size.height = [infoTable_ contentSize].height - 3.0f;
    tableBgImageView_.frame = frame;
    
    frame = infoLabel_.frame;
    frame.origin.y = yPosition;
    
    if([process_ paymentOperationType]== PTEPaymentOtherBank){
    [infoLabel_ setNumberOfLines:5];
    [[process_ message] sizeWithFont:[NXT_Peru_iPhoneStyler normalFontWithSize:14.0f] constrainedToSize:CGSizeMake(300, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    }
    infoLabel_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
  
    
    if ([additionalLabel_ text] != nil && ![[additionalLabel_ text] isEqualToString:@""]) {
        
        frame = [additionalLabel_ frame];
        frame.origin.y = yPosition;
       
        
        [additionalLabel_ setFrame:frame];
        
        
        
        yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
        
    }
    
    if([process_ paymentOperationType]!=PTEPaymentISService)
    {
        if (!isFoFinished_) {
            frame = foButton_.frame;
            frame.origin.y = yPosition;
            foButton_.frame = frame;
            
            yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
        }
    }
    
    
    
    frame = globalPositionButton_.frame;
    frame.origin.y = yPosition;
    globalPositionButton_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
    
    frame = paymentButton_.frame;
    frame.origin.y = yPosition;
    paymentButton_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
    
    [bottomInfoView_ setHidden:YES];
    
    NSString *text = [process_ successExtraInfoText];
    
    if ((text != nil) && ![text isEqualToString:@""]) {
        
        [bottomInfoView_ setHidden:NO];
        
        [bottomInfoTextView_ setText:text];
  
        
        CGRect otherFrame = bottomInfoTextView_.frame;
        otherFrame.origin.y = 3.0f;
        
        CGSize textViewSize = [text sizeWithFont:[NXT_Peru_iPhoneStyler normalFontWithSize:SMALL_TEXT_FONT_SIZE]
                               constrainedToSize:CGSizeMake(otherFrame.size.width, FLT_MAX)
                                   lineBreakMode:UILineBreakModeTailTruncation];
        textViewSize.height=textViewSize.height+20;
        otherFrame.size.height = textViewSize.height;
        bottomInfoTextView_.frame = otherFrame;
        
        otherFrame = bottomInfoBgImageView_.frame;
        otherFrame.origin.y = 0.0f;
        otherFrame.size.height = textViewSize.height + 6.0f;
        bottomInfoBgImageView_.frame = otherFrame;
        
        frame = bottomInfoView_.frame;
        frame.origin.y = yPosition;
        frame.size.height = bottomInfoBgImageView_.frame.size.height;
        bottomInfoView_.frame = frame;
        
        yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;

    }

    [self setScrollContentSize:CGSizeMake(320.0f, yPosition)];
    
}


#pragma mark -
#pragma mark User interaction
/**
 * The fo button has been tapped
 */
- (IBAction)foButtonTapped
{
    [self.appDelegate showActivityIndicator:poai_Both];
    
    foInscriptionBaseProcess_ = [[FOInscriptionBaseProcess alloc] init];
    [foInscriptionBaseProcess_ setDelegate:self];
    
    [process_ startFrequentOperationReactiveRequest];

}

/**
 * The payment button has been tapped
 */
- (IBAction)paymentButtonTapped {

    [[NSNotificationCenter defaultCenter] removeObserver: self  name:@"frequentOperationHasFinished" object: nil];
    [Session getInstance].globalPositionRequestServerData = YES;
    [[self navigationController] popToRootViewControllerAnimated:YES];

}

/**
 * The global position button has been tapped
 */
- (IBAction)globalPositionButtonTapped {

    [[NSNotificationCenter defaultCenter] removeObserver: self  name:@"frequentOperationHasFinished" object: nil];
    [Session getInstance].globalPositionRequestServerData = YES;
    [self.appDelegate displayGlobalPositionTab];

}

#pragma mark -
#pragma mark Setters

/*
 * Sets the process
 */
- (void)setProcess:(PaymentBaseProcess *)process {

    if (process_ != process) {
        
        [process retain];
        [process_ release];
        process_ = process;
        
    }
     [process_ setDelegate:self];
}

- (void)dataAnalysisHasFinished
{
    [self.appDelegate hideActivityIndicator];
    
    frequentOperationReactiveStepOneViewController_ = [FrequentOperationAlterStepOneViewControllerViewController frequentOperationAlterStepOneViewController];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(frequentOperationHasFinished:)  name: @"frequentOperationHasFinished" object: nil];
    
    [foInscriptionBaseProcess_ setRootViewController: self];
    [frequentOperationReactiveStepOneViewController_ setProcess: foInscriptionBaseProcess_];
    [frequentOperationReactiveStepOneViewController_ awakeFromNib];
    [self.navigationController pushViewController:frequentOperationReactiveStepOneViewController_ animated:YES];

}

-(void)frequentOperationHasFinished : (NSNotification *) notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:@"frequentOperationHasFinished" object: nil];
    foButton_.hidden = TRUE;
    isFoFinished_ = TRUE;
    
    [self relocateViews];
    
}

#pragma mark -
#pragma mark UITableView methods

/**
 * Tells the data source to return the number of rows in a given section of a table view. (required)
 *
 * @param tableView: The table-view object requesting this information.
 * @param section: An index number identifying a section in tableView.
 * @return The number of rows in section.
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        
    return [[process_ successInfoArray] count];
    
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. (required)
 *
 * @param tableView: A table-view object requesting the cell.
 * @param indexPath: An index path locating a row in tableView.
 * @return An object inheriting from UITableViewCellthat the table view can use for the specified row.
 */
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
            
    InformationCell *result = (InformationCell *)[tableView dequeueReusableCellWithIdentifier:[InformationCell cellIdentifier]];
    
    if (result == nil) {
            
        result = [[[InformationCell informationCell] retain] autorelease];
            
    }
        
    TitleAndAttributes *titleAndAttribute = [[process_ successInfoArray] objectAtIndex:[indexPath row]];
    [result setInfo:titleAndAttribute];
    
    return result;
                
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 *
 * @param tableView: The table-view object requesting this information.
 * @param indexPath: An index path that locates a row in tableView.
 * @return A floating-point value that specifies the height (in points) that row should be.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    InformationCell * result = [[[InformationCell informationCell] retain] autorelease];
    TitleAndAttributes *titleAndAttribute = [[process_ successInfoArray] objectAtIndex:[indexPath row]];
    [result setInfo:titleAndAttribute];
    return [result dynamicCellHeight];
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    result.hidesBackButton = YES;

    NSString *title = @"";
    
    switch ([process_ paymentOperationType]) {
        
        case PTEPaymentPSElectricServices:
        case PTEPaymentPSWaterServices:
        case PTEPaymentPSCellular:
        case PTEPaymentPSPhone:
            
            title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY, nil)];
            
            break;
            
        case PTEPaymentRecharge:
            
            title = NSLocalizedString(PAYMENT_RECHARGES_TITLE_TEXT_KEY, nil);
            
            break;
            
        case PTEPaymentContOwnCard:
        case PTEPaymentContThirdCard:
        case PTEPaymentOtherBank:
            
            title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_CARDS_TITLE_LOWER_TEXT_KEY, nil)];
            
            break;
            
        case PTEPaymentISService:
            title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(INTERNET_SHOPPING_TITLE_TEXT_KEY, nil)];
            
            break;

        default:
            break;
    }
    
    result.mainTitle = title;
    
    return result;
    
}

@end
