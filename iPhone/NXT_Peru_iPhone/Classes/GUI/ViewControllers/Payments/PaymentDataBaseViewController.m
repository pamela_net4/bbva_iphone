/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PaymentDataBaseViewController.h"

#import "EmailMokCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKDoubleLabel.h"
#import "MOKEditableViewController.h"
#import "MOKEditableViewController+protected.h"
#import "MOKNavigationItem.h"
#import "MOKStringListSelectionButton.h"
#import "MOKTextField.h"
#import "MOKTextView.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentBaseProcess.h"
#import "PaymentCell.h"
#import "PaymentConfirmationViewController.h"
#import "PaymentsConstants.h"
#import "Session.h"
#import "SimpleHeaderView.h"
#import "SMSMokCell.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "Updater.h"

/**
 * Defines de max text lenght for the mail
 */
#define MAX_TEXT_MAIL_LENGHT                    80

/**
 * PaymentDataBaseViewController private extension
 */
@interface PaymentDataBaseViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releasePaymentDataBaseViewControllerGraphicElements;

/**
 * Fixes the add contact button images
 */
- (void)fixAddContactButtonImages;

/**
 * Display the contact selection 
 */
- (void)displayContactSelection;

/**
 * Save contact information into the helper
 */
- (void)saveContactInfoIntoHelper;

@end


#pragma mark -

@implementation PaymentDataBaseViewController

#pragma mark -
#pragma mark Properties

@synthesize firstView = firstView_;
@synthesize secondView = secondView_;
@synthesize thirdView = thirdView_;
@synthesize fourthView = fourthView_;
@synthesize continueButton = continueButton_;
@synthesize brandingLine = brandingLine_;
@synthesize process = process_;
@synthesize hasNavigateForward = hasNavigateForward_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releasePaymentDataBaseViewControllerGraphicElements];
    
    [process_ release];
    process_ = nil;
    
    [paymentConfirmationViewController_ release];
    paymentConfirmationViewController_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasePaymentDataBaseViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releasePaymentDataBaseViewControllerGraphicElements {
    
	[firstView_ release];
    firstView_ = nil;

	[secondView_ release];
    secondView_ = nil;
    
    [thirdView_ release];
    thirdView_ = nil;
    
    [fourthView_ release];
    fourthView_ = nil;
    
    [continueButton_ release];
    continueButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
    [selectionTableView_ release];
    selectionTableView_ = nil;
    
    [emailCell_ release];
    emailCell_ = nil;

    [smsCell_ release];
    smsCell_ = nil;
    
    [fourthHeaderView_ release];
    fourthHeaderView_ = nil;

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    CGRect mainFrame = [[UIScreen mainScreen] bounds];
    [[self view] setFrame:mainFrame];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    [firstView_ setBackgroundColor:[UIColor clearColor]];
    [secondView_ setBackgroundColor:[UIColor clearColor]];
    [thirdView_ setBackgroundColor:[UIColor clearColor]];
    [NXT_Peru_iPhoneStyler styleNXTView:fourthView_];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:continueButton_];
    [continueButton_ setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil) 
                     forState:UIControlStateNormal];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    [[self view] bringSubviewToFront:brandingLine_];
    
    selectionTableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 20.0f) 
                                                       style:UITableViewStylePlain];
    
    [selectionTableView_ setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
    selectionTableView_.backgroundView.backgroundColor = nil;
    [selectionTableView_ setDelegate:self];
    [selectionTableView_ setDataSource:self];
    [selectionTableView_ setBackgroundColor:[UIColor clearColor]];
    
    fourthHeaderView_ = [[SimpleHeaderView simpleHeaderView] retain];
    [fourthHeaderView_ setTitle:NSLocalizedString(PAYMENT_REPORT_TO_BENEFICIARY_TEXT_KEY, nil)];
    
    [fourthView_ addSubview:fourthHeaderView_];
    [fourthView_ addSubview:selectionTableView_];
    
    [process_ resetSMSAndEmailInformation];
        
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releasePaymentDataBaseViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    // when navigation forware don't clear data.
    if (!hasNavigateForward_) {

        [self switchButtonHasBeenTapped:NO];
        [self emailSwitchButtonHasBeenTapped:NO];
        [process_ resetSMSAndEmailInformation];
    }
    
    if (isContactListShowed_) {
        if ([process_ showSMS1]) {
            [[smsCell_ firstTextField] becomeFirstResponder];
            [[smsCell_ firstTextField] resignFirstResponder];
        }else if ([process_ showEmail1]){
            [[emailCell_ firstTextField] becomeFirstResponder];
            [[emailCell_ firstTextField] resignFirstResponder];
        }
        
        isContactListShowed_ = FALSE;
    }

    [self viewForFourthView];

}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

#pragma mark -
#pragma mark View format

/**
 * Relocate views
 */
- (void)relocateViews {

    CGFloat yPosition = 0.0f;
    
    CGRect frame = firstView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [self heightForFirstView];
    firstView_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    frame = secondView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [self heightForSecondView];
    secondView_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    frame = thirdView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [self heightForThirdView];
    thirdView_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    frame = fourthView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [self heightForFourthView];
    fourthView_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
    
    frame = continueButton_.frame;
    frame.origin.y = yPosition;
    continueButton_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
    
    [self setScrollContentSize:CGSizeMake(320.0f, yPosition)];
    
}

#pragma mark -
#pragma mark User interaction

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped {
    
    [self saveContactInfoIntoHelper];

    [process_ startPaymentConfirmationRequest];

}

/**
 * Fixes the add contact button images
 */
- (void)fixAddContactButtonImages {
    
    if (([process_ destinationSMS1] == nil) || [[process_ destinationSMS1] isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                           forState:UIControlStateNormal];
        }else{
            [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
        }
    } else {
        
        [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                           forState:UIControlStateNormal];
    }
    
    if (([process_ destinationSMS2] == nil) || [[process_ destinationSMS2] isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                            forState:UIControlStateNormal];
        }else{
            [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
        }
    } else {
        
        [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                            forState:UIControlStateNormal];
    }
    
    if (([process_ destinationEmail1] == nil) || [[process_ destinationEmail1] isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                             forState:UIControlStateNormal];
        }else{
            [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
        }
    } else {
        
        [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                             forState:UIControlStateNormal];
    }
    
    if (([process_ destinationEmail2] == nil) || [[process_ destinationEmail2] isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                              forState:UIControlStateNormal];
        }else{
            [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]                                              forState:UIControlStateNormal];
        }
    } else {
        
        [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                              forState:UIControlStateNormal];
    }
    
    
}

/*
 * Display the contact selection 
 */
- (void)displayContactSelection {
    
    hasNavigateForward_ = YES;
    [self saveProcessInformation];
    
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
        
    [[picker navigationBar] setTintColor:[[[self navigationController] navigationBar] tintColor]];
    
    [self.appDelegate presentModalViewController:picker animated:YES];
    
    isContactListShowed_ = TRUE;
    
    [picker release]; 
    
}

/*
 * Save contact information into the helper
 */
- (void)saveContactInfoIntoHelper {
        
    [process_ setSendSMS:[[smsCell_ smsSwitch] isOn]];
    
    [process_ setDestinationSMS1:smsCell_.firstTextField.text];
    [process_ setDestinationSMS2:smsCell_.secondTextField.text];
    
    if ([process_ showSMS1]) {
        [process_ setSelectedCarrier1Index:smsCell_.firstComboButton.selectedIndex];
    }else{
        [process_ setSelectedCarrier1Index:-1];
    }
    
    if ([process_ showSMS1]) {
        [process_ setSelectedCarrier2Index:smsCell_.secondComboButton.selectedIndex];
    }else{
        [process_ setSelectedCarrier2Index:-1];
    }
    
    [process_ setSendEmail:[[emailCell_ emailSwitch] isOn]];
    [process_ setDestinationEmail1:emailCell_.firstTextField.text];
    [process_ setDestinationEmail2:emailCell_.secondTextField.text];
    [process_ setEmailMessage:emailCell_.emailTextView.text];
    
}

/*
 * Save process information. By default does nothing. Each child must overrride the method if needed
 */
- (void)saveProcessInformation {
    
    
    
}

#pragma mark -
#pragma mark Views methods

/**
 * Returns the view needed in the first view. NIL if it is not needed
 */
- (void)viewForFirstView {

    firstView_ = nil;
    
}

/**
 * Returns the view needed in the second view. NIL if it is not needed
 */
- (void)viewForSecondView {

    secondView_ = nil;

}

/**
 * Returns the view needed in the third view. NIL if it is not needed
 */
- (void)viewForThirdView {
    
    thirdView_ = nil;

}

/**
 * Returns the view needed in the third view. NIL if it is not needed
 */
- (void)viewForFourthView {
    
    CGFloat yPosition = 0.0f;
    
    CGRect frame = fourthHeaderView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    fourthHeaderView_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);

    [selectionTableView_ reloadData];
    
    frame = selectionTableView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [selectionTableView_ contentSize].height;
    selectionTableView_.frame = frame;
    
    [selectionTableView_ setScrollEnabled:NO];
    
    yPosition = CGRectGetMaxY(frame) + 30.0f;
        
    fourthViewHeight_ = yPosition;

}

/**
 * Returns the height for the first view. 0 if it is NIL
 */
- (CGFloat)heightForFirstView {

    return 0.0f;

}

/**
 * Returns the height for the second view. 0 if it is NIL
 */
- (CGFloat)heightForSecondView {

    return 0.0f;

}

/**
 * Returns the height for the third view. 0 if it is NIL
 */
- (CGFloat)heightForThirdView {
    
    return 0.0f;
    
}

/**
 * Returns the height for the third view. 0 if it is NIL
 */
- (CGFloat)heightForFourthView {
    
    return fourthViewHeight_;
    
}

#pragma mark -
#pragma mark Setters

/*
 * Sets the process
 */
- (void)setProcess:(PaymentBaseProcess *)process {

    if (process_ != process) {
        
        [process retain];
        [process_ release];
        process_ = process;
        
    }

    [process_ setDelegate:self];
    
}

#pragma mark -
#pragma mark UITableView methods

/**
 * Tells the data source to return the number of rows in a given section of a table view. (required)
 *
 * @param tableView: The table-view object requesting this information.
 * @param section: An index number identifying a section in tableView.
 * @return The number of rows in section.
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        
    return 2;
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. (required)
 *
 * @param tableView: A table-view object requesting the cell.
 * @param indexPath: An index path locating a row in tableView.
 * @return An object inheriting from UITableViewCellthat the table view can use for the specified row.
 */
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
                
    if (tableView == selectionTableView_) {
        
        if (indexPath.row == 0) {
            
            SMSMokCell *result = (SMSMokCell *)[tableView dequeueReusableCellWithIdentifier:[SMSMokCell cellIdentifier]];
            
            if (result == nil) {
                
                if (smsCell_ == nil) {
                    
                    smsCell_ = [[SMSMokCell smsMokCell] retain];
                    
                }
                
                result = smsCell_;
                
            }
            
            result.delegate = self;
            result.showSeparator = NO;
            
            [result.titleLabel setText:NSLocalizedString(NOTIFICATIONS_TABLE_SEND_PHONE_TEXT_KEY, nil)];
            result.smsSwitch.on = [process_ sendSMS];
            
            result.firstTextField.text = [process_ destinationSMS1];
            result.firstTextField.delegate = self;
            
            result.selectedFirstOperatorIndex = [process_ selectedCarrier1Index];
            [result.firstComboButton setStringListSelectionButtonDelegate:self];
            
            result.secondTextField.text = [process_ destinationSMS2];
            result.secondTextField.delegate = self;
            
            result.selectedSecondOperatorIndex = [process_ selectedCarrier2Index];
            [[result secondComboButton] setStringListSelectionButtonDelegate:self];
            
            [result setOperatorArray:[process_ carrierList] 
                          firstAddOn:[process_ showSMS1]
                         secondAddOn:[process_ showSMS2]];
            
            [result.firstTextField setDelegate:self];
            [result.secondTextField setDelegate:self];
            
            result.backgroundColor = [UIColor clearColor];
            return result;
            
        } else {
            
            EmailMokCell *result = (EmailMokCell *)[tableView dequeueReusableCellWithIdentifier:[EmailMokCell cellIdentifier]];
            
            if (result == nil) {
                
                if(emailCell_ == nil) {
                    
                    emailCell_ = [[EmailMokCell emailMokCell] retain];
                }
                result = emailCell_;
                
            }
            
            result.delegate = self;
            result.showSeparator = YES;
            
            [result.titleLabel setText:NSLocalizedString(NOTIFICATIONS_TABLE_SEND_EMAIL_TEXT_KEY, nil)];
            result.emailSwitch.on = [process_ sendEmail];
            
            result.firstTextField.text = [process_ destinationEmail1];            
            result.secondTextField.text = [process_ destinationEmail2];
            result.emailTextView.text = [process_ emailMessage];
            
            [result.firstTextField setDelegate:self];
            [result.secondTextField setDelegate:self];
            [result.emailTextView setDelegate:self]; 
            
            [self fixAddContactButtonImages];
            
            [result setConfigurationForFirstAddOn:[process_ showEmail1] 
                                      secondAddOn:[process_ showEmail2]];
            
            result.backgroundColor = [UIColor clearColor];
            
            [self lookForEditViews];
            
            return result;
            
        }
        
    } else {
        
        return nil;
        
    }
        
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 *
 * @param tableView: The table-view object requesting this information.
 * @param indexPath: An index path that locates a row in tableView.
 * @return A floating-point value that specifies the height (in points) that row should be.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat result = 0.0f;

    if (indexPath.row == 0) {
        result = [SMSMokCell cellHeightForFirstAddOn:[process_ showSMS1]  
                                         secondAddOn:[process_ showSMS2]];
    } else {
        result = [EmailMokCell cellHeightForFirstAddOn:[process_ showEmail1] 
                                           secondAddOn:[process_ showEmail2]];
    }

    return result;
}

/**
 * Tells the delegate that the specified row is now selected.
 *
 * @param tableView: A table-view object informing the delegate about the new row selection.
 * @param indexPath: An index path locating the new selected row in tableView.
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

#pragma mark
#pragma mark UITextField Delegate

/** 
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    if( (textField == [smsCell_ firstTextField]) || (textField == [smsCell_ secondTextField]) ) {
        
        if(resultLength == 0) {
            
            result = YES;
        }
        else if(resultLength <= 9) {
            
            NSScanner *scanner = [NSScanner scannerWithString:resultString];
            NSInteger resultInteger = 0;
            
            if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                if (resultInteger >= 0) {
                    
                    result = YES;                                        
                }                
            } 
        }        
    }
    else if ( (textField == [emailCell_ firstTextField]) || (textField == [emailCell_ secondTextField]) ) {
        
        if (resultLength <= 50) {
            
            result = YES;                                        
            
        }           
    }
    
    
    if (resultLength == 0) {
        
        if (textField == smsCell_.firstTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
            }else{
                [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                   forState:UIControlStateNormal];
            }
        } else if (textField == smsCell_.secondTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                forState:UIControlStateNormal];
            }else{
                [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                   forState:UIControlStateNormal];
            }
        } else if (textField == emailCell_.firstTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                 forState:UIControlStateNormal];
            }else{
                [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                   forState:UIControlStateNormal];
            }
        } else if (textField == emailCell_.secondTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                                  forState:UIControlStateNormal];
            }
            else{
                [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                   forState:UIControlStateNormal];
            }
        }
        
    } else {
        
        if (textField == smsCell_.firstTextField) {
            
            [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                               forState:UIControlStateNormal];
            
        } else if (textField == smsCell_.secondTextField) {
            
            [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                                forState:UIControlStateNormal];
            
        } else if (textField == emailCell_.firstTextField) {
            
            [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                                 forState:UIControlStateNormal];
            
        } else if (textField == emailCell_.secondTextField) {
            
            [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                                  forState:UIControlStateNormal];
            
        }
        
    }
    
    return result;
    
}


#pragma mark -
#pragma mark UITextView Delegate

/**
 * Asks the delegate whether the specified text should be replaced in the text view.
 *
 * @param textView The text view containing the changes.
 * @param range The current selection range.
 * @param text The text to insert.
 */
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    BOOL result = YES;
    
    NSString *resultString = [textView.text stringByReplacingCharactersInRange:range
                                                                    withString:text];
    
    if (textView == [emailCell_ emailTextView]) {
        
        if ([resultString length] < MAX_TEXT_MAIL_LENGHT) {
            
            result = ([Tools isValidText:text forCharacterSet:[NSCharacterSet alphanumericCharacterSet]] || [Tools isValidText:text forCharacterSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);
            
        }
        else {
            result = NO;
        }
        
    } 
    
    return result;
    
}

#pragma mark
#pragma mark MOKStringListSelectionButton Delegate

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton {

    //NSLog(@"selectedIndex : %i",[stringListSelectionButton selectedIndex]);
    
    if (stringListSelectionButton == [smsCell_ firstComboButton]) {
        
        [process_ setSelectedCarrier1Index:[stringListSelectionButton selectedIndex]];
        
    } else if (stringListSelectionButton == [smsCell_ secondComboButton]) {
        
        [process_ setSelectedCarrier2Index:[stringListSelectionButton selectedIndex]];
        
    }

}

#pragma mark -
#pragma mark SMSCell Delegate

/**
 * Switch has been tapped.
 */
- (void)switchButtonHasBeenTapped:(BOOL)on {    
    
    [self saveContactInfoIntoHelper];
    
    [process_ setSendSMS:on];
    [process_ setShowSMS1:on];
    
    if (!on) {
        
        addingFirstSMS_ = NO;
        addingSecondSMS_ = NO;
        
        [process_ setShowSMS2:on];
        
        smsCell_.firstTextField.text = @"";
        [process_ setDestinationSMS1:@""];
        
        smsCell_.secondTextField.text = @"";
        [process_ setDestinationSMS2:@""];
        
        [[smsCell_ firstComboButton] setSelectedIndex:-1];
        [process_ setSelectedCarrier1Index:-1];
        
        [[smsCell_ secondComboButton] setSelectedIndex:-1];
        [process_ setSelectedCarrier2Index:-1];
        
    }
    else {
        
        [process_ setShowSMS2:NO];
        [process_ setDestinationSMS1:smsCell_.firstTextField.text];
        [process_ setDestinationSMS2:smsCell_.secondTextField.text];
        
        [[smsCell_ firstComboButton] setSelectedIndex:-1];
        [process_ setSelectedCarrier1Index:-1];
        
    }
    
    [self fixAddContactButtonImages];
    
	[self lookForEditViews];
    [self viewForFourthView];    
    [self relocateViews];
    
}

/**
 * More button has been tapped
 */
- (void)moreButtonHasBeenTapped {
        
    [self saveContactInfoIntoHelper];
    [process_ setShowSMS2:YES];
    
    [[smsCell_ secondComboButton] setSelectedIndex:-1];
    [process_ setSelectedCarrier2Index:-1];
    
	[self lookForEditViews];
    [self viewForFourthView];
    [self relocateViews];    
}

/**
 * Add First Contact Button Tapped
 */
- (void)addFirstContactHasBeenTapped {
    
    if ((smsCell_.firstTextField.text == nil) || [smsCell_.firstTextField.text isEqualToString:@""]) {
        
        addingFirstSMS_ = YES;
        [self displayContactSelection];
        
    } else {
        
        smsCell_.firstTextField.text = @"";
        [process_ setDestinationSMS1:@""];
        
    }
    
    [self fixAddContactButtonImages];
    
}

/**
 * Add Second Contact Button Tapped
 */
- (void)addSecondContactHasBeenTapped {
    
    if ((smsCell_.secondTextField.text == nil) || [smsCell_.secondTextField.text isEqualToString:@""]) {
        
        addingSecondSMS_ = YES;
        [self displayContactSelection];
        
    } else {
        
        smsCell_.secondTextField.text = @"";
        [process_ setDestinationSMS2: @""];
        
    }
    
    [self fixAddContactButtonImages];
    
}

/*
 * First combo button tapped
 */
- (void)firstComboButtonTapped {


}

/*
 * Second combo button tapped
 */
- (void)secondComboButtonTapped {


}


#pragma mark -
#pragma mark EmailCell Delegate

/**
 * Switch has been tapped.
 *
 * @param on The flag
 */
- (void)emailSwitchButtonHasBeenTapped:(BOOL)on {
    
    [self saveContactInfoIntoHelper];
    
    [process_ setSendEmail:on];
    [process_ setShowEmail1:on];
    
    if (!on) {
        
        addingFirstEmail_ = NO;
        addingSecondEmail_ = NO;
        
        [process_ setShowEmail2:on];
        
        emailCell_.firstTextField.text = @"";
        [process_ setDestinationEmail1:@""];
        
        emailCell_.secondTextField.text = @"";
        [process_ setDestinationEmail2:@""];
        
        [[emailCell_ emailTextView] setText:@""];
        [process_ setEmailMessage:@""];
        
        
    } else {
      
        [process_ setShowEmail2:NO];
        [process_ setDestinationEmail1:emailCell_.firstTextField.text];
        [process_ setDestinationEmail2:emailCell_.secondTextField.text];
        [process_ setMessage:emailCell_.emailTextView.text];
    }
    
    [self fixAddContactButtonImages];
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
    
    [selectionTableView_ reloadRowsAtIndexPaths:array 
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [array release];
    
    [self viewForFourthView];
    [self relocateViews];  
    [self lookForEditViews];

}

/**
 * Add First Contact Button Tapped
 */
- (void)emailAddFirstContactHasBeenTapped {
    
    if ((emailCell_.firstTextField.text == nil) || [emailCell_.firstTextField.text isEqualToString:@""]) {
        
        addingFirstEmail_ = YES;  
        [self displayContactSelection];
        
    } else {
        
        emailCell_.firstTextField.text = @"";
        [process_ setDestinationEmail1:@""];
        
    }
    
    [self fixAddContactButtonImages];
    
}

/**
 * Add Second Contact Button Tapped
 */
- (void)emailAddSecondContactHasBeenTapped {
    
    if ((emailCell_.secondTextField.text == nil) || [emailCell_.secondTextField.text isEqualToString:@""]) {
        
        addingSecondEmail_ = YES;        
        [self displayContactSelection];
        
    } else {
        
        emailCell_.secondTextField.text = @"";
        [process_ setDestinationEmail2:@""];
        
    }
    
    [self fixAddContactButtonImages];
    
}

/**
 * More button has been tapped
 */
- (void)emailMoreButtonHasBeenTapped {
    
    [self saveContactInfoIntoHelper];
    [process_ setShowEmail2:YES];
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
    
    [selectionTableView_ reloadRowsAtIndexPaths:array 
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [array release];
    
	
    [self viewForFourthView];
    [self relocateViews];
    [self lookForEditViews];
}

#pragma mark -
#pragma mark ABPeoplePickerNavigationControllerDelegate methods
/**
 * Called in ios 8 redirection to shouldContinueAfterSelectingPerson used in ios 7 or previous
 */
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    
    [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person property:property identifier:identifier];
}

/**
 * Called after the user has pressed cancel. The delegate is responsible for dismissing the peoplePicker
 */
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    
	[self.appDelegate dismissModalViewControllerAnimated:YES];
    
    [self fixAddContactButtonImages];
    addingFirstSMS_ = NO;
    addingSecondSMS_ = NO;
    addingFirstEmail_ = NO;
    addingSecondEmail_ = NO;
    
}

/**
 * Called after a person has been selected by the user.
 *
 * Return YES if you want the person to be displayed.
 * Return NO  to do nothing (the delegate is responsible for dismissing the peoplePicker).
 */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    
	[self.appDelegate dismissModalViewControllerAnimated:YES];
    
    if (addingFirstSMS_ || addingSecondSMS_) {
        
        BOOL isValidMobilePhoneNumber = NO;
        
        ABMultiValueRef phoneProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
        
        if (ABMultiValueGetCount(phoneProperty) > 0) {
            
            ABMultiValueRef phoneValue;
            NSString *phoneNumber;
            
            for (int i = 0; ABMultiValueGetCount(phoneProperty) > i; i++) {
                
                phoneValue = ABMultiValueCopyValueAtIndex(phoneProperty,i);
                
                phoneNumber = (NSString *)phoneValue;
                
                phoneNumber = [Tools mobileNumberLikeFromString:phoneNumber];
                
                isValidMobilePhoneNumber = [Tools isValidMobilePhoneNumberString:phoneNumber];
                
                if (isValidMobilePhoneNumber) {
                    break;
                }
            }
            
            if (isValidMobilePhoneNumber) {
                
                if (addingFirstSMS_) {
                    
                    [process_ setDestinationSMS1:phoneNumber];
                    
                } else if (addingSecondSMS_) {
                    
                    [process_ setDestinationSMS2:phoneNumber];
                    
                }
                
            }
            
            
            if (phoneValue != nil) {
                
                CFRelease(phoneValue);
                
            }
            
        }
        
        if (!isValidMobilePhoneNumber) {
            
            [Tools showInfoWithMessage:NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_NO_EXIST_TEXT_KEY, nil)];
            
        }
        
        addingFirstSMS_ = NO;
        addingSecondSMS_ = NO;
        
        if (phoneProperty != nil) {
            
            CFRelease(phoneProperty);
            
        }
                
    } else if (addingFirstEmail_ || addingSecondEmail_) {
        
        ABMultiValueRef emailProperty = ABRecordCopyValue(person, kABPersonEmailProperty);
        NSInteger emailsNumber = ABMultiValueGetCount(emailProperty);
        
        if (emailsNumber > 0) {
            
            //if (emailsNumber == 1) {
                
                ABMultiValueRef emailValue = ABMultiValueCopyValueAtIndex(emailProperty, 0);
                
                if (addingFirstEmail_) {
                    
                    [process_ setDestinationEmail1:(NSString *)emailValue];
                    addingFirstEmail_ = NO;
                    
                } else if (addingSecondEmail_) {
                    
                    [process_ setDestinationEmail2:(NSString *)emailValue];
                    addingSecondEmail_ = NO;
                    
                }
                
                if (emailValue != nil) {
                    
                    CFRelease(emailValue);
                    
                }
                
            //}
        
        } else {
            
            [Tools showInfoWithMessage:NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_NO_EXIST_TEXT_KEY, nil)];
            
            addingFirstEmail_ = NO;
            addingSecondEmail_ = NO;
            
        }
        
        if (emailProperty != nil) {
            
            CFRelease(emailProperty);
            
        }        
        
    }
    
    [self fixAddContactButtonImages];
    
  	return NO;
}

/**
 * Called after a value has been selected by the user.
 *
 * Return YES if you want default action to be performed.
 * Return NO to do nothing (the delegate is responsible for dismissing the peoplePicker).
 */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker 
      shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property 
                              identifier:(ABMultiValueIdentifier)identifier {
    
    return [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person];
    
}

#pragma mark -
#pragma mark PaymentBaseProcessDelegate

/*
 * Delegate is notified when the initialization has finished 
 */
- (void)confirmationAnalysisHasFinished {

    if (paymentConfirmationViewController_ == nil) {
        
        paymentConfirmationViewController_ = [[PaymentConfirmationViewController paymentConfirmationViewController] retain];
        
    }

    hasNavigateForward_ = YES;
    
    [paymentConfirmationViewController_ setProcess:process_];
    [self.navigationController pushViewController:paymentConfirmationViewController_ 
                                         animated:YES];
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    NSString *title = @"";
    
    switch ([process_ paymentOperationType]) {
        
        case PTEPaymentPSElectricServices:
        case PTEPaymentPSWaterServices:
        case PTEPaymentPSCellular:
        case PTEPaymentPSPhone:
            
            title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY, nil)];
            
            break;
            
        case PTEPaymentRecharge:
            
            title = NSLocalizedString(PAYMENT_RECHARGE_OPERATION_TEXT_KEY, nil);
            
            break;
            
        case PTEPaymentContOwnCard:
        case PTEPaymentContThirdCard:
        case PTEPaymentOtherBank:
            
            title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_CARDS_TITLE_LOWER_TEXT_KEY, nil)];
            
            break;

        default:
            break;
    }
    
    result.mainTitle = title;
    
    return result;
    
}

@end
