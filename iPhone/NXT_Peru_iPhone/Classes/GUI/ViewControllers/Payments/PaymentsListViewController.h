/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTViewController.h"
#import "PaymentBaseProcess.h"

//Forward declarations
@class CardPaymentListViewController;
@class PublicServListViewController;
@class RechargeViewController;
@class PaymentRechargeProcess;
@class InstitutionsAndCompaniesViewController;
@class InternetShoppingStepOneViewController;



/**
 * View controller to show all payments options.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentsListViewController : NXTViewController<UITableViewDelegate, UITableViewDataSource, PaymentBaseProcessDelegate> {	
    
@private
    
    /**
     * Table view
     */
    UITableView *table_;
    
    /**
     * Public Services List View Controller
     */
    PublicServListViewController *publicServListViewController_;
    
    /**
     * Card Payment List View Controller
     */
    CardPaymentListViewController *cardPaymentListViewController_;
    
    /**
     * Institutions and Companies View Controller
     */
    InstitutionsAndCompaniesViewController *institutionsAndCompaniesViewController_;
        /**
     * Recharge View Controller
     */
    RechargeViewController *rechargeViewController_;
    
    /**
     * Payment recharge process
     */
    PaymentRechargeProcess *process_;
    
    InternetShoppingStepOneViewController *internetShoppingViewController_;
}


@property (retain,readwrite, nonatomic) IBOutlet UITableView *table;


/**
 * Creates and returns an autoreleased PaymentsListViewController constructed from a NIB file.
 *
 * @return The autoreleased PaymentsListViewController constructed from a NIB file.
 */
+ (PaymentsListViewController *)paymentsListViewController;
@end
