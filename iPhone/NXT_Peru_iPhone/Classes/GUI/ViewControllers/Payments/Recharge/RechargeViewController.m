 /*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "RechargeViewController.h"

#import "BankAccount.h"
#import "Carrier.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKDoubleLabel.h"
#import "MOKEditableViewController.h"
#import "MOKEditableViewController+protected.h"
#import "MOKNavigationItem.h"
#import "MOKStringListSelectionButton.h"
#import "MOKCurrencyTextField.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentBaseProcess.h"
#import "PaymentRechargeProcess.h"
#import "PaymentsConstants.h"
#import "Session.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "Updater.h"

/**
 * Defines the Nib file name
 */
#define NIB_FILE_NAME                                       @"RechargeViewController"

/**
 * Short vertical distance
 */
#define SHORT_VERTICAL_DISTANCE                             5.0f

/**
 * Long vertical distance
 */
#define LONG_VERTICAL_DISTANCE                              10.0f


/**
 * RechargeViewController private extension
 */
@interface RechargeViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releaseRechargeViewControllerGraphicElements;

/**
 * Updates the interface information
 *
 * @private
 */
- (void)updateInterfaceInfo;

@end

#pragma mark -

@implementation RechargeViewController

#pragma mark -
#pragma mark Properties

@synthesize companyLabel = companyLabel_;
@synthesize companyCombo = companyCombo_;
@synthesize phoneLabel = phoneLabel_;
@synthesize phoneHelperLabel = phoneHelperLabel_;
@synthesize phoneTextField = phoneTextField_;
@synthesize amountLabel = amountLabel_;
@synthesize amountTextField = amountTextField_;
@synthesize clueLabel = clueLabel_;
@synthesize accontCombo = accontCombo_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseRechargeViewControllerGraphicElements];
    
    [clueString_ release];
    clueString_ = nil;
    
    [processRecharge_ release];
    processRecharge_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseRechargeViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseRechargeViewControllerGraphicElements {
        
    [firstViewHeader_ release];
    firstViewHeader_ = nil;
    
    [firstViewSeparator_ release];
    firstViewSeparator_ = nil;
    
    [companyLabel_ release];
    companyLabel_ = nil;

    [companyCombo_ release];
    companyCombo_ = nil;
    
    [phoneLabel_ release];
    phoneLabel_ = nil;
    
    [phoneHelperLabel_ release];
    phoneHelperLabel_ = nil;
    
    [phoneTextField_ release];
    phoneTextField_ = nil;
    
    [amountLabel_ release];
    amountLabel_ = nil;
    
    [amountTextField_ release];
    amountTextField_ = nil;
    
    [clueLabel_ release];
    clueLabel_ = nil;
    
    [secondViewHeader_ release];
    secondViewHeader_ = nil;
    
    [secondViewSeparator_ release];
    secondViewSeparator_ = nil;
    
    [accontCombo_ release];
    accontCombo_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    if (clueString_ == nil) {
        
        clueString_ = [[NSMutableString alloc] init];
        
    }
        
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
    [NXT_Peru_iPhoneStyler styleAccountSelectionButton:accontCombo_];
    [NXT_Peru_iPhoneStyler styleStringListButton:companyCombo_];

    [accontCombo_ setStringListSelectionButtonDelegate:self];
    [companyCombo_ setStringListSelectionButtonDelegate:self];
    [companyCombo_ setNoSelectionText:NSLocalizedString(PAYMENT_RECHARGE_BUSINESS_COMBO_DEFAULT_TEXT_KEY, nil)];

    [NXT_Peru_iPhoneStyler styleMokTextField:amountTextField_ withFontSize:14.0f andColor:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleMokTextField:phoneTextField_ withFontSize:14.0f andColor:[UIColor grayColor]];

    [NXT_Peru_iPhoneStyler styleNXTView:[self firstView]];
    
    firstViewHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [firstViewHeader_ setTitle:NSLocalizedString(PAYMENT_RECHARGE_TITLE_TEXT_KEY, nil)];
    [[self firstView] addSubview:firstViewHeader_];
    firstViewSeparator_ = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 2.0f)];
    [firstViewSeparator_ setImage:[[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
    [[self firstView] addSubview:firstViewSeparator_];
    
    secondViewHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [secondViewHeader_ setTitle:NSLocalizedString(CARD_PAYMENT_CHOOSE_PAYMENT_MODE_TEXT_KEY, nil)];
    [[self secondView] addSubview:secondViewHeader_];
    secondViewSeparator_ = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 2.0f)];
    [secondViewSeparator_ setImage:[[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
    [[self secondView] addSubview:secondViewSeparator_];

    [NXT_Peru_iPhoneStyler styleLabel:companyLabel_ withFontSize:14.0f color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:phoneLabel_ withFontSize:14.0f color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:phoneHelperLabel_ withFontSize:12.0f color:[UIColor BBVAGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withFontSize:14.0f color:[UIColor BBVABlueColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:clueLabel_ withFontSize:11.0f color:[UIColor grayColor]];
    [clueLabel_ setNumberOfLines:2];
    
    [companyLabel_ setText:NSLocalizedString(PAYMENT_RECHARGE_BUSINESS_TITLE_TEXT_KEY, nil)];
    [phoneLabel_ setText:NSLocalizedString(PUBLIC_SERVICE_STEP_ONE_ENTRY_PHONE_NUMBER_OR_CLIENT_CODE_KEY, nil)];
    [phoneHelperLabel_ setText:NSLocalizedString(PAYMENT_RECHARGE_SET_NUMBRE_TEXT_KEY, nil)];
    [amountLabel_ setText:NSLocalizedString(PAYMENT_RECHARGE_AMOUNT_TEXT_KEY, nil)];

	amountTextField_.canContainCents = YES;
    amountTextField_.maxDecimalNumbers = 2;
    amountTextField_.currencySymbol = CURRENCY_SOLES_SYMBOL;
    amountTextField_.numberFormatter = [Tools amountToLocalStringFormatter];
    amountTextField_.placeholder = NSLocalizedString(CARD_PAYMEN_AMOUNT_TO_PAY_TEXT_KEY, nil);
    [amountTextField_ setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    
    [phoneTextField_ setDelegate:self];
    [clueLabel_ setNumberOfLines:10];

}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseRechargeViewControllerGraphicElements];

}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    [self resetScrollToTopLeftAnimated:YES];
        
    lastIndexDownloaded_ = -1;
    
    if (![self hasNavigateForward]) {
    
        [companyCombo_ setSelectedIndex:-1];
        [processRecharge_ setSelectedCarrierRechargeIndex:-1];
        
        [amountTextField_ setText:@""];
        [phoneTextField_ setText:@""];
        
        
        if ([[processRecharge_ carrierRechargeList] count] == 0) {
            
            lastIndexDownloaded_ = 0;
            [processRecharge_ startPaymentDataRequest];
            
        }

    } else {
        
        [processRecharge_ setDelegate:self];
        [amountTextField_ setText:[processRecharge_ amount]];
        [phoneTextField_ setText:[processRecharge_ phoneNumber]];
    }
    
    [self viewForFirstView];
    [self viewForSecondView];
    
    [companyCombo_ setSelectedIndex:[processRecharge_ selectedCarrierRechargeIndex]];
    
    [self updateInterfaceInfo];
    
    [self relocateViews];
    [self lookForEditViews];

}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view.
 *
 * @param animated: If YES, the disappearance of the view is being animated.
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];

}


/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Creates and returns an autoreleased RechargeViewController constructed from a NIB file.
 *
 * @return The autoreleased RechargeViewController constructed from a NIB file.
 */
+ (RechargeViewController *)rechargeViewController {

    RechargeViewController *result =  [[[RechargeViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;

}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]);
    [self setScrollFrame:frame];
    
}

#pragma mark -
#pragma mark Views methods

/**
 * Returns the view needed in the first view. NIL if it is not needed
 */
- (void)viewForFirstView {
    
    CGFloat yPosition = 0.0f;
    
    CGRect frame = firstViewHeader_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    firstViewHeader_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + LONG_VERTICAL_DISTANCE;
    
    frame = companyLabel_.frame;
    frame.origin.y = yPosition;
    companyLabel_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + SHORT_VERTICAL_DISTANCE;

    frame = companyCombo_.frame;
    frame.origin.y = yPosition;
    companyCombo_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + LONG_VERTICAL_DISTANCE;
    
    frame = phoneLabel_.frame;
    frame.origin.y = yPosition;
    phoneLabel_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + SHORT_VERTICAL_DISTANCE - 3.0f;
    
    frame = phoneHelperLabel_.frame;
    frame.origin.y = yPosition;
    phoneHelperLabel_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + SHORT_VERTICAL_DISTANCE + 3.0f;
    
    frame = phoneTextField_.frame;
    frame.origin.y = yPosition;
    phoneTextField_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + LONG_VERTICAL_DISTANCE;
    
    frame = amountLabel_.frame;
    frame.origin.y = yPosition;
    amountLabel_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + SHORT_VERTICAL_DISTANCE;
    
    frame = amountTextField_.frame;
    frame.origin.y = yPosition;
    amountTextField_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + LONG_VERTICAL_DISTANCE;
    
    NSArray *auxCompanies = [processRecharge_ carrierRechargeList];
    [clueString_ setString:@""];
    
    NSMutableDictionary *carriersMessagesDictionary = [NSMutableDictionary dictionary];
    NSString *key = nil;
    NSString *carrierMessage = nil;
    NSString *carrierDisplayName = nil;
    
    for (Carrier *carrier in auxCompanies) {
        
        carrierDisplayName = [carrier carrierDisplayName];
        key = [[carrier carrier] lowercaseString];
        carrierMessage = [NSString stringWithFormat:@"%@: %@", carrierDisplayName, [Tools notNilString:[carrier message]]];
        
        if ((carrierDisplayName != nil) && (key != nil) && (carrierMessage != nil)) {
            
            [carriersMessagesDictionary setObject:carrierMessage
                                           forKey:key];
            
        }
        
    }
    
    carrierMessage = [carriersMessagesDictionary objectForKey:kCarrierMovistarCode];
    
    if (carrierMessage != nil) {
        
        [clueString_ appendString:carrierMessage];
        
    }
    
    carrierMessage = [carriersMessagesDictionary objectForKey:kCarrierClaroCode];
    
    if (carrierMessage != nil) {
        
        if ([clueString_ length] > 0) {
            
            [clueString_ appendString:@"\n"];
            
        }
        
        [clueString_ appendString:carrierMessage];
        
    }

    frame = clueLabel_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [Tools labelHeight:clueLabel_
                                   forText:clueString_] + (2 * LONG_VERTICAL_DISTANCE);
    clueLabel_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + LONG_VERTICAL_DISTANCE;
    
    frame = firstViewSeparator_.frame;
    frame.origin.y = yPosition;
    firstViewSeparator_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    firstViewHeight_ = yPosition;
    
}

/**
 * Returns the view needed in the second view. NIL if it is not needed
 */
- (void)viewForSecondView {
    
    CGFloat yPosition = 0.0f;
    
    CGRect frame = secondViewHeader_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    secondViewHeader_.frame = frame;

    yPosition = CGRectGetMaxY(frame) + LONG_VERTICAL_DISTANCE;

    frame = accontCombo_.frame;
    frame.origin.y = yPosition;
    accontCombo_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + LONG_VERTICAL_DISTANCE;
    
    frame = secondViewSeparator_.frame;
    frame.origin.y = yPosition;
    secondViewSeparator_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
        
    secondViewHeight_ = yPosition;

}


/**
 * Returns the height for the first view. 0 if it is NIL
 */
- (CGFloat)heightForFirstView {

    return firstViewHeight_;

}

/**
 * Returns the height for the second view. 0 if it is NIL
 */
- (CGFloat)heightForSecondView {

    return secondViewHeight_;

}

#pragma mark -
#pragma mark  MOKStringListSelectionButtonDelegate

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton {
    
    BOOL invokeSuperImplementation = YES;

    if (accontCombo_ == stringListSelectionButton) {
        
        [processRecharge_ setSelectedAccountIndex:[stringListSelectionButton selectedIndex]];
        invokeSuperImplementation = NO;
        
    } else if (companyCombo_ == stringListSelectionButton) {
    
        if ([stringListSelectionButton selectedIndex] >= 0) { // should never get a -1 value
        
            [processRecharge_ setSelectedCarrierRechargeIndex:[stringListSelectionButton selectedIndex]];
        
        }

        if ([[processRecharge_ carrierRechargeList] count] > 0) {
            
            [processRecharge_ setCompany:[[processRecharge_ carrierRechargeList] objectAtIndex:[processRecharge_ selectedCarrierRechargeIndex]]];

        }

        if (lastIndexDownloaded_ != [processRecharge_ selectedCarrierRechargeIndex]) {
        
            lastIndexDownloaded_ = [processRecharge_ selectedCarrierRechargeIndex];
            [processRecharge_ startPaymentDataRequest];

        }
        
        invokeSuperImplementation = NO;
                
    }
    
    if (invokeSuperImplementation) {
        
        [super stringListSelectionButtonSelectedIndexChanged:stringListSelectionButton];
        
    }
    
}


/*
 * Updates the interface information
 */
- (void)updateInterfaceInfo {

    NSMutableArray *accountNames = [[[NSMutableArray alloc] init] autorelease];
    NSString *account = @"";
    
    NSArray *accountArray = [processRecharge_ accountsArray]; 
    
    for (BankAccount *bankAccount in accountArray) {
        
        account = [bankAccount accountIdAndDescription];
        [accountNames addObject:account];
        
    }
    
    [accontCombo_ setOptionStringList:accountNames];
    [accontCombo_ setNoSelectionText:NSLocalizedString(ACCOUND_CHARGED_TEXT_KEY, nil)];
    [accontCombo_ setSelectedIndex:[processRecharge_ selectedAccountIndex]];

    NSArray *auxCompanies = [processRecharge_ carrierRechargeList];
    [clueString_ setString:@""];
    
    NSMutableArray *carriersList = [[[NSMutableArray alloc] init] autorelease];
    
    NSMutableDictionary *carriersMessagesDictionary = [NSMutableDictionary dictionary];
    NSString *key = nil;
    NSString *carrierMessage = nil;
    NSString *carrierDisplayName = nil;

    for (Carrier *carrier in auxCompanies) {
        
        carrierDisplayName = [carrier carrierDisplayName];
        key = [[carrier carrier] lowercaseString];
        carrierMessage = [NSString stringWithFormat:@"%@: %@", carrierDisplayName, [Tools notNilString:[carrier message]]];
        
        if ((carrierDisplayName != nil) && (key != nil) && (carrierMessage != nil)) {
            
            [carriersList addObject:carrierDisplayName];
            
            [carriersMessagesDictionary setObject:carrierMessage
                                           forKey:key];
            
        }
        
    }
    
    carrierMessage = [carriersMessagesDictionary objectForKey:kCarrierMovistarCode];
    
    if (carrierMessage != nil) {
        
        [clueString_ appendString:carrierMessage];
        
    }
    
    carrierMessage = [carriersMessagesDictionary objectForKey:kCarrierClaroCode];
    
    if (carrierMessage != nil) {
        
        if ([clueString_ length] > 0) {
            
            [clueString_ appendString:@"\n"];
            
        }
        
        [clueString_ appendString:carrierMessage];
        
    }

    [Tools setText:clueString_
           toLabel:clueLabel_];
    
    [companyCombo_ setOptionStringList:carriersList];
    [companyCombo_ setSelectedIndex:[processRecharge_ selectedCarrierRechargeIndex]];
//    [companyCombo_ setNoSelectionText:[carriersList objectAtIndex:[processRecharge_ selectedCarrierRechargeIndex]]];
    
    [accontCombo_ setSelectedIndex:[processRecharge_ selectedAccountIndex]];
    
}

#pragma mark -
#pragma mark User interaction

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped {
    
    [self saveProcessInformation];
    
    [super continueButtonTapped];
    
}

/**
 * Save process information
 */
- (void)saveProcessInformation {
    
    [processRecharge_ setAmount:[amountTextField_ text]];
    [processRecharge_ setPhoneNumber:[phoneTextField_ text]];
    
    if ([processRecharge_ selectedCarrierRechargeIndex] > 0) {
        [processRecharge_ setCompany:[[processRecharge_ carrierRechargeList] objectAtIndex:[processRecharge_ selectedCarrierRechargeIndex]]];
    }
    
    if ([processRecharge_ selectedAccountIndex] >= 0) {
        
        [processRecharge_ setSelectedAccount:[[processRecharge_ accountsArray] objectAtIndex:[processRecharge_ selectedAccountIndex]]];
        
    }
    
}

#pragma mark -
#pragma mark PaymentBaseProcessDelegate

/**
 * Delegate is notified when the data analysis has finished 
 */
- (void)dataAnalysisHasFinished {
        
    [self updateInterfaceInfo];
    [self viewForFirstView];
    [self relocateViews];
    
    NSUInteger companiesCount = [[companyCombo_ optionStringList] count];
    
    if (([processRecharge_ selectedCarrierRechargeIndex] < 0) || ([processRecharge_ selectedCarrierRechargeIndex] >= companiesCount)) {
        
        if (companiesCount > 0) {
            
//            [processRecharge_ setSelectedCarrierRechargeIndex:0];
//            [companyCombo_ setSelectedIndex:[processRecharge_ selectedCarrierRechargeIndex]];
            [companyCombo_ setSelectedIndex:-1];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark UITextFieldDelegate

/**
 * Asks the delegate if the specified text should be changed.
 * 
 * @param textField: The text field containing the text.
 * @param range: The range of characters to be replaced
 * @param string: The replacement string.
 * @return YES if the specified text range should be replaced; otherwise, NO to keep the old text.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = YES;
    
    if (textField == phoneTextField_) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        NSCharacterSet *numbersSet = [NSCharacterSet decimalDigitCharacterSet];
        
        result = [Tools isValidText:string forCharacterSet:numbersSet];
        
        if (result) {
            
            NSInteger maxCharacters = [processRecharge_ longMaxSumi];
            
            if (newLength > maxCharacters) {
                result = NO;
            }
            
        }
        
    } else {
    
        result = [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
    
    }
	
    return result;	
    
}


#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    result.mainTitle = NSLocalizedString(PAYMENT_RECHARGES_TITLE_TEXT_KEY, nil);
    
    return result;
    
}

/*
 * Set the process
 */
- (void)setProcess:(PaymentBaseProcess *)process {
    
    [super setProcess:process];
    
    if (processRecharge_ != process) {

        [process retain];
        [processRecharge_ release];
        processRecharge_ = (PaymentRechargeProcess *)process;

    }

}


@end
