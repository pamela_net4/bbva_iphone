/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "PaymentDataBaseViewController.h"
#import "PaymentBaseProcess.h"
#import "MOKStringListSelectionButton.h"

@class MOKStringListSelectionButton;
@class PaymentRechargeProcess;
@class SimpleHeaderView;
@class MOKCurrencyTextField;
@class MOKTextField;

/**
 * View controller to show recharge payment.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface RechargeViewController : PaymentDataBaseViewController <MOKStringListSelectionButtonDelegate, PaymentBaseProcessDelegate> {			
@private
    
#pragma Graphic
    
    /**
     * First view header
     */
    SimpleHeaderView *firstViewHeader_;
    
    /**
     * First view separator
     */
    UIImageView *firstViewSeparator_;
    
    /**
     * Company label
     */
    UILabel *companyLabel_;
    
    /**
     * Company combo
     */
    MOKStringListSelectionButton *companyCombo_;

    /**
     * Phone label
     */
    UILabel *phoneLabel_;
    
    /**
     * Phone helper label.
     */
    UILabel *phoneHelperLabel_;
    
    /**
     * Phone text field.
     */
    MOKTextField *phoneTextField_;
    
    /**
     * Amount label
     */
    UILabel *amountLabel_;
    
    /**
     * Amount text field
     */
    MOKCurrencyTextField *amountTextField_;
    
    /**
     * Clue label
     */
    UILabel *clueLabel_;
    
    /**
     * Second view header
     */
    SimpleHeaderView *secondViewHeader_;
    
    /**
     * Second view separator
     */
    UIImageView *secondViewSeparator_;
    
    /**
     * Account combo
     */
    MOKStringListSelectionButton *accontCombo_;

    
#pragma Logic
    
    /**
     * First view height
     */
    CGFloat firstViewHeight_;
    
    /**
     * Second view height
     */
    CGFloat secondViewHeight_;
    
    /**
     * Clue string
     */
    NSMutableString *clueString_;
    
    /**
     * Last index downloaded flag
     */
    NSInteger lastIndexDownloaded_;
    
    /**
     * Process
     */
    PaymentRechargeProcess *processRecharge_;
    
}

/**
 * Provides read-write access to the companyLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *companyLabel;

/**
 * Provides read-write access to the companyCombo and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *companyCombo;

/**
 * Provides read-write access to the phoneLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *phoneLabel;

/**
 * Provides read-write access to the phone helper label and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *phoneHelperLabel;

/**
 * Provides read-write access to the phoneTextField and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKTextField *phoneTextField;

/**
 * Provides read-write access to the amountLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *amountLabel;

/**
 * Provides read-write access to the amountTextField and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKCurrencyTextField *amountTextField;

/**
 * Provides read-write access to the clueLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *clueLabel;

/**
 * Provides read-write access to the accontCombo and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *accontCombo;

/**
 * Creates and returns an autoreleased RechargeViewController constructed from a NIB file.
 *
 * @return The autoreleased RechargeViewController constructed from a NIB file.
 */
+ (RechargeViewController *)rechargeViewController;


@end
