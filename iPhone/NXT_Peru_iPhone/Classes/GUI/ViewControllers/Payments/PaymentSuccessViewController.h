/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKEditableViewController.h"
#import "PaymentBaseProcess.h"
#import "FrequentOperationAlterStepOneViewControllerViewController.h"
#import "FOInscriptionBaseProcess.h"

@class PaymentBaseProcess;
@class SimpleHeaderView;

/**
 * View controller to show public services payment step one.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentSuccessViewController : MOKEditableViewController <UITableViewDataSource, UITableViewDelegate,PaymentBaseProcessDelegate,FOInscriptionBaseProcessDelegate> {
    
@private
    
#pragma Graphic
    
    FrequentOperationAlterStepOneViewControllerViewController *frequentOperationReactiveStepOneViewController_;
    FOInscriptionBaseProcess *foInscriptionBaseProcess_;
    
    /**
     * Header view
     */
    SimpleHeaderView *headerView_;
    
    /**
     * Title view
     */
    UIView *titleView_;
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * The additional label.
     */
    UILabel *additionalLabel_;
    
    /**
     * Transfer tick image
     */
    UIImageView *transferTickImageView_;
    
    /**
     * Transfer tick background
     */
    UIImageView *transferTickBg_;
    
    /**
     * Table bg image view
     */
    UIImageView *tableBgImageView_;
    
    /**
     * Information table
     */
    UITableView *infoTable_;
    
    /**
     * Info label
     */
    UILabel *infoLabel_;
    
    /**
     * Payment button
     */
    UIButton *paymentButton_;
    
    /**
     * Global Position button
     */
    UIButton *globalPositionButton_;
    
    /**
     * Frequenty Operation button
     */
    UIButton *foButton_;
    
    /**
     * Bottom info View
     */
    UIView *bottomInfoView_;
    
    /**
     * Bottom info text view
     */
    UITextView *bottomInfoTextView_;
    
    /**
     * Bottom info bg image view
     */
    UIImageView *bottomInfoBgImageView_;
    
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    
    /**
     *  Flag to dont show the fo button
     */
    BOOL isFoFinnished_;
    
#pragma Logic

    /**
     * Process
     */
    PaymentBaseProcess *process_;
    
    /**
     * flag to indicate if the fo has been finished
     */
    BOOL isFoFinished_;
}

/**
 * flag to indicate if the fo has been finished
 */
@property (nonatomic, readwrite, assign) BOOL isFoFinished;

@property (nonatomic, readwrite, retain) FOInscriptionBaseProcess *foInscriptionBaseProcess;

@property (nonatomic, readwrite, assign) BOOL isFoFinnished;
/**
 * Provides readwrite access to the titleView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet FrequentOperationAlterStepOneViewControllerViewController *frequentOperationReactiveStepOneViewController;

/**
 * Provides readwrite access to the titleView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIView *titleView;

/**
 * Provides readwrite access to the titleLabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides read-write access to additionalMessage. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *additionalLabel;

/**
 * Provides readwrite access to the transferTickImageView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *transferTickImageView;

/**
 * Provides readwrite access to the transferTickBg and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *transferTickBg;

/**
 * Provides readwrite access to the tableBgImageView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *tableBgImageView;

/**
 * Provides readwrite access to the infoTable and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UITableView *infoTable;

/**
 * Provides readwrite access to the infoLabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *infoLabel;

/**
 * Provides readwrite access to the paymentButton and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIButton *paymentButton;

/**
 * Provides readwrite access to the foButton and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIButton *foButton;

/**
 * Provides readwrite access to the globalPositionButton and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIButton *globalPositionButton;

/**
 * Provides readwrite access to the bottomInfoView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIView *bottomInfoView;

/**
 * Provides readwrite access to the bottomInfoTextView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UITextView *bottomInfoTextView;

/**
 * Provides readwrite access to the bottomInfoBgImageView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *bottomInfoBgImageView;

/**
 * Provides readwrite access to the brandingLine and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *brandingLine;

/**
 * Provides readwrite access to the process and exports it to the IB
 */
@property (nonatomic,readwrite, retain) PaymentBaseProcess *process;

/**
 * Creates and returns an autoreleased PaymentSuccessViewController constructed from a NIB file.
 *
 * @return The autoreleased PaymentSuccessViewController constructed from a NIB file.
 */
+ (PaymentSuccessViewController *)paymentSuccessViewController;

/**
 * The payment button has been tapped
 */
- (IBAction)paymentButtonTapped;

/**
 * The global position button has been tapped
 */
- (IBAction)globalPositionButtonTapped;

/**
 * The fobutton has been tapped
 */
- (IBAction)foButtonTapped;

@end
