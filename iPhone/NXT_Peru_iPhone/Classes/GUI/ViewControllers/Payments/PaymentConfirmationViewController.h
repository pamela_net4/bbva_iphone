/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKEditableViewController.h"
#import "EmailMokCell.h"
#import "MOKStringListSelectionButton.h"
#import "PaymentBaseProcess.h"
#import "SMSMokCell.h"
#import <AddressBookUI/AddressBookUI.h>

@class FrequentOperationHeaderView;
@class MOKLimitedLengthTextField;
@class PaymentBaseProcess;
@class SimpleHeaderView;
@class TermsAndConsiderationsViewController;
@class PaymentSuccessViewController;

/**
 * View controller to show public services payment step one.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentConfirmationViewController : MOKEditableViewController <PaymentBaseProcessDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate> {			
    
@private
    
#pragma Graphic
     
    /**
     * Header view
     */
    SimpleHeaderView *headerView_;
    
    /**
     * Header with the main information of the operation
     */
    FrequentOperationHeaderView *informationHeader_;
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Table bg image view
     */
    UIImageView *tableBgImageView_;
    
    /**
     * Information table
     */
    UITableView *infoTable_;
    
    /**
     * OTP key view
     */
    UIView *otpKeyView_;
    
    /**
     * OTP key label
     */
    UILabel *otpKeyLabel_;
    
    /**
     * OTP key textfield
     */
    MOKLimitedLengthTextField *otpKeyTextField_;
    
    /**
     * Coord key View
     */
    UIView *coordKeyView_;
    
    /**
     * Coord title key label
     */
    UILabel *coordKeyTitleLabel_;
    
    /**
     * Coord key bg image view
     */
    UIImageView *coordKeyBgImageView_;
    
    /**
     * Coord subtitle key label
     */
    UILabel *coordKeySubtitleLabel_;
    
    /**
     * Coord key label
     */
    UILabel *coordKeyabel_;
    
    /**
     * Operation key textfield
     */
    MOKLimitedLengthTextField *coordKeyTextField_;
    
    /**
     * Seal View
     */
    UIView *sealView_;
    
    /**
     * Seal title label
     */
    UILabel *sealTitleLabel_;
    
    /**
     * Seal bg image view
     */
    UIImageView *sealBgImageView_;
    
    /**
     * Seal subtitle label
     */
    UILabel *sealSubtitleLabel_;
    
    /**
     * Seal image view
     */
    UIImageView *sealImageView_;
    
    /**
     * Continue button
     */
    UIButton *continueButton_;
    
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    
    /**
     * Extra info view
     */
    UIView *extraInfoView_;
    
    /**
     * Top separator
     */
    UIImageView *topSeparator_;
    
    /**
     * Bottom separator 
     */
    UIImageView *bottomSeparator_;
    
    /**
     * Extra info label
     */
    UILabel *extraInfoLabel_;
    
    /**
     * Legal terms view
     */
    UIView *legalTermsView_;
    
    /**
     * Legal terms title label
     */
    UILabel *legalTermsTitleLabel_;
    
    /**
     * Legal terms bg image view
     */
    UIImageView *legalTermsBgImageView_;
    
    /**
     * Legal terms detail label
     */
    UILabel *legalTermsDetailLabel_;
    
    /**
     * Warning image view 
     */
    UIImageView *warningImageView_;
    
    /**
     * Legal terms switch
     */
    UISwitch *legalTermsSwitch_;
    
    /**
     * Legal terms button
     */
    UIButton *legalTermsButton_;
    
    /**
     * Terms And Considerations View Controller
     */
    TermsAndConsiderationsViewController *termsAndConsiderationsViewController_;
    
    /**
     * Payment Success View Controller
     */
    PaymentSuccessViewController *paymentSuccessViewController_;
    
#pragma Logic

    /**
     * Process
     */
    PaymentBaseProcess *process_;
    
}

/**
 * Provides readwrite access to the continueButton and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides readwrite access to the tableBgImageView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *tableBgImageView;

/**
 * Provides readwrite access to the infoTable and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UITableView *infoTable;

/**
 * Provices readwrite access to the otp key view and exports it to he Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *otpKeyView;

/**
 * Provides readwrite access to the otpKeyLabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *otpKeyLabel;

/**
 * Provides readwrite access to the otpKeyTextField and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet MOKLimitedLengthTextField *otpKeyTextField;

/**
 * Provides readwrite access to the coordKeyView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIView *coordKeyView;

/**
 * Provides readwrite access to the coordKeyTitleLabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *coordKeyTitleLabel;

/**
 * Provides readwrite access to the coordKeyBgImageView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *coordKeyBgImageView;

/**
 * Provides readwrite access to the coordKeySubtitleLabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *coordKeySubtitleLabel;

/**
 * Provides readwrite access to the coordKeyabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *coordKeyabel;

/**
 * Provides readwrite access to the coordKeyTextField and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet MOKLimitedLengthTextField *coordKeyTextField;

/**
 * Provides readwrite access to the sealView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIView *sealView;

/**
 * Provides readwrite access to the sealTitleLabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *sealTitleLabel;

/**
 * Provides readwrite access to the sealBgImageView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *sealBgImageView;

/**
 * Provides readwrite access to the sealSubtitleLabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *sealSubtitleLabel;

/**
 * Provides readwrite access to the sealImageView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *sealImageView;

/**
 * Provides readwrite access to the continueButton and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIButton *continueButton;

/**
 * Provides readwrite access to the brandingLine and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *brandingLine;

/**
 * Provides readwrite access to the extraInfoView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIView *extraInfoView;

/**
 * Provides readwrite access to the topSeparator and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *topSeparator;

/**
 * Provides readwrite access to the bottomSeparator and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *bottomSeparator;

/**
 * Provides readwrite access to the extraInfoLabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *extraInfoLabel;

/**
 * Provides readwrite access to the legalTermsView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIView *legalTermsView;

/**
 * Provides readwrite access to the legalTermsTitleLabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *legalTermsTitleLabel;

/**
 * Provides readwrite access to the legalTermsBgImageView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *legalTermsBgImageView;

/**
 * Provides readwrite access to the legalTermsDetailLabel and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UILabel *legalTermsDetailLabel;

/**
 * Provides readwrite access to the warningImageView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *warningImageView;

/**
 * Provides readwrite access to the legalTermsSwitch and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UISwitch *legalTermsSwitch;

/**
 * Provides readwrite access to the legalTermsButton and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIButton *legalTermsButton;

/**
 * Provides readwrite access to the process and exports it to the IB
 */
@property (nonatomic,readwrite, retain) PaymentBaseProcess *process;

/**
 * Creates and returns an autoreleased PaymentConfirmationViewController constructed from a NIB file.
 *
 * @return The autoreleased PaymentConfirmationViewController constructed from a NIB file.
 */
+ (PaymentConfirmationViewController *)paymentConfirmationViewController;

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped;

/**
 * The legal terms button has been tapped
 */
- (IBAction)legalTermsButtonTapped;

@end
