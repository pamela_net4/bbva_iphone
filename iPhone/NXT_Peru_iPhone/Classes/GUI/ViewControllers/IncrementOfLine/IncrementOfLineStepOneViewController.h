//
//  IncrementOfLineStepOneViewController.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 2/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "NXTEditableViewController.h"
#import "NXTComboButton.h"
#import "IncrementCreditLineStartUpResponse.h"

@class SimpleHeaderView;
@class NXTComboButton;
@class NXTCurrencyTextField;
@class NXTTextField;

@interface IncrementOfLineStepOneViewController : NXTEditableViewController<NXTComboButtonDelegate, UITextFieldDelegate>{
    
@private
    
    /*
     * card label
     */
    UILabel *cardLabel_;
    
    /*
     * card combo
     */
    NXTComboButton *cardCombo_;
    
    UILabel *amountIndicationLabel_;
    
    UILabel *amountValueIndicationLabel_;
    
    NXTCurrencyTextField *amountTextField_;
    
    UILabel *amountLastIndicationLabel_;
    
    UILabel *currentCreditLineLabel_;
    
    /*
     * email label
     */
    UILabel *emailLabel_;
    
    /*
     * email combo
     */
    NXTComboButton *emailCombo_;
    
    NXTTextField * emailTextField_;
    
    UIImageView *brandingImageView_;
    
    UIButton *emailTipButton_;
    
    UIButton *transferButton_;
    
    UIButton *editAmountButton_;
    
    UIButton *editEmailButton_;
    
    UIView * separatorEmailTop_;
    
    UIView * separatorEmailBottom_;
    /**
     * Container view to contain the editable components
     */
    UIView *containerView_;
    
    IncrementCreditLineStartUpResponse *incrementCreditLineStartUpResponse_;
}

/**
 * Provides readwrite access to the cardLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *cardLabel;

/**
 * Provides readwrite access to the cardCombo button. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *cardCombo;

/**
 * Provides readwrite access to the amountIndicationLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *amountIndicationLabel;

/**
 * Provides readwrite access to the amountValueIndicationLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *amountValueIndicationLabel;

/**
 * Provides readwrite access to the amountLastIndicationLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *amountLastIndicationLabel;
/**
 * Provides readwrite access to the amountTextField. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTCurrencyTextField *amountTextField;
/**
 * Provides readwrite access to the currentCreditLineLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *currentCreditLineLabel;

/**
 * Provides readwrite access to the emailLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *emailLabel;

/**
 * Provides readwrite access to the emailCombo button. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *emailCombo;
/**
 * Provides readwrite access to the email tip button. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *emailTipButton;

/**
 * Provides readwrite access to the emailTextField. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTTextField * emailTextField;

/**
 * Provides readwrite access to the editAmountButton_. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIButton *editAmountButton;

/**
 * Provides readwrite access to the editEmailButton_. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIButton *editEmailButton;

/**
 * Provides readwrite access to the separatorEmailTop. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIView * separatorEmailTop;

/**
 * Provides readwrite access to the separatorEmailTop. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIView * separatorEmailBottom;

/**
 * Provides readwrite access to the transfer button. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *transferButton;
/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;
/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *containerView;

/**
 * Provides read-write access to the helper element
 */
@property (nonatomic, readwrite, retain) IncrementCreditLineStartUpResponse *incrementCreditLineStartUpResponse;


+ (IncrementOfLineStepOneViewController *)incrementOfLineStepOneViewController ;

@end
