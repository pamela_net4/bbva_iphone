//
//  IncrementOfLineStepTwoViewController.m
//  NXT_Peru_iPhone
//
//  Created by Estefany on 5/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "IncrementOfLineStepTwoViewController.h"
#import "Constants.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "NXTEditableViewController+protected.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "SealCell.h"
#import "TransferDetailCell.h"
#import "Tools.h"
#import "GlobalAdditionalInformation.h"
#import "SimpleHeaderView.h"
#import "UIColor+BBVA_Colors.h"
#import "Session.H"
#import "NXTNavigationItem.h"
#import "TitleAndAttributes.h"
#import "NXTTextField.h"
#import "Updater.h"
#import "IncrementCreditLineResultResponse.h"
#import "IncrementOfLineStepThreeViewController.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"IncrementOfLineStepTwoViewController"
/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_NEAR_ELEMENT                           5.0f

/**
 * Defines the vertical gap between two far away elements
 */
#define VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT                       10.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_BIG_SIZE                                          17.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              15.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        14.0f

@interface IncrementOfLineStepTwoViewController ()

@end

@implementation IncrementOfLineStepTwoViewController

@synthesize containerView = containerView_;
@synthesize acceptButton = acceptButton_;
@synthesize grettingLabel = grettingLabel_;
@synthesize brandingImageView = brandingImageView_;
@synthesize incrementCreditLineConfirmResponse = incrementCreditLineConfirmResponse_;
@synthesize cardNameLabel = cardNameLabel_;
@synthesize cardNameValueLabel = cardNameValueLabel_;
@synthesize cardNumberLabel = cardNumberLabel_;
@synthesize cardNumberValueLabel = cardNumberValueLabel_;
@synthesize emailLabel = emailLabel_;
@synthesize emailValueLabel = emailValueLabel_;
@synthesize separatorTop = separatorTop_;
@synthesize separatorEmail = separatorEmail_;
@synthesize optCordView = optCordView_;
@synthesize sealView = sealView_;
@synthesize optCordLabel = optCordLabel_;
@synthesize optCordTipButton = optCordTipButton_;
@synthesize optCordTextField = optCordTextField_;
@synthesize sealImageView = sealImageView_;
@synthesize sealLabel = sealLabel_;
@synthesize emailTipButton = emailTipButton_;
@synthesize SPOamountOnlyNumbers = SPOamountOnlyNumbers_;
@synthesize SPOemail = SPOemail_;
@synthesize SPOposition = SPOposition_;
@synthesize modifyButton = modifyButton_;
@synthesize optCordTipImageView = optCordTipImageView_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [SPOposition_ release];
    SPOposition_ = nil;
    
    [SPOemail_ release];
    SPOemail_ = nil;
    
    [SPOamountOnlyNumbers_ release];
    SPOamountOnlyNumbers_ = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationIncrementOfLineStepThreeResponse
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationIncrementOfLineStepTwoResponse
                                                  object:nil];
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransfersStepTwoViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/**
 * Releases graphic elements
 */
- (void)releaseTransfersStepTwoViewControllerGraphicElements {
    
    [containerView_ release];
    containerView_ = nil;
    
    [acceptButton_ release];
    acceptButton_ = nil;
    
    [cardNameLabel_ release];
    cardNameLabel_ = nil;
    
    [cardNameValueLabel_ release];
    cardNameValueLabel_ = nil;
    
    [cardNumberLabel_ release];
    cardNumberLabel_ = nil;
    
    [cardNumberValueLabel_ release];
    cardNumberValueLabel_ = nil;
    
    [emailLabel_ release];
    emailLabel_ = nil;
    
    [emailValueLabel_ release];
    emailValueLabel_ = nil;
    
    [separatorTop_ release];
    separatorTop_ = nil;
    
    [separatorEmail_ release];
    separatorEmail_ = nil;
    
    [optCordView_ release];
    optCordView_ = nil;
    
    [sealView_ release];
    sealView_ = nil;
    
    [optCordLabel_ release];
    optCordLabel_ = nil;
    
    [optCordTipButton_ release];
    optCordTipButton_ = nil;
    
    [optCordTextField_ release];
    optCordTextField_ = nil;
    
    [sealImageView_ release];
    sealImageView_ = nil;
    
    [sealLabel_ release];
    sealLabel_ = nil;
    
    [emailTipButton_ release];
    emailTipButton_ = nil;
    
    [modifyButton_ release];
    modifyButton_ = nil;
    
    [optCordTipImageView_ release];
    optCordTipImageView_ = nil;
    
    [editableViews_ removeAllObjects];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIView *view = self.view;
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    view.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
    containerView_.backgroundColor = [UIColor clearColor];
    
    /**/
    
    [NXT_Peru_iPhoneStyler styleLabel:grettingLabel_ withFontSize:TEXT_FONT_BIG_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:cardNameLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:cardNameValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:cardNumberLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:cardNumberValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:emailLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:emailValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:optCordLabel_ withFontSize:TEXT_FONT_BIG_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:sealLabel_ withFontSize:TEXT_FONT_BIG_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleTextField:optCordTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    
    cardNameLabel_.text = @"Tarjeta de Crédito";
    cardNumberLabel_.text = @"Nro Tarjeta";
    emailLabel_.text = @"Correo Electrónico";
    sealLabel_.text = @"Sello de Operaciones";
    
    [emailTipButton_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    [optCordTipButton_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    
    optCordTextField_.keyboardType = UIKeyboardTypeNumberPad;
    optCordTextField_.secureTextEntry = YES;
    optCordTextField_.delegate = self;
    
    
    [NXT_Peru_iPhoneStyler styleBlueButton:acceptButton_];
    [acceptButton_ setTitle:NSLocalizedString(CONFIRM_TEXT_KEY, nil) forState:UIControlStateNormal];
    [acceptButton_ addTarget:self action:@selector(acceptButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [NXT_Peru_iPhoneStyler styleWhiteButton:modifyButton_];
    [modifyButton_ setTitle:@"Modificar" forState:UIControlStateNormal];
    [modifyButton_ addTarget:self action:@selector(modifyButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    
    /**/
    
    CGRect containerFrame = containerView_.frame;
    containerFrame.origin.x = 0.0f;
    containerFrame.origin.y = 0.0f;
    containerView_.frame = containerFrame;
    
    [self setScrollableView:containerView_];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.origin.y = 0;
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
    [self layoutViews];
    
    [view bringSubviewToFront:brandingImageView_];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
    
    transparentScroll_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    
}



/*
 * Creates and returns an autoreleased IncrementOfLineStepTwoViewController constructed from a NIB file
 */
+ (IncrementOfLineStepTwoViewController *)incrementOfLineStepTwoViewController {
    
    IncrementOfLineStepTwoViewController *result = [[[IncrementOfLineStepTwoViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}


/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(incrementOfLineResponseReceived:)
                                                 name:kNotificationIncrementOfLineStepTwoResponse
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(confirmationResponseReceived:)
                                                 name:kNotificationIncrementOfLineStepThreeResponse
                                               object:nil];
    
    [self displayStoredInformation];
    
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];

}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the
 * transfer last step confirmation notification
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationIncrementOfLineStepThreeResponse
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationIncrementOfLineStepTwoResponse
                                                  object:nil];
    
  // [self storeInformationIntoHelper];
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingImageView] frame]);
    [self setScrollNominalFrame:scrollFrame];
    
    
    
}

/*
 * Lays out the views
 */
- (void)layoutViews {
    
    if ([self isViewLoaded]) {
        
        CGRect frame = grettingLabel_.frame;
        frame.origin.y = VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        grettingLabel_.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = separatorTop_.frame;
        frame.origin.y = auxPos;
        separatorTop_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = cardNameLabel_.frame;
        frame.origin.y = auxPos;
        cardNameLabel_.frame = frame;
        
        frame = cardNameValueLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:cardNameValueLabel_ forText:cardNameValueLabel_.text];
        cardNameValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = cardNumberLabel_.frame;
        frame.origin.y = auxPos;
        cardNumberLabel_.frame = frame;
        
        frame = cardNumberValueLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:cardNumberValueLabel_ forText:cardNumberValueLabel_.text];
        cardNumberValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = separatorEmail_.frame;
        frame.origin.y = auxPos;
        separatorEmail_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = emailLabel_.frame;
        frame.origin.y = auxPos;
        emailLabel_.frame = frame;
        
        frame = emailTipButton_.frame;
        frame.origin.y = auxPos - 8;
        emailTipButton_.frame = frame;
        
        frame = emailValueLabel_.frame;
        frame.size.height = [Tools labelHeight:emailValueLabel_ forText:emailValueLabel_.text];
        frame.origin.y = auxPos;
        emailValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = optCordView_.frame;
        frame.origin.y = auxPos;
        optCordView_.frame = frame;
        auxPos = CGRectGetMaxY(frame);
        
        if(incrementCreditLineConfirmResponse_.seal && [incrementCreditLineConfirmResponse_.seal length]>0){
        
            frame = sealView_.frame;
            frame.origin.y = auxPos;
            sealView_.frame = frame;
            auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
            
            sealView_.hidden = NO;
            
        }
        else{
            auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
            
            sealView_.hidden = YES;
        }
        
        
        frame = acceptButton_.frame;
        frame.origin.y = auxPos;
        acceptButton_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = modifyButton_.frame;
        frame.origin.y = auxPos;
        modifyButton_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
    
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.origin.y = 0;
        frame.size.height = auxPos;
        containerView.frame = frame;
    
        [self recalculateScroll];
    }
    
}

-(void)displayStoredInformation{
    
    grettingLabel_.text = [NSString stringWithFormat:@"%@, tu nueva línea sería de %@ %@",
                           incrementCreditLineConfirmResponse_.clientName,
                           incrementCreditLineConfirmResponse_.currency,
                           incrementCreditLineConfirmResponse_.amountIncrement];
    
    cardNameValueLabel_.text = incrementCreditLineConfirmResponse_.cardName;
    cardNumberValueLabel_.text = incrementCreditLineConfirmResponse_.cardNumber;
    emailValueLabel_.text = incrementCreditLineConfirmResponse_.email;
    
    OTP_Usage otpUsage  = [[[Session getInstance] additionalInformation] otpUsage];;
    NSString *title = @"";
    
    if (otpUsage == otp_UsageTC) {
        title = @"Tarjeta de Coordenadas";
        optCordTextField_.placeholder = [NSString stringWithFormat:@"Ingrese la coordenada %@ de tu tarjeta",
                                         incrementCreditLineConfirmResponse_.coordinate];
    } else if (otpUsage == otp_UsageOTP) {
        title = @"Clave SMS (enviada a tu celular)";
        optCordTextField_.placeholder = @"Ingrese los 6 dígitos de tu clave";
    }
    optCordLabel_.text = title;
    
    if(incrementCreditLineConfirmResponse_.seal && [incrementCreditLineConfirmResponse_.seal length]>0){
        
        NSData *data = [Tools base64DataFromString:incrementCreditLineConfirmResponse_.seal];
        UIImage *image = [[[UIImage alloc] initWithData:data] autorelease];
        [sealImageView_ setImage:image];
    }
    
    UIView *popButtonsView = [self popButtonsView];
    [optCordTextField_ setInputAccessoryView:popButtonsView];
    
    if (editableViews_ == nil) {
        editableViews_ = [[NSMutableArray alloc] init];
    } else {
        [editableViews_ removeAllObjects];
    }
    
    [editableViews_ addObject:optCordTextField_];
    
    [self layoutViews];

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initial configuration is set.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
}



#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
    [self makeViewVisible:textField];
    
    if(textField == optCordTextField_){
        [NXT_Peru_iPhoneStyler styleLabel:optCordLabel_
                                    withFontSize:TEXT_FONT_BIG_SIZE
                                    color:[UIColor BBVABlueSpectrumToneTwoColor]];
        [NXT_Peru_iPhoneStyler styleTextField:optCordTextField_
                                 withFontSize:TEXT_FONT_SMALL_SIZE
                                     andColor:[UIColor BBVAGreyColor]];
        [NXT_Peru_iPhoneStyler removeError:optCordTextField_];
    }
    
    return YES;
    
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self editableViewHasBeenClicked:textField];
    
}

/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    OTP_Usage otpUsage  = [[[Session getInstance] additionalInformation] otpUsage];
    if (otpUsage == otp_UsageTC) {
        
        if (resultLength <= COORDINATES_MAXIMUM_LENGHT) {
            
            result = YES;
            
        }
    }
    else if (otpUsage == otp_UsageOTP) {
        
        if(resultLength <= OTP_MAXIMUM_LENGHT) {
            
            if ([Tools isValidText:resultString forCharacterString:OTP_VALID_CHARACTERS]) {
                
                result = YES;
            }
        }
    }
    else {
        
        result = YES;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark UITextViewDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text view.
 *
 * @param textView The text view for which editing is about to begin.
 */
- (BOOL)textViewShouldBeginEditing:(UITextView*)textView {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
    }
    return YES;
}

/**
 * Tells the delegate that editing began for the specified text view.
 *
 * @param textView The text view for which an editing session began.
 */
- (void)textViewDidBeginEditing:(UITextView *)textView {

    [self editableViewHasBeenClicked:textView];
    
}



#pragma mark -
#pragma mark User Interaction

-(IBAction)modifyButtonTapped{
    [[self navigationController] popViewControllerAnimated:YES];
}

/*
 * Performs accept buttom action
 */
- (IBAction)acceptButtonTapped {
    
    [optCordTextField_ endEditing:YES];
    
    BOOL canStartProcess = YES;
    
    OTP_Usage otpUsage  = [[[Session getInstance] additionalInformation] otpUsage];
    
    if ((otpUsage == otp_UsageOTP) && ([[self secondFactorKey] length] == 0)) {
        
       /* [NXT_Peru_iPhoneStyler styleLabel:optCordLabel_ withFontSize:TEXT_FONT_SIZE
                                    color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
        [NXT_Peru_iPhoneStyler styleTextFieldError:optCordTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                          andColor:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
        [self showErrorTooltipFromView:optCordTextField_ message:@"Ingresa tu Clave SMS."];
        */
        [Tools showAlertWithMessage:@"Ingresa los 6 dígitos de tu Clave SMS." title:@""];
        canStartProcess = NO;
        
    } else if ((otpUsage == otp_UsageTC) && (([[self secondFactorKey] length] == 0) || ([[self secondFactorKey] length] < 3))) {
        
        /*[NXT_Peru_iPhoneStyler styleLabel:optCordLabel_ withFontSize:TEXT_FONT_SIZE
                                    color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
        [NXT_Peru_iPhoneStyler styleTextFieldError:optCordTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                          andColor:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
        [self showErrorTooltipFromView:optCordTextField_ message:@"Ingresa los 3 dígitos de la coordenada solicitada."];
        */
        [Tools showAlertWithMessage:@"Ingresa los 3 dígitos de la coordenada solicitada." title:@""];
        canStartProcess = NO;
        
    }
    
    if(canStartProcess){
        [self.appDelegate showActivityIndicator:poai_Both];
        [[Updater getInstance] incrementLineOperationSucess:[self secondFactorKey]];
    }
    
}

/*
 * Returns the second factor key
 */
- (NSString *)secondFactorKey {
    NSString *secondFactor = @"";
    secondFactor = optCordTextField_.text;
    return secondFactor;
    
}

- (IBAction)showTooltip:(UIView*)view {
    if(view == optCordTipButton_){
        OTP_Usage otpUsage  = [[[Session getInstance] additionalInformation] otpUsage];
        if (otpUsage == otp_UsageTC) {
            [self showInfoTooltipFromView:optCordTipImageView_
                                  message:@"Al ingresar tu coordenada aceptas el incremento y al presionar \"Confirmar\" podrás disponer de él."];
        }
        else if (otpUsage == otp_UsageOTP) {
            [self showInfoTooltipFromView:optCordTipImageView_
                                  message:@"Al ingresar tu Clave SMS aceptas el incremento y al presionar \"Confirmar\" podrás disponer de él."];
        }
    }
    else if(view == emailTipButton_){
        [self showInfoTooltipFromView:view
                              message:@"Para enviarte la constancia de aceptación de tu incremento de línea"];
    }
}

#pragma mark -
#pragma mark Notifications management

/*
 * Invoked by framework when the response to the transfer operation is received. The information is analized
 */
- (void)confirmationResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    NSString * physicalError = @"Se presentaron inconvenientes al solicitar tu Incremento de Línea. Por favor, inténtalo más tarde.";
    
    if (![response isError]) {
        
        if ([response isKindOfClass:[IncrementCreditLineResultResponse class]]) {
            
            //Sucess
            IncrementCreditLineResultResponse *incrementCreditLineResultResponse = (IncrementCreditLineResultResponse *)response;
            
             IncrementOfLineStepThreeViewController * incrementOfLineStepThreeViewController = [IncrementOfLineStepThreeViewController IncrementOfLineStepThreeViewController];
             incrementOfLineStepThreeViewController.IncrementCreditLineResultResponse = incrementCreditLineResultResponse;
             [[self navigationController] pushViewController:incrementOfLineStepThreeViewController animated:YES];
            
            
        }
        else{
            [Tools showAlertWithMessage:physicalError title:@"" andDelegate:self];
        }
        
    } else {
        
        
        if([response errorCode] != nil  &&
           (    [[response errorCode] isEqualToString:@WRONG_CONFIRM_CODE_ERROR]
            || [[response errorCode] isEqualToString:@REMAIN_ONE_TRY_CONFIRM_CODE_ERROR]
            || [[response errorCode] isEqualToString:@SMS_CODE_EXPIRED_ERROR]
            || [[response errorCode] isEqualToString:@WRONG_COORDINATE_CONFIRM_CODE_ERROR]
            )){
               
               NSString * message = [response errorMessage];
               if (message == nil || [@"" isEqualToString:message]) {
                   message = physicalError;// physical error
               }
               
               NSString *buttonText = NSLocalizedString(OK_TEXT_KEY, nil);
               UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"" message:message
                                                                   delegate:self cancelButtonTitle:buttonText otherButtonTitles:nil] autorelease];
               alertView.tag = 333;
               [alertView show];
           }
        else{
            NSString * message = [response errorMessage];
            if (message == nil || [@"" isEqualToString:message]) {
                message = physicalError;// physical error
            }
            [Tools showAlertWithMessage:message title:@"" andDelegate:self];
        }
        
        
    }
    
}

- (BOOL)incrementOfLineResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    optCordTextField_.text = @"";
    
    StatusEnabledResponse *response = [notification object];
    
    BOOL result = NO;
    
    if (response && !response.isError) {
        
        if ([response isKindOfClass:[IncrementCreditLineConfirmResponse class]]) {
            
            IncrementCreditLineConfirmResponse *incrementCreditLineConfirmResponse = (IncrementCreditLineConfirmResponse *)response;
            
            OTP_Usage otpUsage  = [[[Session getInstance] additionalInformation] otpUsage];;
            if (otpUsage == otp_UsageTC) {
                optCordTextField_.placeholder = [NSString stringWithFormat:@"Ingrese la coordenada %@ de tu tarjeta",
                                                 incrementCreditLineConfirmResponse.coordinate];
            }
            result = YES;
            
        }
    }
    else if(response && [response errorCode] != nil  &&
           [[response errorCode] isEqualToString:@MAX_TRY_CONFIRM_CODE_ERROR]){
        NSString * message =  response ? [response errorMessage] : nil;
        if (message == nil || [@"" isEqualToString:message]) {
            message = NSLocalizedString(COMMUNICATION_ERROR_KEY, nil);// physical error
        }
        [Tools showAlertWithMessage:message title:@"" andDelegate:self];
    }
    else{
        NSString * message =  response ? [response errorMessage] : nil;
        if (message == nil || [@"" isEqualToString:message]) {
            message = NSLocalizedString(COMMUNICATION_ERROR_KEY, nil);// physical error
        }
        [Tools showAlertWithMessage:message title:@"" andDelegate:self];
    }
    
    return result;
    
}

#pragma mark -
#pragma mark UIAlertViewDelegate methods

/**
 * Sent to the delegate when the user clicks a button on an alert view.
 *
 * @param alertView: The alert view containing the button.
 * @param buttonIndex: The index of the button that was clicked.
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 333){
        
        [self.appDelegate showActivityIndicator:poai_Both];
        [[Updater getInstance] incrementLineOperationConfirmationFromCodeCard:SPOposition_
                                                            amountToIncrement:SPOamountOnlyNumbers_
                                                                        email:SPOemail_];
    }
    else{
        [[self navigationController] popToRootViewControllerAnimated:YES];
    }
}


#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = @"Incremento de Línea";
    return result;
    
}


@end
