//
//  IncrementOfLineStepThreeViewController.m
//  NXT_Peru_iPhone
//
//  Created by Estefany on 5/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "IncrementOfLineStepThreeViewController.h"
#import "Constants.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "NXTEditableViewController+protected.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "SealCell.h"
#import "TransferDetailCell.h"
#import "Tools.h"
#import "GlobalAdditionalInformation.h"
#import "SimpleHeaderView.h"
#import "UIColor+BBVA_Colors.h"
#import "Session.H"
#import "NXTNavigationItem.h"
#import "TitleAndAttributes.h"
#import "NXTTextField.h"
#import "Updater.h"
#import "IncrementCreditLineResultResponse.h"


/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"IncrementOfLineStepThreeViewController"
/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_NEAR_ELEMENT                           5.0f

/**
 * Defines the vertical gap between two far away elements
 */
#define VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT                       10.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_BIG_SIZE                                          17.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              15.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                        13.0f

@interface IncrementOfLineStepThreeViewController ()

@end

@implementation IncrementOfLineStepThreeViewController

@synthesize containerView = containerView_;
@synthesize headerImageView = headerImageView_;
@synthesize acceptButton = acceptButton_;
@synthesize grettingLabel = grettingLabel_;
@synthesize brandingImageView = brandingImageView_;
@synthesize IncrementCreditLineResultResponse = IncrementCreditLineResultResponse_;
@synthesize dateOperationLabel = dateOperationLabel_;
@synthesize dateOperationValueLabel = dateOperationValueLabel_;
@synthesize cardNameLabel = cardNameLabel_;
@synthesize cardNameValueLabel = cardNameValueLabel_;
@synthesize cardNumberLabel = cardNumberLabel_;
@synthesize cardNumberValueLabel = cardNumberValueLabel_;
@synthesize oldCreditLineLabel = oldCreditLineLabel_;
@synthesize oldCreditLineValueLabel = oldCreditLineValueLabel_;
@synthesize lastCreditLineLabel = lastCreditLineLabel_;
@synthesize lastCreditLineValueLabel = lastCreditLineValueLabel_;
@synthesize separatorTop = separatorTop_;
@synthesize optCordView = optCordView_;
@synthesize optCordLabel = optCordLabel_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransfersStepTwoViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/**
 * Releases graphic elements
 */
- (void)releaseTransfersStepTwoViewControllerGraphicElements {
    
    [containerView_ release];
    containerView_ = nil;
    
    [headerImageView_ release];
    headerImageView_ = nil;
    
    [acceptButton_ release];
    acceptButton_ = nil;
    
    [dateOperationLabel_ release];
    dateOperationLabel_ = nil;
    
    [dateOperationValueLabel_ release];
    dateOperationValueLabel_ = nil;
    
    [cardNameLabel_ release];
    cardNameLabel_ = nil;
    
    [cardNameValueLabel_ release];
    cardNameValueLabel_ = nil;
    
    [cardNumberLabel_ release];
    cardNumberLabel_ = nil;
    
    [cardNumberValueLabel_ release];
    cardNumberValueLabel_ = nil;
    
    [oldCreditLineLabel_ release];
    oldCreditLineLabel_ = nil;
    
    [oldCreditLineValueLabel_ release];
    oldCreditLineValueLabel_ = nil;
    
    [lastCreditLineLabel_ release];
    lastCreditLineLabel_ = nil;
    
    [lastCreditLineValueLabel_ release];
    lastCreditLineValueLabel_ = nil;
    
    [separatorTop_ release];
    separatorTop_ = nil;
    
    [optCordView_ release];
    optCordView_ = nil;
    
    [optCordLabel_ release];
    optCordLabel_ = nil;
    
    [editableViews_ removeAllObjects];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIView *view = self.view;
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    view.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
    containerView_.backgroundColor = [UIColor clearColor];
    
    /**/
    
    [NXT_Peru_iPhoneStyler styleLabel:grettingLabel_ withFontSize:TEXT_FONT_BIG_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:dateOperationLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:dateOperationValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:cardNameLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:cardNameValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:cardNumberLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:cardNumberValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:oldCreditLineLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:oldCreditLineValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    [NXT_Peru_iPhoneStyler styleLabel:lastCreditLineLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyDarkColor]];
    [NXT_Peru_iPhoneStyler styleLabel:lastCreditLineValueLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyLightColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:optCordLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyLightColor]];
    
    dateOperationLabel_.text = @"Fecha y Hora de Operación";
    cardNameLabel_.text = @"Tarjeta de Crédito";
    cardNumberLabel_.text = @"Nro Tarjeta";
    oldCreditLineLabel_.text = @"Línea Anterior";
    lastCreditLineLabel_.text = @"Nueva Línea";
    
    /**/
    
    CGRect containerFrame = containerView_.frame;
    containerFrame.origin.x = 0.0f;
    containerFrame.origin.y = 0.0f;
    containerView_.frame = containerFrame;
    
    [self setScrollableView:containerView_];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.origin.y = 0;
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
    [self layoutViews];
    
    [view bringSubviewToFront:brandingImageView_];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:acceptButton_];
    [acceptButton_ setTitle:@"Volver a Mis Cuentas" forState:UIControlStateNormal];
    [acceptButton_ addTarget:self action:@selector(acceptButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
    
    transparentScroll_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
}



/*
 * Creates and returns an autoreleased IncrementOfLineStepThreeViewController constructed from a NIB file
 */
+ (IncrementOfLineStepThreeViewController *)IncrementOfLineStepThreeViewController {
    
    IncrementOfLineStepThreeViewController *result = [[[IncrementOfLineStepThreeViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}


/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self displayStoredInformation];
    
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the
 * transfer last step confirmation notification
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [Session getInstance].globalPositionRequestServerData = YES;
    [[self navigationController] popToRootViewControllerAnimated:NO];
    
  // [self storeInformationIntoHelper];
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingImageView] frame]);
    [self setScrollNominalFrame:scrollFrame];
}

/*
 * Lays out the views
 */
- (void)layoutViews {
    
    if ([self isViewLoaded]) {
        
        CGRect frame = headerImageView_.frame;
        frame.origin.y = VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        headerImageView_.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame);
        
        frame = grettingLabel_.frame;
        frame.origin.y = auxPos;
        grettingLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = separatorTop_.frame;
        frame.origin.y = auxPos;
        separatorTop_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = dateOperationLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:dateOperationLabel_ forText:dateOperationLabel_.text];
        dateOperationLabel_.frame = frame;
        
        frame = dateOperationValueLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:dateOperationValueLabel_ forText:dateOperationValueLabel_.text];
        dateOperationValueLabel_.frame = frame;
        auxPos = MAX(CGRectGetMaxY(dateOperationValueLabel_.frame ),CGRectGetMaxY(dateOperationLabel_.frame)) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = cardNameLabel_.frame;
        frame.origin.y = auxPos;
        cardNameLabel_.frame = frame;
        
        frame = cardNameValueLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:cardNameValueLabel_ forText:cardNameValueLabel_.text];
        cardNameValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = cardNumberLabel_.frame;
        frame.origin.y = auxPos;
        cardNumberLabel_.frame = frame;
        
        frame = cardNumberValueLabel_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:cardNumberValueLabel_ forText:cardNumberValueLabel_.text];
        cardNumberValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = oldCreditLineLabel_.frame;
        frame.origin.y = auxPos;
        oldCreditLineLabel_.frame = frame;
        
        frame = oldCreditLineValueLabel_.frame;
        frame.size.height = [Tools labelHeight:oldCreditLineValueLabel_ forText:oldCreditLineValueLabel_.text];
        frame.origin.y = auxPos;
        oldCreditLineValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = lastCreditLineLabel_.frame;
        frame.origin.y = auxPos;
        lastCreditLineLabel_.frame = frame;
        
        frame = lastCreditLineValueLabel_.frame;
        frame.size.height = [Tools labelHeight:lastCreditLineValueLabel_ forText:lastCreditLineValueLabel_.text];
        frame.origin.y = auxPos;
        lastCreditLineValueLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = optCordLabel_.frame;
        frame.origin.y = 0;
        frame.size.height = [Tools labelHeight:optCordLabel_ forText:optCordLabel_.text] + 16;
        optCordLabel_.frame = frame;
        
        frame = optCordView_.frame;
        frame.origin.y = auxPos;
        frame.size.height = [Tools labelHeight:optCordLabel_ forText:optCordLabel_.text] + 16;
        optCordView_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = acceptButton_.frame;
        frame.origin.y = auxPos;
        acceptButton_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
    
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.origin.y = 0;
        frame.size.height = auxPos;
        containerView.frame = frame;
    
        [self recalculateScroll];
    }
    
}

-(void)displayStoredInformation{
    
    grettingLabel_.text = [NSString stringWithFormat:@"¡%@, ya puedes usar tu nueva línea de crédito!",
                           IncrementCreditLineResultResponse_.clientName];
    
    dateOperationValueLabel_.text = [NSString stringWithFormat:@"%@ - %@",
                                     IncrementCreditLineResultResponse_.date,
                                     IncrementCreditLineResultResponse_.hour];
    cardNameValueLabel_.text = IncrementCreditLineResultResponse_.cardName;
    cardNumberValueLabel_.text = IncrementCreditLineResultResponse_.cardNumber;
    oldCreditLineValueLabel_.text = [NSString stringWithFormat:@"%@ %@",
                                     IncrementCreditLineResultResponse_.currency,
                                     IncrementCreditLineResultResponse_.amountCreditPrevious];
    lastCreditLineValueLabel_.text = [NSString stringWithFormat:@"%@ %@",
                                     IncrementCreditLineResultResponse_.currency,
                                     IncrementCreditLineResultResponse_.amountCredit];
    
    optCordLabel_.text = IncrementCreditLineResultResponse_.confirmationMessage;
    
    [self layoutViews];

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initial configuration is set.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
}





#pragma mark -
#pragma mark User Interaction

/*
 * Performs accept buttom action
 */
- (IBAction)acceptButtonTapped {
    
    [Session getInstance].globalPositionRequestServerData = YES;
    [[self navigationController] popToRootViewControllerAnimated:YES];
    
}



#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    result.hidesBackButton = YES;
    result.customTitleView.topLabelText = @"Incremento de Línea";
    return result;
    
}


@end
