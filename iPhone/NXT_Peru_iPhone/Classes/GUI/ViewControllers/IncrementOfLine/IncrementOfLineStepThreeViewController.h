//
//  IncrementOfLineStepThreeViewController.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 5/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "NXTEditableViewController.h"
#import "IncrementCreditLineResultResponse.h"
@class NXTTextField;

@interface IncrementOfLineStepThreeViewController : NXTEditableViewController<UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate>{
    
    @private
    
    /**
     * Container view
     */
    UIView *containerView_;
    
    UIImageView *headerImageView_;
    
    UILabel * dateOperationLabel_;
    
    UILabel * dateOperationValueLabel_;
    
    UILabel * cardNameLabel_;
    
    UILabel * cardNameValueLabel_;
    
    UILabel * cardNumberLabel_;
    
    UILabel * cardNumberValueLabel_;
    
    UILabel * oldCreditLineLabel_;
    
    UILabel * oldCreditLineValueLabel_;
    
    UILabel * lastCreditLineLabel_;
    
    UILabel * lastCreditLineValueLabel_;
    
    UIView * separatorTop_;
    
    /**
     * Accept button
     */
    UIButton *acceptButton_;
    
    /*
     * gretting label
     */
    UILabel *grettingLabel_;
    
    /*
     * branding ImageView
     */
    UIImageView *brandingImageView_;
    
    UIView * optCordView_;
    
    UILabel * optCordLabel_;
    
    IncrementCreditLineResultResponse *IncrementCreditLineResultResponse_;
    
    IncrementOfLineStepThreeViewController * IncrementOfLineStepThreeViewController_;
}

/**
 * Provides readwrite access to the containerView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *containerView;

/**
 * Provides readwrite access to the headerImageView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *headerImageView;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *grettingLabel;

/**
 * Provides readwrite access to the dateOperationLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dateOperationLabel;

/**
 * Provides readwrite access to the dateOperationValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * dateOperationValueLabel;

/**
 * Provides readwrite access to the cardNameLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * cardNameLabel;

/**
 * Provides readwrite access to the cardNameValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * cardNameValueLabel;

/**
 * Provides readwrite access to the cardNumberLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * cardNumberLabel;

/**
 * Provides readwrite access to the cardNumberValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * cardNumberValueLabel;

/**
 * Provides readwrite access to the emailLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * oldCreditLineLabel;

/**
 * Provides readwrite access to the emailValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * oldCreditLineValueLabel;

/**
 * Provides readwrite access to the lastCreditLineLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * lastCreditLineLabel;

/**
 * Provides readwrite access to the lastCreditLineValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * lastCreditLineValueLabel;

/**
 * Provides readwrite access to the separatorTop. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView * separatorTop;

/**
 * Provides readwrite access to the acceptButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *acceptButton;
/**
 * Provides readwrite access to the brandingImageView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;


@property (nonatomic, readwrite, retain) IBOutlet UIView * optCordView;

@property (nonatomic, readwrite, retain) IBOutlet UILabel * optCordLabel;

/**
 * Provides read-write access to the helper element
 */
@property (nonatomic, readwrite, retain) IncrementCreditLineResultResponse *IncrementCreditLineResultResponse;

/**
 * Creates and returns an autoreleased IncrementOfLineStepThreeViewController constructed from a NIB file
 *
 * @return The autoreleased IncrementOfLineStepThreeViewController constructed from a NIB file
 */
+ (IncrementOfLineStepThreeViewController *)IncrementOfLineStepThreeViewController;

@end
