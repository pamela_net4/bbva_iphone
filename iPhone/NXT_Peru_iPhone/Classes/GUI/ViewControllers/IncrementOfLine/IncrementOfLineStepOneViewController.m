//
//  IncrementOfLineStepOneViewController.m
//  NXT_Peru_iPhone
//
//  Created by Estefany on 2/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "IncrementOfLineStepOneViewController.h"
#import "SimpleHeaderView.h"
#import "NXTNavigationItem.h"
#import "ImagesCache.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "ImagesFileNames.h"
#import "NXTEditableViewController+protected.h"
#import "Tools.h"
#import "NXTCurrencyTextField.h"
#import "NXTComboButton.h"
#import "NXTTextField.h"
#import "UIColor+BBVA_Colors.h"
#import "CardForIncrement.h"
#import "CardForIncrementList.h"
#import "EmailInfo.h"
#import "EmailInfoList.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "IncrementCreditLineConfirmResponse.h"
#import "Updater.h"
#import "IncrementOfLineStepTwoViewController.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"IncrementOfLineStepOneViewController"
/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_VERY_NEAR_ELEMENT                      1.0f
/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_NEAR_ELEMENT                           5.0f

/**
 * Defines the vertical gap between two far away elements
 */
#define VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT                       10.0f
/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              16.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        14.0f


@interface IncrementOfLineStepOneViewController ()

@end

@implementation IncrementOfLineStepOneViewController

#pragma mark -
#pragma mark Properties

@synthesize cardCombo = cardCombo_;
@synthesize cardLabel = cardLabel_;
@synthesize emailCombo = emailCombo_;
@synthesize emailLabel = emailLabel_;
@synthesize amountIndicationLabel = amountIndicationLabel_;
@synthesize amountTextField = amountTextField_;
@synthesize emailTipButton = emailTipButton_;
@synthesize transferButton = transferButton_;
@synthesize brandingImageView = brandingImageView_;
@synthesize containerView = containerView_;
@synthesize incrementCreditLineStartUpResponse = incrementCreditLineStartUpResponse_;
@synthesize emailTextField = emailTextField_;
@synthesize editEmailButton = editEmailButton_;
@synthesize editAmountButton = editAmountButton_;
@synthesize currentCreditLineLabel = currentCreditLineLabel_;
@synthesize separatorEmailBottom = separatorEmailBottom_;
@synthesize separatorEmailTop = separatorEmailTop_;
@synthesize amountLastIndicationLabel = amountLastIndicationLabel_;
@synthesize amountValueIndicationLabel = amountValueIndicationLabel_;

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransferWithCashMobileViewControllerGraphicElements];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationIncrementOfLineStepTwoResponse
                                                  object:nil];
    
    [incrementCreditLineStartUpResponse_ release];
    incrementCreditLineStartUpResponse_ = nil;
    
    [editableViews_ release];
    editableViews_ = nil;
    
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransferWithCashMobileViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransferWithCashMobileViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseTransferWithCashMobileViewControllerGraphicElements {
    
    [cardCombo_ release];
    cardCombo_ = nil;
    
    [cardLabel_ release];
    cardLabel_ = nil;
    
    [emailCombo_ release];
    emailCombo_ = nil;
    
    [emailLabel_ release];
    emailLabel_ = nil;
    
    [amountIndicationLabel_ release];
    amountIndicationLabel_ = nil;
    
    [amountValueIndicationLabel_ release];
    amountValueIndicationLabel_ = nil;
    
    [amountTextField_ release];
    amountTextField_ = nil;
    
    [emailTipButton_ release];
    emailTipButton_ = nil;
    
    [transferButton_ release];
    transferButton_ = nil;
    
    [brandingImageView_ release];
    brandingImageView_ = nil;
    
    [emailTextField_ release];
    emailTextField_ = nil;
    
    [editEmailButton_ release];
    editEmailButton_ = nil;
    
    [editAmountButton_ release];
    editAmountButton_ = nil;
    
    [currentCreditLineLabel_ release];
    currentCreditLineLabel_ = nil;
    
    [separatorEmailBottom_ release];
    separatorEmailBottom_ = nil;
    
    [separatorEmailTop_ release];
    separatorEmailTop_ = nil;
    
    [amountLastIndicationLabel_ release];
    amountLastIndicationLabel_ = nil;
    
    [editableViews_ removeAllObjects];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self view] setFrame:[[UIScreen mainScreen] bounds]];
    
    UIView *view = self.view;
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    view.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
    containerView_.backgroundColor = [UIColor clearColor];
    
    
    /**/
    
    [NXT_Peru_iPhoneStyler styleLabel:cardLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [cardLabel_ setText:@"Tarjeta de Crédito"];
    
    [cardCombo_ setTitle:@"Selecciona Tarjeta de Crédito"];
    [cardCombo_ setIsNewDesign:YES];
    [cardCombo_ setDelegate:self];
    [cardCombo_ setInputAccessoryView:popButtonsView_];
    
    [NXT_Peru_iPhoneStyler styleLabel:currentCreditLineLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyColor]];
    [currentCreditLineLabel_ setText:@"(Línea Actual: )"];
    
    [NXT_Peru_iPhoneStyler styleLabel:amountIndicationLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [amountIndicationLabel_ setText:@"Nueva Línea"];
    
    [NXT_Peru_iPhoneStyler styleLabel:amountValueIndicationLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVABlueColor]];
    [amountValueIndicationLabel_ setText:@"(mín.  y máx.  )"];
    
    [NXT_Peru_iPhoneStyler styleTextField:amountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    [amountTextField_ setCanContainCents:NO];
    [amountTextField_ setCurrencyShowAlways:YES];
    [amountTextField_ setDelegate:self];
    [amountTextField_ setInputAccessoryView:popButtonsView_];
    
    [NXT_Peru_iPhoneStyler styleLabel:amountLastIndicationLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyColor]];
    [amountLastIndicationLabel_ setText:@"(Podrás utilizar tu nueva línea hoy mismo)"];
    
    
    [NXT_Peru_iPhoneStyler styleLabel:emailLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [emailLabel_ setText:@"Correo Electrónico"];
    
    [emailCombo_ setTitle:@"Selecciona Correo Electrónico"];
    [emailCombo_ setDelegate:self];
    [emailCombo_ setIsNewDesign:YES];
    [emailCombo_ setInputAccessoryView:popButtonsView_];
    
    [NXT_Peru_iPhoneStyler styleTextField:emailTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    emailTextField_.keyboardType = UIKeyboardTypeEmailAddress;
    emailTextField_.enabled = YES;
    [emailTextField_ setPlaceholder:@"Escribe tu correo"];
    [emailTextField_ setDelegate:self];
    [emailTextField_ setInputAccessoryView:popButtonsView_];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:transferButton_];
    [transferButton_ setTitle:@"Continuar" forState:UIControlStateNormal];
    [transferButton_ addTarget:self
                        action:@selector(continueButton:)
              forControlEvents:UIControlEventTouchUpInside];
    
    [emailTipButton_ addTarget:self action:@selector(showTooltip:) forControlEvents:UIControlEventTouchUpInside];
    [editEmailButton_ addTarget:self action:@selector(editTextField:) forControlEvents:UIControlEventTouchUpInside];
    [editAmountButton_ addTarget:self action:@selector(editTextField:) forControlEvents:UIControlEventTouchUpInside];
    
    /**/
    
    CGRect containerFrame = containerView_.frame;
    containerFrame.origin.x = 0.0f;
    containerFrame.origin.y = 0.0f;
    containerView_.frame = containerFrame;
    
    [self setScrollableView:containerView_];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.origin.y = 0;
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
    [self layoutViews];
    
    [view bringSubviewToFront:brandingImageView_];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
    
    transparentScroll_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
   
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(incrementOfLineResponseReceived:)
                                                 name:kNotificationIncrementOfLineStepTwoResponse
                                               object:nil];
    
    if (shouldClearInterfaceData_) {
        
      /*  cardCombo_.informFirstSelection = YES;
        [transferWithCashMobileHelper_ setSelectedOriginAccountIndex:-1];
        [transferWithCashMobileHelper_ setAmountString:@""];*/
        
        [transparentScroll_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f) animated:NO];
        
        shouldClearInterfaceData_ = NO;
        
    }
    
    if (editableViews_ == nil) {
        editableViews_ = [[NSMutableArray alloc] init];
    } else {
        [editableViews_ removeAllObjects];
    }
    [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:cardCombo_,
                                         amountTextField_,
                                         emailCombo_,
                                         nil]];
    
    
    
    [self displayStoredInformation];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the confirmation response and stores the information
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationIncrementOfLineStepTwoResponse
                                                  object:nil];
    
    [self okButtonClickedInPopButtonsView:popButtonsView_];
    
    [super viewWillDisappear:animated];
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
}

/**
 * Lays out the views
 */
- (void)layoutViews {
    
    if ([self isViewLoaded]) {
        
        CGRect frame = cardLabel_.frame;
        frame.origin.y = VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        CGFloat height = [Tools labelHeight:cardLabel_ forText:cardLabel_.text];
        frame.size.height = height;
        cardLabel_.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = cardCombo_.frame;
        frame.origin.y = auxPos;
        cardCombo_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_VERY_NEAR_ELEMENT;
        
        frame = currentCreditLineLabel_.frame;
        frame.origin.y = auxPos;
        currentCreditLineLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = amountValueIndicationLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:amountIndicationLabel_ forText:amountIndicationLabel_.text];
        frame.size.height = height;
        amountValueIndicationLabel_.frame = frame;
        
        frame = amountIndicationLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:amountIndicationLabel_ forText:amountIndicationLabel_.text];
        frame.size.height = height;
        amountIndicationLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        
        frame = amountTextField_.frame;
        frame.origin.y = auxPos;
        amountTextField_.frame = frame;
        
        frame = editAmountButton_.frame;
        frame.origin.y = auxPos - 5;
        editAmountButton_.frame = frame;
        
        auxPos = CGRectGetMaxY(amountTextField_.frame) + VERTICAL_DISTANCE_TO_VERY_NEAR_ELEMENT;;
        
        frame = amountLastIndicationLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:amountLastIndicationLabel_ forText:amountLastIndicationLabel_.text];
        frame.size.height = height;
        amountLastIndicationLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = separatorEmailTop_.frame;
        frame.origin.y = auxPos;
        separatorEmailTop_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        frame = emailLabel_.frame;
        frame.origin.y = auxPos;
        emailLabel_.frame = frame;
        
        frame = emailTipButton_.frame;
        frame.origin.y = auxPos - 8;
        emailTipButton_.frame = frame;
        
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        
        frame = emailCombo_.frame;
        frame.origin.y = auxPos;
        emailCombo_.frame = frame;
        
        frame = emailTextField_.frame;
        frame.origin.y = auxPos;
        emailTextField_.frame = frame;
        
        frame = editEmailButton_.frame;
        frame.origin.y = auxPos - 5;
        editEmailButton_.frame = frame;
        
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = separatorEmailBottom_.frame;
        frame.origin.y = auxPos;
        separatorEmailBottom_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UIButton *transferButton = self.transferButton;
        frame = transferButton.frame;
        frame.origin.y = auxPos;
        transferButton.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.size.height = auxPos;
        containerView.frame = frame;
        
        [self recalculateScroll];
        
    }
}

- (UINavigationItem *)navigationItem {
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = @"Incremento de Línea";
    return result;
}


#pragma mark -
#pragma mark IncrementOfLine selectors

/**
 * Displays the stored information. Sets the information into the views
 */
- (void)displayStoredInformation {
    
    if ([self isViewLoaded]) {
        
        //cards
        
        NSMutableArray *array = [NSMutableArray array];
        NSArray *cardList = [incrementCreditLineStartUpResponse_.cardForIncrementList cardforincrementList];
        
        for(CardForIncrement *cardForIncrement in cardList) {
            [array addObject:[NSString stringWithFormat:@"%@ (%@)",
              cardForIncrement.cardNumber,
             cardForIncrement.cardDescription]];
        }
        cardCombo_.stringsList = [NSArray arrayWithArray:array];
        
        if ([array count] > 0) {
            [cardCombo_ setSelectedIndex:0];
            
            CardForIncrement *cardForIncrement = [cardList objectAtIndex:0];
            [amountValueIndicationLabel_ setText:[NSString stringWithFormat:@"(mín. %@ %@ y máx %@ %@)",
                                cardForIncrement.currencySimbol,
                                [Tools formatAmount:[NSDecimalNumber decimalNumberWithString:cardForIncrement.minIncrement]
                                                    withDecimalCount:0 plusSign:false],
                                cardForIncrement.currencySimbol,
                                [Tools formatAmount:[NSDecimalNumber decimalNumberWithString:cardForIncrement.maxIncrement]
                                                withDecimalCount:0 plusSign:false]]];
            
            [currentCreditLineLabel_ setText:[NSString stringWithFormat:@"(Línea Actual: %@ %@)",
                                              cardForIncrement.currencySimbol,
                                              [Tools formatAmount:[NSDecimalNumber decimalNumberWithString:cardForIncrement.creditLine]
                                                 withDecimalCount:0 plusSign:false]]];
            [amountTextField_ setCurrencySymbol:cardForIncrement.currencySimbol];
            [amountTextField_ setText:[Tools formatAmount:[NSDecimalNumber
                                                           decimalNumberWithString:cardForIncrement.maxIncrement]
                                                            withDecimalCount:0 plusSign:false]];
        }
        
        if([array count] == 1){
            [cardCombo_ setEnabled:NO];
        }
        else{
            [cardCombo_ setEnabled:YES];
        }
        
        [array removeAllObjects];
        
        
        //emails
        
        array = [NSMutableArray array];
        NSArray *emailList = [incrementCreditLineStartUpResponse_.emailInfoList emailinfoList];
        
        for(EmailInfo *email in emailList) {
            [array addObject:email.email];
        }
        emailCombo_.stringsList = [NSArray arrayWithArray:array];
        
        if ([array count] > 0) {
            [emailCombo_ setSelectedIndex:0];
            [emailCombo_ setHidden:NO];
            [emailTextField_ setHidden:YES];
            
            
            if (editableViews_ == nil) {
                editableViews_ = [[NSMutableArray alloc] init];
            } else {
                [editableViews_ removeAllObjects];
            }
            [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:cardCombo_,
                                                 amountTextField_,
                                                 emailCombo_,
                                                 nil]];
        }
        else{
            [emailCombo_ setHidden:YES];
            [emailTextField_ setHidden:NO];
            
            
            if (editableViews_ == nil) {
                editableViews_ = [[NSMutableArray alloc] init];
            } else {
                [editableViews_ removeAllObjects];
            }
            [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:cardCombo_,
                                                 amountTextField_,
                                                 emailTextField_,
                                                 nil]];
        }
        
        if([array count] == 1){
            [emailCombo_ setEnabled:NO];
        }
        else{
            [emailCombo_ setEnabled:YES];
        }
        
        [array removeAllObjects];
        
        
        
        
        /*[originCombo_ setSelectedIndex:transferWithCashMobileHelper_.selectedOriginAccountIndex];
        [phoneDestinationView_.phoneNumberTextField setText:transferWithCashMobileHelper_.selectedDestinationPhoneNumber];
        [amountTextField_ setText:transferWithCashMobileHelper_.amountString];*/
        
    }
}

/**
 * Stores the information into the transfer operation helper instance
 */
- (void)storeInformationIntoHelper {
    
    //transferWithCashMobileHelper_.selectedDestinationPhoneNumber = phoneDestinationView_.phoneNumberTextField.text;
    //transferWithCashMobileHelper_.amountString = amountTextField_.text;
    
}

#pragma mark -
#pragma mark User interaction

- (IBAction)comboPressed:(UIView*)view {
    
    [self editableViewHasBeenClicked:view];
}

- (IBAction)editTextField:(id)view {
    
    if(view == editAmountButton_){
        [amountTextField_ setText:@""];
        [amountTextField_ becomeFirstResponder];
    }
    else if(view == editEmailButton_){
        [emailCombo_ setHidden:YES];
        [emailTextField_ setHidden:NO];
        
        if (editableViews_ == nil) {
            editableViews_ = [[NSMutableArray alloc] init];
        } else {
            [editableViews_ removeAllObjects];
        }
        [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:cardCombo_,
                                             amountTextField_,
                                             emailTextField_,
                                             nil]];
    }
    
   
}

- (IBAction)showTooltip:(UIView*)view {
    [self showInfoTooltipFromView:view message:@"Para enviarte la constancia de aceptación de tu incremento de línea"];
}

- (IBAction)continueButton:(UIView*)view {
    
    BOOL isValid = YES;
    
   [self storeInformationIntoHelper];
    
    [amountTextField_ resignFirstResponder];
    [emailTextField_ resignFirstResponder];
    
    int selectedCardIndex = (int)cardCombo_.selectedIndex;
    CardForIncrement * card = nil;
    
    NSString * amountString = amountTextField_.text;
    NSString *amountOnlyNumbers = [amountString stringByReplacingOccurrencesOfString:@"." withString:@""];
    amountOnlyNumbers = [amountOnlyNumbers stringByReplacingOccurrencesOfString:@"," withString:@""];
    long int amount = [amountOnlyNumbers intValue];
    
    NSString * email = emailCombo_.isHidden ? emailTextField_.text : @"";
    NSArray *emailList = [incrementCreditLineStartUpResponse_.emailInfoList emailinfoList];
    if(!emailCombo_.isHidden && [emailList count]>0){
        EmailInfo * emailInfo = [ emailList objectAtIndex:emailCombo_.selectedIndex];
        email = emailInfo.email;
    }
    
    if (selectedCardIndex == NSNotFound || selectedCardIndex < 0 ||
        selectedCardIndex >=
        [[[[self incrementCreditLineStartUpResponse] cardForIncrementList] cardforincrementList] count]) {
        
        [Tools showAlertWithMessage:@"Selecciona Tarjeta de Crédito" title:nil];
        isValid = NO;
        
    }
    else{
        
        card = [[[[self incrementCreditLineStartUpResponse] cardForIncrementList] cardforincrementList] objectAtIndex:selectedCardIndex];
        
        if(![Tools isValidEmail:email]){
            
            [self showErrorTooltipFromView:emailTextField_ message:@"Ingresa un correo válido"];
            
            [NXT_Peru_iPhoneStyler styleLabel:emailLabel_ withFontSize:TEXT_FONT_SIZE
                                        color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            [NXT_Peru_iPhoneStyler styleTextFieldError:emailTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                         andColor:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            isValid = NO;
        }
        if (!([amountOnlyNumbers length] > 0) || ([amountString isEqualToString:@"0.00"]) ||
            ([amountString floatValue] == 0.0f)) {
            
            NSString * message = [NSString stringWithFormat:@"Ingresa un monto"];
            
            [NXT_Peru_iPhoneStyler styleLabel:amountIndicationLabel_ withFontSize:TEXT_FONT_SIZE
                                color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            [NXT_Peru_iPhoneStyler styleLabel:amountValueIndicationLabel_ withFontSize:TEXT_FONT_SIZE
                                        color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            [NXT_Peru_iPhoneStyler styleTextFieldError:amountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                              andColor:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            [self showErrorTooltipFromView:amountTextField_ message:message];
            isValid = NO;
            
        }
        else if (amount > [[card maxIncrement] floatValue] ||
                 amount < [[card minIncrement] floatValue]) {
            
            NSString * message = [NSString stringWithFormat:@"Ingresa un monto entre %@ %@ y %@ %@",
                                  card.currencySimbol,
                                  [Tools formatAmount:[NSDecimalNumber decimalNumberWithString:card.minIncrement] withDecimalCount:0 plusSign:false],
                                  card.currencySimbol,
                                  [Tools formatAmount:[NSDecimalNumber decimalNumberWithString:card.maxIncrement] withDecimalCount:0 plusSign:false]
                                  ];
            
            [NXT_Peru_iPhoneStyler styleLabel:amountIndicationLabel_ withFontSize:TEXT_FONT_SIZE
                                        color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            [NXT_Peru_iPhoneStyler styleLabel:amountValueIndicationLabel_ withFontSize:TEXT_FONT_SIZE
                                        color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            [NXT_Peru_iPhoneStyler styleTextFieldError:amountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                              andColor:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
            [self showErrorTooltipFromView:amountTextField_ message:message];
            isValid = NO;
            
        }
    }
    
    if(isValid){
            
        [self.appDelegate showActivityIndicator:poai_Both];
        [[Updater getInstance] incrementLineOperationConfirmationFromCodeCard:card.position
                                                            amountToIncrement:amountOnlyNumbers
                                                                        email:email];
    }
}


#pragma mark -
#pragma mark NXTComboButtonDelegate selectors

/*
 * Informs the delegate that a value has been selected
 */
- (void)NXTComboButtonValueSelected:(NXTComboButton *)comboButton {
    
    if (comboButton == cardCombo_) {
        
        NSArray *cardList = [incrementCreditLineStartUpResponse_.cardForIncrementList cardforincrementList];
        CardForIncrement *cardForIncrement = [cardList objectAtIndex:cardCombo_.selectedIndex];
        
        [amountValueIndicationLabel_ setText:[NSString stringWithFormat:@"(mín. %@ %@ y máx %@ %@)",
                                         cardForIncrement.currencySimbol,
                                         [Tools formatAmount:[NSDecimalNumber decimalNumberWithString:cardForIncrement.minIncrement]
                                            withDecimalCount:0 plusSign:false],
                                         cardForIncrement.currencySimbol,
                                         [Tools formatAmount:[NSDecimalNumber decimalNumberWithString:cardForIncrement.maxIncrement]
                                            withDecimalCount:0 plusSign:false]]];
        
        [currentCreditLineLabel_ setText:[NSString stringWithFormat:@"(Línea Actual %@ %@)",
                                          cardForIncrement.currencySimbol,
                                          [Tools formatAmount:[NSDecimalNumber decimalNumberWithString:cardForIncrement.creditLine]
                                             withDecimalCount:0 plusSign:false]]];
        [amountTextField_ setCurrencySymbol:cardForIncrement.currencySimbol];
        [amountTextField_ setText:[Tools formatAmount:[NSDecimalNumber
                                                       decimalNumberWithString:cardForIncrement.maxIncrement]
                                     withDecimalCount:0 plusSign:false]];
        
       // [transferWithCashMobileHelper_ setSelectedOriginAccountIndex:comboButton.selectedIndex];
    }
    else if (comboButton == emailCombo_) {
        
        // [transferWithCashMobileHelper_ setSelectedOriginAccountIndex:comboButton.selectedIndex];
    }
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        if (popButtonsView_.superview == nil) {
            popButtonsState_ = pbse_Show;
        } else {
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
    if(textField == amountTextField_){
        [NXT_Peru_iPhoneStyler styleLabel:amountIndicationLabel_ withFontSize:TEXT_FONT_SIZE
                                    color:[UIColor BBVABlueColor]];
        [NXT_Peru_iPhoneStyler styleLabel:amountValueIndicationLabel_ withFontSize:TEXT_FONT_SMALL_SIZE
                                    color:[UIColor BBVABlueColor]];
        [NXT_Peru_iPhoneStyler styleTextField:amountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                          andColor:[UIColor BBVAGreyColor]];
        [NXT_Peru_iPhoneStyler removeError:amountTextField_];
    }
    else if(textField == emailTextField_){
        [NXT_Peru_iPhoneStyler styleLabel:emailLabel_ withFontSize:TEXT_FONT_SIZE
                                    color:[UIColor BBVABlueColor]];
        [NXT_Peru_iPhoneStyler styleTextField:emailTextField_ withFontSize:TEXT_FONT_SMALL_SIZE
                                     andColor:[UIColor BBVAGreyColor]];
        [NXT_Peru_iPhoneStyler removeError:emailTextField_];
    }
    
    return YES;
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self editableViewHasBeenClicked:textField];
}

/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    NSUInteger finalTotalChars = [textField.text length] + [string length] - range.length;

    if(textField == amountTextField_) {
        
        if (resultLength == 0) {
            result = YES;
        } else {
            NSScanner *scanner = [NSScanner scannerWithString:resultString];
            NSInteger resultInteger = 0;
            
            if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                if (resultInteger > 0) {
                    result = YES;
                }
            }
        }
        
    }
    else if(textField == emailTextField_){
        
        result = (finalTotalChars <= 50 && [Tools isValidText:string forCharacterString:EMAIL_TEXT_VALID_CHARACTERS]);
        return result;
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased constructed from a NIB file
 */
+ (IncrementOfLineStepOneViewController *)incrementOfLineStepOneViewController {
    
    return [[[IncrementOfLineStepOneViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
}


/*
 * Notifies the transfer operation helper the tranfer response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)incrementOfLineResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    BOOL result = NO;
    
    if (!response.isError) {
        
        if ([response isKindOfClass:[IncrementCreditLineConfirmResponse class]]) {
            
            
            //get values selection
            NSString * amountString = amountTextField_.text;
            NSString *amountOnlyNumbers = [amountString stringByReplacingOccurrencesOfString:@"." withString:@""];
            amountOnlyNumbers = [amountOnlyNumbers stringByReplacingOccurrencesOfString:@"," withString:@""];
            
            NSString * email = emailCombo_.isHidden ? emailTextField_.text : @"";
            NSArray *emailList = [incrementCreditLineStartUpResponse_.emailInfoList emailinfoList];
            if(!emailCombo_.isHidden && [emailList count]>0){
                EmailInfo * emailInfo = [ emailList objectAtIndex:emailCombo_.selectedIndex];
                email = emailInfo.email;
            }
    
            CardForIncrement * card = [[[[self incrementCreditLineStartUpResponse] cardForIncrementList] cardforincrementList] objectAtIndex:(int)cardCombo_.selectedIndex];
            
            
            //call step two
            
            IncrementCreditLineConfirmResponse *incrementCreditLineConfirmResponse = (IncrementCreditLineConfirmResponse *)response;
        
            IncrementOfLineStepTwoViewController * incrementOfLineStepTwoViewController = [IncrementOfLineStepTwoViewController incrementOfLineStepTwoViewController];
            incrementOfLineStepTwoViewController.incrementCreditLineConfirmResponse = incrementCreditLineConfirmResponse;
            incrementOfLineStepTwoViewController.SPOposition = card.position;
            incrementOfLineStepTwoViewController.SPOemail = email;
            incrementOfLineStepTwoViewController.SPOamountOnlyNumbers = amountOnlyNumbers;
            [[self navigationController] pushViewController:incrementOfLineStepTwoViewController animated:YES];
            
            result = YES;
        
        }
    }
    
    return result;
 
}


@end
