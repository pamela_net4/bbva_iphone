//
//  IncrementOfLineStepTwoViewController.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 5/02/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "NXTEditableViewController.h"
#import "IncrementCreditLineConfirmResponse.h"
@class NXTTextField;

@interface IncrementOfLineStepTwoViewController : NXTEditableViewController<UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate>{
    
    @private
    
    /**
     * Container view
     */
    UIView *containerView_;
    
    UILabel * cardNameLabel_;
    
    UILabel * cardNameValueLabel_;
    
    UILabel * cardNumberLabel_;
    
    UILabel * cardNumberValueLabel_;
    
    UILabel * emailLabel_;
    
    UILabel * emailValueLabel_;
    
    UIView * separatorTop_;
    
    UIView * separatorEmail_;
    
    UIButton * emailTipButton_;
    
    /**
     * Accept button
     */
    UIButton *acceptButton_;
    
    UIButton *modifyButton_;
    
    /*
     * gretting label
     */
    UILabel *grettingLabel_;
    
    /*
     * branding ImageView
     */
    UIImageView *brandingImageView_;
    
    UIView * optCordView_;
    
    UIView * sealView_;
    
    UILabel * optCordLabel_;
    
    UIButton * optCordTipButton_;

    UIImageView *optCordTipImageView_;
    
    NXTTextField * optCordTextField_;
    
    UIImageView * sealImageView_;
    
     UILabel *sealLabel_;
    
    IncrementCreditLineConfirmResponse *incrementCreditLineConfirmResponse_;
    
    NSString * SPOposition_;
    
    NSString * SPOamountOnlyNumbers_;
    
    NSString * SPOemail_;
    
    IncrementOfLineStepTwoViewController * incrementOfLineStepTwoViewController_;
}

/**
 * Provides readwrite access to the containerView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *containerView;

/**
 * Provides readwrite access to the grettingLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *grettingLabel;

/**
 * Provides readwrite access to the cardNameLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * cardNameLabel;

/**
 * Provides readwrite access to the cardNameValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * cardNameValueLabel;

/**
 * Provides readwrite access to the cardNumberLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * cardNumberLabel;

/**
 * Provides readwrite access to the cardNumberValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * cardNumberValueLabel;

/**
 * Provides readwrite access to the emailLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * emailLabel;

/**
 * Provides readwrite access to the emailValueLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel * emailValueLabel;

/**
 * Provides readwrite access to the separatorTop. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView * separatorTop;

/**
 * Provides readwrite access to the separatorEmail. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView * separatorEmail;

/**
 * Provides readwrite access to the acceptButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *acceptButton;

/**
 * Provides readwrite access to the acceptButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *modifyButton;
/**
 * Provides readwrite access to the brandingImageView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView *optCordTipImageView;

@property (nonatomic, readwrite, retain) IBOutlet UIView * optCordView;

@property (nonatomic, readwrite, retain) IBOutlet UIView * sealView;

@property (nonatomic, readwrite, retain) IBOutlet UILabel * optCordLabel;

@property (nonatomic, readwrite, retain) IBOutlet UIButton * optCordTipButton;

@property (nonatomic, readwrite, retain) IBOutlet NXTTextField * optCordTextField;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView * sealImageView;

@property (nonatomic, readwrite, retain) IBOutlet UIButton * emailTipButton;

@property (nonatomic, readwrite, retain) IBOutlet UILabel * sealLabel;

/**
 * Provides read-write access to the helper element
 */
@property (nonatomic, readwrite, retain) IncrementCreditLineConfirmResponse *incrementCreditLineConfirmResponse;

@property (nonatomic, readwrite, retain) NSString * SPOposition;

@property (nonatomic, readwrite, retain) NSString * SPOamountOnlyNumbers;

@property (nonatomic, readwrite, retain) NSString * SPOemail;

/**
 * Creates and returns an autoreleased IncrementOfLineStepTwoViewController constructed from a NIB file
 *
 * @return The autoreleased IncrementOfLineStepTwoViewController constructed from a NIB file
 */
+ (IncrementOfLineStepTwoViewController *)incrementOfLineStepTwoViewController;

@end
