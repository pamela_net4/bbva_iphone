/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>
#import "NXTViewController.h"
#import "GlobalPositionSummaryView.h"

@class AccountTransactionsListViewController;
@class CCIViewController;
@class CardTransactionsListViewController;
@class CampaignList;
/**
 * Global position view. It only has a table view 
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */



@interface GlobalPositionViewController : NXTViewController <UITableViewDelegate, UITableViewDataSource, BannerSelectionDelegate> {
    
    /**
     * Table for amounts
     */
    UITableView *table_;
    
    /**
     * CCIViewController
     */
    CCIViewController *cciViewController_;
    
    /**
     * AccountTransactionsListViewController
     */
    AccountTransactionsListViewController *accountTransactionsListViewController_;
    
    /**
     * CardTransactionsListViewController
     */
    CardTransactionsListViewController *cardTransactionsListViewController_;
    
    /**
     * Accounts array
     */
    NSMutableArray *accountsArray_;
    
    /**
     * Deposits Dictionary
     */
    NSMutableDictionary *depositsDictionary_;
    
    /**
     * Step accounts array
     */
    NSMutableArray *stepAccountsArray_;
    
    /**
     * FFMM array
     */
    NSMutableArray *ffmmArray_;
    
    /**
     * Continental stock array
     */
    NSMutableArray *continentalStockArray_;
    
    /**
     * Cards Array
     */
    NSMutableArray *cardsArray_;
    
    /**
     * Loans Array
     */
    NSMutableArray *loansArray_;
    
    /**
     * Array to fill the table
     */
    NSMutableArray *array_;
    
    /**
     * Dictionary to fill the table
     */
    NSMutableDictionary *amounts_;
    
    /**
     * CTS indicator 1 for (*)
     */
    BOOL ctsIndicator1_;
    
    /**
     * CTS indicator 1 for (**)
     */
    BOOL ctsIndicator2_;
    
    /**
     * Is step account flag
     */
    BOOL isStepAccount_;
	
	/**
	 * To be should view.
	 */
	BOOL shouldBeCleaned_;
    
    
    NSArray * campaignList_;

    
}

/**
 * Provides readwrite access to the table. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *table;

/**
 * Provides read-write access to shouldBeCleaned.
 */
@property (nonatomic, readwrite, assign) BOOL shouldBeCleaned;

@property (nonatomic, readwrite, retain) NSArray * campaignList;

/**
 * Creates a new autoreleased instance from the associated NIB file
 */
+ (GlobalPositionViewController *)globalPositionViewController;

-(IBAction)bannerButtonClose;


@end
