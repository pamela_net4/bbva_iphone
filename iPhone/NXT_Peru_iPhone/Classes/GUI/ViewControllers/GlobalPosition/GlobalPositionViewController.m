/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "GlobalPositionViewController.h"

#import "AccountList.h"
#import "AccountTransactionsListViewController.h"
#import "BankAccount.h"
#import "Card.h"
#import "CardList.h"
#import "CardTransactionsListViewController.h"
#import "CCIViewController.h"
#import "Constants.h"
#import "Deposit.h"
#import "DepositList.h"
#import "GlobalAdditionalInformation.h"
#import "GlobalPositionCell.h"
#import "GlobalPositionSummaryView.h"
#import "Loan.h"
#import "LoanList.h"
#import "MutualFund.h"
#import "MutualFundList.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTNavigationItem.h"
#import "ProductFooterView.h"
#import "ProductHeaderView.h"
#import "Session.h"
#import "StockMarketAccount.h"
#import "StockMarketAccountList.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "Updater.h"
#import "CampaignList.h"
#import "IncrementCreditLineStartUpResponse.h"
#import "IncrementOfLineStepOneViewController.h"
#import "FastLoanStartup.h"
#import "FastLoanStepOneViewController.h"
#import "SalaryAdvanceReceive.h"
#import "SalaryAdvanceAffiliation.h"
#import "SalaryAdvanceAffiliationViewController.h"
#import "SalaryAdvanceReceiveViewController.h"

/**
 * Defines the nib name
 */
#define NIB_NAME                                                        @"GlobalPositionViewController"

#pragma mark -

/**
 * GlobalPositionViewController private extension
 */
@interface GlobalPositionViewController()

/**
 * Release the graphic elements
 */
- (void)releaseGlobalPositionViewControllerGraphicElements;

/**
 * Loads the information from the session
 */
- (void)loadInformationFromSession;

/**
 * Calculates the amounts for each row or section in the table
 */
- (void)calculateAmounts;

/**
 * Returns a global position cell for the object sent
 *
 * @param object The object
 * @param smallRightTopText Small right top text flag. YES to display the right top text in a smaller font, NO otherwise.
 */
- (GlobalPositionCell *)globalPositionCellForObject:(NSObject *)object
                              withSmallRightTopText:(BOOL)smallRightTopText;

/**
 * Global position has been received. The information is updated in the view
 *
 * @param notification The notification
 */
- (void)globalPositionReceived:(NSNotification *)notification;

/**
 * Displays the selected bank account
 *
 * @param account The account
 */
- (void)displayAccount:(BankAccount *)account;

/**
 * Displays the selected card
 *
 * @param card The card
 */
- (void)displayCard:(Card *)card;

/**
 * Displays the CCI information
 */
- (void)displayCCIInformation;

/**
 * Decices if the row corresponds to the deposit summary
 *
 * @param  index The row
 * @return YES if the row corresponds to the summary
 */
- (BOOL)isDepositSummary:(NSInteger)index;

/**
 * Normalizes the table seciton depending on whether the coordinate key section must be displayed or not
 *
 * @param section The section to normalized
 * @return The normalized section
 */
- (NSUInteger)normalizeSection:(NSUInteger)section;


@end

#pragma mark -

@implementation GlobalPositionViewController

#pragma mark -
#pragma mark Properties

@synthesize table = table_;
@synthesize shouldBeCleaned = shouldBeCleaned_;
@synthesize campaignList = campaignList_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self releaseGlobalPositionViewControllerGraphicElements];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self
                                  name:kNotificationGlobalPositionResponseReceived
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:kNotificationCampaignListResponseReceived
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:kNotificationIncrementOfLineStepOneResponse
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:kNotificationFastLoanStepOneResponse
                                object:nil];
    
    [notificationCenter removeObserver:self
                                  name:kNotificationSalaryAdvanceAffiliationResponse
                                object:nil];
    [notificationCenter removeObserver:self
                                  name:kNotificationSalaryAdvanceReceiveResponse
                                object:nil];
    
    [cciViewController_ release];
    cciViewController_ = nil;
    
    [accountTransactionsListViewController_ release];
    accountTransactionsListViewController_ = nil;
    
    [cardTransactionsListViewController_ release];
    cardTransactionsListViewController_ = nil;
    
    [accountsArray_ release];
    accountsArray_ = nil;
    
    [depositsDictionary_ release];
    depositsDictionary_ = nil;
    
    [stepAccountsArray_ release];
    stepAccountsArray_ = nil;
    
    [ffmmArray_ release];
    ffmmArray_ = nil;
    
    [continentalStockArray_ release];
    continentalStockArray_ = nil;
    
    [cardsArray_ release];
    cardsArray_ = nil;
    
    [loansArray_ release];
    loansArray_ = nil;
    
    [array_ release];
    array_ = nil;
    
    [amounts_ release];
    amounts_ = nil;
    
    [campaignList_ release];
    campaignList_ = nil;
    
    [super dealloc];
    
}

/*
 * Release the graphic elements
 */
- (void)releaseGlobalPositionViewControllerGraphicElements {
    
    [table_ release];
    table_ = nil;
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [[self navigationController] setNavigationBarHidden:NO];
    
    [self releaseGlobalPositionViewControllerGraphicElements];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseGlobalPositionViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Creates the internal structures
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    accountsArray_ = [[NSMutableArray alloc] init];
    depositsDictionary_ = [[NSMutableDictionary alloc] init];
    stepAccountsArray_ = [[NSMutableArray alloc] init];
    ffmmArray_ = [[NSMutableArray alloc] init];
    continentalStockArray_ = [[NSMutableArray alloc] init];
    cardsArray_ = [[NSMutableArray alloc] init];
    loansArray_ = [[NSMutableArray alloc] init];
    
    amounts_ = [[NSMutableDictionary alloc] init];
	
	shouldBeCleaned_ = YES;
    campaignList_ = nil;
    
}

/*
 * Creates a new autoreleased instance from the associated NIB file
 */
+ (GlobalPositionViewController *)globalPositionViewController {
    
    GlobalPositionViewController *result = [[[GlobalPositionViewController alloc] initWithNibName:NIB_NAME bundle:nil] autorelease];
    [result awakeFromNib];
    
	return result;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory. Controls are initialized
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    ctsIndicator1_ = NO;
    ctsIndicator2_ = NO;
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        self.navigationController.navigationBar.tintColor = [UIColor BBVABlueSpectrumColor];
        
    } else
    {
        self.navigationController.navigationBar.barTintColor = [UIColor BBVABlueSpectrumColor];
        self.navigationController.navigationBar.translucent = NO;
        
    }
	
	[NXT_Peru_iPhoneStyler styleNXTView:[self view]];
	[NXT_Peru_iPhoneStyler styleTableView:table_];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
	[notificationCenter addObserver:self 
                           selector:@selector(globalPositionReceived:) 
                               name:kNotificationGlobalPositionResponseReceived object:nil];
    [notificationCenter addObserver:self
                           selector:@selector(campaignListReceived:)
                               name:kNotificationCampaignListResponseReceived object:nil];
    [notificationCenter addObserver:self
                           selector:@selector(incrementOfLineReceived:)
                               name:kNotificationIncrementOfLineStepOneResponse object:nil];
    [notificationCenter addObserver:self
                           selector:@selector(fastLoanReceived:)
                               name:kNotificationFastLoanStepOneResponse object:nil];
    [notificationCenter addObserver:self
                           selector:@selector(salaryAdvanceAffiliationReceived:)
                               name:kNotificationSalaryAdvanceAffiliationResponse object:nil];
    [notificationCenter addObserver:self
                           selector:@selector(salaryAdvanceReceiveReceived:)
                               name:kNotificationSalaryAdvanceReceiveResponse object:nil];

    [[self navigationController] setNavigationBarHidden:NO animated:NO];
#if defined(__IPHONE_3_2) && (__IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2)
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
#else
    [[UIApplication sharedApplication] setStatusBarHidden:NO animated:NO];
#endif
    
    campaignList_ = nil;
    [self loadInformationFromSession];
    
    [self.appDelegate showActivityIndicator:poai_Both];
    [[Updater getInstance] obtainCampaignList];
	
}

-(void)openGlobalPosition{
    if ([Session getInstance].globalPositionRequestServerData) {
        [Session getInstance].globalPositionRequestServerData = NO;
        
        ctsIndicator1_ = NO;
        ctsIndicator2_ = NO;
        
        [self.appDelegate showActivityIndicator:poai_Both];
        [[Updater getInstance] obtainGlobalPosition];
        
        
        if (shouldBeCleaned_) {
            
            [table_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f) animated:NO];
            
            shouldBeCleaned_ = NO;
            
            [table_ setHidden:YES];
            
        }
        
    } else {
        
        [self loadInformationFromSession];
        
    }
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [[self appDelegate] setTabBarVisibility:YES animated:YES];

}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view.
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self
                                  name:kNotificationGlobalPositionResponseReceived
                                object:nil];
    [notificationCenter removeObserver:self
                                  name:kNotificationCampaignListResponseReceived
                                object:nil];
    [notificationCenter removeObserver:self
                               name:kNotificationIncrementOfLineStepOneResponse
                                object:nil];
    [notificationCenter removeObserver:self
                                  name:kNotificationFastLoanStepOneResponse
                                object:nil];
    [notificationCenter removeObserver:self
                                  name:kNotificationSalaryAdvanceAffiliationResponse
                                object:nil];
    [notificationCenter removeObserver:self
                                  name:kNotificationSalaryAdvanceReceiveResponse
                                object:nil];
}

#pragma mark -
#pragma mark UITableView methods

/**	
 * Asks the data source to return the number of sections in the table view
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger result = 1;
    
    BOOL savingsAccounts = ([accountsArray_ count] > 0) ? YES : NO;
    BOOL savingsDeposits = ([depositsDictionary_ count] > 0) ? YES : NO;
    BOOL savings = savingsAccounts || savingsDeposits;
    
    BOOL investmentStepAccounts = ([stepAccountsArray_ count] > 0) ? YES : NO;
    BOOL investmentFFMM = ([ffmmArray_ count] > 0) ? YES : NO;
    BOOL investmentContinentalStock = ([continentalStockArray_ count] > 0) ? YES : NO;
    BOOL investment = investmentStepAccounts || investmentFFMM || investmentContinentalStock;
    
    BOOL fundingCards = ([cardsArray_ count] > 0) ? YES : NO;
    BOOL fundingLoans = ([loansArray_ count] > 0) ? YES : NO;
    BOOL funding = fundingCards || fundingLoans; 
    
    result = savingsAccounts ? result + 1 : result;
    result = savingsDeposits ? result + 1 : result;
    result = savings ? result + 1 : result;
    result = investmentStepAccounts ? result + 1 : result;
    result = investmentFFMM ? result + 1 : result;
    result = investmentContinentalStock ? result + 1 : result;
    result = investment ? result + 1 : result;
    result = fundingCards ? result + 1 : result;
    result = fundingLoans ? result + 1 : result;
    result = funding ? result + 1 : result;
    
	return result;
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        
    NSInteger normalizedSection = [self normalizeSection:indexPath.section];
    
    UITableViewCell *result = nil;
        
    if (normalizedSection == 0) {
        
        NXTTableCell *result = (NXTTableCell *)[tableView dequeueReusableCellWithIdentifier:[NXTTableCell cellIdentifier]];
        
        if (result == nil) {
            result = [NXTTableCell NXTTableCell];
        }
        
        result.leftText = NSLocalizedString(IAC_POPUP_TITLE_TEXT_KEY, nil);
        [NXT_Peru_iPhoneStyler styleLabel:result.leftTextLabel withFontSize:13.0f color:[UIColor BBVAGreyColor]];
        result.rightText = @"";
        result.showDisclosureArrow = YES;
        result.showSeparator = YES;
        result.cellSeparatorTopImage.hidden = YES;
        return result;
        
    } if (normalizedSection == 2 ) {
        
        NSObject *object = nil;
        isStepAccount_ = NO;
        object = [accountsArray_ objectAtIndex:indexPath.row];
        GlobalPositionCell *result = [self globalPositionCellForObject:object
                                                 withSmallRightTopText:YES];
        return result;
    
    } else if (normalizedSection == 3) {
    
        if ([self isDepositSummary:indexPath.row]) {
            
            NXTTableCell *result = (NXTTableCell *)[tableView dequeueReusableCellWithIdentifier:[NXTTableCell cellIdentifier]];
            
            if (result == nil) {
                result = [NXTTableCell NXTTableCell];
            }
            
            NSArray *keys = [depositsDictionary_ allKeys];
            NSInteger bottom = 0;
            NSInteger top = 0;
            NSObject *object = nil;
            NSInteger row = indexPath.row - 1; //Pics one of the group
            
            for (NSObject *keyObject in keys) {
                
                NSArray *array = [depositsDictionary_ objectForKey:keyObject];
                
                bottom = top;
                top = top + [array count];
                
                if (row >= bottom && row < top) {
                    
                    object = [array objectAtIndex:row - bottom];
                    break;
                    
                }
                
                top = top + 1;
            }
            
            Deposit *deposit = (Deposit *)object;
            
            if ([[deposit ctsIndicator] isEqualToString:@""]) {
                
                result.rightText = [NSString stringWithFormat:@"%@ %@",[Tools translateServerCurrencySymbolIntoClientCurrencySymbol:deposit.currencyAvailable], [deposit availableBalanceString]];
                
            } else if ([[deposit ctsIndicator] isEqualToString:@"(*)"]) {
                
                result.rightText = [NSString stringWithFormat:@"%@ %@",deposit.availableBalanceString,deposit.ctsIndicator];

            } else if ([[deposit ctsIndicator] isEqualToString:@"(**)"]) {
                                
                result.rightText = [NSString stringWithFormat:@"%@ %@ %@",[Tools translateServerCurrencySymbolIntoClientCurrencySymbol:deposit.currencyAvailable], [deposit availableBalanceString], [deposit ctsIndicator]];
                
            }

            result.leftText = NSLocalizedString(CUSTOMER_AVAILABLE_TEXT_KEY, nil);
            [NXT_Peru_iPhoneStyler styleLabel:result.leftTextLabel withFontSize:13.0f color:[UIColor BBVAGreyToneTwoColor]];
            [NXT_Peru_iPhoneStyler styleLabel:result.rightTextLabel withFontSize:13.0f color:[UIColor BBVAGreyToneTwoColor]];

            result.showDisclosureArrow = NO;
            result.showSeparator = YES;
            result.cellSeparatorTopImage.hidden = YES;
            return result;
            
        } else {
        
            NSArray *keys = [depositsDictionary_ allKeys];
            NSInteger bottom = 0;
            NSInteger top = 0;
            NSObject *object = nil;
            NSInteger row = indexPath.row;
            
            for (NSObject *keyObject in keys) {
                
                NSArray *array = [depositsDictionary_ objectForKey:keyObject];
                
                bottom = top;
                top = top + [array count];
                                
                if (row >= bottom && row < top) {
                    
                    object = [array objectAtIndex:row - bottom];
                    break;
                    
                }
                
                top = top + 1;
            }
            
            GlobalPositionCell *result = [self globalPositionCellForObject:object
                                                     withSmallRightTopText:NO];
            
            return result;        
        
        }

    } else if (normalizedSection == 5 ) {
            
        NSObject *object = nil;
        isStepAccount_ = YES;
        object = [stepAccountsArray_ objectAtIndex:indexPath.row];
        GlobalPositionCell *result = [self globalPositionCellForObject:object
                                                 withSmallRightTopText:NO];
        return result;
        
    } else if (normalizedSection == 6 ) {
            
        NSObject *object = nil;
        object = [ffmmArray_ objectAtIndex:indexPath.row];
        GlobalPositionCell *result = [self globalPositionCellForObject:object
                                                 withSmallRightTopText:NO];
        return result;

    } else if (normalizedSection == 7 ) {
         
        NSObject *object = nil;
        object = [continentalStockArray_ objectAtIndex:indexPath.row];
        GlobalPositionCell *result = [self globalPositionCellForObject:object
                                                 withSmallRightTopText:NO];
        return result;

    } else if (normalizedSection == 9 ) {
        
        NSObject *object = nil;
        object = [cardsArray_ objectAtIndex:indexPath.row];
        GlobalPositionCell *result = [self globalPositionCellForObject:object
                                                 withSmallRightTopText:NO];
        return result;

    } else if (normalizedSection == 10 ) {

        NSObject *object = nil;
        object = [loansArray_ objectAtIndex:indexPath.row];
        GlobalPositionCell *result = [self globalPositionCellForObject:object
                                                 withSmallRightTopText:NO];
        return result;

    }
    
    return result;
    
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger normalizedSection = [self normalizeSection:indexPath.section];
    
    if (normalizedSection == 0) {
    
        [self displayCCIInformation];
    
    } else if (normalizedSection == 2) {
        
        BankAccount *account = [accountsArray_ objectAtIndex:indexPath.row];
        [self displayAccount:account];
        
    } else if (normalizedSection == 9) {
        
        Card *card = [cardsArray_ objectAtIndex:indexPath.row];
        [self displayCard:card];
        
    }
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger result = 0;
    
    NSInteger normalizedSection = [self normalizeSection:section];
     
    if (normalizedSection == 0) {
        
        result = 1;
        
    } else if (normalizedSection == 1) {
    
        result = 0;
        
    } else if (normalizedSection == 2) {
        
        result = [accountsArray_ count];
        
    } else if (normalizedSection == 3) {
        
        NSArray *keys = [depositsDictionary_ allKeys];
        
        for (Deposit *object in keys) {
            
            NSArray *array = [depositsDictionary_ objectForKey:object];
            result = result + [array count] + 1;
            
            
        }
                
    } else if (normalizedSection == 4) {
        
        result = 0;
        
    } else if (normalizedSection == 5) {
        
        result = [stepAccountsArray_ count];
        
    } else if (normalizedSection == 6) {
        
        result = [ffmmArray_ count];
        
    } else if (normalizedSection == 7) {
        
        result = [continentalStockArray_ count];
        
    } else if (normalizedSection == 8) {
        
        result = 0;
        
    } else if (normalizedSection == 9) {
        
        result = [cardsArray_ count];
        
    } else if (normalizedSection == 10) {
        
        result = [loansArray_ count];
        
    }
    
    return result;
    
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat result = 0.0f;
    
    NSInteger normalizedSection = [self normalizeSection:indexPath.section];
    
    if (normalizedSection == 0) {
        
        result = [NXTTableCell cellHeight];
        
    } else if (normalizedSection == 2 || 
               normalizedSection == 5 || 
               normalizedSection == 6 ||
               normalizedSection == 7 || 
               normalizedSection == 9 ||
               normalizedSection == 10) {
    
        result = [GlobalPositionCell cellHeightForDetailsNumber:2];

    } else if (normalizedSection == 3) {
    
        if ([self isDepositSummary:indexPath.row]) {
        
            result = [NXTTableCell cellHeight];
        
        } else {
        
            result = [GlobalPositionCell cellHeightForDetailsNumber:1];

        }

    }
    
    return result;
    
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    CGFloat result = 0.0f;
	
	NSInteger normalizedSection = [self normalizeSection:section];
    
    if (normalizedSection > 0) {
        
        result = [ProductHeaderView height];
        
    }
    
    return result;
    
}

/**
 * Asks the delegate for a view object to display in the header of the specified section of the table view.
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        
        return nil;
        
    } else {
		
		NSInteger normalizedSection = [self normalizeSection:section];
        
        ProductHeaderView *result = [ProductHeaderView productHeaderView];

        if (normalizedSection == 1) {
            
            [result setTitle:NSLocalizedString(SAVINGS_TITLE_TEXT_KEY, nil)];
            [result setAmount:[amounts_ objectForKey:NSLocalizedString(SAVINGS_TITLE_TEXT_KEY, nil)]];
            
        } else if (normalizedSection == 2) {

            [result setTitle:NSLocalizedString(ACCOUNTS_PRODUCT_GROUP_KEY, nil)];
            [result setAmount:[amounts_ objectForKey:NSLocalizedString(ACCOUNTS_PRODUCT_GROUP_KEY, nil)]];
            
        } else if (normalizedSection == 3) {
            
            [result setTitle:NSLocalizedString(DEPOSITS_PRODUCT_GROUP_KEY, nil)];
            [result setAmount:[amounts_ objectForKey:NSLocalizedString(DEPOSITS_PRODUCT_GROUP_KEY, nil)]];
            
        } else if (normalizedSection == 4) {
            
            [result setTitle:NSLocalizedString(INVESTMENTS_TITLE_TEXT_KEY, nil)];
            [result setAmount:[amounts_ objectForKey:NSLocalizedString(INVESTMENTS_TITLE_TEXT_KEY, nil)]];
            
        } else if (normalizedSection == 5) {
            
            [result setTitle:NSLocalizedString(TERM_ACCOUNTS_PRODUCT_GROUP_KEY, nil)];
            [result setAmount:[amounts_ objectForKey:NSLocalizedString(TERM_ACCOUNTS_PRODUCT_GROUP_KEY, nil)]];
            
        } else if (normalizedSection == 6) {
            
            [result setTitle:NSLocalizedString(FUNDS_PRODUCT_GROUP_KEY, nil)];
            [result setAmount:[amounts_ objectForKey:NSLocalizedString(FUNDS_PRODUCT_GROUP_KEY, nil)]];
            
        } else if (normalizedSection == 7) {
            
            [result setTitle:NSLocalizedString(STOCKS_PRODUCT_GROUP_KEY, nil)];
            [result setAmount:[amounts_ objectForKey:NSLocalizedString(STOCKS_PRODUCT_GROUP_KEY, nil)]];
            
        } else if (normalizedSection == 8) {
            
            [result setTitle:NSLocalizedString(FINANCING_TITLE_TEXT_KEY, nil)];
            [result setAmount:[amounts_ objectForKey:NSLocalizedString(FINANCING_TITLE_TEXT_KEY, nil)]];
            
        } else if (normalizedSection == 9) {
            
            [result setTitle:NSLocalizedString(CARDS_PRODUCT_GROUP_KEY, nil)];
            [result setAmount:[amounts_ objectForKey:NSLocalizedString(CARDS_PRODUCT_GROUP_KEY, nil)]];
            
        } else if (normalizedSection == 10) {
            
            [result setTitle:NSLocalizedString(LOANS_PRODUCT_GROUP_KEY, nil)];
            [result setAmount:[amounts_ objectForKey:NSLocalizedString(LOANS_PRODUCT_GROUP_KEY, nil)]];
            
        }
             
        return result;

    }
    
}

/**
 * Asks the delegate for a view object to display in the footer of the specified section of the table view.
 */
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    NSInteger normalizedSection = [self normalizeSection:section];
    
    if (normalizedSection == 3) {
        
        Session *session = [Session getInstance];
        NSString *text = nil;

        
        if (ctsIndicator1_) {
            text = session.additionalInformation.messageCTS1;
        }
        
        if (ctsIndicator2_) {
            
            if (text == nil) {
                text = session.additionalInformation.messageCTS2;
            } else {
            
                text = [NSString stringWithFormat:@"%@\n%@", text, session.additionalInformation.messageCTS2];
            
            }
                        
        }
                
        NSInteger numberOfLines = 0;
        
        UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(15.0f, 3.0f, 289.0f, 34.0f)] autorelease];
        label.backgroundColor = [UIColor clearColor];
        [NXT_Peru_iPhoneStyler styleLabel:label withFontSize:11.0f color:[UIColor darkGrayColor]]; 
        
        [label setText:text];
        
        if (ctsIndicator1_) {
            numberOfLines += 2;
        }
        if (ctsIndicator2_) {
            numberOfLines *= 2;
        }
        
        [label setNumberOfLines:numberOfLines];
        
        CGFloat labelHeight = [Tools labelHeight:label
                                         forText:text];
        
        CGRect frame = label.frame;
        frame.size.height = labelHeight;
        label.frame = frame;
                
        UIView *result = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 289.0f, labelHeight + 10.0f)] autorelease];

        [NXT_Peru_iPhoneStyler styleNXTView:result];
        
        [result addSubview:label];
        
        return result;
        
    } else {
    
        return nil;
    
    }

}

/**
 * Asks the delegate for the height to use for the footer of a particular section.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {

    CGFloat result = 0.0f;
    
    NSInteger normalizedSection = [self normalizeSection:section];
    
    if (normalizedSection == 3) {
        
        Session *session = [Session getInstance];
        
        NSString *text = nil;
        
        if (ctsIndicator1_) {
            text = session.additionalInformation.messageCTS1;
        }
        
        if (ctsIndicator2_) {
            
            if (text == nil) {
                text = session.additionalInformation.messageCTS2;
            } else {
                
                text = [NSString stringWithFormat:@"%@\n%@", text, session.additionalInformation.messageCTS2];
                
            }
            
        }
        
        //Calculates the label height
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 0.0f, 289.0f, 34.0f)];
        
        NSInteger numberOfLines = 0;
        
        if (ctsIndicator1_) {
            numberOfLines += 2;
        }
        if (ctsIndicator2_) {
            numberOfLines += 2;
        }
        
        [NXT_Peru_iPhoneStyler styleLabel:label withFontSize:11.0f color:[UIColor darkGrayColor]]; 
        [label setNumberOfLines:numberOfLines];
        
        result = [Tools labelHeight:label
                            forText:text];
        
        result = result + 10.0f;
        
        [label release];
        
    }

    return result;

}


#pragma mark -
#pragma mark Cells configuration

/**
 * Returns a global position cell for the object sent
 *
 * @param object The object
 * @param smallRightTopText Will a small right top text.
 * @return The global position cell.
 */
- (GlobalPositionCell *)globalPositionCellForObject:(NSObject *)object
                              withSmallRightTopText:(BOOL)smallRightTopText {

    GlobalPositionCell *cell = (GlobalPositionCell *)[table_ dequeueReusableCellWithIdentifier:[GlobalPositionCell cellIdentifier]];    
    if( !cell ) {
        cell = [GlobalPositionCell globalPositionCell];
    }
    
    cell.smallRightTopText = smallRightTopText;
    
    if ([object isKindOfClass:[BankAccount class]]) {
       
        BankAccount *account = (BankAccount *)object;

        [cell setTitleText:[account accountType]];
        [cell setSubtitleText:[Tools obfuscateAccountNumber:[account number]]];
        
        NSString *currency = [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:[account currency]];
        
        [cell setRightTopTitleText:NSLocalizedString(ACCOUNT_AMOUNT_HEADER2_KEY, nil)];
        [cell setRightTopText:[NSString stringWithFormat:@"%@ %@", currency, [account currentBalanceString]]];
        [cell setRightMiddleTitleText:NSLocalizedString(ACCOUNT_AMOUNT_HEADER3_KEY, nil)];
        [cell setRightMiddleText:[NSString stringWithFormat:@"%@ %@", currency, [account availableBalanceString]]];
        [cell setRightBottomTitleText:@""];
        [cell setRightBottomText:@""];

        if (!isStepAccount_) {
            cell.showDisclosureArrow = [@"YES" isEqualToString:NSLocalizedString(ACCOUNTS_ARROW, nil)] ? YES : NO;
            cell.selectionStyle = [@"YES" isEqualToString:NSLocalizedString(ACCOUNTS_ARROW, nil)] ? UITableViewCellSelectionStyleGray : UITableViewCellSelectionStyleNone;
        } else {
            cell.showDisclosureArrow = [@"YES" isEqualToString:NSLocalizedString(STEP_ACCOUNTS_ARROW, nil)] ? YES : NO;
            cell.selectionStyle = [@"YES" isEqualToString:NSLocalizedString(STEP_ACCOUNTS_ARROW, nil)] ? UITableViewCellSelectionStyleGray : UITableViewCellSelectionStyleNone;
        }
            
    } else if ([object isKindOfClass:[Deposit class]]) {
        
        Deposit *deposit = (Deposit *)object;
        
        [cell setTitleText:NSLocalizedString(DEPOSIT_TYPE_CONSTANT_TEXT_KEY, nil)];
        [cell setSubtitleText:[Tools obfuscateAccountNumber:deposit.accountNumber]];
        
        NSString *currency = [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:deposit.currency];

        [cell setRightTopTitleText:NSLocalizedString(DEPOSIT_AMOUNT_HEADER2_KEY, nil)];
        [cell setRightTopText:[NSString stringWithFormat:@"%@ %@", currency, [deposit currentBalanceString]]];
        [cell setRightMiddleTitleText:@""];
        [cell setRightMiddleText:@""];
        [cell setRightBottomTitleText:@""];
        [cell setRightBottomText:@""];
        cell.showDisclosureArrow = [@"YES" isEqualToString:NSLocalizedString(DEPOSITS_ARROW, nil)] ? YES : NO;
        cell.selectionStyle = [@"YES" isEqualToString:NSLocalizedString(DEPOSITS_ARROW, nil)] ? UITableViewCellSelectionStyleGray : UITableViewCellSelectionStyleNone;

        
    } else if ([object isKindOfClass:[MutualFund class]]) {
        
        MutualFund *fund = (MutualFund *)object;

        [cell setTitleText:[fund type]];
        [cell setSubtitleText:[Tools obfuscateAccountNumber:[fund number]]];
        
        NSString *currency = [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:fund.currency];

        [cell setRightTopTitleText:NSLocalizedString(MUTUAL_FUND_AMOUNT_HEADER2_KEY, nil)];
        [cell setRightTopText:[NSString stringWithFormat:@"%@ %@", currency, [fund shareValueString]]];
        [cell setRightMiddleTitleText:NSLocalizedString(MUTUAL_FUND_AMOUNT_HEADER1_KEY, nil)];
        [cell setRightMiddleText:[NSString stringWithFormat:@"%@ %@", currency, [fund valueString]]];
//        [cell setRightBottomTitleText:NSLocalizedString(MUTUAL_FUND_AMOUNT_HEADER3_KEY, nil)];
//        [cell setRightBottomText:[NSString stringWithFormat:@"%@ %@", currency, [fund totalShareString]]];        
        cell.showDisclosureArrow = [@"YES" isEqualToString:NSLocalizedString(FFMM_ARROW, nil)] ? YES : NO;
        cell.selectionStyle = [@"YES" isEqualToString:NSLocalizedString(FFMM_ARROW, nil)] ? UITableViewCellSelectionStyleGray : UITableViewCellSelectionStyleNone;
    
    } else if ([object isKindOfClass:[StockMarketAccount class]]) {
        
        StockMarketAccount *stock = (StockMarketAccount *)object;
        
        [cell setTitleText:NSLocalizedString(STOCK_MARKET_TEXT_KEY, nil)];
        [cell setSubtitleText:[Tools obfuscateAccountNumber:[stock valueAccount]]];
        
        NSString *currency = [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:[stock currency]];
        
        [cell setRightTopTitleText:NSLocalizedString(STOCK_MARKET_AMOUNT_HEADER2_KEY, nil)];
        [cell setRightTopText:[NSString stringWithFormat:@"%@ %@", currency, [stock cashBalanceString]]];
        [cell setRightMiddleTitleText:NSLocalizedString(STOCK_MARKET_AMOUNT_HEADER3_KEY, nil)];
        [cell setRightMiddleText:[NSString stringWithFormat:@"%@ %@", currency, [stock valueBalanceString]]];    
        [cell setRightBottomTitleText:@""];
        [cell setRightBottomText:@""];          
        cell.showDisclosureArrow = [@"YES" isEqualToString:NSLocalizedString(CONTINENTAL_STOCK_ARROW, nil)] ? YES : NO;
        cell.selectionStyle = [@"YES" isEqualToString:NSLocalizedString(CONTINENTAL_STOCK_ARROW, nil)] ? UITableViewCellSelectionStyleGray : UITableViewCellSelectionStyleNone;
        
    } else if ([object isKindOfClass:[Card class]]) {
        
        Card *card = (Card *)object;
        
        [cell setTitleText:[card cardType]];
        [cell setSubtitleText:[card cardIdentifierDisplayedToTheUser]];
        
        NSString *currency = [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:card.currency];
        
        [cell setRightTopTitleText:NSLocalizedString(CARD_AMOUNT_HEADER3_KEY, nil)];
        [cell setRightTopText:[NSString stringWithFormat:@"%@ %@", currency, [card availableCreditString]]];
        [cell setRightMiddleTitleText:NSLocalizedString(CARD_AMOUNT_HEADER2_KEY, nil)];
        [cell setRightMiddleText:[NSString stringWithFormat:@"%@ %@", currency, [card creditLimitString]]];    
//        [cell setRightBottomTitleText:NSLocalizedString(CARD_AMOUNT_HEADER1_KEY, nil)];
//        [cell setRightBottomText:[card type]];       
        cell.showDisclosureArrow = [@"YES" isEqualToString:NSLocalizedString(CARDS_ARROW, nil)] ? YES : NO;
        cell.selectionStyle = [@"YES" isEqualToString:NSLocalizedString(CARDS_ARROW, nil)] ? UITableViewCellSelectionStyleGray : UITableViewCellSelectionStyleNone;
        
    } else if ([object isKindOfClass:[Loan class]]) {
        
        Loan *loan = (Loan *)object;
        
        [cell setTitleText:[loan type]];
        [cell setSubtitleText:[Tools obfuscateAccountNumber:[loan number]]];

        NSString *currency = [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:loan.currency];
        
        [cell setRightTopTitleText:NSLocalizedString(LOAN_AMOUNT_HEADER3_KEY, nil)];
        [cell setRightTopText:[NSString stringWithFormat:@"%@ %@", currency, [loan pendingAmountString]]];
        [cell setRightMiddleTitleText:NSLocalizedString(LOAN_DEBT_TEXT_KEY, nil)];
        
        if ([loan pastDueDebt]) {
            [cell setRightMiddleText:NSLocalizedString(LOAN_PASS_DEBT_TEXT_KEY, nil)];
            cell.rightMiddleTextLabel.textColor = [UIColor BBVAMagentaColor];
        } else {
            [cell setRightMiddleText:NSLocalizedString(LOAN_PENDING_TEXT_KEY, nil)];
            cell.rightMiddleTextLabel.textColor = [UIColor BBVABlueSpectrumToneTwoColor];
        }

        [cell setRightBottomTitleText:@""];
        [cell setRightBottomText:@""]; 
        cell.showDisclosureArrow = [@"YES" isEqualToString:NSLocalizedString(LOANS_ARROW, nil)] ? YES : NO;
        cell.selectionStyle = [@"YES" isEqualToString:NSLocalizedString(LOANS_ARROW, nil)] ? UITableViewCellSelectionStyleGray : UITableViewCellSelectionStyleNone;
        
    } 

    return cell;

}

#pragma mark -
#pragma mark Information management

/**
 * Loads the information from the session
 */
- (void)loadInformationFromSession {

    Session *session = [Session getInstance];
    
    [accountsArray_ removeAllObjects];
    [accountsArray_ addObjectsFromArray:session.accountList.accountList];
    
    [depositsDictionary_ removeAllObjects];
    [depositsDictionary_ addEntriesFromDictionary:session.depositsList.depositDictionary];
    
    [stepAccountsArray_ removeAllObjects];
    [stepAccountsArray_ addObjectsFromArray:session.accountList.termAccountList];
    
    [ffmmArray_ removeAllObjects];
    [ffmmArray_ addObjectsFromArray:session.mutualFundsList.mutualFundsList];
    
    [continentalStockArray_ removeAllObjects];
    [continentalStockArray_ addObjectsFromArray:session.stockMarketAccountsList.stockMarketAccountList];
    
    [cardsArray_ removeAllObjects];
    [cardsArray_ addObjectsFromArray:session.cardList.cardList];
    
    [loansArray_ removeAllObjects];
    [loansArray_ addObjectsFromArray:session.loansList.loanList];
	
    GlobalPositionSummaryView *globalPositionSummaryView = [GlobalPositionSummaryView globalPositionSummaryView];
    globalPositionSummaryView.delegate = self;
	GlobalAdditionalInformation *ginf = [[Session getInstance] additionalInformation];
	
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    // Name + email
    NSString *userName = [ginf titularName];
    
    //75
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    NSString *attribute = @"";

    // VIP
    if ([[ginf customerType] isEqualToString:@"S"]) {
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(VIP_TITLE_KEY, nil);
        
        attribute = @"";
        
        if ([ginf executiveName] != nil) {
            attribute = [ginf executiveName];
        }
        
        [titleAndAttributes addAttribute:attribute];
        
        if (titleAndAttributes != nil) {
            
            [mutableResult addObject:titleAndAttributes];
            
        }
        
    }
    
    // Accumulated points
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ACCUMULATED_POINTS_TITLE_KEY, nil);
    
    attribute = @"";
    
    if ([ginf accumulatedPoints] != nil) {
        attribute = [ginf accumulatedPoints];
    }
    
    [titleAndAttributes addAttribute:attribute];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Purchase change type
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(PURCHASE_EXCHANGE_TITLE_KEY, nil);
    
    attribute = @"0.00";
    
    if ([ginf purchaseExchangeRate] != nil) {
        attribute = [ginf purchaseExchangeRate];
    }
        
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools mainCurrencySymbol], attribute]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Sale change type
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SALE_EXCHANGE_TITLE_KEY, nil);
    
    attribute = @"0.00";
    
    if ([ginf saleExchangeRate] != nil) {
        attribute = [ginf saleExchangeRate];
    }
        
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools mainCurrencySymbol], attribute]];
        
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    BOOL isBannerVisible = (campaignList_ && [campaignList_ count]>0);
    CGFloat height = [GlobalPositionSummaryView heightForInfo:mutableResult andBannerVisible:isBannerVisible];
    
    CGRect frame = globalPositionSummaryView.frame;
    frame.size.height = height;
    globalPositionSummaryView.frame = frame;
    
//    frame = [[table_ tableHeaderView] frame];
//    frame.size.height = height;
//    [[table_ tableHeaderView] setFrame:frame];
    
    [globalPositionSummaryView setTitleAndAttributesArray:mutableResult
                                                  forName:userName
                                                     date:[Tools now]
                                                    andCampaigns:campaignList_
                                                    andController:self andBannerVisible:isBannerVisible];
    
	[table_ setTableHeaderView:globalPositionSummaryView];
    
    [self calculateAmounts];
    [table_ reloadData];
	[table_ scrollsToTop];

}

/**
 * Calculates the amounts for each row or section in the table
 */
- (void)calculateAmounts {

    [amounts_ removeAllObjects];
    
    NSDecimalNumber *total = [NSDecimalNumber zero];
    NSDecimalNumber *groupTotal = [NSDecimalNumber zero];
    NSDecimalNumber *amount;
    
    //
    // SAVINGS
    //
    
    //  Accounts
    
    for (BankAccount *prod in accountsArray_ ) {
        amount = prod.balanceInCountryCurrency;//prod.availableBalance;
        if( amount != [NSDecimalNumber notANumber] ) {
            total = [total decimalNumberByAdding:amount];
        }
    }
    
    [amounts_ setObject:total forKey:NSLocalizedString(ACCOUNTS_PRODUCT_GROUP_KEY, nil)];
    groupTotal = [groupTotal decimalNumberByAdding:total];
    
    //  Deposits
    
    total = [NSDecimalNumber zero];
    
    NSMutableDictionary *auxDictionary = depositsDictionary_;
    NSArray *keys = [auxDictionary allKeys];
    
    Deposit *prod = nil;
    
    for (NSUInteger i = 0; i < [keys count]; i++) {
        
        NSArray *auxArray = [auxDictionary objectForKey:[keys objectAtIndex:i]];
        prod = [auxArray objectAtIndex:0];
        
        amount = prod.balanceInCountryCurrency;//prod.availableBalance;
        if( amount != [NSDecimalNumber notANumber] ) {
            total = [total decimalNumberByAdding:amount];
        }
        
        
        if ([prod.ctsIndicator isEqualToString:@"(*)" ]) {
            ctsIndicator1_ = YES;
        }
        
        if ([prod.ctsIndicator isEqualToString:@"(**)" ]) {
            ctsIndicator2_ = YES;
        }

    }
    
    [amounts_ setObject:total forKey:NSLocalizedString(DEPOSITS_PRODUCT_GROUP_KEY, nil)];
    groupTotal = [groupTotal decimalNumberByAdding:total];
    
    [amounts_ setObject:groupTotal forKey:NSLocalizedString(SAVINGS_TITLE_TEXT_KEY, nil)];

    //
    // INVESTMENTS
    //
    
    total = [NSDecimalNumber zero];
    groupTotal = [NSDecimalNumber zero];
    
    //  Term accounts
    
    for (BankAccount *prod in stepAccountsArray_) {
        amount = prod.balanceInCountryCurrency;//prod.availableBalance;
        if( amount != [NSDecimalNumber notANumber] ) {
            total = [total decimalNumberByAdding:amount];
        }
    }
    
    [amounts_ setObject:total forKey:NSLocalizedString(TERM_ACCOUNTS_PRODUCT_GROUP_KEY, nil)];
    groupTotal = [groupTotal decimalNumberByAdding:total];
    
    //  Mutual funds
    
    total = [NSDecimalNumber zero];
    for (MutualFund *prod in ffmmArray_) {
        amount = prod.valueInCountryCurrency;//prod.totalShare;
        if( amount != [NSDecimalNumber notANumber] ) {
            total = [total decimalNumberByAdding:amount];
        }
    }
    
    [amounts_ setObject:total forKey:NSLocalizedString(FUNDS_PRODUCT_GROUP_KEY, nil)];
    groupTotal = [groupTotal decimalNumberByAdding:total];
    
    
    //  Stock market
    
    total = [NSDecimalNumber zero];
    for (StockMarketAccount *prod in continentalStockArray_) {
        amount = prod.balanceInCountryCurrency;//prod.valueBalance;
        if( amount != [NSDecimalNumber notANumber] ) {
            total = [total decimalNumberByAdding:amount];
        }
    }
    
    [amounts_ setObject:total forKey:NSLocalizedString(STOCKS_PRODUCT_GROUP_KEY, nil)];
    groupTotal = [groupTotal decimalNumberByAdding:total];
    
    [amounts_ setObject:groupTotal forKey:NSLocalizedString(INVESTMENTS_TITLE_TEXT_KEY, nil)];
    
    //
    // FUNDING
    //
    
    groupTotal = [NSDecimalNumber zero];
    
    // Cards
    total = [NSDecimalNumber zero];
    
    for (Card *prod in cardsArray_) {
        amount = prod.creditInCountryCurrency;//prod.availableCredit;
        if( amount != [NSDecimalNumber notANumber] ) {
            total = [total decimalNumberByAdding:amount];
        }
    }
    
    [amounts_ setObject:total forKey:NSLocalizedString(CARDS_PRODUCT_GROUP_KEY, nil)];
    groupTotal = [groupTotal decimalNumberByAdding:total];
    
    // Loans
    total = [NSDecimalNumber zero];
    for( Loan *prod in loansArray_ ) {
        amount = prod.amountInCountryCurrency;//prod.pendingAmount;
        if( amount != [NSDecimalNumber notANumber] ) {
            total = [total decimalNumberByAdding:amount];
        }
    }
    
    [amounts_ setObject:total forKey:NSLocalizedString(LOANS_PRODUCT_GROUP_KEY, nil)];
    groupTotal = [groupTotal decimalNumberByAdding:total];
    
    [amounts_ setObject:groupTotal forKey:NSLocalizedString(FINANCING_TITLE_TEXT_KEY, nil)];

}

/*
 * Decices if the row corresponds to the deposit summary
 *
 * @param  index The row
 * @return YES if the row corresponds to the summary
 */
- (BOOL)isDepositSummary:(NSInteger)index {

    BOOL result = NO;
    
    NSArray *keys = [depositsDictionary_ allKeys];
    NSInteger count = 0;
    
    for (NSObject *object in keys) {
        
        NSArray *array = [depositsDictionary_ objectForKey:object];
        
        count += [array count] + 1;
        
        if (count == (index + 1)) {
            return YES;
        }
        
    }
    
    return result;

}


#pragma mark -
#pragma mark User interaction

/*
 * Displays the selected bank account
 */
- (void)displayAccount:(BankAccount *)account {
    
    if (accountTransactionsListViewController_ == nil) {
        
        accountTransactionsListViewController_ = [[AccountTransactionsListViewController accountTransactionsListViewController] retain];
        
    }
    
    accountTransactionsListViewController_.account = account;

    [self.navigationController pushViewController:accountTransactionsListViewController_ animated:YES];

    
}

/*
 * Displays the selected card
 */
- (void)displayCard:(Card *)card {

    if (cardTransactionsListViewController_ == nil) {
        
        cardTransactionsListViewController_ = [[CardTransactionsListViewController cardTransactionsListViewController] retain];
        
    }
    
    cardTransactionsListViewController_.card = card;
    
    [self.navigationController pushViewController:cardTransactionsListViewController_ animated:YES];
    
}

/*
 * Displays the CCI information
 */
- (void)displayCCIInformation {

    if (cciViewController_ == nil) {
        
        cciViewController_ = [[CCIViewController CCIViewController] retain];
        
    }
    
    [self.navigationController pushViewController:cciViewController_ animated:YES];
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Normalizes the table seciton depending on whether the coordinate key section must be displayed or not
 */
- (NSUInteger)normalizeSection:(NSUInteger)section {
    
    NSUInteger result = section;
    
    if (section != 0) {
        
        BOOL savingsAccounts = ([accountsArray_ count] > 0) ? YES : NO;
        BOOL savingsDeposits = ([depositsDictionary_ count] > 0) ? YES : NO;
        BOOL savings = savingsAccounts || savingsDeposits;
        
        BOOL investmentStepAccounts = ([stepAccountsArray_ count] > 0) ? YES : NO;
        BOOL investmentFFMM = ([ffmmArray_ count] > 0) ? YES : NO;
        BOOL investmentContinentalStock = ([continentalStockArray_ count] > 0) ? YES : NO;
        BOOL investment = investmentStepAccounts || investmentFFMM || investmentContinentalStock;
        
        BOOL fundingCards = ([cardsArray_ count] > 0) ? YES : NO;
        BOOL fundingLoans = ([loansArray_ count] > 0) ? YES : NO;
        BOOL funding = fundingCards || fundingLoans; 
        
        if (result >= 1 && !savings) {
            
            result ++;
        
        } 
        
        if (result >= 2 && !savingsAccounts) {
            
            result ++;
            
        } 
        
        if (result >= 3 && !savingsDeposits) {
            
            result ++;
            
        } 
        
        if (result >= 4 && !investment) {
            
            result ++;
            
        }
        
        if (result >= 5 && !investmentStepAccounts) {
            
            result ++;
            
        }
        
        if (result >= 6 && !investmentFFMM) {
            
            result ++;
            
        }
        
        if (result >= 7 && !investmentContinentalStock) {
            
            result ++;
            
        }
        
        if (result >= 8 && !funding) {
            
            result ++;
            
        }
        
        if (result >= 9 && !fundingCards) {
            
            result ++;
            
        }

    }
    
    return result;
    
}

#pragma mark -
#pragma mark Notifications

/*
 * Global position has been received. The information is updated in the view
 */
- (void)globalPositionReceived:(NSNotification *)notification {
    
    [[self appDelegate] hideActivityIndicator];
    
    [self loadInformationFromSession];
	
	[table_ setHidden:NO];
    
}


- (void)campaignListReceived:(NSNotification *)notification {

    [[self appDelegate] hideActivityIndicator];
    
    CampaignList * response = [notification object];
    campaignList_ = [[response campaignList] retain];
    
    [self openGlobalPosition];
    
}

/*
 *
 */
- (void)incrementOfLineReceived:(NSNotification *)notification {
    
    [[self appDelegate] hideActivityIndicator];
    
    IncrementCreditLineStartUpResponse * response = [notification object];
    
    if(response && ![response isError]){
        IncrementOfLineStepOneViewController * controller = [[IncrementOfLineStepOneViewController incrementOfLineStepOneViewController] retain];
        controller.incrementCreditLineStartUpResponse = response;
        [[self navigationController] pushViewController:controller animated:YES];
    }
    
}

- (void)fastLoanReceived:(NSNotification *)notification {
    
    [[self appDelegate] hideActivityIndicator];
    
    FastLoanStartup * response = [notification object];
    
    if(response && ![response isError]){
        FastLoanStepOneViewController * controller = [[FastLoanStepOneViewController FastLoanStepOneViewController] retain];
        controller.FastLoanStartup = response;
        [[self navigationController] pushViewController:controller animated:YES];
        
    }
    
    
}

- (void)salaryAdvanceAffiliationReceived:(NSNotification *)notification {
    
    [[self appDelegate] hideActivityIndicator];
    
    SalaryAdvanceAffiliation * response = [notification object];
    
    if(response && ![response isError]){
        SalaryAdvanceAffiliationViewController * controller = [[SalaryAdvanceAffiliationViewController salaryAdvanceAffiliationViewController] retain];
        controller.salaryAdvanceAffiliation = response;
        [[self navigationController] pushViewController:controller animated:YES];
    }
}

- (void)salaryAdvanceReceiveReceived:(NSNotification *)notification {
    
    [[self appDelegate] hideActivityIndicator];
    
    SalaryAdvanceReceive * response = [notification object];
    
    if(response && ![response isError]){
        SalaryAdvanceReceiveViewController * controller = [[SalaryAdvanceReceiveViewController SalaryAdvanceReceiveViewController] retain];
        controller.SalaryAdvanceReceive = response;
        [[self navigationController] pushViewController:controller animated:YES];
    }
}


#pragma BannerSelectionDelegate

-(void) bannerSelected:(int32_t)position {

    Campaign * campaign = [campaignList_ objectAtIndex:position];
    //NSLog(@"selected %@  - %@", campaign.type, campaign.text);
    
    
    if ([[campaign type] isEqualToString:@"IL"]) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        [[Updater getInstance] incrementLineOperationStartup];
       
    }
    else if([[campaign type] isEqualToString:@"PI"]){
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        [[Updater getInstance] fastLoanOperationStartup];
    }
    else if([[campaign type] isEqualToString:@"AS"]){
        
        if([[campaign url] isEqualToString:@SALARY_ADVANCE_AFILIATED]){
            [[self appDelegate] showActivityIndicator:poai_Both];
            [[Updater getInstance] salaryAdvanceReceiveOperation:NO];
        }
        else if([[campaign url] isEqualToString:@SALARY_ADVANCE_NO_AFILIATED]){
            [[self appDelegate] showActivityIndicator:poai_Both];
            [[Updater getInstance] salaryAdvanceAffiliationOperation];
        }
        
        
    }
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [self customNavigationItem];
  
    [[result customTitleView] setTopLabelText:NSLocalizedString(GLOBAL_POSITION_TITLE_TEXT_KEY, nil)];
	
	[result setRightBarButtonItem:[[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(EXIT_BUTTON_TITLE_KEY, nil) style:UIBarButtonItemStyleBordered target:[self appDelegate] action:@selector(disconnect)] autorelease]];
    
    return result;
    
}



-(IBAction)bannerButtonClose{
    GlobalPositionSummaryView *globalPositionSummaryView = [GlobalPositionSummaryView globalPositionSummaryView];
    GlobalAdditionalInformation *ginf = [[Session getInstance] additionalInformation];
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    // Name + email
    NSString *userName = [ginf titularName];
    
    //75
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    NSString *attribute = @"";
    
    // VIP
    if ([[ginf customerType] isEqualToString:@"S"]) {
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(VIP_TITLE_KEY, nil);
        
        attribute = @"";
        
        if ([ginf executiveName] != nil) {
            attribute = [ginf executiveName];
        }
        
        [titleAndAttributes addAttribute:attribute];
        
        if (titleAndAttributes != nil) {
            
            [mutableResult addObject:titleAndAttributes];
            
        }
        
    }
    
    // Accumulated points
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ACCUMULATED_POINTS_TITLE_KEY, nil);
    
    attribute = @"";
    
    if ([ginf accumulatedPoints] != nil) {
        attribute = [ginf accumulatedPoints];
    }
    
    [titleAndAttributes addAttribute:attribute];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Purchase change type
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(PURCHASE_EXCHANGE_TITLE_KEY, nil);
    
    attribute = @"0.00";
    
    if ([ginf purchaseExchangeRate] != nil) {
        attribute = [ginf purchaseExchangeRate];
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools mainCurrencySymbol], attribute]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Sale change type
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SALE_EXCHANGE_TITLE_KEY, nil);
    
    attribute = @"0.00";
    
    if ([ginf saleExchangeRate] != nil) {
        attribute = [ginf saleExchangeRate];
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools mainCurrencySymbol], attribute]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    CGFloat height = [GlobalPositionSummaryView heightForInfo:mutableResult andBannerVisible:NO];
    
    CGRect frame = globalPositionSummaryView.frame;
    frame.size.height = height;
    globalPositionSummaryView.frame = frame;
    
    //    frame = [[table_ tableHeaderView] frame];
    //    frame.size.height = height;
    //    [[table_ tableHeaderView] setFrame:frame];
    
    [globalPositionSummaryView setTitleAndAttributesArray:mutableResult
                                                  forName:userName
                                                     date:[Tools now]
                                             andCampaigns:campaignList_
                                            andController:self andBannerVisible:NO];
    
    [table_ setTableHeaderView:globalPositionSummaryView];
    
    
    [table_ reloadData];
    [table_ reloadInputViews];
    
   
}

@end

