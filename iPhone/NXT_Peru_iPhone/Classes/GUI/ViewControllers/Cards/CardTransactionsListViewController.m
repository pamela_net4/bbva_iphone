/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "CardTransactionsListViewController.h"

#import "AccountHeaderView.h"
#import "Card.h"
#import "CardList.h"
#import "CardTableCell.h"
#import "CardTransaction.h"
#import "CardTransactionCell.h"
#import "CardTransactionDetailViewController.h"
#import "CardTransactionList.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "InformationTextCell.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Session.h"
#import "StringKeys.h"
#import "Tools.h"
#import "Updater.h"

/**
 * Defines the Nib file name
 */
#define NIB_NAME                        @"CardTransactionsListViewController"

#pragma mark -

/**
 * CardTransactionsListViewController private extension
 */
@interface CardTransactionsListViewController()

/**
 * Releases the graphic elements
 */
- (void)releaseCardTransactionsListViewControllerGraphicElements;

/**
 * The card has been updated with the transactions list
 */
-(void)cardUpdated:(NSNotification *)notification;

/**
 * Displays the selected transaction detail.
 *
 * @param cardTransaction The account selected.
 */
- (void)displayTransaction:(CardTransaction *)cardTransaction;
    

@end


#pragma mark -

@implementation CardTransactionsListViewController

#pragma mark -
#pragma mark Properties

@synthesize table = table_;
@synthesize brandingLine = brandingLine_;
@dynamic card;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self releaseCardTransactionsListViewControllerGraphicElements];
        
    [card_ release];
    card_ = nil;
    
    [cardTransactionDetailViewController_ release];
    cardTransactionDetailViewController_ = nil;
    
    [transactionsList_ release];
    transactionsList_ = nil;
    
    [super dealloc];
}

/**
 * Releases the graphic elements
 */
- (void)releaseCardTransactionsListViewControllerGraphicElements {

    [table_ release];
    table_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;

}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseCardTransactionsListViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates a new autoreleased instance from the associated NIB file
 */
+ (CardTransactionsListViewController *)cardTransactionsListViewController {
	return [[[CardTransactionsListViewController alloc] initWithNibName:NIB_NAME
                                                                 bundle:nil] autorelease];
}

#pragma mark -
#pragma mark View managment

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
    [NXT_Peru_iPhoneStyler styleTableView:table_];
    
    if (transactionsList_ == nil) {
        transactionsList_ = [[NSMutableArray alloc] init];
    }
    
    brandingLine_.image = [[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];

}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseCardTransactionsListViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];   
    [notificationCenter addObserver:self
                           selector:@selector(cardUpdated:)
                               name:kNotificationRetrieveCardTransactionListEnds
                             object:nil];
    
    
    if ((!activeDownload_) && (card_ != nil) &&
        ((card_.transactionsAdditionalInformation == nil) || (card_.cardTransactionsList == nil))) {
        
        [self.appDelegate showActivityIndicator:poai_Both];
        [[Updater getInstance] obtainCardTransactionsForCardNumber:[card_ cardNumber]];
        activeDownload_ = YES;
        
    }
    
    [table_ reloadData];
}

/**
 * Notifies the view controller that its view was added to a window.
 */
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.appDelegate setTabBarVisibility:NO animated:YES];
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];   
    [notificationCenter removeObserver:self
                                  name:kNotificationRetrieveAccountTransactionListEnds
                                object:nil];
    
}


#pragma mark -
#pragma mark UITableView methods

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *result = nil;
    
    if (tableView == table_) {
        
        if (indexPath.row == 0) {
            
            CardTableCell *cardTableCell = (CardTableCell *)[tableView dequeueReusableCellWithIdentifier:[CardTableCell cellIdentifier]];
            
            if (cardTableCell == nil) {
                
                cardTableCell = [CardTableCell cardTableCell];
            }
            
            cardTableCell.card = card_;
            result = cardTableCell;
            
        } else {
            
            if ([transactionsList_ count] > 0) {
                
                CardTransactionCell *cardTransactionCell = (CardTransactionCell*)[tableView dequeueReusableCellWithIdentifier: [CardTransactionCell cellIdentifier]];
                
                if (cardTransactionCell == nil) {
                    cardTransactionCell = [CardTransactionCell cardTransactionCell];
                }
                
                cardTransactionCell.cardTransaction = [transactionsList_ objectAtIndex:indexPath.row - 1];
                result = cardTransactionCell;
                
            } else {
                
                InformationTextCell *informationTextCell = (InformationTextCell *)[tableView dequeueReusableCellWithIdentifier:[InformationTextCell cellIdentifier]];
                
                if (informationTextCell == nil) {
                    
                    informationTextCell = [InformationTextCell informationTextCell];
                    
                }
                
                [informationTextCell setInformationText:NSLocalizedString(CARD_DOES_NOT_HAVE_TRANSACTIONS_TEXT_KEY, nil)];
                
                result = informationTextCell;
                
            }
            
        }
        
    }
    
    return result;
    
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if ((tableView == table_) && ([transactionsList_ count] > 0) && (indexPath.row != 0)) {
    
        [self displayTransaction:[transactionsList_ objectAtIndex:indexPath.row - 1]];
    
    }
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger result = 1;
    
    NSUInteger transactionsCount = [transactionsList_ count];
    
    if (transactionsCount == 0) {
        
        if (!activeDownload_) {
            
            result ++;
            
        }
        
    } else {
        
        result += transactionsCount;
        
    }
    
    return result;
    
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat result = 0.0f;
    CGFloat ios7Factor = 0.0f;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && [tableView tag] != 3)
        ios7Factor = 35.0f;
    
    
    if (tableView == table_) {
        
        if (indexPath.row == 0) {
            
            result = [CardTableCell cellHeight] + ios7Factor;
            
        } else {
            
            if ([transactionsList_ count] > 0) {
                
                result = [CardTransactionCell cellHeight];
                
            } else {
                
                result = [InformationTextCell cellHeightForText:NSLocalizedString(CARD_DOES_NOT_HAVE_TRANSACTIONS_TEXT_KEY, nil)
                                                          width:CGRectGetWidth([tableView frame])];
                
            }
            
        }
        
    }
    
    return result;
    
}

/**
 * Asks the delegate for a view object to display in the header of the specified section of the table view.
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    AccountHeaderView *result = [AccountHeaderView accountHeaderView];
    //TODO: change titles
    result.topTitle = NSLocalizedString(LIMIT_BALANCE_ACCOUNT_KEY , nil);
    result.topDetail = [NSString stringWithFormat:@"%@ %@",[Tools clientCurrencySymbolForServerCurrency:card_.currency], [card_ creditLimitString]];
    
    result.bottomTitle = NSLocalizedString(AVAILABLE_BALANCE_ACCOUNT_KEY , nil);
    result.bottomDetail = [NSString stringWithFormat:@"%@ %@",[Tools clientCurrencySymbolForServerCurrency:card_.currency], [card_ availableCreditString]];
    
    return result;
}

/**
 * Asks the delegate for the height to use for the header of a particular section.
 *
 * @param tableView The table-view object requesting this information.
 * @param section An index number identifying a section of tableView 
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [AccountHeaderView height];
}

#pragma mark -
#pragma mark User interaction

/*
 * Displays the selected transaction detail
 */
- (void)displayTransaction:(CardTransaction *)cardTransaction {

    if (cardTransactionDetailViewController_ == nil) {
        cardTransactionDetailViewController_ = [[CardTransactionDetailViewController cardTransactionDetailViewController] retain];
    }
    
    cardTransactionDetailViewController_.movementsList = transactionsList_;
    cardTransactionDetailViewController_.selectedMovement = cardTransaction;
    cardTransactionDetailViewController_.card = card_;
    [self.navigationController pushViewController:cardTransactionDetailViewController_ animated:YES];
    
}


#pragma mark -
#pragma mark Notifications

/*
 * The account has been updated with the transactions list
 */
- (void)cardUpdated:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    activeDownload_ = NO;
    
    Card *theCard = [notification object];
        
    if ([theCard.errorCode length] == 0) {
        
        [transactionsList_ removeAllObjects];
        [transactionsList_ addObjectsFromArray:[[card_ cardTransactionsList] cardTransactionList]];
        
        [table_ reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
    
}



#pragma mark -
#pragma mark Properties methods

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    
    result.customTitleView.topLabelText = [card_ cardType];
    result.customTitleView.bottomLabelText = [card_ cardNumber];
    
    return result;
    
}


/**
 * Returns the bank account
 */
- (Card *)card {
    return card_;
}

/*
 * Sets the bank account
 */
- (void)setCard:(Card *)card {
    
    if (card_ != card) {
        [card_ release];
        card_ = [card retain];
    }
    
    if (transactionsList_ == nil) {
        transactionsList_ = [[NSMutableArray alloc] init];
    }
    
    [transactionsList_ removeAllObjects];
    [transactionsList_ addObjectsFromArray:[[card_ cardTransactionsList] cardTransactionList]];
    
    [table_ reloadData];

}

@end
