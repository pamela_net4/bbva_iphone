/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "NXTViewController.h"


//Forward delcataions
@class Card;
@class CardTransactionDetailViewController;

/**
 * Movements list view. It only has a table view and a bottom bar with options. Allow the filtering of movements
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardTransactionsListViewController : NXTViewController <UITableViewDelegate, UITableViewDataSource> {
@private
    /**
     * Table
     */
    UITableView *table_;
    
    /**
     * Branding
     */
    UIImageView *brandingLine_;
    
    /**
     * Card 
     */
    Card *card_;
    
    /**
     * Movement detail view controller
     */
    CardTransactionDetailViewController *cardTransactionDetailViewController_;
    
    /**
     * List of transactions
     */
    NSMutableArray *transactionsList_;
    
    /**
     * Active download flag. YES when there is an active download, NO otherwise.
     */
    BOOL activeDownload_;
    
}

/**
 * Provides readwrite access to the table. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *table;

/**
 * Provides readwrite access to the brandingLine. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingLine;

/**
 * Provides readwrite access to the account
 */
@property (nonatomic, readwrite, retain) Card *card;

/**
 * Creates a new autoreleased instance from the associated NIB file
 */
+ (CardTransactionsListViewController *)cardTransactionsListViewController;

@end
