/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "AccountList.h"

#import "GiftCardStepOneResponse.h"
#import "GlobalAdditionalInformation.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Session.h"
#import "Session.h"
#import "StatusEnabledResponse.h"
#import "StringKeys.h"
#import "Tools.h"
#import "TransferBetweenAccounts.h"
#import "TransferBetweenAccountsViewController.h"
#import "TransferToGiftCardViewController.h"
#import "TransferToGiftCard.h"
#import "TransferMenuCell.h"
#import "TransferStartupResponse.h"
#import "TransferToOtherBankAccountViewController.h"
#import "TransferToOtherBankAccounts.h"
#import "TransferToThirdAccounts.h"
#import "TransferToThirdAccountsViewController.h"
#import "RegisteredAccountListViewController.h"
#import "TransfersListViewController.h"
#import "TransferListCashMobileViewController.h"
#import "UIColor+BBVA_Colors.h"
#import "Updater.h"

#pragma mark -
#pragma mark Static attributes

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"TransfersListViewController"

/**
 * GlobalPositionViewController private extension
 */
@interface TransfersListViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releaseTransfersListViewControllerGraphicElements;

/**
 * Receives the response of the transfer between accounts startup
 *
 * @private
 */
- (void)transferBetweenAccountsStartupReceived:(NSNotification *)notification;

/**
 * Receives the response of the transfer to third accounts startup
 *
 * @private
 */
- (void)transferToThirdAccountsStartupReceived:(NSNotification *)notification;

/**
 * Receives the response of the transfer to account from other banks startup
 *
 * @param notification The notification
 * @private
 */
- (void)transferToAccountsFromOtherBanksStartupReceived:(NSNotification *)notification;

/**
 * Receives the response of the transfer to gift card startup
 *
 * @param notification The notification
 * @private
 */
- (void)transferToGiftCardStartupReceived:(NSNotification *)notification;

@end

#pragma mark -


@implementation TransfersListViewController


#pragma mark -
#pragma mark Properties

@synthesize transfersOptionsTableView = transfersOptionsTableView_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransfersListViewControllerGraphicElements];
    
    [transferBetweenAccountsViewController_ release];
    transferBetweenAccountsViewController_ = nil;
    
    [transferToThirdAccountsViewController_ release];
    transferToThirdAccountsViewController_ = nil;
    
    [transferToGiftCardViewController_ release];
    transferToGiftCardViewController_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransfersListViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseTransfersListViewControllerGraphicElements {
    
	[transfersOptionsTableView_ release];
    transfersOptionsTableView_ = nil;
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        self.navigationController.navigationBar.tintColor = [UIColor BBVABlueSpectrumColor];
        
    } else
    {
        self.navigationController.navigationBar.barTintColor = [UIColor BBVABlueSpectrumColor];
        self.navigationController.navigationBar.translucent = NO;
        
    }
    
    [NXT_Peru_iPhoneStyler styleTableView:transfersOptionsTableView_];
    
    transfersOptionsTableView_.scrollEnabled = NO;    
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releaseTransfersListViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    
    [notificationCenter addObserver:self selector:@selector(transferBetweenAccountsStartupReceived:) name:kNotificationTransferBetweenAccountsStartupEnds object:nil];
    [notificationCenter addObserver:self selector:@selector(transferToThirdAccountsStartupReceived:) name:kNotificationTransferToThirdAccountsStartupEnds object:nil];
	[notificationCenter addObserver:self selector:@selector(transferToAccountsFromOtherBanksStartupReceived:) name:kNotificationTransferToAccountFromOtherBanksStartupEnds object:nil];
    [notificationCenter addObserver:self selector:@selector(transferToGiftCardStartupReceived:) name:kNotificationTransferToGiftCardStartupEnds object:nil];
	
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:YES animated:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    NSNotificationCenter *notifCenter = [NSNotificationCenter defaultCenter];
    
    [notifCenter removeObserver:self name:kNotificationTransferBetweenAccountsStartupEnds object:nil];
    [notifCenter removeObserver:self name:kNotificationTransferToThirdAccountsStartupEnds object:nil];
	[notifCenter removeObserver:self name:kNotificationTransferToAccountFromOtherBanksStartupEnds object:nil];
    [notifCenter removeObserver:self name:kNotificationTransferToGiftCardStartupEnds object:nil];
	
}

/*
 * Creates and returns an autoreleased TransfersListViewController constructed from a NIB file.
 */
+ (TransfersListViewController *)transfersListViewController {
    
    TransfersListViewController *result =  [[[TransfersListViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark UITableView methods

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TransferMenuCell *result = (TransferMenuCell *)[tableView dequeueReusableCellWithIdentifier:[TransferMenuCell cellIdentifier]];
    
    if (result == nil) {
        result = [TransferMenuCell transferMenuCell];
    }
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    switch (indexPath.row) {
        case 0:
            
            result.titleLabel.text = NSLocalizedString(TRANSFER_WITH_CASH_MOBILE_TEXT_1_KEY, nil);
            result.image.image = [imagesCache imageNamed:TRANSFER_TO_OTHERS_IMAGE_FILE_NAME];
            break;
            
        case 1:
            result.titleLabel.text = NSLocalizedString(TRANSFER_BETWEEN_ACCOUNTS_TEXT_1_KEY, nil);
            result.image.image = [imagesCache imageNamed:TRANSFER_BETWEEN_ACCOUNTS_IMAGE_FILE_NAME];
            break;
            
        case 2:
            result.titleLabel.text = NSLocalizedString(TRANSFER_TO_THIRD_ACCOUNTS_TEXT_1_KEY, nil);
            result.image.image = [imagesCache imageNamed:TRANSFER_TO_OTHERS_IMAGE_FILE_NAME];
            break;
            
        case 3:
            
            result.titleLabel.text = NSLocalizedString(TRANSFER_TO_ANOTHER_BANK_TEXT_1_KEY, nil);
            result.image.image = [imagesCache imageNamed:TRANSFER_TO_OTHERS_IMAGE_FILE_NAME];
            break;
            
        case 4:
            
            result.titleLabel.text = NSLocalizedString(TRANSFER_GIFT_CARD_TEXT_1_KEY, nil);
            result.image.image = [imagesCache imageNamed:TRANSFER_TO_OTHERS_IMAGE_FILE_NAME];
            break;
            
        case 5:
            
            result.titleLabel.text = NSLocalizedString(TRANSFER_INSCRIP_CARD_TITLE_KEY, nil);
            result.image.image = [imagesCache imageNamed:TRANSFER_TO_OTHERS_IMAGE_FILE_NAME];
            break;
            
        default:
            break;                  
    }
    
    result.selectionStyle = UITableViewCellSelectionStyleGray;
    result.backgroundColor = [UIColor clearColor];
    return result;
    
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void) tableView: (UITableView*) tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Session *session = [Session getInstance];
    BOOL otpActive = [[session additionalInformation] otpActive];
    
    NSArray *transfersAcconts = session.accountList.transferAccountList;
    NSUInteger transfersAccountsCont = [transfersAcconts count];
    
    
        
    switch (indexPath.row) {
        case 0: {
            
            if (otpActive) {
                
                TransferListCashMobileViewController *transferList = [[TransferListCashMobileViewController transferListCashMobileViewController] retain];
                
                [self.navigationController pushViewController:transferList animated:YES];
                
            } else {
                
                [Tools showInfoWithMessage:NSLocalizedString(OTP_SERVICE_NO_ACTIVE_TEXT_KEY, nil)];
                
            }
            
            break;
            
        }
        case 1: {
            
            if (transfersAccountsCont == 1) {
                
                [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ONLY_ONE_ACCOUNT_TEXT_KEY, nil)];
                
            } else {
                
                [self.appDelegate showActivityIndicator:poai_Both];    
                [[Updater getInstance] transferBetweenAccountsStartup];
                
            }
            
            break;
            
        }
        case 2: {
            
            if (otpActive) {
            
                [self.appDelegate showActivityIndicator:poai_Both];   
                [[Updater getInstance] transferToThirdAccountsStartup];
                
            } else {
                
                [Tools showInfoWithMessage:NSLocalizedString(OTP_SERVICE_NO_ACTIVE_TEXT_KEY, nil)];
                
            }
            
            break;  
            
        }
        case 3: {
            
            if (otpActive) {
                
                [self.appDelegate showActivityIndicator:poai_Both];
                [[Updater getInstance] transferToAccountsFromOtherBanksStartup];
                
            } else {
                
                [Tools showInfoWithMessage:NSLocalizedString(OTP_SERVICE_NO_ACTIVE_TEXT_KEY, nil)];
                
            }
            
            break;
            
        }
        case 4: {
            
            if (otpActive) {
                
                [self.appDelegate showActivityIndicator:poai_Both];
                [[Updater getInstance] transferToGiftCardStartup];
                
            } else {
                
                [Tools showInfoWithMessage:NSLocalizedString(OTP_SERVICE_NO_ACTIVE_TEXT_KEY, nil)];
                
            }
            
            break;
            
        }
        case 5: {
            
            if (otpActive) {
                
                [self.appDelegate showActivityIndicator:poai_Both];
                //[[Updater getInstance] transferToGiftCardStartup];
                [self registerThirdCardStartup];
                
            } else {
                
                [Tools showInfoWithMessage:NSLocalizedString(OTP_SERVICE_NO_ACTIVE_TEXT_KEY, nil)];
                
            }
            
            break;
            
        }

        default: {
            
            break;
            
        }
            
    }
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section {
    //oculto incripcion cuentas de terceros
    return 5;

}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [TransferMenuCell cellHeight];

}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [self customNavigationItem];
    
    [[result customTitleView] setTopLabelText:NSLocalizedString(TRANSFERS_POSITION_TITLE_TEXT_KEY, nil)];
    
    return result;
    
}

#pragma mark -
#pragma mark Notifications

/**
 * Receives the response of the transfer between accounts startup
 */
- (void)transferBetweenAccountsStartupReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    TransferStartupResponse *response = [notification object];
    
    if (!response.isError) {
        
        if (transferBetweenAccountsViewController_ == nil) {
            
            transferBetweenAccountsViewController_ = [[TransferBetweenAccountsViewController transferBetweenAccountsViewController] retain];
        }
        
                
        TransferBetweenAccounts *transferHelper = [[[TransferBetweenAccounts alloc] initWithTransferStartupResponse:response] autorelease];
        [transferBetweenAccountsViewController_ setTransferBetweenAccountsHelper:transferHelper];
        [transferBetweenAccountsViewController_ setShouldClearInterfaceData:YES];
        [transferBetweenAccountsViewController_ resetInformation];
        [self.navigationController pushViewController:transferBetweenAccountsViewController_ animated:YES];
   
    }

}

/*
 * Receives the response of the transfer to third accounts startup
 */
- (void)transferToThirdAccountsStartupReceived:(NSNotification *)notification {
   
    [self.appDelegate hideActivityIndicator];
    TransferStartupResponse *response = [notification object];
    
    if (!response.isError) {

        if (transferToThirdAccountsViewController_ == nil) {
            
            transferToThirdAccountsViewController_ = [[TransferToThirdAccountsViewController transferToThirdAccountsViewController] retain];
        }
        
        TransferToThirdAccounts *transferHelper = [[TransferToThirdAccounts alloc] initWithTransferStartupResponse:response];
        [transferToThirdAccountsViewController_ setTransferToThirdAccountsHelper:transferHelper];
        [transferToThirdAccountsViewController_ setShouldClearInterfaceData:YES];
        [transferToThirdAccountsViewController_ resetInformation];
        [self.navigationController pushViewController:transferToThirdAccountsViewController_ animated:YES];
        
        [transferHelper release];
        
    }

}

/*
 * Receives the response of the transfer to account from other banks startup
 */
- (void)transferToAccountsFromOtherBanksStartupReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
	TransferStartupResponse *response = [notification object];
    
    if (!response.isError) {
        
        if(transferToOtherBankAccountViewController_ == nil) {
            
            transferToOtherBankAccountViewController_ = [[TransferToOtherBankAccountViewController transferToOtherAccountViewController] retain];
            
        }
        
        TransferToOtherBankAccounts *transferHelper = [[[TransferToOtherBankAccounts alloc] initWithTransferStartupResponse:response] autorelease];
        [transferToOtherBankAccountViewController_ setTransferToOtherBankAccountsHelper:transferHelper];
        [transferToOtherBankAccountViewController_ setShouldClearInterfaceData:YES];
        [transferToOtherBankAccountViewController_ resetInformation];
        [self.navigationController pushViewController:transferToOtherBankAccountViewController_ animated:YES];
        
    }
    
}

/*
 * Receives the response of the transfer to account from other banks startup
 */
- (void)transferToGiftCardStartupReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
	GiftCardStepOneResponse *response = [notification object];
    
    if (!response.isError) {
        
        if ([@"N" isEqualToString:response.activeState]) {
            return;
        }
        
        if(transferToGiftCardViewController_ == nil) {
            
            transferToGiftCardViewController_ = [[TransferToGiftCardViewController transferToGiftCardViewController] retain];
            
        }
        
        TransferToGiftCard *transferHelper = [[[TransferToGiftCard alloc] initWithTransferStartupResponse:response] autorelease];
        [transferToGiftCardViewController_ setTransferToGiftCardHelper:transferHelper];
        [transferToGiftCardViewController_ setShouldClearInterfaceData:YES];
        [transferToGiftCardViewController_ resetInformation];
        [self.navigationController pushViewController:transferToGiftCardViewController_ animated:YES];
        
    }
    
}

/*
 * Receives the response of the transfer to account from other banks startup
 */
- (void)inscripThirdCardStartupReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    GiftCardStepOneResponse *response = [notification object];
    
    if (!response.isError) {
        
        if ([@"N" isEqualToString:response.activeState]) {
            return;
        }
        
        if(transferToGiftCardViewController_ == nil) {
            
            transferToGiftCardViewController_ = [[TransferToGiftCardViewController transferToGiftCardViewController] retain];
            
        }
        
        TransferToGiftCard *transferHelper = [[[TransferToGiftCard alloc] initWithTransferStartupResponse:response] autorelease];
        [transferToGiftCardViewController_ setTransferToGiftCardHelper:transferHelper];
        [transferToGiftCardViewController_ setShouldClearInterfaceData:YES];
        [transferToGiftCardViewController_ resetInformation];
        [self.navigationController pushViewController:transferToGiftCardViewController_ animated:YES];
        
    }
    
}

/*
 * Receives the response of the registration of account from other banks startup
 */
- (void)registerThirdCardStartup {
    
    [self.appDelegate hideActivityIndicator];
    
  

        
        if(registeredAccountListViewController_ == nil) {
            
            registeredAccountListViewController_ = [[RegisteredAccountListViewController registeredAccountListViewController] retain];
            
        }
    
    [registeredAccountListViewController_ setIsProactive:YES];
        registeredAccountListViewController_.operationListType =@"TODO";
        [self.navigationController pushViewController:registeredAccountListViewController_ animated:YES];
        
   
    
}

@end
