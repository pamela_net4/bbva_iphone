/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TransfersStepOneViewController.h"
#import "NXTComboButton.h"
#import "RegisteredAccountListViewController.h"

//Forward declarations
@class NXTTextField;
@class NXTCurrencyTextField;
@class SimpleSelectorAccountView;
@class OwnAccountView;
@class TransferToOtherBankAccounts;
@class TransferTINOnlineStepViewController;

/**
 * 
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferToOtherBankAccountViewController : TransfersStepOneViewController <UITextFieldDelegate,UIAlertViewDelegate,NXTComboButtonDelegate,RegisteredAccountListViewControllerDelegate>{
    
@private
    
    /*
     * origin account label
     */
    UILabel *originAccountLabel_;
    
    /*
     * origin combo
     */
    NXTComboButton *originCombo_;
    
    /*
     * destination account label
     */
    UILabel *destinationAccountLabel_;
    
    /*
     * destination account view
     */
    SimpleSelectorAccountView *destinationAccountView_;
    
    /*
     * currency label
     */
    UILabel *currencyLabel_;
    
    /*
     * currency combo
     */
    NXTComboButton *currencycombo_;
    
    /*
     * amount label
     */
    UILabel *amountLabel_;
    
    /*
     * amount text field
     */
    NXTCurrencyTextField *amountTextField_;
    
    /*
     * transfer operation helper
     */
    TransferToOtherBankAccounts *TransferToOtherBankAccountsHelper_;
    
    RegisteredAccountListViewController *registeredAccountListViewController_;
    
    
    TransferTINOnlineStepViewController *transferTINOnlineStepViewController_;
    /*
     * the entity string
     */
    NSString *entityString_;
    
    /*
     * the account string
     */
    NSString *accountString_;
    
    /*
     * the office string
     */
    NSString *officeString_;
    
    /*
     * the cc string
     */
    NSString *ccString_;
    
    BOOL isSelecting_;
}

/**
 * Provides readwrite access to the originAccountLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *originAccountLabel;

/**
 * Provides readwrite access to the origin combo. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *originCombo;

/**
 * Provides readwrite access to the destination account label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *destinationAccountLabel;

/**
 * Provides readwrite access to the currency label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *currencyLabel;

/**
 * Provides readwrite access to the currency combo. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *currencyCombo;

/**
 * Provides readwrite access to the amount label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *amountLabel;

/**
 * Provides readwrite access to the amount text field. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTCurrencyTextField *AmountTextField;


/**
 * Provides read-write access to the transfer between accounts helper element
 */
@property (nonatomic, readwrite, retain) TransferToOtherBankAccounts *TransferToOtherBankAccountsHelper;

/**
 * Creates and returns an autoreleased TransferToMyAccountsViewController constructed from a NIB file
 *
 * @return The autoreleased TransferToMyAccountsViewController constructed from a NIB file
 */
+ (TransferToOtherBankAccountViewController *)transferToOtherAccountViewController;

/**
 * Invoked by framework when the origin combo is tapped. Sets it as the editing control
 */
- (IBAction)originComboPressed;


/**
 * Invoked by framework when the currency combo is tapped. Sets it as the editing control
 */
- (IBAction)currencyComboPressed;

@end
