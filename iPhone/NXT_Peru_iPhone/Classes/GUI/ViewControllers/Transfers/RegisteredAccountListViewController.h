//
//  RegisteredAccountListViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/14/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "NXTViewController.h"
@class RegistrationTransferThirdCardViewController;
@class ThirdAccountListResponse;
@class ThirdAccountOperator;
@class TransfersStepTwoViewController;
@class ThirdAccount;

@protocol RegisteredAccountListViewControllerDelegate

@optional
-(void) selectedAccountFromList:(ThirdAccount*)thirdAccount;

@end

@interface RegisteredAccountListViewController : NXTViewController <UITableViewDataSource, UITableViewDelegate>
{
    /**
     * register button to register and account
     */
    UIButton *registerButton_;
    
    /**
     * delete button to remove an account
     */
    UIButton *deleteButton_;
    
    /**
     * List with all the frequents operation
     */
    UITableView *accountsTable_;
    
    /**
     * Indicator to know if the user has been in this view and then goes forward
     */
    BOOL hasForward_;
    
    /**
     * Page to use for the pagination
     */
    NSInteger page_;
    
    /**
     * Page to use for the pagination
     */
    NSInteger selectedIndex_;
    
    /*
     * Array with the accounts
     */
    NSArray *accountArray_;
    
    RegistrationTransferThirdCardViewController *registrationTransferThirdCardViewController_;
    
    ThirdAccountListResponse *initialResponse;
    
    ThirdAccountOperator *thirdAccountOperatorHelper_;
    
    TransfersStepTwoViewController *transfersStepTwoViewController_;
    
    id<RegisteredAccountListViewControllerDelegate> delegate_;
    
    NSString *operationListType_;
    
    BOOL isProactive_;
}

/**
 * Creates and returns an autoreleased RegisteredAccountListViewController constructed from a NIB file.
 *
 * @return The autoreleased RegisteredAccountListViewController constructed from a NIB file.
 */
+ (RegisteredAccountListViewController *)registeredAccountListViewController;

/**
 * Provides readwrite access to the register button. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *registerButton;

/**
 * Provides readwrite access to the delete button. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *deleteButton;


/**
 * Provides readwrite access to the frequent opertaions table. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *accountsTable;

/**
 * Provides readonly access to the frequent operations array
 */
@property (nonatomic, readonly, retain) NSArray *accountArray;

/**
 * Provides readonlywrite access to the flag that indicates if the user has been forward
 */
@property (nonatomic, readwrite, assign) BOOL hasForward;

/**
 * Provides readwrite access to the flag to know if is cash mobile operation
 */
@property (nonatomic, readwrite, assign) BOOL isProactive;

@property (nonatomic, readwrite, assign) id<RegisteredAccountListViewControllerDelegate> delegate;

@property (nonatomic,readwrite,retain) NSString *operationListType;
/**
 * When the user tap the register button
 */
- (IBAction)onTapRegister:(id)sender;

/**
 * When the user tap the delete button
 */
- (IBAction)onTapDelete:(id)sender;

@end
