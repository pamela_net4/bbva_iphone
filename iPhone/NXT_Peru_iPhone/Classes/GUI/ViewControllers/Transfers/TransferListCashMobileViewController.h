//
//  TransferListCashMobileViewController.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 9/25/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "NXTViewController.h"
#import "TransferWithCashMobileConsultViewController.h"

#import "TransferWithCashMobileViewController.h"

@interface TransferListCashMobileViewController : NXTViewController <UITableViewDataSource, UITableViewDelegate>
{
    /**
     * Transfer With cash mobile
     */
	TransferWithCashMobileViewController *TransferWithCashMobileViewController_;
    
    /**
     * Detail of transfers with cash mobile
     */
	TransferWithCashMobileConsultViewController *TransferWithCashMobileConsultViewController_;
}


@property (retain, nonatomic) IBOutlet UITableView *table;

/**
 * Creates and returns an autoreleased TransfersListViewController constructed from a NIB file.
 *
 * @return The autoreleased TransfersListViewController constructed from a NIB file.
 */
+ (TransferListCashMobileViewController *)transferListCashMobileViewController;

@end
