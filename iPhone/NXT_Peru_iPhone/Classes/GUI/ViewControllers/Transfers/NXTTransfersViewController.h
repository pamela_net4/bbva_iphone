/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTEditableViewController.h"


/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        15.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                      12.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLEST_SIZE                                     10.0f

/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_NEAR_ELEMENT                           5.0f

/**
 * Defines the vertical gap between two far away elements
 */
#define VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT2                       10.0f

//Forward declarations
@class NXT_Peru_iPhone_AppDelegate;
@class NXTNavigationItem;
@class SimpleHeaderView;
@class TransferOperationHelper;

/**
 * Base view controller for the transfer steps others than the first one. It provides a basic behaviour and elements
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTTransfersViewController : NXTEditableViewController {
    
@private
    
    /**
     * Operation type header
     */
    SimpleHeaderView *operationTypeHeader_;
    
    /**
     * Container view
     */
    UIView *containerView_;
    
	/**
     * Information table view
     */
    UITableView *informationTableView_;
    
    /**
     * Bottom information label
     */
    UILabel *bottomInfoLabel_;
    
    /**
     * Branding image view
     */
    UIImageView *brandingImageView_;
    
    /**
     * Transfer operation helper
     */
    TransferOperationHelper *transferOperationHelper_;

    /**
     * Variable information array. All objects are TitleAndAttributes instances
     */
    NSMutableArray *variableInformationArray_;
    
    /**
     * To customize when is cash mobile
     */
    BOOL isCashMobileSend_;
    
}

/**
 * Provides readwrite access to the containerView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *containerView;

/**
 * Provides readwrite access to the information table view. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *informationTableView;

/**
 * Provides readwrite access to variableInformationArray.
 */
@property (nonatomic, readwrite, retain) IBOutlet NSMutableArray *variableInformationArray;

/**
 * Provides readwrite access to the bottom information label
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *bottomInfoLabel;

/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;

/**
 * Provides read-only access to operationTypeHeader.
 */
@property (nonatomic, readonly, retain) SimpleHeaderView *operationTypeHeader;

/**
 * Provides readwrite access to the flag to know if is cash mobile operation
 */
@property (nonatomic, readwrite, assign) BOOL isCashMobileSend;

/**
 * Provides read-write access to the transfer operation helper
 */
@property (nonatomic, readwrite, retain) TransferOperationHelper *transferOperationHelper;

@end
