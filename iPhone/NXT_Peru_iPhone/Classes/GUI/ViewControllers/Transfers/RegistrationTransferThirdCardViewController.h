//
//  RegistrationTransferThirdCardViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/5/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//
#import "NXTEditableViewController.h"
#import "TransfersConstants.h"
#import "NXTComboButton.h"


/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        15.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                      12.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLEST_SIZE                                     10.0f

/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_NEAR_ELEMENT                           5.0f

/**
 * Defines the vertical gap between two far away elements
 */
#define VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT                       10.0f

@class NXTTextField;
@class SimpleHeaderView;
@class SimpleSelectorAccountView;
@class ThirdAccountOperator;
@class TransfersStepTwoViewController;

@interface RegistrationTransferThirdCardViewController :   NXTEditableViewController <UITextFieldDelegate, NXTComboButtonDelegate,UIAlertViewDelegate>
{
    
@private
    
    /**
     * Operation title header
     */
    SimpleHeaderView *operationTitleHeader_;
    
    /**
     * Container view to contain the editable components
     */
    UIView *containerView_;
    
    /**
     * Send button
     */
    UIButton *acceptButton_;
    
    /**
     * Advise bottom info label
     */
    UILabel *bottomInfoLabel_;
    
    /**
     * Separator image view
     */
    UIImageView *separator_;
    
    /**
     * Branding image view
     */
    UIImageView *brandingImageView_;
    /*
     * nickname label
     */
    UILabel *nickNameLabel_;
    
    
    /*
     * nickname text field
     */
    NXTTextField *nickNameTextField_;

    
    /*
     * amount label
     */
    UILabel *operationTypeLabel_;
    
    /*
     * origin combo
     */
    NXTComboButton *operationTypeCombo_;
    
    /*
     * destination label
     */
    UILabel *accountLabel_;
    
    /*
     * Destination account
     */
    SimpleSelectorAccountView *numberAccountView_;
    
    /*
     * the entity string
     */
    NSString *entityString_;
    
    /*
     * the account string
     */
    NSString *accountString_;
    
    /*
     * the office string
     */
    NSString *officeString_;
    
    /*
     * the cc string
     */
    NSString *ccString_;
    
    
    NSString *accountType_;
    
    
    int registerOtherBank_;
    
    /*
     * transfer operation helper
     */
    ThirdAccountOperator *ThirdAccountOperatorHelper_;
    
    /**
     * Transfers step two view controller
     */
    TransfersStepTwoViewController *transfersStepTwoViewController_;

}

/**
 * Provides readwrite access to the originAccountLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIView  *containerView;

@property (retain, nonatomic) IBOutlet UIButton *acceptButton;

/**
 * Advise bottom info label
 */
@property (retain, nonatomic) IBOutlet UILabel *bottomInfoLabel;

/**
 * Separator image view
 */
@property (retain, nonatomic) IBOutlet UIImageView *separator;

/**
 * Branding image view
 */
@property (retain, nonatomic) IBOutlet UIImageView *brandingImageView;
/*
 * nickname label
 */
@property (retain, nonatomic) IBOutlet UILabel *nickNameLabel;


/*
 * nickname text field
 */
@property (retain, nonatomic) IBOutlet NXTTextField *nickNameTextField;


/*
 * amount label
 */
@property (retain, nonatomic) IBOutlet UILabel *operationTypeLabel;

/*
 * origin combo
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *operationTypeCombo;

/*
 * destination label
 */
@property (retain, nonatomic) IBOutlet UILabel *accountLabel;

/**
 * Provides readwrite access to the boolean that verify registration of other bank account
 */
@property (nonatomic,readwrite) int registerOtherBank;

@property (nonatomic, readwrite, retain) ThirdAccountOperator *ThirdAccountOperatorHelper;


+ (RegistrationTransferThirdCardViewController *)registrationTransferThirdCardViewController ;

- (IBAction)operationComboPressed;

@end
