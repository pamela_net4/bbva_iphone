/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TransfersStepThreeViewController.h"

#import "TransferToGiftCard.h"
#import "Updater.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTEditableViewController+protected.h"
#import "NXTNavigationItem.h"
#import "NXTTableCell.h"
#import "NXTTransfersViewController+protected.h"
#import "SimpleHeaderView.h"
#import "Session.h"
#import "TitleAndAttributes.h"
#import "StringKeys.h"
#import "Tools.h"
#import "TransferDetailCell.h"
#import "TransferDoneCell.h"
#import "TransferOperationHelper.h"
#import "TransferSuccessResponse.h"
#import "TransferSuccessAdditionalInformation.h"
#import "Constants.h"
#import "StringKeys.h"
#import "FrequentOperationReactiveStepOneViewController.h"
#import "FORCashMobileStepOneResponse.h"
#import "FOInscriptionBaseProcess.h"


/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"TransfersStepThreeViewController"


#pragma mark -

/**
 * TransfersStepThreeViewController private extension
 */
@interface TransfersStepThreeViewController()

/**
 * Releases graphic elements
 */
- (void)releaseTransfersStepThreeViewControllerGraphicElements;

/**
 * Returns the NXTTableCell
 *
 * @return The cell
 */
- (UITableViewCell *)nxtTableCell;

/**
 * Returns the TransferDoneCell
 *
 * @return The cell
 */
- (UITableViewCell *)transferDoneCell;

@end


#pragma mark -

@implementation TransfersStepThreeViewController

#pragma mark -
#pragma mark Properties

@synthesize gpButton = gpButton_;
@synthesize transfersButton = transfersButton_;
@synthesize resendButton = resendButton_;
@synthesize foButton = foButton_;
@synthesize isDetail;
@synthesize foInscriptionProcess = foInscriptionProcess_;
@synthesize isFoFinished = isFoFinished_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self releaseTransfersStepThreeViewControllerGraphicElements];
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransfersStepThreeViewControllerGraphicElements];
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransfersStepThreeViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseTransfersStepThreeViewControllerGraphicElements {
    
	[gpButton_ release];
	gpButton_ = nil;
    
    [transfersButton_ release];
	transfersButton_ = nil;
    
    [resendButton_ release];
	resendButton_ = nil;
    
    [foButton_ release];
	foButton_ = nil;
    
}

#pragma mark -
#pragma mark View managment

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
	[NXT_Peru_iPhoneStyler styleBlueButton:gpButton_];
    [gpButton_ setTitle:NSLocalizedString(GLOBAL_POSITION_TITLE_TEXT_KEY, nil) forState:UIControlStateNormal];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:transfersButton_];
    [transfersButton_ setTitle:NSLocalizedString(TRANSFERS_POSITION_TITLE_TEXT_KEY, nil) forState:UIControlStateNormal];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:resendButton_];
    [resendButton_ setTitle:NSLocalizedString(RESEND_POSITION_TITLE_TEXT_KEY, nil) forState:UIControlStateNormal];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:foButton_];
    [foButton_ setTitle:@"Inscribir a operación frecuente" forState:UIControlStateNormal];
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingImageView] frame]) - CGRectGetHeight([[self operationTypeHeader] frame]);
    [self setScrollNominalFrame:scrollFrame];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased TransfersStepThreeViewController constructed from a NIB file
 */
+ (TransfersStepThreeViewController *)transfersStepThreeViewController {
    
    return [[[TransfersStepThreeViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
}

#pragma mark -
#pragma mark User interaction
/*
 * The foButtonPressed has been pressed
 */
- (IBAction)foButtonPressed
{
    [self.appDelegate showActivityIndicator:poai_Both];
    
    TransferOperationHelper *transferOperationHelper = self.transferOperationHelper;
    
    foInscriptionProcess_ = [[FOInscriptionBaseProcess alloc] init];
    [foInscriptionProcess_ setDelegate:self];
    [transferOperationHelper startFrequentOperationReactiveRequest];
}

- (void) dataAnalysisHasFinished {
    
    [self.appDelegate hideActivityIndicator];
    
    frequentOperationReactiveStepOneViewController_ = [FrequentOperationAlterStepOneViewControllerViewController frequentOperationAlterStepOneViewController];

    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(frequentOperationTransferHasFinished:)  name: @"frequentOperationHasFinished" object: nil];
    
    [foInscriptionProcess_ setRootViewController: self];
    [frequentOperationReactiveStepOneViewController_ setProcess: foInscriptionProcess_];
    [frequentOperationReactiveStepOneViewController_ awakeFromNib];
    
    [self.navigationController pushViewController:frequentOperationReactiveStepOneViewController_ animated:YES];
}

-(void)frequentOperationTransferHasFinished : (NSNotification *) notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"frequentOperationHasFinished" object:nil];
  
    [self SetContentOffSetScrollView:CGPointMake(0.0f, 0.0f)];
    
    foButton_.hidden = TRUE;
    isFoFinished_ = true;
    [self layoutViews];
    
}


/*
 * The gpButtonPressed has been pressed
 */
- (IBAction)gpButtonPressed {
	
    [self.appDelegate displayGlobalPositionTab];
    
}

/*
 * The transfersButtonPressed has been pressed
 */
- (IBAction)transfersButtonPressed {
    
    [[self navigationController] popToRootViewControllerAnimated:YES];
    
}

- (IBAction)resendButtonPressed {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(confirmTransferResendResult:)
                                                 name:kNotificationTransferWithCashMobileDetailResendEnds
                                               object:nil];
    
    [self.appDelegate showActivityIndicator:poai_Both];
    [self.transferOperationHelper startTransferConfirmationRequest];
}

- (void)confirmTransferResendResult: (NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationTransferWithCashMobileDetailResendEnds object:nil];
    
    [self.appDelegate hideActivityIndicator];
    
    TransferSuccessResponse *response = [notification object];
    
    [Tools showInfoWithMessage:response.additionalInformation.confirmationMessage];
    
    if (!response.isError) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ResendSuccess" object:nil];
    }
    
    [resendButton_ setHidden:TRUE];
    
    [self.navigationController popViewControllerAnimated:TRUE];
}

#pragma mark -
#pragma mark Table Cell configuration

/*
 * Returns the NXTTableCell
 */
- (UITableViewCell *)nxtTableCell {
    
    NSString *cellId = [NXTTableCell cellIdentifier];
    
    NXTTableCell *result = (NXTTableCell *)[self.informationTableView dequeueReusableCellWithIdentifier:cellId];
    
    if (result == nil) {
        
        result = [NXTTableCell NXTTableCell];
        
    }
    
    result.leftText = NSLocalizedString(TRANSFER_STEP_3_TEXT_KEY, nil);
    
    result.rightText = @"";
    result.showDisclosureArrow = NO;
    result.showSeparator = NO;
    
    return result;
    
}


/**
 * Returns the TransferDoneCell
 *
 * @return The cell
 */
- (UITableViewCell *)transferDoneCell {
    
    NSString *cellId = [TransferDoneCell cellIdentifier];
    
    TransferDoneCell *result = (TransferDoneCell *)[self.informationTableView dequeueReusableCellWithIdentifier:cellId];
    
    if (result == nil) {
        
        result = [TransferDoneCell transferDoneCell];
        
    }
    
    if (self.isCashMobileSend) {
        result.leftText = NSLocalizedString(TRANSFER_STEP_THREE_CASH_MOBILE_DONE_TEXT_KEY, nil);
    }else if([[self transferOperationHelper] transferOperationType] == TTETransferToGiftCard){
        result.leftText = NSLocalizedString(@"Recarga realizada con éxito", nil);
    }else if([[self transferOperationHelper] transferOperationType] == TTETransferToGiftCard || [[self transferOperationHelper] transferOperationType] ==TTEThirdTransferAccount ){
        result.leftText = NSLocalizedString(@"Operación realizada", nil);
    }else{
        result.leftText = NSLocalizedString(TRANSFER_STEP_THREE_DONE_TEXT_KEY, nil);
    }
    
    result.rightText = @"";
    result.showDisclosureArrow = NO;
    result.showSeparator = NO;
    
    return result;
    
}

#pragma mark -
#pragma mark TransfersViewController selectors

/**
 * Returns the step string to display in the step label. Returns the transfer third step string
 *
 * @return The transfer third step string
 */
- (NSString *)stepLabelString {
    
    return NSLocalizedString(TRANSFER_STEP_3_TEXT_KEY, nil);
    
}

/**
 * Displays the stored information
 */
- (void)displayStoredInformation {
    
    if (!isDetail) {
        [resendButton_ setHidden:TRUE];
        if([self.transferOperationHelper transferOperationType] == TTETransferBetweenUserAccounts || [self.transferOperationHelper transferOperationType] == TTEThirdTransferAccount ||
           isFoFinished_)
        {
            [foButton_ setHidden:TRUE];
        }
        else
        {
            [foButton_ setHidden:FALSE];
        }
    }else{
        [transfersButton_ setHidden:TRUE];
        [gpButton_ setHidden:TRUE];
        if(([self.transferOperationHelper transferOperationType] == TTETransferBetweenUserAccounts &&
           !isFoFinished_ ) || [self.transferOperationHelper transferOperationType] == TTEThirdTransferAccount)
        {
            [foButton_ setHidden:FALSE];
        }
        else
        {
            [foButton_ setHidden:TRUE];
        }
        [resendButton_ setHidden:!_canResend];
    }
    
    [super displayStoredInformation];
    
    if ([self isViewLoaded]) {
        
        [self layoutViews];
        
    }
    
}

/**
 * Lays out the views
 */
- (void)layoutViews {
    
    [super layoutViews];
    
    if ([self isViewLoaded]) {
        
        UITableView *informationTableView = self.informationTableView;
        [informationTableView reloadData];
        CGSize contentSize = informationTableView.contentSize;
        CGFloat height = contentSize.height;
        CGRect frame = informationTableView.frame;
        frame.origin.y = 0.0f;
        frame.size.height = height;
        informationTableView.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT2;
        
        if (!isDetail) {
            
            if([self.transferOperationHelper transferOperationType] != TTETransferBetweenUserAccounts && [self.transferOperationHelper transferOperationType] !=TTEThirdTransferAccount ){
                
                if (!isFoFinished_) {
                    frame = foButton_.frame;
                    frame.origin.y = auxPos;
                    foButton_.frame = frame;
                    auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT2;
                }
            }
            
            frame = gpButton_.frame;
            frame.origin.y = auxPos;
            gpButton_.frame = frame;
            auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
            
            frame = transfersButton_.frame;
            frame.origin.y = auxPos;
            transfersButton_.frame = frame;
            auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
            
            
        }else{
            if (_canResend)
            {
                frame = resendButton_.frame;
                frame.origin.y = auxPos;
                resendButton_.frame = frame;
                auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
            }
        }
        
        
        UILabel *bottomInfoLabe = self.bottomInfoLabel;
        frame = bottomInfoLabe.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:bottomInfoLabe
                            forText:bottomInfoLabe.text];
        frame.size.height = height;
        bottomInfoLabe.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.size.height = auxPos;
        containerView.frame = frame;
        
        [self recalculateScroll];
        
    }
    
}

/**
 * Returns the variable information for the view controller. All objects must be TitleAndAttributes instances (even though the array if
 * filtered). The transfer second step information is returned
 *
 * @return The variable information for the view controller
 * @protected
 */
- (NSArray *)variableInformation {
    
    return [self.transferOperationHelper transferThirdStepInformation];
    
}

#pragma mark -
#pragma mark UITableViewDatasource protocol selector

/**
 * Tells the data source to return the number of rows in a given section of a table view. The number of cells is equal to the number of
 * information elements plus one
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section in tableView
 * @return The number of rows in section
 */
- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section {
    
    NSInteger result = 1;
    result += [self variableInformationEntriesCount];
    
    return result;
    
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. Cells can be a blue transfer done cell or
 * an information cell
 *
 * @param tableView The table-view object requesting this information
 * @param indexPath An index number identifying a section in tableView
 * @return The number of rows in section
 */
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    UITableViewCell *result = nil;
    NSUInteger section = indexPath.section;
    NSUInteger row = indexPath.row;
    
    if (section == 0) {
        
        if (row == 0 && !isDetail) {
            
            result = [self transferDoneCell];
            
        } else {
            
            if (!isDetail) {
                row--;
            }
            result = [self transferDetailCellAtIndex:row
                                        forTableView:tableView];
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark UITableViewDelegate protocol selectors

/**
 * Asks the delegate for the height to use for a row in a specified location.
 *
 * @param tableView The table-view object requesting this information
 * @param indexPath An index path that locates a row in tableView
 * @return A floating-point value that specifies the height (in points) that row should be
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat result = 0.0f;
    
    NSUInteger row = indexPath.row;
    
    if (row == 0) {
        
        result = [TransferDoneCell cellHeight];
        
    } else {
        
        row --;
        result = [self transferDetailCellHeightAtIndex:row];
        
    }
    
    return result;
    
}

/**
 * Asks the delegate for the height to use for the footer of a particular section.
 *
 * @param tableView The table-view object requesting this information.
 * @param section An index number identifying a section of tableView .
 */
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    CGFloat margin = 5.0f;
    CGFloat labelHeight = 30.0f;
    CGFloat result = 0.0f;
    
    TransferOperationHelper *transferOperationHelper = self.transferOperationHelper;
    
    if(transferOperationHelper.transferOperationType == TTEThirdTransferAccount)
        labelHeight =45.0f;
    
    if (transferOperationHelper.canShowITF) {
        
        result = result + margin + labelHeight;
        
    }
    
    if (transferOperationHelper.confirmationMessage != nil && (![transferOperationHelper.confirmationMessage isEqualToString:@""])) {
        
        CGSize size = [self.transferOperationHelper.confirmationMessage sizeWithFont:[NXT_Peru_iPhoneStyler normalFontWithSize:11.0f] constrainedToSize:CGSizeMake(310, 9999) lineBreakMode:NSLineBreakByWordWrapping];
        labelHeight = size.height+15;
        result = result + margin + labelHeight;
        
    }
    
    if (transferOperationHelper.notificationMessage != nil && ![transferOperationHelper.notificationMessage isEqualToString:@""]) {
        
        result = result + margin + labelHeight;
        
    }
    
    result = result + margin;
    return result;
    
}

/**
 * Asks the delegate for a view object to display in the footer of the specified section of the table view.
 */
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 0.0f)] autorelease];
    view.backgroundColor = [UIColor clearColor];
    
    CGFloat yPosition;
    CGFloat margin = 5.0f;
    CGFloat labelHeight = 30.0f;
    
    yPosition = margin;
    
    TransferOperationHelper *transferOperationHelper = self.transferOperationHelper;
    
    if(transferOperationHelper.transferOperationType == TTEThirdTransferAccount)
        labelHeight =45.0f;
    
    if (transferOperationHelper.canShowITF) {
        
        UILabel *itfLabel = [[[UILabel alloc] initWithFrame:CGRectMake(5.0f, yPosition, 310.0f, labelHeight)] autorelease];
        yPosition = yPosition + labelHeight + margin;
        
        [NXT_Peru_iPhoneStyler styleLabel:itfLabel withFontSize:11.0f color:[UIColor grayColor]];
        itfLabel.numberOfLines = 2;
        itfLabel.textAlignment = UITextAlignmentCenter;
        itfLabel.backgroundColor = [UIColor clearColor];
        
        if (self.transferOperationHelper.itfSelected) {
            itfLabel.text = NSLocalizedString(TRANSFER_STEP_ITF_OWN_TEXT_KEY, nil);
            
        } else {
            itfLabel.text = NSLocalizedString(TRANSFER_STEP_ITF_NOT_OWN_TEXT_KEY, nil);
        }
        
        if([self.transferOperationHelper itfMessageSuccess]){
            itfLabel.text = [self.transferOperationHelper itfMessageSuccess];
        }
        
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, yPosition);
        [view addSubview:itfLabel];
        
    }
    
    if (transferOperationHelper.confirmationMessage != nil && (![transferOperationHelper.confirmationMessage isEqualToString:@""])) {
        
        UILabel *confLabel = [[[UILabel alloc] initWithFrame:CGRectMake(5.0f, yPosition, 310.0f, labelHeight)] autorelease];
        [confLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [confLabel setNumberOfLines:5];
        if(transferOperationHelper.transferOperationType == TTETransferToOtherBankAccount){
            confLabel.numberOfLines = 5;
            CGRect frame = [confLabel  frame];
            CGSize size = [self.transferOperationHelper.confirmationMessage sizeWithFont:[NXT_Peru_iPhoneStyler normalFontWithSize:11.0f] constrainedToSize:CGSizeMake(310, 9999) lineBreakMode:NSLineBreakByWordWrapping];
            frame.size.height = size.height+15.0f;
            labelHeight =size.height; 
            [confLabel setFrame:frame];
        }
        
        yPosition = yPosition + labelHeight + margin;
        
        [NXT_Peru_iPhoneStyler styleLabel:confLabel withFontSize:11.0f color:[UIColor grayColor]];
        //confLabel.numberOfLines = 2;
        
        if(transferOperationHelper.transferOperationType == TTEThirdTransferAccount)
             confLabel.numberOfLines = 3;
       
        
        confLabel.textAlignment = UITextAlignmentCenter;
        confLabel.backgroundColor = [UIColor clearColor];
        confLabel.text = self.transferOperationHelper.confirmationMessage;
        
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, yPosition);
        [view addSubview:confLabel];
        
    }
    
    if([[Tools notNilString:transferOperationHelper.notificationMessage] length]>0){
        UILabel *notiLabel = [[[UILabel alloc] initWithFrame:CGRectMake(5.0f, yPosition, 310.0f, labelHeight)] autorelease];
        
        
        if(transferOperationHelper.transferOperationType == TTETransferToOtherBankAccount){
            notiLabel.numberOfLines = 5;
            CGRect frame = [notiLabel  frame];
            CGSize size = [self.transferOperationHelper.notificationMessage sizeWithFont:[NXT_Peru_iPhoneStyler normalFontWithSize:11.0f] constrainedToSize:CGSizeMake(310, 9999) lineBreakMode:NSLineBreakByWordWrapping];
            frame.size.height = size.height;
            labelHeight =size.height;
            [notiLabel setFrame:frame];
        }
        
        yPosition = yPosition + labelHeight + margin;
        
        [NXT_Peru_iPhoneStyler styleLabel:notiLabel withFontSize:11.0f color:[UIColor grayColor]];
        notiLabel.numberOfLines = 2;
        
        if(transferOperationHelper.transferOperationType == TTEThirdTransferAccount)
            notiLabel.numberOfLines = 3;
        
        
        notiLabel.textAlignment = UITextAlignmentCenter;
        notiLabel.backgroundColor = [UIColor clearColor];
        notiLabel.text = self.transferOperationHelper.notificationMessage;
        
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, yPosition);
        [view addSubview:notiLabel];
    }
    
    return view;
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    if (self.isCashMobileSend || isDetail) {
        result.customTitleView.topLabelText = NSLocalizedString(TRANSFER_WITH_CASH_MOBILE_TEXT_1_KEY, nil);
    }else
        result.customTitleView.topLabelText = NSLocalizedString(TRANSFER_TITLE_TEXT_KEY, nil);
    
    if([self.transferOperationHelper transferOperationType] == TTEThirdTransferAccount)
    {
        result.customTitleView.topLabelText = NSLocalizedString(@"Inscripción cuenta de terceros", nil);
    }
    result.hidesBackButton = !isDetail;
    
    return result;
    
}

@end
