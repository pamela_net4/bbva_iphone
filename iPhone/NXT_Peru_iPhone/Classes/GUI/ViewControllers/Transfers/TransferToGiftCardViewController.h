//
//  TransferToGiftCardViewController.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/31/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "TransfersStepOneViewController.h"
#import "TransfersConstants.h"
#import "NXTComboButton.h"
#import "MOKStringListSelectionButton.h"

//Forward declarations
@class NXTCurrencyTextField;
@class TransferToGiftCard;
@class SimpleSelectorAccountView;
@class MOKLimitedLengthTextField;
@interface TransferToGiftCardViewController : TransfersStepOneViewController<UITextFieldDelegate, NXTComboButtonDelegate, MOKStringListSelectionButtonDelegate, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
    
@private
    
    /*
     * origin account label
     */
    UILabel *originAccountLabel_;
    
    /*
     * origin combo
     */
    NXTComboButton *originCombo_;
    
    /*
     * destination label
     */
    UILabel *destinationLabel_;
    
    /**
     * Card View
     */
    UIView *cardView_;
    
    /**
     * First text field
     */
    MOKLimitedLengthTextField *firstTextField_;
    
    /**
     * Second text field
     */
    MOKLimitedLengthTextField *secondTextField_;
    
    /**
     * Third text field
     */
    MOKLimitedLengthTextField *thirdTextField_;
    
    /**
     * Fourth text field
     */
    MOKLimitedLengthTextField *fourthTextField_;
    
    /*
     * currency label
     */
    UILabel *currencyLabel_;
    
    /*
     * currency combo
     */
    NXTComboButton *currencyCombo_;
    
    /*
     * amount label
     */
    UILabel *amountLabel_;
    
    /*
     * amount text field
     */
    NXTCurrencyTextField *amountTextField_;
    
    /*
     * transfer to third account helper
     */
    TransferToGiftCard *transferToGiftCardHelper_;
    
    /*
     * account string
     */
    NSString *accountString_;
    
    /*
     * Table for account selection
     */
    UITableView *accountTypeTableView_;
    
    /**
     *  index of the card selected
     */
    NSInteger selectedIndex;
    
    /**
     * combo with accounts
     */
    MOKStringListSelectionButton *cellAccountCombo_;
    
    /**
     * cell with card
     */
    MOKStringListSelectionButton *cellCardCombo_;
    
    /**
     * Picker View for accounts
     */
    UIPickerView *pickerView_;
}

/**
 * Provides readwrite access to the originAccountLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *originAccountLabel;

/**
 * Provides readwrite access to the originCombo button. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *originCombo;

/**
 * Provides readwrite access to the destinationLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *destinationLabel;

/**
 * Provides read-write access to the cardView and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIView *cardView;

/**
 * Provides read-write access to the firstTextField and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet MOKLimitedLengthTextField *firstTextField;

/**
 * Provides read-write access to the secondTextField and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet MOKLimitedLengthTextField *secondTextField;

/**
 * Provides read-write access to the thirdTextField and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet MOKLimitedLengthTextField *thirdTextField;

/**
 * Provides read-write access to the fourthTextField and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet MOKLimitedLengthTextField *fourthTextField;
/**
 * Provides readwrite access to the currencyLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *currencyLabel;

/**
 * Provides readwrite access to the currencyCombo. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *currencyCombo;


/**
 * Provides readwrite access to the amountLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *amountLabel;

/**
 * Provides readwrite access to the amountTextField. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTCurrencyTextField *amountTextField;

/**
 * Provides readwrite access to the accountTypeTableView.
 */
@property (retain, nonatomic) UITableView *accountTypeTableView;

/**
 * Provides read-write access to the transfer between accounts helper element
 */
@property (nonatomic, readwrite, retain) TransferToGiftCard *transferToGiftCardHelper;

/**
 * Creates and returns an autoreleased TransferToMyAccountsViewController constructed from a NIB file
 *
 * @return The autoreleased TransferToMyAccountsViewController constructed from a NIB file
 */
+ (TransferToGiftCardViewController *)transferToGiftCardViewController;

/**
 * Invoked by framework when the origin combo is tapped. Sets it as the editing control
 */
- (IBAction)originComboPressed;

/**
 * Invoked by framework when the currency combo is tapped. Sets it as the editing control
 */
- (IBAction)currencyComboPressed;


@end
