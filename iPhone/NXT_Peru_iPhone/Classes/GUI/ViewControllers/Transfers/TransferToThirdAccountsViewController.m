/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "Constants.h"
#import "RegisteredAccountListViewController.h"
#import "TransferToThirdAccountsViewController.h"
#import "TransfersStepOneViewController+protected.h"
#import "SimpleSelectorAccountView.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"
#import "StringKeys.h"
#import "Tools.h"
#import "NXTCurrencyTextField.h"
#import "NXTTextField.h"
#import "NXTEditableViewController+protected.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "TransferToThirdAccounts.h"
#import "BankAccount.h"
#import "ThirdAccount.h"
#import "Updater.h"
#import "RegistrationTransferThirdCardViewController.h"
#import "ThirdAccountOperator.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"TransferToThirdAccountsViewController"

/*
 * Max lenght of reference field
 */
#define MAX_TEXT_REFERENCE_LENGHT                                   20



/*
 *max number office text field
 */
#define MAX_NUMBER_OFFICE                                           4

/*
 *max number account text field
 */
#define MAX_NUMBER_ACCOUNT                                          10


#pragma mark -


/**
 * TransferToMyAccountsViewController private extension
 */
@interface TransferToThirdAccountsViewController()

/**
 * Releases graphic elements
 */
- (void)releaseTransferToThirdAccountsViewControllerGraphicElements;

/**
 * Receives the response of the transfer confirmation
 *
 * @param notification The NSNotification object containing the notification information
 * @private
 */
- (void)transferResponseReceived:(NSNotification *)notification;

/**
 * SMS Second text Field is shown.
 *
 * @param on The flag
 */
- (void)smsSecondTextFieldShow:(BOOL)on;

/**
 * Email Second text Field is shown.
 *
 * @param on The flag
 */
- (void)emailSecondTextFieldShow:(BOOL)on;

@end

#pragma mark -

@implementation TransferToThirdAccountsViewController

#pragma mark -
#pragma mark Properties

@synthesize originAccountLabel = originAccountLabel_;
@synthesize originCombo = originCombo_;
@synthesize destinationLabel = destinationLabel_;
@synthesize currencyLabel = currencyLabel_;
@synthesize currencyCombo = currencyCombo_;
@synthesize amountLabel = amountLabel_;
@synthesize amountTextField = amountTextField_;
@synthesize referenceLabel = referenceLabel_;
@synthesize referenceTextField = referenceTextField_;
@synthesize transferToThirdAccountsHelper = transferToThirdAccountsHelper_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransferToThirdAccountsViewControllerGraphicElements];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationTransferConfirmationResponseReceived 
                                                  object:nil];
    
    [transferToThirdAccountsHelper_ release];
    transferToThirdAccountsHelper_ = nil;
    
    [editableViews_ release];
    editableViews_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransferToThirdAccountsViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransferToThirdAccountsViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseTransferToThirdAccountsViewControllerGraphicElements {
    
    [originAccountLabel_ release];
    originAccountLabel_ = nil;
    
    [originCombo_ release];
    originCombo_ = nil;
    
    [destinationLabel_ release];
    destinationLabel_ = nil;
    
	[destinationAccountView_ release];
    destinationAccountView_ = nil;
    
    [currencyLabel_ release];
    currencyLabel_ = nil;
    
    [currencyCombo_ release];
    currencyCombo_ = nil;
    
    [amountLabel_ release];
    amountLabel_ = nil;
    
    [amountTextField_ release];
    amountTextField_ = nil;
    
    [referenceLabel_ release];
    referenceLabel_ = nil;
    
    [referenceTextField_ release];
    referenceTextField_ = nil;
    
    [editableViews_ removeAllObjects];
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler  styleLabel:originAccountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];     
	[originAccountLabel_ setText:NSLocalizedString(ACCOUND_CHARGED_TEXT_KEY, nil)];
    
    UILabel *buttonLabel = originCombo_.titleLabel;
    buttonLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:10.0f];
    buttonLabel.lineBreakMode = UILineBreakModeTailTruncation;
    
    [NXT_Peru_iPhoneStyler styleLabel:destinationLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [destinationLabel_ setText:NSLocalizedString(ACCOUND_PAID_TEXT_KEY, nil)];
    
    if (destinationAccountView_ == nil) {
        
        destinationAccountView_ = [[SimpleSelectorAccountView simpleSelectorAccountView] retain];
        
        
        [destinationAccountView_ setShowAccountSelector:YES];
        
        [destinationAccountView_ setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth];
        
        [destinationAccountView_.accountsButton addTarget:self action:@selector(accountsTapped) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [NXT_Peru_iPhoneStyler styleLabel:currencyLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [currencyLabel_ setText:NSLocalizedString(TRANSACTION_CURRENCY_TEXT_KEY, nil)];
    currencyLabel_.textAlignment = UITextAlignmentLeft;
    
    
    buttonLabel = currencyCombo_.titleLabel;
    buttonLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:12.0f];
    buttonLabel.lineBreakMode = UILineBreakModeHeadTruncation;
    
    [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [amountLabel_ setText:NSLocalizedString(TRANSFER_AMOUNT_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleTextField:amountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    [amountTextField_ setCanContainCents:YES];
    [amountTextField_ setCurrencySymbol:[Tools getCurrencySimbol:CURRENCY_SOLES_LITERAL]];
    [amountTextField_ setDelegate:self];
    [amountTextField_ setPlaceholder:NSLocalizedString(TRANSFER_AMOUNT_HINT_TEXT_KEY, nil)];
    [amountTextField_ setInputAccessoryView:popButtonsView_];

    [NXT_Peru_iPhoneStyler styleLabel:referenceLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [referenceLabel_ setText:NSLocalizedString(REFERENCE_TEXT_KEY, nil)];
        
    [NXT_Peru_iPhoneStyler styleTextField:referenceTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    [referenceTextField_ setDelegate:self];
    [referenceTextField_ setPlaceholder:NSLocalizedString(REFERENCE_HINT_TEXT_KEY, nil)];
    [referenceTextField_ setInputAccessoryView:popButtonsView_];

    [originCombo_ setTitle:NSLocalizedString(TRANSFER_SELECT_ACCOUNT_TEXT_KEY, nil)];    
    [originCombo_ setDelegate:self];
    [originCombo_ setInputAccessoryView:popButtonsView_];
    
    [currencyCombo_ setDelegate:self];
    [currencyCombo_ setInputAccessoryView:popButtonsView_];
    
    NXTTextField *officeTextField = destinationAccountView_.officeTextField;
    
    [officeTextField setDelegate:self];
    [officeTextField setInputAccessoryView:popButtonsView_];

    NXTTextField *accountTextField = destinationAccountView_.accountTextField;
    [accountTextField setDelegate:self];
    [accountTextField setInputAccessoryView:popButtonsView_];

    CGRect frame = [destinationAccountView_ frame];
    frame.origin.x = 10.0f;
    frame.origin.y = 0.0f;
    [destinationAccountView_ setFrame:frame];
    
    UIView *superView = [self containerView];
    [superView addSubview:destinationAccountView_];
    [superView bringSubviewToFront:destinationAccountView_];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(transferResponseReceived:)
                                                 name:kNotificationTransferConfirmationResponseReceived 
                                               object:nil];
    
    if(isSelecting_)
    {
        isSelecting_ = NO;
      //  [self displayStoredInformation];
        return;
    }
    
    
	if (shouldClearInterfaceData_) {
        
        originCombo_.informFirstSelection = YES;
        
        [transferToThirdAccountsHelper_ setSelectedOriginAccountIndex:-1];
        [transferToThirdAccountsHelper_  setSelectedCurrencyIndex:0];
        [transferToThirdAccountsHelper_ setAmountString:@""];
        [transferToThirdAccountsHelper_ setReference:@""];
        [transferToThirdAccountsHelper_ setShowSMS1:NO];
        [transferToThirdAccountsHelper_ setShowSMS2:NO];
        [transferToThirdAccountsHelper_ setSendSMS:NO];
        [transferToThirdAccountsHelper_ setSelectedCarrier1Index:0];
        [transferToThirdAccountsHelper_ setSelectedCarrier2Index: -1];
        [transferToThirdAccountsHelper_ setShowEmail1:NO];
        [transferToThirdAccountsHelper_ setShowEmail2:NO];
        [transferToThirdAccountsHelper_ setSendEmail:NO];
        [transparentScroll_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f) animated:NO];
		
		shouldClearInterfaceData_ = NO;
        
	}
    
    if (editableViews_ == nil) {
        
        editableViews_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [editableViews_ removeAllObjects];
        
    }
    
    [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:originCombo_, 
                                         destinationAccountView_.officeTextField, 
                                         destinationAccountView_.accountTextField, 
                                         currencyCombo_ ,amountTextField_, 
                                         referenceTextField_,
                                         nil]];
    
    if ([transferToThirdAccountsHelper_ canSendEmailandSMS]) {
        
        SMSCell *smsCell = [self smsCell];
        
        if ([transferToThirdAccountsHelper_ sendSMS]) {
            
            if ([transferToThirdAccountsHelper_ showSMS1]) {
                
                NXTTextField *textField = [smsCell firstTextField];
                
                if (textField != nil) {
                    
                    [editableViews_ addObject:textField];
                    
                }
                
                NXTComboButton *comboButton = [smsCell firstComboButton];
                
                if (comboButton != nil) {
                    
                    [editableViews_ addObject:comboButton];
                    
                }
                
            }
            
            if ([transferToThirdAccountsHelper_ showSMS2]) {
                
                if ([transferToThirdAccountsHelper_ showSMS1]) {
                    
                    NXTTextField *textField = [smsCell secondTextField];
                    
                    if (textField != nil) {
                        
                        [editableViews_ addObject:textField];
                        
                    }
                    
                    NXTComboButton *comboButton = [smsCell secondComboButton];
                    
                    if (comboButton != nil) {
                        
                        [editableViews_ addObject:comboButton];
                        
                    }
                    
                }
                
            }
            
        }
        
        if ([transferToThirdAccountsHelper_ sendEmail]) {
            
            EmailCell *emailCell = [self emailCell];
            
            if ([transferToThirdAccountsHelper_ showEmail1]) {
                
                NXTTextField *textField = [emailCell firstTextField];
                
                if (textField != nil) {
                    
                    [editableViews_ addObject:textField];
                    
                }

            }
            
            if ([transferToThirdAccountsHelper_ showEmail2]) {
                
                NXTTextField *textField = [emailCell secondTextField];
                
                if (textField != nil) {
                    
                    [editableViews_ addObject:textField];
                    
                }
                
            }
            
            NXTTextView *textView = [emailCell emailTextView];
            
            if (textView != nil) {
                
                [editableViews_ addObject:textView];
                
            }
            
        }
        
    }
    
    [self displayStoredInformation];
    
    [destinationAccountView_.accountsButton setHidden:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the confirmation response and stores the information
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationTransferConfirmationResponseReceived 
                                                  object:nil];
    
    if(!isSelecting_){
        [officeString_ release];
        officeString_ = nil;
    
        [accountString_ release];
        accountString_ = nil;
       
    [self okButtonClickedInPopButtonsView:popButtonsView_];
    
    [super viewWillDisappear:animated];
    }

}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased TransferToMyAccountsViewController constructed from a NIB file
 */
+ (TransferToThirdAccountsViewController *)transferToThirdAccountsViewController {
    
    return [[[TransferToThirdAccountsViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
}

#pragma mark -
#pragma mark User interaction

- (IBAction)originComboPressed {
    
    [self editableViewHasBeenClicked:originCombo_];
}


- (IBAction)currencyComboPressed {
    
    [self editableViewHasBeenClicked:currencyCombo_];
}

#pragma mark -
#pragma mark TransfersStepOneViewController selectors

/**
 * Displays the stored information. Sets the information into the views
 */
- (void)displayStoredInformation {
    
    [super displayStoredInformation];
    
    if ([self isViewLoaded]) {
        
        NSMutableArray *array = [NSMutableArray array];
        NSArray *originAccountList = transferToThirdAccountsHelper_.originAccountList;
        
        for(BankAccount *account in originAccountList) {
            
            [array addObject:account.accountIdAndDescription];
        }
        
        originCombo_.stringsList = [NSArray arrayWithArray:array];        
        [array removeAllObjects];
        
        NSArray *currencyList = transferToThirdAccountsHelper_.currencyList;
        
        for(NSString *currency in currencyList) {        
            
            [array addObject:[NSLocalizedString(currency, nil) capitalizedString]];
        }        
        
        currencyCombo_.stringsList = [NSArray arrayWithArray:array];
        
        [originCombo_ setSelectedIndex:transferToThirdAccountsHelper_.selectedOriginAccountIndex];
        [currencyCombo_ setSelectedIndex:transferToThirdAccountsHelper_.selectedCurrencyIndex];
        
        [destinationAccountView_.officeTextField setText:transferToThirdAccountsHelper_.selectedDestinationAccountOffice];
        [destinationAccountView_.accountTextField setText:transferToThirdAccountsHelper_.selectedDestinationAccountAccNumber];
        
        [amountTextField_ setText:transferToThirdAccountsHelper_.amountString];
        [referenceTextField_ setText:transferToThirdAccountsHelper_.reference];
        
    }
}

/**
 * Lays out the views
 */
- (void)layoutViews {
    
    [super layoutViews];
    
    if ([self isViewLoaded]) {
                
        CGRect frame = originAccountLabel_.frame;
        frame.origin.y = VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        CGFloat height = [Tools labelHeight:originAccountLabel_ forText:originAccountLabel_.text];        
        frame.size.height = height;
        originAccountLabel_.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = originCombo_.frame;
        frame.origin.y = auxPos;
        originCombo_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = destinationLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:destinationLabel_ forText:destinationLabel_.text];
        frame.size.height = height;
        destinationLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = [destinationAccountView_ frame];
        frame.origin.y = auxPos;
        [destinationAccountView_ setFrame:frame];
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = currencyLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:currencyLabel_ forText:currencyLabel_.text];
        frame.size.height = height;
        currencyLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = currencyCombo_.frame;
        frame.origin.y = auxPos;
        currencyCombo_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = amountLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:amountLabel_ forText:amountLabel_.text];
        frame.size.height = height;
        amountLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = amountTextField_.frame;
        frame.origin.y = auxPos;
        amountTextField_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = referenceLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:referenceLabel_ forText:referenceLabel_.text];
        frame.size.height = height;
        referenceLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = referenceTextField_.frame;
        frame.origin.y = auxPos;
        referenceTextField_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        UILabel *referenceInfoLabel = self.bottomInfoLabel;
        frame = referenceInfoLabel.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:referenceInfoLabel forText:referenceInfoLabel.text];
        frame.size.height = height;
        referenceInfoLabel.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UITableView *selectionTableView = self.selectionTableView;
        [selectionTableView reloadData];
        CGSize contentSize = selectionTableView.contentSize;  
        frame = selectionTableView.frame;
        frame.origin.y = auxPos;
        height = contentSize.height;
        frame.size.height = height;
        selectionTableView.frame = frame;
        selectionTableView.scrollEnabled = NO;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UIButton *transferButton = self.transferButton;
        frame = transferButton.frame;
        frame.origin.y = auxPos;
        transferButton.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.size.height = auxPos;
        containerView.frame = frame;
        
        [self recalculateScroll];
    
    }
}

/**
 * Stores the information into the transfer operation helper instance
 */
- (void)storeInformationIntoHelper {
    
    [super storeInformationIntoHelper];
    
    transferToThirdAccountsHelper_.selectedDestinationAccountOffice = destinationAccountView_.officeTextField.text;
    
    transferToThirdAccountsHelper_.selectedDestinationAccountAccNumber = destinationAccountView_.accountTextField.text;
   
    transferToThirdAccountsHelper_.amountString = amountTextField_.text;
    transferToThirdAccountsHelper_.reference = referenceTextField_.text;

    
}

/**
 * Returns the transfer operation helper associated to the view controller. Returns the transfer between accounts helper
 *
 * @return The transfer operation helper associated to the view controller
 */
- (TransferOperationHelper *)transferOperationHelper {
    
    return transferToThirdAccountsHelper_;
    
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/** 
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
	return YES;
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self editableViewHasBeenClicked:textField];
    
    if( textField == destinationAccountView_.officeTextField) {
        
        if(officeString_ != nil) {
            
            [destinationAccountView_.officeTextField setText:officeString_.description];
        }
    }
    else if(textField == destinationAccountView_.accountTextField) {        
        
        if(accountString_ != nil) {
            
            [destinationAccountView_.accountTextField setText:accountString_.description];
        }
    }            
    
}

/** 
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
        
    result = [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    if(textField == amountTextField_) {  
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            NSScanner *scanner = [NSScanner scannerWithString:resultString];
            NSInteger resultInteger = 0;
            
            if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                if (resultInteger > 0) {
                    
                    result = YES;                                        
                }                
            }               
        }
    } else if (textField == destinationAccountView_.accountTextField) {
    
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            if(resultLength <= MAX_NUMBER_ACCOUNT) {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        result = YES;                                        
                    }                
                } 
            }
            else {
                result = NO;
            }
        }
        
    } else if(textField == destinationAccountView_.officeTextField) {

        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            if(resultLength <= MAX_NUMBER_OFFICE) {            
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        result = YES;                                        
                    }                
                }
            } 
            else {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        if([accountString_ length] == MAX_NUMBER_ACCOUNT) {
                            
                            result = NO;
                        }
                        else {
                            result = YES;    
                        }
                        [destinationAccountView_.accountTextField becomeFirstResponder];
                    }                
                }            
            }
        }
                
    } else if (textField == referenceTextField_) {
        
        if ([resultString length] <= MAX_TEXT_REFERENCE_LENGHT) {
            
            if([Tools isValidText:resultString forCharacterString:REFERENCE_VALID_CHARACTERS]) {
                
                result = YES;
                
            }
            
        } else {
            
            result = NO;
            
        }
        
    }
    
    return result;
    
}

/**
 * Asks the delegate if editing should stop in the specified text field. Forwards it to the external delegate
 *
 * @param textField The text field for which editing is about to end
 * @return YES if editing should stop; otherwise, NO if the editing session should continue
 */
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    BOOL result = YES;
    
    NSString *resultString = [NSString stringWithString:textField.text];
    
//    NSUInteger resultLength = [resultString length];
    
    if(textField == destinationAccountView_.officeTextField) {
        
        if (officeString_ != nil) {
            [officeString_ release];
            officeString_ = nil;
        }
        
        officeString_ = [resultString retain];
        
//        if(resultLength < MAX_NUMBER_OFFICE) {
//            
//            NSString *completeString = [NSString string];
//            for(NSUInteger i=0;i<MAX_NUMBER_OFFICE-resultLength;i++) {
//                
//                completeString = [completeString stringByAppendingString:@"0"];
//            }
//            completeString = [completeString stringByAppendingString:resultString];
//            [destinationAccountView_.officeTextField setText:completeString];
//        }  
    }
    else if (textField == destinationAccountView_.accountTextField) {
        
        if (accountString_ != nil) {
            [accountString_ release];
            accountString_ = nil;
        }
        
        accountString_ = [resultString retain];      
        
//        if(resultLength < MAX_NUMBER_ACCOUNT) {
//            
//            NSString *completeString = [NSString string];
//            for(NSUInteger i=0;i<MAX_NUMBER_ACCOUNT-resultLength;i++) {
//                
//                completeString = [completeString stringByAppendingString:@"0"];
//            }
//            completeString = [completeString stringByAppendingString:resultString];
//            [destinationAccountView_.accountTextField setText:completeString];
//        }
    }
    
    return result;
}

#pragma mark -
#pragma mark NXTComboButtonDelegate selectors

/*
 * Informs the delegate that a value has been selected
 */
- (void)NXTComboButtonValueSelected:(NXTComboButton *)comboButton {
    
    if (comboButton == originCombo_) {
        
        [transferToThirdAccountsHelper_ setSelectedOriginAccountIndex:comboButton.selectedIndex];
    }
    else if (comboButton == currencyCombo_) {
        
        [transferToThirdAccountsHelper_ setSelectedCurrencyIndex:currencyCombo_.selectedIndex];
        
        NSString *currency =[transferToThirdAccountsHelper_ selectedCurrency];        
        [amountTextField_ setCurrencySymbol:[Tools getCurrencySimbol:currency]];
    }
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the transfer between accounts operation helper element
 *
 * @param transferBetweenAccountsHelper The new transfer between accounts operation helper element to set
 */
- (void)setTransferToThirdAccountsHelper:(TransferToThirdAccounts *)transferToThirdAccountsHelper {
    
    if (transferToThirdAccountsHelper != transferToThirdAccountsHelper_) {
        
        [originCombo_ setSelectedIndex:-1];
        [currencyCombo_ setSelectedIndex:-1] ;
        
        [transferToThirdAccountsHelper retain];
        [transferToThirdAccountsHelper_ release];
        transferToThirdAccountsHelper_ = transferToThirdAccountsHelper;
        
        [self displayStoredInformation];
        
    }
    
    [transferToThirdAccountsHelper_ setSecondFactorKey:@""]; 
    
    [self resetScrollToTopLeftAnimated:NO];
    
}

#pragma mark -
#pragma mark TransfersStepOneViewController override methods


/**
 * sms switch has been tapped.
 */
- (void)switchButtonHasBeenTapped:(BOOL)on {
    
    [super switchButtonHasBeenTapped:on];
    
    if (on) {
        
        NSUInteger position = [editableViews_ indexOfObject:self.emailCell.firstTextField];
        
        if (position == NSNotFound) {
            
            [editableViews_  addObject:self.smsCell.firstTextField];
            [editableViews_  addObject:self.smsCell.firstComboButton];
            
        } else {
            
            [editableViews_ insertObject:self.smsCell.firstTextField atIndex:position];
            [editableViews_ insertObject:self.smsCell.firstComboButton atIndex:position + 1];
            
        }
        
    } else {
        
        [editableViews_  removeObject:self.smsCell.firstTextField];
        [editableViews_  removeObject:self.smsCell.firstComboButton];
        
        NSUInteger position = [editableViews_ indexOfObject:self.smsCell.secondTextField];
        
        if (position != NSNotFound) {
            
            [editableViews_  removeObject:self.smsCell.secondTextField];
            [editableViews_  removeObject:self.smsCell.secondComboButton];
            
        }
        
    }
    
}

/**
 * More button has been tapped
 */
- (void)moreButtonHasBeenTapped {
    
    [super moreButtonHasBeenTapped];
    
    [self smsSecondTextFieldShow:YES];
}

/**
 * SMS Second text Field is shown.
 *
 * @param on The flag
 */
- (void)smsSecondTextFieldShow:(BOOL)on {
    
    if (on) {
        
        NSUInteger position = [editableViews_ indexOfObject:self.smsCell.firstComboButton];
        
        [editableViews_ insertObject:self.smsCell.secondTextField atIndex:position + 1];
        [editableViews_ insertObject:self.smsCell.secondComboButton atIndex:position + 2];
        
    } else {
        
        [editableViews_  removeObject:self.smsCell.secondTextField];
        [editableViews_  removeObject:self.smsCell.secondComboButton];
        
    }
    
}

/**
 * Switch has been tapped.
 *
 * @param on The flag
 */
- (void)emailSwitchButtonHasBeenTapped:(BOOL)on {
    
    [super emailSwitchButtonHasBeenTapped:on];
    
    if (on) {
        
        [editableViews_  addObject:self.emailCell.firstTextField];
        [editableViews_  addObject:self.emailCell.emailTextView];
        
    } else {
        
        [editableViews_  removeObject:self.emailCell.firstTextField];
        
        
        NSUInteger position = [editableViews_ indexOfObject:self.emailCell.secondTextField];
        
        if (position != NSNotFound) {
            
            [editableViews_  removeObject:self.emailCell.secondTextField];
            
        }
        
        [editableViews_  removeObject:self.emailCell.emailTextView];
        
    }
}

/**
 * More button has been tapped
 */
- (void)emailMoreButtonHasBeenTapped {
    
    [super emailMoreButtonHasBeenTapped];
    
    [self emailSecondTextFieldShow:YES];
}

/**
 * Email Second text Field is shown.
 *
 * @param on The flag
 */
- (void)emailSecondTextFieldShow:(BOOL)on {
    
    if (on) {
        
        NSUInteger position = [editableViews_ indexOfObject:self.emailCell.emailTextView];
        
        if (position != NSNotFound) {
            
            [editableViews_ insertObject:self.emailCell.secondTextField atIndex:position];
            
        }
        
    } else {
        
        [editableViews_  removeObject:self.emailCell.secondTextField];
        
    }
    
}


#pragma mark -
#pragma mark Notifications management

/*
 * Receives the response of the transfer confirmation
 */
- (void)transferResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (!response.isError) {        
        
        
        if ([transferToThirdAccountsHelper_ transferResponseReceived:response]) {
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationTransferConfirmationResponseReceived object:nil];
            
            [self displayTransferSecondStepView];
            
        } else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
            
        }
        
    }else
    {
        if([response.errorCode isEqualToString:THIRD_ACCOUNT_REGISTRATION_ERROR])
            [Tools showConfirmationDialogWithMessage:response.errorMessage andDelegate:self];
    }
    
}
#pragma mark -
#pragma mark - Implements alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        RegistrationTransferThirdCardViewController *registrationTransferThirdCardViewController = [[RegistrationTransferThirdCardViewController registrationTransferThirdCardViewController] retain];
        
        
        [registrationTransferThirdCardViewController setThirdAccountOperatorHelper:[[ThirdAccountOperator alloc] init] ];
        [registrationTransferThirdCardViewController setRegisterOtherBank:2];
        [self.navigationController pushViewController:registrationTransferThirdCardViewController animated:YES];
    }
}
-(IBAction)accountsTapped
{
    if(registeredAccountListViewController_ == nil){
    
            registeredAccountListViewController_ =
        [[RegisteredAccountListViewController registeredAccountListViewController] retain];
    
    }
    registeredAccountListViewController_.delegate = self;
    [registeredAccountListViewController_ setOperationListType:THIRD_ACCOUNT_ID];
    isSelecting_ =YES;
    [self.navigationController pushViewController:registeredAccountListViewController_ animated:YES];
}

-(void) selectedAccountFromList:(ThirdAccount*)thirdAccount{
    
    NSArray *accountParts=   [thirdAccount.accountNumber componentsSeparatedByString:@"-"];
    officeString_ = [[accountParts objectAtIndex:1] copyWithZone:self.zone];
    accountString_ = [[accountParts objectAtIndex:2] copyWithZone:self.zone];
    
    destinationAccountView_.officeTextField.text = officeString_.description;
    destinationAccountView_.accountTextField.text = accountString_.description;
    
    
    
}

@end

