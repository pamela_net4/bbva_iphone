/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTTransfersViewController.h"
#import "LegalTermsTopCell.h"

@class TermsAndConsiderationsViewController;
@class CoordKeyCell;
@class OpKeyCell;
@class LegalTermsTopCell;
@class TransfersStepThreeViewController;

/**
 * General second step in the transfer process
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransfersStepTwoViewController : NXTTransfersViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate, LegalTermsTopCellDelegate> {
    
@private
    
    /**
     * Accept button
     */
    UIButton *acceptButton_;
    
    /**
     * Coordinates key cell
     */
    CoordKeyCell *coordKeyCell_;
    
    /**
     * OTP key cell
     */
    OpKeyCell *otpKeyCell_;
    
    /**
     * Legal terms cell
     */
    LegalTermsTopCell *legalTermsTopCell_;
    
    /**
     * Terms And Considerations View Controller
     */
    TermsAndConsiderationsViewController *termsAndConsiderationsViewController_;
    
    /**
     * Transfers Step Three View Controller
     */
    TransfersStepThreeViewController *transfersStepThreeViewController_;
    
    
}

/**
 * Provides readwrite access to the acceptButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *acceptButton;


/**
 * Creates and returns an autoreleased TransfersStepTwoViewController constructed from a NIB file
 *
 * @return The autoreleased TransfersStepTwoViewController constructed from a NIB file
 */
+ (TransfersStepTwoViewController *)transfersStepTwoViewController;


/**
 * The acceptButton has been tapped
 */
- (IBAction)acceptButtonTapped;


@end
