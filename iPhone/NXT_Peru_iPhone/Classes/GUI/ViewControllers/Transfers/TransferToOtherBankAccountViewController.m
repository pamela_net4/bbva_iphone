/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "BankAccount.h"
#import "RegisteredAccountListViewController.h"
#import "Constants.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTCurrencyTextField.h"
#import "NXTEditableViewController+protected.h"
#import "NXTTextField.h"
#import "OwnAccountView.h"
#import "SimpleSelectorAccountView.h"
#import "StringKeys.h"
#import "RegistrationTransferThirdCardViewController.h"
#import "TransferTINOnlineStepViewController.h"
#import "TransferToOtherBankAccounts.h"
#import "TransferToOtherBankAccountViewController.h"
#import "TransferStartupResponse.h"
#import "TransfersStepOneViewController+protected.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "ThirdAccount.h"
#import "Updater.h"
#import "ThirdAccountOperator.h"


/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"TransferToOtherBankAccountViewController"


#pragma mark -

/**
 * TransferToMyAccountsViewController private extension
 */
@interface TransferToOtherBankAccountViewController()

/**
 * Releases graphic elements
 */
- (void)releaseTransferToOtherBankAccountViewControllerGraphicElements;

/*
 * validate the doc number document
 *
 *@param the document type
 */
- (BOOL)validateTextFieldDocumentNumber:(NSString *)code textToValidate:(NSString *)text;

/**
 * SMS Second text Field is shown.
 *
 * @param on The flag
 */
- (void)smsSecondTextFieldShow:(BOOL)on;

/**
 * Email Second text Field is shown.
 *
 * @param on The flag
 */
- (void)emailSecondTextFieldShow:(BOOL)on;


@end

#pragma mark -

@implementation TransferToOtherBankAccountViewController

#pragma mark -
#pragma mark Properties

@synthesize originAccountLabel = originAccountLabel_;
@synthesize originCombo = originCombo_;
@synthesize destinationAccountLabel = destinationAccountLabel_;
@synthesize currencyLabel = currencyLabel_;
@synthesize currencyCombo = currencycombo_;
@synthesize amountLabel = amountLabel_;
@synthesize AmountTextField = amountTextField_;
@synthesize TransferToOtherBankAccountsHelper = TransferToOtherBankAccountsHelper_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransferToOtherBankAccountViewControllerGraphicElements];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationTransferTypeCheckResponseReceived
                                                  object:nil];
    
    [TransferToOtherBankAccountsHelper_ release];
    TransferToOtherBankAccountsHelper_ = nil;        
    
    [editableViews_ release];
    editableViews_ = nil;

    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [self releaseTransferToOtherBankAccountViewControllerGraphicElements];

    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransferToOtherBankAccountViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseTransferToOtherBankAccountViewControllerGraphicElements {
    
    [originAccountLabel_ release];
    originAccountLabel_ = nil;
    
    [originCombo_ release];
    originCombo_ = nil;
    
    [destinationAccountLabel_ release];
    destinationAccountLabel_ = nil;
    
    [destinationAccountView_ release];
    destinationAccountView_ = nil;
    
    
    [amountLabel_ release];
    amountLabel_ = nil;
    
    [amountTextField_ release];
    amountTextField_ = nil;
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    
    [NXT_Peru_iPhoneStyler  styleLabel:originAccountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];     
	[originAccountLabel_ setText:NSLocalizedString(ACCOUND_CHARGED_TEXT_KEY, nil)];
    
    UILabel *buttonLabel = originCombo_.titleLabel;
    buttonLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:10.0f];
    buttonLabel.lineBreakMode = UILineBreakModeTailTruncation;
    
    [originCombo_ setTitle:NSLocalizedString(TRANSFER_SELECT_ACCOUNT_TEXT_KEY, nil)];    
    [originCombo_ setDelegate:self];
    [originCombo_ setInputAccessoryView:popButtonsView_];
    
    [NXT_Peru_iPhoneStyler styleLabel:destinationAccountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [destinationAccountLabel_ setText:NSLocalizedString(DESTINATION_INTER_ACCOUNT_TEXT_KEY, nil)];
    
    if(destinationAccountView_ == nil) {
        
        destinationAccountView_ = [[SimpleSelectorAccountView simpleSelectorAccountView] retain];
        
        [destinationAccountView_ setShowCc:YES];
        
        [destinationAccountView_ setShowAccountSelector:YES];
        
        [destinationAccountView_ setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth];
        
        [destinationAccountView_.accountsButton addTarget:self action:@selector(accountsTapped) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [NXT_Peru_iPhoneStyler styleLabel:currencyLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [currencyLabel_ setText:NSLocalizedString(TRANSACTION_CURRENCY_TEXT_KEY, nil)];
    
    buttonLabel = currencycombo_.titleLabel;
    buttonLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:12.0f];
    buttonLabel.lineBreakMode = UILineBreakModeHeadTruncation;
    [currencycombo_ setDelegate:self];
    [currencycombo_ setInputAccessoryView:popButtonsView_];
    
    [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [amountLabel_ setText:NSLocalizedString(TRANSFER_AMOUNT_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleTextField:amountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    [amountTextField_ setCanContainCents:YES];
    [amountTextField_ setCurrencySymbol:[Tools getCurrencySimbol:CURRENCY_SOLES_LITERAL]];    
    [amountTextField_ setDelegate:self];
    [amountTextField_ setPlaceholder:NSLocalizedString(TRANSFER_AMOUNT_HINT_TEXT_KEY, nil)];
    [amountTextField_ setInputAccessoryView:popButtonsView_];
    
    NXTTextField *officeTextField = destinationAccountView_.officeTextField;
    [officeTextField setDelegate:self];
    [officeTextField setInputAccessoryView:popButtonsView_];
    
    NXTTextField *accountTextField = destinationAccountView_.accountTextField;
    [accountTextField setDelegate:self];
    [accountTextField setInputAccessoryView:popButtonsView_];
    
    NXTTextField *entityTextField = destinationAccountView_.entityTextField;
    [entityTextField setDelegate:self];
    [entityTextField setInputAccessoryView:popButtonsView_];
    
    NXTTextField *ccTextField = destinationAccountView_.CcTextField;
    [ccTextField setDelegate:self];
    [ccTextField setInputAccessoryView:popButtonsView_];    
    
    CGRect frame = [destinationAccountView_ frame];
    frame.origin.x = 10.0f;
    frame.origin.y = 0.0f;
    [destinationAccountView_ setFrame:frame];
    
    UIView *superView = [self containerView];
    
    [superView addSubview:destinationAccountView_];
    [superView bringSubviewToFront:destinationAccountView_];
   
    [self setScrollableView:superView];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(transferResponseReceived:)
                                                 name:kNotificationTransferTypeCheckResponseReceived
                                               object:nil];
    
    if(isSelecting_){
        isSelecting_ = NO;
        return;
    }
    
    [self.transferButton setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil)
                         forState:UIControlStateNormal];
    
	if (shouldClearInterfaceData_) {
        
        originCombo_.informFirstSelection = YES;

        [TransferToOtherBankAccountsHelper_ setSelectedOriginAccountIndex:-1];
        [TransferToOtherBankAccountsHelper_ setSelectedDocumentTypeIndex:0];
        [TransferToOtherBankAccountsHelper_ setSelectedCurrencyIndex:-1];
        [TransferToOtherBankAccountsHelper_ setSelectedCurrencyIndex:0];
        [TransferToOtherBankAccountsHelper_ setBeneficiary:@""];
        [TransferToOtherBankAccountsHelper_ setAmountString:@""];
        [TransferToOtherBankAccountsHelper_ setDocumentNumber:@""];
        [TransferToOtherBankAccountsHelper_ setSelectedDestinationAccountOffice:@""];
        [TransferToOtherBankAccountsHelper_ setSelectedDestinationAccountAccNumber:@""];
        [TransferToOtherBankAccountsHelper_ setItfSelected:NO];
        [TransferToOtherBankAccountsHelper_ setSelectedDestinationAccountCc:@""];
        [TransferToOtherBankAccountsHelper_ setShowSMS1:NO];
        [TransferToOtherBankAccountsHelper_ setShowSMS2:NO];
        [TransferToOtherBankAccountsHelper_ setSendSMS:NO];
        [TransferToOtherBankAccountsHelper_ setShowEmail1:NO];
        [TransferToOtherBankAccountsHelper_ setShowEmail2:NO];
        [TransferToOtherBankAccountsHelper_ setSendEmail:NO];
        [TransferToOtherBankAccountsHelper_ setDestinationSMS1:@""];
        [TransferToOtherBankAccountsHelper_ setDestinationSMS2:@""];
        [TransferToOtherBankAccountsHelper_ setDestinationEmail1:@""];
        [TransferToOtherBankAccountsHelper_ setDestinationEmail2:@""];
        
        
        [transparentScroll_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f) animated:NO];
		
		shouldClearInterfaceData_ = NO;
        
	}
    
    if (editableViews_ == nil) {
        
        editableViews_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [editableViews_ removeAllObjects];
        
    }
    
    [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:originCombo_, 
                                         destinationAccountView_.entityTextField, 
                                         destinationAccountView_.officeTextField, 
                                         destinationAccountView_.accountTextField, 
                                         destinationAccountView_.CcTextField,
                                         currencycombo_, 
                                         amountTextField_,
                                         nil]];
    
    if ([TransferToOtherBankAccountsHelper_ canSendEmailandSMS]) {
        
        SMSCell *smsCell = [self smsCell];
        
        if ([TransferToOtherBankAccountsHelper_ sendSMS]) {
            
            if ([TransferToOtherBankAccountsHelper_ showSMS1]) {
                
                NXTTextField *textField = [smsCell firstTextField];
                
                if (textField != nil) {
                    
                    [editableViews_ addObject:textField];
                    
                }
                
                NXTComboButton *comboButton = [smsCell firstComboButton];
                
                if (comboButton != nil) {
                    
                    [editableViews_ addObject:comboButton];
                    
                }
                
            }
            
            if ([TransferToOtherBankAccountsHelper_ showSMS2]) {
                
                if ([TransferToOtherBankAccountsHelper_ showSMS1]) {
                    
                    NXTTextField *textField = [smsCell secondTextField];
                    
                    if (textField != nil) {
                        
                        [editableViews_ addObject:textField];
                        
                    }
                    
                    NXTComboButton *comboButton = [smsCell secondComboButton];
                    
                    if (comboButton != nil) {
                        
                        [editableViews_ addObject:comboButton];
                        
                    }
                    
                }
                
            }
            
        }
        
        if ([TransferToOtherBankAccountsHelper_ sendEmail]) {
            
            EmailCell *emailCell = [self emailCell];
            
            if ([TransferToOtherBankAccountsHelper_ showEmail1]) {
                
                NXTTextField *textField = [emailCell firstTextField];
                
                if (textField != nil) {
                    
                    [editableViews_ addObject:textField];
                    
                }
                
            }
            
            if ([TransferToOtherBankAccountsHelper_ showEmail2]) {
                
                NXTTextField *textField = [emailCell secondTextField];
                
                if (textField != nil) {
                    
                    [editableViews_ addObject:textField];
                    
                }
                
            }
            
            NXTTextView *textView = [emailCell emailTextView];
            
            if (textView != nil) {
                
                [editableViews_ addObject:textView];
                
            }
            
        }
        
    }

    [self displayStoredInformation];
    
    [destinationAccountView_.accountsButton setHidden:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the confirmation response and stores the information
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationTransferTypeCheckResponseReceived
                                               object:nil];

    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationTransferConfirmationResponseReceived 
                                                  object:nil];
    
    
    if(!isSelecting_)
    {
    [entityString_ release];
    entityString_ = nil;
    
    [officeString_ release];
    officeString_ = nil;
    
    [accountString_ release];
    accountString_ = nil;
    
    [ccString_ release];
    ccString_ = nil;
    
    [self okButtonClickedInPopButtonsView:popButtonsView_];
    
    [super viewWillDisappear:animated];
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased TransferToMyAccountsViewController constructed from a NIB file
 */
+ (TransferToOtherBankAccountViewController *)transferToOtherAccountViewController {
    
    return [[[TransferToOtherBankAccountViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
}

#pragma mark -
#pragma mark User iteraction


- (IBAction)originComboPressed {
    
    [self editableViewHasBeenClicked:originCombo_];
}


- (IBAction)currencyComboPressed {
    
    [self editableViewHasBeenClicked:currencycombo_];
}

#pragma mark -
#pragma mark TransfersStepOneViewController selectors

/**
 * Displays the stored information. Sets the information into the views
 */
- (void)displayStoredInformation {
    
    [super displayStoredInformation];
    
    if ([self isViewLoaded]) {
        
        NSMutableArray *array = [NSMutableArray array];
        NSArray *originAccountList = TransferToOtherBankAccountsHelper_.originAccountList;
        
        for(BankAccount *account in originAccountList) {
            
            [array addObject:account.accountIdAndDescription];
        }
        
        originCombo_.stringsList = [NSArray arrayWithArray:array];        
        [array removeAllObjects];
        
        
        NSArray *currencyList = TransferToOtherBankAccountsHelper_.currencyList;
        
        for(NSString *currency in currencyList) {        
            
            [array addObject:[NSLocalizedString(currency, nil) capitalizedString]];
        }
        
        currencycombo_.stringsList = [NSArray arrayWithArray:array];                
        
        [originCombo_ setSelectedIndex:TransferToOtherBankAccountsHelper_.selectedOriginAccountIndex];
        [currencycombo_ setSelectedIndex:TransferToOtherBankAccountsHelper_.selectedCurrencyIndex];
        
        [destinationAccountView_.entityTextField setText:TransferToOtherBankAccountsHelper_.selectedDestinationAccountEntity];
        [destinationAccountView_.officeTextField setText:TransferToOtherBankAccountsHelper_.selectedDestinationAccountOffice];
        [destinationAccountView_.accountTextField setText:TransferToOtherBankAccountsHelper_.selectedDestinationAccountAccNumber];
        [destinationAccountView_.CcTextField setText:TransferToOtherBankAccountsHelper_.selectedDestinationAccountCc];

        [amountTextField_ setText:TransferToOtherBankAccountsHelper_.amountString];
    }
}

/**
 * Lays out the views
 */
- (void)layoutViews {
    
    [super layoutViews];
    
    if ([self isViewLoaded]) {
        
        CGRect frame = originAccountLabel_.frame;
        frame.origin.y = VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        CGFloat height = [Tools labelHeight:originAccountLabel_ forText:originAccountLabel_.text];        
        frame.size.height = height;
        originAccountLabel_.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = originCombo_.frame;
        frame.origin.y = auxPos;
        originCombo_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = destinationAccountLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:destinationAccountLabel_ forText:destinationAccountLabel_.text];
        frame.size.height = height;
        destinationAccountLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = [destinationAccountView_ frame];
        frame.origin.y = auxPos;
        [destinationAccountView_ setFrame:frame];
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = currencyLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:currencyLabel_ forText:currencyLabel_.text];
        frame.size.height = height;
        currencyLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = currencycombo_.frame;
        frame.origin.y = auxPos;
        currencycombo_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = amountLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:amountLabel_ forText:amountLabel_.text];
        frame.size.height = height;
        amountLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = amountTextField_.frame;
        frame.origin.y = auxPos;
        amountTextField_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UITableView *selectionTableView = self.selectionTableView;
        [selectionTableView reloadData];
        CGSize contentSize = selectionTableView.contentSize;  
        frame = selectionTableView.frame;
        frame.origin.y = auxPos;
        height = contentSize.height;
        frame.size.height = height;
        selectionTableView.frame = frame;
        selectionTableView.scrollEnabled = NO;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UIButton *transferButton = self.transferButton;
        frame = transferButton.frame;
        frame.origin.y = auxPos;
        transferButton.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.size.height = auxPos;
        containerView.frame = frame;
        
        [self recalculateScroll];
    }
}

/**
 * Stores the information into the transfer operation helper instance
 */
- (void)storeInformationIntoHelper {
    
    [super storeInformationIntoHelper];
    
    [TransferToOtherBankAccountsHelper_ setSelectedDestinationAccountEntity:destinationAccountView_.entityTextField.text];
    [TransferToOtherBankAccountsHelper_ setSelectedDestinationAccountOffice:destinationAccountView_.officeTextField.text];
    [TransferToOtherBankAccountsHelper_ setSelectedDestinationAccountAccNumber:destinationAccountView_.accountTextField.text];
    [TransferToOtherBankAccountsHelper_ setSelectedDestinationAccountCc:destinationAccountView_.CcTextField.text];
    [TransferToOtherBankAccountsHelper_  setAmountString:amountTextField_.text];
    [TransferToOtherBankAccountsHelper_ setAmountTextField:amountTextField_];
}

/**
 * Returns the transfer operation helper associated to the view controller. Returns the transfer between accounts helper
 *
 * @return The transfer operation helper associated to the view controller
 */
- (TransferOperationHelper *)transferOperationHelper {
    
    return TransferToOtherBankAccountsHelper_;
    
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/** 
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
	return YES;
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self editableViewHasBeenClicked:textField];
        
           
    if( textField == destinationAccountView_.officeTextField) {
        
        if(officeString_ != nil) {
            
            [destinationAccountView_.officeTextField setText:officeString_]; 
        }
    }
    else if (textField == destinationAccountView_.entityTextField) {        
        
        if(entityString_ != nil) {
            
            [destinationAccountView_.entityTextField setText:entityString_];            
            
        }
                
    }
    else if (textField == destinationAccountView_.accountTextField) {
        
        if(accountString_ != nil) {
            
            [destinationAccountView_.accountTextField setText:accountString_]; 
        }
                
    }
    else if (textField == destinationAccountView_.CcTextField) {
        
        if(ccString_ != nil) {
            
            [destinationAccountView_.CcTextField setText:ccString_]; 
        }        
    }
}

/** 
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {            
    
    BOOL result = NO;
    
    result = [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    if(textField == destinationAccountView_.entityTextField) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
        
            if(resultLength <= MAX_OTHER_BANK_NUMBER_ENTITY) { 
            
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
            
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                    if (resultInteger >= 0) {
                    
                        result = YES;                                        
                    }                
                } 
            }
            else {
            
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
            
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                    if (resultInteger >= 0) {
                    
                        if([officeString_ length] == MAX_OTHER_BANK_NUMBER_OFFICE) {
                            
                            result = NO;
                        }
                        else {
                            result = YES;
                        }
                        [destinationAccountView_.officeTextField becomeFirstResponder];
                    }                
                }             
            }
        }
        
    }
    
    if(textField == destinationAccountView_.officeTextField) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
        
            if(resultLength <= MAX_OTHER_BANK_NUMBER_OFFICE) {            
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
            
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                    if (resultInteger >= 0) {
                    
                        result = YES;                                        
                    }                
                }
            } 
            else {
            
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
            
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                    if (resultInteger >= 0) {
                    
                        if([accountString_ length] == MAX_OTHER_BANK_NUMBER_ACCOUNT) {
                            result = NO;
                        }
                        else {
                            result = YES;
                        }
                        [destinationAccountView_.accountTextField becomeFirstResponder];
                    }                
                }            
            }
        }
    }
    else if (textField == destinationAccountView_.accountTextField) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
                
            if(resultLength <= MAX_OTHER_BANK_NUMBER_ACCOUNT) {                        
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
            
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                    if (resultInteger >= 0) {
                    
                        result = YES;                                        
                    }                
                }                 
            
            }
            else {
            
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
            
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                    if (resultInteger >= 0) {
                    
                        if([ccString_ length] == MAX_OTHER_BANK_NUMBER_CC) {
                            result = NO;
                        }
                        else {
                                result = YES;
                        }                        
                        [destinationAccountView_.CcTextField becomeFirstResponder];
                    }                
                }             
            }
        }                       
    }
    else if (textField == destinationAccountView_.CcTextField) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
        
            if(resultLength <= MAX_OTHER_BANK_NUMBER_CC) {
        
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
            
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                    if (resultInteger >= 0) {
                    
                        result = YES;                                        
                    }                
                } 
            }
            else {
                result = NO;
            }
        }
        
    }
    else if (textField == amountTextField_) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            NSScanner *scanner = [NSScanner scannerWithString:resultString];
            NSInteger resultInteger = 0;
            
            if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                if (resultInteger > 0) {
                    
                    result = YES;                                        
                }                
            }               
        }
    } 
    
    return result;    
}

/**
 * Asks the delegate if editing should stop in the specified text field. Forwards it to the external delegate
 *
 * @param textField The text field for which editing is about to end
 * @return YES if editing should stop; otherwise, NO if the editing session should continue
 */
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    BOOL result = YES;
    
    NSString *resultString = [NSString stringWithString:textField.text];
    
    if(textField == destinationAccountView_.entityTextField) {
        
        if (entityString_ != nil) {
            [entityString_ release];
            entityString_ = nil;
        }
        
        entityString_ = [resultString retain];      
        
    }
    else if(textField == destinationAccountView_.officeTextField) {
            
            if (officeString_ != nil) {
                [officeString_ release];
                officeString_ = nil;
            }
            
            officeString_ = [resultString retain];      
            
    }
    else if(textField == destinationAccountView_.accountTextField) {
        
        if (accountString_ != nil) {
            [accountString_ release];
            accountString_ = nil;
        }
        
        accountString_ = [resultString retain];      
        
    }
    else if(textField == destinationAccountView_.CcTextField) {
        
        if (ccString_ != nil) {
            [ccString_ release];
            ccString_ = nil;
        }
        
        ccString_ = [resultString retain];      
        
    }
    
    
    return result;
}

#pragma mark -
#pragma mark NXTComboButtonDelegate selectors

/*
 * Informs the delegate that a value has been selected
 */
- (void)NXTComboButtonValueSelected:(NXTComboButton *)comboButton {
    
    if (comboButton == originCombo_) {
        
        [TransferToOtherBankAccountsHelper_ setSelectedOriginAccountIndex:comboButton.selectedIndex];
    }
    else if (comboButton == currencycombo_) {
        
        [TransferToOtherBankAccountsHelper_ setSelectedCurrencyIndex:currencycombo_.selectedIndex];
        
        NSString *currency =[TransferToOtherBankAccountsHelper_ selectedCurrency];        
        [amountTextField_ setCurrencySymbol:[Tools getCurrencySimbol:currency]];
    }
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the transfer between accounts operation helper element
 *
 * @param transferBetweenAccountsHelper The new transfer between accounts operation helper element to set
 */
- (void)setTransferToOtherBankAccountsHelper:(TransferToOtherBankAccounts *)TransferToOtherBankAccountsHelper {
    
    if (TransferToOtherBankAccountsHelper != TransferToOtherBankAccountsHelper_) {
        
        [originCombo_ setSelectedIndex:-1];
        [currencycombo_ setSelectedIndex:-1];
        
        [TransferToOtherBankAccountsHelper retain];
        [TransferToOtherBankAccountsHelper_ release];
        TransferToOtherBankAccountsHelper_ = TransferToOtherBankAccountsHelper;
        
        [self displayStoredInformation];
        
    }
    
    [TransferToOtherBankAccountsHelper_ setSecondFactorKey:@""];
    
    [self resetScrollToTopLeftAnimated:NO];
    
}

/*
 * validate the doc number document
 *
 *@param the document type
 */
- (BOOL)validateTextFieldDocumentNumber:(NSString *)code textToValidate:(NSString *)text {
    
    BOOL valid = YES;
    
    NSInteger textLength = [text length];
    
    NSCharacterSet *numbersSet = [NSCharacterSet decimalDigitCharacterSet];
	NSCharacterSet *lowercaseLetterSet = [NSCharacterSet lowercaseLetterCharacterSet];
	NSCharacterSet *uppercaseLetterSet = [NSCharacterSet uppercaseLetterCharacterSet];
    
    if([code isEqualToString:DOC_TYPE_DNI]) {
        
        if(textLength <= 8 ) {                
                        
            unichar charExtracted;
            NSInteger size = [text length];
            for (NSInteger i = 0; (i < size) && (valid); i++) {
                charExtracted = [text characterAtIndex:i];
                valid = [numbersSet characterIsMember:charExtracted];
            }                       
        }
        else {
                        
            valid = NO;
        }
    }
    else if ([code isEqualToString:DOC_TYPE_RUC]) {
        
        if(textLength <= 11) {
            
            unichar charExtracted;
            NSInteger size = [text length];
            for (NSInteger i = 0; (i < size) && (valid); i++) {
                charExtracted = [text characterAtIndex:i];
                valid = [numbersSet characterIsMember:charExtracted];
            }
        }
        else {
            
            valid = NO;
        }
    }
    else if ([code isEqualToString:DOC_TYPE_MILITAR_ID]) {
        
        if(textLength <= 8) {
            
            unichar charExtracted;
            NSInteger size = [text length];
            for (NSInteger i = 0; (i < size) && (valid); i++) {
                charExtracted = [text characterAtIndex:i];
                valid = ([numbersSet characterIsMember:charExtracted] || 
                         [lowercaseLetterSet characterIsMember:charExtracted] || 
                         [uppercaseLetterSet characterIsMember:charExtracted]);
            }
        }
        else {
            
            valid = NO;
        }        
    }
    else if ([code isEqualToString:DOC_TYPE_PASAPORT]) {
        
        if(textLength <= 11) {
            
            unichar test1 = '-';
            unichar test2 = '/';
            unichar charExtracted;
            NSInteger size = [text length];
            for (NSInteger i = 0; (i < size) && (valid); i++) {
                charExtracted = [text characterAtIndex:i];
                valid = ([numbersSet characterIsMember:charExtracted] || 
                         ([lowercaseLetterSet characterIsMember:charExtracted]) || 
                         ([uppercaseLetterSet characterIsMember:charExtracted]) || 
                         (test1 == charExtracted) || (test2 == charExtracted));
            }
        }
        else {
            
            valid = NO;
        }         
    }
    else if ([code isEqualToString:DOC_TYPE_INMIGRATION_ID]) {
        
        if(textLength <= 11) {
            
            unichar test1 = '-';
            unichar test2 = '/';
            unichar charExtracted;
            NSInteger size = [text length];
            for (NSInteger i = 0; (i < size) && (valid); i++) {
                charExtracted = [text characterAtIndex:i];
                valid = ([numbersSet characterIsMember:charExtracted] || 
                         ([lowercaseLetterSet characterIsMember:charExtracted]) || 
                         ([uppercaseLetterSet characterIsMember:charExtracted]) || 
                         (test1 == charExtracted) || (test2 == charExtracted));
            }
        }
        else {
            
            valid = NO;
        } 
    }    
    return valid;
}

#pragma mark -
#pragma mark TransfersStepOneViewController override methods


/**
 * sms switch has been tapped.
 */
- (void)switchButtonHasBeenTapped:(BOOL)on {
    
    [super switchButtonHasBeenTapped:on];
    
    if (on) {
        
        NSUInteger position = [editableViews_ indexOfObject:self.emailCell.firstTextField];
        
        if (position == NSNotFound) {
            
            [editableViews_  addObject:self.smsCell.firstTextField];
            [editableViews_  addObject:self.smsCell.firstComboButton];
            
        } else {
            
            [editableViews_ insertObject:self.smsCell.firstTextField atIndex:position];
            [editableViews_ insertObject:self.smsCell.firstComboButton atIndex:position + 1];
            
        }
        
    } else {
        
        [editableViews_  removeObject:self.smsCell.firstTextField];
        [editableViews_  removeObject:self.smsCell.firstComboButton];
        
        NSUInteger position = [editableViews_ indexOfObject:self.smsCell.secondTextField];
        
        if (position != NSNotFound) {
            
            [editableViews_  removeObject:self.smsCell.secondTextField];
            [editableViews_  removeObject:self.smsCell.secondComboButton];
            
        }
        
    }
    
}

/**
 * More button has been tapped
 */
- (void)moreButtonHasBeenTapped {
    
    [super moreButtonHasBeenTapped];
    
    [self smsSecondTextFieldShow:YES];
}

/**
 * SMS Second text Field is shown.
 *
 * @param on The flag
 */
- (void)smsSecondTextFieldShow:(BOOL)on {
    
    if (on) {
        
        NSUInteger position = [editableViews_ indexOfObject:self.smsCell.firstComboButton];
        
        [editableViews_ insertObject:self.smsCell.secondTextField atIndex:position + 1];
        [editableViews_ insertObject:self.smsCell.secondComboButton atIndex:position + 2];
        
    } else {
        
        [editableViews_  removeObject:self.smsCell.secondTextField];
        [editableViews_  removeObject:self.smsCell.secondComboButton];
        
    }
    
}

/**
 * Switch has been tapped.
 *
 * @param on The flag
 */
- (void)emailSwitchButtonHasBeenTapped:(BOOL)on {
    
    [super emailSwitchButtonHasBeenTapped:on];
    
    if (on) {
        
        [editableViews_  addObject:self.emailCell.firstTextField];
        [editableViews_  addObject:self.emailCell.emailTextView];
        
    } else {
        
        [editableViews_  removeObject:self.emailCell.firstTextField];
        
        
        NSUInteger position = [editableViews_ indexOfObject:self.emailCell.secondTextField];
        
        if (position != NSNotFound) {
            
            [editableViews_  removeObject:self.emailCell.secondTextField];
            
        }
        
        [editableViews_  removeObject:self.emailCell.emailTextView];
        
    }
}

/**
 * More button has been tapped
 */
- (void)emailMoreButtonHasBeenTapped {
    
    [super emailMoreButtonHasBeenTapped];
    
    [self emailSecondTextFieldShow:YES];
}

/**
 * Email Second text Field is shown.
 *
 * @param on The flag
 */
- (void)emailSecondTextFieldShow:(BOOL)on {
    
    if (on) {
        
        NSUInteger position = [editableViews_ indexOfObject:self.emailCell.emailTextView];
        
        if (position != NSNotFound) {
            
            [editableViews_ insertObject:self.emailCell.secondTextField atIndex:position];
            
        }
        
    } else {
        
        [editableViews_  removeObject:self.emailCell.secondTextField];
        
    }
    
}

#pragma mark -
#pragma mark Notifications management

/*
 * Receives the response of the transfer confirmation
 */
- (void)transferResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
     // [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationTransferTypeCheckResponseReceived object:nil];
    
    StatusEnabledResponse *response = [notification object];
    
    if (!response.isError) {        
        
        
        if ([TransferToOtherBankAccountsHelper_ typeTransferStepReceived:response]) {
            
          
            
            if([[TransferToOtherBankAccountsHelper_ typeFlow] isEqualToString:@"L"]){
                
                [self displayTransferSecondStepView];

                
            }else{
                
                [self displayTypeTransferStepView];
                
            }

            
        } else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
            
        }
        
    }else
    {
        if([response.errorCode isEqualToString:THIRD_ACCOUNT_REGISTRATION_ERROR])
            [Tools showConfirmationDialogWithMessage:response.errorMessage andDelegate:self];
    }
    
}

/*
 * Receives the response of the transfer confirmation
 */
- (void)transferResponseToConfirmReceived:(NSNotification *)notification{
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (!response.isError) {
        
        
        if ([TransferToOtherBankAccountsHelper_ transferResponseReceived:response]) {
           
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationTransferConfirmationResponseReceived object:nil];
            
            [self displayTransferSecondStepView];
            
        }else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
            
        }
    }
}

#pragma mark -
#pragma mark - Implements alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        RegistrationTransferThirdCardViewController *registrationTransferThirdCardViewController = [[RegistrationTransferThirdCardViewController registrationTransferThirdCardViewController] retain];
        
        
        [registrationTransferThirdCardViewController setThirdAccountOperatorHelper:[[ThirdAccountOperator alloc] init] ];
        [registrationTransferThirdCardViewController setRegisterOtherBank:1];
        [self.navigationController pushViewController:registrationTransferThirdCardViewController animated:YES];
    }
}

-(IBAction)accountsTapped
{
    //aqui
    if(registeredAccountListViewController_ ==nil){
        registeredAccountListViewController_ =
    [[RegisteredAccountListViewController registeredAccountListViewController] retain];
    }
    
    registeredAccountListViewController_.delegate = self;
    [registeredAccountListViewController_ setOperationListType:OTHER_BANK_ACCOUNT_ID];
    isSelecting_ =YES;
    [self.navigationController pushViewController:registeredAccountListViewController_ animated:YES];
}

-(void) selectedAccountFromList:(ThirdAccount*)thirdAccount{
    
    NSArray *accountParts= [thirdAccount.accountNumber componentsSeparatedByString:@"-"];
    
    entityString_ = [[accountParts objectAtIndex:0] copyWithZone:self.zone];
    [destinationAccountView_.entityTextField setText:entityString_];
    
    officeString_ = [[accountParts objectAtIndex:1] copyWithZone:self.zone];
    [destinationAccountView_.officeTextField setText:officeString_];
    
    accountString_ = [[accountParts objectAtIndex:2] copyWithZone:self.zone];
    [destinationAccountView_.accountTextField  setText:accountString_];
    
    ccString_ = [[accountParts objectAtIndex:3] copyWithZone:self.zone];
    [destinationAccountView_.CcTextField setText:ccString_];
}


-(void)displayTypeTransferStepView{
    
    if(transferTINOnlineStepViewController_ == nil){
        transferTINOnlineStepViewController_ = nil;
        [transferTINOnlineStepViewController_ release];
    }
    
    transferTINOnlineStepViewController_ = [[TransferTINOnlineStepViewController transferTINOnlineStepViewController]retain];
    
     self.hasNavigateForward = YES;
    
    
    transferTINOnlineStepViewController_.TransferToOtherBankAccountsHelper = TransferToOtherBankAccountsHelper_;
    transferTINOnlineStepViewController_.disclaimer = TransferToOtherBankAccountsHelper_.disclaimer;
    [self.navigationController pushViewController:transferTINOnlineStepViewController_
                                         animated:YES];
}

@end
