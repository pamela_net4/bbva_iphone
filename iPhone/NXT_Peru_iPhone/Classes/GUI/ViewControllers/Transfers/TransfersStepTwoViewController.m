/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TransfersStepTwoViewController.h"

#import "Constants.h"
#import "CoordKeyCell.h"
#import "GlobalAdditionalInformation.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "LegalTermsTopCell.h"
#import "NXTEditableViewController+protected.h"
#import "NXTNavigationItem.h"
#import "NXTTableCell.h"
#import "NXTTextField.h"
#import "NXTTransfersViewController+protected.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "OpKeyCell.h"
#import "SealCell.h"
#import "Session.h"
#import "SimpleHeaderView.h"
#import "StatusEnabledResponse.h"
#import "StringKeys.h"
#import "TermsAndConsiderationsViewController.h"
#import "TextCell.h"
#import "Tools.h"
#import "TransferConfirmationAdditionalInformation.h"
#import "TransferDetailCell.h"
#import "TransferOperationHelper.h"
#import "TransferSummaryCell.h"
#import "TransfersStepThreeViewController.h"
#import "UIColor+BBVA_Colors.h"
#import "ThirdAccountOperator.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"TransfersStepTwoViewController"

#pragma mark -

/**
 * TransfersStepTwoViewController private extension
 */
@interface TransfersStepTwoViewController()

/**
 * Releases graphic elements
 *
 * @private
 */
- (void)releaseTransfersStepTwoViewControllerGraphicElements;

/**
 * Displays the transfer third step view
 *
 * @private
 */
- (void)displayTransferThirdStepView;

/**
 * Configures the second factor cell. It shows the OTP key or Coordinate key, depending on the otp service usage
 *
 * @return The cell configured
 * @private
 */
- (UITableViewCell *)configureSecondFactorCell;

/**
 * Returns the title for second factor section, depending on the otp service usage
 *
 * @return The section title
 * @private
 */
- (NSString *)secondFactorTitleSection;

/**
 * Returns the second factor key
 *
 * @return The second factor key
 * @private
 */
- (NSString *)secondFactorKey;

/**
 * Invoked by framework when the response to the transfer operation is received. The information is analized
 *
 * @param notification The NSNotification instance containing the notification information
 * @private
 */
- (void)confirmationResponseReceived:(NSNotification *)notification;

/**
 * Normalizes the table seciton depending on whether the coordinate key section must be displayed or not
 *
 * @param section The section to normalized
 * @return The normalized section
 */
- (NSUInteger)normalizeSection:(NSUInteger)section;

/**
 * Returns an ITF Cell
 *
 * @return The normalized section
 */
- (TextCell *)itfCell;


@end

#pragma mark -

@implementation TransfersStepTwoViewController


#pragma mark -
#pragma mark Properties

@synthesize acceptButton = acceptButton_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationTransferSuccessResponseReceived 
                                                  object:nil];
    
    [transfersStepThreeViewController_ release];
    transfersStepThreeViewController_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransfersStepTwoViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransfersStepTwoViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/**
 * Releases graphic elements
 */
- (void)releaseTransfersStepTwoViewControllerGraphicElements {
    
    [acceptButton_ release];
    acceptButton_ = nil;
    
    [coordKeyCell_ release];
    coordKeyCell_ = nil;
    
    [otpKeyCell_ release];
    otpKeyCell_ = nil;
    
    [legalTermsTopCell_ release];
    legalTermsTopCell_ = nil;
    
    [editableViews_ removeAllObjects];
    
}

#pragma mark -
#pragma mark View managment

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
        
    self.informationTableView.backgroundColor = [UIColor clearColor];
    self.informationTableView.scrollsToTop = YES;
	[NXT_Peru_iPhoneStyler styleBlueButton:acceptButton_];
    [acceptButton_ setTitle:NSLocalizedString(CONFIRM_TEXT_KEY, nil) forState:UIControlStateNormal];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(confirmationResponseReceived:)
                                                 name:kNotificationTransferSuccessResponseReceived
                                               object:nil];
    
    
    [[otpKeyCell_ opKeyTextField] setText:@""];
    [[coordKeyCell_ keyTextField] setText:@""];
    
    if(legalTermsTopCell_ != nil) {
        
        [[legalTermsTopCell_ legalTermsSwitch] setOn:NO];
    
    }
    
    
    
    [self.informationTableView reloadData];
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    

    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the 
 * transfer last step confirmation notification
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationTransferSuccessResponseReceived 
                                                  object:nil];        
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([[self brandingImageView] frame]) - CGRectGetHeight([[self operationTypeHeader] frame]);
    [self setScrollNominalFrame:scrollFrame];
    
}

/*
 * Lays out the views
 */
- (void)layoutViews {
    
    UITableView *tableView = self.informationTableView;
    
    [tableView reloadData];
    CGFloat tableHeight = tableView.contentSize.height;
    CGRect tableFrame = tableView.frame;
    tableFrame.size.height = tableHeight;
    tableView.frame = tableFrame;
    
    acceptButton_.frame = CGRectMake(acceptButton_.frame.origin.x, tableHeight + 10.0f, acceptButton_.frame.size.width, acceptButton_.frame.size.height);
    
    self.containerView.frame = CGRectMake(self.containerView.frame.origin.x, self.containerView.frame.origin.y, self.containerView.frame.size.width, tableHeight + 10.0f + acceptButton_.frame.size.height + 10.0f);
    
    [self recalculateScroll];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initial configuration is set.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
}

/*
 * Creates and returns an autoreleased TransferToAnotherAccountViewController constructed from a NIB file
 */
+ (TransfersStepTwoViewController *)transfersStepTwoViewController {
    
    TransfersStepTwoViewController *result = [[[TransfersStepTwoViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}


#pragma mark -
#pragma mark Displaying views

/*
 * Displays the transfer third step view
 */
- (void)displayTransferThirdStepView {
    
    if (transfersStepThreeViewController_ == nil) {
        
        transfersStepThreeViewController_ = [[TransfersStepThreeViewController transfersStepThreeViewController] retain];
        
    }
    transfersStepThreeViewController_.isCashMobileSend = self.isCashMobileSend;
    transfersStepThreeViewController_.transferOperationHelper = self.transferOperationHelper;
    transfersStepThreeViewController_.isFoFinished = FALSE;
    [self.navigationController pushViewController:transfersStepThreeViewController_ animated:YES];
    
}


#pragma mark -
#pragma mark UITableViewDatasource protocol selectors

/**	
 * Asks the data source to return the number of sections in the table view. When no coordinate key is needed, 2 is returned, when
 * it is needed to display the coordinate key, 3 is returned
 *
 * @param tableView An object representing the table view requesting this information
 * @return The number of sections in tableView
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
    NSInteger result = 1;
    
    TransferOperationHelper *transferOperationHelper = self.transferOperationHelper;
    
    if (transferOperationHelper.canShowLegalTerms) {
        
        result ++;
        
    }
    
    if (![transferOperationHelper.sealText isEqualToString:@""]) {
    
        result ++;
        
    }
    
    if ([transferOperationHelper transferOperationType] != TTETransferBetweenUserAccounts && ![transferOperationHelper canOmitOTP]) {
        
        result += 1;
        
    }
    
    return result;
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view. Each section has a calculated number of rows
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section in tableView
 * @return The number of rows in section
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger result = 1;
    
    NSUInteger normalizedSection = [self normalizeSection:section];
    
    if (normalizedSection == 0) {
        
        result = [self.transferOperationHelper.transferSecondStepInformation count];

    }
    
    return result;
    
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. Returns the cell that fit the index path provided
 *
 * @param tableView The table-view object requesting this information
 * @param indexPath An index number identifying a section in tableView
 * @return The number of rows in section
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *result = nil;
            
    NSInteger normalizedSection = [self normalizeSection:indexPath.section];

    TransferOperationHelper *transferOperationHelper = self.transferOperationHelper;
    
    if (normalizedSection == 0) {
        
        result = [self transferDetailCellAtIndex:indexPath.row
                                    forTableView:tableView];
        
    } else if (normalizedSection == 1) {
        
        LegalTermsTopCell *legalTermsTopCell = (LegalTermsTopCell *)[tableView dequeueReusableCellWithIdentifier:[LegalTermsTopCell cellIdentifier]];
        
        if (legalTermsTopCell == nil) {
            
            if (legalTermsTopCell_ == nil) {
                legalTermsTopCell_ = [[LegalTermsTopCell legalTermsTopCell] retain];
            }
            
            legalTermsTopCell_.delegate = self;
            
            legalTermsTopCell = legalTermsTopCell_;
        }
        
        result = legalTermsTopCell;
        
    } else if (normalizedSection == 2) {
        
        result = [self configureSecondFactorCell];
                        
    } else if (normalizedSection == 3) {
        
        SealCell *sealCell = (SealCell *)[tableView dequeueReusableCellWithIdentifier:[SealCell cellIdentifier]];
        
        if (sealCell == nil) {
            sealCell = [SealCell sealCell];
        }
        
        sealCell.selectionStyle = UITableViewCellSelectionStyleNone;
        sealCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSData *data = [Tools base64DataFromString:transferOperationHelper.sealText];
        UIImage *image = [[[UIImage alloc] initWithData:data] autorelease];
        [[sealCell sealImageView] setImage:image];
        result = sealCell;
        
        result = sealCell;
        
    }
    
    result.backgroundColor = [UIColor clearColor];
    
    return result;
    
}

#pragma mark -
#pragma mark UITableViewDelegate selectors

/**
 * Asks the delegate for the height to use for the footer of a particular section.
 *
 * @param tableView The table-view object requesting this information.
 * @param section An index number identifying a section of tableView .
 */
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {

    CGFloat result = 0.0f;

    if (section == 0) {
    
        CGFloat margin = 5.0f;
        CGFloat labelHeight = 30.0f;
        
        TransferOperationHelper *transferOperationHelper = self.transferOperationHelper;
        
        if (transferOperationHelper.canShowITF) {
            
            result = result + margin + labelHeight;
            
        }
        
        result = result + margin;
        
    }

    return result;
    
}

/**
 * Asks the delegate for a view object to display in the footer of the specified section of the table view.
 */
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 0.0f)] autorelease];
    view.backgroundColor = [UIColor whiteColor];
    
    if (section == 0 && self.transferOperationHelper.canShowITF) {
        
        CGFloat yPosition;
        CGFloat margin = 5.0f;
        CGFloat labelHeight = 30.0f;
        
        yPosition = margin;    
        UILabel *itfLabel = [[[UILabel alloc] initWithFrame:CGRectMake(5.0f, yPosition, 300.0f, labelHeight)] autorelease];        
        yPosition = yPosition + labelHeight + margin;
        
        [NXT_Peru_iPhoneStyler styleLabel:itfLabel withFontSize:11.0f color:[UIColor grayColor]];
        itfLabel.numberOfLines = 2;
        itfLabel.textAlignment = UITextAlignmentCenter;
        itfLabel.backgroundColor = [UIColor clearColor];
        
        if (self.transferOperationHelper.itfSelected) {
            itfLabel.text = NSLocalizedString(TRANSFER_STEP_ITF_OWN_TEXT_KEY, nil);
        } else {
            itfLabel.text = NSLocalizedString(TRANSFER_STEP_ITF_NOT_OWN_TEXT_KEY, nil);
        }
        if([self.transferOperationHelper itfMessageConfirm]){
            itfLabel.text = [self.transferOperationHelper itfMessageConfirm];
        }
        
        UIImageView *separator = [[[UIImageView alloc] initWithFrame:CGRectMake(0.0f, yPosition - 2.0f, 320.0f, 2.0f)] autorelease];
        separator.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
        
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, yPosition);
        [view addSubview:itfLabel];
        [view addSubview:separator];

    }
    
    return view;
    
}


#pragma mark -
#pragma mark UITableViewDelegate protocol selectors

/**
 * Tells the delegate that the specified row is now selected
 *
 * @param tableView A table-view object informing the delegate about the new row selection
 * @param indexPath An index path locating the new selected row in tableView
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 *
 * @param tableView The table-view object requesting this information
 * @param indexPath An index path that locates a row in tableView
 * @return A floating-point value that specifies the height (in points) that row should be
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat result = 0.0f;
        
    NSUInteger normalizedSection = [self normalizeSection:indexPath.section];
    
    if (normalizedSection == 0) {
        
        result = [self transferDetailCellHeightAtIndex:indexPath.row];
        
    } else if (normalizedSection == 1) {
                    
        result = [LegalTermsTopCell cellHeight];
        
    } else if (normalizedSection == 2) {
        
        OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
        
        if (otpUsage == otp_UsageOTP) {
        
            result = [OpKeyCell cellHeight];
            
        } else if (otpUsage == otp_UsageTC) {
        
            result = [CoordKeyCell cellHeight];
            
        }
        
    } else if (normalizedSection == 3) {
        
        result = [SealCell cellHeight];
        
    }
        
    return result;
    
}

/**
 * Asks the delegate for the height to use for the header of a particular section.
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section of tableView
 * @return A floating-point value that specifies the height (in points) of the header for section
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
   
    CGFloat result = 0.0f;

    
    
    if (section != 0) {
		
        result = [SimpleHeaderView height];
		
    } else {
	
		result = 30.0f;
       
        if([self.transferOperationHelper isKindOfClass:[ThirdAccountOperator class]]){
            return 0.0f;
        }
	}
	
    return result;
        
}

/**
 * Asks the delegate for a view object to display in the header of the specified section of the table view. The appropriate section
 * header is displayed
 *
 * @param tableView The table-view object asking for the view object
 * @param section An index number identifying a section of tableView
 * @return A view object to be displayed in the header of section 
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *result = nil;
    
    NSUInteger normalizedSection = [self normalizeSection:section];
    
    if (normalizedSection == 0) {
		
		UIView *header = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth([tableView frame]), 30.0f)] autorelease];
		[header setBackgroundColor:[UIColor whiteColor]];
		
		UILabel *title = [[[UILabel alloc] initWithFrame:CGRectMake(5.0, 0.0f, CGRectGetWidth([tableView frame]) - 10.0f, 30.0f)] autorelease];
		[NXT_Peru_iPhoneStyler styleLabel:title withBoldFontSize:13.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
		
		switch ([[self transferOperationHelper] transferOperationType]) {
				
			case TTETransferBetweenUserAccounts:
				
				[title setText:NSLocalizedString(TRANFER_MESSAGE_HEADER_BETWEEN_ACCOUNTS_TEXT_KEY, nil)];
				
				break;
				
			case TTETransferToOtherUserAccount:
				
				[title setText:NSLocalizedString(TRANSFER_MESSAGE_HEADER_TO_THIRD_ACCOUNTS_TEXT_KEY, nil)];
				
				break;
				
			case TTETransferToOtherBankAccount:
				
				[title setText:NSLocalizedString(TRANSFER_MESSAGE_HEADER_TO_ANOTHER_BANK_TEXT_KEY, nil)];
				
				break;
                
            case TTETransferWithCashMobile:
                [title setText:@""];
                
                break;
            case TTETransferToGiftCard:
                [title setText:NSLocalizedString(TRANSFER_MESSAGE_HEADER_TO_GIFT_CARD_TEXT_KEY, nil)];
                
                break;
            case TTETransferConsultCashMobile:
                //to avoid the warning
                break;
            case TTEThirdTransferAccount:
                break;
				
		}
		
		[header addSubview:title];
		result = header;
        
    } else if (normalizedSection == 1) {
        
        SimpleHeaderView *simpleHeaderView = [SimpleHeaderView simpleHeaderView];
        [simpleHeaderView setTitle:NSLocalizedString(TRANSFER_STEP_TWO_LEGAL_TERMS_TEXT_KEY, nil)];
        result = simpleHeaderView;
 
    } else if (normalizedSection == 2) {
                
        SimpleHeaderView *simpleHeaderView = [SimpleHeaderView simpleHeaderView];
        [simpleHeaderView setTitle:[self secondFactorTitleSection]];
        result = simpleHeaderView;

    } else if (normalizedSection == 3) {
        
        SimpleHeaderView *simpleHeaderView = [SimpleHeaderView simpleHeaderView];
        [simpleHeaderView setTitle:NSLocalizedString(TRANSFER_STEP_TWO_SEAL_TEXT_KEY, nil)];
        result = simpleHeaderView;

    }
    
    return result;
    
}

#pragma mark -
#pragma mark Cells management

/*
 * Returns the title for second factor section, depending on the otp service usage
 */
- (NSString *)secondFactorTitleSection {
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    NSString *title = @"";
    
    if (otpUsage == otp_UsageTC) {
        
        title = NSLocalizedString(TRANSFER_STEP_TWO_COORD_KEY_TEXT_KEY, nil);
        
    } else if (otpUsage == otp_UsageOTP) {
        
        title = NSLocalizedString(TRANSFER_STEP_TWO_OTP_KEY_TEXT_KEY, nil);
        
    }
    
    return title;
    
}

/*
 * Configures the second factor cell. It shows the OTP key or Coordinate key, depending on the otp service usage
 */
- (UITableViewCell *)configureSecondFactorCell {
    
    UITableViewCell *cell = nil;
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
    if ((otpUsage == otp_UsageOTP) || (otpUsage == otp_UsageUnknown)) {
        
        OpKeyCell *otpKeyCell = (OpKeyCell *)[[self informationTableView] dequeueReusableCellWithIdentifier:[OpKeyCell cellIdentifier]];
        
        if (otpKeyCell == nil) {
            
            if (otpKeyCell_ == nil) {
                
                otpKeyCell_ = [[OpKeyCell opKeyCell] retain];
                
            }
            
            [otpKeyCell_ setSelectionStyle:UITableViewCellSelectionStyleNone];
            [[otpKeyCell_ opKeyTextField] setDelegate:self];
            
            UIView *popButtonsView = [self popButtonsView];
            [[otpKeyCell_ opKeyTextField] setInputAccessoryView:popButtonsView];
            
            [editableViews_ addObject:[otpKeyCell_ opKeyTextField]];
            
            otpKeyCell = otpKeyCell_;
            
        }
        
        cell = otpKeyCell;
        
        
    } else if (otpUsage == otp_UsageTC) {
        
        CoordKeyCell *coordKeyCell = (CoordKeyCell *)[[self informationTableView] dequeueReusableCellWithIdentifier:[CoordKeyCell cellIdentifier]];
        
        if (coordKeyCell == nil) {
            
            if (coordKeyCell_ == nil) {
                
                coordKeyCell_ = [[CoordKeyCell coordKeyCell] retain];
                
            }
            
            coordKeyCell = coordKeyCell_;
            [editableViews_ addObject:[coordKeyCell keyTextField]];
            
        }
        
        [coordKeyCell setCoordinate:[[self transferOperationHelper] coordinateKeyTitle]];
        [[coordKeyCell keyTextField] setDelegate:self];
        
        UIView *popButtonsView = [self popButtonsView];
        [[coordKeyCell keyTextField] setInputAccessoryView:popButtonsView];
        
        [coordKeyCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        cell = coordKeyCell;
        
    }
    
    return cell;
    
}

/*
 * Returns a table cell that represents the operation step information stored at teh given index. The cell is a TransferDetailCell instance
 */
- (TransferDetailCell *)transferDetailCellAtIndex:(NSInteger)index
                                     forTableView:(UITableView *)tableView {
    
    NSString *cellId = [TransferDetailCell cellIdentifier];
    
    TransferDetailCell *result = (TransferDetailCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (result == nil) {
        
        result = [TransferDetailCell transferDetailCell];
    }
    
    if([self.transferOperationHelper isKindOfClass:[ThirdAccountOperator class]])
    {
    [result.contentView setBackgroundColor:[UIColor BBVAGreyToneFiveColor]];
    [result setBackgroundColor:[UIColor BBVAGreyToneFiveColor]];
    
    }

    TitleAndAttributes *titleAndAttributes = nil;
    
    NSArray *informationArray = self.transferOperationHelper.transferSecondStepInformation;
    
    NSInteger informationArrayCount = [informationArray count];
    
    if ((index >= 0) && (index < informationArrayCount)) {
        
        titleAndAttributes = [informationArray objectAtIndex:index];
        
    }
    
    [result displayTitleAndAttributes:titleAndAttributes];
    
    if (index == (informationArrayCount - 1)) {
    
        if (self.transferOperationHelper.canShowITF) {
            result.displaySeparatorImage = NO;
        } else {
            result.displaySeparatorImage = YES;
        }

    } else {
    
        result.displaySeparatorImage = NO;

    }
    
    return result;
    
}

/*
 * Returns the table cell height for the cell that represents the operation step information stored at teh given index.
 * The cell is a TransferDetailCell instance
 */
- (CGFloat)transferDetailCellHeightAtIndex:(NSInteger)index {
    
    CGFloat result = 0.0f;
    
    TitleAndAttributes *titleAndAttributes = nil;
    
    NSArray *informationArray = self.transferOperationHelper.transferSecondStepInformation;
    NSInteger informationArrayCount = [informationArray count];
    
    if (index < informationArrayCount) {
        
        titleAndAttributes = [informationArray objectAtIndex:index];
        
    }
    
    result = [TransferDetailCell cellHeightForTitleAndAttributes:titleAndAttributes];
    
    return result;
    
}

/**
 * Returns an ITF Cell
 *
 * @return The normalized section
 */
- (TextCell *)itfCell {

    NSString *cellId = [TextCell cellIdentifier];
    
    TextCell *result = (TextCell *)[self.informationTableView dequeueReusableCellWithIdentifier:cellId];
    
    if (result == nil) {
        
        result = [TextCell textCell];
        
    }
    
    if (self.transferOperationHelper.itfSelected) {
        result.text = NSLocalizedString(TRANSFER_STEP_ITF_OWN_TEXT_KEY, nil);
    } else {
        result.text = NSLocalizedString(TRANSFER_STEP_ITF_NOT_OWN_TEXT_KEY, nil);
    }
    
    return result;

}

#pragma mark -
#pragma mark Utility selectors

/*
 * Normalizes the table seciton depending on whether the coordinate key section must be displayed or not
 */
- (NSUInteger)normalizeSection:(NSUInteger)section {
    
    NSUInteger result = section;
    
    TransferOperationHelper *transferOperationHelper = self.transferOperationHelper;
     
    if (!(transferOperationHelper.canShowLegalTerms) && (result > 0)) {
        
        result ++;
        
    }
    
    BOOL omitOTP = NO;
    if( [transferOperationHelper transferOperationType] == TTETransferToOtherUserAccount || [transferOperationHelper transferOperationType] == TTETransferToOtherBankAccount){
        omitOTP = [transferOperationHelper canOmitOTP];
    }
    
    if (([transferOperationHelper transferOperationType] == TTETransferBetweenUserAccounts || omitOTP) && (result > 1)) {
        
        result ++;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark User Interaction

/*
 * Performs accept buttom action
 */
- (IBAction)acceptButtonTapped {

    TransferOperationHelper *transferOperationHelper = self.transferOperationHelper;
   
    [transferOperationHelper setLegalTermsAccepted:[[legalTermsTopCell_ legalTermsSwitch] isOn]];
    [transferOperationHelper setSecondFactorKey:[self secondFactorKey]];

    if ([transferOperationHelper startTransferConfirmationRequest]) {
        
        [self.appDelegate showActivityIndicator:poai_Both];
        
    }     
    
}

/*
 * Returns the second factor key
 */
- (NSString *)secondFactorKey {
    
    NSString *secondFactor = @"";
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
    if (otpUsage == otp_UsageOTP) {
        
        secondFactor = [[otpKeyCell_ opKeyTextField] text];
        
    } else if (otpUsage == otp_UsageTC) {
        
        secondFactor = [[coordKeyCell_ keyTextField] text];
        
    }
    
    return secondFactor;
    
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/** 
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
    [self makeViewVisible:textField];
    
	return YES;
    
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self editableViewHasBeenClicked:textField];
    
}

/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    if (textField == [coordKeyCell_ keyTextField]) {
        
        if (resultLength <= COORDINATES_MAXIMUM_LENGHT) {
            
            result = YES;
            
        }
        
    } else if (textField == [otpKeyCell_ opKeyTextField]) {
        
        if(resultLength <= OTP_MAXIMUM_LENGHT) {
            
            if ([Tools isValidText:resultString forCharacterString:OTP_VALID_CHARACTERS]) {
                
                result = YES;
            }
        }
   
    } else {
        
        result = YES;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark UITextViewDelegate protocol selectors

/** 
 * Asks the delegate if editing should begin in the specified text view.
 *
 * @param textView The text view for which editing is about to begin.
 */
- (BOOL)textViewShouldBeginEditing:(UITextView*)textView {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
	return YES;
    
}

/**
 * Tells the delegate that editing began for the specified text view.
 *
 * @param textView The text view for which an editing session began.
 */
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    [self editableViewHasBeenClicked:textView];
    
}

#pragma mark -
#pragma mark LegalTermsTopCell Delegate

/**
 * This method is called when the switch has changed its state.
 */
- (void)warningButtonHasBeenTapped {

    if (termsAndConsiderationsViewController_ == nil) {
            
        termsAndConsiderationsViewController_ = [[TermsAndConsiderationsViewController TermsAndConsiderationsViewController] retain];
            
    }
        
    TransferOperationHelper *transferOperationHelper = self.transferOperationHelper;
    [termsAndConsiderationsViewController_ setURLText:self.transferOperationHelper.legalTermsURL 
                                               headerText:transferOperationHelper.localizedTransferOperationTypeString];
        
    [self.navigationController pushViewController:termsAndConsiderationsViewController_ animated:YES];        
            

}


#pragma mark -
#pragma mark Notifications management

/*
 * Invoked by framework when the response to the transfer operation is received. The information is analized
 */
- (void)confirmationResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (![response isError]) {
        
        if ([self.transferOperationHelper confirmationResponseReceived:response]) {
            
            [[NSNotificationCenter defaultCenter] removeObserver:self
                                                            name:kNotificationTransferSuccessResponseReceived
                                                          object:nil];
            
            [self displayTransferThirdStepView];
            
        } else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
            
        }
        
    } else {
        
        if ([response errorMessage] == nil || [@"" isEqualToString:[response errorMessage]]) {
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
        }else if ([self.transferOperationHelper transferOperationType] == TTETransferBetweenUserAccounts ||
                  [self.transferOperationHelper transferOperationType] == TTETransferToOtherBankAccount ||
                  [self.transferOperationHelper transferOperationType] == TTETransferToOtherUserAccount){
            [Tools showErrorWithMessage:[response errorMessage]];
        }
        
    }
}

#pragma mark -
#pragma mark UIAlertViewDelegate methods

/**
 * Sent to the delegate when the user clicks a button on an alert view.
 *
 * @param alertView: The alert view containing the button.
 * @param buttonIndex: The index of the button that was clicked.
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    [[self navigationController] popViewControllerAnimated:YES];
    
}


#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    
    if (self.isCashMobileSend) {
        result.customTitleView.topLabelText = NSLocalizedString(TRANSFER_WITH_CASH_MOBILE_TEXT_1_KEY, nil);
    }else if([self transferOperationHelper].transferOperationType == TTEThirdTransferAccount){
        result.customTitleView.topLabelText = NSLocalizedString(@"Inscripción de Cuentas de Terceros", nil);
    }else
        result.customTitleView.topLabelText = NSLocalizedString(TRANSFER_TITLE_TEXT_KEY, nil);
    
    return result;
    
}

@end
