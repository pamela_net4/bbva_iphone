/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTViewController.h"


//Forward declarations
@class SimpleHeaderView;


/**
 * View to transfer money from an account of the user to an account of the user
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TermsAndConsiderationsViewController : NXTViewController<UIWebViewDelegate> {
    
@private
    
    /**
     * Operation type header
     */
    SimpleHeaderView *operationTypeHeader_;
    
	/**
     * Web view
     */
    UIWebView *webView_;
    
    /**
     * Branding image view
     */
    UIImageView *brandingImageView_;
    
    /**
     * URL
     */
    NSString *url_;
    
    /**
     * title
     */
    NSString *title_;
    
    /**
     * title
     */
    NSString *headerTitle_;
}

/**
 * Provides readwrite access to the titleLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIWebView *webView;

/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;

/**
 * Creates and returns an autoreleased TermsAndConsiderationsViewController constructed from a NIB file
 *
 * @return The autoreleased TermsAndConsiderationsViewController constructed from a NIB file
 */
+ (TermsAndConsiderationsViewController *)TermsAndConsiderationsViewController;

/**
 * Sets the title text and the terms text
 */
- (void)setURLText:(NSString *)urlText 
        headerText:(NSString *)headerText;

- (void)setURLText:(NSString *)urlText
         titleText:(NSString*)titleText
        headerText:(NSString *)headerText;

@end
