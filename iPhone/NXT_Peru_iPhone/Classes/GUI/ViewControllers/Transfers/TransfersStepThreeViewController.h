/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTTransfersViewController.h"
#import "FrequentOperationAlterStepOneViewControllerViewController.h"
#import "FOInscriptionBaseProcess.h"

/**
 * View to transfer money from an account of the user to an account of the user
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransfersStepThreeViewController : NXTTransfersViewController <UITableViewDelegate, UITableViewDataSource, FOInscriptionBaseProcessDelegate> {
    
@private
    
	/**
     * Global position button
     */
    UIButton *gpButton_;
	
    /**
     * Transfers button
     */
    UIButton *transfersButton_;
    
    /**
     * Resend information Button
     */
    UIButton *resendButton_;
    
    /**
     * Frecuenty Operation Button
     */
    UIButton *foButton_;
    
    /**
     * Process for the operation
     */
    FOInscriptionBaseProcess *foInscriptionProcess_;
    
    /**
     * flag to indicate if the fo has been finished
     */
    BOOL isFoFinished_;
    
    /**
     * Modal view controller for the Frequent Operation Reactive
     */
    FrequentOperationAlterStepOneViewControllerViewController *frequentOperationReactiveStepOneViewController_;
}
/**
 * flag to indicate if the fo has been finished
 */
@property (nonatomic, readwrite, assign) BOOL isFoFinished;

@property (nonatomic, readwrite, retain) FOInscriptionBaseProcess *foInscriptionProcess;

/**
 * Provides readwrite access to the gpButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *gpButton;

/**
 * Provides readwrite access to the foButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *foButton;

/**
 * Provides readwrite access to the transfersButton. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *transfersButton;

/**
 * Provides readwrite access to the Resend. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *resendButton;

/**
 * Boolean to know if is the detail view
 */
@property (nonatomic) BOOL isDetail;

/**
 * Boolean to know if is the resend button should be display
 */
@property (nonatomic) BOOL canResend;

/**
 * Creates and returns an autoreleased TransfersStepThreeViewController constructed from a NIB file
 *
 * @return The autoreleased TransfersStepThreeViewController constructed from a NIB file
 */
+ (TransfersStepThreeViewController *)transfersStepThreeViewController;


/**
 * The acceptButton has been pressed
 */
- (IBAction)gpButtonPressed;

/**
 * The saveButton has been pressed
 */
- (IBAction)transfersButtonPressed;

/**
 * The ofButton has been pressed
 */

- (IBAction)foButtonPressed;

/**
 * The resendButton has been pressed
 */

- (IBAction)resendButtonPressed;

@end
