/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TransferBetweenAccountsViewController.h"
#import "TransfersStepOneViewController+protected.h"
#import "NXTEditableViewController+protected.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "StatusEnabledResponse.h"
#import "StringKeys.h"
#import "Tools.h"
#import "NXTCurrencyTextField.h"
#import "NXTTextField.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "BankAccount.h"
#import "TransferBetweenAccounts.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"TransferBetweenAccountsViewController"

/**
 * Defines de max text lenght for the reference
 */
#define MAX_TEXT_REFERENCE_LENGHT               20


#pragma mark -


/**
 * TransferToMyAccountsViewController private extension
 */
@interface TransferBetweenAccountsViewController()

/**
 * Releases Graphic Elements
 *
 * @private
 */
- (void)releaseTransferToMyAccountsViewControllerGraphicElements;

/**
 * Receives the response of the transfer confirmation
 *
 * @param notification The NSNotification object containing the notification information
 * @private
 */
- (void)transferResponseReceived:(NSNotification *)notification;

@end

#pragma mark -

@implementation TransferBetweenAccountsViewController

#pragma mark -
#pragma mark Properties

@synthesize originAccountLabel = originAccountLabel_;
@synthesize originCombo = originCombo_;
@synthesize destinationAccountLabel = destinationAccountLabel_;
@synthesize destinationCombo = destinationCombo_;
@synthesize currencyLabel = currencyLabel_;
@synthesize currencyCombo = currencyCombo_;
@synthesize amountLabel = amountLabel_;
@synthesize amountTextField = amountTextField_;
@synthesize referenceLabel = referenceLabel_;
@synthesize refenceTextField = referenceTextField_;
@synthesize transferBetweenAccountsHelper = transferBetweenAccountsHelper_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransferToMyAccountsViewControllerGraphicElements];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationTransferConfirmationResponseReceived 
                                                  object:nil];
    
    
    [transferBetweenAccountsHelper_ release];
    transferBetweenAccountsHelper_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransferToMyAccountsViewControllerGraphicElements];
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransferToMyAccountsViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/* 
 * Releases graphic elements
 */
- (void)releaseTransferToMyAccountsViewControllerGraphicElements{
    
	[originAccountLabel_ release];
	originAccountLabel_ = nil;
	
	[originCombo_ release];
	originCombo_ = nil;
	
	[destinationAccountLabel_ release];
	destinationAccountLabel_ = nil;
	
	[destinationCombo_ release];
	destinationCombo_ = nil;
    
    [currencyLabel_ release];
    currencyLabel_ = nil;
    
    [currencyCombo_ release];
    currencyCombo_ = nil;
    
    [amountLabel_ release];
    amountLabel_ = nil;
	
	[amountTextField_ release];
	amountTextField_ = nil;
    
    [referenceLabel_ release];
    referenceLabel_ = nil;
    
    [referenceTextField_ release];
    referenceTextField_ = nil;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler  styleLabel:originAccountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];     
	[originAccountLabel_ setText:NSLocalizedString(ACCOUND_CHARGED_TEXT_KEY, nil)];
    
    UILabel *buttonLabel = originCombo_.titleLabel;
    buttonLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:10.0f];
    buttonLabel.lineBreakMode = UILineBreakModeTailTruncation;        
    
    [NXT_Peru_iPhoneStyler styleLabel:destinationAccountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [destinationAccountLabel_ setText:NSLocalizedString(ACCOUND_PAID_TEXT_KEY, nil)];
    
    buttonLabel = destinationCombo_.titleLabel;
    buttonLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:10.0f];
    buttonLabel.lineBreakMode = UILineBreakModeTailTruncation;        
    
    [NXT_Peru_iPhoneStyler styleLabel:currencyLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [currencyLabel_ setText:NSLocalizedString(TRANSACTION_CURRENCY_TEXT_KEY, nil)];
    
    buttonLabel = currencyCombo_.titleLabel;
    buttonLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:12.0f];
    buttonLabel.lineBreakMode = UILineBreakModeHeadTruncation;
    
    
    [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [amountLabel_ setText:NSLocalizedString(TRANSFER_AMOUNT_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleTextField:amountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    [amountTextField_ setCanContainCents:YES];
    [amountTextField_ setCurrencySymbol:[Tools getCurrencySimbol:CURRENCY_SOLES_LITERAL]];

	[amountTextField_ setDelegate:self];
    [amountTextField_ setPlaceholder:NSLocalizedString(TRANSFER_AMOUNT_HINT_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleLabel:referenceLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [referenceLabel_ setText:NSLocalizedString(REFERENCE_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleTextField:referenceTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    [referenceTextField_ setDelegate:self];
    [referenceTextField_ setPlaceholder:NSLocalizedString(REFERENCE_HINT_TEXT_KEY, nil)];     
    
    [originCombo_ setTitle:NSLocalizedString(TRANSFER_SELECT_ACCOUNT_TEXT_KEY, nil)];
    [destinationCombo_ setTitle:NSLocalizedString(TRANSFER_SELECT_ACCOUNT_TEXT_KEY, nil)];
    
    [originCombo_ setDelegate:self];    
    [destinationCombo_ setDelegate:self];
    [currencyCombo_ setDelegate:self];
    
    [originCombo_ setInputAccessoryView:popButtonsView_];
    [destinationCombo_ setInputAccessoryView:popButtonsView_];
    [currencyCombo_ setInputAccessoryView:popButtonsView_];
    [amountTextField_ setInputAccessoryView:popButtonsView_];
    [referenceTextField_ setInputAccessoryView:popButtonsView_];
    
    
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] initWithObjects:originCombo_, destinationCombo_, currencyCombo_ ,amountTextField_, referenceTextField_ ,nil];

}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(transferResponseReceived:)
                                                 name:kNotificationTransferConfirmationResponseReceived 
                                               object:nil];
    
	if (shouldClearInterfaceData_) {
        
        originCombo_.informFirstSelection = YES;
        destinationCombo_.informFirstSelection = YES;
        [transferBetweenAccountsHelper_ setSelectedOriginAccountIndex:-1];
        [transferBetweenAccountsHelper_ setSelectedDestinationAccountIndex:-1];
        [transferBetweenAccountsHelper_ setSelectedCurrencyIndex:0];
        [transferBetweenAccountsHelper_ setAmountString:@""];
        [transferBetweenAccountsHelper_ setReference:@""];

        [transparentScroll_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f) animated:NO];
		
		shouldClearInterfaceData_ = NO;
        
	}
    
    [self displayStoredInformation];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the confirmation response and stores the information
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationTransferConfirmationResponseReceived 
                                                  object:nil];
    
    [self okButtonClickedInPopButtonsView:popButtonsView_];
    
    [super viewWillDisappear:animated];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased TransferToMyAccountsViewController constructed from a NIB file
 */
+ (TransferBetweenAccountsViewController *)transferBetweenAccountsViewController {
    
    return [[[TransferBetweenAccountsViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
}



#pragma mark -
#pragma mark User interaction

/*
 * Invoked by framework when the origin combo is tapped. Sets it as the editing control
 */
- (IBAction)originComboPressed {
    
    [self editableViewHasBeenClicked:originCombo_];

}

/**
 * Invoked by framework when the destination combo is tapped. Sets it as the editing control
 */
- (IBAction)destinationComboPressed {
    
    [self editableViewHasBeenClicked:destinationCombo_];
}

/*
 *Invoked by framework when the currency combo is tapped. Sets it as the editing control
 */
- (IBAction)currencyComboPressed {
    
    [self editableViewHasBeenClicked:currencyCombo_];
}

#pragma mark -
#pragma mark Notifications management

/*
 * Receives the response of the transfer confirmation
 */
- (void)transferResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (!response.isError) {        
        
         
         if ([transferBetweenAccountsHelper_ transferResponseReceived:response]) {
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationTransferConfirmationResponseReceived object:nil];
             
            [self displayTransferSecondStepView];
            
        } else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
            
        }
        
    }
    
}

#pragma mark -
#pragma mark TransfersStepOneViewController selectors

/**
 * Displays the stored information. Sets the information into the views
 */
- (void)displayStoredInformation {
    
    
    [super displayStoredInformation];
    
    if ([self isViewLoaded]) {
        
        NSMutableArray *array = [NSMutableArray array];
        NSArray *originAccountList = transferBetweenAccountsHelper_.originAccountList;
        
        for (BankAccount *account in originAccountList) {
            
            [array addObject:account.accountIdAndDescription];
        }
        
        originCombo_.stringsList = [NSArray arrayWithArray:array];
        
        [array removeAllObjects];
        
        NSArray *destinationList = transferBetweenAccountsHelper_.destinationAccountList;
        
        for (BankAccount *account in destinationList) {
            
            [array addObject:account.accountIdAndDescription];
        }
        
        destinationCombo_.stringsList = [NSArray arrayWithArray:array];
        
        [array removeAllObjects];
        
        NSArray *currencyList = transferBetweenAccountsHelper_.currencyList;
        
        for (NSString *currency in currencyList) {        
            
            [array addObject:[NSLocalizedString(currency, nil) capitalizedString]];
        }
        
        currencyCombo_.stringsList = [NSArray arrayWithArray:array];
        
        [originCombo_ setSelectedIndex:transferBetweenAccountsHelper_.selectedOriginAccountIndex];
        [destinationCombo_ setSelectedIndex:transferBetweenAccountsHelper_.selectedDestinationAccountIndex];
        [currencyCombo_ setSelectedIndex:transferBetweenAccountsHelper_.selectedCurrencyIndex];
        
        amountTextField_.text = transferBetweenAccountsHelper_.amountString;
        referenceTextField_.text = transferBetweenAccountsHelper_.reference;
    }
}

/**
 * Lays out the views
 */
- (void)layoutViews {
    
    [super layoutViews];
    
    if ([self isViewLoaded]) {
        
        CGRect frame = originAccountLabel_.frame;
        frame.origin.y = VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        CGFloat height = [Tools labelHeight:originAccountLabel_ forText:originAccountLabel_.text];        
        frame.size.height = height;
        originAccountLabel_.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = originCombo_.frame;
        frame.origin.y = auxPos;
        originCombo_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
      
        
        frame = destinationAccountLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:destinationAccountLabel_ forText:destinationAccountLabel_.text];
        frame.size.height = height;
        destinationAccountLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = destinationCombo_.frame;
        frame.origin.y = auxPos;
        destinationCombo_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = currencyLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:currencyLabel_ forText:currencyLabel_.text];
        frame.size.height = height;
        currencyLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = currencyCombo_.frame;
        frame.origin.y = auxPos;
        currencyCombo_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        frame = amountLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:amountLabel_ forText:amountLabel_.text];
        frame.size.height = height;
        amountLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = amountTextField_.frame;
        frame.origin.y = auxPos;
        amountTextField_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = referenceLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:referenceLabel_ forText:referenceLabel_.text];
        frame.size.height = height;
        referenceLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = referenceTextField_.frame;
        frame.origin.y = auxPos;
        referenceTextField_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        UILabel *referenceInfoLabel = self.bottomInfoLabel;
        frame = referenceInfoLabel.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:referenceInfoLabel forText:referenceInfoLabel.text];
        frame.size.height = height;
        referenceInfoLabel.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UIButton *transferButton = self.transferButton;
        frame = transferButton.frame;
        frame.origin.y = auxPos;
        transferButton.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = self.separator.frame;
        frame.origin.y = transferButton.frame.origin.y - 10.0f;
        self.separator.frame = frame;
        
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.size.height = auxPos;
        containerView.frame = frame;
        
        [self recalculateScroll];
        
        
    }
}

/**
 * Stores the information into the transfer operation helper instance
 */
- (void)storeInformationIntoHelper {
    
    [super storeInformationIntoHelper];
    
    transferBetweenAccountsHelper_.amountString = amountTextField_.text;
    transferBetweenAccountsHelper_.reference = referenceTextField_.text;
    
}

/**
 * Returns the transfer operation helper associated to the view controller. Returns the transfer between accounts helper
 *
 * @return The transfer operation helper associated to the view controller
 */
- (TransferOperationHelper *)transferOperationHelper {
    
    return transferBetweenAccountsHelper_;
    
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/** 
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
        
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
            
    }

	return YES;
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self editableViewHasBeenClicked:textField];
    
}

/** 
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    if(textField == amountTextField_) {  
    
        if (resultLength == 0) {
        
            result = YES;
        
        } else {
            
            NSScanner *scanner = [NSScanner scannerWithString:resultString];
            NSInteger resultInteger = 0;
        
            if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
            
                if (resultInteger > 0) {
                
                    result = YES;
                
                }
            
            }   
        
        }
        
    } else if (textField == referenceTextField_) {
            
        if ([resultString length] <= MAX_TEXT_REFERENCE_LENGHT) {
                
            if([Tools isValidText:resultString forCharacterString:REFERENCE_VALID_CHARACTERS]) {
                
                result = YES;
                
            }
                
        } else {
        
            result = NO;

        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark NXTComboButtonDelegate selectors

/*
 * Informs the delegate that a value has been selected
 */
- (void)NXTComboButtonValueSelected:(NXTComboButton *)comboButton {
    
    if (comboButton == originCombo_) {
        
        [transferBetweenAccountsHelper_ setSelectedOriginAccountIndex:comboButton.selectedIndex];
        
        NSMutableArray *array = [[[NSMutableArray alloc] init] autorelease];
        NSArray *destinationAccountList = [NSArray arrayWithArray:transferBetweenAccountsHelper_.destinationAccountList];
        
        NSString *selectedDestinationAccount = @"";
        NSInteger selectedDestinationAccountIndex = destinationCombo_.selectedIndex;
        NSArray *selectedDestinationAccountList = [NSArray arrayWithArray:destinationCombo_.stringsList];
        
        if ((selectedDestinationAccountIndex >= 0) && (selectedDestinationAccountIndex < [selectedDestinationAccountList count])) {
            
            selectedDestinationAccount = [selectedDestinationAccountList objectAtIndex:selectedDestinationAccountIndex];
            
        }
        
        for (BankAccount *account in destinationAccountList) {
                     
                [array addObject:account.accountIdAndDescription];                           
        }
        
        [destinationCombo_ setStringsList:array];
        
        if ([selectedDestinationAccount length] > 0) {
            
            NSUInteger newSelectedIndex = [destinationCombo_.stringsList indexOfObject:selectedDestinationAccount];
            
            if (newSelectedIndex != NSNotFound) {
                
                destinationCombo_.selectedIndex = newSelectedIndex;
                
            } else {
                
                destinationCombo_.selectedIndex = -1;
                
            }
            
        } else {
            
            destinationCombo_.selectedIndex = -1;
            
        }

        
    } else if (comboButton == destinationCombo_) {
        
        [transferBetweenAccountsHelper_ setSelectedDestinationAccountIndex:comboButton.selectedIndex];
        
    } else if (comboButton == currencyCombo_) {
        
        [transferBetweenAccountsHelper_ setSelectedCurrencyIndex:comboButton.selectedIndex];
        
        NSString *currency =[transferBetweenAccountsHelper_ selectedCurrency];
        
        [amountTextField_ setCurrencySymbol:[Tools getCurrencySimbol:currency]];
        
    }
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the transfer between accounts operation helper element
 *
 * @param transferBetweenAccountsHelper The new transfer between accounts operation helper element to set
 */
- (void)setTransferBetweenAccountsHelper:(TransferBetweenAccounts *)transferBetweenAccountsHelper {
    
    if (transferBetweenAccountsHelper != transferBetweenAccountsHelper_) {
        
        [originCombo_ setSelectedIndex:-1];
        [destinationCombo_ setSelectedIndex:-1 ];
        [currencyCombo_ setSelectedIndex:-1] ;
        
        [transferBetweenAccountsHelper retain];
        [transferBetweenAccountsHelper_ release];
        transferBetweenAccountsHelper_ = transferBetweenAccountsHelper;
        [self displayStoredInformation];
        
    }
    
    [transferBetweenAccountsHelper_ setSecondFactorKey:@""];
    
    [self resetScrollToTopLeftAnimated:NO];
    
}
@end



