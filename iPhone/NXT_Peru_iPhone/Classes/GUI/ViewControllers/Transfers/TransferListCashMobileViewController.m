//
//  TransferListCashMobileViewController.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 9/25/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "TransferListCashMobileViewController.h"
#import "GlobalAdditionalInformation.h"
#import "TransferMenuCell.h"
#import "StringKeys.h"
#import "Session.h"
#import "ImagesCache.h"
#import "Constants.h"
#import "ImagesFileNames.h"
#import "AccountList.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "Updater.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "TransferWithCashMobileViewController.h"
#import "TransferStartupResponse.h"
#import "TransferWithCashMobile.h"
#import "TransferDetailResponse.h"
#import "NXTNavigationItem.h"

@interface TransferListCashMobileViewController ()

@end

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"TransferListCashMobileViewController"

@implementation TransferListCashMobileViewController

/*
 * Creates and returns an autoreleased TransfersListViewController constructed from a NIB file.
 */
+ (TransferListCashMobileViewController *)transferListCashMobileViewController {
    
    TransferListCashMobileViewController *result =  [[[TransferListCashMobileViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [NXT_Peru_iPhoneStyler styleTableView:_table];
    
    _table.scrollEnabled = NO;
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:TRUE];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(transferToAccountsWithCashMobileStartupReceived:)
                                                 name:kNotificationTransferWithCashMobileStartupEnds
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:TRUE];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationTransferWithCashMobileStartupEnds object:nil];
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = NSLocalizedString(TRANSFER_WITH_CASH_MOBILE_TEXT_1_KEY, nil);
    
    return result;
    
}

#pragma mark - Notifications

/*
 * Receives the response of the transfer to account with cash mobile startup
 */
- (void)transferToAccountsWithCashMobileStartupReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationTransferWithCashMobileStartupEnds
                                                  object:nil];
    
    [self.appDelegate hideActivityIndicator];
    
	TransferStartupResponse *response = [notification object];
    
    if (!response.isError) {
        
        if (TransferWithCashMobileViewController_ != nil) {
            TransferWithCashMobileViewController_ = nil;
            [TransferWithCashMobileViewController_ release];
        }
        
        TransferWithCashMobileViewController_ = [[TransferWithCashMobileViewController transferWithCashMobileViewController] retain];
        
        TransferWithCashMobile *transferHelper = [[TransferWithCashMobile alloc] initWithTransferStartupResponse:response];
        [TransferWithCashMobileViewController_ setTransferWithCashMobileHelper:transferHelper];
        [TransferWithCashMobileViewController_ setShouldClearInterfaceData:YES];
        [TransferWithCashMobileViewController_ resetInformation];
        [self.navigationController pushViewController:TransferWithCashMobileViewController_ animated:YES];
    }
    
}

/*
 * Receives the response of the detail cash mobile transfers startup
 */
- (void)transferToAccountsWithCashMobileDetailStartupReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationTransferWithCashMobileDetailStartupEnds
                                                  object:nil];
    
    [self.appDelegate hideActivityIndicator];
    
	TransferDetailResponse *response = [notification object];
    
    if (!response.isError) {
        
        if (TransferWithCashMobileConsultViewController_ == nil) {
            TransferWithCashMobileConsultViewController_ = [[TransferWithCashMobileConsultViewController TransferWithCashMobileViewController] retain];
        }
        
        TransferWithCashMobileConsultViewController_.movementsList = response.additionalInformationArray;
        [self.navigationController pushViewController:TransferWithCashMobileConsultViewController_ animated:YES];
    }
    
}

#pragma mark -
#pragma mark UITableView methods

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TransferMenuCell *result = (TransferMenuCell *)[tableView dequeueReusableCellWithIdentifier:[TransferMenuCell cellIdentifier]];
    
    if (result == nil) {
        result = [TransferMenuCell transferMenuCell];
//        result.withouthLeftImage = TRUE;
    }
	
    [result accomodateViewsForBool];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    switch (indexPath.row) {
        case 0:
            result.titleLabel.text = NSLocalizedString(TRANSFER_WITH_CASH_MOBILE_SEND_TEXT_1_KEY, nil);
            result.image.image = [imagesCache imageNamed:TRANSFER_WITH_CASH_MOBILE_FILE_NAME];
            break;
            
        case 1:
            result.titleLabel.text = NSLocalizedString(TRANSFER_WITH_CASH_MOBILE_DETAIL_TEXT_1_KEY, nil);
            result.image.image = [imagesCache imageNamed:CONSULT_CASH_MOBILE_FILE_NAME];
            break;
            
        default:
            break;
    }
    
    //[NXT_Peru_iPhoneStyler styleLabel:result.titleLabel withFontSize:16.0f color:[UIColor BBVABlueSpectrumColor]];
    result.selectionStyle = UITableViewCellSelectionStyleGray;
    result.backgroundColor = [UIColor clearColor];
    return result;
    
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void) tableView: (UITableView*) tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Session *session = [Session getInstance];
    BOOL otpActive = [[session additionalInformation] otpActive];
    
    
    switch (indexPath.row) {
        
        case 0: {
            
            if (otpActive) {
                
                [self.appDelegate showActivityIndicator:poai_Both];
                [[Updater getInstance] transferToAccountsWithCashMobileStartup];
                
            } else {
                
                [Tools showInfoWithMessage:NSLocalizedString(OTP_SERVICE_NO_ACTIVE_TEXT_KEY, nil)];
                
            }
            
            break;
            
        }
        case 1: {
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(transferToAccountsWithCashMobileDetailStartupReceived:)
                                                         name:kNotificationTransferWithCashMobileDetailStartupEnds
                                                       object:nil];
            
            [self.appDelegate showActivityIndicator:poai_Both];
            [[Updater getInstance] obtainCashMobileTransactionDetail];
            
            break;
            
        }
        default: {
            
            break;
            
        }
            
    }
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section {
    
    return 2;
    
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [TransferMenuCell cellHeight];
    
}

#pragma mark - Memory management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_table release];
    [TransferWithCashMobileViewController_ release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTable:nil];
    [super viewDidUnload];
}
@end
