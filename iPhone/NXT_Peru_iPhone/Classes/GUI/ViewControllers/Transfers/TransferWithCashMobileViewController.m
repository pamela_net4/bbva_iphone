//
//  TransferWithCashMobileViewController.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 9/25/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "TransferWithCashMobileViewController.h"

#import "Constants.h"
#import "TransfersStepOneViewController+protected.h"
#import "SimpleSelectorAccountView.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"
#import "StringKeys.h"
#import "Tools.h"
#import "NXTCurrencyTextField.h"
#import "NXTTextField.h"
#import "NXTEditableViewController+protected.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "TransferWithCashMobile.h"
#import "BankAccount.h"
#import "NXTNavigationItem.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the NIB file name
 */
#define TRANSFERS_VIEW_CONTROLLER_NIB_FILE                          @"TransferWithCashMobileViewController"

/*
 * Max lenght of reference field
 */
#define MAX_TEXT_REFERENCE_LENGHT                                   20



/*
 *max number office text field
 */
#define MAX_NUMBER_OFFICE                                           4

/*
 *max number account text field
 */
#define MAX_NUMBER_ACCOUNT                                          10

@interface TransferWithCashMobileViewController ()

/**
 * Releases graphic elements
 */
- (void)releaseTransferWithCashMobileViewControllerGraphicElements;

/**
 * Receives the response of the transfer confirmation
 *
 * @param notification The NSNotification object containing the notification information
 * @private
 */
- (void)transferResponseReceived:(NSNotification *)notification;

@end

#pragma mark -

@implementation TransferWithCashMobileViewController

#pragma mark -
#pragma mark Properties

@synthesize originAccountLabel = originAccountLabel_;
@synthesize originCombo = originCombo_;
@synthesize destinationLabel = destinationLabel_;
@synthesize amountLabel = amountLabel_;
@synthesize amountTextField = amountTextField_;
@synthesize transferWithCashMobileHelper = transferWithCashMobileHelper_;
@synthesize phoneDestinationView = phoneDestinationView_;
@synthesize amountIndicationLabel = amountIndicationLabel_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransferWithCashMobileViewControllerGraphicElements];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationTransferConfirmationResponseReceived
                                                  object:nil];
    
    [transferWithCashMobileHelper_ release];
    transferWithCashMobileHelper_ = nil;
    
    [editableViews_ release];
    editableViews_ = nil;
    
    [phoneDestinationView_ release];
    phoneDestinationView_ = nil;
    
    [amountIndicationLabel_ release];
    amountIndicationLabel_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransferWithCashMobileViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransferWithCashMobileViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseTransferWithCashMobileViewControllerGraphicElements {
    
    [originAccountLabel_ release];
    originAccountLabel_ = nil;
    
    [originCombo_ release];
    originCombo_ = nil;
    
    [destinationLabel_ release];
    destinationLabel_ = nil;
    
	[destinationAccountView_ release];
    destinationAccountView_ = nil;
    
    [amountLabel_ release];
    amountLabel_ = nil;
    
    [amountTextField_ release];
    amountTextField_ = nil;
    
    [editableViews_ removeAllObjects];
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler  styleLabel:originAccountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
	[originAccountLabel_ setText:NSLocalizedString(CASH_MOBILE_ACCOUNT_CHARGED_TEXT_KEY, nil)];
    
    UILabel *buttonLabel = originCombo_.titleLabel;
    buttonLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:10.0f];
    buttonLabel.lineBreakMode = UILineBreakModeTailTruncation;
    
    [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [amountLabel_ setText:NSLocalizedString(SEND_AMOUNT_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleTextField:amountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    [amountTextField_ setCanContainCents:NO];
    [amountTextField_ setCurrencySymbol:[Tools getCurrencySimbol:CURRENCY_SOLES_LITERAL]];
    [amountTextField_ setDelegate:self];
    [amountTextField_ setPlaceholder:NSLocalizedString(TRANSFER_AMOUNT_HINT_TEXT_KEY, nil)];
    [amountTextField_ setInputAccessoryView:popButtonsView_];
    
    [originCombo_ setTitle:NSLocalizedString(TRANSFER_SELECT_ACCOUNT_TEXT_KEY, nil)];
    [originCombo_ setDelegate:self];
    [originCombo_ setInputAccessoryView:popButtonsView_];
    
    [self.transferButton setTitle:NSLocalizedString(OK_TEXT_KEY, nil) forState:UIControlStateNormal];
    
    NXTTextField *officeTextField = destinationAccountView_.officeTextField;
    
    [officeTextField setDelegate:self];
    [officeTextField setInputAccessoryView:popButtonsView_];
    
    NXTTextField *accountTextField = destinationAccountView_.accountTextField;
    [accountTextField setDelegate:self];
    [accountTextField setInputAccessoryView:popButtonsView_];
    
    if(phoneDestinationView_ == nil) {
        
        phoneDestinationView_ = [[PhoneDestinationView phoneDestinationView] retain];
        
        phoneDestinationView_.delegate = self;
        
        phoneDestinationView_.phoneNumberTextField.delegate = self;
        
        self.destinationPhoneNumber = phoneDestinationView_.phoneNumberTextField;
        
        [phoneDestinationView_.phoneNumberTextField setInputAccessoryView:popButtonsView_];
        
        [phoneDestinationView_ setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth];
    }

    self.destinationPhoneNumber.delegate = self;
    
    CGRect frame = [phoneDestinationView_ frame];
    frame.origin.x = 10.0f;
    frame.origin.y = 0.0f;
    [phoneDestinationView_ setFrame:frame];
    
    UIView *superView = [self containerView];
    [superView addSubview:phoneDestinationView_];
    [superView bringSubviewToFront:phoneDestinationView_];
    
    amountIndicationLabel_.text = NSLocalizedString(AMOUNT_MULTIPLE_TWENTY_KEY, nil);
    
    [NXT_Peru_iPhoneStyler styleLabel:amountIndicationLabel_ withFontSize:10.0f color:[UIColor BBVABlackColor]];
    
    self.isCashMobileSend = TRUE;
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(transferResponseReceived:)
                                                 name:kNotificationTransferConfirmationResponseReceived
                                               object:nil];
    
	if (shouldClearInterfaceData_) {
        
        originCombo_.informFirstSelection = YES;
        
        [transferWithCashMobileHelper_ setSelectedOriginAccountIndex:-1];

        [transferWithCashMobileHelper_ setAmountString:@""];

        [transparentScroll_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f) animated:NO];
		
		shouldClearInterfaceData_ = NO;
        
	}
    
    if (editableViews_ == nil) {
        
        editableViews_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [editableViews_ removeAllObjects];
        
    }
    
    [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:originCombo_,
                                         amountTextField_,
                                         phoneDestinationView_.phoneNumberTextField,
                                         nil]];
        
    
    
    [self displayStoredInformation];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the confirmation response and stores the information
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationTransferConfirmationResponseReceived
                                                  object:nil];
    
    [officeString_ release];
    officeString_ = nil;
    
    [accountString_ release];
    accountString_ = nil;
    
    [self okButtonClickedInPopButtonsView:popButtonsView_];
    
    [super viewWillDisappear:animated];
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = NSLocalizedString(TRANSFER_WITH_CASH_MOBILE_TEXT_1_KEY, nil);
    
    return result;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Creates and returns an autoreleased TransferToMyAccountsViewController constructed from a NIB file
 *
 * @return The autoreleased TransferToMyAccountsViewController constructed from a NIB file
 */
+ (TransferWithCashMobileViewController *)transferWithCashMobileViewController
{
    
    TransferWithCashMobileViewController *result = [[TransferWithCashMobileViewController alloc] initWithNibName:TRANSFERS_VIEW_CONTROLLER_NIB_FILE bundle:nil];
    [result awakeFromNib];
    
    return result;
}

#pragma mark -
#pragma mark User interaction

- (IBAction)originComboPressed {
    
    [self editableViewHasBeenClicked:originCombo_];
}

/*
 * Display the contact selection
 */
- (void)displayContactSelection {
    
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    
    self.hasNavigateForward = YES;
    
    [[picker navigationBar] setTintColor:[[[self navigationController] navigationBar] tintColor]];
    
    [self.appDelegate presentModalViewController:picker animated:YES];
    
    [picker release];
    
}

#pragma mark -
#pragma mark TransfersStepOneViewController selectors

/**
 * Displays the stored information. Sets the information into the views
 */
- (void)displayStoredInformation {
    
    [super displayStoredInformation];
    
    if ([self isViewLoaded]) {
        
        NSMutableArray *array = [NSMutableArray array];
        NSArray *originAccountList = transferWithCashMobileHelper_.originAccountList;
        
        for(BankAccount *account in originAccountList) {
            
            [array addObject:account.accountIdAndDescription];
        }
        
        originCombo_.stringsList = [NSArray arrayWithArray:array];
        
        if ([array count] == 1) {
            [originCombo_ setSelectedIndex:0];
        }
        
        [array removeAllObjects];
        
        [originCombo_ setSelectedIndex:transferWithCashMobileHelper_.selectedOriginAccountIndex];
        
        [phoneDestinationView_.phoneNumberTextField setText:transferWithCashMobileHelper_.selectedDestinationPhoneNumber];
        
        [amountTextField_ setText:transferWithCashMobileHelper_.amountString];
        
    }
}

/**
 * Lays out the views
 */
- (void)layoutViews {
    
    [super layoutViews];
    
    if ([self isViewLoaded]) {
        
        CGRect frame = originAccountLabel_.frame;
        frame.origin.y = VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        CGFloat height = [Tools labelHeight:originAccountLabel_ forText:originAccountLabel_.text];
        frame.size.height = height;
        originAccountLabel_.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = originCombo_.frame;
        frame.origin.y = auxPos;
        originCombo_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = amountLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:amountLabel_ forText:amountLabel_.text];
        frame.size.height = height;
        amountLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = amountTextField_.frame;
        frame.origin.y = auxPos;
        amountTextField_.frame = frame;
        auxPos = CGRectGetMaxY(frame);
        
        frame = amountIndicationLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:amountIndicationLabel_ forText:amountIndicationLabel_.text];
        frame.size.height = height;
        amountIndicationLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = [phoneDestinationView_ frame];
        frame.origin.y = auxPos;
        [phoneDestinationView_ setFrame:frame];
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UIButton *transferButton = self.transferButton;
        frame = transferButton.frame;
        frame.origin.y = auxPos;
        transferButton.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.size.height = auxPos;
        containerView.frame = frame;
        
        [self recalculateScroll];
        
    }
}

/**
 * Stores the information into the transfer operation helper instance
 */
- (void)storeInformationIntoHelper {
    
    [super storeInformationIntoHelper];
    
    transferWithCashMobileHelper_.selectedDestinationPhoneNumber = phoneDestinationView_.phoneNumberTextField.text;
    transferWithCashMobileHelper_.amountString = amountTextField_.text;
    
}

/**
 * Returns the transfer operation helper associated to the view controller. Returns the transfer between accounts helper
 *
 * @return The transfer operation helper associated to the view controller
 */
- (TransferOperationHelper *)transferOperationHelper {
    
    return transferWithCashMobileHelper_;
    
}



#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
	return YES;
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self editableViewHasBeenClicked:textField];
    
    
}

/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    result = [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    if(textField == amountTextField_) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            NSScanner *scanner = [NSScanner scannerWithString:resultString];
            NSInteger resultInteger = 0;
            
            if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                if (resultInteger > 0) {
                    
                    result = YES;
                }
            }
        }
    } else if (textField == destinationAccountView_.accountTextField) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            if(resultLength <= MAX_NUMBER_ACCOUNT) {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        result = YES;
                    }
                }
            }
            else {
                result = NO;
            }
        }
        
    } else if(textField == destinationAccountView_.officeTextField) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            if(resultLength <= MAX_NUMBER_OFFICE) {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        result = YES;
                    }
                }
            }
            else {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        if([accountString_ length] == MAX_NUMBER_ACCOUNT) {
                            
                            result = NO;
                        }
                        else {
                            result = YES;
                        }
                        [destinationAccountView_.accountTextField becomeFirstResponder];
                    }
                }
            }
        }
        
    }
    
    return result;
    
}

/**
 * Asks the delegate if editing should stop in the specified text field. Forwards it to the external delegate
 *
 * @param textField The text field for which editing is about to end
 * @return YES if editing should stop; otherwise, NO if the editing session should continue
 */
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    BOOL result = YES;
    
    NSString *resultString = [NSString stringWithString:textField.text];
    
    //    NSUInteger resultLength = [resultString length];
    
    if(textField == destinationAccountView_.officeTextField) {
        
        if (officeString_ != nil) {
            [officeString_ release];
            officeString_ = nil;
        }
        
        officeString_ = [resultString retain];
    
    }
    else if (textField == destinationAccountView_.accountTextField) {
        
        if (accountString_ != nil) {
            [accountString_ release];
            accountString_ = nil;
        }
        
        accountString_ = [resultString retain];
        
    }
    
    return result;
}

#pragma mark -
#pragma mark NXTComboButtonDelegate selectors

/*
 * Informs the delegate that a value has been selected
 */
- (void)NXTComboButtonValueSelected:(NXTComboButton *)comboButton {
    
    if (comboButton == originCombo_) {
        
        [transferWithCashMobileHelper_ setSelectedOriginAccountIndex:comboButton.selectedIndex];
    }
}

#pragma mark - PhoneNumberDestinationDelegate
- (void)addPhoneContactTapped
{
    if ((phoneDestinationView_.phoneNumberTextField.text == nil) || [phoneDestinationView_.phoneNumberTextField.text isEqualToString:@""]) {
        
//        addingFirstEmail_ = YES;
        [self displayContactSelection];
        
    } else {
        
        phoneDestinationView_.phoneNumberTextField.text = @"";
        self.transferOperationHelper.destinationEmail1= @"";
        
    }
}

#pragma mark -
#pragma mark ABPeoplePickerNavigationControllerDelegate methods

/**
 * Called in ios 8 redirection to shouldContinueAfterSelectingPerson used in ios 7 or previous
 */
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    
    [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person property:property identifier:identifier];
}

/**
 * Called after the user has pressed cancel. The delegate is responsible for dismissing the peoplePicker
 */
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    
	[self.appDelegate dismissModalViewControllerAnimated:YES];
    
}

/**
 * Called after a person has been selected by the user.
 *
 * Return YES if you want the person to be displayed.
 * Return NO  to do nothing (the delegate is responsible for dismissing the peoplePicker).
 */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    
	[self.appDelegate dismissModalViewControllerAnimated:YES];
    
    
    BOOL isValidMobilePhoneNumber = NO;
    
    ABMultiValueRef phoneProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
    
    if (ABMultiValueGetCount(phoneProperty) > 0) {
        
        for (int i = 0; ABMultiValueGetCount(phoneProperty) > i; i++) {
            
            ABMultiValueRef phoneValue = ABMultiValueCopyValueAtIndex(phoneProperty,i);
            
            NSString *phoneNumber = (NSString *)phoneValue;
            
            phoneNumber = [Tools mobileNumberLikeFromString:phoneNumber];
            
            isValidMobilePhoneNumber = [Tools isValidMobilePhoneNumberString:phoneNumber];
            
            if (isValidMobilePhoneNumber) {
                
                [phoneDestinationView_.phoneNumberTextField setText:phoneNumber];
                transferWithCashMobileHelper_.selectedDestinationPhoneNumber = phoneNumber;
            }
            
            if (phoneValue != nil) {
                
                CFRelease(phoneValue);
                
            }
            
            if (isValidMobilePhoneNumber) {
                break;
            }
        }
        
    }
    
    
    if (!isValidMobilePhoneNumber) {
        
        [Tools showInfoWithMessage:NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_NO_EXIST_TEXT_KEY, nil)];
        
    }
    
    if (phoneProperty != nil) {
        
        CFRelease(phoneProperty);
        
    }
    
  	return NO;
}

/**
 * Called after a value has been selected by the user.
 *
 * Return YES if you want default action to be performed.
 * Return NO to do nothing (the delegate is responsible for dismissing the peoplePicker).
 */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property
                              identifier:(ABMultiValueIdentifier)identifier {
    
    return [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person];
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the transfer between accounts operation helper element
 *
 * @param transferBetweenAccountsHelper The new transfer between accounts operation helper element to set
 */
- (void)settransferWithCashMobileHelper:(TransferWithCashMobile *)transferWithCashMobileHelper {
    
    if (transferWithCashMobileHelper != transferWithCashMobileHelper_) {
        
        [originCombo_ setSelectedIndex:-1];
        
        [transferWithCashMobileHelper retain];
        [transferWithCashMobileHelper_ release];
        transferWithCashMobileHelper_ = transferWithCashMobileHelper;
        
        [self displayStoredInformation];
        
    }
    
    [transferWithCashMobileHelper_ setSecondFactorKey:@""];
    
    [self resetScrollToTopLeftAnimated:NO];
    
}


#pragma mark -
#pragma mark Notifications management

/*
 * Receives the response of the transfer confirmation
 */
- (void)transferResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (!response.isError) {
        
        
        if ([transferWithCashMobileHelper_ transferResponseReceived:response]) {
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationTransferConfirmationResponseReceived object:nil];
            
            [self displayTransferSecondStepView];
            
        } else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
            
        }
        
    }
    
}

@end

