/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTTransfersViewController.h"

@class TransferDetailCell;

/**
 * TransfersViewController protected category
 */
@interface NXTTransfersViewController(protected)

/**
 * Returns the step string to display in the step label. Default implementation returns an empty string
 *
 * @return The step string to display in the step label
 * @protected
 */
- (NSString *)stepLabelString;

/**
 * Displays the stored information. Default implementation does nothing
 *
 * @protected
 */
- (void)displayStoredInformation;

/**
 * Lays out the views. Default implementation does nothing
 *
 * @protected
 */
- (void)layoutViews;

/**
 * Returns a table cell that represents the operation step information stored at teh given index. The cell is a TransferDetailCell instance
 *
 * @param index The index where the information is stored
 * @param tableView The table view displaying the cell
 * @return The table cell representing the information
 * @protected
 */
- (TransferDetailCell *)transferDetailCellAtIndex:(NSInteger)index
                                     forTableView:(UITableView *)tableView;

/**
 * Returns the table cell height for the cell that represents the operation step information stored at teh given index.
 * The cell is a TransferDetailCell instance
 *
 * @param index The index where the information is stored
 * @return The table cell representing the information
 * @protected
 */
- (CGFloat)transferDetailCellHeightAtIndex:(NSInteger)index;

/**
 * Saves the information inside the editable elements into the transfer operation helper. Default implementation does nothing
 *
 * @protected
 */
- (void)storeInformationIntoHelper;

/**
 * Returns the variable information for the view controller. All objects must be TitleAndAttributes instances (even though the array if
 * filtered). Default implementation returns an empty array
 *
 * @return The variable information for the view controller
 * @protected
 */
- (NSArray *)variableInformation;

/**
 * Returns the number of entries in the variable information array
 *
 * @return The number of entries in the variable information array
 * @protected
 */
- (NSUInteger)variableInformationEntriesCount;

@end
