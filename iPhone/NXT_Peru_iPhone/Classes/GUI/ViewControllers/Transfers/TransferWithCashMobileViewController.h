//
//  TransferWithCashMobileViewController.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 9/25/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "NXTEditableViewController.h"

#import "TransfersStepOneViewController.h"
#import "TransfersConstants.h"
#import "NXTComboButton.h"
#import "PhoneDestinationView.h"

//Forward declarations
@class NXTTextField;
@class NXTCurrencyTextField;
@class TransferWithCashMobile;
@class SimpleSelectorAccountView;

@interface TransferWithCashMobileViewController : TransfersStepOneViewController <UITextFieldDelegate, NXTComboButtonDelegate, PhoneDestinationViewDelegate, ABPeoplePickerNavigationControllerDelegate> {
    
@private
    
    /*
     * origin account label
     */
    UILabel *originAccountLabel_;
    
    /*
     * origin combo
     */
    NXTComboButton *originCombo_;
    
    /*
     * destination label
     */
    UILabel *destinationLabel_;
    
    /*
     * Destination account
     */
    SimpleSelectorAccountView *destinationAccountView_;
    
    /*
     * amount label
     */
    UILabel *amountLabel_;
    
    /*
     * Amount consideration label
     */
    UILabel *amountIndicationLabel_;
    
    /*
     * amount text field
     */
    NXTCurrencyTextField *amountTextField_;
    
    /*
     * transfer with cash mobile helper
     */
    TransferWithCashMobile *transferWithCashMobileHelper_;
    
    /*
     * Phone destination view
     */
    PhoneDestinationView *phoneDestinationView_;
    
    /*
     * office string
     */
    NSString *officeString_;
    
    /*
     * account string
     */
    NSString *accountString_;
}

/**
 * Provides readwrite access to the originAccountLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *originAccountLabel;

/**
 * Provides readwrite access to the originCombo button. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *originCombo;

/**
 * Provides readwrite access to the destinationLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *destinationLabel;

/**
 * Provides readwrite access to the amountLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *amountLabel;

/**
 * Provides readwrite access to the amountIndicationLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *amountIndicationLabel;

/**
 * Provides readwrite access to the amountTextField. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTCurrencyTextField *amountTextField;

/**
 * Provides read-write access to the transfer withCashMobile helper element
 */
@property (nonatomic, readwrite, retain) TransferWithCashMobile *transferWithCashMobileHelper;

/**
 * Provides read-write access to the phone view element
 */
@property (nonatomic, readwrite, retain) PhoneDestinationView *phoneDestinationView;

/**
 * Creates and returns an autoreleased TransferToMyAccountsViewController constructed from a NIB file
 *
 * @return The autoreleased TransferToMyAccountsViewController constructed from a NIB file
 */
+ (TransferWithCashMobileViewController *)transferWithCashMobileViewController;

/**
 * Invoked by framework when the origin combo is tapped. Sets it as the editing control
 */
- (IBAction)originComboPressed;


@end
