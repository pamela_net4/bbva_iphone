/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "TransfersStepOneViewController+protected.h"

#import "NXTTextField.h"
#import "NXTTextView.h"
#import "TransferOperationHelper.h"
#import "TransfersStepTwoViewController.h"
#import "SimpleHeaderView.h"


#pragma mark -

@implementation TransfersStepOneViewController(protected)

#pragma mark -
#pragma mark Graphic management

/*
 * Displays the stored information. Default implementation updates the header and the table
 */
- (void)displayStoredInformation {
    
    if ([self isViewLoaded]) {
        
        TransferOperationHelper *transferOperationHelper = [self transferOperationHelper];
        [operationTitleHeader_ setTitle:[transferOperationHelper localizedTransferOperationTypeString]];
        [selectionTableView_ reloadData];
        [self layoutViews];
        
    }
    
}

/*
 * Lays out the views
 */
- (void)layoutViews {
    
}

#pragma mark -
#pragma mark Information management

/*
 * Stores the information into the transfer operation helper instance. Default implementation does nothing
 */
- (void)storeInformationIntoHelper {    
    
    if(self.transferOperationHelper.canSendEmailandSMS) { 
        
        [self saveContactInfoIntoHelper];
//        [[self transferOperationHelper] setDestinationSMS1:[[self smsCell] firstTextField].text];
//        [[self transferOperationHelper] setDestinationSMS2:[[self smsCell] secondTextField].text];
//        [[self transferOperationHelper] setSendSMS:[[self smsCell] smsSwitch].on];
//        [[self transferOperationHelper] setDestinationEmail1:[[self emailCell] firstTextField].text];
//        [[self transferOperationHelper] setDestinationEmail2:[[self emailCell] secondTextField].text];
//        [[self transferOperationHelper] setSendEmail:[[self emailCell] emailSwitch].on];
        
    }
         
}

/*
 * Save contact information into the helper
 */
- (void)saveContactInfoIntoHelper {
    
    TransferOperationHelper *transferOperationHelper = self.transferOperationHelper;
    
    transferOperationHelper.sendSMS = smsCell_.smsSwitch.on;
    transferOperationHelper.destinationSMS1 = smsCell_.firstTextField.text;
    transferOperationHelper.destinationSMS2 = smsCell_.secondTextField.text;
    transferOperationHelper.selectedCarrier1Index = smsCell_.firstComboButton.selectedIndex;
    transferOperationHelper.selectedCarrier2Index = smsCell_.secondComboButton.selectedIndex;
    
    transferOperationHelper.sendEmail = emailCell_.emailSwitch.on;
    transferOperationHelper.destinationEmail1 = emailCell_.firstTextField.text;
    transferOperationHelper.destinationEmail2 = emailCell_.secondTextField.text;
    transferOperationHelper.emailMessage = emailCell_.emailTextView.text;
    
}

/*
 * Returns the transfer operation helper associated to the view controller. Default implementation returns nil
 */
- (TransferOperationHelper *)transferOperationHelper {
    
    return nil;
    
}

#pragma mark -
#pragma mark Displaying views

/*
 * Displays the transfer second step view controller
 */
- (void)displayTransferSecondStepView {
    
    if (transfersStepTwoViewController_ != nil) {
        
        transfersStepTwoViewController_ = nil;
        [transfersStepTwoViewController_ release];
    }
    
    transfersStepTwoViewController_ = [[TransfersStepTwoViewController transfersStepTwoViewController] retain];
    
    self.hasNavigateForward = YES;
    
    TransferOperationHelper *transferOperationHelper = [self transferOperationHelper];
    
    transfersStepTwoViewController_.transferOperationHelper = transferOperationHelper;
    transfersStepTwoViewController_.isCashMobileSend = isCashMobileSend_;
    [self.navigationController pushViewController:transfersStepTwoViewController_
                                         animated:YES];
    
}


@end
