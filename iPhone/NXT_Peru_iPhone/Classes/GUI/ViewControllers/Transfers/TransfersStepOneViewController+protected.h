/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "TransfersStepOneViewController.h"


//Forward declarations
@class TransferDetailCell;
@class TransferOperationHelper;


/**
 * TransfersStepOneViewController protected category
 */
@interface TransfersStepOneViewController(protected)

/**
 * Displays the stored information. Default implementation updates the header and the table
 *
 * @protected
 */
- (void)displayStoredInformation;

/**
 * Lays out the views. Default implementation does nothing
 *
 * @protected
 */
- (void)layoutViews;

/**
 * Stores the information into the transfer operation helper instance. Default implementation does nothing
 *
 * @protected
 */
- (void)storeInformationIntoHelper;

/**
 * Save contact information into the helper
 */
- (void)saveContactInfoIntoHelper;

/**
 * Returns the transfer operation helper associated to the view controller. Default implementation returns nil
 *
 * @return The transfer operation helper associated to the view controller
 * @protected
 */
- (TransferOperationHelper *)transferOperationHelper;

/**
 * Displays the transfer second step view controller
 *
 * @private
 */
- (void)displayTransferSecondStepView;

@end
