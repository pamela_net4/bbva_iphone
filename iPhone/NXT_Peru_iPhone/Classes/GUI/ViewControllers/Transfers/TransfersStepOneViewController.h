/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */
#import "NXTEditableViewController.h"
#import "SMSCell.h"
#import "EmailCell.h"
#import <AddressBookUI/AddressBookUI.h>
#import "NXTComboButton.h"

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        15.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                      12.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLEST_SIZE                                     10.0f

/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_NEAR_ELEMENT                           5.0f

/**
 * Defines the vertical gap between two far away elements
 */
#define VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT                       10.0f


//Forward declarations
@class SimpleHeaderView;
@class TransfersStepTwoViewController;
@class SMSCell;
@class EmailCell;


/**
 * Base view controller for the first transfer steps. It provides a basic behaviour and elements
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransfersStepOneViewController : NXTEditableViewController <UITableViewDataSource, UITableViewDelegate, SMSCellDelegate, EmailCellDelegate, ABPeoplePickerNavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate, NXTComboButtonDelegate> {
    
@private
    
    /**
     * Operation title header
     */
    SimpleHeaderView *operationTitleHeader_;
    
    /**
     * Container view to contain the editable components
     */
    UIView *containerView_;
    
    /**
     * Last editing view
     */
    UIView *lastEditingView_;
    
    /**
     * Selection table view
     */
    UITableView *selectionTableView_;
	
	/**
     * Send button
     */
    UIButton *transferButton_;
    
    /**
     * Advise bottom info label
     */
    UILabel *bottomInfoLabel_;
    
    /**
     * Separator image view
     */
    UIImageView *separator_;
    
    /**
     * Branding image view
     */
    UIImageView *brandingImageView_;
    
    /**
     * Adding first contact flag
     */
    BOOL addingFirstSMS_;
    
    /**
     * Adding second contact flag
     */
    BOOL addingSecondSMS_;
    
    /**
     * Adding first email flag
     */
    BOOL addingFirstEmail_;
    
    /**
     * Adding first email flag
     */
    BOOL addingSecondEmail_;
    
    /**
     * SMS Cell
     */
    SMSCell *smsCell_;
    
    /**
     * Email Cell
     */
    EmailCell *emailCell_;
    
    /**
     * Destination phone number
     */
    NXTTextField *destinationPhoneNumber_;
    
    /**
     * Transfers step two view controller
     */
    TransfersStepTwoViewController *transfersStepTwoViewController_;
    
    /**
     * Adding first email flag
     */
    BOOL hasNavigateForward_;
    
    /**
     * To customize when is cash mobile
     */
    BOOL isCashMobileSend_;
        
}

/**
 * Provides readwrite access to container view to contain the editable components and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *containerView;


/**
 * Provides readwrite access to the selection table. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *selectionTableView;

/**
 * Provides readwrite access to the transfer button. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *transferButton;

/**
 * Provides readwrite access to the bottom info label.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *bottomInfoLabel;

/**
 * Provides readwrite access to the separator image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;

/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) EmailCell *emailCell;

/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) SMSCell *smsCell;

/**
 * Provides readwrite access to the phone destination phone number
 */
@property (nonatomic, readwrite, retain) NXTTextField *destinationPhoneNumber;

/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, assign) BOOL hasNavigateForward;

/**
 * Provides readwrite access to the flag to know if is cash mobile operation
 */
@property (nonatomic, readwrite, assign) BOOL isCashMobileSend;


/**
 * Resets the internal information.
 */
- (void)resetInformation;

@end
