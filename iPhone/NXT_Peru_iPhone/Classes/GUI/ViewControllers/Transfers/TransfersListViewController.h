/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTViewController.h"

//Forward declarations
@class TransferBetweenAccountsViewController;
@class TransferToThirdAccountsViewController;
@class TransferToOtherBankAccountViewController;
@class TransferToGiftCardViewController;
@class RegisteredAccountListViewController;
@class SimpleHeaderView;


/**
 * View controller to show all transfer options.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransfersListViewController : NXTViewController<UITableViewDelegate, UITableViewDataSource> {			
    
@private
    
    /**
     * Transfers options table view
     */
    UITableView *transfersOptionsTableView_;
    
    /**
     * Transfer between products view
     */
    TransferBetweenAccountsViewController *transferBetweenAccountsViewController_;
    
    /**
     * Transfer to third accounts
     */
    TransferToThirdAccountsViewController *transferToThirdAccountsViewController_;
	
	/**
     * Transfer to account from other banks
     */
	TransferToOtherBankAccountViewController *transferToOtherBankAccountViewController_;
    
    /**
     * Transfer to Gift card
     */
	TransferToGiftCardViewController *transferToGiftCardViewController_;
    
    /*
     * Register a third account
     */
    RegisteredAccountListViewController *registeredAccountListViewController_;
    
}


@property (retain,readwrite, nonatomic) IBOutlet UITableView *transfersOptionsTableView;


/**
 * Creates and returns an autoreleased TransfersListViewController constructed from a NIB file.
 *
 * @return The autoreleased TransfersListViewController constructed from a NIB file.
 */
+ (TransfersListViewController *)transfersListViewController;

@end
