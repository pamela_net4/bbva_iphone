/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "CheckComboCell.h"
#import "Constants.h"
#import "TransferToGiftCardViewController.h"
#import "TransfersStepOneViewController+protected.h"
#import "SimpleSelectorAccountView.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"
#import "StringKeys.h"
#import "Tools.h"
#import "MOKLimitedLengthTextField.h"
#import "NXTCurrencyTextField.h"
#import "NXTTextField.h"
#import "NXTEditableViewController+protected.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "TransferToGiftCard.h"
#import "BankAccount.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"TransferToGiftCardViewController"

/*
 * Max lenght of reference field
 */
#define MAX_TEXT_REFERENCE_LENGHT                                   20

/**
 * Defines the max number of chars in each text field
 */
#define MAX_CHARS                                                   4

/*
 *max number office text field
 */
#define MAX_NUMBER_OFFICE                                           4

/*
 *max number account text field
 */
#define MAX_NUMBER_ACCOUNT                                          10


#pragma mark -


/**
 * TransferToMyAccountsViewController private extension
 */
@interface TransferToGiftCardViewController()

/**
 * Releases graphic elements
 */
- (void)releaseTransferToGiftCardViewControllerGraphicElements;

/**
 * Receives the response of the transfer confirmation
 *
 * @param notification The NSNotification object containing the notification information
 * @private
 */
- (void)transferResponseReceived:(NSNotification *)notification;

/**
 * SMS Second text Field is shown.
 *
 * @param on The flag
 */
- (void)smsSecondTextFieldShow:(BOOL)on;

/**
 * Email Second text Field is shown.
 *
 * @param on The flag
 */
- (void)emailSecondTextFieldShow:(BOOL)on;

@end

#pragma mark -

@implementation TransferToGiftCardViewController

#pragma mark -
#pragma mark Properties

@synthesize originAccountLabel = originAccountLabel_;
@synthesize originCombo = originCombo_;
@synthesize destinationLabel = destinationLabel_;
@synthesize cardView = cardView_;
@synthesize firstTextField = firstTextField_;
@synthesize secondTextField = secondTextField_;
@synthesize thirdTextField = thirdTextField_;
@synthesize fourthTextField = fourthTextField_;
@synthesize currencyLabel = currencyLabel_;
@synthesize currencyCombo = currencyCombo_;
@synthesize amountLabel = amountLabel_;
@synthesize amountTextField = amountTextField_;
@synthesize accountTypeTableView = accountTypeTableView_;
@synthesize transferToGiftCardHelper = transferToGiftCardHelper_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransferToGiftCardViewControllerGraphicElements];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationTransferConfirmationResponseReceived
                                                  object:nil];
    
    [transferToGiftCardHelper_ release];
    transferToGiftCardHelper_ = nil;
    
    [editableViews_ release];
    editableViews_ = nil;
    
    [accountTypeTableView_ release];
    accountTypeTableView_ = nil;
    
    [pickerView_ release];
    pickerView_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransferToGiftCardViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransferToGiftCardViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseTransferToGiftCardViewControllerGraphicElements {
    
    [originAccountLabel_ release];
    originAccountLabel_ = nil;
    
    [originCombo_ release];
    originCombo_ = nil;
    
    [destinationLabel_ release];
    destinationLabel_ = nil;
    
	[cardView_ release];
    cardView_ = nil;
    
    [firstTextField_ release];
    firstTextField_ = nil;
    
    [secondTextField_ release];
    secondTextField_ = nil;
    
    [thirdTextField_ release];
    thirdTextField_ = nil;
    
    [fourthTextField_ release];
    fourthTextField_ = nil;
    
    [currencyLabel_ release];
    currencyLabel_ = nil;
    
    [currencyCombo_ release];
    currencyCombo_ = nil;
    
    [amountLabel_ release];
    amountLabel_ = nil;
    
    [amountTextField_ release];
    amountTextField_ = nil;
    
    [editableViews_ removeAllObjects];
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler  styleLabel:originAccountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
	[originAccountLabel_ setText:NSLocalizedString(PAYMENT_SELECT_PAYMENT_MODE_KEY, nil)];
    
    UILabel *buttonLabel = originCombo_.titleLabel;
    buttonLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:10.0f];
    buttonLabel.lineBreakMode = UILineBreakModeTailTruncation;
    
    [NXT_Peru_iPhoneStyler styleLabel:destinationLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [destinationLabel_ setText:NSLocalizedString(GIFT_CARD_KEY, nil)];
    
    [cardView_ setBackgroundColor:[UIColor clearColor]];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:firstTextField_ withFontSize:13.0f andColor:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleMokTextField:secondTextField_ withFontSize:13.0f andColor:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleMokTextField:thirdTextField_ withFontSize:13.0f andColor:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleMokTextField:fourthTextField_ withFontSize:13.0f andColor:[UIColor grayColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:currencyLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [currencyLabel_ setText:NSLocalizedString(TRANSACTION_CURRENCY_TEXT_KEY, nil)];
    currencyLabel_.textAlignment = UITextAlignmentLeft;
    
    buttonLabel = currencyCombo_.titleLabel;
    buttonLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:12.0f];
    buttonLabel.lineBreakMode = UILineBreakModeHeadTruncation;
    
    [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [amountLabel_ setText:NSLocalizedString(TRANSFER_RECHARGE_AMOUNT_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleTextField:amountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    [amountTextField_ setCanContainCents:YES];
    [amountTextField_ setCurrencySymbol:[Tools getCurrencySimbol:CURRENCY_SOLES_LITERAL]];
    [amountTextField_ setDelegate:self];
    [amountTextField_ setPlaceholder:NSLocalizedString(TRANSFER_AMOUNT_HINT_TEXT_KEY, nil)];
    [amountTextField_ setInputAccessoryView:popButtonsView_];

    [originCombo_ setTitle:NSLocalizedString(TRANSFER_SELECT_ACCOUNT_TEXT_KEY, nil)];
    [originCombo_ setDelegate:self];
    [originCombo_ setInputAccessoryView:popButtonsView_];
    
    [currencyCombo_ setDelegate:self];
    [currencyCombo_ setInputAccessoryView:popButtonsView_];
    
    [firstTextField_ setDelegate:self];
    [firstTextField_ setInputAccessoryView:popButtonsView_];
    [secondTextField_ setDelegate:self];
    [secondTextField_ setInputAccessoryView:popButtonsView_];
    [thirdTextField_ setDelegate:self];
    [thirdTextField_ setInputAccessoryView:popButtonsView_];
    [fourthTextField_ setDelegate:self];
    [fourthTextField_ setInputAccessoryView:popButtonsView_];
    
    if ([transferToGiftCardHelper_ hasCard]) {
        [originCombo_ setHidden:TRUE];

        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            accountTypeTableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0,0 , CGRectGetWidth([self.view frame]), 100) style:UITableViewStylePlain];
        }else
            accountTypeTableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0,0 , CGRectGetWidth([self.view frame]), 100) style:UITableViewStyleGrouped];
        
        [accountTypeTableView_ setTag:2];
        
        [NXT_Peru_iPhoneStyler styleTableView:accountTypeTableView_];
        
        [accountTypeTableView_ setDataSource:self];
        [accountTypeTableView_ setDelegate:self];
        [accountTypeTableView_ setScrollEnabled:FALSE];
    
        UIView *superView = [self containerView];
        [superView addSubview:accountTypeTableView_];
        [superView bringSubviewToFront:accountTypeTableView_];
        
        pickerView_ = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 215.0f)];
        
        pickerView_.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        pickerView_.backgroundColor=[UIColor BBVAGreyToneFourColor];
        
        [pickerView_ setDelegate:self];
        [pickerView_ setDataSource:self];
        [pickerView_ setShowsSelectionIndicator:TRUE];
        
    }else{
        if (accountTypeTableView_ != nil) {
            [accountTypeTableView_ removeFromSuperview];
            [accountTypeTableView_ release];
            accountTypeTableView_ = nil;
        }
        
        [originCombo_ setHidden:FALSE];
    }
    
    selectedIndex = -1;
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(transferResponseReceived:)
                                                 name:kNotificationTransferConfirmationResponseReceived
                                               object:nil];
    
	if (shouldClearInterfaceData_) {
        
        originCombo_.informFirstSelection = YES;
        
        [transferToGiftCardHelper_ setSelectedOriginAccountIndex:-1];
        [transferToGiftCardHelper_ setSelectedOriginCardIndex:-1];
        [transferToGiftCardHelper_  setSelectedCurrencyIndex:0];
        [transferToGiftCardHelper_ setAmountString:@""];
        [transferToGiftCardHelper_ setReference:@""];
        [transferToGiftCardHelper_ setShowSMS1:NO];
        [transferToGiftCardHelper_ setShowSMS2:NO];
        [transferToGiftCardHelper_ setSendSMS:NO];
        [transferToGiftCardHelper_ setSelectedCarrier1Index:0];
        [transferToGiftCardHelper_ setSelectedCarrier2Index: -1];
        [transferToGiftCardHelper_ setShowEmail1:NO];
        [transferToGiftCardHelper_ setShowEmail2:NO];
        [transferToGiftCardHelper_ setSendEmail:NO];
        
        [firstTextField_ setText:@""];
        [secondTextField_ setText:@""];
        [thirdTextField_ setText:@""];
        [fourthTextField_ setText:@""];
        
        [transparentScroll_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f) animated:NO];
		
		shouldClearInterfaceData_ = NO;
        selectedIndex = -1;
        if ([transferToGiftCardHelper_ hasCard]) {
            [accountTypeTableView_ reloadData];
        }else{
            [originCombo_ setSelectedIndex:0];
        }
	}
    
    if (editableViews_ == nil) {
        
        editableViews_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [editableViews_ removeAllObjects];
        
    }
    if (![transferToGiftCardHelper_ hasCard]) {
        [editableViews_ addObject:originCombo_];
    }
    [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:
                                         firstTextField_,
                                         secondTextField_,
                                         thirdTextField_,
                                         fourthTextField_,
                                         currencyCombo_ ,amountTextField_,
                                         nil]];
    
    if ([transferToGiftCardHelper_ canSendEmailandSMS]) {
        
        SMSCell *smsCell = [self smsCell];
        
        if ([transferToGiftCardHelper_ sendSMS]) {
            
            if ([transferToGiftCardHelper_ showSMS1]) {
                
                NXTTextField *textField = [smsCell firstTextField];
                
                if (textField != nil) {
                    
                    [editableViews_ addObject:textField];
                    
                }
                
                NXTComboButton *comboButton = [smsCell firstComboButton];
                
                if (comboButton != nil) {
                    
                    [editableViews_ addObject:comboButton];
                    
                }
                
            }
            
            if ([transferToGiftCardHelper_ showSMS2]) {
                
                if ([transferToGiftCardHelper_ showSMS1]) {
                    
                    NXTTextField *textField = [smsCell secondTextField];
                    
                    if (textField != nil) {
                        
                        [editableViews_ addObject:textField];
                        
                    }
                    
                    NXTComboButton *comboButton = [smsCell secondComboButton];
                    
                    if (comboButton != nil) {
                        
                        [editableViews_ addObject:comboButton];
                        
                    }
                    
                }
                
            }
            
        }
        
        if ([transferToGiftCardHelper_ sendEmail]) {
            
            EmailCell *emailCell = [self emailCell];
            
            if ([transferToGiftCardHelper_ showEmail1]) {
                
                NXTTextField *textField = [emailCell firstTextField];
                
                if (textField != nil) {
                    
                    [editableViews_ addObject:textField];
                    
                }
                
            }
            
            if ([transferToGiftCardHelper_ showEmail2]) {
                
                NXTTextField *textField = [emailCell secondTextField];
                
                if (textField != nil) {
                    
                    [editableViews_ addObject:textField];
                    
                }
                
            }
            
            NXTTextView *textView = [emailCell emailTextView];
            
            if (textView != nil) {
                
                [editableViews_ addObject:textView];
                
            }
            
        }
        
    }
    
    [self.view sendSubviewToBack: accountTypeTableView_];
    [self.view bringSubviewToFront: destinationLabel_];
    [self displayStoredInformation];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the confirmation response and stores the information
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationTransferConfirmationResponseReceived
                                                  object:nil];    
    [accountString_ release];
    accountString_ = nil;
    
    [self okButtonClickedInPopButtonsView:popButtonsView_];
    
    [super viewWillDisappear:animated];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased TransferToMyAccountsViewController constructed from a NIB file
 */
+ (TransferToGiftCardViewController *)transferToGiftCardViewController {
    
    return [[[TransferToGiftCardViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
}

#pragma mark -
#pragma mark User interaction

- (IBAction)originComboPressed {
    
    [self editableViewHasBeenClicked:originCombo_];
}


- (IBAction)currencyComboPressed {
    
    [self editableViewHasBeenClicked:currencyCombo_];
}
- (void)comboPressed:(UIView *)combo_{
    if (combo_ != nil) {
        [self editableViewHasBeenClicked:combo_];
    }
}

#pragma mark -
#pragma mark TransfersStepOneViewController selectors

/**
 * Displays the stored information. Sets the information into the views
 */
- (void)displayStoredInformation {
    
    [super displayStoredInformation];
    
    if ([self isViewLoaded]) {
        
        NSMutableArray *array = [NSMutableArray array];
        NSArray *originAccountList = transferToGiftCardHelper_.originAccountList;
        
        for(BankAccount *account in originAccountList) {
            
            [array addObject:account.accountIdAndDescription];
        }
        
        originCombo_.stringsList = [NSArray arrayWithArray:array];
        [array removeAllObjects];
        
        NSArray *currencyList = transferToGiftCardHelper_.currencyList;
        
        for(NSString *currency in currencyList) {
            
            [array addObject:[NSLocalizedString(currency, nil) capitalizedString]];
        }
        
        currencyCombo_.stringsList = [NSArray arrayWithArray:array];
        
        [originCombo_ setSelectedIndex:transferToGiftCardHelper_.selectedOriginAccountIndex];
        [currencyCombo_ setSelectedIndex:transferToGiftCardHelper_.selectedCurrencyIndex];
        
        [amountTextField_ setText:transferToGiftCardHelper_.amountString];
        
    }
}

/**
 * Lays out the views
 */
- (void)layoutViews {
    
    [super layoutViews];
    
    if ([self isViewLoaded]) {
        
        CGRect frame = originAccountLabel_.frame;
        frame.origin.y = VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        CGFloat height = [Tools labelHeight:originAccountLabel_ forText:originAccountLabel_.text];
        frame.size.height = height;
        originAccountLabel_.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        if ([transferToGiftCardHelper_ hasCard])
        {
            
            frame = accountTypeTableView_.frame;
            frame.origin.y = auxPos;
            if (selectedIndex != -1) {
                frame.size.height = 150.0f;
            } else {
                frame.size.height = 100.0f;
            }
            accountTypeTableView_.frame = frame;
            auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        }else
        {
            frame = originCombo_.frame;
            frame.origin.y = auxPos;
            originCombo_.frame = frame;
            auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        }
        
        frame = destinationLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:destinationLabel_ forText:destinationLabel_.text];
        frame.size.height = height;
        destinationLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = [cardView_ frame];
        frame.origin.y = auxPos;
        [cardView_ setFrame:frame];
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = currencyLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:currencyLabel_ forText:currencyLabel_.text];
        frame.size.height = height;
        currencyLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = currencyCombo_.frame;
        frame.origin.y = auxPos;
        currencyCombo_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        frame = amountLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:amountLabel_ forText:amountLabel_.text];
        frame.size.height = height;
        amountLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = amountTextField_.frame;
        frame.origin.y = auxPos;
        amountTextField_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;

        UITableView *selectionTableView = self.selectionTableView;
        [selectionTableView reloadData];
        CGSize contentSize = selectionTableView.contentSize;
        frame = selectionTableView.frame;
        frame.origin.y = auxPos;
        height = contentSize.height;
        frame.size.height = height;
        selectionTableView.frame = frame;
        selectionTableView.scrollEnabled = NO;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UIButton *transferButton = self.transferButton;
        frame = transferButton.frame;
        frame.origin.y = auxPos;
        transferButton.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.size.height = auxPos;
        containerView.frame = frame;
        
        [self recalculateScroll];
        
    }
}

/**
 * Stores the information into the transfer operation helper instance
 */
- (void)storeInformationIntoHelper {
    
    [super storeInformationIntoHelper];
    
    transferToGiftCardHelper_.firstText = firstTextField_.text;
    transferToGiftCardHelper_.secondText = secondTextField_.text;
    transferToGiftCardHelper_.thirdText = thirdTextField_.text;
    transferToGiftCardHelper_.fourthText = fourthTextField_.text;
    transferToGiftCardHelper_.amountString = amountTextField_.text;
}

/**
 * Returns the transfer operation helper associated to the view controller. Returns the transfer between accounts helper
 *
 * @return The transfer operation helper associated to the view controller
 */
- (TransferOperationHelper *)transferOperationHelper {
    
    return transferToGiftCardHelper_;
    
}
#pragma mark -
#pragma mark UITableDelegate / Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView tag] == 2) {
        return 2;
    }
   return [super tableView:tableView numberOfRowsInSection:section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([tableView tag] == 2) {
        return 1;
    }
    return [super numberOfSectionsInTableView:tableView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( [tableView tag] == 2)
    {
        return  [CheckComboCell cellHeightCheckOn:(selectedIndex == indexPath.row)];
    }
    
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([tableView tag] == 2){
        
        CheckComboCell *resultAux = (CheckComboCell *)[tableView dequeueReusableCellWithIdentifier:[CheckComboCell cellIdentifier]];
        
        if (resultAux == nil) {
            resultAux = [CheckComboCell checkComboCell];
            [[resultAux combo] setStringListSelectionButtonDelegate:self];
        }
        //cuentas
        if (indexPath.row == 0) {
            
            [resultAux applyAccountSelectionStyle:YES];
            
            [[resultAux topTextLabel] setText:NSLocalizedString(IAC_ACCOUNT_TITLE_TEXT_KEY, nil)];
            
            NSMutableArray *stringsArray = [[NSMutableArray alloc] initWithArray:[transferToGiftCardHelper_ accountListString]];
            
            [cellAccountCombo_ release];
            cellAccountCombo_ = nil;
            
            cellAccountCombo_ = [[resultAux combo] retain];
            
            if (cellAccountCombo_ == cellCardCombo_) {
                
                [cellCardCombo_ release];
                cellCardCombo_ = nil;
                
            }
            
            [cellAccountCombo_ setOptionStringList:stringsArray];
            [cellAccountCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECT_ACCOUNT_TEXT_KEY, nil)];
            [cellAccountCombo_ setStringListSelectionButtonDelegate:self];
            [cellAccountCombo_ setSelectedIndex:0];
            [cellAccountCombo_ addTarget:self action:@selector(comboPressed:) forControlEvents:UIControlEventTouchUpInside];
            [resultAux setCheckActive:(selectedIndex == indexPath.row)];
            
        } else {
            
            [resultAux applyAccountSelectionStyle:NO];
            
            [[resultAux topTextLabel] setText:NSLocalizedString(PAYMENT_CARDS_TITLE_TEXT_KEY, nil)];
            
            NSMutableArray *stringsArray = [[NSMutableArray alloc] initWithArray:[transferToGiftCardHelper_ cardListString]];
            
            [cellCardCombo_ release];
            cellCardCombo_ = nil;
            
            cellCardCombo_ = [[resultAux combo] retain];
            
            if (cellCardCombo_ == cellAccountCombo_) {
                
                [cellAccountCombo_ release];
                cellAccountCombo_ = nil;
                
            }
            
            [cellCardCombo_ setOptionStringList:stringsArray];
            [cellCardCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECT_CARD_TEXT_KEY, nil)];
            [cellCardCombo_ setStringListSelectionButtonDelegate:self];
            [cellCardCombo_ setSelectedIndex:0];
            [cellCardCombo_ addTarget:self action:@selector(comboPressed:) forControlEvents:UIControlEventTouchUpInside];
            [resultAux setCheckActive:(selectedIndex == indexPath.row)];
        }
        
        return resultAux;
        
    }    
    
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 2)
    {
        
        if (selectedIndex == indexPath.row) {
            return;
        }
        if (indexPath.row == 0) {
            [cellAccountCombo_ setSelectedIndex:0];
            [transferToGiftCardHelper_ setSelectedOriginAccountIndex:0];
            [transferToGiftCardHelper_ setSelectedOriginCardIndex:-1];
            [editableViews_ removeObject:cellCardCombo_];
            [editableViews_ insertObject:cellAccountCombo_ atIndex:0];
            [cellAccountCombo_ setInputAccessoryView:popButtonsView_];
            [cellCardCombo_ setInputAccessoryView:nil];
            [cellAccountCombo_ setInputView:pickerView_];
            [cellCardCombo_ setInputView:nil];

            if (cellCardCombo_ != nil) {
                [cellCardCombo_ release];
                cellCardCombo_ = nil;
            }
            
        }else{
            
            if ([transferToGiftCardHelper_ hasCard]) {
                
                [cellCardCombo_ setSelectedIndex:0];
                [transferToGiftCardHelper_ setSelectedOriginAccountIndex:-1];
                [transferToGiftCardHelper_ setSelectedOriginCardIndex:0];
                [editableViews_ removeObject:cellAccountCombo_];
                [editableViews_ insertObject:cellCardCombo_ atIndex:0];
                [cellCardCombo_ setInputAccessoryView:popButtonsView_];
                [cellAccountCombo_ setInputAccessoryView:nil];
                [cellCardCombo_ setInputView:pickerView_];
                [cellAccountCombo_ setInputView:nil];
                
                if (cellAccountCombo_ != nil) {
                    [cellAccountCombo_ release];
                    cellAccountCombo_ = nil;
                }
            }
        }
        selectedIndex = indexPath.row;
        [tableView reloadData];
        [pickerView_ reloadAllComponents];
      
        [self layoutViews];

    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if ([tableView tag] != 2) {
        return [super tableView:tableView viewForHeaderInSection:section];
    }
    
    return nil;
}

- (CGFloat )tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if ([tableView tag] != 2) {
        return [super tableView:tableView heightForHeaderInSection:section];
    }
    return 0.0f;
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
	return YES;
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self editableViewHasBeenClicked:textField];
    
}

/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    result = [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    if(textField == amountTextField_) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            NSScanner *scanner = [NSScanner scannerWithString:resultString];
            NSInteger resultInteger = 0;
            
            if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                if (resultInteger > 0) {
                    
                    result = YES;
                }
            }
        }
    } else if( textField == firstTextField_ || textField == secondTextField_ ||
              textField == thirdTextField_ ||
              textField == fourthTextField_){
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        NSCharacterSet *numbersSet = [NSCharacterSet decimalDigitCharacterSet];
        
        result = [Tools isValidText:string forCharacterSet:numbersSet];
        
        if (result) {
            
            if (textField == firstTextField_) {
                
                if (newLength == MAX_CHARS && ![string isEqualToString:@""]) {
                    textField.text = [textField.text stringByAppendingString:string];
                    [secondTextField_ becomeFirstResponder];
                    result = NO;
                } else if (newLength > MAX_CHARS) {
                    
                    [secondTextField_ becomeFirstResponder];
                    result = NO;
                    
                }
                
            } else if (textField == secondTextField_) {
                
                if (newLength == MAX_CHARS && ![string isEqualToString:@""]) {
                    textField.text = [textField.text stringByAppendingString:string];
                    [thirdTextField_ becomeFirstResponder];
                    result = NO;
                } else if (newLength > MAX_CHARS) {
                    
                    [thirdTextField_ becomeFirstResponder];
                    result = NO;
                    
                }
                
                
            } else if (textField == thirdTextField_) {
                
                if (newLength == MAX_CHARS && ![string isEqualToString:@""]) {
                    textField.text = [textField.text stringByAppendingString:string];
                    [fourthTextField_ becomeFirstResponder];
                    result = NO;
                }else if (newLength > MAX_CHARS) {
                    
                    [fourthTextField_ becomeFirstResponder];
                    result = NO;
                    
                }
     			
            } else if (textField == fourthTextField_) {
                
                if (newLength == MAX_CHARS && ![string isEqualToString:@""]) {
                    textField.text = [textField.text stringByAppendingString:string];
                    result = NO;
                } else if (newLength > MAX_CHARS) {
                    
                    result = NO;
                    
                }
                
            }
            
            if (textField == fourthTextField_) {
                if (textField.text.length + string.length > MAX_CHARS) {
                    result = NO;
                }
            }
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark NXTComboButtonDelegate selectors

/*
 * Informs the delegate that a value has been selected
 */
- (void)NXTComboButtonValueSelected:(NXTComboButton *)comboButton {
    
    if (comboButton == originCombo_) {
        
        [transferToGiftCardHelper_ setSelectedOriginAccountIndex:comboButton.selectedIndex];
    }
    else if (comboButton == currencyCombo_) {
        
        [transferToGiftCardHelper_ setSelectedCurrencyIndex:currencyCombo_.selectedIndex];
        
        NSString *currency =[transferToGiftCardHelper_ selectedCurrency];
        [amountTextField_ setCurrencySymbol:[Tools getCurrencySimbol:currency]];
    }
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the transfer between accounts operation helper element
 *
 * @param transferBetweenAccountsHelper The new transfer between accounts operation helper element to set
 */
- (void)setTransferToGiftCardHelper:(TransferToGiftCard *)transferToGiftCardHelper {
    
    if (transferToGiftCardHelper != transferToGiftCardHelper_) {
        
        [originCombo_ setSelectedIndex:-1];
        [currencyCombo_ setSelectedIndex:-1] ;
        
        [transferToGiftCardHelper retain];
        [transferToGiftCardHelper_ release];
        transferToGiftCardHelper_ = transferToGiftCardHelper;
        
        [self displayStoredInformation];
        
    }
    
    [transferToGiftCardHelper_ setSecondFactorKey:@""];
    
    [self resetScrollToTopLeftAnimated:NO];
    
}

#pragma mark -
#pragma mark UIPickerView Delegate Datasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if (selectedIndex == 0) {
        NSArray *accounts = [transferToGiftCardHelper_ accountListString];
        return [accounts count];
    }else if (selectedIndex == 1){
        NSArray *cards = [transferToGiftCardHelper_ cardListString];
        return [cards count];
    }
    
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (selectedIndex == 0) {
        NSArray *accounts = [transferToGiftCardHelper_ accountListString];
        return [accounts objectAtIndex:row];
    }else if (selectedIndex == 1){
        NSArray *cards = [transferToGiftCardHelper_ cardListString];
        return [cards objectAtIndex:row];
    }
    return @"";
}

/**
 * Called by the picker view when it needs the view to use for a given row in a given component.
 *
 * @param pickerView An object representing the picker view requesting the data.
 * @param row A zero-indexed number identifying a row of component. Rows are numbered top-to-bottom.
 * @param component A zero-indexed number identifying a component of pickerView. Components are numbered left-to-right.
 * @param view A view object that was previously used for this row, but is now hidden and cached by the picker view.
 */
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    if (view == nil) {
        
        CGSize size = [pickerView rowSizeForComponent:component];
        
        view = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, size.width, size.height)] autorelease];
        
    } else {
        
        NSArray *views = [view subviews];
        
        for (UIView *view in views) {
            
            [view removeFromSuperview];
            
        }
        
    }
    
    // label and style to show text
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 0.0f, CGRectGetWidth([view frame]) - 5.0f, CGRectGetHeight([view frame]))];
    
    if (selectedIndex == 0) {
        NSArray *accounts = [transferToGiftCardHelper_ accountListString];
        [label setText:[accounts objectAtIndex:row]];
    }else if (selectedIndex == 1){
        NSArray *cards = [transferToGiftCardHelper_ cardListString];
        [label setText:[cards objectAtIndex:row]];
    }
    
    [label setBackgroundColor:[UIColor clearColor]];
    [label setMinimumFontSize:12.0f];
    [label setAdjustsFontSizeToFitWidth:YES];
    [label setFont:[UIFont boldSystemFontOfSize:18.0f]];
    [label setLineBreakMode:UILineBreakModeHeadTruncation];
    [view addSubview:label];
    
    return view;
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (selectedIndex == 0) {
        if (cellAccountCombo_ != nil) {
            [transferToGiftCardHelper_ setSelectedOriginAccountIndex:row];
            [transferToGiftCardHelper_ setSelectedOriginCardIndex:-1];
            [cellAccountCombo_ setSelectedIndex:row];
        }
    }else if (selectedIndex == 1){
        if (cellCardCombo_ != nil) {
            [transferToGiftCardHelper_ setSelectedOriginCardIndex:row];
            [transferToGiftCardHelper_ setSelectedOriginAccountIndex:-1];
            [cellCardCombo_ setSelectedIndex:row];
        }
    }
}

#pragma mark -
#pragma mark  MOKStringListSelectionButtonDelegate

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton
{
    if (selectedIndex == 0 && stringListSelectionButton == cellAccountCombo_) {
        
        [transferToGiftCardHelper_ setSelectedOriginAccountIndex:stringListSelectionButton.selectedIndex];
        [transferToGiftCardHelper_ setSelectedOriginCardIndex:-1];
        
    }else if (selectedIndex == 1 && stringListSelectionButton == cellCardCombo_){
        [transferToGiftCardHelper_ setSelectedOriginAccountIndex:-1];
        [transferToGiftCardHelper_ setSelectedOriginCardIndex:stringListSelectionButton.selectedIndex];
        
    }
    
}

#pragma mark -
#pragma mark TransfersStepOneViewController override methods


/**
 * sms switch has been tapped.
 */
- (void)switchButtonHasBeenTapped:(BOOL)on {
    
    [super switchButtonHasBeenTapped:on];
    
    if (on) {
        
        NSUInteger position = [editableViews_ indexOfObject:self.emailCell.firstTextField];
        
        if (position == NSNotFound) {
            
            [editableViews_  addObject:self.smsCell.firstTextField];
            [editableViews_  addObject:self.smsCell.firstComboButton];
            
        } else {
            
            [editableViews_ insertObject:self.smsCell.firstTextField atIndex:position];
            [editableViews_ insertObject:self.smsCell.firstComboButton atIndex:position + 1];
            
        }
        
    } else {
        
        [editableViews_  removeObject:self.smsCell.firstTextField];
        [editableViews_  removeObject:self.smsCell.firstComboButton];
        
        NSUInteger position = [editableViews_ indexOfObject:self.smsCell.secondTextField];
        
        if (position != NSNotFound) {
            
            [editableViews_  removeObject:self.smsCell.secondTextField];
            [editableViews_  removeObject:self.smsCell.secondComboButton];
            
        }
        
    }
    
}

/**
 * More button has been tapped
 */
- (void)moreButtonHasBeenTapped {
    
    [super moreButtonHasBeenTapped];
    
    [self smsSecondTextFieldShow:YES];
}

/**
 * SMS Second text Field is shown.
 *
 * @param on The flag
 */
- (void)smsSecondTextFieldShow:(BOOL)on {
    
    if (on) {
        
        NSUInteger position = [editableViews_ indexOfObject:self.smsCell.firstComboButton];
        
        [editableViews_ insertObject:self.smsCell.secondTextField atIndex:position + 1];
        [editableViews_ insertObject:self.smsCell.secondComboButton atIndex:position + 2];
        
    } else {
        
        [editableViews_  removeObject:self.smsCell.secondTextField];
        [editableViews_  removeObject:self.smsCell.secondComboButton];
        
    }
    
}

/**
 * Switch has been tapped.
 *
 * @param on The flag
 */
- (void)emailSwitchButtonHasBeenTapped:(BOOL)on {
    
    [super emailSwitchButtonHasBeenTapped:on];
    
    if (on) {
        
        [editableViews_  addObject:self.emailCell.firstTextField];
        [editableViews_  addObject:self.emailCell.emailTextView];
        
    } else {
        
        [editableViews_  removeObject:self.emailCell.firstTextField];
        
        
        NSUInteger position = [editableViews_ indexOfObject:self.emailCell.secondTextField];
        
        if (position != NSNotFound) {
            
            [editableViews_  removeObject:self.emailCell.secondTextField];
            
        }
        
        [editableViews_  removeObject:self.emailCell.emailTextView];
        
    }
}

/**
 * More button has been tapped
 */
- (void)emailMoreButtonHasBeenTapped {
    
    [super emailMoreButtonHasBeenTapped];
    
    [self emailSecondTextFieldShow:YES];
}

/**
 * Email Second text Field is shown.
 *
 * @param on The flag
 */
- (void)emailSecondTextFieldShow:(BOOL)on {
    
    if (on) {
        
        NSUInteger position = [editableViews_ indexOfObject:self.emailCell.emailTextView];
        
        if (position != NSNotFound) {
            
            [editableViews_ insertObject:self.emailCell.secondTextField atIndex:position];
            
        }
        
    } else {
        
        [editableViews_  removeObject:self.emailCell.secondTextField];
        
    }
    
}


#pragma mark -
#pragma mark Notifications management

/*
 * Receives the response of the transfer confirmation
 */
- (void)transferResponseReceived:(NSNotification *)notification {
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (!response.isError) {
        
        
        if ([transferToGiftCardHelper_ transferResponseReceived:response]) {
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationTransferConfirmationResponseReceived object:nil];
            
            [self displayTransferSecondStepView];
            
        } else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
            
        }
        
    }
    
}

@end

