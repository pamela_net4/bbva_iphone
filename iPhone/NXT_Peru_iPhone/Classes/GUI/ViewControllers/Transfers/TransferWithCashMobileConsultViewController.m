//
//  TransferWithCashMobileConsultViewController.m
//  NXT_Peru_iPad
//
//  Created by R Herrera on 17/09/13.
//
//

#import "TransferWithCashMobileConsultViewController.h"
#import "DetailCashMobileCell.h"
#import "TransferDetailAdditionalInformation.h"
#import "Updater.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "TransferShowDetailResponse.h"
#import "Constants.h"
#import "StringKeys.h"
#import "Tools.h"
#import "TransferDetailCashMobile.h"
#import "TransfersStepThreeViewController.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"
#import "NXTNavigationItem.h"
#import "SimpleHeaderView.h"

@interface TransferWithCashMobileConsultViewController ()

@end

@implementation TransferWithCashMobileConsultViewController

#define TRANSFERS_VIEW_CONTROLLER_NIB_FILE                      @"TransferWithCashMobileConsultViewController"

@synthesize movementsList = movementsList_;
@synthesize transferWithCashMobileHelper = transferWithCashMobileHelper_;
@synthesize messageEmptyDataLabel = messageEmptyDataLabel_;

#pragma mark - initialization methods
+ (TransferWithCashMobileConsultViewController *)TransferWithCashMobileViewController
{
    TransferWithCashMobileConsultViewController *result = [[[TransferWithCashMobileConsultViewController alloc] initWithNibName:TRANSFERS_VIEW_CONTROLLER_NIB_FILE
                                                                                                           bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view bringSubviewToFront:_table];
    
    if ([movementsList_ count] > 0) {
        [messageEmptyDataLabel_ setHidden:TRUE];
    }
    
    [messageEmptyDataLabel_ setText:NSLocalizedString(NO_MOVEMENTS_TO_LIST_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
    [NXT_Peru_iPhoneStyler styleTableView:_table];
    [NXT_Peru_iPhoneStyler styleLabel:messageEmptyDataLabel_ withFontSize:14.0 color:[UIColor BBVABlueColor]];
    
    operationsCounts_ = [[NSMutableDictionary alloc] initWithCapacity:[movementsList_ count]];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:TRUE];
    
    [_table reloadData];
    
    if ([movementsList_ count] > 0) {
        [messageEmptyDataLabel_ setHidden:TRUE];
    }else{
        [messageEmptyDataLabel_ setHidden:FALSE];
        [self.view bringSubviewToFront:messageEmptyDataLabel_];
    }

}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:TRUE];
    
}

#pragma mark - UITableDelegate Datasource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 86.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"Cellidentifier";
    
    DetailCashMobileCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        
        cell = [[DetailCashMobileCell detailCashMobileCell] retain];
        
    }
    
    cell.isHeader = FALSE;
    [cell setStyleCell];
    
    TransferDetailAdditionalInformation *additionalInfo = [movementsList_ objectAtIndex:indexPath.row];
    
    cell.lblPhoneNumber.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(BENEFICIARY_DETAIL_CASH_MOBILE_TITLE,nil), additionalInfo.beneficiary];
    cell.lblAmount.text = [NSString stringWithFormat:@"%@ %@", additionalInfo.currencyAmountToPay, additionalInfo.amountToPay];
    cell.lblDate.text = additionalInfo.dateString;
    cell.lblState.text = additionalInfo.transferState;
    cell.lblOperationNumber.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(NUM_OPER_DETAIL_CASH_MOBILE_TITLE,nil), additionalInfo.operationNumber];
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{/*
    if ([movementsList_ count] > 0) {
        return [movementsList_ count] + 1;
    }else
        return 0;
  */
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [movementsList_ count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    
    TransferDetailAdditionalInformation *additionalInfo = [movementsList_ objectAtIndex:indexPath.row];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(confirmTransferResult:)
                                                 name:kNotificationTransferWithCashMobileDetailEnds
                                               object:nil];
    
    [self.appDelegate showActivityIndicator:poai_Both];
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    
    [user setObject:additionalInfo.operationNumber forKey:@"lastCashMobileConsult"];
    
    [user synchronize];
    
    [[Updater getInstance] obtainCashMobileTransactionDetailWithOperationCode:additionalInfo.operationNumber];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [SimpleHeaderView height];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SimpleHeaderView *result = [SimpleHeaderView simpleHeaderView];
    
    [result setTitle:NSLocalizedString(CASH_MOBILE_MOVEMENTS_KEY, nil)];
    
    return result;
}

#pragma mark -
#pragma mark Notifications

/*
 * Account has been updated.
 */
- (void)confirmTransferResult:(NSNotification *)notification {
    
    //DLog( @"confirmTransferResult" );
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationTransferWithCashMobileDetailEnds
                                                  object:nil];
    
    [self.appDelegate hideActivityIndicator];
    
    TransferShowDetailResponse *response = [notification object];
    
    if (transferWithCashMobileHelper_ != nil) {
        [transferWithCashMobileHelper_ release];
        transferWithCashMobileHelper_ = nil;
    }
    
    transferWithCashMobileHelper_ = [[TransferDetailCashMobile alloc] initWithTransferStartupResponse:response.additionalInformation];
    
    UIAlertView *alert = nil;
    
    if (![response isError]) {
        
        if (transferWithCashMobileHelper_){//[self.transferOperationHelper confirmationResponseReceived:response]) {
            
            [self displayTransferThirdStepView];
            
        } else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
            
        }
        
    } else {
        
        alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(INFO_MESSAGE_TITLE_KEY, nil)
                                            message:[response errorMessage]
                                           delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:NSLocalizedString(OK_TEXT_KEY, nil), nil] autorelease];
        
        [alert show];
        
    
    }
}

- (void)confirmResendTransfer
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ResendSuccess" object:nil];
    //decrement the number of allowed resends
    NSNumber *maxTimes = [operationsCounts_ objectForKey:lastOperationNumber_];
    
    int maxTimesInteger = [maxTimes intValue];
    
    [operationsCounts_ setObject:[NSNumber numberWithInt:maxTimesInteger-1] forKey:lastOperationNumber_];
}
#pragma mark -
#pragma mark Displaying views

/*
 * Displays the transfer third step view
 */
- (void)displayTransferThirdStepView {
    
    if (transfersStepThreeViewController_ == nil) {
        
        transfersStepThreeViewController_ = [[TransfersStepThreeViewController transfersStepThreeViewController] retain];
        
    }
    transfersStepThreeViewController_.isDetail = TRUE;
    
    [lastOperationNumber_ release];
    lastOperationNumber_ = nil;
    lastOperationNumber_ = transferWithCashMobileHelper_.transferSuccessAdditionalInfo.operationNumber;
    [lastOperationNumber_ retain];
    
    NSNumber *maxTimes = [operationsCounts_ objectForKey:lastOperationNumber_];
    
    if (maxTimes == nil) {
        
        maxTimes = [NSNumber numberWithInt:[transferWithCashMobileHelper_.transferSuccessAdditionalInfo.maxResendString intValue]];
        
        [operationsCounts_ setObject:maxTimes forKey:lastOperationNumber_];
    }
    
    
    if ([maxTimes intValue] > 0 &&
        [[NSLocalizedString(STATE_RESEND_ACTIVE_TEXT_KEY, nil) lowercaseString] isEqualToString:[transferWithCashMobileHelper_.transferSuccessAdditionalInfo.transferState lowercaseString]])
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ResendSuccess" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(confirmResendTransfer) name:@"ResendSuccess" object:nil];
        transfersStepThreeViewController_.canResend = TRUE;
    }else{
        transfersStepThreeViewController_.canResend = FALSE;
    }
    
    transfersStepThreeViewController_.transferOperationHelper = self.transferOperationHelper;
    [self.navigationController pushViewController:transfersStepThreeViewController_ animated:YES];
    
}

#pragma mark - DetailCashMobileDelegate
- (void)onTapArrowButton:(int)index
{

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(confirmTransferResult:)
                                                 name:kNotificationTransferWithCashMobileDetailEnds
                                               object:nil];
    
    [self.appDelegate showActivityIndicator:poai_Both];
    
    [[Updater getInstance] obtainCashMobileTransactionDetailWithOperationCode:[NSString stringWithFormat:@"%d", index]];
}

/**
 * Returns the transfer operation helper associated to the view controller. Returns the transfer between accounts helper
 *
 * @return The transfer operation helper associated to the view controller
 */
- (TransferOperationHelper *)transferOperationHelper {
    
    return transferWithCashMobileHelper_;
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = NSLocalizedString(TRANSFER_WITH_CASH_MOBILE_TEXT_1_KEY, nil);
    
    return result;
    
}


#pragma mark - memory management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [movementsList_ release];
    [_table release];
    [messageEmptyDataLabel_ release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setTable:nil];
    [self setMessageEmptyDataLabel:nil];
    [super viewDidUnload];
}
@end
