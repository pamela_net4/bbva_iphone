//
//  RegisteredAccountListViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/14/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "RegisteredAccountListViewController.h"

#import "RegistrationTransferThirdCardViewController.h"
#import "ThirdAccountListResponse.h"
#import "ThirdAccountList.h"
#import "ThirdAccount.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "Updater.h"
#import "CheckAccountView.h"
#import "ThirdAccountOperator.h"
#import "TransfersStepTwoViewController.h"

#pragma mark -
#pragma mark Static attributes

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"RegisteredAccountListViewController"

/**
 * Defines statics values
 */
#define INITIAL_TABLE_HEIGHT                                        238
#define INITIAL_TABLE_HEIGHT_4Inch                                  338

@interface RegisteredAccountListViewController ()

@end

@implementation RegisteredAccountListViewController

@synthesize registerButton = registerButton_;
@synthesize deleteButton = deleteButton_;
@synthesize accountArray = accountArray_;
@synthesize accountsTable = accountsTable_;
@synthesize hasForward = hasForward_;
@synthesize isProactive = isProactive_;
@synthesize delegate = delegate_;
@synthesize operationListType = operationListType_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [registerButton_ release];
    registerButton_ = nil;
    
    [accountArray_ release];
    accountArray_ = nil;
    
    [accountsTable_ release];
    accountsTable_ = nil;
    
   
    
    if (registrationTransferThirdCardViewController_ != nil) {
        [registrationTransferThirdCardViewController_ release];
        registrationTransferThirdCardViewController_ =  nil;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationRegisterAccountInitialListResponse object:nil];
    
    [super dealloc];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark intiailization methods

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    accountArray_ = [[NSArray alloc] init];
}

/*
 * Creates and returns an autoreleased RegisteredAccountListViewController constructed from a NIB file.
 */
+ (RegisteredAccountListViewController *)registeredAccountListViewController {
    
    RegisteredAccountListViewController *result =  [[[RegisteredAccountListViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Do any additional setup after loading the view from its nib.
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        self.navigationController.navigationBar.tintColor = [UIColor BBVABlueSpectrumColor];
        
    } else
    {
        self.navigationController.navigationBar.barTintColor = [UIColor BBVABlueSpectrumColor];
        self.navigationController.navigationBar.translucent = NO;
        
    }
    
    hasForward_ = FALSE;
    
    page_ = 1;
    
    [NXT_Peru_iPhoneStyler styleBlueButton:registerButton_];
       [NXT_Peru_iPhoneStyler styleBlueButton:deleteButton_];
   
    
    if(isProactive_)
    {
        [registerButton_ setTitle:NSLocalizedString(REGISTER_ACCOUNT_TITLE_KEY, nil) forState:UIControlStateNormal];
        [deleteButton_ setTitle:NSLocalizedString(DELETE_ACCOUNT_TITLE_KEY, nil) forState:UIControlStateNormal];
    }
    else{
        [registerButton_ setTitle:NSLocalizedString(OK_TEXT_KEY, nil) forState:UIControlStateNormal];
        [deleteButton_ setTitle:NSLocalizedString(REGISTER_ACCOUNT_TITLE_KEY, nil) forState:UIControlStateNormal];
    }
    
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:TRUE];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.appDelegate setTabBarVisibility:YES animated:YES];
    
    if (!hasForward_) {
        page_ = 1;
        selectedIndex_ = -1;
        [accountsTable_ setBackgroundColor:[UIColor clearColor]];
        [accountsTable_ setHidden:TRUE];
        if(isProactive_)
        {
            [registerButton_ setHidden:NO];
            [deleteButton_ setHidden:NO];
        }else{
            [deleteButton_ setHidden:NO];
            [registerButton_ setHidden:NO];
        }
        [accountsTable_ setDelegate:self];
        [accountsTable_ setDataSource:self];
        [self listThirdAccountOperationRequest:@"masDatos"];
        
    }else{
        
        hasForward_ = FALSE;
        if(initialResponse.backPage ==nil ){
            
            [self listThirdAccountOperationRequest:@"masDatos"];

        }

    }
    
}

#pragma mark -
#pragma mark user interactions
/**
 * delete an account
 *
 */
- (IBAction)onTapDelete:(id)sender{
    
    if(isProactive_)
    {
        [self deleteAccountAction];
        
    }
    else
    {
        [self registerAccountAction];
    }
    
}

-(void)deleteAccountAction
{
    if (selectedIndex_ == -1 || selectedIndex_ > [accountArray_ count]) {
        [Tools showAlertWithMessage:NSLocalizedString(@"Seleccione un alias inscrito", nil)];
        return;
    }
    
    [self.appDelegate showActivityIndicator:poai_Both];
    
    ThirdAccount *thirdAccountObject = [accountArray_ objectAtIndex:selectedIndex_];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteAccountResponse:) name:kNotificationTransferConfirmationResponseReceived object:nil];
    
    thirdAccountOperatorHelper_ = [[ThirdAccountOperator  alloc] initWithTransferStartupResponse:thirdAccountObject];
    
    [thirdAccountOperatorHelper_  startDeleteRequest];

}

/**
 * register an account
 *
 */
- (IBAction)onTapRegister:(id)sender{
   
  
    if(isProactive_)
    {
        [self registerAccountAction];
      }
    else{
        if (selectedIndex_ == -1 || selectedIndex_ > [accountArray_ count]) {
            [Tools showAlertWithMessage:NSLocalizedString(@"Seleccione un alias inscrito", nil)];
            return;
        }
        
        hasForward_ = FALSE;
        [delegate_ selectedAccountFromList:[accountArray_ objectAtIndex:selectedIndex_]];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

-(void) registerAccountAction
{
    
    if (registrationTransferThirdCardViewController_ != nil) {
        registrationTransferThirdCardViewController_ = nil;
        [registrationTransferThirdCardViewController_ release];
    }
    
    registrationTransferThirdCardViewController_ = [[RegistrationTransferThirdCardViewController registrationTransferThirdCardViewController] retain];
    
    [registrationTransferThirdCardViewController_ setThirdAccountOperatorHelper:[[ThirdAccountOperator alloc] init] ];
    
    [self.navigationController pushViewController:registrationTransferThirdCardViewController_ animated:TRUE];
    
    
    hasForward_ = TRUE;

}

#pragma mark -
#pragma mark Request operations
/**
 * Request the list of frequent operations
 */
- (void)listThirdAccountOperationRequest:(NSString *)action{
    
    [self.appDelegate showActivityIndicator:poai_Both];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initialAccountListResponse:) name:kNotificationRegisterAccountInitialListResponse object:nil];
    [[Updater getInstance] obtainThirdAccountInformationInitialListInfo:@"-1" type:operationListType_];
}

#pragma mark -
#pragma mark Notification
#pragma mark -
#pragma mark Notification
-(void)initialAccountListResponse:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationRegisterAccountInitialListResponse object:nil];
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (response != nil && ![response isError]) {
        
        if (initialResponse != nil) {
            
            [initialResponse release];
            initialResponse = nil;
        }
        
        initialResponse = (ThirdAccountListResponse *)[notification object];
        
        if (accountArray_ != nil) {
            [accountArray_ release];
            accountArray_ = nil;
        }
        
        accountArray_ = [[NSArray alloc] initWithArray:[initialResponse.thirdAccountList  thirdAccountList]];
        
        selectedIndex_ = -1;
        
        [accountsTable_ reloadData];
        
        if ([accountArray_ count] == 0) {
            
            if(!isProactive_){
                [registerButton_ setHidden:NO];
                 [deleteButton_ setHidden:NO];
            }
            
            if(isProactive_){
                [deleteButton_ setHidden:NO];
                [registerButton_ setHidden:NO];
            }
            
            [accountsTable_ setHidden:TRUE];
        }else{
            
            [registerButton_ setHidden:FALSE];
            [deleteButton_ setHidden:FALSE];
            [accountsTable_ setHidden:FALSE];
            
            CGRect tableFrame = [accountsTable_ frame];
            
            if ([accountsTable_ contentSize].height < INITIAL_TABLE_HEIGHT) {
                
                tableFrame.size.height = [accountsTable_ contentSize].height;
                [accountsTable_ setFrame:tableFrame];
            }else{
                tableFrame.size.height = (isiPhone5)?INITIAL_TABLE_HEIGHT_4Inch:INITIAL_TABLE_HEIGHT;
                [accountsTable_ setFrame:tableFrame];
            }
            
            CGRect buttonframe=registerButton_.frame;
            buttonframe.origin.y=CGRectGetMaxY(tableFrame)+8;
            [registerButton_ setFrame:buttonframe];
            
            buttonframe=registerButton_.frame;
            CGRect deleteButtonFrame=deleteButton_.frame;
            deleteButtonFrame.origin.y=CGRectGetMaxY(buttonframe)+5;
            [deleteButton_ setFrame:deleteButtonFrame];
            
            [accountsTable_ scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:FALSE];
        }
        
        [initialResponse retain];
        
    }
    
}
-(void)deleteAccountResponse:(NSNotification*)notification
{
        
        [self.appDelegate hideActivityIndicator];
        
        StatusEnabledResponse *response = [notification object];
        
        if (!response.isError) {
            
            if([thirdAccountOperatorHelper_ transferResponseReceived:response])
                [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationTransferConfirmationResponseReceived object:nil];
            [self displayTransferSecondStepView];
            
        }else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
            
        }
        
}


#pragma mark -
#pragma mark UITableDelegate - UITableDatasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [accountArray_ count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [CheckAccountView cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CheckAccountView *cell = [tableView dequeueReusableCellWithIdentifier:[CheckAccountView cellIdentifier]];
    
    if (cell == nil) {
        cell = [[[CheckAccountView checkAccountView] retain] autorelease];
    }
    
    ThirdAccount *TAObject = [accountArray_ objectAtIndex:indexPath.row];
    
    [cell.titleLabel setText:TAObject.nickName];
    if([operationListType_ isEqualToString:@"TODO"]){
    [cell.subtitleLabel setText:TAObject.operationType];
    [cell.subtitleLabel2 setText:TAObject.accountNumber];

    [cell.dayLabel setText: TAObject.operationDate];
    }else{
        [cell.subtitleLabel setText:TAObject.accountNumber];
        [cell.subtitleLabel2 setText:TAObject.operationDate];

    }

    [cell setCheckActive:(selectedIndex_ == indexPath.row)];
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedIndex_ = indexPath.row;
    
    [accountsTable_ reloadData];
}
#pragma mark -
#pragma mark Scroll delegate
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    
    if ((currentOffset > 0) && (maximumOffset - currentOffset) <= 10)
    {
        if(initialResponse != nil)
        {
            [self loadNextFOSearchPaged];
        }
        
    }
    else if(currentOffset<=0)
    {
        [self loadBackFOSearchPaged];
    }
    
}

-(void)loadBackFOSearchPaged
{
    if([initialResponse backPage] == nil)
        return;
    
    [[self appDelegate] showActivityIndicator:poai_Both];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initialAccountListResponse:) name:kNotificationRegisterAccountInitialListResponse object:nil];
    
    [[Updater getInstance] obtainThirdAccountInformationInitialListInfo:initialResponse.backPage type:operationListType_];
    
}

-(void)loadNextFOSearchPaged
{
    if(![initialResponse hasMoreData])
        return;
    
    [self.appDelegate showActivityIndicator:poai_Both];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initialAccountListResponse:) name:kNotificationRegisterAccountInitialListResponse object:nil];
    
  
    
    [[Updater getInstance] obtainThirdAccountInformationInitialListInfo:initialResponse.nextPage type:operationListType_];
    
    
    
}
#pragma mark -
#pragma mark Displaying views

/*
 * Displays the transfer second step view controller
 */
- (void)displayTransferSecondStepView {
    
    if (transfersStepTwoViewController_ != nil) {
        
        transfersStepTwoViewController_ = nil;
        [transfersStepTwoViewController_ release];
    }
    
    transfersStepTwoViewController_ = [[TransfersStepTwoViewController transfersStepTwoViewController] retain];
    thirdAccountOperatorHelper_.deleteFlag = YES;
    TransferOperationHelper *transferOperationHelper = thirdAccountOperatorHelper_;
    transfersStepTwoViewController_.transferOperationHelper = transferOperationHelper;
    transfersStepTwoViewController_.isCashMobileSend = NO;
    [self.navigationController pushViewController:transfersStepTwoViewController_
                                         animated:YES];
    
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [self customNavigationItem];
    if(isProactive_){
    [[result customTitleView] setTopLabelText:NSLocalizedString(@"Inscripción Cuentas de Terceros", nil)];
    }
    else
    {
        [[result customTitleView] setTopLabelText:NSLocalizedString(@"Elige una cuenta inscrita", nil)];
    }
    
    return result;
    
}
@end
