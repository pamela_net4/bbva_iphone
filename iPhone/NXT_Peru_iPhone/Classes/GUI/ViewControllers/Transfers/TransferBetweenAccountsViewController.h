/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TransfersStepOneViewController.h"
#import "TransfersConstants.h"
#import "NXTComboButton.h"

//Forward declarations
@class NXTTextField;
@class NXTCurrencyTextField;
@class TransferBetweenAccounts;

/**
 * View to transfer money from an account of the user to an account of the user
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferBetweenAccountsViewController : TransfersStepOneViewController <UITextFieldDelegate,NXTComboButtonDelegate> {
    
@private
    
    /**
     * Origin account label
     */
    UILabel *originAccountLabel_;
	
	/**
     * Origin account combo
     */
    NXTComboButton *originCombo_;
    
    /**
     * Destination account label
     */
    UILabel *destinationAccountLabel_;
	
	/**
     * Destination account combo
     */
    NXTComboButton *destinationCombo_;
    
    /*
     *Currency Label
     */
    UILabel *currencyLabel_;
    
    /*
     *Currency combo
     */
    NXTComboButton *currencyCombo_;
    
    /*
     *Amount Label
     */
    UILabel *amountLabel_;
    
    /**
     * Transfer amount text field
     */
    NXTCurrencyTextField *amountTextField_;
    
    /*
     *Reference label;
     */
    UILabel *referenceLabel_;
    
    /*
     *Reference Text field;
     */
    NXTTextField *referenceTextField_;
    
    
    /**
     * Transfer between accounts helper element
     */
    TransferBetweenAccounts *transferBetweenAccountsHelper_;
    
}

/**
 * Provides readwrite access to the originAccountLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *originAccountLabel;

/**
 * Provides readwrite access to the originCombo button. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *originCombo;

/**
 * Provides readwrite access to the destinationAccountLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *destinationAccountLabel;

/**
 * Provides readwrite access to the destinationCombo button. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *destinationCombo;

/**
 * Provides readwrite access to the currency label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *currencyLabel;

/**
 *Provides readwrite access to the currencyCombo button. Exported to ID
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *currencyCombo;

/*
 *Provides readwrite access to the amount label. Exported to ID
 */
@property (retain, nonatomic) IBOutlet UILabel *amountLabel;


@property (retain, nonatomic) IBOutlet NXTCurrencyTextField *amountTextField;

/**
 * Provides readwrite access to the reference Label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *referenceLabel;

/**
 * Provides readwrite access to the referenceTextField. Exported to IB
 */

@property (retain, nonatomic) IBOutlet NXTTextField *refenceTextField;

/**
 * Provides read-write access to the transfer between accounts helper element
 */
@property (nonatomic, readwrite, retain) TransferBetweenAccounts *transferBetweenAccountsHelper;

/**
 * Creates and returns an autoreleased TransferToMyAccountsViewController constructed from a NIB file
 *
 * @return The autoreleased TransferToMyAccountsViewController constructed from a NIB file
 */
+ (TransferBetweenAccountsViewController *)transferBetweenAccountsViewController;

/**
 * Invoked by framework when the origin combo is tapped. Sets it as the editing control
 */
- (IBAction)originComboPressed;

/**
 * Invoked by framework when the destination combo is tapped. Sets it as the editing control
 */
- (IBAction)destinationComboPressed;

/*
 *Invoked by framework when the currency combo is tapped. Sets it as the editing control
 */
- (IBAction)currencyComboPressed;

@end
