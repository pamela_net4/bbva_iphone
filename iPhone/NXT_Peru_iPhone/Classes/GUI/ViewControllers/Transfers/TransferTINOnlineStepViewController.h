//
//  TransferTINOnlineStepViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 10/2/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "NXTEditableViewController.h"
#import "NXTComboButton.h"
#import "OtherBankExtraView.h"
#import "PaymentBaseProcess.h"
#import "FOOperationHelper.h"
#import "MOKInputAccessoryView.h"

@class PaymentConfirmationViewController;
@class OwnAccountView;
@class TransferToOtherBankAccounts;
@class OtherBankExtraView;
@class SimpleHeaderView;
@class PaymentCOtherBankProcess;
@class TransferToOtherBankAccounts;
@class TransfersStepTwoViewController;
@class MOKTextField;
@class TermsAndConsiderationsViewController;

@interface TransferTINOnlineStepViewController : NXTEditableViewController <UITextFieldDelegate,NXTComboButtonDelegate,OtherBankExtraViewDelegate,
    PaymentBaseProcessDelegate,MOKInputAccessoryViewDelegate,
    UITableViewDataSource,UITableViewDelegate>
{
    /**
     * Operation title header
     */
    SimpleHeaderView *operationTitleHeader_;

    /**
     * Branding image view
     */
    UIImageView *brandingImageView_;
    
    /**
     * Separator image view
     */
    UIImageView *separator_;


    /**
     * Container view to contain the editable components
     */
    UIView *containerView_;
    
    /*
     * Type transfer label
     */
    UILabel *typeTransferLabel_;
    
    /*
     * Type transfer Table
     */
    UITableView *typeTransferTableView_;
    
    /**
     * Send button
     */
    UIButton *transferButton_;
    
    /*
     * transfer operation helper
     */
    TransferToOtherBankAccounts *TransferToOtherBankAccountsHelper_;
    
    
    PaymentCOtherBankProcess *processCard_;
    
    /**
     *  Helper for frequent operation
     */
    FOOperationHelper *foOperationHelper_;
    
    /**
     * Payment Confirmation View Controller
     */
    PaymentConfirmationViewController *paymentConfirmationViewController_;
    
    /**
     * Terms And Considerations View Controller
     */
    TermsAndConsiderationsViewController *termsAndConsiderationsViewController_;
    
    /*
     * own account label
     */
    UILabel *ownAccountLabel;
    
    /*
     * own account view
     */
    OwnAccountView *ownAccountView_;
    
    OtherBankExtraView *otherBankExtraView_;
    
    /**
     * Beneficiary label
     */
    UILabel *beneficiaryLabel_;
    
    
    /**
     * Input accessory view
     */
    MOKInputAccessoryView *inputAccessoryView_;

    
    /**
     * Beneficiary text field
     */
    MOKTextField *beneficiaryTextField_;

    /**
     * Seleccton option
     */
    NSInteger selectedIndex_;
    
    
    /**
     * Navigation flag
     */
    BOOL hasNavigateForward_;
    
    /**
     * Transfers step two view controller
     */
    TransfersStepTwoViewController *transfersStepTwoViewController_;
    
    UIView *alertMessageBannerView_;
    
    UIImageView *topAletViewBannerSeparator_;
    
    UIImageView *bottomAletViewBannerSeparator_;
    
    UIButton *iconAlertViewBanner_;
    
    UILabel *messageLabelViewBanner_;
    
    NSString *disclaimer_;
}

/**
 * Provides read-write access to the transfer between accounts helper element
 */
@property (nonatomic, readwrite, retain) TransferToOtherBankAccounts *TransferToOtherBankAccountsHelper;


/**
 * Provides read-write access to the transfer between accounts helper element
 */
@property (nonatomic, readwrite, retain) PaymentCOtherBankProcess *processCard;


@property (nonatomic, readwrite, retain)  FOOperationHelper *foOperationHelper;
/**
 * Provides readwrite access to the own account label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *ownAccountLabel;


/**
 * Provides readwrite access to the type transfer statement label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *typeTransferLabel;
/**
 * Provides readwrite access to container view to contain the editable components and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *containerView;

/**
 * Provides readwrite access to the originAccountLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UITableView *typeTransferTableView;

/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;

/**
 * Provides readwrite access to the separator image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

/**
 * Provides readwrite access to the transfer button. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *transferButton;


/**
 * Provides read-write access to the beneficiaryLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *beneficiaryLabel;

/**
 * Provides read-write access to the beneficiaryTextField and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKTextField *beneficiaryTextField;


/**
 * Provides read-write access to the alertMessageBannerView and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *alertMessageBannerView;

/**
 * Provides read-write access to the topAletViewBannerSeparator and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *topAletViewBannerSeparator;

/**
 * Provides read-write access to the bottomAletViewBannerSeparator and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *bottomAletViewBannerSeparator;
/**
 * Provides read-write access to the messageLabelViewBanner and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *messageLabelViewBanner;



/**
 * Provides read-write access to the iconAlertViewBanner and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *iconAlertViewBanner;

/**
 * Provides read-write access to the hasNavigateForward
 */
@property (nonatomic, readwrite, assign) BOOL hasNavigateForward;



/**
 * Provides read-write access to the disclaimer and exports it to the IB
 */
@property (nonatomic, readwrite, retain) NSString *disclaimer;
/**
 * Creates and returns an autoreleased TransferToMyAccountsViewController constructed from a NIB file
 *
 * @return The autoreleased TransferToMyAccountsViewController constructed from a NIB file
 */
+ (TransferTINOnlineStepViewController *)transferTINOnlineStepViewController;

-(IBAction)showLegalTerms:(id)sender;

@end
