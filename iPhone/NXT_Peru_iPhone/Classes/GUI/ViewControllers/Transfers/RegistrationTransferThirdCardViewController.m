//
//  RegistrationTransferThirdCardViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/5/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "RegistrationTransferThirdCardViewController.h"

#import "Updater.h"
#import "Constants.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXTComboButton.h"
#import "NXTEditableViewController+protected.h"
#import "NXTNavigationItem.h"
#import "NXTTextField.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"
#import "SimpleHeaderView.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "StringKeys.h"
#import "Tools.h"
#import "SimpleSelectorAccountView.h"
#import "ThirdAccountOperator.h"
#import "StatusEnabledResponse.h"
#import "TransfersStepTwoViewController.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"RegistrationTransferThirdCardViewController"


/*
 * Max lenght of nickname field
 */
#define MAX_TEXT_NICKNAME_LENGHT                                   30
#define MIN_TEXT_NICKNAME_LENGHT                                   2



/*
 *max number office text field
 */
#define MAX_NUMBER_OFFICE                                           4

/*
 *max number account text field
 */
#define MAX_NUMBER_ACCOUNT                                          10



@interface RegistrationTransferThirdCardViewController ()

/**
 * Releases graphic elements
 */
- (void)releaseRegistrationTransferThirdCardViewControllerGraphicElements;

/**
 * Invoked by framework when tranfer button is tapped. The information is stored into the transfer operation helper instance and the
 * information validity is checked. When everithing is OK, the server confirm operation is invoked
 *
 * @private
 */
- (void)transferButtonTapped;

@end

@implementation RegistrationTransferThirdCardViewController

#pragma mark -
#pragma mark Properties
@synthesize containerView =containerView_;
@synthesize bottomInfoLabel = bottomInfoLabel_;
@synthesize brandingImageView =brandingImageView_;
@synthesize acceptButton =acceptButton_;
@synthesize operationTypeCombo = operationTypeCombo_;
@synthesize nickNameLabel = nickNameLabel_;
@synthesize operationTypeLabel = operationTypeLabel_;
@synthesize accountLabel = accountLabel_;
@synthesize nickNameTextField = nickNameTextField_;
@synthesize ThirdAccountOperatorHelper = ThirdAccountOperatorHelper_;
@synthesize registerOtherBank = registerOtherBank_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseRegistrationTransferThirdCardViewControllerGraphicElements];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                   name:kNotificationTransferConfirmationResponseReceived
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationValidateThirdAccountResponse
                                                  object:nil];
    
    [editableViews_ release];
    editableViews_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased TransferToMyAccountsViewController constructed from a NIB file
 */
+ (RegistrationTransferThirdCardViewController *)registrationTransferThirdCardViewController {
    
    return [[[RegistrationTransferThirdCardViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[self view] setFrame:[[UIScreen mainScreen] bounds]];
    //NSLog(@"primeros bounds :%@",NSStringFromCGRect([[UIScreen mainScreen] bounds]));
    UIView *view = self.view;
    CGRect frame = view.frame;
    CGFloat width = CGRectGetWidth(frame);
    
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    separator_.image = [imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    CGFloat operationTitleHeaderHeight = [SimpleHeaderView height];
    
    if (operationTitleHeader_ == nil) {
        
        operationTitleHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
        operationTitleHeader_.frame = CGRectMake(0.0f, 0.0f, width, operationTitleHeaderHeight);
    }
    
    [operationTitleHeader_ setTitle:@"Cuenta de terceros"];
    [view addSubview:operationTitleHeader_];
    
      containerView_.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:bottomInfoLabel_ withFontSize:11.0f color:[UIColor BBVAGreyToneTwoColor]];
    //[bottomInfoLabel_ setText:NSLocalizedString(REFERENCE_INFO_TEXT_KEY, nil)];
    [bottomInfoLabel_ setNumberOfLines:1];
    [bottomInfoLabel_ setTextAlignment:UITextAlignmentLeft];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:acceptButton_];
    [acceptButton_ setTitle:NSLocalizedString(@"Aceptar", nil) forState:UIControlStateNormal];
    
    [acceptButton_ addTarget:self action:@selector(transferButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    

    
    CGRect containerFrame = containerView_.frame;
    containerFrame.origin.x = 0.0f;
    containerFrame.origin.y = 0.0f;
    containerView_.frame = containerFrame;
    
    [self setScrollableView:containerView_];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    CGFloat headerMaxY = CGRectGetMaxY([operationTitleHeader_ frame]);
    scrollFrame.origin.y = headerMaxY;
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]) - headerMaxY;
    [self setScrollNominalFrame:scrollFrame];


    [view bringSubviewToFront:brandingImageView_];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
    
    
    /// Iniciar controles nuevos
     [NXT_Peru_iPhoneStyler  styleLabel:accountLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    
    [accountLabel_ setText:NSLocalizedString(ACCOUNT_NUMBER_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler  styleLabel:nickNameLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    
    [nickNameLabel_ setText:NSLocalizedString(NICKNAME_ACCOUNT_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler  styleLabel:operationTypeLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    
    [operationTypeLabel_ setText:NSLocalizedString(OPERATION_TYPE_ACCOUNT_TEXT_KEY, nil)];
    
    UILabel *buttonLabel = operationTypeCombo_.titleLabel;
    buttonLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:10.0f];
    buttonLabel.lineBreakMode = UILineBreakModeTailTruncation;
    
    
    [operationTypeCombo_ setDelegate:self];
    [operationTypeCombo_ setInputAccessoryView:popButtonsView_];
    
    if (numberAccountView_ == nil) {
        
        numberAccountView_ = [[SimpleSelectorAccountView simpleSelectorAccountView] retain];
        [numberAccountView_ setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth];
        
    }
    
    [numberAccountView_ setHidden:YES];
    [accountLabel_ setHidden:YES];
    
    NXTTextField *entityTextField = numberAccountView_.entityTextField;
    
    [entityTextField setDelegate:self];
    [entityTextField setInputAccessoryView:popButtonsView_];
    
    NXTTextField *officeTextField = numberAccountView_.officeTextField;
    
    [officeTextField setDelegate:self];
    [officeTextField setInputAccessoryView:popButtonsView_];
    
    NXTTextField *accountTextField = numberAccountView_.accountTextField;
    [accountTextField setDelegate:self];
    [accountTextField setInputAccessoryView:popButtonsView_];
    
    NXTTextField *ccTextField = numberAccountView_.CcTextField;
    
    [ccTextField setDelegate:self];
    [ccTextField setInputAccessoryView:popButtonsView_];
    
    [NXT_Peru_iPhoneStyler styleTextField:nickNameTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    [nickNameTextField_ setInputAccessoryView:popButtonsView_];    [nickNameTextField_ setDelegate:self];
    
    UIView *superView = [self containerView];
    [superView addSubview:numberAccountView_];
    [superView bringSubviewToFront:numberAccountView_];
    
    [self layoutViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]) - CGRectGetHeight([operationTitleHeader_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
     //NSLog(@"seg bounds :%@",NSStringFromCGRect([[UIScreen mainScreen] bounds]));
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]) - CGRectGetHeight([operationTitleHeader_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
}
/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
    selector:@selector(transferResponseReceived:)
    name:kNotificationTransferConfirmationResponseReceived
    object:nil];
    
    
    
    
    if (shouldClearInterfaceData_) {
        
        operationTypeCombo_.informFirstSelection = YES;
   
        [ThirdAccountOperatorHelper_ setNickname:@""];
        [ThirdAccountOperatorHelper_ setAccountNumber:@""];
        [ThirdAccountOperatorHelper_ setOperationType:@""];
        
        [transparentScroll_ scrollRectToVisible:CGRectMake(0.0f, 0.0f, 1.0f, 1.0f) animated:NO];
        
        shouldClearInterfaceData_ = NO;
        
    }
    
    if (editableViews_ == nil) {
        
        editableViews_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [editableViews_ removeAllObjects];
        
    }
    
    if(registerOtherBank_ == 1){
        [numberAccountView_ setShowCc:YES];
        [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:nickNameTextField_,
                                             numberAccountView_.officeTextField,
                                             numberAccountView_.accountTextField,
                                             numberAccountView_.entityTextField,
                                             numberAccountView_.CcTextField,
                                             operationTypeCombo_ ,
                                             nil]];

    }else{
        
    
    [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:nickNameTextField_,
                                         numberAccountView_.officeTextField,
                                         numberAccountView_.accountTextField,
                                         operationTypeCombo_ ,
                                         nil]];
    }
    
    
    
  
   
    [self displayStoredInformation];
}

-(void)displayStoredInformation
{
    if ([self isViewLoaded]) {
        
        NSMutableArray *array = [NSMutableArray array];
       
        [array addObject:@"Transferencia a terceros"];
        [array addObject:@"Transferencia a otros bancos"];
        operationTypeCombo_.stringsList = [NSArray arrayWithArray:array];
        
        if(registerOtherBank_ == 1){
            [operationTypeCombo_ setSelectedIndex:1];
            [ThirdAccountOperatorHelper_ setOperationType:OTHER_BANK_ACCOUNT_ID];
            [accountLabel_ setText:NSLocalizedString(TRANSFER_TO_ANOTHER_BANK_DESTINATION_TITLE_TEXT_KEY, nil)];
            
        }
        else if (registerOtherBank_ == 2){
        
            [operationTypeCombo_ setSelectedIndex:0];
            [ThirdAccountOperatorHelper_ setOperationType:THIRD_ACCOUNT_ID];
            [accountLabel_ setText:NSLocalizedString(ACCOUNT_NUMBER_TEXT_KEY, nil)];
        }
        else
        {
            [operationTypeCombo_ setTitle:@"Seleccione una opción"];
            [operationTypeCombo_ setSelectedIndex:-1];
            
            [ThirdAccountOperatorHelper_ setOperationType:@""];
        }
        
      
        
        [nickNameTextField_ setText:[ThirdAccountOperatorHelper_ nickname]];
        [self layoutViews];
        
    }
    
   
    
    
}

/**
 * Returns the transfer operation helper associated to the view controller. Returns the transfer between accounts helper
 *
 * @return The transfer operation helper associated to the view controller
 */
- (TransferOperationHelper *)transferOperationHelper {
    
    return ThirdAccountOperatorHelper_;
    
}
/*
 * Lays out the views
 */
- (void)layoutViews {
    
    if ([self isViewLoaded]) {
    
        CGRect frame = nickNameLabel_.frame;
        frame.origin.y = VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        CGFloat height = [Tools labelHeight:nickNameLabel_ forText:nickNameLabel_.text];
        frame.size.height = height;
        nickNameLabel_.frame = frame;
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = nickNameTextField_.frame;
        frame.origin.y = auxPos;
        nickNameTextField_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        frame = operationTypeLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:operationTypeLabel_ forText:operationTypeLabel_.text];
        frame.size.height = height;
        operationTypeLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = [operationTypeCombo_ frame];
        frame.origin.y = auxPos;
        [operationTypeCombo_ setFrame:frame];
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        
        frame = accountLabel_.frame;
        frame.origin.y = auxPos;
        height = [Tools labelHeight:accountLabel_ forText:accountLabel_.text];
        frame.size.height = height;
        accountLabel_.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = [numberAccountView_ frame];
        frame.origin.y = auxPos;
        [numberAccountView_ setFrame:frame];
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;

        UIButton *transferButton = self.acceptButton;
        frame = transferButton.frame;
        frame.origin.y = auxPos;
        transferButton.frame = frame;
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
        
        UIView *containerView = self.containerView;
        frame = containerView.frame;
        frame.size.height = auxPos;
        containerView.frame = frame;
        
        [self recalculateScroll];
    }
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
    return YES;
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self editableViewHasBeenClicked:textField];
    
    if (textField == numberAccountView_.entityTextField) {
        
        if(entityString_ != nil) {
            
            [numberAccountView_.entityTextField setText:entityString_];
            
        }
        
    }
    else if( textField == numberAccountView_.officeTextField) {
        
        if(officeString_ != nil) {
            
            [numberAccountView_.officeTextField setText:officeString_.description];
        }
    }
    else if(textField == numberAccountView_.accountTextField) {
        
        if(accountString_ != nil) {
            
            [numberAccountView_.accountTextField setText:accountString_.description];
        }
    }else if (textField == numberAccountView_.CcTextField) {
        
        if(ccString_ != nil) {
            
            [numberAccountView_.CcTextField setText:ccString_];
        }
    }
    
}

/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;

    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    if(textField == numberAccountView_.entityTextField) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            if(resultLength <= MAX_OTHER_BANK_NUMBER_ENTITY) {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        result = YES;
                    }
                }
            }
            else {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        if([officeString_ length] == MAX_OTHER_BANK_NUMBER_OFFICE) {
                            
                            result = NO;
                        }
                        else {
                            result = YES;
                        }
                        [numberAccountView_.officeTextField becomeFirstResponder];
                    }
                }
            }
        }
        
    }
    
    BOOL isThirdAccount = ([[self.ThirdAccountOperatorHelper operationType] isEqualToString:THIRD_ACCOUNT_ID] || [[self.ThirdAccountOperatorHelper operationType] isEqualToString:@""]);
    
   if (textField == numberAccountView_.accountTextField && isThirdAccount) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            if(resultLength <= MAX_NUMBER_ACCOUNT) {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        result = YES;
                    }
                }
            }
            else {
                result = NO;
            }
        }
        
   } else if(textField == numberAccountView_.officeTextField && isThirdAccount) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            if(resultLength <= MAX_NUMBER_OFFICE) {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        result = YES;
                    }
                }
            }
            else {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        if([accountString_ length] == MAX_NUMBER_ACCOUNT) {
                            
                            result = NO;
                        }
                        else {
                            result = YES;
                        }
                        [numberAccountView_.accountTextField becomeFirstResponder];
                    }
                }
            }
        }
        
    }
    else if(textField == numberAccountView_.officeTextField && [[self.ThirdAccountOperatorHelper operationType] isEqualToString:OTHER_BANK_ACCOUNT_ID]) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            if(resultLength <= MAX_OTHER_BANK_NUMBER_OFFICE) {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        result = YES;
                    }
                }
            }
            else {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        if([accountString_ length] == MAX_OTHER_BANK_NUMBER_ACCOUNT) {
                            result = NO;
                        }
                        else {
                            result = YES;
                        }
                        [numberAccountView_.accountTextField becomeFirstResponder];
                    }
                }
            }
        }
    }
    else if (textField == numberAccountView_.accountTextField && [[self.ThirdAccountOperatorHelper operationType] isEqualToString:OTHER_BANK_ACCOUNT_ID]) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            if(resultLength <= MAX_OTHER_BANK_NUMBER_ACCOUNT) {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        result = YES;
                    }
                }
                
            }
            else {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        if([ccString_ length] == MAX_OTHER_BANK_NUMBER_CC) {
                            result = NO;
                        }
                        else {
                            result = YES;
                        }
                        [numberAccountView_.CcTextField becomeFirstResponder];
                    }
                }
            }
        }                       
    }

   
   else if (textField == nickNameTextField_) {
        
        if ([resultString length] <= MAX_TEXT_NICKNAME_LENGHT) {
            
            if([Tools isValidText:resultString forCharacterString:REFERENCE_VALID_CHARACTERS]) {
                
                result = YES;
                
            }
            
        } else {
            
            result = NO;
            
        }
        
    }
    else if (textField == numberAccountView_.CcTextField) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            if(resultLength <= MAX_OTHER_BANK_NUMBER_CC) {
                
                NSScanner *scanner = [NSScanner scannerWithString:resultString];
                NSInteger resultInteger = 0;
                
                if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                    
                    if (resultInteger >= 0) {
                        
                        result = YES;
                    }
                }
            }
            else {
                result = NO;
            }
        }
        
    }

    
    return result;
    
}
#pragma mark -
#pragma mark User interaction

- (IBAction)operationComboPressed {
    
    [self editableViewHasBeenClicked:operationTypeCombo_];
}

-(void)transferButtonTapped{
    
     [self storeInformationIntoHelper];
    
    ThirdAccountOperator *transferOperationHelper = self.ThirdAccountOperatorHelper;
    NSString *validationErrorMessage = [transferOperationHelper startTransferRequestDataValidation];
    
    if (validationErrorMessage != nil) {
        
        [Tools showAlertWithMessage:validationErrorMessage
                              title:NSLocalizedString(INFO_MESSAGE_TITLE_KEY, nil) andDelegate:self];
        
        
        
    } else {
        //if ([transferOperationHelper startTransferRequest]){
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(validateResponseReceived:)
                                                     name:kNotificationValidateThirdAccountResponse
                                                   object:nil];
        
        if ([transferOperationHelper startNicknameValidation]) {
            
            [self.appDelegate showActivityIndicator:poai_Both];
            
        } else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATIONS_ERROR_TEXT_KEY, nil)];
            
        }
    }

}

-(void)storeInformationIntoHelper
{
    [self.ThirdAccountOperatorHelper setNickname:nickNameTextField_.text];
    if([[ThirdAccountOperatorHelper_ nickname]length]==0){
        [nickNameTextField_ becomeFirstResponder];
        [self resignFirstResponder];
    }
    
    [self.ThirdAccountOperatorHelper setEntityNumber:[numberAccountView_ entityTextField].text];
    [self.ThirdAccountOperatorHelper setOfficeNumber:[numberAccountView_ officeTextField].text];
    [self.ThirdAccountOperatorHelper setAccountNumber:[numberAccountView_ accountTextField].text];
    [self.ThirdAccountOperatorHelper setCccNumber:[numberAccountView_ CcTextField].text];
    
}


#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;

    result.customTitleView.topLabelText = NSLocalizedString(@"Inscripción de Cuentas de Terceros", nil);
    
    return result;
    
}

/*
 * Releases graphic elements
 */
- (void)releaseRegistrationTransferThirdCardViewControllerGraphicElements {
    
//    [originAccountLabel_ release];
//    originAccountLabel_ = nil;
//    
//    [originCombo_ release];
//    originCombo_ = nil;
//    
//    [destinationLabel_ release];
//    destinationLabel_ = nil;
//    
//    [destinationAccountView_ release];
//    destinationAccountView_ = nil;
//    
//    [currencyLabel_ release];
//    currencyLabel_ = nil;
//    
//    [currencyCombo_ release];
//    currencyCombo_ = nil;
//    
//    [amountLabel_ release];
//    amountLabel_ = nil;
//    
//    [amountTextField_ release];
//    amountTextField_ = nil;
//    
//    [referenceLabel_ release];
//    referenceLabel_ = nil;
//    
//    [referenceTextField_ release];
//    referenceTextField_ = nil;
    
    [editableViews_ removeAllObjects];
    
}
-(void)validateResponseReceived:(NSNotification*)notification
{
    [self.appDelegate hideActivityIndicator];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationValidateThirdAccountResponse object:nil];
    
    StatusEnabledResponse *response = [notification object];
    
    if (!response.isError) {
    
        
        
        ThirdAccountOperator *transferOperationHelper = self.ThirdAccountOperatorHelper;

        

        
        if([transferOperationHelper validationNicknameResponseReceived:response withDelegate:self])
        {
            
            [transferOperationHelper startTransferRequest];
        }
        
    }else {
        
        [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
        
    }
}

-(void)transferResponseReceived:(NSNotification*)notification
{
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (!response.isError) {
        
        if([ThirdAccountOperatorHelper_ transferResponseReceived:response])
            [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationTransferConfirmationResponseReceived object:nil];
        [self displayTransferSecondStepView];
        
    }else {
        
       // [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
        
    }

}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if([self.ThirdAccountOperatorHelper.nickname length]==0){
    
        [self editableViewHasBeenClicked:nickNameTextField_];
    }else{
        
        if([self.ThirdAccountOperatorHelper.entityNumber length]==0 && [[self.ThirdAccountOperatorHelper operationType] isEqualToString:OTHER_BANK_ACCOUNT_ID]){
             [self editableViewHasBeenClicked:numberAccountView_.entityTextField];
            
        }else if([self.ThirdAccountOperatorHelper.officeNumber length]==0 ){
            
            [self editableViewHasBeenClicked:numberAccountView_.officeTextField];
            
        }else if([self.ThirdAccountOperatorHelper.accountNumber length]==0 ){
            [self editableViewHasBeenClicked:numberAccountView_.accountTextField];
            
        }else if ([self.ThirdAccountOperatorHelper.cccNumber length]==0  && [[self.ThirdAccountOperatorHelper operationType] isEqualToString:OTHER_BANK_ACCOUNT_ID]){
            
            [self editableViewHasBeenClicked:numberAccountView_.CcTextField];
        }
    }
    
}
#pragma mark -
#pragma mark Displaying views

/*
 * Displays the transfer second step view controller
 */
- (void)displayTransferSecondStepView {
    
    if (transfersStepTwoViewController_ != nil) {
        
        transfersStepTwoViewController_ = nil;
        [transfersStepTwoViewController_ release];
    }
    
    transfersStepTwoViewController_ = [[TransfersStepTwoViewController transfersStepTwoViewController] retain];
    
    TransferOperationHelper *transferOperationHelper = [self transferOperationHelper];
    
    transfersStepTwoViewController_.transferOperationHelper = transferOperationHelper;
    transfersStepTwoViewController_.isCashMobileSend = NO;
    [self.navigationController pushViewController:transfersStepTwoViewController_
                                         animated:YES];
    
}

#pragma mark -
#pragma mark NXTComboButtonDelegate selectors

/*
 * Informs the delegate that a value has been selected
 */
- (void)NXTComboButtonValueSelected:(NXTComboButton *)comboButton
{
    [self validateOperationSelectionPicker];
}

-(void) validateOperationSelectionPicker
{
    NSString *lastOperationType = [self.ThirdAccountOperatorHelper operationType];
    if(operationTypeCombo_.selectedIndex == 0){
        
        
        [self.ThirdAccountOperatorHelper setOperationType: THIRD_ACCOUNT_ID];
        [accountLabel_ setText:NSLocalizedString(ACCOUNT_NUMBER_TEXT_KEY, nil)];
        registerOtherBank_ = 2;
        [numberAccountView_ setShowCc:NO];
        
        if (editableViews_ == nil) {
            
            editableViews_ = [[NSMutableArray alloc] init];
            
        } else {
            
            [editableViews_ removeAllObjects];
            
        }
        if(![lastOperationType isEqualToString:THIRD_ACCOUNT_ID]){
            [numberAccountView_.entityTextField setText:@""];
            [numberAccountView_.officeTextField setText:@""];
            [numberAccountView_.accountTextField setText:@""];
            [numberAccountView_.CcTextField setText:@""];
            
            [self.ThirdAccountOperatorHelper setEntityNumber:@""];
            [self.ThirdAccountOperatorHelper setOfficeNumber:@""];
            [self.ThirdAccountOperatorHelper setAccountNumber:@""];
            [self.ThirdAccountOperatorHelper setCccNumber:@""];
        }
        
        [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:nickNameTextField_,
                                             operationTypeCombo_ ,
                                             numberAccountView_.officeTextField,
                                             numberAccountView_.accountTextField,
                                             nil]];
        [numberAccountView_ setHidden:NO];
        [accountLabel_ setHidden:NO];

    }
    else if(operationTypeCombo_.selectedIndex == 1)
    {
        [self.ThirdAccountOperatorHelper setOperationType:OTHER_BANK_ACCOUNT_ID];
        [accountLabel_ setText:NSLocalizedString(TRANSFER_TO_ANOTHER_BANK_DESTINATION_TITLE_TEXT_KEY, nil)];
        registerOtherBank_ = 1;
        [numberAccountView_ setShowCc:YES];
        
        if (editableViews_ == nil) {
            
            editableViews_ = [[NSMutableArray alloc] init];
            
        } else {
            
            [editableViews_ removeAllObjects];
            
        }
        if(![lastOperationType isEqualToString:OTHER_BANK_ACCOUNT_ID]){
            [numberAccountView_.officeTextField setText:@""];
            [numberAccountView_.accountTextField setText:@""];
            [self.ThirdAccountOperatorHelper setOfficeNumber:@""];
            [self.ThirdAccountOperatorHelper setAccountNumber:@""];

        }
        [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:nickNameTextField_,
                                             operationTypeCombo_ ,
                                             numberAccountView_.entityTextField,
                                             numberAccountView_.officeTextField,
                                             numberAccountView_.accountTextField,
                                             numberAccountView_.CcTextField,
                                             nil]];
        
        [numberAccountView_ setHidden:NO];
        [accountLabel_ setHidden:NO];

        
    }
}
- (void)okButtonClickedInPopButtonsView:(PopButtonsView *)aPopButtonsView{
    
    [super okButtonClickedInPopButtonsView:aPopButtonsView];
    
    if(operationTypeCombo_.inputAccessoryView == aPopButtonsView){
        [self validateOperationSelectionPicker];
        
    }
  
}

@end
