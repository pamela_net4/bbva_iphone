/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TransfersStepOneViewController.h"

#import "EmailCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "TransfersStepOneViewController+protected.h"
#import "NXTComboButton.h"
#import "NXTEditableViewController+protected.h"
#import "NXTNavigationItem.h"
#import "NXTSwitchCell.h"
#import "NXTTextField.h"
#import "NXTTextView.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "SMSCell.h"
#import "Tools.h"
#import "TransfersStepTwoViewController.h"
#import "TermsAndConsiderationsViewController.h"
#import "TransferOperationHelper.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Defines de max text lenght for the mail
 */
#define MAX_TEXT_MAIL_LENGHT                    80


/**
 * TransfersStepOneViewController private extension
 */
@interface TransfersStepOneViewController()

/**
 * Releases Graphic Elements
 *
 * @private
 */
- (void)releaseTransfersStepOneViewControllerGraphicElements;

/**
 * Invoked by framework when tranfer button is tapped. The information is stored into the transfer operation helper instance and the
 * information validity is checked. When everithing is OK, the server confirm operation is invoked
 *
 * @private
 */
- (void)transferButtonTapped;

/**
 * Display the contact selection 
 */
- (void)displayContactSelection;

/**
 * Fixes the add contact button images
 */
- (void)fixAddContactButtonImages;

@end

#pragma mark -

@implementation TransfersStepOneViewController

#pragma mark -
#pragma mark Properties

@synthesize containerView = containerView_;

@synthesize selectionTableView = selectionTableView_;
@synthesize transferButton = transferButton_;
@synthesize separator = separator_;
@synthesize brandingImageView = brandingImageView_;
@synthesize bottomInfoLabel = bottomInfoLabel_;
@synthesize smsCell = smsCell_;
@synthesize emailCell = emailCell_;
@synthesize hasNavigateForward = hasNavigateForward_;
@synthesize destinationPhoneNumber = destinationPhoneNumber_;
@synthesize isCashMobileSend = isCashMobileSend_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransfersStepOneViewControllerGraphicElements];
    
    [transfersStepTwoViewController_ release];
    transfersStepTwoViewController_ = nil;    
    
    [emailCell_ release];
    emailCell_ = nil;
    
    [smsCell_ release];
    smsCell_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTransfersStepOneViewControllerGraphicElements];
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTransfersStepOneViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/* 
 * Releases graphic elements
 */
- (void)releaseTransfersStepOneViewControllerGraphicElements{
    
    [operationTitleHeader_ release];
    operationTitleHeader_ = nil;
    
    [containerView_ release];
    containerView_ = nil;
        
	[transferButton_ release];
	transferButton_ = nil;
    
    [selectionTableView_ release];
    selectionTableView_ = nil;
    
    [bottomInfoLabel_ release];
    bottomInfoLabel_ = nil;
    
    [separator_ release];
    separator_ = nil;
    
    [brandingImageView_ release];
    brandingImageView_ = nil;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[self view] setFrame:[[UIScreen mainScreen] bounds]];
    
    UIView *view = self.view;
    CGRect frame = view.frame;
    CGFloat width = CGRectGetWidth(frame);
    
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    separator_.image = [imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    CGFloat operationTitleHeaderHeight = [SimpleHeaderView height];
    
    if (operationTitleHeader_ == nil) {
        
        operationTitleHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
        operationTitleHeader_.frame = CGRectMake(0.0f, 0.0f, width, operationTitleHeaderHeight);
    }
    
    [view addSubview:operationTitleHeader_];
    
    containerView_.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:bottomInfoLabel_ withFontSize:11.0f color:[UIColor BBVAGreyToneTwoColor]];
//    [bottomInfoLabel_ setText:NSLocalizedString(REFERENCE_INFO_TEXT_KEY, nil)];
    [bottomInfoLabel_ setNumberOfLines:1];
    [bottomInfoLabel_ setTextAlignment:UITextAlignmentLeft];
    
    [selectionTableView_ setScrollEnabled:YES];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:transferButton_];
    [transferButton_ setTitle:NSLocalizedString(TRANSFER_DO_TRANSFER_TEXT_KEY, nil) forState:UIControlStateNormal];
    [transferButton_ addTarget:self
                        action:@selector(transferButtonTapped)
              forControlEvents:UIControlEventTouchUpInside];
    
    selectionTableView_.backgroundColor = [UIColor clearColor];
    
    CGRect containerFrame = containerView_.frame;
    containerFrame.origin.x = 0.0f;
    containerFrame.origin.y = 0.0f;
    containerView_.frame = containerFrame;
    
    [self setScrollableView:containerView_];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    CGFloat headerMaxY = CGRectGetMaxY([operationTitleHeader_ frame]);
    scrollFrame.origin.y = headerMaxY;
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]) - headerMaxY;
    [self setScrollNominalFrame:scrollFrame];
    
    [self layoutViews];
    [view bringSubviewToFront:brandingImageView_];
    
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
    
    self.hasNavigateForward = NO;
    
    transparentScroll_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]) - CGRectGetHeight([operationTitleHeader_ frame]);
    [self setScrollNominalFrame:scrollFrame];

}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Ask the concrete class to store
 * the information into the helper object
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
        
    [self storeInformationIntoHelper];
    
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]) - CGRectGetHeight([operationTitleHeader_ frame]);    
    [self setScrollNominalFrame:scrollFrame];
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Resets the internal information.
 */
- (void)resetInformation {
    
    TransferOperationHelper *transferOperationHelper = [self transferOperationHelper];
    
    [transferOperationHelper setSendSMS:NO];
    [transferOperationHelper setShowSMS1:NO];
    [transferOperationHelper setShowSMS2:NO];
    [transferOperationHelper setSelectedCarrier1Index:-1];
    [transferOperationHelper setDestinationSMS1:@""];
    [transferOperationHelper setSelectedCarrier2Index:-1];
    [transferOperationHelper setDestinationSMS2:@""];
    
    [transferOperationHelper setSendEmail:NO];
    [transferOperationHelper setShowEmail1:NO];
    [transferOperationHelper setShowEmail2:NO];
    [transferOperationHelper setDestinationEmail1:@""];
    [transferOperationHelper setDestinationEmail2:@""];
    [transferOperationHelper setEmailMessage:@""];
    
}

#pragma mark -
#pragma mark User interaction

/*
 * Invoked by framework when tranfer button is tapped. The information is stored into the transfer operation helper instance and the
 * information validity is checked. When everithing is OK, the server confirm operation is invoked
 */
- (void)transferButtonTapped {
    
    [self storeInformationIntoHelper];
    
    [self.transferOperationHelper setEmailMessage:emailCell_.emailTextView.text];
    
    TransferOperationHelper *transferOperationHelper = self.transferOperationHelper;
    NSString *validationErrorMessage = [transferOperationHelper startTransferRequestDataValidation];
    
    if (validationErrorMessage != nil) {
        
        [Tools showAlertWithMessage:validationErrorMessage
                              title:NSLocalizedString(INFO_MESSAGE_TITLE_KEY, nil)];
        
    } else {
        
        if ([transferOperationHelper startTransferRequest]) {
            
            [self.appDelegate showActivityIndicator:poai_Both];
            
        } else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATIONS_ERROR_TEXT_KEY, nil)];
            
        }
        
    }
    
}

/*
 * Display the contact selection 
 */
- (void)displayContactSelection {

    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    
    self.hasNavigateForward = YES;
    
    [[picker navigationBar] setTintColor:[[[self navigationController] navigationBar] tintColor]];
    
    [self.appDelegate presentModalViewController:picker animated:YES];
    
    [picker release]; 

}

/**
 * Fixes the add contact button images
 */
- (void)fixAddContactButtonImages {

    if ((self.transferOperationHelper.destinationSMS1 == nil) || [self.transferOperationHelper.destinationSMS1 isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                           forState:UIControlStateNormal];
        } else{
            
            [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                  forState:UIControlStateNormal];
        }
    } else {
        
        [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                           forState:UIControlStateNormal];
    }
    
    if ((self.transferOperationHelper.destinationSMS2 == nil) || [self.transferOperationHelper.destinationSMS2 isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                            forState:UIControlStateNormal];
        } else{
          
            [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                  forState:UIControlStateNormal];
        }
    } else {
        
        [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                           forState:UIControlStateNormal];
    }
    
    if ((self.transferOperationHelper.destinationEmail1 == nil) || [self.transferOperationHelper.destinationEmail1 isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                             forState:UIControlStateNormal];
        } else{
            [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                  forState:UIControlStateNormal];
        }
    } else {
        
        [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                             forState:UIControlStateNormal];
    }
    
    if ((self.transferOperationHelper.destinationEmail2 == nil) || [self.transferOperationHelper.destinationEmail2 isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                              forState:UIControlStateNormal];
        } else{
             [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                  forState:UIControlStateNormal];
        }
    } else {
        
        [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                              forState:UIControlStateNormal];
    }
          

}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    if (isCashMobileSend_) {
        result.customTitleView.topLabelText = NSLocalizedString(TRANSFER_WITH_CASH_MOBILE_TEXT_1_KEY, nil);
    }else
        result.customTitleView.topLabelText = NSLocalizedString(TRANSFER_TITLE_TEXT_KEY, nil);
    
    return result;
    
}

#pragma mark -
#pragma mark UITableViewDatasource protocol selectors


/**
 * Tells the data source to return the number of rows in a given section of a table view. In this case, only one cell is displayed
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section in tableView
 * @return The number of rows in section
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 2;
    
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. An NXTTableCell is
 * returned
 *
 * @param tableView A table-view object requesting the cell
 * @param indexPath An index path locating a row in tableView
 * @return An object inheriting from UITableViewCellthat the table view can use for the specified row
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    TransferOperationHelper *transferOperationHelper = self.transferOperationHelper;
    
    if (transferOperationHelper.canSendEmailandSMS) {
        
        if (indexPath.row == 0) {
            
            SMSCell *result = (SMSCell *)[tableView dequeueReusableCellWithIdentifier:[SMSCell cellIdentifier]];
            
            if (result == nil) {
                
                if (smsCell_ == nil) {
                    
                    smsCell_ = [[SMSCell smsCell] retain];
                    
                }
                
                result = smsCell_;

            }
            
            result.delegate = self;
            result.showSeparator = NO;
            
            [result.titleLabel setText:NSLocalizedString(NOTIFICATIONS_TABLE_SEND_PHONE_TEXT_KEY, nil)];
            result.smsSwitch.on = transferOperationHelper.sendSMS;
            
            result.firstTextField.text = transferOperationHelper.destinationSMS1;
            result.firstTextField.delegate = self;
            
            result.selectedFirstOperatorIndex = transferOperationHelper.selectedCarrier1Index;
            [result.firstComboButton setDelegate:self];
            
            result.secondTextField.text = transferOperationHelper.destinationSMS2;
            result.secondTextField.delegate = self;
            
            result.selectedSecondOperatorIndex = transferOperationHelper.selectedCarrier2Index;
            [[result secondComboButton] setDelegate:self];
            
            [result setOperatorArray:transferOperationHelper.carrierList 
                          firstAddOn:transferOperationHelper.showSMS1
                         secondAddOn:transferOperationHelper.showSMS2];
            
            [result.firstComboButton setSelectedIndex:transferOperationHelper.selectedCarrier1Index];
            
            [result.secondComboButton setSelectedIndex:transferOperationHelper.selectedCarrier2Index];

            [result.firstTextField setInputAccessoryView:popButtonsView_];
            [result.secondTextField setInputAccessoryView:popButtonsView_];
            [result.firstComboButton setInputAccessoryView:popButtonsView_];
            [result.secondComboButton setInputAccessoryView:popButtonsView_];
            
            [result.firstTextField setDelegate:self];
            [result.secondTextField setDelegate:self];
            [result.firstComboButton setDelegate:self];
            [result.secondComboButton setDelegate:self];

            result.backgroundColor = [UIColor clearColor];
            return result;
            
        } else {
            
            EmailCell *result = (EmailCell *)[tableView dequeueReusableCellWithIdentifier:[EmailCell cellIdentifier]];
            
            if (result == nil) {
                
                if(emailCell_ == nil) {
                
                    emailCell_ = [[EmailCell emailCell] retain];
                }
                result = emailCell_;

            }
            
            result.delegate = self;
            result.showSeparator = YES;

            [result.titleLabel setText:NSLocalizedString(NOTIFICATIONS_TABLE_SEND_EMAIL_TEXT_KEY, nil)];
            result.emailSwitch.on = transferOperationHelper.sendEmail;
            
            result.firstTextField.text = transferOperationHelper.destinationEmail1;            
            result.secondTextField.text = transferOperationHelper.destinationEmail2;
            result.emailTextView.text = transferOperationHelper.emailMessage;
            
            [result.firstTextField setInputAccessoryView:popButtonsView_];
            [result.secondTextField setInputAccessoryView:popButtonsView_];
            [result.emailTextView setInputAccessoryView:popButtonsView_];
            
            [result.firstTextField setDelegate:self];
            [result.secondTextField setDelegate:self];
            [result.emailTextView setDelegate:self]; 
            
            [self fixAddContactButtonImages];
            
            [result setConfigurationForFirstAddOn:transferOperationHelper.showEmail1 
                                      secondAddOn:transferOperationHelper.showEmail2];
            
            result.backgroundColor = [UIColor clearColor];
            return result;

        }
                
    }

    return nil;
    
}

/**	
 * Asks the data source to return the number of sections in the table view. When no coordinate key is needed, 2 is returned, when
 * it is needed to display the coordinate key, 3 is returned
 *
 * @param tableView An object representing the table view requesting this information
 * @return The number of sections in tableView
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
    return 1;
    
}

/**
 * Asks the delegate for a view object to display in the header of the specified section of the table view. The appropriate section
 * header is displayed
 *
 * @param tableView The table-view object asking for the view object
 * @param section An index number identifying a section of tableView
 * @return A view object to be displayed in the header of section 
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    SimpleHeaderView *simpleHeaderView = [SimpleHeaderView simpleHeaderView];
    [simpleHeaderView setTitle:NSLocalizedString(TRANSFER_REPORT_TO_BENEFICIARY_TEXT_KEY, nil)];
    
    return simpleHeaderView;
    
}


/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TransferOperationHelper *transferOperationHelper = self.transferOperationHelper;
    
    if (indexPath.row == 0) {
        
        CGFloat result = [SMSCell cellHeightForFirstAddOn:transferOperationHelper.showSMS1 
                                              secondAddOn:transferOperationHelper.showSMS2];
        
        
        return result;
        
    } else {
        
        return [EmailCell cellHeightForFirstAddOn:transferOperationHelper.showEmail1
                                    secondAddOn:transferOperationHelper.showEmail2];
        
    }
    
}

/**
 * Asks the delegate for the height to use for the header of a particular section.
 *
 * @param tableView The table-view object requesting this information.
 * @param section An index number identifying a section of tableView 
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return [SimpleHeaderView height];
}



#pragma mark -
#pragma mark SMSCell Delegate

/**
 * Switch has been tapped.
 */
- (void)switchButtonHasBeenTapped:(BOOL)on {    
    
    [self saveContactInfoIntoHelper];

    self.transferOperationHelper.sendSMS = on;
    self.transferOperationHelper.showSMS1 = on;
    
    if (!on) {
        
        self.transferOperationHelper.showSMS2 = on;
        
        addingFirstSMS_ = NO;
        addingSecondSMS_ = NO;
        
        smsCell_.firstComboButton.selectedIndex = -1;
        smsCell_.firstTextField.text = @"";
        self.transferOperationHelper.destinationSMS1= @"";

        smsCell_.secondComboButton.selectedIndex = -1;
        smsCell_.secondTextField.text = @"";
        self.transferOperationHelper.destinationSMS2= @"";
        
        [[self transferOperationHelper] setSelectedCarrier1Index:-1];
        [[self transferOperationHelper] setSelectedCarrier2Index:-1];

    } else {
        
        self.transferOperationHelper.showSMS2 = NO;
        [self transferOperationHelper].destinationSMS1 = smsCell_.firstTextField.text;
        [self transferOperationHelper].destinationSMS2 = smsCell_.secondTextField.text;
        
        smsCell_.firstComboButton.selectedIndex = -1;
        [[self transferOperationHelper] setSelectedCarrier1Index:-1];
    }
    
    [self fixAddContactButtonImages];

    
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
    
    [selectionTableView_ reloadRowsAtIndexPaths:array 
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    [array release];
    
    [self layoutViews];
    
}

/**
 * More button has been tapped
 */
- (void)moreButtonHasBeenTapped {
    
    [self saveContactInfoIntoHelper];
    self.transferOperationHelper.showSMS2 = YES;
    
    smsCell_.secondComboButton.selectedIndex = -1;
    [[self transferOperationHelper] setSelectedCarrier2Index:-1];
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
    
    [selectionTableView_ reloadRowsAtIndexPaths:array 
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [array release];
    
    [self layoutViews];  

}

/**
 * Add First Contact Button Tapped
 */
- (void)addFirstContactHasBeenTapped {
    
    if ((smsCell_.firstTextField.text == nil) || [smsCell_.firstTextField.text isEqualToString:@""]) {
        
        addingFirstSMS_ = YES;
        [self displayContactSelection];
        
    } else {
        
        smsCell_.firstTextField.text = @"";
        self.transferOperationHelper.destinationSMS1 = @"";

    }

    [self fixAddContactButtonImages];

}

/**
 * Add Second Contact Button Tapped
 */
- (void)addSecondContactHasBeenTapped {
    
    if ((smsCell_.secondTextField.text == nil) || [smsCell_.secondTextField.text isEqualToString:@""]) {
        
        addingSecondSMS_ = YES;
        [self displayContactSelection];
        
    } else {
        
        smsCell_.secondTextField.text = @"";
        self.transferOperationHelper.destinationSMS2 = @"";

    }

    [self fixAddContactButtonImages];
    
}

/*
 *called when the user tapped in first combo button
 */
- (void)firstComboButtonTapped {
    
    [self editableViewHasBeenClicked:smsCell_.firstComboButton];
    
}

/*
 *called when the user tapped in second combo button
 */
-(void)secondComboButtonTapped {
    
    [self editableViewHasBeenClicked:smsCell_.secondComboButton];
}

#pragma mark -
#pragma mark EmailCell Delegate

/**
 * Switch has been tapped.
 *
 * @param on The flag
 */
- (void)emailSwitchButtonHasBeenTapped:(BOOL)on {

    [self saveContactInfoIntoHelper];
    
    self.transferOperationHelper.sendEmail = on;
    self.transferOperationHelper.showEmail1 = on;
    
    if (!on) {
        
        self.transferOperationHelper.showEmail2 = on;

        addingFirstEmail_ = NO;
        addingSecondEmail_ = NO;
        
        emailCell_.firstTextField.text = @"";
        self.transferOperationHelper.destinationEmail1= @"";
        
        emailCell_.secondTextField.text = @"";
        self.transferOperationHelper.destinationEmail2= @"";
        
        emailCell_.emailTextView.text = @"";
        self.transferOperationHelper.emailMessage = @"";
        
    } else {
        
        self.transferOperationHelper.showEmail2 = NO;
        [self transferOperationHelper].destinationEmail1 = emailCell_.firstTextField.text;
        [self transferOperationHelper].destinationEmail2 = emailCell_.secondTextField.text;
        [self transferOperationHelper].emailMessage = emailCell_.emailTextView.text;
    
    }
    
    [self fixAddContactButtonImages];
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
    
    [selectionTableView_ reloadRowsAtIndexPaths:array 
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [array release];
    
    [self layoutViews];
    
}

/**
 * Add First Contact Button Tapped
 */
- (void)emailAddFirstContactHasBeenTapped {

    if ((emailCell_.firstTextField.text == nil) || [emailCell_.firstTextField.text isEqualToString:@""]) {
        
        addingFirstEmail_ = YES;  
        [self displayContactSelection];
        
    } else {
        
        emailCell_.firstTextField.text = @"";
        self.transferOperationHelper.destinationEmail1= @"";

    }

    [self fixAddContactButtonImages];
    
}

/**
 * Add Second Contact Button Tapped
 */
- (void)emailAddSecondContactHasBeenTapped {
    
    if ((emailCell_.secondTextField.text == nil) || [emailCell_.secondTextField.text isEqualToString:@""]) {
        
        addingSecondEmail_ = YES;        
        [self displayContactSelection];
        
    } else {
        
        emailCell_.secondTextField.text = @"";
        self.transferOperationHelper.destinationEmail2= @"";

    }
    
    [self fixAddContactButtonImages];

}

/**
 * More button has been tapped
 */
- (void)emailMoreButtonHasBeenTapped {

    [self saveContactInfoIntoHelper];
    self.transferOperationHelper.showEmail2 = YES;

    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
    
    [selectionTableView_ reloadRowsAtIndexPaths:array 
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [array release];
    
    [self layoutViews];
    
}

#pragma mark -
#pragma mark ABPeoplePickerNavigationControllerDelegate methods

/**
 * Called in ios 8 redirection to shouldContinueAfterSelectingPerson used in ios 7 or previous
 */
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    
    [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person property:property identifier:identifier];
}

/**
 * Called after the user has pressed cancel. The delegate is responsible for dismissing the peoplePicker
 */
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    
	[self.appDelegate dismissModalViewControllerAnimated:YES];
    
    [self fixAddContactButtonImages];
    addingFirstSMS_ = NO;
    addingSecondSMS_ = NO;
    addingFirstEmail_ = NO;
    addingSecondEmail_ = NO;
    
}

/**
 * Called after a person has been selected by the user.
 *
 * Return YES if you want the person to be displayed.
 * Return NO  to do nothing (the delegate is responsible for dismissing the peoplePicker).
 */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    
	[self.appDelegate dismissModalViewControllerAnimated:YES];
        
    if (addingFirstSMS_ || addingSecondSMS_) {
        
        BOOL isValidMobilePhoneNumber = NO;

        ABMultiValueRef phoneProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
        
        if (ABMultiValueGetCount(phoneProperty) > 0) {
            
            ABMultiValueRef phoneValue;
            NSString *phoneNumber;
            
            for (int i = 0; ABMultiValueGetCount(phoneProperty) > i; i++) {
                
                phoneValue = ABMultiValueCopyValueAtIndex(phoneProperty,i);
                
                phoneNumber = (NSString *)phoneValue;
                
                phoneNumber = [Tools mobileNumberLikeFromString:phoneNumber];
                
                isValidMobilePhoneNumber = [Tools isValidMobilePhoneNumberString:phoneNumber];

                if (isValidMobilePhoneNumber) {
                    break;
                }
            }

            
            if (isValidMobilePhoneNumber) {
                
                if (addingFirstSMS_) {
                    
                    [[self transferOperationHelper] setDestinationSMS1:phoneNumber];
                    addingFirstSMS_ = NO;
                    
                } else if (addingSecondSMS_) {
                    
                    [[self transferOperationHelper] setDestinationSMS2:phoneNumber];
                    addingSecondSMS_ = NO;
                    
                }
                
            }
            
            if (phoneValue != nil) {
                
                CFRelease(phoneValue);
                
            }
       
        }
        
        
        if (!isValidMobilePhoneNumber) {
            
            [Tools showInfoWithMessage:NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_NO_EXIST_TEXT_KEY, nil)];
            
        } 
        
        addingFirstSMS_ = NO;
        addingSecondSMS_ = NO;
        
        if (phoneProperty != nil) {
            
            CFRelease(phoneProperty);
            
        }       
    
    } else if (addingFirstEmail_ || addingSecondEmail_) {
        
        
        ABMultiValueRef emailProperty = ABRecordCopyValue(person, kABPersonEmailProperty);
        NSInteger emailsNumber = ABMultiValueGetCount(emailProperty);
           
        if (emailsNumber > 0) {
                
            //if (emailsNumber == 1) {
                    
                ABMultiValueRef emailValue = ABMultiValueCopyValueAtIndex(emailProperty, 0);
                
                if (addingFirstEmail_) {
                    
                    [[self transferOperationHelper] setDestinationEmail1:(NSString *)emailValue];
                    addingFirstEmail_ = NO;

                } else if (addingSecondEmail_) {
                
                    [[self transferOperationHelper] setDestinationEmail2:(NSString *)emailValue];
                    addingSecondEmail_ = NO;

                }
                    
                if (emailValue != nil) {
                       
                    CFRelease(emailValue);
                        
                }
            
            //}
        
        } else {
            
            [Tools showInfoWithMessage:NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_NO_EXIST_TEXT_KEY, nil)];
            
            addingFirstEmail_ = NO;
            addingSecondEmail_ = NO;
            
        }
            
        if (emailProperty != nil) {
               
            CFRelease(emailProperty);
               
        }        
        
    }

    [self fixAddContactButtonImages];

  	return NO;
}

/**
 * Called after a value has been selected by the user.
 *
 * Return YES if you want default action to be performed.
 * Return NO to do nothing (the delegate is responsible for dismissing the peoplePicker).
 */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker 
      shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property 
                              identifier:(ABMultiValueIdentifier)identifier {
    
    return [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person];
    
}


#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/** 
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
	return YES;
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (lastEditingView_ != textField) {
        lastEditingView_ = textField;
        [self editableViewHasBeenClicked:textField];
    }
    
}

/** 
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    if( (textField == [[self smsCell] firstTextField]) || (textField == [[self smsCell] secondTextField]) || textField == destinationPhoneNumber_ ) {
        
        if(resultLength == 0) {
            
            result = YES;
        }
        else if(resultLength <= 9) {
            
            NSScanner *scanner = [NSScanner scannerWithString:resultString];
            NSInteger resultInteger = 0;
            
            if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                if (resultInteger >= 0) {
                    
                    result = YES;                                        
                }                
            } 
        }        
    }
    else if ( (textField == [[self emailCell] firstTextField]) || (textField == [[self emailCell] secondTextField]) ) {
        
        if (resultLength <= 50) {
            
            result = YES;                                        

        }           
    }
    
   
    if (resultLength == 0) {
        
        if (textField == smsCell_.firstTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
            } else{
            
                [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                      forState:UIControlStateNormal];
            }
        } else if (textField == smsCell_.secondTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
            } else{
               
                [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                      forState:UIControlStateNormal];
            }
        } else if (textField == emailCell_.firstTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
            } else{
                
                [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                      forState:UIControlStateNormal];
            }
        } else if (textField == emailCell_.secondTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal
                 ];
            } else{
                
                [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                      forState:UIControlStateNormal];
            }
        }
        
    } else {
    
        if (textField == smsCell_.firstTextField) {
            
            [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                               forState:UIControlStateNormal];
            
        } else if (textField == smsCell_.secondTextField) {
            
            [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                                forState:UIControlStateNormal];
            
        } else if (textField == emailCell_.firstTextField) {
            
            [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                                 forState:UIControlStateNormal];
            
        } else if (textField == emailCell_.secondTextField) {
            
            [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  
                                                  forState:UIControlStateNormal];
            
        }
    
    }
    
    return result;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

    lastEditingView_ = nil;

}


#pragma mark -
#pragma mark UITextViewDelegate protocol selectors

/** 
 * Asks the delegate if editing should stop in the specified text view.
 */
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
 
    if (textView != lastEditingView_) {
        
        lastEditingView_ = textView;
        
        [self editableViewHasBeenClicked:textView];
        
        NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
        
        if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
            
            if (popButtonsView_.superview == nil) {
                
                popButtonsState_ = pbse_Show;
                
            } else {
                
                popButtonsState_ = pbse_Relocate;
                
            }
            
        }
    }
    
	return YES;
    
}

/**
 * Asks the delegate whether the specified text should be replaced in the text view.
 *
 * @param textView The text view containing the changes.
 * @param range The current selection range.
 * @param text The text to insert.
 */
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {

    BOOL result = YES;
    
    NSString *resultString = [textView.text stringByReplacingCharactersInRange:range
                                                                    withString:text];
    
    if (textView == [[self emailCell] emailTextView]) {
        
        if ([resultString length] < MAX_TEXT_MAIL_LENGHT) {
        
            result = ([Tools isValidText:text forCharacterSet:[NSCharacterSet alphanumericCharacterSet]] || [Tools isValidText:text forCharacterSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);

        }
        else {
            result = NO;
        }

    } 
    
    return result;
    
}


/**
 * Tells the delegate that editing of the specified text view has ended.
 */
- (void)texViewDidBeginEditing:(UITextView *)textView {
    
    [self editableViewHasBeenClicked:textView];
    
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    lastEditingView_ = nil;
}


#pragma mark -
#pragma mark NXTComboButtonDelegate selectors

/*
 * Informs the delegate that a value has been selected
 */
- (void)NXTComboButtonValueSelected:(NXTComboButton *)comboButton {
    
    if (comboButton == smsCell_.firstComboButton) {
        
        self.transferOperationHelper.selectedCarrier1Index = comboButton.selectedIndex;
    
    } else if (comboButton == smsCell_.secondComboButton) {
        
        self.transferOperationHelper.selectedCarrier2Index = comboButton.selectedIndex;

    }
}

@end