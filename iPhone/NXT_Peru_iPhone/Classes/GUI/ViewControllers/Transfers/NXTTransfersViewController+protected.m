/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTTransfersViewController+protected.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "TransferDetailCell.h"
#import "TransferOperationHelper.h"

#pragma mark -

@implementation NXTTransfersViewController(protected)

#pragma mark -
#pragma mark Graphic elements configuration

/*
 * Returns the step string to display in the step label. Default implementation returns an empty string
 */
- (NSString *)stepLabelString {
    
    return @"";
    
}

#pragma mark -
#pragma mark Graphic management

/*
 * Displays the stored information
 */
- (void)displayStoredInformation {
    
    if ([self isViewLoaded]) {
        
        operationTypeHeader_.title = transferOperationHelper_.localizedTransferOperationTypeString;
        [informationTableView_ reloadData];
        [self layoutViews];
        
    }
    
}

/*
 * Lays out the views
 */
- (void)layoutViews {
    
}

#pragma mark -
#pragma mark Cells management

/*
 * Returns a table cell that represents the operation step information stored at teh given index. The cell is a TransferDetailCell instance
 */
- (TransferDetailCell *)transferDetailCellAtIndex:(NSInteger)index
                                     forTableView:(UITableView *)tableView {
    
    NSString *cellId = [TransferDetailCell cellIdentifier];
    
    TransferDetailCell *result = (TransferDetailCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (result == nil) {
        
        result = [TransferDetailCell transferDetailCell];
        
    }
    
    TitleAndAttributes *titleAndAttributes = nil;
    NSUInteger variableInformationCount = [variableInformationArray_ count];
    
    
    if ((index >= 0) && (index < variableInformationCount)) {
        
        titleAndAttributes = [variableInformationArray_ objectAtIndex:index];
        
    }

    [result displayTitleAndAttributes:titleAndAttributes];
        
    result.displaySeparatorImage = NO;
        
    return result;
    
}

/*
 * Returns the table cell height for the cell that represents the operation step information stored at teh given index.
 * The cell is a TransferDetailCell instance
 */
- (CGFloat)transferDetailCellHeightAtIndex:(NSInteger)index {
    
    CGFloat result = 0.0f;
    
    TitleAndAttributes *titleAndAttributes = nil;
    
    if (index < [variableInformationArray_ count]) {
        
        titleAndAttributes = [variableInformationArray_ objectAtIndex:index];
        
    }
    
    result = [TransferDetailCell cellHeightForTitleAndAttributes:titleAndAttributes];
    
    return result;
    
}



#pragma mark -
#pragma mark Information management

/*
 * Saves the information inside the editable elements into the transfer operation helper. Default implementation does nothing
 */
- (void)storeInformationIntoHelper {
    
}

/*
 * Returns the variable information for the view controller. All objects must be TitleAndAttributes instances (even though the array if
 * filtered). Default implementation returns an empty array
 */
- (NSArray *)variableInformation {
    
    return [NSArray array];
    
}

/*
 * Returns the number of entries in the variable information array
 */
- (NSUInteger)variableInformationEntriesCount {
    
    return [variableInformationArray_ count];
    
}

@end
