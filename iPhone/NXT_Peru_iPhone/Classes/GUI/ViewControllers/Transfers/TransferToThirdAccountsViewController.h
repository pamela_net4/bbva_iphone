/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TransfersStepOneViewController.h"
#import "TransfersConstants.h"
#import "NXTComboButton.h"
#import "RegisteredAccountListViewController.h"

//Forward declarations
@class NXTTextField;
@class NXTCurrencyTextField;
@class TransferToThirdAccounts;
@class SimpleSelectorAccountView;

/**
 * View to transfer money from an account of the user to third account
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferToThirdAccountsViewController : TransfersStepOneViewController <UITextFieldDelegate, NXTComboButtonDelegate,RegisteredAccountListViewControllerDelegate> {
    
@private
    
    /*
     * origin account label
     */
    UILabel *originAccountLabel_;
    
    /*
     * origin combo
     */
    NXTComboButton *originCombo_;
    
    /*
     * destination label
     */
    UILabel *destinationLabel_;
    
    /*
     * Destination account
     */
    SimpleSelectorAccountView *destinationAccountView_;
    
    /*
     * currency label
     */
    UILabel *currencyLabel_;
    
    /*
     * currency combo
     */
    NXTComboButton *currencyCombo_;
    
    /*
     * amount label
     */
    UILabel *amountLabel_;
    
    /*
     * amount text field
     */
    NXTCurrencyTextField *amountTextField_;
    
    /*
     * reference label
     */
    UILabel *referenceLabel_;
    
    /*
     * reference text field
     */
    NXTTextField *referenceTextField_;
    
    /*
     * transfer to third account helper     
     */
    TransferToThirdAccounts *transferToThirdAccountsHelper_;
    
    RegisteredAccountListViewController *registeredAccountListViewController_;
    /*
     * office string
     */
    NSString *officeString_;
    
    /*
     * account string
     */
    NSString *accountString_;
    
    BOOL isSelecting_;
}

/**
 * Provides readwrite access to the originAccountLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *originAccountLabel;

/**
 * Provides readwrite access to the originCombo button. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *originCombo;

/**
 * Provides readwrite access to the destinationLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *destinationLabel;

/**
 * Provides readwrite access to the currencyLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *currencyLabel;

/**
 * Provides readwrite access to the currencyCombo. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *currencyCombo;


/**
 * Provides readwrite access to the amountLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *amountLabel;

/**
 * Provides readwrite access to the amountTextField. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTCurrencyTextField *amountTextField;

/**
 * Provides readwrite access to the referenceLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *referenceLabel;

/**
 * Provides readwrite access to the referenceTextField. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTTextField *referenceTextField;

/**
 * Provides read-write access to the transfer between accounts helper element
 */
@property (nonatomic, readwrite, retain) TransferToThirdAccounts *transferToThirdAccountsHelper;

/**
 * Creates and returns an autoreleased TransferToMyAccountsViewController constructed from a NIB file
 *
 * @return The autoreleased TransferToMyAccountsViewController constructed from a NIB file
 */
+ (TransferToThirdAccountsViewController *)transferToThirdAccountsViewController;

/**
 * Invoked by framework when the origin combo is tapped. Sets it as the editing control
 */
- (IBAction)originComboPressed;

/**
 * Invoked by framework when the currency combo is tapped. Sets it as the editing control
 */
- (IBAction)currencyComboPressed;


@end
