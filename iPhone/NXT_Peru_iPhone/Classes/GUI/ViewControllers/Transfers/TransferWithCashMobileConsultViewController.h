//
//  TransferWithCashMobileConsultViewController.h
//  NXT_Peru_iPad
//
//  Created by R Herrera on 17/09/13.
//
//

#import "NXTEditableViewController.h"
#import "TransferShowDetailAddtionalInformation.h"
#import "TransfersStepOneViewController.h"
#import "DetailCashMobileCell.h"

@class TransferDetailCashMobile;
@class TransfersStepThreeViewController;
@interface TransferWithCashMobileConsultViewController : NXTEditableViewController<UITableViewDataSource, UITableViewDelegate>{
    
    /**
     * array with movement information
     */
    NSArray *movementsList_;
    
    /**
     * TransferSuccessAdditionalInformation view
     */
    TransferShowDetailAddtionalInformation *transferSuccessAdditionalInformation_;
    
    /*
     * Detail of cash mobile helper
     */
    TransferDetailCashMobile *transferWithCashMobileHelper_;
    
    
    /*
     * Confirmation View
     */
    TransfersStepThreeViewController *transfersStepThreeViewController_;

    /*
     * Label to show when there is no data
     */
    UILabel *messageEmptyDataLabel_;
    
    /**
     *  Dictionary with the max resend times alowed per operation
     */
    NSMutableDictionary *operationsCounts_;
    
    /**
     *  Operation Number of the last selected transfer
     */
    NSString *lastOperationNumber_;
    
}

/**
 * Provides readwrite access to the amountsList
 */
@property (nonatomic, readwrite, retain) NSArray *movementsList;

/**
 * Table to display the information
 */
@property (retain, nonatomic) IBOutlet UITableView *table;

/**
 * Provides read-write access to the transfer withCashMobile helper element
 */
@property (nonatomic, readwrite, retain) TransferDetailCashMobile *transferWithCashMobileHelper;

/**
 * Provides read-write access to the label of empty data
 */
@property (retain, nonatomic) IBOutlet UILabel *messageEmptyDataLabel;


/**
 * Creates and returns an autoreleased TransferViewController constructed from a NIB file
 *
 * @return The autoreleased TransferViewController constructed from a NIB file
 */
+ (TransferWithCashMobileConsultViewController *)TransferWithCashMobileViewController;

@end
