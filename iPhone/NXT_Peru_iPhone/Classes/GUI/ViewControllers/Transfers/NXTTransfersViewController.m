/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTTransfersViewController.h"
#import "NXTTransfersViewController+protected.h"
#import "NXTEditableViewController+protected.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "StringKeys.h"
#import "SimpleHeaderView.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "TransferOperationHelper.h"
#import "TitleAndAttributes.h"
#import "UIColor+BBVA_Colors.h"

#pragma mark -

/**
 * TransfersViewController private extension
 */
@interface NXTTransfersViewController()

/**
 * Releases graphic elements
 */
- (void)releaseNXTTransfersViewControllerGraphicElements;

@end

#pragma mark -

@implementation NXTTransfersViewController

#pragma mark -
#pragma mark Properties

@synthesize containerView = containerView_;
@synthesize informationTableView = informationTableView_;
@synthesize bottomInfoLabel = bottomInfoLabel_;
@synthesize brandingImageView = brandingImageView_;
@synthesize transferOperationHelper = transferOperationHelper_;
@synthesize operationTypeHeader = operationTypeHeader_;
@synthesize isCashMobileSend = isCashMobileSend_;
@synthesize variableInformationArray=variableInformationArray_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseNXTTransfersViewControllerGraphicElements];
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseNXTTransfersViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseNXTTransfersViewControllerGraphicElements {
    
    [operationTypeHeader_ release];
    operationTypeHeader_ = nil;
    
    [containerView_ release];
    containerView_ = nil;    
    
    [informationTableView_ release];
    informationTableView_ = nil;
    
    [bottomInfoLabel_ release];
    bottomInfoLabel_ = nil;
    
    [brandingImageView_ release];
    brandingImageView_ = nil;
    
    [operationTypeHeader_ release];
    operationTypeHeader_ = nil;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called when the controller’s view is released from memory. Visual elements are released
 */
- (void)viewDidUnload {
    
    [self releaseNXTTransfersViewControllerGraphicElements];
    
    [super viewDidUnload];
    
}

/**
 * Called when the controller’s view is loaded. The autoresizing mask and autoresizes subviews flag are set. Visual elements are styled
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIView *view = self.view;
    CGRect frame = view.frame;
    CGFloat width = CGRectGetWidth(frame);
    
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
    CGFloat operationTitleHeaderHeight = [SimpleHeaderView height];
    
    if (operationTypeHeader_ == nil) {
        
        operationTypeHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
        operationTypeHeader_.frame = CGRectMake(0.0f, 0.0f, width, operationTitleHeaderHeight);
    }
    
    [view addSubview:operationTypeHeader_];
    
    containerView_.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:bottomInfoLabel_ withFontSize:11.0f color:[UIColor BBVAGreyToneTwoColor]];
    [bottomInfoLabel_ setText:@""];
    [bottomInfoLabel_ setNumberOfLines:1];
    [bottomInfoLabel_ setTextAlignment:UITextAlignmentLeft];
    
    informationTableView_.scrollEnabled = NO;
    
    CGRect containerFrame = containerView_.frame;
    containerFrame.origin.x = 0.0f;
    containerFrame.origin.y = 0.0f;
    containerView_.frame = containerFrame;
    
    [self setScrollableView:containerView_];
    
    CGRect scrollFrame = CGRectMake(0.0f,
                                    operationTitleHeaderHeight,
                                    width,
                                    CGRectGetMinY(brandingImageView_.frame) - operationTitleHeaderHeight);
    
    [self setScrollNominalFrame:scrollFrame];
    
    [self layoutViews];
    
    [view bringSubviewToFront:brandingImageView_];
    
}

/**
 * Notifies the view controller that its view is about to be become visible. Displays the stored information
 *
 * @param animated If YES, the view is being added to the window using an animation
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self displayStoredInformation];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. The view information
 * is stored into the transfer operation helper
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self storeInformationIntoHelper];
    
}

#pragma mark -
#pragma mark Properties methods

/*
 * Sets the transfer operation helper. The variable information is extracted
 *
 * @param transferOperationHelper The new transfer operation helper to set
 */
- (void)setTransferOperationHelper:(TransferOperationHelper *)transferOperationHelper {
    
    if (transferOperationHelper != transferOperationHelper_) {
        
        [transferOperationHelper retain];
        [transferOperationHelper_ release];
        transferOperationHelper_ = transferOperationHelper;
        
    }
    
    NSArray *variableInformationArray = [self variableInformation];
    
    if (variableInformationArray_ == nil) {
        
        variableInformationArray_ = [[NSMutableArray alloc] init];
        
    }
    
    [variableInformationArray_ removeAllObjects];
    
    Class titleAndAttributesClass = [TitleAndAttributes class];
    
    for (NSObject *storedObject in variableInformationArray) {
        
        if ([storedObject isKindOfClass:titleAndAttributesClass]) {
            
            [variableInformationArray_ addObject:storedObject];
            
        }
        
    }
    
    
    [self resetScrollToTopLeftAnimated:NO];
    
    [self displayStoredInformation];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer.
 * Implemented by subclasses to initialize a new object (the receiver) immediately after memory for it has been allocated.
 *
 * @return An initialized object.
 */
- (id)init {
    
    self = [super init];
    
    if (self) {
        
        // Your code here.
        
    }
    
    return self;
    
}

@end
