//
//  TransferTINOnlineStepViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 10/2/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "TransferTINOnlineStepViewController.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "OwnAccountView.h"
#import "OtherBankExtraView.h"
#import "TransferToOtherBankAccounts.h"
#import "SimpleHeaderView.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "StringKeys.h"
#import "CheckTypeTransferCell.h"
#import "Tools.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXTTextField.h"
#import "TransferTypePaymentResponse.h"
#import "UIColor+BBVA_Colors.h"
#import "NXTEditableViewController+protected.h"
#import "NXTNavigationItem.h"
#import "Constants.h"
#import "TransfersStepTwoViewController.h"
#import "PaymentCOtherBankProcess.h"
#import "MOKTextField.h"
#import "PaymentConfirmationViewController.h"
#import "FOTransferToOtherBankAccount.h"
#import "FOPaymentCOtherBank.h"
#import "FrequentOperationStepTwoViewController.h"
#import "TermsAndConsiderationsViewController.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"TransferTINOnlineStepViewController"

/**
 * Defines the vertical gap between two far away elements
 */

@interface TransferTINOnlineStepViewController ()
- (void)releaseTransferTINOnlineStepViewControllerrGraphicElements;
@end

@implementation TransferTINOnlineStepViewController

@synthesize containerView = containerView_;
@synthesize typeTransferTableView = typeTransferTableView_;
@synthesize transferButton = transferButton_;
@synthesize brandingImageView = brandingImageView_;
@synthesize separator = separator_;
@synthesize TransferToOtherBankAccountsHelper = TransferToOtherBankAccountsHelper_;
@synthesize typeTransferLabel = typeTransferLabel_;
@synthesize ownAccountLabel = ownAccountLabel;
@synthesize beneficiaryLabel = beneficiaryLabel_;
@synthesize beneficiaryTextField = beneficiaryTextField_;
@synthesize hasNavigateForward = hasNavigateForward_;
@synthesize processCard = processCard_;
@synthesize foOperationHelper = foOperationHelper_;
@synthesize alertMessageBannerView = alertMessageBannerView_;
@synthesize iconAlertViewBanner = iconAlertViewBanner_;
@synthesize topAletViewBannerSeparator = topAletViewBannerSeparator_;
@synthesize bottomAletViewBannerSeparator = bottomAletViewBannerSeparator_;
@synthesize messageLabelViewBanner = messageLabelViewBanner_;
@synthesize disclaimer = disclaimer_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransferTINOnlineStepViewControllerrGraphicElements];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationTransferConfirmationResponseReceived
                                                  object:nil];
    [super dealloc];
}

/*
 * Releases graphic elements
 */
- (void)releaseTransferTINOnlineStepViewControllerrGraphicElements {
    [containerView_ release];
    containerView_ = nil;
    
    [typeTransferTableView_ release];
    typeTransferTableView_ = nil;
    
    [transferButton_ release];
    transferButton_ = nil;
    
    
    [ownAccountLabel release];
    ownAccountLabel = nil;

    
    [typeTransferLabel_ release];
    typeTransferLabel_ = nil;

    
    [brandingImageView_ release];
    brandingImageView_ = nil;
    
    [beneficiaryLabel_ release];
    beneficiaryLabel_ = nil;
    
    [separator_ release];
    separator_ = nil;
    
    
    [inputAccessoryView_ release];
    inputAccessoryView_ = nil;

    
    
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[self view] setFrame:[[UIScreen mainScreen] bounds]];
    
    UIView *view = self.view;
    CGRect frame = view.frame;
    CGFloat width = CGRectGetWidth(frame);
    
    [NXT_Peru_iPhoneStyler styleNXTView:view];

    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    separator_.image = [imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    bottomAletViewBannerSeparator_.image = [imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    topAletViewBannerSeparator_.image = [imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    CGFloat operationTitleHeaderHeight = [SimpleHeaderView height];
    
    if (operationTitleHeader_ == nil) {
        
        operationTitleHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
        operationTitleHeader_.frame = CGRectMake(0.0f, 0.0f, width, operationTitleHeaderHeight);
    }
    
    [view addSubview:operationTitleHeader_];
    

    
    containerView_.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:transferButton_];
    [transferButton_ addTarget:self
                        action:@selector(transferButtonTapped)
              forControlEvents:UIControlEventTouchUpInside];
    
    CGRect containerFrame = containerView_.frame;
    containerFrame.origin.x = 0.0f;
    containerFrame.origin.y = 0.0f;
    containerView_.frame = containerFrame;
    
    [self setScrollableView:containerView_];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    CGFloat headerMaxY = CGRectGetMaxY([operationTitleHeader_ frame]);
    scrollFrame.origin.y = headerMaxY;
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]) - headerMaxY;
    [self setScrollNominalFrame:scrollFrame];

    [view bringSubviewToFront:brandingImageView_];
   
    [editableViews_ release];
    editableViews_ = [[NSMutableArray alloc] init];
    
    transparentScroll_.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    //Specific interface
    [NXT_Peru_iPhoneStyler styleLabel:typeTransferLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVABlueColor]];
     [NXT_Peru_iPhoneStyler styleLabel:messageLabelViewBanner_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVABlueColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:ownAccountLabel withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [ownAccountLabel setText:NSLocalizedString(OWN_ACCOUNT_DESTINATION_TEXT_KEY, nil)];
    
    if(ownAccountView_ == nil) {
        
        ownAccountView_ = [[OwnAccountView ownAccountView] retain];
        
        [ownAccountView_ setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth];
    }
    
    if(otherBankExtraView_ == nil){
        otherBankExtraView_ = [OtherBankExtraView otherBankExtraView:popButtonsView_ viewController:self];
    }
    [otherBankExtraView_ setDelegate:self];
    [otherBankExtraView_ setBackgroundColor:[UIColor clearColor]];
    typeTransferTableView_.backgroundColor = [UIColor clearColor];

    [typeTransferTableView_ setScrollEnabled:NO];
    
    [NXT_Peru_iPhoneStyler styleLabel:beneficiaryLabel_ withFontSize:14.0f color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleMokTextField:beneficiaryTextField_ withFontSize:14.0f andColor:[UIColor grayColor]];
    
    [alertMessageBannerView_ setBackgroundColor:[UIColor clearColor]];
    [iconAlertViewBanner_ setImage:[[ImagesCache getInstance] imageNamed:BLUE_ICON_ALERT_IMAGE_FILE_NAME] forState:UIControlStateNormal];
    
    [beneficiaryLabel_ setText:NSLocalizedString(CARD_PAYMENT_OWNER_TEXT_KEY, nil)];

    beneficiaryTextField_.placeholder = NSLocalizedString(CARD_PAYMENT_OWNER_HINT_TEXT_KEY, nil);
    
    if (inputAccessoryView_ == nil) {
        
        inputAccessoryView_ = [[MOKInputAccessoryView alloc] init];
        [inputAccessoryView_ auxiliaryButtonVisible:NO];
        [inputAccessoryView_ previousAndNextResponderButtonsVisible:NO];
        [inputAccessoryView_ okButtonEnabled:YES];
        inputAccessoryView_.delegate = self;
        [inputAccessoryView_ setPreviousButtonTitle:NSLocalizedString(TXT_MOK_EDITABLE_VIEW_CONTROLLER_PREVIOUS, nil)];
        [inputAccessoryView_ setNextButtonTitle:NSLocalizedString(TXT_MOK_EDITABLE_VIEW_CONTROLLER_NEXT, nil)];
        [inputAccessoryView_ setOKButtonTitle:NSLocalizedString(TXT_MOK_EDITABLE_VIEW_CONTROLLER_OK, nil)];
        
    }

    [beneficiaryTextField_ setInputAccessoryView:inputAccessoryView_];
    
    UIView *superView = [self containerView];
    
    [superView addSubview:otherBankExtraView_];
    [superView bringSubviewToFront:otherBankExtraView_];
    
    [superView addSubview:ownAccountView_];
    [superView bringSubviewToFront:ownAccountView_];
    
    [self setScrollableView:superView];
    
   
    [self layoutViews];


}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]) - CGRectGetHeight([operationTitleHeader_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (editableViews_ == nil) {
        
        editableViews_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [editableViews_ removeAllObjects];
        
    }
    if(![self hasNavigateForward]){
        [ownAccountLabel setHidden:YES];
        [ownAccountView_ setHidden:YES];
        [otherBankExtraView_ setHidden:YES];
        [beneficiaryLabel_ setHidden:YES];
        [beneficiaryTextField_ setHidden:YES];
        [alertMessageBannerView_ setHidden:YES];
         selectedIndex_ = -1;
    }


    
    if (TransferToOtherBankAccountsHelper_ != nil || [foOperationHelper_ isKindOfClass:[FOTransferToOtherBankAccount class]]) {
         [typeTransferLabel_ setText:NSLocalizedString(@"Selecciona el tipo de transferencia a realizar", nil)];
        
        [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:otherBankExtraView_.
                                             beneficiaryNameTextField,
                                             otherBankExtraView_.docTypeCombo,
                                             otherBankExtraView_.docTypeTextField,
                                             nil]];
        [transferButton_ setTitle:NSLocalizedString(TRANSFER_DO_TRANSFER_TEXT_KEY, nil) forState:UIControlStateNormal];

        [messageLabelViewBanner_ setText:NSLocalizedString(@"Consulta el tipo de transferencia", nil)];
        if(![self hasNavigateForward]){
            [otherBankExtraView_.beneficiaryNameTextField setText:@""];
            [otherBankExtraView_.docTypeTextField setText:@""];
        }

        if(foOperationHelper_){

            [operationTitleHeader_ setTitle:NSLocalizedString(FO_HEADER_TITLE_KEY, nil)];
            if([((FOTransferToOtherBankAccount*)foOperationHelper_).typeFlow isEqualToString:@"N"])
                [self visibleTransferByScheduleOptions];
            else
                [alertMessageBannerView_ setHidden:NO];
            
            
        }else{
            TransferOperationHelper *transferOperationHelper = [self TransferToOtherBankAccountsHelper];
            [operationTitleHeader_ setTitle:[transferOperationHelper localizedTransferOperationTypeString]];
            
            if([TransferToOtherBankAccountsHelper_.typeFlow  isEqualToString:@"N"])
            {
                 [self visibleTransferByScheduleOptions];
            } else
                [alertMessageBannerView_ setHidden:NO];
            
        }
        
       

    }else if(processCard_ !=nil || [foOperationHelper_ isKindOfClass:[FOPaymentCOtherBank class]]){
      
        [typeTransferLabel_ setText:NSLocalizedString(@"Selecciona el tipo de pago a realizar", nil)];
        [editableViews_ addObjectsFromArray:[NSArray arrayWithObjects:
                                             beneficiaryTextField_,
                                             nil]];
        [transferButton_ setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil) forState:UIControlStateNormal];

        [messageLabelViewBanner_ setText:NSLocalizedString(@"Consulta el tipo de pago", nil)];
        if (![self hasNavigateForward]) {
            
            [beneficiaryTextField_ setText:@""];
    
        }
        if(foOperationHelper_){
              if([((FOPaymentCOtherBank*)foOperationHelper_).typeFlow isEqualToString:@"N"])
              {
                  
                  [self visiblePaymentByScheduleOptions];
              } else
                  [alertMessageBannerView_ setHidden:NO];
            
                [operationTitleHeader_ setTitle:NSLocalizedString(FO_HEADER_TITLE_KEY, nil)];
       }
       else{
           [processCard_ setDelegate:self];
           if([processCard_.typeFlow isEqualToString:@"N"])
           {
               [processCard_ setTypeFlowChoosen:@"PH"];
               [self visiblePaymentByScheduleOptions];
           } else{
               [alertMessageBannerView_ setHidden:NO];
           }
           
          
           
         
            
        [operationTitleHeader_ setTitle:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TITLE_TEXT_KEY, nil)];
      }
            
    

    }
    
    
    [self displayStoredInformation];

    [self setHasNavigateForward:NO];
   
    [typeTransferTableView_ reloadData];
    

    
}
-(void)visibleTransferByScheduleOptions{
    [ownAccountLabel setHidden:NO];
    [ownAccountView_ setHidden:NO];
    [otherBankExtraView_ setHidden:NO];
  

}
-(void)visiblePaymentByScheduleOptions{
    [beneficiaryLabel_ setHidden:NO];
    [beneficiaryTextField_ setHidden:NO];
    
}

-(void)displayStoredInformation
{
    if ([self isViewLoaded]) {

        NSMutableArray *array = [NSMutableArray array];

        if(TransferToOtherBankAccountsHelper_ || [foOperationHelper_ isKindOfClass:[FOTransferToOtherBankAccount class]]){
            NSArray *docTypeList = (TransferToOtherBankAccountsHelper_)?TransferToOtherBankAccountsHelper_.documentTypeList: ((FOTransferToOtherBankAccount*)foOperationHelper_).documentTypeList;
        
            for (NSString *type in docTypeList) {
            
                [array addObject:type.description];
            }
        
            otherBankExtraView_.docTypeCombo.stringsList = [NSArray arrayWithArray:array];
            [array removeAllObjects];
        
            [otherBankExtraView_.docTypeCombo setSelectedIndex:TransferToOtherBankAccountsHelper_.selectedDocumentTypeIndex];

        }
        
        if(foOperationHelper_){
            if([foOperationHelper_ isKindOfClass:[FOTransferToOtherBankAccount class]]){
                
                otherBankExtraView_.beneficiaryNameTextField.text = ((FOTransferToOtherBankAccount*)foOperationHelper_).beneficiary;
                otherBankExtraView_.docTypeTextField.text =((FOTransferToOtherBankAccount*)foOperationHelper_).documentNumber;
                NSUInteger index = 0;
                for(Document *doc in ((FOTransferToOtherBankAccount*)foOperationHelper_).documentTypeList){
                    
                    if([[doc code] isEqualToString:((FOTransferToOtherBankAccount*)foOperationHelper_).beneficiaryDocumentTypeId]){
                        [otherBankExtraView_.docTypeCombo setSelectedIndex:index];
                        break;
                    }
                    
                    index++;
                }
                
            }else if([foOperationHelper_ isKindOfClass:[FOPaymentCOtherBank class]]){
                
                beneficiaryTextField_.text = ((FOPaymentCOtherBank*)foOperationHelper_).beneficiary;
                [beneficiaryTextField_ setEnabled:NO];
            }
        }
        
        if(![self hasNavigateForward])
            [self layoutViews];
    }
    
    
    
    NSString *vienedeFrecuente=[[NSUserDefaults standardUserDefaults] objectForKey:@"vienedeFrecuente"];
    
    if ([vienedeFrecuente isEqualToString:@"NO"]) {
        otherBankExtraView_.docTypeTextField.text = @"";
    }
    else
    {
        otherBankExtraView_.docTypeTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"docNumber"];
        
    }
    
    
}


/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect scrollFrame = [transparentScroll_ frame];
    scrollFrame.size.height = CGRectGetMinY([brandingImageView_ frame]) - CGRectGetHeight([operationTitleHeader_ frame]);
    [self setScrollNominalFrame:scrollFrame];
    
}
/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the confirmation response and stores the information
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationTransferConfirmationResponseReceived
                                                  object:nil];
    [super viewWillDisappear:animated];

}
-(void)layoutViews{
    
    if ([self isViewLoaded]) {
        
        NSString *typeFlow = @"";
        
        if(TransferToOtherBankAccountsHelper_){
            typeFlow = TransferToOtherBankAccountsHelper_.typeFlow;
        }else if(processCard_){
            typeFlow = processCard_.typeFlow;
           
        }else if(foOperationHelper_){
            
            if([foOperationHelper_ isKindOfClass:[FOTransferToOtherBankAccount class]]){
                typeFlow =  ((FOTransferToOtherBankAccount*)foOperationHelper_).typeFlow;
                
            }else if([foOperationHelper_ isKindOfClass:[FOPaymentCOtherBank class]]){
                typeFlow =  ((FOPaymentCOtherBank*)foOperationHelper_).typeFlow;
                
            }
        }

        
    CGRect frame = [typeTransferLabel_ frame];
    
    frame.origin.y = VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
     
    CGFloat height = [Tools labelHeight:typeTransferLabel_ forText:typeTransferLabel_.text];
    
    if ([typeFlow isEqualToString:@"A"]) {
        frame.size.height = height;
    }else{
         frame.size.height = 0.0f;
        
    }
        
    typeTransferLabel_.frame = frame;
    CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;

    
    frame = [typeTransferTableView_ frame];
    frame.origin.y = auxPos;
    
        
    if ([typeFlow isEqualToString:@"A"]) {
        
        frame.size.height = [CheckTypeTransferCell cellHeight]*2;
    }else{
        frame.size.height =  0.0f;
    }
    
    [typeTransferTableView_ setFrame:frame];

    auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
    if(TransferToOtherBankAccountsHelper_ || [foOperationHelper_ isKindOfClass:[FOTransferToOtherBankAccount class]])
    {
        frame = [ownAccountLabel frame];
        frame.origin.x = 10.0f;
    
    
        frame.origin.y = auxPos;

        [ownAccountLabel setFrame:frame];
    
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = [ownAccountView_ frame];
        frame.origin.x = 10.0f;
        
        frame.origin.y = auxPos;
        [ownAccountView_ setFrame:frame];
    
        
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        frame = [otherBankExtraView_ frame];
        frame.origin.x = 0.0f;
    
    
        frame.origin.y = auxPos;
        [otherBankExtraView_ setFrame:frame];
        
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
    }else{
        frame = [beneficiaryLabel_ frame];
        frame.origin.y = auxPos;
        [beneficiaryLabel_ setFrame:frame];
        
         auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = [beneficiaryTextField_ frame];
        frame.origin.y = auxPos;
        
        [beneficiaryTextField_ setFrame:frame];
        
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
    }
        
        
    frame = [transferButton_ frame];
    
    frame.origin.y = auxPos;
    
    [transferButton_ setFrame:frame];
    
    auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT2;
    
    UIView *containerView = self.containerView;
    frame = containerView.frame;
    frame.size.height = auxPos;
    containerView.frame = frame;
    
    [self recalculateScroll];
        
      
      if ([typeFlow isEqualToString:@"A"] && selectedIndex_==-1) {
                [self showInmediateOptions];
        }
    
    }
    
}
-(void)showInmediateOptions{
     [topAletViewBannerSeparator_ setHidden:YES];
    CGRect frame = [alertMessageBannerView_ frame];
    frame.origin.y = CGRectGetMaxY([typeTransferTableView_ frame]);
    [alertMessageBannerView_ setFrame:frame];
    
    CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
    
    frame = [transferButton_ frame];
    frame.origin.y = auxPos;
    
    [transferButton_ setFrame:frame];
    auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT2;
    
    UIView *containerView = self.containerView;
    frame = containerView.frame;
    frame.size.height = auxPos;
    containerView.frame = frame;
    
    [self recalculateScroll];
    
}
-(void)showByScheduleOptions{
     [topAletViewBannerSeparator_ setHidden:NO];
    CGRect frame = [alertMessageBannerView_ frame];
    frame.origin.y = VERTICAL_DISTANCE_TO_NEAR_ELEMENT+ CGRectGetMaxY((TransferToOtherBankAccountsHelper_ || [foOperationHelper_ isKindOfClass:[FOTransferToOtherBankAccount class]])?[otherBankExtraView_ frame]:[beneficiaryTextField_ frame]);
    
    [alertMessageBannerView_ setFrame:frame];
    
    CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
    
    frame = [transferButton_ frame];
    frame.origin.y = auxPos;
    
    [transferButton_ setFrame:frame];
    
    auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT2;
    
    UIView *containerView = self.containerView;
    frame = containerView.frame;
    frame.size.height = auxPos;
    containerView.frame = frame;
    
    [self recalculateScroll];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


+ (TransferTINOnlineStepViewController *)transferTINOnlineStepViewController
{
    return [[[TransferTINOnlineStepViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
}

-(void) docTypeComboPressed:(NXTComboButton*) comboButton{
    
    [self editableViewHasBeenClicked:comboButton];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger totalRows = 0;
    if(TransferToOtherBankAccountsHelper_)
        totalRows =[TransferToOtherBankAccountsHelper_.typeFlow isEqualToString:@"A"]?2:0;
    else if(processCard_)
        totalRows =[processCard_.typeFlow isEqualToString:@"A"]?2:0;
    else if([foOperationHelper_ isKindOfClass:[FOTransferToOtherBankAccount class]]){
        totalRows = [[(FOTransferToOtherBankAccount*)foOperationHelper_  typeFlow] isEqualToString:@"A"]?2:0;
    }
    else if([foOperationHelper_ isKindOfClass:[FOPaymentCOtherBank class]]){
        totalRows = [[(FOPaymentCOtherBank*)foOperationHelper_  typeFlow] isEqualToString:@"A"]?2:0;
    }
    
    return totalRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [CheckTypeTransferCell cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CheckTypeTransferCell *cell = [tableView dequeueReusableCellWithIdentifier:[CheckTypeTransferCell cellIdentifier]];
    
    if (cell == nil) {
        cell = [[[CheckTypeTransferCell checkTypeTransferCell] retain] autorelease];
    }
    
    [cell setCheckActive:(selectedIndex_ == indexPath.row)];
    NSString *commissionL = @"";
    NSString *totalPaymentL = @"";
    NSString *commissionN =@"";
    NSString *totalPaymentN = @"";
    
    if(TransferToOtherBankAccountsHelper_){
        NSString *currencySimbol = [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:TransferToOtherBankAccountsHelper_.currency];
        commissionL = [NSString stringWithFormat:@"%@%@",currencySimbol,TransferToOtherBankAccountsHelper_.commissionL];
        totalPaymentL = [NSString stringWithFormat:@"%@%@",currencySimbol,TransferToOtherBankAccountsHelper_.totalPaymentL];
        commissionN =[NSString stringWithFormat:@"%@%@",currencySimbol,TransferToOtherBankAccountsHelper_.commissionN];
        totalPaymentN = [NSString stringWithFormat:@"%@%@",currencySimbol,TransferToOtherBankAccountsHelper_.totalPaymentN];
    }else if(processCard_){
        NSString *currencySimbol = [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:processCard_.selectedCurrency];
        commissionL = [NSString stringWithFormat:@"%@%@",currencySimbol,processCard_.commissionL];
        totalPaymentL = [NSString stringWithFormat:@"%@%@",currencySimbol,processCard_.totalPayL];
        commissionN =[NSString stringWithFormat:@"%@%@",currencySimbol,processCard_.commissionN];
        totalPaymentN = [NSString stringWithFormat:@"%@%@",currencySimbol,processCard_.totalPayN];
    }else if(foOperationHelper_){
        
        if([foOperationHelper_ isKindOfClass:[FOTransferToOtherBankAccount class]]){
            NSString *currencySimbol = [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:((FOTransferToOtherBankAccount*)foOperationHelper_).currency];
            commissionL = [NSString stringWithFormat:@"%@%@",currencySimbol,((FOTransferToOtherBankAccount*)foOperationHelper_).commissionL];
            totalPaymentL = [NSString stringWithFormat:@"%@%@",currencySimbol,((FOTransferToOtherBankAccount*)foOperationHelper_).totalPaymentL];
            commissionN =[NSString stringWithFormat:@"%@%@",currencySimbol,((FOTransferToOtherBankAccount*)foOperationHelper_).commissionN];
            totalPaymentN = [NSString stringWithFormat:@"%@%@",currencySimbol,((FOTransferToOtherBankAccount*)foOperationHelper_).totalPaymentN];
        }else if([foOperationHelper_ isKindOfClass:[FOPaymentCOtherBank class]]){
            
            NSString *currencySimbol = [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:((FOPaymentCOtherBank*)foOperationHelper_).currency];
            commissionL =  [NSString stringWithFormat:@"%@%@",currencySimbol,  ((FOPaymentCOtherBank*)foOperationHelper_).commissionL];
            totalPaymentL = [NSString stringWithFormat:@"%@%@",currencySimbol,((FOPaymentCOtherBank*)foOperationHelper_).totalPaymentL];
            commissionN =[NSString stringWithFormat:@"%@%@",currencySimbol,((FOPaymentCOtherBank*)foOperationHelper_).commissionN];
            totalPaymentN = [NSString stringWithFormat:@"%@%@",currencySimbol,((FOPaymentCOtherBank*)foOperationHelper_).totalPaymentN];
        }

    }
        switch (indexPath.row) {
            case 0:
                [cell.titleLabel setText:@"Inmediata"];
                [cell.comisionValueLabel setText:commissionL];
                [cell.totalPaymentValueLabel setText:totalPaymentL];
                break;
            case 1:
                [cell.titleLabel setText:@"Por horarios"];
                [cell.comisionValueLabel setText:commissionN];
                [cell.totalPaymentValueLabel setText:totalPaymentN];
                break;
            default:
                break;
        }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
    
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedIndex_ = indexPath.row;
    
    [typeTransferTableView_ reloadData];

    switch (indexPath.row) {
        case 0:
            if(TransferToOtherBankAccountsHelper_ || [foOperationHelper_ isKindOfClass:[FOTransferToOtherBankAccount class]]){
                [ownAccountView_ setHidden:YES];
                [otherBankExtraView_ setHidden:YES];
                [ownAccountLabel setHidden:YES];
            }else{
                [beneficiaryTextField_ setHidden:YES];
                [beneficiaryLabel_ setHidden:YES];
            }
            if(!foOperationHelper_)
                [beneficiaryTextField_ setText:@""];
            
            [self showInmediateOptions];

            if(foOperationHelper_){
                ((FOTransferToOtherBankAccount*)foOperationHelper_).selectedTransferTypeIndex = selectedIndex_;
            }

            break;
        case 1:
            if(TransferToOtherBankAccountsHelper_ || [foOperationHelper_ isKindOfClass:[FOTransferToOtherBankAccount class]]){
                [ownAccountView_ setHidden:NO];
                [otherBankExtraView_ setHidden:NO];
                [ownAccountLabel setHidden:NO];
            }else{
                [beneficiaryTextField_ setHidden:NO];
                [beneficiaryLabel_ setHidden:NO];
                if(foOperationHelper_){
                    ((FOTransferToOtherBankAccount*)foOperationHelper_).selectedTransferTypeIndex = selectedIndex_;
                }

            }
            [self showByScheduleOptions];
            break;
        default:
            break;
    }
}

-(IBAction)transferButtonTapped{
    
    [self storeInformationIntoHelper];
    
    if(TransferToOtherBankAccountsHelper_){
        
        TransferToOtherBankAccounts *transferOperationHelper = self.TransferToOtherBankAccountsHelper;
        NSString *validationErrorMessage = [transferOperationHelper startSecondTransferRequestDataValidation];
        
        if (validationErrorMessage != nil) {
            
            [Tools showAlertWithMessage:validationErrorMessage
                                  title:NSLocalizedString(INFO_MESSAGE_TITLE_KEY, nil)];
            
        } else {
            
            
            if ([transferOperationHelper startTypeTransferToConfirmRequest]) {
                
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(transferResponseReceived:)
                                                             name:kNotificationTransferConfirmationResponseReceived
                                                           object:nil];
                [self.appDelegate showActivityIndicator:poai_Both];
                
            } else {
                
                [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATIONS_ERROR_TEXT_KEY, nil)];
                
            }
            
        }
    }else if(processCard_){
        
        [processCard_ startTINPaymentConfirmationRequest];
        
    }else if(foOperationHelper_ && [foOperationHelper_ isKindOfClass:[FOTransferToOtherBankAccount class]]){
        
        FOTransferToOtherBankAccount *fOOperationHelper = (FOTransferToOtherBankAccount*)self.foOperationHelper;
        NSString *validationErrorMessage = [fOOperationHelper startSecondTransferRequestDataValidation];
        
        if (validationErrorMessage != nil) {
            
            [Tools showAlertWithMessage:validationErrorMessage
                                  title:NSLocalizedString(INFO_MESSAGE_TITLE_KEY, nil)];
            
        } else {
            
            if ([fOOperationHelper startTypeTransferToConfirmRequest]) {
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initialFrequentOperationResponse:) name:[fOOperationHelper notificationTINConfirmationKey] object:nil];
                
                [self.appDelegate showActivityIndicator:poai_Both];
                
            } else {
                
                [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATIONS_ERROR_TEXT_KEY, nil)];
                
            }
            
        }

        
    }else if(foOperationHelper_ && [foOperationHelper_ isKindOfClass:[FOPaymentCOtherBank class]]){
        
        FOPaymentCOtherBank *fOOperationHelper = (FOPaymentCOtherBank*)self.foOperationHelper;
        NSString *validationErrorMessage = [fOOperationHelper startSecondTransferRequestDataValidation];
        
        if (validationErrorMessage != nil) {
            
            [Tools showAlertWithMessage:validationErrorMessage
                                  title:NSLocalizedString(INFO_MESSAGE_TITLE_KEY, nil)];
            
        } else {
            
            if ([fOOperationHelper startTypePaymentRequest]) {
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initialFrequentOperationResponse:) name:[fOOperationHelper notificationTINConfirmationKey] object:nil];
                
                [self.appDelegate showActivityIndicator:poai_Both];
                
            } else {
                
                [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATIONS_ERROR_TEXT_KEY, nil)];
                
            }
            
        }
    }
    
}

#pragma mark -
#pragma mark Notification Responses
- (void)initialFrequentOperationResponse:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[foOperationHelper_ notificationTINConfirmationKey] object:nil];
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (![response isError]) {
        if ([self.foOperationHelper fOResponseReceived:response]) {
            [self displayTransferSecondStepView];

        }
    } else {
        
        [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
        
    }
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    
    if ([currentVersion compare: MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION options: NSNumericSearch] == NSOrderedAscending) {
        
        if (popButtonsView_.superview == nil) {
            
            popButtonsState_ = pbse_Show;
            
        } else {
            
            popButtonsState_ = pbse_Relocate;
            
        }
        
    }
    
    return YES;
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self editableViewHasBeenClicked:textField];
}

/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
 
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
   // NSUInteger resultLength = [resultString length];
    
    if (textField == otherBankExtraView_.beneficiaryNameTextField) {
        
        if([Tools isValidText:resultString forCharacterString:ALPHABET_VALID_CHARACTERS]) {
            
            result = YES;
            
        }
        
        else {
            
            result = NO;
            
        }
    }
    else if (textField == otherBankExtraView_.docTypeTextField) {
        
        Document *document = (TransferToOtherBankAccountsHelper_)? [TransferToOtherBankAccountsHelper_.documentTypeList objectAtIndex:otherBankExtraView_.docTypeCombo.selectedIndex] : [((FOTransferToOtherBankAccount*)foOperationHelper_).documentTypeList objectAtIndex:((FOTransferToOtherBankAccount*)foOperationHelper_).selectedDocumentTypeIndex] ;
        NSString *code = document.code;
        
        result = [self validateTextFieldDocumentNumber:code textToValidate:resultString];
    }
    
    return result;

}

-(IBAction)showLegalTerms:(id)sender{
    
    if (termsAndConsiderationsViewController_ == nil) {
        
        termsAndConsiderationsViewController_ = [[TermsAndConsiderationsViewController TermsAndConsiderationsViewController] retain];
        
    }
    
    [termsAndConsiderationsViewController_ setURLText:disclaimer_
                                           headerText:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TITLE_TEXT_KEY, nil)];
    hasNavigateForward_ = YES;

    [self.navigationController pushViewController:termsAndConsiderationsViewController_ animated:YES];
}
/*
 * validate the doc number document
 *
 *@param the document type
 */
- (BOOL)validateTextFieldDocumentNumber:(NSString *)code textToValidate:(NSString *)text {
    
    BOOL valid = YES;
    
    NSInteger textLength = [text length];
    
    NSCharacterSet *numbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *lowercaseLetterSet = [NSCharacterSet lowercaseLetterCharacterSet];
    NSCharacterSet *uppercaseLetterSet = [NSCharacterSet uppercaseLetterCharacterSet];
    
    if([code isEqualToString:DOC_TYPE_DNI]) {
        
        if(textLength <= 8 ) {
            
            unichar charExtracted;
            NSInteger size = [text length];
            for (NSInteger i = 0; (i < size) && (valid); i++) {
                charExtracted = [text characterAtIndex:i];
                valid = [numbersSet characterIsMember:charExtracted];
            }
        }
        else {
            
            valid = NO;
        }
    }
    else if ([code isEqualToString:DOC_TYPE_RUC]) {
        
        if(textLength <= 11) {
            
            unichar charExtracted;
            NSInteger size = [text length];
            for (NSInteger i = 0; (i < size) && (valid); i++) {
                charExtracted = [text characterAtIndex:i];
                valid = [numbersSet characterIsMember:charExtracted];
            }
        }
        else {
            
            valid = NO;
        }
    }
    else if ([code isEqualToString:DOC_TYPE_MILITAR_ID]) {
        
        if(textLength <= 8) {
            
            unichar charExtracted;
            NSInteger size = [text length];
            for (NSInteger i = 0; (i < size) && (valid); i++) {
                charExtracted = [text characterAtIndex:i];
                valid = ([numbersSet characterIsMember:charExtracted] ||
                         [lowercaseLetterSet characterIsMember:charExtracted] ||
                         [uppercaseLetterSet characterIsMember:charExtracted]);
            }
        }
        else {
            
            valid = NO;
        }
    }
    else if ([code isEqualToString:DOC_TYPE_PASAPORT]) {
        
        if(textLength <= 11) {
            
            unichar test1 = '-';
            unichar test2 = '/';
            unichar charExtracted;
            NSInteger size = [text length];
            for (NSInteger i = 0; (i < size) && (valid); i++) {
                charExtracted = [text characterAtIndex:i];
                valid = ([numbersSet characterIsMember:charExtracted] ||
                         ([lowercaseLetterSet characterIsMember:charExtracted]) ||
                         ([uppercaseLetterSet characterIsMember:charExtracted]) ||
                         (test1 == charExtracted) || (test2 == charExtracted));
            }
        }
        else {
            
            valid = NO;
        }
    }
    else if ([code isEqualToString:DOC_TYPE_INMIGRATION_ID]) {
        
        if(textLength <= 11) {
            
            unichar test1 = '-';
            unichar test2 = '/';
            unichar charExtracted;
            NSInteger size = [text length];
            for (NSInteger i = 0; (i < size) && (valid); i++) {
                charExtracted = [text characterAtIndex:i];
                valid = ([numbersSet characterIsMember:charExtracted] ||
                         ([lowercaseLetterSet characterIsMember:charExtracted]) ||
                         ([uppercaseLetterSet characterIsMember:charExtracted]) ||
                         (test1 == charExtracted) || (test2 == charExtracted));
            }
        }
        else {
            
            valid = NO;
        }
    }
    return valid;
}


/*
 * Informs the delegate that a value has been selected
 */
- (void)NXTComboButtonValueSelected:(NXTComboButton *)comboButton {
    
   if (comboButton == otherBankExtraView_.docTypeCombo) {
        if(TransferToOtherBankAccountsHelper_)
            [TransferToOtherBankAccountsHelper_ setSelectedDocumentTypeIndex:otherBankExtraView_.docTypeCombo.selectedIndex];
       else
           [((FOTransferToOtherBankAccount*)foOperationHelper_) setSelectedDocumentTypeIndex:otherBankExtraView_.docTypeCombo.selectedIndex];
        
        Document *document = (TransferToOtherBankAccountsHelper_)?[TransferToOtherBankAccountsHelper_.documentTypeList objectAtIndex:otherBankExtraView_.docTypeCombo.selectedIndex]:[((FOTransferToOtherBankAccount*)foOperationHelper_).documentTypeList objectAtIndex:((FOTransferToOtherBankAccount*)foOperationHelper_).selectedDocumentTypeIndex] ;
        NSString *code = document.code;
        
        if( ([code isEqualToString:DOC_TYPE_DNI]) || ([code isEqualToString:DOC_TYPE_RUC]) ) {
            
            [otherBankExtraView_.docTypeTextField setKeyboardType:UIKeyboardTypeNumberPad];
            
        }
        else if( ([code isEqualToString:DOC_TYPE_MILITAR_ID]) || ([code isEqualToString:DOC_TYPE_PASAPORT]) || ([code isEqualToString:DOC_TYPE_INMIGRATION_ID]) ) {
            
            [otherBankExtraView_.docTypeTextField setKeyboardType:UIKeyboardTypeDefault];
        }
        [otherBankExtraView_.docTypeTextField setText:@""];
    }
}

#pragma mark -
#pragma mark Notifications management

/*
 * Receives the response of the transfer confirmation
 */
- (void)transferResponseReceived:(NSNotification *)notification {
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationTransferConfirmationResponseReceived object:nil];
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (!response.isError) {
        
        
        if ([TransferToOtherBankAccountsHelper_ transferResponseReceived:response]) {
            
            [self displayTransferSecondStepView];
            
        }else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
            
        }
    }

}

-(void)storeInformationIntoHelper{
 
    
         if(TransferToOtherBankAccountsHelper_){
             [TransferToOtherBankAccountsHelper_ setSelectedTransferTypeIndex:selectedIndex_];
             [TransferToOtherBankAccountsHelper_ setItfSelected:ownAccountView_.ownAccountSwitch.on];
             [TransferToOtherBankAccountsHelper_ setBeneficiary:otherBankExtraView_.beneficiaryNameTextField.text];
             [TransferToOtherBankAccountsHelper_ setDocumentNumber:otherBankExtraView_.docTypeTextField.text];
             
         }else if(processCard_){
        
                [processCard_ setTypeFlowChoosen:selectedIndex_==0?@"IN":(selectedIndex_==1)?@"PH":@""];
             if(selectedIndex_==1 || [[processCard_ typeFlow] isEqualToString:@"N"]){
                 [processCard_ setBeneficiary:beneficiaryTextField_.text];
                 [processCard_ setTypeFlowChoosen:@"PH"];
             }
        }else if(foOperationHelper_!=nil &&  [foOperationHelper_ foOperationType]==FOTEPaymentOtherBankCard){
            [((FOPaymentCOtherBank*)foOperationHelper_) setSelectedTransferTypeIndex:selectedIndex_];
            if(selectedIndex_==1 || [[((FOPaymentCOtherBank*)foOperationHelper_) typeFlow] isEqualToString:@"N"])
                     [((FOPaymentCOtherBank*)foOperationHelper_) setBeneficiary:beneficiaryTextField_.text];
            
        }else if(foOperationHelper_!=nil &&  [foOperationHelper_ foOperationType]==FOTETransferToOtherBankAccount){
            [((FOTransferToOtherBankAccount*)foOperationHelper_) setSelectedTransferTypeIndex:selectedIndex_];
            [((FOTransferToOtherBankAccount*)foOperationHelper_) setIsItf:ownAccountView_.ownAccountSwitch.on];
            [((FOTransferToOtherBankAccount*)foOperationHelper_) setBeneficiary:otherBankExtraView_.beneficiaryNameTextField.text];
            [((FOTransferToOtherBankAccount*)foOperationHelper_) setDocumentNumber:otherBankExtraView_.docTypeTextField.text];
        }
       
}


#pragma mark -
#pragma mark Displaying views

/*
 * Displays the transfer second step view controller
 */
- (void)displayTransferSecondStepView {
    
    if(foOperationHelper_)
    {
        FrequentOperationStepTwoViewController *stepTwoView = [FrequentOperationStepTwoViewController frequentOperationStepTwoViewController];
        self.hasNavigateForward = YES;
        [stepTwoView setFoOperationHelper:foOperationHelper_];
        
        [self.navigationController pushViewController:stepTwoView animated:TRUE];
        return;
    }
    
    if (transfersStepTwoViewController_ != nil) {
        
        transfersStepTwoViewController_ = nil;
        [transfersStepTwoViewController_ release];
    }
    
    transfersStepTwoViewController_ = [[TransfersStepTwoViewController transfersStepTwoViewController] retain];
    
    self.hasNavigateForward = YES;
    
    TransferOperationHelper *transferOperationHelper = TransferToOtherBankAccountsHelper_;
    
    transfersStepTwoViewController_.transferOperationHelper = transferOperationHelper;
    transfersStepTwoViewController_.isCashMobileSend = NO;
    [self.navigationController pushViewController:transfersStepTwoViewController_
                                         animated:YES];
    
}
#pragma mark -
#pragma mark PaymentBaseProcessDelegate

/*
 * Delegate is notified when the initialization has finished
 */
- (void)confirmationAnalysisHasFinished {
    
    if (paymentConfirmationViewController_ == nil) {
        
        paymentConfirmationViewController_ = [[PaymentConfirmationViewController paymentConfirmationViewController] retain];
        
    }
    
    hasNavigateForward_ = YES;
    
    [paymentConfirmationViewController_ setProcess:processCard_];
    [self.navigationController pushViewController:paymentConfirmationViewController_
                                         animated:YES];
    
}


/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    if(TransferToOtherBankAccountsHelper_){
        result.customTitleView.topLabelText = NSLocalizedString(TRANSFER_TITLE_TEXT_KEY, nil);
    }else if(processCard_){
         result.customTitleView.topLabelText = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_CARDS_TITLE_LOWER_TEXT_KEY, nil)];
    }else{
        result.customTitleView.topLabelText = NSLocalizedString(FO_TITLE_TEXT_REACTIVE_KEY, nil);
    }
    
    return result;
    
}

#pragma mark -
#pragma mark MOKAccessoryViewDelegate protocol selectors

/**
 * Informs the delegate that OK button was tapped. Editing is stopped in the view
 *
 * @param inputAccessoryView The imput accessory view triggering the event.
 */
- (void)inputAccessoryViewOKButtonTapped:(MOKInputAccessoryView *)inputAccessoryView {
    
    [self.view endEditing:YES];
    
}

/**
 * Informs the delegate that auxiliary button button was tapped.
 *
 * @param inputAccessoryView The imput accessory view triggering the event.
 */
- (void)inputAccessoryViewAuxiliaryButtonTapped:(MOKInputAccessoryView *)inputAccessoryView {
    
    //Do nothing (by now)
    
}
/**
 * Informs the delegate that previous responder button was tapped. The previous responder is checked.
 *
 * @param inputAccessoryView The imput accessory view triggering the event.
 */
- (void)inputAccessoryViewPreviousResponderButtonTapped:(MOKInputAccessoryView *)inputAccessoryView {

}
/**
 * Informs the delegate that next responder button was tapped. The next responder is checked
 *
 * @param inputAccessoryView The imput accessory view triggering the event.
 */
- (void)inputAccessoryViewNextResponderButtonTapped:(MOKInputAccessoryView *)inputAccessoryView {
}

@end
