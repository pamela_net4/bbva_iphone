/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TermsAndConsiderationsViewController.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXTNavigationItem.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "Tools.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"TermsAndConsiderationsViewController"

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        15.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                      12.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLEST_SIZE                                     10.0f

/**
 * Defines the maximum textview height
 */
#define MAX_TEXTVIEW_HEIGHT                                         250.0f

#pragma mark -

/**
 * TermsAndConsiderationsViewController private extension
 */
@interface TermsAndConsiderationsViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releaseTermsAndConsiderationsViewControllerGraphicElements;

/**
 * Displays the stored information
 *
 * @private
 */
- (void)displayTermsAndConsiderationsInformation;

@end


#pragma mark -

@implementation TermsAndConsiderationsViewController

#pragma mark -
#pragma mark Properties

@synthesize webView = webView_;
@synthesize brandingImageView = brandingImageView_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self releaseTermsAndConsiderationsViewControllerGraphicElements];
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseTermsAndConsiderationsViewControllerGraphicElements];
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseTermsAndConsiderationsViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases the graphic elements
 */
- (void)releaseTermsAndConsiderationsViewControllerGraphicElements {
    
    [operationTypeHeader_ release];
    operationTypeHeader_ = nil;
    
    [webView_ release];
    webView_ = nil;
    
    [brandingImageView_ release];
    brandingImageView_ = nil;
    
}

#pragma mark -
#pragma mark View managment

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIView *view = self.view;
    
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    
    if (operationTypeHeader_ == nil) {
        
        operationTypeHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
        operationTypeHeader_.frame = CGRectMake(0.0f, 0.0f, view.frame.size.width, [SimpleHeaderView height]);
        [view addSubview:operationTypeHeader_];
        
    }
    

    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    
    webView_.scalesPageToFit = YES;
	webView_.backgroundColor = [UIColor clearColor];
    webView_.delegate = self;
    
    [self displayTermsAndConsiderationsInformation];
    
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    webView_.frame = CGRectMake(0.0f, CGRectGetMaxY(operationTypeHeader_.frame), self.view.frame.size.width, self.view.frame.size.height - [SimpleHeaderView height] - brandingImageView_.frame.size.height);
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
	
	[webView_ loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased TermsAndConsiderationsViewController constructed from a NIB file
 */
+ (TermsAndConsiderationsViewController *)TermsAndConsiderationsViewController {
    
    return [[[TermsAndConsiderationsViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
}

#pragma mark -
#pragma mark Graphic elements management

/*
 * Displays the stored information
 */
- (void)displayTermsAndConsiderationsInformation {
    
    if ([self isViewLoaded]) {

        [webView_ loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url_]]];
         operationTypeHeader_.title = headerTitle_;
        
    }
    
}

#pragma UIWebViewDelegate

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error{
    DLog(@"error %@", [error description]);
}


#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = title_;
    
    return result; 
    
}

#pragma mark -
#pragma mark Properties methods

/**
 * Sets the title text and the terms text
 */
- (void)setURLText:(NSString *)urlText 
        headerText:(NSString *)headerText {
    
    operationTypeHeader_.titleLabel.text = headerText;
    
    title_ = headerText;
    headerTitle_ = headerText;
    
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = headerText;
    
    url_ = urlText;
    
    [self displayTermsAndConsiderationsInformation];
        
}

/**
 * Sets the title text and the terms text
 */
- (void)setURLText:(NSString *)urlText
         titleText:(NSString*)titleText
        headerText:(NSString *)headerText {
    
    operationTypeHeader_.titleLabel.text = headerText;
    headerTitle_ = headerText;
    
    title_ = titleText;
    NXTNavigationItem *result = self.customNavigationItem;
    result.customTitleView.topLabelText = titleText;
    
    url_ = urlText;
    
    [self displayTermsAndConsiderationsInformation];
    
}


@end