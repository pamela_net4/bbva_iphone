//
//  SlideOutNavigationViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/28/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "NXTViewController.h"
#import "RadioPlayerViewController.h"
#import "OptionsMenuViewController.h"
#import "TermsAndConditionsViewController.h"
#import "BePartViewController.h"
#import "RadioPlayerFooterView.h"

@interface SlideOutNavigationViewController : NXTViewController<OptionMenuViewControllerDelegate,RadioPlayerFooterViewDelegate>{
    
    UIView *currentView_;
    UIBarButtonItem *rightButton_;
    UINavigationBar *navigationBar_;
    NSString *currentTitle_;
    RadioPlayerViewController *radioPlayerViewController_;
    OptionsMenuViewController *optionsMenuViewController_;
    BePartViewController *bePartViewController_;
    TermsAndConditionsViewController *termsAndConditionsViewController_;
    
    NXTViewController *currentViewController_;
    
    BOOL showPanel_;
    BOOL showingRightPanel_;
    
    NSInteger selectedOption_;
}
@property (nonatomic, assign) BOOL showPanel;

@property (nonatomic, assign) BOOL showingRightPanel;

/**
 * Provides read-write access to the background image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UINavigationBar *navigationBar;


@property (nonatomic, readwrite, retain) IBOutlet UIBarButtonItem *rightButton;
/**
 * Provides read-write access to the background image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *currentView;

@property (nonatomic, retain) RadioPlayerViewController *radioPlayerViewController;

@property (nonatomic, retain) OptionsMenuViewController *optionsMenuViewController;


@property (nonatomic, retain)  BePartViewController *bePartViewController;

@property (nonatomic, retain) TermsAndConditionsViewController *termsAndConditionsViewController;

@property(nonatomic,retain,readwrite) NSString *currentTitle;


+ (SlideOutNavigationViewController *)slideOutNavigationViewController;

-(IBAction)btnMovePanelLeft:(id)sender;
@end
