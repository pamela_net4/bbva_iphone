//
//  OptionsMenuViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/28/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "OptionsMenuViewController.h"
#import "RadioBBVAMenuCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"OptionsMenuViewController"

#define PLAYER_SCREEN_OPTION 0
#define JOIN_IN_SCREEN_OPTION 1
#define TERMS_SCREEN_OPTION 2
#define BACK_MAIN_OPTION 3

#define NUMBER_ROWS 4

@interface OptionsMenuViewController ()

@end

@implementation OptionsMenuViewController

@synthesize optionsTableView = optionsTableView_;
@synthesize delegate = delegate_;
@synthesize selectedIndex = selectedIndex_;

- (CALayer *)gradientBGLayerForBounds:(CGRect)bounds
{
    CAGradientLayer * gradientBG = [CAGradientLayer layer];
    gradientBG.frame = bounds;
    gradientBG.colors = @[ (id)[[UIColor BBVABlueSpectrumColorGradientBackgroundLight] CGColor], (id)[[UIColor BBVABlueSpectrumColorGradientBackgroundMidDark] CGColor], (id)[[UIColor BBVABlueSpectrumColorGradientBackgroundDark] CGColor] ];
    return gradientBG;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    ImagesCache *imagesCache = [ImagesCache getInstance];

    
    [optionRadioPlayerCell_ release];
    optionRadioPlayerCell_ = [[RadioBBVAMenuCell radioBBVAMenuCell] retain];
    [optionRadioPlayerCell_ setTitleText:@"Ponle Play a Tu Música"];
    [optionRadioPlayerCell_ setMenuIcon:[imagesCache imageNamed:RADIO_PLAYER_ICON_IMAGE_FILE_NAME]];

    [optionBePartCell_ release];
    optionBePartCell_ = [[RadioBBVAMenuCell radioBBVAMenuCell] retain];
    [optionBePartCell_ setTitleText:@"Sé Parte de Radio BBVA"];
    [optionBePartCell_ setMenuIcon:[imagesCache imageNamed:RADIO_BE_PART_ICON_IMAGE_FILE_NAME]];
    
    [optionTermsCell_ release];
    optionTermsCell_ = [[RadioBBVAMenuCell radioBBVAMenuCell] retain];
    [optionTermsCell_ setTitleText:@"Acerca de Radio BBVA"];
    [optionTermsCell_ setMenuIcon:[imagesCache imageNamed:RADIO_TERMS_ICON_IMAGE_FILE_NAME]];
    
    [optionBackMainCell_ release];
    optionBackMainCell_ = [[RadioBBVAMenuCell radioBBVAMenuCell] retain];
    [optionBackMainCell_ setTitleText:@"Volver al Menú Principal"];
    [optionBackMainCell_ setMenuIcon:[imagesCache imageNamed:RADIO_BACK_MAIN_ICON_IMAGE_FILE_NAME]];
    
    CGRect frame = [optionsTableView_ frame];
    frame.size.height = NUMBER_ROWS *[RadioBBVAMenuCell cellHeight];
    [optionsTableView_ setFrame:frame];
    
    [optionsTableView_ setBackgroundColor:[UIColor clearColor]];
    
    [optionsTableView_ setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    selectedIndex_ = 0;


}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CALayer * bgGradientLayer = [self gradientBGLayerForBounds:self.view.bounds];
    [self.view.layer insertSublayer:bgGradientLayer atIndex:0];
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 * Creates and returns an autoreleased SplashViewController constructed from a NIB file
 */
+ (OptionsMenuViewController *)optionsMenuViewController {
    
    OptionsMenuViewController *result = [[[OptionsMenuViewController alloc] initWithNibName:NIB_FILE_NAME
                                                                                     bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    RadioBBVAMenuCell *cell = nil;

    switch (indexPath.row) {
        case PLAYER_SCREEN_OPTION:
            cell = optionRadioPlayerCell_;
            break;
        case JOIN_IN_SCREEN_OPTION:
            cell = optionBePartCell_;
            break;
        case TERMS_SCREEN_OPTION:
            cell = optionTermsCell_;
            break;
        case BACK_MAIN_OPTION:
            cell = optionBackMainCell_;
            break;

        default:
            break;
    }
    
    if(selectedIndex_ == indexPath.row)
        [cell setCheckActive:YES];
    else
        [cell setCheckActive:NO];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [RadioBBVAMenuCell cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    selectedIndex_ = indexPath.row;
    
    RadioBBVAMenuCell *cell =  (RadioBBVAMenuCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    [delegate_ selectedOption:indexPath.row withTitle:[[cell topTextLabel] text]];
}

+(NSInteger) radioPlayerOptionIndex{
    return PLAYER_SCREEN_OPTION;
}
+(NSInteger) bePartOptionIndex{
    return JOIN_IN_SCREEN_OPTION;
}
+(NSInteger) termsOptionIndex{
    return TERMS_SCREEN_OPTION;
}
+(NSInteger) backToMainOptionIndex{
    return BACK_MAIN_OPTION;
}

-(void) reloadSelection{
    [optionsTableView_ reloadData];
}

@end
