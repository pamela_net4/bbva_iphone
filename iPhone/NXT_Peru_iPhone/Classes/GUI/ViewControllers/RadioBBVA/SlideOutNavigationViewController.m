//
//  SlideOutNavigationViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/28/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "SlideOutNavigationViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "UIColor+BBVA_Colors.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"


#define CORNER_RADIUS 4
#define SLIDE_TIMING .25

#define PANEL_WIDTH 60
#define NIB_FILE_NAME @"SlideOutNavigationViewController"

@interface SlideOutNavigationViewController ()<UIGestureRecognizerDelegate>
{
    CGPoint preVelocity_;
}

@property (nonatomic, assign) CGPoint preVelocity;
@end

@implementation SlideOutNavigationViewController

@synthesize radioPlayerViewController = radioPlayerViewController_;
@synthesize optionsMenuViewController = optionsMenuViewController_;
@synthesize termsAndConditionsViewController = termsAndConditionsViewController_;
@synthesize showPanel = showPanel_;
@synthesize showingRightPanel = showingRightPanel_;
@synthesize currentView = currentView_;
@synthesize preVelocity = preVelocity_;
@synthesize navigationBar = navigationBar_;
@synthesize rightButton = rightButton_;
@synthesize bePartViewController = bePartViewController_;
@synthesize currentTitle = currentTitle_;



+ (SlideOutNavigationViewController *)slideOutNavigationViewController{
    
    SlideOutNavigationViewController *result = [[[SlideOutNavigationViewController alloc] initWithNibName:NIB_FILE_NAME
                                                                                     bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;

}
- (CALayer *)gradientBGLayerForBounds:(CGRect)bounds
{
    CAGradientLayer * gradientBG = [CAGradientLayer layer];
    gradientBG.frame = bounds;
    gradientBG.colors = @[ (id)[[UIColor BBVABlueSpectrumColorGradientDark] CGColor], (id)[[UIColor BBVABlueSpectrumColorGradientLight] CGColor] ];
    return gradientBG;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor BBVABlueSpectrumColorGradientDark]];
    
    CALayer * bgGradientLayer = [self gradientBGLayerForBounds:navigationBar_.bounds];
    UIGraphicsBeginImageContext(bgGradientLayer.bounds.size);
    [bgGradientLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * bgAsImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if (bgAsImage != nil)
    {
        [navigationBar_ setBackgroundImage:bgAsImage
                                           forBarMetrics:UIBarMetricsDefault];
    }
    
    [navigationBar_ setTintColor:[UIColor BBVAWhiteColor]];
    
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor BBVAWhiteColor],UITextAttributeTextColor, nil];
    
    [navigationBar_  setTitleTextAttributes:navbarTitleTextAttributes];
    
    currentTitle_ =  @"Ponle Play a Tu Música";
    navigationBar_.topItem.title =  currentTitle_;
    
    [rightButton_ setImage:[[ImagesCache getInstance] imageNamed:RADIO_RIGHT_BUTTON_ICON_IMAGE_FILE_NAME]];

    [self loadRadioViewController];
    
    
 
  
}

-(BOOL)canBecomeFirstResponder
{
    return NO;
}



-(void)loadRadioViewController{
    
    if(radioPlayerViewController_ == nil){
        radioPlayerViewController_ = [[RadioPlayerViewController radioPlayerViewController] retain];
    }
    
    CGRect mainFrame  = self.currentView.bounds;
    mainFrame.origin.y = 5;
    
    radioPlayerViewController_.view.frame = mainFrame;
    [self.currentView addSubview:radioPlayerViewController_.view];
    
    [self addChildViewController:radioPlayerViewController_];
    [radioPlayerViewController_ didMoveToParentViewController:self];
    
    
    
    currentViewController_ = radioPlayerViewController_;
    
    rightButton_.tag = 1;

    
    selectedOption_ = 0;


}
-(void)viewWillAppear:(BOOL)animated{
    
    [self clearMainViewController];
    [self loadRadioViewController];
    currentTitle_ =  @"Ponle Play a Tu Música";
    navigationBar_.topItem.title =  currentTitle_;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)getRightView {
    // init view if it doesn't already exist
    if (optionsMenuViewController_ == nil)
    {
        // this is where you define the view for the right panel
        optionsMenuViewController_ = [OptionsMenuViewController optionsMenuViewController];
        
        [self.currentView addSubview:optionsMenuViewController_.view];
        
        [self addChildViewController:optionsMenuViewController_];
        [optionsMenuViewController_ didMoveToParentViewController:self];
        optionsMenuViewController_.view.frame = CGRectMake(CGRectGetMaxX([self.currentView frame]), 0, self.view.frame.size.width, self.view.frame.size.height);
        
        optionsMenuViewController_.delegate = self;
        
        UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(movePanel:)];
        [panRecognizer setMinimumNumberOfTouches:1];
        [panRecognizer setMaximumNumberOfTouches:1];
        [panRecognizer setDelegate:self];
        
        [optionsMenuViewController_.view addGestureRecognizer:panRecognizer];

    }
    
    [optionsMenuViewController_ setSelectedIndex:selectedOption_];
    self.showingRightPanel = YES;
    
    // setup view shadows
    [self showCenterViewWithShadow:YES withOffset:2];
    
    UIView *view = optionsMenuViewController_.view;
    
    [optionsMenuViewController_ reloadSelection];
    return view;
}

-(void)showCenterViewWithShadow:(BOOL)value withOffset:(double)offset {
   /* if (value) {
        //[currentView_.layer setCornerRadius:CORNER_RADIUS];
        [radioPlayerViewController_.view.layer setShadowColor:[UIColor blackColor].CGColor];
        [radioPlayerViewController_.view.layer setShadowOpacity:0.5];
        [radioPlayerViewController_.view.layer setShadowOffset:CGSizeMake(offset, offset)];
        
    } else {
        //[currentView_.layer setCornerRadius:0.0f];
        [radioPlayerViewController_.view.layer setShadowOffset:CGSizeMake(offset, offset)];
    }*/
}

-(void)movePanel:(id)sender {
    [[[(UITapGestureRecognizer*)sender view] layer] removeAllAnimations];
    
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
    CGPoint velocity = [(UIPanGestureRecognizer*)sender velocityInView:[sender view]];
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        UIView *childView = nil;
        
        if(velocity.x <= 0) {
                childView = [self getRightView];
           
            
        }else{
            return;
        }
        // make sure the view we're working with is front and center
        [self.view sendSubviewToBack:childView];
        [[sender view] bringSubviewToFront:[(UIPanGestureRecognizer*)sender view]];
    }
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        
        if(velocity.x > 0) {
            // NSLog(@"gesture went right");
        } else {
            // NSLog(@"gesture went left");
        }
        
        if (!showPanel_) {
            [navigationBar_.topItem setTitle:currentTitle_];
            [self movePanelToOriginalPosition];
        } else {
            
           if (showingRightPanel_) {
                [self movePanelLeft];
            }
        }
    }
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged) {
        if(velocity.x > 0) {
            // NSLog(@"gesture went right");
        } else {
            // NSLog(@"gesture went left");
        }
        
      
        // are we more than halfway, if so, show the panel when done dragging by setting this value to YES (1)
        showPanel_ = fabs([sender view].center.x - currentView_.frame.size.width/2) > optionsMenuViewController_.view.frame.size.width/2;
          if(showPanel_){
        // allow dragging only in x coordinates by only updating the x coordinate with translation position
        [sender view].center = CGPointMake([sender view].center.x + translatedPoint.x, [sender view].center.y);
        [(UIPanGestureRecognizer*)sender setTranslation:CGPointMake(0,0) inView:self.view];
        
        // if you needed to check for a change in direction, you could use this code to do so
        if(velocity.x*preVelocity_.x + velocity.y*preVelocity_.y > 0) {
            // NSLog(@"same direction");
        } else {
            // NSLog(@"opposite direction");
        }
        
        preVelocity_ = velocity;
        }
    }
}

-(void)movePanelToOriginalPosition {
    
    CGRect mainFrame  = CGRectMake(CGRectGetMaxX(self.currentView.bounds), 0, self.optionsMenuViewController.view.frame.size.width, self.optionsMenuViewController.view.frame.size.height);
    

    
    [UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        optionsMenuViewController_.view.frame = mainFrame;
    }
                     completion:^(BOOL finished) {
                         if (finished) {
                              [self applySelection];
                             [self resetMainView];
                            
                         }
                     }];
}

-(void)resetMainView {
    // remove left and right views, and reset variables, if needed
   
    if (optionsMenuViewController_ != nil) {
        [optionsMenuViewController_.view removeFromSuperview];
        optionsMenuViewController_ = nil;
     
        self.showingRightPanel = NO;
        rightButton_.tag = 1;
    }
    // remove view shadows
    [self showCenterViewWithShadow:NO withOffset:0];
}


-(void)movePanelLeft {
    UIView *childView = [self getRightView];
    [childView sendSubviewToBack:self.currentView];
    
    [UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        optionsMenuViewController_.view.frame = CGRectMake(PANEL_WIDTH, 0, self.optionsMenuViewController.view.frame.size.width, optionsMenuViewController_.view.frame.size.height);
    }
                     completion:^(BOOL finished) {
                         if (finished) {
                             
                             rightButton_.tag = 0;
                             navigationBar_.topItem.title = @"Radio BBVA";
                         }
                     }];
}

-(IBAction)btnMovePanelLeft:(id)sender {
    UIButton *button = sender;
    switch (button.tag) {
        case 0: {
            [navigationBar_.topItem setTitle:currentTitle_];
            [self movePanelToOriginalPosition];
            break;
        }
            
        case 1: {
            [self movePanelLeft];
            break;
        }
            
        default:
            break;
    }
}
-(void)selectedOption:(NSInteger)indexRow withTitle:(NSString*)title{
    
    selectedOption_ = indexRow;
    currentTitle_ = title;
    navigationBar_.topItem.title = title;
    
    
    [self movePanelToOriginalPosition];

    
}

-(void)applySelection{
    if (selectedOption_ == [OptionsMenuViewController radioPlayerOptionIndex]) {
        [self clearMainViewController];
        
        [self loadRadioViewController];
        
    }else if(selectedOption_ == [OptionsMenuViewController bePartOptionIndex]){
        [self clearMainViewController];
        [self loadBePart];
        
    }else if(selectedOption_ ==  [OptionsMenuViewController termsOptionIndex]){
        [self clearMainViewController];
        
        [self loadTermsAndConditions];
    }else if(selectedOption_ ==  [OptionsMenuViewController backToMainOptionIndex]){
       /* if(![self.appDelegate.radioStreamManager isPlayingRadio]){
            [self.appDelegate.radioStreamManager stopFinalRadioStream];
        }*/
        [self.navigationController popViewControllerAnimated:YES];
    }

}


-(void)clearMainViewController{
    if(radioPlayerViewController_!=nil){
    [radioPlayerViewController_.view removeFromSuperview];
    [radioPlayerViewController_ removeFromParentViewController];
    }
    
    if(termsAndConditionsViewController_!=nil){
    [termsAndConditionsViewController_.view removeFromSuperview];
    [termsAndConditionsViewController_ removeFromParentViewController];
    }
    
    if(bePartViewController_ !=nil){
    
    [bePartViewController_.view removeFromSuperview];
    

    [bePartViewController_ removeFromParentViewController];
    }
    
}

-(void)loadTermsAndConditions{
   
    
    if(termsAndConditionsViewController_ == nil){
        termsAndConditionsViewController_ = [[TermsAndConditionsViewController termsAndConditionsViewController] retain];
    }
    
    
    CGRect mainFrame  = self.currentView.bounds;
    mainFrame.origin.y = 5;
    
    termsAndConditionsViewController_.view.frame = mainFrame;
    [self.currentView addSubview:termsAndConditionsViewController_.view];
    
    [self.currentView bringSubviewToFront:termsAndConditionsViewController_.view];
    
    [self addChildViewController:termsAndConditionsViewController_];
    [termsAndConditionsViewController_ didMoveToParentViewController:self];
    
    currentViewController_ = termsAndConditionsViewController_;
    
    rightButton_.tag = 1;
    
    selectedOption_ = 2;
}

-(void)loadBePart{
    
    
    if(bePartViewController_ == nil){
        bePartViewController_ = [[BePartViewController bePartViewController] retain];
    }
    
    
    CGRect mainFrame  = self.currentView.bounds;
    mainFrame.origin.y = 5;
    
    bePartViewController_.view.frame = mainFrame;
    [self.currentView addSubview:bePartViewController_.view];
    
    [self.currentView bringSubviewToFront:bePartViewController_.view];
    
    [self addChildViewController:bePartViewController_];
    [bePartViewController_ didMoveToParentViewController:self];
    
    currentViewController_ = bePartViewController_;
    
    rightButton_.tag = 1;
    
    selectedOption_ = 1;
}

-(void)goPlayerAction{
    [self clearMainViewController];
    [self loadRadioViewController];
}
-(void)playRadioAction{
    
    if(currentViewController_ == termsAndConditionsViewController_){
        [termsAndConditionsViewController_ playRadio];
    }else if(currentViewController_ == bePartViewController_){
        [bePartViewController_ playRadio];
    }
}
-(void)dealloc{
    [termsAndConditionsViewController_ removeFromParentViewController];
    [bePartViewController_ removeFromParentViewController];
    [radioPlayerViewController_ removeFromParentViewController];
    [currentViewController_ removeFromParentViewController];
    
    [termsAndConditionsViewController_ release];
    termsAndConditionsViewController_ = nil;
    [bePartViewController_  release];
    bePartViewController_ = nil;
    [radioPlayerViewController_ release];
    radioPlayerViewController_ = nil;
    [currentViewController_ release];
    currentViewController_ = nil;
    [super dealloc];
}
@end
