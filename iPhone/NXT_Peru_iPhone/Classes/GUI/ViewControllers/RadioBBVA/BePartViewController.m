//
//  BePartViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/31/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "BePartViewController.h"
#import "ImagesFileNames.h"
#import "ImagesCache.h"
#import "RadioPlayerFooterView.h"
#import "NXT_Peru_iPhone_AppDelegate.h"

#define NIB_FILE_NAME @"BePartViewController"

@interface BePartViewController ()

@end

@implementation BePartViewController

@synthesize scrollView = scrollView_;
@synthesize optionComunicationLabel1 = optionComunicationLabel1_;
@synthesize optionComunicationLabel2 = optionComunicationLabel2_;
@synthesize optionComunicationLabel3 = optionComunicationLabel3_;
@synthesize optionComunicationLabel4 = optionComunicationLabel4_;
@synthesize optionComunicationLabel5 = optionComunicationLabel5_;
@synthesize optionComunicationLabel6 = optionComunicationLabel6_;
@synthesize optionComunicationLabel7 = optionComunicationLabel7_;
@synthesize lastInfoLabel = lastInfoLabel_;
@synthesize textView = textView_;

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [super setBannerMessageText:@"¡Tú también puedes sonar en nuestra radio!"];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    if(iOSDeviceScreenSize.height == 568 && [[UIScreen mainScreen] scale] == 2.0f){
        
        backgroundImageView_.image = [imagesCache imageNamed:IMG_SPLASH_RADIO_LARGE_WINDOW_BACKGROUND];
        
    }
    else{
        
        backgroundImageView_.image = [imagesCache imageNamed:IMG_SPLASH_RADIO_WINDOW_BACKGROUND];
    }
    
    if(radioPlayerFooterView_ ==nil ){
        radioPlayerFooterView_ = [RadioPlayerFooterView radioPlayerFooterView];
        [self.view addSubview:radioPlayerFooterView_];
    }
    
    [textView_ setBackgroundColor:[UIColor clearColor]];
    textView_.opaque = NO;
    [textView_.scrollView setBackgroundColor:[UIColor clearColor]];

    [textView_ loadHTMLString:@"<html><head><meta chartset=“utf-8”></head><body style='background-color: transparent;color:white;font-family: Helvetica Neue;font-size: 14px;'><p>Envíanos un correo a <b>Radiobbva@noize.pe</b> con el link de todas tus canciones alojadas en cualquiera de estos servidores y Pelo Madueño ó Pedro Suárez Vértiz te responderán:</p></body></html>" baseURL:nil];
    
    
    [optionComunicationLabel1_ setText:@"\u2022 Página web"];
    [optionComunicationLabel2_ setText:@"\u2022 SoundCloud"];
    [optionComunicationLabel3_ setText:@"\u2022 YouTube"];
    [optionComunicationLabel4_ setText:@"\u2022 BandCamp"];
    [optionComunicationLabel5_ setText:@"\u2022 Reverbnation"];
    [optionComunicationLabel6_ setText:@"\u2022 Mix Cloud"];
    [optionComunicationLabel7_ setText:@"\u2022 Last.FM"];
    
    [optionComunicationLabel1_ removeFromSuperview];
    [optionComunicationLabel2_ removeFromSuperview];
    [optionComunicationLabel3_ removeFromSuperview];
    [optionComunicationLabel4_ removeFromSuperview];
    [optionComunicationLabel5_ removeFromSuperview];
    [optionComunicationLabel6_ removeFromSuperview];
    [optionComunicationLabel7_ removeFromSuperview];
    [lastInfoLabel_ removeFromSuperview];
    
    [lastInfoLabel_ removeFromSuperview];
    
    CGRect frame = [optionComunicationLabel1_ frame];
    frame.origin.y = CGRectGetMaxY([textView_ frame]);
    [optionComunicationLabel1_ setFrame:frame];
    [scrollView_ addSubview:optionComunicationLabel1_];
    
    frame = [optionComunicationLabel2_ frame];
    frame.origin.y = CGRectGetMaxY([optionComunicationLabel1_ frame])+10;
    [optionComunicationLabel2_ setFrame:frame];
    [scrollView_ addSubview:optionComunicationLabel2_];
    
    frame = [optionComunicationLabel3_ frame];
    frame.origin.y = CGRectGetMaxY([optionComunicationLabel2_ frame])+10;
    [optionComunicationLabel3_ setFrame:frame];
    [scrollView_ addSubview:optionComunicationLabel3_];
    
    frame = [optionComunicationLabel4_ frame];
    frame.origin.y = CGRectGetMaxY([optionComunicationLabel3_ frame])+10;
    [optionComunicationLabel4_ setFrame:frame];
    [scrollView_ addSubview:optionComunicationLabel4_];
    
    frame = [optionComunicationLabel5_ frame];
    frame.origin.y = CGRectGetMaxY([optionComunicationLabel4_ frame])+10;
    [optionComunicationLabel5_ setFrame:frame];
    [scrollView_ addSubview:optionComunicationLabel5_];
    
    frame = [optionComunicationLabel6_ frame];
    frame.origin.y = CGRectGetMaxY([optionComunicationLabel5_ frame]) +10;
    [optionComunicationLabel6_ setFrame:frame];
    [scrollView_ addSubview:optionComunicationLabel6_];
    
    frame = [optionComunicationLabel7_ frame];
    frame.origin.y = CGRectGetMaxY([optionComunicationLabel6_ frame])+10;
    [optionComunicationLabel7_ setFrame:frame];
    [scrollView_ addSubview:optionComunicationLabel7_];
    
    
    frame = [lastInfoLabel_ frame];
    frame.origin.y = CGRectGetMaxY([optionComunicationLabel7_ frame])+20;
    [lastInfoLabel_ setFrame:frame];
    [scrollView_ addSubview:lastInfoLabel_];
    
    
    
    CGSize sizeScroll = CGSizeMake(CGRectGetWidth([scrollView_ frame]), CGRectGetMaxY([lastInfoLabel_ frame])+5);
    
    [scrollView_ setContentSize:sizeScroll];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(radioPlayerFooterView_ !=nil ){
        CGRect frame = [radioPlayerFooterView_ frame];
        frame.origin.y = CGRectGetHeight([self.view bounds]) -[RadioPlayerFooterView footerHeight]- CGRectGetHeight([brandImageView_ frame]);
        [radioPlayerFooterView_ setFrame:frame];
    }
    
    [self.appDelegate.radioStreamManager setRadioStreamingDelegate:self];
    if([self.appDelegate.radioStreamManager isPlayingRadio]){
        
        if([self.appDelegate.radioStreamManager lastTrackName].length>0){
            [radioPlayerFooterView_ setTitleTrackInfo:[self.appDelegate.radioStreamManager lastTrackName]];
        }
        
        [radioPlayerFooterView_ activeRadioPlaying];
        
    }else{
        
        [radioPlayerFooterView_ deactiveRadioPlaying];
    }

    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    
    
    [self.appDelegate.radioStreamManager setRadioStreamingDelegate:nil];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSLog(@"padre bepart didapear:%@",self.parentViewController );
    
    [radioPlayerFooterView_ setDelegate:self.parentViewController];
    
    if(radioPlayerFooterView_ !=nil ){
        CGRect frame = [radioPlayerFooterView_ frame];
        frame.origin.y = CGRectGetHeight([self.view bounds]) -[RadioPlayerFooterView footerHeight]- CGRectGetHeight([brandImageView_ frame]);
        [radioPlayerFooterView_ setFrame:frame];
    }
}

+(BePartViewController *) bePartViewController{
    
    BePartViewController *result = [[[BePartViewController alloc] initWithNibName:NIB_FILE_NAME
                                                                                                   bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;

}


-(void)updateTrackName:(NSString*)trackName{
    [radioPlayerFooterView_ setTitleTrackInfo:trackName];
    
    
}

-(void)radioStreamingStatus:(RadioPlayerStatus)status{
    if(status ==  npia_Stop ){
        //[self setUpStopState];
        [radioPlayerFooterView_ deactiveRadioPlaying];
        
    }else if(status  == npia_Play){
        
        [radioPlayerFooterView_ activeRadioPlaying];
        
    }else if(status == npia_Connecting){
        
        [radioPlayerFooterView_ activeRadioPlaying];
    }else if(status == npia_Error || status == npia_ErrorNetwork){
        [radioPlayerFooterView_ deactiveRadioPlaying];
        
    }
}
-(void)streamingValueAudioLeft:(NSArray*)valueLeft valueAudioRight:(NSArray*)valueRight{
    
    
    NSMutableArray *fullArray = [NSMutableArray arrayWithArray:valueLeft];
    [fullArray addObjectsFromArray:valueRight];
    
    [radioPlayerFooterView_ setValuesForEqualizer:fullArray];
    
}
-(void)playRadio
{
    if([self.appDelegate.radioStreamManager isPlayingRadio]){
        
        [self.appDelegate.radioStreamManager stopRadioStream];
        [radioPlayerFooterView_ deactiveRadioPlaying];
        [radioPlayerFooterView_ setTitleTrackInfo:@""];
        
    }else{
        
        [self.appDelegate.radioStreamManager playRadioStream];
        [radioPlayerFooterView_ activeRadioPlaying];
        
        
    }
    
}

-(void)dealloc{
    
    [radioPlayerFooterView_ release];
    radioPlayerFooterView_ = nil;
    
    [super dealloc];
}

@end
