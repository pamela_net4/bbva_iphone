//
//  BaseRadioViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/29/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "BaseRadioViewController.h"
#import "UIColor+BBVA_Colors.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"

@interface BaseRadioViewController ()

@end

@implementation BaseRadioViewController

@synthesize backgroundImageView = backgroundImageView_;
@synthesize bannerMessageLabel = bannerMessageLabel_;
@synthesize bannerMessageView = bannerMessageView_;
@synthesize brandImageView = brandImageView_;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    [brandImageView_ setImage:[imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];

    
    [bannerMessageLabel_ setTextColor:[UIColor BBVAWhiteColor]];
    [bannerMessageLabel_ setTextAlignment:UITextAlignmentCenter];
    [bannerMessageLabel_ setNumberOfLines:2];
    [bannerMessageLabel_ setLineBreakMode:UILineBreakModeWordWrap];
    [bannerMessageView_ setBackgroundColor:[UIColor BBVABlackTransparentColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setBannerMessageText:(NSString*)messageText{
    [bannerMessageLabel_ setText:messageText];
    
}



@end
