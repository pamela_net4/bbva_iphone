//
//  TermsAndConditionsViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/28/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "TermsAndConditionsViewController.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "RadioPlayerFooterView.h"
#import "NXT_Peru_iPhone_AppDelegate.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"TermsAndConditionsViewController"

@interface TermsAndConditionsViewController ()

@end

@implementation TermsAndConditionsViewController
@synthesize webView = webView_;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [super setBannerMessageText:@"Sigue estas recomendaciones y escucha Radio BBVA sin problemas"];

    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    if(iOSDeviceScreenSize.height == 568 && [[UIScreen mainScreen] scale] == 2.0f){
        
        backgroundImageView_.image = [imagesCache imageNamed:IMG_SPLASH_RADIO_LARGE_WINDOW_BACKGROUND];
        
    }
    else{
        
        backgroundImageView_.image = [imagesCache imageNamed:IMG_SPLASH_RADIO_WINDOW_BACKGROUND];
    }
    
    if(radioPlayerFooterView_ ==nil ){
        radioPlayerFooterView_ = [RadioPlayerFooterView radioPlayerFooterView];
        [self.view addSubview:radioPlayerFooterView_];
    }
    
    [webView_ setBackgroundColor:[UIColor clearColor]];
    webView_.opaque = NO;
    [webView_.scrollView setBackgroundColor:[UIColor clearColor]];
    [webView_ loadHTMLString:@"<html><head><meta chartset=“utf-8”></head><body style='background-color: transparent;color:white;font-family: Helvetica Neue;font-size: 14px;'><p>Accede a Radio BBVA en cualquier momento y lugar desde tu smartphone a través de la <b>aplicación del BBVA Continental</b>, ya sea por conexión a una red Wi-Fi o mediante consumo de tu plan de datos.</p>Escucha Radio BBVA cuando navegues por cualquier aplicación de tu teléfono móvil. Puedes <b>pausarla y volver a activarla cuando quieras.</b> Ten en cuenta que mientras más veloz sea tu conexión WiFi o tu plan de datos, mayor será la calidad de reproducción.<p>Ten en cuenta que por respeto a los <b>derecho de autores, intérpretes, ejecutantes y demás participantes</b> no podrás grabar las obras que se transmiten por Radio BBVA, pues, de lo contrario podrías estar cometiendo un delito.</p></body></html>" baseURL:nil];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(radioPlayerFooterView_ !=nil ){
        CGRect frame = [radioPlayerFooterView_ frame];
        frame.origin.y = CGRectGetHeight([self.view bounds]) -[RadioPlayerFooterView footerHeight]- CGRectGetHeight([brandImageView_ frame]);
        [radioPlayerFooterView_ setFrame:frame];
    }
    
    
    [self.appDelegate.radioStreamManager setRadioStreamingDelegate:self];
    
        
   if([self.appDelegate.radioStreamManager isPlayingRadio]){
            
      if([self.appDelegate.radioStreamManager lastTrackName].length>0){
                [radioPlayerFooterView_ setTitleTrackInfo:[self.appDelegate.radioStreamManager lastTrackName]];
      }
          [radioPlayerFooterView_ activeRadioPlaying];
          
      }else{
          
          [radioPlayerFooterView_ deactiveRadioPlaying];
      }
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [self.appDelegate.radioStreamManager setRadioStreamingDelegate:nil];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(radioPlayerFooterView_ !=nil ){
        CGRect frame = [radioPlayerFooterView_ frame];
        frame.origin.y = CGRectGetHeight([self.view bounds]) -[RadioPlayerFooterView footerHeight]- CGRectGetHeight([brandImageView_ frame]);
        [radioPlayerFooterView_ setFrame:frame];
    }
    
    
    [radioPlayerFooterView_ setDelegate:self.parentViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


+(TermsAndConditionsViewController *) termsAndConditionsViewController{
    TermsAndConditionsViewController *result = [[[TermsAndConditionsViewController alloc] initWithNibName:NIB_FILE_NAME
                                                                                     bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
}

-(void)updateTrackName:(NSString*)trackName{
    [radioPlayerFooterView_ setTitleTrackInfo:trackName];
    
    
}

-(void)radioStreamingStatus:(RadioPlayerStatus)status{
    if(status ==  npia_Stop ){
        //[self setUpStopState];
        [radioPlayerFooterView_ deactiveRadioPlaying];
        
    }else if(status  == npia_Play){
        
        [radioPlayerFooterView_ activeRadioPlaying];
        
    }else if(status == npia_Connecting){
        
        [radioPlayerFooterView_ activeRadioPlaying];
    }else if(status == npia_Error || status == npia_ErrorNetwork){
        [radioPlayerFooterView_ deactiveRadioPlaying];

    }
}
-(void)streamingValueAudioLeft:(NSArray*)valueLeft valueAudioRight:(NSArray*)valueRight{
    
    
    NSMutableArray *fullArray = [NSMutableArray arrayWithArray:valueLeft];
    [fullArray addObjectsFromArray:valueRight];
    
    [radioPlayerFooterView_ setValuesForEqualizer:fullArray];
    
}
-(void)playRadio{
    if([self.appDelegate.radioStreamManager isPlayingRadio]){
        
        [self.appDelegate.radioStreamManager stopRadioStream];
        [radioPlayerFooterView_ deactiveRadioPlaying];
        
        [radioPlayerFooterView_ setTitleTrackInfo:@""];
        
        
    }else{
        
        [self.appDelegate.radioStreamManager playRadioStream];
        [radioPlayerFooterView_ activeRadioPlaying];

        
    }

}

-(void)dealloc{
    
    [radioPlayerFooterView_ release];
    radioPlayerFooterView_ = nil;
    
    [super dealloc];
}
@end
