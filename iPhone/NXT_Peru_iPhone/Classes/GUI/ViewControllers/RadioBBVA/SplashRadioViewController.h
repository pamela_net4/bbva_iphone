//
//  SplashRadioViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/24/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "NXTViewController.h"

@interface SplashRadioViewController : NXTViewController
{
    /**
     * Background image view
     */
    UIImageView *splashImageView_;
    
    /**
     * BBVA logo image view
     */
    UIImageView *bbvaLogoImageView_;
    
    /**
     * Stores the time the view controller appeared
     */
    NSTimeInterval viewControllerAppearTime_;
    
    /**
     * Branding image view.
     */
    UIImageView *brandImageView_;

}

/**
 * Provides read-write access to the background image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *splashImageView;

/**
 * Provides read-write access to the BBVA logo image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *bbvaLogoImageView;

@property(nonatomic,readwrite,retain) IBOutlet  UIImageView *brandImageView;



/**
 * Creates and returns an autoreleased SplashViewController constructed from a NIB file
 *
 * @return The autoreleased SplashViewController constructed from a NIB file
 */
+ (SplashRadioViewController *)splashRadioViewController;

@end
