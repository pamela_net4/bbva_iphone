//
//  SplashRadioViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/24/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "SplashRadioViewController.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIView+Frame.h"

#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Constants.h"


/**
 * Define the SplashViewController NIB file name
 */
NSString * const kSplashRadioViewControllerNibFileName = @"SplashRadioViewController";

@interface SplashRadioViewController ()

@end

@implementation SplashRadioViewController

#pragma mark -
#pragma mark Properties

@synthesize splashImageView = splashImageView_;
@synthesize bbvaLogoImageView = bbvaLogoImageView_;
@synthesize brandImageView = brandImageView_;

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased SplashViewController constructed from a NIB file
 */
+ (SplashRadioViewController *)splashRadioViewController {
    
    SplashRadioViewController *result = [[[SplashRadioViewController alloc] initWithNibName:kSplashRadioViewControllerNibFileName
                                                                           bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}

/**
 * Called after the controller’s view is loaded into memory. Style is applied to graphic elements
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    if(iOSDeviceScreenSize.height == 568 && [[UIScreen mainScreen] scale] == 2.0f){
        
        splashImageView_.image = [imagesCache imageNamed:IMG_SPLASH_RADIO_LARGE_WINDOW_BACKGROUND];

    }
    else{
            
        splashImageView_.image = [imagesCache imageNamed:IMG_SPLASH_RADIO_WINDOW_BACKGROUND];
    }
    
    
    [brandImageView_ setImage:[imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];

    
    bbvaLogoImageView_.image = [imagesCache imageNamed:IMG_BBVA_RADIO_WHITE_LOGO];
    bbvaLogoImageView_.width = bbvaLogoImageView_.image.size.width;
    
    
    
    
}
/**
 * Notifies the view controller that its view was added to a window. The current time is stored
 *
 * @param animated If YES, the view was added to the window using an animation
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    viewControllerAppearTime_ = [[NSDate date] timeIntervalSinceReferenceDate];
    
}

/**
 * Notifies the view controller that its view is about to be become visible. Starts the information download
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Delay of splash screen
    NSTimeInterval delay = self.appDelegate.firstRun ? FIRST_SPLASH_DELAY : SPLASH_DELAY;
    [self performSelector:@selector(checkViewControllerCanDisappearNow) withObject:nil afterDelay:delay];
    
}

/*
 * Checks whether the view controller can disappear or not. If it is too early, a delay to notify the delegate is started
 */
- (void)checkViewControllerCanDisappearNow {
    
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSinceReferenceDate];
    NSTimeInterval lastTimeToDisplayViewController = viewControllerAppearTime_;
    if (currentTime >= lastTimeToDisplayViewController) {
        
        
    }
    
}


@end
