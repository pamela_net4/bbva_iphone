//
//  OptionsMenuViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/28/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "NXTViewController.h"

@class RadioBBVAMenuCell;

@protocol OptionMenuViewControllerDelegate <NSObject>

-(void)selectedOption:(NSInteger)indexRow withTitle:(NSString*)title;

@end

@interface OptionsMenuViewController : NXTViewController<UITableViewDataSource,UITableViewDelegate>{
    
    UITableView *optionsTableView_;
    
    RadioBBVAMenuCell *optionRadioPlayerCell_;
    
    RadioBBVAMenuCell *optionBePartCell_;
    
    RadioBBVAMenuCell *optionTermsCell_;
    
    RadioBBVAMenuCell *optionBackMainCell_;
    
    id<OptionMenuViewControllerDelegate> delegate_;
    
    NSInteger selectedIndex_;
    
}

@property (nonatomic, readwrite, retain) IBOutlet  UITableView *optionsTableView;

@property (nonatomic, readwrite) NSInteger selectedIndex;
@property(nonatomic,assign) id<OptionMenuViewControllerDelegate> delegate;

+(OptionsMenuViewController *) optionsMenuViewController;

+(NSInteger) radioPlayerOptionIndex;
+(NSInteger) bePartOptionIndex;
+(NSInteger) termsOptionIndex;
+(NSInteger) backToMainOptionIndex;

-(void)reloadSelection;

@end
