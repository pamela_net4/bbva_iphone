//
//  RadioPlayerViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/28/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "BaseRadioViewController.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
@class VisualEqualizerView;

@interface RadioPlayerViewController : BaseRadioViewController<RadioStreamingDelegate>{
    UILabel *activeStatusLabel_;
    UILabel *informationMessageLabel_;
    UILabel *animateMarqueeLabelSub1_;
    UILabel *animateMarqueeLabelSub2_;
    UIView *marqueeView_;
    UIButton *playRadioButton_;
    VisualEqualizerView *equalizerView_;

}
@property(nonatomic,readwrite,retain) IBOutlet  UILabel *activeStatusLabel;
@property(nonatomic,readwrite,retain) IBOutlet  UILabel *informationMessageLabel;
@property(nonatomic,readwrite,retain) IBOutlet  UILabel *animateMarqueeLabelSub1;
@property(nonatomic,readwrite,retain) IBOutlet  UILabel *animateMarqueeLabelSub2;
@property(nonatomic,readwrite,retain) IBOutlet  UIView *marqueeView;
@property(nonatomic,readwrite,retain) IBOutlet  UIButton *playRadioButton;

+ (RadioPlayerViewController *)radioPlayerViewController;


-(IBAction)playRadio:(id)sender;

@end
