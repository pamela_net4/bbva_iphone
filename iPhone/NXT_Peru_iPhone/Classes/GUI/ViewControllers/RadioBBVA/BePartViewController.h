//
//  BePartViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/31/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "BaseRadioViewController.h"
#import "RadioStreamController.h"

@class RadioPlayerFooterView;

@interface BePartViewController : BaseRadioViewController<RadioStreamingDelegate>{
    RadioPlayerFooterView *radioPlayerFooterView_;
    UILabel *optionComunicationLabel1_;
    UILabel *optionComunicationLabel2_;
    UILabel *optionComunicationLabel3_;
    UILabel *optionComunicationLabel4_;
    UILabel *optionComunicationLabel5_;
    UILabel *optionComunicationLabel6_;
    UILabel *optionComunicationLabel7_;
    UILabel *lastInfoLabel_;
    UIWebView *textView_;
    UIScrollView *scrollView_;
    

}
@property(nonatomic,readwrite,retain) IBOutlet UIWebView *textView;
@property(nonatomic,readwrite,retain) IBOutlet UIScrollView *scrollView;
@property(nonatomic,readwrite,retain) IBOutlet UILabel *optionComunicationLabel1;
@property(nonatomic,readwrite,retain) IBOutlet UILabel *optionComunicationLabel2;
@property(nonatomic,readwrite,retain) IBOutlet UILabel *optionComunicationLabel3;
@property(nonatomic,readwrite,retain) IBOutlet UILabel *optionComunicationLabel4;
@property(nonatomic,readwrite,retain) IBOutlet UILabel *optionComunicationLabel5;
@property(nonatomic,readwrite,retain) IBOutlet UILabel *optionComunicationLabel6;
@property(nonatomic,readwrite,retain) IBOutlet UILabel *optionComunicationLabel7;
@property(nonatomic,readwrite,retain) IBOutlet UILabel *lastInfoLabel;

+(BePartViewController *) bePartViewController;

-(void)playRadio;

@end
