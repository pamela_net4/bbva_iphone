//
//  RadioPlayerViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/28/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "RadioPlayerViewController.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "UIColor+BBVA_Colors.h"
#import "VisualEqualizerView.h"
/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"RadioPlayerViewController"

@interface RadioPlayerViewController ()
{
    NSTimer *marqueeTimer_;
}
@property (nonatomic,readwrite,retain) NSTimer *marqueeTimer;

@end

@implementation RadioPlayerViewController
@synthesize activeStatusLabel = activeStatusLabel_;
@synthesize informationMessageLabel = informationMessageLabel_;
@synthesize animateMarqueeLabelSub1 = animateMarqueeLabelSub1_;
@synthesize animateMarqueeLabelSub2 = animateMarqueeLabelSub2_;
@synthesize marqueeView = marqueeView_;
@synthesize playRadioButton = playRadioButton_;
@synthesize marqueeTimer = marqueeTimer_;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    if(iOSDeviceScreenSize.height == 568 && [[UIScreen mainScreen] scale] == 2.0f){
        
        backgroundImageView_.image = [imagesCache imageNamed:IMG_SPLASH_RADIO_LARGE_WINDOW_BACKGROUND];
        
    }
    else{
        
        backgroundImageView_.image = [imagesCache imageNamed:IMG_SPLASH_RADIO_WINDOW_BACKGROUND];
    }
    
    [activeStatusLabel_ setTextColor:[UIColor BBVAWhiteColor]];
    [activeStatusLabel_ setTextAlignment:UITextAlignmentCenter];
    
    [informationMessageLabel_ setTextColor:[UIColor BBVAWhiteColor]];
    [informationMessageLabel_ setTextAlignment:UITextAlignmentCenter];
    
    [animateMarqueeLabelSub1_ setTextColor:[UIColor BBVAWhiteColor]];
    [animateMarqueeLabelSub1_ setTextAlignment:UITextAlignmentCenter];
    
    [animateMarqueeLabelSub2_ setTextColor:[UIColor BBVAWhiteColor]];
    [animateMarqueeLabelSub2_ setTextAlignment:UITextAlignmentCenter];
    
    //RADIO_BIG_PLAY_BUTTON_IMAGE_FILE_NAME
    [playRadioButton_ setImage:[imagesCache imageNamed:RADIO_BIG_PLAY_BUTTON_IMAGE_FILE_NAME] forState:UIControlStateNormal];
    
    
    if(equalizerView_==nil){
        equalizerView_ = [[VisualEqualizerView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width - 130)/2, CGRectGetMinY([activeStatusLabel_ frame])-65, 130, 60) withSeparator:SEPARATOR_NORMAL];
        equalizerView_.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleRightMargin |
        UIViewAutoresizingFlexibleLeftMargin |
        UIViewAutoresizingFlexibleBottomMargin;
        [self.view addSubview:equalizerView_];
        [backgroundImageView_ sendSubviewToBack:equalizerView_];
        
    }

    [self setBannerMessageText:@"Lo mejor y más reciente de la movida musical peruana está aquí"];
    [self setMarqueeText:@"Estas Escuchando Radio BBVA"];
    
    [self showMarquee:NO];
 
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    [self.appDelegate.radioStreamManager setRadioStreamingDelegate:self];
    
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

    
    [self.appDelegate.radioStreamManager setRadioStreamingDelegate:nil];

}



-(void)showMarquee:(BOOL)show{
    [animateMarqueeLabelSub1_ setHidden:!show];
    [animateMarqueeLabelSub2_ setHidden:!show];
    if(show){
    
        if(marqueeTimer_!=nil){
            [marqueeTimer_ invalidate];
          
        }
        marqueeTimer_ = [NSTimer scheduledTimerWithTimeInterval:0.06f
                                     target: self
                                   selector:@selector(scrollMarquee:)
                                   userInfo: nil
                                    repeats:YES];
    
    }else{
        
        if(marqueeTimer_!=nil){
            [marqueeTimer_ invalidate];
            marqueeTimer_ = nil;
        }
    }

}



-(void)scrollMarquee:(id)parameter{
    
    
        CGRect animatedFrame1 = animateMarqueeLabelSub1_.frame;
        animatedFrame1.origin.x-=1;
        [animateMarqueeLabelSub1_ setFrame:animatedFrame1];
        
        
        CGRect animatedFrame2 = animateMarqueeLabelSub2_.frame;
        animatedFrame2.origin.x-=1;
        [animateMarqueeLabelSub2_ setFrame:animatedFrame2];

        animatedFrame1 = animateMarqueeLabelSub1_.frame;
        if(animatedFrame1.origin.x + animatedFrame1.size.width < 0){
            [animateMarqueeLabelSub1_ setHidden:YES];
            animatedFrame1.origin.x = (CGRectGetMaxX(animatedFrame2)+20> [marqueeView_ frame].size.width)?CGRectGetMaxX(animatedFrame2)+10:[marqueeView_ frame].size.width;
            [animateMarqueeLabelSub1_ setFrame:animatedFrame1];
            [animateMarqueeLabelSub1_ setHidden:NO];
        }
        animatedFrame2 = animateMarqueeLabelSub2_.frame;
        if(animatedFrame2.origin.x + animatedFrame2.size.width < 0){
            [animateMarqueeLabelSub2_ setHidden:YES];
            animatedFrame2.origin.x = CGRectGetMaxX(animatedFrame1)+20;
            [animateMarqueeLabelSub2_ setFrame:animatedFrame2];
            [animateMarqueeLabelSub2_ setHidden:NO];
        }
}


-(void)setMarqueeText:(NSString*)marqueeText{
 
    CGRect animatedFrame1 = animateMarqueeLabelSub1_.frame;
    CGSize sizeFrame1 = [marqueeText sizeWithFont:[UIFont systemFontOfSize:14.0f] constrainedToSize:CGSizeMake(999, 21)];
   // animatedFrame1.origin.x = marqueeView_.frame.size.width;
    animatedFrame1.size.width = sizeFrame1.width;
    [animateMarqueeLabelSub1_ setFrame:animatedFrame1];
    
    
    CGRect animatedFrame2 = animateMarqueeLabelSub2_.frame;
    CGSize sizeFrame2 = [marqueeText sizeWithFont:[UIFont systemFontOfSize:14.0f] constrainedToSize:CGSizeMake(999, 21)];
    animatedFrame2.origin.x = CGRectGetMaxX(animatedFrame1)+20;
    animatedFrame2.size.width = sizeFrame2.width;
    [animateMarqueeLabelSub2_ setFrame:animatedFrame2];
    
    [animateMarqueeLabelSub1_ setText:marqueeText];
    [animateMarqueeLabelSub2_ setText:marqueeText];
    

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if([self.appDelegate.radioStreamManager isPlayingRadio]){
    
        if([self.appDelegate.radioStreamManager lastTrackName].length>0){
            [self setMarqueeText:[self.appDelegate.radioStreamManager lastTrackName]];
        }else{
            [self setMarqueeText:@"Estas Escuchando Radio BBVA"];
        }

        [self showMarquee:YES];
        
    }else{
        
        [self showMarquee:NO];
        [self.appDelegate.radioStreamManager playRadioStream];
        [equalizerView_ setEnable:NO];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 * Creates and returns an autoreleased SplashViewController constructed from a NIB file
 */
+ (RadioPlayerViewController *)radioPlayerViewController {
    
    RadioPlayerViewController *result = [[[RadioPlayerViewController alloc] initWithNibName:NIB_FILE_NAME
                                                                                     bundle:nil] autorelease];
    [result awakeFromNib];
    
    return result;
    
}

-(IBAction)playRadio:(id)sender{
    if([self.appDelegate.radioStreamManager isPlayingRadio]){
        
        [self.appDelegate.radioStreamManager stopRadioStream];

    }else{
        
        [self.appDelegate.radioStreamManager playRadioStream];
        

    }
}


-(void)updateTrackName:(NSString*)trackName{
    [self setMarqueeText:trackName];

    
}

-(void)radioStreamingStatus:(RadioPlayerStatus)status{
    if(status ==  npia_Stop ){
        [self setUpStopState];
        
    }else if(status  == npia_Play){
        
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        [self becomeFirstResponder];

         [self setUpPlayState];
        
    }else if(status == npia_Connecting){
        
        [self setUpConnectingState];
    }else if(status == npia_Error){
        
        [self setUpErrorStateWithMessage:@"Se ha producido un error... Por favor espere unos momentos para volver a escuchar tu música"];
        
    }else if(status == npia_ErrorNetwork){
        
        [self setUpErrorStateWithMessage:@"Por favor revisa que tu conexión no tenga problemas para que vuelvas a escuchar tu música"];
    }
}
-(void)setUpErrorStateWithMessage:(NSString*)message{
     [self.view.layer removeAllAnimations];
    [playRadioButton_ setImage:[[ImagesCache getInstance] imageNamed:RADIO_BIG_PLAY_BUTTON_IMAGE_FILE_NAME] forState:UIControlStateNormal];
    [playRadioButton_ setEnabled:YES];
    [activeStatusLabel_ setText:@"OFF"];
    [activeStatusLabel_ setHidden:NO];
    [self showMarquee:NO];
   
    [UIView animateWithDuration:0.5 animations:^{
        CGRect labelFrame = [informationMessageLabel_ frame];
        labelFrame.origin.y = CGRectGetMaxY([activeStatusLabel_ frame])+5;

        CGSize sizeMessage  = [message sizeWithFont:[informationMessageLabel_ font] constrainedToSize:CGSizeMake( CGRectGetWidth(labelFrame), 999) lineBreakMode:UILineBreakModeWordWrap];
        labelFrame.size.height = sizeMessage.height;
        
        [informationMessageLabel_ setFrame:labelFrame];
        [informationMessageLabel_ setHidden:NO];
        [informationMessageLabel_ setText:message];

        CGRect buttonFrame = [playRadioButton_ frame];
        buttonFrame.origin.y = CGRectGetMaxY(informationMessageLabel_.frame) +10;
        [playRadioButton_ setFrame:buttonFrame];
        [equalizerView_ loadDefaultLevels];
        [equalizerView_ setEnable:YES];
        [self showMarquee:NO];

    }];
    

}
-(void)setUpStopState{
    
   [self.view.layer removeAllAnimations];

    [playRadioButton_ setImage:[[ImagesCache getInstance] imageNamed:RADIO_BIG_PLAY_BUTTON_IMAGE_FILE_NAME] forState:UIControlStateNormal];
    [playRadioButton_ setEnabled:YES];
    [activeStatusLabel_ setText:@"OFF"];
    [activeStatusLabel_ setHidden:NO];
    [informationMessageLabel_ setHidden:YES];
    [self setMarqueeText:@"No es más que un hasta luego... vuelve a escucharnos cuando quieras"];
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        CGRect buttonFrame = [playRadioButton_ frame];
        buttonFrame.origin.y = CGRectGetMaxY(marqueeView_.frame) +10;
        [playRadioButton_ setFrame:buttonFrame];
        [self showMarquee:YES];
        
        [equalizerView_ loadDefaultLevels];
        [equalizerView_ setEnable:YES];
       
    }];
    
}
-(void)setUpPlayState{
    
    
    [self.view.layer removeAllAnimations];
    [equalizerView_ setEnable:YES];
    [playRadioButton_ setImage:[[ImagesCache getInstance] imageNamed:RADIO_BIG_STOP_BUTTON_IMAGE_FILE_NAME] forState:UIControlStateNormal];
    [playRadioButton_ setEnabled:YES];
    [UIView animateWithDuration:0.5  animations:^{

       
        CGRect frame = [informationMessageLabel_ frame];
        frame.origin.y = CGRectGetMinY([activeStatusLabel_ frame]);
        [informationMessageLabel_ setFrame:frame];
        [informationMessageLabel_ setHidden:YES];
        [activeStatusLabel_ setText:@"ON"];
        [activeStatusLabel_ setHidden:NO];
        
    } ];
    
    CGRect marqueeFrame = [marqueeView_ frame];
    marqueeFrame.origin.y = CGRectGetMaxY([activeStatusLabel_ frame])+10;
    [marqueeView_ setFrame:marqueeFrame];
    
    if([self.appDelegate.radioStreamManager lastTrackName].length>0){
        [self setMarqueeText:[self.appDelegate.radioStreamManager lastTrackName]];
    }else{
        [self setMarqueeText:@"Estas Escuchando Radio BBVA"];
    }
    [self showMarquee:YES];
    [UIView animateWithDuration:0.5 animations:^{
        
    CGRect buttonFrame = [playRadioButton_ frame];
    buttonFrame.origin.y = CGRectGetMaxY(marqueeView_.frame) +10;
    [playRadioButton_ setFrame:buttonFrame];
        
       
    } completion:^(BOOL finished) {
       
    }];
    
}
-(void)setUpConnectingState{
    
    [self.view.layer removeAllAnimations];
    
    [playRadioButton_ setImage:[[ImagesCache getInstance] imageNamed:RADIO_BIG_STOP_BUTTON_IMAGE_FILE_NAME] forState:UIControlStateNormal];
    [playRadioButton_ setEnabled:NO];
    [activeStatusLabel_ setText:@""];
    [activeStatusLabel_ setHidden:YES];
    NSString *message = @"Conectando...";
    [informationMessageLabel_ setText:message];
    CGRect labelFrame = [informationMessageLabel_ frame];
    CGSize sizeMessage  = [message sizeWithFont:[informationMessageLabel_ font] constrainedToSize:CGSizeMake( CGRectGetWidth(labelFrame), 999) lineBreakMode:UILineBreakModeWordWrap];
    labelFrame.size.height = sizeMessage.height;
    [informationMessageLabel_ setFrame:labelFrame];
    
    [equalizerView_ setEnable:NO];
    [self showMarquee:NO];
    [informationMessageLabel_ setHidden:NO];
   
    [UIView animateWithDuration:0.5 animations:^{
     
    CGRect buttonFrame = [playRadioButton_ frame];
    buttonFrame.origin.y = CGRectGetMaxY(informationMessageLabel_.frame) +10;
    [playRadioButton_ setFrame:buttonFrame];
    }];
    
   
}

-(void)streamingValueAudioLeft:(NSArray*)valueLeft valueAudioRight:(NSArray*)valueRight{
    
    
    NSMutableArray *fullArray = [NSMutableArray arrayWithArray:valueLeft];
    [fullArray addObjectsFromArray:valueRight];
    
    [equalizerView_ setValuesForEqualizer:fullArray];

}

-(void)dealloc{
    
    if(marqueeTimer_!=nil){
        [marqueeTimer_ invalidate];
        [marqueeTimer_ release];
        marqueeTimer_ = nil;
    }
    [equalizerView_ release];
    equalizerView_ = nil;

    [super dealloc];
}
@end
