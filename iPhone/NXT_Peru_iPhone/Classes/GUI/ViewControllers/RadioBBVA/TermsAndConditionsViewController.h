//
//  TermsAndConditionsViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/28/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "BaseRadioViewController.h"
#import "RadioStreamController.h"
@class RadioPlayerFooterView;

@interface TermsAndConditionsViewController : BaseRadioViewController<RadioStreamingDelegate,RadioStreamingDelegate>{
    RadioPlayerFooterView *radioPlayerFooterView_;
    UIWebView *webView_;
}
@property(nonatomic,retain,readwrite) IBOutlet UIWebView *webView;

+(TermsAndConditionsViewController *) termsAndConditionsViewController;
-(void)playRadio;
@end
