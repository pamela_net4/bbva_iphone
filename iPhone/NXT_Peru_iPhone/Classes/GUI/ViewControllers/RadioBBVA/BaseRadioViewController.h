//
//  BaseRadioViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/29/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "NXTViewController.h"

@interface BaseRadioViewController : NXTViewController{
    UIImageView *backgroundImageView_;
    UIView *bannerMessageView_;
    UILabel *bannerMessageLabel_;
    /**
     * Branding image view.
     */
    UIImageView *brandImageView_;
    
}

@property(nonatomic,readwrite,retain) IBOutlet UIImageView *backgroundImageView;
@property(nonatomic,readwrite,retain) IBOutlet UIView *bannerMessageView;
@property(nonatomic,readwrite,retain) IBOutlet UILabel *bannerMessageLabel;
@property(nonatomic,readwrite,retain) IBOutlet  UIImageView *brandImageView;

-(void)setBannerMessageText:(NSString*)messageText;


@end
