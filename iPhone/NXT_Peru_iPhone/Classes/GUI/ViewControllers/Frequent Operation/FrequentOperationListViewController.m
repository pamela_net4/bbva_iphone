//
//  FrequentOperationListViewController.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/5/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationListViewController.h"

#import "CheckFOCell.h"
#import "Constants.h"
#import "FOPGeneralMenuStepOneResponse.h"
#import "FrequentOperationList.h"
#import "FrequentOperation.h"
#import "FrequentOperationStepOneViewController.h"
#import "FreqOpeViewControllerStepZeroViewController.h"
#import "FreqOpeAgreementsAndServicesStepOneViewController.h"
#import "FOEThirdCardPaymentStepOneResponse.h"
#import "FOECashMobileStepOneResponse.h"
#import "FOInstitutionsAndCompanies.h"
#import "FOEInstitutionPaymentStepOneResponse.h"
#import "FOTransferWithCashMobile.h"
#import "FOTransferToThirdAccount.h"
#import "FOTransferToOtherBankAccount.h"
#import "FOEServicePaymentStepOneResponse.h"
#import "FOPaymentCCThirdCard.h"
#import "FOPaymentCOtherBank.h"
#import "FORechargeCellPhone.h"
#import "FORechargeGiftCard.h"
#import "PaymentPSProcess.h"
#import "PaymentPSCellServProcess.h"
#import "PaymentPSLandlineServProcess.h"
#import "PaymentPSWaterServProcess.h"
#import "PublicServiceResponse.h"
#import "PublicServStepThreeViewController.h"
#import "PaymentInstitutionsAndCompaniesDetailResponse.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentDataResponse.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "Updater.h"

#pragma mark -
#pragma mark Static attributes

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"FrequentOperationListViewController"

/**
 * Defines statics values
 */
#define INITIAL_TABLE_HEIGHT                                        262
#define INITIAL_TABLE_HEIGHT_4Inch                                  352

@interface FrequentOperationListViewController ()

/**
 * Request the list of frequent operations
 */
- (void)listFrequentOperationRequest:(NSString *)action;

/**
 * Request the Third cardp pay
 */
- (void)thirdCardPaymentRequest:(FOEThirdCardPaymentStepOneResponse *)response;

/**
 * Request The initial data for Cash mobile
 */
- (void)cashMobileRequest:(FOECashMobileStepOneResponse *)response;

/**
 * Request The initial data for institutions and companies
 */
- (void)paymentInstAndCompSearchRequest:(FOEInstitutionPaymentStepOneResponse *)response;

/**
 * Request The initial data for institutions and companies
 */
- (void)paymentInstAndCompRequest:(PaymentInstitutionsAndCompaniesDetailResponse *)response;

/**
 * Response from initial request
 */
-(void)initialListResponse:(NSNotification *)notification;

/**
 * Response from execute request
 */
-(void)executeFOResponse:(NSNotification *)notification;

/**
 * Notifies the payment operation process the data response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The data response received from the server
 * @return YES when the data response is correct, NO otherwise
 */
- (void)dataResponseReceived:(NSNotification *)notification;

/**
 * Search list response.
 *
 * @param notification: data send by the notification
 * @private
 */
- (void)institutionDetailResponse:(NSNotification *)notification;

/**
 * List of pending documents and other information response.
 *
 * @param notification: data send by the notification
 * @private
 */
- (void)pendingPaysInformationResponse:(NSNotification *)notification;

@end

@implementation FrequentOperationListViewController

@synthesize executeButton = executeButton_;
@synthesize frequentOperationArray = frequentOperationArray_;
@synthesize frequentOperationTable = frequentOperationTable_;
@synthesize hasForward = hasForward_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [executeButton_ release];
    executeButton_ = nil;
    
    [frequentOperationArray_ release];
    frequentOperationArray_ = nil;
    
    [frequentOperationTable_ release];
    frequentOperationTable_ = nil;
    
    if (foOperationHelper != nil) {
        [foOperationHelper release];
        foOperationHelper = nil;
    }
    
    if (publicServStepThreeViewController_ != nil) {
        [publicServStepThreeViewController_ release];
        publicServStepThreeViewController_ =  nil;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFrequentOperationsInitialListResponse object:nil];
    
    [super dealloc];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark intiailization methods

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    frequentOperationArray_ = [[NSArray alloc] init];
}

/*
 * Creates and returns an autoreleased TransfersListViewController constructed from a NIB file.
 */
+ (FrequentOperationListViewController *)frequentOperationListViewController {
    
    FrequentOperationListViewController *result =  [[[FrequentOperationListViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        self.navigationController.navigationBar.tintColor = [UIColor BBVABlueSpectrumColor];
        
    } else
    {
        self.navigationController.navigationBar.barTintColor = [UIColor BBVABlueSpectrumColor];
        self.navigationController.navigationBar.translucent = NO;
        
    }
    
    hasForward_ = FALSE;
    
    page_ = 1;
    
    [NXT_Peru_iPhoneStyler styleBlueButton:executeButton_];
    [executeButton_ setTitle:NSLocalizedString(EXECUTE_TITLE_KEY, nil) forState:UIControlStateNormal];
    
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:TRUE];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.appDelegate setTabBarVisibility:YES animated:YES];
    
    if (!hasForward_) {
        page_ = 1;
        selectedIndex_ = -1;
        [frequentOperationTable_ setBackgroundColor:[UIColor clearColor]];
        [frequentOperationTable_ setHidden:TRUE];
        [executeButton_ setHidden:TRUE];
        [frequentOperationTable_ setDelegate:self];
        [frequentOperationTable_ setDataSource:self];
        [self listFrequentOperationRequest:@"adelante"];
        
    }else{
        hasForward_ = FALSE;
    }
    
}

#pragma mark -
#pragma mark user interactions
/**
 * execute the request operation
 *
 */
- (IBAction)onTapExecute:(id)sender{
 
    if (selectedIndex_ == -1 || selectedIndex_ > [frequentOperationArray_ count]) {
        [Tools showAlertWithMessage:NSLocalizedString(FO_ERROR_SELECT_OPERATION_KEY, nil)];
        return;
    }
    
    [self.appDelegate showActivityIndicator:poai_Both];
    
    FrequentOperation *FOObject = [frequentOperationArray_ objectAtIndex:selectedIndex_];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(executeFOResponse:) name:kNotificationFrequentOperationsExecuteResponse object:nil];
    
    [[Updater getInstance] obtainFrequentOperationExecuteInfoFromCodService:FOObject.operationCode AfilitionNum:FOObject.memberNumber AsociationNick:FOObject.nickname AfilitationType:FOObject.afilitationForm];
    
}
#pragma mark -
#pragma mark Request operations
/**
 * Request the list of frequent operations
 */
- (void)listFrequentOperationRequest:(NSString *)action{
    
    [self.appDelegate showActivityIndicator:poai_Both];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initialListResponse:) name:kNotificationFrequentOperationsInitialListResponse object:nil];
    [[Updater getInstance] obtainFrequentInformationInitialListInfo:@"-1"];
}

/**
 * Request the Third cardp pay
 */
- (void)thirdCardPaymentRequest:(FOEThirdCardPaymentStepOneResponse *)response{
    
    [self.appDelegate showActivityIndicator:poai_Both];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dataResponseReceived:)
                                                 name:kNotificationPaymentDataResultEnds object:nil];
    
    FOEThirdCardPaymentStepOneResponse *thirdCardResponse = (FOEThirdCardPaymentStepOneResponse *)response;
    
    NSString *cardNumber = [[thirdCardResponse thirdCard] stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    [[Updater getInstance] obtainPaymentCardContinentalThirdAccountDataForIssue:cardNumber];
}

/**
 * Request The initial data for Cash mobile
 */
- (void)cashMobileRequest:(FOECashMobileStepOneResponse *)response{
    
    [self.appDelegate showActivityIndicator:poai_Both];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dataResponseReceived:)
                                                 name:kNotificationTransferWithCashMobileStartupEnds object:nil];
    
    
    [[Updater getInstance] transferToAccountsWithCashMobileStartup];
}

/**
 * Request The initial data for institutions and companies
 */
- (void)paymentInstAndCompSearchRequest:(FOEInstitutionPaymentStepOneResponse *)response{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(institutionDetailResponse:) name:kNotificationPaymentInstitutionsDetailResult object:nil];
    
    [[self appDelegate] showActivityIndicator:poai_Both];
    
    [[Updater getInstance] obtainPaymentInstitutionsDetailForCod:response.param optionType:@"INST"];
    
    foeInstResponse_ = [response retain];
}

/**
 * Request The initial data for institutions and companies
 */
- (void)paymentInstAndCompRequest:(PaymentInstitutionsAndCompaniesDetailResponse *)response{
    
    
    NSArray *parameterArray = [foeInstResponse_.param componentsSeparatedByString:@"$"];
    
    
    if ([[parameterArray objectAtIndex:4] isEqualToString:@"N"]) {
        
        [self.appDelegate hideActivityIndicator];
        
        FreqOpeViewControllerStepZeroViewController *freqOpeViewControllerStepZeroViewController = [FreqOpeViewControllerStepZeroViewController freqOpeViewControllerStepZeroViewController];
        
        [freqOpeViewControllerStepZeroViewController setFoOperationHelper:(FOInstitutionsAndCompanies *)foOperationHelper];
        
        [freqOpeViewControllerStepZeroViewController setResponseInformation:response];
        
        [freqOpeViewControllerStepZeroViewController setArrayFilledFields:foeInstResponse_.array];
        
        [freqOpeViewControllerStepZeroViewController setArrayEditableFields:foeInstResponse_.editableArray];
        
        [self.navigationController pushViewController:freqOpeViewControllerStepZeroViewController animated:TRUE];
    }
    else
    {
    [self.appDelegate showActivityIndicator:poai_Both];
        
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pendingPaysInformationResponse:) name:kNotificationPaymentInstitutionsPendingPaysDetailResult object:nil];
    
    
    [[Updater getInstance] obtainInstitutionsAndCompaniesPendingPaysForCode:foeInstResponse_.matriCode
                                                                  arrayLong:foeInstResponse_.arrayLong
                                                                      array:foeInstResponse_.array
                                                                titlesArray:foeInstResponse_.titlesArray
                                                             validationData:response.dataValidation
                                                                 flagModule:response.moduleFlag];
    }
}

#pragma mark -
#pragma mark Notification
-(void)initialListResponse:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFrequentOperationsInitialListResponse object:nil];
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (response != nil && ![response isError]) {
        
        if (initialResponse != nil) {
            
            [initialResponse release];
            initialResponse = nil;
        }
        
        initialResponse = (FOPGeneralMenuStepOneResponse *)[notification object];
        
        if (frequentOperationArray_ != nil) {
            [frequentOperationArray_ release];
            frequentOperationArray_ = nil;
        }
        
        frequentOperationArray_ = [[NSArray alloc] initWithArray:[initialResponse.frequentOperationList frequentOperationArray]];
        
        selectedIndex_ = -1;
        
        [frequentOperationTable_ reloadData];
        
        if ([frequentOperationArray_ count] == 0) {
            
            [executeButton_ setHidden:TRUE];
            [frequentOperationTable_ setHidden:TRUE];
        }else{
            
            [executeButton_ setHidden:FALSE];
            [frequentOperationTable_ setHidden:FALSE];
            
            CGRect tableFrame = [frequentOperationTable_ frame];
            
            if ([frequentOperationTable_ contentSize].height < INITIAL_TABLE_HEIGHT) {
                
                tableFrame.size.height = [frequentOperationTable_ contentSize].height;
                [frequentOperationTable_ setFrame:tableFrame];
            }else{
                tableFrame.size.height = (isiPhone5)?INITIAL_TABLE_HEIGHT_4Inch:INITIAL_TABLE_HEIGHT;
                [frequentOperationTable_ setFrame:tableFrame];
            }
            
            CGRect buttonframe=executeButton_.frame;
            buttonframe.origin.y=CGRectGetMaxY(tableFrame)+25;
            [executeButton_ setFrame:buttonframe];
            
            [frequentOperationTable_ scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:FALSE];
        }
        
        [initialResponse retain];
        
    }
    
}

-(void)executeFOResponse:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFrequentOperationsExecuteResponse object:nil];
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (![response isError]) {
        
        FrequentOperation *FOObject = [frequentOperationArray_ objectAtIndex:selectedIndex_];
        
        if (foOperationHelper != nil) {
            [foOperationHelper release];
            foOperationHelper = nil;
        }
        
        if (paymentProcess_ != nil) {
            [paymentProcess_ release];
            paymentProcess_ = nil;
        }
        
        if ([FO_TRANS_TO_OTHER_BANK_CODE isEqualToString:FOObject.operationCode])
        {
            foOperationHelper = [[FOTransferToOtherBankAccount alloc] initWithTransferStartupResponse:[notification object]];
            
        } else if ([FO_TRANS_TO_THIRD_CODE isEqualToString:FOObject.operationCode])
        {
            foOperationHelper = [[FOTransferToThirdAccount alloc] initWithTransferStartupResponse:[notification object]];
            
        } else if ([FO_RECHARGE_GIFT_CARD isEqualToString:FOObject.operationCode])
        {
            foOperationHelper = [[FORechargeGiftCard alloc] initWithRechargeStartupResponse:[notification object]];
            
        } else if ([FO_PAY_THIRD_CARD isEqualToString:FOObject.operationCode])
        {
            foOperationHelper = [[FOPaymentCCThirdCard alloc] initWithPaymentStartupResponse:[notification object]];
            
        } else if ([FO_PAY_OTHER_BANK_CARD isEqualToString:FOObject.operationCode])
        {
            foOperationHelper = [[FOPaymentCOtherBank alloc] initWithPaymentStartupResponse:[notification object]];
            
        } else if ([FO_CASH_MOBILE isEqualToString:FOObject.operationCode])
        {
            foOperationHelper = [[FOTransferWithCashMobile alloc] initWithTransferStartupResponse:[notification object]];
            
        } else if ([FO_RECHARGE_CELL_PHONE isEqualToString:FOObject.operationCode])
        {
            foOperationHelper = [[FORechargeCellPhone alloc] initWithPaymentStartupResponse:[notification object]];
            
        } else if ([FO_PAY_AGREEMENT isEqualToString:FOObject.operationCode]){
            
            foOperationHelper = [[FOInstitutionsAndCompanies alloc] initWithPaymentStartupResponse:[notification object]];
            
        } else if ([FO_PAY_TELE_SERVICE isEqualToString:FOObject.operationCode]){
            
            FOEServicePaymentStepOneResponse *serviceResponse = [notification object];
            
            if ([FO_LANDLINE isEqualToString:serviceResponse.serviceType]) {
                paymentProcess_ = [[PaymentPSLandlineServProcess alloc] init];
                [paymentProcess_ setSelectedCompany:@"Telefónica"];
                [paymentProcess_ setSelectedCompanyCode:COMPANY_TELEFONICA];
                
            }else if([FO_CELLPHONE isEqualToString:serviceResponse.serviceType]) {
                paymentProcess_ = [[PaymentPSCellServProcess alloc] init];
                [paymentProcess_ setSelectedCompany:@"Movistar"];
                [paymentProcess_ setSelectedCompanyCode:@"movi"];
                
            }
        } else if ([FO_PAY_CLARO_SERVICE isEqualToString:FOObject.operationCode]){
            
            paymentProcess_ = [[PaymentPSCellServProcess alloc] init];
            [paymentProcess_ setSelectedCompany:@"Claro"];
            [paymentProcess_ setSelectedCompanyCode:@"clar"];
            
        } else if ([FO_PAY_WATER_SERVICE isEqualToString:FOObject.operationCode]){
            
            paymentProcess_ = [[PaymentPSWaterServProcess alloc] init];
        }
        
        if ([foOperationHelper foOperationType] == FOTEPaymentThirdCard) {
            
            [self thirdCardPaymentRequest:[notification object]];
            
        } else if ([foOperationHelper foOperationType] == FOTEPaymentInstAndComp){
            
            [self paymentInstAndCompSearchRequest:[notification object]];
            
        } else if (paymentProcess_ != nil){
            
            foeServiceResponse = [[notification object] retain];
           /*
            if ([[foeServiceResponse.company lowercaseString] rangeOfString:@"tele"].location != NSNotFound) {
                
                [paymentProcess_ setSelectedCompanyCode:COMPANY_TELEFONICA];
                
                
                

                
            } else if ([[foeServiceResponse.company lowercaseString] rangeOfString:@"claro"].location != NSNotFound){
                
                [paymentProcess_ setSelectedCompanyCode:COMPANY_CLARO3PLAY];
                
            } else*/
            
            if ([[foeServiceResponse.company lowercaseString] rangeOfString:@"sedapal"].location != NSNotFound && foeServiceResponse.company !=nil){
                [paymentProcess_ setSelectedCompany:foeServiceResponse.company];
                [paymentProcess_ setSelectedCompanyCode:COMPANY_SEDAPAL];
                
            }
            
            if ([@"" isEqualToString:paymentProcess_.selectedCompany]) {
                [paymentProcess_ setSelectedCompany:foeServiceResponse.company];
            }
        
            
            [paymentProcess_ setSelectedCompanyParameter:@""];
            [paymentProcess_ setIsFO:TRUE];
            [paymentProcess_ setDelegate:self];
            [paymentProcess_ setPsDelegate:self];
            [paymentProcess_ setFoeResponse:foeServiceResponse];
            [paymentProcess_ startPaymentInitialRequest];
            
        } else{
            
            FrequentOperationStepOneViewController *stepOneView = [FrequentOperationStepOneViewController frequentOperationStepOneViewController];
            [stepOneView setFoOperationHelper:foOperationHelper];
            
            [self.navigationController pushViewController:stepOneView animated:TRUE];
            
            hasForward_ = TRUE;
        }
    }
    
}

/**
 * Notifies the payment operation process the data response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The data response received from the server
 * @return YES when the data response is correct, NO otherwise
 */
- (void)dataResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationPaymentDataResultEnds
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationTransferWithCashMobileStartupEnds object:nil];
    [[self appDelegate] hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
	
	if (![response isError]) {
        
        FrequentOperationStepOneViewController *stepOneView = [FrequentOperationStepOneViewController frequentOperationStepOneViewController];
        [stepOneView setFoOperationHelper:foOperationHelper];
        
        [self.navigationController pushViewController:stepOneView animated:TRUE];
        
        hasForward_ = TRUE;
    }
    
}

/**
 * Search list response.
 *
 * @param notification: data send by the notification
 * @private
 */
- (void)institutionDetailResponse:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationPaymentInstitutionsDetailResult object:nil];
    
    PaymentInstitutionsAndCompaniesDetailResponse *response = [notification object];
    
    [[self appDelegate] hideActivityIndicator];
    
    if (response != nil && [response isKindOfClass:[PaymentInstitutionsAndCompaniesDetailResponse class]] && !response.isError) {
        
        [self paymentInstAndCompRequest:response];
    }
    
}

/**
 * List of pending documents and other information response.
 *
 * @param notification: data send by the notification
 * @private
 */
- (void)pendingPaysInformationResponse:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationPaymentInstitutionsPendingPaysDetailResult object:nil];
    
    [[self appDelegate] hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
	
	if (![response isError]) {
        
        FreqOpeAgreementsAndServicesStepOneViewController *stepOneView = [FreqOpeAgreementsAndServicesStepOneViewController freqOpeAgreementsAndServicesStepOneViewController];
        
        [stepOneView setFoOperationHelper:(FOInstitutionsAndCompanies *)foOperationHelper];
        
        [stepOneView setPaymentDetailEntity:[notification object]];
        
        [stepOneView resetInformation];
        
        [self.navigationController pushViewController:stepOneView animated:TRUE];
        
        hasForward_ = TRUE;
        
        [foeInstResponse_ release];
        foeInstResponse_ = nil;
    }
    
}


#pragma mark -
#pragma mark - process payment delegate

-(void)initilizationHasFinished:(BOOL)isNextDynamicView{
    
    if (!isNextDynamicView) {
        
        [paymentProcess_ setSupplies:foeServiceResponse.companyCode];
        [paymentProcess_ startPaymentDataRequest];
        
    }else{
        [foeServiceResponse release];
        foeServiceResponse = nil;
    }
}

/**
 * Delegate is notified when the data analysis has finished
 */
- (void)dataAnalysisHasFinished {
    
    if (publicServStepThreeViewController_ != nil) {
        
        [publicServStepThreeViewController_ release];
        publicServStepThreeViewController_ = nil;
        
    }
    
    publicServStepThreeViewController_ = [[PublicServStepThreeViewController publicServStepThreeViewController] retain];
    
    hasForward_ = YES;
    
    [publicServStepThreeViewController_ setProcess:paymentProcess_];
    [publicServStepThreeViewController_ setHasNavigateForward:NO];
    
    [[self navigationController] pushViewController:publicServStepThreeViewController_
                                           animated:YES];
    
    [foeServiceResponse release];
    foeServiceResponse = nil;
    
}

#pragma mark -
#pragma mark UITableDelegate - UITableDatasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [frequentOperationArray_ count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [CheckFOCell cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CheckFOCell *cell = [tableView dequeueReusableCellWithIdentifier:[CheckFOCell cellIdentifier]];
    
    if (cell == nil) {
        cell = [[[CheckFOCell checkFOCell] retain] autorelease];
    }
    
    FrequentOperation *FOObject = [frequentOperationArray_ objectAtIndex:indexPath.row];
    
    [cell.titleLabel setText:FOObject.nickname];
    [cell.subtitleLabel setText:FOObject.service];
    
    if (FOObject.dayMonth != nil && ![@"" isEqualToString:FOObject.dayMonth] && [FOObject.dayMonth integerValue] != 0) {
        [cell.dayLabel setText:[NSString stringWithFormat:NSLocalizedString(FO_ACTIVE_DAY_TEXT_KEY, nil), FOObject.dayMonth]];
    }
    
    if (FOObject.cellphone != nil && ![@"" isEqualToString:FOObject.cellphone]) {
        [cell setHasPhoneActive:TRUE];
    }else{
        [cell setHasPhoneActive:FALSE];
    }
    
    if (FOObject.email != nil && ![@"" isEqualToString:FOObject.email]) {
        [cell setHasMailActive:TRUE];
    }else{
        [cell setHasMailActive:FALSE];
    }
    [cell setCheckActive:(selectedIndex_ == indexPath.row)];
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedIndex_ = indexPath.row;
    
    [frequentOperationTable_ reloadData];
}

#pragma mark -
#pragma mark Scroll delegate
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    
    if ((currentOffset > 0) && (maximumOffset - currentOffset) <= 10)
    {
        if(initialResponse != nil)
        {
            [self loadNextFOSearchPaged];
        }
        
    }
    else if(currentOffset<=0)
    {
        [self loadBackFOSearchPaged];
    }
    
}


-(void)loadBackFOSearchPaged
{
    if([initialResponse backPage] == nil)
        return;
    
    [[self appDelegate] showActivityIndicator:poai_Both];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initialListResponse:) name:kNotificationFrequentOperationsInitialListResponse object:nil];
    
    [[Updater getInstance] obtainFrequentInformationInitialListInfo:initialResponse.backPage];
    
}

-(void)loadNextFOSearchPaged
{
    if(![initialResponse hasMoreData])
        return;
    
    [self.appDelegate showActivityIndicator:poai_Both];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initialListResponse:) name:kNotificationFrequentOperationsInitialListResponse object:nil];
    
    [[Updater getInstance] obtainFrequentInformationInitialListInfo:initialResponse.nextPage];
    
}
#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = [self customNavigationItem];
    
    [[result customTitleView] setTopLabelText:NSLocalizedString(FO_POSITION_TITLE_TEXT_KEY, nil)];
    
    return result;
    
}

@end
