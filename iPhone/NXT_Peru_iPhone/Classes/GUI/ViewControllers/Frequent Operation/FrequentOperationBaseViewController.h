/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MOKEditableViewController.h"
#import "FOEmailMokCell.h"
#import "MOKStringListSelectionButton.h"
#import "AliasFOCell.h"
#import "FOSMSMokCell.h"
#import "CheckChannelCell.h"
#import "FOInscriptionBaseProcess.h"
#import "NotificationSwitchCell.h"
#import "FrequentOperationInscriptionConfirmationViewController.h"
#import <AddressBookUI/AddressBookUI.h>

@class EmailMokCell;
@class PaymentBaseProcess;
@class SimpleHeaderView;
@class SMSMokCell;
@class PaymentConfirmationViewController;

/**
 * View controller to show public services payment step one.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface FrequentOperationBaseViewController : MOKEditableViewController <FOInscriptionBaseProcessDelegate, UITableViewDataSource, UITableViewDelegate, FOSMSMokCellDelegate, FOEmailMokCellDelegate, NotificationSwitchDelegate, UITextFieldDelegate, UITextViewDelegate, MOKStringListSelectionButtonDelegate, AliasFOCellDelegate> {
    
@private
    
#pragma Graphic
    
    MOKTextField *largeAliasTextField_;
    MOKTextField *shortAliasTextField_;
    
    /**
     * First View
     */
    UIView *firstView_;
    
    SimpleHeaderView *secondHeaderView_;
    
    /**
     * Second View
     */
    UIView *secondView_;
    
    /**
     * Third header View
     */
    SimpleHeaderView *thirdHeaderView_;
    
    /**
     * Third View
     */
    UIView *thirdView_;
    
    /**
     * Fourth View
     */
    UIView *fourthView_;
    
    /**
     * Fourth header view
     */
    SimpleHeaderView *fourthHeaderView_;
    
    /**
     * Continue button
     */
    UIButton *continueButton_;
    
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    
    
    NotificationSwitchCell *activeNotificationCell_;
    
    
    CheckChannelCell *checkChannelCell_;
    
    
    //AliasFOCell *aliasCell_;
    
    /**
     * SMS Mok Cell
     */
    FOSMSMokCell *smsCell_;
    
    /**
     * Email Mok Cell
     */
    FOEmailMokCell *emailCell_;
    
    
    /**
     * Selection table third view
     */
    UITableView *selectionTableSecondView_;
    
    /**
     * Selection table third view
     */
    AliasFOCell *selectionTableThirdView_;
    
    /**
     * Selection table fourth view
     */
    UITableView *selectionTableFourthView_;
    
    
    /**
     * Payment Confirmation View Controller
     */
    FrequentOperationInscriptionConfirmationViewController *frequentOperationInscriptionConfirmationViewController_;
    
#pragma Logic

    /**
     * Adding first email flag
     */
    BOOL addingFirstEmail_;
    
    /**
     * Adding first email flag
     */
    BOOL addingSecondEmail_;
    
    
    CGFloat secondViewHeight_;
    
    /**
     * Third view height
     */
    CGFloat thirdViewHeight_;
    
    /**
     * Fourth view height
     */
    CGFloat fourthViewHeight_;
    
    /**
     * Navigation flag
     */
    BOOL hasNavigateForward_;
    
    
    NSInteger activeCellIndex_;
    
@public
    /**
     * Process
     */
    FOInscriptionBaseProcess *process_;
    
}

/**
 * Provides readwrite access to the firstView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIView *firstView;

/**
 * Provides readwrite access to the secondView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIView *secondView;

/**
 * Provides readwrite access to the thirdView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIView *thirdView;

/**
 * Provides readwrite access to the fourthView and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIView *fourthView;

/**
 * Provides readwrite access to the continueButton and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIButton *continueButton;

/**
 * Provides readwrite access to the brandingLine and exports it to the IB
 */
@property (nonatomic,readwrite, retain) IBOutlet UIImageView *brandingLine;

/**
 * Provides readwrite access to the process and exports it to the IB
 */
@property (nonatomic,readwrite, retain) FOInscriptionBaseProcess *process;

/**
 * Provides readwrite access to the process and exports it to the IB
 */
@property (nonatomic,readwrite, retain) MOKTextField *largeAliasTextField;

/**
 * Provides readwrite access to the process and exports it to the IB
 */
@property (nonatomic,readwrite, retain) MOKTextField *shortAliasTextField;



/**
 * Provides read-write access to the hasNavigateForward
 */
@property (nonatomic, readwrite, assign) BOOL hasNavigateForward;

/**
 * Returns the view needed in the first view. NIL if it is not needed
 */
- (void)viewForFirstView;

/**
 * Returns the view needed in the second view. NIL if it is not needed
 */
- (void)viewForSecondView;

/**
 * Returns the view needed in the third view. NIL if it is not needed
 */
- (void)viewForThirdView;

/**
 * Returns the view needed in the fourth view. NIL if it is not needed
 */
- (void)viewForFourthView;

/**
 * Returns the height for the first view. 0 if it is NIL
 */
- (CGFloat)heightForFirstView;

/**
 * Returns the height for the second view. 0 if it is NIL
 */
- (CGFloat)heightForSecondView;

/**
 * Returns the height for the third view. 0 if it is NIL
 */
- (CGFloat)heightForThirdView;

/**
 * Returns the height for the third view. 0 if it is NIL
 */
- (CGFloat)heightForFourthView;

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped;

/**
 * Relocate views
 */
- (void)relocateViews;

/**
 * Save process information. By default does nothing. Each child must overrride the method if needed
 */
- (void)saveProcessInformation;

@end
