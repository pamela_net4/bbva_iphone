//
//  FrequentOperationAlterStepOneViewControllerlViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 12/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationAlterStepOneViewControllerViewController.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "SimpleHeaderView.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "UIButton+Util.h"
#import "MOKEditableViewController+protected.h"
#import "TitleAndAttributes.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "BankAccount.h"
#import "FrequentOperationInscriptionConfirmationViewController.h"
#import "Tools.h"



#define SHORT_VERTICAL_DISTANCE                                50.f
#define LONG_VERTICAL_DISTANCE                                 10.f

#define NIB_FILE_NAME @"FrequentOperationStepAlterOneViewController"

#define TEXT_FONT_SIZE 14.0f
#define MAXLENGTH 10

/**
 * Defines the cell height
 */
#define FONT_SIZE                                           14.0f

/**
 * Denifes the vertical margin size
 */
#define VERTICAL_MARGIN_SIZE                                10.0f

/**
 * Denifes the vertical margin size
 */
#define SWITCH_HEIGHT                                       27.0f

/**
 * Denifes the vertical margin size
 */
#define TXTFIELD_HEIGHT                                     31.0f

/**
 * Denifes the vertical margin size
 */
#define COMBO_HEIGHT                                        37.0f

/**
 * Denifes the vertical margin size
 */
#define BUTTON_HEIGHT                                       37.0f

@interface FrequentOperationAlterStepOneViewControllerViewController ()


- (void) releaseFrequentOperationAlterStepOneViewControllerlViewControllerGraphicsElements;

@end

@implementation FrequentOperationAlterStepOneViewControllerViewController

@synthesize largeAliasTextLabel = largeAliasTextLabel_;
@synthesize shortAliasTextLabel = shortAliasTextLabel_;
@synthesize separator = separator_;
@synthesize suggestionLinkButton = suggestionLinkButton_;
@synthesize suggestionLineView = suggestionLineView_;

- (void)dealloc
{
    [self releaseFrequentOperationAlterStepOneViewControllerlViewControllerGraphicsElements];
    
    [super dealloc];
}

- (void) didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseFrequentOperationAlterStepOneViewControllerlViewControllerGraphicsElements];
        [self setView:nil];
        
    }

}

- (void) releaseFrequentOperationAlterStepOneViewControllerlViewControllerGraphicsElements {
    
    
    for (UILabel *label in labelsArray_) {
        
        [label removeFromSuperview];
        [label release];
        label = nil;
        
    }
    
    //[labelsArray_ removeAllObjects];
    
    [infoSectionViewHeader_ release];
    infoSectionViewHeader_ = nil;
    
    [infoSectionViewSeparator_ release];
    infoSectionViewSeparator_ = nil;
    
    [channelsViewHeader_ release];
    channelsViewHeader_ = nil;
    
    [channelsViewSeparator_ release];
    channelsViewSeparator_ = nil;
    
}

- (void) awakeFromNib {

    [super awakeFromNib];
    
    labelsArray_ = [[NSMutableArray alloc] init];
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];

}

- (void) viewDidLoad {

    [super viewDidLoad];
    
    [[self firstView] setBackgroundColor: [UIColor whiteColor]];
    [NXT_Peru_iPhoneStyler styleNXTView:[self firstView]];
    [NXT_Peru_iPhoneStyler styleNXTView:[self secondView]];
    
    infoSectionViewHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [[self firstView] addSubview:infoSectionViewHeader_];
    
    infoSectionViewSeparator_ = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 2.0f)];
    
    [infoSectionViewSeparator_ setImage:[[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
    [[self firstView] addSubview:infoSectionViewSeparator_];
    
    [infoSectionViewHeader_ setTitle:NSLocalizedString(FO_AFFILIATION_TITLE_TEXT_KEY, nil)];
    
    
    separator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    
    [NXT_Peru_iPhoneStyler styleButton:suggestionLinkButton_ withBoldFont:FALSE fontSize: FONT_SIZE color: [UIColor BBVABlueColor] disabledColor: [UIColor BBVAWhiteColor] andShadowColor: [UIColor BBVAGreyToneFiveColor]];
    
    [suggestionLineView_ setBackgroundColor: [UIColor BBVABlueColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:largeAliasTextLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlackColor]];
    largeAliasTextLabel_.numberOfLines = 0;
    
    [NXT_Peru_iPhoneStyler styleMokTextField:[self largeAliasTextField] withFontSize:TEXT_FONT_SIZE andColor:[UIColor BBVAGreyColor]];
    [[self largeAliasTextField] setDelegate:self];
    //    largeAliasTextField_.inputAccessoryView=popButtonsView_;
    
    [NXT_Peru_iPhoneStyler styleLabel:shortAliasTextLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlackColor]];
    shortAliasTextLabel_.numberOfLines = 0;
    [[self shortAliasTextField] setDelegate:self];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:[self shortAliasTextField] withFontSize:TEXT_FONT_SIZE andColor:[UIColor BBVAGreyColor]];
    
}

- (void) viewDidUnload {

    [super viewDidUnload];
    [self releaseFrequentOperationAlterStepOneViewControllerlViewControllerGraphicsElements];
}


- (void) viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    [self resetScrollToTopLeftAnimated: YES];
    [self.appDelegate setTabBarVisibility:NO animated:YES];

    if([self hasNavigateForward]){
    
        [self setHasNavigateForward:NO];
        
    }
    
    [self viewForFirstView];
    [self relocateViews];
}

- (void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:TRUE];
    
    [self viewForFirstView];
    [self relocateViews];
    
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]);
    [self setScrollFrame:frame];
    
}

- (void) viewForFirstView {
    
    
    CGFloat yPosition = 0.0f;
    
    CGRect frame = infoSectionViewHeader_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    infoSectionViewHeader_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;

    NSArray *infoArray = [[NSArray alloc] initWithArray: [process_ dataInfoArray]];
    [labelsArray_ removeAllObjects];
    
    for (TitleAndAttributes *titleAndAttributes in infoArray)
    {
        
        UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(20.0f, yPosition, 280.0f, 15.0f)] autorelease];
        
        yPosition += 15.0f;
        
        
        CGSize detailSize = [[[titleAndAttributes attributesArray] objectAtIndex:0] sizeWithFont:
         [NXT_Peru_iPhoneStyler normalFontWithSize:14.0f] constrainedToSize:CGSizeMake(9999.0f, 15.0f)] ;
        
        
        UILabel *detailLabel = [[[UILabel alloc] initWithFrame:CGRectMake(20.0f, yPosition, 280.0f,(detailSize.width>280.0f)?38.0f: 15.0f)] autorelease];
        
        [detailLabel setNumberOfLines:2];
        
        yPosition += 20.0f+((detailSize.width>280.0f)?20:0);
        
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [detailLabel setBackgroundColor:[UIColor clearColor]];
        
        [NXT_Peru_iPhoneStyler styleLabel:titleLabel withBoldFontSize:13.0f color:[UIColor grayColor]];
        
        [NXT_Peru_iPhoneStyler styleLabel:detailLabel withFontSize:14.0f color:[UIColor BBVAGreyToneTwoColor]];
        
        [titleLabel setText:[titleAndAttributes titleString]];
        
        if ([[titleAndAttributes attributesArray] count] == 0) {
            
            [detailLabel setText:@""];
            
        } else {
            
            [detailLabel setText:[[titleAndAttributes attributesArray] objectAtIndex:0]];
            
        }
        
        [labelsArray_ addObject:titleLabel];
        [labelsArray_ addObject:detailLabel];
        
        [[self firstView] addSubview:titleLabel];
        [[self firstView] addSubview:detailLabel];
        
    }
    
    yPosition += 10.0f;
    
    frame = infoSectionViewSeparator_.frame;
    frame.origin.y = yPosition;
    infoSectionViewSeparator_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    firstViewHeight_ = yPosition;
    
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == [self largeAliasTextField]) {
        
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [Tools isValidText:string forCharacterString:REFERENCE_VALID_CHARACTERS];
        
        return newLength <= 30 && returnKey;
        
    } else if(textField == [self shortAliasTextField]){
        
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [Tools isValidText:string forCharacterString:REFERENCE_VALID_CHARACTERS];
        
        return newLength <= 10 && returnKey;
        
    }
    
    return YES;
    
}


- (IBAction)suggestionLinkButtonTapped:(id)sender {
    
    NSString *newString = [[process_ suggestion] stringByReplacingOccurrencesOfString: @"\\n" withString:@"\r"];
    UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(ALERT_MESSAGE_TITLE_KEY, nil) message:newString delegate:nil cancelButtonTitle:NSLocalizedString(OK_TEXT_KEY, nil) otherButtonTitles:nil] autorelease];
    [alertView show];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
       ((UILabel*)[[alertView subviews] objectAtIndex:2]).textAlignment = UITextAlignmentLeft;
    }
    
    
    
}

- (CGFloat) heightForFirstView {
    
    return firstViewHeight_;
    
}

- (UINavigationItem *) navigationItem {
    
    UINavigationItem *result  = [super navigationItem];
    
    NSString *title = @"";
    
    title = NSLocalizedString(FO_TITLE_TEXT_REACTIVE_KEY, nil);
    
    result.mainTitle = title;
    
    return result;
    
}

+ (FrequentOperationAlterStepOneViewControllerViewController *) frequentOperationAlterStepOneViewController {

    FrequentOperationAlterStepOneViewControllerViewController *result = [[[FrequentOperationAlterStepOneViewControllerViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    return result;

}

-(void) dataAnalysisHasFinished {}

- (void) setProcess:(FOInscriptionBaseProcess *)process {
    
    [super setProcess: process];
    
}

@end
