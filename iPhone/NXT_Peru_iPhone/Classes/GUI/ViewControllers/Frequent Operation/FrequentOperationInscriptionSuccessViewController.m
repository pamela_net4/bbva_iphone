//
//  FrequentOperationInscriptionSuccessViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 21/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationInscriptionSuccessViewController.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "SimpleHeaderView.h"
#import "Constants.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"
#import "MOKEditableViewController+protected.h"
#import "UINavigationItem+DoubleLabel.h"
#import "TitleAndAttributes.h"
#import "InformationCell.h"

/**
 * Defines the interior view distance
 */
#define INTERIOR_VIEW_DISTANCE                      5.0f

/**
 * Defines the exterior view distance
 */
#define EXTERIOR_VIEW_DISTANCE                      10.0f

/**
 * Defines the text font size
 */
#define TEXT_FONT_SIZE                              14.0f

/**
 * Defines the small text font size
 */
#define SMALL_TEXT_FONT_SIZE                        12.0f

/**
 * Defines the big text font size
 */
#define BIG_TEXT_FONT_SIZE                          17.0f

/**
 * Defines the Nib file name
 */
#define NIB_FILE_NAME                               @"FOInscriptionSuccessViewController"



@interface FrequentOperationInscriptionSuccessViewController()

- (void)releaseGraphicElements;

- (void)relocateViews;

- (void)updateInformation;

@end

@implementation FrequentOperationInscriptionSuccessViewController

@synthesize infoTable = infoTable_;
@synthesize backToOperationButton = backToOperationButton_;
@synthesize brandingLine = brandingLine_;
@synthesize extraInfoLabel = extraInfoLabel_;
@synthesize bottomSeparator = bottomSeparator_;
@synthesize process = process_;


- (void)dealloc {

    [process_ release];
    process_ = nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseGraphicElements];
        [self setView:nil];
        
    }
}

- (void)releaseGraphicElements {
    
    [infoTable_ release];
    infoTable_ = nil;
    
    [backToOperationButton_ release];
    backToOperationButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
}

- (void)awakeFromNib {

    [super awakeFromNib];
    
}

- (void)viewDidLoad {

    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    
    [tableBgImageView_ setImage:[[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME]];
    
    headerView_ = [[SimpleHeaderView simpleHeaderView] retain];
    [[self view ] addSubview:headerView_];
    
    [bottomSeparator_ setImage:[[ImagesCache getInstance] imageNamed: HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:backToOperationButton_];
    [backToOperationButton_ setTitle:NSLocalizedString(FO_BACK_TO_OPERATION_TITLE_TEXT_KEY, nil)
                    forState:UIControlStateNormal];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    [NXT_Peru_iPhoneStyler styleLabel:extraInfoLabel_ withFontSize:SMALL_TEXT_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    
    [extraInfoLabel_ setText:NSLocalizedString(CARD_PAYMENT_SUCCESS_BOTTOM_TEXT_KEY, nil)];
    [extraInfoLabel_ setNumberOfLines:2];
    [extraInfoLabel_ setTextAlignment:UITextAlignmentCenter];
    
    [infoTable_ setUserInteractionEnabled:NO];
    
}

- (void)viewDidUnload{

    [super viewDidUnload];
    [self releaseGraphicElements];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self resetScrollToTopLeftAnimated:NO];
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    [self updateInformation];
    [self relocateViews];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.origin.y = CGRectGetHeight([headerView_ frame]);
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]) - CGRectGetHeight([headerView_ frame]);
    [self setScrollFrame:frame];
    
}

- (void)relocateViews {
    
    [infoTable_ reloadData];
    
    CGRect frame = infoTable_.frame;
    
    CGFloat yPosition = EXTERIOR_VIEW_DISTANCE;

    frame.origin.y = yPosition;
    frame.size.height = [infoTable_ contentSize].height;
    infoTable_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
    
    frame = tableBgImageView_.frame;
    frame.origin.y = infoTable_.frame.origin.y + 3.0f;
    frame.size.height = [infoTable_ contentSize].height - 3.0f;
    tableBgImageView_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
    
    frame = extraInfoLabel_.frame;
    frame.origin.y = yPosition;
    extraInfoLabel_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
    
    frame = bottomSeparator_.frame;
    frame.origin.y = yPosition;
    bottomSeparator_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
    
    frame = backToOperationButton_.frame;
    frame.origin.y = yPosition;
    backToOperationButton_.frame = frame;
    
    
    yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
    
    [self setScrollContentSize:CGSizeMake(320.0f, yPosition)];
}

- (void)updateInformation {

    NSString *headerTitle = NSLocalizedString(FO_AFFILIATION_TITLE_TEXT_KEY, nil);
    [headerView_ setTitle:headerTitle];
    
    [extraInfoLabel_ setText: [process_ successMessage]];
    
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    InformationCell *result = (InformationCell *)[tableView dequeueReusableCellWithIdentifier:[InformationCell cellIdentifier]];
    
    if (result == nil) {
        
        result = [[[InformationCell informationCell] retain] autorelease];
        
    }
    
    TitleAndAttributes *titleAndAttribute = [[process_ successInfoArray] objectAtIndex:[indexPath row]];
    [result setInfo:titleAndAttribute];
    
    return result;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[process_ successInfoArray] count];
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TitleAndAttributes *titleAndAttributes = (TitleAndAttributes*) [[process_ successInfoArray] objectAtIndex: indexPath.row];
    CGFloat result = [InformationCell cellHeight];
    int resultCount = [[titleAndAttributes attributesArray] count];
    result += (20.0f * (resultCount - 1));
    if(resultCount==1)
    {
        CGSize recognizeWith = [[[titleAndAttributes attributesArray] objectAtIndex:0] sizeWithFont:[NXT_Peru_iPhoneStyler normalFontWithSize:TEXT_FONT_SIZE] constrainedToSize:CGSizeMake(9999, 17)];
        result+= (recognizeWith.width>270)?20.0f:0.0f;
    }
    
    return result;
    

    
}

+ (FrequentOperationInscriptionSuccessViewController *) frequentOperationInscriptionSuccessViewController {
    
    FrequentOperationInscriptionSuccessViewController *instance = [[[FrequentOperationInscriptionSuccessViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [instance awakeFromNib];
    
    return instance;
}

- (IBAction)backToOperationButtonTapped:(id)sender {
    
    [[self navigationController] popToViewController: [process_ rootViewController] animated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"frequentOperationHasFinished" object: nil];
    
}


- (void)setProcess:(FOInscriptionBaseProcess *)process {
    
    if (process_ != process) {
        
        [process retain];
        [process_ release];
        process_ = process;
        
    }
    [process_ setDelegate:self];
}

- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    result.hidesBackButton = YES;
    
    NSString *title = NSLocalizedString(FO_TITLE_TEXT_REACTIVE_KEY, nil);
    result.mainTitle = title;
    
    return result;
    
}

@end
