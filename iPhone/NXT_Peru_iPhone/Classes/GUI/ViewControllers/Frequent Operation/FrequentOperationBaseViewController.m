/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "FrequentOperationBaseViewController.h"

#import "AliasFOCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKDoubleLabel.h"
#import "MOKEditableViewController.h"
#import "MOKEditableViewController+protected.h"
#import "MOKNavigationItem.h"
#import "MOKStringListSelectionButton.h"
#import "MOKTextField.h"
#import "MOKTextView.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentConfirmationViewController.h"
#import "Session.h"
#import "SimpleHeaderView.h"
#import "FOSMSMokCell.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "Updater.h"
#import "NotificationSwitchCell.h"
#import "MOKStringListSelectionButton.h"
#import "FOEmailMokCell.h"
#import "Channel.h"
#import "ChannelList.h"
#import "FOPhone.h"
#import "FOEmail.h"

/**
 * Defines de max text lenght for the mail
 */
#define MAX_TEXT_MAIL_LENGHT                    80

/**
 * PaymentDataBaseViewController private extension
 */
@interface FrequentOperationBaseViewController()

/**
 * Releases the graphic elements
 *
 * @private
 */
- (void)releasePaymentDataBaseViewControllerGraphicElements;

/**
 * Fixes the add contact button images
 */
//- (void)fixAddContactButtonImages;

/**
 * Save contact information into the helper
 */
- (void)saveContactInfoIntoHelper;

@end


#pragma mark -

@implementation FrequentOperationBaseViewController

#pragma mark -
#pragma mark Properties

@synthesize firstView = firstView_;
@synthesize secondView = secondView_;
@synthesize thirdView = thirdView_;
@synthesize fourthView = fourthView_;
@synthesize continueButton = continueButton_;
@synthesize brandingLine = brandingLine_;
@synthesize process = process_;
@synthesize hasNavigateForward = hasNavigateForward_;
@synthesize largeAliasTextField=largeAliasTextField_;
@synthesize shortAliasTextField=shortAliasTextField_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releasePaymentDataBaseViewControllerGraphicElements];
    
    [process_ release];
    process_ = nil;
    
    //[frequentOperationInscriptionConfirmationViewController_ release];
    //frequentOperationInscriptionConfirmationViewController_ = nil;
    
    [super dealloc];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releasePaymentDataBaseViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releasePaymentDataBaseViewControllerGraphicElements {
    
	[firstView_ release];
    firstView_ = nil;

	[secondView_ release];
    secondView_ = nil;
    
    [thirdView_ release];
    thirdView_ = nil;
    
    [thirdHeaderView_ release];
    thirdHeaderView_ = nil;
    
    [fourthView_ release];
    fourthView_ = nil;
    
    [continueButton_ release];
    continueButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
    [selectionTableSecondView_ release];
    selectionTableSecondView_ = nil;
    
    [selectionTableThirdView_ release];
    selectionTableThirdView_ = nil;
    
    [selectionTableFourthView_ release];
    selectionTableFourthView_ = nil;
    
    //[aliasCell_ release];
    //aliasCell_ = nil;
    
    [checkChannelCell_ release];
    checkChannelCell_ = nil;
    
    [emailCell_ release];
    emailCell_ = nil;

    [smsCell_ release];
    smsCell_ = nil;
    
    [fourthHeaderView_ release];
    fourthHeaderView_ = nil;

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
}


#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    CGRect mainFrame = [[UIScreen mainScreen] bounds];
    [[self view] setFrame:mainFrame];
    
    [NXT_Peru_iPhoneStyler styleNXTView:[self view]];
    [firstView_ setBackgroundColor:[UIColor clearColor]];
    [secondView_ setBackgroundColor:[UIColor clearColor]];
    [thirdView_ setBackgroundColor:[UIColor clearColor]];
    
    [NXT_Peru_iPhoneStyler styleNXTView:secondView_];
    [NXT_Peru_iPhoneStyler styleNXTView:thirdView_];
    [NXT_Peru_iPhoneStyler styleNXTView:fourthView_];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:continueButton_];
    [continueButton_ setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil) 
                     forState:UIControlStateNormal];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    [[self view] bringSubviewToFront:brandingLine_];
    
    
    selectionTableSecondView_ = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 20.0f) style:UITableViewStylePlain];
    [selectionTableSecondView_ setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    selectionTableSecondView_.backgroundView.backgroundColor = nil;
    [selectionTableSecondView_ setDelegate:self];
    [selectionTableSecondView_ setDataSource:self];
    [selectionTableSecondView_ setBackgroundColor:[UIColor clearColor]];
    [selectionTableSecondView_ setAllowsMultipleSelection:TRUE];
    
    secondHeaderView_ = [[SimpleHeaderView simpleHeaderView] retain];
    [secondHeaderView_ setTitle:NSLocalizedString(FO_SELECT_CHANNELS_TEXT_KEY, nil)];
    
    [secondView_ addSubview:secondHeaderView_];
    [secondView_ addSubview:selectionTableSecondView_];
    
    thirdHeaderView_ = [[SimpleHeaderView simpleHeaderView] retain];
    [thirdHeaderView_ setTitle:NSLocalizedString(FO_ASSOCIATED_ALIAS_TEXT_KEY, nil)];
    
    //selectionTableThirdView_ = [[AliasFOCell aliasFOCell] retain];
    
    //[selectionTableThirdView_ setDelegate: self];
    [thirdView_ addSubview:thirdHeaderView_];
    // [thirdView_ addSubview:selectionTableThirdView_];
    
    
    selectionTableFourthView_ = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 20.0f) 
                                                       style:UITableViewStylePlain];
    [selectionTableFourthView_ setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    selectionTableFourthView_.backgroundView.backgroundColor = nil;
    [selectionTableFourthView_ setDelegate:self];
    [selectionTableFourthView_ setDataSource:self];
    [selectionTableFourthView_ setBackgroundColor:[UIColor clearColor]];
    
    fourthHeaderView_ = [[SimpleHeaderView simpleHeaderView] retain];
    [fourthHeaderView_ setTitle:NSLocalizedString(FO_REPORT_TO_BENEFICIARY_TEXT_KEY, nil)];
    
    [fourthView_ addSubview:fourthHeaderView_];
    [fourthView_ addSubview:selectionTableFourthView_];
    
    [selectionTableSecondView_ reloadData];
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [self releasePaymentDataBaseViewControllerGraphicElements];
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    // when navigation forware don't clear data.
    if (!hasNavigateForward_) {
        
        [self smsSwitchButtonHasBeenTapped:NO];
        [self emailSwitchButtonHasBeenTapped:NO];
        
    } else {
        
        [process_ setDelegate: self];
        
    }

    [self viewForSecondView];
    [self viewForThirdView];
    [self viewForFourthView];

}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self relocateViews];
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Unregisters from the orientation change service
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

#pragma mark -
#pragma mark View format

/**
 * Relocate views
 */
- (void)relocateViews {

    if ([self isViewLoaded]) {
        
        CGFloat yPosition = 0.0f;
        
        CGRect frame = firstView_.frame;
        frame.origin.y = yPosition;
        frame.size.height = [self heightForFirstView];
        firstView_.frame = frame;
        
        yPosition = CGRectGetMaxY(frame);
        
        frame = secondView_.frame;
        frame.origin.y = yPosition;
        frame.size.height = [self heightForSecondView];
        secondView_.frame = frame;
        
        yPosition = CGRectGetMaxY(frame);
        
        frame = thirdView_.frame;
        frame.origin.y = yPosition;
        frame.size.height = [self heightForThirdView];
        thirdView_.frame = frame;
        
        yPosition = CGRectGetMaxY(frame);
        
        frame = fourthView_.frame;
        frame.origin.y = yPosition;
        frame.size.height = [self heightForFourthView];
        fourthView_.frame = frame;
        
        yPosition = CGRectGetMaxY(frame) + 10.0f;
        
        frame = continueButton_.frame;
        frame.origin.y = yPosition - 30;
        continueButton_.frame = frame;
        
        yPosition = CGRectGetMaxY(frame);
        
        [self setScrollContentSize:CGSizeMake(320.0f, yPosition + 15.0f)];
        [self lookForEditViews];
    }
    
}

#pragma mark -
#pragma mark User interaction

/**
 * The continue button has been tapped
 */
- (IBAction)continueButtonTapped {
    
    [self saveContactInfoIntoHelper];
    [process_ startInscriptionDataRequest];

}



/*
 * Save contact information into the helper
 */
- (void)saveContactInfoIntoHelper {
    
    process_.selectedChannels = [[NSMutableArray alloc] init];
    
    for (int row = 0; row < [selectionTableSecondView_ numberOfRowsInSection: 0]; row++) {
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow: row inSection: 0];
        CheckChannelCell *channelCell = (CheckChannelCell *) [selectionTableSecondView_ cellForRowAtIndexPath:indexPath];
        if ([channelCell checkActive]) {
            [process_.selectedChannels addObject:[channelCell value]];
        }
    }
    
    [process_ setLargeAlias:[largeAliasTextField_ text]];
    [process_ setShortAlias:[shortAliasTextField_ text]];
    
    [process_ setNotificationEnabled:[activeNotificationCell_.notificationSwitch isOn]];
    
    if ([process_ notificationEnabled]) {
        
        NSInteger dayOfMonth = [[activeNotificationCell_.daysOfMonthArray objectAtIndex:[activeNotificationCell_.dayOfMonthComboButton selectedIndex]] intValue];
        [process_ setDayOfMonthNotification: dayOfMonth];
        
        [process_ setSendSMS:[[smsCell_ smsSwitch] isOn]];
        [process_ setSendEmail:[[emailCell_ emailSwitch] isOn]];
        
        if ([[smsCell_ smsSwitch] isOn]) {
            [process_ setSelectedPhoneNumberIndex: [smsCell_.smsComboButton selectedIndex]];
        }
        if ([[emailCell_ emailSwitch] isOn]) {
            [process_ setSelectedEmailIndex: [emailCell_.emailComboButton selectedIndex]];
        }

        
    } else {
    
        [process_ setDayOfMonthNotification: nil];
        
        [process_ setSendSMS:[[smsCell_ smsSwitch] isOn]];
        [process_ setSendEmail:[[emailCell_ emailSwitch] isOn]];
        
        [process_ setSelectedEmailIndex: -1];
        [process_ setSelectedPhoneNumberIndex: -1];
    }
    
}

/*
 * Save process information. By default does nothing. Each child must overrride the method if needed
 */
- (void)saveProcessInformation {
    
    
    
}

#pragma mark -
#pragma mark Views methods

/**
 * Returns the view needed in the first view. NIL if it is not needed
 */
- (void)viewForFirstView {

    firstView_ = nil;
    
}

/**
 * Returns the view needed in the second view. NIL if it is not needed
 */
- (void)viewForSecondView {
    
    CGFloat yPosition = 0.0f;
    
    CGRect frame = secondHeaderView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    secondHeaderView_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    frame = selectionTableSecondView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [selectionTableSecondView_ contentSize].height;
    selectionTableSecondView_.frame = frame;
    
    [selectionTableSecondView_ setScrollEnabled:NO];
    
    yPosition = CGRectGetMaxY(frame);
    
    secondViewHeight_ = yPosition;

}

/**
 * Returns the view needed in the third view. NIL if it is not needed
 */
- (void)viewForThirdView {
    
    CGFloat yPosition = 0.0f;
    
    CGRect frame = thirdHeaderView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    thirdHeaderView_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);
    
    //[selectionTableThirdView_ reloadData];
    
    frame = selectionTableThirdView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = 124.0f;
    selectionTableThirdView_.frame = frame;
    
    //[selectionTableThirdView_ setScrollEnabled:NO];
    
    yPosition = CGRectGetMaxY(frame);
    
    thirdViewHeight_ = yPosition;

}

/**
 * Returns the view needed in the third view. NIL if it is not needed
 */
- (void)viewForFourthView {
    
    CGFloat yPosition = 0.0f;
    
    CGRect frame = fourthHeaderView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    fourthHeaderView_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame);

    [selectionTableFourthView_ reloadData];
    
    frame = selectionTableFourthView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [selectionTableFourthView_ contentSize].height;
    selectionTableFourthView_.frame = frame;
    
    [selectionTableFourthView_ setScrollEnabled:NO];
    
    yPosition = CGRectGetMaxY(frame) + 30.0f;
        
    fourthViewHeight_ = yPosition;

}

/**
 * Returns the height for the first view. 0 if it is NIL
 */
- (CGFloat)heightForFirstView {

    return 0.0f;

}

/**
 * Returns the height for the second view. 0 if it is NIL
 */
- (CGFloat)heightForSecondView {

    return secondViewHeight_;

}

/**
 * Returns the height for the third view. 0 if it is NIL
 */
- (CGFloat)heightForThirdView {
    
    return thirdViewHeight_;
    
}

/**
 * Returns the height for the third view. 0 if it is NIL
 */
- (CGFloat)heightForFourthView {
    
    return fourthViewHeight_;
    
}

#pragma mark -
#pragma mark Setters

/*
 * Sets the process
 */
- (void)setProcess:(FOInscriptionBaseProcess *)process {

    if (process_ != process) {
        
        [process retain];
        [process_ release];
        process_ = process;
        
    }

    [process_ setDelegate:self];
    
}

#pragma mark -
#pragma mark UITableView methods

/**
 * Tells the data source to return the number of rows in a given section of a table view. (required)
 *
 * @param tableView: The table-view object requesting this information.
 * @param section: An index number identifying a section in tableView.
 * @return The number of rows in section.
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger numberOfRows = 0;
    if (tableView == selectionTableSecondView_) {
        numberOfRows = [process_.channelArray count];
    }
    if (tableView == selectionTableFourthView_) {
        if ([process_ notificationEnabled]) {
            numberOfRows = 3;
        } else {
            numberOfRows = 1;
        }
        
    }
    return numberOfRows;
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. (required)
 *
 * @param tableView: A table-view object requesting the cell.
 * @param indexPath: An index path locating a row in tableView.
 * @return An object inheriting from UITableViewCellthat the table view can use for the specified row.
 */
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (tableView == selectionTableSecondView_) {
        
        CheckChannelCell *checkChannelCell = [[[CheckChannelCell checkChannelCell] retain] autorelease];
        
        Channel *channel = [process_.channelArray objectAtIndex:indexPath.row];
        
        [checkChannelCell setValue: channel.code];
        [[checkChannelCell  titleTextLabel] setText: [channel description]];
        
        [checkChannelCell setShowSeparator:YES];
        
        [checkChannelCell setMultipleTouchEnabled:TRUE];
        
        return checkChannelCell;
    }
    if (tableView == selectionTableFourthView_) {
        
        if (indexPath.row == 0) {
            
            NotificationSwitchCell *result = (NotificationSwitchCell *) [tableView dequeueReusableCellWithIdentifier:[NotificationSwitchCell cellIdentifier]];
            
            if (result == nil) {
                
                if (activeNotificationCell_ == nil) {
                    activeNotificationCell_ = [[NotificationSwitchCell notificationCell] retain];
                    activeNotificationCell_.dayOfMonthComboButton.hidden=YES;
                    
                    [activeNotificationCell_ setDelegate:self];
                    [activeNotificationCell_.dayOfMonthComboButton setStringListSelectionButtonDelegate:self];
                    [NXT_Peru_iPhoneStyler styleAccountSelectionButton:[activeNotificationCell_ dayOfMonthComboButton]];
                    [activeNotificationCell_ setDaysOfMonthArray:[self daysOfMonthArray]];
                }
                result = activeNotificationCell_;
            }
            return result;
            
            
        } else if (indexPath.row == 1) {
            
            
            if(process_.phoneNumberArray==nil||[process_.phoneNumberArray count]==0){
                NXTTableCell *result = (NXTTableCell *)[tableView dequeueReusableCellWithIdentifier:[NXTTableCell cellIdentifier]];
                
                if (result == nil) {
                    result = [NXTTableCell NXTTableCell];
                }
                result.backgroundColor = [UIColor whiteColor];
                

                result.leftText=process_.phoneDisclaimer;
                CGRect framecell=result.leftTextLabel.frame;
                framecell.size.width=295;
                
                result.leftTextLabel.frame=framecell;
                result.leftTextLabel.numberOfLines=2;
                result.leftTextLabel.textAlignment = UITextAlignmentLeft;
                result.rightTextLabel.textAlignment = UITextAlignmentRight;
                
                [NXT_Peru_iPhoneStyler styleLabel:result.leftTextLabel withFontSize:12.0f color:[UIColor blackColor]];
                
                return result;
                
            }
            
            
                FOSMSMokCell *result = (FOSMSMokCell *)[tableView dequeueReusableCellWithIdentifier:[FOSMSMokCell cellIdentifier]];
                
                if (result == nil) {
                    
                    if (smsCell_ == nil) {
                        
                        smsCell_ = [[FOSMSMokCell FOSMSMokCell] retain];
                        
                        NSMutableArray *array = [[NSMutableArray alloc] init];
                        for (FOPhone *phone in process_.phoneNumberArray) {
                            [array addObject: [phone phoneNumber]];
                        }
                        [smsCell_ setSmsArray: array];
                    }
                    
                    result = smsCell_;
                }
                
                result.delegate = self;
                result.showSeparator = NO;
                
                [result.titleLabel setText:NSLocalizedString(NOTIFICATIONS_TABLE_SEND_PHONE_TEXT_KEY, nil)];
            [result.headerLabel setText: NSLocalizedString(NOTIFICATION_TYPE_TEXT_KEY, nil)];
            
                result.backgroundColor = [UIColor clearColor];
                
                //[result setConfigurationForFirstAddOn:[process_ showPhoneNumberCombo]];
            [result setConfigurationForFirstAddOn: [process_ sendSMS]];
            
            [self lookForEditViews];
            
                return result;
            
        } else {
            
            
            if(process_.emailArray==nil||[process_.emailArray count]==0){
                NXTTableCell *result = (NXTTableCell *)[tableView dequeueReusableCellWithIdentifier:[NXTTableCell cellIdentifier]];
                
                if (result == nil) {
                    result = [NXTTableCell NXTTableCell];
                }
                result.backgroundColor = [UIColor whiteColor];
                
                
                result.leftText=process_.emailDisclaimer;
                CGRect framecell=result.leftTextLabel.frame;
                framecell.size.width=295;
                
                result.leftTextLabel.frame=framecell;
                result.leftTextLabel.numberOfLines=2;
                result.leftTextLabel.textAlignment = UITextAlignmentLeft;
                result.rightTextLabel.textAlignment = UITextAlignmentRight;
                
                [NXT_Peru_iPhoneStyler styleLabel:result.leftTextLabel withFontSize:12.0f color:[UIColor blackColor]];
                
                return result;
                
            }
            
            
                
                FOEmailMokCell *result = (FOEmailMokCell *)[tableView dequeueReusableCellWithIdentifier:[FOEmailMokCell cellIdentifier]];
                
                if (result == nil) {
                    
                    if (emailCell_ == nil) {
                        
                        emailCell_ = [[FOEmailMokCell FOEmailMokCell] retain];
                        
                        NSMutableArray *array = [[NSMutableArray alloc] init];
                        
                        for(FOEmail *email in process_.emailArray){
                        
                            [array addObject:email.email];
                        }
                        
                        [emailCell_ setEmailArray: array];
                        
                    }
                    result = emailCell_;
                }
                
                [result setDelegate:self];
                result.showSeparator = YES;
                
                [result.titleLabel setText:NSLocalizedString(NOTIFICATIONS_TABLE_SEND_EMAIL_TEXT_KEY, nil)];
                
                result.backgroundColor = [UIColor clearColor];
                
                [result setConfigurationForFirstAddOn:[process_ sendEmail]];
                
                [self lookForEditViews];
                
                return result;
            
            
        }
        
    } else {
        
        return nil;
        
    }
        
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 *
 * @param tableView: The table-view object requesting this information.
 * @param indexPath: An index path that locates a row in tableView.
 * @return A floating-point value that specifies the height (in points) that row should be.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat result = 0.0f;

    if (tableView == selectionTableSecondView_) {
        
        result = [CheckChannelCell cellHeightForFirstAddOn:YES secondAddOn:NO];
        
    } else if (tableView == selectionTableFourthView_){
        
        if (indexPath.row == 0) {
            
            if ([process_ notificationEnabled]) {
                
                result =120.f;
                
            } else {
                
                result = 70.0f;
                
            }
            
        } else if (indexPath.row == 1) {
            
            //result = [FOSMSMokCell cellHeightForFirstAddOn:[process_ showPhoneNumberCombo]];
            
            result =([process_.phoneNumberArray count]==0)?40:[FOSMSMokCell cellHeightForFirstAddOn:[process_ sendSMS]];
        } else {
            //result = [FOEmailMokCell cellHeightForFirstAddOn:[process_ showEmailCombo]];
            result =([process_.emailArray count]==0)?40: [FOEmailMokCell cellHeightForFirstAddOn:[process_ sendEmail]];
            
        }
    }
    

    return result;
}

/**
 * Tells the delegate that the specified row is now selected.
 *
 * @param tableView: A table-view object informing the delegate about the new row selection.
 * @param indexPath: An index path locating the new selected row in tableView.
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == selectionTableSecondView_) {
        
        CheckChannelCell *checkChannel = (CheckChannelCell *)[tableView cellForRowAtIndexPath:indexPath];
        [checkChannel setCheckActive:![checkChannel checkActive]];
        
        activeCellIndex_ = indexPath.row;
    }
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    CheckChannelCell *checkChannel = (CheckChannelCell *)[tableView cellForRowAtIndexPath: indexPath];
    [checkChannel setCheckActive:![checkChannel checkActive]];
}

#pragma mark
#pragma mark Frequent Operation Inscription Delegate
- (void) confirmationAnalysisHasFinished {

    frequentOperationInscriptionConfirmationViewController_ = [FrequentOperationInscriptionConfirmationViewController frequentOperationInscriptionConfirmationViewController];
    [frequentOperationInscriptionConfirmationViewController_ setProcess: process_];
    
    hasNavigateForward_ = YES;
    [self.navigationController pushViewController: frequentOperationInscriptionConfirmationViewController_ animated:YES];
    
}


#pragma mark
#pragma mark UITextField Delegate

/** 
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    /*NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];*/
    
    return result;
    
}


#pragma mark -
#pragma mark UITextView Delegate

/**
 * Asks the delegate whether the specified text should be replaced in the text view.
 *
 * @param textView The text view containing the changes.
 * @param range The current selection range.
 * @param text The text to insert.
 */
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    BOOL result = YES;
    
   /* NSString *resultString = [textView.text stringByReplacingCharactersInRange:range
                                                                    withString:text];*/
   
    
    return result;
    
}

#pragma mark
#pragma mark MOKStringListSelectionButton Delegate

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton {

    //NSLog(@"selectedIndex : %i",[stringListSelectionButton selectedIndex]);
    
    if (stringListSelectionButton == [activeNotificationCell_ dayOfMonthComboButton]){
        
        [activeNotificationCell_.dayOfMonthComboButton setSelectedIndex: [stringListSelectionButton selectedIndex]];
        
    }

}

/**
 * Switch has been tapped.
 */
- (void) switchButtonHasBeenTapped:(BOOL)on {

    [self saveContactInfoIntoHelper];
    
    [process_ setNotificationEnabled: on];
    
    
    if (!on) {
        
        [[smsCell_ smsSwitch] setOn: NO];
        [[emailCell_ emailSwitch] setOn: NO];
        activeNotificationCell_.dayOfMonthComboButton.hidden=YES;
        //[process_ setShowEmailCombo: NO];
        //[process_ setShowPhoneNumberCombo:NO];
        
        [process_ setSelectedEmailIndex:-1];
        [process_ setSelectedPhoneNumberIndex:-1];
        [activeNotificationCell_.dayOfMonthComboButton setSelectedIndex:0];
        

        [selectionTableFourthView_ bringSubviewToFront:emailCell_];
        
    }
    else {
        
        [[smsCell_ smsSwitch] setOn: NO];
        [[emailCell_ emailSwitch] setOn: NO];
        activeNotificationCell_.dayOfMonthComboButton.hidden=NO;
        //[process_ setShowEmailCombo: NO];
        //[process_ setShowPhoneNumberCombo: NO];
        
        [process_ setSelectedEmailIndex:-1];
        [process_ setSelectedPhoneNumberIndex:-1];        
        [selectionTableFourthView_ bringSubviewToFront:emailCell_];
        
    }
    
    
	[self lookForEditViews];
    [self viewForSecondView];
    [self viewForThirdView];
    [self viewForFourthView];
    [self relocateViews];
    
}


#pragma mark AliasFOCell delegate methods

- (void) suggestionLinkButtonTapped {
    
    NSString *newString = [[process_ suggestion] stringByReplacingOccurrencesOfString: @"\\n" withString:@"\r"];
    UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(ALERT_MESSAGE_TITLE_KEY, nil) message:newString delegate:nil cancelButtonTitle:NSLocalizedString(OK_TEXT_KEY, nil) otherButtonTitles:nil] autorelease];
    [alertView show];
    ((UILabel*)[[alertView subviews] objectAtIndex:2]).textAlignment = UITextAlignmentLeft;

}

#pragma mark-


#pragma mark -
#pragma mark SMSCell Delegate

- (void)smsSwitchButtonHasBeenTapped:(BOOL)on {
    
    [self saveContactInfoIntoHelper];
    
    [process_ setSendSMS:on];
    //[process_ setShowPhoneNumberCombo: on];
    
    if (on) {
        
        NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
        NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
        
        [selectionTableFourthView_ reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [array release];
    }
    
    
    [self viewForFourthView];
    [self relocateViews];
    [self lookForEditViews];

}

#pragma mark -
#pragma mark EmailCell Delegate

/**
 * Switch has been tapped.
 *
 * @param on The flag
 */
- (void)emailSwitchButtonHasBeenTapped:(BOOL)on {
    
    [self saveContactInfoIntoHelper];
    
    [process_ setSendEmail:on];
    //[process_ setShowEmailCombo:on];
    
    if (on) {
        
        NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
        NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
        
        [selectionTableFourthView_ reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [array release];
    }
    
    
    [self viewForFourthView];
    [self relocateViews];  
    [self lookForEditViews];

}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    NSString *title = @"";
    
    /*switch ([process_ paymentOperationType]) {
        
        case PTEPaymentPSElectricServices:
        case PTEPaymentPSWaterServices:
        case PTEPaymentPSCellular:
        case PTEPaymentPSPhone:
            
            title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY, nil)];
            
            break;
            
        case PTEPaymentRecharge:
            
            title = NSLocalizedString(PAYMENT_RECHARGE_OPERATION_TEXT_KEY, nil);
            
            break;
            
        case PTEPaymentContOwnCard:
        case PTEPaymentContThirdCard:
        case PTEPaymentOtherBank:
            
            title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_CARDS_TITLE_TEXT_KEY, nil)];
            
            break;

        default:
            break;
    }*/
    

    title = NSLocalizedString(FO_TITLE_TEXT_REACTIVE_KEY, nil);
    
    result.mainTitle = title;
    
    return result;
    
}

- (NSArray *) daysOfMonthArray {

    NSMutableArray *daysOfMonthMutableArray = [[NSMutableArray alloc] init];
    for (int i = 1; i < 32 ; i++) {
        [daysOfMonthMutableArray addObject:[NSString stringWithFormat:@"%i",i]];
    }
    return daysOfMonthMutableArray;
}

@end
