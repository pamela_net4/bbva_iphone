//
//  FrequentOperationInscriptionConfirmationViewController.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 19/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationInscriptionConfirmationViewController.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "UIColor+BBVA_Colors.h"
#import "Constants.h"
#import "MOKEditableViewController+protected.h"
#import "Tools.h"
#import "Session.h"
#import "GlobalAdditionalInformation.h"
#import "InformationCell.h"
#import "UINavigationItem+DoubleLabel.h"
#import "TitleAndAttributes.h"
#import "FrequentOperationInscriptionSuccessViewController.h"

/**
 * Defines the interior view distance
 */
#define INTERIOR_VIEW_DISTANCE                      5.0f

/**
 * Defines the exterior view distance
 */
#define EXTERIOR_VIEW_DISTANCE                      10.0f

/**
 * Defines the text font size
 */
#define TEXT_FONT_SIZE                              14.0f

/**
 * Defines the small text font size
 */
#define SMALL_TEXT_FONT_SIZE                        12.0f

/**
 * Defines the big text font size
 */
#define BIG_TEXT_FONT_SIZE                          17.0f

/**
 * Define the operation key maximum lenght
 */
#define OTP_MAXIMUM_LENGHT                                              6


#define NIB_FILE_NAME @"FOInscriptionConfirmationViewController"

@interface FrequentOperationInscriptionConfirmationViewController()

- (void) releaseFrequentOperationInscriptionConfirmationViewControllerGraphicElements;

- (void)relocateViews;

- (void)updateInformation;

@end

@implementation FrequentOperationInscriptionConfirmationViewController

@synthesize tableBgImageView = tableBgImageView_;
@synthesize infoTable = infoTable_;
@synthesize otpKeyView = otpKeyView_;
@synthesize otpKeyLabel = otpKeyLabel_;
@synthesize otpKeyTextField = otpKeyTextField_;
@synthesize coordKeyView = coordKeyView_;
@synthesize coordKeyTitleLabel = coordKeyTitleLabel_;
@synthesize coordKeyBgImageView = coordKeyBgImageView_;
@synthesize coordKeySubtitleLabel = coordKeySubtitleLabel_;
@synthesize coordKeyabel = coordKeyLabel_;
@synthesize coordKeyTextField = coordKeyTextField_;
@synthesize sealView = sealView_;
@synthesize sealTitleLabel = sealTitleLabel_;
@synthesize sealBgImageView = sealBgImageView_;
@synthesize sealSubtitleLabel = sealSubtitleLabel_;
@synthesize sealImageView = sealImageView_;
@synthesize continueButton = continueButton_;
@synthesize brandingLine = brandingLine_;
@synthesize process = process_;
@synthesize extraInfoView = extraInfoView_;
@synthesize topSeparator = topSeparator_;
@synthesize bottomSeparator = bottomSeparator_;
@synthesize extraInfoLabel = extraInfoLabel_;

- (void)dealloc
{
    [self releaseFrequentOperationInscriptionConfirmationViewControllerGraphicElements];
    
    [super dealloc];
}

-(void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseFrequentOperationInscriptionConfirmationViewControllerGraphicElements];
        [self setView:nil];
        
    }
}

-(void)releaseFrequentOperationInscriptionConfirmationViewControllerGraphicElements {
    
	[headerView_ release];
    headerView_ = nil;
    
    [tableBgImageView_ release];
    tableBgImageView_ = nil;
    
    [infoTable_ release];
    infoTable_ = nil;
    
    [otpKeyView_ release];
    otpKeyView_ = nil;
    
    [otpKeyLabel_ release];
    otpKeyLabel_ = nil;
    
    [otpKeyTextField_ release];
    otpKeyTextField_ = nil;
    
    [coordKeyView_ release];
    coordKeyView_ = nil;
    
    [coordKeyTitleLabel_ release];
    coordKeyTitleLabel_ = nil;
    
    [coordKeyBgImageView_ release];
    coordKeyBgImageView_ = nil;
    
    [coordKeySubtitleLabel_ release];
    coordKeySubtitleLabel_ = nil;
    
    [coordKeyLabel_ release];
    coordKeyLabel_ = nil;
    
    [coordKeyTextField_ release];
    coordKeyTextField_ = nil;
    
    [sealView_ release];
    sealView_ = nil;
    
    [sealTitleLabel_ release];
    sealTitleLabel_ = nil;
    
    [sealBgImageView_ release];
    sealBgImageView_ = nil;
    
    [sealSubtitleLabel_ release];
    sealSubtitleLabel_ = nil;
    
    [sealImageView_ release];
    sealImageView_ = nil;
    
    [continueButton_ release];
    continueButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
    [extraInfoView_ release];
    extraInfoView_ = nil;
    
    [topSeparator_ release];
    topSeparator_ = nil;
    
    [bottomSeparator_ release];
    bottomSeparator_ = nil;
    
    [extraInfoLabel_ release];
    extraInfoLabel_ = nil;
}

-(void)awakeFromNib{

    [super awakeFromNib];

}

-(void)viewDidLoad{

    [super viewDidLoad];
    
    [NXT_Peru_iPhoneStyler styleNXTView: [self view]];
    
    [sealView_ setBackgroundColor: [UIColor clearColor]];
    
    headerView_ = [[SimpleHeaderView simpleHeaderView] retain];
    [self.scrollableView addSubview: headerView_];
    
    [NXT_Peru_iPhoneStyler styleBlueButton: continueButton_];
    [continueButton_ setTitle:NSLocalizedString(OK_TEXT_KEY, nil) forState:UIControlStateNormal];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed: BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    [tableBgImageView_ setImage:[[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME]];
    
    [sealBgImageView_ setImage:[[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME]];
    [coordKeyBgImageView_ setImage:[[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME]];

    [NXT_Peru_iPhoneStyler styleLabel:otpKeyLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:coordKeyTitleLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:sealTitleLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:coordKeySubtitleLabel_ withFontSize:SMALL_TEXT_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:sealSubtitleLabel_ withFontSize:SMALL_TEXT_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:coordKeyLabel_ withFontSize:BIG_TEXT_FONT_SIZE color:[UIColor blackColor]];
    [NXT_Peru_iPhoneStyler styleMokTextField:coordKeyTextField_ withFontSize:TEXT_FONT_SIZE andColor:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleMokTextField:otpKeyTextField_ withFontSize:TEXT_FONT_SIZE andColor:[UIColor grayColor]];
    [otpKeyTextField_ setTextAlignment:UITextAlignmentLeft];
    
    [coordKeySubtitleLabel_ setNumberOfLines:2];
    
    
    [otpKeyLabel_ setText:NSLocalizedString(PUBLIC_SERVICE_OTPKEY_TEXT_KEY, nil)];
    [otpKeyTextField_ setPlaceholder:@""];
    
    [coordKeyTitleLabel_ setText:NSLocalizedString(COORDINATE_REQUEST_HEADER_TEXT_KEY, nil)];
    [coordKeySubtitleLabel_ setText:NSLocalizedString(COORDINATE_REQUEST_INFO_TEXT_KEY, nil)];
    
    [sealTitleLabel_ setText:NSLocalizedString(TRANSFER_STEP_TWO_SEAL_TEXT_KEY, nil)];
    [sealSubtitleLabel_ setText:NSLocalizedString(SHARE_SEAL_TEXT_KEY, nil)];

    [otpKeyView_ setBackgroundColor:[UIColor clearColor]];
    [coordKeyView_ setBackgroundColor:[UIColor clearColor]];
    [sealView_ setBackgroundColor:[UIColor clearColor]];
    [infoTable_ setBackgroundColor:[UIColor clearColor]];
    [extraInfoView_ setBackgroundColor:[UIColor clearColor]];
    [extraInfoLabel_ setNumberOfLines:4];
    [extraInfoLabel_ setText:NSLocalizedString(CARD_PAYMENT_CONFIRMATION_BOTTOM_TEXT_KEY, nil)];
    [NXT_Peru_iPhoneStyler styleLabel:extraInfoLabel_ withFontSize:11.0f color:[UIColor grayColor]];
    [extraInfoLabel_ setTextAlignment:UITextAlignmentCenter];
    
    [otpKeyTextField_ setMaxLength:OTP_MAXIMUM_LENGHT];
    [coordKeyTextField_ setMaxLength:COORDINATES_MAXIMUM_LENGHT];
    
}

-(void)viewDidUnload{

    [super viewDidUnload];
    [self releaseFrequentOperationInscriptionConfirmationViewControllerGraphicElements];

}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self resetScrollToTopLeftAnimated:NO];
    
    [self updateInformation];
    [self relocateViews];
    [self lookForEditViews];

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]);
    [self setScrollFrame:frame];
    
}

+ (FrequentOperationInscriptionConfirmationViewController *) frequentOperationInscriptionConfirmationViewController {

    FrequentOperationInscriptionConfirmationViewController *result = [[[FrequentOperationInscriptionConfirmationViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];

    [result awakeFromNib];
    
    return result;
}

- (void)updateInformation {
    
    NSString *title = @"";
    NSString *headerTitle = @"";
    
    title = NSLocalizedString(FO_TITLE_TEXT_REACTIVE_KEY, nil);
    headerTitle = NSLocalizedString(FO_AFFILIATION_TITLE_TEXT_KEY, nil);
    
    [headerView_ setTitle:headerTitle];
    
    [coordKeyTextField_ setText:@""];
    [otpKeyTextField_ setText:@""];
    
    [coordKeyLabel_ setText:[process_ coordHint]];
    
    [infoTable_ reloadData];
    
	NSData *data = [Tools base64DataFromString:[process_ seal]];
	UIImage *image = [[[UIImage alloc] initWithData:data] autorelease];
	[sealImageView_ setImage:image];
    
}

- (void)relocateViews {
    
    CGFloat yPosition = 0.0f;
    
    CGRect frame = headerView_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [SimpleHeaderView height];
    headerView_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
    
    [infoTable_ reloadData];
    
    frame = infoTable_.frame;
    frame.origin.y = yPosition;
    frame.size.height = [infoTable_ contentSize].height;
    infoTable_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
    
    frame = tableBgImageView_.frame;
    frame.origin.y = infoTable_.frame.origin.y + 3.0f;
    frame.size.height = [infoTable_ contentSize].height - 3.0f;
    tableBgImageView_.frame = frame;
    
   // if ([process_ frequentOperationType] == FOTETransferToGiftCard) {
        
        [coordKeyView_ setHidden:YES];
        [otpKeyLabel_ setHidden:YES];
        [otpKeyTextField_ setHidden:YES];
        
        if ([process_ typeOTPKey]) {
            
            [coordKeyView_ setHidden:YES];
            [otpKeyView_ setHidden:NO];
            [otpKeyLabel_ setHidden:NO];
            [otpKeyTextField_ setHidden:NO];
            
            frame = [otpKeyView_ frame];
            frame.origin.y = yPosition;
            [otpKeyView_ setFrame:frame];

            
            yPosition = CGRectGetMaxY(frame) + INTERIOR_VIEW_DISTANCE;
            
        } else {
            
            [otpKeyView_ setHidden:YES];
            [coordKeyView_ setHidden:NO];
            [coordKeyLabel_ setHidden:NO];
            
            frame = coordKeyView_.frame;
            frame.origin.y = yPosition;
            coordKeyView_.frame = frame;
            
            yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
            
        }
        
    //}
    
    if (([process_ seal] == nil) || ([[process_ seal] isEqualToString:@""])) {
        
        [sealView_ setHidden:YES];
        
    } else {
        
        [sealView_ setHidden:NO];
        
        frame = sealView_.frame;
        frame.origin.y = yPosition;
        sealView_.frame = frame;
        
        yPosition = CGRectGetMaxY(frame) + EXTERIOR_VIEW_DISTANCE;
        
    }
    
    frame = continueButton_.frame;
    frame.origin.y = yPosition;
    continueButton_.frame = frame;
    
    yPosition = CGRectGetMaxY(frame) + 10.0f;
    
    [extraInfoView_ setHidden:YES];
    
    [self setScrollContentSize:CGSizeMake(320.0f, yPosition)];
    
}


- (IBAction)continueButtonTapped {
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    NSString *secondFactorKey = @"";
    
    if (otpUsage == otp_UsageOTP) {
        
        secondFactorKey = [otpKeyTextField_ text];
        
    } else if (otpUsage == otp_UsageTC) {
        
        secondFactorKey = [coordKeyTextField_ text];
        
    }
    
    [process_ setSecondFactorKey:secondFactorKey];
    
    [process_ startInscriptionSuccessRequest];
    
    
}


- (void)setProcess:(FOInscriptionBaseProcess *)process {
    
    if (process_ != process) {
        
        [process retain];
        [process_ release];
        process_ = process;
        
    }
    
    [process_ setDelegate:self];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[process_ confirmationInfoArray] count];
    
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    InformationCell *result = (InformationCell *)[tableView dequeueReusableCellWithIdentifier:[InformationCell cellIdentifier]];
    
    if (result == nil) {
        
        result = [[[InformationCell informationCell] retain] autorelease];
        
    }
    
    TitleAndAttributes *titleAndAttribute = [[process_ confirmationInfoArray] objectAtIndex:[indexPath row]];
    [result setInfo:titleAndAttribute];
    
    return result;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TitleAndAttributes *titleAndAttributes = (TitleAndAttributes*) [[process_ confirmationInfoArray] objectAtIndex: indexPath.row];
    CGFloat result = [InformationCell cellHeight];
    int resultCount = [[titleAndAttributes attributesArray] count];
    result += (20.0f * (resultCount - 1));
    if(resultCount==1)
    {
        CGSize recognizeWith = [[[titleAndAttributes attributesArray] objectAtIndex:0] sizeWithFont:[NXT_Peru_iPhoneStyler normalFontWithSize:TEXT_FONT_SIZE] constrainedToSize:CGSizeMake(9999, 17)];
        result+= (recognizeWith.width>268)?20.0f:0.0f;
    }
    
    return result;
    
}

- (void)successAnalysisHasFinished {
    
    FrequentOperationInscriptionSuccessViewController *controller = [[FrequentOperationInscriptionSuccessViewController frequentOperationInscriptionSuccessViewController] retain];
    
    [controller setProcess:process_];
    
    [[self navigationController] pushViewController: controller animated:YES];
    
}

- (void)successAnalysisHasFinishedWithError{
    
    [[self navigationController] popViewControllerAnimated:YES];
    
}

- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    NSString *title = NSLocalizedString(FO_TITLE_TEXT_REACTIVE_KEY, nil);
    /*
    switch ([process_ frequentOperationType]) {
           
        case FOTETransferToGiftCard:
            
            title = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(INTERNET_SHOPPING_TITLE_TEXT_KEY, nil)];
            
        default:
            break;
    }*/
    
    result.mainTitle = title;
    
    return result;
    
}

@end
