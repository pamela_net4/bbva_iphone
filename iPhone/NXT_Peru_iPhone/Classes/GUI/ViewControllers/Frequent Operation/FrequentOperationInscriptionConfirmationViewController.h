//
//  FrequentOperationInscriptionConfirmationViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 19/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MOKEditableViewController.h"
#import "FOInscriptionBaseProcess.h"
#import "MOKLimitedLengthTextField.h"

@class SimpleHeaderView;

@interface FrequentOperationInscriptionConfirmationViewController : MOKEditableViewController <UITableViewDataSource, UITableViewDelegate, FOInscriptionBaseProcessDelegate, UITextFieldDelegate, UITextViewDelegate> {

@private
    
    SimpleHeaderView *headerView_;
    
    UIImageView *tableBgImageView_;
    
    UITableView *infoTable_;
    
    
    /**
     * OTP key view
     */
    UIView *otpKeyView_;
    
    /**
     * OTP key label
     */
    UILabel *otpKeyLabel_;
    
    /**
     * OTP key textfield
     */
    MOKLimitedLengthTextField *otpKeyTextField_;
    
    /**
     * Coord key View
     */
    UIView *coordKeyView_;
    
    /**
     * Coord title key label
     */
    UILabel *coordKeyTitleLabel_;
    
    /**
     * Coord key bg image view
     */
    UIImageView *coordKeyBgImageView_;
    
    /**
     * Coord subtitle key label
     */
    UILabel *coordKeySubtitleLabel_;
    
    /**
     * Coord key label
     */
    UILabel *coordKeyLabel_;
    
    /**
     * Operation key textfield
     */
    MOKLimitedLengthTextField *coordKeyTextField_;
    
    /**
     * Seal View
     */
    UIView *sealView_;
    
    /**
     * Seal title label
     */
    UILabel *sealTitleLabel_;
    
    /**
     * Seal bg image view
     */
    UIImageView *sealBgImageView_;
    
    /**
     * Seal subtitle label
     */
    UILabel *sealSubtitleLabel_;
    
    /**
     * Seal image view
     */
    UIImageView *sealImageView_;
    
    /**
     * Continue button
     */
    UIButton *continueButton_;
    
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    
    /**
     * Top separator
     */
    UIImageView *topSeparator_;
    
    /**
     * Bottom separator
     */
    UIImageView *bottomSeparator_;
    
    
    FOInscriptionBaseProcess *process_;

}

@property (nonatomic,readwrite, retain) IBOutlet UILabel *titleLabel;

@property (nonatomic,readwrite, retain) IBOutlet UIImageView *tableBgImageView;

@property (nonatomic,readwrite, retain) IBOutlet UITableView *infoTable;

@property (nonatomic, readwrite, retain) IBOutlet UIView *otpKeyView;

@property (nonatomic,readwrite, retain) IBOutlet UILabel *otpKeyLabel;

@property (nonatomic,readwrite, retain) IBOutlet MOKLimitedLengthTextField *otpKeyTextField;

@property (nonatomic,readwrite, retain) IBOutlet UIView *coordKeyView;

@property (nonatomic,readwrite, retain) IBOutlet UILabel *coordKeyTitleLabel;

@property (nonatomic,readwrite, retain) IBOutlet UIImageView *coordKeyBgImageView;

@property (nonatomic,readwrite, retain) IBOutlet UILabel *coordKeySubtitleLabel;

@property (nonatomic,readwrite, retain) IBOutlet UILabel *coordKeyabel;

@property (nonatomic,readwrite, retain) IBOutlet MOKLimitedLengthTextField *coordKeyTextField;

@property (nonatomic,readwrite, retain) IBOutlet UIView *sealView;

@property (nonatomic,readwrite, retain) IBOutlet UILabel *sealTitleLabel;

@property (nonatomic,readwrite, retain) IBOutlet UIImageView *sealBgImageView;

@property (nonatomic,readwrite, retain) IBOutlet UILabel *sealSubtitleLabel;

@property (nonatomic,readwrite, retain) IBOutlet UIImageView *sealImageView;

@property (nonatomic,readwrite, retain) IBOutlet UIButton *continueButton;

@property (nonatomic,readwrite, retain) IBOutlet UIImageView *brandingLine;

@property (nonatomic,readwrite, retain) IBOutlet UIView *extraInfoView;

@property (nonatomic,readwrite, retain) IBOutlet UIImageView *topSeparator;

@property (nonatomic,readwrite, retain) IBOutlet UIImageView *bottomSeparator;

@property (nonatomic,readwrite, retain) IBOutlet UILabel *extraInfoLabel;

@property (nonatomic,readwrite, retain) FOInscriptionBaseProcess *process;

+ (FrequentOperationInscriptionConfirmationViewController *) frequentOperationInscriptionConfirmationViewController;

- (IBAction)continueButtonTapped;

@end
