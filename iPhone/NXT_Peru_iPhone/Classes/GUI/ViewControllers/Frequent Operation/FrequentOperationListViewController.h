//
//  FrequentOperationListViewController.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/5/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "NXTViewController.h"
#import "PaymentPSProcess.h"

@class FOOperationHelper;
@class FOEInstitutionPaymentStepOneResponse;
@class FOPGeneralMenuStepOneResponse;
@class FOEServicePaymentStepOneResponse;
@class PaymentPSProcess;
@class PublicServStepThreeViewController;
@interface FrequentOperationListViewController : NXTViewController <UITableViewDataSource, UITableViewDelegate, PaymentPSProcessDelegate, PaymentBaseProcessDelegate>{
    
    /**
    * execute button to start the request operation
    */
    UIButton *executeButton_;
    
    /**
     * List with all the frequents operation
     */
    UITableView *frequentOperationTable_;
    
    /**
     * Indicator to know if the user has been in this view and then goes forward
     */
    BOOL hasForward_;
    
    /**
     * Page to use for the pagination
     */
    NSInteger page_;
    
    /**
     * Page to use for the pagination
     */
    NSInteger selectedIndex_;
    
    /*
     * Array with the frequent operations
     */
    NSArray *frequentOperationArray_;
    
    /*
     * Operation Helper
     */
    FOOperationHelper *foOperationHelper;
    
    /*
     * Operation Helper
     */
    FOEInstitutionPaymentStepOneResponse *foeInstResponse_;
    
    /*
     * initial Response
     */
    FOPGeneralMenuStepOneResponse *initialResponse;
    
    /*
     * foe service payment response
     */
    FOEServicePaymentStepOneResponse *foeServiceResponse;
    
    /*
     *  payment process
     */
    PaymentPSProcess *paymentProcess_;
    
    /*
     * Payment next view
     */
    PublicServStepThreeViewController *publicServStepThreeViewController_;
}

/**
 * Creates and returns an autoreleased FrequentOperationListViewController constructed from a NIB file.
 *
 * @return The autoreleased FrequentOperationListViewController constructed from a NIB file.
 */
+ (FrequentOperationListViewController *)frequentOperationListViewController;

/**
 * Provides readwrite access to the execution button. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *executeButton;

/**
 * Provides readwrite access to the frequent opertaions table. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *frequentOperationTable;

/**
 * Provides readonly access to the frequent operations array
 */
@property (nonatomic, readonly, retain) NSArray *frequentOperationArray;

/**
 * Provides readonlywrite access to the flag that indicates if the user has been forward
 */
@property (nonatomic, readwrite, assign) BOOL hasForward;

/**
 * When the user tap the execute button
 */
- (IBAction)onTapExecute:(id)sender;

@end
