//
//  FrequentOperationInscriptionSuccessViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 21/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MOKEditableViewController.h"
#import "FOInscriptionBaseProcess.h"


@class SimpleHeaderView;


@interface FrequentOperationInscriptionSuccessViewController : MOKEditableViewController <UITableViewDataSource, UITableViewDelegate, FOInscriptionBaseProcessDelegate> {

@private
    
    SimpleHeaderView *headerView_;
    
    UIImageView *tableBgImageView_;
    
    UITableView *infoTable_;
    
    UIButton *backToOperationButton_;
    
    UIImageView *brandingLine_;
    
    UILabel *extraInfoLabel_;
    
    UIImageView *bottomSeparator_;
    
    FOInscriptionBaseProcess *process_;

}

@property (nonatomic, readwrite, retain) IBOutlet UITableView *infoTable;

@property (nonatomic, readwrite, retain) IBOutlet UIButton *backToOperationButton;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingLine;

@property (nonatomic, readwrite, retain) IBOutlet  UIImageView *tableBgImageView;

@property (nonatomic, readwrite, retain) IBOutlet UIView *extraInfoView;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *extraInfoLabel;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView *bottomSeparator;

@property (nonatomic, readwrite, retain) FOInscriptionBaseProcess *process;


+ (FrequentOperationInscriptionSuccessViewController *) frequentOperationInscriptionSuccessViewController;


- (IBAction)backToOperationButtonTapped:(id)sender;


@end
