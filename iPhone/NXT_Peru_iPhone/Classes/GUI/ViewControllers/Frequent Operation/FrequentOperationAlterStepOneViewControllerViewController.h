//
//  FrequentOperationAlterStepOneViewControllerlViewController.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 12/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FrequentOperationBaseViewController.h"
#import "MOKStringListSelectionButton.h"
#import "MOKCurrencyTextField.h"
#import "FOInscriptionBaseProcess.h"
#import "FrequentOperationInscriptionConfirmationViewController.h"

@interface FrequentOperationAlterStepOneViewControllerViewController : FrequentOperationBaseViewController <MOKStringListSelectionButtonDelegate,UITextFieldDelegate> {
@private
    
    SimpleHeaderView *infoSectionViewHeader_;
    
    UIImageView *infoSectionViewSeparator_;
    
    SimpleHeaderView *channelsViewHeader_;
    
    UIImageView *channelsViewSeparator_;
    
    NSMutableArray *labelsArray_;
    
    CGFloat firstViewHeight_;
    
    /***/
    
    UILabel *largeAliasTextLabel_;
    
    UILabel *shortAliasTextLabel_;
    

    
    UIButton *suggestionLinkButton_;
    
    UIView *suggestionLineView_;
    
    UIImageView *separator_;
    
}
@property (retain, nonatomic) IBOutlet UIView *suggestionLineView;

@property (retain, nonatomic) IBOutlet UIButton *suggestionLinkButton;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *largeAliasTextLabel;

@property (nonatomic, readwrite, retain) IBOutlet MOKTextField *largeAliasTextField;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *shortAliasTextLabel;

@property (nonatomic, readwrite, retain) IBOutlet MOKTextField *shortAliasTextField;

- (IBAction)suggestionLinkButtonTapped:(id)sender;

+ (FrequentOperationAlterStepOneViewControllerViewController *) frequentOperationAlterStepOneViewController;

@end
