//
//  FrequentOperationBaseExecutionViewController+protected.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/26/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationBaseExecutionViewController+protected.h"


#import "FOOperationHelper.h"
#import "FrequentOperationStepTwoViewController.h"
#import "MOKTextField.h"
#import "MOKTextView.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"


#pragma mark -

@implementation FrequentOperationBaseExecutionViewController(protected)

#pragma mark -
#pragma mark Graphic management

/*
 * Displays the stored information. Default implementation updates the header and the table
 */
- (void)displayStoredInformation {
    
    if ([self isViewLoaded]) {
        
        [selectionTableView_ reloadData];
        [self layoutViews];
        
    }
    
}

/*
 * Lays out the views
 */
- (void)layoutViews {
    
}

#pragma mark -
#pragma mark Information management

/*
 * Stores the information into the transfer operation helper instance. Default implementation does nothing
 */
- (void)storeInformationIntoHelper {
    
    if(self.fOOperationHelper.canSendEmailandSMS) {
        
        [self saveContactInfoIntoHelper];
        
    }
    
}

/*
 * Save contact information into the helper
 */
- (void)saveContactInfoIntoHelper {
    
    FOOperationHelper *fOOperationHelper = self.fOOperationHelper;
    
    fOOperationHelper.sendSMS = smsCell_.smsSwitch.on;
    fOOperationHelper.destinationSMS1 = smsCell_.firstTextField.text;
    fOOperationHelper.destinationSMS2 = smsCell_.secondTextField.text;
    fOOperationHelper.selectedCarrier1Index = smsCell_.firstComboButton.selectedIndex;
    fOOperationHelper.selectedCarrier2Index = smsCell_.secondComboButton.selectedIndex;
    
    fOOperationHelper.sendEmail = emailCell_.emailSwitch.on;
    fOOperationHelper.destinationEmail1 = emailCell_.firstTextField.text;
    fOOperationHelper.destinationEmail2 = emailCell_.secondTextField.text;
    fOOperationHelper.emailMessage = emailCell_.emailTextView.text;
    
}

/*
 * Returns the transfer operation helper associated to the view controller. Default implementation returns nil
 */
- (FOOperationHelper *)fOOperationHelper {
    
    return nil;
    
}

#pragma mark -
#pragma mark Displaying views

/*
 * Displays the transfer second step view controller
 */
- (void)displayTransferSecondStepView {
    
    if (foStepTwoViewController_ != nil) {
        
        foStepTwoViewController_ = nil;
        [foStepTwoViewController_ release];
    }
    
    foStepTwoViewController_ = [[FrequentOperationStepTwoViewController frequentOperationStepTwoViewController] retain];
    
    self.hasNavigateForward = YES;
    
    FOOperationHelper *fOOperationHelper = [self fOOperationHelper];
    
    foStepTwoViewController_.foOperationHelper = fOOperationHelper;
    [self.navigationController pushViewController:foStepTwoViewController_
                                         animated:YES];
    
}


@end
