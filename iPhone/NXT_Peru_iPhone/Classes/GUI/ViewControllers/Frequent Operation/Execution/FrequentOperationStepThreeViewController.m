//
//  FrequentOperationStepThreeViewController.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/17/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationStepThreeViewController.h"

#import "Constants.h"
#import "FrequentOperationHeaderView.h"
#import "FOOperationHelper.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKEditableViewController+protected.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "SimpleHeaderView.h"
#import "UIColor+BBVA_Colors.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UINavigationItem+DoubleLabel.h"
/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"FrequentOperationStepThreeViewController"
/**
 * Define the font size for the titles
 */
#define FONT_SIZE                                                   14.0f

@interface FrequentOperationStepThreeViewController ()

@end

@implementation FrequentOperationStepThreeViewController

#pragma mark -
#pragma mark Properties
@synthesize foOperationHelper = foOperationHelper_;
@synthesize frequentOperationButton = frequentOperationButton_;
@synthesize brandingLine = brandingLine_;
@synthesize informationHeader = informationHeader_;

#pragma mark -
#pragma mark initialization
+ (FrequentOperationStepThreeViewController *)frequentOperationStepThreeViewController{
    return [[[FrequentOperationStepThreeViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    informationHeader_ = [[FrequentOperationHeaderView frequentOperationHeaderView] retain];
    
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
    [NXT_Peru_iPhoneStyler styleBlueButton:frequentOperationButton_];
    
    [frequentOperationButton_ setTitle:NSLocalizedString(FO_TITLE_TEXT_REACTIVE_KEY, nil) forState:UIControlStateNormal];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    [self foLoadViews];
    
    [self relocateViews];
    
    [self lookForEditViews];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:TRUE];
    
    [self relocateViews];
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]);
    [self setScrollFrame:frame];
    
}

- (void)foLoadViews{
    
    CGRect frame = [informationHeader_ frame];
    frame.size.width = 320;
    //frame.origin.x = 10;
    frame.size.height = [informationHeader_ height];
    [informationHeader_ setFrame:frame];
    
    [informationHeader_ setTitlesAndAttributeArray:[foOperationHelper_ foThirdStepInformation]];
    [informationHeader_ updateInformationView];
    
    [[self scrollableView] addSubview:informationHeader_];
    
    //First header
    firstHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [firstHeader_ setTitle:NSLocalizedString(FO_OPERATION_DONE_KEY, nil)];
    
    [[self scrollableView] addSubview:firstHeader_];
    
    if(!itfMessageLabel_){
        
        BOOL empty = [[foOperationHelper_ itfMessage] length]==0;
        
        itfMessageLabel_ = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, (empty)?0:30)];
        
        [itfMessageLabel_ setNumberOfLines:2];
        [itfMessageLabel_ setLineBreakMode:NSLineBreakByWordWrapping];
        
        [NXT_Peru_iPhoneStyler styleLabel:itfMessageLabel_ withFontSize:12.0f color:[UIColor BBVAGreyToneTwoColor]];
        
        [itfMessageLabel_ setTextAlignment:UITextAlignmentCenter];
        [itfMessageLabel_ setText:[foOperationHelper_ itfMessage]];
        [[self scrollableView] addSubview:itfMessageLabel_];
    }

    
    if(!notificationLabel_){
        
        BOOL empty = [[foOperationHelper_ notificationMessage] length]==0;
        
        notificationLabel_ = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, (empty)?0:30)];
    
        [notificationLabel_ setNumberOfLines:2];
        [notificationLabel_ setLineBreakMode:NSLineBreakByWordWrapping];
        
        [NXT_Peru_iPhoneStyler styleLabel:notificationLabel_ withFontSize:12.0f color:[UIColor BBVAGreyToneTwoColor]];
    
        [notificationLabel_ setTextAlignment:UITextAlignmentCenter];
        [notificationLabel_ setText:[foOperationHelper_ notificationMessage]];
        [[self scrollableView] addSubview:notificationLabel_];
    }
    
    if(!confirmationLabel_){
        
        BOOL empty = [[foOperationHelper_ confirmationMessage] length]==0;
        
        CGSize size = [[foOperationHelper_ confirmationMessage] sizeWithFont:[NXT_Peru_iPhoneStyler normalFontWithSize:12.0f] constrainedToSize:CGSizeMake(300, 9999) lineBreakMode:NSLineBreakByWordWrapping];
        
        confirmationLabel_ = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, empty?0:size.height)];
    
        [confirmationLabel_ setNumberOfLines:5];
        [confirmationLabel_ setLineBreakMode:NSLineBreakByWordWrapping];
        
        [confirmationLabel_ setTextAlignment:UITextAlignmentCenter];
        
        [NXT_Peru_iPhoneStyler styleLabel:confirmationLabel_ withFontSize:12.0f color:[UIColor BBVAGreyToneTwoColor]];

        [confirmationLabel_ setText:[foOperationHelper_ confirmationMessage] ];
    
        [[self scrollableView] addSubview:confirmationLabel_];
     }
    
    
}

#pragma mark -
#pragma mark User interaction
- (IBAction)onTapFOButton:(id)sender{
    
    [self.navigationController popToRootViewControllerAnimated:TRUE];
}

#pragma mark -
#pragma mark View style
/**
 * Relocate views
 */
- (void)relocateViews {
    
    CGFloat yPosition = 0.0f;
    CGFloat nearSeparation = 5.0f;
    CGFloat farSeparation = 10.0f;
    
    CGRect frame = [firstHeader_ frame];
    frame.origin.y = yPosition;
    [firstHeader_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + farSeparation;
    
    frame = [informationHeader_ frame];
    frame.origin.y = yPosition;
    [informationHeader_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + nearSeparation;

    
    if (itfMessageLabel_) {
        
        frame = [itfMessageLabel_ frame];
        frame.origin.y = yPosition;
        [itfMessageLabel_ setFrame:frame];
        
        if([[foOperationHelper_ itfMessage] length]>0)
            yPosition = CGRectGetMaxY(frame) + nearSeparation;
    }
    
    if(confirmationLabel_){
        frame = [confirmationLabel_ frame];
        frame.origin.y = yPosition;
        [confirmationLabel_ setFrame:frame];
        
        if([[foOperationHelper_ confirmationMessage] length]>0)
            yPosition = CGRectGetMaxY(frame) + nearSeparation;
    }
    
    if (notificationLabel_) {

        frame = [notificationLabel_ frame];
        frame.origin.y = yPosition;
        [notificationLabel_ setFrame:frame];
    
        if([[foOperationHelper_ notificationMessage] length]>0)
            yPosition = CGRectGetMaxY(frame) + nearSeparation;
    }
    
    frame = [frequentOperationButton_ frame];
    frame.origin.y = yPosition;
    [frequentOperationButton_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + farSeparation;
    
    [self setScrollContentSize:CGSizeMake(320.0f, yPosition)];
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    result.mainTitle = NSLocalizedString(FO_TITLE_TEXT_REACTIVE_KEY, nil);
    
    result.hidesBackButton = TRUE;
    
    return result;
    
}

#pragma mark -
#pragma mark memory management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [firstHeader_ release];
    firstHeader_ = nil;
    
    [informationHeader_ release];
    informationHeader_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
}

- (void)dealloc{

    [foOperationHelper_ release];
    foOperationHelper_ = nil;
    
    [frequentOperationButton_ release];
    frequentOperationButton_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
    [informationHeader_ release];
    informationHeader_ = nil;
    
    [firstHeader_ release];
    firstHeader_ = nil;
    
    [super dealloc];
}

@end
