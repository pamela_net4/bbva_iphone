//
//  FrequentOperationBaseExecutionViewController+protected.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/26/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationBaseExecutionViewController.h"


//Forward declarations
@class TransferDetailCell;
@class FOOperationHelper;


/**
 * TransfersStepOneViewController protected category
 */
@interface FrequentOperationBaseExecutionViewController(protected)

/**
 * Displays the stored information. Default implementation updates the header and the table
 *
 * @protected
 */
- (void)displayStoredInformation;

/**
 * Lays out the views. Default implementation does nothing
 *
 * @protected
 */
- (void)layoutViews;

/**
 * Stores the information into the transfer operation helper instance. Default implementation does nothing
 *
 * @protected
 */
- (void)storeInformationIntoHelper;

/**
 * Save contact information into the helper
 */
- (void)saveContactInfoIntoHelper;

/**
 * Returns the Frequent operation helper associated to the view controller. Default implementation returns nil
 *
 * @return The Frequent operation helper associated to the view controller
 * @protected
 */
- (FOOperationHelper *)fOOperationHelper;

/**
 * Displays the transfer second step view controller
 *
 * @private
 */
- (void)displayTransferSecondStepView;

@end