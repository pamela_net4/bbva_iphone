//
//  FrequentOperationBaseExecutionViewController.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/26/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationBaseExecutionViewController.h"

#import "EmailMokCell.h"
#import "FrequentOperationBaseExecutionViewController+protected.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKEditableViewController.h"
#import "MOKEditableViewController+protected.h"
#import "MOKStringListSelectionButton.h"
#import "MOKTextField.h"
#import "MOKTextView.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "SMSMokCell.h"
#import "Tools.h"
#import "FrequentOperationStepTwoViewController.h"
#import "TermsAndConsiderationsViewController.h"
#import "FOOperationHelper.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"

/**
 * Defines de max text lenght for the mail
 */
#define MAX_TEXT_MAIL_LENGHT                    80


/**
 * FrequentOperationBaseExecutionViewController private extension
 */
@interface FrequentOperationBaseExecutionViewController()

/**
 * Releases Graphic Elements
 *
 * @private
 */
- (void)releaseFrequentOperationBaseExecutionViewControllerGraphicElements;

/**
 * Display the contact selection
 */
- (void)displayContactSelection;

/**
 * Fixes the add contact button images
 */
- (void)fixAddContactButtonImages;

@end

#pragma mark -

@implementation FrequentOperationBaseExecutionViewController

#pragma mark -
#pragma mark Properties

@synthesize containerView = containerView_;

@synthesize selectionTableView = selectionTableView_;
@synthesize continueButton = continueButton_;
@synthesize separator = separator_;
@synthesize brandingImageView = brandingImageView_;
@synthesize bottomInfoLabel = bottomInfoLabel_;
@synthesize smsCell = smsCell_;
@synthesize emailCell = emailCell_;
@synthesize hasNavigateForward = hasNavigateForward_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseFrequentOperationBaseExecutionViewControllerGraphicElements];
    
    [foStepTwoViewController_ release];
    foStepTwoViewController_ = nil;
    
    [emailCell_ release];
    emailCell_ = nil;
    
    [smsCell_ release];
    smsCell_ = nil;
    
    [super dealloc];
    
}

/**
 * Called when the controller’s view is released from memory
 */
- (void)viewDidUnload {
    
    [self releaseFrequentOperationBaseExecutionViewControllerGraphicElements];
    [super viewDidUnload];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseFrequentOperationBaseExecutionViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

/*
 * Releases graphic elements
 */
- (void)releaseFrequentOperationBaseExecutionViewControllerGraphicElements{
    
    [containerView_ release];
    containerView_ = nil;
    
	[continueButton_ release];
	continueButton_ = nil;
    
    [selectionTableView_ release];
    selectionTableView_ = nil;
    
    [bottomInfoLabel_ release];
    bottomInfoLabel_ = nil;
    
    [separator_ release];
    separator_ = nil;
    
    [brandingImageView_ release];
    brandingImageView_ = nil;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[self view] setFrame:[[UIScreen mainScreen] bounds]];
    
    UIView *view = self.view;
    
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    brandingImageView_.image = [imagesCache imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME];
    separator_.image = [imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];

    
    containerView_.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:bottomInfoLabel_ withFontSize:11.0f color:[UIColor BBVAGreyToneTwoColor]];

    [bottomInfoLabel_ setNumberOfLines:1];
    [bottomInfoLabel_ setTextAlignment:UITextAlignmentLeft];
    
    [selectionTableView_ setScrollEnabled:YES];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:continueButton_];
    [continueButton_ setTitle:NSLocalizedString(CONTINUE_TEXT_KEY, nil) forState:UIControlStateNormal];
    [continueButton_ addTarget:self
                        action:@selector(onTapContinueButton)
              forControlEvents:UIControlEventTouchUpInside];
    
    selectionTableView_.backgroundColor = [UIColor clearColor];
    
    
    self.hasNavigateForward = NO;
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingImageView] frame]);
    [self setScrollFrame:frame];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view. Ask the concrete class to store
 * the information into the helper object
 *
 * @param animated If YES, the disappearance of the view is being animated
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self storeInformationIntoHelper];
    
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingImageView] frame]);
    [self setScrollFrame:frame];
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Resets the internal information.
 */
- (void)resetInformation {
    
    FOOperationHelper *fOOperationHelper = [self fOOperationHelper];
    
    [fOOperationHelper setSendSMS:NO];
    [fOOperationHelper setShowSMS1:NO];
    [fOOperationHelper setShowSMS2:NO];
    [fOOperationHelper setSelectedCarrier1Index:-1];
    [fOOperationHelper setDestinationSMS1:@""];
    [fOOperationHelper setSelectedCarrier2Index:-1];
    [fOOperationHelper setDestinationSMS2:@""];
    
    [fOOperationHelper setSendEmail:NO];
    [fOOperationHelper setShowEmail1:NO];
    [fOOperationHelper setShowEmail2:NO];
    [fOOperationHelper setDestinationEmail1:@""];
    [fOOperationHelper setDestinationEmail2:@""];
    [fOOperationHelper setEmailMessage:@""];
    
}

#pragma mark -
#pragma mark User interaction

/*
 * Invoked by framework when tranfer button is tapped. The information is stored into the transfer operation helper instance and the
 * information validity is checked. When everithing is OK, the server confirm operation is invoked
 */
- (void)onTapContinueButton {
    
    [self storeInformationIntoHelper];
    
    [self.fOOperationHelper setEmailMessage:emailCell_.emailTextView.text];
    
    FOOperationHelper *fOOperationHelper = self.fOOperationHelper;
    NSString *validationErrorMessage = [fOOperationHelper startFORequestDataValidation];
    
    if (validationErrorMessage != nil) {
        
        [Tools showAlertWithMessage:validationErrorMessage
                              title:NSLocalizedString(INFO_MESSAGE_TITLE_KEY, nil)];
        
    } else {
        
        if ([fOOperationHelper startFORequest]) {
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initialFrequentOperationResponse:) name:[fOOperationHelper notificationConfirmationKey] object:nil];
            
            [self.appDelegate showActivityIndicator:poai_Both];
            
        } else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATIONS_ERROR_TEXT_KEY, nil)];
            
        }
        
    }
    
}

/*
 * Display the contact selection
 */
- (void)displayContactSelection {
    
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    
    self.hasNavigateForward = YES;
    
    [[picker navigationBar] setTintColor:[[[self navigationController] navigationBar] tintColor]];
    
    [self.appDelegate presentModalViewController:picker animated:YES];
    
    [picker release];
    
}

/**
 * Fixes the add contact button images
 */
- (void)fixAddContactButtonImages {
    
    if ((self.fOOperationHelper.destinationSMS1 == nil) || [self.fOOperationHelper.destinationSMS1 isEqualToString:@""]) {
        
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
        }
        else{
            [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
        }
        
       
        
    } else {
        
        [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                           forState:UIControlStateNormal];
    }
    
    if ((self.fOOperationHelper.destinationSMS2 == nil) || [self.fOOperationHelper.destinationSMS2 isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                            forState:UIControlStateNormal];
        }else{
            [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
        }
        
    } else {
        
        [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                            forState:UIControlStateNormal];
    }
    
    if ((self.fOOperationHelper.destinationEmail1 == nil) || [self.fOOperationHelper.destinationEmail1 isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                             forState:UIControlStateNormal];
        }
        else{
            [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
        }
    } else {
        
        [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                             forState:UIControlStateNormal];
    }
    
    if ((self.fOOperationHelper.destinationEmail2 == nil) || [self.fOOperationHelper.destinationEmail2 isEqualToString:@""]) {
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
        {
            [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                              forState:UIControlStateNormal];
        }
        else{
            [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
        }
    } else {
        
        [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                              forState:UIControlStateNormal];
    }
    
    
}

/*
 *called when the user tapped in first combo button
 */
- (void)firstComboButtonTapped {

}

/*
 *called when the user tapped in second combo button
 */
-(void)secondComboButtonTapped {
    
}

#pragma mark -
#pragma mark UIViewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    NSString *title = NSLocalizedString(FO_TITLE_TEXT_REACTIVE_KEY, nil);
    
    result.mainTitle = title;
    
    return result;
    
}

#pragma mark -
#pragma mark UITableViewDatasource protocol selectors


/**
 * Tells the data source to return the number of rows in a given section of a table view. In this case, only one cell is displayed
 *
 * @param tableView The table-view object requesting this information
 * @param section An index number identifying a section in tableView
 * @return The number of rows in section
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 2;
    
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view. An NXTTableCell is
 * returned
 *
 * @param tableView A table-view object requesting the cell
 * @param indexPath An index path locating a row in tableView
 * @return An object inheriting from UITableViewCellthat the table view can use for the specified row
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FOOperationHelper *fOOperationHelper = self.fOOperationHelper;
    
    if (fOOperationHelper.canSendEmailandSMS) {
        
        if (indexPath.row == 0) {
            
            SMSMokCell *result = (SMSMokCell *)[tableView dequeueReusableCellWithIdentifier:[SMSMokCell cellIdentifier]];
            
            if (result == nil) {
                
                if (smsCell_ == nil) {
                    
                    smsCell_ = [[SMSMokCell smsMokCell] retain];
                    
                }
                
                result = smsCell_;
                
            }
            
            result.delegate = self;
            result.showSeparator = NO;
            
            [result.titleLabel setText:NSLocalizedString(NOTIFICATIONS_TABLE_SEND_PHONE_TEXT_KEY, nil)];
            result.smsSwitch.on = fOOperationHelper.sendSMS;
            
            result.firstTextField.text = fOOperationHelper.destinationSMS1;
            result.firstTextField.delegate = self;
            
            result.selectedFirstOperatorIndex = fOOperationHelper.selectedCarrier1Index;
            [result.firstComboButton setStringListSelectionButtonDelegate:self];
            
            result.secondTextField.text = fOOperationHelper.destinationSMS2;
            result.secondTextField.delegate = self;
            
            result.selectedSecondOperatorIndex = fOOperationHelper.selectedCarrier2Index;
            [[result secondComboButton] setStringListSelectionButtonDelegate:self];
            
            [result setOperatorArray:fOOperationHelper.carrierList
                          firstAddOn:fOOperationHelper.showSMS1
                         secondAddOn:fOOperationHelper.showSMS2];
            
            [result.firstComboButton setSelectedIndex:fOOperationHelper.selectedCarrier1Index];
            
            [result.secondComboButton setSelectedIndex:fOOperationHelper.selectedCarrier2Index];
            
            [result.firstTextField setDelegate:self];
            [result.secondTextField setDelegate:self];
            [result.firstComboButton setStringListSelectionButtonDelegate:self];
            [result.secondComboButton setStringListSelectionButtonDelegate:self];
            
            result.backgroundColor = [UIColor clearColor];
            
            [self lookForEditViews];
            
            return result;
            
        } else {
            
            EmailMokCell *result = (EmailMokCell *)[tableView dequeueReusableCellWithIdentifier:[EmailMokCell cellIdentifier]];
            
            if (result == nil) {
                
                if(emailCell_ == nil) {
                    
                    emailCell_ = [[EmailMokCell emailMokCell] retain];
                }
                result = emailCell_;
                
            }
            
            result.delegate = self;
            result.showSeparator = YES;
            
            [result.titleLabel setText:NSLocalizedString(NOTIFICATIONS_TABLE_SEND_EMAIL_TEXT_KEY, nil)];
            result.emailSwitch.on = fOOperationHelper.sendEmail;
            
            result.firstTextField.text = fOOperationHelper.destinationEmail1;
            result.secondTextField.text = fOOperationHelper.destinationEmail2;
            result.emailTextView.text = fOOperationHelper.emailMessage;
            
            [result.firstTextField setDelegate:self];
            [result.secondTextField setDelegate:self];
            [result.emailTextView setDelegate:self];
            
            [self fixAddContactButtonImages];
            
            [result setConfigurationForFirstAddOn:fOOperationHelper.showEmail1
                                      secondAddOn:fOOperationHelper.showEmail2];
            
            result.backgroundColor = [UIColor clearColor];
            
            [self lookForEditViews];
            
            return result;
            
        }
        
    }
    
    UITableViewCell *cell4 = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell4 == nil) {
        
        cell4=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }

    
    cell4.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell4;
    
  //  return nil;
    
}

/**
 * Asks the data source to return the number of sections in the table view. When no coordinate key is needed, 2 is returned, when
 * it is needed to display the coordinate key, 3 is returned
 *
 * @param tableView An object representing the table view requesting this information
 * @return The number of sections in tableView
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
    return 1;
    
}

/**
 * Asks the delegate for a view object to display in the header of the specified section of the table view. The appropriate section
 * header is displayed
 *
 * @param tableView The table-view object asking for the view object
 * @param section An index number identifying a section of tableView
 * @return A view object to be displayed in the header of section
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    SimpleHeaderView *simpleHeaderView = [SimpleHeaderView simpleHeaderView];
    [simpleHeaderView setTitle:NSLocalizedString(TRANSFER_REPORT_TO_BENEFICIARY_TEXT_KEY, nil)];
    
    return simpleHeaderView;
    
}


/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FOOperationHelper *fOOperationHelper = self.fOOperationHelper;
    
    if (indexPath.row == 0) {
        
        CGFloat result = [SMSMokCell cellHeightForFirstAddOn:fOOperationHelper.showSMS1
                                              secondAddOn:fOOperationHelper.showSMS2];
        
        
        return result;
        
    } else {
        
        return [EmailMokCell cellHeightForFirstAddOn:fOOperationHelper.showEmail1
                                      secondAddOn:fOOperationHelper.showEmail2];
        
    }
    
}

/**
 * Asks the delegate for the height to use for the header of a particular section.
 *
 * @param tableView The table-view object requesting this information.
 * @param section An index number identifying a section of tableView
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return [SimpleHeaderView height];
}



#pragma mark -
#pragma mark SMSCell Delegate

/**
 * Switch has been tapped.
 */
- (void)switchButtonHasBeenTapped:(BOOL)on {
    
    [self saveContactInfoIntoHelper];
    
    [[self fOOperationHelper] setSendSMS:on];
    [[self fOOperationHelper] setShowSMS1:on];
    
    if (!on) {
        
        addingFirstSMS_ = NO;
        addingSecondSMS_ = NO;
        
        [[self fOOperationHelper] setShowSMS2:on];
        
        smsCell_.firstTextField.text = @"";
        [[self fOOperationHelper] setDestinationSMS1:@""];
        
        smsCell_.secondTextField.text = @"";
        [[self fOOperationHelper] setDestinationSMS2:@""];
        
        [[smsCell_ firstComboButton] setSelectedIndex:-1];
        [[self fOOperationHelper] setSelectedCarrier1Index:-1];
        
        [[smsCell_ secondComboButton] setSelectedIndex:-1];
        [[self fOOperationHelper] setSelectedCarrier2Index:-1];
        
    }
    else {
        
        [[self fOOperationHelper] setShowSMS2:NO];
        [[self fOOperationHelper] setDestinationSMS1:smsCell_.firstTextField.text];
        [[self fOOperationHelper] setDestinationSMS2:smsCell_.secondTextField.text];
        
        [[smsCell_ firstComboButton] setSelectedIndex:-1];
        [[self fOOperationHelper] setSelectedCarrier1Index:-1];
        
    }
    
    [self fixAddContactButtonImages];
    
	[self lookForEditViews];
    [self layoutViews];
    
}

/**
 * More button has been tapped
 */
- (void)moreButtonHasBeenTapped {
    
    [self saveContactInfoIntoHelper];
    self.fOOperationHelper.showSMS2 = YES;
    
    smsCell_.secondComboButton.selectedIndex = -1;
    [[self fOOperationHelper] setSelectedCarrier2Index:-1];
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
    
    [selectionTableView_ reloadRowsAtIndexPaths:array
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [array release];
    
    [self layoutViews];
    
}

/**
 * Add First Contact Button Tapped
 */
- (void)addFirstContactHasBeenTapped {
    
    if ((smsCell_.firstTextField.text == nil) || [smsCell_.firstTextField.text isEqualToString:@""]) {
        
        addingFirstSMS_ = YES;
        [self displayContactSelection];
        
    } else {
        
        smsCell_.firstTextField.text = @"";
        self.fOOperationHelper.destinationSMS1 = @"";
        
    }
    
    [self fixAddContactButtonImages];
    
}

/**
 * Add Second Contact Button Tapped
 */
- (void)addSecondContactHasBeenTapped {
    
    if ((smsCell_.secondTextField.text == nil) || [smsCell_.secondTextField.text isEqualToString:@""]) {
        
        addingSecondSMS_ = YES;
        [self displayContactSelection];
        
    } else {
        
        smsCell_.secondTextField.text = @"";
        self.fOOperationHelper.destinationSMS2 = @"";
        
    }
    
    [self fixAddContactButtonImages];
    
}

#pragma mark -
#pragma mark EmailCell Delegate

/**
 * Switch has been tapped.
 *
 * @param on The flag
 */
- (void)emailSwitchButtonHasBeenTapped:(BOOL)on {
    
    [self saveContactInfoIntoHelper];
    
    self.fOOperationHelper.sendEmail = on;
    self.fOOperationHelper.showEmail1 = on;
    
    if (!on) {
        
        self.fOOperationHelper.showEmail2 = on;
        
        addingFirstEmail_ = NO;
        addingSecondEmail_ = NO;
        
        emailCell_.firstTextField.text = @"";
        self.fOOperationHelper.destinationEmail1= @"";
        
        emailCell_.secondTextField.text = @"";
        self.fOOperationHelper.destinationEmail2= @"";
        
        emailCell_.emailTextView.text = @"";
        self.fOOperationHelper.emailMessage = @"";
       
        
        
    } else {
        
        self.fOOperationHelper.showEmail2 = NO;
        [self fOOperationHelper].destinationEmail1 = emailCell_.firstTextField.text;
        [self fOOperationHelper].destinationEmail2 = emailCell_.secondTextField.text;
        [self fOOperationHelper].emailMessage = emailCell_.emailTextView.text;
    }
    
    [self fixAddContactButtonImages];
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
    
    [selectionTableView_ reloadRowsAtIndexPaths:array
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [array release];
    
    [self layoutViews];
    
}

/**
 * Add First Contact Button Tapped
 */
- (void)emailAddFirstContactHasBeenTapped {
    
    if ((emailCell_.firstTextField.text == nil) || [emailCell_.firstTextField.text isEqualToString:@""]) {
        
        addingFirstEmail_ = YES;
        [self displayContactSelection];
        
    } else {
        
        emailCell_.firstTextField.text = @"";
        self.fOOperationHelper.destinationEmail1= @"";
        
    }
    
    [self fixAddContactButtonImages];
    
}

/**
 * Add Second Contact Button Tapped
 */
- (void)emailAddSecondContactHasBeenTapped {
    
    if ((emailCell_.secondTextField.text == nil) || [emailCell_.secondTextField.text isEqualToString:@""]) {
        
        addingSecondEmail_ = YES;
        [self displayContactSelection];
        
    } else {
        
        emailCell_.secondTextField.text = @"";
        self.fOOperationHelper.destinationEmail2= @"";
        
    }
    
    [self fixAddContactButtonImages];
    
}

/**
 * More button has been tapped
 */
- (void)emailMoreButtonHasBeenTapped {
    
    [self saveContactInfoIntoHelper];
    self.fOOperationHelper.showEmail2 = YES;
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:index, nil];
    
    [selectionTableView_ reloadRowsAtIndexPaths:array
                               withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [array release];
    
    [self layoutViews];
    
}

#pragma mark -
#pragma mark ABPeoplePickerNavigationControllerDelegate methods
/**
 * Called in ios 8 redirection to shouldContinueAfterSelectingPerson used in ios 7 or previous
 */
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    
    [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person property:property identifier:identifier];
}

/**
 * Called after the user has pressed cancel. The delegate is responsible for dismissing the peoplePicker
 */
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    
	[self.appDelegate dismissModalViewControllerAnimated:YES];
    
    [self fixAddContactButtonImages];
    addingFirstSMS_ = NO;
    addingSecondSMS_ = NO;
    addingFirstEmail_ = NO;
    addingSecondEmail_ = NO;
    
}

/**
 * Called after a person has been selected by the user.
 *
 * Return YES if you want the person to be displayed.
 * Return NO  to do nothing (the delegate is responsible for dismissing the peoplePicker).
 */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    
	[self.appDelegate dismissModalViewControllerAnimated:YES];
    
    if (addingFirstSMS_ || addingSecondSMS_) {
        
        BOOL isValidMobilePhoneNumber = NO;
        
        ABMultiValueRef phoneProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
        
        if (ABMultiValueGetCount(phoneProperty) > 0) {
            
            ABMultiValueRef phoneValue;
            NSString *phoneNumber;
            
            for (int i = 0; ABMultiValueGetCount(phoneProperty) > i; i++) {
                
                phoneValue = ABMultiValueCopyValueAtIndex(phoneProperty,i);
                
                phoneNumber = (NSString *)phoneValue;
                
                phoneNumber = [Tools mobileNumberLikeFromString:phoneNumber];
                
                isValidMobilePhoneNumber = [Tools isValidMobilePhoneNumberString:phoneNumber];
                
                if (isValidMobilePhoneNumber) {
                    break;
                }
            }
            
            if (isValidMobilePhoneNumber) {
                
                if (addingFirstSMS_) {
                    
                    [[self fOOperationHelper] setDestinationSMS1:phoneNumber];
                    addingFirstSMS_ = NO;
                    
                } else if (addingSecondSMS_) {
                    
                    [[self fOOperationHelper] setDestinationSMS2:phoneNumber];
                    addingSecondSMS_ = NO;
                    
                }
                
            }
            
            if (phoneValue != nil) {
                
                CFRelease(phoneValue);
                
            }
            
        }
        
        
        if (!isValidMobilePhoneNumber) {
            
            [Tools showInfoWithMessage:NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_NO_EXIST_TEXT_KEY, nil)];
            
        }
        
        addingFirstSMS_ = NO;
        addingSecondSMS_ = NO;
        
        if (phoneProperty != nil) {
            
            CFRelease(phoneProperty);
            
        }
        
    } else if (addingFirstEmail_ || addingSecondEmail_) {
        
        
        ABMultiValueRef emailProperty = ABRecordCopyValue(person, kABPersonEmailProperty);
        NSInteger emailsNumber = ABMultiValueGetCount(emailProperty);
        
        if (emailsNumber > 0) {
            
            //if (emailsNumber == 1) {
                
                ABMultiValueRef emailValue = ABMultiValueCopyValueAtIndex(emailProperty, 0);
                
                if (addingFirstEmail_) {
                    
                    [[self fOOperationHelper] setDestinationEmail1:(NSString *)emailValue];
                    addingFirstEmail_ = NO;
                    
                } else if (addingSecondEmail_) {
                    
                    [[self fOOperationHelper] setDestinationEmail2:(NSString *)emailValue];
                    addingSecondEmail_ = NO;
                    
                }
                
                if (emailValue != nil) {
                    
                    CFRelease(emailValue);
                    
                }
                
            //}
            
        } else {
            
            [Tools showInfoWithMessage:NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_NO_EXIST_TEXT_KEY, nil)];
            
            addingFirstEmail_ = NO;
            addingSecondEmail_ = NO;
            
        }
        
        if (emailProperty != nil) {
            
            CFRelease(emailProperty);
            
        }
        
    }
    
    [self fixAddContactButtonImages];
    
  	return NO;
}

/**
 * Called after a value has been selected by the user.
 *
 * Return YES if you want default action to be performed.
 * Return NO to do nothing (the delegate is responsible for dismissing the peoplePicker).
 */
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property
                              identifier:(ABMultiValueIdentifier)identifier {
    
    return [self peoplePickerNavigationController:peoplePicker shouldContinueAfterSelectingPerson:person];
    
}


#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors

/**
 * Asks the delegate if editing should begin in the specified text field.
 *
 * @param textField The text field for which editing is about to begin.
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    
	return YES;
}

/**
 * Tells the delegate that editing began for the specified text field.
 *
 * @param textField The text field for which an editing session began.
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (lastEditingView_ != textField) {
        lastEditingView_ = textField;
    }
    
}

/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    if( (textField == [[self smsCell] firstTextField]) || (textField == [[self smsCell] secondTextField])) {
        
        if(resultLength == 0) {
            
            result = YES;
        }
        else if(resultLength <= 9) {
            
            NSScanner *scanner = [NSScanner scannerWithString:resultString];
            NSInteger resultInteger = 0;
            
            if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                if (resultInteger >= 0) {
                    
                    result = YES;
                }
            }
        }
    }
    else if ( (textField == [[self emailCell] firstTextField]) || (textField == [[self emailCell] secondTextField]) ) {
        
        if (resultLength <= 50) {
            
            result = YES;
            
        }
    }
    
    
    if (resultLength == 0) {
        
        if (textField == smsCell_.firstTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
            }else{
                [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                   forState:UIControlStateNormal];
            }
        } else if (textField == smsCell_.secondTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                forState:UIControlStateNormal];
            }else{
                [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                   forState:UIControlStateNormal];
            }
        } else if (textField == emailCell_.firstTextField) {
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                 forState:UIControlStateNormal];
            }
            else{
                [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                   forState:UIControlStateNormal];
            }
        } else if (textField == emailCell_.secondTextField) {
            
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
            {
                [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                  forState:UIControlStateNormal];
            }
            else{
                [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME] 
                                                   forState:UIControlStateNormal];
            }
        }
        
    } else {
        
        if (textField == smsCell_.firstTextField) {
            
            [smsCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                               forState:UIControlStateNormal];
            
        } else if (textField == smsCell_.secondTextField) {
            
            [smsCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                forState:UIControlStateNormal];
            
        } else if (textField == emailCell_.firstTextField) {
            
            [emailCell_.firstAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                 forState:UIControlStateNormal];
            
        } else if (textField == emailCell_.secondTextField) {
            
            [emailCell_.secondAddButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                                                  forState:UIControlStateNormal];
            
        }
        
    }
    
    return result;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    lastEditingView_ = nil;
    
}


#pragma mark -
#pragma mark UITextViewDelegate protocol selectors

/**
 * Asks the delegate if editing should stop in the specified text view.
 */
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    if (textView != lastEditingView_) {
        
        lastEditingView_ = textView;
        
    }
    
	return YES;
    
}

/**
 * Asks the delegate whether the specified text should be replaced in the text view.
 *
 * @param textView The text view containing the changes.
 * @param range The current selection range.
 * @param text The text to insert.
 */
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    BOOL result = YES;
    
    NSString *resultString = [textView.text stringByReplacingCharactersInRange:range
                                                                    withString:text];
    
    if (textView == [[self emailCell] emailTextView]) {
        
        if ([resultString length] < MAX_TEXT_MAIL_LENGHT) {
            
            result = ([Tools isValidText:text forCharacterSet:[NSCharacterSet alphanumericCharacterSet]] || [Tools isValidText:text forCharacterSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);
            
        }
        else {
            result = NO;
        }
        
    }
    
    return result;
    
}


/**
 * Tells the delegate that editing of the specified text view has ended.
 */
- (void)texViewDidBeginEditing:(UITextView *)textView {
    
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    lastEditingView_ = nil;
}


#pragma mark -
#pragma mark NXTComboButtonDelegate selectors

/*
 * Informs the delegate that a value has been selected
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton{
    
    if (stringListSelectionButton == smsCell_.firstComboButton) {
        
        self.fOOperationHelper.selectedCarrier1Index = stringListSelectionButton.selectedIndex;
        
    } else if (stringListSelectionButton == smsCell_.secondComboButton) {
        
        self.fOOperationHelper.selectedCarrier2Index = stringListSelectionButton.selectedIndex;
        
    }
}

@end