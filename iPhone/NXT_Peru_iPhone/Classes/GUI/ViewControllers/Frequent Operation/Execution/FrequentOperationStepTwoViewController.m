//
//  FrequentOperationStepTwoViewController.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/14/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationStepTwoViewController.h"

#import "Constants.h"
#import "FOOperationHelper.h"
#import "FrequentOperationHeaderView.h"
#import "FrequentOperationStepThreeViewController.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "LegalTermsTopCell.h"
#import "MOKEditableViewController+protected.h"
#import "MOKEditableViewController.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "StatusEnabledResponse.h"
#import "TermsAndConsiderationsViewController.h"
#import "Tools.h"
#import "UINavigationItem+DoubleLabel.h"
#import "UIColor+BBVA_Colors.h"
/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"FrequentOperationStepTwoViewController"
/**
 * Define the font size for the titles
 */
#define FONT_SIZE                                                   12.0f

@interface FrequentOperationStepTwoViewController ()

/*
 * Invoked by framework when the response to the transfer operation is received. The information is analized
 */
- (void)confirmationResponseReceived:(NSNotification *)notification;
/**
 * Display the confirmation view for the operation
 */
- (void)displayTransferThirdStepView;
/**
 * Load the views to the scrollable view
 */
- (void)foLoadViews;
/**
 * Relocate views
 */
- (void)relocateViews;

@end

@implementation FrequentOperationStepTwoViewController

@synthesize acceptButton = acceptButton_;
@synthesize brandingLine = brandingLine_;
@synthesize foOperationHelper = foOperationHelper_;
@synthesize informationHeader = informationHeader_;
@synthesize sealImage = sealImage_;
@synthesize sealDescriptionLabel = sealDescriptionLabel_;

#pragma mark -
#pragma mark initialization
+ (FrequentOperationStepTwoViewController *)frequentOperationStepTwoViewController{
    return [[[FrequentOperationStepTwoViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    informationHeader_ = [[FrequentOperationHeaderView frequentOperationHeaderView] retain];
    [NXT_Peru_iPhoneStyler styleLabel:sealDescriptionLabel_ withFontSize:FONT_SIZE color:[UIColor BBVAGreyColor]];
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:acceptButton_];
    [acceptButton_ setTitle:NSLocalizedString(OK_TEXT_KEY, nil) forState:UIControlStateNormal];
    
    [sealDescriptionLabel_ setText:NSLocalizedString(SHARE_SEAL_TEXT_KEY, nil)];
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    NSData *data = [Tools base64DataFromString:foOperationHelper_.seal];
    UIImage *image = [[[UIImage alloc] initWithData:data] autorelease];
    [sealImage_ setImage:image];
    
    [self foLoadViews];
    
    [self relocateViews];
    
    [self lookForEditViews];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:TRUE];
    
    [self relocateViews];
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:TRUE];
    
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]);
    [self setScrollFrame:frame];
    
}

- (void)foLoadViews{
    
    //First header
    firstHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [firstHeader_ setTitle:[foOperationHelper_ foHeaderTitle]];
    
    [[self scrollableView] addSubview:firstHeader_];
    
    //information header
    
    CGRect frame = [informationHeader_ frame];
    frame.size.width = 320;
    [informationHeader_ setFrame:frame];
    [informationHeader_ setClassName:[foOperationHelper_ class]];
    [informationHeader_ setTitlesAndAttributeArray:[foOperationHelper_ foSecondStepInformation]];
    [informationHeader_ updateInformationView];
    
    
    [[self scrollableView] addSubview:informationHeader_];
    
    if(!itfMessageLabel_){
        
        BOOL empty = [[foOperationHelper_ itfMessageConfirm] length]==0;
        
        itfMessageLabel_ = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, (empty)?0:30)];
        
        [itfMessageLabel_ setNumberOfLines:2];
        [itfMessageLabel_ setLineBreakMode:NSLineBreakByWordWrapping];
        
        [NXT_Peru_iPhoneStyler styleLabel:itfMessageLabel_ withFontSize:12.0f color:[UIColor BBVAGreyToneTwoColor]];
        
        [itfMessageLabel_ setTextAlignment:UITextAlignmentCenter];
        [itfMessageLabel_ setText:[foOperationHelper_ itfMessageConfirm]];
        [[self scrollableView] addSubview:itfMessageLabel_];
    }
    
    //Legal Terms if needed
    if ([self.foOperationHelper canShowLegalTerms]) {
        
        //second header
        secondHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
        [secondHeader_ setTitle:NSLocalizedString(TRANSFER_STEP_TWO_LEGAL_TERMS_TEXT_KEY, nil)];
        
        [[self scrollableView] addSubview:secondHeader_];
        
        
        legalTermsTopCell_ = [[LegalTermsTopCell legalTermsTopCell] retain];
            
        legalTermsTopCell_.delegate = self;
        
        [[self scrollableView] addSubview:legalTermsTopCell_];
        
    }
    
    //trhid header
    thirdHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [thirdHeader_ setTitle:NSLocalizedString(@"Sello de operaciones", nil)];
    
    [[self scrollableView] addSubview:thirdHeader_];
}

#pragma mark -
#pragma mark User interaction
- (IBAction)onTapAcceptButton:(id)sender{
    
    if ([foOperationHelper_ canShowLegalTerms]) {
        if (![legalTermsTopCell_ legalTermsSwitch].isOn) {
            [Tools showAlertWithMessage:NSLocalizedString(TRANSFER_ERROR_LEGAL_TERMS_TEXT_KEY, nil)];
            return;
        }
    }
    
    if ([foOperationHelper_ startFoConfirmationRequest]) {
        [self.appDelegate showActivityIndicator:poai_Both];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(confirmationResponseReceived:) name:[foOperationHelper_ notificationSuccessKey] object:nil];
    }
    
}

#pragma mark -
#pragma mark View style
/**
 * Relocate views
 */
- (void)relocateViews {
    
    CGFloat yPosition = 0.0f;
    CGFloat nearSeparation = 5.0f;
    CGFloat farSeparation = 10.0f;
    CGFloat tooFarSeparation = 20.0f;
    
    CGRect frame = [firstHeader_ frame];
    frame.origin.y = yPosition;
    [firstHeader_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + nearSeparation;
    
    frame = [informationHeader_ frame];
    frame.origin.y = yPosition;
    frame.origin.x = 0.0f;
    [informationHeader_ setFrame:frame];
    
    if (itfMessageLabel_) {
       
        yPosition = CGRectGetMaxY(frame) + nearSeparation;

        frame = [itfMessageLabel_ frame];
        frame.origin.y = yPosition;
        [itfMessageLabel_ setFrame:frame];
        
        
    }
    
    yPosition = CGRectGetMaxY(frame) + farSeparation;
    
    if ([foOperationHelper_ canShowLegalTerms]) {
        frame = [secondHeader_ frame];
        frame.origin.y = yPosition;
        [secondHeader_ setFrame:frame];
        
        yPosition = CGRectGetMaxY(frame) + nearSeparation;
        
        frame = [legalTermsTopCell_ frame];
        frame.origin.y = yPosition;
        [legalTermsTopCell_ setFrame:frame];
        
        yPosition = CGRectGetMaxY(frame) + nearSeparation;
    }
    
    frame = [thirdHeader_ frame];
    frame.origin.y = yPosition;
    if([[Tools notNilString:foOperationHelper_.seal] length]==0){
        frame.size.height = 0;
        [thirdHeader_ setHidden:YES];
    }else{
        frame.size.height = [SimpleHeaderView height];
        [thirdHeader_ setHidden:NO];
    }

    [thirdHeader_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + farSeparation;
    
    frame = [sealDescriptionLabel_ frame];
    frame.origin.y = yPosition;
    frame.origin.x = 10.0f;
    if([[Tools notNilString:foOperationHelper_.seal] length]==0){
        frame.size.height = 0;
        [sealDescriptionLabel_ setHidden:YES];
    }else{
        frame.size.height = 21;
        [sealDescriptionLabel_ setHidden:NO];
    }
    [sealDescriptionLabel_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + nearSeparation;
    
    frame = [sealImage_ frame];
    frame.origin.y = yPosition;
    if([[Tools notNilString:foOperationHelper_.seal] length]==0){
        frame.size.height = 0;
        [sealImage_ setHidden:YES];
    }else{
        frame.size.height = 44;
        [sealImage_ setHidden:NO];
    }

    [sealImage_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + nearSeparation;
    
    frame = [acceptButton_ frame];
    frame.origin.y = yPosition;
    [acceptButton_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + tooFarSeparation;

    
    [self setScrollContentSize:CGSizeMake(320.0f, yPosition)];
}

#pragma mark -
#pragma mark Notifications management

/*
 * Invoked by framework when the response to the transfer operation is received. The information is analized
 */
- (void)confirmationResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[foOperationHelper_ notificationSuccessKey] object:nil];
    
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if ([self.foOperationHelper confirmationResponseReceived:response]) {
        
        [self displayTransferThirdStepView];
        
    } else {
        
        if ([[response errorMessage] length] >0) {
            [Tools showAlertWithMessage:[response errorMessage]];
        }
        else
        {
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
        }
        
    }
}
#pragma mark -
#pragma mark LegalTermsTopCell Delegate

/**
 * This method is called when the switch has changed its state.
 */
- (void)warningButtonHasBeenTapped {
    
    if (termsAndConsiderationsViewController_ == nil) {
        
        termsAndConsiderationsViewController_ = [[TermsAndConsiderationsViewController TermsAndConsiderationsViewController] retain];
        
    }
    
    [termsAndConsiderationsViewController_ setURLText:self.foOperationHelper.legalTermsURL
                                           headerText:foOperationHelper_.localizedFOOperationTypeString];
    
    [self.navigationController pushViewController:termsAndConsiderationsViewController_ animated:YES];
    
    
}

#pragma mark -
#pragma mark utility
- (void)displayTransferThirdStepView{
    
    FrequentOperationStepThreeViewController *stepThreeView = [FrequentOperationStepThreeViewController frequentOperationStepThreeViewController];
    [stepThreeView setFoOperationHelper:foOperationHelper_];
    
    [self.navigationController pushViewController:stepThreeView animated:TRUE];
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    result.mainTitle = NSLocalizedString(FO_TITLE_TEXT_REACTIVE_KEY, nil);
    
    return result;
    
}

#pragma mark -
#pragma mark memory management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [informationHeader_ release];
    informationHeader_ = nil;
}

- (void)dealloc
{
    [acceptButton_ release];
    acceptButton_ = nil;
    
    [informationHeader_ release];
    informationHeader_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
    [foOperationHelper_ release];
    foOperationHelper_ = nil;
    
    [sealDescriptionLabel_ release];
    sealDescriptionLabel_ = nil;
    
    [sealImage_ release];
    sealImage_ = nil;
    
    [legalTermsTopCell_ release];
    legalTermsTopCell_ = nil;
    
    [firstHeader_ release];
    firstHeader_ = nil;
    
    [secondHeader_ release];
    secondHeader_ = nil;
    
    [thirdHeader_ release];
    thirdHeader_ = nil;
    
    [super dealloc];
}

@end
