//
//  FrequentOperationStepOneViewController.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/7/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationBaseExecutionViewController.h"
#import "MOKStringListSelectionButton.h"

@class FrequentOperationHeaderView;
@class FOOperationHelper;
@class MOKStringListSelectionButton;
@class MOKCurrencyTextField;
@class MOKTextField;
@class SimpleHeaderView;
@interface FrequentOperationStepOneViewController : FrequentOperationBaseExecutionViewController <MOKStringListSelectionButtonDelegate, UITableViewDataSource, UITableViewDelegate>{
    /**
     * Label for the account selection
     */
    UILabel *accountLabel_;
    /**
     * Combo with accounts
     */
    MOKStringListSelectionButton *accountCombo_;
    /**
     * combo with accounts
     */
    MOKStringListSelectionButton *cellAccountCombo_;
    /**
     * Combo with cards
     */
    MOKStringListSelectionButton *cellCardCombo_;
    /**
     *  index of the card selected
     */
    NSInteger selectedIndex;
    /**
     * Label for the currency selection
     */
    UILabel *currencyLabel_;
    /**
     * Combo with currency
     */
    MOKStringListSelectionButton *currencyCombo_;
    /**
     * Label for the amount text field
     */
    UILabel *amountEnterLabel_;
    /**
     * Field to enter the amount for the operation
     */
    MOKCurrencyTextField *amounTextField_;
    /**
     * Label for the Reference
     */
    UILabel *referenceLabel_;
    /**
     * Field to enter the amount for the operation
     */
    MOKTextField *referenceTextField_;
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    /**
     * Header with the main information of the operation
     */
    FrequentOperationHeaderView *informationHeader_;
    /**
     * First Header for the view
     */
    SimpleHeaderView *firstHeader_;
    /**
     * Second Header for the view
     */
    SimpleHeaderView *secondHeader_;
    /**
     * Flag for card
     */
    BOOL hasCards_;
    /**
     *  Helper for the operation
     */
    FOOperationHelper *foOperationHelper_;
    /*
     * Table for account selection
     */
    UITableView *accountTypeTableView_;
}

/**
 * Provides readwrite access to the continue button. Exported to IB
 */
@property(nonatomic, readwrite, retain) IBOutlet UIButton *continueButton;
/**
 * Provides readwrite access to the account label. Exported to IB
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *accountLabel;
/**
 * Provides readwrite access to the account Combo. Exported to IB
 */
@property(nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *accountCombo;
/**
 * Provides readwrite access to the amount label. Exported to IB
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *currencyLabel;
/**
 * Provides readwrite access to the currency label. Exported to IB
 */
@property(nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *currencyCombo;
/**
 * Provides readwrite access to the amount label. Exported to IB
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *amountEnterLabel;
/**
 * Provides readwrite access to the amount text field. Exported to IB
 */
@property(nonatomic, readwrite, retain) IBOutlet  MOKCurrencyTextField *amounTextField;
/**
 * Provides readwrite access to the reference label. Exported to IB
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *referenceLabel;
/**
 * Provides readwrite access to the reference text field. Exported to IB
 */
@property(nonatomic, readwrite, retain) IBOutlet  MOKTextField *referenceTextField;
/**
 * Provides read-write access to the table and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIImageView *brandingLine;
/**
 * Provides readwrite access to the information header. Exported to IB
 */
@property(nonatomic, readwrite, retain) FrequentOperationHeaderView *informationHeader;
/**
 *  Provides readwrite acces to the helper for the operation
 */
@property(nonatomic, readwrite, retain) FOOperationHelper *foOperationHelper;
/**
 * Provides readwrite access to the accountTypeTableView.
 */
@property (retain, nonatomic) UITableView *accountTypeTableView;
/**
 * Creates and returns an autoreleased FrequentOperationStepOneViewController constructed from a NIB file
 *
 * @return The autoreleased FrequentOperationStepOneViewController constructed from a NIB file
 */
+ (FrequentOperationStepOneViewController *)frequentOperationStepOneViewController;


@end
