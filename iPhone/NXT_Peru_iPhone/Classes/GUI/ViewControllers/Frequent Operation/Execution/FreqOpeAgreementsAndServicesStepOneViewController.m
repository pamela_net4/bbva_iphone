//
//  FreqOpeAgreementsAndServicesStepOneViewController.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FreqOpeAgreementsAndServicesStepOneViewController.h"

#import "accountPay.h"
#import "accountPayList.h"
#import "CardType.h"
#import "CardTypeList.h"
#import "Carrier.h"
#import "CarrierList.h"
#import "CheckComboCell.h"
#import "CheckPendingDocCell.h"
#import "FrequentOperationHeaderView.h"
#import "FrequentOperationBaseExecutionViewController+protected.h"
#import "FrequentOperationBaseExecutionViewController.h"
#import "FOOperationHelper.h"
#import "FOInstitutionsAndCompanies.h"
#import "InstitutionsAndCompaniesStepFourViewController.h"
#import "MOKStringListSelectionButton.h"
#import "MOKEditableViewController.h"
#import "MOKEditableViewController+protected.h"
#import "MOKCurrencyTextField.h"
#import "MOKTextView.h"
#import "NXTEditableViewController+protected.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTTextField.h"
#import "NXTTextView.h"
#import "NXTCurrencyTextField.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Ocurrency.h"
#import "OcurrencyList.h"
#import "TitleAndAttributes.h"
#import "PaymentInstitutionAndCompaniesConfirmationResponse.h"
#import "PaymentInstitutionAndCompaniesConfirmationInformationResponse.h"
#import "pendingDocumentList.h"
#import "PendingDocument.h"
#import "SimpleHeaderView.h"
#import "UINavigationItem+DoubleLabel.h"

#import "UIColor+BBVA_Colors.h"

#import "SMSCell.h"
#import "EmailCell.h"
#import "StringKeys.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "Updater.h"
#import "Tools.h"

/**
 * Defines the Nib file name
 */
#define NIB_FILE_NAME                                       @"FreqOpeAgreementsAndServicesStepOneViewController"

/**
 * Defines the interior view distance
 */
#define INTERIOR_VIEW_DISTANCE                      5.0f

/**
 * Defines the exterior view distance
 */
#define EXTERIOR_VIEW_DISTANCE                      10.0f

/**
 * Defines the text font size
 */
#define TEXT_FONT_SIZE_PAY                              13.0f

/**
 * Defines the text font size
 */
#define TEXT_PARTIAL_FONT_SIZE                              10.0f

/**
 * Defines the small text font size
 */
#define SMALL_TEXT_FONT_SIZE                        12.0f

/**
 * Defines the big text font size
 */
#define BIG_TEXT_FONT_SIZE                          17.0f

/**
 * Defines de max text lenght for the mail
 */
#define MAX_TEXT_MAIL_LENGHT                    80


@interface FreqOpeAgreementsAndServicesStepOneViewController ()


/**
 * Return an array with carriers
 */
-(NSArray*) carrierListString;

/**
 * Re-Accomodate the views
 */
- (void)layoutViews;

@end



@implementation FreqOpeAgreementsAndServicesStepOneViewController


@synthesize paymentDetailEntity;
@synthesize serviceType = serviceType_;
@synthesize foOperationHelper = foOperationHelper_;
@synthesize informationHeader = informationHeader_;

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    
}

/**
 * Creates and returns an autoreleased RechargeViewController constructed from a NIB file.
 *
 * @return The autoreleased RechargeViewController constructed from a NIB file.
 */
+ (FreqOpeAgreementsAndServicesStepOneViewController *)freqOpeAgreementsAndServicesStepOneViewController {
    
    FreqOpeAgreementsAndServicesStepOneViewController *result =  [[[FreqOpeAgreementsAndServicesStepOneViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
    
    [result awakeFromNib];
    
    return result;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[self view] setFrame:[[UIScreen mainScreen] bounds]];
    
    pendingDocumentsArray = [[NSArray alloc] initWithArray:paymentDetailEntity.pendingDocumentList.pendingDocumentList];
    
    brand = [[NSMutableArray alloc] initWithCapacity:[pendingDocumentsArray count]];
    
    for (int i = 0 ; i < [pendingDocumentsArray count]; i++) {
        [brand addObject:@"0"];
    }
    
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
    [[self brandingLine] setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    selectedIndex = -1;
    
    informationHeader_ = [[FrequentOperationHeaderView frequentOperationHeaderView] retain];
    
    arrayViews_ = [[NSMutableArray alloc] init];
    
    if (foOperationHelper_.originAccountList != nil) {
        [foOperationHelper_.originAccountList release];
        foOperationHelper_.originAccountList = nil;
    }
    
    foOperationHelper_.originAccountList = [[NSMutableArray alloc] initWithArray:[[paymentDetailEntity accountList] accountPayList]];
    
    if (foOperationHelper_.originCardList != nil) {
        [foOperationHelper_.originCardList release];
        foOperationHelper_.originCardList = nil;
    }
    
    if ([[[paymentDetailEntity cardList] cardTypeList] count] > 0) {
        foOperationHelper_.originCardList = [[NSMutableArray alloc] initWithArray:[[paymentDetailEntity cardList] cardTypeList]];
        [foOperationHelper_ setHasCard:TRUE];
    }else{
        [foOperationHelper_ setHasCard:FALSE];
    }
    
    [self loadControls];
    
    [NXT_Peru_iPhoneStyler styleAccountSelectionButton:cellAccountCombo_];
    [NXT_Peru_iPhoneStyler styleAccountSelectionButton:cellCardCombo_];
    
    [self lookForEditViews];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:TRUE];
    
    [self lookForEditViews];
    [self layoutViews];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:TRUE];
    
    [self layoutViews];
}

-(void) loadControls
{
    UIView *view = self.view;
    CGRect frame = view.frame;
    CGFloat width = CGRectGetWidth(frame);
    selectedIndex= -1;
    [NXT_Peru_iPhoneStyler styleNXTView:view];
    
    pendingDocumentList =[paymentDetailEntity pendingDocumentList];
    
    
    isDB =!([[pendingDocumentList pendingDocumentList] count] ==0);
    
    isPartial = [paymentDetailEntity partialVal];
    
    //First header
    firstHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [firstHeader_ setTitle:NSLocalizedString(FO_HEADER_TITLE_KEY, nil)];
    
    frame = [firstHeader_ frame];
    frame.origin.y = 0.0f;
    [firstHeader_ setFrame:frame];
    
    [[self scrollableView] addSubview:firstHeader_];
    
    frame = [informationHeader_ frame];
    frame.size.width = 320;
    //frame.origin.x = 10;
    frame.origin.y = [SimpleHeaderView height];
    frame.size.height = [informationHeader_ height];
    [informationHeader_ setFrame:frame];
    
    NSMutableArray *titleAndAttributesArray = [foOperationHelper_ foFirstStepInformation];
    
    TitleAndAttributes *titleAndAttributes = nil;
    for (int i = 0; i < [[[paymentDetailEntity ocurrencyList] ocurrencyList] count] ; i++) {
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        Ocurrency *ocurrency = [[[paymentDetailEntity ocurrencyList] ocurrencyList] objectAtIndex: i];
        [titleAndAttributes setTitleString: [Tools notNilString: [ocurrency description]]];
        if ([ocurrency valueCurrency] != nil ) {
            
            
            [titleAndAttributes addAttribute: [NSString stringWithFormat: @"%@%@",[ocurrency valueCurrency],[ocurrency value]]];
        } else {
            [titleAndAttributes addAttribute: [ocurrency value]];
        }
        if(titleAndAttributes != nil){
            [titleAndAttributesArray addObject: titleAndAttributes];
        }
    }
    
    [informationHeader_ setTitlesAndAttributeArray: titleAndAttributesArray];
    [informationHeader_ updateInformationView];
    
    [[self scrollableView] addSubview:informationHeader_];
    
    NSInteger heightReferential = CGRectGetMaxY([informationHeader_ frame]);
    
    heightReferential += 10;
    
    if (isDB) {
        
        //next section
        SimpleHeaderView *simpleSecondHeaderView = [SimpleHeaderView simpleHeaderView] ;
        [simpleSecondHeaderView setTitle:NSLocalizedString(PAYMENT_SELECT_RECEIPT_TO_CANCEL_TEXT_KEY, nil)];
        
        [simpleSecondHeaderView setFrame:CGRectMake(0, heightReferential, width, CGRectGetHeight([simpleSecondHeaderView frame]) )];
        
        [[self scrollableView] addSubview:simpleSecondHeaderView];
        [arrayViews_ addObject:simpleSecondHeaderView];
        
        heightReferential += CGRectGetHeight([simpleSecondHeaderView frame]);
        
        UITableView *selectionDocsTableView = nil;
        
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            selectionDocsTableView = [[UITableView alloc]initWithFrame:CGRectMake(0,heightReferential-100 , width, 100) style:UITableViewStylePlain];
        }else{
            selectionDocsTableView = [[UITableView alloc]initWithFrame:CGRectMake(0,heightReferential-100 , width, 100) style:UITableViewStyleGrouped];
        }
        [selectionDocsTableView setTag:1];
        
        [NXT_Peru_iPhoneStyler styleTableView:selectionDocsTableView];
        
        
        [selectionDocsTableView setDataSource:self];
        [selectionDocsTableView setDelegate:self];
        [selectionDocsTableView setAllowsMultipleSelection:!isPartial];
        
        [selectionDocsTableView reloadData];
        
        CGSize sizeTable =  [selectionDocsTableView contentSize];
        
        [selectionDocsTableView setFrame:CGRectMake([selectionDocsTableView frame].origin.x
                                                , [selectionDocsTableView frame].origin.y, [selectionDocsTableView frame].size.width, sizeTable.height)];
        
        
        [[self scrollableView] addSubview:selectionDocsTableView];
        [arrayViews_ addObject:selectionDocsTableView];
        heightReferential += CGRectGetHeight([selectionDocsTableView frame]);
        
        if (isPartial) {
            
            SimpleHeaderView *amountTextHeader = [SimpleHeaderView simpleHeaderView] ;
            
            [amountTextHeader setTitle:NSLocalizedString(INSTITUTIONS_COMPANIES_PARTIAL_AMOUNT_KEY, nil)];
            
            [amountTextHeader setFrame:CGRectMake(0, heightReferential, width, CGRectGetHeight([amountTextHeader frame]) )];
            
            CGRect frame = [amountTextHeader.titleLabel frame];
            frame.size.height *= 2;
            frame.origin.y -= 20.0f;
            [amountTextHeader.titleLabel setFrame:frame];
            [amountTextHeader.titleLabel setNumberOfLines:2];
            [amountTextHeader.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
            [amountTextHeader setBackgroundColor:[UIColor clearColor]];
            
            [NXT_Peru_iPhoneStyler styleLabel:amountTextHeader.titleLabel withFontSize:14.0f color:[UIColor BBVABlueSpectrumColor]];
            [[self scrollableView] addSubview:amountTextHeader];
            [arrayViews_ addObject:amountTextHeader];
            
            heightReferential+=CGRectGetHeight([amountTextHeader frame]);
            
            currencyText = [[MOKCurrencyTextField alloc] init];
            [currencyText setFrame:CGRectMake(10, CGRectGetMaxY(amountTextHeader.frame), self.view.frame.size.width - 20, 30)];
            [NXT_Peru_iPhoneStyler styleMokTextField:currencyText withFontSize:14.0f andColor:[UIColor BBVAGreyColor]];
            [currencyText setPlaceholder:NSLocalizedString(INSTITUTIONS_COMPANIES_CONFIRMATION_AMOUNT_KEY, nil)];
            [currencyText setText:@""];
            [currencyText setBorderStyle:UITextBorderStyleRoundedRect];
            currencyText.canContainCents = YES;
            currencyText.maxDecimalNumbers = 2;
            currencyText.currencySymbol = CURRENCY_SOLES_SYMBOL;
            [currencyText setKeyboardType:UIKeyboardTypeNumberPad];
            [currencyText setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
            [[self scrollableView] addSubview:currencyText];
            [arrayViews_ addObject:currencyText];
            
            heightReferential += CGRectGetHeight([currencyText frame]);
            
        }
    }
        
    SimpleHeaderView *simpleThirdHeaderView = [SimpleHeaderView simpleHeaderView] ;
    
    [simpleThirdHeaderView setTitle:NSLocalizedString(PAYMENT_SELECT_PAYMENT_MODE_KEY, nil)];
    
    [simpleThirdHeaderView setFrame:CGRectMake(0, heightReferential, width, [SimpleHeaderView height] )];
    
    
    [[self scrollableView] addSubview:simpleThirdHeaderView];
    [arrayViews_ addObject:simpleThirdHeaderView];
    
    heightReferential = CGRectGetMaxY([simpleThirdHeaderView frame]);
    
    CGFloat accountTableHeight = 150.0f;
    
    if ([foOperationHelper_ hasCard]) {
        
        UITableView *accountTypeTableView = nil;
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            
            accountTypeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,heightReferential-50, width, accountTableHeight-15.0f) style:UITableViewStylePlain];
        }else{
            accountTypeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,heightReferential , width, accountTableHeight) style:UITableViewStyleGrouped];
        }
        
        [accountTypeTableView setTag:2];
        
        [NXT_Peru_iPhoneStyler styleTableView:accountTypeTableView];
        
        [accountTypeTableView setDataSource:self];
        [accountTypeTableView setDelegate:self];
        [accountTypeTableView setScrollEnabled:FALSE];
        
        [accountTypeTableView setScrollEnabled:FALSE];
        
        [[self scrollableView] addSubview:accountTypeTableView];
        [arrayViews_ addObject:accountTypeTableView];
        
        [self setScrollContentSize:CGSizeMake(width, heightReferential+ CGRectGetHeight([accountTypeTableView frame]))];
        
        heightReferential += [accountTypeTableView frame].size.height ;
    } else {
        
        accountTableHeight = 110;
        
        if (accountsCombo == nil) {
            accountsCombo = [[MOKStringListSelectionButton alloc] initWithFrame:CGRectMake(20, heightReferential + 10, 280, 44)];
        }
        [NXT_Peru_iPhoneStyler styleStringListButton:accountsCombo];
        [NXT_Peru_iPhoneStyler styleAccountSelectionButton:accountsCombo];
        [accountsCombo setOptionStringList:[foOperationHelper_ accountStringList]];
        [accountsCombo setStringListSelectionButtonDelegate:self];
        [accountsCombo setSelectedIndex:0];
        [accountsCombo setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECT_ACCOUNT_TEXT_KEY, nil)];
        
        [self setScrollContentSize:CGSizeMake(width, heightReferential+ CGRectGetHeight([accountsCombo frame]))];
        
        [[self scrollableView] addSubview:accountsCombo];
        [arrayViews_ addObject:accountsCombo];
        
        heightReferential = CGRectGetMaxY([accountsCombo frame]) + 10;
        
    }
    
    heightReferential+= [self.selectionTableView frame].size.height;
    
    [self.continueButton setFrame:CGRectMake(10, heightReferential, self.view.frame.size.width - 20, 50)];
    
    [self setScrollContentSize:CGSizeMake(width, heightReferential + CGRectGetHeight([self.continueButton frame]) + 100)];
    
    [self setScrollContentSize:CGSizeMake(320.0f, heightReferential)];
    [self lookForEditViews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)layoutViews
{
    if ([self isViewLoaded]) {
        
        CGRect frame = [firstHeader_ frame];
        frame.origin.y = 0;
        [firstHeader_ setFrame:frame];
        
        CGFloat auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        frame = [informationHeader_ frame];
        frame.origin.y = auxPos;
        [informationHeader_ setFrame:frame];
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        
        for (UIView *aView in arrayViews_) {
            
            frame = [aView frame];
            frame.origin.y = auxPos;
            [aView setFrame:frame];
            
            auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_NEAR_ELEMENT;
        }
        
        UITableView *selectionTableView = [self selectionTableView];
        [selectionTableView reloadData];
        CGSize contentSize = selectionTableView.contentSize;
        frame = selectionTableView.frame;
        CGFloat height = contentSize.height;
        frame.size.height = height;
        frame.origin.y = auxPos;
        selectionTableView.frame = frame;
        selectionTableView.scrollEnabled = NO;
        
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT2;
        
        UIButton *button = self.continueButton;
        frame = button.frame;
        frame.origin.y = auxPos;
        button.frame = frame;
        
        auxPos = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT2;
        
        [self setScrollContentSize:CGSizeMake(320.0f, auxPos + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT2)];
        
        [self lookForEditViews];
        
    }
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]);
    [self setScrollFrame:frame];
    
}

#pragma mark -
#pragma mark Notification Responses
- (void)initialFrequentOperationResponse:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[foOperationHelper_ notificationConfirmationKey] object:nil];
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (![response isError]) {
        if ([self.foOperationHelper fOResponseReceived:response]) {
            
            [self displayTransferSecondStepView];
            
        } else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
            
        }
    }
    
}

#pragma mark -
#pragma mark General Properties
/**
 * Returns the transfer operation helper associated to the view controller. Returns the transfer between accounts helper
 *
 * @return The transfer operation helper associated to the view controller
 */
- (FOOperationHelper *)fOOperationHelper {
    
    return (FOOperationHelper *)foOperationHelper_;
    
}

#pragma mark -
#pragma mark UITableDelegate / Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([tableView tag] == 1)
    {
        return [[pendingDocumentList pendingDocumentList] count];
    }
    else if ([tableView tag] == 2 )
    {
        if ([foOperationHelper_ hasCard]) {
            return 2;
        }else{
            return 1;
        }
    }
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView tag]==1)
    {
        return 100;
        
    }else if( [tableView tag] == 2){
     return  [CheckComboCell cellHeightCheckOn:(selectedIndex == indexPath.row)];
    }
    
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([tableView tag] == 1)
    {
        CheckPendingDocCell *tableviewCell  = [tableView dequeueReusableCellWithIdentifier:[CheckPendingDocCell cellIdentifier]];
        [tableviewCell setTag:[indexPath row]];
        
        PendingDocument *pendingDocument = [[pendingDocumentList pendingDocumentList] objectAtIndex:indexPath.row];
        
        if(tableviewCell == nil)
        {
            
            tableviewCell = [[CheckPendingDocCell checkPendingDocCell] retain];
            
            [tableviewCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            [tableviewCell accomodateForDocumentType:isPartial];
            
            if (isPartial) {
                
                [tableviewCell.mainLabel setText:[Tools capitalizeFirstWordOnly: pendingDocument.description]];
                [tableviewCell.minAmount setText:[NSString stringWithFormat:@"%@%@", pendingDocument.minAmountCurrency, pendingDocument.minAmount]];
                [tableviewCell.maxAmount setText:[NSString stringWithFormat:@"%@%@", pendingDocument.maxAmountCurrency, pendingDocument.maxAmount]];
                
            }else{
                [tableviewCell.firstTitle setText:[Tools capitalizeFirstWordOnly: pendingDocument.description]];
                [tableviewCell.mainDetail setText:[NSString stringWithFormat:@"%@%@", pendingDocument.totalAmountCurrency, pendingDocument.totalAmount]];
            }
            
            [tableviewCell.expirationDate setText:[pendingDocument expirationDate]];
            [tableviewCell setBackgroundColor:[UIColor whiteColor]];
            
            
            
            return tableviewCell;
        }
    }
    else if([tableView tag] == 2)
    {
        CheckComboCell *resultAux = (CheckComboCell *)[tableView dequeueReusableCellWithIdentifier:[CheckComboCell cellIdentifier]];
        
        if (resultAux == nil) {
            resultAux = [CheckComboCell checkComboCell];
            [[resultAux combo] setStringListSelectionButtonDelegate:self];
        }
        //cuentas
        if (indexPath.row == 0) {
            
            [resultAux applyAccountSelectionStyle:YES];
            
            [[resultAux topTextLabel] setText:NSLocalizedString(IAC_ACCOUNT_TITLE_TEXT_KEY, nil)];
            
            NSMutableArray *stringsArray = [[NSMutableArray alloc] initWithArray:[foOperationHelper_ accountStringList]];
            
            [cellAccountCombo_ release];
            cellAccountCombo_ = nil;
            
            cellAccountCombo_ = [[resultAux combo] retain];
            
            if (cellAccountCombo_ == cellCardCombo_) {
                
                [cellCardCombo_ release];
                cellCardCombo_ = nil;
                
            }
            
            [cellAccountCombo_ setOptionStringList:stringsArray];
            [cellAccountCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECT_ACCOUNT_TEXT_KEY, nil)];
            [cellAccountCombo_ setStringListSelectionButtonDelegate:self];
            [cellAccountCombo_ setSelectedIndex:0];
            [resultAux setCheckActive:(selectedIndex == indexPath.row)];
            
        } else {
            
            [resultAux applyAccountSelectionStyle:NO];
            
            [[resultAux topTextLabel] setText:NSLocalizedString(PAYMENT_CARDS_TITLE_TEXT_KEY, nil)];
            
            NSMutableArray *stringsArray = [[NSMutableArray alloc] initWithArray:[foOperationHelper_ cardStringList]];
            
            [cellCardCombo_ release];
            cellCardCombo_ = nil;
            
            cellCardCombo_ = [[resultAux combo] retain];
            
            if (cellCardCombo_ == cellAccountCombo_) {
                
                [cellAccountCombo_ release];
                cellAccountCombo_ = nil;
                
            }
            
            [cellCardCombo_ setOptionStringList:stringsArray];
            [cellCardCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECT_CARD_TEXT_KEY, nil)];
            [cellCardCombo_ setStringListSelectionButtonDelegate:self];
            [cellCardCombo_ setSelectedIndex:0];
            [resultAux setCheckActive:(selectedIndex == indexPath.row)];
            
        }
        
        [self lookForEditViews];
        
        return resultAux;
        
    }
    
    [self lookForEditViews];
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView tag] == 1) {
        
        CheckPendingDocCell *cell = (CheckPendingDocCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        if (isPartial) {
            
            if (![cell isSelectedDoc]) {
                [brand replaceObjectAtIndex:indexPath.row withObject:@"0"];
            }else{
                [brand replaceObjectAtIndex:indexPath.row withObject:@"1"];
                
                
                PendingDocument *pendDoc = [pendingDocumentsArray objectAtIndex:indexPath.row];
                
                currencyText.currencySymbol = pendDoc.maxAmountCurrency;
                
            }
        }else{
            [brand replaceObjectAtIndex:indexPath.row withObject:@"1"];
            
        }
        
        if (currencyText != nil) {
            [currencyText resignFirstResponder];
        }
        
    }else if(tableView.tag == 2)
    {
        
        if (selectedIndex == indexPath.row) {
            return;
        }
        if (indexPath.row == 0) {
            selectedAccount = [[[paymentDetailEntity accountList] accountPayList] objectAtIndex:0];
            [foOperationHelper_ setSelectedOriginAccountIndex:0];
            [foOperationHelper_ setSelectedOriginCardIndex:NSNotFound];
        }else{
            NSArray *cardArray = [[paymentDetailEntity cardList] cardTypeList];
            
            if ([cardArray count] > 0) {
                selectedCard = [cardArray objectAtIndex:0];
            }
            
            [foOperationHelper_ setSelectedOriginCardIndex:0];
            
            [foOperationHelper_ setSelectedOriginAccountIndex:NSNotFound];
        }
        selectedIndex = indexPath.row;
        [tableView reloadData];
    }
    
    [self lookForEditViews];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView tag] == 1) {
        
        [brand replaceObjectAtIndex:indexPath.row withObject:@"0"];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && ([tableView tag] == 1 || [tableView tag] == 2) ){
        return 0.01f;
    } else if ([tableView tag] == 1 || [tableView tag] == 2) {
        return 0.0;
    }
    
    return [super tableView:tableView heightForHeaderInSection:section];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if ([tableView tag] == 1 || [tableView tag] == 2) {
        return nil;
    }
    
    return [super tableView:tableView viewForHeaderInSection:section];
}

#pragma mark -
#pragma mark utility methods
-(NSArray*) carrierListString
{
    CarrierList *carrierList = [paymentDetailEntity carrierList];
    NSArray *carrierArray = [carrierList carrierList];
    NSMutableArray *carrierStringArray  = [[NSMutableArray alloc]init];
    
    for (Carrier *aCarrier in carrierArray)
    {
        [carrierStringArray addObject: aCarrier.description];
    }
    
    return  carrierStringArray;
}

#pragma mark -
#pragma mark  MOKStringListSelectionButtonDelegate

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton
{
    if (selectedIndex == 0 && stringListSelectionButton == cellAccountCombo_) {

        [foOperationHelper_ setSelectedOriginAccountIndex:stringListSelectionButton.selectedIndex];
        [foOperationHelper_ setSelectedOriginCardIndex:NSNotFound];
        
    }else if (selectedIndex == 1 && stringListSelectionButton == cellCardCombo_){

        [foOperationHelper_ setSelectedOriginCardIndex:stringListSelectionButton.selectedIndex];
        [foOperationHelper_ setSelectedOriginAccountIndex:NSNotFound];
        
    } else if (stringListSelectionButton == accountsCombo){

        [foOperationHelper_ setSelectedOriginAccountIndex:stringListSelectionButton.selectedIndex];
        [foOperationHelper_ setSelectedOriginCardIndex:NSNotFound];
    }
    
}

#pragma mark -
#pragma mark user interaction
-(void)selectionButton:(id)sender
{
    UIButton *buttonSelected = ((UIButton*)sender);
    [buttonSelected setSelected: ![buttonSelected isSelected]];
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Resets the internal information.
 */
- (void)resetInformation {
 
    [foOperationHelper_ setAmountString:@""];
    [foOperationHelper_ setBrand:[NSArray array]];
    [foOperationHelper_ setPendingDocumentsArray:[NSArray array]];
    [foOperationHelper_ setIsPartial:FALSE];
    [foOperationHelper_ setIsDB:FALSE];
    selectedAccount = nil;
    selectedCard = nil;
     [foOperationHelper_ setSelectedOriginAccountIndex:NSNotFound];
     [foOperationHelper_ setSelectedOriginCardIndex:NSNotFound];
    [super resetInformation];
}

/**
 * Stores the information into the transfer operation helper instance
 */
- (void)storeInformationIntoHelper {
    
    [foOperationHelper_ setAmountString:currencyText.text];
    [foOperationHelper_ setBrand:brand];
    [foOperationHelper_ setPendingDocumentsArray:pendingDocumentsArray];
    [foOperationHelper_ setIsPartial:isPartial];
    [foOperationHelper_ setIsDB:isDB];
    
    [super storeInformationIntoHelper];
}

/**
 * Update the currency symbol on amount text field when user changes currency
 *
 * @param currency The currency name to set
 * @private
 */
- (void)updateAmountTextFieldWithCurreny:(NSString *)currency{
    
    if ([[currency lowercaseString] isEqualToString:[NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil) lowercaseString]]){
        
        currencyText.currencySymbol = CURRENCY_SOLES_SYMBOL;
        
    } else {
        
        currencyText.currencySymbol = CURRENCY_DOLARES_SYMBOL;
        
    }
    
}

#pragma mark -
#pragma mark UITextFieldDelegate protocol selectors
/**
 * Asks the delegate edited chars should be accepted.
 *
 * @param textField The text field for which editing is about to begin.
 * @param range The text field range.
 * @param string The replacement string.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:string];
    NSUInteger resultLength = [resultString length];
    
    if(textField == currencyText) {
        
        if (resultLength == 0) {
            
            result = YES;
            
        } else {
            
            NSScanner *scanner = [NSScanner scannerWithString:resultString];
            NSInteger resultInteger = 0;
            
            if (([scanner scanInteger:&resultInteger]) && ([scanner isAtEnd])) {
                
                if (resultInteger > 0) {
                    
                    result = YES;
                }
            }
        }
    } else{
        result = [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    
    return result;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [currencyText resignFirstResponder];
    return FALSE;
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    NSString *title = @"";
    
    if ([@"SERV" isEqualToString:paymentDetailEntity.payType])
    {
        title=  [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil),
                 NSLocalizedString(PAYMENT_PUBLIC_SERVICES_TITLE_TEXT_KEY, nil)];
    }
    else
    {
        
        title= [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PAYMENT_INSTITUTE_COMPANIE_TITLE_TEXT_LOWER_KEY, nil)];
    }
    title=[NSString stringWithFormat:NSLocalizedString(FO_POSITION_TITLE_TEXT_KEY, nil)];
    result.mainTitle = title;
    
    return result;
    
}
#pragma mark -
#pragma mark Memory management

- (void)dealloc
{
    if (selectedAccount) {
        [selectedAccount release];
        selectedAccount = nil;
    }
    
    if (selectedCard) {
        [selectedCard release];
        selectedCard = nil;
    }
    
    serviceType_ = nil;
    
    [arrayViews_ release];
    arrayViews_ = nil;
    
    [super dealloc];
}

@end
