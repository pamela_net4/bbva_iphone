//
//  FrequentOperationStepTwoViewController.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/14/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "MOKEditableViewController.h"
#import "LegalTermsTopCell.h"

@class FOOperationHelper;
@class FrequentOperationHeaderView;
@class SimpleHeaderView;
@class TermsAndConsiderationsViewController;
@interface FrequentOperationStepTwoViewController : MOKEditableViewController<LegalTermsTopCellDelegate>{
    
    /**
     * button to start the operation
     */
    UIButton *acceptButton_;
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    /**
     * seal image
     */
    UIImageView *sealImage_;
    /**
     * Description for the seal Image
     */
    UILabel *sealDescriptionLabel_;
    /**
     * Legal terms cell
     */
    LegalTermsTopCell *legalTermsTopCell_;
    
    /**
     * Terms And Considerations View Controller
     */
    TermsAndConsiderationsViewController *termsAndConsiderationsViewController_;
    /**
     * Header with the main information of the operation
     */
    FrequentOperationHeaderView *informationHeader_;
    /**
     *  Helper for the operation
     */
    FOOperationHelper *foOperationHelper_;
    /**
     * First Header for the view
     */
    SimpleHeaderView *firstHeader_;
    /**
     * Second Header for the view
     */
    SimpleHeaderView *secondHeader_;
    /**
     * Second Header for the view
     */
    SimpleHeaderView *thirdHeader_;
    
    
    UILabel *itfMessageLabel_;

}

/**
 * Provides readwrite access to the continue button. Exported to IB
 */
@property(nonatomic, readwrite, retain) IBOutlet UIButton *acceptButton;
/**
 * Provides read-write access to the Brand Image and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIImageView *brandingLine;
/**
 * Provides read-write access to the seal image and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIImageView *sealImage;
/**
 * Provides read-write access to the seal description label and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UILabel *sealDescriptionLabel;
/**
 * Provides readwrite access to the information header. Exported to IB
 */
@property(nonatomic, readwrite, retain) FrequentOperationHeaderView *informationHeader;
/**
 *  Provides readwrite acces to the helper for the operation
 */
@property(nonatomic, readwrite, retain) FOOperationHelper *foOperationHelper;

/**
 * Creates and returns an autoreleased FrequentOperationStepTwoViewController constructed from a NIB file
 *
 * @return The autoreleased FrequentOperationStepTwoViewController constructed from a NIB file
 */
+ (FrequentOperationStepTwoViewController *)frequentOperationStepTwoViewController;
/**
 * Method to call the service when the user toch the accept button
 *
 */
- (IBAction)onTapAcceptButton:(id)sender;

@end
