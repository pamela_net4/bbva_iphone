//
//  FrequentOperationBaseExecutionViewController.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/26/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "MOKEditableViewController.h"
#import "SMSMokCell.h"
#import "EmailMokCell.h"
#import <AddressBookUI/AddressBookUI.h>
#import "MOKStringListSelectionButton.h"

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        15.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                      12.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLEST_SIZE                                     10.0f

/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_NEAR_ELEMENT                           5.0f

/**
 * Defines the vertical gap between two far away elements
 */
#define VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT                       10.0f


//Forward declarations
@class FrequentOperationStepTwoViewController;
@class SMSMokCell;
@class EmailMokCell;


/**
 * Base view controller for the first transfer steps. It provides a basic behaviour and elements
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface FrequentOperationBaseExecutionViewController : MOKEditableViewController <UITableViewDataSource, UITableViewDelegate, SMSMokCellDelegate, EmailMokCellDelegate, ABPeoplePickerNavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate, MOKStringListSelectionButtonDelegate> {
    
@private
    
    /**
     * Container view to contain the editable components
     */
    UIView *containerView_;
    
    /**
     * Last editing view
     */
    UIView *lastEditingView_;
    
    /**
     * Selection table view
     */
    UITableView *selectionTableView_;
	
	/**
     * Send button
     */
    UIButton *continueButton_;
    
    /**
     * Advise bottom info label
     */
    UILabel *bottomInfoLabel_;
    
    /**
     * Separator image view
     */
    UIImageView *separator_;
    
    /**
     * Branding image view
     */
    UIImageView *brandingImageView_;
    
    /**
     * Adding first contact flag
     */
    BOOL addingFirstSMS_;
    
    /**
     * Adding second contact flag
     */
    BOOL addingSecondSMS_;
    
    /**
     * Adding first email flag
     */
    BOOL addingFirstEmail_;
    
    /**
     * Adding first email flag
     */
    BOOL addingSecondEmail_;
    
    /**
     * SMS Cell
     */
    SMSMokCell *smsCell_;
    
    /**
     * Email Cell
     */
    EmailMokCell *emailCell_;
    
    /**
     * Transfers step two view controller
     */
    FrequentOperationStepTwoViewController *foStepTwoViewController_;
    
    /**
     * Adding first email flag
     */
    BOOL hasNavigateForward_;
}

/**
 * Provides readwrite access to container view to contain the editable components and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *containerView;


/**
 * Provides readwrite access to the selection table. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *selectionTableView;

/**
 * Provides readwrite access to the transfer button. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *continueButton;

/**
 * Provides readwrite access to the bottom info label.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *bottomInfoLabel;

/**
 * Provides readwrite access to the separator image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;

/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) EmailMokCell *emailCell;

/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) SMSMokCell *smsCell;

/**
 * Provides readwrite access to the branding image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, assign) BOOL hasNavigateForward;


/**
 * Resets the internal information.
 */
- (void)resetInformation;

/**
 * Invoked by framework when tranfer button is tapped. The information is stored into the transfer operation helper instance and the
 * information validity is checked. When everithing is OK, the server confirm operation is invoked
 *
 * @private
 */
- (void)onTapContinueButton;

@end
