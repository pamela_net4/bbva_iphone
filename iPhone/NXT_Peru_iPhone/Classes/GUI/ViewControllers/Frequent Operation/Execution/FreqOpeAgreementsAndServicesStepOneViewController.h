//
//  FreqOpeAgreementsAndServicesStepOneViewController.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/28/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationBaseExecutionViewController.h"

@class accountPay;
@class CardType;
@class CheckComboCell;
@class FOInstitutionsAndCompanies;
@class FrequentOperationHeaderView;
@class MOKCurrencyTextField;
@class PaymentInstitutionAndCompaniesConfirmationResponse;
@class PaymentInstitutionAndCompaniesConfirmationInformationResponse;
@class PendingDocumentList;
@class SimpleHeaderView;

@interface FreqOpeAgreementsAndServicesStepOneViewController : FrequentOperationBaseExecutionViewController
<UITableViewDataSource,UITableViewDelegate,MOKStringListSelectionButtonDelegate, UITextFieldDelegate>
{
    PendingDocumentList *pendingDocumentList;
    
    /**
     *  index of the card selected
     */
    int selectedIndex;
    
    /**
     *  When the payment is partial or not
     */
    BOOL isPartial;
    
    /**
     *  When the payment is with BD or no
     */
    BOOL isDB;
    
    /**
     * Amount field
     */
    MOKCurrencyTextField *currencyText;
    
    /**
     * Combo with accounts
     */
    MOKStringListSelectionButton *accountsCombo;
    
    /**
     *  Array to know the selcteds documents
     */
    NSMutableArray *brand;
    
    /**
     *  array with the pending documents
     */
    NSArray *pendingDocumentsArray;
    
    /**
     *  response
     */
    PaymentInstitutionAndCompaniesConfirmationInformationResponse *responseConfirmation_;

    /**
     *  Card to pay
     */
    CardType *selectedCard;
    
    /**
     *  Account to pay
     */
    accountPay *selectedAccount;
    
    /**
     *  combo button with accounts
     */
    MOKStringListSelectionButton *cellAccountCombo_;
    
    /**
     *  combo button with cards
     */
    MOKStringListSelectionButton *cellCardCombo_;
    
    /**
     * Service name
     */
    NSString *serviceType_;

    /**
     *  Helper for the operation
     */
    FOInstitutionsAndCompanies *foOperationHelper_;
    /**
     * Header with the main information of the operation
     */
    FrequentOperationHeaderView *informationHeader_;
    /**
     * First Header for the view
     */
    SimpleHeaderView *firstHeader_;
    /**
     * array With editable views
     */
    NSMutableArray *arrayViews_;
}

/**
 *  Entity with the response information
 */
@property (nonatomic, readwrite, retain)  PaymentInstitutionAndCompaniesConfirmationResponse *paymentDetailEntity;

/**
 * Provides readwrite Bottom border IBOutlet
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingLine;

/**
 * Provides readwrite access to the service type name
 */
@property (nonatomic, readwrite, retain) NSString *serviceType;

/**
 *  Provides readwrite acces to the helper for the operation
 */
@property(nonatomic, readwrite, retain) FOInstitutionsAndCompanies *foOperationHelper;
/**
 * Provides readwrite access to the information header. Exported to IB
 */
@property(nonatomic, readwrite, retain) FrequentOperationHeaderView *informationHeader;

/**
 * Creates and returns an autoreleased InstitutionsAndCompaniesStepThirdViewController constructed from a NIB file.
 *
 * @return The autoreleased InstitutionsAndCompaniesStepThirdViewController constructed from a NIB file.
 */
+ (FreqOpeAgreementsAndServicesStepOneViewController *)freqOpeAgreementsAndServicesStepOneViewController;

@end
