//
//  FrequentOperationStepThreeViewController.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/17/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "MOKEditableViewController.h"

@class FOOperationHelper;
@class SimpleHeaderView;
@class FrequentOperationHeaderView;

@interface FrequentOperationStepThreeViewController : MOKEditableViewController{
    
    /**
     * button to return to the frequent operation tab main
     */
    UIButton *frequentOperationButton_;
    /**
     * Branding line
     */
    UIImageView *brandingLine_;
    /**
     * Header with the main information of the operation
     */
    FrequentOperationHeaderView *informationHeader_;
    /**
     *  Helper for the operation
     */
    FOOperationHelper *foOperationHelper_;
    /**
     * First Header for the view
     */
    SimpleHeaderView *firstHeader_;
    
    UILabel *itfMessageLabel_;
  
    UILabel *notificationLabel_;
    
    UILabel *confirmationLabel_;
}

/**
 * Provides readwrite access to the continue button. Exported to IB
 */
@property(nonatomic, readwrite, retain) IBOutlet UIButton *frequentOperationButton;
/**
 * Provides read-write access to the Brand Image and exports it to the IB
 */
@property (retain,readwrite, nonatomic) IBOutlet UIImageView *brandingLine;
/**
 * Provides readwrite access to the information header. Exported to IB
 */
@property(nonatomic, readwrite, retain) FrequentOperationHeaderView *informationHeader;
/**
 *  Provides readwrite acces to the helper for the operation
 */
@property(nonatomic, readwrite, retain) FOOperationHelper *foOperationHelper;

/**
 * Creates and returns an autoreleased FrequentOperationStepTwoViewController constructed from a NIB file
 *
 * @return The autoreleased FrequentOperationStepTwoViewController constructed from a NIB file
 */
+ (FrequentOperationStepThreeViewController *)frequentOperationStepThreeViewController;
/**
 * Method to Back to the main frequen operation view
 *
 */
- (IBAction)onTapFOButton:(id)sender;

@end
