//
//  FrequentOperationStepOneViewController.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/7/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationStepOneViewController.h"

#import "Constants.h"
#import "CheckComboCell.h"
#import "FrequentOperationHeaderView.h"
#import "FrequentOperationStepTwoViewController.h"
#import "FrequentOperationBaseExecutionViewController+protected.h"
#import "FOOperationHelper.h"
#import "FOTransferToThirdAccount.h"
#import "FOTransferToOtherBankAccount.h"
#import "FOPaymentCOtherBank.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKEditableViewController.h"
#import "MOKEditableViewController+protected.h"
#import "MOKStringListSelectionButton.h"
#import "MOKCurrencyTextField.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "SimpleHeaderView.h"
#import "StatusEnabledResponse.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "UINavigationItem+DoubleLabel.h"
#import "TransferTINOnlineStepViewController.h"
/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"FrequentOperationStepOneViewController"
/**
 * Define the font size for the titles
 */
#define FONT_SIZE                                                   14.0f

@interface FrequentOperationStepOneViewController ()
/**
 * Display the confirmation view for the operation
 */
- (void)displayTransferSecondStepView;
/**
 * Load the views to the scrollable view
 */
- (void)foLoadViews;
/**
 * Recieve the notification for the Frequent operation execution
 */
- (void)initialFrequentOperationResponse:(NSNotification *)notification;
/**
 * Relocate views
 */
- (void)layoutViews;

@end

@implementation FrequentOperationStepOneViewController

@synthesize accountCombo = accountCombo_;
@synthesize accountLabel = accountLabel_;
@synthesize accountTypeTableView = accountTypeTableView_;
@synthesize amountEnterLabel = amountEnterLabel_;
@synthesize amounTextField = amounTextField_;
@synthesize brandingLine = brandingLine_;
@synthesize currencyLabel = currencyLabel_;
@synthesize currencyCombo = currencyCombo_;
@synthesize foOperationHelper = foOperationHelper_;
@synthesize informationHeader = informationHeader_;
@synthesize referenceLabel = referenceLabel_;
@synthesize referenceTextField = referenceTextField_;

#pragma mark -
#pragma mark initialization
+ (FrequentOperationStepOneViewController *)frequentOperationStepOneViewController{
    return [[[FrequentOperationStepOneViewController alloc] initWithNibName:NIB_FILE_NAME bundle:nil] autorelease];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    informationHeader_ = [[FrequentOperationHeaderView frequentOperationHeaderView] retain];
    
    selectedIndex = 0;
    
    [NXT_Peru_iPhoneStyler styleLabel:amountEnterLabel_ withFontSize:FONT_SIZE color:[UIColor BBVABlackColor]];
    [amountEnterLabel_ setText:NSLocalizedString(FO_AMOUNT_TITLE_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleLabel:currencyLabel_ withFontSize:FONT_SIZE color:[UIColor BBVABlackColor]];
    [currencyLabel_ setText:NSLocalizedString(FO_CURRENCY_TITLE_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleLabel:accountLabel_ withFontSize:FONT_SIZE color:[UIColor BBVABlackColor]];
   if([self.foOperationHelper hasCard])
   {
    [accountLabel_ setText:NSLocalizedString(PAYMENT_SELECT_PAYMENT_MODE_KEY, nil)];
   }
   else
    {
        [accountLabel_ setText:NSLocalizedString(FO_CHARGE_ACCOUNT_TITLE_TEXT_KEY, nil)];
    }
    
    [brandingLine_ setImage:[[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]];
    
    //show reference field when needed
    if ([self.foOperationHelper foOperationType] != FOTETransferToOtherUserAccount) {
        [referenceLabel_ setHidden:TRUE];
        [referenceTextField_ setHidden:TRUE];
        
    }else{
        [NXT_Peru_iPhoneStyler styleLabel:referenceLabel_ withFontSize:FONT_SIZE color:[UIColor BBVABlackColor]];
        [referenceLabel_ setText:NSLocalizedString(REFERENCE_TEXT_KEY, nil)];
        [NXT_Peru_iPhoneStyler styleMokTextField:referenceTextField_ withFontSize:FONT_SIZE andColor:[UIColor BBVAGreyColor]];
        [referenceTextField_ setPlaceholder:NSLocalizedString(REFERENCE_HINT_TEXT_KEY, nil)];
    }
    
    //Show card option when needed
    if ([self.foOperationHelper hasCard]) {
        [accountCombo_ setHidden:TRUE];
        
        selectedIndex = -1;
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            accountTypeTableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0,0 , CGRectGetWidth([self.view frame]), 150) style:UITableViewStylePlain];
        }else
            accountTypeTableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0,0 , CGRectGetWidth([self.view frame]), 150) style:UITableViewStyleGrouped];
        
        [accountTypeTableView_ setTag:2];
        
        [NXT_Peru_iPhoneStyler styleTableView:accountTypeTableView_];
        
        [accountTypeTableView_ setDataSource:self];
        [accountTypeTableView_ setDelegate:self];
        [accountTypeTableView_ setScrollEnabled:FALSE];
        
        [[self scrollableView] addSubview:accountTypeTableView_];
        
    }else{
        [NXT_Peru_iPhoneStyler styleComboButton:accountCombo_];
        [NXT_Peru_iPhoneStyler styleAccountSelectionButton:accountCombo_];
        [accountCombo_ setOptionStringList:[foOperationHelper_ accountStringList]];
        [accountCombo_ setStringListSelectionButtonDelegate:self];
        [accountCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECT_ACCOUNT_TEXT_KEY, nil)];
        [accountCombo_ setSelectedIndex:-1];
    }
    
    [NXT_Peru_iPhoneStyler styleNXTView:self.view];
    [NXT_Peru_iPhoneStyler styleComboButton:currencyCombo_];
    [NXT_Peru_iPhoneStyler styleMokTextField:amounTextField_ withFontSize:FONT_SIZE andColor:[UIColor BBVAGreyColor]];
    
    [currencyCombo_ setStringListSelectionButtonDelegate:self];
    [currencyCombo_ setOptionStringList:[foOperationHelper_ currencyList]];
    [currencyCombo_ setSelectedIndex:0];
    
    
    
    
    NSString *selectedCurrency = [[foOperationHelper_ currencyList] objectAtIndex:[foOperationHelper_ selectedCurrencyIndex]];
    
    [amounTextField_ setCurrencySymbol:[Tools getCurrencySimbol:[Tools getCurrencyServer:selectedCurrency]]];
    [amounTextField_ setPlaceholder:NSLocalizedString(CARD_PAYMEN_AMOUNT_TO_PAY_TEXT_KEY, nil)];
    if ([foOperationHelper_ foOperationType] == FOTETransferCashMobile) {
        [amounTextField_ setCanContainCents:FALSE];
        [amounTextField_ setKeyboardType:UIKeyboardTypeNumberPad];
    }else{
        [amounTextField_ setCanContainCents:TRUE];
        [amounTextField_ setMaxDecimalNumbers:2];
        [amounTextField_ setKeyboardType:UIKeyboardTypeDecimalPad];
    }
    
    if ([foOperationHelper_ foOperationType] == FOTETransferCashMobile) {
        [[self selectionTableView] setHidden:TRUE];
        [[self selectionTableView] setDelegate:nil];
        [[self selectionTableView] setDataSource:nil];
        [[self selectionTableView] removeFromSuperview];
        self.selectionTableView = nil;
    }
    
    [self foLoadViews];
    
    [self layoutViews];
    
    [self lookForEditViews];
    
}

- (void)foLoadViews{
    
    CGRect frame = [informationHeader_ frame];
    frame.size.width = 320;
    frame.origin.x = 0;
    frame.size.height = [informationHeader_ height];
    [informationHeader_ setFrame:frame];
    
    [informationHeader_ setTitlesAndAttributeArray:[foOperationHelper_ foFirstStepInformation]];
    [informationHeader_ updateInformationView];
    
    [[self scrollableView] addSubview:informationHeader_];
    
    //First header
    firstHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [firstHeader_ setTitle:NSLocalizedString(FO_HEADER_TITLE_KEY, nil)];
    
    frame = [[firstHeader_ titleLabel] frame];
    frame.origin.y = -5.0f;
    [firstHeader_.titleLabel setFrame:frame];
    
    frame = [[firstHeader_ separator] frame];
    frame.origin.y = 30.0f;
    [firstHeader_.separator setFrame:frame];
    
    [[self scrollableView] addSubview:firstHeader_];
    
    //second header
    secondHeader_ = [[SimpleHeaderView simpleHeaderView] retain];
    [secondHeader_ setTitle:NSLocalizedString(FO_COMPLEMENTARY_DATA_TITLE_TEXT_KEY, nil)];
    
    frame = [[secondHeader_ titleLabel] frame];
    frame.origin.y = -5.0f;
    [secondHeader_.titleLabel setFrame:frame];
    
    frame = [[secondHeader_ separator] frame];
    frame.origin.y = 30.0f;
    [secondHeader_.separator setFrame:frame];
    
    [[self scrollableView] addSubview:secondHeader_];
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
    
    if([[foOperationHelper_ currencyList] count]<2)
    {
        [currencyLabel_ setHidden:YES];
        [currencyCombo_ setHidden:YES];
    }
    else
    {
        [currencyLabel_ setHidden:NO];
        [currencyCombo_ setHidden:NO];
        
    }
    
    [self layoutViews];
}

/**
 * Notifies the view controller that its view just laid out its subviews.
 */
- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    CGRect frame = [[self scrollableView] frame];
    frame.size.height = CGRectGetMinY([[self brandingLine] frame]);
    [self setScrollFrame:frame];
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Resets the internal information.
 */
- (void)resetInformation {
    
    foOperationHelper_.amountString = @"";
    foOperationHelper_.selectedCurrencyIndex = 0;
    foOperationHelper_.selectedOriginAccountIndex = -1;
    foOperationHelper_.hasCard = FALSE;
}

/**
 * Stores the information into the transfer operation helper instance
 */
- (void)storeInformationIntoHelper {
    
    FOOperationHelper *foOperationHelper = [self foOperationHelper];
    
    [foOperationHelper setAmountString:amounTextField_.text];
    [foOperationHelper_ setReference:referenceTextField_.text];
    
    [super storeInformationIntoHelper];
}

- (void)displayTransferTINStepView{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"vienedeFrecuente"];
    
    TransferTINOnlineStepViewController *stepTwoView = [TransferTINOnlineStepViewController transferTINOnlineStepViewController];
    stepTwoView.disclaimer = foOperationHelper_.optionalDisclaimer;
    [stepTwoView setFoOperationHelper:foOperationHelper_];
    
    [self.navigationController pushViewController:stepTwoView animated:TRUE];
}

- (void)displayTransferSecondStepView{
    
    FrequentOperationStepTwoViewController *stepTwoView = [FrequentOperationStepTwoViewController frequentOperationStepTwoViewController];
    [stepTwoView setFoOperationHelper:foOperationHelper_];
    
    [self.navigationController pushViewController:stepTwoView animated:TRUE];
}

/**
 * Returns the transfer operation helper associated to the view controller. Returns the transfer between accounts helper
 *
 * @return The transfer operation helper associated to the view controller
 */
- (FOOperationHelper *)fOOperationHelper {
    
    return foOperationHelper_;
    
}

#pragma mark -
#pragma mark user Interaction
/**
 * Method to call the service when the user toch the continue button
 */
- (void)onTapContinueButton{

    [super onTapContinueButton];
}

#pragma mark -
#pragma mark UITableDelegate / Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView tag] == 2) {
        return 2;
    }
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([tableView tag] == 2) {
        return 1;
    }
    return [super numberOfSectionsInTableView:tableView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( [tableView tag] == 2)
    {
        return  [CheckComboCell cellHeightCheckOn:(selectedIndex == indexPath.row)];
    }
    
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([tableView tag] == 2){
        
        CheckComboCell *resultAux = (CheckComboCell *)[tableView dequeueReusableCellWithIdentifier:[CheckComboCell cellIdentifier]];
        
        if (resultAux == nil) {
            resultAux = [CheckComboCell checkComboCell];
            [[resultAux combo] setStringListSelectionButtonDelegate:self];
        }
        //cuentas
        if (indexPath.row == 0) {
            
            [resultAux applyAccountSelectionStyle:YES];
            
            [[resultAux topTextLabel] setText:NSLocalizedString(IAC_ACCOUNT_TITLE_TEXT_KEY, nil)];
            
            NSMutableArray *stringsArray = [[NSMutableArray alloc] initWithArray:[foOperationHelper_ accountStringList]];
            
            [cellAccountCombo_ release];
            cellAccountCombo_ = nil;
            
            cellAccountCombo_ = [[resultAux combo] retain];
            
            if (cellAccountCombo_ == cellCardCombo_) {
                
                [cellCardCombo_ release];
                cellCardCombo_ = nil;
                
            }
            
            [cellAccountCombo_ setOptionStringList:stringsArray];
            [cellAccountCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECT_ACCOUNT_TEXT_KEY, nil)];
            [cellAccountCombo_ setStringListSelectionButtonDelegate:self];
            [cellAccountCombo_ setSelectedIndex:0];
            [resultAux setCheckActive:(selectedIndex == indexPath.row)];
            
        } else {
            
            [resultAux applyAccountSelectionStyle:NO];
            
            [[resultAux topTextLabel] setText:NSLocalizedString(PAYMENT_CARDS_TITLE_TEXT_KEY, nil)];
            
            NSMutableArray *stringsArray = [[NSMutableArray alloc] initWithArray:[foOperationHelper_ cardStringList]];
            
            [cellCardCombo_ release];
            cellCardCombo_ = nil;
            
            cellCardCombo_ = [[resultAux combo] retain];
            
            if (cellCardCombo_ == cellAccountCombo_) {
                
                [cellAccountCombo_ release];
                cellAccountCombo_ = nil;
                
            }
            
            [cellCardCombo_ setOptionStringList:stringsArray];
            [cellCardCombo_ setNoSelectionText:NSLocalizedString(CARD_PAYMENT_SELECT_CARD_TEXT_KEY, nil)];
            [cellCardCombo_ setStringListSelectionButtonDelegate:self];
            [cellCardCombo_ setSelectedIndex:0];
            [resultAux setCheckActive:(selectedIndex == indexPath.row)];
        }
        
        [self lookForEditViews];
        
        return resultAux;
        
    }
    
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 2)
    {
        
        if (selectedIndex == indexPath.row) {
            return;
        }
        if (indexPath.row == 0) {
            [cellAccountCombo_ setSelectedIndex:0];
            [foOperationHelper_ setSelectedOriginAccountIndex:0];
            [foOperationHelper_ setSelectedOriginCardIndex:-1];
            
            if (cellCardCombo_ != nil) {
                [cellCardCombo_ release];
                cellCardCombo_ = nil;
            }
            
        }else{
            
            if ([foOperationHelper_ hasCard]) {
                [cellCardCombo_ setSelectedIndex:0];
                [foOperationHelper_ setSelectedOriginAccountIndex:-1];
                [foOperationHelper_ setSelectedOriginCardIndex:0];
                
                if (cellAccountCombo_ != nil) {
                    [cellAccountCombo_ release];
                    cellAccountCombo_ = nil;
                }
            }
        }
        selectedIndex = indexPath.row;
        [tableView reloadData];
        
        [self layoutViews];
        [self lookForEditViews];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if ([tableView tag] != 2) {
        return [super tableView:tableView viewForHeaderInSection:section];
    }
    
    return nil;
}

- (CGFloat )tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if ([tableView tag] != 2) {
        return [super tableView:tableView heightForHeaderInSection:section];
    }
    return 0.0f;
}
#pragma mark -
#pragma mark  MOKStringListSelectionButtonDelegate

/**
 * Notifies the delegate that the selected string index was changed.
 *
 * @param stringListSelectionButton The MOKStringListSelectionButton triggering the event.
 */
- (void)stringListSelectionButtonSelectedIndexChanged:(MOKStringListSelectionButton *)stringListSelectionButton
{
    if (selectedIndex == 0 && (stringListSelectionButton == accountCombo_ ||
                               stringListSelectionButton == cellAccountCombo_)) {
        
        [foOperationHelper_ setSelectedOriginAccountIndex:stringListSelectionButton.selectedIndex];
        
    }else if (selectedIndex == 1 && stringListSelectionButton == cellCardCombo_){

        [foOperationHelper_ setSelectedOriginCardIndex:stringListSelectionButton.selectedIndex];
        
    } else if (stringListSelectionButton == currencyCombo_) {
        
        [foOperationHelper_ setSelectedCurrencyIndex:stringListSelectionButton.selectedIndex];
        
        NSString *selectedCurrency = [[foOperationHelper_ currencyList] objectAtIndex:[foOperationHelper_ selectedCurrencyIndex]];
        [amounTextField_ setCurrencySymbol:[Tools getCurrencySimbol:[Tools getCurrencyServer:selectedCurrency]]];
    }
    
}

#pragma mark -
#pragma mark TransfersStepOneViewController override methods


/**
 * sms switch has been tapped.
 */
- (void)switchButtonHasBeenTapped:(BOOL)on {
    
    [super switchButtonHasBeenTapped:on];
    
    [self lookForEditViews];
    
}

/**
 * More button has been tapped
 */
- (void)moreButtonHasBeenTapped {
    
    [super moreButtonHasBeenTapped];
    
    [self smsSecondTextFieldShow:YES];
    
    [self lookForEditViews];
}

/**
 * SMS Second text Field is shown.
 *
 * @param on The flag
 */
- (void)smsSecondTextFieldShow:(BOOL)on {
    
    [self lookForEditViews];
    
}

/**
 * Switch has been tapped.
 *
 * @param on The flag
 */
- (void)emailSwitchButtonHasBeenTapped:(BOOL)on {
    
    [super emailSwitchButtonHasBeenTapped:on];
    
    [self lookForEditViews];
}

/**
 * More button has been tapped
 */
- (void)emailMoreButtonHasBeenTapped {
    
    [super emailMoreButtonHasBeenTapped];
    
    [self emailSecondTextFieldShow:YES];
    
    [self lookForEditViews];
}

/**
 * Email Second text Field is shown.
 *
 * @param on The flag
 */
- (void)emailSecondTextFieldShow:(BOOL)on {
    
    [self lookForEditViews];
    
}


#pragma mark -
#pragma mark View style
/**
 * Relocate views
 */
- (void)layoutViews {
    
    CGFloat yPosition = 0.0f;
    CGFloat nearSeparation = 5.0f;
    CGFloat farSeparation = 10.0f;
    CGFloat tooFarSeparation = 20.0f;
    
    CGRect frame = [firstHeader_ frame];
    frame.origin.y = yPosition;
    frame.size.height = 30.0f;
    [firstHeader_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + nearSeparation;
    
    frame = [informationHeader_ frame];
    frame.origin.y = yPosition;
    [informationHeader_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + nearSeparation;
    
    frame = [secondHeader_ frame];
    frame.origin.y = yPosition;
    frame.size.height = 30.0f;
    [secondHeader_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + farSeparation;
    
    frame = [accountLabel_ frame];
    frame.origin.y = yPosition;
    frame.origin.x = 10.0f;
    [accountLabel_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + nearSeparation;
    
    if ([foOperationHelper_ hasCard]) {
        frame = [accountTypeTableView_ frame];
        frame.origin.y = yPosition;
        
        CGFloat height = 0.0f;
        
        if (selectedIndex == -1) {
            height = 110.0f;
        }else{
            height = 150.0f;
        }
        frame.size.height = height;
        [accountTypeTableView_ setFrame:frame];
    } else{
        frame = [accountCombo_ frame];
        frame.origin.y = yPosition;
        [accountCombo_ setFrame:frame];
    }
    
    yPosition = CGRectGetMaxY(frame) + farSeparation;
    
    if([[foOperationHelper_ currencyList] count]>=2)
    {
        
    frame = [currencyLabel_ frame];
    frame.origin.y = yPosition;
    frame.origin.x = 10.0f;
    [currencyLabel_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + nearSeparation;
    
     frame = [currencyCombo_ frame];
        frame.origin.y = yPosition;
        [currencyCombo_ setFrame:frame];
    
        yPosition = CGRectGetMaxY(frame) + farSeparation;
    }
    
    frame = [amountEnterLabel_ frame];
    frame.origin.y = yPosition;
    frame.origin.x = 10.0f;
    [amountEnterLabel_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + nearSeparation;
    
    frame = [amounTextField_ frame];
    frame.origin.y = yPosition;
    [amounTextField_ setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + tooFarSeparation;
    
    if ([self.foOperationHelper foOperationType] == FOTETransferToOtherUserAccount)
    {
        frame = [referenceLabel_ frame];
        frame.origin.y = yPosition;
        frame.origin.x = 10.0f;
        [referenceLabel_ setFrame:frame];
        
        yPosition = CGRectGetMaxY(frame) + nearSeparation;
        
        frame = [referenceTextField_ frame];
        frame.origin.y = yPosition;
        [referenceTextField_ setFrame:frame];
        
        yPosition = CGRectGetMaxY(frame) + tooFarSeparation;
    }

    if ([self.foOperationHelper foOperationType] != FOTETransferCashMobile)
    {
        UITableView *selectionTableView = self.selectionTableView;
        [selectionTableView reloadData];
        CGSize contentSize = selectionTableView.contentSize;
        frame = selectionTableView.frame;
        frame.origin.y = yPosition;
        CGFloat height = contentSize.height;
        frame.size.height = height;
        selectionTableView.frame = frame;
        selectionTableView.scrollEnabled = NO;
        yPosition = CGRectGetMaxY(frame) + VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT;
    }
    
    frame = [self.continueButton frame];
    frame.origin.y = yPosition;
    [self.continueButton setFrame:frame];
    
    yPosition = CGRectGetMaxY(frame) + tooFarSeparation;
    
    [self setScrollContentSize:CGSizeMake(320.0f, yPosition)];
    
    [self lookForEditViews];
}

#pragma mark -
#pragma mark Notification Responses
- (void)initialFrequentOperationResponse:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[foOperationHelper_ notificationConfirmationKey] object:nil];
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (![response isError]) {
        if ([self.foOperationHelper fOResponseReceived:response]) {
            
            if([self.foOperationHelper isKindOfClass:[FOTransferToOtherBankAccount class]]){
                
                if([((FOTransferToOtherBankAccount*)self.foOperationHelper).typeFlow isEqualToString:@"L"]){
                   
                  [self displayTransferSecondStepView];
                }else{
                    
                    [self  displayTransferTINStepView];
                }
                
            }else if([self.foOperationHelper isKindOfClass:[FOPaymentCOtherBank class]])
            {
                NSString *typeFlow =((FOPaymentCOtherBank*)self.foOperationHelper).typeFlow;
                
                [((FOPaymentCOtherBank*)self.foOperationHelper) setTypeFlow:typeFlow];
                
                if([typeFlow isEqualToString:@"L"]){
                    
                    [self displayTransferSecondStepView];


                }else{
                    [self  displayTransferTINStepView];
                }
            }
            else{
                [self displayTransferSecondStepView];
            }
            
        } else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
            
        }
    }
    
}


#pragma mark -
#pragma mark Notification Responses
- (void)nextStepFrequentOperationResponse:(NSNotification *)notification{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[foOperationHelper_ notificationConfirmationKey] object:nil];
    [self.appDelegate hideActivityIndicator];
    
    StatusEnabledResponse *response = [notification object];
    
    if (![response isError]) {
        if ([self.foOperationHelper fOResponseReceived:response]) {
            
              [self displayTransferSecondStepView];
        }else {
            
            [Tools showAlertWithMessage:NSLocalizedString(COMMUNICATION_ERROR_KEY, nil)];
            
        }
    }
}

#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 *
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    UINavigationItem *result = [super navigationItem];
    
    result.mainTitle = NSLocalizedString(FO_TITLE_TEXT_REACTIVE_KEY, nil);
    
    return result;
    
}

#pragma mark -
#pragma mark memory management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [informationHeader_ release];
    informationHeader_ = nil;
}

- (void)dealloc
{
    [accountCombo_ release];
    accountCombo_ = nil;
    
    [amounTextField_ release];
    amounTextField_ = nil;
    
    [accountTypeTableView_ release];
    accountTypeTableView_ = nil;
    
    [currencyCombo_ release];
    currencyCombo_ = nil;
    
    [informationHeader_ release];
    informationHeader_ = nil;
    
    [accountLabel_ release];
    accountLabel_ = nil;
    
    [currencyLabel_ release];
    currencyLabel_ = nil;
    
    [amountEnterLabel_ release];
    amountEnterLabel_ = nil;
    
    [brandingLine_ release];
    brandingLine_ = nil;
    
    [foOperationHelper_ release];
    foOperationHelper_ = nil;
    
    [referenceLabel_ release];
    referenceLabel_ = nil;
    
    [referenceTextField_ release];
    referenceTextField_ = nil;
    
    [super dealloc];
}

@end
