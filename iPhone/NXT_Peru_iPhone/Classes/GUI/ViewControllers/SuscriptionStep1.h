//
//  SuscriptionStep1.h
//  NXT_Peru_iPhone
//
//  Created by Canales on 22-02-16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "XMLReader.h"
@interface SuscriptionStep1 : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate,NSXMLParserDelegate>
{
    UITapGestureRecognizer* tapgestureRecognizerr ;
    NSMutableString *textInProgress;
    
    NSMutableArray *dictionaryStack;
    
    NSString *mstrXMLString;
    
    XMLReader *reader;
    
    NSString *fetchedXML;

}
@end
