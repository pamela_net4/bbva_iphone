//
//  SuscriptionStep1.m
//  NXT_Peru_iPhone
//
//  Created by Canales on 22-02-16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "SuscriptionStep1.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+BBVA_Colors.h"
#import "SuscriptionStep2.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "XMLReader.h"
#import "AFUpdater.h"
#import "Constants.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "LoanList.h"
#import "MATLogNames.h"
#import "MCBFacade.h"
#import "MutualFundList.h"
#import "NSString+URLAndHTMLUtils.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Updater.h"
#import "StringKeys.h"
#import "Tools.h"

@interface SuscriptionStep1 ()
@property (retain, nonatomic) IBOutlet UIButton *btnCancelar;
@property (retain, nonatomic) IBOutlet UIButton *btnContinuar;
@property (retain, nonatomic) IBOutlet UITextField *txtNumtarjeta;
@property (retain, nonatomic) IBOutlet UITextField *txtClaveTarjeta;

@end

@implementation SuscriptionStep1
- (IBAction)onClickInfo:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                    message:@"No olvides que es la misma clave de 4 dígitos que usas en el cajero"
                                                   delegate:nil
                                          cancelButtonTitle:@"Aceptar"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void)wsOpenSession
{

     UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner setFrame:CGRectMake(150, 300, 20, 20)];
    [self.view addSubview:spinner];
    
    [spinner startAnimating];
    
    
    [[AFUpdater sharedManager] openSession:@"" andParameters:nil OnCompletion:^(BOOL finished, NSDictionary *dictionary) {
        
        if ([[dictionary objectForKey:@"MSG-S"] objectForKey:@"INFORMACIONADICIONAL"] != nil) {
            
            
            [self wsVerifyAlta];
            
        }
        else
        {
            
            
            NSDictionary *dictError = [[[dictionary objectForKey:@"MSG-S"] objectForKey:@"ERROR"] objectForKey:@"MENSAJE"];
            
            if (dictError == nil) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                                message:NSLocalizedString(@"GENERIC_MESSAGE_TEXT", nil)
                                                               delegate:nil
                                                      cancelButtonTitle:@"Aceptar"
                                                      otherButtonTitles:nil];
                
                [alert show];
            }
            else
            {
                
                NSString *mensajeError = [[NSUserDefaults standardUserDefaults] objectForKey:@"mensajeError"];
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                                message:mensajeError
                                                               delegate:nil
                                                      cancelButtonTitle:@"Aceptar"
                                                      otherButtonTitles:nil];
                [alert show];
            }

        }
        
        [spinner stopAnimating];
        [spinner removeFromSuperview];
        
        
    }];
   
}

-(void)wsVerifyAlta
{
    
     UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner setFrame:CGRectMake(150, 300, 20, 20)];
    [self.view addSubview:spinner];
    
    [spinner startAnimating];
    
    
    [[AFUpdater sharedManager] verifyUser:_txtNumtarjeta.text andClave:_txtClaveTarjeta.text andParameters:nil OnCompletion:^(BOOL finished, NSDictionary *dictionary) {
        
        if ([[[dictionary objectForKey:@"MSG-S"] objectForKey:@"INFORMACIONADICIONAL"] objectForKey:@"NOMBRECLIENTE"] != nil) {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"alta" forKey:@"Verifying"];
            SuscriptionStep2 *SuscriptionStep2_ = [[SuscriptionStep2 alloc] init];
            [self.navigationController pushViewController:SuscriptionStep2_ animated:YES];
            
            
        }
        else
        {
            
            
            NSDictionary *dictError = [[[dictionary objectForKey:@"MSG-S"] objectForKey:@"ERROR"] objectForKey:@"MENSAJE"];
            
            if (dictError == nil) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                                message:NSLocalizedString(@"GENERIC_MESSAGE_TEXT", nil)
                                                               delegate:nil
                                                      cancelButtonTitle:@"Aceptar"
                                                      otherButtonTitles:nil];
                
                [alert show];
            }
            else
            {
                
                NSString *mensajeError = [[NSUserDefaults standardUserDefaults] objectForKey:@"mensajeError"];
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                                message:mensajeError
                                                               delegate:nil
                                                      cancelButtonTitle:@"Aceptar"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
        
        
        
        [spinner stopAnimating];
        [spinner removeFromSuperview];
    }];
        
}
    
    



- (void)viewDidLoad {
    [super viewDidLoad];
    
    //registra MAT Ingreso
    [self processMCBNotifications:MAT_LOG_SUSCRIPTION_CLIENT_IN];
    
    //bar
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar setBarStyle:UIBarStyleDefault];
    [toolbar sizeToFit];
    
    UISegmentedControl *segControl = [[UISegmentedControl alloc] initWithItems:@[@"Anterior", @"Siguiente"]];
    [segControl setSegmentedControlStyle:UISegmentedControlStyleBar];
    segControl.tintColor = [UIColor BBVABlueColor];
    segControl.momentary = YES;
    segControl.highlighted = YES;
    
    [segControl addTarget:self action:@selector(changeRow:) forControlEvents:(UIControlEventValueChanged)];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"OK" style:UIBarButtonItemStyleDone target:self action:@selector(doneAction)];
    doneButton.tintColor = [UIColor BBVABlueColor];
    UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithCustomView:segControl];
    
    NSArray *itemsArray = @[nextButton,extraSpace,doneButton];
    
    [toolbar setItems:itemsArray];
    
    _txtNumtarjeta.inputAccessoryView=toolbar;
    _txtClaveTarjeta.inputAccessoryView=toolbar;
    
    //
    
    
    _txtClaveTarjeta.background = [[ImagesCache getInstance] imageNamed:TEXT_FIELD_BACKGROUND_IMAGE_FILE_NAME];
    _txtClaveTarjeta.leftViewMode = UITextFieldViewModeAlways;
    _txtClaveTarjeta.leftView = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 10.0f, 10.0f)] autorelease];
    
    _txtNumtarjeta.background = [[ImagesCache getInstance] imageNamed:TEXT_FIELD_BACKGROUND_IMAGE_FILE_NAME];
    _txtNumtarjeta.leftViewMode = UITextFieldViewModeAlways;
    _txtNumtarjeta.leftView = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 10.0f, 10.0f)] autorelease];

    
    //Recognizer
    
    tapgestureRecognizerr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    tapgestureRecognizerr.delegate=self;
    
    [self.view addGestureRecognizer:tapgestureRecognizerr];
    
    //round corners button



}
-(void) doneAction
{
    
    [_txtClaveTarjeta resignFirstResponder];
    [_txtNumtarjeta resignFirstResponder];

}
- (void)changeRow:(id)sender {
    
    int idx = [sender selectedSegmentIndex];
    
    if ([_txtNumtarjeta isFirstResponder]) {
        
        
        
        if (idx == 1) {
            
            [_txtClaveTarjeta becomeFirstResponder];
            
        }


        
    }
    else
    {
        
        if (idx == 0) {
            
            [_txtNumtarjeta becomeFirstResponder];
            
        }
    
    }
    
   }

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer{
    
    
    [_txtClaveTarjeta resignFirstResponder];
    [_txtNumtarjeta resignFirstResponder];
    
    
}



- (IBAction)onClickBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)dealloc {
    [_btnCancelar release];
    [_btnContinuar release];
    [_txtNumtarjeta release];
    [_txtClaveTarjeta release];
    [super dealloc];
}


- (IBAction)onClickContinuar:(id)sender {
    
    
    
    
    if ([_txtNumtarjeta.text isEqualToString:@""] || [_txtNumtarjeta.text length] != 16) {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                        message:@"Has ingresado un número de tarjeta incorrecto. Intenta nuevamente"
                                                       delegate:nil
                                              cancelButtonTitle:@"Aceptar"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        if ([_txtClaveTarjeta.text isEqualToString:@""] || [_txtClaveTarjeta.text length] !=4) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                            message:@"Has ingresado una clave incorrecta. Recuerda que es la clave que utilizas en el cajero"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Aceptar"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        else
        {
            
            [self wsOpenSession];
            

        }
 
    }

    
}



//max lengt
- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == _txtNumtarjeta) {
        
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        if ([_txtNumtarjeta.text length] == 16 && ![string isEqualToString:@""] ) {
            
            [_txtClaveTarjeta becomeFirstResponder];
        }
        
        return newLength <= 16 || returnKey;
    }
    else
    {
    
        
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        if (newLength <= 4 || returnKey) {
            return YES;
        }

        
        
        
    
    }
    

}


/**
 * Process MCB notifications after an UpdaterOperation invocation
 *
 * @param notification The notification.
 * @private
 */
- (void)processMCBNotifications:(NSString *)notification {
    
    [MCBFacade invokeLogInvokedAppMethodWithAppName:MCB_APPLICATION_NAME
                                             appKey:MCB_APPLICATION_KEY
                                         appCountry:APP_COUNTRY
                                         appVersion:APP_VERSION_STRING
                                           latitude:0.0f
                                          longitude:0.0f
                                      validLocation:NO
                                          appMethod:notification];
}
@end
