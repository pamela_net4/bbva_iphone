//
//  SuscriptionStep1.h
//  NXT_Peru_iPhone
//
//  Created by Canales on 22-02-16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMLReader.h"

@interface ChangePassStep1 : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    
    UITapGestureRecognizer* tapgestureRecognizerr ;

    NSMutableString *textInProgress;
    
    NSMutableArray *dictionaryStack;
    
    NSString *mstrXMLString;
    
    XMLReader *reader;
    
    NSString *fetchedXML;
    
    
}
@end
