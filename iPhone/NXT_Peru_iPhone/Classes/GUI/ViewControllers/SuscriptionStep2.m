//
//  SuscriptionStep2.m
//  NXT_Peru_iPhone
//
//  Created by Cardenas Torres, B. on 31/03/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "SuscriptionStep2.h"
#import "UIColor+BBVA_Colors.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "SecurityTerms.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "XMLReader.h"
#import "AFUpdater.h"
#import "Constants.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"




@interface SuscriptionStep2 ()
@property (retain, nonatomic) IBOutlet UITextField *txtContrasenia;
@property (retain, nonatomic) IBOutlet UITextField *txtContraseniaCOnfirma;
@property (retain, nonatomic) IBOutlet UITextField *txtEmail;
@property (retain, nonatomic) IBOutlet UIButton *btnCheck;
@property (retain, nonatomic) IBOutlet UIButton *btnHeLeido;

@end

#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."

@implementation SuscriptionStep2
- (IBAction)onClickHeleido:(id)sender {
    
    [_txtEmail resignFirstResponder];
    [_txtContrasenia resignFirstResponder];
    [_txtContraseniaCOnfirma resignFirstResponder];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"vieneDeChangePass"];
    
    SecurityTerms *suscriptionLegales = [[SecurityTerms alloc] init];
    [self.navigationController pushViewController:suscriptionLegales animated:YES];
}
- (IBAction)onClickCheck:(id)sender {
    
    UIImage *imageCheck = [UIImage imageNamed:@"radio_selected"];
    UIImage *imageUnCheck = [UIImage imageNamed:@"radio_unselected"];

    if (checked) {
        
        [_btnCheck setImage:imageUnCheck forState:UIControlStateNormal];
        checked = NO;
        
    }
    else
    {
        
        [_btnCheck setImage:imageCheck forState:UIControlStateNormal];
        checked = YES;
    }


}

-(void)viewDidAppear:(BOOL)animated
{
    NSString *aceptaAviso = [[NSUserDefaults standardUserDefaults] objectForKey:@"aceptaAviso"];
    UIImage *imageCheck = [UIImage imageNamed:@"radio_selected"];
    UIImage *imageUnCheck = [UIImage imageNamed:@"radio_unselected"];
    
    if ([aceptaAviso isEqualToString:@"YES"]) {
        
        [_btnCheck setImage:imageCheck forState:UIControlStateNormal];
        checked = YES;
    }
    else
    {
        [_btnCheck setImage:imageUnCheck forState:UIControlStateNormal];
        checked = NO;
    
    }
    
    

}
- (IBAction)onClickInfoCorreo:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                    message:@"Este correo nos servirá para mantenernos en contacto contigo"
                                                   delegate:nil
                                          cancelButtonTitle:@"Aceptar"
                                          otherButtonTitles:nil];
    [alert show];

    
}
- (IBAction)onClickContinuar:(id)sender {
    
    if ([_txtEmail.text isEqualToString:@""] || ![self NSStringIsValidEmail:_txtEmail.text]) {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                        message:@"Ingresa un correo válido"
                                                       delegate:nil
                                              cancelButtonTitle:@"Aceptar"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        
        if ([_txtContrasenia.text isEqualToString:@""] || [_txtContrasenia.text length]<4 || [_txtContrasenia.text length]>9) {
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                            message:@"Has ingresado una contraseña incorrecta. Recuerda que\npuede contener letras y/o números\n (min. 4 y max. 9 caracteres)."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Aceptar"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        else
        {
        
            if (![_txtContrasenia.text isEqualToString:_txtContraseniaCOnfirma.text]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                                message:@"Ingresa nuevamente tu contraseña"
                                                               delegate:nil
                                                      cancelButtonTitle:@"Aceptar"
                                                      otherButtonTitles:nil];
                [alert show];

            }
            else
            {
            
                
                //validar terminos
                
                if (!checked) {
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                                    message:@"Para continuar debes confirmar que leíste el Aviso de Seguridad"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Aceptar"
                                                          otherButtonTitles:nil];
                    [alert show];
                    
                }
                else
                {
                    
                    
                    
                    
                    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                    [spinner setFrame:CGRectMake(150, 300, 20, 20)];
                    [self.view addSubview:spinner];
                    
                    [spinner startAnimating];
                    
                    
                    
                 [[AFUpdater sharedManager] doAlta:_txtEmail.text andContrasenia:_txtContraseniaCOnfirma.text andParameters:nil OnCompletion:^(BOOL finished, NSDictionary *dictionary) {
                     
                     
                     if ([[dictionary objectForKey:@"MSG-S"] objectForKey:@"INFORMACIONADICIONAL"]!= nil) {
                         
                         [[NSUserDefaults standardUserDefaults] setObject:@"alta" forKey:@"registration"];
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"¡BIENVENIDO!"
                                                                         message:@"\nDesde ahora evita las colas y haz tus operaciones con total seguridad desde donde estés.\n\nRecuerda que la contraseña de acceso que has creado podrás utilizarla tambien para ingresar a Banca por internet en bbvacontinental.pe"
                                                                        delegate:nil
                                                               cancelButtonTitle:@"Aceptar"
                                                               otherButtonTitles:nil];
                         alert.delegate = self;
                         [alert show];
                         
                         
                         
                     }
                     else
                     {
                         
                         
                         NSDictionary *dictError = [[[dictionary objectForKey:@"MSG-S"] objectForKey:@"ERROR"] objectForKey:@"MENSAJE"];
                         
                         if (dictError == nil) {
                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                                             message:NSLocalizedString(@"GENERIC_MESSAGE_TEXT", nil)
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"Aceptar"
                                                                   otherButtonTitles:nil];
                             
                             [alert show];
                         }
                         else
                         {
                             
                             NSString *mensajeError = [[NSUserDefaults standardUserDefaults] objectForKey:@"mensajeError"];
                             
                             
                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                                             message:mensajeError
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"Aceptar"
                                                                   otherButtonTitles:nil];
                             [alert show];
                         }

                     }
                     
                     
                     
                     [spinner stopAnimating];
                     [spinner removeFromSuperview];
                     
                     
                     
                 }];
                    
                    
                    
                    
                    
                    
                    


                    
                }
                
            
            
            }

            
        
        }
        
        
    
    }

    
}
- (IBAction)onCLickInfoContraseña:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Información"
                                                    message:@"Esta contraseña sirve para la \nBanca por Internet \ny este aplicativo móvil. \nRecuerda que puede contener \nletras y/o números. \n(min. 4 y max. 9 caracteres)"
                                                   delegate:nil
                                          cancelButtonTitle:@"Aceptar"
                                          otherButtonTitles:nil];
    [alert show];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"aceptaAviso"];
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    _btnHeLeido.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"He leído y acepto el aviso de seguridad"
                                                             attributes:underlineAttribute];
    
    checked = NO;
    
    //bar
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar setBarStyle:UIBarStyleDefault];
    [toolbar sizeToFit];
    
    UISegmentedControl *segControl = [[UISegmentedControl alloc] initWithItems:@[@"Anterior", @"Siguiente"]];
    [segControl setSegmentedControlStyle:UISegmentedControlStyleBar];
    segControl.tintColor = [UIColor BBVABlueColor];
    segControl.momentary = YES;
    segControl.highlighted = YES;
    
    [segControl addTarget:self action:@selector(changeRow:) forControlEvents:(UIControlEventValueChanged)];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"OK" style:UIBarButtonItemStyleDone target:self action:@selector(doneAction)];
    doneButton.tintColor = [UIColor BBVABlueColor];
    UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithCustomView:segControl];
    
    NSArray *itemsArray = @[nextButton,extraSpace,doneButton];
    
    [toolbar setItems:itemsArray];
    
    _txtEmail.inputAccessoryView=toolbar;
    _txtContrasenia.inputAccessoryView=toolbar;
    _txtContraseniaCOnfirma.inputAccessoryView=toolbar;
    //

    
    // Do any additional setup after loading the view from its nib.
    _txtEmail.background = [[ImagesCache getInstance] imageNamed:TEXT_FIELD_BACKGROUND_IMAGE_FILE_NAME];
    _txtEmail.leftViewMode = UITextFieldViewModeAlways;
    _txtEmail.leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 10.0f, 10.0f)] ;
    
    _txtContrasenia.background = [[ImagesCache getInstance] imageNamed:TEXT_FIELD_BACKGROUND_IMAGE_FILE_NAME];
    _txtContrasenia.leftViewMode = UITextFieldViewModeAlways;
    _txtContrasenia.leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 10.0f, 10.0f)] ;
    
    _txtContraseniaCOnfirma.background = [[ImagesCache getInstance] imageNamed:TEXT_FIELD_BACKGROUND_IMAGE_FILE_NAME];
    _txtContraseniaCOnfirma.leftViewMode = UITextFieldViewModeAlways;
    _txtContraseniaCOnfirma.leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 10.0f, 10.0f)];
}

-(void) doneAction
{
    
    [_txtEmail resignFirstResponder];
    [_txtContrasenia resignFirstResponder];
    [_txtContraseniaCOnfirma resignFirstResponder];
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y = +0;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationDelay:0];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];

    
}
- (void)changeRow:(id)sender {
    
    int idx = [sender selectedSegmentIndex];
    
    if ([_txtContrasenia isFirstResponder]) {

        if (idx == 1) {
            
            [_txtContraseniaCOnfirma becomeFirstResponder];
            
        }
 
    }
    else
    {
    if ([_txtContraseniaCOnfirma isFirstResponder]) {
        
        if (idx == 1) {
            
            
            CGRect viewFrame = self.view.frame;
            viewFrame.origin.y = -150;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:.3];
            [UIView setAnimationDelay:0];
            [self.view setFrame:viewFrame];
            [UIView commitAnimations];

            
            [_txtEmail becomeFirstResponder];
            
            
            
            
        }
        if (idx == 0) {
            
            CGRect viewFrame = self.view.frame;
            viewFrame.origin.y = +0;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:.3];
            [UIView setAnimationDelay:0];
            [self.view setFrame:viewFrame];
            [UIView commitAnimations];

            [_txtContrasenia becomeFirstResponder];
            
        }
        
        
    }
    else
    {
        if ([_txtEmail isFirstResponder]) {
        
            if (idx == 0) {
            
                CGRect viewFrame = self.view.frame;
                viewFrame.origin.y = +0;
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationBeginsFromCurrentState:YES];
                [UIView setAnimationDuration:.3];
                [UIView setAnimationDelay:0];
                [self.view setFrame:viewFrame];
                [UIView commitAnimations];

                [_txtContraseniaCOnfirma becomeFirstResponder];
            
            }
        
        }
    }
    }
}

- (IBAction)onClickBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    
    if (textField == _txtEmail) {
        CGRect viewFrame = self.view.frame;
        viewFrame.origin.y = -150;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationDelay:0];
        [self.view setFrame:viewFrame];
        [UIView commitAnimations];

    }
    else
    {
        
    
        CGRect viewFrame = self.view.frame;
        viewFrame.origin.y = +0;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationDelay:0];
        [self.view setFrame:viewFrame];
        [UIView commitAnimations];

        
    }
    
    
}

- (void)dealloc {
    [_btnCheck release];
    [_btnHeLeido release];
    [super dealloc];
}


-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
#pragma mark alertviewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    if (buttonIndex == 0) {

        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
- (IBAction)onClickAfiliarmeMasTarde:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}


//max lengt
- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == _txtContrasenia || textField == _txtContraseniaCOnfirma) {

        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        
        if ((newLength <= 9 || returnKey ) && [string isEqualToString:filtered])
        {
            return YES;
        }
        else
        {
            return NO;
            
        }
        
    }
    else
    {
        
        return  YES;
        
    }
    
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == _txtContrasenia)
        {
        
            [_txtContraseniaCOnfirma becomeFirstResponder];
        
        }
        if (textField == _txtContraseniaCOnfirma)
            {
                
                [_txtEmail becomeFirstResponder];
                
            }
    
    return NO;
  
}
@end
