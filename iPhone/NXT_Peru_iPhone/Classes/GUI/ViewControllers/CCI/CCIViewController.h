/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>
#import "NXTViewController.h"

/**
 * CCI view. It only has a table view 
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CCIViewController : NXTViewController <UITableViewDelegate, UITableViewDataSource> {
    
    /**
     * Table for amounts
     */
    UITableView *table_;
    
    /**
     * Branding image view
     */
    UIImageView *brandingImageView_;

}

/**
 * Provides readwrite access to the table. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *table;

/**
 * Provides readwrite access to the brandingImageView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *brandingImageView;

/**
 * Creates a new autoreleased instance from the associated NIB file
 */
+ (CCIViewController *)CCIViewController;

@end
