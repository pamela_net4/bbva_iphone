/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "CCIViewController.h"

#import "AccountList.h"
#import "BankAccount.h"
#import "CCICell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTNavigationItem.h"
#import "Session.h"
#import "SimpleHeaderView.h"
#import "StringKeys.h"
#import "TextCell.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "Updater.h"

/**
 * Defines the nib name
 */
#define NIB_NAME                                                        @"CCIViewController"

#pragma mark -

/**
 * CCIViewController private extension
 */
@interface CCIViewController()

/**
 * Release the graphic elements
 */
- (void)releaseCCIViewControllerGraphicElements;

@end

#pragma mark -

@implementation CCIViewController

#pragma mark -
#pragma mark Properties

@synthesize table = table_;
@synthesize brandingImageView = brandingImageView_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self releaseCCIViewControllerGraphicElements];
    [super dealloc];
    
}

/*
 * Release the graphic elements
 */
- (void)releaseCCIViewControllerGraphicElements {
    
    [table_ release];
    table_ = nil;
    
    [brandingImageView_ release];
    brandingImageView_ = nil;
    
}

/**
 * Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload {
    
    [super viewDidUnload];
    
    [[self navigationController] setNavigationBarHidden:YES];
    
    [self releaseCCIViewControllerGraphicElements];
    
}

/**
 * Sent to the view controller when the application receives a memory warning. Releases the graphic elements when view is not on a window.
 */
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    if ((![self isViewLoaded]) || ([[self view] window] == nil)) {
        
        [self releaseCCIViewControllerGraphicElements];
        [self setView:nil];
        
    }
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Creates the internal structures
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
}

/*
 * Creates a new autoreleased instance from the associated NIB file
 */
+ (CCIViewController *)CCIViewController {
    
    CCIViewController *result = [[[CCIViewController alloc] initWithNibName:NIB_NAME bundle:nil] autorelease];
    [result awakeFromNib];
    
	return result;
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Called after the controller’s view is loaded into memory. Controls are initialized
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    brandingImageView_.image = [[ImagesCache getInstance] imageNamed:BRANDING_GRADIENT_IMAGE_FILE_NAME]; 
    
}

/**
 * Notifies the view controller that its view is about to be become visible.
 *
 * @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [table_ reloadData];
    table_.scrollsToTop = YES;
}

/**
 * Notifies the view controller that its view did appear
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.appDelegate setTabBarVisibility:NO animated:YES];
    
}

/**
 * Notifies the view controller that its view is about to be dismissed, covered, or otherwise hidden from view.
 *
 * @param animated If YES, the view has been added to the window using an animation.
 */
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
}

#pragma mark -
#pragma mark UITableView methods

/**	
 * Asks the data source to return the number of sections in the table view
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
	return 1;
}

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
      
    NSInteger rowsCount = [self tableView:tableView numberOfRowsInSection:indexPath.section];
    
    if (indexPath.row == (rowsCount - 1)) {
        
        TextCell *result = (TextCell *)[tableView dequeueReusableCellWithIdentifier:[TextCell cellIdentifier]];
        
        if (result == nil) {
            result = [TextCell textCell];
        }
        result.selectionStyle = UITableViewCellSelectionStyleNone;

        result.text =  NSLocalizedString(IAC_IAC_TITLE_TEXT_KEY, nil);
                
        return result;
        
        
    } else {
        
        CCICell *result = (CCICell *)[tableView dequeueReusableCellWithIdentifier:[CCICell cellIdentifier]];

        if (result == nil) {
            result = [CCICell cciCell];
        }
        result.selectionStyle = UITableViewCellSelectionStyleNone;

        Session *session = [Session getInstance];
        BankAccount *account = [session.accountList accountAtPosition:indexPath.row];
        
        result.topText = [Tools notNilString:[account accountType]];
        result.middleText = [Tools notNilString:[account number]];
        result.bottomText = [NSString stringWithFormat:@"CCI %@",[Tools notNilString:[account IAC]]];
        
        return result;

    }

 }

/**
 * Tells the delegate that the specified row is now selected
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    Session *session = [Session getInstance];
    NSInteger result = [session.accountList accountCount] + 1;
    
    return result;
    
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger rowsCount = [self tableView:tableView numberOfRowsInSection:indexPath.section];
    
    CGFloat result = 0.0;
    
    if (indexPath.row == (rowsCount - 1)) {    
        
        result = [TextCell cellHeightForText:NSLocalizedString(IAC_IAC_TITLE_TEXT_KEY, nil)];
        
    } else {
        
        result = [CCICell cellHeight];
    
    }
     
    return result;
    
}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return [SimpleHeaderView height];
    
}

/**
 * Asks the delegate for a view object to display in the header of the specified section of the table view.
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
             
    SimpleHeaderView *result = [SimpleHeaderView simpleHeaderView];
    result.title = NSLocalizedString(IAC_POPUP_TITLE_TEXT_KEY, nil);
    
    return result;
}



#pragma mark -
#pragma mark UIVIewController selectors

/**
 * Returns the custom navigation item
 * 
 * @return The custom navigation item
 */
- (UINavigationItem *)navigationItem {
    
    NXTNavigationItem *result = self.customNavigationItem;
  
    [[result customTitleView] setTopLabelText:NSLocalizedString(GLOBAL_POSITION_TITLE_TEXT_KEY, nil)];
    
    return result;
    
}

@end

