/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "SMSMokCell.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "MOKStringListSelectionButton.h"
#import "MOKTextField.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the AddressCell NIB file name
 */
NSString * const kSMSMokCellNibFileName = @"SMSMokCell";

/**
 * Defines the cell identifier
 */
NSString * const kSMSMokCellIdentifier = @"SMSMokCell";

/**
 * Defines the cell height
 */
#define FONT_SIZE                                           14.0f

/**
 * Denifes the vertical margin size
 */
#define VERTICAL_MARGIN_SIZE                                10.0f

/**
 * Denifes the vertical margin size
 */
#define SWITCH_HEIGHT                                       27.0f

/**
 * Denifes the vertical margin size
 */
#define TXTFIELD_HEIGHT                                     31.0f

/**
 * Denifes the vertical margin size
 */
#define COMBO_HEIGHT                                        37.0f

/**
 * Denifes the vertical margin size
 */
#define BUTTON_HEIGHT                                       37.0f


#pragma mark -

@implementation SMSMokCell

#pragma -
#pragma mark Properties

@synthesize backgroundImageView = backgroundImageView_;
@synthesize titleLabel = titleLabel_;
@synthesize smsSwitch = smsSwitch_;
@synthesize firstTextField = firstTextField_;
@synthesize firstAddButton = firstAddButton_;
@synthesize firstComboButton = firstComboButton_;
@synthesize secondTextField = secondTextField_;
@synthesize secondAddButton = secondAddButton_;
@synthesize secondComboButton = secondComboButton_;
@synthesize moreButton = moreButton_;
@synthesize operatorArray = operatorArray_;
@synthesize selectedFirstOperatorIndex = selectedFirstOperatorIndex_;
@synthesize selectedSecondOperatorIndex = selectedSecondOperatorIndex_;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [backgroundImageView_ release];
    backgroundImageView_ = nil;
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [smsSwitch_ release];
    smsSwitch_ = nil;
    
    [firstTextField_ release];
    firstTextField_ = nil;
    
    [firstAddButton_ release];
    firstAddButton_ = nil;
    
    [firstComboButton_ release];
    firstComboButton_ = nil;
    
    [secondTextField_ release];
    secondTextField_ = nil;
    
    [secondAddButton_ release];
    secondAddButton_ = nil;
    
    [secondComboButton_ release];
    secondComboButton_ = nil;
    
    [moreButton_ release];
    moreButton_ = nil;
    
    [operatorArray_ release];
    operatorArray_ = nil;
    
    [addContactImage_ release];
    addContactImage_ = nil;
    
    [deleteContactImage_ release];
    deleteContactImage_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization


/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.showDisclosureArrow = NO;
    self.showSeparator = NO;
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:FONT_SIZE color:[UIColor BBVABlackColor]];
    [titleLabel_ setText:NSLocalizedString(SEND_PHONE_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:firstTextField_ withFontSize:FONT_SIZE andColor:[UIColor BBVAGreyColor]];
    [firstTextField_ setPlaceholder:NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_FILL_KEY, nil)];   
    
    [NXT_Peru_iPhoneStyler styleMokTextField:secondTextField_ withFontSize:FONT_SIZE andColor:[UIColor BBVAGreyColor]];
    [secondTextField_ setPlaceholder:NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_FILL_KEY, nil)]; 
    
    [NXT_Peru_iPhoneStyler styleStringListButton:firstComboButton_];
    [NXT_Peru_iPhoneStyler styleStringListButton:secondComboButton_];
    
    [firstComboButton_ setNoSelectionText:NSLocalizedString(NOTIFICATIONS_TABLE_SELECT_CARRIER_TEXT_KEY, nil)];
    [secondComboButton_ setNoSelectionText:NSLocalizedString(NOTIFICATIONS_TABLE_SELECT_CARRIER_TEXT_KEY, nil)];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        [NXT_Peru_iPhoneStyler styleButton:moreButton_
                         withWhiteFontSize:FONT_SIZE
                                background:[[ImagesCache getInstance] imageNamed:BLUE_BUTTON_BACKGROUND_IMAGE_FILE_NAME]];
        
    } else
    {
        [NXT_Peru_iPhoneStyler styleButton:moreButton_
                         withWhiteFontSize:FONT_SIZE
                           backgroundColor:[UIColor BBVABlueSpectrumColor]];
        
    }
    
    
    [moreButton_ setTitle:NSLocalizedString(NOTIFICATIONS_TABLE_ADD_PHONE_TEXT_KEY, nil) 
                 forState:UIControlStateNormal];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        addContactImage_ = [[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME] retain];
        [firstAddButton_ setBackgroundImage:addContactImage_ forState:UIControlStateNormal];
        [secondAddButton_ setBackgroundImage:addContactImage_ forState:UIControlStateNormal];
    }else{
        [firstAddButton_ setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  forState:UIControlStateNormal];
        [secondAddButton_ setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME] forState:UIControlStateNormal];
    }
    deleteContactImage_ = [[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME] retain];
    
       
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1){
        
        [smsSwitch_ setOnTintColor:[UIColor BBVABlueSpectrumColor]];
        [smsSwitch_ setTintColor: [UIColor BBVABlueSpectrumColor]];
    }
    
}

/*
 * Creates and return an autoreleased SMSMokCell constructed from a NIB file
 */
+ (SMSMokCell *)smsMokCell {
    
    SMSMokCell *result = (SMSMokCell *)[NibLoader loadObjectFromNIBFile:kSMSMokCellNibFileName];
    
    [result awakeFromNib];
    
    return result;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return kSMSMokCellIdentifier;
    
}

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeightForFirstAddOn:(BOOL)firstAddOn 
                       secondAddOn:(BOOL)secondAddOn {

    
    CGFloat result = 0.0f;
    
    result += 4*VERTICAL_MARGIN_SIZE + SWITCH_HEIGHT;
    
    if (firstAddOn) {
        result += TXTFIELD_HEIGHT + 3*VERTICAL_MARGIN_SIZE + COMBO_HEIGHT + BUTTON_HEIGHT;
    }
    
    if (secondAddOn) {
        
        result += TXTFIELD_HEIGHT + VERTICAL_MARGIN_SIZE;
    }
    
    return result;

}


/**
 * Switch tapped
 */
- (IBAction)switchButtonTapped {

    [delegate_ switchButtonHasBeenTapped:smsSwitch_.on];

}

/**
 * First add button tapped
 */
- (IBAction)firstAddButtonTapped {
        
    [delegate_ addFirstContactHasBeenTapped];
    
}

/**
 * Second add button tapped
 */
- (IBAction)secondAddButtonTapped {
    
    [delegate_ addSecondContactHasBeenTapped];
    
}

/**
 * More button tapped
 */
- (IBAction)moreButtonTapped {
    
    [delegate_ moreButtonHasBeenTapped];
    
}

- (IBAction)firstComboButtonTapped {
    
    [delegate_ firstComboButtonTapped];
}

- (IBAction)secondComboButtonTapped {
    
    [delegate_ secondComboButtonTapped];
}

/**
 * Configures the cell depending on the different flags
 *
 * @param operatorArray The operators array
 * @param firstAddOn The first add elements on
 * @param secondAddON The second add elements on
 */
- (void)setOperatorArray:(NSArray *)operatorArray 
              firstAddOn:(BOOL)firstAddOn 
             secondAddOn:(BOOL)secondAddON {

    if (operatorArray_ == nil) {
        
        operatorArray_ = [[NSMutableArray alloc] init];
        
    }
    
    [operatorArray_ removeAllObjects];
    [operatorArray_ addObjectsFromArray:operatorArray];
    
    NSMutableArray *array = [NSMutableArray array];
    
    for (NSString *carrier in operatorArray_) {
        
        [array addObject:carrier.description];
    }
    
    [firstComboButton_ setOptionStringList:[NSArray arrayWithArray:array]];
    [secondComboButton_ setOptionStringList:[NSArray arrayWithArray:array]];
    [array removeAllObjects];
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
    firstTextField_.hidden = !firstAddOn;
    firstAddButton_.hidden = !firstAddOn;
    firstComboButton_.hidden = !firstAddOn;
        
    if (firstAddOn && !secondAddON) {
        moreButton_.hidden = NO;
    } else {
        moreButton_.hidden = YES;
    }
    
    secondTextField_.hidden = !secondAddON;
    secondAddButton_.hidden = !secondAddON;
    secondComboButton_.hidden = !secondAddON;
    
    CGRect frame;
    CGFloat yPos;
            
    frame = backgroundImageView_.frame;
    frame.origin.y = VERTICAL_MARGIN_SIZE;
    frame.size.height = 48.0f;
    backgroundImageView_.frame = frame;
    yPos = frame.origin.y + frame.size.height;
        
    
    if (firstAddOn) {
        
        yPos += VERTICAL_MARGIN_SIZE;
        
        frame = firstTextField_.frame;
        frame.origin.y = yPos;
        firstTextField_.frame = frame;
        
        frame = firstAddButton_.frame;
        frame.origin.y = firstTextField_.frame.origin.y + 6.5f;
        firstAddButton_.frame = frame;

        yPos += firstTextField_.frame.size.height + VERTICAL_MARGIN_SIZE;
        
        frame = firstComboButton_.frame;
        frame.origin.y = yPos;
        firstComboButton_.frame = frame;
        
        yPos += firstComboButton_.frame.size.height + VERTICAL_MARGIN_SIZE;
        
        frame = moreButton_.frame;
        frame.origin.y = yPos;
        moreButton_.frame = frame;
        
        yPos += moreButton_.frame.size.height + VERTICAL_MARGIN_SIZE;
        
        frame = backgroundImageView_.frame;
        frame.size.height = yPos - backgroundImageView_.frame.origin.y;
        backgroundImageView_.frame = frame;
    }
    
    if (secondAddON) {
    
        yPos = moreButton_.frame.origin.y;
        
        frame = secondTextField_.frame;
        frame.origin.y = yPos;
        secondTextField_.frame = frame;
        
        frame = secondAddButton_.frame;
        frame.origin.y = secondTextField_.frame.origin.y + 6.5f;
        secondAddButton_.frame = frame;
        
        yPos += secondTextField_.frame.size.height + VERTICAL_MARGIN_SIZE;
        
        frame = secondComboButton_.frame;
        frame.origin.y = yPos;
        secondComboButton_.frame = frame;
        
        yPos += secondComboButton_.frame.size.height + VERTICAL_MARGIN_SIZE;
        
        frame = backgroundImageView_.frame;
        frame.size.height = yPos - backgroundImageView_.frame.origin.y;
        backgroundImageView_.frame = frame;
    
    }

}    
    
@end
