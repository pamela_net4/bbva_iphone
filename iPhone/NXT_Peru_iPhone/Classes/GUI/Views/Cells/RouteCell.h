/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTCell.h"

/**
 * Enum of types by route.
 */
typedef enum {
    
    rt_food = 0, //<!RouteType of the food route.
    rt_car, //<!RouteType of the car route.

} RouteType;

/**
 * Cell to present information of route.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface RouteCell : NXTCell {
    
@private
    
    /**
     * The icon image view.
     */
    UIImageView *iconImageView_;
	
	/**
     * Disclosure arrow
     */
    UIImageView *disclosureArrow_;
    
    /**
     * The text label to change title.
     */
    UILabel *textLabel_;
    
    /**
     * The distance label.
     **/
    UILabel *distanceLabel_;
    
    /**
     * The route type.
     */
    RouteType routeType_;
    
}

/**
 * Provides read-write access to iconImageView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *iconImageView;

/**
 * Provides read-write access to disclosureArrow. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *disclosureArrow;

/**
 * Provides read-write access to textLabel_. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *textLabel;

/**
 * Provides read-write access to distanceLabel. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *distanceLabel;

/**
 * Provides readwrite access to the showDisclosureArrow
 */
@property (nonatomic, readwrite, assign) BOOL showDisclosureArrow;

/**
 * Provides read-write access to routeType.
 */
@property (nonatomic, readwrite, assign) RouteType routeType;

/**
 * Creates and returns an autoreleased RouteCell constructed from a NIB file.
 *
 * @return The autoreleased RouteCell constructed from a NIB file.
 */
+ (RouteCell *)routeCell;

/**
 * Returns the cell height.
 *
 * @return The cell height.
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier.
 *
 * @param routeType: The type of route.
 * @return The cell identifier.
 */
+ (NSString *)cellIdentifierWithRouteType:(RouteType)routeType;

/**
 * Set the cell information by distance and duration.
 *
 * @param duration: The duration of the route.
 * @param distance: The distance of the route.
 */
- (void)setRouteInformationDuration:(NSString *)duration distance:(NSString *)distance;

@end
