/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "FastLoanScheduleCell.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"FastLoanScheduleCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"FastLoanScheduleCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 30.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                      12.0f


#pragma mark -

@implementation FastLoanScheduleCell

#pragma mark -
#pragma mark Properties

@synthesize textLabel1 = textLabel1_;
@synthesize textLabel2 = textLabel2_;
@synthesize textLabel3 = textLabel3_;
@synthesize textLabel4 = textLabel4_;
@synthesize textLabel5 = textLabel5_;
@synthesize textLabel6 = textLabel6_;
@synthesize separatorView1 = separatorView1_;
@synthesize separatorView2 = separatorView2_;
@synthesize separatorView3 = separatorView3_;
@synthesize separatorView4 = separatorView4_;
@synthesize separatorView5 = separatorView5_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [textLabel1_ release];
    textLabel1_ = nil;
    
    [textLabel2_ release];
    textLabel2_ = nil;
    
    [textLabel3_ release];
    textLabel3_ = nil;
    
    [textLabel4_ release];
    textLabel4_ = nil;
    
    [textLabel6_ release];
    textLabel6_ = nil;
    
    [textLabel5_ release];
    textLabel5_ = nil;
    
    [separatorView1_ release];
    separatorView1_ = nil;
    
    [separatorView2_ release];
    separatorView2_ = nil;
    
    [separatorView3_ release];
    separatorView3_ = nil;
    
    [separatorView4_ release];
    separatorView4_ = nil;
    
    [separatorView5_ release];
    separatorView5_ = nil;
        
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = NO;
    self.showSeparator = NO;
    
    [NXT_Peru_iPhoneStyler styleLabel:textLabel1_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:textLabel2_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:textLabel3_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:textLabel4_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:textLabel5_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:textLabel6_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor grayColor]];
    
}

/*
 * Creates and returns an autoreleased FastLoanScheduleCell constructed from a NIB file
 */
+ (FastLoanScheduleCell *)FastLoanScheduleCell {
    
    FastLoanScheduleCell *result = (FastLoanScheduleCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;    
}

#pragma mark -
#pragma mark Getters and setters



#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    return CELL_HEIGHT;
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

@end