/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class NXTTextField;


/**
 * CoordKeyCell. It's a subclass of NXTTableCell.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CoordKeyCell : NXTTableCell {
@private  
    
    /**
     * Key text field
     */
    NXTTextField *keyTextField_;
    
}

/**
 * Provides read-write access to the opKeyTextField and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTTextField *keyTextField;

/**
 * Provides read-write access to the coordinate
 */
@property (nonatomic, readwrite, copy) NSString *coordinate;

/**
 * Creates and returns an autoreleased CoordKeyCell constructed from a NIB file
 *
 * @return The autoreleased CoordKeyCell constructed from a NIB file
 */
+ (CoordKeyCell *)coordKeyCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end