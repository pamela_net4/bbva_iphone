/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTTableCell.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the NXTTableCell NIB file name
 */
#define NIB_FILE_NAME                                               @"NXTTableCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"NXTTableCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 40.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f


#pragma mark -

/**
 * NXTTableCellProvider private category
 */
@interface NXTTableCellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (NXTTableCellProvider *)getInstance;

/**
 * Creates and returns an autoreleased NXTTableCell constructed from a NIB file
 *
 * @return The autoreleased NXTTableCell constructed from a NIB file
 */
- (NXTTableCell *)NXTTableCell;

@end


#pragma mark -

@implementation NXTTableCellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * NXTTableCellProvier singleton only instance
 */
static NXTTableCellProvider *NXTTableCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([NXTTableCellProvider class]) {
        
        if (NXTTableCellProviderInstance_ == nil) {
            
            NXTTableCellProviderInstance_ = [super allocWithZone:zone];
            return NXTTableCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (NXTTableCellProvider *)getInstance {
    
    if (NXTTableCellProviderInstance_ == nil) {
        
        @synchronized([NXTTableCellProvider class]) {
            
            if (NXTTableCellProviderInstance_ == nil) {
                
                NXTTableCellProviderInstance_ = [[NXTTableCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return NXTTableCellProviderInstance_;
    
}

#pragma mark -
#pragma mark NXTTableCell creation

/*
 * Creates and returns aa autoreleased NXTTableCell constructed from a NIB file
 */
- (NXTTableCell *)NXTTableCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    NXTTableCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation NXTTableCell

#pragma mark -
#pragma mark Properties

@synthesize leftTextLabel = leftTextLabel_;
@synthesize rightTextLabel = rightTextLabel_;
@synthesize cellSeparatorTopImage = cellSeparatorTopImage_;
@synthesize cellSeparatorBottomImage = cellSeparatorBottomImage_;
@synthesize disclosureArrow = disclosureArrow_;
@dynamic leftText;
@dynamic rightText;
@dynamic showDisclosureArrow;
@dynamic showSeparator;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [leftTextLabel_ release];
    leftTextLabel_ = nil;
    
    [rightTextLabel_ release];
    rightTextLabel_ = nil;
    
    [cellSeparatorTopImage_ release];
    cellSeparatorTopImage_ = nil;
    
    [cellSeparatorBottomImage_ release];
    cellSeparatorBottomImage_ = nil;
    
    [disclosureArrow_ release];
    disclosureArrow_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
	
	[super awakeFromNib];
	
	[NXT_Peru_iPhoneStyler styleNXTView:self];
	
    [NXT_Peru_iPhoneStyler styleLabel:leftTextLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor blackColor]];
    
    self.selectionStyle = UITableViewCellSelectionStyleGray;
    disclosureArrow_.image = [[ImagesCache getInstance] imageNamed:ARROW_ICON_IMAGE_FILE_NAME];
    disclosureArrow_.hidden = YES;
    
}

/*
 * Creates and returns an autoreleased NXTTableCell constructed from a NIB file
 */
+ (NXTTableCell *)NXTTableCell {
    
    return [[NXTTableCellProvider getInstance] NXTTableCell];
    
}

#pragma mark -
#pragma mark Getters and setters

- (NSString *)leftText {
    return leftTextLabel_.text;
}

- (NSString *)rightText {
    return rightTextLabel_.text;
}

- (void)setLeftText:(NSString *)value {
    leftTextLabel_.text = value;

}

- (void)setRightText:(NSString *)value {
    rightTextLabel_.text = value;
    
    NSRange range = [value rangeOfString:@"-"];
    if (range.length == 0) {
        [NXT_Peru_iPhoneStyler styleLabel:rightTextLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    } else {
        [NXT_Peru_iPhoneStyler styleLabel:rightTextLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAMagentaColor]];    }
    
    if (value == nil || [value length] == 0) {
        leftTextLabel_.frame = CGRectMake(leftTextLabel_.frame.origin.x, leftTextLabel_.frame.origin.y, 283.0f, leftTextLabel_.frame.size.height);
    } else {
        leftTextLabel_.frame = CGRectMake(leftTextLabel_.frame.origin.x, leftTextLabel_.frame.origin.y, 170.0f, leftTextLabel_.frame.size.height);
    }

}

/**
 * Layouts the cell to set s
 */
- (void)setShowDisclosureArrow:(BOOL)value {
    
    disclosureArrow_.hidden = !value;

    if (!value) {
        self.accessoryType = UITableViewCellAccessoryNone;
    }

}

/*
 * Sets the separator line if the value received is YES
 */
- (void)setShowSeparator:(BOOL)value {
    if (value) {
        
        cellSeparatorBottomImage_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
        cellSeparatorBottomImage_.hidden = NO;
        
        cellSeparatorTopImage_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
        cellSeparatorTopImage_.hidden = NO;

    } else {
    
        cellSeparatorBottomImage_.hidden = YES;
        cellSeparatorBottomImage_.hidden = YES;
    
    }
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    
    return CELL_HEIGHT;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return CELL_IDENTIFIER;
    
}

@end
