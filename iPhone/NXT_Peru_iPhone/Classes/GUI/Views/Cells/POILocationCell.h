/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

@class ATMData;

/**
 * Enum for categories.
 */
typedef enum {
    
    plcOfficeType = 0, //<!The office type for poi.
    plcATMType, //<!The ATM type for poi.
    plcAgentesType //<!The agent type for poi.
    
} POILocationCellType;

/**
 * Cell with information about a POI
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface POILocationCell : UITableViewCell {
@private
    /**
     * Image to show network icon
     */
    UIImageView *networkImageView_;
    
    /**
     * Arrow icon
     */
    UIImageView *disclosureImageView_;
    
    /**
     * Separator image
     */
    UIImageView *separatorImageView_;
    
    /**
     * Name text (network name in a readable string)
     */
    UILabel *networkLabel_;
    
    /**
     * Distance text
     */
    UILabel *distanceLabel_;
    
    /**
     * Address text
     */
    UILabel *addressLabel_;
    
    /**
     * POI data object
     */
    ATMData *poi_;
    
    /**
     * The poi type.
     */
    POILocationCellType poiType_;
}

/**
 * Provides readwrite access to networkImageView_. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *networkImageView;

/**
 * Provides readwrite access to disclosureImageView_. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *disclosureImageView;

/**
 * Provides readwrite access to separatorImageView_. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separatorImageView;

/**
 * Provides readwrite access to networkLabel_. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *networkLabel;

/**
 * Provides readwrite access to distanceLabel_. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *distanceLabel;

/**
 * Provides readwrite access to addressLabel_. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *addressLabel;

/**
 * Provides readwrite access to poi_
 */
@property (nonatomic, readwrite, retain) ATMData *poi;

/**
 * Provides read-write access to poiType.
 */
@property (nonatomic, readwrite, assign) POILocationCellType poiType;

/**
 * Creates and returns an autoreleased POILocationCell constructed from a NIB file
 *
 * @return The autoreleased POILocationCell constructed from a NIB file
 */
+ (POILocationCell *)poiLocationCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end
