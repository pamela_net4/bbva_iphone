/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "NXTTableCell.h"

@class LegalTermsTopCell;

@protocol LegalTermsTopCellDelegate

/**
 * This method is called when filter is finished.
 */
- (void)warningButtonHasBeenTapped;

@end


/**
 * Legal terms top cell. It's a subclass of NXTTableCell.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface LegalTermsTopCell : NXTTableCell {
@private  
    
    /**
     * Switch bg image view
     */
    UIImageView *switchBgImageView_;
    
    /**
     * Legal terms switch
     */
    UISwitch *legalTermsSwitch_;
    
    /**
     * Warning button
     */
    UIImageView *warningImageView_;
    
    /**
     * The delegate of LegalTermsTopCell.
     */
    id <LegalTermsTopCellDelegate> delegate_;
    
}

/**
 * Provides read-write access to the switchBgImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *switchBgImageView;

/**
 * Provides read-write access to the legalTermsSwitch and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UISwitch *legalTermsSwitch;

/**
 * Provides read-write access to the warningImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *warningImageView;

/**
 * Provides read-write access to delegate.
 */
@property (nonatomic, readwrite, assign) id <LegalTermsTopCellDelegate> delegate;

/**
 * Creates and returns an autoreleased LegalTermsTopCell constructed from a NIB file
 *
 * @return The autoreleased LegalTermsTopCell constructed from a NIB file
 */
+ (LegalTermsTopCell *)legalTermsTopCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

/**
 * Warning button tapped
 */
- (IBAction)warningButtonTapped;

@end