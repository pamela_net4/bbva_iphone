/*
 * Copyright (c) 2013 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "InformationTextCell.h"

#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"


/**
 * Define the InformationTextCell NIB file name.
 */
NSString * const kInformationTextCellNibFileName = @"InformationTextCell";

/**
 * Defines the cell identifier.
 */
NSString * const kInformationTextCellIdentifier = @"InformationTextCell";


/**
 * Defines the labels vertical and horizontal offset to the borders.
 */
CGFloat const kInformationTextCellLabelOffset = 20.0f;


#pragma mark -

/**
 * InformationTextCell private extension.
 */
@interface InformationTextCell()

/**
 * Returns the information label frame for the provided string and the provided cell widht.
 *
 * @parma informationText The information text to display.
 * @param cellWidht The cell width to use.
 * @return An array containing an NSValue with the CGRect representing the information label frame calculated.
 * @private
 */
+ (NSArray *)calculateControlsFrameForInformationText:(NSString *)informationText
                                            cellWidth:(CGFloat)cellWidth;

/**
 * Returns the font for the information label.
 *
 * @return The font for the information label.
 * @private
 */
+ (UIFont *)informationLabelFont;

/**
 * Returns the information label line break mode.
 *
 * @return The information label line break mode.
 * @private
 */
+ (UILineBreakMode)informationLabelLineBreakMode;

@end


#pragma mark -

@implementation InformationTextCell

#pragma -
#pragma mark Properties

@synthesize informationLabel = informationLabel_;
@dynamic informationText;

#pragma -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [informationLabel_ release];
    informationLabel_ = nil;

    [super dealloc];
    
}

#pragma -
#pragma mark Instances initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];

    [NXT_Peru_iPhoneStyler styleLabel:informationLabel_
                     withBoldFontSize:16.0f
                                color:[UIColor BBVABlueSpectrumToneTwoColor]];
    
    [informationLabel_ setFont:[InformationTextCell informationLabelFont]];
    [informationLabel_ setLineBreakMode:[InformationTextCell informationLabelLineBreakMode]];
    [informationLabel_ setNumberOfLines:0];
    
}

/*
 * Creates and return an autoreleased InformationTextCell constructed from a NIB file.
 */
+ (InformationTextCell *)informationTextCell {
    
    return (InformationTextCell *)[NibLoader loadObjectFromNIBFile:kInformationTextCellNibFileName];
    
}

#pragma -
#pragma mark UIView selectors

/**
 * Lays-out the subviews. Cell labels are relocated depending on whether the secondary title is empty or not
 */
- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    
    NSArray *framesArray = [InformationTextCell calculateControlsFrameForInformationText:[informationLabel_ text]
                                                                               cellWidth:CGRectGetWidth([self frame])];
    
    if ([framesArray count] > 0) {
        
        NSValue *valueFrame = [framesArray objectAtIndex:0];
        CGRect frame = [valueFrame CGRectValue];
        
        [informationLabel_ setFrame:frame];
        
    }
    
}

#pragma mark -
#pragma mark Interface layout

/*
 * Returns the information label frame for the provided string and the provided cell widht.
 */
+ (NSArray *)calculateControlsFrameForInformationText:(NSString *)informationText
                                            cellWidth:(CGFloat)cellWidth {
    
    NSMutableArray *result = [NSMutableArray array];
    
    CGFloat availableWidth = cellWidth - (2.0f * kInformationTextCellLabelOffset);
    CGSize labelMaxSize = CGSizeMake(availableWidth, CGFLOAT_MAX);
    
    CGSize labelSize = [informationText sizeWithFont:[InformationTextCell informationLabelFont]
                                   constrainedToSize:labelMaxSize
                                       lineBreakMode:[InformationTextCell informationLabelLineBreakMode]];
    
    CGRect labelFrame = CGRectMake(kInformationTextCellLabelOffset,
                                   kInformationTextCellLabelOffset,
                                   labelSize.width,
                                   labelSize.height);
    
    NSValue *valueFrame = [NSValue valueWithCGRect:labelFrame];
    
    if (valueFrame != nil) {
        
        [result addObject:valueFrame];
        
    }
    
    return [NSArray arrayWithArray:result];
    
}

#pragma mark -
#pragma mark Graphic interface management

/*
 * Returns the font for the information label.
 */
+ (UIFont *)informationLabelFont {
    
    return [NXT_Peru_iPhoneStyler boldFontWithSize:16.0f];
    
}

/*
 * Returns the information label line break mode.
 */
+ (UILineBreakMode)informationLabelLineBreakMode {
    
    return UILineBreakModeWordWrap;
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height for the given text and width.
 */
+ (CGFloat)cellHeightForText:(NSString *)text
                       width:(CGFloat)width {
    
    CGFloat result = 0.0f;
    
    NSArray *framesArray = [InformationTextCell calculateControlsFrameForInformationText:text
                                                                               cellWidth:width];
    
    CGRect frame;
    
    CGFloat maxBottom = 0.0f;
    CGFloat currentBottom = 0.0f;
    
    for (NSValue *valueFrame in framesArray) {
        
        frame = [valueFrame CGRectValue];
        
        currentBottom = CGRectGetMaxY(frame);
        
        if (currentBottom > maxBottom) {
            
            maxBottom = currentBottom;
            
        }
        
    }
    
    if (maxBottom == 0.0f) {
        
        result = 2.0f * kInformationTextCellLabelOffset;
        
    } else {
        
        result = maxBottom + kInformationTextCellLabelOffset;
        
    }
    
    return result;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return kInformationTextCellIdentifier;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the information text.
 * 
 * @return The information text.
 */
- (NSString *)informationText {
    
    return [informationLabel_ text];
    
}

/*
 * Sets the new information text to display.
 *
 * @param informationText The new information text to display.
 */
- (void)setInformationText:(NSString *)informationText {
    
    [informationLabel_ setText:informationText];
    
    [self setNeedsLayout];
    
}

@end
