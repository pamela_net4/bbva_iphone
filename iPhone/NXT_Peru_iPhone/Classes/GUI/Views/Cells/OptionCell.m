/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "OptionCell.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the OptionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"OptionCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"OptionCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 57.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              16.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        12.0f


#pragma mark -

/**
 * OptionCellProvider private category
 */
@interface OptionCellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (OptionCellProvider *)getInstance;

/**
 * Creates and returns an autoreleased OptionCell constructed from a NIB file
 *
 * @return The autoreleased OptionCell constructed from a NIB file
 */
- (OptionCell *)optionCell;

@end


#pragma mark -

@implementation OptionCellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * OptionCellProvier singleton only instance
 */
static OptionCellProvider *optionCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([OptionCellProvider class]) {
        
        if (optionCellProviderInstance_ == nil) {
            
            optionCellProviderInstance_ = [super allocWithZone:zone];
            return optionCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (OptionCellProvider *)getInstance {
    
    if (optionCellProviderInstance_ == nil) {
        
        @synchronized([OptionCellProvider class]) {
            
            if (optionCellProviderInstance_ == nil) {
                
                optionCellProviderInstance_ = [[OptionCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return optionCellProviderInstance_;
    
}

#pragma mark -
#pragma mark OptionCell creation

/*
 * Creates and returns aa autoreleased OptionCell constructed from a NIB file
 */
- (OptionCell *)optionCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    OptionCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation OptionCell

#pragma mark -
#pragma mark Properties

@synthesize image = image_;
@synthesize optionSelector = optionSelector_;
@dynamic icon;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [image_ release];
    image_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    [NXT_Peru_iPhoneStyler styleLabel:self.leftTextLabel withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:self.rightTextLabel withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    
    self.showDisclosureArrow = YES;
    self.showSeparator = YES;

}

/*
 * Creates and returns an autoreleased OptionCell constructed from a NIB file
 */
+ (OptionCell *)optionCell {
    
    return [[OptionCellProvider getInstance] optionCell];
    
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Sets the icon image
 */
- (void)setIcon:(UIImage *)icon {
    image_.image = icon;
}

/**
 * Sets the right text label
 */
- (void)setRightText:(NSString *)value {
    [super setRightText:value];
    [NXT_Peru_iPhoneStyler styleLabel:self.rightTextLabel withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    
    if (value == nil) {
        CGRect aFrame = self.leftTextLabel.frame;
        aFrame.origin.y = round(self.center.y - aFrame.size.height / 2);
        self.leftTextLabel.frame = aFrame;
    }
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    
    return CELL_HEIGHT;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return CELL_IDENTIFIER;
    
}

@end
