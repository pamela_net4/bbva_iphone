/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class GlobalPositionCell;

/**
 * Provider to obtain a GlobalPositionCell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GlobalPositionCellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary GlobalPositionCell to create it from a NIB file
     */
    GlobalPositionCell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary GlobalPositionCell
 */
@property (nonatomic, readwrite, assign) IBOutlet GlobalPositionCell *auxView;

@end


/**
 * GlobalPosition cell. It's a subclass of NXTTableCell but it has two labels on the right instead of one.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GlobalPositionCell : NXTTableCell {
@private    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Subtitle label
     */
    UILabel *subtitleLabel_;
    
    /**
     * Right bottom text label
     */
    UILabel *rightTopTextLabel_;
    
    /**
     * Right bottom title text label
     */
    UILabel *rightTopTitleTextLabel_;
    
    /**
     * Right middle text label
     */
    UILabel *rightMiddleTextLabel_;
    
    /**
     * Right middle title text label
     */
    UILabel *rightMiddleTitleTextLabel_;
    
    /**
     * Right bottom text label
     */
    UILabel *rightBottomTextLabel_;
    
    /**
     * Right bottom title text label
     */
    UILabel *rightBottomTitleTextLabel_;
    
    /**
     * Small right top text flag. YES when the right top text must have a smaller font that usual, NO otherwise.
     */
    BOOL smallRightTopText_;

}

/**
 * Provides read-write access to the titleLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides read-write access to the subtitleLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *subtitleLabel;

/**
 * Provides read-write access to the rightTopTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *rightTopTextLabel;

/**
 * Provides read-write access to the rightTopTitleTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *rightTopTitleTextLabel;

/**
 * Provides read-write access to the rightMiddleTitleTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *rightMiddleTitleTextLabel;

/**
 * Provides read-write access to the rightMiddleTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *rightMiddleTextLabel;

/**
 * Provides read-write access to the rightBottomTitleTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *rightBottomTitleTextLabel;

/**
 * Provides read-write access to the rightBottomTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *rightBottomTextLabel;

/**
 * Provides readwrite access to the titleText
 */
@property (nonatomic, readwrite, copy) NSString *titleText;

/**
 * Provides readwrite access to the subtitleText
 */
@property (nonatomic, readwrite, copy) NSString *subtitleText;

/**
 * Provides readwrite access to the rightTopTitleText
 */
@property (nonatomic, readwrite, copy) NSString *rightTopTitleText;

/**
 * Provides readwrite access to the rightTopText
 */
@property (nonatomic, readwrite, copy) NSString *rightTopText;

/**
 * Provides readwrite access to the rightMiddleTitleText
 */
@property (nonatomic, readwrite, copy) NSString *rightMiddleTitleText;

/**
 * Provides readwrite access to the rightMiddleText
 */
@property (nonatomic, readwrite, copy) NSString *rightMiddleText;

/**
 * Provides readwrite access to the rightBottomTitleText
 */
@property (nonatomic, readwrite, copy) NSString *rightBottomTitleText;

/**
 * Provides readwrite access to the rightBottomText
 */
@property (nonatomic, readwrite, copy) NSString *rightBottomText;

/**
 * Provides read-write access to the small right top text flag.
 */
@property (nonatomic, readwrite, assign) BOOL smallRightTopText;



/**
 * Creates and returns an autoreleased GlobalPositionCell constructed from a NIB file
 *
 * @return The autoreleased GlobalPositionCell constructed from a NIB file
 */
+ (GlobalPositionCell *)globalPositionCell;

/**
 * Returns the cell height
 *
 * @param integer Number of details
 * @return The cell height
 */
+ (CGFloat)cellHeightForDetailsNumber:(NSInteger)integer;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;


@end