/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class CardTransaction;
@class CardTransactionCell;

/**
 * Provider to obtain a CardTransactionCell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardTransactionCellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary CardTransactionCell to create it from a NIB file
     */
    CardTransactionCell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary CardTransactionCell
 */
@property (nonatomic, readwrite, assign) IBOutlet CardTransactionCell *auxView;

@end


/**
 * Card Transaction cell. It's a subclass of NXTTableCell.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardTransactionCell : NXTTableCell {
@private    
    /**
     * Bottom left text label
     */
    UILabel *bottomLeftTextLabel_;
    
    /**
     * Card Transaction used to populate the cell
     */
    CardTransaction *cardTransaction_;
    
}

/**
 * Provides readwrite access to the CardTransaction
 */
@property (nonatomic, readwrite, retain) CardTransaction *cardTransaction;

/**
 * Provides read-write access to the bottomLeftTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *bottomLeftTextLabel;

/**
 * Creates and returns an autoreleased CardTransactionCell constructed from a NIB file
 *
 * @return The autoreleased CardTransactionCell constructed from a NIB file
 */
+ (CardTransactionCell *)cardTransactionCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end