/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TextCell.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TextCell NIB file name
 */
#define NIB_FILE_NAME                                               @"TextCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"TextCell"

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              13.0f


#pragma mark -

/**
 * TextCellProvider private category
 */
@interface TextCellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (TextCellProvider *)getInstance;

/**
 * Creates and returns an autoreleased TextCell constructed from a NIB file
 *
 * @return The autoreleased TextCell constructed from a NIB file
 */
- (TextCell *)textCell;

@end


#pragma mark -

@implementation TextCellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * TextCellProvier singleton only instance
 */
static TextCellProvider *textCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([TextCellProvider class]) {
        
        if (textCellProviderInstance_ == nil) {
            
            textCellProviderInstance_ = [super allocWithZone:zone];
            return textCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (TextCellProvider *)getInstance {
    
    if (textCellProviderInstance_ == nil) {
        
        @synchronized([TextCellProvider class]) {
            
            if (textCellProviderInstance_ == nil) {
                
                textCellProviderInstance_ = [[TextCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return textCellProviderInstance_;
    
}

#pragma mark -
#pragma mark TextCell creation

/*
 * Creates and returns aa autoreleased TextCell constructed from a NIB file
 */
- (TextCell *)textCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    TextCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation TextCell

#pragma mark -
#pragma mark Properties

@synthesize textLabel = textLabel_;
@dynamic text;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
        
    [textLabel_ release];
    textLabel_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.rightText = nil;
    self.showDisclosureArrow = NO;
    self.showSeparator = NO;
    
    self.backgroundColor = [UIColor BBVAGreyToneFiveColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:textLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    textLabel_.numberOfLines = 0; //0 = no limit

}

/*
 * Creates and returns an autoreleased TextCell constructed from a NIB file
 */
+ (TextCell *)textCell {
    
    return [[TextCellProvider getInstance] textCell];
    
}

#pragma mark -
#pragma mark Getters and setters



#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeightForText:(NSString *)text {
     
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 14.0f, 291.0f, 25.0f)];
    label.numberOfLines = 0;
    [NXT_Peru_iPhoneStyler styleLabel:label withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];

    CGFloat height = [Tools labelHeight:label forText:text];
    
    [label release];
    
    return height + 35.0f;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the top text
 *
 * @return The top text
 */
- (NSString *)text {
    
    return textLabel_.text;
    
}

/*
 * Sets top text label
 */
- (void)setText:(NSString *)aString {

    CGFloat height = [Tools labelHeight:textLabel_ forText:aString];

    textLabel_.frame = CGRectMake(textLabel_.frame.origin.x , textLabel_.frame.origin.y , textLabel_.frame.size.width, height);
    textLabel_.text = aString;
}



@end