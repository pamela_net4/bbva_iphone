/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "AddressCell.h"
#import "NibLoader.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"

/**
 * Define the AddressCell NIB file name
 */
NSString * const kAddressCellNibFileName = @"AddressCell";

/**
 * Defines the cell identifier
 */
NSString * const kAddressCellIdentifier = @"AddressCell";

/**
 * Defines the cell height
 */
CGFloat const kAddressCellHeight = 60.0f;

/**
 * Defines the labels vertical distance when both are displayed
 */
CGFloat const kAddressCellLabelsVerticalDistance = 6.0f;

/**
 * Defines the main title label font size
 */
CGFloat const kAddressCellFirstAddressLabelFontSize = 15.0f;

/**
 * Defines the main title label font size
 */
CGFloat const kAddressCellSecondAddressLabelFontSize = 12.0f;

/**
 * Defines the top padding with cell
 */
CGFloat const kAddressCellTopPadding = 3.0f;


#pragma mark -

@implementation AddressCell

#pragma -
#pragma mark Properties

@synthesize addressFirstLineLabel = addressFirstLineLabel_;
@synthesize addressSecondLineLabel = addressSecondLineLabel_;
@dynamic addressFirstLineText;
@dynamic addressSecondLineText;

#pragma -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [addressFirstLineLabel_ release];
    addressFirstLineLabel_ = nil;
    
    [addressSecondLineLabel_ release];
    addressSecondLineLabel_ = nil;
    
    [super dealloc];
    
}

#pragma -
#pragma mark Instances initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:addressFirstLineLabel_
                 withFontSize:kAddressCellFirstAddressLabelFontSize
                        color:[UIColor darkGrayColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:addressSecondLineLabel_
                 withFontSize:kAddressCellSecondAddressLabelFontSize
                        color:[UIColor grayColor]];
    
//    ImagesCache *imagesCache = [ImagesCache getInstance];
//    
//    self.backgroundImageView.image = [imagesCache imageNamed:ACTION_CELL_BACKGROUND_IMAGE_FILE_NAME];
//    self.selectedBackgroundImageView.image = [imagesCache imageNamed:ACTION_CELL_SELECTED_BACKGROUND_IMAGE_FILE_NAME];
    
}

/*
 * Creates and return an autoreleased AddresCell constructed from a NIB file
 */
+ (AddressCell *)addressCell {
    
    return (AddressCell *)[NibLoader loadObjectFromNIBFile:kAddressCellNibFileName];
    
}

#pragma -
#pragma mark UIView selectors

/**
 * Lays-out the subviews. Cell labels are relocated depending on whether the secondary title is empty or not
 */
- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    if ([[addressSecondLineLabel_ text] isEqualToString:@""]) {
    
        CGPoint center = [addressFirstLineLabel_ center];
        center.y = (kAddressCellHeight / 2) - ([addressFirstLineLabel_ frame].size.height / 2);
        [addressFirstLineLabel_ setCenter:center];
        
    } else {
        
        CGPoint center = [addressFirstLineLabel_ center];
        center.y = ([addressFirstLineLabel_ frame].size.height / 2) + kAddressCellTopPadding;
        [addressFirstLineLabel_ setCenter:center];        
        
    }
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    
    return kAddressCellHeight;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return kAddressCellIdentifier;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the address first line text
 * 
 * @return The address first line text
 */
- (NSString *)addressFirstLineText {
    
    return addressFirstLineLabel_.text;
    
}

/*
 * Sets the new address first line text to display
 *
 * @param addressFirstLineText The new address first line text to display
 */
- (void)setAddressFirstLineText:(NSString *)addressFirstLineText {
    
    addressFirstLineLabel_.text = addressFirstLineText;
    
}

/*
 * Returns the address second line text
 * 
 * @return The address second line text
 */
- (NSString *)addressSecondLineText {
    
    return addressSecondLineLabel_.text;
    
}

/*
 * Sets the new address second line text to display
 *
 * @param addressSecondLineText The new address second line text to display
 */
- (void)setAddressSecondLineText:(NSString *)addressSecondLineText {
    
    addressSecondLineLabel_.text = addressSecondLineText;
    
}

@end
