/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * Cell to display a warning information for a given route
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MapWarningCell : UITableViewCell {

@private
    
    /**
     * Warning label
     */
    UILabel *warningLabel_;

}

/**
 * Provides read-write access to the warning label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *warningLabel;

/**
 * Provides read-write access to the warning text
 */
@property (nonatomic, readwrite, copy) NSString *warningText;


/**
 * Creates and returns an autoreleased MapWarningCell constructed from a NIB file
 *
 * @return The autoreleased MapWarningCell constructed from a NIB file
 */
+ (MapWarningCell *)mapWarningCell;

/**
 * Returns the cell height for the given information
 *
 * @param aCellText
 * @param aCellWidth
 * @return The cell height
 */
+ (CGFloat)cellHeightForText:(NSString *)aCellText
                       width:(CGFloat)aCellWidth;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end
