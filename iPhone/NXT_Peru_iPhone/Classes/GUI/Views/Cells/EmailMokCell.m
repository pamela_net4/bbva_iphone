/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "EmailMokCell.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "MOKTextField.h"
#import "MOKTextView.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the EmailMokCell NIB file name
 */
NSString * const kEmailMokCellNibFileName = @"EmailMokCell";

/**
 * Defines the cell identifier
 */
NSString * const kEmailMokCellIdentifier = @"EmailMokCell";

/**
 * Defines the cell height
 */
#define FONT_SIZE                                           14.0f

/**
 * Denifes the vertical margin size
 */
#define VERTICAL_MARGIN_SIZE                                10.0f

/**
 * Denifes the vertical margin size
 */
#define SWITCH_HEIGHT                                       27.0f

/**
 * Denifes the vertical margin size
 */
#define TXTFIELD_HEIGHT                                     31.0f

/**
 * Denifes the vertical margin size
 */
#define TEXTVIEW_HEIGHT                                     93.0f

/**
 * Denifes the vertical margin size
 */
#define BUTTON_HEIGHT                                       37.0f


#pragma mark -

@implementation EmailMokCell

#pragma -
#pragma mark Properties

@synthesize backgroundImageView = backgroundImageView_;
@synthesize titleLabel = titleLabel_;
@synthesize emailSwitch = emailSwitch_;
@synthesize firstTextField = firstTextField_;
@synthesize firstAddButton = firstAddButton_;
@synthesize secondTextField = secondTextField_;
@synthesize secondAddButton = secondAddButton_;
@synthesize emailTextView = emailTextView_;
@synthesize moreButton = moreButton_;
@synthesize delegate = delegate_;
@synthesize emailTextViewBackground = emailTextViewBackground_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [backgroundImageView_ release];
    backgroundImageView_ = nil;
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [emailSwitch_ release];
    emailSwitch_ = nil;
    
    [firstTextField_ release];
    firstTextField_ = nil;
    
    [firstAddButton_ release];
    firstAddButton_ = nil;
    
    [secondTextField_ release];
    secondTextField_ = nil;
    
    [secondAddButton_ release];
    secondAddButton_ = nil;
    
    [emailTextView_ release];
    emailTextView_ = nil;
    
    [moreButton_ release];
    moreButton_ = nil;
    
    [emailTextViewBackground_ release];
    emailTextViewBackground_ = nil;
    
    [addContactImage_ release];
    addContactImage_ = nil;
    
    [deleteContactImage_ release];
    deleteContactImage_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization


/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.showDisclosureArrow = NO;
    self.showSeparator = NO;
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:FONT_SIZE color:[UIColor BBVABlackColor]];
    [titleLabel_ setText:NSLocalizedString(SEND_PHONE_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:firstTextField_ withFontSize:FONT_SIZE andColor:[UIColor BBVAGreyColor]];
    [firstTextField_ setPlaceholder:NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_PLACEHOLDER_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:secondTextField_ withFontSize:FONT_SIZE andColor:[UIColor BBVAGreyColor]];
    [secondTextField_ setPlaceholder:NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_PLACEHOLDER_KEY, nil)]; 

    [NXT_Peru_iPhoneStyler styleTextView:emailTextView_ withFontSize:FONT_SIZE andColor:[UIColor BBVAGreyColor]];
    [emailTextView_ setPlaceholder:NSLocalizedString(NOTIFICATIONS_TABLE_ENTER_MESSAGE_TEXT_KEY, nil)];
    emailTextView_.backgroundColor = [UIColor clearColor];
    emailTextViewBackground_.image = [[ImagesCache getInstance] imageNamed:TEXT_FIELD_BACKGROUND_IMAGE_FILE_NAME];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        [NXT_Peru_iPhoneStyler styleButton:moreButton_
                         withWhiteFontSize:FONT_SIZE
                                background:[[ImagesCache getInstance] imageNamed:BLUE_BUTTON_BACKGROUND_IMAGE_FILE_NAME]];
    }
    else
    {
        [NXT_Peru_iPhoneStyler styleButton:moreButton_
                         withWhiteFontSize:FONT_SIZE
                           backgroundColor:[UIColor BBVABlueSpectrumColor]];
    }

    
    [moreButton_ setTitle:NSLocalizedString(NOTIFICATIONS_TABLE_ADD_EMAIL_TEXT_KEY, nil) 
                 forState:UIControlStateNormal];

    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        addContactImage_ = [[[ImagesCache getInstance] imageNamed:ADD_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME] retain];
        [firstAddButton_ setBackgroundImage:addContactImage_ forState:UIControlStateNormal];
        [secondAddButton_ setBackgroundImage:addContactImage_ forState:UIControlStateNormal];
    }
    else{
        
        [firstAddButton_ setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  forState:UIControlStateNormal];
        [secondAddButton_ setBackgroundImage:[[ImagesCache getInstance] imageNamed:ADD_CONTACT_IOS7_BUTTON_BACKGROUND_IMAGE_FILE_NAME]  forState:UIControlStateNormal];
    }
    
    deleteContactImage_ = [[[ImagesCache getInstance] imageNamed:DELETE_CONTACT_BUTTON_BACKGROUND_IMAGE_FILE_NAME] retain];
    
    
    [firstAddButton_ setBackgroundImage:nil forState:UIControlStateNormal];
    [secondAddButton_ setBackgroundImage:nil forState:UIControlStateNormal];
    
    [emailTextView_ setPlaceholder:NSLocalizedString(NOTIFICATIONS_TABLE_ENTER_MESSAGE_TEXT_KEY, nil)];
    [emailTextView_ setPlaceholderColor:[UIColor lightGrayColor]];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1){
        
        [emailSwitch_ setOnTintColor:[UIColor BBVABlueSpectrumColor]];
        [emailSwitch_ setTintColor: [UIColor BBVABlueSpectrumColor]];
    }

}

/*
 * Creates and return an autoreleased EmailMokCell constructed from a NIB file
 */
+ (EmailMokCell *)emailMokCell {
    
    EmailMokCell *result = (EmailMokCell *)[NibLoader loadObjectFromNIBFile:kEmailMokCellNibFileName];
    
    [result awakeFromNib];
    
    return result;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return kEmailMokCellIdentifier;
    
}

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeightForFirstAddOn:(BOOL)firstAddOn 
                       secondAddOn:(BOOL)secondAddOn {

    CGFloat result = 0.0f;
    
    result += 4 * VERTICAL_MARGIN_SIZE + SWITCH_HEIGHT;
    
    if (firstAddOn) {
        result += TXTFIELD_HEIGHT + 3*VERTICAL_MARGIN_SIZE + BUTTON_HEIGHT + TEXTVIEW_HEIGHT + VERTICAL_MARGIN_SIZE;
    }
    
    return result;

}


/**
 * Switch tapped
 */
- (IBAction)switchButtonTapped {

    [delegate_ emailSwitchButtonHasBeenTapped:emailSwitch_.on];

}

/**
 * First add button tapped
 */
- (IBAction)firstAddButtonTapped {
    
    [delegate_ emailAddFirstContactHasBeenTapped];

}

/**
 * Second add button tapped
 */
- (IBAction)secondAddButtonTapped {
    
    [delegate_ emailAddSecondContactHasBeenTapped];
    
}

/**
 * More button tapped
 */
- (IBAction)moreButtonTapped {
    
    [delegate_ emailMoreButtonHasBeenTapped];
    
}

/**
 * Configures the cell depending on the different flags
 *
 * @param firstAddOn The first add elements on
 * @param secondAddON The second add elements on
 */
- (void)setConfigurationForFirstAddOn:(BOOL)firstAddOn 
                          secondAddOn:(BOOL)secondAddON {
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
    firstTextField_.hidden = !firstAddOn;
    firstAddButton_.hidden = !firstAddOn;
    emailTextView_.hidden = !firstAddOn;
  
    moreButton_.hidden = secondAddON;
    
    secondTextField_.hidden = !secondAddON;
    secondAddButton_.hidden = !secondAddON;
    
    CGRect frame;
    CGFloat yPos;
            
    frame = backgroundImageView_.frame;
    frame.origin.y = VERTICAL_MARGIN_SIZE;
    frame.size.height = 48.0f;
    backgroundImageView_.frame = frame;
    yPos = frame.origin.y + frame.size.height;
        
    
    if (firstAddOn) {
        
        yPos += VERTICAL_MARGIN_SIZE;
        
        frame = firstTextField_.frame;
        frame.origin.y = yPos;
        firstTextField_.frame = frame;
        
        frame = firstAddButton_.frame;
        frame.origin.y = firstTextField_.frame.origin.y + 6.5f;
        firstAddButton_.frame = frame;

        yPos += firstTextField_.frame.size.height + VERTICAL_MARGIN_SIZE;
        
        frame = moreButton_.frame;
        frame.origin.y = yPos;
        moreButton_.frame = frame;
        
        yPos += moreButton_.frame.size.height + VERTICAL_MARGIN_SIZE;
        
        frame = emailTextViewBackground_.frame;
        frame.origin.y = yPos;
        emailTextViewBackground_.frame = frame;
        
        frame = emailTextView_.frame;
        frame.origin.y = emailTextViewBackground_.frame.origin.y + 7.0f;
        emailTextView_.frame = frame;
        
        yPos += emailTextViewBackground_.frame.size.height + VERTICAL_MARGIN_SIZE;
        
        frame = backgroundImageView_.frame;
        frame.size.height = yPos - backgroundImageView_.frame.origin.y;
        backgroundImageView_.frame = frame;
        
    }
    
    if (secondAddON) {
    
        yPos = moreButton_.frame.origin.y;
        
        frame = secondTextField_.frame;
        frame.origin.y = yPos;
        secondTextField_.frame = frame;
        
        frame = secondAddButton_.frame;
        frame.origin.y = secondTextField_.frame.origin.y + 6.5f;
        secondAddButton_.frame = frame;
        
        yPos += secondTextField_.frame.size.height + VERTICAL_MARGIN_SIZE;
        
        frame = emailTextViewBackground_.frame;
        frame.origin.y = yPos;
        emailTextViewBackground_.frame = frame;
        
        frame = emailTextView_.frame;
        frame.origin.y = emailTextViewBackground_.frame.origin.y + 7.0f;
        emailTextView_.frame = frame;
                
        yPos += emailTextViewBackground_.frame.size.height + VERTICAL_MARGIN_SIZE;
        
        frame = backgroundImageView_.frame;
        frame.size.height = yPos - backgroundImageView_.frame.origin.y;
        backgroundImageView_.frame = frame;
    
    }

}
    
    
@end
