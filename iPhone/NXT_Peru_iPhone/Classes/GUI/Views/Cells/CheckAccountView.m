//
//  CheckAccountView.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/14/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "CheckAccountView.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"

#define NIB_FILE_NAME                                               @"CheckAccountView"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"CheckAccountView"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 84.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              14.0f

/**
 * Defines the size of the font fopr subtitle text
 */
#define TEXT_SUB_FONT_SIZE                                          10.0f

@implementation CheckAccountView

@synthesize titleLabel = titleLabel_;
@synthesize subtitleLabel = subtitleLabel_;
@synthesize subtitleLabel2 = subtitleLabel2_;
@synthesize dayLabel = dayLabel_;
@synthesize checkImageView = checkImageView_;
@synthesize checkActive = checkActive_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [subtitleLabel_ release];
    subtitleLabel_ = nil;

    
    [subtitleLabel2_ release];
    subtitleLabel2_ = nil;

    [dayLabel_ release];
    dayLabel_ = nil;
    
    [checkImageView_ release];
    checkImageView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlackColor]];
    [NXT_Peru_iPhoneStyler styleLabel:subtitleLabel_ withFontSize:TEXT_SUB_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:subtitleLabel2_ withFontSize:TEXT_SUB_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:dayLabel_ withFontSize:TEXT_SUB_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    
}

/*
 * Creates and returns an autoreleased CheckAccountView constructed from a NIB file
 */
+ (CheckAccountView *)checkAccountView {
    
    CheckAccountView *result = (CheckAccountView *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    [self awakeFromNib];
    
    return result;
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Sets the check active
 */
- (void)setCheckActive:(BOOL)checkActive {
    
    checkActive_ = checkActive;
    
    if (checkActive) {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_SELECTED_RADIO]];
        
    } else {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_UNSELECTED_RADIO]];
        
    }
    
}

#pragma mark -
#pragma mark Cell associated information


/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;
}

/*
 * Returns the cell identifier
 */
+ (CGFloat)cellHeight {
    return CELL_HEIGHT;
}



@end
