/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTTableCell.h"

@class MOKTextField;
@class MOKStringListSelectionButton;

@protocol SMSMokCellDelegate

/**
 * Switch has been tapped.
 *
 * @param on The flag
 */
- (void)switchButtonHasBeenTapped:(BOOL)on;

/**
 * Add First Contact Button Tapped
 */
- (void)addFirstContactHasBeenTapped;

/**
 * Add Second Contact Button Tapped
 */
- (void)addSecondContactHasBeenTapped;

/**
 * More button has been tapped
 */
- (void)moreButtonHasBeenTapped;

/*
 *first combo button tapped
 */
- (void)firstComboButtonTapped;

/*
 *second combo button tapped
 */
- (void)secondComboButtonTapped;


@end

/**
 * SMS cell
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface SMSMokCell: NXTTableCell {
    
@private
    
    /**
     * Background image
     */
    UIImageView *backgroundImageView_;
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * SMS Switch
     */
    UISwitch *smsSwitch_;
    
    /**
     * First text field
     */
    MOKTextField *firstTextField_;
    
    /**
     * First add button
     */
    UIButton *firstAddButton_;
    
    /**
     * First combo button
     */
    MOKStringListSelectionButton *firstComboButton_;

    /**
     * Second text field
     */
    MOKTextField *secondTextField_;
    
    /**
     * Second add button
     */
    UIButton *secondAddButton_;
    
    /**
     * Second combo button
     */
    MOKStringListSelectionButton *secondComboButton_;
    
    /**
     * More button
     */
    UIButton *moreButton_;
    
    /**
     * Add contact image
     */
    UIImage *addContactImage_;
    
    /**
     * Delete contact image
     */
    UIImage *deleteContactImage_;
    
    /**
     * Operator array
     */
    NSMutableArray *operatorArray_;
    
    /**
     * Selected first operator index
     */
    NSInteger selectedFirstOperatorIndex_;
    
    /**
     * Selected second operator index
     */
    NSInteger selectedSecondOperatorIndex_;
    
    /**
     * SMS Mok Cell Delegate
     */    
    id<SMSMokCellDelegate> delegate_;
    
}

/**
 * Provides readwrite access to the backgroundImageView and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *backgroundImageView;

/**
 * Provides readwrite access to the titleLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides readwrite access to the smsSwitch and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UISwitch *smsSwitch;

/**
 * Provides readwrite access to the firstTextField and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKTextField *firstTextField;

/**
 * Provides readwrite access to the firstAddButton and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *firstAddButton;

/**
 * Provides readwrite access to the firstComboButton and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *firstComboButton;

/**
 * Provides readwrite access to the secondTextField and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKTextField *secondTextField;

/**
 * Provides readwrite access to the secondAddButton and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *secondAddButton;

/**
 * Provides readwrite access to the secondComboButton and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *secondComboButton;

/**
 * Provides readwrite access to the moreButton and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *moreButton;

/**
 * Provides readwrite access to the operatorArray
 */
@property (nonatomic, readwrite, retain) NSArray *operatorArray;

/**
 * Provides readwrite access to the selectedFirstOperatorIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedFirstOperatorIndex;

/**
 * Provides readwrite access to the selectedSecondOperatorIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedSecondOperatorIndex;

/**
 * Provides readwrite access to the delegate
 */
@property (nonatomic, readwrite, assign) id<SMSMokCellDelegate> delegate;


/*
 * Creates and return an autoreleased SMSCell constructed from a NIB file
 */
+ (SMSMokCell *)smsMokCell;

/**
 * Returns the cell height
 *
 * @param firstAddOn The first add elements on
 * @param secondAddON The second add elements on
 * @return A height for cell.
 */
+ (CGFloat)cellHeightForFirstAddOn:(BOOL)firstAddOn 
                       secondAddOn:(BOOL)secondAddON;

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier;

/**
 * Switch tapped
 */
- (IBAction)switchButtonTapped;

/**
 * First add button tapped
 */
- (IBAction)firstAddButtonTapped;

/**
 * Second add button tapped
 */
- (IBAction)secondAddButtonTapped;

/**
 * More button tapped
 */
- (IBAction)moreButtonTapped;

/*
 *first combo button tapped
 */
- (IBAction)firstComboButtonTapped;

/*
 *second combo button tapped
 */
- (IBAction)secondComboButtonTapped;

/**
 * Configures the cell depending on the different flags
 *
 * @param operatorArray The operators array
 * @param firstAddOn The first add elements on
 * @param secondAddON The second add elements on
 */
- (void)setOperatorArray:(NSArray *)operatorArray 
              firstAddOn:(BOOL)firstAddOn 
             secondAddOn:(BOOL)secondAddON;
@end
