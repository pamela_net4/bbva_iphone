/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTCell.h"

@class ATMData;

/**
 * Cell to show schedule from Branch.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ScheduleCell : NXTCell {
    
@private
    
    /**
     * The title label of cell
     */
    UILabel *titleLabel_;
    
    /**
     * Label to show other messages.
     */
    UILabel *otherLabel_;
    
    /**
     * The monday to friday label text.
     */
    UILabel *weekLabel_;
    
    /**
     * The monday to friday hours label.
     */
    UILabel *weekHoursLabel_;
    
    /**
     * The saturday label text.
     */
    UILabel *saturdayLabel_;
    
    /**
     * The saturday hours label.
     */
    UILabel *saturdayHoursLabel_;
    
    /**
     * The sunday label text.
     */
    UILabel *sundayLabel_;
    
    /**
     * The sunday hours label.
     */
    UILabel *sundayHoursLabel_;
    
    /**
     * The atm to show hours.
     */
    ATMData *poi_;
    
}

/**
 * Provides read-write access to titleLabel. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides read-write access to otherLabel. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *otherLabel;

/**
 * Provides read-write access to mondayToFridayLabel. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *weekLabel;

/**
 * Provides read-write access to mondayToFridayHoursLabel. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *weekHoursLabel;

/**
 * Provides read-write access to saturdayLabel. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *saturdayLabel;

/**
 * Provides read-write access to saturdayHoursLabel. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *saturdayHoursLabel;

/**
 * Provides read-write access to sundayLabel. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *sundayLabel;

/**
 * Provides read-write access to sundayHoursLabel. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *sundayHoursLabel;

/**
 * Provides read-write access to poi.
 */
@property (nonatomic, readwrite, retain) ATMData *poi;

/**
 * Creates and returns an autoreleased ScheduleCell constructed from a NIB file.
 *
 * @return The autoreleased ScheduleCell constructed from a NIB file.
 */
+ (ScheduleCell *)scheduleCell;

/**
 * Returns the cell height.
 *
 * @return The cell height.
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier.
 *
 * @return The cell identifier.
 */
+ (NSString *)cellIdentifier;

@end
