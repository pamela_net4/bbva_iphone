//
//  CheckAccountView.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/14/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "NXTTableCell.h"

@interface CheckAccountView : NXTTableCell
{
    
    /*
     *  Label for the alias of the operation
     */
    UILabel *titleLabel_;
    /*
     *  Label for the service of the operation
     */
    UILabel *subtitleLabel_;
    /*
     *  Label for the service of the operation
     */
    UILabel *subtitleLabel2_;
    /*
     *  Label for the day of the month
     */
    UILabel *dayLabel_;
    /*
     *  Image to indicate if the cell is selected or not
     */
    UIImageView *checkImageView_;
    
    /**
     * Check active flag
     */
    BOOL checkActive_;
}
/**
 * Provides read-write access to the titleLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;
/**
 * Provides read-write access to the subtitleLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *subtitleLabel;

/**
 * Provides read-write access to the subtitleLabel 2 and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *subtitleLabel2;
/**
 * Provides read-write access to the dayLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *dayLabel;
/**
 * Provides read-write access to the checkImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *checkImageView;


/**
 * Provides read-write access to the checkActive
 */
@property (nonatomic, readwrite, assign) BOOL checkActive;

/**
 * Creates and returns an autoreleased CheckFOCell constructed from a NIB file
 *
 * @return The autoreleased CheckFOCell constructed from a NIB file
 */
+ (CheckAccountView *)checkAccountView;


/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;
@end
