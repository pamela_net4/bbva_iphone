/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "SealCell.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"SealCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"SealCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 104.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                      12.0f


#pragma mark -

@implementation SealCell

#pragma mark -
#pragma mark Properties

@synthesize topTextLabel = topTextLabel_;
@synthesize sealImageView = sealImageView_;
@synthesize seal = seal_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [topTextLabel_ release];
    topTextLabel_ = nil;
    
    [sealImageView_ release];
    sealImageView_ = nil;
        
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    
    [NXT_Peru_iPhoneStyler styleLabel:topTextLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor grayColor]];
    [topTextLabel_ setText:NSLocalizedString(SHARE_SEAL_TEXT_KEY, nil)];
}

/*
 * Creates and returns an autoreleased SealCell constructed from a NIB file
 */
+ (SealCell *)sealCell {
    
    SealCell *result = (SealCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;    
}

#pragma mark -
#pragma mark Getters and setters


/**
 * Sets the Seal
 */
- (void)setSeal:(NSString *)seal {
    
    seal_ = seal;
    
//    NSMutableData *frontNSData = [[[NSMutableData alloc] init] autorelease];
    NSData *data=[Tools base64DataFromString:seal ];
    //[Tools convertFromBase64Text:seal toData:frontNSData];
    UIImage *image = [[[UIImage alloc] initWithData:data] autorelease];
    sealImageView_.image = image;
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    return CELL_HEIGHT;
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

@end