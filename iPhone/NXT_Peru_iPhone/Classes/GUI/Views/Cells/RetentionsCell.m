/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "RetentionsCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTTableCell.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"RetentionsCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"RetentionsCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 60.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        12.0f


#pragma mark -

/**
 * RetentionsCellProvider private category
 */
@interface RetentionsCellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (RetentionsCellProvider *)getInstance;

/**
 * Creates and returns an autoreleased RetentionsCell constructed from a NIB file
 *
 * @return The autoreleased RetentionsCell constructed from a NIB file
 */
- (RetentionsCell *)retentionsCell;

@end


#pragma mark -

@implementation RetentionsCellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * RetentionsCellProvier singleton only instance
 */
static RetentionsCellProvider *retentionsCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([RetentionsCellProvider class]) {
        
        if (retentionsCellProviderInstance_ == nil) {
            
            retentionsCellProviderInstance_ = [super allocWithZone:zone];
            return retentionsCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (RetentionsCellProvider *)getInstance {
    
    if (retentionsCellProviderInstance_ == nil) {
        
        @synchronized([RetentionsCellProvider class]) {
            
            if (retentionsCellProviderInstance_ == nil) {
                
                retentionsCellProviderInstance_ = [[RetentionsCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return retentionsCellProviderInstance_;
    
}

#pragma mark -
#pragma mark RetentionsCell creation

/*
 * Creates and returns an autoreleased RetentionsCell constructed from a NIB file
 */
- (RetentionsCell *)retentionsCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    RetentionsCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation RetentionsCell

#pragma mark -
#pragma mark Properties

@synthesize backgroundImageView = backgroundImageView_;
@synthesize separatorImageView = separatorImageView_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
        
    [backgroundImageView_ release];
    backgroundImageView_ = nil;
    
    [separatorImageView_ release];
    separatorImageView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = YES;
    self.showSeparator = YES;

    [NXT_Peru_iPhoneStyler styleLabel:self.leftTextLabel withFontSize:TEXT_FONT_SIZE color:[UIColor BBVACoolGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:self.rightTextLabel withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyColor]];
    
    UIImageView *background = [[[UIImageView alloc] initWithImage:[[ImagesCache getInstance] imageNamed:BLUE_CELL_BACKGROUND_IMAGE_FILENAME]] autorelease];
    background.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    
    [separatorImageView_ setImage:[[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
    
    self.backgroundView = background;

}

/*
 * Creates and returns an autoreleased RetentionsCell constructed from a NIB file
 */
+ (RetentionsCell *)retentionsCell {
    
    return [[RetentionsCellProvider getInstance] retentionsCell];
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    return CELL_HEIGHT;
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

@end