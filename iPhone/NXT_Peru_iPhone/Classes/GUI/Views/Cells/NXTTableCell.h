/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "SingletonBase.h"

//Forward declarations
@class NXTTableCell;

/**
 * Provider to obtain a NXTTableCell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTTableCellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary NXTTableCell to create it from a NIB file
     */
    NXTTableCell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary NXTTableCell
 */
@property (nonatomic, readwrite, assign) IBOutlet NXTTableCell *auxView;

@end


/**
 * Basic cell of the application. It has a left text, right text and a disclosure button
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTTableCell : UITableViewCell {
@private
    /**
     * Left text label
     */
    UILabel *leftTextLabel_;
    
    /**
     * Right text label
     */
    UILabel *rightTextLabel_;
    
    /**
     * Cell separator top image.
     */
    UIImageView *cellSeparatorTopImage_;
    
    /**
     * Cell separator bottom image
     */
    UIImageView *cellSeparatorBottomImage_;
    
    /**
     * Disclosure arrow
     */
    UIImageView *disclosureArrow_;
    
}

/**
 * Provides readwrite access to the leftText
 */
@property (nonatomic, readwrite, copy) NSString *leftText;

/**
 * Provides readwrite access to the rightText
 */
@property (nonatomic, readwrite, copy) NSString *rightText;

/**
 * Provides readwrite access to the showDisclosureArrow
 */
@property (nonatomic, readwrite, assign) BOOL showDisclosureArrow;

/**
 * Provides readwrite access to the showSeparator
 */
@property (nonatomic, readwrite, assign) BOOL showSeparator;

/**
 * Provides read-write access to the leftTextLabel_ and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *leftTextLabel;

/**
 * Provides read-write access to the rightTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *rightTextLabel;

/**
 * Provides read-write access to the cell separator image and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *cellSeparatorBottomImage;

/**
 * Provides read-write access to the cell separator image and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *cellSeparatorTopImage;

/**
 * Provides read-write access to the cell disclosureArrow image and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *disclosureArrow;

/**
 * Creates and returns an autoreleased NXTTableCell constructed from a NIB file
 *
 * @return The autoreleased NXTTableCell constructed from a NIB file
 */
+ (NXTTableCell *)NXTTableCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end
