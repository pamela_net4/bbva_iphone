/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <QuartzCore/QuartzCore.h>
#import "NXTCell.h"

/**
 * Cell to show information of an phone
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PhoneCell : NXTCell {
@private
    
    /**
     * Text phone label
     */
    UILabel *phoneLabel_;
    
    /**
     * Phone number label
     */
    UILabel *phoneNumberLabel_;
    
    /**
     * The icon image view.
     */
    UIImageView *iconImageView_;
	
	/**
     * Disclosure arrow
     */
    UIImageView *disclosureArrow_;
	
}

/**
 * Provides read-write access to the phone label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *phoneLabel;

/**
 * Provides read-write access to the phone number label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *phoneNumberLabel;

/**
 * Provides read-write access to iconImageView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *iconImageView;

/**
 * Provides read-write access to disclosureArrow. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *disclosureArrow;

/**
 * Provides readwrite access to the showDisclosureArrow
 */
@property (nonatomic, readwrite, assign) BOOL showDisclosureArrow;

/**
 * Provides read-write access to the phone number
 */
@property (nonatomic, readwrite, copy) NSString *phoneNumber;


/**
 * Creates and return an autoreleased PhoneCell constructed from a NIB file
 *
 * @return AddressCell The autoreleased PhoneCell constructed from a NIB file
 */
+ (PhoneCell *)phoneCell;

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight;

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier;

@end
