//
//  RadioBBVAMenuCell.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/28/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NXTTableCell.h"

@class TextCell;

@interface RadioBBVAMenuCell : NXTTableCell{
    
    UIImageView *iconOptionImageView_;
    
    UILabel *topTextLabel_;
    
    UIView *tagSelectionView_;
    
    /**
     * Check active flag
     */
    BOOL checkActive_;
}

/**
 * Provides read-write access to the topTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *topTextLabel;

/**
 * Provides read-write access to the checkImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *iconOptionImageView;


/**
 * Provides read-write access to the checkImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *tagSelectionView;

/**
 * Provides read-write access to the checkActive
 */
@property (nonatomic, readwrite, assign) BOOL checkActive;

/**
 * Creates and returns an autoreleased CheckCell constructed from a NIB file
 *
 * @return The autoreleased CheckCell constructed from a NIB file
 */
+ (RadioBBVAMenuCell *)radioBBVAMenuCell;


/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

/**
 * Sets the action label text
 *
 * @param anActionText The action label text
 */
- (void)setTitleText:(NSString *)anTitleText;

/**
 * Sets the action icon image
 *
 * @param anActionIcon The action icon image
 */
- (void)setMenuIcon:(UIImage *)anMenuIcon;



@end
