/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTCell.h"
#import "Constants.h"

@class NXTSwitchCell;

@protocol NXTSwitchCellDelegate

/**
 * Tells the delegate that the specified cell is change switch.
 *
 * @param keyCell: The key cell.
 * @param isTurnedOn: YES if switch turn ON. NO otherwise.
 */
- (void)didSwitchCellWithIndexPath:(NSInteger)keyCell turned:(BOOL)isTurnedOn;

@end

/**
 * This class extends from UITableViewCell and add a UISwitch in view.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTSwitchCell : NXTCell {
    
@private
    
    /**
     * The switch in view.
     */
    UISwitch *optionSwitch_;
    
    /**
     * Text label.
     */
    UILabel *textLabel_;
    
    /**
     * The icon image view.
     */
    UIImageView *imageView_;
    
    /**
     * The key of cell.
     */
    NSInteger keyCell_;
    
    /**
     * The delegate.
     */
    id<NXTSwitchCellDelegate> delegate_;
    
}

/**
 * Provides read-write access to switch. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UISwitch *optionSwitch;

/**
 * Provides read-write access to textLabel_. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *textLabel;

/**
 * Provides read-write access to imageView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *imageView;

/**
 * Creates and returns an autoreleased NXTSwitchCell contructed from NIB file.
 *
 * @return An autoreleased NXTSwitchCell contructed from NIB file.
 */
+ (NXTSwitchCell *)nxtSwitchCell;

/**
 * Returns the cell height.
 *
 * @return The cell height.
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier.
 *
 * @return The cell identifier.
 */
+ (NSString *)cellIdentifier;

/**
 * Action when switch state is changed.
 */
- (IBAction)switchChangedState:(id)sender;

/**
 * Set delegate and index path of cell.
 *
 * @param delegate: The delegate to tells information.
 * @param key: The key to identifier a cell.
 */
- (void)setDelegate:(id<NXTSwitchCellDelegate>)delegate withKey:(NSInteger)key;

@end
