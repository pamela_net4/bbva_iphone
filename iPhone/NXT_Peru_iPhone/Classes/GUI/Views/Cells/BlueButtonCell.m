/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BlueButtonCell.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"


/**
 * Define the BlueButtonCell NIB file name
 */
#define NIB_FILE_NAME                                               @"BlueButtonCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"BlueButtonCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 68.0f


#pragma mark -

/**
 * BlueButtonCellProvider private category
 */
@interface BlueButtonCellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (BlueButtonCellProvider *)getInstance;

/**
 * Creates and returns an autoreleased BlueButtonCell constructed from a NIB file
 *
 * @return The autoreleased BlueButtonCell constructed from a NIB file
 */
- (BlueButtonCell *)blueButtonCell;

@end


#pragma mark -

@implementation BlueButtonCellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * BlueButtonCellProvier singleton only instance
 */
static BlueButtonCellProvider *blueButtonCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([BlueButtonCellProvider class]) {
        
        if (blueButtonCellProviderInstance_ == nil) {
            
            blueButtonCellProviderInstance_ = [super allocWithZone:zone];
            return blueButtonCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (BlueButtonCellProvider *)getInstance {
    
    if (blueButtonCellProviderInstance_ == nil) {
        
        @synchronized([BlueButtonCellProvider class]) {
            
            if (blueButtonCellProviderInstance_ == nil) {
                
                blueButtonCellProviderInstance_ = [[BlueButtonCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return blueButtonCellProviderInstance_;
    
}

#pragma mark -
#pragma mark BlueButtonCell creation

/*
 * Creates and returns aa autoreleased BlueButtonCell constructed from a NIB file
 */
- (BlueButtonCell *)blueButtonCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    BlueButtonCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation BlueButtonCell

#pragma mark -
#pragma mark Properties

@synthesize blueButton = blueButton_;
@synthesize separatorImageView = separatorImageView_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [blueButton_ release];
    blueButton_ = nil;
    
    [separatorImageView_ release];
    separatorImageView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell button
 */
- (void)awakeFromNib {
    
    [NXT_Peru_iPhoneStyler styleBlueButton:blueButton_];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    separatorImageView_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    [self setSeparatorLineVisible:NO];
    
}

/*
 * Creates and returns an autoreleased BlueButtonCell constructed from a NIB file
 */
+ (BlueButtonCell *)blueButtonCell {
    
    return [[BlueButtonCellProvider getInstance] blueButtonCell];
    
}

#pragma mark -
#pragma mark Cell configuration

/*
 * Sets the button title
 */
- (void)setButtonTitle:(NSString *)aTitle {
    
    [blueButton_ setTitle:aTitle forState:UIControlStateNormal];
    
}

/*
 * Adds the target and action for the cell button
 */
- (void)addTarget:(id)aTarget action:(SEL)anAction {
    
    [blueButton_ addTarget:aTarget action:anAction forControlEvents:UIControlEventTouchUpInside];
    
}

/*
 * Shows or hides the separator line
 */
- (void)setSeparatorLineVisible:(BOOL)visible {
    
    separatorImageView_.hidden = !visible;
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    
    return CELL_HEIGHT;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return CELL_IDENTIFIER;
    
}

@end
