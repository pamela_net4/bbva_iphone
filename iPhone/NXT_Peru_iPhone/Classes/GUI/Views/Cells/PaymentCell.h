/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class PaymentCell;

/**
 * Provider to obtain a PaymentCell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentCellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary PaymentCell to create it from a NIB file
     */
    PaymentCell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary PaymentCell
 */
@property (nonatomic, readwrite, assign) IBOutlet PaymentCell *auxView;

@end


/**
 * GlobalPosition cell. It's a subclass of NXTTableCell but it has two labels on the right instead of one.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentCell : NXTTableCell {
@private    
    
    /**
     * Table
     */
    UILabel *titleLabel_;
    
}

/**
 * Provides read-write access to the titleLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Creates and returns an autoreleased PaymentCell constructed from a NIB file
 *
 * @return The autoreleased PaymentCell constructed from a NIB file
 */
+ (PaymentCell *)paymentCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;


@end