/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */



#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

//Forward declarations
@class OptionCell;

/**
 * Provider to obtain a OptionCell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface OptionCellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary OptionCell to create it from a NIB file
     */
    OptionCell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary OptionCell
 */
@property (nonatomic, readwrite, assign) IBOutlet OptionCell *auxView;

@end

/**
 * Cell for an option of an options table
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface OptionCell : NXTTableCell {
@private
    /**
     * Cell separator image
     */
    UIImageView *image_;
    
    /**
     * Selector
     */
    SEL optionSelector_;
}

/**
 * Provides read-write access to the selector to call when this cell is tapped
 */
@property (nonatomic, readwrite, assign) SEL optionSelector;

/**
 * Provides read-write access to the cell image and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *image;

/**
 * Provides read-write access to the cell icon
 */
@property (nonatomic, readwrite, retain) UIImage *icon;

/**
 * Creates and returns an autoreleased OptionCell constructed from a NIB file
 *
 * @return The autoreleased OptionCell constructed from a NIB file
 */
+ (OptionCell *)optionCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end