//
//  CheckView.m
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 26/11/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "CheckView.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKStringListSelectionButton.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TransactionView NIB file name
 */
#define NIB_FILE_NAME                                               @"CheckView"

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              14.0f


#pragma mark -

@implementation CheckView

#pragma mark -
#pragma mark Properties

@synthesize topTextLabel = topTextLabel_;
@synthesize checkImageView = checkImageView_;
@synthesize checkActive = checkActive_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [topTextLabel_ release];
    topTextLabel_ = nil;
    
    [checkImageView_ release];
    checkImageView_ = nil;

    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {

        [self addSubview:[[[UINib nibWithNibName:NIB_FILE_NAME bundle:nil] instantiateWithOwner:self options:nil] objectAtIndex:0]];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        [self addSubview:[[[UINib nibWithNibName:NIB_FILE_NAME bundle:nil] instantiateWithOwner:self options:nil] objectAtIndex:0]];
    }
    return self;
}


/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the View elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    //[NXT_Peru_iPhoneStyler styleLabel:topTextLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor grayColor]];
    
    checkActive_ = FALSE;
    [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_UNSELECTED_RADIO]];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkDetected)];
    //singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [checkImageView_ setUserInteractionEnabled:YES];
    [checkImageView_ addGestureRecognizer:singleTap];
    
}

/*
 * Creates and returns an autoreleased CheckComboView constructed from a NIB file
 */
+ (CheckView *)checkView {
    
    CheckView *result = (CheckView *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Sets the Seal
 */
- (void)setCheckActive:(BOOL)checkActive {
    
    checkActive_ = checkActive;
    
    if (checkActive) {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_SELECTED_RADIO]];
        
    } else {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_UNSELECTED_RADIO]];
        
    }
    
    [self.delegate checkView:self checkStateChanged:checkActive_];
    
}

-(void)checkDetected{
    
    [self setCheckActive:!checkActive_];
    
}


@end