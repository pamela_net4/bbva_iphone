//
//  CheckTypeTransferCell.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 10/5/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "NXTTableCell.h"

@interface CheckTypeTransferCell : NXTTableCell
{
    UILabel *titleLabel_;
    
    UILabel *comisionLabel_;
    
    UILabel *comisionValueLabel_;
    
    UILabel *totalPaymentLabel_;
    
    UILabel *totalPaymentValueLabel_;
    
    /*
     *  Image to indicate if the cell is selected or not
     */
    UIImageView *checkImageView_;
    /**
     * Check active flag
     */
    BOOL checkActive_;
}
/**
 * Provides read-write access to the titleLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides read-write access to the comisionLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *comisionLabel;
/**
 * Provides read-write access to the comisionValueLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *comisionValueLabel;
/**
 * Provides read-write access to the totalPaymentLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *totalPaymentLabel;
/**
 * Provides read-write access to the totalPaymentValueLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *totalPaymentValueLabel;

/**
 * Provides read-write access to the checkImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *checkImageView;

/**
 * Provides read-write access to the checkActive
 */
@property (nonatomic, readwrite, assign) BOOL checkActive;


/**
 * Creates and returns an autoreleased CheckTypeTransferCell constructed from a NIB file
 *
 * @return The autoreleased CheckTypeTransferCell constructed from a NIB file
 */
+ (CheckTypeTransferCell *)checkTypeTransferCell;



@end
