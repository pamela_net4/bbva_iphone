//
//  CheckChannelCell.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 13/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "CheckChannelCell.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"CheckChannelCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"CheckChannelCell"

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              12.0f



@implementation CheckChannelCell

@synthesize contentView = contentView_;
@synthesize titleTextLabel = titleTextLabel_;
@synthesize checkImageView = checkImageView_;
@synthesize checkActive = checkActive_;

- (void)dealloc {
    
    [titleTextLabel_ release];
    titleTextLabel_ = nil;
    
    [checkImageView_ release];
    checkImageView_ = nil;
    
    [contentView_ release];
    contentView_ = nil;
    
    [super dealloc];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    [contentView_ setBackgroundColor:[UIColor clearColor]];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [NXT_Peru_iPhoneStyler styleLabel:titleTextLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlackColor]];
    [titleTextLabel_ setText:NSLocalizedString(SHARE_SEAL_TEXT_KEY, nil)];
    
    checkActive_ = YES;
}

+ (CheckChannelCell *) checkChannelCell {

    CheckChannelCell *result = (CheckChannelCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    [self awakeFromNib];
    
    return result;
}

- (void) setCheckActive:(BOOL)checkActive {

    checkActive_ = checkActive;
    
    if (checkActive) {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_SELECTED_RADIO]];
        
    } else {
    
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_UNSELECTED_RADIO]];
        
    }
    
}

+ (CGFloat)cellHeightForFirstAddOn:(BOOL)firstAddOn
                       secondAddOn:(BOOL)secondAddOn {
    
    CGFloat result = 48.0f;
    return result;
    
}

+ (NSString *) cellIdentifier {

    return CELL_IDENTIFIER;
    
}

@end
