//
//  FOEmailMokCell.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 17/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NXTTableCell.h"
#import "MOKStringListSelectionButton.h"

@protocol FOEmailMokCellDelegate

-(void) emailSwitchButtonHasBeenTapped:(BOOL)on;

@end

@interface FOEmailMokCell : NXTTableCell {

@private
    
    UIImageView *backgroundImageView_;
    
    UILabel *titleLabel_;
    
    UISwitch *emailSwitch_;
    
    MOKStringListSelectionButton *emailComboButton_;
    
    NSMutableArray *emailArray_;
    
    id<FOEmailMokCellDelegate> delegate_;
}

@property (nonatomic, readwrite, retain) NSArray *emailArray;

@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *emailComboButton;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView *backgroundImageView;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

@property (nonatomic, readwrite, retain) IBOutlet UISwitch *emailSwitch;

@property (nonatomic, readwrite, assign) id<FOEmailMokCellDelegate> delegate;

+ (FOEmailMokCell *) FOEmailMokCell;

+ (CGFloat)cellHeightForFirstAddOn:(BOOL)firstAddOn ;

+ (NSString *)cellIdentifier;

- (IBAction)switchButtonTapped;

- (void)setConfigurationForFirstAddOn:(BOOL)firstAddOn;

@end
