/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TransferSummaryCell.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TextCell NIB file name
 */
#define NIB_FILE_NAME                                               @"TransferSummaryCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"TransferSummaryCell"

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              11.0f

/**
 * Defines the title text font size
 */
#define TITLE_TEXT_FONT_SIZE                                        12.0f

/**
 * Defines the big size of the font
 */
#define BIG_TEXT_FONT_SIZE                                          17.0f

/**
 * Defines the different heights
 */
#define MARGIN_HEIGHT                                               5.0f
#define LEFT_MARGIN                                                 10.0f
#define LABEL_HEIGHT                                                15.0f
#define SPECIAL_LABEL_HEIGHT                                        22.0f

#pragma mark -

#pragma mark -

@implementation TransferSummaryCell

#pragma mark -
#pragma mark Properties

@synthesize information = information_;
@synthesize itfMessage = itfMessage_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [information_ release];
    information_ = nil;
    
    for (UILabel *label in labelsArray_) {
        
        [label release];
        label = nil;
        
    }
    
    [labelsArray_ release];
    labelsArray_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;

    labelsArray_ = [[NSMutableArray alloc] init];
    information_ = [[NSMutableArray alloc] init];
}

/*
 * Creates and returns an autoreleased TransferSummaryCell constructed from a NIB file
 */
+ (TransferSummaryCell *)transferSummaryCell {
    
    TransferSummaryCell *result = (TransferSummaryCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;    
}

#pragma mark -
#pragma mark Getters and setters


/**
 * Sets the Seal
 */
- (void)setInformation:(NSArray *)information {

    if (information != information_) {
        
        [information_ removeAllObjects];
        [information_ addObjectsFromArray:information];

    }
    
    
    if ([labelsArray_ count] > 0) {
        
        for (UILabel *label in labelsArray_) {
            
            [label release];
            label = nil;
            
        }
        
        [labelsArray_ removeAllObjects];
        
    }
    
    CGFloat yPosition = MARGIN_HEIGHT;

    NSString *title;
    NSString *detail;
    
    for (TitleAndAttributes *titleAndAttributes in information_) {
        
        title = [titleAndAttributes titleString];
        detail = [[titleAndAttributes attributesArray] objectAtIndex:0];
        
        UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, yPosition, 300.0f, LABEL_HEIGHT)] autorelease];
        [NXT_Peru_iPhoneStyler styleLabel:titleLabel withFontSize:TITLE_TEXT_FONT_SIZE color:[UIColor darkGrayColor]];
        yPosition = yPosition + LABEL_HEIGHT;
        
        UILabel *detailLabel;
        
        if ([title isEqualToString:NSLocalizedString(TRANSFER_TO_MY_ACCOUNTS_AMOUNT_TEXT_KEY, nil)]) {
            
            detailLabel = [[[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, yPosition, 300.0f, SPECIAL_LABEL_HEIGHT)] autorelease];
            [NXT_Peru_iPhoneStyler styleLabel:detailLabel withFontSize:BIG_TEXT_FONT_SIZE color:[UIColor BBVAMagentaColor]];
            
        } else {
            
            detailLabel = [[[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, yPosition, 300.0f, LABEL_HEIGHT)] autorelease];
            
            if (([title isEqualToString:NSLocalizedString(AMOUNT_TEXT_KEY, nil)]) || 
                ([title isEqualToString:NSLocalizedString(FINAL_AMOUNT_TEXT_KEY, nil)])) {
                
                [NXT_Peru_iPhoneStyler styleLabel:detailLabel withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAMagentaColor]];
                
            } else {
                
                [NXT_Peru_iPhoneStyler styleLabel:detailLabel withFontSize:TEXT_FONT_SIZE color:[UIColor grayColor]];

            }

        }
        
        [titleLabel setText:title];
        [detailLabel setText:detail];
        
        yPosition = yPosition + detailLabel.frame.size.height + MARGIN_HEIGHT;
       
        titleLabel.backgroundColor = [UIColor clearColor];
        detailLabel.backgroundColor = [UIColor clearColor];

        [labelsArray_ addObject:titleLabel];
        [labelsArray_ addObject:detailLabel];

        [self addSubview:titleLabel];
        [self addSubview:detailLabel];
        
    }
    
    if (![itfMessage_ isEqualToString:@""]) {
        UILabel *itfLabel = [[[UILabel alloc] initWithFrame:CGRectMake(LEFT_MARGIN, yPosition, 300.0f, 2*LABEL_HEIGHT)] autorelease];
        itfLabel.backgroundColor = [UIColor clearColor];
        [NXT_Peru_iPhoneStyler styleLabel:itfLabel withFontSize:TITLE_TEXT_FONT_SIZE color:[UIColor darkGrayColor]];
        itfLabel.numberOfLines = 2;
        itfLabel.textAlignment = UITextAlignmentCenter;
        itfLabel.text = itfMessage_;
        [self addSubview:itfLabel];
    }
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeightForArray:(NSArray *)array itf:(BOOL)itf {
    
    CGFloat result = 0.0f;
    //labels
    result = [array count] * LABEL_HEIGHT * 2 + [array count] * MARGIN_HEIGHT;
    
    if (itf) {
        result = result + 2*LABEL_HEIGHT + MARGIN_HEIGHT;
    }
    
    //ajustes
    result = result + (SPECIAL_LABEL_HEIGHT - LABEL_HEIGHT) + 2 * MARGIN_HEIGHT;
    
    return result;
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

@end