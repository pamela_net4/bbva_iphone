/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MapWarningCell.h"
#import "Tools.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"


/**
 * Define the MapWarningCell NIB file name
 */
NSString * const kMapWarningCellNibFileName = @"MapWarningCell";

/**
 * Defines the cell identifier
 */
NSString * const kMapWarningCellIdentifier = @"MapWarningCell";

/**
 * Defines the cell font size
 */
CGFloat const kMapWarningCellFontSize = 15.0f;

/**
 * Defines the label horizontal and vertical offset to the cell border
 */
CGFloat const kMapWarningCellLabelOffsetToBorder = 5.0f;


#pragma mark -

@implementation MapWarningCell

#pragma mark -
#pragma mark Properties

@synthesize warningLabel = warningLabel_;
@dynamic warningText;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [warningLabel_ release];
    warningLabel_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [NXT_Peru_iPhoneStyler styleLabel:warningLabel_
                        withFontSize:kMapWarningCellFontSize
                               color:[UIColor whiteColor]];
    self.backgroundColor = [UIColor clearColor];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}

/*
 * Creates and returns an autoreleased MapWarningCell constructed from a NIB file
 */
+ (MapWarningCell *)mapWarningCell {
    
    return (MapWarningCell *)[NibLoader loadObjectFromNIBFile:kMapWarningCellNibFileName];
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height for the given information
 */
+ (CGFloat)cellHeightForText:(NSString *)aCellText
                       width:(CGFloat)aCellWidth {
    
    UIFont *font = [UIFont systemFontOfSize:kMapWarningCellFontSize];
    CGFloat totalLabelOffset = (2.0f * kMapWarningCellLabelOffsetToBorder);
    CGFloat labelWidth = aCellWidth - totalLabelOffset;
    CGSize labelMaxSize = CGSizeMake(labelWidth, 1000000.0f);
    
    CGSize realSize = [aCellText sizeWithFont:font 
                            constrainedToSize:labelMaxSize];
                       
    
    return realSize.height + totalLabelOffset;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return kMapWarningCellIdentifier;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the warning text
 *
 * @return The warning text
 */
- (NSString *)warningText {
    
    return warningLabel_.text;
    
}

/*
 * Sets the warning text
 * 
 * @param aValue The new warning text
 */
- (void)setWarningText:(NSString *)aValue {

    warningLabel_.text = aValue;
    
}

@end
