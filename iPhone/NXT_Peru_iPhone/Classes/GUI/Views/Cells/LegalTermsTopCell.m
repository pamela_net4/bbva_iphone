/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "LegalTermsTopCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"LegalTermsTopCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"LegalTermsTopCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 92.0f


#pragma mark -

@implementation LegalTermsTopCell

#pragma mark -
#pragma mark Properties

@synthesize switchBgImageView = switchBgImageView_;
@synthesize legalTermsSwitch = legalTermsSwitch_;
@synthesize warningImageView = warningImageView_;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [switchBgImageView_ release];
    switchBgImageView_ = nil;
    
    [legalTermsSwitch_ release];
    legalTermsSwitch_ = nil;
    
    [warningImageView_ release];
    warningImageView_ = nil;
        
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [NXT_Peru_iPhoneStyler styleLabel:self.leftTextLabel 
                         withFontSize:10.0f 
                                color:[UIColor grayColor]];

    [self.leftTextLabel setNumberOfLines:3];
    
    self.leftText = NSLocalizedString(LEGAL_TERMS_TEXT_KEY, nil);
    
    switchBgImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    legalTermsSwitch_.on = NO;
    
    [warningImageView_ setImage:[[ImagesCache getInstance] imageNamed:BLUE_ICON_ALERT_IMAGE_FILE_NAME]];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1){
        
        [legalTermsSwitch_ setOnTintColor:[UIColor BBVABlueSpectrumColor]];
        [legalTermsSwitch_ setTintColor: [UIColor BBVABlueSpectrumColor]];
    }

}

/*
 * Creates and returns an autoreleased LegalTermsTopCell constructed from a NIB file
 */
+ (LegalTermsTopCell *)legalTermsTopCell {
    
    LegalTermsTopCell *result = (LegalTermsTopCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    return CELL_HEIGHT;
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

/**
 * Switch state changed
 */
- (IBAction)warningButtonTapped {
    
    [delegate_ warningButtonHasBeenTapped];

}


@end