/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "LoginInfoCell.h"
#import "Constants.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "StringKeys.h"
#import "NXTTextField.h"
#import "MOKUILimitedLengthTextField.h"
#import "CheckView.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "NXTComboButtonLogin.h"
#import "FrequentCard.h"

/**
 * Define the LoginInfoCell NIB file name
 */
#define NIB_FILE_NAME                                               @"LoginInfoCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"LoginInfoCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 327.0f


/**
 * Defines the text field font size
 */
#define TEXT_FIELD_FONT_SIZE                                        14.0f

/**
 * Defines the text field font size
 */
#define TEXT_FIELD_FONT_SMALL_SIZE                                  14.0f

/**
 * Defines the vertical gap between two near elements
 */
#define VERTICAL_DISTANCE_TO_NEAR_ELEMENT                           5.0f

/**
 * Defines the vertical gap between two far away elements
 */
#define VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT                      10.0f

/**
 * Defines the requred version of the iOS to perform certain operations
 */
#define MINIMUM_EDITABLE_VIEW_REQUIRED_IOS_VERSION                          @"3.2"


#pragma mark -

/**
 * LoginInfoCellProvider private category
 */
@interface LoginInfoCellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (LoginInfoCellProvider *)getInstance;

/**
 * Creates and returns an autoreleased LoginInfoCell constructed from a NIB file
 *
 * @return The autoreleased LoginInfoCell constructed from a NIB file
 */
- (LoginInfoCell *)loginInfoCell;

@end


#pragma mark -

@implementation LoginInfoCellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * LoginInfoCellProvier singleton only instance
 */
static LoginInfoCellProvider *loginInfoCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([LoginInfoCellProvider class]) {
        
        if (loginInfoCellProviderInstance_ == nil) {
            
            loginInfoCellProviderInstance_ = [super allocWithZone:zone];
            return loginInfoCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (LoginInfoCellProvider *)getInstance {
    
    if (loginInfoCellProviderInstance_ == nil) {
        
        @synchronized([LoginInfoCellProvider class]) {
            
            if (loginInfoCellProviderInstance_ == nil) {
                
                loginInfoCellProviderInstance_ = [[LoginInfoCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return loginInfoCellProviderInstance_;
    
}

#pragma mark -
#pragma mark LoginInfoCell creation

/*
 * Creates and returns aa autoreleased LoginInfoCell constructed from a NIB file
 */
- (LoginInfoCell *)loginInfoCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    LoginInfoCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end

@interface LoginInfoCell (Private)

#pragma mark -
#pragma mark Private methods

/**
 * This method run when animation cell is ended.
 * 
 * @param animationID: An NSString containing an optional application-supplied identifier. This is the identifier that is passed to the beginAnimations:context: method. This argument can be nil.
 * @param finished: An NSNumber object containing a Boolean value. The value is YES if the animation ran to completion before it stopped or NO if it did not.
 * @param context: An optional application-supplied context. This is the context data passed to the beginAnimations:context: method. This argument can be nil.
 * @private
 */
- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context;

@end

#pragma mark -

@implementation LoginInfoCell


#pragma mark -
#pragma mark Properties

@synthesize userLabel = userLabel_;
@synthesize userTextField = userTextField_;
@synthesize frequentCardCombo = frequentCardCombo_;
@synthesize frequentCardCheck = frequentCardCheck_;
@synthesize aliasTextField = aliasTextField_;
@synthesize passwordLabel = passwordLabel_;
@synthesize passwordTextField = passwordTextField_;
@synthesize loginButton = loginButton_;
@synthesize loginInformationLabel = loginInformationLabel_;
@synthesize suscribedButton = suscribedButton_;
@synthesize forgotPasswordHelperButton = forgotPasswordHelperButton_;
@synthesize cellSeparatorImage = cellSeparatorImage_;
@synthesize hideCellView = hideCellView_;
@synthesize frequentCardHelperButton = frequentCardHelperButton_;
@synthesize cardfrequentLabel = cardfrequentLabel_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [userLabel_ release];
    userLabel_ = nil;
    
    [userTextField_ release];
    userTextField_ = nil;
    
    [cardfrequentLabel_ release];
    cardfrequentLabel_ = nil;
    
    [frequentCardCombo_ release];
    frequentCardCombo_ = nil;
    
    [frequentCardCheck_ release];
    frequentCardCheck_ = nil;
    
    [aliasTextField_ release];
    aliasTextField_ = nil;
    
    [passwordLabel_ release];
    passwordLabel_ = nil;
    
    [passwordTextField_ release];
    passwordTextField_ = nil;
    
    [loginButton_ release];
    loginButton_ = nil;
    
    [loginInformationLabel_ release];
    loginInformationLabel_ = nil;
    
    [frequentCardHelperButton_ release];
    frequentCardHelperButton_ = nil;
    
    [suscribedButton_ release];
    suscribedButton_ = nil;
    
    [forgotPasswordHelperButton_ release];
    forgotPasswordHelperButton_ = nil;
    
    [cellSeparatorImage_ release];
    cellSeparatorImage_ = nil;
    
    [hideCellView_ release];
    hideCellView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles cell elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [NXT_Peru_iPhoneStyler styleLoginInformationCell:self];
    
    [NXT_Peru_iPhoneStyler styleLoginInfoCellLabel:userLabel_];
    userLabel_.text = NSLocalizedString(USER_TEXT_KEY, nil);
    
    [userTextField_ setPlaceholder:NSLocalizedString(HINT_USER_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleTextField:userTextField_ withFontSize:TEXT_FIELD_FONT_SIZE andColor:[UIColor BBVACoolGreyColor]];
    
    
    CGRect frame = frequentCardHelperButton_.frame;
    frame.size = CGSizeMake(16, 16);
    frequentCardHelperButton_.frame = frame;
    
    [NXT_Peru_iPhoneStyler styleLoginInfoCellLabel:cardfrequentLabel_];    
    cardfrequentLabel_.text = NSLocalizedString(FREQUENT_CARD_TEXT_KEY, nil);
    
    [NXT_Peru_iPhoneStyler styleLoginInfoCellLabel:[frequentCardCheck_ topTextLabel]];
    [[frequentCardCheck_ topTextLabel] setText:@""];
    
    [aliasTextField_ setPlaceholder:NSLocalizedString(ALIAS_HINT_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleTextField:aliasTextField_ withFontSize:TEXT_FIELD_FONT_SMALL_SIZE andColor:[UIColor BBVACoolGreyColor]];
    
    
    [NXT_Peru_iPhoneStyler styleLoginInfoCellLabel:passwordLabel_];
    passwordLabel_.text = NSLocalizedString(PASSWORD_TEXT_KEY, nil);
    
    [passwordTextField_ setPlaceholder:NSLocalizedString(HINT_PASSWORD_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleTextField:passwordTextField_ withFontSize:TEXT_FIELD_FONT_SIZE andColor:[UIColor BBVACoolGreyColor]];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:loginButton_];
    [loginButton_ setTitle:NSLocalizedString(LOGIN_TEXT_KEY, nil) forState:UIControlStateNormal];
    
    [NXT_Peru_iPhoneStyler styleWhiteButton:suscribedButton_];
    [suscribedButton_ setBackgroundColor:[UIColor colorWithRed:73.0/255.0 green:154.0/255.0 blue:0.0/255.0 alpha:1]];
    [suscribedButton_ setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [suscribedButton_ setTitle:NSLocalizedString(@"¿Ingresas por primera vez?", nil) forState:UIControlStateNormal];
    
    [NXT_Peru_iPhoneStyler styleWhiteButton:forgotPasswordHelperButton_];
    [forgotPasswordHelperButton_ setTitle:NSLocalizedString(FORGOT_PASSWORD_HELPER_TEXT_KEY, nil) forState:UIControlStateNormal];
    
    [loginInformationLabel_ setNumberOfLines:2];
    [loginInformationLabel_ setTextAlignment:UITextAlignmentCenter];
    [NXT_Peru_iPhoneStyler styleLabel:loginInformationLabel_ withBoldFontSize:10.0f color:[UIColor BBVABlueColor]];
    [loginInformationLabel_ setText:NSLocalizedString(LOGIN_HINT_TEXT_KEY, nil)];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    UIImage *cellSeparatorImage = [imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    cellSeparatorImage_.image = cellSeparatorImage;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    heightContentView_ = CELL_HEIGHT;
}


-(void) updateComponents:(NSMutableArray *) frequentCards stateAdd:(BOOL)add{
    
    if([frequentCards count] == 0){// registrar tarjeta , no hay ninguna
    
        cardfrequentLabel_.hidden = YES;
        frequentCardCombo_.hidden = YES;
        
        [[frequentCardCheck_ topTextLabel] setText:NSLocalizedString(REMEMBER_FREQUENT_CARD_TEXT_KEY, nil)];
        
        frequentCardCheck_.checkActive = NO;
        [aliasTextField_ setHidden:YES];
        
    }
    else {
        
        NSMutableArray * cards = [[NSMutableArray alloc] init];
        [cards addObject:NSLocalizedString(ADD_FREQUENT_CARD_TEXT_KEY, nil)];
        for (FrequentCard * frequentCard in frequentCards) {
            [cards addObject:frequentCard.alias];
        }
        
        NSArray * cardArray = [NSArray arrayWithArray:cards];
        [frequentCardCombo_ setStringsList:cardArray];
        
        if(add){// agregar tarjeta , ya existen otras
        
            cardfrequentLabel_.hidden = NO;
            frequentCardCombo_.hidden = NO;
        
            [frequentCardCombo_ setSelectedIndex:0];
            [[frequentCardCheck_ topTextLabel] setText:NSLocalizedString(REMEMBER_FREQUENT_CARD_TEXT_KEY, nil)];
        
            frequentCardCheck_.checkActive = YES;
            [aliasTextField_ setHidden:NO];
        }
    
        else {//borrar tarjeta
        
            cardfrequentLabel_.hidden = NO;
            frequentCardCombo_.hidden = NO;
        
            frequentCardCheck_.checkActive = NO;
            [aliasTextField_ setHidden:YES];
        
            [[frequentCardCheck_ topTextLabel] setText:NSLocalizedString(DELETE_FREQUENT_CARD_TEXT_KEY, nil)];
        }
    }
    
    aliasTextField_.text = @"";
    
    //alinear items
    
    CGFloat heightComponent = userTextField_.frame.size.height;
    CGFloat marginComponentInScreen = 11;
    
    if(frequentCardCombo_.hidden){
        
        [self alignVertical:userLabel_ fromSuperView:frequentCardCombo_ distance:(heightComponent*-1)];
    }
    else{
        
        [self alignVertical:userLabel_ fromSuperView:frequentCardCombo_ distance:VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT];
        
    }
    
    [self alignVertical:userTextField_ fromSuperView:userLabel_ distance:VERTICAL_DISTANCE_TO_NEAR_ELEMENT];
    [self alignVertical:frequentCardCheck_ fromSuperView:userTextField_ distance:VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT];
    [self alignVertical:aliasTextField_ fromSuperView:userTextField_ distance:VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT];
    
    CGFloat margin = (heightComponent-frequentCardHelperButton_.frame.size.height)/2;
    [self alignVertical:frequentCardHelperButton_ fromSuperView:userTextField_ distance:VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT+margin];
    [self alignVertical:passwordLabel_ fromSuperView:aliasTextField_ distance:VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT];
    [self alignVertical:passwordTextField_ fromSuperView:passwordLabel_ distance:VERTICAL_DISTANCE_TO_NEAR_ELEMENT];
    [self alignVertical:loginButton_ fromSuperView:passwordTextField_ distance:VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT];
    [self alignVertical:suscribedButton_ fromSuperView:loginButton_ distance:VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT];
    [self alignVertical:forgotPasswordHelperButton_ fromSuperView:suscribedButton_ distance:VERTICAL_DISTANCE_TO_FAR_AWAY_ELEMENT];
    
    heightContentView_ = CGRectGetMaxY([forgotPasswordHelperButton_ frame]) + marginComponentInScreen;
}

-(NSMutableArray*)updateEditableViews:(NSMutableArray *) editableViews{
    if (editableViews == nil) {
        
        editableViews = [[NSMutableArray alloc] init];
        
    } else {
        
        [editableViews removeAllObjects];
        
    }
    if(aliasTextField_.hidden){
        [editableViews addObjectsFromArray:[NSArray arrayWithObjects:frequentCardCombo_,
                                             userTextField_,
                                             passwordTextField_,
                                             nil]];
    }
    else{
        [editableViews addObjectsFromArray:[NSArray arrayWithObjects:frequentCardCombo_,
                                             userTextField_,
                                             aliasTextField_,
                                             passwordTextField_,
                                             nil]];
    }
    return editableViews;
}

/*
 * Creates and returns an autoreleased LoginInfoCell constructed from a NIB file
 */
+ (LoginInfoCell *)loginInfoCell {
    
    LoginInfoCell *result = [[LoginInfoCellProvider getInstance] loginInfoCell];
    
    result.userTextField.maxLength = LOGIN_USER_MAX_LENGTH;
    result.passwordTextField.maxLength = LOGIN_PASSWORD_MAX_LENGTH;
    result.aliasTextField.maxLength = TARGET_ALIAS_MAX_LENGTH;
    
    return result;
    
}

#pragma mark -
#pragma mark Cell configuration


/*
 * Adds the target and action for the login button
 */
- (void)addLoginTarget:(id)aTarget action:(SEL)anAction {
    
    [loginButton_ addTarget:aTarget action:anAction forControlEvents:UIControlEventTouchUpInside];
    
}

/*
 * Adds the target and action for the forgot password button
 */
- (void)addForgotPasswordTarget:(id)aTarget action:(SEL)anAction {
    
    [forgotPasswordHelperButton_ addTarget:aTarget action:anAction forControlEvents:UIControlEventTouchUpInside];
    
}

/*
 * Adds the target and action for the suscribe button.
 */
- (void)addSuscribeTarget:(id)aTarget action:(SEL)anAction {
    
    [suscribedButton_ addTarget:aTarget action:anAction forControlEvents:UIControlEventTouchUpInside];
    
}

/*
 * Adds the target and action for the frequent card helper button.
 */
- (void)addFrequentCardCheckTarget:(id)aTarget action:(SEL)anAction {
    
    [frequentCardHelperButton_ addTarget:aTarget action:anAction forControlEvents:UIControlEventTouchUpInside];
    
}

/*
 * Adds the target and action for the frequent card combo button.
 */
- (void)addFrequentCardComboTarget:(id)aTarget action:(SEL)anAction {
    
    [frequentCardCombo_ addTarget:aTarget action:anAction forControlEvents:UIControlEventTouchUpInside];
    
}


#pragma mark -
#pragma mark UITextFieldDelegate methods

/**
 * Asks the delegate if the specified text should be changed.
 *
 * @param textField: The text field containing the text.
 * @param range: The range of characters to be replaced
 * @param string: The replacement string.
 * @return YES if the specified text range should be replaced; otherwise, NO to keep the old text.
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
	NSInteger maxChars = 0;
	if( textField == userTextField_ ) {
        maxChars = LOGIN_USER_MAX_LENGTH;
    } else if( textField == passwordTextField_ ) {
        maxChars = LOGIN_PASSWORD_MAX_LENGTH;
    } else if( textField == aliasTextField_ ) {
        maxChars = TARGET_ALIAS_MAX_LENGTH;
    }
	
	BOOL result = YES;
	
	if (textField == userTextField_) {
		NSCharacterSet *numbersSet = [NSCharacterSet decimalDigitCharacterSet];
		result = [Tools isValidText:string forCharacterSet:numbersSet];
	} else if (textField == passwordTextField_) {
		result = [Tools isValidText:string forCharacterString:PASSWORD_VALID_CHARACTERS];
	} else if (textField == aliasTextField_) {
		result = [Tools isValidText:string forCharacterString:REFERENCE_VALID_CHARACTERS];
	}
    
	if (result) {
        
		NSUInteger finalTotalChars = [textField.text length] + [string length] - range.length;
		
		result = (!maxChars) || (finalTotalChars <= maxChars);
		
        if ((textField == userTextField_) && (finalTotalChars == maxChars)) {
			
            if(aliasTextField_.hidden){
            
                [passwordTextField_ performSelector:@selector(becomeFirstResponder)
									 withObject:nil
									 afterDelay:0.1f];
            }
            else{
                
                [aliasTextField_ performSelector:@selector(becomeFirstResponder)
                                         withObject:nil
                                         afterDelay:0.1f];
            }
			
		}
        
		if ((textField == aliasTextField_) && (finalTotalChars == maxChars) && !aliasTextField_.hidden) {
			
			[passwordTextField_ performSelector:@selector(becomeFirstResponder)
									 withObject:nil
									 afterDelay:0.1f];
			
		}
        
	}
	
    return result;
    
}

/**
 * Asks the delegate if the text field should process the pressing of the return button. Switches from the user to the password text field or
 * dismisses the keyboard, depending on the text field currently active
 *
 * @param textField The text field whose return button was pressed
 * @return Always NO, as the delegate manages the event
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == userTextField_) {
        
        if(aliasTextField_.hidden){
        
            [passwordTextField_ becomeFirstResponder];
        }
        else{
            
            [aliasTextField_ becomeFirstResponder];
            
        }
        
    } else if (textField == aliasTextField_) {
        
        [passwordTextField_ becomeFirstResponder];
        
    } else {
        
        [passwordTextField_ resignFirstResponder];
        [loginButton_ sendActionsForControlEvents:UIControlEventTouchUpInside];
        
    }
    
    return NO;
    
}

#pragma mark -
#pragma mark Utility methods

/*
 * Dismisses the keyboard
 */
- (void)dismissKeyboard {

    if ([userTextField_ isFirstResponder]) {
        
        [userTextField_ resignFirstResponder];
        
    } else if ([passwordTextField_ isFirstResponder]) {
        
        [passwordTextField_ resignFirstResponder];
        
    }
    
}

#pragma mark -
#pragma mark Cell show or hide

/*
 * This method run when animation cell is ended.
 */
- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    
    [hideCellView_ setHidden:YES];
    
}

/*
 * Set the cell visible with animation.
 */
- (void)setCellVisible:(BOOL)visible {
    
    if ([UIView respondsToSelector:@selector(animateWithDuration:animations:completion:)]) {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            if (visible) {
                
                CGRect frame = [hideCellView_ frame];
                frame.origin.y = CGRectGetHeight([self frame]);
                [hideCellView_ setFrame:frame];
                
            } else {
                
                [hideCellView_ setHidden:NO];
                
                CGRect frame = [hideCellView_ frame];
                frame.origin.y = CGRectGetHeight([self frame]) / 2;
                [hideCellView_ setFrame:frame];
                
            }
            
        } completion:^(BOOL finished) {
            
            if (visible) {
                
                [self animationDidStop:nil finished:nil context:nil];
                
            }
            
        }];
        
    } else {
        
        [hideCellView_ setHidden:NO];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
        
        if (visible) {
            
            CGRect frame = [hideCellView_ frame];
            frame.origin.y = CGRectGetHeight([self frame]);
            [hideCellView_ setFrame:frame];
            
        } else {
            
            CGRect frame = [hideCellView_ frame];
            frame.origin.y = CGRectGetHeight([self frame]) / 2;
            [hideCellView_ setFrame:frame];
            
        }
        
        [UIView commitAnimations];
        
    }
    
}

#pragma mark -
#pragma mark Information access

/*
 * Returns the user name introduced inside the user text field
 */
- (NSString *)userName {
    
    return userTextField_.text;
    
}

/*
 * Returns the password introduced inside the paswword text field
 */
- (NSString *)password {
    
    return passwordTextField_.text;
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
- (CGFloat)cellHeight {
    
    return heightContentView_;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return CELL_IDENTIFIER;
    
}


/**
 * util
 */
- (void) alignVertical:(UIView *) view fromSuperView:(UIView *)superView distance:(int32_t) distance{
    CGRect frame = [view frame];
    frame.origin.y = CGRectGetMaxY([superView frame]) + distance;
    [view setFrame:frame];
}


@end
