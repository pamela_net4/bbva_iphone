/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "DirectionCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "StringKeys.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"

/**
 * Define the DirectionCell NIB file name
 */
NSString * const kDirectionCellNibFileName = @"DirectionCell";

/**
 * Defines the cell identifier
 */
NSString * const kDirectionCellIdentifier = @"DirectionCell";

/**
 * Defines the cell height
 */
CGFloat const kDirectionCellHeight = 50.0f;

/**
 * Defines the labels vertical distance when both are displayed
 */
CGFloat const kDirectionCellLabelsVerticalDistance = 6.0f;

/**
 * Defines the main title label font size
 */
CGFloat const kDirectionCellFirstAddressLabelFontSize = 15.0f;

/**
 * Defines the main title label font size
 */
CGFloat const kDirectionCellSecondAddressLabelFontSize = 12.0f;


#pragma mark -

@implementation DirectionCell

#pragma -
#pragma mark Properties

@synthesize directionLabel = directionLabel_;
@synthesize directionAddressLabel = directionAddressLabel_;
@dynamic directionAddress;

#pragma -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [directionLabel_ release];
    directionLabel_ = nil;
    
    [directionAddressLabel_ release];
    directionAddressLabel_ = nil;
    
    [super dealloc];
    
}

#pragma -
#pragma mark Instances initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:directionLabel_
                 withFontSize:kDirectionCellFirstAddressLabelFontSize
                        color:[UIColor darkGrayColor]];
    directionLabel_.text = NSLocalizedString(ADDRESS_TEXT_KEY, nil);
    
    [NXT_Peru_iPhoneStyler styleLabel:directionAddressLabel_
                 withFontSize:kDirectionCellSecondAddressLabelFontSize
                        color:[UIColor grayColor]];
    directionAddressLabel_.textAlignment = UITextAlignmentRight;
    
//    ImagesCache *imagesCache = [ImagesCache getInstance];
//    
//    self.backgroundImageView.image = [imagesCache imageNamed:ACTION_CELL_BACKGROUND_IMAGE_FILE_NAME];
//    self.selectedBackgroundImageView.image = [imagesCache imageNamed:ACTION_CELL_SELECTED_BACKGROUND_IMAGE_FILE_NAME];
    
}

/*
 * Creates and return an autoreleased DirectionCell constructed from a NIB file
 */
+ (DirectionCell *)directionCell {
    
    return (DirectionCell *)[NibLoader loadObjectFromNIBFile:kDirectionCellNibFileName];
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    
    return kDirectionCellHeight;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return kDirectionCellIdentifier;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the address text
 *
 * @return The address text
 */
- (NSString *)directionAddress {
    
    return directionAddressLabel_.text;
    
}

/*
 * Sets the new direction address text to display
 *
 * @param directionAddress The new direction address text to display
 */
- (void)setDirectionAddress:(NSString *)directionAddress {
    
    directionAddressLabel_.text = directionAddress;
    
}

@end
