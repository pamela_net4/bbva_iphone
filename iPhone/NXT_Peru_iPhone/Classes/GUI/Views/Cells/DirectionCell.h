/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTCell.h"

/**
 * Cell to show information of a direcction address cell
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface DirectionCell : NXTCell {
@private
    
    /**
     * Text direction label
     */
    UILabel *directionLabel_;
    
    /**
     * Text direction address label
     */
    UILabel *directionAddressLabel_;
    
}

/**
 * Provides read-write access to the direction label text and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *directionLabel;

/**
 * Provides read-write access to the direction address label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *directionAddressLabel;

/**
 * Provides read-write access to the direction address
 */
@property (nonatomic, readwrite, copy) NSString *directionAddress;


/**
 * Creates and return an autoreleased DirectionCell constructed from a NIB file
 *
 * @return AddressCell The autoreleased DirectionCell constructed from a NIB file
 */
+ (DirectionCell *)directionCell;

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight;

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier;

@end
