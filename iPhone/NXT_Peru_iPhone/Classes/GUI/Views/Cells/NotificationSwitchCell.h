//
//  NotificationSwitchCell.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 12/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "NXTTableCell.h"

@class NXTTextField;
@class MOKStringListSelectionButton;

@protocol NotificationSwitchDelegate

- (void) switchButtonHasBeenTapped:(BOOL)on;

@end



@interface NotificationSwitchCell : NXTTableCell{

@private
    
    UIImageView *backgroundImageView_;
    
    UILabel *titleLabel_;
    
    UISwitch *notificationSwitch_;
    
    MOKStringListSelectionButton *dayOfMonthComboButton_;
    
    UIImageView *separator_;
    
    NSMutableArray *daysOfMonthArray_;
    
    id<NotificationSwitchDelegate> delegate_;

}

@property (nonatomic, readwrite, retain) IBOutlet UIImageView *backgroundImageView;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

@property (nonatomic, readwrite, retain) IBOutlet UISwitch *notificationSwitch;

@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *dayOfMonthComboButton;

@property (nonatomic, readwrite, retain) NSArray *daysOfMonthArray;

@property (nonatomic, readwrite, assign) NSInteger dayOfMonthSelectedIndex;

@property (retain, nonatomic) IBOutlet UIImageView *separator;

@property (nonatomic, readwrite, assign) id<NotificationSwitchDelegate> delegate;

- (IBAction)switchButtonHasBeenTapped:(id)sender;

+ (NotificationSwitchCell *) notificationCell;

+ (NSString *) cellIdentifier;

- (void) setDaysOfMonthArray:(NSArray *)daysOfMonthArray;

@end
