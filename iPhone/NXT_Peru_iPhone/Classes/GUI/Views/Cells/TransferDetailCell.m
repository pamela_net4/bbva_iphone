/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TransferDetailCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
/**
 * Define the LoginInfoCell NIB file name
 */
#define NIB_FILE_NAME                                               @"TransferDetailCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"TransferDetailCell"

/**
 * Defines the labels horizontal offset to the cell borders
 */
#define CELL_HORIZONTAL_OFFSET_TO_BORDER                            10.0f

/**
 * Defines the labels vertical offset to the cell top border
 */
#define CELL_VERTICAL_OFFSET_TO_TOP_BORDER                          1.0f

/**
 * Defines the labels vertical offset to the cell bottom border
 */
#define CELL_VERTICAL_OFFSET_TO_BOTTOM_BORDER                       9.0f

/**
 * Define the labels vertical distance (to the next label)
 */
#define LABEL_VERTICAL_DISTANCE                                     1.0f

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 40.0f


/**
 * Defines the big font size
 */
#define BIG_FONT_SIZE                                               13.0f

/**
 * Defines the small font size
 */
#define SMALL_FONT_SIZE                                             11.0f


#define LABEL_HEIGHT                                                15.0f



#pragma mark -

/**
 * TransferDetailCell private extension
 */
@interface TransferDetailCell()

/**
 * Returns the font used for the title label
 *
 * @return The font used for the title label
 * @private
 */
+ (UIFont *)titleLabelFont;

/**
 * Returns the font used for the atribute labels
 *
 * @return The font used for the attribute labels
 * @private
 */
+ (UIFont *)attributeLabelsFont;

@end


#pragma mark -

@implementation TransferDetailCell

#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;
@synthesize separatorImageView = separatorImageView_;
@dynamic displaySeparatorImage;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [separatorImageView_ release];
    separatorImageView_ = nil;
    
    [attributesLablesArray_ release];
    attributesLablesArray_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles cell elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [NXT_Peru_iPhoneStyler styleLoginInformationCell:self];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:BIG_FONT_SIZE color:[UIColor grayColor]];
    titleLabel_.font = [TransferDetailCell titleLabelFont];
    
    separatorImageView_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    attributesLablesArray_ = [[NSMutableArray alloc] init];
    self.backgroundView.backgroundColor = [UIColor whiteColor];
    
}

/*
 * Creates and returns an autoreleased TransferDetailCell constructed from a NIB file
 */
+ (TransferDetailCell *)transferDetailCell {
    
    return (TransferDetailCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
}

#pragma mark -
#pragma mark Fonts management

/*
 * Returns the font used for the title label
 */
+ (UIFont *)titleLabelFont {
    
    return [NXT_Peru_iPhoneStyler boldFontWithSize:BIG_FONT_SIZE];
    
}

/**
 * Returns the font used for the atribute labels
 */
+ (UIFont *)attributeLabelsFont {
    
    return [NXT_Peru_iPhoneStyler boldFontWithSize:SMALL_FONT_SIZE];
    
}


#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height for the provided title and attributes instance
 */
+ (CGFloat)cellHeightForTitleAndAttributes:(TitleAndAttributes *)titleAndAttributes {
    
    CGFloat result = CELL_VERTICAL_OFFSET_TO_TOP_BORDER + CELL_VERTICAL_OFFSET_TO_BOTTOM_BORDER;
    
    NSString *title = titleAndAttributes.titleString;
    CGSize titleSize = [title sizeWithFont:[TransferDetailCell titleLabelFont]];
    CGFloat titleHeight = titleSize.height;
    
    result += titleHeight;
    
    NSArray *attributesArrays = titleAndAttributes.attributesArray;
    
    if ([attributesArrays count] > 0) {
        
        if (titleHeight > 0.0f) {
            
            result -= LABEL_VERTICAL_DISTANCE;
            
        }
        
        UIFont *attributeLabelFont = [TransferDetailCell attributeLabelsFont];
        CGSize attributeTextSize;
        
        for (NSString *attribute in attributesArrays) {
            
            attributeTextSize = [attribute sizeWithFont:attributeLabelFont];
            result += (LABEL_VERTICAL_DISTANCE + attributeTextSize.height);
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return CELL_IDENTIFIER;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Displays the title and attributes instance to populate the cell
 */
- (void)displayTitleAndAttributes:(TitleAndAttributes *)titleAndAttributes {
    
    CGFloat top = CELL_VERTICAL_OFFSET_TO_TOP_BORDER;
    NSString *title = titleAndAttributes.titleString;
    titleLabel_.text = title;
    CGSize titleSize = [title sizeWithFont:[TransferDetailCell titleLabelFont]];
    CGFloat titleHeight = titleSize.height;
    CGRect frame = titleLabel_.frame;
    frame.origin.y = top;
    frame.size.height = titleHeight;
    titleLabel_.frame = frame;
    
    if (titleHeight > 0) {
        
        top = CGRectGetMaxY(frame) + LABEL_VERTICAL_DISTANCE;
        
    }
    
    for (UILabel *attributeLabel in attributesLablesArray_) {
        
        [attributeLabel removeFromSuperview];
        
    }
    
    [attributesLablesArray_ removeAllObjects];
    
    NSArray *attributesArrays = titleAndAttributes.attributesArray;
    
    CGFloat labelLeft = CGRectGetMinX(frame);
    
    if ([attributesArrays count] > 0) {
        
        CGFloat labelWidth = CGRectGetWidth(frame);
        UIViewAutoresizing labelAutoresizing = titleLabel_.autoresizingMask;
        UIFont *attributeLabelFont = [TransferDetailCell attributeLabelsFont];
        CGSize attributeTextSize;
        CGFloat attributeLabelHeight = 0.0f;
        UILabel *attributeLabel = nil;
        UIColor *clearColor = [UIColor clearColor];
        
        for (NSString *attribute in attributesArrays) {
            
            if ([title isEqualToString:NSLocalizedString(TRANSFER_TO_MY_ACCOUNTS_AMOUNT_TEXT_KEY, nil)] ||
                [title isEqualToString:NSLocalizedString(TRANSFER_GIFT_CARD_AMOUNT_TO_PAY_KEY, nil)]) {
                
                attributeLabelHeight = 20.0f;
                attributeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(labelLeft, top, labelWidth, attributeLabelHeight)] autorelease];
                [NXT_Peru_iPhoneStyler styleLabel:attributeLabel withFontSize:attributeLabelHeight color:[UIColor BBVAMagentaColor]];
                attributeLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:attributeLabelHeight];

            } else if ( [title isEqualToString:NSLocalizedString(SEND_AMOUNT_TEXT_KEY, nil)]){
                
                attributeLabelHeight = 20.0f;
                attributeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(labelLeft, top, labelWidth, attributeLabelHeight)] autorelease];
                [NXT_Peru_iPhoneStyler styleLabel:attributeLabel withFontSize:attributeLabelHeight color:[UIColor BBVAMagentaColor]];
                attributeLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:attributeLabelHeight];
                
            }else if ([@"" isEqualToString:title]){
                
                attributeLabelHeight = 68.0f;
                top -= 37;
                
                attributeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(labelLeft, top, labelWidth, attributeLabelHeight)] autorelease];
                
                [NXT_Peru_iPhoneStyler styleLabel:attributeLabel withFontSize:9.0f color:[UIColor lightGrayColor]];

                [attributeLabel setNumberOfLines:4];
                
            } else {
                
                attributeTextSize = [attribute sizeWithFont:attributeLabelFont];
                attributeLabelHeight = attributeTextSize.height;
                attributeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(labelLeft, top, labelWidth, attributeLabelHeight)] autorelease];
                [NXT_Peru_iPhoneStyler styleLabel:attributeLabel withFontSize:SMALL_FONT_SIZE color:[UIColor lightGrayColor]];
                attributeLabel.font = attributeLabelFont;

            }
            
            if ([title isEqualToString:NSLocalizedString(AMOUNT_TEXT_KEY, nil)] ||  [title isEqualToString:NSLocalizedString(CARD_PAYMENT_PAYED_AMOUNT_TEXT_KEY, nil)]  ||  [title isEqualToString:NSLocalizedString(TRANSFERED_SEND_IMPORT_TEXT_KEY, nil)]  ||  [title isEqualToString:NSLocalizedString(CASH_MOBILE_AMOUNT_PAID_TITLE_TEXT_KEY, nil)]) {
                
                attributeLabel.textColor = [UIColor BBVAMagentaColor];
                
            }
            
            
            attributeLabel.autoresizingMask = labelAutoresizing;
            attributeLabel.backgroundColor = clearColor;

            
            
            attributeLabel.text = attribute;
            
            
            
            if (attributeLabel != nil) {
                
                [attributesLablesArray_ addObject:attributeLabel];
                [self addSubview:attributeLabel];
                top = CGRectGetMaxY(attributeLabel.frame) + LABEL_VERTICAL_DISTANCE;
                
            }
            
        }
        
    }
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the display separator image flag
 *
 * @return The display separator image flag
 */
- (BOOL)displaySeparatorImage {
    
    return !(separatorImageView_.hidden);
    
}

/*
 * Sets the display separator image flag
 *
 * @param displaySeparatorImage The new display separator image flag to set
 */
- (void)setDisplaySeparatorImage:(BOOL)displaySeparatorImage {
    
    separatorImageView_.hidden = !displaySeparatorImage;
    
}

@end
