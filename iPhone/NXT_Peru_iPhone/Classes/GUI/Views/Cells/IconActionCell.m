/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "IconActionCell.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the IconActionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"IconActionCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"IconActionCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 60.0f


#pragma mark -

/**
 * IconActionCellProvider private category
 */
@interface IconActionCellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (IconActionCellProvider *)getInstance;

/**
 * Creates and returns an autoreleased IconActionCell constructed from a NIB file
 *
 * @return The autoreleased IconActionCell constructed from a NIB file
 */
- (IconActionCell *)iconActionCell;

@end


#pragma mark -

@implementation IconActionCellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * IconActionCellProvier singleton only instance
 */
static IconActionCellProvider *iconActionCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([IconActionCellProvider class]) {
        
        if (iconActionCellProviderInstance_ == nil) {
            
            iconActionCellProviderInstance_ = [super allocWithZone:zone];
            return iconActionCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (IconActionCellProvider *)getInstance {
    
    if (iconActionCellProviderInstance_ == nil) {
        
        @synchronized([IconActionCellProvider class]) {
            
            if (iconActionCellProviderInstance_ == nil) {
                
                iconActionCellProviderInstance_ = [[IconActionCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return iconActionCellProviderInstance_;
    
}

#pragma mark -
#pragma mark IconActionCell creation

/*
 * Creates and returns aa autoreleased IconActionCell constructed from a NIB file
 */
- (IconActionCell *)iconActionCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    IconActionCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation IconActionCell

#pragma mark -
#pragma mark Properties

@synthesize actionImageView = actionImageView_;
@synthesize actionLabel = actionLabel_;
@synthesize actionButton = actionButton_;
@synthesize cellSeparatorImage = cellSeparatorImage_;
@synthesize displayImage = displayImage_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [actionImageView_ release];
    actionImageView_ = nil;
    
    [actionLabel_ release];
    actionLabel_ = nil;
    
    [actionButton_ release];
    actionButton_ = nil;
    
    [cellSeparatorImage_ release];
    cellSeparatorImage_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {

    [super awakeFromNib];
    
    displayImage_ = YES;
    
    [NXT_Peru_iPhoneStyler styleActionIconButton:actionButton_];
    [NXT_Peru_iPhoneStyler styleIconActionCellLabel:actionLabel_];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    UIImage *cellSeparatorImage = [imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    cellSeparatorImage_.image = cellSeparatorImage;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self setNeedsLayout];
    
}

/*
 * Creates and returns an autoreleased IconActionCell constructed from a NIB file
 */
+ (IconActionCell *)iconActionCell {
    
    return [[IconActionCellProvider getInstance] iconActionCell];
    
}

#pragma mark -
#pragma mark Cell configuration

/*
 * Sets the action label text
 */
- (void)setActionText:(NSString *)anActionText {
    
    actionLabel_.text = anActionText;
    
}

/*
 * Sets the action icon image
 */
- (void)setActionIcon:(UIImage *)anActionIcon {
    
    actionImageView_.image = anActionIcon;
    
}

/*
 * Sets the action button image
 */
- (void)setActionButtonImage:(UIImage *)anActionButtonImage {
    
    [actionButton_ setImage:anActionButtonImage forState:UIControlStateNormal];
    
}

/*
 * Displays the cell background in gray color
 */
- (void)displayGrayBackground:(BOOL)displayGray {
    
    if (displayGray) {
        
        if (self.backgroundView == nil) {

            UIView *backgroundView = [[[UIView alloc] init] autorelease];
            backgroundView.backgroundColor = [UIColor BBVAGreyToneFiveColor];
            self.backgroundView = backgroundView;
            
        }
        
    } else {
        
        self.backgroundView = nil;
        
    }
    
}

/*
 * Adds the target and action for the cell button
 */
- (void)addTarget:(id)aTarget action:(SEL)anAction {
    
    [actionButton_ addTarget:aTarget action:anAction forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    
    return CELL_HEIGHT;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return CELL_IDENTIFIER;
    
}


#pragma mark -
#pragma mark UIView selectors

/**
 * Lays out the subviews
 */
- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    CGRect imageFrame = actionImageView_.frame;
    CGRect textFrame = actionLabel_.frame;
    CGFloat textOriginLeft = 0.0f;
    
    if (!displayImage_) {
        
        textOriginLeft = CGRectGetMinX(imageFrame);
        
    } else {
        
        textOriginLeft = CGRectGetMaxX(imageFrame) + 6.0f;
        
    }

    textFrame.size.width = CGRectGetMaxX(textFrame) - textOriginLeft;
    textFrame.origin.x = textOriginLeft;
    actionLabel_.frame = textFrame;
    
    actionImageView_.hidden = !displayImage_;

}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the new display image flag
 *
 * @param displayImage The new display image flag to set
 */
- (void)setDisplayImage:(BOOL)displayImage {
    
    if (displayImage_ != displayImage) {
        
        displayImage_ = displayImage;
        
        [self setNeedsLayout];
        
    }
    
}

@end
