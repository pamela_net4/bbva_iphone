//
//  CheckCell.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 26/11/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "NXTTableCell.h"

#import "SingletonBase.h"
#import "NXTTableCell.h"

@class MOKStringListSelectionButton;
@class CheckView;

@protocol CheckViewDelegate

@optional

- (void)checkView:(CheckView *)checkView checkStateChanged:(BOOL)isSelected;

@end


@interface CheckView : UIView
{
@private
    
    /**
     * Top text label
     */
    UILabel *topTextLabel_;
    
    /**
     * Check image view
     */
    UIImageView *checkImageView_;
    
    /**
     * Check active flag
     */
    BOOL checkActive_;
    
}

/**
 * Provides read-write access to the topTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *topTextLabel;

/**
 * Provides read-write access to the checkImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *checkImageView;

/**
 * Provides read-write access to the checkActive
 */
@property (nonatomic, readwrite, assign) BOOL checkActive;

// define delegate property
@property (nonatomic, assign) IBOutlet id  delegate;


/**
 * Creates and returns an autoreleased CheckView constructed from a NIB file
 *
 * @return The autoreleased CheckCell constructed from a NIB file
 */
+ (CheckView *)checkView;




@end

