//
//  CheckFOCell.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/6/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "CheckFOCell.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"

#pragma mark -
#pragma mark static values definition
/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"CheckFOCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"CheckFOCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 63.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              14.0f

/**
 * Defines the size of the font fopr subtitle text
 */
#define TEXT_SUB_FONT_SIZE                                          10.0f

#pragma mark -
@implementation CheckFOCell

@synthesize titleLabel = titleLabel_;
@synthesize subtitleLabel = subtitleLabel_;
@synthesize dayLabel = dayLabel_;
@synthesize checkImageView = checkImageView_;
@synthesize phoneImageView = phoneImageView_;
@synthesize mailImageView = mailImageView_;
@synthesize checkActive = checkActive_;
@synthesize hasMailActive = hasMailActive_;
@synthesize hasPhoneActive = hasPhoneActive_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [subtitleLabel_ release];
    subtitleLabel_ = nil;
    
    [dayLabel_ release];
    dayLabel_ = nil;
    
    [checkImageView_ release];
    checkImageView_ = nil;
    
    [phoneImageView_ release];
    phoneImageView_ = nil;
    
    [mailImageView_ release];
    mailImageView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;

    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlackColor]];
    [NXT_Peru_iPhoneStyler styleLabel:subtitleLabel_ withFontSize:TEXT_SUB_FONT_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:dayLabel_ withFontSize:TEXT_SUB_FONT_SIZE color:[UIColor BBVABlackColor]];

}

/*
 * Creates and returns an autoreleased CheckComboCell constructed from a NIB file
 */
+ (CheckFOCell *)checkFOCell {
    
    CheckFOCell *result = (CheckFOCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    [self awakeFromNib];
    
    return result;
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Sets the check active
 */
- (void)setCheckActive:(BOOL)checkActive {
    
    checkActive_ = checkActive;
    
    if (checkActive) {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_SELECTED_RADIO]];
        
    } else {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_UNSELECTED_RADIO]];
        
    }
    
}

- (void)setHasMailActive:(BOOL)hasMailActive{
    
    hasMailActive_ = hasMailActive;
    
    if (hasMailActive) {
        //set the image for the mail
        ImagesCache *imagesCache = [ImagesCache getInstance];
        [mailImageView_ setImage:[imagesCache imageNamed:ICON_ACTIVE_MAIL_FILE_NAME]];
        [mailImageView_ setHidden:FALSE];
    }else{
        //remove the image for the mail
        [mailImageView_ setHidden:TRUE];
    }
    
}

- (void)setHasPhoneActive:(BOOL)hasPhoneActive{
    
    hasPhoneActive_ = hasPhoneActive;
    
    if (hasPhoneActive) {
        //set the image for the phone
        ImagesCache *imagesCache = [ImagesCache getInstance];
        [phoneImageView_ setImage:[imagesCache imageNamed:ICON_ACTIVE_PHONE_FILE_NAME]];
        [phoneImageView_ setHidden:FALSE];
    }else{
        //remove the image for the phone
        [phoneImageView_ setHidden:TRUE];
    }
    
}


#pragma mark -
#pragma mark Cell associated information


/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;
}

/*
 * Returns the cell identifier
 */
+ (CGFloat)cellHeight {
    return CELL_HEIGHT;
}

@end