/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "InformationCell.h"

#import "StringKeys.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the InformationCell NIB file name
 */
#define NIB_FILE_NAME                                               @"InformationCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"InformationCell"

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              14.0f

/**
 * Defines the main amount size of the font
 */
#define MAIN_AMOUNT_TEXT_FONT_SIZE                                  18.0f

#pragma mark -

@implementation InformationCell

#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;
@synthesize detailLabel = detailLabel_;
@synthesize info = info_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [detailLabel_ release];
    detailLabel_ = nil;
        
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
     
    self.showDisclosureArrow = NO;
    self.showSeparator = NO;
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withBoldFontSize:TEXT_FONT_SIZE color:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:detailLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor grayColor]];
    
    
    [detailLabel_ setAdjustsFontSizeToFitWidth:YES];
    
}

/*
 * Creates and returns an autoreleased InformationCell constructed from a NIB file
 */
+ (InformationCell *)informationCell {
    
    InformationCell *result = (InformationCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    
    return 46.0f;
    
}

- (CGFloat) dynamicCellHeight {

    return (titleLabel_.frame.size.height + detailLabel_.frame.size.height);
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

/**
 * Sets the info
 */
- (void)setInfo:(TitleAndAttributes *)info {

    [titleLabel_ setText:[info titleString]];
    
    if (([[info titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_AMOUNT_TO_PAY_TEXT_KEY, nil)]) ||
        ([[info titleString] isEqualToString:NSLocalizedString(PAYMENT_RECHARGE_AMOUNT_TO_RECHARGE_TEXT_KEY, nil)])) {
        
        [NXT_Peru_iPhoneStyler styleLabel:detailLabel_ withFontSize:MAIN_AMOUNT_TEXT_FONT_SIZE color:[UIColor BBVAMagentaColor]];

    } else if ([[info titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_CHARGED_AMOUNT_TEXT_KEY, nil)] ||
               [[info titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_PAID_AMOUNT_TEXT_KEY, nil)] ||
               [[info titleString] isEqualToString:NSLocalizedString(PAYMENT_RECHARGE_AMOUNT_OF_RECHARGE_TEXT_KEY, nil)] ||
               [[info titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_PAYED_AMOUNT_TEXT_KEY, nil)] ||
               [[info titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TOTAL_AMOUNT_TEXT_KEY, nil)] ||
               [[info titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_FINAL_AMOUNT_TEXT_KEY, nil)] ||
               [[info titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_AMOUNT_TEXT_KEY, nil)]
               ||
               [[info titleString] isEqualToString:NSLocalizedString(INSTITUTIONS_COMPANIES_CHARGED_AMOUNT_KEY, nil)]
               
               
               ) {
        
        [NXT_Peru_iPhoneStyler styleLabel:detailLabel_ withBoldFontSize:TEXT_FONT_SIZE color:[UIColor BBVAMagentaColor]];
        
    } else {
        
        [NXT_Peru_iPhoneStyler styleLabel:detailLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor grayColor]];
        
    }
    
    if ([[info attributesArray] count] > 1) {
        
        NSMutableString *detailString = [[NSMutableString alloc] init];
        
        for (NSString *stringValue in [info attributesArray]) {
            [detailString appendString: [NSString stringWithFormat: @"%@ \n", stringValue]];
        }
        
        CGFloat yPosition = titleLabel_.frame.size.height;
        CGRect frame = detailLabel_.frame;
        frame.origin.y = yPosition;
        
        
        
        frame.size.height =  (20.0f * [[info attributesArray] count]);
        
      

        
        detailLabel_.frame = frame;

        
        [detailLabel_ setNumberOfLines: [[info attributesArray] count]];
        [detailLabel_ setText:[detailString copy]];
        
    } else if ([[info attributesArray] count] == 1) {
        
        CGSize recognizeSize = [[[info attributesArray] objectAtIndex:0] sizeWithFont:[NXT_Peru_iPhoneStyler normalFontWithSize:TEXT_FONT_SIZE] constrainedToSize:CGSizeMake(9000, 99)];

        
        CGRect frame = detailLabel_.frame;
        
        CGFloat tempHeight = (recognizeSize.width>268)?40.0f:20.0f;
        
        CGFloat alternativeHeight =40;
        
        tempHeight = ([[[info attributesArray] objectAtIndex:0] rangeOfString:@"\n"].location !=NSNotFound)?alternativeHeight:tempHeight;
        
        frame.size.height =tempHeight;
         detailLabel_.frame = frame;
        [detailLabel_ setText:[[info attributesArray] objectAtIndex:0]];
        
    }

}


@end