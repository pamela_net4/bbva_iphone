/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ReceiptCell.h"

#import "ImagesCache.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NibLoader.h"
#import "Payment.h"
#import "PaymentsConstants.h"
#import "StringKeys.h"
#import "TagAndValue.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "ImagesFileNames.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"ReceiptCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"ReceiptCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 148.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                      12.0f


#pragma mark -

@implementation ReceiptCell

#pragma mark -
#pragma mark Properties

@synthesize titleLeftLabel = titleLeftLabel_;
@synthesize titleRightLabel = titleRightLabel_;
@synthesize firstLineLeftLabel = firstLineLeftLabel_;
@synthesize firstLineRightLabel = firstLineRightLabel_;
@synthesize secondLineLeftLabel = secondLineLeftLabel_;
@synthesize secondLineRightLabel = secondLineRightLabel_;
@synthesize thirdLineLeftLabel = thirdLineLeftLabel_;
@synthesize thirdLineRightLabel = thirdLineRightLabel_;
@synthesize waterAmount = waterAmount_;
@synthesize checkImageView = checkImageView_;
@synthesize payment = payment_;
@synthesize checkActive = checkActive_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [titleLeftLabel_ release];
    titleLeftLabel_ = nil;
    
    [titleRightLabel_ release];
    titleRightLabel_ = nil;
    
    [firstLineLeftLabel_ release];
    firstLineLeftLabel_ = nil;
    
    [firstLineLeftLabel_ release];
    firstLineLeftLabel_ = nil;
    
    [secondLineRightLabel_ release];
    secondLineRightLabel_ = nil;
    
    [secondLineRightLabel_ release];
    secondLineRightLabel_ = nil;
    
    [thirdLineLeftLabel_ release];
    thirdLineLeftLabel_ = nil;
    
    [thirdLineRightLabel_ release];
    thirdLineRightLabel_ = nil;
    
    [waterAmount_ release];
    waterAmount_ = nil;
    
    [checkImageView_ release];
    checkImageView_ = nil;
    
    [payment_ release];
    payment_ = nil;
        
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLeftLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor blackColor]];
    [NXT_Peru_iPhoneStyler styleLabel:titleRightLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:firstLineLeftLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:firstLineRightLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:secondLineLeftLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:secondLineRightLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:thirdLineLeftLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:thirdLineRightLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor grayColor]];
    
    [titleRightLabel_ setTextAlignment:UITextAlignmentRight];
    [firstLineRightLabel_ setTextAlignment:UITextAlignmentRight];
    [secondLineRightLabel_ setTextAlignment:UITextAlignmentRight];
    [thirdLineRightLabel_ setTextAlignment:UITextAlignmentRight];

    [self setCheckActive:NO];
    
    [self setBackgroundColor:[UIColor whiteColor]];
    
}

/*
 * Creates and returns an autoreleased ReceiptCell constructed from a NIB file
 */
+ (ReceiptCell *)receiptCell {
    
    ReceiptCell *result = (ReceiptCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;    
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Sets the check image depending on the status
 */
- (void)setCheckActive:(BOOL)checkActive {

    checkActive_ = checkActive;
    
    if (checkActive) {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_SELECTED_RADIO]];
        
    } else {
    
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_UNSELECTED_RADIO]];

    }
    
    
}

/**
 * Configures the cell for a type
 */
- (void)setPayment:(Payment *)payment 
       paymentType:(PaymentTypeEnum)paymentType {
    
    if (payment_ != payment) {
        
        [payment_ release];
        payment_ = [payment retain];
        
    }

    if (paymentType == PTEPaymentPSWaterServices) {
        
        [titleLeftLabel_ setText:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:[[payment_ receipt] tag]], [Tools notNilString:[[payment_ receipt] value]]]];
        [titleRightLabel_ setText:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:[Tools translateServerCurrencySymbolIntoClientCurrencySymbol:[self waterAmount]]], [payment_ amount]]];
        
        [firstLineLeftLabel_ setText:[NSString stringWithFormat:@"%@:", NSLocalizedString(DATE_OF_ISSUE_TEXT_KEY, nil)]];
        [secondLineLeftLabel_ setText:[NSString stringWithFormat:@"%@:", NSLocalizedString(STATE_TEXT_KEY, nil)]];
        [thirdLineLeftLabel_ setText:[NSString stringWithFormat:@"%@:", NSLocalizedString(TYPE_TEXT_KEY, nil)]];
        
        [firstLineRightLabel_ setText:[Tools notNilString:[[payment_ date] value]]];
        [secondLineRightLabel_ setText:[payment_ state]];
        [thirdLineRightLabel_ setText:[payment_ type]];
        
    } else if (paymentType == PTEPaymentPSElectricServices) {
        
        [titleLeftLabel_ setText:[NSString stringWithFormat:NSLocalizedString(RECEIPT_VALUE_TEXT, nil), [Tools notNilString:[payment_ ticket]]]];
        [titleRightLabel_ setText:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:[Tools translateServerCurrencySymbolIntoClientCurrencySymbol:[payment state]]], [payment_ amount]]];
        
        [firstLineLeftLabel_ setText:[NSString stringWithFormat:@"%@:", NSLocalizedString(PUBLIC_SERVICE_STEP_TWO_SELECTED_DUE_DATE_TEXT_KEY, nil)]];
        [secondLineLeftLabel_ setText:@""];
        [thirdLineLeftLabel_ setText:@""];
        
        [firstLineRightLabel_ setText:[Tools notNilString:[payment_ dueDate]]];
        [secondLineRightLabel_ setText:@""];
        [thirdLineRightLabel_ setText:@""];
        
    } else if ((paymentType == PTEPaymentPSPhone) || (paymentType == PTEPaymentPSCellular)) {
		
        [titleLeftLabel_ setText:[NSString stringWithFormat:@"%@ %@",
                                  [Tools notNilString:[Tools capitalizeFirstWordOnly: [[payment_ receipt]tag]]], [Tools notNilString:[[payment_ receipt] value]]]];
        [titleRightLabel_ setText:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:[Tools translateServerCurrencySymbolIntoClientCurrencySymbol:[payment state]]], [payment_ amount]]];
        
        [firstLineLeftLabel_ setText:[NSString stringWithFormat:@"%@:", NSLocalizedString(DATE_OF_ISSUE_TEXT_KEY, nil)]];
        [secondLineLeftLabel_ setText:@""];
        [thirdLineLeftLabel_ setText:@""];
        
        [firstLineRightLabel_ setText:[Tools notNilString:[[payment_ date] value]]];
        [secondLineRightLabel_ setText:@""];
        [thirdLineRightLabel_ setText:@""];
		
	}


}


#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeightForReceiptType:(PaymentTypeEnum)paymemtTypeEnum {
    
    CGFloat result = 0.0f;
    
    switch (paymemtTypeEnum) {
        case PTEPaymentPSElectricServices:
            result = 80.0f;
            break;
        case PTEPaymentPSWaterServices:
            result = 100.0f;
            break;
        case PTEPaymentPSCellular:
        case PTEPaymentPSPhone:
            result = 60.0f;
            break;
        default:
            break;
    }
    
    return  result;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

@end