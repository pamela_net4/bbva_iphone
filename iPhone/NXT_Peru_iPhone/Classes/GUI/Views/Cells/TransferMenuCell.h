/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTTableCell.h"



/**
 * The transfer menu cell
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferMenuCell : NXTTableCell {
    
@private
    /**
     * Cell separator image
     */
    UIImageView *image_;
    
    /**
     * Title Label
     */
    UILabel *titleLabel_;
    
    
    
}


/**
 * Provides read-write access to the cell image and exports it for interface builder
 */
@property (retain, nonatomic) IBOutlet UIImageView *image;

/**
 * Provides read-write access to the cell title label and exports it for interface builder
 */
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;

/**
 * Provides read-write access to the bool for determinate if the left image should exists
 */
@property(nonatomic) BOOL withouthLeftImage;

/**
 * Creates and return an autoreleased TransferMenuCell constructed from a NIB file
 *
 * @return TransferMenuCell The autoreleased TransferMenuCell constructed from a NIB file
 */
+ (TransferMenuCell *)transferMenuCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

/*
 * Reorder the views according to the bool withouthLeftImage
 */
- (void)accomodateViewsForBool;


@end
