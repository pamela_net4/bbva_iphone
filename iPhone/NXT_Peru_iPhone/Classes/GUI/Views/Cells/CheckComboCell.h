/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class MOKStringListSelectionButton;

/**
 * Check combo cell. It's a subclass of NXTTableCell.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CheckComboCell : NXTTableCell {
@private  
    
    /**
     * Top text label
     */
    UILabel *topTextLabel_;
    
    /**
     * Check image view
     */
    UIImageView *checkImageView_;
    
    /**
     * Check image view
     */
    MOKStringListSelectionButton *combo_;
    
    /**
     * Check active flag
     */
    BOOL checkActive_;
    
}

/**
 * Provides read-write access to the topTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *topTextLabel;

/**
 * Provides read-write access to the checkImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *checkImageView;

/**
 * Provides read-write access to the combo and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *combo;

/**
 * Provides read-write access to the checkActive
 */
@property (nonatomic, readwrite, assign) BOOL checkActive;

/**
 * Creates and returns an autoreleased CheckComboCell constructed from a NIB file
 *
 * @return The autoreleased CheckComboCell constructed from a NIB file
 */
+ (CheckComboCell *)checkComboCell;

/**
 * Returns the cell height
 *
 * @param check Check flag
 * @return The cell height
 */
+ (CGFloat)cellHeightCheckOn:(BOOL)check;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

/**
 * Styles the cell for an account selecction (combo text size will be smaller).
 *
 * @param accountSelectionStyle YES to apply an account selection style, NO otherwise.
 */
- (void)applyAccountSelectionStyle:(BOOL)accountSelectionStyle;

@end