/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "CoordKeyCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXTTextField.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"CoordKeyCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"CoordKeyCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 50.0f
#pragma mark -

@implementation CoordKeyCell

#pragma mark -
#pragma mark Properties

@synthesize keyTextField = keyTextField_; 
@dynamic coordinate;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [keyTextField_ release];
    keyTextField_ = nil;
        
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    keyTextField_.leftViewMode = UITextFieldViewModeAlways;
    keyTextField_.leftView = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 10.0f, 10.0f)] autorelease];
    keyTextField_.background = [[ImagesCache getInstance] imageNamed:TEXT_FIELD_BACKGROUND_IMAGE_FILE_NAME];

    self.leftText = NSLocalizedString(COORDINATE_REQUEST_INFO_TEXT_KEY, nil);
    [self.leftTextLabel setNumberOfLines:2];
    
    [NXT_Peru_iPhoneStyler styleLabel:self.leftTextLabel 
                         withFontSize:11.0f 
                                color:[UIColor grayColor]];
        
}

/*
 * Creates and returns an autoreleased CoordKeyCell constructed from a NIB file
 */
+ (CoordKeyCell *)coordKeyCell {
    
    CoordKeyCell *result = (CoordKeyCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;    
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Sets the coordinate text
 */
- (void)setCoordinate:(NSString *)coordinate {
    
    self.rightText = coordinate;

    [NXT_Peru_iPhoneStyler styleLabel:self.rightTextLabel 
                         withFontSize:17.0f 
                                color:[UIColor blackColor]];
    

}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    return CELL_HEIGHT;
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

@end