/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "NXTTableCell.h"

/**
 * Transfer Summary Cell. It's a subclass of NXTTableCell.
 *
 * NOTE: The confirmation message and the itf message have to be setted before setting the information array
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferSummaryCell : NXTTableCell {
@private  
    
    /**
     * Information Array
     */
    NSMutableArray *information_;
    
    /**
     * Array of labels
     */
    NSMutableArray *labelsArray_;
    
    /**
     * ITF Message
     */
    NSString *itfMessage_;
    
}

/**
 * Provides read-write access to the information
 */
@property (nonatomic, readwrite, retain) NSArray *information;

/**
 * Provides read-write access to the itf message
 */
@property (nonatomic, readwrite, copy) NSString *itfMessage;

/**
 * Creates and returns an autoreleased TransferSummaryCell constructed from a NIB file
 *
 * @return The autoreleased TransferSummaryCell constructed from a NIB file
 */
+ (TransferSummaryCell *)transferSummaryCell;

/**
 * Returns the cell height
 *
 * @param array The information array
 * @param itf The itf flag
 * @return The cell height
 */
+ (CGFloat)cellHeightForArray:(NSArray *)array itf:(BOOL)itf;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end