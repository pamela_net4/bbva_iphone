/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "GlobalPositionCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the GlobalPositionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"GlobalPositionCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"GlobalPositionCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 77.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        15.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                      13.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLEST_SIZE                                     11.0f


#pragma mark -

/**
 * GlobalPositionCellProvider private category
 */
@interface GlobalPositionCellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (GlobalPositionCellProvider *)getInstance;

/**
 * Creates and returns an autoreleased GlobalPositionCell constructed from a NIB file
 *
 * @return The autoreleased GlobalPositionCell constructed from a NIB file
 */
- (GlobalPositionCell *)globalPositionCell;

@end


#pragma mark -

@implementation GlobalPositionCellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * GlobalPositionCellProvier singleton only instance
 */
static GlobalPositionCellProvider *globalPositionCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([GlobalPositionCellProvider class]) {
        
        if (globalPositionCellProviderInstance_ == nil) {
            
            globalPositionCellProviderInstance_ = [super allocWithZone:zone];
            return globalPositionCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (GlobalPositionCellProvider *)getInstance {
    
    if (globalPositionCellProviderInstance_ == nil) {
        
        @synchronized([GlobalPositionCellProvider class]) {
            
            if (globalPositionCellProviderInstance_ == nil) {
                
                globalPositionCellProviderInstance_ = [[GlobalPositionCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return globalPositionCellProviderInstance_;
    
}

#pragma mark -
#pragma mark GlobalPositionCell creation

/*
 * Creates and returns aa autoreleased GlobalPositionCell constructed from a NIB file
 */
- (GlobalPositionCell *)globalPositionCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    GlobalPositionCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation GlobalPositionCell

#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;
@synthesize subtitleLabel = subtitleLabel_;
@synthesize rightTopTextLabel = rightTopTextLabel_;
@synthesize rightTopTitleTextLabel = rightTopTitleTextLabel_;
@synthesize rightMiddleTextLabel = rightMiddleTextLabel_;
@synthesize rightMiddleTitleTextLabel = rightMiddleTitleTextLabel_;
@synthesize rightBottomTitleTextLabel = rightBottomTitleTextLabel_;
@synthesize rightBottomTextLabel = rightBottomTextLabel_;
@dynamic titleText;
@dynamic subtitleText;
@dynamic rightTopTitleText;
@dynamic rightTopText;
@dynamic rightMiddleTitleText;
@dynamic rightMiddleText;
@dynamic rightBottomTitleText;
@dynamic rightBottomText;
@synthesize smallRightTopText = smallRightTopText_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
        
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [subtitleLabel_ release];
    subtitleLabel_ = nil;
    
    [rightTopTextLabel_ release];
    rightTopTextLabel_ = nil;
    
    [rightTopTitleTextLabel_ release];
    rightTopTitleTextLabel_ = nil;
    
    [rightMiddleTitleTextLabel_ release];
    rightMiddleTitleTextLabel_ = nil;
    
    [rightMiddleTextLabel_ release];
    rightMiddleTextLabel_ = nil;
    
    [rightBottomTitleTextLabel_ release];
    rightBottomTitleTextLabel_ = nil;
    
    [rightBottomTextLabel_ release];
    rightBottomTextLabel_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.rightText = nil;
    self.showDisclosureArrow = YES;
    self.showSeparator = YES;
    
    self.backgroundColor = [UIColor BBVAGreyToneFiveColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:subtitleLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:rightTopTitleTextLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:rightTopTextLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:rightMiddleTitleTextLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:rightMiddleTextLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:rightBottomTitleTextLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:rightBottomTextLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];

    rightTopTitleTextLabel_.textAlignment = UITextAlignmentRight;
    rightTopTextLabel_.textAlignment = UITextAlignmentRight;
    rightMiddleTitleTextLabel_.textAlignment = UITextAlignmentRight;
    rightMiddleTextLabel_.textAlignment = UITextAlignmentRight;
    rightBottomTitleTextLabel_.textAlignment = UITextAlignmentRight;
    rightBottomTextLabel_.textAlignment = UITextAlignmentRight;

}

/*
 * Creates and returns an autoreleased GlobalPositionCell constructed from a NIB file
 */
+ (GlobalPositionCell *)globalPositionCell {
    
    return [[GlobalPositionCellProvider getInstance] globalPositionCell];
    
}

#pragma mark -
#pragma mark Getters and setters



#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeightForDetailsNumber:(NSInteger)integer {
    
    CGFloat result = 48.0f;
    
    if (integer == 1) {
        result = 48.0f;        
    } else if (integer == 2) {
        result = 79.0f;
    } else if (integer == 3) {
        result = 116.0f;
    }  
           
    return result;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the title text
 *
 * @return The title text
 */
- (NSString *)titleText {
    
    return titleLabel_.text;
    
}

/*
 * Sets right bottom title text label
 */
- (void)setTitleText:(NSString *)aString {
    
    titleLabel_.text = aString;
    
}

/*
 * Returns the subtitle text
 *
 * @return The subtitle text
 */
- (NSString *)subtitleText {
    
    return subtitleLabel_.text;
    
}

/*
 * Sets subtitle text label
 */
- (void)setSubtitleText:(NSString *)aString {
    
    subtitleLabel_.text = aString;
    
}

/*
 * Returns the right top title text
 *
 * @return The right top title text
 */
- (NSString *)rightTopTitleText {
    
    return rightTopTitleTextLabel_.text;
    
}

/*
 * Sets right top title text label
 */
- (void)setRightTopTitleText:(NSString *)aString {
    
    rightTopTitleTextLabel_.text = aString;
    
}

/*
 * Returns the right text
 *
 * @return The right text
 */
- (NSString *)rightTopText {
    
    return rightTopTextLabel_.text;
    
}

/*
 * Sets right top text label
 */
- (void)setRightTopText:(NSString *)aString {
    
    rightTopTextLabel_.text = aString;
    
}


/*
 * Returns the middle top title text
 *
 * @return The middle top title text
 */
- (NSString *)rightMiddleTitleText {
    
    return rightMiddleTitleTextLabel_.text;
    
}

/*
 * Sets right middle title text label
 */
- (void)setRightMiddleTitleText:(NSString *)aString {
    
    rightMiddleTitleTextLabel_.text = aString;
    
}

/*
 * Returns the right middle text
 *
 * @return The right middle text
 */
- (NSString *)rightMiddleText {
    
    return rightMiddleTextLabel_.text;
    
}

/*
 * Sets right middle text label
 */
- (void)setRightMiddleText:(NSString *)aString {
    
    rightMiddleTextLabel_.text = aString;
    
}


/*
 * Sets right bottom title text label
 */
- (void)setRightBottomTitleText:(NSString *)aString {
    
    rightBottomTitleTextLabel_.text = aString;
    
}

/*
 * Returns the right botton text
 *
 * @return The right bottom text
 */
- (NSString *)rightBottomText {
    
    return rightBottomTextLabel_.text;
    
}

/*
 * Sets right bottom text label
 */
- (void)setRightBottomText:(NSString *)aString {
    
    rightBottomTextLabel_.text = aString;
    
}

/**
 * Layouts the cell to set s
 */
- (void)setShowDisclosureArrow:(BOOL)value {
    
    self.disclosureArrow.hidden = !value;
    
    if (!value) {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
    
}

/*
 * Sets the small right top text flag and updates the label size.
 *
 * @param smallRightTopText The new small right top text flag to set.
 */
- (void)setSmallRightTopText:(BOOL)smallRightTopText {
    
    smallRightTopText_ = smallRightTopText;
    
    if (smallRightTopText) {
        
        [NXT_Peru_iPhoneStyler styleLabel:rightTopTextLabel_
                             withFontSize:TEXT_FONT_SMALLEST_SIZE
                                    color:[UIColor BBVABlueSpectrumToneTwoColor]];
        
    } else {
        
        [NXT_Peru_iPhoneStyler styleLabel:rightTopTextLabel_
                             withFontSize:TEXT_FONT_SMALLER_SIZE
                                    color:[UIColor BBVABlueSpectrumToneTwoColor]];

    }
    
}

@end