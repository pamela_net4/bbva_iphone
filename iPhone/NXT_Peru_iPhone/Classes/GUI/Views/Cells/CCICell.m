/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "CCICell.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the CCICell NIB file name
 */
#define NIB_FILE_NAME                                               @"CCICell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"CCICell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 72.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        13.0f


#pragma mark -

/**
 * CCICellProvider private category
 */
@interface CCICellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (CCICellProvider *)getInstance;

/**
 * Creates and returns an autoreleased CCICell constructed from a NIB file
 *
 * @return The autoreleased CCICell constructed from a NIB file
 */
- (CCICell *)cciCell;

@end


#pragma mark -

@implementation CCICellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * CCICellProvier singleton only instance
 */
static CCICellProvider *cciCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([CCICellProvider class]) {
        
        if (cciCellProviderInstance_ == nil) {
            
            cciCellProviderInstance_ = [super allocWithZone:zone];
            return cciCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (CCICellProvider *)getInstance {
    
    if (cciCellProviderInstance_ == nil) {
        
        @synchronized([CCICellProvider class]) {
            
            if (cciCellProviderInstance_ == nil) {
                
                cciCellProviderInstance_ = [[CCICellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return cciCellProviderInstance_;
    
}

#pragma mark -
#pragma mark CCICell creation

/*
 * Creates and returns aa autoreleased CCICell constructed from a NIB file
 */
- (CCICell *)cciCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    CCICell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation CCICell

#pragma mark -
#pragma mark Properties

@synthesize topLabel = topLabel_;
@synthesize middleLabel = middleLabel_;
@synthesize bottomLabel = bottomLabel_;
@dynamic topText;
@dynamic bottomText;
@dynamic middleText;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
        
    [topLabel_ release];
    topLabel_ = nil;
    
    [middleLabel_ release];
    middleLabel_ = nil;
    
    [bottomLabel_ release];
    bottomLabel_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.rightText = nil;
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    
    self.backgroundColor = [UIColor whiteColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:topLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:middleLabel_ withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor BBVAGreyToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:bottomLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAGreyToneOneColor]];

}

/*
 * Creates and returns an autoreleased CCICell constructed from a NIB file
 */
+ (CCICell *)cciCell {
    
    return [[CCICellProvider getInstance] cciCell];
    
}

#pragma mark -
#pragma mark Getters and setters



#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
     
    return CELL_HEIGHT;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the top text
 *
 * @return The top text
 */
- (NSString *)topText {
    
    return topLabel_.text;
    
}

/*
 * Sets top text label
 */
- (void)setTopText:(NSString *)aString {
    
    topLabel_.text = aString;
    
}

/*
 * Returns the middle text
 *
 * @return The middle text
 */
- (NSString *)middleText {
    
    return middleLabel_.text;
    
}

/*
 * Sets middle text label
 */
- (void)setMiddleText:(NSString *)aString {
    
    middleLabel_.text = aString;
    
}

/*
 * Returns the bottom text
 *
 * @return The bottom text
 */
- (NSString *)bottomText {
    
    return bottomLabel_.text;
    
}

/*
 * Sets bottom text label
 */
- (void)setBottomText:(NSString *)aString {
    
    bottomLabel_.text = aString;
    
}

@end