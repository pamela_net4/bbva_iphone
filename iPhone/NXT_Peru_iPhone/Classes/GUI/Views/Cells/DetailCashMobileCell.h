//
//  DetailCashMobileCell.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 9/24/13.
//
//

#import <UIKit/UIKit.h>

@interface DetailCashMobileCell : UITableViewCell{
    
    /**
     * Separator image view
     */
    UIImageView *separatorImageView_;
}

/**
 *  number of the transfer operation
 */
@property (retain, nonatomic) IBOutlet UILabel *lblOperationNumber;

/**
 *  number of the transfer operation
 */
@property (retain, nonatomic) IBOutlet UILabel *lblOperationNumberDetail;

/**
 *  Date of the cash mobile transfer
 */
@property (retain, nonatomic) IBOutlet UILabel *lblDate;

/**
 *  Current state of the transfer
 */
@property (retain, nonatomic) IBOutlet UILabel *lblState;

/**
 *  Amount sent with cash mobile
 */
@property (retain, nonatomic) IBOutlet UILabel *lblAmount;

/**
 *  Phone number of the beneficiary
 */
@property (retain, nonatomic) IBOutlet UILabel *lblPhoneNumber;

/**
 *  Phone number of the beneficiary
 */
@property (retain, nonatomic) IBOutlet UILabel *lblPhoneNumberDetail;

/**
 *  Bool to know if the cell is the header
 */
@property (nonatomic) BOOL isHeader;

/**
 * Provides read-write access to the backgroundImageView and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separatorImageView;


/**
 *  return an instance of the cell
 */
+ (DetailCashMobileCell *)detailCashMobileCell;

/**
 *  set the style of the views in the cell
 */
- (void)setStyleCell;

@end
