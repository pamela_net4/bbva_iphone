//
//  PhoneNumberCell.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 9/26/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "PhoneNumberCell.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTComboButton.h"
#import "NXTTextField.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Defines the cell nib name
 */
#define kPhoneNumberCellNibFileName @"PhoneNumberCell"

/**
 * Defines the cell identifier
 */
NSString * const kPhoneNumberCellIdentifier = @"PhoneCell";

/**
 * Defines the cell height
 */
#define FONT_SIZE                                           14.0f

@implementation PhoneNumberCell

@synthesize backgroundImageView = backgroundImageView_;
@synthesize lblTituloPhoneNumber = titleLabel_;
@synthesize lblIndication = subTitleLabel_;
@synthesize btnContacts = firstAddButton_;
@synthesize phoneNumberTextField = firstTextField_;

#pragma mark -
#pragma mark Instance initialization


+ (PhoneNumberCell *)phoneNumberCell
{
    PhoneNumberCell *result = (PhoneNumberCell *)[NibLoader loadObjectFromNIBFile:kPhoneNumberCellNibFileName];
    
    [result awakeFromNib];
    
    return result;
}

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.showDisclosureArrow = NO;
    self.showSeparator = NO;
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:FONT_SIZE color:[UIColor BBVABlackColor]];
    [titleLabel_ setText:NSLocalizedString(SEND_PHONE_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleTextField:firstTextField_ withFontSize:FONT_SIZE andColor:[UIColor BBVAGreyColor]];
    [firstTextField_ setPlaceholder:NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_FILL_KEY, nil)];
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return kPhoneNumberCellIdentifier;
    
}

#pragma mark - phone number cell delegate

/**
 * First add button tapped
 */
- (IBAction)firstAddButtonTapped {
    
    [delegate_ addFirstContactHasBeenTapped];
    
}

#pragma mark - memory management
- (void)dealloc {
    [titleLabel_ release];
    [subTitleLabel_ release];
    [firstAddButton_ release];
    [firstTextField_ release];
    [super dealloc];
}
@end
