/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "CheckComboCell.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKStringListSelectionButton.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"CheckComboCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"CheckComboCell"

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              12.0f


#pragma mark -

@implementation CheckComboCell

#pragma mark -
#pragma mark Properties

@synthesize topTextLabel = topTextLabel_;
@synthesize checkImageView = checkImageView_;
@synthesize combo = combo_;
@synthesize checkActive = checkActive_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [topTextLabel_ release];
    topTextLabel_ = nil;
    
    [checkImageView_ release];
    checkImageView_ = nil;
    
    [combo_ release];
    combo_ = nil;
        
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
     
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    [self setBackgroundColor:[UIColor whiteColor]];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [NXT_Peru_iPhoneStyler styleStringListButton:combo_];
    
    [NXT_Peru_iPhoneStyler styleLabel:topTextLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor grayColor]];
    [topTextLabel_ setText:NSLocalizedString(SHARE_SEAL_TEXT_KEY, nil)];
}

/*
 * Creates and returns an autoreleased CheckComboCell constructed from a NIB file
 */
+ (CheckComboCell *)checkComboCell {
    
    CheckComboCell *result = (CheckComboCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;    
}

#pragma mark -
#pragma mark Cell styling

/*
 * Styles the cell for an account selecction (combo text size will be smaller).
 */
- (void)applyAccountSelectionStyle:(BOOL)accountSelectionStyle {
    
    if (accountSelectionStyle) {
        
        [NXT_Peru_iPhoneStyler styleAccountSelectionButton:combo_];
        
    } else {
        
        [NXT_Peru_iPhoneStyler styleStringListButton:combo_];
        
    }
    
}

#pragma mark -
#pragma mark Getters and setters
+ (CGFloat)cellHeightCheckOn:(BOOL)check {
    
    if (check) {
        return  90.0f;
    } else {
        return  44.0f;
    }
    
}
/**
 * Sets the Seal
 */
- (void)setCheckActive:(BOOL)checkActive {
    
    checkActive_ = checkActive;
    [combo_ setHidden:!checkActive];
    
    if (checkActive) {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_SELECTED_RADIO]];
        
    } else {
    
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_UNSELECTED_RADIO]];
    
    }
    
}


#pragma mark -
#pragma mark Cell associated information


/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

@end