/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * Basic cell for the application. It contains a background image view and a selected background image view
 * to set the cell backgrounds. Subclasses must create both images view
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTCell : UITableViewCell {

@private
    
    /**
     * Cell background image view
     */
    UIImageView *backgroundImageView_;
    
    /**
     * Cell selected background image view
     */
    UIImageView *selectedBackgroundImageView_;

    /**
     * Separator image view
     */
    UIImageView *separatorImageView_;

    /**
     * Hides separator if YES
     */
    BOOL hideSeparator_;
    
}

/**
 * Provides read-write access to the cell background image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *backgroundImageView;

/**
 * Provides read-write access to the cell selected background image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *selectedBackgroundImageView;

/**
 * Provides read-write access to the hideSeparator flag
 */
@property (nonatomic, readwrite, assign) BOOL hideSeparator;

@end
