/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTCell.h"

/**
 * Cell to show information of an address
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AddressCell : NXTCell {
@private
    
    /**
     * First line to show address
     */
    UILabel *addressFirstLineLabel_;
    
    /**
     * Second line to show address
     */
    UILabel *addressSecondLineLabel_;
    
}

/**
 * Provides read-write access to the address firts line and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *addressFirstLineLabel;

/**
 * Provides read-write access to the address second line and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *addressSecondLineLabel;

/**
 * Provides read-write access to the address first line text
 */
@property (nonatomic, readwrite, copy) NSString *addressFirstLineText;

/**
 * Provides read-write access to the address second line text
 */
@property (nonatomic, readwrite, copy) NSString *addressSecondLineText;


/**
 * Creates and return an autoreleased AddresCell constructed from a NIB file
 *
 * @return AddressCell The autoreleased AddressCell constructed from a NIB file
 */
+ (AddressCell *)addressCell;

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight;

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier;

@end
