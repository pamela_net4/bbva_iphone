/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTTableCell.h"

@class NXTTextField;
@class NXTTextView;

@protocol EmailCellDelegate

/**
 * Switch has been tapped.
 *
 * @param on The flag
 */
- (void)emailSwitchButtonHasBeenTapped:(BOOL)on;

/**
 * Add First Contact Button Tapped
 */
- (void)emailAddFirstContactHasBeenTapped;

/**
 * Add Second Contact Button Tapped
 */
- (void)emailAddSecondContactHasBeenTapped;

/**
 * More button has been tapped
 */
- (void)emailMoreButtonHasBeenTapped;

@end

/**
 * Email cell
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface EmailCell: NXTTableCell {
    
@private
    
    /**
     * Background image
     */
    UIImageView *backgroundImageView_;
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Email Switch
     */
    UISwitch *emailSwitch_;
    
    /**
     * First text field
     */
    NXTTextField *firstTextField_;
    
    /**
     * First add button
     */
    UIButton *firstAddButton_;

    /**
     * Second text field
     */
    NXTTextField *secondTextField_;
    
    /**
     * Second add button
     */
    UIButton *secondAddButton_;
    
    /**
     * More button
     */
    UIButton *moreButton_;
    
    /**
     * Add contact image
     */
    UIImage *addContactImage_;
    
    /**
     * Delete contact image
     */
    UIImage *deleteContactImage_;
    
    /**
     * Email text view
     */
    NXTTextView *emailTextView_;
    
    /**
     * Email text view bg image
     */
    UIImageView *emailTextViewBackground_;
    
    /**
     * Email Cell Delegate
     */    
    id<EmailCellDelegate> delegate_;
    
}

/**
 * Provides readwrite access to the backgroundImageView and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *backgroundImageView;

/**
 * Provides readwrite access to the titleLabel and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides readwrite access to the smsSwitch and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UISwitch *emailSwitch;

/**
 * Provides readwrite access to the firstTextField and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTTextField *firstTextField;

/**
 * Provides readwrite access to the firstAddButton and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *firstAddButton;

/**
 * Provides readwrite access to the secondTextField and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTTextField *secondTextField;

/**
 * Provides readwrite access to the secondAddButton and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *secondAddButton;

/**
 * Provides readwrite access to the emailTextView and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTTextView *emailTextView;

/**
 * Provides readwrite access to the emailTextViewBackground and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *emailTextViewBackground;

/**
 * Provides readwrite access to the moreButton and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *moreButton;

/**
 * Provides readwrite access to the delegate
 */
@property (nonatomic, readwrite, assign) id<EmailCellDelegate> delegate;


/*
 * Creates and return an autoreleased emailCell constructed from a NIB file
 */
+ (EmailCell *)emailCell;

/**
 * Returns the cell height
 *
 * @param firstAddOn The first add elements on
 * @param secondAddON The second add elements on
 * @return A height for cell.
 */
+ (CGFloat)cellHeightForFirstAddOn:(BOOL)firstAddOn 
                       secondAddOn:(BOOL)secondAddON;

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier;

/**
 * Switch tapped
 */
- (IBAction)switchButtonTapped;

/**
 * First add button tapped
 */
- (IBAction)firstAddButtonTapped;

/**
 * Second add button tapped
 */
- (IBAction)secondAddButtonTapped;

/**
 * More button tapped
 */
- (IBAction)moreButtonTapped;

/**
 * Configures the cell depending on the different flags
 *
 * @param firstAddOn The first add elements on
 * @param secondAddON The second add elements on
 */
- (void)setConfigurationForFirstAddOn:(BOOL)firstAddOn 
                          secondAddOn:(BOOL)secondAddON;
@end
