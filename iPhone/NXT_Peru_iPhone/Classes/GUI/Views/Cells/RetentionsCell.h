/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class RetentionsCell;

/**
 * Provider to obtain a RetentionsCell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface RetentionsCellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary RetentionsCell to create it from a NIB file
     */
    RetentionsCell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary RetentionsCell
 */
@property (nonatomic, readwrite, assign) IBOutlet RetentionsCell *auxView;


@end


/**
 * Retentions cell. It's a subclass of NXTTableCell.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface RetentionsCell : NXTTableCell {
@private    
    
    /**
     * Background image view
     */
    UIImageView *backgroundImageView_;
    
    /**
     * Separator image view
     */
    UIImageView *separatorImageView_;
    
}

/**
 * Provides read-write access to the backgroundImageView and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *backgroundImageView;

/**
 * Provides read-write access to the backgroundImageView and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separatorImageView;

/**
 * Creates and returns an autoreleased RetentionsCell constructed from a NIB file
 *
 * @return The autoreleased RetentionsCell constructed from a NIB file
 */
+ (RetentionsCell *)retentionsCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end