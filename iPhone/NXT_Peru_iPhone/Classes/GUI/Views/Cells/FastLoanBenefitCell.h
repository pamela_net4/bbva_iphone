//
//  FastLoanBenefitCell.h
//  NXT_Peru_iPhone
//
//  Created by Flavio Franco Tunqui on 2/23/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NXTTableCell.h"

@class TextCell;

@interface FastLoanBenefitCell : NXTTableCell {
    UIImageView *iconImageView_;
    UILabel *titleLabel_;
    UILabel *arrowLabel_;
}

/**
 * Provides read-write access to the iconImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *iconImageView;


/**
 * Provides read-write access to the titleLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides read-write access to the arrowLabel
 */
@property (nonatomic, readwrite, assign) IBOutlet UILabel *arrowLabel;

/**
 * Creates and returns an autoreleased CheckCell constructed from a NIB file
 *
 * @return The autoreleased FastLoanBenefitCell constructed from a NIB file
 */
+ (FastLoanBenefitCell *)fastLoanBenefitCell;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

/**
 * Sets the action icon image
 *
 * @param anActionIcon The action icon image
 */
- (void)setIcon:(UIImage *)icon;

/**
 * Sets the action label text
 *
 * @param anActionText The action label text
 */
- (void)setTitle:(NSString *)title;

/**
 * Sets the action label text
 *
 * @param anActionText The action label text
 */
- (void)setArrow:(NSString *)symbol;

@end