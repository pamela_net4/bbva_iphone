/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "TransferMenuCell.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the AddressCell NIB file name
 */
NSString * const kTransferMenuCellNibFileName = @"TransferMenuCell";

/**
 * Defines the cell identifier
 */
NSString * const kTransferMenuCellIdentifier = @"TransferMenuCell";

/**
 * Defines the cell height
 */
CGFloat const kTransferMenuCellHeight = 57.0f;

/**
 * Defines the font size
 */
CGFloat const kTransferMenuCellFontSize = 16.0f;

#pragma mark -

@implementation TransferMenuCell

#pragma -
#pragma mark Properties

@synthesize image= image_;
@synthesize titleLabel = titleLabel_;
@synthesize withouthLeftImage;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [image_ release];
    image_ = nil;
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization


/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:kTransferMenuCellFontSize color:[UIColor BBVABlueColor]];        
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self setNeedsLayout];
    
    self.showDisclosureArrow = YES;
    self.cellSeparatorBottomImage.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    self.showSeparator = YES;
    
    
}

/*
 * Creates and return an autoreleased AddresCell constructed from a NIB file
 */
+ (TransferMenuCell *)transferMenuCell {
    
    TransferMenuCell *result = (TransferMenuCell *)[NibLoader loadObjectFromNIBFile:kTransferMenuCellNibFileName];
    
    [result awakeFromNib];
    
    return result;
    
}

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    
    return kTransferMenuCellHeight;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return kTransferMenuCellIdentifier;
    
}
/*
 * Reorder the views according to the bool withouthLeftImage
 */
- (void)accomodateViewsForBool
{
    if (withouthLeftImage) {
        [image_ setHidden:TRUE];
        [titleLabel_ setCenter:CGPointMake(titleLabel_.center.x - (image_.frame.size.width + image_.frame.origin.x), self.center.y)];
    }
  
}

#pragma mark -
#pragma mark Properties selectors

/**
 * Sets the option image
 */
- (void)setOptionImage:(UIImage *)anImage {
    image_.image = anImage;
}

/*
 * Sets the action label text
 */
- (void)setTransferMenuCellText:(NSString *)anActionText {
    
    titleLabel_.text = anActionText;
    
}

@end
