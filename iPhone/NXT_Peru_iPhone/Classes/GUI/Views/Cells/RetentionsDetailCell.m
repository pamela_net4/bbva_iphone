/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "RetentionsDetailCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTTableCell.h"
#import "Retention.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"RetentionsDetailCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"RetentionsDetailCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 97.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              12.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        12.0f


#pragma mark -

/**
 * RetentionsDetailCellProvider private category
 */
@interface RetentionsDetailCellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (RetentionsDetailCellProvider *)getInstance;

/**
 * Creates and returns an autoreleased RetentionsDetailCell constructed from a NIB file
 *
 * @return The autoreleased RetentionsDetailCell constructed from a NIB file
 */
- (RetentionsDetailCell *)retentionsDetailCell;

@end


#pragma mark -

@implementation RetentionsDetailCellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * RetentionsDetailCellProvier singleton only instance
 */
static RetentionsDetailCellProvider *retentionsDetailCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([RetentionsDetailCellProvider class]) {
        
        if (retentionsDetailCellProviderInstance_ == nil) {
            
            retentionsDetailCellProviderInstance_ = [super allocWithZone:zone];
            return retentionsDetailCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (RetentionsDetailCellProvider *)getInstance {
    
    if (retentionsDetailCellProviderInstance_ == nil) {
        
        @synchronized([RetentionsDetailCellProvider class]) {
            
            if (retentionsDetailCellProviderInstance_ == nil) {
                
                retentionsDetailCellProviderInstance_ = [[RetentionsDetailCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return retentionsDetailCellProviderInstance_;
    
}

#pragma mark -
#pragma mark RetentionsDetailCell creation

/*
 * Creates and returns an autoreleased RetentionsDetailCell constructed from a NIB file
 */
- (RetentionsDetailCell *)retentionsDetailCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    RetentionsDetailCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation RetentionsDetailCell

#pragma mark -
#pragma mark Properties

@synthesize title1Label = title1Label_;
@synthesize title2Label = title2Label_;
@synthesize title3Label = title3Label_;
@synthesize title4Label = title4Label_;
@synthesize title5Label = title5Label_;
@synthesize detail1Label = detail1Label_;
@synthesize detail2Label = detail2Label_;
@synthesize detail3Label = detail3Label_;
@synthesize detail4Label = detail4Label_;
@synthesize detail5Label = detail5Label_;
@synthesize retention = retention_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
        
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = YES;
    self.showSeparator = YES;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor=[UIColor whiteColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:title1Label_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:title2Label_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:title3Label_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:title4Label_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:title5Label_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:detail1Label_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:detail2Label_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:detail3Label_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:detail4Label_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:detail5Label_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];

    [title1Label_ setText:NSLocalizedString(AMOUNT_RETENTION_TEXT_KEY, nil)];
    [title2Label_ setText:NSLocalizedString(DUE_DATE_RETENTION_TEXT_KEY, nil)];
    [title3Label_ setText:NSLocalizedString(DISCHARGE_DATE_RETENTION_TEXT_KEY, nil)];
    [title4Label_ setText:NSLocalizedString(NUMBER_RETENTION_TEXT_KEY, nil)];
    [title5Label_ setText:NSLocalizedString(REMARKS_RETENTION_TEXT_KEY, nil)];
    
}

/*
 * Creates and returns an autoreleased RetentionsDetailCell constructed from a NIB file
 */
+ (RetentionsDetailCell *)retentionsDetailCell {
    
    return [[RetentionsDetailCellProvider getInstance] retentionsDetailCell];
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    return CELL_HEIGHT;
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

/*
 * Sets the retention information
 */
- (void)setRetention:(Retention *)retention {

    if (retention_ != retention) {
        [retention_ release];
        retention_ = [retention retain];
    }
    
    [detail1Label_ setText:[NSString stringWithFormat:@"%@ %@", [Tools mainCurrencySymbol], [retention_ currentAmountString]]];
    [detail2Label_ setText:[retention_ dischargeDateString]];
    [detail3Label_ setText:[retention_ dueDateString]];
    [detail4Label_ setText:[retention_ number]];
    [detail5Label_ setText:[retention_ remarks]];

}


@end