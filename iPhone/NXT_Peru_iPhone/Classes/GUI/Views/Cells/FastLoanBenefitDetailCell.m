//
//  FastLoanBenefitDetailCell.m
//  NXT_Peru_iPhone
//
//  Created by Flavio Franco Tunqui on 2/23/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "FastLoanBenefitDetailCell.h"
#import "UIColor+BBVA_Colors.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME @"FastLoanBenefitDetailCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER @"FastLoanBenefitDetailCell"

#define CELL_HEIGHT 45

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              14.0f

@implementation FastLoanBenefitDetailCell

@synthesize detailTextView = detailTextView_;

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [detailTextView_ release];
    detailTextView_ = nil;
    
    [super dealloc];
}

/**
 * Creates and returns an autoreleased CheckCell constructed from a NIB file
 *
 * @return The autoreleased CheckCell constructed from a NIB file
 */
+ (FastLoanBenefitDetailCell *)fastLoanBenefitDetailCell{
    
    FastLoanBenefitDetailCell *result = (FastLoanBenefitDetailCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;
}

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier{
    return CELL_IDENTIFIER;
}

- (void)awakeFromNib {
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [NXT_Peru_iPhoneStyler styleTextView:detailTextView_ withFontSize:TEXT_FONT_SIZE andColor:[UIColor BBVAGrayFourToneColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Sets the action label
 *
 * @param setTitle The action icon image
 */
- (void)setDetail:(NSString *)detail{
    
    detailTextView_.text = detail;
}

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    
    return CELL_HEIGHT;
}

@end