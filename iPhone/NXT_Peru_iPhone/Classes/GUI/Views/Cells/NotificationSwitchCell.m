//
//  NotificationSwitchCell.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 12/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "NotificationSwitchCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTComboButton.h"
#import "NXTTextField.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"
#import "NXTComboButton.h"
#import "MOKStringListSelectionButton.h"


NSString *const kNotificationSwitchCellFileName = @"NotificationSwitchCell";
NSString *const kNotificationSwitchIdentifier = @"NotificationSwitchCell";


#define FONT_SIZE                                                   14.0f
#define VERTICAL_MARGIN_SIZE                                        10.0f
#define SWITCH_HEIGHT                                               27.0f
#define TXTFIELD_HEIGHT                                             31.0f
#define COMBO_HEIGHT                                                37.0f
#define BUTTON_HEIGHT                                               37.0f


@implementation NotificationSwitchCell

@synthesize backgroundImageView = backgroundImageView_;
@synthesize titleLabel = titleLabel_;
@synthesize notificationSwitch = notificationSwitch_;
@synthesize dayOfMonthComboButton = dayOfMonthComboButton_;
@synthesize separator = separator_;
@synthesize daysOfMonthArray = daysOfMonthArray_;
@synthesize delegate = delegate_;


- (void)dealloc
{
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [notificationSwitch_ release];
    notificationSwitch_ = nil;
    
    [dayOfMonthComboButton_ release];
    dayOfMonthComboButton_ = nil;
    
    [daysOfMonthArray_ release];
    daysOfMonthArray_ = nil;
    
    [separator_ release];
    [super dealloc];
}

- (void) awakeFromNib {

    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.showDisclosureArrow = NO;
    self.showSeparator = NO;
    
    
    separator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:FONT_SIZE color:[UIColor BBVABlackColor]];
    [titleLabel_ setText:NSLocalizedString(SEND_ACTIVE_NOTIFICATION_TEXT_KEY, nil)];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1){
        
        [notificationSwitch_ setOnTintColor:[UIColor BBVABlueSpectrumColor]];
        [notificationSwitch_ setTintColor: [UIColor BBVABlueSpectrumColor]];
    }
    
    [notificationSwitch_ setOn:NO];

}

- (IBAction)switchButtonHasBeenTapped:(id)sender {
    
    [delegate_ switchButtonHasBeenTapped:[notificationSwitch_ isOn]];
    
}

+ (NotificationSwitchCell *) notificationCell {

    NotificationSwitchCell *result = (NotificationSwitchCell *) [NibLoader loadObjectFromNIBFile:kNotificationSwitchCellFileName];
    
    [result awakeFromNib];
    
    return result;
}

+ (NSString *) cellIdentifier {

    return kNotificationSwitchIdentifier;
    
}

- (void) setDaysOfMonthArray:(NSArray *)daysOfMonthArray {

    if (daysOfMonthArray_ == nil) {
        
        daysOfMonthArray_ = [[NSMutableArray alloc] init];
        
    }
    
    [daysOfMonthArray_ removeAllObjects];
    [daysOfMonthArray_ addObjectsFromArray:daysOfMonthArray];
    
    NSMutableArray *array = [NSMutableArray array];
    
    for (NSString *dayOfMonth in daysOfMonthArray_) {
        
        [array addObject:dayOfMonth.description];
        
    }
    [dayOfMonthComboButton_ setOptionStringList:array];
    [dayOfMonthComboButton_ setSelectedIndex:0];
    [array removeAllObjects];
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
}

@end
