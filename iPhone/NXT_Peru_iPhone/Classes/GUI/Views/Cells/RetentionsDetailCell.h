/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class RetentionsDetailCell;
@class Retention;
/**
 * Provider to obtain a RetentionsDetailCell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface RetentionsDetailCellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary RetentionsDetailCell to create it from a NIB file
     */
    RetentionsDetailCell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary RetentionsDetailCell
 */
@property (nonatomic, readwrite, assign) IBOutlet RetentionsDetailCell *auxView;


@end


/**
 * Retentions Detail cell. It's a subclass of NXTTableCell.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface RetentionsDetailCell : NXTTableCell {
@private    
    
    /**
     * Title 1
     */
    UILabel *title1Label_;
    
    /**
     * Title 2
     */
    UILabel *title2Label_;
    
    /**
     * Title 3
     */
    UILabel *title3Label_;
    
    /**
     * Title 4
     */
    UILabel *title4Label_;
    
    /**
     * Title 5
     */
    UILabel *title5Label_;
    
    /**
     * Detail 1
     */
    UILabel *detail1Label_;

    /**
     * Detail 2
     */
    UILabel *detail2Label_;
    
    /**
     * Detail 3
     */
    UILabel *detail3Label_;
    
    /**
     * Detail 4
     */
    UILabel *detail4Label_;
    
    /**
     * Detail 5
     */
    UILabel *detail5Label_;
    
    /**
     * Retention
     */
    Retention *retention_;
}

/**
 * Provides read-write access to the title1Label and exports it to the IB
 */
@property (nonatomic, readwrite, assign) IBOutlet UILabel *title1Label;

/**
 * Provides read-write access to the title2Label and exports it to the IB
 */
@property (nonatomic, readwrite, assign) IBOutlet UILabel *title2Label;

/**
 * Provides read-write access to the title3Label and exports it to the IB
 */
@property (nonatomic, readwrite, assign) IBOutlet UILabel *title3Label;

/**
 * Provides read-write access to the title4Label and exports it to the IB
 */
@property (nonatomic, readwrite, assign) IBOutlet UILabel *title4Label;

/**
 * Provides read-write access to the title5Label and exports it to the IB
 */
@property (nonatomic, readwrite, assign) IBOutlet UILabel *title5Label;

/**
 * Provides read-write access to the detail1Label and exports it to the IB
 */
@property (nonatomic, readwrite, assign) IBOutlet UILabel *detail1Label;

/**
 * Provides read-write access to the detail2Label and exports it to the IB
 */
@property (nonatomic, readwrite, assign) IBOutlet UILabel *detail2Label;

/**
 * Provides read-write access to the detail3Label and exports it to the IB
 */
@property (nonatomic, readwrite, assign) IBOutlet UILabel *detail3Label;

/**
 * Provides read-write access to the detail4Label and exports it to the IB
 */
@property (nonatomic, readwrite, assign) IBOutlet UILabel *detail4Label;

/**
 * Provides read-write access to the detail5Label and exports it to the IB
 */
@property (nonatomic, readwrite, assign) IBOutlet UILabel *detail5Label;

/**
 * Provides read-write access to the retention and exports it to the IB
 */
@property (nonatomic, readwrite, assign) Retention *retention;

/**
 * Creates and returns an autoreleased RetentionsCell constructed from a NIB file
 *
 * @return The autoreleased RetentionsCell constructed from a NIB file
 */
+ (RetentionsDetailCell *)retentionsDetailCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end