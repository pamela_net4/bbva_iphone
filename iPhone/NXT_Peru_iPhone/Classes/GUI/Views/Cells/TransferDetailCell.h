/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>


//Forward declarations
@class TitleAndAttributes;


/**
 * Cell containing two labels where the pair of details are writen
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferDetailCell : UITableViewCell <UITextFieldDelegate> {

@private
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Separator image view
     */
    UIImageView *separatorImageView_;
    
    /**
     * Array containing the attributes lables
     */
    NSMutableArray *attributesLablesArray_;
    
}


/**
 * Provides read-write access to the title label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides read-write access to the separator image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separatorImageView;

/**
 * Provides read-write access to the display separator image flag
 */
@property (nonatomic, readwrite, assign) BOOL displaySeparatorImage;


/**
 * Creates and returns an autoreleased TransferDetailCell constructed from a NIB file
 *
 * @return The autoreleased TransferDetailCell constructed from a NIB file
 */
+ (TransferDetailCell *)transferDetailCell;

/**
 * Returns the cell height for the provided title and attributes instance
 *
 * @param titleAndAttributes The title and attributes to display inside the cell
 * @return The cell height for the provided information
 */
+ (CGFloat)cellHeightForTitleAndAttributes:(TitleAndAttributes *)titleAndAttributes;
/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

/**
 * Displays the title and attributes instance to populate the cell
 *
 * @param titleAndAttributes The title and attributes to display inside the cell
 */
- (void)displayTitleAndAttributes:(TitleAndAttributes *)titleAndAttributes;

@end
