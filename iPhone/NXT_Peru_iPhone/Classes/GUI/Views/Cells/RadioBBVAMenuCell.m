//
//  RadioBBVAMenuCell.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/28/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "RadioBBVAMenuCell.h"

#import "UIColor+BBVA_Colors.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"RadioBBVAMenuCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"RadioBBVAMenuCell"

#define CELL_HEIGHT 45

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              14.0f



@implementation RadioBBVAMenuCell

@synthesize topTextLabel = topTextLabel_;
@synthesize iconOptionImageView = iconOptionImageView_;
@synthesize tagSelectionView = tagSelectionView_;
@synthesize checkActive = checkActive_;


/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [topTextLabel_ release];
    topTextLabel_ = nil;
    
    [iconOptionImageView_ release];
    iconOptionImageView_ = nil;
    
    [super dealloc];
    
}


/**
 * Creates and returns an autoreleased CheckCell constructed from a NIB file
 *
 * @return The autoreleased CheckCell constructed from a NIB file
 */
+ (RadioBBVAMenuCell *)radioBBVAMenuCell{
    
    RadioBBVAMenuCell *result = (RadioBBVAMenuCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;

}



/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier{
      return CELL_IDENTIFIER;
}


#pragma mark -
#pragma mark Getters and setters

/**
 * Sets the Seal
 */
- (void)setCheckActive:(BOOL)checkActive {
    
    checkActive_ = checkActive;
    
    if (checkActive) {
        [tagSelectionView_ setHidden:NO];
    }else{
        [tagSelectionView_ setHidden:YES];
    }
}



- (void)awakeFromNib {
    // Initialization code
    
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [NXT_Peru_iPhoneStyler styleLabel:topTextLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVAWhiteColor]];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

/**
 * Sets the action label text
 *
 * @param anActionText The action label text
 */
- (void)setTitleText:(NSString *)anTitleText{
    topTextLabel_.text = anTitleText;
}

/**
 * Sets the action icon image
 *
 * @param anActionIcon The action icon image
 */
- (void)setMenuIcon:(UIImage *)anMenuIcon{
    
    iconOptionImageView_.image = anMenuIcon;

}
/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    
    return CELL_HEIGHT;
    
}

@end
