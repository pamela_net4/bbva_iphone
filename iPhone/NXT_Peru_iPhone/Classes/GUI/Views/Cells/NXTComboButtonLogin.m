/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTComboButtonLogin.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"

@interface NXTComboButtonLogin(private)

/**
 * Initiliazes the control
 */
- (void)initialize;

@end

@implementation NXTComboButtonLogin

@dynamic inputView;
@dynamic inputAccessoryView;
@dynamic stringsList;
@dynamic selectedIndex;
@synthesize picker = picker_;
@synthesize fillFormMessage = fillFormMessage_;
@synthesize delegate = delegate_;
@synthesize informFirstSelection = informFirstSelection_;
@dynamic title;
@dynamic valid;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    [inputView_ release];
    inputView_ = nil;
    
    [inputAccessoryView_ release];
    inputAccessoryView_ = nil;
    
    [picker_ release];
    picker_ = nil;
    
    [stringsList_ release];
    stringsList_ = nil;
    
    [title_ release];
    title_ = nil;
    
    [fillFormMessage_ release];
    fillFormMessage_ = nil;
    
    [super dealloc];
}

#pragma mark -
#pragma mark Initilization

/**
 * View is loaded form a NIB file
 */
- (void)awakeFromNib {
    [NXT_Peru_iPhoneStyler styleComboButtonLogin:self];
    [self initialize];    
}

/**
 * Designated initializer
 */
- (id)init {
    if (self = [super init]) {
        [NXT_Peru_iPhoneStyler styleComboButton:self];
        [self initialize];
    }
    
    return self;
}

/**
 * Designated initializer. The view labels are created
 *
 * @param frame The view original frame
 * @return The initialized NXTNaviationItemTitleView instance
 */
- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initialize];
    }
    
    return self;
}

/*
 * Initiliazes the control
 */
- (void)initialize {
    [stringsList_ release];
    stringsList_ = [[NSArray alloc] init];

    [picker_ release];
    picker_ = [[UIPickerView alloc] initWithFrame: CGRectMake(0.0f, 0.0f, 320.0f, 216.0f)];
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        picker_.backgroundColor = [UIColor BBVAGreyToneFourColor];
    }
	picker_.delegate = self;
	picker_.dataSource = self;
	picker_.showsSelectionIndicator = YES;
    
    selectedIndex_ = -1;
    
    [title_ release];
    title_ = nil;
    
    self.inputView = picker_;
}

#pragma mark -
#pragma mark Getters and setters overriden from UIResponder

/**
 * Returns the custom input view to display when the object becomes the first responder
 */
- (UIView *)inputView {
    return inputView_;
}

/**
 * Sets the custom input view to display when the object becomes the first responder
 *
 * @param anInputView to show when control becomes first responder
 */
- (void)setInputView:(UIView *)anInputView {
    if (inputView_ != anInputView) {
        [inputView_ release];
        inputView_ = [anInputView retain];
    }
}

/**
 * Returns the custom accessory view to display when the object becomes the first responder
 */
- (UIView *)inputAccessoryView {
    return inputAccessoryView_;
}

/**
 * Sets the custom accessory view to display when the object becomes the first responder
 *
 * @param anInputAccessoryView to show when control becomes first responder
 */
- (void)setInputAccessoryView:(UIView *)anInputAccessoryView {
    if (inputAccessoryView_ != anInputAccessoryView) {
        [inputAccessoryView_ release];
        inputAccessoryView_ = [anInputAccessoryView retain];
    }
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Returns the strings list used for populate picker
 */
- (NSArray *)stringsList {
    return stringsList_;
}

/**
 * Sets the strings list used for populate picker
 *
 * @param aStringsList to use
 */
- (void)setStringsList:(NSArray *)aStringsList {
    if (stringsList_ != aStringsList) {
        [stringsList_ release];
        stringsList_ = [aStringsList retain];
    }
    
    [picker_ reloadAllComponents];
}

- (NSInteger)selectedIndex {
    return selectedIndex_;
}

- (void)setSelectedIndex:(NSInteger)index {

    if (index != selectedIndex_) {
        
        selectedIndex_ = index;
        
        if (index == -1) {
            
            if ([stringsList_ count] > 1) {
                
                if (title_ != nil) {
                    
                    [self setTitle:title_ forState:UIControlStateNormal];

                    [picker_ selectRow:0 inComponent:0 animated:NO];
                    
                } else {
                    
                    selectedIndex_ = 0;
                    [picker_ selectRow:selectedIndex_ inComponent:0 animated:NO];
                    [self pickerView:picker_ didSelectRow:selectedIndex_ inComponent:0];
                }
                
            } else {
                
                selectedIndex_ = 0;
                [picker_ selectRow:selectedIndex_ inComponent:0 animated:NO];
                [self pickerView:picker_ didSelectRow:selectedIndex_ inComponent:0];                
            }
            
        } else {
            
            [picker_ selectRow:selectedIndex_ inComponent:0 animated:NO];
            [self pickerView:picker_ didSelectRow:selectedIndex_ inComponent:0];
        }
    }    
}

- (NSString *)title {
    return title_;
}

- (void)setTitle:(NSString *)aTitle {
    if (title_ != aTitle) {
        [title_ release];
        title_ = [aTitle copy];
    }
    
    [self setTitle:title_ forState:UIControlStateNormal];
}

- (BOOL)valid {
    return (selectedIndex_ > -1 && [stringsList_ count] > 0);
}

#pragma mark -
#pragma mark UIResponder overriden methods

/**
 * Returns a Boolean value indicating whether the receiver can become first responder.
 */
- (BOOL)canBecomeFirstResponder {
    return YES;
}

/**
 * Notifies the receiver that it is about to become first responder in its window.
 */
- (BOOL)becomeFirstResponder {
    self.highlighted = YES;
    [self pickerView:picker_ didSelectRow:(selectedIndex_ > -1 ? selectedIndex_ : 0) inComponent:0];
    return [super becomeFirstResponder];
}

/**
 * Returns a Boolean value indicating whether the receiver is willing to relinquish first-responder status.
 */
- (BOOL)canResignFirstResponder {
    return YES;
}

/**
 * Notifies the receiver that it has been asked to relinquish its status as first responder in its window.
 */
- (BOOL)resignFirstResponder {
    self.highlighted = NO;
    return [super resignFirstResponder];
}

#pragma mark -
#pragma mark Touches

/**
 * Tells the receiver when one or more fingers are raised from a view or window. Disables the listener notifications
 * Here we force the control to maintain highlighted bacause when a touch ended the control goes to normal state
 *
 * @param touches A set of UITouch instances that represent the touches for the ending phase of the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    self.highlighted = YES;
}
#pragma mark -
#pragma mark UIButton selectors

/**
 * Returns the rectangle in which the receiver draws its title. The image on the right flag is used to determine the frame.
 *
 * @param contentRect The content rectangle for the receiver.
 * @return The rectangle in which the receiver draws its title.
 */
- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    
    CGRect result = [super titleRectForContentRect:contentRect];
    
    CGFloat width = CGRectGetWidth(contentRect);
    CGFloat heigh = CGRectGetHeight(contentRect);
    
    CGFloat offset = 5.0f;
    
    if (width < 15.0f) {
        
        offset = 0.0f;
        
    }
    
    UIImage *image = [self imageForState:self.state];
    
    if (image != nil) {
        
        CGSize imageSize = image.size;
        CGFloat imageWidth = imageSize.width;
        
        result = CGRectMake(offset,
                            0.0f,
                            width - imageWidth - (2.0f * offset),
                            heigh);
        
    } else {
        
        result = CGRectMake(offset,
                            0.0f,
                            width - offset,
                            heigh);
        
    }
    
    return result;
    
}

/**
 * Returns the rectangle in which the receiver draws its image. The image on the right flag is used to determine the frame.
 *
 * @param contentRect The content rectangle for the receiver.
 * @return The rectangle in which the receiver draws its image.
 */
- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    
    CGRect result = [super imageRectForContentRect:contentRect];
    
    CGFloat width = CGRectGetWidth(contentRect);
    CGFloat heigh = CGRectGetHeight(contentRect);
    
    CGFloat offset = 5.0f;
    
    if (width < 15.0f) {
        
        offset = 0.0f;
        
    }
    
    UIImage *image = [self imageForState:self.state];
    
    if (image != nil) {
        
        CGSize imageSize = image.size;
        CGFloat imageWidth = imageSize.width;
        CGFloat imageHeight = imageSize.height;
        
        result = CGRectMake(width - imageWidth - offset,
                            round((heigh - imageHeight) / 2.0f),
                            imageWidth,
                            imageHeight);
        
    } else {
        
        result = CGRectZero;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark UIPickerViewDelegate selectors

/**
 * Invoked by framework when a picker row is selected. When showing prediction information
 * current text field text is updated with selected row element
 *
 * @param pickerView An object representing the picker view requesting the data
 * @param row A zero-indexed number identifying a row of component. Rows are numbered top-to-bottom
 * @param component A zero-indexed number identifying a component of pickerView. Components are numbered left-to-right
 */
- (void) pickerView: (UIPickerView*) pickerView didSelectRow: (NSInteger) row inComponent: (NSInteger) component {
    
    if ([stringsList_ count] > 0) {
    
        [self setTitle:[stringsList_ objectAtIndex:row] forState:UIControlStateNormal];
        selectedIndex_ = row;
    
        if (delegate_ != nil && informFirstSelection_) {
            if ([(NSObject *)delegate_ respondsToSelector:@selector(NXTComboButtonValueSelected:)]) {
                [delegate_ NXTComboButtonValueSelected:self];
            }
        }
        
        if (!informFirstSelection_) {
            informFirstSelection_ = YES;
        }
        
    } else {
        
        [self setTitle:title_ forState:UIControlStateNormal];
        
    }

}

/**
 * Invoked when framework needs the number of rows for a picker view in a given component. When showing
 * the predictive information, the predictive array size plus one (the current text) is shown. 
 *
 * @param pickerView The picker view requesting the data
 * @param component A zero-indexed number identifying a component of pickerView. Components are numbered left-to-right
 * and in this case, it must be always 0
 * @return The number of rows the picker view should display at the given component
 */
- (NSInteger) pickerView: (UIPickerView*) pickerView numberOfRowsInComponent: (NSInteger) component {
	return [stringsList_ count];
}

/**
 * Invoked by framework to query about a picker title for a row and component. When showing the
 * predictive information, the predictive text at the given position is shown
 *
 * @param pickerView object representing the picker view requesting the data
 * @param row A zero-indexed number identifying a row of component. Rows are numbered top-to-bottom
 * @param component A zero-indexed number identifying a component of pickerView. Components are numbered left-to-right
 * @return The string to use as the title of the indicated component row
 */
- (NSString*) pickerView: (UIPickerView*) pickerView titleForRow: (NSInteger) row forComponent: (NSInteger) component {
	return nil;
}

/**
 * Called by the picker view when it needs the view to use for a given row in a given component.
 *
 * @param pickerView An object representing the picker view requesting the data.
 * @param row A zero-indexed number identifying a row of component. Rows are numbered top-to-bottom.
 * @param component A zero-indexed number identifying a component of pickerView. Components are numbered left-to-right.
 * @param view A view object that was previously used for this row, but is now hidden and cached by the picker view.
 */
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *result = nil;
    
    if (view == nil) {
        result = [[[UILabel alloc] initWithFrame:CGRectMake(15.0f, 0.0f, 280.0f, 30.0f)] autorelease];
        [NXT_Peru_iPhoneStyler stylePickerLabel:result];
    } else {
        result = (UILabel *) view;
    }
    
    result.text = [stringsList_ objectAtIndex:row];
    
    return result;
}

/**
 * Invoked when framework needs the number of components (columns) for a picker view. In this case
 * this number is always 1
 *
 * @param pickerView The picker view requesting the data
 * @return The number of components (columns) the picker view should display
 */
- (NSInteger) numberOfComponentsInPickerView: (UIPickerView*) pickerView {
	return 1;
}

@end
