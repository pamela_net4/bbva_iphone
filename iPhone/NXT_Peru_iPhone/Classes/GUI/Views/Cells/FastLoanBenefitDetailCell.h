//
//  FastLoanBenefitDetailCell.h
//  NXT_Peru_iPhone
//
//  Created by Flavio Franco Tunqui on 2/23/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NXTTableCell.h"

@class TextCell;

@interface FastLoanBenefitDetailCell : NXTTableCell <UITextViewDelegate> {
    UITextView *detailTextView_;
}

/**
 * Provides read-write access to the titleLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UITextView *detailTextView;

/**
 * Creates and returns an autoreleased CheckCell constructed from a NIB file
 *
 * @return The autoreleased FastLoanBenefitDetailCell constructed from a NIB file
 */
+ (FastLoanBenefitDetailCell *)fastLoanBenefitDetailCell;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

/**
 * Sets the action label text
 *
 * @param anActionText The action label text
 */
- (void)setDetail:(NSString *)detail;


@end