/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "CheckCardCell.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKLimitedLengthTextField.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"CheckCardCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"CheckCardCell"

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              12.0f


#pragma mark -

@implementation CheckCardCell

#pragma mark -
#pragma mark Properties

@synthesize topTextLabel = topTextLabel_;
@synthesize checkImageView = checkImageView_;
@synthesize firstTextField = firstTextField_;
@synthesize secondTextField = secondTextField_;
@synthesize thirdTextField = thirdTextField_;
@synthesize fourthTextField = fourthTextField_;
@synthesize checkActive = checkActive_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [topTextLabel_ release];
    topTextLabel_ = nil;
    
    [checkImageView_ release];
    checkImageView_ = nil;
    
    [firstTextField_ release];
    firstTextField_ = nil;
    
    [secondTextField_ release];
    secondTextField_ = nil;
    
    [thirdTextField_ release];
    thirdTextField_ = nil;
    
    [fourthTextField_ release];
    fourthTextField_ = nil;
        
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
     
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    [self setBackgroundColor:[UIColor whiteColor]];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [NXT_Peru_iPhoneStyler styleMokTextField:firstTextField_ 
                                withFontSize:TEXT_FONT_SIZE 
                                    andColor:[UIColor grayColor]];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:secondTextField_ 
                                withFontSize:TEXT_FONT_SIZE 
                                    andColor:[UIColor grayColor]];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:thirdTextField_ 
                                withFontSize:TEXT_FONT_SIZE 
                                    andColor:[UIColor grayColor]];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:fourthTextField_ 
                                withFontSize:TEXT_FONT_SIZE 
                                    andColor:[UIColor grayColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:topTextLabel_ 
                         withFontSize:TEXT_FONT_SIZE 
                                color:[UIColor grayColor]];
    
    [firstTextField_ setTextAlignment:UITextAlignmentCenter];
    [secondTextField_ setTextAlignment:UITextAlignmentCenter];
    [thirdTextField_ setTextAlignment:UITextAlignmentCenter];
    [fourthTextField_ setTextAlignment:UITextAlignmentCenter];
    
    [topTextLabel_ setText:NSLocalizedString(SHARE_SEAL_TEXT_KEY, nil)];
}

/*
 * Creates and returns an autoreleased CheckCardCell constructed from a NIB file
 */
+ (CheckCardCell *)checkCardCell {
    
    CheckCardCell *result = (CheckCardCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;    
}

#pragma mark -
#pragma mark Getters and setters


/**
 * Sets the Seal
 */
- (void)setCheckActive:(BOOL)checkActive {
    
    checkActive_ = checkActive;
    [firstTextField_ setHidden:!checkActive];
    [secondTextField_ setHidden:!checkActive];
    [thirdTextField_ setHidden:!checkActive];
    [fourthTextField_ setHidden:!checkActive];

    if (checkActive) {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_SELECTED_RADIO]];
        
    } else {
    
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_UNSELECTED_RADIO]];
    
    }
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeightCheckOn:(BOOL)check {
    
    if (check) {
        return  90.0f;
    } else {
        return  44.0f;
    }

}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

@end