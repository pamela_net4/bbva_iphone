/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class AccountTransaction;
@class AccountTransactionCell;

/**
 * Provider to obtain a AccountTransactionCell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountTransactionCellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary AccountTransactionCell to create it from a NIB file
     */
    AccountTransactionCell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary AccountTransactionCell
 */
@property (nonatomic, readwrite, assign) IBOutlet AccountTransactionCell *auxView;

@end


/**
 * Account Transaction cell. It's a subclass of NXTTableCell.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountTransactionCell : NXTTableCell {
@private    
    /**
     * Bottom left text label
     */
    UILabel *bottomLeftTextLabel_;
    
    /**
     * Bottom right text label
     */
    UILabel *bottomRightTextLabel_;
    
    /**
     * Account Transaction used to populate the cell
     */
    AccountTransaction *accountTransaction_;
    
}

/**
 * Provides readwrite access to the AccountTransaction
 */
@property (nonatomic, readwrite, retain) AccountTransaction *accountTransaction;

/**
 * Provides read-write access to the bottomLeftTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *bottomLeftTextLabel;

/**
 * Provides read-write access to the bottomLeftTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *bottomRightTextLabel;

/**
 * Creates and returns an autoreleased AccountTransactionCell constructed from a NIB file
 *
 * @return The autoreleased AccountTransactionCell constructed from a NIB file
 */
+ (AccountTransactionCell *)accountTransactionCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end