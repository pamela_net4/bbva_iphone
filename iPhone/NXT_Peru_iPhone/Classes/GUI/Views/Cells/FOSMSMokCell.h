//
//  FOEmailMokCell.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 17/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NXTTableCell.h"
#import "MOKStringListSelectionButton.h"

@protocol FOSMSMokCellDelegate

-(void) smsSwitchButtonHasBeenTapped:(BOOL)on;

@end

@interface FOSMSMokCell : NXTTableCell {

@private
    
    UILabel *headerLabel_;
    
    UIView *switchView_;
    
    UIImageView *backgroundImageView_;
    
    UILabel *titleLabel_;
    
    UISwitch *smsSwitch_;
    
    MOKStringListSelectionButton *smsComboButton_;
    
    NSMutableArray *smsArray_;
    
    id<FOSMSMokCellDelegate> delegate_;
}

@property (retain, nonatomic) IBOutlet UIView *switchView;

@property (nonatomic, readwrite, retain) NSArray *smsArray;

@property (retain, nonatomic) IBOutlet UILabel *headerLabel;

@property (nonatomic, readwrite, retain) IBOutlet MOKStringListSelectionButton *smsComboButton;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView *backgroundImageView;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

@property (nonatomic, readwrite, retain) IBOutlet UISwitch *smsSwitch;

@property (nonatomic, readwrite, assign) id<FOSMSMokCellDelegate> delegate;

+ (FOSMSMokCell *) FOSMSMokCell;

+ (CGFloat)cellHeightForFirstAddOn:(BOOL)firstAddOn;

+ (NSString *)cellIdentifier;

- (IBAction)switchButtonTapped;

- (void)setConfigurationForFirstAddOn:(BOOL)firstAddOn;

@end
