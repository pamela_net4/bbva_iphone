/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class FastLoanScheduleCell;

/**
 *FastLoanScheduleCell cell. It's a subclass of NXTTableCell.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface FastLoanScheduleCell : NXTTableCell {
@private  
    UILabel *textLabel1_;
    UILabel *textLabel2_;
    UILabel *textLabel3_;
    UILabel *textLabel4_;
    UILabel *textLabel5_;
    UILabel *textLabel6_;
    UIView *separatorView1_;
    UIView *separatorView2_;
    UIView *separatorView3_;
    UIView *separatorView4_;
    UIView *separatorView5_;
    
}

/**
 * Provides read-write access to the textLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *textLabel1;
/**
 * Provides read-write access to the separatorView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *separatorView1;

/**
 * Provides read-write access to the textLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *textLabel2;
/**
 * Provides read-write access to the separatorView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *separatorView2;

/**
 * Provides read-write access to the textLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *textLabel3;
/**
 * Provides read-write access to the separatorView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *separatorView3;

/**
 * Provides read-write access to the textLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *textLabel4;
/**
 * Provides read-write access to the separatorView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *separatorView4;

/**
 * Provides read-write access to the textLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *textLabel5;
/**
 * Provides read-write access to the separatorView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *separatorView5;

/**
 * Provides read-write access to the textLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *textLabel6;

/**
 * Creates and returns an autoreleased FastLoanScheduleCell constructed from a NIB file
 *
 * @return The autoreleased FastLoanScheduleCell constructed from a NIB file
 */
+ (FastLoanScheduleCell *)FastLoanScheduleCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end