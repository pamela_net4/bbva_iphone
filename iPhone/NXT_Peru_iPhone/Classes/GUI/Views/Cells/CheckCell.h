//
//  CheckCell.h
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 26/11/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "NXTTableCell.h"

#import "SingletonBase.h"
#import "NXTTableCell.h"

@class MOKStringListSelectionButton;

@interface CheckCell : NXTTableCell
{
@private
    
    /**
     * Top text label
     */
    UILabel *topTextLabel_;
    
    /**
     * Check image view
     */
    UIImageView *checkImageView_;
    
    /**
     * Check active flag
     */
    BOOL checkActive_;
    
}

/**
 * Provides read-write access to the topTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *topTextLabel;

/**
 * Provides read-write access to the checkImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *checkImageView;

/**
 * Provides read-write access to the checkActive
 */
@property (nonatomic, readwrite, assign) BOOL checkActive;

/**
 * Creates and returns an autoreleased CheckCell constructed from a NIB file
 *
 * @return The autoreleased CheckCell constructed from a NIB file
 */
+ (CheckCell *)checkCell;


/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;



@end

