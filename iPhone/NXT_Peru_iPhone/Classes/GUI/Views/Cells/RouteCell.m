/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "RouteCell.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NibLoader.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"RouteCell"

/**
 * Define the cell of food type.
 */
#define CELL_IDENTIFIER_FOOD                                        @"RouteCellFood"

/**
 * Defines the cell of car type.
 */
#define CELL_IDENTIFIER_CAR                                         @"RouteCellCar"

/**
 * Define the cell height
 */
#define CELL_HEIGHT                                                 50.0f

@implementation RouteCell

#pragma mark -
#pragma mark Properties

@synthesize iconImageView = iconImageView_;
@synthesize disclosureArrow = disclosureArrow_;
@synthesize textLabel = textLabel_;
@synthesize distanceLabel = distanceLabel_;
@synthesize routeType = routeType_;
@dynamic showDisclosureArrow;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [iconImageView_ release];
    iconImageView_ = nil;
	
	[disclosureArrow_ release];
	disclosureArrow_ = nil;
    
    [textLabel_ release];
    textLabel_ = nil;
    
    [distanceLabel_ release];
    distanceLabel_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    [distanceLabel_ setTextColor:[UIColor BBVABlueSpectrumToneTwoColor]];
	
	[disclosureArrow_ setImage:[[ImagesCache getInstance] imageNamed:ARROW_ICON_IMAGE_FILE_NAME]];
    [disclosureArrow_ setHidden:YES];
    
    [NXT_Peru_iPhoneStyler styleLabel:textLabel_
                         withFontSize:15.0f
                                color:[UIColor darkGrayColor]];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased RouteCell constructed from a NIB file.
 */
+ (RouteCell *)routeCell {
    
    RouteCell *result = (RouteCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark RouteCell selectors

/*
 * Returns the cell height.
 */
+ (CGFloat)cellHeight {
    
    return CELL_HEIGHT;
    
}

/*
 * Returns the cell identifier.
 */
+ (NSString *)cellIdentifierWithRouteType:(RouteType)routeType {
    
    if (routeType == rt_food) {
        
        return CELL_IDENTIFIER_FOOD;
        
    } else if (routeType == rt_car) {
        
        return CELL_IDENTIFIER_CAR;
        
    } else {
        
        return nil;
        
    }
    
}

#pragma mark -
#pragma mark Cell selectors

/**
 * Layouts the cell to set s
 */
- (void)setShowDisclosureArrow:(BOOL)value {
    
    disclosureArrow_.hidden = !value;
	
    if (!value) {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
	
}

#pragma mark -
#pragma mark RouteCell selectors

/*
 * Set the cell information by distance and duration.
 */
- (void)setRouteInformationDuration:(NSString *)duration distance:(NSString *)distance {
    
	if (!([duration isEqualToString:@""] && [distance isEqualToString:@""])) {
		
		[distanceLabel_ setText:[NSString stringWithFormat:@"%@, %@", duration, distance]];
		
	}
    
}

/**
 * Set the routeType_ value to another value.
 *
 * @param routeType: Another value to routeType_.
 */
- (void)setRouteType:(RouteType)routeType {
    
    if (routeType_ != routeType) {
        
        routeType_ = routeType;
        
    }
    
    if (routeType == rt_car) {
        
        [iconImageView_ setImage:[[ImagesCache getInstance] imageNamed:MAP_ICON_CAR_IMAGE_FILE]];
        
        [textLabel_ setText:NSLocalizedString(ROUTE_BY_CAR_BUTTON_TEXT_KEY, nil)];        
        
    } else {
        
        [iconImageView_ setImage:[[ImagesCache getInstance] imageNamed:MAP_ICON_FOOD_IMAGE_FILE]];
        
        [textLabel_ setText:NSLocalizedString(ROUTE_ON_FOOT_BUTTON_TEXT_KEY, nil)];
        
    }
    
}

@end
