/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class MOKLimitedLengthTextField;

/**
 * Check card cell. It's a subclass of NXTTableCell.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CheckCardCell : NXTTableCell {
@private  
    
    /**
     * Top text label
     */
    UILabel *topTextLabel_;
    
    /**
     * Check image view
     */
    UIImageView *checkImageView_;
    
    /**
     * First text field
     */
    MOKLimitedLengthTextField *firstTextField_;
    
    /**
     * Second text field
     */
    MOKLimitedLengthTextField *secondTextField_;
    
    /**
     * Third text field
     */
    MOKLimitedLengthTextField *thirdTextField_;
    
    /**
     * Fourth text field
     */
    MOKLimitedLengthTextField *fourthTextField_;
    
    /**
     * Check active flag
     */
    BOOL checkActive_;
    
}

/**
 * Provides read-write access to the topTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *topTextLabel;

/**
 * Provides read-write access to the checkImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *checkImageView;

/**
 * Provides read-write access to the firstTextField and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKLimitedLengthTextField *firstTextField;

/**
 * Provides read-write access to the secondTextField and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKLimitedLengthTextField *secondTextField;

/**
 * Provides read-write access to the thirdTextField and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKLimitedLengthTextField *thirdTextField;

/**
 * Provides read-write access to the fourthTextField and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKLimitedLengthTextField *fourthTextField;

/**
 * Provides read-write access to the checkActive
 */
@property (nonatomic, readwrite, assign) BOOL checkActive;

/**
 * Creates and returns an autoreleased CheckCardCell constructed from a NIB file
 *
 * @return The autoreleased CheckCardCell constructed from a NIB file
 */
+ (CheckCardCell *)checkCardCell;

/**
 * Returns the cell height
 *
 * @param check Check flag
 * @return The cell height
 */
+ (CGFloat)cellHeightCheckOn:(BOOL)check;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end