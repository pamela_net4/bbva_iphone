//
//  FOEmailMokCell.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 17/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOEmailMokCell.h"
#import "NXTTableCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"
#import "StringKeys.h"
#import "NibLoader.h"

NSString *const kFOEmailMokCellNibFileName = @"FOEmailMokCell";
 
NSString *const kFOEmailCellIdentifier = @"FOEmailMokCell";

/**
 * Defines the cell height
 */
#define FONT_SIZE                                           14.0f

/**
 * Denifes the vertical margin size
 */
#define VERTICAL_MARGIN_SIZE                                10.0f

/**
 * Denifes the vertical margin size
 */
#define SWITCH_HEIGHT                                       27.0f

/**
 * Denifes the vertical margin size
 */
#define TXTFIELD_HEIGHT                                     31.0f

/**
 * Denifes the vertical margin size
 */
#define TEXTVIEW_HEIGHT                                     93.0f

/**
 * Denifes the vertical margin size
 */
#define BUTTON_HEIGHT                                       37.0f



@implementation FOEmailMokCell


@synthesize backgroundImageView = backgroundImageView_;
@synthesize titleLabel = titleLabel_;
@synthesize emailSwitch = emailSwitch_;
@synthesize delegate = delegate_;
@synthesize emailComboButton = emailComboButton_;
@synthesize emailArray = emailArray_;

- (void)dealloc
{
    [backgroundImageView_ release];
    backgroundImageView_ = nil;
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [emailSwitch_ release];
    emailSwitch_ = nil;
    
    [emailComboButton_ release];
    emailComboButton_ = nil;
    
    [super dealloc];
}

-(void)awakeFromNib{

    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.showDisclosureArrow = NO;
    self.showSeparator = NO;
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:FONT_SIZE color: [UIColor BBVABlackColor]];
    [titleLabel_ setText:NSLocalizedString(SEND_PHONE_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleAccountSelectionButton: emailComboButton_];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1){
        
        [emailSwitch_ setOnTintColor:[UIColor BBVABlueSpectrumColor]];
        [emailSwitch_ setTintColor: [UIColor BBVABlueSpectrumColor]];
    }
    
    [emailSwitch_ setOn:NO];
    
}


+ (FOEmailMokCell *) FOEmailMokCell {

    FOEmailMokCell *result = (FOEmailMokCell *)[NibLoader loadObjectFromNIBFile:kFOEmailMokCellNibFileName];
    
    [result awakeFromNib];
    
    return result;
}

+ (NSString *) cellIdentifier {

    return kFOEmailCellIdentifier;
    
}

+ (CGFloat) cellHeightForFirstAddOn:(BOOL)firstAddOn {

    CGFloat result = 60.0f;
    
    if (firstAddOn) {
        
        result = 120.0f;
        
    }
    
    return result;
}

- (IBAction) switchButtonTapped {

    [delegate_ emailSwitchButtonHasBeenTapped: [emailSwitch_ isOn]];
    
}

- (void)setConfigurationForFirstAddOn:(BOOL)firstAddOn {
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
    CGRect frame;
    CGFloat yPos;
    
    frame = backgroundImageView_.frame;
    frame.origin.y = VERTICAL_MARGIN_SIZE;
    frame.size.height = 60.0f;
    backgroundImageView_.frame = frame;
    yPos = frame.origin.y + frame.size.height;
    
    self.emailComboButton.hidden = YES;
    
    if (firstAddOn) {
        
        self.emailComboButton.hidden = NO ;
        
        yPos += VERTICAL_MARGIN_SIZE;
        frame = backgroundImageView_.frame;
        frame.size.height = 120.0f;//yPos + backgroundImageView_.frame.origin.y;
        backgroundImageView_.frame = frame;
        
    }
    
}

-(void) setEmailArray:(NSArray *)emailArray {

    if (emailArray_ == nil) {
        
        emailArray_ = [[NSMutableArray alloc] init];
        
    }
    [emailArray_ removeAllObjects];
    [emailArray_ addObjectsFromArray:emailArray];
    
    NSMutableArray *array = [NSMutableArray array];
    
    for (NSString *email in emailArray) {
        
        [array addObject:email.description];
        
    }
    [emailComboButton_ setOptionStringList:array];
    [emailComboButton_ setSelectedIndex:0];
    [array removeAllObjects];
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
}

@end
