/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "AccountTransactionCell.h"
#import "AccountTransaction.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"AccountTransactionCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"AccountTransactionCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 57.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                      12.0f


#pragma mark -

/**
 * AccountTransactionCellProvider private category
 */
@interface AccountTransactionCellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (AccountTransactionCellProvider *)getInstance;

/**
 * Creates and returns an autoreleased AccountTransactionCell constructed from a NIB file
 *
 * @return The autoreleased AccountTransactionCell constructed from a NIB file
 */
- (AccountTransactionCell *)accountTransactionCell;

@end


#pragma mark -

@implementation AccountTransactionCellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * AccountTransactionCellProvier singleton only instance
 */
static AccountTransactionCellProvider *accountTransactionCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([AccountTransactionCellProvider class]) {
        
        if (accountTransactionCellProviderInstance_ == nil) {
            
            accountTransactionCellProviderInstance_ = [super allocWithZone:zone];
            return accountTransactionCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (AccountTransactionCellProvider *)getInstance {
    
    if (accountTransactionCellProviderInstance_ == nil) {
        
        @synchronized([AccountTransactionCellProvider class]) {
            
            if (accountTransactionCellProviderInstance_ == nil) {
                
                accountTransactionCellProviderInstance_ = [[AccountTransactionCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return accountTransactionCellProviderInstance_;
    
}

#pragma mark -
#pragma mark AccountTransactionCell creation

/*
 * Creates and returns aa autoreleased AccountTransactionCell constructed from a NIB file
 */
- (AccountTransactionCell *)accountTransactionCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    AccountTransactionCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation AccountTransactionCell

#pragma mark -
#pragma mark Properties

@synthesize bottomLeftTextLabel = bottomLeftTextLabel_;
@synthesize bottomRightTextLabel = bottomRightTextLabel_;
@dynamic accountTransaction;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [bottomLeftTextLabel_ release];
    bottomLeftTextLabel_ = nil;
    
    [bottomRightTextLabel_ release];
    bottomRightTextLabel_ = nil;
    
    [accountTransaction_ release];
    accountTransaction_ = nil;
        
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = YES;
    self.showSeparator = YES;

    [NXT_Peru_iPhoneStyler styleLabel:self.leftTextLabel withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor blackColor]];
    [NXT_Peru_iPhoneStyler styleLabel:bottomLeftTextLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:bottomRightTextLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyColor]];

    [self.leftTextLabel setMinimumFontSize:TEXT_FONT_SMALL_SIZE];
    
}

/*
 * Creates and returns an autoreleased AccountTransactionCell constructed from a NIB file
 */
+ (AccountTransactionCell *)accountTransactionCell {
    
    return [[AccountTransactionCellProvider getInstance] accountTransactionCell];
    
}

#pragma mark -
#pragma mark Getters and setters

/*
 * Returns the account
 */
- (AccountTransaction *)accountTransaction {
    return accountTransaction_;
}

/**
 * Sets the AccountTransaction and sets the info into the cell
 */
- (void)setAccountTransaction:(AccountTransaction *)accountTransaction {
    
    if (accountTransaction_ != accountTransaction) {
        
        [accountTransaction_ release];
        accountTransaction_ = [accountTransaction retain];
        
    }
    
    NSString *currency = [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:[accountTransaction currency]];
    
    self.leftText = [accountTransaction description];
    self.rightText = [NSString stringWithFormat:@"%@ %@", currency, [accountTransaction amountString]];
    
    if ([[accountTransaction amount] compare:[NSDecimalNumber zero]] == NSOrderedAscending) {
        
        [self.rightTextLabel setTextColor:[UIColor BBVAMagentaColor]];
        
    } else {
        
        [self.rightTextLabel setTextColor:[UIColor BBVABlueSpectrumToneTwoColor]];
        
    }

        
    self.bottomLeftTextLabel.text = [accountTransaction operationDateString];
    self.bottomRightTextLabel.text = [NSString stringWithFormat:@"ITF %@ %@", [Tools mainCurrencySymbol], [accountTransaction ITF]];
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    return CELL_HEIGHT;
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

@end