//
//  DetailCashMobileCell.m
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 9/24/13.
//
//

#import "DetailCashMobileCell.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "Constants.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"

/**
 * Define the CoordinateCell NIB file name
 */
#define NIB_FILE_NAME                                               @"DetailCashMobileCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"DetailCashMobileCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 54.0f

/**
 * Font of detail text
 */
#define font_size                                                   13.0f

/**
 * Font of header text
 */
#define font_header_size                                            16.0f

@implementation DetailCashMobileCell

@synthesize isHeader;
@synthesize lblAmount;
@synthesize lblDate;
@synthesize lblOperationNumber;
@synthesize lblOperationNumberDetail;
@synthesize lblPhoneNumber;
@synthesize lblPhoneNumberDetail;
@synthesize lblState;
@synthesize separatorImageView = separatorImageView_;


+ (DetailCashMobileCell *)detailCashMobileCell
{
    return (DetailCashMobileCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark -
#pragma mark Initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    
    [self setStyleCell];
    
}

/**
 *  set the style for the view on the cel depending on the boolean isheader
 */
- (void)setStyleCell
{
    if (isHeader) {
        
        [NXT_Peru_iPhoneStyler styleLabel:lblOperationNumber withBoldFontSize:font_header_size color:[UIColor BBVABlueColor]];
        
        [lblOperationNumber setText:NSLocalizedString(STATES_TEXT_KEY,nil)];
        
        [lblAmount setHidden:TRUE];
        [lblDate setHidden:TRUE];
        [lblPhoneNumber setHidden:TRUE];
        [lblState setHidden:TRUE];
        
    }else{
        [NXT_Peru_iPhoneStyler styleLabel:lblOperationNumber withFontSize:16.0 color:[UIColor BBVAGreyToneOneColor]];
        [NXT_Peru_iPhoneStyler styleLabel:lblOperationNumberDetail withFontSize:16.0 color:[UIColor BBVAGreyColor]];
        [NXT_Peru_iPhoneStyler styleLabel:lblPhoneNumber withFontSize:14.0f color:[UIColor BBVAGreyToneOneColor]];
        [NXT_Peru_iPhoneStyler styleLabel:lblPhoneNumberDetail withFontSize:14.0f color:[UIColor BBVAGreyToneOneColor]];
        [NXT_Peru_iPhoneStyler styleLabel:lblDate withFontSize:14.0f color:[UIColor BBVAGreyToneTwoColor]];
        [NXT_Peru_iPhoneStyler styleLabel:lblAmount withBoldFontSize:18.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
        [NXT_Peru_iPhoneStyler styleLabel:lblState withFontSize:14.0 color:[UIColor BBVAGreyToneOneColor]];
    }
    
    [separatorImageView_ setImage:[[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];
}

#pragma mark - memory management
- (void)dealloc {
    [lblPhoneNumber release];
    [lblAmount release];
    [lblState release];
    [lblDate release];
    [lblOperationNumber release];
    
    [separatorImageView_ release];
    separatorImageView_ = nil;
    [super dealloc];
}
@end
