/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "NXTTableCell.h"

/**
 * Cell containing two labels where the pair of details are writen
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferDoneCell : NXTTableCell <UITextFieldDelegate> {
    
    /**
     * Tick image view
     */
    UIImageView *tickImageView_;
    
    /**
     * Icon image view
     */
    UIImageView *iconImageView_;
    
}


/**
 * Provides read-write access to the tickImageView and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *tickImageView;

/**
 * Provides read-write access to the iconImageView and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *iconImageView;

/**
 * Creates and returns an autoreleased TransferDoneCell constructed from a NIB file
 *
 * @return The autoreleased TransferDoneCell constructed from a NIB file
 */
+ (TransferDoneCell *)transferDoneCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;


@end
