/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "SingletonBase.h"


//Forward declarations
@class IconActionCell;


/**
 * Provider to obtain a IconActionCell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface IconActionCellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary IconActionCell to create it from a NIB file
     */
    IconActionCell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary IconActionCell
 */
@property (nonatomic, readwrite, assign) IBOutlet IconActionCell *auxView;

@end


/**
 * Cell containing an icon on the left hand side, an action label in the middle (left aligned) and a button on the right hand side to execute the action (e.g. the
 * cell inside login table to login to mobile banking and to locate the ATMS and branches)
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface IconActionCell : UITableViewCell {

@private
    
    /**
     * Image view describing the action to perform
     */
    UIImageView *actionImageView_;
    
    /**
     * Label describing the action to perform
     */
    UILabel *actionLabel_;
    
    /**
     * Button to perform the action
     */
    UIButton *actionButton_;
    
    /**
     * Cell separator image
     */
    UIImageView *cellSeparatorImage_;
    
    /**
     * Display image flag. YES to display image, NO to hide it
     */
    BOOL displayImage_;
    
}

/**
 * Provides read-write access to the image view describing the action to perform and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *actionImageView;

/**
 * Provides read-write access to the label describing the action to perform and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *actionLabel;

/**
 * Provides read-write access to the button to perform the action and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *actionButton;

/**
 * Provides read-write access to the cell separator image and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *cellSeparatorImage;

/**
 * Provides read-write access to the display image flag
 */
@property (nonatomic, readwrite, assign) BOOL displayImage;


/**
 * Sets the action label text
 * 
 * @param anActionText The action label text
 */
- (void)setActionText:(NSString *)anActionText;

/**
 * Sets the action icon image
 *
 * @param anActionIcon The action icon image
 */
- (void)setActionIcon:(UIImage *)anActionIcon;

/**
 * Sets the action button image
 *
 * @param anActionButtonImage The action button image
 */
- (void)setActionButtonImage:(UIImage *)anActionButtonImage;

/**
 * Displays the cell background in gray color
 *
 * @param displayGray YES to display the background gray, NO to set it transparent
 */
- (void)displayGrayBackground:(BOOL)displayGray;

/**
 * Adds the target and action for the action button
 *
 * @param aTarget The instance to respond when the button is clicked
 * @param anAction The action (selector) to invoke when the button is clicked. It cannot be nil
 */
- (void)addTarget:(id)aTarget action:(SEL)anAction;


/**
 * Creates and returns an autoreleased IconActionCell constructed from a NIB file
 *
 * @return The autoreleased IconActionCell constructed from a NIB file
 */
+ (IconActionCell *)iconActionCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end
