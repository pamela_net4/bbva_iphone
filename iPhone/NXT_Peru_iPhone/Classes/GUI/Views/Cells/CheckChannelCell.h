//
//  CheckChannelCell.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 13/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NXTTableCell.h"
#import "SingletonBase.h"

@class MOKStringListSelectionButton;

@interface CheckChannelCell : NXTTableCell {

@private
    UIView *contentView_;
    
    UILabel *titleTextLabel_;
    
    UIImageView *checkImageView_;
    
    NSString *value_;
    
    BOOL checkActive_;
}

@property (retain, nonatomic) IBOutlet UIView *contentView;

@property (retain, nonatomic) IBOutlet UILabel *titleTextLabel;

@property (retain, nonatomic) IBOutlet UIImageView *checkImageView;

@property (nonatomic, readwrite, assign) BOOL checkActive;

@property (nonatomic, readwrite, assign) NSString *value;

+ (CheckChannelCell *) checkChannelCell;


+ (CGFloat)cellHeightForFirstAddOn:(BOOL)firstAddOn
                       secondAddOn:(BOOL)secondAddOn;

+ (NSString *) cellIdentifier;

@end
