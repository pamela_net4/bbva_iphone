/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class TextCell;

/**
 * Provider to obtain a TextCell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TextCellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary TextCell to create it from a NIB file
     */
    TextCell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary TextCell
 */
@property (nonatomic, readwrite, assign) IBOutlet TextCell *auxView;

@end


/**
 * GlobalPosition cell. It's a subclass of NXTTableCell but it has two labels on the right instead of one.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TextCell : NXTTableCell {
@private    
    /**
     * Text label
     */
    UILabel *textLabel_;
    
}

/**
 * Provides read-write access to the textLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *textLabel;

/**
 * Provides readwrite access to the text
 */
@property (nonatomic, readwrite, copy) NSString *text;


/**
 * Creates and returns an autoreleased TextCell constructed from a NIB file
 *
 * @return The autoreleased TextCell constructed from a NIB file
 */
+ (TextCell *)textCell;

/**
 * Returns the cell height
 *
 * @param text The text
 * @return The cell height
 */
+ (CGFloat)cellHeightForText:(NSString *)text;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;


@end