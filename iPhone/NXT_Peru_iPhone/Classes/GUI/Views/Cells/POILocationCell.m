/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "POILocationCell.h"

#import "ATMList.h"
#import "BranchesList.h"
#import "Constants.h"
#import "GeoServerManager.h"
#import "GeocodedAddressList.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NibLoader.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"POILocationCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"POILocationCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 55.0f

/**
 * Defines the max of label.
 */
#define MAX_WIDTH													180.0f

@interface POILocationCell()

/**
 * Returns image for the network of the POI
 *
 * @return The correct UIImage
 * @private
 */
- (UIImage *)imageForPOINetwork;

@end

@implementation POILocationCell

@synthesize networkImageView = networkImageView_;
@synthesize disclosureImageView = disclosureImageView_;
@synthesize separatorImageView = separatorImageView_;
@synthesize networkLabel = networkLabel_;
@synthesize distanceLabel = distanceLabel_;
@synthesize addressLabel = addressLabel_;
@synthesize poi = poi_;
@synthesize poiType = poiType_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    [networkImageView_ release];
    networkImageView_ = nil;
    
    [disclosureImageView_ release];
    disclosureImageView_ = nil;
    
    [separatorImageView_ release];
    separatorImageView_ = nil;
    
    [networkLabel_ release];
    networkLabel_ = nil;
    
    [distanceLabel_ release];
    distanceLabel_ = nil;
    
    [addressLabel_ release];
    addressLabel_ = nil;

    [poi_ release];
    poi_ = nil;
    
    [super dealloc];
}

#pragma mark -
#pragma mark Initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleGray;
    self.backgroundColor=[UIColor clearColor];
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    separatorImageView_.image = [imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    disclosureImageView_.image = [imagesCache imageNamed:ARROW_ICON_IMAGE_FILE_NAME];
    
    [NXT_Peru_iPhoneStyler styleLabel:networkLabel_ withFontSize:14.0f color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:distanceLabel_ withFontSize:12.0f color:[UIColor BBVABlueSpectrumToneOneColor]];
    [NXT_Peru_iPhoneStyler styleLabel:addressLabel_ withFontSize:12.0f color:[UIColor BBVACoolGreyColor]];
}

/*
 * Creates and returns an autoreleased POILocationCell constructed from a NIB file
 */
+ (POILocationCell *)poiLocationCell {
    return (POILocationCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    return CELL_HEIGHT;
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

#pragma mark -
#pragma mark Data

/*
 * Returns image for the network of the POI
 */
- (UIImage *)imageForPOINetwork {
    
    UIImage *result = nil;
    
    if ([poi_ isKindOfClass:[BranchData class]]) {
        
        result = [[ImagesCache getInstance] imageNamed:MAP_FILTER_BBVA_CONTINENTAL_BRACH_IMAGE_FILE];
    
    } else {
        
        switch ([poi_ category]) {
                
            case atmType:
                
                result = [[ImagesCache getInstance] imageNamed:MAP_FILTER_BBVA_CONTINENTAL_IMAGE_FILE];
                
                break;
                
            case atmBCType:
                
                result = [[ImagesCache getInstance] imageNamed:MAP_FILTER_BCP_IMAGE_FILE];
                
                break;
                
            case atmInterbankType:
                
                result = [[ImagesCache getInstance] imageNamed:MAP_FILTER_INTERBANK_GLOBALNET_IMAGE_FILE];
                
                break;
                
            case atmScotiabankType:
                
                result = [[ImagesCache getInstance] imageNamed:MAP_FILTER_SCOTIABANK_IMAGE_FILE];
                
                break;
                
            case expressAgentType:
                
                result = [[ImagesCache getInstance] imageNamed:MAP_FILTER_EXPRESS_AGENT_IMAGE_FILE];
                
                break;
                
            case expressAgentPlusType:
                
                result = [[ImagesCache getInstance] imageNamed:MAP_FILTER_EXPRESS_AGENT_PLUS_IMAGE_FILE];
                
                break;
                
            case kasnetType:
                
                result = [[ImagesCache getInstance] imageNamed:MAP_FILTER_KASNET_IMAGE_FILE];
                
                break;
                
            case multifacilType:
                
                result = [[ImagesCache getInstance] imageNamed:MAP_FILTER_MULTIFACIL_IMAGE_FILE];
                
                break;
                
            default:
                
                break;
                
        }

    }
    
    return result;
}

/**
 * Sets the poi
 *
 * @param poi The value to set
 */
- (void)setPoi:(ATMData *)poi {
    
    if (poi_ != poi) {
        
        [poi_ release];
        poi_ = [poi retain];
    }
    
    NSMutableString *address = [NSMutableString stringWithString:poi_.geocodedAddress.address.addressFirstLine];
    
    if (poi_.geocodedAddress.address.addressSecondLine.length > 0) {
        
        [address appendFormat:@", %@", poi_.geocodedAddress.address.addressSecondLine];
    }
    
    addressLabel_.text = address;
    
    NSString *distance = [Tools distanceAsString:poi_.geocodedAddress.geoFeatures.addressDistance];
    
    distanceLabel_.text = distance;
    
    NSString *type;
    
    switch (poiType_) {
        case plcOfficeType:
            
            type = NSLocalizedString(SINGLE_BRANCH_POI_MAP_KEY, nil);
            
            break;
            
        case plcATMType:
            
            type = NSLocalizedString(SINGLE_ATM_POI_MAP_KEY, nil);
            
            break;
            
        case plcAgentesType:
            
            type = NSLocalizedString(SINGLE_AGENT_POI_MAP_KEY, nil);
            
            break;
            
        default:
            
            break;
            
    }
    
    NSString *bankName = [NSString stringWithFormat:@"%@ %@", type, poi_.bankName];
    networkLabel_.text = bankName;
    
    networkImageView_.image = [self imageForPOINetwork];
    
    // Reposition name and distance labels

    CGSize networkSize = [bankName sizeWithFont:networkLabel_.font];
    
    CGRect frame = networkLabel_.frame;
    frame.size.width = MIN(networkSize.width, MAX_WIDTH);
    networkLabel_.frame = frame;
    
    frame = distanceLabel_.frame;
    frame.origin.x = CGRectGetMaxX(networkLabel_.frame) + 5.0f;
    distanceLabel_.frame = frame;
}

@end
