//
//  CheckPendingDocCell.m
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/25/13.
//
//

#import "CheckPendingDocCell.h"

#import "ImagesCache.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "NibLoader.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Defines the cell idenfitier.
 */
#define CELL_IDENTIFIER											@"CheckPendingDocCell"

#define NIB_FILE_NAME  											@"CheckPendingDocCell"     

/**
 * font sizes
 */
#define SMALL_FONT_SIZE                                         12.0f

#define EXP_DATE_TITLE_PARTIAL                                  CGRectMake(mainLabel_.frame.origin.x, 34, 162, 21)
#define EXP_DATE_PARTIAL                                        CGRectMake(183, 34, 113, 21)
#define EXP_DATE_TITLE_TOTAL                                    CGRectMake(firstTitle_.frame.origin.x, 58, 162, 21)
#define EXP_DATE_TOTAL                                          CGRectMake(230, 58, 80, 21)

@implementation CheckPendingDocCell

#pragma mark -
#pragma mark Properties
@synthesize mainLabel = mainLabel_;
@synthesize mainDetail = mainDetail_;
@synthesize isSelectedDoc = isSelectedDoc_;
@synthesize firstDetail = firstDetail_;
@synthesize firstTitle = firstTitle_;
@synthesize expirationDateTitle = expirationDateTitle_;
@synthesize minAmountTitle = minAmountTitle_;
@synthesize maxAmountTitle = maxAmountTitle_;
@synthesize expirationDate = expirationDate_;
@synthesize minAmount = minAmount_;
@synthesize maxAmount = maxAmount_;
@synthesize checkImage = checkImage_;


#pragma mark -
#pragma mark View lifecycle

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [checkImage_ setImage:[[ImagesCache getInstance] imageNamed:@"radio_unselected.png"]];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    [NXT_Peru_iPhoneStyler styleNXTView:self];
    
    [NXT_Peru_iPhoneStyler styleLabel:mainDetail_ withFontSize:SMALL_FONT_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:mainLabel_ withFontSize:SMALL_FONT_SIZE color:[UIColor BBVABlackColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:firstTitle_ withFontSize:SMALL_FONT_SIZE color:[UIColor BBVABlackColor]];
    [NXT_Peru_iPhoneStyler styleLabel:firstDetail_ withFontSize:SMALL_FONT_SIZE color:[UIColor grayColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:expirationDateTitle_ withFontSize:SMALL_FONT_SIZE color:[UIColor grayColor]];
    [expirationDateTitle_ setText:NSLocalizedString(PUBLIC_SERVICE_STEP_TWO_SELECTED_DUE_DATE_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleLabel:expirationDate_ withFontSize:SMALL_FONT_SIZE color:[UIColor grayColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:minAmountTitle_ withFontSize:SMALL_FONT_SIZE color:[UIColor grayColor]];
    [minAmountTitle_ setText:NSLocalizedString(INSTITUTIONS_COMPANIES_MIN_AMOUNT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleLabel:minAmount_ withFontSize:SMALL_FONT_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:maxAmountTitle_ withFontSize:SMALL_FONT_SIZE color:[UIColor grayColor]];
    [maxAmountTitle_ setText:NSLocalizedString(INSTITUTIONS_COMPANIES_MAX_AMOUNT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleLabel:maxAmount_ withFontSize:SMALL_FONT_SIZE color:[UIColor BBVABlueSpectrumToneTwoColor]];
	
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased CheckPendingDocCell constructed from a NIB file.
 */
+ (CheckPendingDocCell *)checkPendingDocCell {
    
    CheckPendingDocCell *result = (CheckPendingDocCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;
    
}


/**
 * Sets the selected with an animation
 *
 * @param selected The selected
 * @param animated The animation YES/NO
 */
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    if (selected) {
        if (isSelectedDoc_) {
            checkImage_.image = [[ImagesCache getInstance] imageNamed:@"radio_unselected.png"];
            isSelectedDoc_ = FALSE;
        }else{
            checkImage_.image = [[ImagesCache getInstance] imageNamed:@"radio_selected.png"];
            isSelectedDoc_ = TRUE;
        }
    } else {
        checkImage_.image = [[ImagesCache getInstance] imageNamed:@"radio_unselected.png"];
        isSelectedDoc_ = FALSE;
    }
}

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [firstTitle_ release];
    firstTitle_ = nil;
    
    [firstDetail_ release];
    firstDetail_ = nil;
    
    [expirationDateTitle_ release];
    expirationDateTitle_ = nil;
    
    [expirationDate_ release];
    expirationDate_ = nil;
    
    [minAmountTitle_ release];
    minAmountTitle_ = nil;
    
    [minAmount_ release];
    minAmount_ = nil;
    
    [maxAmountTitle_ release];
    maxAmountTitle_ = nil;
    
    [maxAmount_ release];
    maxAmount_ = nil;
    
    [checkImage_ release];
    checkImage_ = nil;
    
    [super dealloc];
}

/*
 * The cell identifier.
 */
+ (NSString *)cellIdentifier {
	
	return CELL_IDENTIFIER;
	
}

/*
 * Accomodate Views
 */
- (void)accomodateForDocumentType:(BOOL)isPartial
{
    [mainLabel_ setHidden:!isPartial];
    [minAmountTitle_ setHidden:!isPartial];
    [minAmount_ setHidden:!isPartial];
    [maxAmountTitle_ setHidden:!isPartial];
    [maxAmount_ setHidden:!isPartial];
    [firstTitle_ setHidden:isPartial];
    [mainDetail_ setHidden:isPartial];
    [firstDetail_ setHidden:TRUE];
    
    if (!isPartial) {
        [expirationDateTitle_ setFrame:EXP_DATE_TITLE_TOTAL];
        [expirationDate_ setFrame:EXP_DATE_TOTAL];
    }else{
        [expirationDateTitle_ setFrame:EXP_DATE_TITLE_PARTIAL];
        [expirationDate_ setFrame:EXP_DATE_PARTIAL];
    }
}

@end
