/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PhoneCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "StringKeys.h"
#import "Tools.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the PhoneCell NIB file name
 */
NSString * const kPhoneCellNibFileName = @"PhoneCell";

/**
 * Defines the cell identifier
 */
NSString * const kPhoneCellIdentifier = @"PhoneCell";

/**
 * Defines the cell height
 */
CGFloat const kPhoneCellHeight = 50.0f;

/**
 * Defines the phone label font size
 */
CGFloat const kPhoneCellPhoneLabelFontSize = 15.0f;

/**
 * Defines the phone number label font size
 */
CGFloat const kPhoneCellPhoneNumberFontSize = 13.0f;

/**
 * Defines the phone label text offset to left and right borders
 */
CGFloat const kPhoneCellPhoneNumberLabelTextOffset = 12.0f;

/**
 * Defines the phone label offset to the cell right border
 */
CGFloat const kPhoneCellPhoneLabelRightOffset = 20.0f;


#pragma mark -

@implementation PhoneCell

#pragma -
#pragma mark Properties

@synthesize phoneLabel = phoneLabel_;
@synthesize phoneNumberLabel = phoneNumberLabel_;
@synthesize iconImageView = iconImageView_;
@synthesize disclosureArrow = disclosureArrow_;
@dynamic phoneNumber;
@dynamic showDisclosureArrow;

#pragma -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [phoneLabel_ release];
    phoneLabel_ = nil;
    
    [phoneNumberLabel_ release];
    phoneNumberLabel_ = nil;
    
    [iconImageView_ release];
    iconImageView_ = nil;
	
	[disclosureArrow_ release];
	disclosureArrow_ = nil;
    
    [super dealloc];
    
}

#pragma -
#pragma mark Instances initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:phoneLabel_
                 withFontSize:kPhoneCellPhoneLabelFontSize
                        color:[UIColor darkGrayColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:phoneNumberLabel_
                 withFontSize:kPhoneCellPhoneNumberFontSize
                        color:[UIColor darkGrayColor]];
    
    [phoneNumberLabel_ setHighlightedTextColor:[UIColor darkGrayColor]];                
    [phoneNumberLabel_ setTextAlignment:UITextAlignmentCenter];
	
	[disclosureArrow_ setImage:[[ImagesCache getInstance] imageNamed:ARROW_ICON_IMAGE_FILE_NAME]];
    [disclosureArrow_ setHidden:YES];
	
	[iconImageView_ setImage:[[ImagesCache getInstance] imageNamed:MAP_ICON_CALL_IMAGE_FILE]];    
}

/*
 * Creates and return an autoreleased PhoneCell constructed from a NIB file
 */
+ (PhoneCell *)phoneCell {
    
    return (PhoneCell *)[NibLoader loadObjectFromNIBFile:kPhoneCellNibFileName];
    
}

#pragma mark -
#pragma mark UIView selectors

/**
 * Lays-out the subviews. Cell labels are relocated depending on whether the phone number is long or short
 */
- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    [Tools fitLabelWidth:phoneNumberLabel_];

    CGRect labelFrame = [phoneNumberLabel_ frame];
    labelFrame.size.width = labelFrame.size.width + (2.0f * kPhoneCellPhoneNumberLabelTextOffset);
    labelFrame.origin.x = self.frame.size.width - kPhoneCellPhoneLabelRightOffset - labelFrame.size.width;
    phoneNumberLabel_.frame = labelFrame;
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    
    return kPhoneCellHeight;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return kPhoneCellIdentifier;
    
}

#pragma mark -
#pragma mark Cell selectors

/**
 * Layouts the cell to set s
 */
- (void)setShowDisclosureArrow:(BOOL)value {
    
    disclosureArrow_.hidden = !value;
	
    if (!value) {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
	
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the phone number
 *
 * @return The phone number
 */
- (NSString *)phoneNumber {
    
    return phoneNumberLabel_.text;
    
}

/*
 * Sets the new phone number to display
 *
 * @param phoneNumber The new phone number to display
 */
- (void)setPhoneNumber:(NSString *)phoneNumber {
    
    phoneNumberLabel_.text = phoneNumber;
    [self setNeedsLayout];
    
}

@end
