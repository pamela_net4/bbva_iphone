//
//  CheckFOCell.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/6/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "NXTTableCell.h"

@interface CheckFOCell : NXTTableCell{
    
    /*
     *  Label for the alias of the operation
     */
    UILabel *titleLabel_;
    /*
     *  Label for the service of the operation
     */
    UILabel *subtitleLabel_;
    /*
     *  Label for the day of the month
     */
    UILabel *dayLabel_;
    /*
     *  Image to indicate if the cell is selected or not
     */
    UIImageView *checkImageView_;
    /*
     *  Image to indicate if the operation have phone reminder
     */
    UIImageView *phoneImageView_;
    /*
     *  Image to indicate if the operation have mail reimder
     */
    UIImageView *mailImageView_;
    /**
     * Check active flag
     */
    BOOL checkActive_;
    /**
     * Check active phone reminder flag
     */
    BOOL hasPhoneActive_;
    /**
     * Check active email reminder flag
     */
    BOOL hasMailActive_;
}

/**
 * Provides read-write access to the titleLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;
/**
 * Provides read-write access to the subtitleLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *subtitleLabel;
/**
 * Provides read-write access to the dayLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *dayLabel;
/**
 * Provides read-write access to the checkImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *checkImageView;
/**
 * Provides read-write access to the phoneImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *phoneImageView;
/**
 * Provides read-write access to the mailImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *mailImageView;
/**
 * Provides read-write access to the checkActive
 */
@property (nonatomic, readwrite, assign) BOOL checkActive;
/**
 * Provides read-write access to the hasPhoneActive
 */
@property (nonatomic, readwrite, assign) BOOL hasPhoneActive;
/**
 * Provides read-write access to the hasMailActive
 */
@property (nonatomic, readwrite, assign) BOOL hasMailActive;

/**
 * Creates and returns an autoreleased CheckFOCell constructed from a NIB file
 *
 * @return The autoreleased CheckFOCell constructed from a NIB file
 */
+ (CheckFOCell *)checkFOCell;


/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

@end
