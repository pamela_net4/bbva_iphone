/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTSwitchCell.h"

#import "NXT_Peru_iPhoneStyler.h"
#import "NibLoader.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Defines the NIB file name.
 */
#define NIB_FILE_NAME                                               @"NXTSwitchCell"

/**
 * Defines the cell identifier.
 */
#define CELL_IDENTIFIER                                             @"NXTSwitchCell"

/**
 * Defines the cell height.
 */
#define CELL_HEIGHT                                                 50.0f

@implementation NXTSwitchCell

#pragma mark -
#pragma mark Properties

@synthesize optionSwitch = optionSwitch_;
@synthesize textLabel = textLabel_;
@synthesize imageView = imageView_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [optionSwitch_ release];
    optionSwitch_ = nil;
    
    [textLabel_ release];
    textLabel_ = nil;
    
    [imageView_ release];
    imageView_ = nil;
    
    delegate_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [optionSwitch_ setOn:NO];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    UIView *backgroundView = [[[UIView alloc] init] autorelease];
    [[self backgroundView] setBackgroundColor:[UIColor BBVAGreyToneFiveColor]];
    [self setBackgroundView:backgroundView];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1){
        
        [optionSwitch_ setOnTintColor:[UIColor BBVABlueSpectrumColor]];
        [optionSwitch_ setTintColor: [UIColor BBVABlueSpectrumColor]];
    }
}

/*
 * Creates and returns an autoreleased NXTSwitchCell contructed from NIB file.
 */
+ (NXTSwitchCell *)nxtSwitchCell {
    
    return (NXTSwitchCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
}

/**
 * Lays out subviews.
 */
- (void)layoutSubviews {
    
    if ([imageView_ image] == nil) {
        
        [imageView_ setHidden:YES];
        
        CGRect frame = [textLabel_ frame];
        frame.origin.x = 11.0f;
        frame.size.width = 190.0f;
        [textLabel_ setFrame:frame];
        
    } else {
        
        [imageView_ setHidden:NO];
        
        CGRect frame = [textLabel_ frame];
        frame.origin.x = 54.0f;
        frame.size.width = 150.0f;
        [textLabel_ setFrame:frame];
        
    }
    
}


#pragma mark -
#pragma mark NXTSwitchCell selectors

/**
 * Action when switch state is changed.
 */
- (IBAction)switchChangedState:(id)sender {
    
    UISwitch *sw = (UISwitch *)sender;
    
    if ([sw isKindOfClass:[UISwitch class]]) {
     
        [delegate_ didSwitchCellWithIndexPath:keyCell_ turned:[sw isOn]];
        
    }
    
}

/*
 * Set delegate and index path of cell.
 */
- (void)setDelegate:(id<NXTSwitchCellDelegate>)delegate withKey:(NSInteger)key {
    
    if (delegate_ != delegate) {
        
        delegate_ = delegate;
        
    }
    
    if (keyCell_ != key) {
        
        keyCell_ = key;
        
    }
    
    [self setNeedsLayout];
    
}

#pragma mark -
#pragma mark NXTSwitchCell selectors

/*
 * Returns the cell height.
 */
+ (CGFloat)cellHeight {
    
    return CELL_HEIGHT;
    
}

/*
 * Returns the cell identifier.
 */
+ (NSString *)cellIdentifier {
    
    return CELL_IDENTIFIER;
    
}

@end
