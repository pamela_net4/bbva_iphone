//
//  FastLoanBenefitCell.m
//  NXT_Peru_iPhone
//
//  Created by Flavio Franco Tunqui on 2/23/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "FastLoanBenefitCell.h"
#import "UIColor+BBVA_Colors.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"FastLoanBenefitCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"FastLoanBenefitCell"

#define CELL_HEIGHT 45

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              14.0f

/**
 * Defines the size of the arrow
 */
#define ARROW_FONT_SIZE                                              12.0f

@implementation FastLoanBenefitCell

@synthesize iconImageView = iconImageView_;
@synthesize titleLabel = titleLabel_;
@synthesize arrowLabel = arrowLabel_;

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [iconImageView_ release];
    iconImageView_ = nil;
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [arrowLabel_ release];
    arrowLabel_ = nil;
    
    [super dealloc];
}

/**
 * Creates and returns an autoreleased CheckCell constructed from a NIB file
 *
 * @return The autoreleased CheckCell constructed from a NIB file
 */
+ (FastLoanBenefitCell *)fastLoanBenefitCell{
    
    FastLoanBenefitCell *result = (FastLoanBenefitCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;
}

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier{
    return CELL_IDENTIFIER;
}

- (void)awakeFromNib {
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:arrowLabel_ withFontSize:ARROW_FONT_SIZE color:[UIColor BBVAGrayFourToneColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Sets the action image
 *
 * @param icon The action label text
 */
- (void)setIcon:(UIImage *)icon{
    iconImageView_.image = icon;
}

/**
 * Sets the action label
 *
 * @param setTitle The action icon image
 */
- (void)setTitle:(NSString *)title{
    
    titleLabel_.text = title;
}

/**
 * Sets the action label
 *
 * @param setTitle The action icon image
 */
- (void)setArrow:(NSString *)symbol{
    
    arrowLabel_.text = symbol;
}

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    
    return CELL_HEIGHT;
}

@end