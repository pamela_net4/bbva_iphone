//
//  CheckTypeTransferCell.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 10/5/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "CheckTypeTransferCell.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"


#define NIB_FILE_NAME                                               @"CheckTypeTransferCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"CheckTypeTransferCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 92.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              14.0f

/**
 * Defines the size of the font fopr subtitle text
 */
#define TEXT_SUB_FONT_SIZE                                          12.0f


@implementation CheckTypeTransferCell


@synthesize titleLabel = titleLabel_;
@synthesize comisionLabel = comisionLabel_;
@synthesize comisionValueLabel = comisionValueLabel_;
@synthesize totalPaymentLabel = totalPaymentLabel_;
@synthesize totalPaymentValueLabel = totalPaymentValueLabel_;

@synthesize checkImageView = checkImageView_;
@synthesize checkActive = checkActive_;


#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [comisionValueLabel_ release];
    comisionValueLabel_ = nil;
    
    
    [comisionLabel_ release];
    comisionLabel_ = nil;
    
    [totalPaymentValueLabel_ release];
    totalPaymentValueLabel_ = nil;
    
    [totalPaymentLabel_ release];
    totalPaymentLabel_ = nil;
    
    
    [checkImageView_ release];
    checkImageView_ = nil;
    
    [super dealloc];
    
}
#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withBoldFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:comisionLabel_ withFontSize:TEXT_SUB_FONT_SIZE color:[UIColor BBVABlueColor]];
      [NXT_Peru_iPhoneStyler styleLabel:comisionValueLabel_ withFontSize:TEXT_SUB_FONT_SIZE color:[UIColor BBVABlueColor]];
   
    [NXT_Peru_iPhoneStyler styleLabel:totalPaymentLabel_ withBoldFontSize:TEXT_SUB_FONT_SIZE color:[UIColor BBVABlueColor]];
    [NXT_Peru_iPhoneStyler styleLabel:totalPaymentValueLabel_ withBoldFontSize:TEXT_SUB_FONT_SIZE color:[UIColor BBVABlueColor]];
    
}

/*
 * Creates and returns an autoreleased CheckAccountView constructed from a NIB file
 */
+ (CheckTypeTransferCell *)checkTypeTransferCell {
    
    CheckTypeTransferCell *result = (CheckTypeTransferCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    [self awakeFromNib];
    
    return result;
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Sets the check active
 */
- (void)setCheckActive:(BOOL)checkActive {
    
    checkActive_ = checkActive;
    
    if (checkActive) {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_SELECTED_RADIO]];
        
    } else {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_UNSELECTED_RADIO]];
        
    }
    
}

#pragma mark -
#pragma mark Cell associated information


/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;
}

/*
 * Returns the cell identifier
 */
+ (CGFloat)cellHeight {
    return CELL_HEIGHT;
}

@end
