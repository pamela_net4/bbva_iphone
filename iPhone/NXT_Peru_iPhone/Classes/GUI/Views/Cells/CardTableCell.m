
/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "CardTableCell.h"

#import "Card.h"
#import "CardTransactionsAdditionalInformation.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the CardTableCell NIB file name
 */
#define NIB_FILE_NAME                                               @"CardTableCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"CardTableCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 360.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        13.0f


#pragma mark -

/**
 * CardTableCellProvider private category
 */
@interface CardTableCellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (CardTableCellProvider *)getInstance;

/**
 * Creates and returns an autoreleased CardTableCell constructed from a NIB file
 *
 * @return The autoreleased CardTableCell constructed from a NIB file
 */
- (CardTableCell *)cardTableCell;

@end


#pragma mark -

@implementation CardTableCellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * CardTableCellProvier singleton only instance
 */
static CardTableCellProvider *cardTableCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([CardTableCellProvider class]) {
        
        if (cardTableCellProviderInstance_ == nil) {
            
            cardTableCellProviderInstance_ = [super allocWithZone:zone];
            return cardTableCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (CardTableCellProvider *)getInstance {
    
    if (cardTableCellProviderInstance_ == nil) {
        
        @synchronized([CardTableCellProvider class]) {
            
            if (cardTableCellProviderInstance_ == nil) {
                
                cardTableCellProviderInstance_ = [[CardTableCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return cardTableCellProviderInstance_;
    
}

#pragma mark -
#pragma mark CardTableCell creation

/*
 * Creates and returns aa autoreleased CardTableCell constructed from a NIB file
 */
- (CardTableCell *)cardTableCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    CardTableCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation CardTableCell

#pragma mark -
#pragma mark Properties

@synthesize table = table_;
@synthesize card = card_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
        
    [table_ release];
    table_ = nil;
    
    [card_ release];
    card_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
        
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    
    self.backgroundColor = [UIColor BBVAGreyToneFiveColor];
    table_.userInteractionEnabled = NO;
    
    [NXT_Peru_iPhoneStyler styleTableView:table_];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];

}

/*
 * Creates and returns an autoreleased CardTableCell constructed from a NIB file
 */
+ (CardTableCell *)cardTableCell {
    
    return [[CardTableCellProvider getInstance] cardTableCell];
    
}

#pragma mark -
#pragma mark UITableView methods

/**
 * Asks the data source for a cell to insert in a particular location of the table view
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NXTTableCell *result = (NXTTableCell *)[tableView dequeueReusableCellWithIdentifier:[NXTTableCell cellIdentifier]];
    
    if (result == nil) {
        result = [NXTTableCell NXTTableCell];
    }
    result.backgroundColor = [UIColor whiteColor];

    NSString *detail = @"";

    CardTransactionsAdditionalInformation *additionalInformation = card_.transactionsAdditionalInformation;
    
    if (indexPath.section == 0) {
        
        switch (indexPath.row) {
            case 0:
                result.leftText = NSLocalizedString(CLOSED_DAY_CARD_KEY, nil);
                
                NSString *closingDate = @"";
                
                if (additionalInformation != nil) {
                    
                    closingDate = additionalInformation.closingDateString;
                    
                    if ([closingDate length] == 0) {
                        
                        closingDate = @"-";
                        
                    }
                }
                                
                result.rightText = closingDate;
                [NXT_Peru_iPhoneStyler styleLabel:result.rightTextLabel withFontSize:14.0f color:[UIColor blackColor]];
                break;
                
            case 1:
                
                result.leftText = NSLocalizedString(LAST_PAYMENT_DAY_CARD_KEY, nil);
                
                NSString *lastPaymentDate = @"";
                
                if (additionalInformation != nil) {
                    
                    lastPaymentDate = additionalInformation.lastPaymentDayString;
                    
                    if ([lastPaymentDate length] == 0) {
                        
                        lastPaymentDate = @"-";
                        
                    }
                }
                
                result.rightText = lastPaymentDate;
                [NXT_Peru_iPhoneStyler styleLabel:result.rightTextLabel withFontSize:14.0f color:[UIColor blackColor]];

                break;
            case 2:
                result.leftText = NSLocalizedString(MINIMUN_PAYMENT_CARD_KEY, nil);
                                
                if (additionalInformation != nil) {
                    detail = [NSString stringWithFormat:@"%@ %@", [Tools mainCurrencySymbol], [Tools notNilString:additionalInformation.minimumPaymentMLString]];
                }
                
                result.rightText = detail;
                
                if ([[additionalInformation totalPaymentML] compare:[NSDecimalNumber zero]] == NSOrderedAscending) {
                    
                    [self.rightTextLabel setTextColor:[UIColor BBVAMagentaColor]];
                    
                } else {
                    
                    [self.rightTextLabel setTextColor:[UIColor BBVABlueSpectrumToneTwoColor]];
                    
                }
                break;
            case 3:
                result.leftText = NSLocalizedString(TOTAL_PAYMENT_CARD_KEY, nil);
                
                if (additionalInformation != nil) {
                    detail = [NSString stringWithFormat:@"%@ %@", [Tools mainCurrencySymbol], [Tools notNilString:additionalInformation.totalPaymentMLString]];
                }
                
                result.rightText = detail;
                
                if ([[additionalInformation minimumPaymentML] compare:[NSDecimalNumber zero]] == NSOrderedAscending) {
                    
                    [self.rightTextLabel setTextColor:[UIColor BBVAMagentaColor]];
                    
                } else {
                    
                    [self.rightTextLabel setTextColor:[UIColor BBVABlueSpectrumToneTwoColor]];
                    
                }
                
                break;
            case 4:
                result.leftText = NSLocalizedString(DELAY_PAYMENT_CARD_KEY, nil);
                                
                if (additionalInformation != nil) {
                    detail = [NSString stringWithFormat:@"%@ %@", [Tools mainCurrencySymbol], [Tools notNilString:additionalInformation.delayedPaymentMLString]];
                }
                
                result.rightText = detail;
                
                if ([[additionalInformation delayedPaymentML] compare:[NSDecimalNumber zero]] == NSOrderedAscending) {
                    
                    [self.rightTextLabel setTextColor:[UIColor BBVAMagentaColor]];
                    
                } else {
                    
                    [self.rightTextLabel setTextColor:[UIColor BBVABlueSpectrumToneTwoColor]];
                    
                }
                
                break;
                
        }
        
    } else {
        
        [NXT_Peru_iPhoneStyler styleLabel:result.rightTextLabel withFontSize:14.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
    
        switch (indexPath.row) {
            case 0:
                result.leftText = NSLocalizedString(MINIMUN_PAYMENT_CARD_KEY, nil);
                
                if (additionalInformation != nil) {
                    detail = [NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:CURRENCY_DOLARES_LITERAL], [Tools notNilString:additionalInformation.minimumPaymentMEString]];
                }
                
                result.rightText = detail;
                
                if ([[additionalInformation minimumPaymentME] compare:[NSDecimalNumber zero]] == NSOrderedAscending) {
                    
                    [self.rightTextLabel setTextColor:[UIColor BBVAMagentaColor]];
                    
                } else {
                    
                    [self.rightTextLabel setTextColor:[UIColor BBVABlueSpectrumToneTwoColor]];
                    
                }                
                
                break;
                
            case 1:
                result.leftText = NSLocalizedString(TOTAL_PAYMENT_CARD_KEY, nil);
                
                if (additionalInformation != nil) {
                    detail = [NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:CURRENCY_DOLARES_LITERAL], [Tools notNilString:additionalInformation.totalPaymentMEString]];
                }
                
                if ([[additionalInformation totalPaymentME] compare:[NSDecimalNumber zero]] == NSOrderedAscending) {
                    
                    [self.rightTextLabel setTextColor:[UIColor BBVAMagentaColor]];
                    
                } else {
                    
                    [self.rightTextLabel setTextColor:[UIColor BBVABlueSpectrumToneTwoColor]];
                    
                }
                
                result.rightText = detail;
                
                break;
            case 2:
                result.leftText = NSLocalizedString(DELAY_PAYMENT_CARD_KEY, nil);
                
                if (additionalInformation != nil) {
                    detail = [NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:CURRENCY_DOLARES_LITERAL], [Tools notNilString:additionalInformation.delayedPaymentMEString]];
                }
                
                if ([[additionalInformation delayedPaymentME] compare:[NSDecimalNumber zero]] == NSOrderedAscending) {
                    
                    [self.rightTextLabel setTextColor:[UIColor BBVAMagentaColor]];
                    
                } else {
                    
                    [self.rightTextLabel setTextColor:[UIColor BBVABlueSpectrumToneTwoColor]];
                    
                }
                
                result.rightText = detail;
                
                break;
        
        }
    }
    
    result.leftTextLabel.textAlignment = UITextAlignmentLeft;
    result.rightTextLabel.textAlignment = UITextAlignmentRight;
    
    [NXT_Peru_iPhoneStyler styleLabel:result.leftTextLabel withFontSize:14.0f color:[UIColor blackColor]];
    
    return result;
    
}

/**
 * Tells the delegate that the specified row is now selected
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

/**
 * Tells the data source to return the number of rows in a given section of a table view
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger result = 0;
    
    if (section == 0) {
        result = 5;
    } else {
        result = 3;
    } 
    
    return result;
    
}

/**	
 * Asks the data source to return the number of sections in the table view
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;

}

/**
 * Asks the delegate for the height to use for a row in a specified location.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 40.0f;
    
}


#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
     
    return CELL_HEIGHT;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Set card
 */
- (void)setCard:(Card *)card {

    if (card_ != card) {
        [card_ release];
        card_ = [[Card alloc] init];
        [card_ updateFrom:card];
    }
    
    [table_ reloadData];

}


@end