//
//  AliasFOCell.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 12/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "AliasFOCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"
#import "Tools.h"
#import "NibLoader.h"

#define NIB_FILE_NAME @"AliasFOCell"
#define CELL_IDENTIFIER @"AliasFOCell"
#define TEXT_FONT_SIZE 14.0f
#define MAXLENGTH 10

/**
 * Defines the cell height
 */
#define FONT_SIZE                                           14.0f

/**
 * Denifes the vertical margin size
 */
#define VERTICAL_MARGIN_SIZE                                10.0f

/**
 * Denifes the vertical margin size
 */
#define SWITCH_HEIGHT                                       27.0f

/**
 * Denifes the vertical margin size
 */
#define TXTFIELD_HEIGHT                                     31.0f

/**
 * Denifes the vertical margin size
 */
#define COMBO_HEIGHT                                        37.0f

/**
 * Denifes the vertical margin size
 */
#define BUTTON_HEIGHT                                       37.0f


@interface AliasFOCellProvider(private)

+ (AliasFOCellProvider *) getInstance;

- (AliasFOCell *) aliasFOCell;

@end



@implementation AliasFOCellProvider

@synthesize auxView = auxView_;

static AliasFOCellProvider *aliasFOCellProviderInstance_ = nil;

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([AliasFOCellProvider class]) {
        
        if (aliasFOCellProviderInstance_ == nil) {
            
            aliasFOCellProviderInstance_ = [super allocWithZone:zone];
            return aliasFOCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}


/*
 * Returns the singleton only instance
 */
+ (AliasFOCellProvider *)getInstance {
    
    if (aliasFOCellProviderInstance_ == nil) {
        
        @synchronized([AliasFOCellProvider class]) {
            
            if (aliasFOCellProviderInstance_ == nil) {
                
                aliasFOCellProviderInstance_ = [[AliasFOCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return aliasFOCellProviderInstance_;
    
}

#pragma mark -
#pragma mark MovementDetailView creation

/*
 * Creates and returns aa autoreleased SimpleHeaderView constructed from a NIB file
 */
- (AliasFOCell *) aliasFOCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    AliasFOCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}


@end



@implementation AliasFOCell

@synthesize largeAliasTextLabel = largeAliasTextLabel_;
@synthesize largeAliasTextField = largeAliasTextField_;
@synthesize shortAliasTextLabel = shortAliasTextLabel_;
@synthesize shortAliasTextField = shortAliasTextField_;
@synthesize separator = separator_;
@synthesize suggestionLinkButton = suggestionLinkButton_;
@synthesize suggestionLineView = suggestionLineView_;
@synthesize delegate = delegate_;

- (void) dealloc
{
    [largeAliasTextField_ release];
    largeAliasTextField_ = nil;
    
    [largeAliasTextLabel_ release];
    largeAliasTextLabel_ = nil;
    
    [shortAliasTextLabel_ release];
    shortAliasTextLabel_ = nil;
    
    [shortAliasTextField_ release];
    shortAliasTextField_ = nil;
    
    [separator_ release];
    
    [suggestionLinkButton_ release];
    suggestionLinkButton_ = nil;
    
    [suggestionLineView_ release];
    suggestionLineView_ = nil;
    
    [super dealloc];
}

- (void) awakeFromNib {

    [super awakeFromNib];
    
    
    self.backgroundColor = [UIColor BBVAGreyToneFiveColor];
    
    separator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    
    [NXT_Peru_iPhoneStyler styleButton:suggestionLinkButton_ withBoldFont:FALSE fontSize: FONT_SIZE color: [UIColor BBVABlueColor] disabledColor: [UIColor BBVAWhiteColor] andShadowColor: [self backgroundColor]];
    
    [suggestionLineView_ setBackgroundColor: [UIColor BBVABlueColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:largeAliasTextLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlackColor]];
    largeAliasTextLabel_.numberOfLines = 0;
    
    [largeAliasTextLabel_ setText:@"Descripción para recordar su pago"];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:largeAliasTextField_ withFontSize:TEXT_FONT_SIZE andColor:[UIColor BBVAGreyColor]];
    [largeAliasTextField_ setDelegate:self];
//    largeAliasTextField_.inputAccessoryView=popButtonsView_;
    
    [NXT_Peru_iPhoneStyler styleLabel:shortAliasTextLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlackColor]];
    shortAliasTextLabel_.numberOfLines = 0;
    [shortAliasTextField_ setDelegate:self];
    
    [NXT_Peru_iPhoneStyler styleMokTextField:shortAliasTextField_ withFontSize:TEXT_FONT_SIZE andColor:[UIColor BBVAGreyColor]];
    
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == largeAliasTextField_) {
    
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= 30 || returnKey;
        
    } else if(textField == shortAliasTextField_){
    
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= 10 || returnKey;
        
    }
    
    return YES;
    
}

+ (AliasFOCell *) aliasFOCell {
    
    return [[AliasFOCellProvider getInstance] aliasFOCell];
}

/*
 * Returns the width of the view
 */
+ (CGFloat)width {
    return 320.0f;
}

/*
 * Returns the height of the view
 */
+ (CGFloat)height {
    return 124.0f;
}


- (IBAction)suggestionLinkButtonTapped:(id)sender {
    
    
    [self.delegate suggestionLinkButtonTapped];
    
}

/**
 * Asks the delegate if the text field should process the pressing of the return button. Switches from the user to the password text field or
 * dismisses the keyboard, depending on the text field currently active
 *
 * @param textField The text field whose return button was pressed
 * @return Always NO, as the delegate manages the event
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == largeAliasTextField_) {
        
        [shortAliasTextField_ becomeFirstResponder];
        
    } else {
        
        [shortAliasTextField_ resignFirstResponder];
        
    }
    
    return NO;
    
}

@end
