/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class CardTableCell;
@class Card;

/**
 * Provider to obtain a CardTableCell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardTableCellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary CardTableCell to create it from a NIB file
     */
    CardTableCell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary CardTableCell
 */
@property (nonatomic, readwrite, assign) IBOutlet CardTableCell *auxView;

@end


/**
 * GlobalPosition cell. It's a subclass of NXTTableCell but it has two labels on the right instead of one.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardTableCell : NXTTableCell <UITableViewDataSource, UITableViewDelegate>{
@private    
    
    /**
     * Table
     */
    UITableView *table_;
    
    /**
     * Information array
     */
    Card *card_;
    
}

/**
 * Provides read-write access to the topLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UITableView *table;

/**
 * Provides read-write access to the middleLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) Card *card;

/**
 * Creates and returns an autoreleased CardTableCell constructed from a NIB file
 *
 * @return The autoreleased CardTableCell constructed from a NIB file
 */
+ (CardTableCell *)cardTableCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;


@end