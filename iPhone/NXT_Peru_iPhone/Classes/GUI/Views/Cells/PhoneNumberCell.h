//
//  PhoneNumberCell.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 9/26/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "NXTTableCell.h"
#import "NXTTextField.h"

@protocol PhoneNumberCellDelegate

/*
 * Add First Contact Button Tapped
 */
- (void)addFirstContactHasBeenTapped;

@end

@interface PhoneNumberCell : NXTTableCell
{
@private
    
    /**
     * Background image
     */
    UIImageView *backgroundImageView_;
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Subtitle label
     */
    UILabel *subTitleLabel_;
    
    /**
     * First text field
     */
    NXTTextField *firstTextField_;
    
    /**
     * First add button
     */
    UIButton *firstAddButton_;
    
    /**
     * Add contact image
     */
    UIImage *addContactImage_;
    
    /**
     * Delete contact image
     */
    UIImage *deleteContactImage_;
    
    /**
     * phone Cell Delegate
     */
    id<PhoneNumberCellDelegate> delegate_;
}

/**
 * Provides readwrite access to the backgroundImageView and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *backgroundImageView;

/**
 * Provides readwrite access to the phone title
 */
@property (retain, nonatomic) IBOutlet UILabel *lblTituloPhoneNumber;

/**
 * Provides readwrite access to the optianl label
 */
@property (retain, nonatomic) IBOutlet UILabel *lblIndication;

/**
 * Provides readwrite access to the contacts button
 */
@property (retain, nonatomic) IBOutlet UIButton *btnContacts;

/**
 * Provides readwrite access to the phone number text field
 */
@property (retain, nonatomic) IBOutlet NXTTextField *phoneNumberTextField;

/**
 * Provides readwrite access to the delegate
 */
@property (nonatomic, readwrite, assign) id<PhoneNumberCellDelegate> delegate;


+ (PhoneNumberCell *)phoneNumberCell;

/**
 * First add button tapped
 */
- (IBAction)firstAddButtonTapped;

@end
