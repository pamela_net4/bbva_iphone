/*
 * Copyright (c) 2013 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * Cell to show an information text.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface InformationTextCell : UITableViewCell {
    
@private
    
    /**
     * Information label.
     */
    UILabel *informationLabel_;
    
}


/**
 * Provides read-write access to the information label and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *informationLabel;

/**
 * Provides read-write access to the information text.
 */
@property (nonatomic, readwrite, copy) NSString *informationText;


/**
 * Creates and return an autoreleased InformationTextCell constructed from a NIB file
 *
 * @return AddressCell The autoreleased InformationTextCell constructed from a NIB file
 */
+ (InformationTextCell *)informationTextCell;

/**
 * Returns the cell height for the given text and width.
 *
 * @param text The text to display.
 * @param width The cell width.
 * @return The cell height for the given width.
 */
+ (CGFloat)cellHeightForText:(NSString *)text
                       width:(CGFloat)width;

/**
 * Returns the cell identifier.
 *
 * @return The cell identifier.
 */
+ (NSString *)cellIdentifier;

@end
