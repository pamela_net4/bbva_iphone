/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "SingletonBase.h"


//Forward declarations
@class BlueButtonCell;


/**
 * Provider to obtain a BlueButtonCell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BlueButtonCellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary BlueButtonCell to create it from a NIB file
     */
    BlueButtonCell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary BlueButtonCell
 */
@property (nonatomic, readwrite, assign) IBOutlet BlueButtonCell *auxView;

@end


/**
 * Cell containing a blue button (e.g. the last cell in the login view table). It defines a button text, the action to perform (a selector) and a target instance
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BlueButtonCell : UITableViewCell {
    
@private
    
    /**
     * Cell blue button
     */
    UIButton *blueButton_;
    
    /**
     * Separator image view. It can be displayed or hidden (initial state is hidden)
     */
    UIImageView *separatorImageView_;

}

/**
 * Provides read-write access to the cell blue button and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *blueButton;

/**
 * Provides read-write access to the separator image view and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separatorImageView;


/**
 * Sets the button title
 * 
 * @param aTitle The button title
 */
- (void)setButtonTitle:(NSString *)aTitle;

/**
 * Adds the target and action for the cell button
 *
 * @param aTarget The instance to respond when the button is clicked
 * @param anAction The action (selector) to invoke when the button is clicked. It cannot be nil
 */
- (void)addTarget:(id)aTarget action:(SEL)anAction;

/**
 * Shows or hides the separator line
 *
 * @param visible YES to show the separator line, NO to hide it
 */
- (void)setSeparatorLineVisible:(BOOL)visible;

/**
 * Creates and returns an autoreleased BlueButtonCell constructed from a NIB file
 *
 * @return The autoreleased BlueButtonCell constructed from a NIB file
 */
+ (BlueButtonCell *)blueButtonCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end
