//
//  AliasFOCell.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 12/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"
#import "MOKTextField.h"

@class AliasFOCell;

@protocol AliasFOCellDelegate


-(void) suggestionLinkButtonTapped;

@end


@interface AliasFOCellProvider : SingletonBase {

    @private
    
    AliasFOCell *auxView_;
}

@property (nonatomic, readwrite, assign) IBOutlet AliasFOCell *auxView;

@end




@interface AliasFOCell : UIView <UITextFieldDelegate> {

@private
    
    UILabel *largeAliasTextLabel_;
    
    MOKTextField *largeAliasTextField_;
    
    UILabel *shortAliasTextLabel_;
    
    MOKTextField *shortAliasTextField_;
    
    UIButton *suggestionLinkButton_;
    
    UIView *suggestionLineView_;
    
    UIImageView *separator_;
    
    id<AliasFOCellDelegate> delegate_;

}

@property (nonatomic, readwrite, assign) id<AliasFOCellDelegate> delegate;

@property (retain, nonatomic) IBOutlet UIView *suggestionLineView;

@property (retain, nonatomic) IBOutlet UIButton *suggestionLinkButton;

@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *largeAliasTextLabel;

@property (nonatomic, readwrite, retain) IBOutlet MOKTextField *largeAliasTextField;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *shortAliasTextLabel;

@property (nonatomic, readwrite, retain) IBOutlet MOKTextField *shortAliasTextField;


- (IBAction)suggestionLinkButtonTapped:(id)sender;


+ (AliasFOCell *) aliasFOCell;

/**
 * Returns the width of the view
 */
+ (CGFloat)width;

/**
 * Returns the height of the view
 */
+ (CGFloat)height;


@end
