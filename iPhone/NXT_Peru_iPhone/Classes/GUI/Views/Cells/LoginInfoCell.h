/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "CheckView.h"
#import "NXTComboButtonLogin.h"


//Forward declarations
@class LoginInfoCell;
@class NXTTextField;
@class MOKUILimitedLengthTextField;
@class CheckView;
@class NXTComboButtonLogin;

/**
 * Provider to obtain a LoginInfoCell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface LoginInfoCellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary LoginInfoCell to create it from a NIB file
     */
    LoginInfoCell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary LoginInfoCell
 */
@property (nonatomic, readwrite, assign) IBOutlet LoginInfoCell *auxView;

@end


/**
 * Cell containing the user and password fields neede to login, plus the login button and a helper button in case you forgot your password
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface LoginInfoCell : UITableViewCell <UITextFieldDelegate> {

@private
    
    /**
     * User text label
     */
    UILabel *userLabel_;
    
    /**
     * User text field to edit the user name
     */
    MOKUILimitedLengthTextField *userTextField_;
    
    /**
     * Card frequent label
     */
    UILabel *cardfrequentLabel_;
    
    /**
     * Target check combo
     */
    NXTComboButtonLogin *frequentCardCombo_;
    
    /**
     * Target check field
     */
    CheckView *frequentCardCheck_;
    
    /**
     * Alias text field to edit the alias target
     */
    MOKUILimitedLengthTextField *aliasTextField_;
    
    /**
     * Password text label
     */
    UILabel *passwordLabel_;
    
    /**
     * Password text field to edit the user password
     */
    MOKUILimitedLengthTextField *passwordTextField_;
    
    /**
     * Login button
     */
    UIButton *loginButton_;
    
    /**
     * The information login.
     */
    UILabel *loginInformationLabel_;
    
    /**
     * The suscribed button.
     */
    UIButton *suscribedButton_;
    
    /**
     * Forgot password helper button
     */
    UIButton *forgotPasswordHelperButton_;
    
    /**
     * Frequent target helper button
     */
    UIButton *frequentCardHelperButton_;
    
    /**
     * Cell separator image
     */
    UIImageView *cellSeparatorImage_;
    
    /**
     * View to hide cell in tableView.
     */
    UIView *hideCellView_;
    
    /*
     * height content view
     */
    CGFloat heightContentView_;
    
}

/**
 * Provides read-write access to the user text label and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *userLabel;

/**
 * Provides read-write access to the user text field to edit the user name and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKUILimitedLengthTextField *userTextField;

/**
 * Provides read-write access to the card frequent text label and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *cardfrequentLabel;

/**
 * Provides read-write access to the card combo field to edit the card and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTComboButtonLogin *frequentCardCombo;

/**
 * Provides read-write access to the card check field to edit the card and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet CheckView *frequentCardCheck;


/**
 * Provides read-write access to the alias text field to edit the user name and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKUILimitedLengthTextField *aliasTextField;


/**
 * Provides read-write access to the password text label and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *passwordLabel;

/**
 * Provides read-write access to the password text field to edit the user password and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet MOKUILimitedLengthTextField *passwordTextField;

/**
 * Provides read-write access to the login button and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *loginButton;

/**
 * Provides read-write access to loginInformationLabel.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *loginInformationLabel;

/**
 * Provides read-write access to suscribedButton.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *suscribedButton;

/**
 * Provides read-write access to the forgot password helper button and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *forgotPasswordHelperButton;

/**
 * Provides read-write access to the forgot frequent target helper button and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *frequentCardHelperButton;

/**
 * Provides read-write access to the cell separator image and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *cellSeparatorImage;

/**
 * Provides read-write access to hideCellView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *hideCellView;


/**
 * Adds the target and action for the login button
 *
 * @param aTarget The instance to respond when the login button is clicked
 * @param anAction The action (selector) to invoke when the login button is clicked. It cannot be nil
 */
- (void)addLoginTarget:(id)aTarget action:(SEL)anAction;

/**
 * Adds the target and action for the forgot password button
 *
 * @param aTarget The instance to respond when the forgot password button is clicked
 * @param anAction The action (selector) to invoke when the forgot password button is clicked. It cannot be nil
 */
- (void)addForgotPasswordTarget:(id)aTarget action:(SEL)anAction;

/**
 * Adds the target and action for the suscribe button.
 *
 * @param aTarget: The instance to respond when the forgot password button is clicked.
 * @param anAction: The action (selector) to invoke when the suscribe button is clicked. It cannot be nil.
 */
- (void)addSuscribeTarget:(id)aTarget action:(SEL)anAction;

/**
 * Adds the target and action for the frequent card helper button.
 *
 * @param aTarget: The instance to respond when the forgot password button is clicked.
 * @param anAction: The action (selector) to invoke when the frequent card helper button is clicked. It cannot be nil.
 */
- (void)addFrequentCardCheckTarget:(id)aTarget action:(SEL)anAction;

/**
 * Adds the target and action for the frequent card combo button.
 *
 * @param aTarget: The instance to respond when the forgot password button is clicked.
 * @param anAction: The action (selector) to invoke when the frequent card combo button is clicked. It cannot be nil.
 */
- (void)addFrequentCardComboTarget:(id)aTarget action:(SEL)anAction;


/**
 * Returns the user name introduced inside the user text field
 *
 * @return The user name inside the user name text field
 */
- (NSString *)userName;

/**
 * Returns the password introduced inside the paswword text field
 *
 * @return The password inside the password text field
 */
- (NSString *)password;

/**
 * Dismisses the keyboard
 */
- (void)dismissKeyboard;

/**
 * Creates and returns an autoreleased IconActionCell constructed from a NIB file
 *
 * @return The autoreleased IconActionCell constructed from a NIB file
 */
+ (LoginInfoCell *)loginInfoCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
- (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

/**
 * Set the cell visible with animation.
 *
 * @param visible: If the cell turn to visible or hide.
 */
- (void)setCellVisible:(BOOL)visible;

-(void) updateComponents:(NSMutableArray *) frequentCards stateAdd:(BOOL)add;

-(NSMutableArray*)updateEditableViews:(NSMutableArray *) editableViews;


@end
