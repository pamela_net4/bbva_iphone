/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "CardTransactionCell.h"
#import "CardTransaction.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"CardTransactionCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"CardTransactionCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 57.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                      12.0f


#pragma mark -

/**
 * CardTransactionCellProvider private category
 */
@interface CardTransactionCellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (CardTransactionCellProvider *)getInstance;

/**
 * Creates and returns an autoreleased CardTransactionCell constructed from a NIB file
 *
 * @return The autoreleased CardTransactionCell constructed from a NIB file
 */
- (CardTransactionCell *)cardTransactionCell;

@end


#pragma mark -

@implementation CardTransactionCellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * CardTransactionCellProvier singleton only instance
 */
static CardTransactionCellProvider *cardTransactionCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([CardTransactionCellProvider class]) {
        
        if (cardTransactionCellProviderInstance_ == nil) {
            
            cardTransactionCellProviderInstance_ = [super allocWithZone:zone];
            return cardTransactionCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (CardTransactionCellProvider *)getInstance {
    
    if (cardTransactionCellProviderInstance_ == nil) {
        
        @synchronized([CardTransactionCellProvider class]) {
            
            if (cardTransactionCellProviderInstance_ == nil) {
                
                cardTransactionCellProviderInstance_ = [[CardTransactionCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return cardTransactionCellProviderInstance_;
    
}

#pragma mark -
#pragma mark CardTransactionCell creation

/*
 * Creates and returns aa autoreleased CardTransactionCell constructed from a NIB file
 */
- (CardTransactionCell *)cardTransactionCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    CardTransactionCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation CardTransactionCell

#pragma mark -
#pragma mark Properties

@synthesize bottomLeftTextLabel = bottomLeftTextLabel_;
@dynamic cardTransaction;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [bottomLeftTextLabel_ release];
    bottomLeftTextLabel_ = nil;
    
    [cardTransaction_ release];
    cardTransaction_ = nil;
        
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = YES;
    self.showSeparator = YES;

    [NXT_Peru_iPhoneStyler styleLabel:self.leftTextLabel withFontSize:TEXT_FONT_SMALL_SIZE color:[UIColor blackColor]];
    [NXT_Peru_iPhoneStyler styleLabel:bottomLeftTextLabel_ withFontSize:TEXT_FONT_SMALLER_SIZE color:[UIColor BBVAGreyColor]];

}

/*
 * Creates and returns an autoreleased CardTransactionCell constructed from a NIB file
 */
+ (CardTransactionCell *)cardTransactionCell {
    
    return [[CardTransactionCellProvider getInstance] cardTransactionCell];
    
}

#pragma mark -
#pragma mark Getters and setters

/*
 * Returns the Card
 */
- (CardTransaction *)cardTransaction {
    return cardTransaction_;
}

/**
 * Sets the CardTransaction and sets the info into the cell
 */
- (void)setCardTransaction:(CardTransaction *)cardTransaction {
    
    if (cardTransaction_ != cardTransaction) {
        
        [cardTransaction retain];
        [cardTransaction_ release];
        cardTransaction_ = cardTransaction;
        
    }
    
    NSString *currency = [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:[cardTransaction currency]];
    
    self.leftText = [cardTransaction concept];
    self.rightText = [NSString stringWithFormat:@"%@ %@", currency, [cardTransaction amountString]];
    
    if ([[cardTransaction amount] compare:[NSDecimalNumber zero]] == NSOrderedAscending) {
        
        [self.rightTextLabel setTextColor:[UIColor BBVAMagentaColor]];
        
    } else {
        
        [self.rightTextLabel setTextColor:[UIColor BBVABlueSpectrumToneTwoColor]];
        
    }

        
    self.bottomLeftTextLabel.text = [cardTransaction operationDateString];
    
}

#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
    return CELL_HEIGHT;
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}

@end