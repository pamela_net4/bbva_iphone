/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class CCICell;

/**
 * Provider to obtain a CCICell created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CCICellProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary CCICell to create it from a NIB file
     */
    CCICell *auxView_;
    
}

/**
 * Provides read-write access to the auxililary CCICell
 */
@property (nonatomic, readwrite, assign) IBOutlet CCICell *auxView;

@end


/**
 * GlobalPosition cell. It's a subclass of NXTTableCell but it has two labels on the right instead of one.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CCICell : NXTTableCell {
@private    
    /**
     * Top label
     */
    UILabel *topLabel_;
    
    /**
     * Middle label
     */
    UILabel *middleLabel_;
    
    /**
     * Bottom label
     */
    UILabel *bottomLabel_;
    
}

/**
 * Provides read-write access to the topLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *topLabel;

/**
 * Provides read-write access to the middleLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *middleLabel;

/**
 * Provides read-write access to the bottomLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *bottomLabel;

/**
 * Provides readwrite access to the topText
 */
@property (nonatomic, readwrite, copy) NSString *topText;

/**
 * Provides readwrite access to the middleText
 */
@property (nonatomic, readwrite, copy) NSString *middleText;

/**
 * Provides readwrite access to the bottomText
 */
@property (nonatomic, readwrite, copy) NSString *bottomText;

/**
 * Creates and returns an autoreleased CCICell constructed from a NIB file
 *
 * @return The autoreleased CCICell constructed from a NIB file
 */
+ (CCICell *)cciCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;


@end