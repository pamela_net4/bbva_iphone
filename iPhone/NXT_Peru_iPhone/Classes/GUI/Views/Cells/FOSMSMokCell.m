//
//  FOSMSMokCell.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 17/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOSMSMokCell.h"
#import "NXTTableCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"
#import "StringKeys.h"
#import "NibLoader.h"

NSString *const kFOSMSMokCellNibFileName = @"FOSMSMokCell";

NSString *const kFOSMSCellIdentifier = @"FOSMSMokCell";

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f

/**
 * Defines the cell height
 */
#define FONT_SIZE                                           14.0f

/**
 * Denifes the vertical margin size
 */
#define VERTICAL_MARGIN_SIZE                                10.0f

/**
 * Denifes the vertical margin size
 */
#define SWITCH_HEIGHT                                       27.0f

/**
 * Denifes the vertical margin size
 */
#define TXTFIELD_HEIGHT                                     31.0f

/**
 * Denifes the vertical margin size
 */
#define TEXTVIEW_HEIGHT                                     93.0f

/**
 * Denifes the vertical margin size
 */
#define BUTTON_HEIGHT                                       37.0f



@implementation FOSMSMokCell


@synthesize backgroundImageView = backgroundImageView_;
@synthesize headerLabel = headerLabel_;
@synthesize titleLabel = titleLabel_;
@synthesize smsSwitch = smsSwitch_;
@synthesize delegate = delegate_;
@synthesize smsComboButton = smsComboButton_;
@synthesize smsArray = smsArray_;
@synthesize switchView = switchView_;

- (void)dealloc
{
    [backgroundImageView_ release];
    backgroundImageView_ = nil;
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [smsSwitch_ release];
    smsSwitch_ = nil;
    
    [smsComboButton_ release];
    smsComboButton_ = nil;
    
    [headerLabel_ release];
    headerLabel_ = nil;
    
    [switchView_ release];
    switchView_ = nil;
    
    [super dealloc];
}

-(void)awakeFromNib{

    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.showDisclosureArrow = NO;
    self.showSeparator = NO;
    
    [switchView_ setBackgroundColor: [UIColor clearColor]];
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
    [NXT_Peru_iPhoneStyler styleLabel:headerLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [headerLabel_ setText:NSLocalizedString(NOTIFICATION_TYPE_TEXT_KEY, nil)];
    
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:FONT_SIZE color: [UIColor BBVABlackColor]];
    [titleLabel_ setText:NSLocalizedString(SEND_PHONE_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleAccountSelectionButton: smsComboButton_];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1){
        
        [smsSwitch_ setOnTintColor:[UIColor BBVABlueSpectrumColor]];
        [smsSwitch_ setTintColor: [UIColor BBVABlueSpectrumColor]];
    }
    [smsSwitch_ setOn:NO];
}


+ (FOSMSMokCell *) FOSMSMokCell {

    FOSMSMokCell *result = (FOSMSMokCell *)[NibLoader loadObjectFromNIBFile:kFOSMSMokCellNibFileName];
    
    [result awakeFromNib];
    
    return result;
}

+ (NSString *) cellIdentifier {

    return kFOSMSCellIdentifier;
    
}

+ (CGFloat) cellHeightForFirstAddOn:(BOOL)firstAddOn {

    CGFloat result = 90.0f;
    
    if (firstAddOn) {
        result = 150.0f;
    }
    
    return result;
}

- (IBAction) switchButtonTapped {

    [delegate_ smsSwitchButtonHasBeenTapped: [smsSwitch_ isOn]];
    
}

- (void)setConfigurationForFirstAddOn:(BOOL)firstAddOn {
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
    CGRect frame;
    CGFloat yPos;
    
    frame = backgroundImageView_.frame;
    frame.origin.y = VERTICAL_MARGIN_SIZE;
    frame.size.height = 60.0f;
    backgroundImageView_.frame = frame;
    yPos = frame.origin.y + frame.size.height;
    
    self.smsComboButton.hidden = YES;
    
    if (firstAddOn) {
        
        self.smsComboButton.hidden = NO;
        
        yPos += VERTICAL_MARGIN_SIZE;
        frame = backgroundImageView_.frame;
        frame.size.height = 115.0f;//yPos - backgroundImageView_.frame.origin.y;
        backgroundImageView_.frame = frame;
        
    }
}

-(void) setSmsArray:(NSArray *)smsArray {

    if (smsArray_ == nil) {
        
        smsArray_ = [[NSMutableArray alloc] init];
        
    }
    [smsArray_ removeAllObjects];
    [smsArray_ addObjectsFromArray:smsArray];
    
    NSMutableArray *array = [NSMutableArray array];
    
    for (NSString *email in smsArray) {
        
        [array addObject:email.description];
        
    }
    [smsComboButton_ setOptionStringList:array];
    [smsComboButton_ setSelectedIndex:0];
    [array removeAllObjects];
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
}

@end
