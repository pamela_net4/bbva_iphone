
/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PaymentCell.h"

#import "Card.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the PaymentCell NIB file name
 */
#define NIB_FILE_NAME                                               @"PaymentCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"PaymentCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                                 50.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              14.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        13.0f


#pragma mark -

/**
 * PaymentCellProvider private category
 */
@interface PaymentCellProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (PaymentCellProvider *)getInstance;

/**
 * Creates and returns an autoreleased PaymentCell constructed from a NIB file
 *
 * @return The autoreleased PaymentCell constructed from a NIB file
 */
- (PaymentCell *)paymentCell;

@end


#pragma mark -

@implementation PaymentCellProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * PaymentCellProvier singleton only instance
 */
static PaymentCellProvider *paymentCellProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([PaymentCellProvider class]) {
        
        if (paymentCellProviderInstance_ == nil) {
            
            paymentCellProviderInstance_ = [super allocWithZone:zone];
            return paymentCellProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (PaymentCellProvider *)getInstance {
    
    if (paymentCellProviderInstance_ == nil) {
        
        @synchronized([PaymentCellProvider class]) {
            
            if (paymentCellProviderInstance_ == nil) {
                
                paymentCellProviderInstance_ = [[PaymentCellProvider alloc] init];
                
            }
            
        }
        
    }
    
    return paymentCellProviderInstance_;
    
}

#pragma mark -
#pragma mark PaymentCell creation

/*
 * Creates and returns aa autoreleased PaymentCell constructed from a NIB file
 */
- (PaymentCell *)paymentCell {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    PaymentCell *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation PaymentCell

#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
        
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
        
    self.showDisclosureArrow = YES;
    self.showSeparator = YES;
    
    self.backgroundColor = [UIColor clearColor];

	[NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:16.0f color:[UIColor BBVABlueColor]];
}

/*
 * Creates and returns an autoreleased PaymentCell constructed from a NIB file
 */
+ (PaymentCell *)paymentCell {
    
    return [[PaymentCellProvider getInstance] paymentCell];
    
}


#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
     
    return CELL_HEIGHT;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;    
}



@end