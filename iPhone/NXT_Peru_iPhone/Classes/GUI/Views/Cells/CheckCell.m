//
//  CheckCell.m
//  NXT_Peru_iPhone
//
//  Created by Luis Trujillo Osorio on 26/11/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "CheckCell.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKStringListSelectionButton.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the TransactionCell NIB file name
 */
#define NIB_FILE_NAME                                               @"CheckCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"CheckCell"

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              12.0f


#pragma mark -

@implementation CheckCell

#pragma mark -
#pragma mark Properties

@synthesize topTextLabel = topTextLabel_;
@synthesize checkImageView = checkImageView_;
@synthesize checkActive = checkActive_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [topTextLabel_ release];
    topTextLabel_ = nil;
    
    [checkImageView_ release];
    checkImageView_ = nil;

    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.showDisclosureArrow = NO;
    self.showSeparator = YES;
    [self setBackgroundColor:[UIColor whiteColor]];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [NXT_Peru_iPhoneStyler styleLabel:topTextLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor grayColor]];
    [topTextLabel_ setText:NSLocalizedString(SHARE_SEAL_TEXT_KEY, nil)];
}

/*
 * Creates and returns an autoreleased CheckComboCell constructed from a NIB file
 */
+ (CheckCell *)checkCell {
    
    CheckCell *result = (CheckCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Sets the Seal
 */
- (void)setCheckActive:(BOOL)checkActive {
    
    checkActive_ = checkActive;
    
    if (checkActive) {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_SELECTED_RADIO]];
        
    } else {
        
        [checkImageView_ setImage:[[ImagesCache getInstance] imageNamed:IMG_UNSELECTED_RADIO]];
        
    }
    
}

#pragma mark -
#pragma mark Cell associated information


/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    return CELL_IDENTIFIER;
}

@end