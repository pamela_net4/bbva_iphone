/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "ScheduleCell.h"
 
#import "ATMList.h"
#import "BranchesList.h"
#import "Constants.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NibLoader.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"ScheduleCell"

/**
 * Define the cell identifier
 */
#define CELL_IDENTIFIER                                             @"ScheduleCell"

/**
 * Define the cell height
 */
#define CELL_HEIGHT                                                 115.0f

@interface ScheduleCell (Private)

#pragma mark -
#pragma mark Private methods

/**
 * Toggle visible labels except open24Hours label, this label is visible instead of other labels.
 *
 * @param hidden: If YES all labels turn visible less open24Hours label. NO otherwise.
 * @private
 */
- (void)toggleVisibleLabelsSetHidden:(BOOL)hidden;

@end

@implementation ScheduleCell

#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;
@synthesize otherLabel = otherLabel_;
@synthesize weekLabel = weekLabel_;
@synthesize weekHoursLabel = weekHoursLabel_;
@synthesize saturdayLabel = saturdayLabel_;
@synthesize saturdayHoursLabel = saturdayHoursLabel_;
@synthesize sundayLabel = sundayLabel_;
@synthesize sundayHoursLabel = sundayHoursLabel_;
@synthesize poi = poi_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [otherLabel_ release];
    otherLabel_ = nil;
    
    [weekLabel_ release];
    weekLabel_ = nil;
    
    [weekHoursLabel_ release];
    weekHoursLabel_ = nil;
    
    [saturdayLabel_ release];
    saturdayLabel_ = nil;
    
    [saturdayHoursLabel_ release];
    saturdayHoursLabel_ = nil;
    
    [sundayLabel_ release];
    sundayLabel_ = nil;
    
    [sundayHoursLabel_ release];
    sundayHoursLabel_ = nil;
    
    [poi_ release];
    poi_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark View lifecycle

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_
                         withFontSize:15.0f
                                color:[UIColor BBVACoolGreyColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:weekLabel_
                         withFontSize:14.0f
                                color:[UIColor BBVAGreyToneOneColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:saturdayLabel_
                         withFontSize:14.0f
                                color:[UIColor BBVAGreyToneOneColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:sundayLabel_
                         withFontSize:14.0f
                                color:[UIColor BBVAGreyToneOneColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:weekHoursLabel_
                         withFontSize:14.0f
                                color:[UIColor BBVAGreyToneOneColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:saturdayHoursLabel_
                         withFontSize:14.0f
                                color:[UIColor BBVAGreyToneOneColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:sundayHoursLabel_
                         withFontSize:14.0f
                                color:[UIColor BBVAGreyToneOneColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:otherLabel_
                         withFontSize:14.0f
                                color:[UIColor BBVAGreyToneOneColor]];
    
    [titleLabel_ setText:NSLocalizedString(SCHEDULE_TITLE_KEY, nil)];
    [weekLabel_ setText:NSLocalizedString(SCHEDULE_WEEK_TITLE_KEY, nil)];
    [saturdayLabel_ setText:NSLocalizedString(SCHEDULE_SATURDAY_TITLE_KEY, nil)];
    [sundayLabel_ setText:NSLocalizedString(SCHEDULE_SUNDAY_TITLE_KEY, nil)];
    
    [weekHoursLabel_ setText:[Tools notNilString:[poi_ hoursMf] byDefaylt:@"     -     "]];
    [saturdayHoursLabel_ setText:[Tools notNilString:[poi_ hoursSat] byDefaylt:@"     -     "]];
    [sundayHoursLabel_ setText:[Tools notNilString:[poi_ hoursSun] byDefaylt:@"     -     "]];
    
    [otherLabel_ setHidden:YES];
    [otherLabel_ setNumberOfLines:0];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates and returns an autoreleased ScheduleCell constructed from a NIB file.
 */
+ (ScheduleCell *)scheduleCell {
    
    ScheduleCell *result = (ScheduleCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [self awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark ScheduleCell private selecotrs

/*
 * Toggle visible labels except open24Hours label, this label is visible instead of other labels.
 */
- (void)toggleVisibleLabelsSetHidden:(BOOL)hidden {

    [weekLabel_ setHidden:hidden];
    [weekHoursLabel_ setHidden:hidden];
    
    [saturdayLabel_ setHidden:hidden];
    [saturdayHoursLabel_ setHidden:hidden];
    
    [sundayLabel_ setHidden:hidden];
    [sundayHoursLabel_ setHidden:hidden];
    
    [otherLabel_ setHidden:!hidden];
    
}

#pragma mark -
#pragma mark ScheduleCell selectors

/*
 * Returns the cell height.
 */
+ (CGFloat)cellHeight {
    
    return CELL_HEIGHT;
    
}

/*
 * Returns the cell identifier.
 */
+ (NSString *)cellIdentifier {
    
    return CELL_IDENTIFIER;
    
}

/**
 * Set the poi_ value to another value.
 *
 * @param poi: Another value to poi_.
 */
- (void)setPoi:(ATMData *)poi {
    
    if (poi_ != poi) {
        
        [poi_ release];
        poi_ = [poi retain];
        
    }
    
    // Display correct information depending category poi's.
    switch ([poi_ category]) {
            
        case atmType:
        case atmBCType:
        case atmInterbankType:
        case atmScotiabankType:
            
            [self toggleVisibleLabelsSetHidden:YES];
            
            [otherLabel_ setText:NSLocalizedString(SCHEDULE_24_TITLE_KEY, nil)];
            
            break;
            
        case branchType:
        case expressAgentPlusType:
            
            [weekHoursLabel_ setText:[poi_ hoursMf]];
            [saturdayHoursLabel_ setText:[poi_ hoursSat]];
            [sundayHoursLabel_ setText:[poi_ hoursSun]];
            
            [self toggleVisibleLabelsSetHidden:NO];
            
            break;
            
        case expressAgentType:
        case kasnetType:
        case multifacilType:
            
            [self toggleVisibleLabelsSetHidden:YES];
            
            [otherLabel_ setText:NSLocalizedString(AGENT_SCHEDULE_TITLE_KEY, nil)];
            
            break;
            
    }
    
}

@end
