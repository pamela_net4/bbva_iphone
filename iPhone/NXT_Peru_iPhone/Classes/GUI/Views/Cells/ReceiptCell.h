/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"
#import "PaymentBaseProcess.h"

@class Payment;

/**
 * Receipt cell. It's a subclass of NXTTableCell.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ReceiptCell : NXTTableCell {
@private  
    
    /**
     * Title left label
     */
    UILabel *titleLeftLabel_;
    
    /**
     * Title right label
     */
    UILabel *titleRightLabel_;
    
    /**
     * First line left label
     */
    UILabel *firstLineLeftLabel_;
    
    /**
     * First line right label
     */
    UILabel *firstLineRightLabel_;
    
    /**
     * Second line left label
     */
    UILabel *secondLineLeftLabel_;
    
    /**
     * Second line right label
     */
    UILabel *secondLineRightLabel_;
    
    /**
     * Third line left label
     */
    UILabel *thirdLineLeftLabel_;
    
    /**
     * Third line right label
     */
    UILabel *thirdLineRightLabel_;
    
    /**
     * Check image view
     */
    UIImageView *checkImageView_;
    
    /**
     * In water case the money is in super node.
     */
    NSString *waterAmount_;
    
    /**
     * Check flag
     */
    BOOL checkActive_;
    
    /**
     * Payment
     */
    Payment *payment_;
    
}

/**
 * Provides read-write access to the titleLeftLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLeftLabel;

/**
 * Provides read-write access to the titleRightLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleRightLabel;

/**
 * Provides read-write access to the firstLineLeftLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *firstLineLeftLabel;

/**
 * Provides read-write access to the firstLineRightLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *firstLineRightLabel;

/**
 * Provides read-write access to the secondLineLeftLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *secondLineLeftLabel;

/**
 * Provides read-write access to the secondLineRightLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *secondLineRightLabel;

/**
 * Provides read-write access to the thirdLineLeftLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *thirdLineLeftLabel;

/**
 * Provides read-write access to the thirdLineRightLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *thirdLineRightLabel;

/**
 * Provides read-write access to the checkImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *checkImageView;

/**
 * Provides read-write access to waterAmount.
 */
@property (nonatomic, readwrite, copy) NSString *waterAmount;

/**
 * Provides read-write access to the checkActive
 */
@property (nonatomic, readwrite, assign) BOOL checkActive;

/**
 * Provides read-write access to the receipt
 */
@property (nonatomic, readwrite, retain) Payment *payment;

/**
 * Creates and returns an autoreleased ReceiptCell constructed from a NIB file
 *
 * @return The autoreleased ReceiptCell constructed from a NIB file
 */
+ (ReceiptCell *)receiptCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeightForReceiptType:(PaymentTypeEnum)paymemtTypeEnum;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

/**
 * Sets the check image depending on the status
 */
- (void)setCheckActive:(BOOL)checkActive;

/**
 * Configures the cell for a type
 */
- (void)setPayment:(Payment *)payment 
       paymentType:(PaymentTypeEnum)paymentType;

@end