/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TransferDoneCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTTableCell.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the LoginInfoCell NIB file name
 */
#define NIB_FILE_NAME                                               @"TransferDoneCell"

/**
 * Defines the cell identifier
 */
#define CELL_IDENTIFIER                                             @"TransferDoneCell"

/**
 * Defines the cell height
 */
#define CELL_HEIGHT                                         41.0f


/**
 * Defines the big font size
 */
#define BIG_FONT_SIZE                                       16.0f

/**
 * Defines the small font size
 */
#define SMALL_FONT_SIZE                                     12.0f


#pragma mark -

@implementation TransferDoneCell

#pragma mark -
#pragma mark Properties

@synthesize tickImageView = tickImageView_;
@synthesize iconImageView = iconImageView_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [tickImageView_ release];
    tickImageView_ = nil;
    
    [iconImageView_ release];
    iconImageView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles cell elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    [NXT_Peru_iPhoneStyler styleLoginInformationCell:self];
    self.showSeparator = NO;
    self.showDisclosureArrow = NO;

    [NXT_Peru_iPhoneStyler styleLabel:self.leftTextLabel withFontSize:BIG_FONT_SIZE color:[UIColor BBVABlueSpectrumColor]];
    tickImageView_.image = [[ImagesCache getInstance] imageNamed:TRANSFER_DONE_IMAGE_FILE_NAME];
    iconImageView_.image = [[ImagesCache getInstance] imageNamed:TRANSFER_TICK_IMAGE_FILE_NAME];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundView.backgroundColor = [UIColor whiteColor];

}

/*
 * Creates and returns an autoreleased TransferDoneCell constructed from a NIB file
 */
+ (TransferDoneCell *)transferDoneCell {
    
    return (TransferDoneCell *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];

}


#pragma mark -
#pragma mark Cell associated information

/*
 * Returns the cell height
 */
+ (CGFloat)cellHeight {
        
    return CELL_HEIGHT;
    
}

/*
 * Returns the cell identifier
 */
+ (NSString *)cellIdentifier {
    
    return CELL_IDENTIFIER;
    
}

/*
 * Set left text
 */
- (void)setLeftText:(NSString *)leftText {

    self.leftTextLabel.text = leftText;
    [NXT_Peru_iPhoneStyler styleLabel:self.leftTextLabel withBoldFontSize:BIG_FONT_SIZE color:[UIColor BBVABlueSpectrumToneOneColor]];

}


@end
