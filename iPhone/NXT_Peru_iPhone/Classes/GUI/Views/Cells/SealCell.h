/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class SealCell;

/**
 * Card Transaction cell. It's a subclass of NXTTableCell.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface SealCell : NXTTableCell {
@private  
    
    /**
     * Top text label
     */
    UILabel *topTextLabel_;
    
    /**
     * Seal image view
     */
    UIImageView *sealImageView_;
    
    /**
     * Seal
     */
    NSString *seal_;
    
}

/**
 * Provides read-write access to the topTextLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *topTextLabel;

/**
 * Provides read-write access to the sealImageView and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *sealImageView;

/**
 * Provides read-write access to the seal
 */
@property (nonatomic, readwrite, copy) NSString *seal;

/**
 * Creates and returns an autoreleased SealCell constructed from a NIB file
 *
 * @return The autoreleased SealCell constructed from a NIB file
 */
+ (SealCell *)sealCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end