//
//  CheckPendingDocCell.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/25/13.
//
//

#import <UIKit/UIKit.h>

@interface CheckPendingDocCell : UITableViewCell{
    
    /**
     * The flag to know if the doc is checked.
     *
     */
    BOOL isSelectedDoc_;
    
    /**
     * the Main Title
     *
     */
    UILabel *mainLabel_;
    
    /**
     * the Main Title
     *
     */
    UILabel *mainDetail_;
    
    /**
     * the first Title
     *
     */
    UILabel *firstTitle_;
    
    /**
     * the first detail
     *
     */
    UILabel *firstDetail_;
    
    /**
     * the expiration Date Title
     *
     */
    UILabel *expirationDateTitle_;
    
    /**
     * the expiration min Amount Title
     *
     */
    UILabel *minAmountTitle_;
    
    /**
     * the expiration max amount Title
     *
     */
    UILabel *maxAmountTitle_;
    
    /**
     * the expiration Date
     *
     */
    UILabel *expirationDate_;
    
    /**
     * the min Amount
     *
     */
    UILabel *minAmount_;
    
    /**
     * the max amount
     *
     */
    UILabel *maxAmount_;
    
    /**
     * the check image
     *
     */
    UIImageView *checkImage_;
    
}

/**
 * provide readwrite access to the flag isSelectedDoc
 *
 */
@property(nonatomic, readwrite, assign) BOOL isSelectedDoc;

/**
 * provide readwrite access to the Main Title
 *
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *mainLabel;

/**
 * provide readwrite access to the Main Title
 *
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *mainDetail;

/**
 * provide readwrite access to the First Title
 *
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *firstTitle;

/**
 * provide readwrite access to the First Detail
 *
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *firstDetail;

/**
 * provide readwrite access to the Expiration date title Label
 *
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *expirationDateTitle;

/**
 * provide readwrite access to the Min Amount title Label
 *
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *minAmountTitle;

/**
 * provide readwrite access to the Max Amount title Label
 *
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *maxAmountTitle;

/**
 * provide readwrite access to the Expiration date data Label
 *
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *expirationDate;

/**
 * provide readwrite access to the Min amount data Label
 *
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *minAmount;

/**
 * provide readwrite access to the Max amount data Label
 *
 */
@property(nonatomic, readwrite, retain) IBOutlet UILabel *maxAmount;

/**
 * provide readwrite access to the check image data Label
 *
 */
@property(nonatomic, readwrite, retain) IBOutlet UIImageView *checkImage;

/**
 * The cell identifier.
 *
 * @return The cell idenfitier.
 */
+ (NSString *)cellIdentifier;

/**
 * Creates and returns an autoreleased CheckOptionAccountTableViewCell constructed from a NIB file.
 *
 * @return The autoreleased CheckOptionAccountTableViewCell constructed from a NIB file.
 */
+ (CheckPendingDocCell *)checkPendingDocCell;

/**
 * Accomodate Views
 */
- (void)accomodateForDocumentType:(BOOL)isPartial;

@end
