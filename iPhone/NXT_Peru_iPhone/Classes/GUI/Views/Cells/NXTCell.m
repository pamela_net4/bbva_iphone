/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTCell.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"

#pragma mark -

@interface NXTCell()

/**
 * Setups the cell
 *
 * @private
 */
- (void)setup;

@end

@implementation NXTCell

#pragma mark -
#pragma mark Properties

@synthesize backgroundImageView = backgroundImageView_;
@synthesize selectedBackgroundImageView = selectedBackgroundImageView_;
@synthesize hideSeparator = hideSeparator_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [backgroundImageView_ release];
    backgroundImageView_ = nil;
    
    [selectedBackgroundImageView_ release];
    selectedBackgroundImageView_ = nil;
    
    [separatorImageView_ release];
    separatorImageView_  = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Setups the cell
 */
- (void)setup {
    self.selectionStyle = UITableViewCellSelectionStyleGray;
    self.backgroundView = backgroundImageView_;
    self.selectedBackgroundView = selectedBackgroundImageView_;
    self.hideSeparator = NO;
}

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Backgrounds and accesory views are associated
 * to the cell elements
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    [self setup];
}

/**
 * Initializes a table cell with a style and a reuse identifier and returns it to the caller.
 *
 * @param style: A constant indicating a cell style.
 * @param reuseIdentifier: A string used to identify the cell object if it is to be reused for drawing multiple rows of a table view.
 * @return An initialized UITableViewCellobject or nil if the object could not be created.
 */
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setup];
    }
    
    return self;
}

#pragma mark -
#pragma mark Properties

/**
 * Sets the hide separator flag
 *
 * @param hideSeparator
 */
- (void)setHideSeparator:(BOOL)hideSeparator {
    
    hideSeparator_ = hideSeparator;
    
    if (!hideSeparator_) {
        
        if (separatorImageView_ == nil) {
            
            separatorImageView_ = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, self.frame.size.height - 2.0f, self.frame.size.width, 2.0f)];
            separatorImageView_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
            separatorImageView_.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
        }
        
        [self addSubview:separatorImageView_];
        
    } else {
        
        [separatorImageView_ removeFromSuperview];
        [separatorImageView_ release];
        separatorImageView_ = nil; 
        
    }
    
}

@end
