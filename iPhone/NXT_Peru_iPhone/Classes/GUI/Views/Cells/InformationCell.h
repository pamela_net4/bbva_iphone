/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "NXTTableCell.h"

@class TitleAndAttributes;

/**
 * Information cell. It's a subclass of NXTTableCell.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface InformationCell : NXTTableCell {
@private  
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Detail label
     */
    UILabel *detailLabel_;
    
    /**
     * Information
     */
    TitleAndAttributes *info_;
    
}

/**
 * Provides read-write access to the titleLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides read-write access to the detailLabel and exports it for interface builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *detailLabel;

/**
 * Provides read-write access to the info
 */
@property (nonatomic, readwrite, retain) TitleAndAttributes *info;

/**
 * Creates and returns an autoreleased InformationCell constructed from a NIB file
 *
 * @return The autoreleased InformationCell constructed from a NIB file
 */
+ (InformationCell *)informationCell;

/**
 * Returns the cell height
 *
 * @return The cell height
 */
+ (CGFloat)cellHeight;

- (CGFloat)dynamicCellHeight;

/**
 * Returns the cell identifier
 *
 * @return The cell identifier
 */
+ (NSString *)cellIdentifier;

@end