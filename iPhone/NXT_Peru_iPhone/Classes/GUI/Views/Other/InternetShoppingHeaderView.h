/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>
#import "SingletonBase.h"

@class InternetShoppingHeaderView;

/**
 * Provider to obtain a SimpleHeaderView created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface InternetShoppingHeaderViewProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary PublicServiceHeaderView to create it from a NIB file
     */
    InternetShoppingHeaderView *auxView_;
    
}

/**
 * Provides read-write access to the auxililary PublicServiceHeaderView
 */
@property (nonatomic, readwrite, assign) IBOutlet InternetShoppingHeaderView *auxView;

@end

/**
 * PublicService header with the concept, date and amount
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface InternetShoppingHeaderView : UIView {
@private
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Title Separator line
     */
    UIImageView *titleSeparator_;
    
    /**
     * Company label
     */
    UILabel *companyLabel_;

    /**
     * Company name label
     */
    UILabel *companyNameLabel_;
    
    
    UILabel *transactionLabel_;
    
    
    UILabel *transactionNumberLabel_;
    
    
    UILabel *businessLabel_;
    
    
    UILabel *businessNameLabel_;
    
    
    UILabel *currencyLabel_;
    
    
    UILabel *currencyNameLabel_;
    
    
    UILabel *amountLabel_;
    
    
    UILabel *amountValueLabel_;
    /**
     * Separator line
     */
    UIImageView *separator_;

}

/**
 * Provides readwrite access to the titleLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides readwrite access to the titleSeparator. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *titleSeparator;

/**
 * Provides readwrite access to the titleLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *companyLabel;

/**
 * Provides readwrite access to the titleLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *companyNameLabel;


@property (nonatomic, readwrite, retain) IBOutlet UILabel *transactionLabel;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *transactionNumberLabel;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *businessLabel;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *businessNameLabel;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *currencyLabel;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *currencyNameLabel;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *amountLabel;

@property (nonatomic, readwrite, retain) IBOutlet UILabel *amountValueLabel;



/**
 * Provides readwrite access to the separator1. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

/**
 * Provides readwrite access to the title
 */
@property (nonatomic, readwrite, copy) NSString *title;

/**
 * Provides readwrite access to the companyName
 */
@property (nonatomic, readwrite, copy) NSString *companyName;

@property (nonatomic, readwrite, copy) NSString *transactionNumber;

@property (nonatomic, readwrite, copy) NSString *businessName;

@property (nonatomic, readwrite, copy) NSString *currencyName;

@property (nonatomic, readwrite, copy) NSString *amountValue;


/**
 * Creates and returns an autoreleased PublicServiceHeaderView constructed from a NIB file
 *
 * @return The autoreleased PublicServiceHeaderView constructed from a NIB file
 */
+ (InternetShoppingHeaderView *)internetShoppingHeaderView;

/**
 * Returns the width of the view
 */
+ (CGFloat)width;

/**
 * Returns the height of the view
 */
+ (CGFloat)height;

@end
