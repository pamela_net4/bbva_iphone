/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * View to display a route distance and time as a smitransparent header
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TimeAndDistanceHeaderView : UIView {
    
@private
    
    /**
     * Header background image view
     */
    UIImageView *backgroundImageView_;
    
    /**
     * Distance and time label
     */
    UILabel *distanceAndTimeLabel_;
    
    /**
     * Route type selector segmented control
     */
    UISegmentedControl *routeTypeSelectorSegmentedControl_;
    
    /**
     * Distance text
     */
    NSString *distanceText_;
    
    /**
     * Time text
     */
    NSString *timeText_;
    
}


/**
 * Provides read-write access to the header background image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *backgroundImageView;

/**
 * Provides read-write access to the distance and time label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *distanceAndTimeLabel;

/**
 * Provides read-write access to the route type selector segmented control and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UISegmentedControl *routeTypeSelectorSegmentedControl;

/**
 * Provides read-write access to the distance text
 */
@property (nonatomic, readwrite, copy) NSString *distanceText;

/**
 * Provides read-write access to the time text
 */
@property (nonatomic, readwrite, copy) NSString *timeText;


/**
 * Creates and returns an autoreleased TimeAndDistanceHeaderView constructed from a NIB file
 *
 * @return The autoreleased TimeAndDistanceHeaderView constructed from a NIB file
 */
+ (TimeAndDistanceHeaderView *)timeAndDistanceHeaderView;


/**
 * Returns the view height
 *
 * @return The view height
 */
+ (CGFloat)viewHeight;

@end
