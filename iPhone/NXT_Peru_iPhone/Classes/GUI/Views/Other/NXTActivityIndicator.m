/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTActivityIndicator.h"
#import <QuartzCore/QuartzCore.h>

/**
 * Define the NIB file name
 */
#define NIB_FILE_NAME                                               @"NXTActivityIndicator"

/**
 * Defines the view width
 */
#define VIEW_WIDTH                                                  150.0f

/**
 * Defines the view height
 */
#define VIEW_HEIGHT                                                 150.0f

/**
 * Sizes of the background layer
 */
#define BACKGROUND_LAYER_WIDTH                                      85.0f
#define BACKGROUND_LAYER_HEIGHT                                     85.0f

/**
 * Private category
 */
@interface NXTActivityIndicatorProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (NXTActivityIndicatorProvider *)getInstance;

/**
 * Creates and returns an autoreleased NXTActivityIndicator constructed from a NIB file
 *
 * @return The autoreleased NXTActivityIndicator constructed from a NIB file
 */
- (NXTActivityIndicator *)NXTActivityIndicator;

@end

#pragma mark -

@implementation NXTActivityIndicatorProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * NXTActivityIndicatorProvier singleton only instance
 */
static NXTActivityIndicatorProvider *NXTActivityIndicatorProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    @synchronized([NXTActivityIndicatorProvider class]) {
        if (NXTActivityIndicatorProviderInstance_ == nil) {
            NXTActivityIndicatorProviderInstance_ = [super allocWithZone:zone];
            return NXTActivityIndicatorProviderInstance_;
        }        
    }
    
    return nil;
}

/*
 * Returns the singleton only instance
 */
+ (NXTActivityIndicatorProvider *)getInstance {
    if (NXTActivityIndicatorProviderInstance_ == nil) {
        @synchronized([NXTActivityIndicatorProvider class]) {
            if (NXTActivityIndicatorProviderInstance_ == nil) {
                NXTActivityIndicatorProviderInstance_ = [[NXTActivityIndicatorProvider alloc] init];
            }   
        }
    }
    
    return NXTActivityIndicatorProviderInstance_;
    
}

#pragma mark -
#pragma mark NXTActivityIndicator creation

/*
 * Creates and returns aa autoreleased NXTTableCell constructed from a NIB file
 */
- (NXTActivityIndicator *)NXTActivityIndicator {
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    NXTActivityIndicator *result = auxView_;
    auxView_ = nil;
    
    return result;    
}

@end

#pragma mark -

@implementation NXTActivityIndicator

@synthesize indicator = indicator_;
@synthesize messageLabel = messageLabel_;
@synthesize backgroundView = backgroundView_;
@dynamic showBackgroundLayer;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    [indicator_ release];
    indicator_ = nil;
    
    [messageLabel_ release];
    messageLabel_ = nil;

    [backgroundView_ release];
    backgroundView_ = nil;
    
    [super dealloc];
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the cell elements
 */
- (void)awakeFromNib {
    
    self.backgroundColor = [UIColor clearColor];
    self.alpha = 0.0f;
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
    backgroundView_.backgroundColor = [UIColor blackColor];
     backgroundView_.layer.cornerRadius = 15;
        backgroundView_.alpha = 0.50f;
        backgroundView_.frame = CGRectMake(self.frame.size.width / 2 - BACKGROUND_LAYER_WIDTH / 2,
                                           self.frame.size.height / 2 - BACKGROUND_LAYER_HEIGHT / 2,
                                           BACKGROUND_LAYER_WIDTH, BACKGROUND_LAYER_HEIGHT);
    }
    else
    {
    backgroundView_.backgroundColor = [UIColor clearColor];
    [indicator_ setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [indicator_ setColor:[UIColor darkGrayColor]];
        
    }
     
}

/*
 * Creates and returns an autoreleased NXTActivityIndicator constructed from a NIB file
 */
+ (NXTActivityIndicator *)NXTActivityIndicator {
    return [[NXTActivityIndicatorProvider getInstance] NXTActivityIndicator];
    
}

#pragma mark -
#pragma mark View information

/*
 * Returns the width of the view
 */
+ (CGFloat)width {
    return VIEW_WIDTH;
}

/*
 * Returns the height of the view
 */
+ (CGFloat)height {
    return VIEW_HEIGHT;
}

#pragma mark -
#pragma mark Getters and setters

- (BOOL)showBackgroundLayer {
    return (!backgroundView_.hidden);
}

- (void)setShowBackgroundLayer:(BOOL)value {
    if (value) {
        backgroundView_.hidden = NO;
    } else {
        backgroundView_.hidden = YES;
    }
}

#pragma mark -
#pragma mark View management

/*
 * Shows the view with a message (optional)
 */
- (void)showIndicatorWithMessage:(NSString *)message {
    [indicator_ startAnimating];
    messageLabel_.text = message;
    
    if (message == nil || [message length] == 0) {
        messageLabel_.hidden = YES;
        indicator_.frame = CGRectMake(56.0f, 56.0f, 37.0f, 37.0f);
    } else {
        messageLabel_.hidden = NO;
        indicator_.frame = CGRectMake(56.0f, 25.0f, 37.0f, 37.0f);
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.25];
    
    self.alpha = 1.0f;
    
    [UIView commitAnimations];
}

/*
 * Hides the indicator
 */
- (void)hideIndicator {
    
    if ([indicator_ isAnimating]) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.25];
        
        self.alpha = 0.0f;
                
        [UIView commitAnimations];
        
        [indicator_ stopAnimating];
    }
}

@end
