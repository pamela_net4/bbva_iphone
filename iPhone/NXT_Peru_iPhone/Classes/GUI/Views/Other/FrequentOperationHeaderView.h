//
//  FrequentOperationHeaderView.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/10/14.
//  Copyright (c) 2014 MDP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SingletonBase.h"

/**
 * Define the tag to identify labels to remove (ignoring the title)
 */
#define LABELS_TAG                                                  123

@class FrequentOperationHeaderView;

/**
 * Provider to obtain a FrequentOperationHeaderView created from a NIB file
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface FOHeaderViewProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary FrequentOperationHeaderView to create it from a NIB file
     */
    FrequentOperationHeaderView *auxView_;
    
}

/**
 * Provides read-write access to the auxililary FrequentOperationHeaderView
 */
@property (nonatomic, readwrite, assign) IBOutlet FrequentOperationHeaderView *auxView;

@end


@interface FrequentOperationHeaderView : UIView{
@private
    
    /**
     * Label of the concept of the transaction
     */
    UILabel *titleLabel_;
    /**
     * Separator line
     */
    UIImageView *separator_;
    /**
     * Array for the titles
     */
    NSArray *titlesAndAttributeArray_;
    
    Class className_;
    
}

/**
 * Provides readwrite access to the keysArray.
 */
@property (nonatomic, readwrite, retain) NSArray *titlesAndAttributeArray;
/**
 * Provides readwrite access to the titleLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides readwrite access to the separator1. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

/**
 * Provides readwrite access to the title
 */
@property (nonatomic, readwrite, copy) NSString *title;
/**
 * Provides readwrite access to the height
 */
@property (nonatomic, readwrite, assign) CGFloat height;

@property(nonatomic,readwrite,assign) Class className;


/**
 * Creates the titles and subtitles for the view using the key and values arrays
 *
 */
- (void)updateInformationView;

/**
 * Creates and returns an autoreleased SimpleHeaderView constructed from a NIB file
 *
 * @return The autoreleased SimpleHeaderView constructed from a NIB file
 */
+ (FrequentOperationHeaderView *)frequentOperationHeaderView;

/**
 * Returns the width of the view
 */
+ (CGFloat)width;

/**
 * Returns the height of the view
 */
+ (CGFloat)height;

@end
