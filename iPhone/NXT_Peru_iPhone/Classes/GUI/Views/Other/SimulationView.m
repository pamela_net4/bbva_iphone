//
//  SimulationView.m
//  Created by Author.
//

#import "SimulationView.h"
#import <QuartzCore/QuartzCore.h>
#import "NPopup.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"
#import "Tooltip.h"
#import "FastLoanAlternativeList.h"
#import "FastLoanAlternative.h"

@implementation SimulationView

@synthesize view = view_;
@synthesize delegate = delegate_;

@synthesize quotaMonthLabel = quotaMonthLabel_;
@synthesize termLabel = termLabel_;
@synthesize typeOfQuotaLabel = typeOfQuotaLabel_;
@synthesize dayOfPayLabel = dayOfPayLabel_;
@synthesize teaLabel = teaLabel_;
@synthesize tceaLabel = tceaLabel_;
@synthesize quotaMonthValueLabel = quotaMonthValueLabel_;
@synthesize termValueLabel = termValueLabel_;
@synthesize typeOfQuotaValueLabel = typeOfQuotaValueLabel_;
@synthesize dayOfPayValueLabel = dayOfPayValueLabel_;
@synthesize teaValueLabel = teaValueLabel_;
@synthesize tceaValueLabel = tceaValueLabel_;

@synthesize quotaMonthButton = quotaMonthButton_;
@synthesize teaButton = teaButton_;
@synthesize tceaButton = tceaButton_;
@synthesize continueButton = continueButton_;
@synthesize modifyButton = modifyButton_;

-(void)dealloc{
    
    delegate_ = nil;
    
    [view_ release];
    view_ = nil;
    
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code              
        [[NSBundle mainBundle] loadNibNamed:@"SimulationView" owner:self options:nil];
        [self addSubview: self.view];
        
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        
        UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
        if (UIDeviceOrientationIsLandscape(deviceOrientation)) {
            CGFloat tmp = screenWidth;
            screenWidth = screenHeight;
            screenHeight = tmp;
        }
        self.frame = CGRectMake(screenWidth/2 - CGRectGetWidth(self.view.bounds)/2,
                                screenHeight/2 - CGRectGetHeight(self.view.bounds)/2,
                                CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
        
        //awake from nib
    }
    return self;
}



-(void) setData:(FastLoanSimulation *)fastLoanSimulation{
    
    dayOfPayValueLabel_.text = fastLoanSimulation.dayOfPay;
    teaValueLabel_.text = [NSString stringWithFormat:@"%@%%",fastLoanSimulation.teaFormated];
    
    
    if(fastLoanSimulation.fastLoanAlternativeList && [fastLoanSimulation.fastLoanAlternativeList fastloanalternativeCount]>0){
        FastLoanAlternative * fastLoanAlternative = [fastLoanSimulation.fastLoanAlternativeList fastloanalternativeAtPosition:0];
        
        NSString * months = (fastLoanAlternative.monthDobles!=nil) ?
                            [NSString stringWithFormat:@" %@", fastLoanAlternative.monthDobles] :
                            @"";
        
        typeOfQuotaValueLabel_.text = [NSString stringWithFormat:@"%@%@",
                                      ( fastLoanAlternative.quotaType !=nil ? fastLoanAlternative.quotaType : @""),
                                        months];
        
        quotaMonthValueLabel_.text = [NSString stringWithFormat:@"%@ %@", fastLoanSimulation.currencySymbol, fastLoanAlternative.quotaValue];
        termValueLabel_.text = [NSString stringWithFormat:@"%@ meses", fastLoanAlternative.term];
        tceaValueLabel_.text = [NSString stringWithFormat:@"%@%%",fastLoanAlternative.tcea];
        
    }
    
    
  }

- (void) layoutSubviews{   
    [super layoutSubviews];
    
    //styles
    self.view.layer.cornerRadius = 10;
    self.view.layer.masksToBounds = YES;
    
    [NXT_Peru_iPhoneStyler styleLabel:quotaMonthLabel_ withFontSize:16.0f color:[UIColor BBVAGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:quotaMonthValueLabel_ withFontSize:16.0f color:[UIColor BBVAGreyColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:dayOfPayLabel_ withFontSize:14.0f color:[UIColor BBVAGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:dayOfPayValueLabel_ withFontSize:14.0f color:[UIColor BBVAGreyColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:termLabel_ withFontSize:14.0f color:[UIColor BBVAGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:termValueLabel_ withFontSize:14.0f color:[UIColor BBVAGreyColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:typeOfQuotaLabel_ withFontSize:14.0f color:[UIColor BBVAGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:typeOfQuotaValueLabel_ withFontSize:14.0f color:[UIColor BBVAGreyColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:teaLabel_ withFontSize:14.0f color:[UIColor BBVAGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:teaValueLabel_ withFontSize:14.0f color:[UIColor BBVAGreyColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:tceaLabel_ withFontSize:14.0f color:[UIColor BBVAGreyColor]];
    [NXT_Peru_iPhoneStyler styleLabel:tceaValueLabel_ withFontSize:14.0f color:[UIColor BBVAGreyColor]];
    
    quotaMonthLabel_.text = @"Cuota Mensual";
    dayOfPayLabel_.text = @"Día de Pago";
    termLabel_.text = @"Plazo";
    typeOfQuotaLabel_.text = @"Tipo de Cuota";
    teaLabel_.text = @"TEA";
    tceaLabel_.text = @"TCEA";
    
    [continueButton_ setTitle:@"Continuar" forState:UIControlStateNormal];
    [modifyButton_ setTitle:@"Modificar" forState:UIControlStateNormal];
    
    [NXT_Peru_iPhoneStyler styleBoldWhiteButton:continueButton_];
    [NXT_Peru_iPhoneStyler styleWhiteButton:modifyButton_];
    
    
}

//pop methods

-(IBAction)continueButton:(id)sender{
    if(popup_){
        [popup_ close];
    }
    [delegate_ onNextScreenFromSimulation];
}

-(IBAction)modifyButton:(id)sender{
    if(popup_){
        [popup_ close];
    }
}

-(IBAction)showTooltip:(id)sender{
    if(sender == quotaMonthButton_){
        [self showInfoTooltipFromView:quotaMonthButton_ message:@"No incluye seguro de desgravamen, el cual puede variar entre S/ 0.35 y S/16.50 (ver cuota total en cronograma de pagos)."];
    }
    else if(sender == teaButton_){
        [self showInfoTooltipFromView:teaButton_ message:@"La Tasa de Interés Efectiva Anual calcula el costo o valor de interés esperado en 360 días."];
    }
    else if(sender == tceaButton_){
        [self showInfoTooltipFromView:tceaButton_ message:@"La Tasa de Costo Efectiva Anual representa el costo total que se pagará en 360 días, incluyendo TEA, comisiones, gastos y seguros."];
    }
}

-(void)showInWindow:(UIWindow*)parentWindow{
    
    if(!popup_){
        popup_ = [[NPopup alloc] initWithParentWindow:parentWindow];
        popup_.backgroundColor = [UIColor clearColor];
        popup_.dialogView.backgroundColor = [UIColor clearColor];
        [popup_ setContainerView:self];
    }
    [popup_ show];
}


-(void)showInfoTooltipFromView:(UIView*)fromView message:(NSString*) message{
    Tooltip * tooltipnew = [[Tooltip alloc] initWithMessage:message
                                                     source:fromView parentWindow:self.view.window];
    tooltipnew.delegate = nil;
    tooltipnew.hideShadow = YES;
    tooltipnew.arrowColor = [UIColor colorWithRed:0.0/255.0 green:110.0/255.0 blue:193.0/255.0 alpha:1];
    tooltipnew.borderColor =[UIColor colorWithRed:9.0/255.0 green:79.0/255.0 blue:164.0/255.0 alpha:1];
    tooltipnew.textColor = [UIColor whiteColor];
    [tooltipnew show];
}

@end
