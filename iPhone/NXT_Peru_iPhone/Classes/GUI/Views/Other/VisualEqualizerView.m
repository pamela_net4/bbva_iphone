//
//  VisualEqualizerView.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 1/4/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "VisualEqualizerView.h"

#define SQUARE_IN_BAR 7
#define NUMBER_BARS 6



@interface BarEqualizer : NSObject
{
    NSMutableArray *rectangles_;
    int value_;
    BOOL enable_;
}

@end
@implementation BarEqualizer

-(id)initWithFrame:(CGRect)frame withSeparator:(NSInteger)separator{
    if(self = [super init]){
    
        CGFloat squareHeight = (frame.size.height - (separator*(SQUARE_IN_BAR-1)) )/ SQUARE_IN_BAR;
    
        rectangles_ = [[NSMutableArray alloc] init];
        for(int i=0;i<SQUARE_IN_BAR;i++){
            [rectangles_ addObject:[NSValue valueWithCGRect:CGRectMake(frame.origin.x, i*squareHeight + i*separator, frame.size.width, squareHeight)]];
        }
        
        NSLog(@"%@",NSStringFromCGRect(frame));
    }
    
    return self;
    
}
-(void)setLevelValue:(int)value{
    value_ = value;
}
-(void)setEnable:(BOOL)enable{
    enable_ = enable;
}
-(void) drawRect:(CGContextRef)context{
   
    for(int i=0;i<SQUARE_IN_BAR;i++){
        if(SQUARE_IN_BAR - value_<=i || i == SQUARE_IN_BAR-1 ){
            CGRect rectangle = [[rectangles_ objectAtIndex:i] CGRectValue];
            CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, enable_?1.0:0.5);
            CGContextFillRect(context, rectangle);
        }
     }
    
    
}

@end

@implementation VisualEqualizerView{
    
    NSMutableArray *bars;
}

@dynamic enable;

-(id)initWithFrame:(CGRect)frame withSeparator:(NSInteger)separator{
    if(self = [super initWithFrame:frame]){
        
        [self setBackgroundColor:[UIColor clearColor]];
        
        bars = [[NSMutableArray alloc] init];
        CGFloat widhtBar = (frame.size.width - separator*(NUMBER_BARS-1))/NUMBER_BARS;
        CGFloat heightBar = frame.size.height;
        for (int i=0; i<NUMBER_BARS; i++) {
            
            [bars addObject:[[BarEqualizer alloc]  initWithFrame:CGRectMake(i*widhtBar + i*separator,0,widhtBar,heightBar) withSeparator:separator]];
        }
        enable_ = YES;
        [self loadDefaultLevels];
    }
    
    return self;
}

-(void)loadDefaultLevels{
    
    [((BarEqualizer*)[bars objectAtIndex:0]) setLevelValue:6];
    [((BarEqualizer*)[bars objectAtIndex:1]) setLevelValue:3];
    [((BarEqualizer*)[bars objectAtIndex:2]) setLevelValue:5];
    [((BarEqualizer*)[bars objectAtIndex:3]) setLevelValue:7];
    [((BarEqualizer*)[bars objectAtIndex:4]) setLevelValue:3];
    [((BarEqualizer*)[bars objectAtIndex:5]) setLevelValue:6];
    
    
}
-(void)setValuesForEqualizer:(NSArray*)values{
    
    if([values count]== NUMBER_BARS){
    
        for(int i= 0;i<NUMBER_BARS;i++)
            [((BarEqualizer*)[bars objectAtIndex:i]) setLevelValue:([[values objectAtIndex:i] floatValue]*7)];

    }
    [self setNeedsDisplay];
    
}

-(void)setEnable:(BOOL)enable{
    enable_ = enable;
    
    [self setNeedsDisplay];
}


-(void) drawRect:(CGRect)rect{
    
    [super drawRect:rect];
    
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextClearRect(context, rect);

    for (int i=0; i<NUMBER_BARS; i++) {
        [((BarEqualizer*)[bars objectAtIndex:i]) setEnable:enable_];
        [((BarEqualizer*)[bars objectAtIndex:i]) drawRect:context];
    }
    
}


@end
