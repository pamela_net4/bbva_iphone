//
//  RadioPlayerFooterView.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/31/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "RadioPlayerFooterView.h"
#import "UIColor+BBVA_Colors.h"
#import "NibLoader.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "VisualEqualizerView.h"

/**
 * Define the Simple selector account NIB file name
 */
#define NIB_FILE_NAME                                               @"RadioPlayerFooterView"


@implementation RadioPlayerFooterView

@synthesize goPlayerButton = goPlayerButton_;
@synthesize playRadioButton = playRadioButton_;
@synthesize titleTrackInfoLabel = titleTrackInfoLabel_;
@synthesize delegate = delegate_;

/*
* Creates and returns an autoreleased TimeAndDistanceHeaderView constructed from a NIB file
*/
+ (RadioPlayerFooterView *)radioPlayerFooterView {
    
    RadioPlayerFooterView *result = (RadioPlayerFooterView *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [result awakeFromNib];
    
    return result;
    
}


#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles the header label
 * and loads the map icon image
 */
- (void)awakeFromNib {
    
    [self.layer insertSublayer:[self gradientBGLayerForBounds:[self bounds]] atIndex:0];
    
    
    if(equalizerSmallView_ == nil){
        equalizerSmallView_ = [[VisualEqualizerView alloc] initWithFrame:CGRectMake(([self bounds].size.width-70)/2, CGRectGetMinY([titleTrackInfoLabel_ frame])-30 , 70, 28) withSeparator:SEPARATOR_SMALL];
        [self addSubview:equalizerSmallView_];
    }
    
    ImagesCache *imageCache = [ImagesCache getInstance];
    [goPlayerButton_ setBackgroundImage:[imageCache imageNamed:RADIO_GO_PLAYER_BUTTON_IMAGE_FILE_NAME ] forState:UIControlStateNormal];
    
    [playRadioButton_ setBackgroundImage:[imageCache imageNamed:RADIO_PLAY_BUTTON_IMAGE_FILE_NAME ] forState:UIControlStateNormal];
    
}

-(void)activeRadioPlaying{
    
    ImagesCache *imageCache = [ImagesCache getInstance];
    [playRadioButton_ setBackgroundImage:[imageCache imageNamed:RADIO_STOP_BUTTON_IMAGE_FILE_NAME ] forState:UIControlStateNormal];
    [equalizerSmallView_ loadDefaultLevels];
    [equalizerSmallView_ setEnable:YES];
}

-(void)deactiveRadioPlaying{
    ImagesCache *imageCache = [ImagesCache getInstance];
    [playRadioButton_ setBackgroundImage:[imageCache imageNamed:RADIO_PLAY_BUTTON_IMAGE_FILE_NAME ] forState:UIControlStateNormal];
    
    [equalizerSmallView_ loadDefaultLevels];
    [equalizerSmallView_ setEnable:YES];


}

- (CALayer *)gradientBGLayerForBounds:(CGRect)bounds
{
    CAGradientLayer * gradientBG = [CAGradientLayer layer];
    gradientBG.frame = bounds;
    gradientBG.colors = @[ (id)[[UIColor BBVABlackFirstToneColor] CGColor], (id)[[UIColor BBVABlackSecondToneColor] CGColor], (id)[[UIColor BBVABlackThirdToneColor] CGColor] ,(id)[[UIColor BBVAGrayFourToneColor] CGColor] ];
    return gradientBG;
}

+(CGFloat)footerHeight{
    return 50;
}

-(void)setTitleTrackInfo:(NSString*)titleTrack
{
    CGSize size = [titleTrack sizeWithFont:[UIFont systemFontOfSize:10.0f] constrainedToSize:CGSizeMake(999, 16)];
    CGRect frame =  [titleTrackInfoLabel_ frame];
    frame.size.width = size.width;
    frame.origin.x = (self.bounds.size.width - size.width)/2.0f;
    [titleTrackInfoLabel_ setFrame:frame];
    [titleTrackInfoLabel_ setText:titleTrack];
    
    
}


-(void)setValuesForEqualizer:(NSArray*)values{
    [equalizerSmallView_ setValuesForEqualizer:values];
}

-(IBAction)playRadioButton:(id)sender{
    [delegate_ playRadioAction];
}

-(IBAction)goPlayerButton:(id)sender{
    [delegate_ goPlayerAction];
}

@end
