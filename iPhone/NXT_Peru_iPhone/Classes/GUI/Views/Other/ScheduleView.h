//
//  ScheduleView.h
//  Created by Author.
//

#import <UIKit/UIKit.h>
#import "FastLoanSchedule.h"

@class NPopup;

@interface ScheduleView : UIView<UITableViewDataSource>{
@private
    NPopup * popup_;
    UIView *view_;
    
    UIView *item1View_;
    UIView *item2View_;
    UIView *item3View_;
    UIView *item4View_;
    UIView *item5View_;
    
    UILabel * headerTextLabel_;
    UILabel * degravamenLabel_;
    UIView *footerBackgroundView_;
    UITableView * tableView_;
    FastLoanSchedule * fastLoanSchedule_;
}


@property (nonatomic, retain) IBOutlet UIView *view;
@property (nonatomic, retain) IBOutlet UIView *footerBackgroundView;
@property (nonatomic, retain) IBOutlet UILabel * headerTextLabel;
@property (nonatomic, retain) IBOutlet UILabel * degravamenLabel;
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) FastLoanSchedule * fastLoanSchedule;

@property (nonatomic, retain) IBOutlet UIView *item1View;
@property (nonatomic, retain) IBOutlet UIView *item2View;
@property (nonatomic, retain) IBOutlet UIView *item3View;
@property (nonatomic, retain) IBOutlet UIView *item4View;
@property (nonatomic, retain) IBOutlet UIView *item5View;

-(void)showInWindow:(UIWindow*)parentWindow;



@end
