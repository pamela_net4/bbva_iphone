/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>
#import "SingletonBase.h"
#import "AccountTransaction.h"
#import "NXTSwipeGestureRecognizer.h"

//Forward declaration
@class TransactionDetailView;

/**
 * Protocol to inform that a swipe has been done over this view
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol TransactionDetailViewDelegate

/**
 * The swipe has been done on the given direction
 *
 * @param direction of the swipe
 */
- (void)swipeGestureDoneOnDirection:(SwipeDirection)direction;

@end


/**
 * Provider to obtain a MovementDetailView created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransactionDetailViewProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary MovementDetailView to create it from a NIB file
     */
    TransactionDetailView *auxView_;
    
}

/**
 * Provides read-write access to the auxililary MovementDetailView
 */
@property (nonatomic, readwrite, assign) IBOutlet TransactionDetailView *auxView;

@end

/**
 * Movement detail view for show data of a movement
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransactionDetailView : UIView <NXTSwipeGestureRecognizerDelegate> {
@private    
    
    /**
     * Amount of the transaction
     */
    UILabel *amountLabel_;
    
    /**
     * Name label
     */
    UILabel *nameLabel_;
    
    /**
     * Information
     */
    NSMutableArray *information_;
    
    /**
     * Labels
     */
    NSMutableArray *labelsArray_;
    
    /**
     * Swipe gesture recognizer to show options view
     */
    NXTSwipeGestureRecognizer *swipeGestureRecognizer_;
    
    /**
     * Delegate
     */
    id<TransactionDetailViewDelegate> delegate_;
}

/**
 * Provides read-write access to the conceptLabel_ and exports it for Interface Builder
 */
@property (nonatomic, readwrite, assign) id<TransactionDetailViewDelegate> delegate;

/**
 * Provides read-write access to the amountLabel and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *amountLabel;

/**
 * Provides read-write access to the nameLabel and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *nameLabel;

/**
 * Provides read-write access to the information
 */
@property (nonatomic, readwrite, retain) NSArray *information;

/**
 * Creates and returns an autoreleased TransactionDetailView constructed from a NIB file
 *
 * @return The autoreleased TransactionDetailView constructed from a NIB file
 */
+ (TransactionDetailView *)transactionDetailView;

/**
 * Returns the width of the view
 */
+ (CGFloat)width;

/**
 * Returns the height of the view
 *
 * @param number The number of teails
 */
+ (CGFloat)heightForDetailsNumber:(NSInteger)number ;

@end
