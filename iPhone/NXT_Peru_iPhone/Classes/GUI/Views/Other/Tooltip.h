

#import <UIKit/UIKit.h>

@protocol TooltipDelegate

-(void)onDismiss:(UIView*)view;

@end

@interface Tooltip : UIView{
    UIColor *arrowColor_;
    UIColor *borderColor_;
    UIColor *textColor_;
}

@property (assign, nonatomic, getter=isShadowHidden) BOOL hideShadow;
@property (nonatomic, readwrite, retain) UIColor *arrowColor;
@property (nonatomic, readwrite, retain) UIColor *borderColor;
@property (nonatomic, readwrite, retain) UIColor *textColor;

@property (strong, nonatomic) UILabel *label;
@property (nonatomic, readwrite, retain) id<TooltipDelegate> delegate;

- (id)initWithMessage:(NSString *)message source:(UIView*)fromView parentWindow:(UIWindow *)parentWindow;

- (void)show;
- (void)dismiss;

@end
