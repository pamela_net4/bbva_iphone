/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>
#import "SingletonBase.h"
#import "BannerView.h"

@class GlobalPositionSummaryView;
@class TitleAndAttributes;
@class CampaignList;

@protocol BannerSelectionDelegate
-(void) bannerSelected:(int32_t)position;
@end
/**
 * Provider to obtain a GlobalPositionSummaryView created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GlobalPositionSummaryViewProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary GlobalPositionSummaryView to create it from a NIB file
     */
    GlobalPositionSummaryView *auxView_;
    
}

/**
 * Provides read-write access to the auxililary MovementDetailView
 */
@property (nonatomic, readwrite, assign) IBOutlet GlobalPositionSummaryView *auxView;

@end

/**
 * Transaction header with the concept, date and amount
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GlobalPositionSummaryView : UIView<BannerDelegate> {
@private
    
    /**
     * Date label
     */
    UILabel *dateLabel_;
    
    /**
     * Welcome label.
     */
    UILabel *welcomeLabel_;
    
    /**
     * Name label.
     */
    UILabel *nameLabel_;
    
    /**
     * Separator
     */
    UIImageView *separator_;
    
    /**
     * Title and attributes
     */
    NSArray *titleAndAttributesArray_;
    
    /**
     * Labels array
     */
    NSMutableArray *labelsArray_;
    
    
    BannerView * bannerView;
    
    
    id<BannerSelectionDelegate> delegate_;

}

/**
 * Provides readwrite access to the dateLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *dateLabel;

/**
 * Provides read-write access to the welcome label and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *welcomeLabel;

/**
 * Provides read-write access to the name label and exports it for Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *nameLabel;

/**
 * Provides readwrite access to the separator. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

/**
 * Provides readwrite access to the nameLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) NSArray *titleAndAttributesArray;


@property (nonatomic, readwrite, assign) id<BannerSelectionDelegate> delegate;

/**
 * Creates and returns an autoreleased GlobalPositionSummaryView constructed from a NIB file
 *
 * @return The autoreleased GlobalPositionSummaryView constructed from a NIB file
 */
+ (GlobalPositionSummaryView *)globalPositionSummaryView;

/**
 * Returns the width of the view
 */
+ (CGFloat)width;

/**
 * Returns the height of the view depending on the amount of info to show
 */
+ (CGFloat)heightForInfo:(NSArray *)info andBannerVisible:(BOOL)isBannerVisible ;


/**
 * Sets the information
 *
 * @param titleAndAttributesArray the information.
 * @param name The user name to display.
 * @param date the date.
 */
- (void)setTitleAndAttributesArray:(NSArray *)titleAndAttributesArray
                           forName:(NSString *)name
                              date:(NSDate *)date
                      andCampaigns:(NSArray*)campaignList
                     andController:(id)controller
                  andBannerVisible:(BOOL)isBannerVisible;



@end
