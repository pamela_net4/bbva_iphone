/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ProductHeaderView.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the ProductHeaderView NIB file name
 */
#define NIB_FILE_NAME                                               @"ProductHeaderView"


#pragma mark -

/**
 * ProductHeaderViewProvider private category
 */
@interface ProductHeaderViewProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (ProductHeaderViewProvider *)getInstance;

/**
 * Creates and returns an autoreleased ProductHeaderView constructed from a NIB file
 *
 * @return The autoreleased ProductHeaderView constructed from a NIB file
 */
- (ProductHeaderView *)productHeaderView;

@end


#pragma mark -

@implementation ProductHeaderViewProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * ProductHeaderViewProvier singleton only instance
 */
static ProductHeaderViewProvider *productHeaderViewProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([ProductHeaderViewProvider class]) {
        
        if (productHeaderViewProviderInstance_ == nil) {
            
            productHeaderViewProviderInstance_ = [super allocWithZone:zone];
            return productHeaderViewProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (ProductHeaderViewProvider *)getInstance {
    
    if (productHeaderViewProviderInstance_ == nil) {
        
        @synchronized([ProductHeaderViewProvider class]) {
            
            if (productHeaderViewProviderInstance_ == nil) {
                
                productHeaderViewProviderInstance_ = [[ProductHeaderViewProvider alloc] init];
                
            }
            
        }
        
    }
    
    return productHeaderViewProviderInstance_;
    
}

#pragma mark -
#pragma mark MovementDetailView creation

/*
 * Creates and returns aa autoreleased ProductHeaderView constructed from a NIB file
 */
- (ProductHeaderView *)productHeaderView {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    ProductHeaderView *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation ProductHeaderView

#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;
@synthesize amountLabel = amountLabel_;
@synthesize separator = separator_;
@dynamic amount;
@dynamic title;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [amountLabel_ release];
    amountLabel_ = nil;
    
    [separator_ release];
    separator_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles the header label
 * and loads the map icon image
 */
- (void)awakeFromNib {
    self.backgroundColor = [UIColor whiteColor];
    
    separator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withBoldFontSize:14.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withBoldFontSize:14.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];

}

/*
 * Creates and returns aa autoreleased ProductHeaderView constructed from a NIB file
 */
+ (ProductHeaderView *)productHeaderView {
    
    return [[ProductHeaderViewProvider getInstance] productHeaderView];
    
}

#pragma mark -
#pragma mark View information

/*
 * Returns the width of the view
 */
+ (CGFloat)width {
    return 320.0f;
}

/*
 * Returns the height of the view
 */
+ (CGFloat)height {
    return 44.0f;
}

#pragma mark -
#pragma mark Properties methods

/**
 * Sets the movement and fills the view
 */
- (void)setAmount:(NSDecimalNumber *)anAmount {
    
    [amountLabel_ setText:[Tools formatAmount:anAmount withCurrency:[Tools mainCurrencyLiteral]]];
     
//     [NSString stringWithFormat:@"%@%@", [Tools mainCurrencySymbol],[Tools formatAmount:anAmount]]];
    
    if ([anAmount compare:[NSDecimalNumber zero]] == NSOrderedAscending) {
        
        [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withBoldFontSize:14.0f color:[UIColor BBVAMagentaColor]];
        
    } else {
        
        [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withBoldFontSize:14.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
        
    }

    amountLabel_.textAlignment = UITextAlignmentRight;
    
}

/**
 * Sets the title
 */
- (void)setTitle:(NSString *)title {

    [titleLabel_ setText:title];

}

@end