/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "OwnAccountView.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the Simple selector account NIB file name
 */
#define NIB_FILE_NAME                                               @"OwnAccountView"

/**
 * Defines the view height
 */
#define VIEW_HEIGHT                                                 47.0f

#pragma mark -

@interface OwnAccountView() 

/**
 * Releases Graphic Elements
 *
 * @private
 */
- (void)releaseOwnAccountGraphicElements;
     

@end

@implementation OwnAccountView

@synthesize backGroundImageView = backGroundImageView_;
@synthesize titleLabel = titleLabel_;
@synthesize bottomInfoLabel = bottomInfoLabel_;
@synthesize ownAccountSwitch = ownAccountSwitch_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseOwnAccountGraphicElements];
    
    [super dealloc];
    
}

/*
 * Releases the memory occupied by the graphic elements.
 */
- (void)releaseOwnAccountGraphicElements {
    
    [backGroundImageView_ release];
    backGroundImageView_ = nil;
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [bottomInfoLabel_ release];
    bottomInfoLabel_ = nil;
    
    [ownAccountSwitch_ release];
    ownAccountSwitch_ = nil;
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the view elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:16.0f color:[UIColor BBVABlackColor]];
    [titleLabel_ setText:NSLocalizedString(OWN_ACCOUNT_LABEL_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleLabel:bottomInfoLabel_ withFontSize:11.0f color:[UIColor BBVAGreyToneTwoColor]];
    [bottomInfoLabel_ setText:NSLocalizedString(BOTTOM_LABEL_OWN_ACCOUNT_TEXT_KEY, nil)];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    UIImage *image = [imagesCache imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
    [backGroundImageView_ setImage:image];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1){
        
        [ownAccountSwitch_ setOnTintColor:[UIColor BBVABlueSpectrumColor]];
        [ownAccountSwitch_ setTintColor: [UIColor BBVABlueSpectrumColor]];
        
        //cambio
        //validando strCTABANCODESTINO
        
        NSString *strCTABANCODESTINO = [[NSUserDefaults standardUserDefaults] objectForKey:@"strCTABANCODESTINO"];
        
        if ([strCTABANCODESTINO isEqualToString:@"SI"]) {
            
            [ownAccountSwitch_ setOn:YES];
        }
        else
        {
            
            [ownAccountSwitch_ setOn:NO];
        }

        
    }
}

/*
 * Creates and returns an autoreleased TimeAndDistanceHeaderView constructed from a NIB file
 */
+ (OwnAccountView *)ownAccountView {
    
    OwnAccountView *result = (OwnAccountView *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [result awakeFromNib]; 
    
    return result;
    
}

#pragma mark -
#pragma mark View information

/*
 * Returns the view height
 */
+ (CGFloat)viewHeight {
    
    return VIEW_HEIGHT;
    
}

@end
