/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

typedef enum {
    swipe_Left = 0,
    swipe_Right
} SwipeDirection;

@protocol NXTSwipeGestureRecognizerDelegate

/**
 * Notifies that a swipe gesture has been done
 */
- (void)swipeGestureDetected:(SwipeDirection)direction;

@end

/**
 * Recognizes a swipe gesture on the given view
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTSwipeGestureRecognizer : NSObject {
@private
    /**
     * View 
     */
    UIView *view_;
    	
	/**
	 * Touch original point
	 */
	CGPoint startTouchPosition_;
	
	/**
	 * Original touch began information set
	 */
	NSSet* originalTouchDownSet_;
    
	/**
	 * Can notify listener flag. YES to allow listener notifications, NO otherwise
	 */
	BOOL canNotifyListener_;
    
    /**
     * Flag that indicates that a swipe has already been notified
     */
    BOOL notified_;
    
    /**
     * Delegate of the gesture recognizer
     */
    id<NXTSwipeGestureRecognizerDelegate> delegate_;
}

/**
 * Provides readwrite access to the view
 */
@property (nonatomic, readwrite, assign) UIView *view;

/**
 * Provides readwrite access to the delegate
 */
@property (nonatomic, readwrite, assign) id<NXTSwipeGestureRecognizerDelegate> delegate;

/**
 * Designated initializer. This class can be instantiated only with this method
 *
 * @param view in wich we want detect gestures
 */
- (id)initWithView:(UIView *)view;

/**
 * Tells the receiver when one or more fingers touch down in a view or window. If ther is only one touch, the notify listener
 * flag is enabled and the origin touch point is stored
 *
 * @param touches A set of UITouch instances that represent the touches for the starting phase of the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void) touchesBegan: (NSSet*) touches withEvent: (UIEvent*) event;

/**
 * Tells the receiver when one or more fingers associated with an event move within a view or window. When notification is
 * enabled, checks the current positions from original touch position and if it meets the requirements, notifies the listener
 *
 * @param touches A set of UITouch instances that represent the touches that are moving during the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void) touchesMoved: (NSSet*) touches withEvent: (UIEvent*) event;

/**
 * Tells the receiver when one or more fingers are raised from a view or window. Disables the listener notifications
 *
 * @param touches A set of UITouch instances that represent the touches for the ending phase of the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void) touchesEnded: (NSSet*) touches withEvent: (UIEvent*) event;

/**
 * Sent to the receiver when a system event (such as a low-memory warning) cancels a touch event. DIsables the listener notifications
 *
 * @param touches A set of UITouch instances that represent the touches for the ending phase of the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void) touchesCancelled: (NSSet*) touches withEvent: (UIEvent*) event;

@end
