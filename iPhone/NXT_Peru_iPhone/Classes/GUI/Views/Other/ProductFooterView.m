/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ProductFooterView.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the ProductFooterView NIB file name
 */
#define NIB_FILE_NAME                                               @"ProductFooterView"


#pragma mark -

/**
 * ProductFooterViewProvider private category
 */
@interface ProductFooterViewProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (ProductFooterViewProvider *)getInstance;

/**
 * Creates and returns an autoreleased ProductFooterView constructed from a NIB file
 *
 * @return The autoreleased ProductFooterView constructed from a NIB file
 */
- (ProductFooterView *)productFooterView;

@end


#pragma mark -

@implementation ProductFooterViewProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * ProductFooterViewProvier singleton only instance
 */
static ProductFooterViewProvider *productFooterViewProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([ProductFooterViewProvider class]) {
        
        if (productFooterViewProviderInstance_ == nil) {
            
            productFooterViewProviderInstance_ = [super allocWithZone:zone];
            return productFooterViewProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (ProductFooterViewProvider *)getInstance {
    
    if (productFooterViewProviderInstance_ == nil) {
        
        @synchronized([ProductFooterViewProvider class]) {
            
            if (productFooterViewProviderInstance_ == nil) {
                
                productFooterViewProviderInstance_ = [[ProductFooterViewProvider alloc] init];
                
            }
            
        }
        
    }
    
    return productFooterViewProviderInstance_;
    
}

#pragma mark -
#pragma mark MovementDetailView creation

/*
 * Creates and returns aa autoreleased ProductFooterView constructed from a NIB file
 */
- (ProductFooterView *)productFooterView {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    ProductFooterView *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation ProductFooterView

#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;
@synthesize separator = separator_;
@dynamic title;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [separator_ release];
    separator_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles the Footer label
 * and loads the map icon image
 */
- (void)awakeFromNib {
	
	[super awakeFromNib];
	
	[self setBackgroundColor:[UIColor BBVAGreyToneFiveColor]];
    
    separator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:12.0f color:[UIColor BBVAGreyColor]];
    titleLabel_. numberOfLines = 8;

}

/*
 * Creates and returns aa autoreleased ProductFooterView constructed from a NIB file
 */
+ (ProductFooterView *)productFooterView {
    
    return [[ProductFooterViewProvider getInstance] productFooterView];
    
}

#pragma mark -
#pragma mark View information

/*
 * Returns the width of the view
 */
+ (CGFloat)width {
    return 320.0f;
}

/*
 * Returns the height of the view
 */
+ (CGFloat)heightForText:(NSString *)text {
    
    CGFloat result;
    
    if ((text == nil) || ([text isEqualToString:@""])) {
        
        result = 0.0f;
        
    } else {
    

        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15.0f, 3.0f, 289.0f, 34.0f)];
                
        result = [Tools labelHeight:label
                            forText:text];
        
        result = result + 20.0f;
        
        [label release];
        
    }
    
    return result;
}

#pragma mark -
#pragma mark Properties methods

/**
 * Sets the title
 */
- (void)setTitle:(NSString *)title {

    [titleLabel_ setText:title];

}

@end