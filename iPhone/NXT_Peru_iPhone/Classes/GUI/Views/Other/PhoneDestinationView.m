//
//  PhoneDestinationView.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 10/3/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "PhoneDestinationView.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the Simple selector account NIB file name
 */
#define NIB_FILE_NAME                                               @"PhoneDestinationView"

/**
 * Defines the view height
 */
#define VIEW_HEIGHT                                                 47.0f

/**
 * Defines the cell height
 */
#define FONT_SIZE                                           16.0f

/**
 * Defines the cell height
 */
#define FONT_SIZE_DETAIL                                    11.0f

@interface PhoneDestinationView ()

/**
 * Releases Graphic Elements
 *
 * @private
 */
- (void)releasePhoneDestinationGraphicElements;

@end

@implementation PhoneDestinationView

@synthesize backgroundImageView = backgroundImageView_;
@synthesize lblTituloPhoneNumber = titleLabel_;
@synthesize lblIndication = subTitleLabel_;
@synthesize btnContacts = firstAddButton_;
@synthesize phoneNumberTextField = firstTextField_;
@synthesize phoneNumber = phone1_;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releasePhoneDestinationGraphicElements];
    
    
    [super dealloc];
    
}

/*
 * Releases the memory occupied by the graphic elements.
 */
- (void)releasePhoneDestinationGraphicElements {
    
    [backgroundImageView_ release];
    backgroundImageView_ = nil;
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [subTitleLabel_ release];
    subTitleLabel_ = nil;
    
    [firstTextField_ release];
    firstTextField_ = nil;
    
    [firstAddButton_ release];
    firstAddButton_ = nil;
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the view elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    
    backgroundImageView_.image = [[ImagesCache getInstance] imageNamed:ALERT_BOX_IMAGE_FILE_NAME];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:FONT_SIZE color:[UIColor BBVABlueColor]];
    [titleLabel_ setText:NSLocalizedString(SEND_PHONE_DESTINARY_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleLabel:subTitleLabel_ withFontSize:FONT_SIZE_DETAIL color:[UIColor BBVABlackColor]];
    [subTitleLabel_ setText:NSLocalizedString(SEND_PHONE_INDICATION_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleTextField:firstTextField_ withFontSize:FONT_SIZE andColor:[UIColor BBVAGreyColor]];
    [firstTextField_ setPlaceholder:NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_FILL_KEY, nil)];
    
    
}

/*
 * Creates and returns an autoreleased TimeAndDistanceHeaderView constructed from a NIB file
 */
+ (PhoneDestinationView *)phoneDestinationView {
    
    PhoneDestinationView *result = (PhoneDestinationView *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark - user interaction
/**
 * Add First Contact Button Tapped
 */
- (void)firstAddButtonTapped {
    
    [firstTextField_ becomeFirstResponder];
    [firstTextField_ resignFirstResponder];
    [delegate_ addPhoneContactTapped];
}


@end
