/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>

@class NXTTextField;

/**
 * 
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface OwnAccountView : UIView <UITextFieldDelegate> {
    
@private
    
    /*
     *background image view
     */
    UIImageView *backGroundImageView_;
    
    /*
     *title label
     */
    UILabel *titleLabel_;
    
    /*
     *bottom info label
     */
    UILabel *bottomInfoLabel_;
    
    /*
     *switch control
     */
    UISwitch *ownAccountSwitch_;
    
}

/**
 * Provides readwrite access to the backGround image. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIImageView *backGroundImageView;

/**
 * Provides readwrite access to the title label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;

/**
 * Provides readwrite access to the bottom info label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *bottomInfoLabel;

/**
 * Provides readwrite access to the switch control. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UISwitch *ownAccountSwitch;

/**
 * Creates and returns an autoreleased OwnAccountView constructed from a NIB file
 *
 * @return The autoreleased OwnAccountView constructed from a NIB file
 */
+ (OwnAccountView *)ownAccountView;

@end
