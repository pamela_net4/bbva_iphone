/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>
#import "SingletonBase.h"

@class SimpleHeaderView;

/**
 * Provider to obtain a SimpleHeaderView created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface SimpleHeaderViewProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary SimpleHeaderView to create it from a NIB file
     */
    SimpleHeaderView *auxView_;
    
}

/**
 * Provides read-write access to the auxililary SimpleHeaderView
 */
@property (nonatomic, readwrite, assign) IBOutlet SimpleHeaderView *auxView;

@end

/**
 * Simple header with the concept, date and amount
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface SimpleHeaderView : UIView {
@private
    
    /**
     * Label of the concept of the transaction
     */
    UILabel *titleLabel_;
    
    /**
     * Separator line
     */
    UIImageView *separator_;

}

/**
 * Provides readwrite access to the titleLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides readwrite access to the separator1. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

/**
 * Provides readwrite access to the title
 */
@property (nonatomic, readwrite, copy) NSString *title;


/**
 * Creates and returns an autoreleased SimpleHeaderView constructed from a NIB file
 *
 * @return The autoreleased SimpleHeaderView constructed from a NIB file
 */
+ (SimpleHeaderView *)simpleHeaderView;

/**
 * Returns the width of the view
 */
+ (CGFloat)width;

/**
 * Returns the height of the view
 */
+ (CGFloat)height;

@end
