//
//  RadioPlayerFooterView.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 12/31/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RadioPlayerFooterViewDelegate<NSObject>

-(void)goPlayerAction;
-(void)playRadioAction;

@end

@class VisualEqualizerView;

@interface RadioPlayerFooterView : UIView{
    UIButton *goPlayerButton_;
    UIButton *playRadioButton_;
    UILabel *titleTrackInfoLabel_;
    id<RadioPlayerFooterViewDelegate> delegate_;
    VisualEqualizerView *equalizerSmallView_;
}
@property(nonatomic,assign) id<RadioPlayerFooterViewDelegate> delegate;
@property(nonatomic,readwrite,retain) IBOutlet UIButton *goPlayerButton;
@property(nonatomic,readwrite,retain) IBOutlet UIButton *playRadioButton;
@property(nonatomic,readwrite,retain) IBOutlet UILabel *titleTrackInfoLabel;

+ (RadioPlayerFooterView *)radioPlayerFooterView;

-(void)activeRadioPlaying;

-(void)deactiveRadioPlaying;

-(void)setTitleTrackInfo:(NSString*)titleTrack;

+(CGFloat)footerHeight;

-(IBAction)playRadioButton:(id)sender;

-(IBAction)goPlayerButton:(id)sender;

-(void)setValuesForEqualizer:(NSArray*)values;
@end
