
#import "Tooltip.h"

#define GRAY_187 [UIColor colorWithRed:187.0f/255.0f green:187.0f/255.0f blue:187.0f/255.0f alpha:0.001f]
#define DRAPE_GRAY [UIColor colorWithWhite:0/255.0f alpha:0.001]

static const CGFloat kTriangleDimensions = 7.0f;
static const CGFloat kBoundingOffset = 7.0f;

@interface Tooltip ()

@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) UIWindow *parentWindow;
@property (assign, nonatomic) CGRect sourceRect;
@property (strong, nonatomic) UIButton *drapeButton;
@property (assign, nonatomic) BOOL expandsToTop;

@property (assign, nonatomic) CGFloat leftInsetLimit;
@property (assign, nonatomic) CGFloat rightInsetLimit;

@property (strong, nonatomic) UIView *rootVC;
@property (strong, nonatomic) UIView *fromView;

@end

@implementation Tooltip{
    UIView *contentView_;
    UILabel *label_;
    UIView * fromView_;
}

@synthesize arrowColor = arrowColor_;
@synthesize borderColor = borderColor_;
@synthesize textColor = textColor_;
@synthesize contentView = contentView_;
@synthesize label = label_;
@synthesize fromView = fromView_;

- (id)initWithMessage:(NSString *)message source:(UIView*)fromView parentWindow:(UIWindow *)parentWindow
{
    self = [super init];
    if (self) {
        
        float padding = 5.0;
        
        UIView * container = [UIView new];
        container.frame = CGRectMake(0.0f, 0.0f, 240.0f, 50.0f);
        container.layer.borderWidth = 1;
        
        label_ = [UILabel new];
        label_.textColor = [UIColor whiteColor];
        label_.frame = CGRectMake(0.0f, 0.0f, 240.0f - 2 * padding, 50.0f);
        label_.text = message;
        label_.numberOfLines = 0;
        label_.font = [UIFont systemFontOfSize:12];
        
        label_.layer.borderColor = [[UIColor clearColor] CGColor];
        label_.backgroundColor = [UIColor clearColor];
        
        [container addSubview:label_];
        
        CGSize sizeText = [label_.text sizeWithFont:label_.font
                                 constrainedToSize:CGSizeMake(label_.frame.size.width, CGFLOAT_MAX)
                                     lineBreakMode:NSLineBreakByWordWrapping];
        label_.frame = CGRectMake(padding, padding, 240.0f - 2 * padding, sizeText.height);
        container.frame = CGRectMake(0.0f, 0.0f, 240.0f, sizeText.height + 2 * padding);
        
        fromView_ = fromView;
        contentView_ = container;
        _sourceRect = fromView_.frame;
        
        CGPoint windowPoint = [fromView convertPoint:fromView.bounds.origin toView:[UIApplication sharedApplication].keyWindow];
        _sourceRect.origin = windowPoint;
        
        _parentWindow = parentWindow;
        
        [self _init];
    }
    return self;
}

- (void)_init
{
    _expandsToTop = YES;
    _rootVC = [UIApplication sharedApplication].keyWindow.rootViewController.view;
    
    [self addSubview:self.contentView];
    
    CGRect drapeButtonRect = [_rootVC convertRect:_rootVC.frame toView:self];
    self.drapeButton = [[UIButton alloc] initWithFrame:drapeButtonRect];
    self.drapeButton.backgroundColor = DRAPE_GRAY;
    [self.drapeButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    
    [_parentWindow insertSubview:self.drapeButton belowSubview:self];
    
    [self checkExpandingDirection];
    [self adjustXPosition];
}

#pragma mark - Setters

- (void)setHideShadow:(BOOL)hideShadow
{
    _hideShadow = hideShadow;
    self.drapeButton.backgroundColor = hideShadow ? [UIColor clearColor] : DRAPE_GRAY;
}

#pragma mark - Position Calculations

- (void)checkExpandingDirection
{
    if ((CGRectGetMaxY(self.sourceRect) +
         CGRectGetHeight(self.contentView.frame) +
         kTriangleDimensions + 10 > CGRectGetHeight(self.rootVC.frame)))
    {
        self.expandsToTop = YES;
        self.frame = CGRectMake(CGRectGetMinX(self.sourceRect) - ((CGRectGetWidth(self.contentView.frame) - CGRectGetWidth(self.sourceRect)) / 2),
                    CGRectGetMinY(self.sourceRect) - CGRectGetHeight(self.contentView.frame) - kTriangleDimensions,
                    CGRectGetWidth(self.contentView.frame),
                    CGRectGetHeight(self.contentView.frame));//y
    }
    
    else
    {
        self.expandsToTop = NO;
        self.frame = CGRectMake(CGRectGetMinX(self.sourceRect) - ((CGRectGetWidth(self.contentView.frame) - CGRectGetWidth(self.sourceRect)) / 2),
                    CGRectGetMaxY(self.sourceRect) + kTriangleDimensions,//y
                    CGRectGetWidth(self.contentView.frame),
                    CGRectGetHeight(self.contentView.frame));
    }
}

- (void)adjustXPosition
{
    //Arrange x position of the view for best position on screen
    CGRect frameWindow =[[[UIApplication sharedApplication] keyWindow] frame];
    
    CGFloat leftOver = CGRectGetMinX(self.frame) - CGRectGetMinX(frameWindow);
    CGFloat rightOver = CGRectGetMaxX(frameWindow) - CGRectGetMaxX(self.frame);
    
    self.leftInsetLimit = leftOver < kBoundingOffset ? leftOver : 0;
    self.rightInsetLimit = rightOver < -kBoundingOffset ? rightOver : 0;
    
    self.frame = self.leftInsetLimit < kBoundingOffset && self.leftInsetLimit != 0 ?
                        CGRectMake(
                                   CGRectGetMinX(self.frame) - self.leftInsetLimit + kBoundingOffset,
                                   CGRectGetMinY(self.frame),
                                   CGRectGetWidth(self.frame),
                                   CGRectGetHeight(self.frame)) : self.frame;
    
    self.frame = self.rightInsetLimit < -kBoundingOffset && self.rightInsetLimit != 0 ?
                    CGRectMake(
                               CGRectGetMinX(self.frame) + self.rightInsetLimit - kBoundingOffset,
                               CGRectGetMinY(self.frame),
                               CGRectGetWidth(self.frame),
                               CGRectGetHeight(self.frame)) : self.frame;
}

#pragma mark - Draw Borders

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    self.alpha = 0.96;
    
    label_.textColor = self.textColor ? self.textColor : [UIColor blackColor];
    contentView_.backgroundColor = self.arrowColor ? self.arrowColor : [UIColor whiteColor];
    contentView_.layer.borderColor = self.borderColor ? self.borderColor.CGColor : [GRAY_187 CGColor];
    
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path,NULL,0.0,0.0);
    
    CGRect intentedSourceRect = [self.superview convertRect:self.sourceRect toView:self];
    
    if (self.expandsToTop)
    {
        CGPathAddLineToPoint(path, NULL, CGRectGetWidth(self.frame), 0.0f);
        CGPathAddLineToPoint(path, NULL, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
        CGPathAddLineToPoint(path, NULL, CGRectGetMidX(intentedSourceRect) + kTriangleDimensions, CGRectGetHeight(self.frame));
        CGPathAddLineToPoint(path, NULL, CGRectGetMidX(intentedSourceRect), CGRectGetHeight(self.frame) + kTriangleDimensions);
        CGPathAddLineToPoint(path, NULL, CGRectGetMidX(intentedSourceRect) - kTriangleDimensions, CGRectGetHeight(self.frame));
        CGPathAddLineToPoint(path, NULL, 0.0f, CGRectGetHeight(self.frame));
        CGPathAddLineToPoint(path, NULL, 0.0f, 0.0f);
    }
    
    else
    {
        CGPathAddLineToPoint(path, NULL, CGRectGetMidX(intentedSourceRect) - kTriangleDimensions, 0.0f);
        CGPathAddLineToPoint(path, NULL, CGRectGetMidX(intentedSourceRect), - kTriangleDimensions);
        CGPathAddLineToPoint(path, NULL, CGRectGetMidX(intentedSourceRect) + kTriangleDimensions, 0.0f);
        CGPathAddLineToPoint(path, NULL, CGRectGetWidth(self.frame), 0.0f);
        CGPathAddLineToPoint(path, NULL, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
        CGPathAddLineToPoint(path, NULL, 0.0f, CGRectGetHeight(self.frame));
        CGPathAddLineToPoint(path, NULL, 0.0f, 0.0f);
    }
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setPath:path];
    [shapeLayer setFillColor:self.arrowColor ? self.arrowColor.CGColor : [[UIColor whiteColor] CGColor]];
    [shapeLayer setStrokeColor:self.borderColor ? self.borderColor.CGColor : [GRAY_187 CGColor]];
    [shapeLayer setAnchorPoint:CGPointMake(0.0f, 0.0f)];
    [shapeLayer setPosition:CGPointMake(0.0f, 0.0f)];
    [self.layer addSublayer:shapeLayer];
    
    CGPathRelease(path);
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self addSubview:self.contentView];
}

-(void)dealloc{
    
    [contentView_ release];
    contentView_ = nil;
    
    [label_ release];
    label_ = nil;
    
    [arrowColor_ release];
    arrowColor_ = nil;
    
    [borderColor_ release];
    borderColor_ = nil;
    
    [textColor_ release];
    textColor_ = nil;
    
    [fromView_ release];
    fromView_ = nil;
    
    [super dealloc];
}

#pragma mark - Public Methods

- (void)show
{
    [self.parentWindow addSubview:self];
}

- (void)dismiss
{
    [self.drapeButton removeFromSuperview];
    [self removeFromSuperview];
    
    if(self.delegate)
        [self.delegate onDismiss:fromView_];
}

@end
