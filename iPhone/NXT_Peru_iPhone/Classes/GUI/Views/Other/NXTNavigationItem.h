/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * Double lable view to show as a navigation item title
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTNavigationItemTitleView : UIView {
    
@private
    
    /**
     * Top label
     */
    UILabel *topLabel_;
    
    /**
     * Bottom label
     */
    UILabel *bottomLabel_;

}

/**
 * Provides read-write access to the top label text
 */
@property (nonatomic, readwrite, copy) NSString* topLabelText;

/**
 * Provides read-write access to the bottom label text
 */
@property (nonatomic, readwrite, copy) NSString* bottomLabelText;

@end


/**
 * Navigation item to show a double label view as a title. The top label is white, and the bottom one blue. When not
 * displaying the second label, the first one appears centered. The text size is shrank when a given size is surpassed
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTNavigationItem : UINavigationItem {

@private
    
    /**
     * The item title view
     */
    NXTNavigationItemTitleView *customTitleView_;
    
}

/**
 * Provides read-only access to the title view
 */
@property (nonatomic, readonly, retain) NXTNavigationItemTitleView *customTitleView;

@end
