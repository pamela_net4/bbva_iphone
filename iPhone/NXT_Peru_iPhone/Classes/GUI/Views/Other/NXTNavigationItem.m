/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTNavigationItem.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Defines the top label text size
 */
#define TOP_LABEL_TEXT_SIZE                                     15.0f

/**
 * Defines the bottom label text size
 */
#define BOTTOM_LABEL_TEXT_SIZE                                  13.0f

/**
 * Defines the view width (measured in points)
 */
#define VIEW_WIDTH                                              200.0f

/**
 * Defines the view height (measured in points)
 */
#define VIEW_HEIGHT                                             32.0f


#pragma mark -

/**
 * NXTNavigationItem private category
 */
@interface NXTNavigationItem(private)

/**
 * Initializes the item elements
 */
- (void)initializeElements;

@end


#pragma mark -

@implementation NXTNavigationItemTitleView

#pragma mark -
#pragma mark Properties

@dynamic topLabelText;
@dynamic bottomLabelText;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc{

    [topLabel_ release];
    topLabel_ = nil;
    
    [bottomLabel_ release];
    bottomLabel_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. The view labels are created
 *
 * @param frame The view original frame
 * @return The initialized NXTNaviationItemTitleView instance
 */
- (id)initWithFrame:(CGRect)frame {
    
    frame.size.height = VIEW_HEIGHT;
    frame.size.width = VIEW_WIDTH;
    
    CGFloat halfHeight = VIEW_HEIGHT / 2.0f;
    
    if (self = [super initWithFrame:frame]) {
        
        UIColor *clearColor = [UIColor clearColor];
        
        self.backgroundColor = clearColor;
        topLabel_ = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, VIEW_WIDTH, halfHeight)];
        [NXT_Peru_iPhoneStyler styleLabel:topLabel_ withBoldFontSize:TOP_LABEL_TEXT_SIZE color:[UIColor BBVAWhiteColor]];
        topLabel_.backgroundColor = clearColor;
        topLabel_.textAlignment = UITextAlignmentCenter;
        [self addSubview:topLabel_];
        
        bottomLabel_ = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, halfHeight, VIEW_WIDTH, halfHeight)];
        [NXT_Peru_iPhoneStyler styleLabel:bottomLabel_ withFontSize:BOTTOM_LABEL_TEXT_SIZE color:[UIColor whiteColor]];
        bottomLabel_.backgroundColor = clearColor;
        bottomLabel_.textAlignment = UITextAlignmentCenter;
        [self addSubview:bottomLabel_];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark UIView methods

/**
 * Lays out subviews
 */
- (void)layoutSubviews {

    NSString *bottomText = bottomLabel_.text;
    
    NSUInteger bottomTextLenght = [bottomText length];
    
    CGRect topLabelFrame = topLabel_.frame;

    if (bottomTextLenght == 0) {
        
        topLabelFrame.origin.y = 7.0f;
        topLabel_.frame = topLabelFrame;
        bottomLabel_.hidden = YES;
        
    } else {
        
        topLabelFrame.origin.y = 0.0f;
        topLabel_.frame = topLabelFrame;
        bottomLabel_.hidden = NO;

    }
    
}

/**
 * Sets the view frame. This view size is fixed, so the rectangle is updated to maintain that size
 *
 * @param frame The new view frame
 */
- (void)setFrame:(CGRect)frame {
    
    frame.size.width = VIEW_WIDTH;
    frame.size.height = VIEW_HEIGHT;
    
    [super setFrame:frame];
    
}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the top label text
 *
 * @return The top label text
 */
- (NSString *)topLabelText {
    
    return topLabel_.text;
    
}

/**
 * Sets the top label text and mark the view to layout
 *
 * @param aText The new top label text
 */
- (void)setTopLabelText:(NSString *)aText {
    
    topLabel_.text = aText;
    
    [self setNeedsLayout];
    
}

/*
 * Returns the bottom label text
 *
 * @return The bottom label text
 */
- (NSString *)bottomLabelText {
    
    return bottomLabel_.text;
    
}

/**
 * Sets the botom label text and mark the view to layout
 *
 * @param aText Th new bottom label text
 */
- (void)setBottomLabelText:(NSString *)aText {
    
    bottomLabel_.text = aText;
    [self setNeedsLayout];
    
}

@end


#pragma mark -

@implementation NXTNavigationItem

#pragma mark -
#pragma mark Properties

@synthesize customTitleView = customTitleView_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [customTitleView_ release];
    customTitleView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initializes the NXTNavigationItem elements
 */
- (void)awakeFromNib {
    
    [self initializeElements];
    
}

/**
 * Superclass designated initializer. It initializes the NXTNavigationItem elements
 *
 * @param title The string to set as the navigation item’s title displayed in the center of the navigation bar
 * @return The initialized NXTNavigationItem instance
 */
- (id)initWithTitle:(NSString *)title {

    if (self = [super initWithTitle:title]) {
        
        [self initializeElements];

        
    }
    
    return self;
    
}

/*
 * Initializes the item elements
 */
- (void)initializeElements {
    
    if (customTitleView_ == nil) {
        
        customTitleView_ = [[NXTNavigationItemTitleView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 0.0f)];
        
    }
    
    self.titleView = customTitleView_;
    
}

@end
