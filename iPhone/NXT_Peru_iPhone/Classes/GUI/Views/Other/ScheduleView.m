//
//  ScheduleView.m
//  Created by Author.
//

#import "ScheduleView.h"
#import <QuartzCore/QuartzCore.h>
#import "NPopup.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"
#import "Tooltip.h"
#import "FastLoanQuotaList.h"
#import "FastLoanQuota.h"
#import "FastLoanScheduleCell.h"

#define MAX_HEIGHT_VIEW 546
#define DEFAULT_HEIGHT_TABLE 264

@implementation ScheduleView

@synthesize view = view_;
@synthesize footerBackgroundView = footerBackgroundView_;
@synthesize degravamenLabel = degravamenLabel_;
@synthesize tableView = tableView_;
@synthesize fastLoanSchedule = fastLoanSchedule_;
@synthesize item1View = item1View_;
@synthesize item2View = item2View_;
@synthesize item3View = item3View_;
@synthesize item4View = item4View_;
@synthesize item5View = item5View_;

-(void)dealloc{
    
    [fastLoanSchedule_ release];
    fastLoanSchedule_ = nil;
    
    [degravamenLabel_ release];
    degravamenLabel_ = nil;
    
    [footerBackgroundView_ release];
    footerBackgroundView_ = nil;
    
    [tableView_ release];
    tableView_ = nil;
    
    [view_ release];
    view_ = nil;
    
    [item1View_ release];
    item1View_ = nil;
    
    [item2View_ release];
    item2View_ = nil;
    
    [item3View_ release];
    item3View_ = nil;
    
    [item4View_ release];
    item4View_ = nil;
    
    [item5View_ release];
    item5View_ = nil;
    
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code              
        [[NSBundle mainBundle] loadNibNamed:@"ScheduleView" owner:self options:nil];
        [self addSubview: self.view];
        
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        
        CGRect frame = self.frame;
        frame.size.height = MIN(screenHeight - 10, MAX_HEIGHT_VIEW);
        self.frame = frame;
        
        frame = self.view.frame;
        frame.size.height = MIN(screenHeight - 10, MAX_HEIGHT_VIEW);
        self.view.frame = frame;
        
        UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
        if (UIDeviceOrientationIsLandscape(deviceOrientation)) {
            CGFloat tmp = screenWidth;
            screenWidth = screenHeight;
            screenHeight = tmp;
        }
        self.frame = CGRectMake(screenWidth/2 - CGRectGetWidth(self.view.bounds)/2,
                                screenHeight/2 - CGRectGetHeight(self.view.bounds)/2,
                                CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
        
        //awake from nib
    }
    return self;
}


- (void) layoutSubviews{   
    [super layoutSubviews];
    
    CGFloat maxHeightScreen = MAX_HEIGHT_VIEW + 10;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat diffScreenHeight = maxHeightScreen - screenHeight;
    CGFloat heigtTable = screenHeight < maxHeightScreen ? (DEFAULT_HEIGHT_TABLE - diffScreenHeight) : DEFAULT_HEIGHT_TABLE;
    
    CGRect frame = tableView_.frame;
    frame.size.height = heigtTable;
    tableView_.frame = frame;
    
    //styles
    self.view.layer.cornerRadius = 10;
    self.view.layer.masksToBounds = YES;
    
    self.footerBackgroundView.layer.cornerRadius = 10;
    self.footerBackgroundView.layer.masksToBounds = YES;

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorColor = [UIColor clearColor];
    
    self.item1View.layer.cornerRadius = 3;
    self.item1View.layer.masksToBounds = YES;
    
    self.item2View.layer.cornerRadius = 3;
    self.item2View.layer.masksToBounds = YES;
    
    self.item3View.layer.cornerRadius = 3;
    self.item3View.layer.masksToBounds = YES;
    
    self.item4View.layer.cornerRadius = 3;
    self.item4View.layer.masksToBounds = YES;
    
    self.item5View.layer.cornerRadius = 3;
    self.item5View.layer.masksToBounds = YES;
    
    if(fastLoanSchedule_ && [fastLoanSchedule_.fastLoanQuotaList fastloanquotaCount]>0){
        FastLoanQuota * fastLoanQuota = [fastLoanSchedule_.fastLoanQuotaList fastloanquotaAtPosition:0];
        if(fastLoanQuota){
            degravamenLabel_.text = [NSString stringWithFormat:
                                     @"El monto de la cuota incluye el pago de seguro de desgravamen (%@ %@ mensuales).",
                                     fastLoanSchedule_.currencySymbol,
                                     fastLoanQuota.commissionSent];
        }
    }
}

#pragma UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    int count = (int)[fastLoanSchedule_.fastLoanQuotaList fastloanquotaCount];
    if(count<7){
        return (count + 1);
    }
    else{
        return 11;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FastLoanScheduleCell *cell = (FastLoanScheduleCell *)[tableView dequeueReusableCellWithIdentifier:[FastLoanScheduleCell cellIdentifier]];
    
    if (cell == nil) {
        cell = [FastLoanScheduleCell FastLoanScheduleCell];
    }
    
    cell.separatorView1.backgroundColor = [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1.0];
    cell.separatorView2.backgroundColor = [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1.0];
    cell.separatorView3.backgroundColor = [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1.0];
    cell.separatorView4.backgroundColor = [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1.0];
    cell.separatorView5.backgroundColor = [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1.0];
    
    if(indexPath.row == 0){
        
        cell.textLabel1.text = @"Mes";
        cell.textLabel2.text = @"Día";
        cell.textLabel3.text = @"Amortización";
        cell.textLabel4.text = @"Interés";
        cell.textLabel5.text = @"Cuota";
        cell.textLabel6.text = @"Saldo";
        
        cell.backgroundColor = [UIColor colorWithRed:17.0/255.0 green:138.0/255.0 blue:222.0/255.0 alpha:1.0];
        
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel1 withFontSize:10.0f color:[UIColor whiteColor]];
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel2 withFontSize:10.0f color:[UIColor whiteColor]];
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel3 withFontSize:10.0f color:[UIColor whiteColor]];
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel4 withFontSize:10.0f color:[UIColor whiteColor]];
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel5 withFontSize:10.0f color:[UIColor whiteColor]];
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel6 withFontSize:10.0f color:[UIColor whiteColor]];
    }
    else{
        
        cell.backgroundColor = [UIColor whiteColor];
        
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel1 withFontSize:11.0f color:[UIColor colorWithRed:49.0/255.0 green:49.0/255.0 blue:49.0/255.0 alpha:1.0]];
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel2 withFontSize:11.0f color:[UIColor colorWithRed:49.0/255.0 green:49.0/255.0 blue:49.0/255.0 alpha:1.0]];
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel3 withFontSize:11.0f color:[UIColor colorWithRed:49.0/255.0 green:49.0/255.0 blue:49.0/255.0 alpha:1.0]];
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel4 withFontSize:11.0f color:[UIColor colorWithRed:49.0/255.0 green:49.0/255.0 blue:49.0/255.0 alpha:1.0]];
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel5 withFontSize:11.0f color:[UIColor colorWithRed:49.0/255.0 green:49.0/255.0 blue:49.0/255.0 alpha:1.0]];
        [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel6 withFontSize:11.0f color:[UIColor colorWithRed:49.0/255.0 green:49.0/255.0 blue:49.0/255.0 alpha:1.0]];
        
        
        int count = (int)[fastLoanSchedule_.fastLoanQuotaList fastloanquotaCount];
        int pos = -1;
        if(count<7){
            pos = (int)indexPath.row - 1;
        }
        else{
            if(indexPath.row<=6){
                pos = (int)indexPath.row - 1;
            }
            else if(indexPath.row == 10){
                pos = 6;
            }
        }
        
        if(pos!=-1){
        
            FastLoanQuota * fastLoanQuota = [fastLoanSchedule_.fastLoanQuotaList fastloanquotaAtPosition:pos];
            cell.textLabel1.text = fastLoanQuota.quotaNumber;
            cell.textLabel2.text = fastLoanQuota.expirationDate;
            cell.textLabel3.text = fastLoanQuota.amortization;
            cell.textLabel4.text = fastLoanQuota.interest;
            cell.textLabel5.text = fastLoanQuota.quotaTotal;
            cell.textLabel6.text = fastLoanQuota.balance;
        }
        else{
            cell.separatorView1.backgroundColor = [UIColor clearColor];
            cell.separatorView2.backgroundColor = [UIColor clearColor];
            cell.separatorView3.backgroundColor = [UIColor clearColor];
            cell.separatorView4.backgroundColor = [UIColor clearColor];
            cell.separatorView5.backgroundColor = [UIColor clearColor];
            
            [NXT_Peru_iPhoneStyler styleLabel:cell.textLabel3 withFontSize:11.0f color:[UIColor colorWithRed:17.0/255.0 green:138.0/255.0 blue:222.0/255.0 alpha:1.0]];
            
            cell.textLabel1.text = @"";
            cell.textLabel2.text = @"";
            cell.textLabel3.text = @".";
            cell.textLabel4.text = @"";
            cell.textLabel5.text = @"";
            cell.textLabel6.text = @"";
        }
        
        
        
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0) return 30;
    if(indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 9) return 10;
    return 30;
}

//pop methods

-(IBAction)continueButton:(id)sender{
    if(popup_){
        [popup_ close];
    }
}

-(void)showInWindow:(UIWindow*)parentWindow{
    
    if(!popup_){
        popup_ = [[NPopup alloc] initWithParentWindow:parentWindow];
        popup_.backgroundColor = [UIColor clearColor];
        popup_.dialogView.backgroundColor = [UIColor clearColor];
        [popup_ setContainerView:self];
    }
    [popup_ show];
}


@end
