/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>
#import "SingletonBase.h"

@class ProductFooterView;

/**
 * Provider to obtain a ProductFooterView created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ProductFooterViewProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary ProductFooterView to create it from a NIB file
     */
    ProductFooterView *auxView_;
    
}

/**
 * Provides read-write access to the auxililary ProductFooterView
 */
@property (nonatomic, readwrite, assign) IBOutlet ProductFooterView *auxView;

@end

/**
 * Product Footer with the concept, date and amount
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ProductFooterView : UIView {
@private
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Separator line
     */
    UIImageView *separator_;

}

/**
 * Provides readwrite access to the titleLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides readwrite access to the separator1. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

/**
 * Provides readwrite access to the title
 */
@property (nonatomic, readwrite, copy) NSString *title;

/**
 * Creates and returns an autoreleased ProductFooterView constructed from a NIB file
 *
 * @return The autoreleased ProductFooterView constructed from a NIB file
 */
+ (ProductFooterView *)productFooterView;

/**
 * Returns the width of the view
 */
+ (CGFloat)width;

/**
 * Returns the height of the view
 *
 * @param text The text
 */
+ (CGFloat)heightForText:(NSString *)text;

@end
