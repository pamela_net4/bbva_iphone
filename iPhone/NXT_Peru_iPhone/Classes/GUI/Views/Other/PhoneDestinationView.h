//
//  PhoneDestinationView.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 10/3/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NXTTextField.h"

@protocol PhoneDestinationViewDelegate

- (void)addPhoneContactTapped;

@end

@interface PhoneDestinationView : UIView 
{
    /**
     * Background image
     */
    UIImageView *backgroundImageView_;
    
    /**
     * Title label
     */
    UILabel *titleLabel_;
    
    /**
     * Subtitle label
     */
    UILabel *subTitleLabel_;
    
    /**
     * First text field
     */
    NXTTextField *firstTextField_;
    
    /**
     * First add button
     */
    UIButton *firstAddButton_;
    
    /**
     * Phone destination number
     */
    NSString *phone1_;
    
    /**
     * Phone destination bool
     */
    BOOL addingFirstPhone_;
    
    /**
     * phone view delegate
     */
    id<PhoneDestinationViewDelegate> delegate_;
}

/**
 * Provides readwrite access to the backgroundImageView and exports it to the IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *backgroundImageView;

/**
 * Provides readwrite access to the phone title
 */
@property (retain, nonatomic) IBOutlet UILabel *lblTituloPhoneNumber;

/**
 * Provides readwrite access to the optianl label
 */
@property (retain, nonatomic) IBOutlet UILabel *lblIndication;

/**
 * Provides readwrite access to the contacts button
 */
@property (retain, nonatomic) IBOutlet UIButton *btnContacts;

/**
 * Provides readwrite access to the phone number text field
 */
@property (retain, nonatomic) IBOutlet NXTTextField *phoneNumberTextField;

/**
 * Provides readwrite access to the phone number text
 */
@property (retain, nonatomic) NSString *phoneNumber;

/**
 * Provides readwrite access to the delegate
 */
@property (nonatomic, readwrite, assign) id<PhoneDestinationViewDelegate> delegate;



+ (PhoneDestinationView *)phoneDestinationView;

/**
 * First add button tapped
 */
- (IBAction)firstAddButtonTapped;

@end
