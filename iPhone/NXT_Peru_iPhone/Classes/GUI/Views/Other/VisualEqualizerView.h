//
//  VisualEqualizerView.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 1/4/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>


#define SEPARATOR_SMALL 1
#define SEPARATOR_NORMAL 2

@interface VisualEqualizerView : UIView{
    
    BOOL enable_;
}
@property(nonatomic,assign) BOOL enable;
-(id)initWithFrame:(CGRect)frame withSeparator:(NSInteger)separator;
-(void)loadDefaultLevels;
-(void)setValuesForEqualizer:(NSArray*)values;

@end
