/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * View to display the text from a route step
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface RouteStepView : UIView {

@private
    
    /**
     * Background image view
     */
    UIImageView *backgroundImageView_;
	
	/**
	 * The separator image view.
	 */
	UIImageView *separatorImageView_;
    
    /**
     * Step number and total label
     */
    UILabel *stepNumberAndTotalLabel_;
    
    /**
     * Step text label
     */
    UILabel *stepTextLabel_;
    
    /**
     * Steps segmented control
     */
    UISegmentedControl *stepsSegmentedControl_;
    
    /**
     * Route steps count
     */
    NSUInteger routeStepsCount_;
    
    /**
     * Step index
     */
    NSUInteger stepIndex_;
    
}


/**
 * Provides read-write access to the background image view and total label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *backgroundImageView;

/**
 * Provides read-write access to separatorImageView. Connected to Interface Builder.
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separatorImageView;

/**
 * Provides read-write access to the step number and total label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *stepNumberAndTotalLabel;

/**
 * Provides read-write access to the step text label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *stepTextLabel;

/**
 * Provides read-write access to the teps segmented control and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UISegmentedControl *stepsSegmentedControl;

/**
 * Provides read-write access to the route steps count
 */
@property (nonatomic, readwrite, assign) NSUInteger routeStepsCount;

/**
 * Provides read-write access to the step index
 */
@property (nonatomic, readwrite, assign) NSUInteger stepIndex;

/**
 * Provides read-write access to the step text
 */
@property (nonatomic, readwrite, copy) NSString *stepText;


/**
 * Creates and returns an autoreleased RouteStepView constructed from a NIB file
 *
 * @return The autoreleased RouteStepView constructed from a NIB file
 */
+ (RouteStepView *)routeStepView;

/**
 * Sets the step text, and the view resizing can be animated or not
 *
 * @param aStepText The new step text to set
 * @param animated YES to animate the view resizing, NO otherwise
 */
- (void)setStepText:(NSString *)aStepText
           animated:(BOOL)animated;

@end
