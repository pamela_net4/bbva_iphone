//
//  FrequentOperationHeaderView.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/10/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentOperationHeaderView.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "FOPaymentCOtherBank.h"

/**
 * Define the SimpleHeaderView NIB file name
 */
#define NIB_FILE_NAME                                               @"FrequentOperationHeaderView"

/**
 * Define the font size for title
 */
#define FONT_SIZE_TITLE                                             14.0f
/**
 * Define the font size for subtitles
 */
#define FONT_SIZE_SUBTITLE                                          14.0f

#define SMALL_FONT_SIZE                                             11.0f

#define TEXT_FONT_SIZE                                              14.0f

#define MAIN_AMOUNT_TEXT_FONT_SIZE                                  18.0f

#pragma mark -

/**
 * SimpleHeaderViewProvider private category
 */
@interface FOHeaderViewProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (FOHeaderViewProvider *)getInstance;

/**
 * Creates and returns an autoreleased SimpleHeaderView constructed from a NIB file
 *
 * @return The autoreleased SimpleHeaderView constructed from a NIB file
 */
- (FrequentOperationHeaderView *)frequentOperationHeaderView;

@end


#pragma mark -

@implementation FOHeaderViewProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * SimpleHeaderViewProvier singleton only instance
 */
static FOHeaderViewProvider *fOHeaderViewProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([FOHeaderViewProvider class]) {
        
        if (fOHeaderViewProviderInstance_ == nil) {
            
            fOHeaderViewProviderInstance_ = [super allocWithZone:zone];
            return fOHeaderViewProviderInstance_;
        }
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (FOHeaderViewProvider *)getInstance {
    
    if (fOHeaderViewProviderInstance_ == nil) {
        
        @synchronized([FOHeaderViewProvider class]) {
            
            if (fOHeaderViewProviderInstance_ == nil) {
                
                fOHeaderViewProviderInstance_ = [[FOHeaderViewProvider alloc] init];
                
            }
        }
    }
    
    return fOHeaderViewProviderInstance_;
    
}

#pragma mark -
#pragma mark MovementDetailView creation

/*
 * Creates and returns aa autoreleased SimpleHeaderView constructed from a NIB file
 */
- (FrequentOperationHeaderView *)frequentOperationHeaderView {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    FrequentOperationHeaderView *result = auxView_;
    [auxView_ setBackgroundColor:[UIColor BBVAWhiteColor]];
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation FrequentOperationHeaderView

#pragma mark -
#pragma mark Properties

@synthesize height;
@synthesize titleLabel = titleLabel_;
@synthesize separator = separator_;
@synthesize titlesAndAttributeArray = titlesAndAttributeArray_;
@dynamic title;
@synthesize className = className_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [separator_ release];
    separator_ = nil;
    
    [titlesAndAttributeArray_ release];
    titlesAndAttributeArray_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles the header label
 * and loads the map icon image
 */
- (void)awakeFromNib {
    self.backgroundColor = [UIColor whiteColor];
    
    separator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    titleLabel_.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withBoldFontSize:16.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
    
    [titleLabel_ setText:NSLocalizedString(FO_HEADER_TITLE_KEY, nil)];
    
   /* CALayer *layer = self.layer;
    [layer setCornerRadius:10.0f];
    [layer setBorderWidth:0.8f];
    [layer setBorderColor:[UIColor BBVAGreyToneThreeColor].CGColor];*/
}

/*
 * Creates and returns aa autoreleased SimpleHeaderView constructed from a NIB file
 */
+ (FrequentOperationHeaderView *)frequentOperationHeaderView {
    
    return [[FOHeaderViewProvider getInstance] frequentOperationHeaderView];
    
}

/**
 * Creates the titles and subtitles for the view using the key and values arrays
 *
 */
- (void)updateInformationView{
    
    [self setAutoresizingMask:UIViewAutoresizingNone];
    
    for (UIView *aView in [self subviews]) {
        if ([aView tag] == LABELS_TAG) {
            [aView removeFromSuperview];
            aView = nil;
        }
    }

    CGFloat referentialHeight = 5.0f;
    
    //update the labels information
    for (int i = 0; i < [titlesAndAttributeArray_ count] ; i++) {
        
        if([[[titlesAndAttributeArray_ objectAtIndex:i] attributesArray] count]==0)
            continue;
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, referentialHeight, self.frame.size.width - 20, 17)];
        TitleAndAttributes *titleAndAttributes = [titlesAndAttributeArray_ objectAtIndex:i];
       
        
        [titleLabel setText:[titleAndAttributes titleString]];
        [NXT_Peru_iPhoneStyler styleLabel:titleLabel withFontSize:FONT_SIZE_TITLE color:[UIColor blackColor]];
        
        referentialHeight = CGRectGetMaxY([titleLabel frame])+1;
        
        [titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [titleLabel setTag:LABELS_TAG];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        
        [NXT_Peru_iPhoneStyler styleLabel:titleLabel withBoldFontSize:13.0f color:[UIColor grayColor]];
        [self addSubview:titleLabel];
        [titleLabel release];
        
        NSArray *attributes = [titleAndAttributes attributesArray];
        
        for (NSString *attribute in attributes) {
            
            
            
            
            CGSize recognizeWith = [attribute sizeWithFont:[NXT_Peru_iPhoneStyler normalFontWithSize:TEXT_FONT_SIZE] constrainedToSize:CGSizeMake(9999, 17)];

            
            CGFloat currenLabelWidth = self.frame.size.width - 25;
            UILabel *subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, referentialHeight,currenLabelWidth , (recognizeWith.width>currenLabelWidth)?34:17)];
             [subtitleLabel setText:attribute];
            
            if ([[titleAndAttributes titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_CHARGED_AMOUNT_TEXT_KEY, nil)] ||
                [[titleAndAttributes titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_PAID_AMOUNT_TEXT_KEY, nil)]  ||
                [[titleAndAttributes titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_PAYED_AMOUNT_TEXT_KEY, nil)] ||
                ([[titleAndAttributes titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TOTAL_AMOUNT_TEXT_KEY, nil)] && className_ == [FOPaymentCOtherBank class]) ||
                ([[titleAndAttributes titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_FINAL_AMOUNT_TEXT_KEY, nil)] && className_ == [FOPaymentCOtherBank class]) ||
                [[titleAndAttributes titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_AMOUNT_TEXT_KEY, nil)] ||
                [[titleAndAttributes titleString] isEqualToString:NSLocalizedString(INSTITUTIONS_COMPANIES_CONFIRMATION_AMOUNT_KEY, nil)] ||
                [[titleAndAttributes titleString] isEqualToString:NSLocalizedString(TRANSFER_AMOUNT_TEXT_KEY,nil)]) {
                
                [NXT_Peru_iPhoneStyler styleLabel:subtitleLabel withBoldFontSize:TEXT_FONT_SIZE color:[UIColor BBVAMagentaColor]];
                
            }
            else if (([[titleAndAttributes titleString] isEqualToString:NSLocalizedString(CARD_PAYMENT_AMOUNT_TO_PAY_TEXT_KEY, nil)]) ||
                     ([[titleAndAttributes titleString] isEqualToString:NSLocalizedString(PAYMENT_RECHARGE_AMOUNT_TO_RECHARGE_TEXT_KEY, nil)]) ||
                     ([[titleAndAttributes titleString] isEqualToString:NSLocalizedString(SEND_AMOUNT_TEXT_KEY, nil)]))
            {
                [NXT_Peru_iPhoneStyler styleLabel:subtitleLabel withFontSize:MAIN_AMOUNT_TEXT_FONT_SIZE color:[UIColor BBVAMagentaColor]];
            }
            else
            {
            
           
             [NXT_Peru_iPhoneStyler styleLabel:subtitleLabel withFontSize:TEXT_FONT_SIZE color:[UIColor grayColor]];
            [subtitleLabel setLineBreakMode:UILineBreakModeWordWrap];
            [subtitleLabel setNumberOfLines:(recognizeWith.width>currenLabelWidth)?3:1];
            }

          //  [NXT_Peru_iPhoneStyler styleLabel:subtitleLabel withFontSize:FONT_SIZE_SUBTITLE color:[UIColor BBVAGreyToneTwoColor]];
            
            referentialHeight = CGRectGetMaxY([subtitleLabel frame])+4;
            
            if([[titleAndAttributes titleString] length]==0)
            {
                [NXT_Peru_iPhoneStyler styleLabel:subtitleLabel withFontSize:9.0 color:[UIColor grayColor]];
                CGRect subtitleRect = subtitleLabel.frame;
                
                subtitleRect.size.height = 50;
                [subtitleLabel setFrame:subtitleRect];
            }
            
            [subtitleLabel setLineBreakMode:NSLineBreakByWordWrapping];
            [subtitleLabel setTag:LABELS_TAG];
            [subtitleLabel setBackgroundColor:[UIColor clearColor]];
            [self addSubview:subtitleLabel];
            [subtitleLabel release];
        }
        
    }    
    
    CGRect frame = [separator_ frame];
    frame.origin.y = referentialHeight;
    [separator_ setFrame:frame];
    
    frame = [self frame];
    height = referentialHeight;
    frame.size.height = referentialHeight;
    [self setFrame:frame];
}

#pragma mark -
#pragma mark View information

/*
 * Returns the width of the view
 */
+ (CGFloat)width {
    return 320.0f;
}

/*
 * Returns the height of the view
 */
+ (CGFloat)height {
    return 60.0f;
}

#pragma mark -
#pragma mark Properties methods

/**
 * Sets the title
 */
- (void)setTitle:(NSString *)title {
    
    [titleLabel_ setText:title];
    
}

@end