/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "SimpleHeaderView.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the SimpleHeaderView NIB file name
 */
#define NIB_FILE_NAME                                               @"SimpleHeaderView"


#pragma mark -

/**
 * SimpleHeaderViewProvider private category
 */
@interface SimpleHeaderViewProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (SimpleHeaderViewProvider *)getInstance;

/**
 * Creates and returns an autoreleased SimpleHeaderView constructed from a NIB file
 *
 * @return The autoreleased SimpleHeaderView constructed from a NIB file
 */
- (SimpleHeaderView *)simpleHeaderView;

@end


#pragma mark -

@implementation SimpleHeaderViewProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * SimpleHeaderViewProvier singleton only instance
 */
static SimpleHeaderViewProvider *simpleHeaderViewProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([SimpleHeaderViewProvider class]) {
        
        if (simpleHeaderViewProviderInstance_ == nil) {
            
            simpleHeaderViewProviderInstance_ = [super allocWithZone:zone];
            return simpleHeaderViewProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (SimpleHeaderViewProvider *)getInstance {
    
    if (simpleHeaderViewProviderInstance_ == nil) {
        
        @synchronized([SimpleHeaderViewProvider class]) {
            
            if (simpleHeaderViewProviderInstance_ == nil) {
                
                simpleHeaderViewProviderInstance_ = [[SimpleHeaderViewProvider alloc] init];
                
            }
            
        }
        
    }
    
    return simpleHeaderViewProviderInstance_;
    
}

#pragma mark -
#pragma mark MovementDetailView creation

/*
 * Creates and returns aa autoreleased SimpleHeaderView constructed from a NIB file
 */
- (SimpleHeaderView *)simpleHeaderView {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    SimpleHeaderView *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation SimpleHeaderView

#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;
@synthesize separator = separator_;
@dynamic title;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [separator_ release];
    separator_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles the header label
 * and loads the map icon image
 */
- (void)awakeFromNib {
    self.backgroundColor = [UIColor whiteColor];
    
    separator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    titleLabel_.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withBoldFontSize:16.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];

}

/*
 * Creates and returns aa autoreleased SimpleHeaderView constructed from a NIB file
 */
+ (SimpleHeaderView *)simpleHeaderView {
    
    return [[SimpleHeaderViewProvider getInstance] simpleHeaderView];
    
}

#pragma mark -
#pragma mark View information

/*
 * Returns the width of the view
 */
+ (CGFloat)width {
    return 320.0f;
}

/*
 * Returns the height of the view
 */
+ (CGFloat)height {
    return 60.0f;
}

#pragma mark -
#pragma mark Properties methods

/**
 * Sets the title
 */
- (void)setTitle:(NSString *)title {

    [titleLabel_ setText:title];

}

@end