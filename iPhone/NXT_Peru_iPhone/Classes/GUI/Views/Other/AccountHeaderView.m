/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "AccountHeaderView.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the AccountHeaderView NIB file name
 */
#define NIB_FILE_NAME                                               @"AccountHeaderView"


#pragma mark -

/**
 * AccountHeaderViewProvider private category
 */
@interface AccountHeaderViewProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (AccountHeaderViewProvider *)getInstance;

/**
 * Creates and returns an autoreleased AccountHeaderView constructed from a NIB file
 *
 * @return The autoreleased AccountHeaderView constructed from a NIB file
 */
- (AccountHeaderView *)accountHeaderView;

@end


#pragma mark -

@implementation AccountHeaderViewProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * AccountHeaderViewProvier singleton only instance
 */
static AccountHeaderViewProvider *accountHeaderViewProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([AccountHeaderViewProvider class]) {
        
        if (accountHeaderViewProviderInstance_ == nil) {
            
            accountHeaderViewProviderInstance_ = [super allocWithZone:zone];
            return accountHeaderViewProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (AccountHeaderViewProvider *)getInstance {
    
    if (accountHeaderViewProviderInstance_ == nil) {
        
        @synchronized([AccountHeaderViewProvider class]) {
            
            if (accountHeaderViewProviderInstance_ == nil) {
                
                accountHeaderViewProviderInstance_ = [[AccountHeaderViewProvider alloc] init];
                
            }
            
        }
        
    }
    
    return accountHeaderViewProviderInstance_;
    
}

#pragma mark -
#pragma mark AccountDetailView creation

/*
 * Creates and returns aa autoreleased AccountHeaderView constructed from a NIB file
 */
- (AccountHeaderView *)accountHeaderView {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    AccountHeaderView *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation AccountHeaderView

#pragma mark -
#pragma mark Properties

@synthesize topLeftLabel = topLeftLabel_;
@synthesize bottomLeftLabel = bottomLeftLabel_;
@synthesize topRightLabel = topRightLabel_;
@synthesize bottomRightLabel = bottomRightLabel_;
@synthesize separator = separator_;
@dynamic topTitle;
@dynamic bottomTitle;
@dynamic topDetail;
@dynamic bottomDetail;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [topLeftLabel_ release];
    topLeftLabel_ = nil;
    
    [bottomLeftLabel_ release];
    bottomLeftLabel_ = nil;
    
    [topRightLabel_ release];
    topRightLabel_ = nil;
    
    [bottomRightLabel_ release];
    bottomRightLabel_ = nil;
    
    [separator_ release];
    separator_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles the header label
 * and loads the map icon image
 */
- (void)awakeFromNib {
    self.backgroundColor = [UIColor whiteColor];
        
    [NXT_Peru_iPhoneStyler styleLabel:topLeftLabel_ withBoldFontSize:14.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:bottomLeftLabel_ withBoldFontSize:14.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:topRightLabel_ withBoldFontSize:14.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:bottomRightLabel_ withBoldFontSize:14.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
    
    separator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
}

/*
 * Creates and returns aa autoreleased AccountHeaderView constructed from a NIB file
 */
+ (AccountHeaderView *)accountHeaderView {
    
    return [[AccountHeaderViewProvider getInstance] accountHeaderView];
    
}

#pragma mark -
#pragma mark View information

/*
 * Returns the width of the view
 */
+ (CGFloat)width {
    return 320.0f;
}

/*
 * Returns the height of the view
 */
+ (CGFloat)height {
    return 60.0f;
}

#pragma mark -
#pragma mark Properties methods

/**
 * Sets the movement and fills the view
 */
- (void)setTopDetail:(NSString *)title {

    topRightLabel_.text = title;
    
}

/**
 * Sets the movement and fills the view
 */
- (void)setBottomDetail:(NSString *)title {
    
    bottomRightLabel_.text = title;
    
}

/**
 * Sets the top title
 */
- (void)setTopTitle:(NSString *)title {

    [topLeftLabel_ setText:title];

}

/**
 * Sets the bottom title
 */
- (void)setBottomTitle:(NSString *)title {
    
    [bottomLeftLabel_ setText:title];
    
}

@end