//
//  SimulationView.h
//  Created by Author.
//

#import <UIKit/UIKit.h>
#import "FastLoanSimulation.h"

@protocol SimulationViewDelegate

-(void)onNextScreenFromSimulation;

@end


@class NPopup;

@interface SimulationView : UIView{
@private
    NPopup * popup_;
    UIView *view_;
    
    UILabel *quotaMonthLabel_;
    UILabel *termLabel_;
    UILabel *typeOfQuotaLabel_;
    UILabel *dayOfPayLabel_;
    UILabel *teaLabel_;
    UILabel *tceaLabel_;
    
    UILabel *quotaMonthValueLabel_;
    UILabel *termValueLabel_;
    UILabel *typeOfQuotaValueLabel_;
    UILabel *dayOfPayValueLabel_;
    UILabel *teaValueLabel_;
    UILabel *tceaValueLabel_;
    
    UIButton *quotaMonthButton_;
    UIButton *teaButton_;
    UIButton *tceaButton_;
    
    UIButton *continueButton_;
    UIButton *modifyButton_;

    id<SimulationViewDelegate> delegate_;
}

@property (nonatomic, readwrite, retain) id<SimulationViewDelegate> delegate;

@property (nonatomic, retain) IBOutlet UIView *view;

@property (nonatomic, retain) IBOutlet UILabel *quotaMonthLabel;
@property (nonatomic, retain) IBOutlet UILabel *termLabel;
@property (nonatomic, retain) IBOutlet UILabel *typeOfQuotaLabel;
@property (nonatomic, retain) IBOutlet UILabel *dayOfPayLabel;
@property (nonatomic, retain) IBOutlet UILabel *teaLabel;
@property (nonatomic, retain) IBOutlet UILabel *tceaLabel;

@property (nonatomic, retain) IBOutlet UILabel *quotaMonthValueLabel;
@property (nonatomic, retain) IBOutlet UILabel *termValueLabel;
@property (nonatomic, retain) IBOutlet UILabel *typeOfQuotaValueLabel;
@property (nonatomic, retain) IBOutlet UILabel *dayOfPayValueLabel;
@property (nonatomic, retain) IBOutlet UILabel *teaValueLabel;
@property (nonatomic, retain) IBOutlet UILabel *tceaValueLabel;

@property (nonatomic, retain) IBOutlet UIButton *quotaMonthButton;
@property (nonatomic, retain) IBOutlet UIButton *teaButton;
@property (nonatomic, retain) IBOutlet UIButton *tceaButton;

@property (nonatomic, retain) IBOutlet UIButton *continueButton;
@property (nonatomic, retain) IBOutlet UIButton *modifyButton;

-(void)showInWindow:(UIWindow*)parentWindow;
-(void) setData:(FastLoanSimulation *)fastLoanSimulation;


@end
