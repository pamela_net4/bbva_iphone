/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>
#import "SingletonBase.h"

@class NXTActivityIndicator;

/**
 * Provider to obtain a NXTActivityIndicator created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTActivityIndicatorProvider: SingletonBase {
@private
    /**
     * Auxiliary NXTActivityIndicator to create it from a NIB file
     */
    NXTActivityIndicator *auxView_;
}

/**
 * Provides read-write access to the auxililary NXTActivityIndicator
 */
@property (nonatomic, readwrite, assign) IBOutlet NXTActivityIndicator *auxView;

@end

/**
 * Shows a semi transparent black layer with an activity indicator and a label with an optional text
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTActivityIndicator : UIView {
@private
    /**
     * Activity indicator
     */
    UIActivityIndicatorView *indicator_;
    
    /**
     * Label
     */
    UILabel *messageLabel_;
    
    /**
     * Background view
     */
    UIView *backgroundView_;
}

/**
 * Provides readwrite access to the indicator. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIActivityIndicatorView *indicator;

/**
 * Provides readwrite access to the messageLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *messageLabel;

/**
 * Provides readwrite access to the backgroundView. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIView *backgroundView;

/**
 * Indicates if the indicator must show the semi transparent background layer
 */
@property (nonatomic, readwrite, assign) BOOL showBackgroundLayer;

/**
 * Creates and returns an autoreleased NXTActivityIndicator constructed from a NIB file
 *
 * @return The autoreleased NXTActivityIndicator constructed from a NIB file
 */
+ (NXTActivityIndicator *)NXTActivityIndicator;

/**
 * Returns the width of the view
 */
+ (CGFloat)width;

/**
 * Returns the height of the view
 */
+ (CGFloat)height;

/**
 * Shows the view with a message (optional)
 *
 * @param message to set
 */
- (void)showIndicatorWithMessage:(NSString *)message;

/**
 * Hides the indicator
 */
- (void)hideIndicator;

@end
