/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "PublicServiceHeaderView.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the PublicServiceHeaderView NIB file name
 */
#define NIB_FILE_NAME                                               @"PublicServiceHeaderView"


#pragma mark -

/**
 * PublicServiceHeaderViewProvider private category
 */
@interface PublicServiceHeaderViewProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (PublicServiceHeaderViewProvider *)getInstance;

/**
 * Creates and returns an autoreleased PublicServiceHeaderView constructed from a NIB file
 *
 * @return The autoreleased PublicServiceHeaderView constructed from a NIB file
 */
- (PublicServiceHeaderView *)publicServiceHeaderView;

@end


#pragma mark -

@implementation PublicServiceHeaderViewProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * PublicServiceHeaderViewProvier singleton only instance
 */
static PublicServiceHeaderViewProvider *publicServiceHeaderViewProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([PublicServiceHeaderViewProvider class]) {
        
        if (publicServiceHeaderViewProviderInstance_ == nil) {
            
            publicServiceHeaderViewProviderInstance_ = [super allocWithZone:zone];
            return publicServiceHeaderViewProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (PublicServiceHeaderViewProvider *)getInstance {
    
    if (publicServiceHeaderViewProviderInstance_ == nil) {
        
        @synchronized([PublicServiceHeaderViewProvider class]) {
            
            if (publicServiceHeaderViewProviderInstance_ == nil) {
                
                publicServiceHeaderViewProviderInstance_ = [[PublicServiceHeaderViewProvider alloc] init];
                
            }
            
        }
        
    }
    
    return publicServiceHeaderViewProviderInstance_;
    
}

#pragma mark -
#pragma mark MovementDetailView creation

/*
 * Creates and returns aa autoreleased PublicServiceHeaderView constructed from a NIB file
 */
- (PublicServiceHeaderView *)publicServiceHeaderView {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    PublicServiceHeaderView *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation PublicServiceHeaderView

#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;
@synthesize titleSeparator = titleSeparator_;
@synthesize companyLabel = companyLabel_;
@synthesize companyNameLabel = companyNameLabel_;
@synthesize separator = separator_;
@dynamic title;
@dynamic companyName;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [titleSeparator_ release];
    titleSeparator_ = nil;
    
    [companyLabel_ release];
    companyLabel_ = nil;
    
    [companyNameLabel_ release];
    companyNameLabel_ = nil;
    
    [separator_ release];
    separator_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles the header label
 * and loads the map icon image
 */
- (void)awakeFromNib {
    self.backgroundColor = [UIColor whiteColor];
    
    titleSeparator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    separator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    titleLabel_.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withBoldFontSize:16.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:companyLabel_ withBoldFontSize:16.0f color:[UIColor grayColor]];
    [NXT_Peru_iPhoneStyler styleLabel:companyNameLabel_ withFontSize:16.0f color:[UIColor BBVAGreyToneTwoColor]];
    
    [companyLabel_ setText:[NSString stringWithFormat:@"%@", NSLocalizedString(PAYMENT_RECHARGE_BUSINESS_TEXT_KEY, nil)]];

}

/*
 * Creates and returns aa autoreleased PublicServiceHeaderView constructed from a NIB file
 */
+ (PublicServiceHeaderView *)publicServiceHeaderView {
    
    return [[PublicServiceHeaderViewProvider getInstance] publicServiceHeaderView];
    
}

#pragma mark -
#pragma mark View information

/*
 * Returns the width of the view
 */
+ (CGFloat)width {
    return 320.0f;
}

/*
 * Returns the height of the view
 */
+ (CGFloat)height {
    return 80.0f;
}

#pragma mark -
#pragma mark Properties methods

/**
 * Sets the title
 */
- (void)setTitle:(NSString *)title {

    [titleLabel_ setText:title];

}

/**
 * Sets the title
 */
- (void)setCompanyName:(NSString *)companyName {
    
    [companyNameLabel_ setText:companyName];
    
}

@end