/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TransactionDetailView.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the MovementDetailView NIB file name
 */
#define NIB_FILE_NAME                                               @"TransactionDetailView"

/**
 * Label height
 */
#define LABEL_HEIGHT                                                16.0f

#pragma mark -

/**
 * MovementDetailViewProvider private extension
 */
@interface TransactionDetailViewProvider()

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (TransactionDetailViewProvider *)getInstance;

/**
 * Creates and returns an autoreleased MovementDetailView constructed from a NIB file
 *
 * @return The autoreleased MovementDetailView constructed from a NIB file
 */
- (TransactionDetailView *)transactionDetailView;

@end


#pragma mark -

@implementation TransactionDetailViewProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * MovementDetailViewProvier singleton only instance
 */
static TransactionDetailViewProvider *transactionDetailViewProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([TransactionDetailViewProvider class]) {
        
        if (transactionDetailViewProviderInstance_ == nil) {
            
            transactionDetailViewProviderInstance_ = [super allocWithZone:zone];
            return transactionDetailViewProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (TransactionDetailViewProvider *)getInstance {
    
    if (transactionDetailViewProviderInstance_ == nil) {
        
        @synchronized([TransactionDetailViewProvider class]) {
            
            if (transactionDetailViewProviderInstance_ == nil) {
                
                transactionDetailViewProviderInstance_ = [[TransactionDetailViewProvider alloc] init];
                
            }
            
        }
        
    }
    
    return transactionDetailViewProviderInstance_;
    
}

#pragma mark -
#pragma mark MovementDetailView creation

/*
 * Creates and returns aa autoreleased MovementDetailView constructed from a NIB file
 */
- (TransactionDetailView *)transactionDetailView {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    TransactionDetailView *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation TransactionDetailView

#pragma mark -
#pragma mark Properties

@synthesize amountLabel = amountLabel_;
@synthesize nameLabel = nameLabel_;
@synthesize information = information_;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [amountLabel_ release];
    amountLabel_ = nil;

    [nameLabel_ release];
    nameLabel_ = nil;
    
    [information_ release];
    information_ = nil;
    
    [labelsArray_ release];
    labelsArray_ = nil;

    [swipeGestureRecognizer_ release];
    swipeGestureRecognizer_ = nil;
    
    delegate_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles the header label
 * and loads the map icon image
 */
- (void)awakeFromNib {
    self.backgroundColor = [UIColor whiteColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withBoldFontSize:26.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [NXT_Peru_iPhoneStyler styleLabel:nameLabel_ withFontSize:13.0f color:[UIColor BBVACoolGreyColor]];
    
    [swipeGestureRecognizer_ release];
    swipeGestureRecognizer_ = [[NXTSwipeGestureRecognizer alloc] initWithView:self];
}

/*
 * Creates and returns aa autoreleased MovementDetailView constructed from a NIB file
 */
+ (TransactionDetailView *)transactionDetailView {
    
    return [[TransactionDetailViewProvider getInstance] transactionDetailView];
    
}

#pragma mark -
#pragma mark View information

/*
 * Returns the width of the view
 */
+ (CGFloat)width {
    return 260.0f;	
}

/*
 * Returns the height of the view
 */
+ (CGFloat)heightForDetailsNumber:(NSInteger)number {
    
    CGFloat result = 76.0f;
    
    result = result + number*LABEL_HEIGHT + 5.0f;
    
    return result;
}

#pragma mark -
#pragma mark Properties methods

/**
 * Returns the movement
 */
- (NSArray *)information {
    return information_;
}

/**
 * Sets the movement and fills the view
 */
- (void)setInformation:(NSArray *)information {
    
    if (information_ == nil) {
        
        information_ = [[NSMutableArray alloc] init];  
        
    }
    
    [information_ removeAllObjects];
    [information_ addObjectsFromArray:information];
    
    if (labelsArray_ == nil) {
        labelsArray_ = [[NSMutableArray alloc] init];
    }
    
    for (UIView *label in labelsArray_) {
        [label removeFromSuperview];
    }
    
    [labelsArray_ removeAllObjects];
    
    NSString *detailText = @"";

    CGFloat yPosition = 76.0f;
    NSInteger i = 0;
    
    for (TitleAndAttributes *titleAndAttributes in information_) {
        
        //The two first entries are used for the amount and the name
        
        if (i == 0) {
            
            amountLabel_.text = [titleAndAttributes titleString];
            
            NSRange range = [amountLabel_.text rangeOfString:@"-"];
            if (range.length == 0) {
                [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withBoldFontSize:26.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
            } else {
                [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withBoldFontSize:26.0f color:[UIColor BBVAMagentaColor]];
            }
            
        } else if (i == 1) {
            
            nameLabel_.text = [titleAndAttributes titleString];
        
        } else {
        
            UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0.0f, yPosition, 150.0f, LABEL_HEIGHT)] autorelease];
            UILabel *detailLabel = [[[UILabel alloc] initWithFrame:CGRectMake(150.0f, yPosition, 120.0f, LABEL_HEIGHT)] autorelease];
            
            [self addSubview:titleLabel];
            [self addSubview:detailLabel];
            
            titleLabel.textAlignment = UITextAlignmentLeft;
            detailLabel.textAlignment = UITextAlignmentLeft;
            
            [NXT_Peru_iPhoneStyler styleLabel:titleLabel withFontSize:11.0f color:[UIColor BBVABlueColor]];
            [NXT_Peru_iPhoneStyler styleLabel:detailLabel withFontSize:11.0f color:[UIColor BBVABlueColor]];
            
            [labelsArray_ addObject:titleLabel];        
            [labelsArray_ addObject:detailLabel];
                        
            titleLabel.text = [titleAndAttributes titleString];
            
            if ([[titleAndAttributes attributesArray] count] == 0) {
                detailText = @"";
            } else {
                detailText = [[titleAndAttributes attributesArray] objectAtIndex:0];
            }
            
            detailLabel.text = detailText;
            
            yPosition = yPosition + LABEL_HEIGHT;
        
        }
        
        i++;
        
    }
    
}


#pragma mark -
#pragma mark Gesture management

/**
 * Tells the receiver when one or more fingers touch down in a view or window. If ther is only one touch, the notify listener
 * flag is enabled and the origin touch point is stored
 *
 * @param touches A set of UITouch instances that represent the touches for the starting phase of the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void) touchesBegan: (NSSet*) touches withEvent: (UIEvent*) event {
    [swipeGestureRecognizer_ touchesBegan:touches withEvent:event];
}

/**
 * Tells the receiver when one or more fingers associated with an event move within a view or window. When notification is
 * enabled, checks the current positions from original touch position and if it meets the requirements, notifies the listener
 *
 * @param touches A set of UITouch instances that represent the touches that are moving during the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void) touchesMoved: (NSSet*) touches withEvent: (UIEvent*) event {
    [swipeGestureRecognizer_ touchesMoved:touches withEvent:event];
}

/**
 * Tells the receiver when one or more fingers are raised from a view or window. Disables the listener notifications
 *
 * @param touches A set of UITouch instances that represent the touches for the ending phase of the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void) touchesEnded: (NSSet*) touches withEvent: (UIEvent*) event {
    [swipeGestureRecognizer_ touchesEnded:touches withEvent:event];
}

/**
 * Sent to the receiver when a system event (such as a low-memory warning) cancels a touch event. DIsables the listener notifications
 *
 * @param touches A set of UITouch instances that represent the touches for the ending phase of the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void) touchesCancelled: (NSSet*) touches withEvent: (UIEvent*) event {
    [swipeGestureRecognizer_ touchesCancelled:touches withEvent:event];
}

/*
 * Notification received from NXTSwipeGestureRecognizer
 */
- (void)swipeGestureDetected:(SwipeDirection)direction {
    [delegate_ swipeGestureDoneOnDirection:direction];
}

@end