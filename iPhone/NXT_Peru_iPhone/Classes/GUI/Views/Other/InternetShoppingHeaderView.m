/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "InternetShoppingHeaderView.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the PublicServiceHeaderView NIB file name
 */
#define NIB_FILE_NAME                                               @"InternetShoppingHeaderView"


#pragma mark -

/**
 * PublicServiceHeaderViewProvider private category
 */
@interface InternetShoppingHeaderViewProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (InternetShoppingHeaderViewProvider *)getInstance;

/**
 * Creates and returns an autoreleased PublicServiceHeaderView constructed from a NIB file
 *
 * @return The autoreleased PublicServiceHeaderView constructed from a NIB file
 */
- (InternetShoppingHeaderView *)internetShoppingHeaderView;

@end


#pragma mark -

@implementation InternetShoppingHeaderViewProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * PublicServiceHeaderViewProvier singleton only instance
 */
static InternetShoppingHeaderViewProvider *internetShoppingHeaderViewProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([InternetShoppingHeaderViewProvider class]) {
        
        if (internetShoppingHeaderViewProviderInstance_ == nil) {
            
            internetShoppingHeaderViewProviderInstance_ = [super allocWithZone:zone];
            return internetShoppingHeaderViewProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (InternetShoppingHeaderViewProvider *)getInstance {
    
    if (internetShoppingHeaderViewProviderInstance_ == nil) {
        
        @synchronized([InternetShoppingHeaderViewProvider class]) {
            
            if (internetShoppingHeaderViewProviderInstance_ == nil) {
                
                internetShoppingHeaderViewProviderInstance_ = [[InternetShoppingHeaderViewProvider alloc] init];
                
            }
            
        }
        
    }
    
    return internetShoppingHeaderViewProviderInstance_;
    
}

#pragma mark -
#pragma mark MovementDetailView creation

/*
 * Creates and returns aa autoreleased PublicServiceHeaderView constructed from a NIB file
 */
- (InternetShoppingHeaderView *)internetShoppingHeaderView {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    InternetShoppingHeaderView *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation InternetShoppingHeaderView

#pragma mark -
#pragma mark Properties

@synthesize titleLabel = titleLabel_;
@synthesize titleSeparator = titleSeparator_;
@synthesize companyLabel = companyLabel_;
@synthesize companyNameLabel = companyNameLabel_;
@synthesize transactionLabel = transactionLabel_;
@synthesize transactionNumberLabel = transactionNumberLabel_;
@synthesize businessLabel = businessLabel_;
@synthesize businessNameLabel = businessNameLabel_;
@synthesize currencyLabel = currencyLabel_;
@synthesize currencyNameLabel = currencyNameLabel_;
@synthesize amountLabel = amountLabel_;
@synthesize amountValueLabel = amountValueLabel_;
@synthesize separator = separator_;
@dynamic title;
@dynamic companyName;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [titleSeparator_ release];
    titleSeparator_ = nil;
    
    [companyLabel_ release];
    companyLabel_ = nil;
    
    [companyNameLabel_ release];
    companyNameLabel_ = nil;
    
    [transactionLabel_ release];
    transactionLabel_ = nil;
    
    [transactionNumberLabel_ release];
    transactionNumberLabel_ = nil;
    
    [businessLabel_ release];
    businessLabel_ = nil;
    
    [businessNameLabel_ release];
    businessNameLabel_ = nil;
    
    [currencyLabel_ release];
    currencyLabel_ = nil;
    
    [currencyNameLabel_ release];
    currencyNameLabel_ = nil;
    
    [amountLabel_ release];
    amountLabel_ = nil;
    
    [amountValueLabel_ release];
    amountValueLabel_ = nil;
    
    [separator_ release];
    separator_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles the header label
 * and loads the map icon image
 */
- (void)awakeFromNib {
    
    self.backgroundColor = [UIColor whiteColor];
    
    titleSeparator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    separator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    titleLabel_.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withBoldFontSize:16.0f color:[UIColor BBVABlueSpectrumToneTwoColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:companyLabel_ withBoldFontSize:16.0f color:[UIColor BBVABlackColor]];
    [NXT_Peru_iPhoneStyler styleLabel:companyNameLabel_ withFontSize:16.0f color:[UIColor BBVAGreyToneTwoColor]];
    [companyLabel_ setText:[NSString stringWithFormat:@"%@:", NSLocalizedString(INTERNET_SHOPPING_COMPANY_TEXT_KEY, nil)]];

    
    [NXT_Peru_iPhoneStyler styleLabel:transactionLabel_ withBoldFontSize:16.0f color:[UIColor BBVABlackColor]];
    [NXT_Peru_iPhoneStyler styleLabel:transactionNumberLabel_ withFontSize:16.0f color:[UIColor BBVAGreyToneTwoColor]];
    [transactionLabel_ setText:[NSString stringWithFormat:@"%@:", NSLocalizedString(INTERNET_SHOPPING_TRANS_NUMBER_TEXT_KEY, nil)]];
    
    [NXT_Peru_iPhoneStyler styleLabel:businessLabel_ withBoldFontSize:16.0f color:[UIColor BBVABlackColor]];
    [NXT_Peru_iPhoneStyler styleLabel:businessNameLabel_ withFontSize:16.0f color:[UIColor BBVAGreyToneTwoColor]];
    [businessLabel_ setText:[NSString stringWithFormat:@"%@:", NSLocalizedString(INTERNET_SHOPPING_BUSINESS_TEXT_KEY, nil)]];
    
    [NXT_Peru_iPhoneStyler styleLabel:currencyLabel_ withBoldFontSize:16.0f color:[UIColor BBVABlackColor]];
    [NXT_Peru_iPhoneStyler styleLabel:currencyNameLabel_ withFontSize:16.0f color:[UIColor BBVAGreyToneTwoColor]];
    [currencyLabel_ setText:[NSString stringWithFormat:@"%@:", NSLocalizedString(INTERNET_SHOPPING_CURRENCY_TEXT_KEY, nil)]];
    
    [NXT_Peru_iPhoneStyler styleLabel:amountLabel_ withBoldFontSize:16.0f color:[UIColor BBVABlackColor]];
    [NXT_Peru_iPhoneStyler styleLabel:amountValueLabel_ withFontSize:16.0f color:[UIColor BBVAGreyToneTwoColor]];
    [amountLabel_ setText:[NSString stringWithFormat:@"%@:", NSLocalizedString(INTERNET_SHOPPING_AMOUNT_TEXT_KEY, nil)]];
    
}

/*
 * Creates and returns aa autoreleased InternetShoppingHeaderView constructed from a NIB file
 */
+ (InternetShoppingHeaderView *) internetShoppingHeaderView {
    
    //[self awakeFromNib];
    
    return [[InternetShoppingHeaderViewProvider getInstance] internetShoppingHeaderView];
    
}

#pragma mark -
#pragma mark View information

/*
 * Returns the width of the view
 */
+ (CGFloat)width {
    return 320.0f;
}

/*
 * Returns the height of the view
 */
+ (CGFloat)height {
    return 320.0f;
}

#pragma mark -
#pragma mark Properties methods

/**
 * Sets the title
 */
- (void)setTitle:(NSString *)title {

    [titleLabel_ setText:title];

}

/**
 * Sets the title
 */
- (void)setCompanyName:(NSString *)companyName {
    
    [companyNameLabel_ setText:companyName];
    
}

- (void)setTransactionNumber:(NSString *) transactionNumber {
    
    [transactionNumberLabel_ setText:transactionNumber];
    
}


- (void)setBusinessName:(NSString *) businessName {
    
    [businessNameLabel_ setText:businessName];
    
}


- (void)setCurrencyName:(NSString *) currencyName {
    
    [currencyNameLabel_ setText:currencyName];
    
}

- (void)setAmountValue:(NSString *)amountValue {

    [amountValueLabel_ setText: amountValue];
   
}

@end