/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>
#import "SingletonBase.h"

@class ProductHeaderView;

/**
 * Provider to obtain a ProductHeaderView created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ProductHeaderViewProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary ProductHeaderView to create it from a NIB file
     */
    ProductHeaderView *auxView_;
    
}

/**
 * Provides read-write access to the auxililary ProductHeaderView
 */
@property (nonatomic, readwrite, assign) IBOutlet ProductHeaderView *auxView;

@end

/**
 * Product header with the concept, date and amount
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ProductHeaderView : UIView {
@private
    /**
     * Label of the concept of the transaction
     */
    UILabel *titleLabel_;
    
    /**
     * Amount of the transaction
     */
    UILabel *amountLabel_;
    
    /**
     * Separator line
     */
    UIImageView *separator_;

}

/**
 * Provides readwrite access to the titleLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *titleLabel;

/**
 * Provides readwrite access to the amountLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *amountLabel;

/**
 * Provides readwrite access to the separator1. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

/**
 * Provides readwrite access to the title
 */
@property (nonatomic, readwrite, copy) NSString *title;

/**
 * Provides readwrite access to the amount
 */
@property (nonatomic, readwrite, copy) NSDecimalNumber *amount;

/**
 * Creates and returns an autoreleased ProductHeaderView constructed from a NIB file
 *
 * @return The autoreleased ProductHeaderView constructed from a NIB file
 */
+ (ProductHeaderView *)productHeaderView;

/**
 * Returns the width of the view
 */
+ (CGFloat)width;

/**
 * Returns the height of the view
 */
+ (CGFloat)height;

@end
