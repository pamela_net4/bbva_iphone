/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        15.0f

@class NXTTextField;


/**
 * 
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface SimpleSelectorAccountView : UIView <UITextFieldDelegate>{
    
@private
    
    /*
     *bank number label
     */
    UILabel *bankNumberLabel_;
    
    /*
     *bank label
     */
    UILabel *bankLabel_;
    
    /*
     *office text field
     */
    NXTTextField *officeTextField_;
    
    /*
     *office label
     */
    UILabel *officeLabel_;
    
    /*
     *account text field
     */
    NXTTextField *accountTextField_;
    
    /*
     *account label
     */
    UILabel *accountLabel_;
    
    /*
     * cc label
     */
    UILabel *ccLabel_;
    
    /*
     * cc text field;
     */
    NXTTextField *CcTextField_;
    
    /*
     * flag to show the cc
     */
    BOOL showCc_;
    
    BOOL showAccountSelector_;
    
    UIButton *accountsButton_;
    
    NXTTextField *entityTextField_;
}

/**
 * Provides readwrite access to the bankNumber label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *bankNumberLabel;

/**
 * Provides readwrite access to the bank label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *bankLabel;

/**
 * Provides readwrite access to the officeTextField. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTTextField *officeTextField;

/**
 * Provides readwrite access to the office label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *officeLabel;

/**
 * Provides readwrite access to the accountTextField. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTTextField *accountTextField;

/**
 * Provides readwrite access to the accountLabel. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *accountLabel;

/**
 * Provides readwrite access to the cc label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *CcLabel;

/**
 * Provides readwrite access to the cc text field. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTTextField *CcTextField;

/**
 * Provides readwrite access to the entity text field. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTTextField *entityTextField;

/**
 * Provides readwrite access to the entity text field. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UIButton *accountsButton;

/**
 * Provides read-write access to the show cc flag
 */
@property (nonatomic, readwrite, assign) BOOL showCc;

/**
 * Provides read-write access to the show cc flag
 */
@property (nonatomic, readwrite, assign) BOOL showAccountSelector;

/**
 * Creates and returns an autoreleased SimpleSelectorAccountView constructed from a NIB file
 *
 * @return The autoreleased SimpleSelectorAccountView constructed from a NIB file
 */
+ (SimpleSelectorAccountView *)simpleSelectorAccountView;


/**
 * Returns the view height
 *
 * @return The view height
 */
+ (CGFloat)viewHeight;

@end
