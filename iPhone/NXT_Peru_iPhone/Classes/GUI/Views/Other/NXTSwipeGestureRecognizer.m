/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTSwipeGestureRecognizer.h"

/**
 * Defines the minimum horizontal drag to consider it a swipe
 */
#define HORIZ_SWIPE_DRAG_MIN											25.0f

/**
 * Defines the maximum horizontal drag to consider it an horizontal swipe
 */
#define VERT_SWIPE_DRAG_MAX												30.0f

@implementation NXTSwipeGestureRecognizer

@synthesize view = view_;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    [super dealloc];
    
    [originalTouchDownSet_ release];
    originalTouchDownSet_ = nil;
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Always returns nil. Use initWithView:
 */
- (id)init {
    [self release];
    return nil;
}

/*
 * Designated initializer. This class can be instantiated only with this method
 */
- (id)initWithView:(id)view {
    if (self = [super init]) {
        self.view = view;
        self.delegate = view;
    }
    
    return self;
}

#pragma mark -
#pragma mark Gesture management

/*
 * Tells the receiver when one or more fingers touch down in a view or window. If ther is only one touch, the notify listener
 * flag is enabled and the origin touch point is stored
 */
- (void) touchesBegan: (NSSet*) touches withEvent: (UIEvent*) event {
	[originalTouchDownSet_ release];
	originalTouchDownSet_ = nil;
	
	if ([touches count] == 1) {
		UITouch* touch = [touches anyObject];
        
		startTouchPosition_ = [touch locationInView: view_];
		canNotifyListener_ = YES;
        notified_ = NO;
		
		originalTouchDownSet_ = [touches copy];
	} else {
		canNotifyListener_ = NO;
		
		[view_.superview touchesBegan: touches withEvent: event];
	}
}

/*
 * Tells the receiver when one or more fingers associated with an event move within a view or window. When notification is
 * enabled, checks the current positions from original touch position and if it meets the requirements, notifies the listener
 */
- (void) touchesMoved: (NSSet*) touches withEvent: (UIEvent*) event {
	if (canNotifyListener_ == YES) {
		UITouch* touch = [touches anyObject];
		CGPoint currentTouchPosition = [touch locationInView:view_];
		
		if (fabsf(startTouchPosition_.y - currentTouchPosition.y) >= VERT_SWIPE_DRAG_MAX) {
			canNotifyListener_ = NO;
            
		} else if ((startTouchPosition_.x < currentTouchPosition.x) && (canNotifyListener_ == YES)) {
			
            if (fabsf(startTouchPosition_.x - currentTouchPosition.x) >= HORIZ_SWIPE_DRAG_MIN) {
                
                if (!notified_) {
                    [delegate_ swipeGestureDetected:swipe_Right];
                    notified_ = YES;
                }
			}
            
		} else if ((startTouchPosition_.x > currentTouchPosition.x) && (canNotifyListener_ == YES)) {
			
             if (fabsf(startTouchPosition_.x - currentTouchPosition.x) >= HORIZ_SWIPE_DRAG_MIN) {
                 
                 if (!notified_) {
                     [delegate_ swipeGestureDetected:swipe_Left];
                     notified_ = YES;
                 }
			}
            
        }
        
	} else {
		[view_.superview touchesMoved: touches withEvent: event];
	}
}

/*
 * Tells the receiver when one or more fingers are raised from a view or window. Disables the listener notifications
 */
- (void) touchesEnded: (NSSet*) touches withEvent: (UIEvent*) event {
	if ((canNotifyListener_ == YES) && (originalTouchDownSet_ != nil)) {
		[view_.superview touchesBegan: originalTouchDownSet_ withEvent: event];
	}
	
    startTouchPosition_ = CGPointZero;
	canNotifyListener_ = NO;
    
	[originalTouchDownSet_ release];
	originalTouchDownSet_ = nil;
    
	[view_.superview touchesEnded: touches withEvent: event];
}

/*
 * Sent to the receiver when a system event (such as a low-memory warning) cancels a touch event. DIsables the listener notifications
 */
- (void) touchesCancelled: (NSSet*) touches withEvent: (UIEvent*) event {
	if ((canNotifyListener_ == YES) && (originalTouchDownSet_ != nil)) {
		[view_.superview touchesBegan: originalTouchDownSet_ withEvent: event];
	}
	
    startTouchPosition_ = CGPointZero;
	canNotifyListener_ = NO;
    
	[originalTouchDownSet_ release];
	originalTouchDownSet_ = nil;
    
	[view_.superview touchesCancelled: touches withEvent: event];
}

@end