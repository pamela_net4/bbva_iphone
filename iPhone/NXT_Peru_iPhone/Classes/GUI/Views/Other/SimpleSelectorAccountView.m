/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "SimpleSelectorAccountView.h"
#import "NibLoader.h"
#import "ImagesFileNames.h"
#import "ImagesCache.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTTextField.h"
#import "StringKeys.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the Simple selector account NIB file name
 */
#define NIB_FILE_NAME                                               @"SimpleSelectorAccountView"


/**
 * Defines the view height
 */
#define VIEW_HEIGHT                                                 63.0f

#pragma mark -

@interface SimpleSelectorAccountView()
    
/**
* Releases Graphic Elements
*
* @private
*/
- (void)releaseTransferSimpleSelectorGraphicElements;

/*
 * Recalculate the size
 */
- (void)recalculateSize;

@end

@implementation SimpleSelectorAccountView

@synthesize bankNumberLabel = bankNumberLabel_;
@synthesize bankLabel = bankLabel_;
@synthesize officeTextField = officeTextField_;
@synthesize officeLabel = officeLabel_;
@synthesize accountTextField = accountTextField_;
@synthesize accountLabel = accountLabel_;
@synthesize CcLabel = ccLabel_;
@synthesize CcTextField = CcTextField_;
@synthesize entityTextField = entityTextField_;
@synthesize showCc = showCc_;
@synthesize showAccountSelector = showAccountSelector_;
@synthesize accountsButton = accountsButton_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [self releaseTransferSimpleSelectorGraphicElements];
    
    [super dealloc];
    
}

/*
 * Updates the distance and time displayed using the stored information
 */
- (void)releaseTransferSimpleSelectorGraphicElements {
    
    [bankNumberLabel_ release];
    bankNumberLabel_ = nil;
    
    [bankLabel_ release];
    bankLabel_ = nil;
    
    [officeTextField_ release];
    officeTextField_ = nil;
    
    [officeLabel_ release];
    officeLabel_ = nil;
    
    [accountLabel_ release];
    accountLabel_ = nil;
    
    [accountTextField_ release];
    officeTextField_ = nil;
    
    [ccLabel_ release];
    ccLabel_ = nil;
    
    [CcTextField_ release];
    CcTextField_ = nil;
    
    [entityTextField_ release];
    entityTextField_ = nil;
    
    [accountsButton_ release];
    accountsButton_ = nil;
    
}


#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the view elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:bankNumberLabel_ withFontSize:15.0f color:[UIColor BBVAGreyToneOneColor]];
    [bankNumberLabel_ setTextAlignment:UITextAlignmentCenter];
    [bankNumberLabel_ setText:@"0011"];
    
    [NXT_Peru_iPhoneStyler styleLabel:bankLabel_ withFontSize:11.0f color:[UIColor BBVAGreyToneTwoColor]];
    [bankLabel_ setTextAlignment:UITextAlignmentCenter];
    [bankLabel_ setText:NSLocalizedString(CCC_ENTITY_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleLabel:officeLabel_ withFontSize:11.0f color:[UIColor BBVAGreyToneTwoColor]];
    [officeLabel_ setText:NSLocalizedString(CCC_BRANCH_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleLabel:accountLabel_ withFontSize:11.0f color:[UIColor BBVAGreyToneTwoColor]];
    [accountLabel_ setText:NSLocalizedString(CCC_ACCOUNT_TEXT_KEY, nil)];        
    
    [NXT_Peru_iPhoneStyler styleTextField:officeTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    [officeTextField_ setPlaceholder:@""];
    
    [NXT_Peru_iPhoneStyler styleTextField:accountTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    [accountTextField_ setPlaceholder:@""];

     ImagesCache *imagesCache = [ImagesCache getInstance];
    [accountsButton_ setImage:[imagesCache imageNamed:ICON_CONTACT_PHOTO_PLACEHOLDER_FILE_NAME] forState:UIControlStateNormal];
    
    [ccLabel_ setAlpha:0.0f];
    [CcTextField_ setAlpha:0.0f];
    [entityTextField_ setAlpha:0.0f];
    
    [accountsButton_ setHidden:YES];
    [self recalculateSize];
    
}

/*
 * Creates and returns an autoreleased TimeAndDistanceHeaderView constructed from a NIB file
 */
+ (SimpleSelectorAccountView *)simpleSelectorAccountView {
    
    SimpleSelectorAccountView *result = (SimpleSelectorAccountView *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [result awakeFromNib];
    
    return result;
    
}

#pragma mark -
#pragma mark View information

/*
 * Returns the view height
 */
+ (CGFloat)viewHeight {
    
    return VIEW_HEIGHT;
    
}

/*
 * Recalculate the view size
 */
- (void)recalculateSize {
    
    CGRect frame = bankLabel_.frame;
    
    float originX = (showAccountSelector_)?-12.0f:0;
    float deltaX = (showAccountSelector_)?-7.0f:0;
    if(showCc_) {
        
        [ccLabel_ setAlpha:1.0f];
        [CcTextField_ setAlpha:1.0f];
        [entityTextField_ setAlpha:1.0f];
        [bankNumberLabel_ setAlpha:0.0f];
        
        frame.origin.x = 7.0f + originX;
        frame.origin.y = 37.0f;
        frame.size.height = [Tools labelHeight:bankLabel_ forText:bankLabel_.text];
        frame.size.width = 42.0f;
        bankLabel_.frame = frame;
        
        frame = entityTextField_.frame;
        frame.origin.x = 7.0f + originX;
        frame.origin.y = 4.0f;
        frame.size.width = 42.f;
        entityTextField_.frame = frame;
        
        originX = (showAccountSelector_)?-10.0f:0;
        
        frame = officeTextField_.frame;
        frame.origin.x = 57.0f + originX + deltaX;
        frame.origin.y = 4.0f;
        frame.size.width = 52.0f;
        officeTextField_.frame = frame;
        
        frame = officeLabel_.frame;
        frame.origin.x = 57.0f + originX + deltaX;
        frame.origin.y = 37.0f;
        frame.size.height = [Tools labelHeight:officeLabel_ forText:officeLabel_.text];
        frame.size.width = 52.0f;
        officeLabel_.frame = frame;
        [officeLabel_ setTextAlignment:UITextAlignmentCenter];
        
        frame = accountTextField_.frame;
        frame.origin.x = 115.0f + originX + deltaX;
        frame.origin.y = 4.0f;
        frame.size.width = 126.0f;
        accountTextField_.frame = frame;
        
        frame = accountLabel_.frame;
        frame.origin.x = 115.0f + originX + deltaX;
        frame.origin.y = 37.0f;
        frame.size.height = [Tools labelHeight:accountLabel_ forText:accountLabel_.text];
        frame.size.width = 126.0f;
        accountLabel_.frame = frame;
        [accountLabel_ setTextAlignment:UITextAlignmentCenter];
        
        frame = ccLabel_.frame;
        frame.origin.x = 246.0f + originX + deltaX;
        frame.origin.y = 37.0f;
        frame.size.height = [Tools labelHeight:ccLabel_ forText:ccLabel_.text];
        frame.size.width = 42.0f;
        [ccLabel_ setTextAlignment:UITextAlignmentCenter];
        ccLabel_.frame = frame;
        
        frame = CcTextField_.frame;
        frame.origin.x = 246.0f + originX + deltaX;
        frame.origin.y = 4.0f;
        frame.size.width = 42.0f;
        CcTextField_.frame = frame;
        
        if(showAccountSelector_)
        {
            frame = [accountsButton_ frame];
            frame.origin.x=275.0f;
            frame.origin.y = 4.0f;
            accountsButton_.frame = frame;
        }
        
    }
    else {         
        
        [ccLabel_ setAlpha:0.0f];
        [CcTextField_ setAlpha:0.0f];
        [entityTextField_ setAlpha:0.0f];
        [bankNumberLabel_ setAlpha:1.0f];
        
        frame.origin.x = 14.0f + originX;
        frame.origin.y = 37.0f;
        frame.size.height = [Tools labelHeight:bankLabel_ forText:bankLabel_.text];
        frame.size.width = 42.0f;
        bankLabel_.frame = frame;
        
        frame = bankNumberLabel_.frame;
        frame.origin.x = 14.0f + originX;
        frame.origin.y = 9.0f;
        CGFloat height = [Tools labelHeight:bankNumberLabel_ forText:bankNumberLabel_.text];
        frame.size.height = height;
        frame.size.width = 42.0f;
        bankNumberLabel_.frame = frame;                
        
        frame = officeTextField_.frame;
        frame.origin.x = 69.0f + originX + deltaX;
        frame.origin.y = 4.0f;
        frame.size.width = 85.0f;
        officeTextField_.frame = frame;
        
        frame = officeLabel_.frame;
        frame.origin.x = 69.0f + originX + deltaX;
        frame.origin.y = 37.0f;
        frame.size.height = [Tools labelHeight:officeLabel_ forText:officeLabel_.text];
        frame.size.width = 85.0f;
        officeLabel_.frame = frame;
        [officeLabel_ setTextAlignment:UITextAlignmentCenter];
        
        frame = accountTextField_.frame;
        frame.origin.x = 168.0f + originX + deltaX;
        frame.origin.y = 4.0f;
        frame.size.width = 122.0f;
        accountTextField_.frame = frame;
        
        frame = accountLabel_.frame;
        frame.origin.x = 168.0f + originX + deltaX;
        frame.origin.y = 37.0f;
        frame.size.height = [Tools labelHeight:accountLabel_ forText:accountLabel_.text];
        frame.size.width = 122.0f;
        accountLabel_.frame = frame;
        [accountLabel_ setTextAlignment:UITextAlignmentCenter];
        
        if(showAccountSelector_)
        {
            frame = [accountsButton_ frame];
            frame.origin.x = 275.0f;
            frame.origin.y = 4.0f;
            accountsButton_.frame = frame;
        }
        
    }
    
}

/*
 * set if cc have to show
 */
-(void)setShowCc:(BOOL)showCc {
    
    showCc_ = showCc;
    
    if(showCc) {
        
        [NXT_Peru_iPhoneStyler styleLabel:ccLabel_ withFontSize:11.0f color:[UIColor BBVAGreyToneTwoColor]];
        [ccLabel_ setText:NSLocalizedString(CCC_CC_TEXT_KEY, nil)];
        
        [NXT_Peru_iPhoneStyler styleTextField:CcTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
        [CcTextField_ setPlaceholder:@""];    
        
        [NXT_Peru_iPhoneStyler styleTextField:entityTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
        [entityTextField_ setPlaceholder:@""];
        
        [ccLabel_ setAlpha:1.0f];
        [CcTextField_ setAlpha:1.0f];
        [entityTextField_ setAlpha:1.0f];
        [bankNumberLabel_ setAlpha:0.0f];
    }
      
    [self recalculateSize];
}

-(void)setShowAccountSelector:(BOOL)showAccountSelector
{
    showAccountSelector_ = showAccountSelector;
    
    [accountsButton_ setHidden:!showAccountSelector];
    [self recalculateSize];

}

@end
