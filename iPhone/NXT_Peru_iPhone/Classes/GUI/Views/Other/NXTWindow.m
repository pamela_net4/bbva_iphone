/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTWindow.h"


#pragma mark -

@implementation NXTWindow

#pragma mark -
#pragma mark UIWindow selectors

/**
 * Returns the farthest descendant of the receiver in the view hierarchy (including itself) that contains a specified point.
 * used for capture all touches and send a notification
 *
 * @param point A point that is in the receiver’s coordinate system
 * @param event The event that triggered this method or nil if this method is invoked programmatically
 * @return A view object that is the farthest descendent of point. Returns nil if the point lies completely outside the receiver
 */
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:WINDOWS_TAPPED_NOTIFICATION object:self];
	
	return [super hitTest:point withEvent:event];
    
}

@end
