/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GlobalPositionSummaryView.h"

#import "NXT_Peru_iPhoneStyler.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "BannerView.h"

/**
 * Define the GlobalPositionSummaryView NIB file name
 */
#define NIB_FILE_NAME                                               @"GlobalPositionSummaryView"

/**
 * Define title font size
 */
#define TITLE_FONT_SIZE                                             14.0f

/**
 * Define detail font size
 */
#define DETAIL_FONT_SIZE                                            12.0f

/**
 * Define the view width
 */
#define VIEW_WIDTH                                                  320.0f

/**
 * Define the view height
 */
#define VIEW_HEIGHT                                                 197.0f

/**
 * Define label heigh
 */
#define LABEL_HEIGHT                                                15.0f

/**
 * Define margin heigh
 */
#define MARGIN_HEIGHT                                               5.0f




#pragma mark -

/**
 * GlobalPositionSummaryViewProvider private category
 */
@interface GlobalPositionSummaryViewProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (GlobalPositionSummaryViewProvider *)getInstance;

/**
 * Creates and returns an autoreleased GlobalPositionSummaryView constructed from a NIB file
 *
 * @return The autoreleased GlobalPositionSummaryView constructed from a NIB file
 */
- (GlobalPositionSummaryView *)globalPositionSummaryView;

@end


#pragma mark -

@implementation GlobalPositionSummaryViewProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * GlobalPositionSummaryViewProvider singleton only instance
 */
static GlobalPositionSummaryViewProvider *globalPositionSummaryViewProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([GlobalPositionSummaryViewProvider class]) {
        
        if (globalPositionSummaryViewProviderInstance_ == nil) {
            
            globalPositionSummaryViewProviderInstance_ = [super allocWithZone:zone];
            return globalPositionSummaryViewProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (GlobalPositionSummaryViewProvider *)getInstance {
    
    if (globalPositionSummaryViewProviderInstance_ == nil) {
        
        @synchronized([GlobalPositionSummaryViewProvider class]) {
            
            if (globalPositionSummaryViewProviderInstance_ == nil) {
                
                globalPositionSummaryViewProviderInstance_ = [[GlobalPositionSummaryViewProvider alloc] init];
                
            }
            
        }
        
    }
    
    return globalPositionSummaryViewProviderInstance_;
    
}

#pragma mark -
#pragma mark MovementDetailView creation

/*
 * Creates and returns aa autoreleased MovementDetailView constructed from a NIB file
 */
- (GlobalPositionSummaryView *)GlobalPositionSummaryView {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    GlobalPositionSummaryView *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation GlobalPositionSummaryView

#pragma mark -
#pragma mark Properties

@synthesize dateLabel = dateLabel_;
@synthesize welcomeLabel = welcomeLabel_;
@synthesize nameLabel = nameLabel_;
@synthesize titleAndAttributesArray = titleAndAttributesArray_;
@synthesize separator = separator_;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [dateLabel_ release];
    dateLabel_ = nil;
    
    [welcomeLabel_ release];
    welcomeLabel_ = nil;
    
    [nameLabel_ release];
    nameLabel_ = nil;
    
    [titleAndAttributesArray_ release];
    titleAndAttributesArray_ = nil;
    
    [labelsArray_ release];
    labelsArray_ = nil;
    
    [separator_ release];
    separator_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles the header label
 * and loads the map icon image
 */
- (void)awakeFromNib {
    self.backgroundColor = [UIColor whiteColor];
    
    separator_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    [NXT_Peru_iPhoneStyler styleLabel:dateLabel_ withFontSize:DETAIL_FONT_SIZE color:[UIColor grayColor]];
    
    [NXT_Peru_iPhoneStyler styleLabel:welcomeLabel_
                         withFontSize:TITLE_FONT_SIZE
                                color:[UIColor BBVABlueSpectrumToneTwoColor]];
    [welcomeLabel_ setText:NSLocalizedString(TXT_GLOBAL_POSITION_WELCOME_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleLabel:nameLabel_
                         withFontSize:TITLE_FONT_SIZE
                                color:[UIColor blackColor]];

    labelsArray_ = [[NSMutableArray alloc] init];

}

/*
 * Creates and returns an autoreleased GlobalPositionSummaryView constructed from a NIB file
 */
+ (GlobalPositionSummaryView *)globalPositionSummaryView {
    
    return [[GlobalPositionSummaryViewProvider getInstance] GlobalPositionSummaryView];
    
}

#pragma mark -
#pragma mark View information

/*
 * Returns the width of the view
 */
+ (CGFloat)width {
    return VIEW_WIDTH;
}

/*
 * Returns the height of the view
 */
+ (CGFloat)heightForInfo:(NSArray *)info andBannerVisible:(BOOL)isBannerVisible {
    
    CGFloat result = (10.0f + (2.0f * (LABEL_HEIGHT + MARGIN_HEIGHT))) + 2.0f;
    
    if(isBannerVisible){
        
        result = result + [BannerView heightBanner] + MARGIN_HEIGHT;
        
    }
    
    for (TitleAndAttributes *titleAndAttributes in info) {
        
        NSString *title = [titleAndAttributes titleString];
        
        if ((title != nil) && ([title length] > 0)) {
            
            result += LABEL_HEIGHT;
            
        }
        
        for (NSString *attribute in [titleAndAttributes attributesArray]) {
            
            if ((attribute != nil) && ([attribute length] > 0)) {
                
                result += LABEL_HEIGHT;
                
            }
            
        }
        
        result += MARGIN_HEIGHT;
        
    }
    
    return result;
}

#pragma mark -
#pragma mark Properties methods


/**
 * Sets the information
 */
- (void)setTitleAndAttributesArray:(NSArray *)titleAndAttributesArray
                           forName:(NSString *)name
                              date:(NSDate *)date
                      andCampaigns:(NSArray*)campaignList
                     andController:(id)controller
                  andBannerVisible:(BOOL)isBannerVisible{
	
	// Convert string to date object
	NSDateFormatter *dateFormat = [[[NSDateFormatter alloc] init] autorelease];
	
	// Convert date object to desired output format
	[dateFormat setDateFormat:NSLocalizedString(DATE_FORMATER_TEXT_KEY, nil)];
	
	// Capitalize the first letter of date.
	NSString *dateString = [dateFormat stringFromDate:date];
	
	NSString *resultString = [dateString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[dateString substringToIndex:1] capitalizedString]];
	
    [dateLabel_ setText:resultString];
    
    CGFloat width = CGRectGetWidth([self frame]);
    
    CGSize welcomeMaxSize = CGSizeMake(round(width / 2.0f), CGFLOAT_MAX);
    CGSize welcomeSize = [[welcomeLabel_ text] sizeWithFont:[welcomeLabel_ font]
                                          constrainedToSize:welcomeMaxSize];
    CGRect welcomeFrame = [welcomeLabel_ frame];
    welcomeFrame.size.width = welcomeSize.width;
    [welcomeLabel_ setFrame:welcomeFrame];
    
    CGFloat nameLeft = CGRectGetMaxX(welcomeFrame) + 5.0f;
    CGFloat nameWidth = width - CGRectGetMinX(welcomeFrame) - nameLeft;
    CGRect nameFrame = [nameLabel_ frame];
    nameFrame.origin.x = nameLeft;
    nameFrame.size.width = nameWidth;
    [nameLabel_ setFrame:nameFrame];
    [nameLabel_ setText:name];
    
    if (titleAndAttributesArray_ != titleAndAttributesArray) {
        
        [titleAndAttributesArray retain];
        [titleAndAttributesArray_ release];
        titleAndAttributesArray_ = titleAndAttributesArray;
    
    }
    
    CGFloat yPosition = CGRectGetMaxY(nameFrame) + MARGIN_HEIGHT;
    
    if(isBannerVisible){
        
        bannerView = [BannerView bannerView];
        CGRect frameBannerView = bannerView.frame;
        frameBannerView.origin.x = 0.0f;
        frameBannerView.origin.y = yPosition;
        frameBannerView.size.height = [BannerView heightBanner];
        frameBannerView.size.width = width - 0.0f;
        bannerView.frame = frameBannerView;
    
        [self addSubview:bannerView];
    
        [bannerView setContent:campaignList];
        [bannerView.closeButton addTarget:controller action:@selector(bannerButtonClose) forControlEvents:UIControlEventTouchUpInside];
        bannerView.delegate = self;
    
        yPosition = yPosition + [BannerView heightBanner] + MARGIN_HEIGHT;
    }
    
    
    [labelsArray_ removeAllObjects];
    
    for (TitleAndAttributes *titleAndAttributes in titleAndAttributesArray_) {
        
        NSString *title = [titleAndAttributes titleString];
        NSArray *attributes = [titleAndAttributes attributesArray];
        
        UILabel *titleLabel = nil;
        UILabel *detailLabel = nil;
        
        if ((title != nil) && ([title length] > 0)) {
            
            titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10.0f, yPosition, 300.0f, LABEL_HEIGHT)] autorelease];
            [NXT_Peru_iPhoneStyler styleLabel:titleLabel withFontSize:TITLE_FONT_SIZE color:[UIColor blackColor]];
            titleLabel.backgroundColor = [UIColor clearColor];

            [titleLabel setText:title];
            
            [self addSubview:titleLabel];
            [labelsArray_ addObject:titleLabel];
            
            yPosition += LABEL_HEIGHT;
            
            
        }
        
        for (NSString *attribute in attributes) {
            
            if ((attribute != nil) && ([attribute length] > 0)) {
                
                detailLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10.0f, yPosition, 300.0f, LABEL_HEIGHT)] autorelease];
                [NXT_Peru_iPhoneStyler styleLabel:detailLabel withFontSize:DETAIL_FONT_SIZE color:[UIColor grayColor]];
                detailLabel.backgroundColor = [UIColor clearColor];
                
                [detailLabel setText:attribute];
                
                [self addSubview:detailLabel];
                [labelsArray_ addObject:detailLabel];

                yPosition += LABEL_HEIGHT;
                
            }
        
        }
        
        yPosition += MARGIN_HEIGHT;
                
    }
    
    CGRect frame = separator_.frame;
    frame.origin.y = yPosition;
    separator_.frame = frame;
    
}


#pragma BannerDelegate
-(void) bannerSelected:(int32_t)position{
    [delegate_ bannerSelected:position];
}

                    

@end