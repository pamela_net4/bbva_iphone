//
//  SelectionItemsPopView.h
//  Created by Author.
//

#import <UIKit/UIKit.h>
#import "Tooltip.h"

@protocol SelectionItemsPopViewDelegate

-(void)onDismissSelectionItems:(NSArray*)selecteds;

@end


@class NPopup;

@interface SelectionItemsPopView : UIView<UITableViewDataSource, UITableViewDelegate, TooltipDelegate>{
@private
    NPopup * popup_;
    
    UIView *view_;
    UITableView *tableView_;
    UIButton *button_;
    UILabel *titleLabel_;
    UIView *headerView_;
    
    NSArray * items_;
    NSMutableArray * selecteds_;
    id<SelectionItemsPopViewDelegate> delegate_;
}

@property (strong, nonatomic) id<SelectionItemsPopViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIView *headerView;

-(void)showInWindow:(UIWindow*)parentWindow;


-(void) setItemsString:(NSArray *)itemsString;
-(void)setValuesSelected:(NSArray *)selections;



@end
