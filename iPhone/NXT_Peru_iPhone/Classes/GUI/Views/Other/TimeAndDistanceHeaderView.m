/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "TimeAndDistanceHeaderView.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "StringKeys.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"


/**
 * Define the TimeAndDistanceHeaderView NIB file name
 */
#define NIB_FILE_NAME                                               @"TimeAndDistanceHeaderView"


/**
 * Defines the view height
 */
#define VIEW_HEIGHT                                                 40.0f


#pragma mark -

/**
 * TimeAndDistanceHeaderView private extension
 */
@interface TimeAndDistanceHeaderView()

/**
 * Updates the distance and time displayed using the stored information
 *
 * @private
 */
- (void)updateDistanceAndTimeDisplayed;

@end


#pragma mark -

@implementation TimeAndDistanceHeaderView

#pragma mark -
#pragma mark Properties

@synthesize backgroundImageView = backgroundImageView_;
@synthesize distanceAndTimeLabel = distanceAndTimeLabel_;
@synthesize routeTypeSelectorSegmentedControl = routeTypeSelectorSegmentedControl_;
@synthesize distanceText = distanceText_;
@synthesize timeText = timeText_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [backgroundImageView_ release];
    backgroundImageView_ = nil;
    
    [distanceAndTimeLabel_ release];
    distanceAndTimeLabel_ = nil;
    
    [distanceText_ release];
    distanceText_ = nil;
    
    [timeText_ release];
    timeText_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the view elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    self.backgroundColor = [UIColor clearColor];

    UIColor *labelColor = [UIColor grayColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:distanceAndTimeLabel_
             withBoldFontSize:15.0f
                        color:labelColor];

    backgroundImageView_.image = [imagesCache imageNamed:HEADER_BACKGROUND_IMAGE_FILE_NAME];
    backgroundImageView_.alpha = 0.75f;
    
    [routeTypeSelectorSegmentedControl_ setImage:[imagesCache imageNamed:ROUTE_ON_FOOT_IMAGE_FILE_NAME]
                               forSegmentAtIndex:0];
    [routeTypeSelectorSegmentedControl_ setImage:[imagesCache imageNamed:ROUTE_BY_CAR_IMAGE_FILE_NAME]
                               forSegmentAtIndex:1];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
    [routeTypeSelectorSegmentedControl_ setTintColor:[UIColor BBVAGreyToneFourColor]];
    }

    
}

/*
 * Creates and returns an autoreleased TimeAndDistanceHeaderView constructed from a NIB file
 */
+ (TimeAndDistanceHeaderView *)timeAndDistanceHeaderView {
    
    return (TimeAndDistanceHeaderView *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
}

#pragma mark -
#pragma mark View information

/*
 * Returns the view height
 */
+ (CGFloat)viewHeight {
    
    return VIEW_HEIGHT;
    
}

#pragma mark -
#pragma mark Graphic elements management

/*
 * Updates the distance and time displayed using the stored information
 */
- (void)updateDistanceAndTimeDisplayed {
    
    NSMutableString *labelText = [NSMutableString stringWithString:distanceText_];
    
    if ([timeText_ length] > 0) {
        
        if ([labelText length] > 0) {
            
            [labelText appendString:@" "];
            
        }
        
        [labelText appendString:timeText_];
        
    }
    
    distanceAndTimeLabel_.text = labelText;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the new distance text to display
 *
 * @param distanceText The new distance text to display
 */
- (void)setDistanceText:(NSString *)distanceText {
    
    if (distanceText != distanceText_) {
        
        [distanceText_ release];
        distanceText_ = nil;
        distanceText_ = [distanceText copyWithZone:self.zone];
        
        [self updateDistanceAndTimeDisplayed];
        
    }
    
}

/*
 * Sets the new time text to display
 *
 * @param timeText The new time text to display
 */
- (void)setTimeText:(NSString *)timeText {
    
    if (timeText != timeText_) {
        
        [timeText_ release];
        timeText_ = nil;
        timeText_ = [timeText copyWithZone:self.zone];
        
        [self updateDistanceAndTimeDisplayed];
        
    }
    
}

@end
