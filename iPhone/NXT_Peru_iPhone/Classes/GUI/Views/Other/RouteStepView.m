/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "RouteStepView.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "Tools.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"


/**
 * Define the RouteStepView NIB file name
 */
#define NIB_FILE_NAME                                               @"RouteStepView"

/**
 * Defines the step text top and bottom offset measured in points
 */
#define STEP_TEXT_OFFSET                                            10.0f

/**
 * Defines the labels horizontal distance from one to another
 */
#define LABELS_HORIZONTAL_DISTANCE                                  5.0f

/**
 * Defines the view expand animation duration, measured in seconds
 */
#define EXPAND_ANIMATION_DURATION                                   0.2f


#pragma mark -

/**
 * RouteStepView private catagory
 */
@interface RouteStepView(private)

/**
 * Sets the route steps count label text with the available information
 */
- (void)updateRouteStepsLabelText;

@end


#pragma mark -

@implementation RouteStepView

#pragma mark -
#pragma mark Properties

@synthesize backgroundImageView = backgroundImageView_;
@synthesize separatorImageView = separatorImageView_;
@synthesize stepNumberAndTotalLabel = stepNumberAndTotalLabel_;
@synthesize stepTextLabel = stepTextLabel_;
@synthesize stepsSegmentedControl = stepsSegmentedControl_;
@synthesize routeStepsCount = routeStepsCount_;
@synthesize stepIndex = stepIndex_;
@dynamic stepText;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {

    [backgroundImageView_ release];
    backgroundImageView_ = nil;
	
	[separatorImageView_ release];
	separatorImageView_ = nil;
    
    [stepNumberAndTotalLabel_ release];
    stepNumberAndTotalLabel_ = nil;
    
    [stepTextLabel_ release];
    stepTextLabel_ = nil;
    
    [stepsSegmentedControl_ release];
    stepsSegmentedControl_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Style is applied to the view elements
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
	
	[separatorImageView_ setImage:[imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME]];    
    
    UIColor *clearColor = [UIColor clearColor];
    [self setBackgroundColor:clearColor];
    self.backgroundColor = clearColor;
    backgroundImageView_.backgroundColor = clearColor;
    backgroundImageView_.image = [imagesCache imageNamed:HEADER_BACKGROUND_IMAGE_FILE_NAME];
    backgroundImageView_.alpha = 0.75f;

    UIColor *labelsColor = [UIColor grayColor];
    
    [NXT_Peru_iPhoneStyler styleLabel:stepNumberAndTotalLabel_
                 withFontSize:14.0f
                        color:labelsColor];
    [stepNumberAndTotalLabel_ setTextAlignment:UITextAlignmentCenter];
    [stepNumberAndTotalLabel_ setNumberOfLines:1];


    [NXT_Peru_iPhoneStyler styleLabel:stepTextLabel_
                 withFontSize:14.0f
                        color:labelsColor];
    [stepTextLabel_ setTextAlignment:UITextAlignmentLeft];
    [stepTextLabel_ setNumberOfLines:0];
    
    [stepsSegmentedControl_ setImage:[imagesCache imageNamed:PREVIOUS_ROUTE_STEP_IMAGE_FILE_NAME]
                   forSegmentAtIndex:0];
    [stepsSegmentedControl_ setImage:[imagesCache imageNamed:NEXT_ROUTE_STEP_IMAGE_FILE_NAME]
                   forSegmentAtIndex:1];
    //[stepsSegmentedControl_ setTintColor:[UIColor BBVABlueSpectrumColor]];
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
    [stepsSegmentedControl_ setBackgroundColor:[UIColor BBVAGreyToneFourColor]];
    }
    
    
}

/*
 * Creates and returns an autoreleased RouteStepView constructed from a NIB file
 */
+ (RouteStepView *)routeStepView {
    
    return (RouteStepView *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
}

#pragma mark -
#pragma mark Step text management

/*
 * Sets the step text, and the view resizing can be animated or not
 */
- (void)setStepText:(NSString *)aStepText
           animated:(BOOL)animated {
    
    UIFont *stepsFont = [stepNumberAndTotalLabel_ font];
    CGSize stepsSize = [[stepNumberAndTotalLabel_  text] sizeWithFont:stepsFont];
    
    CGRect stepsFrame = [stepNumberAndTotalLabel_ frame];
    stepsFrame.size.width = stepsSize.width;
    [stepNumberAndTotalLabel_ setFrame:stepsFrame];
    
    CGRect stepTextFrame = [stepTextLabel_ frame];
    CGFloat maxXPos = CGRectGetMaxX(stepTextFrame);
    CGFloat minXPos = CGRectGetMaxX(stepsFrame) + LABELS_HORIZONTAL_DISTANCE;
    stepTextFrame.origin.x = minXPos;
    stepTextFrame.size.width = maxXPos - minXPos;
    [stepTextLabel_ setFrame:stepTextFrame];
    
    CGFloat labelHeight = [Tools labelHeight:stepTextLabel_
                                     forText:aStepText];
    [stepTextLabel_ setText:aStepText];
    CGFloat viewHeight = MAX(labelHeight, 20.0f) + (2.0f * STEP_TEXT_OFFSET);
    CGRect frame = [self frame];
    frame.size.height = viewHeight;
    
    if (animated) {
        
        if ([[UIView class] respondsToSelector:@selector(animateWithDuration:delay:options:animations:completion:)]) {
            
            [UIView animateWithDuration:EXPAND_ANIMATION_DURATION
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 
                                 [self setFrame:frame];
                                 
                             }
                             completion:nil];
            
        } else {
            
            [UIView beginAnimations:nil
                            context:nil];
            [UIView setAnimationDuration:EXPAND_ANIMATION_DURATION];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            
            [self setFrame:frame];
            
            [UIView commitAnimations];
            
        }

    } else {
        
        [self setFrame:frame];
        
    }
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the new route steps count and updates the corresponding label
 *
 * @param aValue The new route steps count to set
 */
- (void)setRouteStepsCount:(NSUInteger)aValue {
    
    routeStepsCount_ = aValue;
    [self updateRouteStepsLabelText];
    
}

/*
 * Sets the new step index and updates the corresponding label
 *
 * @param aValue The new step index to set
 */
- (void)setStepIndex:(NSUInteger)aValue {
    
    stepIndex_ = aValue;
    [self updateRouteStepsLabelText];
    
}

/*
 * Returns the step text
 *
 * @return The step text
 */
- (NSString *)stepText {
    
    return stepTextLabel_.text;
    
}

/*
 * Sets the step text, and updates the control height if needed
 *
 * @param aValue The new step text to set
 */
- (void)setStepText:(NSString *)aValue {

    [self setStepText:aValue
             animated:NO];
    
}

@end


#pragma mark -

@implementation RouteStepView(private)

#pragma mark -
#pragma mark Graphic interface

/*
 * Sets the route steps count label text with the available information
 */
- (void)updateRouteStepsLabelText {

    stepNumberAndTotalLabel_.text = [NSString stringWithFormat:@"%u/%u", stepIndex_, routeStepsCount_];
    
    [self setStepText:[stepTextLabel_ text]
             animated:YES];
    
}

@end

