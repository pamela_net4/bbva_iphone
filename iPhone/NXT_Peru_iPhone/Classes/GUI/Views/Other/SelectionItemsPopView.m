//
//  SelectionItemsPopView.m
//  Created by Author.
//

#import "SelectionItemsPopView.h"
#import <QuartzCore/QuartzCore.h>
#import "NPopup.h"
#import "CheckCell.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"
#import "Tooltip.h"

@implementation SelectionItemsPopView

@synthesize view = view_;
@synthesize button = button_;
@synthesize tableView = tableView_;
@synthesize titleLabel = titleLabel_;
@synthesize headerView = headerView_;
@synthesize delegate = delegate_;

-(void)dealloc{
    
    [selecteds_ release];
    selecteds_ = nil;
    
    delegate_ = nil;
    
    [button_ release];
    button_ = nil;
    
    [tableView_ release];
    tableView_ = nil;
    
    [titleLabel_ release];
    titleLabel_ = nil;
    
    [headerView_ release];
    headerView_ = nil;
    
    [view_ release];
    view_ = nil;
    
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code              
        [[NSBundle mainBundle] loadNibNamed:@"SelectionItemsPopView" owner:self options:nil];
        [self addSubview: self.view];
        
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        
        UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
        if (UIDeviceOrientationIsLandscape(deviceOrientation)) {
            CGFloat tmp = screenWidth;
            screenWidth = screenHeight;
            screenHeight = tmp;
        }
        self.frame = CGRectMake(screenWidth/2 - CGRectGetWidth(self.view.bounds)/2,
                                screenHeight/2 - CGRectGetHeight(self.view.bounds)/2,
                                CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
        NSLog(@"bounds %@", NSStringFromCGRect(self.frame));
        
        
        
        items_ = [[NSArray alloc] init];
        
        //awake from nib
    }
    return self;
}

-(void)setValuesSelected:(NSArray *)selections{
    for (int i=0; i<selecteds_.count; i++) {
        [selecteds_ replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
    }
    for (int i=0; i<selections.count; i++) {
        int indexSelection = (int)[[selections objectAtIndex:i] integerValue];
        [selecteds_ replaceObjectAtIndex:indexSelection withObject:[NSNumber numberWithBool:YES]];
    }
    [tableView_ reloadData];
}

-(void) setItemsString:(NSArray *)itemsString{
    
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:13.0f color:[UIColor whiteColor]];
    headerView_.backgroundColor = [UIColor colorWithRed:10.0/255.0 green:88.0/255.0 blue:179.0/255.0 alpha:1.0];
    
    items_ = itemsString;
    selecteds_ = [[[NSMutableArray alloc] init] retain];
    
    for (int i=0; i<itemsString.count; i++) {
        [selecteds_ addObject:[NSNumber numberWithBool:NO]];
    }
    
    tableView_.dataSource = self;
    tableView_.delegate = self;
    tableView_.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tableView_ reloadData];
}

- (void) layoutSubviews{   
    [super layoutSubviews];
    
    //styles
    
    
}

//pop methods

-(IBAction)dismissValid:(id)sender{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    if([self isValid]){

        if(popup_){
            [popup_ close];
        }
    }
}

-(void)showInWindow:(UIWindow*)parentWindow{
    
    if(!popup_){
        popup_ = [[NPopup alloc] initWithParentWindow:parentWindow];
        popup_.backgroundColor = [UIColor clearColor];
        popup_.dialogView.backgroundColor = [UIColor clearColor];
        [popup_ setContainerView:self];
    }
    [popup_ show];
}

#pragma UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return items_.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CheckCell * checkCell_ = [[CheckCell checkCell] retain];
    [[checkCell_ topTextLabel] setText:[items_ objectAtIndex:indexPath.row]];
    [checkCell_ setShowSeparator:YES];
    
    NSNumber * boolSelectedNumber = [selecteds_ objectAtIndex:indexPath.row];
    BOOL boolSelected = [boolSelectedNumber boolValue];
    [checkCell_ setCheckActive:boolSelected];
    
    [NXT_Peru_iPhoneStyler styleLabel:[checkCell_ topTextLabel] withFontSize:13.0f color:[UIColor grayColor]];
    [checkCell_ setShowSeparator:NO];
    
    return checkCell_;
}

#pragma UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSNumber * boolSelectedNumber = [selecteds_ objectAtIndex:indexPath.row];
    BOOL opBoolSelected = ![boolSelectedNumber boolValue];
    [selecteds_ replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:opBoolSelected]];
    
    [tableView_ reloadData];
}


#pragma - metodos para no ocultar los textfields con el teclado al editar en ellos

-(IBAction) slideFrameUp:(UIView *) view1
{
    [self slideFrame:view1 up:YES];
}

-(IBAction) slideFrameDown:(UIView *) view1
{
    [self slideFrame:view1 up:NO];
}

-(void) slideFrame:(UIView*)view1 up:(BOOL) up
{
    const float movementDuration = 0.3f; // adjust
    
    if(up){
       // scrollView.contentSize = CGSizeMake(280, 320);
       // scrollView.showsVerticalScrollIndicator = YES;
        
        if((view_.frame.origin.y + view1.frame.size.height) > 60){
            [UIView beginAnimations: @"anim" context: nil];
            [UIView setAnimationBeginsFromCurrentState: YES];
            [UIView setAnimationDuration: movementDuration];
         //   self.scrollView.contentOffset = CGPointMake(0, view1.frame.origin.y + 10);
            [UIView commitAnimations];
        }
    }
    else {
        //scrollView.contentSize = CGSizeMake(280, 205);
        //scrollView.showsVerticalScrollIndicator = NO;
        
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
      //  self.scrollView.contentOffset = CGPointMake(0, 0);
        [UIView commitAnimations];
    }
}

#pragma - validacion

-(BOOL) isValid{
    
    int countYes = 0;
    NSMutableArray * selections = [[NSMutableArray alloc] init];
    for (int i=0; i<selecteds_.count; i++) {
        NSNumber * boolSelectedNumber = [selecteds_ objectAtIndex:i];
        BOOL boolSelected = [boolSelectedNumber boolValue];
        if(boolSelected){
            [selections addObject:[NSNumber numberWithInt:i]];
            countYes++;
        }
    }
    
    if(countYes == 2){
        [delegate_ onDismissSelectionItems:[[NSArray arrayWithArray:selections] retain]];
        return YES;
    }
    else{
     
        headerView_.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1.0];
        [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:13.0f
                                    color:[UIColor colorWithRed:163.0/255.0 green:0.0/255.0 blue:98.0/255.0 alpha:1.0]];
        
        [self showErrorTooltipFromView:titleLabel_ message:@"Selecciona 2 meses para el pago de cuotas dobles"];
        
    }
    

    return NO;
}

-(void)showErrorTooltipFromView:(UIView*)fromView message:(NSString*) message{
    Tooltip * tooltipnew = [[Tooltip alloc] initWithMessage:message
                                                     source:fromView parentWindow:self.view.window];
    tooltipnew.delegate = self;
    tooltipnew.hideShadow = YES;
    tooltipnew.arrowColor = [UIColor colorWithRed:245.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1];
    tooltipnew.borderColor =[UIColor colorWithRed:241.0/255.0 green:210.0/255.0 blue:223.0/255.0 alpha:1];
    tooltipnew.textColor = [UIColor colorWithRed:184.0/255.0 green:0.0/255.0 blue:97.0/255.0 alpha:1];
    tooltipnew.label.textAlignment = NSTextAlignmentCenter;
    [tooltipnew show];
}

#pragma TooltipDelegate

-(void)onDismiss:(UIView*)view{
    headerView_.backgroundColor = [UIColor colorWithRed:10.0/255.0 green:88.0/255.0 blue:179.0/255.0 alpha:1.0];
    [NXT_Peru_iPhoneStyler styleLabel:titleLabel_ withFontSize:13.0f color:[UIColor whiteColor]];
}


@end
