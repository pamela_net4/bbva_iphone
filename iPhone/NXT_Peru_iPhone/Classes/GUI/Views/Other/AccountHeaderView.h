/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>
#import "SingletonBase.h"

@class AccountHeaderView;

/**
 * Provider to obtain a AccountHeaderView created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountHeaderViewProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary AccountHeaderView to create it from a NIB file
     */
    AccountHeaderView *auxView_;
    
}

/**
 * Provides read-write access to the auxililary AccountHeaderView
 */
@property (nonatomic, readwrite, assign) IBOutlet AccountHeaderView *auxView;

@end

/**
 * Account header with the concept, date and amount
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountHeaderView : UIView {
@private
    /**
     * Top left label
     */
    UILabel *topLeftLabel_;
    
    /**
     * Bottom left label
     */
    UILabel *bottomLeftLabel_;
    
    /**
     * Top right label
     */
    UILabel *topRightLabel_;
    
    /**
     * Bottom right label
     */
    UILabel *bottomRightLabel_;
    
    /**
     * Separator line
     */
    UIImageView *separator_;

}

/**
 * Provides readwrite access to the topLeftLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *topLeftLabel;

/**
 * Provides readwrite access to the topLeftLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *bottomLeftLabel;

/**
 * Provides readwrite access to the topLeftLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *topRightLabel;

/**
 * Provides readwrite access to the topLeftLabel. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *bottomRightLabel;

/**
 * Provides readwrite access to the separator1. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *separator;

/**
 * Provides readwrite access to the title
 */
@property (nonatomic, readwrite, copy) NSString *topTitle;

/**
 * Provides readwrite access to the title
 */
@property (nonatomic, readwrite, copy) NSString *bottomTitle;

/**
 * Provides readwrite access to the amount
 */
@property (nonatomic, readwrite, copy) NSString *topDetail;

/**
 * Provides readwrite access to the amount
 */
@property (nonatomic, readwrite, copy) NSString *bottomDetail;

/**
 * Creates and returns an autoreleased AccountHeaderView constructed from a NIB file
 *
 * @return The autoreleased AccountHeaderView constructed from a NIB file
 */
+ (AccountHeaderView *)accountHeaderView;

/**
 * Returns the width of the view
 */
+ (CGFloat)width;

/**
 * Returns the height of the view
 */
+ (CGFloat)height;

@end
