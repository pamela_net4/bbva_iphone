/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <MapKit/MapKit.h>


/**
 * Annotation view to display a route step annotation
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface StepAnnotationView : MKAnnotationView {
    
@private
    
    /**
     * Step icon image view
     */
    UIImageView *stepAnnotationImageView_;
    
}


/**
 * Provides read-write access to the annotation image
 */
@property (nonatomic, readwrite, retain) UIImage *stepAnnotationImage;

@end
