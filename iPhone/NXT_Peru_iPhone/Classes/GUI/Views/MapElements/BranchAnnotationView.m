/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BranchAnnotationView.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"


#pragma mark -

@implementation BranchAnnotationView

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [annotationImageView_ release];
    annotationImageView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes a BranchAnnotationView instance providing it with an image
 *
 * @param annotation The annotation object to associate with the new view
 * @param reuseIdentifier If you plan to reuse the annotation view for similar types of annotations, pass a string to identify it
 * @return The initialized BranchAnnotationView instance
 */
- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
        
        CGRect frame = CGRectMake(0.0f, 0.0f, 26.0f, 32.0f);
        [self setFrame:frame];
        
        annotationImageView_ = [[UIImageView alloc] initWithFrame:frame];
        annotationImageView_.image = [[ImagesCache getInstance] imageNamed:BRANCH_POI_ANNOTATION_VIEW_IMAGE_FILE_NAME];
        [self addSubview:annotationImageView_];
        
    }
    
    return self;
    
}

/**
 * The offset (in pixels) at which to display the view.
 */
- (CGPoint)centerOffset {
    
    return CGPointMake(0.0f, -16.0f);
    
}

@end
