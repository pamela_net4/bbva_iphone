/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseAnnotation.h"


//Forward declarations
@class ATMData;


/**
 * Annotation object used to represent POIs inside a map view
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface POIAnnotation : BaseAnnotation {
    
@private
    
    /**
     * Associated POI
     */
    ATMData *poi_;

}


/**
 * Provides read-write access to the associated POI
 */
@property (nonatomic, readwrite, retain) ATMData *poi;


/**
 * Designated initializer. Initializes a POIAnnotation instances providing the associated POI
 *
 * @param poi The annotation associated POI
 * @return The initialized POIAnnotation instance
 */
- (id)initWithAssociatedPOI:(ATMData *)poi;

@end
