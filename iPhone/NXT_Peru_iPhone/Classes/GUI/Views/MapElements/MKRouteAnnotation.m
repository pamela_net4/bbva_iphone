/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MKRouteAnnotation.h"
#import "GoogleMapsPolyline.h"


#pragma mark -

/**
 * MKRouteAnnotation private category
 */
@interface MKRouteAnnotation(private)

/**
 * Initializes the route annotation elements
 */
- (void)initializeRouteElements;

@end


#pragma mark -

@implementation MKRouteAnnotation

#pragma mark -
#pragma mark Properties

@synthesize polyline = polyline_;
@synthesize northernMostCoordinate = northernMostCoordinate_;
@synthesize westernMostCoordinate = westernMostCoordinate_;
@synthesize southernMostCoordinate = southernMostCoordinate_;
@synthesize easternMostCoordinate = easternMostCoordinate_;
@synthesize delegate = delegate_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {

	[polyline_ release];
	polyline_ = nil;
	
	[super dealloc];
	
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes an unlocated MKRouteAnnotation instance
 *
 * @return The initialized MKRouteAnnotation instance
 */
- (id)init {

	if ((self = [super init])) {
		
		[self initializeRouteElements];
		
	}
	
	return self;
	
}

#pragma mark -
#pragma mark Properties selectors

/**
 * Sets the new Google polyline to draw and invokes the route elements maths
 *
 * @param aPolyline The new Google polyline to store
 */
- (void)setPolyline:(GoogleMapsPolyline *)aPolyline {

    if (aPolyline != polyline_) {
        
        [polyline_ release];
        polyline_ = nil;
        polyline_ = [aPolyline retain];
        
        [self initializeRouteElements];
        [delegate_ routeAnnotationPointsChanged:self];

    }
	
}

@end


#pragma mark -

@implementation MKRouteAnnotation(private)

#pragma mark -
#pragma mark Instance initialization

/*
 * Initializes the route annotation elements
 */
- (void)initializeRouteElements {
	
	northernMostCoordinate_ = -90.0f;
	southernMostCoordinate_ = 90.0f;
	westernMostCoordinate_ = +180.0f;
	easternMostCoordinate_ = -180.0f;
    
    CLLocationCoordinate2D coordinate;
    CLLocationDegrees latitude;
    CLLocationDegrees longitude;
	
    NSArray *routePoints = polyline_.polylineArray;
    
	for (NSValue *coordinateInstance in routePoints) {
        
        [coordinateInstance getValue:&coordinate];
		latitude = coordinate.latitude;
		longitude = coordinate.longitude;
		
		if (latitude > northernMostCoordinate_) {
            
			northernMostCoordinate_ = latitude;
			
		}
		
		if (latitude < southernMostCoordinate_) {
            
			southernMostCoordinate_ = latitude;
			
		}
		
		if (longitude < westernMostCoordinate_) {
            
			westernMostCoordinate_ = longitude;
			
		}
		
		if (longitude > easternMostCoordinate_) {
            
			easternMostCoordinate_ = longitude;
			
		}
		
	}
	
}

@end
