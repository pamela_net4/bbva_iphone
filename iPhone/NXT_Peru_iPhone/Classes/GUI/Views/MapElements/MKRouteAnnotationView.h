/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <MapKit/MapKit.h>


//Forward class
@class MKRouteAnnotationDrawingView;


/**
 * Annotation view to display a route overlay by means of an annotation
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MKRouteAnnotationView : MKAnnotationView {

@private
	
	/**
	 * MapView where the annotation view is being displayed
	 */
	MKMapView *mapView_;
    
	/**
	 * Internal view to draw route
	 */
	MKRouteAnnotationDrawingView *drawingView_;
	
	/**
	 * Path to draw the route
	 */
	CGMutablePathRef routePath_;

}

/**
 * Provides read-write access to the map view where the annotation view is being displayed.
 * The associated MapView is not retained to avoid circular references
 */
@property (nonatomic, readwrite, assign) MKMapView *mapView;

/**
 * Updates the route representation
 */
- (void)updateRoute;

@end
