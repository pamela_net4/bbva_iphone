/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <MapKit/MapKit.h>


/**
 * Base class used for annotations
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BaseAnnotation : NSObject <MKAnnotation> {
    
@private
	
	/**
	 * Stores the annotation coordinate
	 */
	CLLocationCoordinate2D coordinate_;
    
    /**
     * Annotation title
     */
    NSString *title_;
    
    /**
     * Annotation subtitle
     */
    NSString *subtitle_;
    
}

/**
 * Provides read-write access to the annotation coordinate
 */
@property (nonatomic, readwrite, assign) CLLocationCoordinate2D coordinate;

/**
 * Provides read-write access to the annotation title
 */
@property (nonatomic, readwrite, copy) NSString *title;

/**
 * Provides read-write access to the aannotation subtitle
 */
@property (nonatomic, readwrite, copy) NSString *subtitle;

@end
