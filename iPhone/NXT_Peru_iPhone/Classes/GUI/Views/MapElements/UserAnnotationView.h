/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <MapKit/MapKit.h>


/**
 * Annotation view to represent the user position
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface UserAnnotationView : MKAnnotationView {

@private
    
    /**
     * Image to display
     */
    UIImageView *annotationImageView_;
    
}

@end
