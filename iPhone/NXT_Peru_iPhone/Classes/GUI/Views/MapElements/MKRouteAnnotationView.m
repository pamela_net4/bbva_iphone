/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MKRouteAnnotationView.h"
#import "MKRouteAnnotation.h"
#import "GoogleMapsPolyline.h"


#pragma mark -

/**
 * MKRouteAnnotationView internal view to draw the route
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MKRouteAnnotationDrawingView : UIView <MKRouteAnnotationDelegate> {

@private
	
	/**
	 * Path to draw the route
	 */
	CGMutablePathRef routePath_;
		
}


/**
 * Updates the route path
 */
- (void)updateRoutePath;

@end


#pragma mark -

@implementation MKRouteAnnotationDrawingView

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

	CGPathRelease(routePath_);
	routePath_ = NULL;
	
	[super dealloc];
	
}

#pragma mark -
#pragma mark Rendering

/**
 * Renders the route into the view
 *
 * @param rect The view rectangle to render
 */
- (void)drawRect:(CGRect)rect {

    if (routePath_ == NULL) {
    
        [self updateRoutePath];
        
    }
    
    if (routePath_ != NULL) {
        
        CGContextRef context = UIGraphicsGetCurrentContext();
    
        CGContextSaveGState(context);
        CGContextSetRGBStrokeColor(context, 0.0f, 0.0f, 1.0f, 0.5f);
        CGContextSetLineWidth(context, 5.0f);
        CGContextAddPath(context, routePath_);
        CGContextDrawPath(context, kCGPathStroke);
        CGContextRestoreGState(context);
        
    }
 
}

#pragma mark -
#pragma mark Path management

/*
 * Updates the route path
 */
- (void)updateRoutePath {
	
	CGPathRelease(routePath_);
	routePath_ = NULL;
	
	UIView *parentView = self.superview;
	
	if ([parentView isKindOfClass:[MKRouteAnnotationView class]]) {
		
		MKRouteAnnotationView *routeView = (MKRouteAnnotationView *)parentView;
		id <MKAnnotation> annotation = routeView.annotation;
		
		if ([(NSObject *)annotation isKindOfClass:[MKRouteAnnotation class]]) {

			MKRouteAnnotation *routeAnnotation = (MKRouteAnnotation *)annotation;
			MKMapView *mapView = routeView.mapView;
				
			routePath_ = CGPathCreateMutable();
			
            MKCoordinateRegion region = mapView.region;
            CGRect frame = mapView.frame;
            double pixelHeight = CGRectGetMaxY(frame) - CGRectGetMinY(frame);
            double degreesHeight = region.span.longitudeDelta;
            double totalPixels = 0.0f;
            
            if (degreesHeight > 0) {
                
                totalPixels = (pixelHeight / degreesHeight) * (360.0f);
                
            }
            
            double zoomLevelPixels = 256.0f;
            NSInteger zoomLevel = 0;
            
            while (totalPixels > zoomLevelPixels) {
                
                zoomLevel ++;
                zoomLevelPixels = zoomLevelPixels * 2.0f;
                
            }
            
            NSInteger minLevelAllowed = (17 - zoomLevel) / 5;
            minLevelAllowed = (minLevelAllowed > 0) ? minLevelAllowed : 0;
            
			BOOL firstPoint = YES;
            GoogleMapsPolyline *polyline = routeAnnotation.polyline;
			NSArray *routePoints = polyline.polylineArray;
            NSArray *levels = polyline.levelsArray;
			
            NSUInteger routePointsCount = [routePoints count];
            NSUInteger levelsCount = [levels count];
            
            if ((routePointsCount == levelsCount) && (routePoints > 0)) {
                
                NSValue *coordinateInstance;
                NSNumber *levelInstance;
                NSObject *object;
                CLLocationCoordinate2D coordinate;
                CGPoint point;
                
                for (NSUInteger i = 0; i < routePointsCount; i++) {
                    
                    object = [levels objectAtIndex:i];
                    
                    if ([object isKindOfClass:[NSNumber class]]) {
                        
                        levelInstance = (NSNumber *)object;
                        
                        if ([levelInstance unsignedIntegerValue] >= minLevelAllowed) {
                            
                            object = [routePoints objectAtIndex:i];
                            
                            if ([object isKindOfClass:[NSValue class]]) {
                                
                                coordinateInstance = (NSValue *)object;
                                
                                [coordinateInstance getValue:&coordinate];
                                
                                point = [mapView convertCoordinate:coordinate toPointToView:self];
                                
                                if (firstPoint) {
                                    
                                    CGPathMoveToPoint(routePath_, nil, point.x, point.y);
                                    firstPoint = NO;
                                    
                                } else {
                                    
                                    CGPathAddLineToPoint(routePath_, nil, point.x, point.y);
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
		}
		
	}
	
	[self setNeedsDisplay];
	
}

#pragma mark -
#pragma mark MKRouteAnnotationDelegate methods

/**
 * Notifies the delegate that the route points has changed. Updates the route path
 *
 * @param aRouteAnnotation The route annotatin triggering the event
 */
- (void)routeAnnotationPointsChanged:(MKRouteAnnotation *)aRouteAnnotation {

	[self updateRoutePath];
	
}

@end


#pragma mark -

@implementation MKRouteAnnotationView

#pragma mark -
#pragma mark Properties

@synthesize mapView = mapView_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {

	mapView_ = nil;
	
	[drawingView_ release];
	drawingView_ = nil;
	
	CGPathRelease(routePath_);
	routePath_ = NULL;

	[super dealloc];
	
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Default initialization. Initializes an MKRoteAnnotationView instance in a zero frame
 *
 * @return The initialized MKRouteAnnotationView instance
 */
- (id)init {

	return [self initWithFrame:CGRectZero];
	
}

/**
 * Initializes an MKRouteAnnotationView instance
 *
 * @param frame The view initial frame
 * @return The initialized MKRouteAnnotationView instance
 */
- (id)initWithFrame:(CGRect)frame {
	
    if ((self = [super initWithFrame:frame])) {
		
		drawingView_ = [[MKRouteAnnotationDrawingView alloc] init];
		drawingView_.backgroundColor = [UIColor clearColor];
		drawingView_.clipsToBounds = NO;
		
		self.clipsToBounds = NO;
		self.frame = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
		self.backgroundColor = [UIColor clearColor];
		
		[self addSubview:drawingView_];

    }
	
    return self;
	
}

/**
 * Initializes and returns a new annotation view
 *
 * @param annotation The annotation object to associate with the new view
 * @param reuseIdentifier If you plan to reuse the annotation view for similar types of annotations, pass a string to identify it
 * @return The iniitialized annotation view
 */
- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {

	if ((self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier])) {
		
		[drawingView_ updateRoutePath];
		
	}
	
	return self;
	
}

#pragma mark -
#pragma mark Updating route

/*
 * Updates the route representation
 */
- (void)updateRoute {

	CGPoint origin = CGPointMake(0, 0);
	origin = [mapView_ convertPoint:origin toView:self];
	
	drawingView_.frame = CGRectMake(origin.x, origin.y, mapView_.frame.size.width, mapView_.frame.size.height);
	[drawingView_ updateRoutePath];
	
	[self setNeedsDisplay];
	
}

#pragma mark -
#pragma mark Properties selectors

/**
 * Sets the annotation associated. Checks annotation type to set delegate (if necessary)
 *
 * @param anAnnotation The new annotation to set
 */
- (void)setAnnotation:(id<MKAnnotation>)anAnnotation {

	id<MKAnnotation> oldAnnotation = self.annotation;
	
	if ([(NSObject *)oldAnnotation isKindOfClass:[MKRouteAnnotation class]]) {
	
		MKRouteAnnotation *routeAnnotation = (MKRouteAnnotation *)oldAnnotation;
		routeAnnotation.delegate = nil;
		
	}
	
	[super setAnnotation:anAnnotation];
	
	if ([(NSObject *)anAnnotation isKindOfClass:[MKRouteAnnotation class]]) {
	
		MKRouteAnnotation *routeAnnotation = (MKRouteAnnotation *)anAnnotation;
		routeAnnotation.delegate = drawingView_;
		
	}
	
}

/**
 * Returns a boolean value indicating whether the annotation view is able to display extra information in a callout bubble. In this case, it is always NO
 *
 * @return NO, as the route annotation view must never display a callout view
 */
- (BOOL)canShowCallout {

    return NO;
    
}

@end
