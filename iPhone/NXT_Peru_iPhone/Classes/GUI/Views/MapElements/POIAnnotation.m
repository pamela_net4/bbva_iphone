/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "POIAnnotation.h"
#import "ATMList.h"
#import "GeocodedAddressList.h"


#pragma mark -

@implementation POIAnnotation

#pragma mark -
#pragma mark Properties

@synthesize poi = poi_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [poi_ release];
    poi_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. It returns a nil POIAnnotation instance as every POIAnnotation must have and associated POI
 *
 * @return The nil POIAnnotation instance
 */
- (id)init {

    [self autorelease];
    
    return nil;
    
}

/*
 * Designated initializer. Initializes a POIAnnotation instances providing the associated POI
 */
- (id)initWithAssociatedPOI:(ATMData *)poi {
    
    if ((self = [super init])) {
        
        poi_ = [poi retain];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark BaseAnnotation selectors

/**
 * Returns the associated POI coordinate as the annotation coordinate
 *
 * @return The associated POI coordinate
 */
- (CLLocationCoordinate2D)coordinate {
    
    GeocodedAddressData *geocodedAddressData = poi_.geocodedAddress;
    GeoPositionData *geoPosition = geocodedAddressData.geoPosition;
    
    CLLocationCoordinate2D result;
    result.latitude = geoPosition.latitude;
    result.longitude = geoPosition.longitude;
    
    return result;
    
}

/**
 * Returns the associated POI name as the annotation title
 *
 * @return The associated POI name
 */
- (NSString *)title {
    
    GeocodedAddressData *geocodedAddressData = poi_.geocodedAddress;
    AddressData *addressData = geocodedAddressData.address;
    
    return addressData.addressFirstLine;
    
}

@end
