/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "RouteOriginAnnotationView.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"


#pragma mark -

@implementation RouteOriginAnnotationView

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [annotationImageView_ release];
    annotationImageView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes a RouteOriginAnnotationView instance providing it with an image
 *
 * @param annotation The annotation object to associate with the new view
 * @param reuseIdentifier If you plan to reuse the annotation view for similar types of annotations, pass a string to identify it
 * @return The initialized RouteOriginAnnotationView instance
 */
- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
        
        CGRect frame = CGRectMake(0.0f, 0.0f, 29.0f, 29.0f);
        self.frame = frame;
        
        UIImageView *imageView = [[[UIImageView alloc] initWithImage:[[ImagesCache getInstance] imageNamed:ROUTE_ORIGIN_ANNOTATION_VIEW_IMAGE_FILE_NAME]] autorelease];
        
        annotationImageView_ = [[UIImageView alloc] initWithFrame:frame];
        [imageView setCenter:[annotationImageView_ center]];
        [annotationImageView_ addSubview:imageView];
        
        [self addSubview:annotationImageView_];
        
    }
    
    return self;
    
}

@end
