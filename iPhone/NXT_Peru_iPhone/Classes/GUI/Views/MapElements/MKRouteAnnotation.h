/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseAnnotation.h"
#import <CoreLocation/CoreLocation.h>


//Forward declarations
@class MKRouteAnnotation;
@class GoogleMapsPolyline;


/**
 * Route annotation delegate protocol to receive notifications when route points change
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol MKRouteAnnotationDelegate

@required

/**
 * Notifies the delegate that the route points has changed
 *
 * @param aRouteAnnotation The route annotatin triggering the event
 */
- (void)routeAnnotationPointsChanged:(MKRouteAnnotation *)aRouteAnnotation;

@end


/**
 * Annotation to display a route overlay by means of an annotation. It is needed to display routes in iOS 3.x)
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MKRouteAnnotation : BaseAnnotation {

@private
	
	/**
	 * Google polyline to draw
	 */
	GoogleMapsPolyline *polyline_;
	
	/**
	 * Northern-most coordinate in the route
	 */
	CLLocationDegrees northernMostCoordinate_;
	
	/**
	 * Western-most coordinate in the route
	 */
	CLLocationDegrees westernMostCoordinate_;
	
	/**
	 * Southern-most coordinate in the route
	 */
	CLLocationDegrees southernMostCoordinate_;
	
	/**
	 * Eastern-most coordinate in the route
	 */
	CLLocationDegrees easternMostCoordinate_;
	
	/**
	 * Route annotation delegate
	 */
	id<MKRouteAnnotationDelegate> delegate_;
	
}

/**
 * Provide read-write access to the Google polyline to draw
 */
@property (nonatomic, readwrite, retain) GoogleMapsPolyline *polyline;

/**
 * Provides read-only access to the northern-most coordinate in the route
 */
@property (nonatomic, readonly) CLLocationDegrees northernMostCoordinate;

/**
 * Provides read-only access to the western-most coordinate in the route
 */
@property (nonatomic, readonly) CLLocationDegrees westernMostCoordinate;

/**
 * Provides read-only access to the southern-most coordinate in the route
 */
@property (nonatomic, readonly) CLLocationDegrees southernMostCoordinate;

/**
 * Provides read-only access to the eastern-most coordinate in the route
 */
@property (nonatomic, readonly) CLLocationDegrees easternMostCoordinate;

/**
 * Provides read-write access to the route annotation delegate
 */
@property (nonatomic, readwrite, assign) id<MKRouteAnnotationDelegate> delegate;

@end
