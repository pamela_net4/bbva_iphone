/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseAnnotation.h"


#pragma mark -

@implementation BaseAnnotation

#pragma mark -
#pragma mark Properties

@synthesize coordinate = coordinate_;
@synthesize title = title_;
@synthesize subtitle = subtitle_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [title_ release];
    title_ = nil;
    
    [subtitle_ release];
    subtitle_ = nil;
    
    [super dealloc];
    
}

@end
