/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ATMAnnotationView.h"

#import "ImagesCache.h"
#import "ImagesFileNames.h"


#pragma mark -

@implementation ATMAnnotationView

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [annotationImageView_ release];
    annotationImageView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes ATMAnnotationView instance providing it with an image
 *
 * @param annotation The annotation object to associate with the new view
 * @param reuseIdentifier If you plan to reuse the annotation view for similar types of annotations, pass a string to identify it
 * @return The initialized ATMAnnotationView instance
 */
- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
        
        CGRect frame = CGRectMake(0.0f, 0.0f, 26.0f, 32.0f);
        [self setFrame:frame];

        
        annotationImageView_ = [[UIImageView alloc] initWithFrame:frame];
        [self addSubview:annotationImageView_];
        
    }
    
    return self;
    
}

/**
 * The offset (in pixels) at which to display the view.
 */
- (CGPoint)centerOffset {
    
    return CGPointMake(0.0f, -16.0f);
    
}

#pragma mark -
#pragma mark ATMAnnotationView selectors

/*
 * Set current image with atm type.
 */
- (void)setImageWithType:(AgentStates)atmAgentType {
    
    switch (atmAgentType) {
            
        case atmType:
            
            [annotationImageView_ setImage:[[ImagesCache getInstance] imageNamed:MAP_TOOLTIP_BBVA_CONTINENTAL_IMAGE_FILE]];
            
            break;
            
        case atmBCType:
            
            [annotationImageView_ setImage:[[ImagesCache getInstance] imageNamed:MAP_TOOLTIP_BCP_IMAGE_FILE]];
            
            break;
            
        case atmInterbankType:
            
            [annotationImageView_ setImage:[[ImagesCache getInstance] imageNamed:MAP_TOOLTIP_INTERBANK_GLOBALNET_IMAGE_FILE]];
            
            break;
            
        case atmScotiabankType:
            
            [annotationImageView_ setImage:[[ImagesCache getInstance] imageNamed:MAP_TOOLTIP_SCOTIABANK_IMAGE_FILE]];
            
            break;
            
        case expressAgentType:
            
            [annotationImageView_ setImage:[[ImagesCache getInstance] imageNamed:MAP_TOOLTIP_EXPRESS_AGENT_IMAGE_FILE]];
            
            break;
            
        case expressAgentPlusType:
            
            [annotationImageView_ setImage:[[ImagesCache getInstance] imageNamed:MAP_TOOLTIP_EXPRESS_AGENT_PLUS_IMAGE_FILE]];
            
            break;
            
        case kasnetType:
            
            [annotationImageView_ setImage:[[ImagesCache getInstance] imageNamed:MAP_TOOLTIP_KASNET_IMAGE_FILE]];
            
            break;
            
        case multifacilType:
            
            [annotationImageView_ setImage:[[ImagesCache getInstance] imageNamed:MAP_TOOLTIP_MULTIFACIL_IMAGE_FILE]];
            
            break;
            
        default:
            
            break;

    }
    
}

@end
