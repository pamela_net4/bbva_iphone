/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StepAnnotationView.h"


#pragma mark -

@implementation StepAnnotationView

#pragma mark -
#pragma mark Properties

@dynamic stepAnnotationImage;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {

    [stepAnnotationImageView_ release];
    stepAnnotationImageView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes a StepAnnotationView instance creating the image view associated
 *
 * @param annotation The annotation object to associate with the new view
 * @param reuseIdentifier If you plan to reuse the annotation view for similar types of annotations, pass a string to identify it
 * @return The initialized StepAnnotationView instance
 */
- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    
    if ((self = [super initWithAnnotation:annotation
                          reuseIdentifier:reuseIdentifier])) {
        
        self.backgroundColor = [UIColor clearColor];
        stepAnnotationImageView_ = [[UIImageView alloc] init];
        [self addSubview:stepAnnotationImageView_];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the step annotation image
 *
 * @return The step annotation image
 */
- (UIImage *)stepAnnotationImage {
    
    return stepAnnotationImageView_.image;
}

/**
 * Sets the step annotation image and resizes the image view and the view to fit the image
 *
 * @param aValue The new step annotation image to set
 */
- (void)setStepAnnotationImage:(UIImage *)aValue {
    
    CGSize imageSize = aValue.size;
    CGFloat imageWidth = imageSize.width;
    CGFloat imageHeight = imageSize.height;
    
    stepAnnotationImageView_.image = aValue;
    stepAnnotationImageView_.frame = CGRectMake(0.0f, 0.0f, imageWidth, imageHeight);
    
    CGRect frame = self.frame;
    CGFloat width = CGRectGetMaxX(frame) - CGRectGetMinX(frame);
    CGFloat height = CGRectGetMaxY(frame) - CGRectGetMinY(frame);
    
    frame.origin.x = frame.origin.x + round((width - imageWidth) / 2.0f);
    frame.origin.y = frame.origin.y + round((height - imageHeight) / 2.0f);
    frame.size.width = imageWidth;
    frame.size.height = imageHeight;
    
    self.frame = frame;

}

@end
