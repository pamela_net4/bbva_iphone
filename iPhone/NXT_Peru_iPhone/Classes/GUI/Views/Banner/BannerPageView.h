//
//  BannerPageView.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BannerPageView : UIView{
    UIWebView *label_;
    UIButton *button_;
}

+ (BannerPageView *)bannerPageView ;

@property (retain, nonatomic) IBOutlet UIWebView *label;
@property (retain, nonatomic) IBOutlet UIButton *button;

@end
