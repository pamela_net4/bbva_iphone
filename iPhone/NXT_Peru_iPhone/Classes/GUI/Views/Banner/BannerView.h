//
//  BannerView.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 12/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CampaignList;

@protocol BannerDelegate
-(void) bannerSelected:(int32_t)position;
@end

@interface BannerView : UIView{
    UIScrollView *containerScrollView_;
    UILabel * label_;
    UIButton *closeButton_;
    UIButton *previousButton_;
    UIButton *nextButton_;
    UIView *backgroundView_;
    UIView *separatorView_;
    BOOL enabledScroll;
    NSTimer *autoTimer_;
    
    NSMutableArray * contentViews_;
    id<BannerDelegate> delegate_;
}

/**
 * Provides readwrite access to the containerScrollView
 */
@property (retain, nonatomic) IBOutlet UIScrollView *containerScrollView;

/**
 * Provides readwrite access to the button close
 */
@property (retain, nonatomic) IBOutlet UIButton *closeButton;

/**
 * Provides readwrite access to the button previous
 */
@property (retain, nonatomic) IBOutlet UIButton *previousButton;

/**
 * Provides readwrite access to the button next
 */
@property (retain, nonatomic) IBOutlet UIButton *nextButton;

/**
 * Provides readwrite access
 */
@property (retain, nonatomic) IBOutlet UIView *backgroundView;

/**
 * Provides readwrite access
 */
@property (retain, nonatomic) IBOutlet UIView *separatorView;


@property (nonatomic, readwrite, retain) NSMutableArray * contentViews;

@property (nonatomic, readwrite, assign) id<BannerDelegate> delegate;

+ (BannerView *)bannerView;
-(void)setContent:(NSArray*)campaignList;

+(CGFloat)heightBanner;


@end
