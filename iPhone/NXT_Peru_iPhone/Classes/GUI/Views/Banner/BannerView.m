//
//  BannerView.m
//  NXT_Peru_iPhone
//
//  Created by Estefany on 12/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "BannerView.h"
#import "NibLoader.h"
#import "BannerPageView.h"
#import "CampaignList.h"
#import "Campaign.h"
#import "Constants.h"
#import "UIColor+BBVA_Colors.h"

#define BANNER_HEIGHT                                               54.0f
#define BANNER_PAGE_HEIGHT                                          50.0f

#define BannerViewNib  @"BannerView"


@implementation BannerView

@synthesize containerScrollView = containerScrollView_;
@synthesize closeButton = closeButton_;
@synthesize previousButton = previousButton_;
@synthesize nextButton = nextButton_;
@synthesize delegate = delegate_;
@synthesize contentViews = contentViews_;
@synthesize backgroundView = backgroundView_;
@synthesize separatorView = separatorView_;

/*
 * Creates and returns an autoreleased BannerView contructed from NIB file.
 */
+ (BannerView *)bannerView {
    
    BannerView *result = (BannerView *)[NibLoader loadObjectFromNIBFile:BannerViewNib];
    [result awakeFromNib];
    return result;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];

    enabledScroll = YES;
    
    self.backgroundColor = [UIColor whiteColor];
    /*self.containerScrollView.layer.borderWidth = 1;
    self.containerScrollView.layer.borderColor = [[UIColor BannerBorderColor] CGColor];
    self.containerScrollView.layer.cornerRadius = 5;
    self.containerScrollView.layer.masksToBounds = YES;
    
    self.backgroundView.backgroundColor = [UIColor BannerBackgroundColor];
    self.backgroundView.layer.cornerRadius = 5;
    self.backgroundView.layer.masksToBounds = YES;
    
    self.separatorView.backgroundColor = [UIColor BannerBorderColor] ;*/

}

-(void)setContent:(NSArray*)campaignList{
    
     for(UIView * subview in containerScrollView_.subviews){
         [subview removeFromSuperview];
     }
    
    int widthCampaign = 0;
    CGFloat widthPage = CGRectGetWidth(self.frame) -
                        (CGRectGetWidth(closeButton_.frame) + CGRectGetWidth(previousButton_.frame) + CGRectGetWidth(nextButton_.frame));
    contentViews_ = [[NSMutableArray alloc] init];
    
    if(campaignList && [campaignList count]==1){
        previousButton_.hidden = YES;
        nextButton_.hidden = YES;
    }
    else{
        previousButton_.hidden = NO;
        nextButton_.hidden = NO;
    }
    
    for (int i=0; i<[campaignList count]; i++) {
        
        Campaign * campaign = [campaignList objectAtIndex:i];
        
        BannerPageView * bannerViewPage = [[BannerPageView bannerPageView] retain];
        bannerViewPage.frame = CGRectMake(
                                          widthPage * i,
                                          0,
                                          widthPage,
                                          BANNER_PAGE_HEIGHT);
        
        
        UIColor * strokeColor = [UIColor BannerBorderColor];
        
        if ([[campaign type] isEqualToString:@"IL"]) {
            
            strokeColor = [UIColor BannerILColor];
        }
        else if([[campaign type] isEqualToString:@"PI"]){
            strokeColor = [UIColor BannerPATColor];
        }
        else if([[campaign type] isEqualToString:@"AS"]){
            strokeColor = [UIColor BannerASColor];
        }
        
        bannerViewPage.layer.borderWidth = 2;
        bannerViewPage.layer.borderColor = [strokeColor CGColor];
        bannerViewPage.layer.cornerRadius = 10;
        bannerViewPage.layer.masksToBounds = YES;
        
        NSString * strokeColorHex = [UIColor hexStringFromColor:strokeColor];
        
        NSString *myHTMLText = [NSString stringWithFormat:@"<html>"
                                "<head><style type='text/css'>"
                                ".main_text {"
                                "   display: block;"
                                "   font-family:[fontName];"
                                "   text-decoration:none;"
                                "   font-size:[fontSize]px;"
                                "   color:[fontColor];"
                                "   line-height: [fontSize]px;"
                                "   font-weight:normal;"
                                "   text-align:[textAlign];"
                                "   height: 100%%;"
                                "   margin-top: 14px;"
                                "}"
                                "</style></head>"
                                "<body><SPAN class='main_text'>[text]</SPAN></body></html>"];
        
        myHTMLText = [myHTMLText stringByReplacingOccurrencesOfString: @"[text]" withString: campaign.text];
        myHTMLText = [myHTMLText stringByReplacingOccurrencesOfString: @"[fontName]" withString: @"Helvetica"];
        myHTMLText = [myHTMLText stringByReplacingOccurrencesOfString: @"[fontSize]" withString: @"14"];
        myHTMLText = [myHTMLText stringByReplacingOccurrencesOfString: @"[fontColor]" withString: strokeColorHex];
        myHTMLText = [myHTMLText stringByReplacingOccurrencesOfString: @"[textAlign]" withString: @"center"];
        
        [bannerViewPage.label loadHTMLString:myHTMLText baseURL:nil];
        
        [bannerViewPage.button setTitle:campaign.buttonName forState:UIControlStateNormal];
       
        bannerViewPage.tag = i;
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(bannerSelected:)];
        [bannerViewPage addGestureRecognizer:singleFingerTap];
        
        [self.containerScrollView addSubview:bannerViewPage];
        
        widthCampaign += widthPage;
        
        [contentViews_ addObject:bannerViewPage];
        
    }
    
    //NSLog(@"my width %f", widthPage);
    
    self.containerScrollView.pagingEnabled = YES;
    self.containerScrollView.contentSize = CGSizeMake(widthCampaign, BANNER_PAGE_HEIGHT);
    self.containerScrollView.frame = CGRectMake(CGRectGetWidth(previousButton_.frame), 2,
                                                widthPage, BANNER_PAGE_HEIGHT);
    
    [self.containerScrollView setShowsHorizontalScrollIndicator:NO];
    
    if(autoTimer_!=nil){
        [autoTimer_ invalidate];
        autoTimer_ = nil;
    }
    
    autoTimer_ = [NSTimer scheduledTimerWithTimeInterval:5.0
                                     target:self
                                   selector:@selector(nextBannerAutomatic)
                                   userInfo:nil
                                    repeats:YES];
    
}

-(void)nextBannerAutomatic{
    [self nextBanner:nil];
}

-(void)bannerSelected:(UITapGestureRecognizer *)recognizer{
    UIView * view = recognizer.view;
    int position = view.tag;
    
    [delegate_ bannerSelected:position];
}


-(IBAction)nextBanner:(id)sender{
    int page = containerScrollView_.contentOffset.x / containerScrollView_.frame.size.width;
    int numPages = containerScrollView_.contentSize.width / containerScrollView_.frame.size.width;
    if(page < (numPages-1)){
        page++;
        CGPoint offset = CGPointMake(page * containerScrollView_.frame.size.width, 0);
        [containerScrollView_ setContentOffset:offset animated:YES];
    }
}

-(IBAction)previousBanner:(id)sender{
    int page = containerScrollView_.contentOffset.x / containerScrollView_.frame.size.width;
    if(page > 0){
        page--;
        CGPoint offset = CGPointMake(page * containerScrollView_.frame.size.width, 0);
        [containerScrollView_ setContentOffset:offset animated:YES];
    }
}

+(CGFloat)heightBanner{
    return BANNER_HEIGHT;
}

#pragma UIScrollViewDelegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if(autoTimer_!=nil){
        [autoTimer_ invalidate];
        autoTimer_ = nil;
    }
    
    if(enabledScroll){
    
        int numPages = containerScrollView_.contentSize.width / containerScrollView_.frame.size.width;
    
        if(scrollView.contentOffset.x == 0) {
            enabledScroll = NO;
            CGPoint newOffset = CGPointMake(scrollView.bounds.size.width + scrollView.contentOffset.x,
                                        scrollView.contentOffset.y);
            [scrollView setContentOffset:newOffset];
            [self rotateViewsLeft];
        }
        else if(scrollView.contentOffset.x == scrollView.bounds.size.width*(numPages-1)) {
            enabledScroll = NO;
            CGPoint newOffset = CGPointMake(scrollView.contentOffset.x - scrollView.bounds.size.width,
                                        scrollView.contentOffset.y);
            [scrollView setContentOffset:newOffset];
            [self rotateViewsRight];
        }
        
        enabledScroll = YES;
    }
    
    if(autoTimer_!=nil){
        [autoTimer_ invalidate];
        
    }
    
    autoTimer_ = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                  target:self
                                                selector:@selector(nextBannerAutomatic)
                                                userInfo:nil
                                                 repeats:YES];
}

-(void)rotateViewsLeft {
    UIView *endView = [contentViews_ lastObject];
    [contentViews_ removeLastObject];
    [contentViews_ insertObject:endView atIndex:0];
    [self setContentViewFrames];
    
}

-(void)rotateViewsRight {
    UIView *endView = contentViews_[0];
    [contentViews_ removeObjectAtIndex:0];
    [contentViews_ addObject:endView];
    [self setContentViewFrames];
    
}

-(void) setContentViewFrames {
    for(int i = 0; i < 3 && i<contentViews_.count; i++) {
        UIView * view = contentViews_[i];
        [view setFrame:CGRectMake(self.containerScrollView.bounds.size.width*i,
                                  0,
                                  self.containerScrollView.bounds.size.width,
                                  self.containerScrollView.bounds.size.height)];
    }
}

-(void)dealloc{
    
    if(autoTimer_!=nil){
        [autoTimer_ invalidate];
        [autoTimer_ release];
        autoTimer_ = nil;
    }
    
    [containerScrollView_ release];
    containerScrollView_ = nil;
    
    [closeButton_ release];
    closeButton_ = nil;
    
    [previousButton_ release];
    previousButton_ = nil;
    
    [nextButton_ release];
    nextButton_ = nil;
    
    [super dealloc];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
