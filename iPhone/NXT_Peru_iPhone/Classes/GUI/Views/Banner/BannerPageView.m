//
//  BannerPageView.m
//  NXT_Peru_iPhone
//
//  Created by Estefany on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "BannerPageView.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"

#define BannerPageViewNib  @"BannerPageView"

@implementation BannerPageView

@synthesize button = button_;
@synthesize label = label_;

+ (BannerPageView *)bannerPageView {
    
    BannerPageView *result = (BannerPageView *)[NibLoader loadObjectFromNIBFile:BannerPageViewNib];
    [result awakeFromNib];
    return result;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [NXT_Peru_iPhoneStyler styleBlueButton:button_];
    
    self.label.opaque = YES;
    self.label.scrollView.scrollEnabled = NO;
    
    [self.button.titleLabel setFont:[NXT_Peru_iPhoneStyler normalFontWithSize:12]];
    self.button.layer.cornerRadius = 5;
    self.button.layer.masksToBounds = YES;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
