/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * Defines the window tapped notification string
 */
extern NSString * const kMOKUITouchNotifyingWindowTappedNotification;


/**
 * NXT window to capture all touches and reset the application logout timeout
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKUITouchNotifyingWindow : UIWindow {

}

@end
