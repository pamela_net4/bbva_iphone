//
//  OtherBankExtraView.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 10/5/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NXTComboButton.h"

@class NXTTextField;
@class PopButtonsView;

@protocol OtherBankExtraViewDelegate
-(void) docTypeComboPressed:(NXTComboButton*) comboButton;
@end

@interface OtherBankExtraView : UIView<UITextFieldDelegate,NXTComboButtonDelegate>
{
    /*
     * beneficiary name label
     */
    UILabel *beneficiaryNameLabel_;
    
    /*
     * beneficiary text field
     */
    NXTTextField *beneficiaryTextField_;
    
    /*
     * doc type label
     */
    UILabel *docTypeLabel_;
    
    /*
     * doc type combo
     */
    NXTComboButton *docTypeCombo_;
    
    /*
     * doc type text field
     */
    NXTTextField *docTypeTextField_;
    
    /**
     * Pop buttons view to complement the keyboard
     */
    PopButtonsView *popButtonsView_;
    
    id<OtherBankExtraViewDelegate> delegate_;


}
/**
 * Provides read-only access to the pop buttons view to complement the keyboard
 */
@property (nonatomic, readonly, retain) PopButtonsView *popButtonsView;

/**
 * Provides readwrite access to the beneficiary name label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *beneficiaryNameLabel;

/**
 * Provides readwrite access to the beneficiary name text field. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTTextField *beneficiaryNameTextField;

/**
 * Provides readwrite access to the doc type label. Exported to IB
 */
@property (retain, nonatomic) IBOutlet UILabel *docTypeLabel;

/**
 * Provides readwrite access to the doc type combo. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTComboButton *docTypeCombo;

/**
 * Provides readwrite access to the doc type text field. Exported to IB
 */
@property (retain, nonatomic) IBOutlet NXTTextField *docTypeTextField;


/**
 * Provides read-write access to the pop buttons view delegate
 */
@property (nonatomic, readwrite, assign) id<OtherBankExtraViewDelegate> delegate;

+ (OtherBankExtraView *)otherBankExtraView :(PopButtonsView*)mPopButtonView viewController:(id)viewController ;
@end
