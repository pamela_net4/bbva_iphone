/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * Specific NXT text field to return the correct text bounds
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTTextField : UITextField {
@private
    /**
     * The amount of padding to apply to the text bounds
     */
    CGFloat textPadding_;

    /**
     * Custom input view that shows when the button becomes first responder
     */
    UIView *inputView_;
    
    /**
     * Custom input accessory view that shows when the button becomes first responder
     */
    UIView *inputAccessoryView_;
    
    /**
     * Message to show when this control is empty
     */
    NSString *fillFormMessage_;
}

/**
 * Provides readwrite access to the title
 */
@property (readwrite, copy) NSString *fillFormMessage;

/**
 * Provides readwrite access to the inputView
 */
@property (readwrite, retain) UIView *inputView;

/**
 * Provides readwrite access to the inputAccesoryView
 */
@property (readwrite, retain) UIView *inputAccessoryView;

/**
 * Provides read-write access to the text padding
 */
@property (nonatomic, readwrite) CGFloat textPadding;

/**
 * Provides readwrite access to the valid boolean
 */
@property (readonly, assign) BOOL valid;

@end
