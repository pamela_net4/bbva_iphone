/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

@class NXTComboButton;

@protocol NXTComboButtonDelegate
@optional

/**
 * Informs the delegate that a value has been selected
 *
 * @param comboButton that informs
 */
- (void)NXTComboButtonValueSelected:(NXTComboButton *)comboButton;

@end

/**
 * Custom button with a combo like style that responds to UIResponder events and methods.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTComboButton : UIButton <UIPickerViewDelegate, UIPickerViewDataSource> {
    
@private
    /**
     * Custom input view that shows when the button becomes first responder
     */
    UIView *inputView_;
    
    /**
     * Custom input accessory view that shows when the button becomes first responder
     */
    UIView *inputAccessoryView_;
    
    /**
     * Picker of the combo
     */
    UIPickerView *picker_;
    
    /**
     * List with the strings we use to populate the picker
     */
    NSArray *stringsList_;
    
    /**
     * Selected index
     */
    NSInteger selectedIndex_;
    
    /**
     * Title of the combo
     */
    NSString *title_;
    
    /**
     * Message to show when this control is empty
     */
    NSString *fillFormMessage_;
    
    /**
     * Delegate
     */
    id<NXTComboButtonDelegate> delegate_;
    
    /**
     * Flag that indicate if the combo must informs the delegate for the first selection
     */
    BOOL informFirstSelection_;
}

/**
 * Provides readwrite access to the informFirstSelection flag
 */
@property (nonatomic, readwrite, assign) BOOL informFirstSelection;

/**
 * Provides readwrite access to the delegate
 */
@property (nonatomic, readwrite, assign) id<NXTComboButtonDelegate> delegate;

/**
 * Provides readwrite access to the fill form message
 */
@property (nonatomic, readwrite, retain) NSString *fillFormMessage;

/**
 * Provides readwrite access to the inputView
 */
@property (nonatomic, readwrite, retain) UIView *inputView;

/**
 * Provides readwrite access to the inputAccesoryView
 */
@property (nonatomic, readwrite, retain) UIView *inputAccessoryView;

/**
 * Provides readwrite access to the title
 */
@property (nonatomic, readwrite, copy) NSString *title;

/**
 * Provides readwrite access to the stringsList
 */
@property (nonatomic, readwrite, retain) NSArray *stringsList;

/**
 * Provides readwrite access to the picker
 */
@property (nonatomic, readwrite, retain) UIPickerView *picker;

/**
 * Provides readwrite access to the picker selected index
 */
@property (nonatomic, readwrite, assign) NSInteger selectedIndex;

/**
 * Provides readwrite access to the valid boolean
 */
@property (nonatomic, readonly, assign) BOOL valid;

-(void)setIsNewDesign:(BOOL *)isNewDesign;

@end
