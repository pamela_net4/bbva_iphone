/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTTextField.h"


#pragma mark -

/**
 * NXTTextField private category
 */
@interface NXTTextField(private)

/**
 * Returns a rectangle once the padding is applied to it
 *
 * @param aRect The original rectable
 * @return The rectangle once the padding is applied
 */
- (CGRect)applyPaddingToRect:(CGRect)aRect;

@end


#pragma mark -

@implementation NXTTextField

#pragma mark -
#pragma mark Properties

@synthesize textPadding = textPadding_;
@dynamic inputView;
@dynamic inputAccessoryView;
@synthesize fillFormMessage = fillFormMessage_;
@dynamic valid;

#pragma mark -
#pragma mark Memory management

/**
 * Releases allocated memory
 */
- (void)dealloc {
    [inputView_ release];
    inputView_ = nil;    
    
    [inputAccessoryView_ release];
    inputAccessoryView_ = nil;    
    
    [fillFormMessage_ release];
    fillFormMessage_ = nil;    
    
    [super dealloc];
}

#pragma mark -
#pragma mark Getters and setters overriden from UIResponder

/**
 * Returns the custom input view to display when the object becomes the first responder
 */
- (UIView *)inputView {
    return inputView_;
}

/**
 * Sets the custom input view to display when the object becomes the first responder
 *
 * @param anInputView to show when control becomes first responder
 */
- (void)setInputView:(UIView *)anInputView {
    if (inputView_ != anInputView) {
        [inputView_ release];
        inputView_ = [anInputView retain];
    }
}

/**
 * Returns the custom accessory view to display when the object becomes the first responder
 */
- (UIView *)inputAccessoryView {
    return inputAccessoryView_;
}

/**
 * Sets the custom accessory view to display when the object becomes the first responder
 *
 * @param anInputAccessoryView to show when control becomes first responder
 */
- (void)setInputAccessoryView:(UIView *)anInputAccessoryView {
    if (inputAccessoryView_ != anInputAccessoryView) {
        [inputAccessoryView_ release];
        inputAccessoryView_ = [anInputAccessoryView retain];
    }
}

#pragma mark -
#pragma mark Getters and setters

- (BOOL)valid {
    return ([[self.text stringByReplacingOccurrencesOfString:@" " withString:@""] length] > 0);
}

#pragma mark -
#pragma mark UITextField methods

/**
 * Returns the drawing rectangle for the text field’s text. It adds a padding to the returned text field
 *
 * @param bounds The bounding rectangle of the receiver
 * @return The computed drawing rectangle for the label’s text
 */
- (CGRect)textRectForBounds:(CGRect)bounds {
    
    CGRect result = [super textRectForBounds:bounds];
    result = [self applyPaddingToRect:result];
    
    return result;
    
}

/**
 * Returns the rectangle in which editable text can be displayed. It adds a padding to the returned text field
 *
 * @param bounds The bounding rectangle of the receiver
 * @return The computed editing rectangle for the text
 */
- (CGRect)editingRectForBounds:(CGRect)bounds {
    
    CGRect result = [super textRectForBounds:bounds];
    result = [self applyPaddingToRect:result];
    
    return result;
    
}

#pragma mark -
#pragma mark Padding management

/*
 * Returns a rectangle once the padding is applied to it
 */
- (CGRect)applyPaddingToRect:(CGRect)aRect {
    
    CGRect result = aRect;
    
    result.origin.x = CGRectGetMinX(aRect) + textPadding_;
    result.size.width = CGRectGetWidth(aRect) - (2.0f * textPadding_);
    
    return result;
    
}


@end
