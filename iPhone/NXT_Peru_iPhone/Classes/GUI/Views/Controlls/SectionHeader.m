/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "SectionHeader.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "NibLoader.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Defines the header text size
 */
#define HEADER_TEXT_FONT_SIZE                           17.0f


#pragma mark -

@implementation SectionHeader

#pragma mark -
#pragma mark Properties

@synthesize leftTextLabel = leftTextLabel_;
@synthesize rightTextLabel = rightTextLabel_;
@synthesize bottomSeparatorImageView = bottomSeparatorImageView_;
@synthesize topSeparatorImageView = topSeparatorImageView_;
@dynamic leftText;
@dynamic rightText;
@synthesize grayCenteredText = grayCenteredText_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [leftTextLabel_ release];
    leftTextLabel_ = nil;
    
    [rightTextLabel_ release];
    rightTextLabel_ = nil;
    
    [bottomSeparatorImageView_ release];
    bottomSeparatorImageView_ = nil;
    
    [topSeparatorImageView_ release];
    topSeparatorImageView_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Styles the header label
 * and loads the map icon image
 */
- (void)awakeFromNib {
    
    self.backgroundColor = [UIColor whiteColor];
    
    bottomSeparatorImageView_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    topSeparatorImageView_.image = [[ImagesCache getInstance] imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    
    topSeparatorImageView_.hidden = YES;
    
    self.grayCenteredText = NO;
    
}

/*
 * Creates and returns aa autoreleased SectionHeader constructed from a NIB file
 */
+ (SectionHeader *)sectionHeader {
    
    return (SectionHeader *)[NibLoader loadObjectFromNIBFile:@"SectionHeader"];
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Returns the height of the section header
 */
+ (CGFloat)height {
    
    return 49.0f;
    
}

/*
 * Styles the right text with the correct color for the given amount
 */
- (void)styleRightColorForAmount:(NSDecimalNumber *)amount {
    
    [NXT_Peru_iPhoneStyler styleLabelColor:rightTextLabel_
                                 forAmount:amount];
    
}

#pragma mark -
#pragma mark Getters and setters

- (NSString *)leftText {
    
    return leftTextLabel_.text;
    
}

- (NSString *)rightText {
    
    return rightTextLabel_.text;
    
}

- (void)setLeftText:(NSString *)value {
    
    leftTextLabel_.text = value;
    
}

- (void)setRightText:(NSString *)value {
    
    rightTextLabel_.text = value;
    
    if (value.length == 0) {
        
        CGRect frame = leftTextLabel_.frame;
        frame.size.width = self.frame.size.width - 10.0f;
        leftTextLabel_.frame = frame;
    
    } else {
        
        CGRect frame = leftTextLabel_.frame;
        frame.size.width = 240.0f;
        leftTextLabel_.frame = frame;
        
    }
    
}

- (void)setGrayCenteredText:(BOOL)grayCenteredText {
    
    grayCenteredText_ = grayCenteredText;
    
    if (grayCenteredText) {
        
        [NXT_Peru_iPhoneStyler styleLabel:leftTextLabel_
                             withFontSize:12.0f
                                    color:[UIColor grayColor]];
        
        leftTextLabel_.textAlignment = UITextAlignmentCenter;
    
    } else {
        
        [NXT_Peru_iPhoneStyler styleLabel:leftTextLabel_
                         withBoldFontSize:HEADER_TEXT_FONT_SIZE
                                    color:[UIColor BBVABlueSpectrumToneTwoColor]];
        
        leftTextLabel_.textAlignment = UITextAlignmentLeft;
        
    }
    
}

@end
