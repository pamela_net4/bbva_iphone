/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * Enumerates the image and title available alignment
 */
typedef enum {
    abca_ImageLeft = 0, //!<Image is located on the left side (UIButton default behaviour)
    abca_ImageRight, //!<Image is located on the right side
    abca_ImageNone //There is no image and the text is centered
} AlignableButtonContentAlignment;


/**
 * Enumerates the button alignment when shrinking or stretching to accomodate its content.
 * This information is obtained from normal state title and image
 */
typedef  enum {
    absa_AlignNone = 0, //!<No new alignment is performed and no shrinking or expansion will take place
    absa_AlignRight, //!<The button will shrink or expand but will maintain its right position
    absa_AlignCenter, //!<The button will shrink or expand but will maintain its center position
    absa_AlignLeft //!<The button will shrink or expand but will maintain its left position
} AlignableButtonShrinkAlignment;


/**
 * Alignable button allows the user define the button image and title alignment (image left and title right or vice versa),
 * and the button capacity to shrink horizontally to accomodate its content. When shrinking, button can align left, center,
 * or right
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AlignableButton : UIButton {
    
@private
    
    /**
     * Image and text relative alignment
     */
    AlignableButtonContentAlignment contentAlignment_;
    
    /**
     * Button alignment to accomodate its content
     */
    AlignableButtonShrinkAlignment buttonAlignment_;
    
    /**
     * Left border margin (neither image nor text can be left to this position, measured from the left border)
     */
    CGFloat leftMargin_;
    
    /**
     * Right border margin (neither image nor text can be right to position, measured from the right border)
     */
    CGFloat rightMargin_;
    
}

/**
 * Provides read-write access to the content alignment
 */
@property (nonatomic, readwrite) AlignableButtonContentAlignment contentAlignment;

/**
 * Provides read-write access to the button alignment
 */
@property (nonatomic, readwrite) AlignableButtonShrinkAlignment buttonAlignment;

/**
 * Provides read-write access to the left border margin
 */
@property (nonatomic, readwrite) CGFloat leftMargin;

/**
 * Provides read-write access to the right border margin
 */
@property (nonatomic, readwrite) CGFloat rightMargin;

@end
