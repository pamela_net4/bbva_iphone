/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "MOKDateComboButton.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"

@interface MOKDateComboButton()

/**
 * Initiliazes the control
 *
 * @private
 */
- (void)initialize;

/**
 * The value of the picker changed
 *
 * @private
 */
- (void)datePickerValueChanged;

@end

@implementation MOKDateComboButton

@dynamic inputView;
@dynamic inputAccessoryView;
@synthesize picker = picker_;
@synthesize fillFormMessage = fillFormMessage_;
@dynamic title;
@dynamic valid;
@dynamic selectedDate;
@dynamic maximumDate;
@dynamic minimumDate;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    [inputView_ release];
    inputView_ = nil;
    
    [inputAccessoryView_ release];
    inputAccessoryView_ = nil;
    
    [picker_ release];
    picker_ = nil;
    
    [title_ release];
    title_ = nil;
    
    [fillFormMessage_ release];
    fillFormMessage_ = nil;
    
    [super dealloc];
}

#pragma mark -
#pragma mark Initilization

/**
 * View is loaded form a NIB file
 */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self initialize];    
}

/**
 * Designated initializer
 */
- (id)init {
    
    if (self = [super init]) {
        
        [self initialize];
    }
    
    return self;
}

/**
 * Designated initializer. The view labels are created
 *
 * @param frame The view original frame
 * @return The initialized NXTNaviationItemTitleView instance
 */
- (id)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
    
        [self initialize];
    }
    
    return self;
}

/*
 * Initiliazes the control
 */
- (void)initialize {
    
    [NXT_Peru_iPhoneStyler styleComboButton:self];
    
    [picker_ release];
    picker_ = [[UIDatePicker alloc] init];
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        picker_.backgroundColor = [UIColor BBVAGreyToneFourColor];
    }
    [picker_ setDatePickerMode:UIDatePickerModeDate];
    [picker_ addTarget:self action:@selector(datePickerValueChanged) forControlEvents:UIControlEventValueChanged];
    
    [title_ release];
    title_ = nil;
    
    self.inputView = picker_;
}

#pragma mark -
#pragma mark UIResponder overriden methods

/**
 * Returns the custom input view to display when the object becomes the first responder
 */
- (UIView *)inputView {
    return inputView_;
}

/**
 * Sets the custom input view to display when the object becomes the first responder
 *
 * @param anInputView to show when control becomes first responder
 */
- (void)setInputView:(UIView *)anInputView {
    
    if (inputView_ != anInputView) {
        
        [inputView_ release];
        inputView_ = [anInputView retain];
    }
}

/**
 * Returns the custom accessory view to display when the object becomes the first responder
 */
- (UIView *)inputAccessoryView {
    return inputAccessoryView_;
}

/**
 * Sets the custom accessory view to display when the object becomes the first responder
 *
 * @param anInputAccessoryView to show when control becomes first responder
 */
- (void)setInputAccessoryView:(UIView *)anInputAccessoryView {
    if (inputAccessoryView_ != anInputAccessoryView) {
        [inputAccessoryView_ release];
        inputAccessoryView_ = [anInputAccessoryView retain];
    }
}

/**
 * Returns a Boolean value indicating whether the receiver can become first responder.
 */
- (BOOL)canBecomeFirstResponder {
    return YES;
}

/**
 * Notifies the receiver that it is about to become first responder in its window.
 */
- (BOOL)becomeFirstResponder {
    self.highlighted = YES;
    return [super becomeFirstResponder];
}

/**
 * Returns a Boolean value indicating whether the receiver is willing to relinquish first-responder status.
 */
- (BOOL)canResignFirstResponder {
    return YES;
}

/**
 * Notifies the receiver that it has been asked to relinquish its status as first responder in its window.
 */
- (BOOL)resignFirstResponder {
    self.highlighted = NO;
    return [super resignFirstResponder];
}

#pragma mark -
#pragma mark Getters and setters

- (NSString *)title {
    return title_;
}

- (void)setTitle:(NSString *)aTitle {
    if (title_ != aTitle) {
        [title_ release];
        title_ = [aTitle copy];
    }
    
    [self setTitle:title_ forState:UIControlStateNormal];
}

/**
 * Sets the selectedDate
 *
 * @param aSelectedDate The value to set
 */
- (void)setSelectedDate:(NSDate *)aSelectedDate {
    
    if (![picker_.date isEqualToDate:aSelectedDate]) {
        
        picker_.date = aSelectedDate;
        
        [self datePickerValueChanged];
        
    }

}

/**
 * Returns the selectedDate
 *
 * @return selectedDate The variable
 */
- (NSDate *)selectedDate {
    return picker_.date;
}

/**
 * Sets the maximumDate
 *
 * @param maximumDate The value to set
 */
- (void)setMaximumDate:(NSDate *)maximumDate {
    picker_.maximumDate = maximumDate;
}

/**
 * Returns the maximumDate
 *
 * @return maximumDate The variable
 */
- (NSDate *)maximumDate {
    return picker_.maximumDate;
}

/**
 * Sets the minimumDate
 *
 * @param minimumDate The value to set
 */
- (void)setMinimumDate:(NSDate *)minimumDate {
    picker_.minimumDate = minimumDate;
}

/**
 * Returns the minimumDate
 *
 * @return The variable
 */
- (NSDate *)minimumDate {
    return picker_.minimumDate;
}

#pragma mark -
#pragma mark Date picker

/**
 * The value of the picker changed
 *
 * @private
 */
- (void)datePickerValueChanged {
    
    self.title = [Tools getStringFromDate:picker_.date];
    
    [delegate_ MOKDateComboButtonDateChanged:self];
}

#pragma mark -
#pragma mark Touches

/**
 * Tells the receiver when one or more fingers are raised from a view or window. Disables the listener notifications
 * Here we force the control to maintain highlighted bacause when a touch ended the control goes to normal state
 *
 * @param touches A set of UITouch instances that represent the touches for the ending phase of the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    self.highlighted = YES;
}

@end
