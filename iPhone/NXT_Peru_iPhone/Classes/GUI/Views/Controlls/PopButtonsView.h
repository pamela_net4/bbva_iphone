/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import <UIKit/UIKit.h>
#import "SingletonBase.h"


//Forward declarations
@class PopButtonsView;

/**
 * Enumerate to define pop buttons view state
 */
typedef enum {
	pbse_Idle = 0, //!<Poo buttons view is stationary (either shown or hiden)
	pbse_Show, //!<Pop buttons view must start showing state
	pbse_Hide, //!<Pop buttons view must start hidding state
	pbse_Relocate //!<Pop buttons view must start relocating state
} PopButtonsStateEnum;

/**
 * Provider to obtain a PopButtonsView created from a NIB file
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PopButtonsViewProvider: SingletonBase {
    
@private
    
    /**
     * Auxiliary pop buttons view to create it from a NIB file
     */
    PopButtonsView *auxView_;
    
}

/**
 * Provides read-write access to the auxililary pop buttons view
 */
@property (nonatomic, readwrite, assign) IBOutlet PopButtonsView *auxView;

@end


/**
 * Pop buttons view delegate to receive notifications when buttons are clicked
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol PopButtonsViewDelegate

@optional

/**
 * Informs the delegate that OK button was clicked
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void)okButtonClickedInPopButtonsView:(PopButtonsView *)aPopButtonsView;

/**
 * Informs the delegate that show available texts button was clicked
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void)showAvailableTextsButtonClickedInPopButtonsView:(PopButtonsView *)aPopButtonsView;

/**
 * Informs the delegate that previous responder button was clicked
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void)previousButtonClickedInPopButtonsView:(PopButtonsView *)aPopButtonsView;

/**
 * Informs the delegate that next responder button was clicked
 *
 * @param aPopButtonsView The pop buttons view triggering the event
 */
- (void)nextButtonClickedInPopButtonsView:(PopButtonsView *)aPopButtonsView;

@end


/**
 * View to show a bar with buttons to complement keyboard and picker views
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PopButtonsView : UIView {
    
@private
    
    /**
     * View image background
     */
    UIImageView *backgroundImage_;
    
    /**
     * Previous responder button
     */
    UIButton *previousButton_;
    
    /**
     * Next responder button
     */
    UIButton *nextButton_;
    
    /**
     * Show available texts button
     */
    UIButton *showAvailableTextsButton_;
    
    /**
     * OK button
     */
    UIButton *okButton_;
    
    /**
     * Enable button event forwarding flag
     */
    BOOL enableEventForwarding_;
    
    /**
     * Pop buttons view delegate to be informed when buttons are clicked
     */
    id<PopButtonsViewDelegate> delegate_;
}

/**
 * Provides read-write access to the view image background and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *backgroundImage;

/**
 * Provides read-write access to the previous responder button and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *previousButton;

/**
 * Provides read-write access to the next responder button and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *nextButton;

/**
 * Provides read-write access to the show available texts button and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *showAvailableTextsButton;

/**
 * Provides read-write access to the OK button and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIButton *okButton;

/**
 * Provides read-write access to the pop buttons view delegate
 */
@property (nonatomic, readwrite, assign) id<PopButtonsViewDelegate> delegate;


/**
 * Invoked by framework when Ok button is clicked. Delegate is notified that OK button was clicked
 *
 * @param aSender The OK button triggering the event
 */
- (IBAction)okButtonClicked:(UIButton *)aSender;

/**
 * Invoked by framework when show available texts button is clicked. Delegate is notified that
 * show available texts buttons was clicked
 *
 * @param aSender The show available texts button triggering the event
 */
- (IBAction)showAvailableTextButtonClicked:(UIButton *)aSender;

/**
 * Invoked by framework when next responder button is clicked. Delegate is notified that next responder button was clicked
 *
 * @param aSender The next responder button triggering the event
 */
- (IBAction)nextButtonClicked:(UIButton *)aSender;

/**
 * Invoked by framework when previous responder button is clicked. Delegate is notified that previous responder button was clicked
 *
 * @param aSender The previous responder button triggering the event
 */
- (IBAction)previousButtonClicked:(UIButton *)aSender;

/**
 * Creates and returns an autorelease PopButtonsView constructed from a NIB file
 *
 * @return The new PopButtonsView constructed from a NIB file
 */
+ (PopButtonsView *)popButtonsView;

/**
 * Shows or hides the previous responder and next respoder buttons depending on the provided parameter
 *
 * @param aShowFlag YES to show the previous responder and next responder buttons, NO otherwise
 */
- (void)showPreviousAndNextResponderButtons:(BOOL)aShowFlag;

/**
 * Enables or disables the previous responder button, depending on the provided parameter
 *
 * @param anEnableFlag YES to enable the previous responder button, NO otherwise
 */
- (void)enablePreviousResponderButton:(BOOL)anEnableFlag;

/**
 * Enables or disables the next responder button, depending on the provided parameter
 *
 * @param anEnableFlag YES to enable the next responder button, NO otherwise
 */
- (void)enableNextResponderButton:(BOOL)anEnableFlag;

/**
 * Shows or hides the show available texts button depending on the provided parameter
 *
 * @param aShowFlag YES to show the show available texts button, NO otherwise
 */
- (void)showShowAvailableTextsButton:(BOOL)aShowFlag;

/**
 * Enables or disables the OK button, depending on the provided parameter
 *
 * @param anEnableFlag YES to enable the OK button, NO otherwise
 */
- (void)enableOKResponderButton:(BOOL)anEnableFlag;

/**
 * Shows the "Lista" text or the "Teclado" text in available texts button depending on the
 * flat provided
 *
 * @param aShowListText YES to show "Lista" text in available texts button, NO to show "Teclado" text
 */
- (void)showListText:(BOOL)aShowListText;

/**
 * Enables and disables button events fowarding to delegate
 *
 * @param anEnabledState YES to enable forwarding, NO otherwise
 */
- (void)enableEventForwarding:(BOOL)anEnabledState;

@end
