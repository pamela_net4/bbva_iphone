/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * Table view that fades its cells at the bottom and at the top to indicate that there are more cells in that direction
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface FadingTableView : UITableView {

@private
	
	/**
	 * Table height percentage affected by fading (both top and bottom)
	 */
	CGFloat fadingHeightPercentage_;
	
	/**
	 * Scroll distance from first (top fading) or last (bottom fading) to show complete fading intensity. When the scrolling is intermediate, fading is interpolated
	 */
	CGFloat scrollDistanceToWholeFading_;
    
    /**
     * Fading effect intensity (only values from 0.0 to 1.0 are valid)
     */
    CGFloat fadingEffectIntensity_;
    
}

/**
 * Provides read-write access to the table height percentage affected by fading
 */
@property (nonatomic, readwrite, assign) CGFloat fadingHeightPercentage;

/**
 * Provides read-write access to the scroll distance from first (top fading) or last (bottom fading) to show complete fading intensity
 */
@property (nonatomic, readwrite, assign) CGFloat scrollDistanceToWholeFading;

/**
 * Provides read-write access to the fading effect intensity (only values from 0.0 to 1.0 are valid)
 */
@property (nonatomic, readwrite, assign) CGFloat fadingEffectIntensity;

@end
