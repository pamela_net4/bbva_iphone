/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "AlignableButton.h"


/**
 * Defines the components minimum distance from the left and right margins and between them in pixels
 */
#define MIN_MARGIN_DISTANCE                                         4.0f

/**
 * Defines the distance between the image and the title
 */
#define IMAGE_TO_TITLE_DISTANCE                                     4.0f


#pragma mark -

/**
 * AlignableButton private extension
 */
@interface AlignableButton()

/**
 * Calculates the button minimum width taking into account title and image sizes and margins
 */
- (CGFloat)buttonMinWidthToFit;

/**
 * Adjusts the frame to its content and aligns the button depending on the button alignment selected
 */
- (void)adjustButtonFrame;

@end


#pragma mark -

@implementation AlignableButton

#pragma mark -
#pragma mark Properties

@synthesize contentAlignment = contentAlignment_;
@synthesize buttonAlignment = buttonAlignment_;
@synthesize leftMargin = leftMargin_;
@synthesize rightMargin = rightMargin_;

#pragma mark -
#pragma mark Resizing button

/**
 * Sets the title to use for the specified state. Adjusts button frame if needed
 *
 * @param title The title to use for the specified state
 * @param state The state that uses the specified title
 */
- (void)setTitle:(NSString *)title forState:(UIControlState)state {
    
    [super setTitle:title forState:state];
    [self adjustButtonFrame];
    
}

/**
 * Sets the image to use for the specified state. Adjusts button frame if needed
 *
 * @param image The image to use for the specified state
 * @param state The state that uses the specified image
 */
- (void)setImage:(UIImage *)image forState:(UIControlState)state {
    
    [super setImage:image forState:state];
    [self adjustButtonFrame];
    
}

/*
 * Adjusts the frame to its content and aligns the button depending on the button alignment selected
 */
- (void)adjustButtonFrame {
    
    if (buttonAlignment_ != absa_AlignNone) {
        
        CGFloat buttonMinWidth = [self buttonMinWidthToFit];
        
        CGRect frame = self.frame;
        
        switch (buttonAlignment_) {
                
            case absa_AlignCenter:
                
                frame.origin.x = (CGRectGetMaxX(frame) - buttonMinWidth) / 2.0f;
                break;
                
            case absa_AlignRight: 
                
                frame.origin.x = CGRectGetMaxX(frame) - buttonMinWidth;
                break;
                
            default: {
                
                break;
                
            }
                
        }
        
        frame.size.width = buttonMinWidth;
        
        self.frame = frame;
        
        [self setNeedsLayout];
        [self setNeedsDisplay];
        
    }
    
}

/*
 * Calculates the button minimum width taking into account title and image sizes and margins
 */
- (CGFloat)buttonMinWidthToFit {
    
    UIImage *image = [self imageForState:UIControlStateNormal];
    CGSize imageSize = CGSizeZero;
    
    if (image != nil) {
        
        imageSize = image.size;
        
    }
    
    CGFloat imageWidth = imageSize.width;

    NSString *title = [self titleForState:UIControlStateNormal];
    
    UIFont *titleFont = nil;
    
    UILabel *titleLabel = [self titleLabel];
    titleFont = titleLabel.font;
    
    CGSize textSize = [title sizeWithFont:titleFont constrainedToSize:CGSizeMake(1000.0f, 1000.0f)];
    
    if (leftMargin_ < MIN_MARGIN_DISTANCE) {
        
        leftMargin_ = MIN_MARGIN_DISTANCE;
        
    }
    
    if (rightMargin_ < MIN_MARGIN_DISTANCE) {
        
        rightMargin_ = MIN_MARGIN_DISTANCE;
        
    }
    
    CGFloat result = imageWidth + textSize.width + leftMargin_ + rightMargin_;
    
    if ((imageWidth > 0.0f) && (textSize.width > 0.0f)) {
        
        result += IMAGE_TO_TITLE_DISTANCE;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Image and title position managent

/**
 * Returns the rectangle in which the receiver draws its image. Image rect is
 * calculated depending on the content alignment
 *
 * @param contentRect The content rectangle for the receiver
 * @return The rectangle in which the receiver draws its image
 */
- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    
    CGRect result;
    
    if (contentAlignment_ != abca_ImageNone) {
        
        result = self.bounds;
        
        UIImage *image = [self imageForState:UIControlStateNormal];
        CGSize imageSize = CGSizeZero;
        
        if (image != nil) {
            
            imageSize = image.size;
            
        }
        
        if (leftMargin_ < MIN_MARGIN_DISTANCE) {
            
            leftMargin_ = MIN_MARGIN_DISTANCE;
            
        }
        
        if (rightMargin_ < MIN_MARGIN_DISTANCE) {
            
            rightMargin_ = MIN_MARGIN_DISTANCE;
            
        }
        
        result.origin.y = (CGRectGetHeight(result) - imageSize.height) / 2.0f;
        
        if (contentAlignment_ == abca_ImageLeft) {
            
            result.origin.x = leftMargin_;
            
        } else if (contentAlignment_ == abca_ImageRight) {
            
            result.origin.x = CGRectGetMaxX(result) - imageSize.width - rightMargin_;
            
        }
        
        result.size.width = imageSize.width;
        result.size.height = imageSize.height;
        
    } else {
        
        result = CGRectZero;
        
    }
    
    return result;
    
}

/**
 * Returns the rectangle in which the receiver draws its title. Title rect is calculated
 * depending on the content aligmnent
 *
 * @param contentRect The content rectangle for the receiver
 * @return The rectangle in which the receiver draws its title
 */
- (CGRect)titleRectForContentRect:(CGRect)contentRect {
    
	CGRect result = self.bounds;
	
	static BOOL recursionBreak = NO;
	
	if (recursionBreak == NO) {
        
		recursionBreak = YES;
		NSString* title = [self titleForState:UIControlStateNormal];
		
        if ([title length] > 0) {
            
            UIFont* titleFont = nil;
            UILabel* titleLabel = [self titleLabel];
            titleFont = titleLabel.font;
            
            CGSize textSize = [title sizeWithFont:titleFont constrainedToSize:CGSizeMake(1000.0f, 1000.0f)];
            
            
            if (leftMargin_ < MIN_MARGIN_DISTANCE) {
                
                leftMargin_ = MIN_MARGIN_DISTANCE;
                
            }
            
            if (rightMargin_ < MIN_MARGIN_DISTANCE) {
                
                rightMargin_ = MIN_MARGIN_DISTANCE;
                
            }
            
            result.origin.y = floorf((CGRectGetHeight(result) - textSize.height) / 2.0f);
            
            CGFloat availableWidth = CGRectGetWidth(self.frame) - leftMargin_ - rightMargin_;
            
            UIImage* image = [self imageForState:UIControlStateNormal];
            CGFloat usedImageAndMarginWidth = 0.0f;
            
            if (image != nil) {
                
                usedImageAndMarginWidth = image.size.width;
                
            }
            
            if (usedImageAndMarginWidth > 0.0f) {
                
                usedImageAndMarginWidth += IMAGE_TO_TITLE_DISTANCE;
                
            }
            
            availableWidth -= usedImageAndMarginWidth;
            
            if (availableWidth < 0.0f) {
                
                availableWidth = 0.0f;
                
            }
            
            CGFloat titleWidth = textSize.width;
            
            if (titleWidth > availableWidth) {
                
                titleWidth = availableWidth;
                
            }
            
            if (contentAlignment_ == abca_ImageLeft) {
                
                result.origin.x = floorf(CGRectGetMaxX(result) - titleWidth - rightMargin_);
                
            } else if (contentAlignment_ == abca_ImageRight) {
                
                result.origin.x = floorf(leftMargin_);
                
            } else {
                
                result.origin.x = floorf((CGRectGetWidth(result) - titleWidth) / 2.0f);
                
            }
            
            result.size.width = floorf(titleWidth);
            result.size.height = floorf(textSize.height);
            
        }
		
		recursionBreak = NO;
        
	}
    
	return result;
    
}

#pragma mark -
#pragma mark Properties methods

/**
 * Sets the content alignment. Marks the button to redraw to calculate new positions
 *
 * @param aContentAlignment The new title and image relative alignment
 */
- (void)setContentAlignment:(AlignableButtonContentAlignment)aContentAlignment {
    
    if (contentAlignment_ != aContentAlignment) {
        
        contentAlignment_ = aContentAlignment;
        
        [self setNeedsLayout];
        [self setNeedsDisplay];
        
    }
    
}

/**
 * Sets the left border margin. Marks the button to redraw to calculate new positions
 *
 * @param aLeftMargin The new left border margin to store
 */
- (void)setLeftMargin:(CGFloat)aLeftMargin {
    
    if (leftMargin_ != aLeftMargin) {
        
        leftMargin_ = aLeftMargin;
        
        [self setNeedsLayout];
        [self setNeedsDisplay];
        
    }
    
}

/**
 * Sets the right border margin. Marks the button to redraw to calculate new positions
 *
 * @param aRightMargin The new right border margin to store
 */
- (void)setRightMargin:(CGFloat)aRightMargin {
    
    if (rightMargin_ != aRightMargin) {
        
        rightMargin_ = aRightMargin;
        
        [self setNeedsLayout];
        [self setNeedsDisplay];
        
    }
    
}

@end
