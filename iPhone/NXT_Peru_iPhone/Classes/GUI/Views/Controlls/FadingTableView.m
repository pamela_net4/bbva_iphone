/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "FadingTableView.h"


#pragma mark -

/**
 * FadingTableView private extension
 */
@interface FadingTableView()

/**
 * Calculates the cells fading
 */
- (void)calculateCellsFading;

/**
 * Initializes view attributes
 */
- (void)initializeAttributes;

@end


#pragma mark -

@implementation FadingTableView

#pragma mark -
#pragma mark Properties

@synthesize fadingHeightPercentage = fadingHeightPercentage_;
@synthesize scrollDistanceToWholeFading = scrollDistanceToWholeFading_;
@synthesize fadingEffectIntensity = fadingEffectIntensity_;

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Initializes view attributes
 */
- (void)awakeFromNib {

	[self initializeAttributes];
	
}

/**
 * Initializes and returns a newly allocated view object with the specified frame rectangle. Initializes view attributes
 *
 * @param aRect he frame rectangle for the view, measured in points
 * @return The initialized FadingTableView
 */
- (id)initWithFrame:(CGRect)aRect {

	if (self = [super initWithFrame:aRect]) {
		
		[self initializeAttributes];
		
	}
	
	return self;
	
}

/*
 * Initializes view attributes
 */
- (void)initializeAttributes {
	
	fadingHeightPercentage_ = 0.25f;
	scrollDistanceToWholeFading_ = 10.0f;
    fadingEffectIntensity_ = 0.75;
	[self calculateCellsFading];
	
}

#pragma mark -
#pragma mark Fading management

/**
 * Invoked to modify scroll position. used for change cells alpha
 *
 * @param rect The new bounds to store
 */
- (void)setBounds:(CGRect)rect {
	
	[super setBounds:rect];
	[self calculateCellsFading];
	
}

/*
 * Calculates the cells fading
 */
- (void)calculateCellsFading {
	
	CGRect bounds = self.bounds;
	NSArray *visibleIndexes = [self indexPathsForRowsInRect: bounds];
	
	CGFloat alpha;
	CGFloat fadingHeight = fadingHeightPercentage_ * CGRectGetHeight(bounds);
	CGFloat boundsHeight = CGRectGetHeight(bounds);
	CGFloat boundsTop = CGRectGetMinY(bounds);
	CGFloat boundsBottom = CGRectGetMaxY(bounds);

	CGFloat topScrollFadingEffect = 1.0f;
	
	if (boundsTop <= 0.0f) {
	
		topScrollFadingEffect = 0.0f;
		
	} else if (boundsTop < scrollDistanceToWholeFading_) {
		
		topScrollFadingEffect = boundsTop / scrollDistanceToWholeFading_;
		
	}
	
	CGFloat bottomScrollFaddingEffect = 1.0f;
	CGFloat contentHeight = self.contentSize.height;
	
	if (boundsBottom >= contentHeight) {
	
		bottomScrollFaddingEffect = 0.0f;
		
	} else if ((contentHeight - boundsBottom) < scrollDistanceToWholeFading_) {
	
		bottomScrollFaddingEffect = (contentHeight - boundsBottom) / scrollDistanceToWholeFading_;
		
	}
	
	for (NSIndexPath *index in visibleIndexes) {
		
		UITableViewCell * cell = [self cellForRowAtIndexPath:index];
		CGRect cellFrame = [self rectForRowAtIndexPath:index];
		
		cellFrame.origin.y = cellFrame.origin.y - boundsTop;
		
		CGFloat cellTop = CGRectGetMinY(cellFrame);
		CGFloat cellBottom = CGRectGetMaxY(cellFrame);
		
		alpha = 1.0f;
		
		if ((cellBottom >= 0) && (cellTop < fadingHeight)) {
			
            CGFloat fadingFactor = (fadingHeight - cellTop) / fadingHeight;
            
            if (fadingFactor > 1.0f) {
                
                fadingFactor = 1.0f;
                
            }
            
			alpha = 1.0f - (fadingEffectIntensity_ * fadingFactor * topScrollFadingEffect);
			
		} else if ((cellTop < boundsHeight) && (cellBottom > (boundsHeight - fadingHeight))) {
			
            CGFloat fadingFactor = (fadingHeight - boundsHeight + cellBottom) / fadingHeight;
            
            if (fadingFactor > 1.0) {
                
                fadingFactor = 1.0f;
                
            }
            
			alpha = 1.0f - (fadingEffectIntensity_ * fadingFactor * bottomScrollFaddingEffect);
			
		}
		
		cell.alpha = alpha;
		
	}
	
}

#pragma mark -
#pragma mark UIView methods

/**
 * Sets the new table frame and updates the fading cells
 *
 * @param frame The new frame
 */
- (void)setFrame:(CGRect)frame {

    [super setFrame:frame];
    [self calculateCellsFading];
    
}

#pragma mark -
#pragma mark Table view refreshing

/**
 * Reloads the rows and sections of the receiver. Fading is recalculated
 */
- (void)reloadData {

	[super reloadData];
	[self calculateCellsFading];
	
}

#pragma mark -
#pragma mark Properties methods

/**
 * Sets the new height percentage affected by fading and recalculates fading
 *
 * @param aValue The new height percentage to set
 */
- (void)setFadingHeightPercentage:(CGFloat)aValue {
	
	if (fadingHeightPercentage_ != aValue){
	
		if (aValue < 0.0f) {
		
			aValue = 0.0f;
			
		} else if (aValue > 0.5f) {
		
			aValue = 0.5f;
			
		}
		
		fadingHeightPercentage_ = aValue;
		[self calculateCellsFading];
		
	}
	
}

/**
 * Sest the new scroll distance to show complete fading, and recalculates fading
 *
 * @param aValue The new scroll distance to show complete fading
 */
- (void)setScrollDistanceToWholeFading:(CGFloat)aValue {
	
	if (scrollDistanceToWholeFading_ != aValue) {
	
		if (aValue < 0.0f) {
		
			aValue = 0.0f;
			
		}
		
		scrollDistanceToWholeFading_ = aValue;
		[self calculateCellsFading];
		
	}
	
}

/**
 * Sets the new fading effect intensity, and recalculates fading
 *
 * @param fadingEffectIntensity The new fading effect intensity
 */
- (void)setFadingEffectIntensity:(CGFloat)fadingEffectIntensity {
    
    if (fadingEffectIntensity_ != fadingEffectIntensity) {
        
        if (fadingEffectIntensity < 0.0f) {
            
            fadingEffectIntensity_ = 0.0f;
            
        } else if (fadingEffectIntensity > 1.0f) {
            
            fadingEffectIntensity_ = 1.0f;
            
        } else {
            
            fadingEffectIntensity_ = fadingEffectIntensity;
            
        }
        
        [self calculateCellsFading];
        
    }
    
}

@end
