/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


/**
 * Displays an amount (or any other text) and a currency symbol (or any other text) where the
 * currency font can be smaller than the amount and can be located either to the left or to the
 * right of the amount. The control will auto-resize to fit the content, up to a given width and
 * height. When resizing it can be configured whether the control left, righy or middel position
 * is preserved
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AmountAndCurrencyLabel : UIView {
    
@private
    
    /**
     * Amount label
     */
    UILabel *amountLabel_;
    
    /**
     * Currency label
     */
    UILabel *currencyLabel_;
    
    /**
     * Nominal amount text font size
     */
    CGFloat nominalFontSize_;
    
    /**
     * Currency font size ratio. It represents the currency font size with respect to the amount font size. Its valid
     * range is 0.0f - 1.0f
     */
    CGFloat currencyRelativeFontSize_;
    
    /**
     * Text color
     */
    UIColor *textColor_;
    
    /**
     * Text highlighted color
     */
    UIColor *highlightedTextColor_;
    
    /**
     * Currency to the left flag. YES when the currency is located on the amount text left, NO when it is on the right
     */
    BOOL currencyToTheLeft_;
    
    /**
     * Maximum size to resize
     */
    CGSize maximumSize_;
    
    /**
     * Control font. This font is used to obtain the font name, but the font size is calculated
     * dynamically when needed
     */
    UIFont *font_;
    
    /**
     * Text alignment. It is used both to layout the labels when they are smaller than the available size, and to
     * resize the control in the adecuate direction when needed
     */
    UITextAlignment textAlignment_;
    
    /**
     * Can resize horizontally flag. YES to resize the the control horizontally to fit the content, NO otherwise. The
     * text alignment is maintained
     */
    BOOL canResizeHorizontally_;
    
    /**
     * Can resize vertically flag. YES to resize the the control vertically to fit the content, NO otherwise. The
     * control vertical position is maintained
     */
    BOOL canResizeVertically_;
    
    /**
     * Maintain top position flag. YES when the top position must be maintained when the control is resized vertically, NO when
     * the bottom position must be maintained when the control is resized vertically
     */
    BOOL maintainTopPosition_;
    
    /**
     * Size used by the text inside the control
     */
    CGSize textTotalSize_;

}


/**
 * Provides read-write access to the amount to display
 */
@property (nonatomic, readwrite, copy) NSString *amountText;

/**
 * Provides read-write access to the currency to display
 */
@property (nonatomic, readwrite, copy) NSString *currencyText;

/**
 * Provides read-write access to the nominal amount text font size
 */
@property (nonatomic, readwrite, assign) CGFloat nominalFontSize;

/**
 * Provides read-write access to the currency font size ratio
 */
@property (nonatomic, readwrite, assign) CGFloat currencyRelativeFontSize;

/**
 * Provides read-write access to the text color
 */
@property (nonatomic, readwrite, retain) UIColor *textColor;

/**
 * Provides read-write access to the highlighted text highlighted color
 */
@property (nonatomic, readwrite, retain) UIColor *highlightedTextColor;

/**
 * Provides read-write access to the highlighted property
 */
@property (nonatomic, readwrite, assign) BOOL highlighted;

/**
 * Provides read-write access to the currency to the left flag
 */
@property (nonatomic, readwrite, assign) BOOL isCurrencyToTheLeft;

/**
 * Provides read-write access to the maximum size to resize
 */
@property (nonatomic, readwrite, assign) CGSize maximumSize;

/**
 * Provides read-write access to the control font
 */
@property (nonatomic, readwrite, retain) UIFont *font;

/**
 * Provides read-write access to the text alignment
 */
@property (nonatomic, readwrite, assign) UITextAlignment textAlignment;

/**
 * Provides read-write access to the can resize horizontally flag
 */
@property (nonatomic, readwrite, assign) BOOL canResizeHorizontally;

/**
 * Provides read-write access to the can resize vertically flag
 */
@property (nonatomic, readwrite, assign) BOOL canResizeVertically;

/**
 * Provides read-write access to the maintain top position flag
 */
@property (nonatomic, readwrite, assign) BOOL maintainTopPosition;

/**
 * Provides read-only access to the size used by the text inside the control
 */
@property (nonatomic, readonly, assign) CGSize textTotalSize;

@end
