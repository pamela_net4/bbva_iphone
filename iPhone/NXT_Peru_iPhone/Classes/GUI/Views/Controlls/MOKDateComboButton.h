/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

@class MOKDateComboButton;

@protocol MOKDateComboButtonDelegate 

/**
 * Tells delegate that a date has been selected
 *
 * @param combo The combo button
 */
- (void)MOKDateComboButtonDateChanged:(MOKDateComboButton *)combo;

@end

/**
 * Custom button with a combo like style that responds to UIResponder events and methods.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MOKDateComboButton : UIButton {
@private
    /**
     * Custom input view that shows when the button becomes first responder
     */
    UIView *inputView_;
    
    /**
     * Custom input accessory view that shows when the button becomes first responder
     */
    UIView *inputAccessoryView_;
    
    /**
     * Picker of the combo
     */
    UIDatePicker *picker_;
    
    /**
     * Title of the combo
     */
    NSString *title_;
    
    /**
     * Message to show when this control is empty
     */
    NSString *fillFormMessage_;
    
    /**
     * Delegate
     */
    id<MOKDateComboButtonDelegate > delegate_;
}

/**
 * Provides readwrite access to the fill form message
 */
@property (nonatomic, readwrite, retain) NSString *fillFormMessage;

/**
 * Provides readwrite access to the inputView
 */
@property (nonatomic, readwrite, retain) UIView *inputView;

/**
 * Provides readwrite access to the inputAccesoryView
 */
@property (nonatomic, readwrite, retain) UIView *inputAccessoryView;

/**
 * Provides readwrite access to the title
 */
@property (nonatomic, readwrite, copy) NSString *title;

/**
 * Provides readwrite access to the picker
 */
@property (nonatomic, readwrite, retain) UIDatePicker *picker;

/**
 * Provides readwrite access to the valid boolean
 */
@property (nonatomic, readonly, assign) BOOL valid;

/**
 * Provides readwrite access to the selected date of the UIPickerDate
 */
@property (nonatomic, readwrite, retain) NSDate *selectedDate;

/**
 * Provides readwrite access to the maximum date of the UIPickerDate
 */
@property (nonatomic, readwrite, assign) NSDate *maximumDate;

/**
 * Provides readwrite access to the minimum date of the UIPickerDate
 */
@property (nonatomic, readwrite, assign) NSDate *minimumDate;

/**
 * Provides readwrite access to the delegate_
 */
@property (nonatomic, readwrite, assign) id<MOKDateComboButtonDelegate > delegate;

@end
