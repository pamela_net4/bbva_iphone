//
//  OtherBankExtraView.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 10/5/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "OtherBankExtraView.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTTextField.h"
#import "StringKeys.h"
#import "PopButtonsView.h"
#import "NibLoader.h"
#import "UIColor+BBVA_Colors.h"

/**
 * Define the Simple selector account NIB file name
 */
#define NIB_FILE_NAME                                               @"OtherBankExtraView"

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SIZE                                              17.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALL_SIZE                                        15.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLER_SIZE                                      12.0f

/**
 * Defines the size of the font
 */
#define TEXT_FONT_SMALLEST_SIZE                                     10.0f


@implementation OtherBankExtraView

@synthesize delegate = delegate_;
@synthesize beneficiaryNameLabel = beneficiaryNameLabel_;
@synthesize beneficiaryNameTextField = beneficiaryTextField_;
@synthesize docTypeLabel = docTypeLabel_;
@synthesize docTypeCombo = docTypeCombo_;
@synthesize docTypeTextField = docTypeTextField_;

@synthesize popButtonsView = popButtonsView_;


-(void) initializeGraphicComponents{
    [NXT_Peru_iPhoneStyler styleLabel:beneficiaryNameLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [beneficiaryNameLabel_ setText:NSLocalizedString(BENEFICIARY_NAME_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleTextField:beneficiaryTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
        [beneficiaryTextField_ setPlaceholder:NSLocalizedString(BENEFICIARY_HINT_TEXT_KEY, nil)];
    
    [NXT_Peru_iPhoneStyler styleLabel:docTypeLabel_ withFontSize:TEXT_FONT_SIZE color:[UIColor BBVABlueColor]];
    [docTypeLabel_ setText:NSLocalizedString(DOCT_TYPE_LABEL_TEXT_KEY, nil)];
    
    UILabel *buttonLabel = docTypeCombo_.titleLabel;
    buttonLabel.font = [NXT_Peru_iPhoneStyler normalFontWithSize:12.0f];
    buttonLabel.lineBreakMode = UILineBreakModeTailTruncation;
    
    [NXT_Peru_iPhoneStyler styleTextField:docTypeTextField_ withFontSize:TEXT_FONT_SMALL_SIZE andColor:[UIColor BBVAGreyColor]];
    
    [docTypeTextField_ setPlaceholder:NSLocalizedString(DOCT_TYPE_HINT_TEXT_KEY, nil)];
   

}

-(void)setUp :(PopButtonsView*)popButtonView withViewController:(id)viewController{
    
    popButtonsView_ = popButtonView;
    
    [beneficiaryTextField_ setDelegate:viewController];
    [beneficiaryTextField_ setInputAccessoryView:popButtonsView_];
    
    [docTypeCombo_ setDelegate:viewController];
    [docTypeCombo_ setInputAccessoryView:popButtonsView_];
    
    [docTypeTextField_ setDelegate:viewController];
    [docTypeTextField_ setInputAccessoryView:popButtonsView_];
    
    [docTypeCombo_ addTarget:self action:@selector(docTypeComboPressed) forControlEvents:UIControlEventTouchUpInside];

    
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    [self initializeGraphicComponents];
    
}

/*
 * Creates and returns an autoreleased OtherBankExtraView constructed from a NIB file
 */
+ (OtherBankExtraView *)otherBankExtraView :(PopButtonsView*)mPopButtonView viewController:(id)viewController {
    
    OtherBankExtraView *result = (OtherBankExtraView *)[NibLoader loadObjectFromNIBFile:NIB_FILE_NAME];
    
    [result awakeFromNib];
    
    [result setUp:mPopButtonView withViewController:viewController];
    
    return result;
    
}

- (IBAction)docTypeComboPressed {
    
    [delegate_ docTypeComboPressed:docTypeCombo_];
}

@end
