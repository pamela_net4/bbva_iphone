/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import "NXTCurrencyTextField.h"

/**
 * Object that performs the delegate operations
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTCurrencyTextFieldDelegate : NSObject <UITextFieldDelegate> {
@private
    /**
     * External UITextFieldDelegate
     */
    id<UITextFieldDelegate> delegate_;
}

/**
 * Provides read-write access to the external UITextFieldDelegate
 */
@property (nonatomic, readwrite, assign) id<UITextFieldDelegate> delegate;

@end

#pragma mark -

@implementation NXTCurrencyTextFieldDelegate

#pragma mark -
#pragma mark Properties

@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    delegate_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark UITextFieldDelegate

/**
 * Asks the delegate if the specified text should be changed. Used to make sure the text field is visible even
 * above the pop buttons view, and maximum length is not exceeded
 *
 * @param textField The text field containing the text
 * @param range The range of characters to be replaced
 * @param string The replacement string
 * @return YES if text field has not exceeded its maximum length and is constrained to the edit text characteristics (integer number, currency value or free),
 * NO otherwise
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = NO;
    
    NSScanner* scanner = [NSScanner scannerWithString:string];
    
    if (([string length] == 0) || (([scanner scanInteger: nil] == YES) && ([scanner isAtEnd] == YES))) {
        
        NSString *textFieldText = textField.text;
        
        NSLocale *locale = [NSLocale currentLocale];
        NSString *decimalSeparator = [locale objectForKey:NSLocaleDecimalSeparator];
        NSString *groupingSeparator = [locale objectForKey:NSLocaleGroupingSeparator];
        NSString *replacedString = [textFieldText stringByReplacingCharactersInRange:range withString:string];
        replacedString = [replacedString stringByReplacingOccurrencesOfString:decimalSeparator withString:@""];
        replacedString = [replacedString stringByReplacingOccurrencesOfString:[locale objectForKey:NSLocaleGroupingSeparator] withString:@""];
        
        if ([replacedString length] > 0) {
            
            NSNumberFormatter *numberFormater = [[[NSNumberFormatter alloc] init] autorelease];
            [numberFormater setFormatterBehavior:NSNumberFormatterBehavior10_4];
            [numberFormater setUsesGroupingSeparator:YES];
            [numberFormater setGroupingSize:3];
            [numberFormater setGroupingSeparator:groupingSeparator];
            [numberFormater setMinimumFractionDigits:0];
            [numberFormater setMaximumFractionDigits:0];
            [numberFormater setMinimumIntegerDigits:1];
            
            BOOL canContainCents = ((NXTCurrencyTextField *)textField).canContainCents;
            
            if (canContainCents) {
                
                [numberFormater setDecimalSeparator:decimalSeparator];
                [numberFormater setMinimumFractionDigits:2];
                [numberFormater setMaximumFractionDigits:2];
                
            }
            
            scanner = [NSScanner scannerWithString:replacedString];
            NSDecimal intDecimal;
            [scanner scanDecimal:&intDecimal];
            
            NSDecimal resultDecimal;
            
            if (canContainCents) {
                
                NSDecimalMultiplyByPowerOf10(&resultDecimal, &intDecimal, -2, NSRoundPlain);
                
            } else {
                
                resultDecimal = intDecimal;
                
            }
            
            NSDecimalNumber *decimalNumber = [NSDecimalNumber decimalNumberWithDecimal:resultDecimal];
            
            if ((((NXTCurrencyTextField *)textField).maxLength < 0) || ([replacedString length] <= ((NXTCurrencyTextField *)textField).maxLength)) {
                
                textField.text = [numberFormater stringFromNumber:decimalNumber];
                
            }
            
        } else {
            
            [(NXTCurrencyTextField *)textField clear];
            
        }
        
    }
    
    return result;

}

/**
 * Asks the delegate if the text field’s current contents should be removed. When text field
 * contains a currency value, text field text is reseted to a zero value and no removal is
 * allowed
 *
 * @param textField The text field containing the text
 * @return NO it text field contains a currency, YES otherwise
 */
- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}

/**
 * Asks the delegate if editing should begin in the specified text field. Forwards it to the external delegate
 *
 * @param textField The text field for which editing is about to begin
 * @return YES if an editing session should be initiated; otherwise, NO to disallow editing
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    BOOL result = NO;
    
    if (delegate_ != nil) {
        if ([delegate_ respondsToSelector:@selector(textFieldShouldBeginEditing:)]) {
            result = [delegate_ textFieldShouldBeginEditing:textField];
        }
    } else {
        result = YES;
    }
    
    return result;
}

/**
 * Tells the delegate that editing began for the specified text field. Forwards it to the external delegate
 *
 * @param textField The text field for which an editing session began
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    [textField setTextAlignment:UITextAlignmentLeft];
    
    if ([delegate_ respondsToSelector:@selector(textFieldDidBeginEditing:)]) {
        [delegate_ textFieldDidBeginEditing:textField];
    }
    
}

/**
 * Asks the delegate if editing should stop in the specified text field. Forwards it to the external delegate
 *
 * @param textField The text field for which editing is about to end
 * @return YES if editing should stop; otherwise, NO if the editing session should continue
 */
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    BOOL result = YES;
    
    if (delegate_ != nil) {
        if ([delegate_ respondsToSelector:@selector(textFieldShouldEndEditing:)]) {
            result = [delegate_ textFieldShouldEndEditing:textField];
        }
    } else {
        result = YES;
    }
    
    if ([[textField text] length] == 0) {
        
        BOOL currencyShowAlways_ = ((NXTCurrencyTextField *)textField).currencyShowAlways;
        if(!currencyShowAlways_){
            [textField setLeftViewMode:UITextFieldViewModeNever];
        }
        
        
    } else {
        
        [textField setLeftViewMode:UITextFieldViewModeAlways];
        
    }
    
    return result;
}

/**
 * Tells the delegate that editing stopped for the specified text field. Forwards it to the external delegate
 *
 * @param textField The text field for which editing ended
 */
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (![(NXTCurrencyTextField *)textField hasValue]) {
        textField.text = nil;
    }
    
    if ([delegate_ respondsToSelector:@selector(textFieldDidEndEditing:)]) {
        [delegate_ textFieldDidEndEditing:textField];
    }
    
}

/**
 * Asks the delegate if the text field should process the pressing of the return button. Forwards it to the external delegate
 *
 * @param textField The text field whose return button was pressed
 * @return YES if the text field should implement its default behavior for the return button; otherwise, NO
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    BOOL result = NO;
    
    if (delegate_ != nil) {
        if ([delegate_ respondsToSelector:@selector(textFieldShouldReturn:)]) {
            result = [delegate_ textFieldShouldReturn:textField];
        }
    } else {
        result = YES;
    }
    
    return result;    
}

@end

#pragma mark -

@interface NXTCurrencyTextField()

/**
 * Initiales the instance
 */
- (void)initialize;

@end

@implementation NXTCurrencyTextField

#pragma mark -
#pragma mark Properties

@dynamic currencySymbol;
@synthesize canContainCents = canContainCents_;
@synthesize currencyShowAlways = currencyShowAlways_;
@synthesize maxLength = maxLength_;
@synthesize maxDecimalNumbers = maxDecimalNumbers_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    [currencySymbol_ release];
    currencySymbol_ = nil;
    
    [currencyTextFieldDelegate_ release];
    currencyTextFieldDelegate_ = nil;
    
    [super dealloc];    
}

#pragma mark -
#pragma mark Initialization

/**
 * Initializes the instance
 */
- (void)initialize {
    self.textColor = [UIColor colorWithRed:0.0f green:0.5765f blue:0.8784f alpha:1.0f];
    
    [currencySymbol_ release];
    currencySymbol_ = [[NSString alloc] init];
    
    maxDecimalNumbers_ = 2;
    maxLength_ = 12;
	[currencyTextFieldDelegate_ release];
    currencyTextFieldDelegate_ = [[NXTCurrencyTextFieldDelegate alloc] init];
    self.delegate = currencyTextFieldDelegate_;
    
    [currencyTextFieldDelegate_ textFieldShouldClear:self];
    
    [self setLeftViewMode:UITextFieldViewModeNever];
    [self setRightViewMode:UITextFieldViewModeNever];
    [self setTextAlignment:UITextAlignmentLeft];
    
    self.canContainCents = YES;
    self.currencyShowAlways = NO;
}

/**
 * View is loaded form a NIB file
 */
- (void)awakeFromNib {
    [self initialize];
}

/**
 * Desiganted initializer
 */
- (id)init {
    if ((self = [super init])) {
        [self initialize];
    }
    
    return self;
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Returns the currency symbol
 */
- (NSString *)currencySymbol {
    return currencySymbol_;
}

/**
 * Sets the currency symbol
 *
 * @param value The new value to set
 */
- (void)setCurrencySymbol:(NSString *)value {
    
    [currencySymbol_ release];
    currencySymbol_ = [value copy];
    
    CGFloat rightGap = 0.0f;
    
    if (self.borderStyle != UITextBorderStyleRoundedRect) {
        rightGap = 10.0f;
    }
    
    CGSize labelSize = [currencySymbol_ sizeWithFont:self.font constrainedToSize:CGSizeMake(self.frame.size.height, self.frame.size.height)];
    UILabel *currencyLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, labelSize.width + rightGap, labelSize.height)] autorelease];
    currencyLabel.backgroundColor = [UIColor clearColor];
    currencyLabel.textAlignment = UITextAlignmentRight;
    currencyLabel.text = currencySymbol_;
    
    currencyLabel.font = self.font;
    currencyLabel.textColor = self.textColor;
    
    [self setLeftView:currencyLabel];
    
}

/**
 * Sets the UITextField delegate. Passes it to its internal delegate
 *
 * @param delegate The new UITextField delegate
 */
- (void)setDelegate:(id <UITextFieldDelegate>)delegate {
    
    if (delegate == currencyTextFieldDelegate_) {
        [super setDelegate:delegate];
    } else {
        currencyTextFieldDelegate_.delegate = delegate;
    }
    
}

/**
 * Returns the UITextField delegate, obtained from the internal delegate
 *
 * @return The delegate stored in the internal delegate
 */
- (id<UITextFieldDelegate>)delegate {
    return currencyTextFieldDelegate_.delegate;
}

/**
 * Sets the text of the textfield
 *
 * @param text The new text to set
 */
- (void)setText:(NSString *)text {
    [self setTextAlignment:UITextAlignmentLeft];
    [super setText:text];
    
    if ([[self text] length] == 0) {
        
        if(!currencyShowAlways_){
            [self setLeftViewMode:UITextFieldViewModeNever];
        }
        
    } else {
        
        [self setLeftViewMode:UITextFieldViewModeAlways];
        
    }
    
}

#pragma mark -
#pragma mark Information

/*
 * Indicates if the text field has a value
 */
- (BOOL)hasValue {
    return ([self.text length] > 0 && ![self.text isEqualToString:@"0"]);
}

/*
 * Clears the text field
 */
- (void)clear {	
	self.text = nil;
}

/**
 * Sets the value that indicates the currency format
 *
 * @param value The new value to set
 */
- (void)setCanContainCents:(BOOL)value {
    canContainCents_ = value;
        
	self.keyboardType = UIKeyboardTypeNumberPad;
	
}

/**
 * Sets the value that indicates the currency format
 *
 * @param value The new value to set
 */
- (void)setCurrencyShowAlways:(BOOL)value {
    currencyShowAlways_ = value;
    
    if(currencyShowAlways_){
        [self setLeftViewMode:UITextFieldViewModeAlways];
    }
    
}

@end