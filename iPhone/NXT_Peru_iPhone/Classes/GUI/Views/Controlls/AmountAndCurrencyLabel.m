/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "AmountAndCurrencyLabel.h"


#pragma mark -

/**
 * AccountAndCurrencyLabel private extension
 */
@interface AmountAndCurrencyLabel()

/**
 * Creates the view labels in case they are not created, attaches them to the view and initializes the class attributes
 *
 * @private
 */
- (void)initializeAmountAndCurrencyLabelAttributes;

/**
 * Calculates the total size used by the text to display for a given amount font size
 *
 * @param anAmountFontSize The amount font size used to calculate the total size
 * @return The total size used by the text to display
 * @private
 */
- (CGSize)totalTextSizeForAmountFontSize:(CGFloat)anAmountFontSize;

/**
 * Aligns the labels to the adecuate text alignment, maintainin the amount vertical position but modifying the horizonta position
 * when needed
 *
 * @private
 */
- (void)alignLabels;

/**
 * Resizes the control maintaining the configured positions, both horizontally and vertically
 *
 * @private
 */
- (void)resizeControl;

/**
 * Calculates the separator width with the given font. This separates the amount from the currency
 *
 * @param aFont The font to calculate the width with
 * @return The separator width with the given font
 * @private
 */
- (CGFloat)separatorWidthWithFont:(UIFont *)aFont;

/**
 * Lays out the labels
 *
 * @private
 */
- (void)layoutLabels;

@end


#pragma mark -

@implementation AmountAndCurrencyLabel

#pragma mark -
#pragma mark Properties

@dynamic amountText;
@dynamic currencyText;
@synthesize nominalFontSize = nominalFontSize_;
@synthesize currencyRelativeFontSize = currencyRelativeFontSize_;
@synthesize textColor = textColor_;
@synthesize highlightedTextColor = highlightedTextColor_;
@dynamic highlighted;
@synthesize isCurrencyToTheLeft = currencyToTheLeft_;
@synthesize maximumSize = maximumSize_;
@synthesize font = font_;
@synthesize textAlignment = textAlignment_;
@synthesize canResizeHorizontally = canResizeHorizontally_;
@synthesize canResizeVertically = canResizeVertically_;
@synthesize maintainTopPosition = maintainTopPosition_;
@synthesize textTotalSize = textTotalSize_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {

    [amountLabel_ release];
    amountLabel_ = nil;
    
    [currencyLabel_ release];
    currencyLabel_ = nil;
    
    [textColor_ release];
    textColor_ = nil;
    
    [highlightedTextColor_ release];
    highlightedTextColor_ = nil;
    
    [font_ release];
    font_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Creates the labels needed to display information
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initializeAmountAndCurrencyLabelAttributes];
    
}

/**
 * Superclass designated initializer. Initializes an AcountAndCurrencyLabel instance, creating the label needed to display information
 *
 * @param frame The original view frame
 * @return The initialized AcountAndCurrencyLabel instance
 */
- (id)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        [self initializeAmountAndCurrencyLabelAttributes];
        
    }
    
    return self;
    
}

/*
 * Creates the view labels in case they are not created, attaches them to the view and initializes the class attributes
 */
- (void)initializeAmountAndCurrencyLabelAttributes {
    
    CGRect frame = self.frame;
    CGFloat height = frame.size.height;
    CGRect labelsOriginalFrame = CGRectMake(0.0f, 0.0f, 1.0f, height);
    UIColor *clearColor = [UIColor clearColor];
    
    if (amountLabel_ == nil) {
        
        amountLabel_ = [[UILabel alloc] initWithFrame:labelsOriginalFrame];
        amountLabel_.backgroundColor = clearColor;
        amountLabel_.adjustsFontSizeToFitWidth = NO;
        amountLabel_.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [self addSubview:amountLabel_];
        
        if (font_ == nil) {
            
            font_ = [amountLabel_.font retain];
            
        }
        
    }
    
    if (currencyLabel_ == nil) {
        
        currencyLabel_ = [[UILabel alloc] initWithFrame:labelsOriginalFrame];
        currencyLabel_.backgroundColor = clearColor;
        currencyLabel_.adjustsFontSizeToFitWidth = NO;
        currencyLabel_.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [self addSubview:currencyLabel_];
        
    }
    
    maximumSize_ = self.frame.size;

    if(textColor_ != nil){
        amountLabel_.textColor = textColor_;
        currencyLabel_.textColor = textColor_;
    }
    amountLabel_.highlightedTextColor = highlightedTextColor_;
    currencyLabel_.highlightedTextColor = highlightedTextColor_;
    
    [self layoutLabels];
    
}

#pragma mark -
#pragma mark Control maths

/*
 * Calculates the total size used by the text to display for a given amount font size
 */
- (CGSize)totalTextSizeForAmountFontSize:(CGFloat)anAmountFontSize {
    
    NSString *amountText = amountLabel_.text;
    NSString *currencyText = currencyLabel_.text;
    BOOL hasCurrency = ([currencyText length] > 0);
    
    NSString *fontName = font_.fontName;
    UIFont *amountTextFont = [UIFont fontWithName:fontName
                                             size:anAmountFontSize];
    UIFont *currencyTextFont = [UIFont fontWithName:fontName
                                               size:round(anAmountFontSize * currencyRelativeFontSize_)];
    
    CGSize amountTextSize = [amountText sizeWithFont:amountTextFont];
    CGSize currencyTextSize = CGSizeZero;
    
    if (hasCurrency) {
        
        currencyTextSize = [currencyText sizeWithFont:currencyTextFont];
        CGFloat currencyTextSizeWidth = currencyTextSize.width;
        
        if (currencyTextSizeWidth > 0.0f) {
            
            CGFloat separatorWidth = [self separatorWidthWithFont:currencyTextFont];
            currencyTextSize.width = currencyTextSizeWidth + separatorWidth;

        }
        
    }
    
    CGSize totalSize = CGSizeMake(amountTextSize.width + currencyTextSize.width, amountTextSize.height);
    
    return totalSize;
    
}

/*
 * Aligns the labels to the adecuate text alignment, maintainin the amount vertical position but modifying the horizonta position
 * when needed
 */
- (void)alignLabels {
    
    UIFont *amountFont = amountLabel_.font;
    CGFloat amountDescender = amountFont.descender;
    
    UIFont *currencyFont = currencyLabel_.font;
    CGFloat currencyDescender = currencyFont.descender;
    
    CGRect amountFrame = amountLabel_.frame;
    CGFloat amountBottom = CGRectGetMaxY(amountFrame);
    CGFloat amountTop = CGRectGetMinY(amountFrame);
    CGFloat amountWidth = amountFrame.size.width;
    CGFloat amountHeigh = amountFrame.size.height;
    
    CGRect currencyFrame = currencyLabel_.frame;
    CGFloat currencyWidth = currencyFrame.size.width;
    CGFloat currencyHeight = currencyFrame.size.height;
    CGFloat currencyTop = amountBottom + round(amountDescender - currencyDescender) - currencyHeight;
    
    CGFloat separatorWidth = 0.0f;
    
    if (currencyWidth > 0.0f) {
        
        separatorWidth = [self separatorWidthWithFont:currencyFont];
        
    }
    
    CGRect frame = self.frame;
    
    CGFloat leftMostPostion = 0.0f;
    
    if (textAlignment_ == UITextAlignmentRight) {
        
        leftMostPostion = frame.size.width - amountWidth - separatorWidth - currencyWidth;
        
    } else if (textAlignment_ == UITextAlignmentCenter) {
        
        leftMostPostion = round((frame.size.width - amountWidth - separatorWidth - currencyWidth) / 2.0f);
        
    }
    
    if (currencyToTheLeft_) {
        
        currencyLabel_.frame = CGRectMake(leftMostPostion, currencyTop, currencyWidth, currencyHeight);
        
        CGFloat amountLeft = leftMostPostion + currencyWidth + separatorWidth;
        amountLabel_.frame = CGRectMake(amountLeft, amountTop, amountWidth, amountHeigh);
        
    } else {
        
        amountLabel_.frame = CGRectMake(leftMostPostion, amountTop, amountWidth, amountHeigh);
        
        CGFloat currencyLeft = leftMostPostion + amountWidth + separatorWidth;
        currencyLabel_.frame = CGRectMake(currencyLeft, currencyTop, currencyWidth, currencyHeight);
        
    }
    
}

/*
 * Resizes the control maintaining the configured positions, both horizontally and vertically
 */
- (void)resizeControl {
    
    if ((textTotalSize_.width > 0.0f) && (textTotalSize_.height > 0.0f)) {
        
        if ((canResizeHorizontally_) || (canResizeVertically_)) {
            
            CGRect frame = self.frame;
            CGFloat left = frame.origin.x;
            CGFloat top = frame.origin.y;
            CGFloat width = frame.size.width;
            CGFloat height = frame.size.height;
            
            if (canResizeHorizontally_) {
                
                if (textAlignment_ == UITextAlignmentCenter) {
                    
                    left += round((width - textTotalSize_.width) / 2.0f);
                    
                } else if (textAlignment_ == UITextAlignmentRight) {
                    
                    left += round(width - textTotalSize_.width);
                    
                }

                width = textTotalSize_.width;

            }
            
            if (canResizeVertically_) {
                
                if (!maintainTopPosition_) {
                    
                    top += height - textTotalSize_.height;
                    
                }
                
                height = textTotalSize_.height;
                
            }
            
            [super setFrame:CGRectMake(left, top, width, height)];
            
        }
            
    }
    
}

/*
 * Calculates the separator width with the given font. This separates the amount from the currency
 */
- (CGFloat)separatorWidthWithFont:(UIFont *)aFont {
    
    CGSize separatorSize = [@" " sizeWithFont:aFont];
    return separatorSize.width;
    
}

/*
 * Lays out the labels
 */
- (void)layoutLabels {
    
    CGSize availableSize = maximumSize_;
    CGRect frame = self.frame;
    
    if (!canResizeHorizontally_) {
        
        availableSize.width = frame.size.width;
        
    }
    
    if (!canResizeVertically_) {
        
        availableSize.height = frame.size.height;
        
    }
    
    CGFloat amountFontSize = nominalFontSize_;
    CGSize usedSize;
    
    do {
        
        usedSize = [self totalTextSizeForAmountFontSize:amountFontSize];
        
        if ((usedSize.width <= availableSize.width) && (usedSize.height <= availableSize.height)) {
            
            break;
            
        } else {
            
            amountFontSize -= 1.0f;
            
        }
        
    } while (amountFontSize > 0.0f);
    
    CGSize amountSize = CGSizeZero;
    CGSize currencySize = CGSizeZero;
    CGFloat separatorWidth = 0.0f;
    
    if (amountFontSize > 0.0f) {
        
        amountLabel_.hidden = NO;
        
        NSString *fontName = font_.fontName;
        UIFont *amountTextFont = [UIFont fontWithName:fontName
                                                 size:amountFontSize];
        amountLabel_.font = amountTextFont;
        amountSize = [amountLabel_.text sizeWithFont:amountTextFont];
        
        CGFloat currencyFontSize = round(amountFontSize * currencyRelativeFontSize_);
        
        if (currencyFontSize > 0.0f) {
            
            currencyLabel_.hidden = NO;
            UIFont *currencyTextFont = [UIFont fontWithName:fontName
                                                       size:currencyFontSize];
            currencyLabel_.font = currencyTextFont;
            currencySize = [currencyLabel_.text sizeWithFont:currencyTextFont];
            
            if (currencySize.width > 0.0f) {
                
                separatorWidth = [self separatorWidthWithFont:currencyTextFont];
                
            }
            
        } else {
            
            currencyLabel_.hidden = YES;
            
        }
        
    } else {
        
        amountLabel_.hidden = YES;
        currencyLabel_.hidden = YES;
        
    }
    
    if ((amountSize.width == 0.0f) || (amountSize.height == 0.0f)) {
        
        currencySize.width = 0.0f;
        currencySize.height = 0.0f;
        amountSize.width = 0.0f;
        amountSize.height = 0.0f;
        currencyLabel_.hidden = YES;
        
    } else {
        
        currencyLabel_.hidden = NO;
        
    }
    
    textTotalSize_ = CGSizeMake(amountSize.width + currencySize.width + separatorWidth, amountSize.height);
    [self resizeControl];
    amountLabel_.frame = CGRectMake(0.0f, round((self.frame.size.height - amountSize.height) / 2.0f), amountSize.width, amountSize.height);
    currencyLabel_.frame = CGRectMake(0.0f, 0.0f, currencySize.width, currencySize.height);
    [self alignLabels];
    
}

#pragma mark -
#pragma mark UIView selectors

/**
 * Sets the view frame. Sets the control to layout the label again
 *
 * @param frame The new view frame
 */
- (void)setFrame:(CGRect)frame {
    
    [super setFrame:frame];
        
    [self layoutLabels];
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the amount text displayed
 *
 * @return the amount text displayed
 */
- (NSString *)amountText {
    
    return amountLabel_.text;
    
}

/**
 * Sets the new amount text to display and sets the view to layout again
 *
 * @param aValue The new amount text to display
 */
- (void)setAmountText:(NSString *)aValue {
    
    amountLabel_.text = aValue;
    
    [self layoutLabels];
    
}

/*
 * Returns the currency text displayed
 *
 * @return The currency text displayed
 */
- (NSString *)currencyText {
    
    return currencyLabel_.text;
    
}

/**
 * Sets the new currency text to display and sets the view to layout again
 *
 * @param aValue the new currency text to display
 */
- (void)setCurrencyText:(NSString *)aValue {
    
    currencyLabel_.text = aValue;
    
    [self layoutLabels];
    
}

/**
 * Sets the nominal amount text font size and sets the view to layout again
 *
 * @param aValue The new nominal amount text font size
 */
- (void)setNominalFontSize:(CGFloat)aValue {
    
    aValue = round(aValue);
    
    if (aValue != nominalFontSize_) {
        
        nominalFontSize_ = aValue;
        
        [self layoutLabels];
        
    }
    
}

/**
 * Sets the currency font size ratio and sets the view to layout again
 *
 * @param aValue The new currency font size ratio
 */
- (void)setCurrencyRelativeFontSize:(CGFloat)aValue {
    
    if (aValue < 0.0f) {
        
        aValue = 0.0f;
        
    } else if (aValue > 1.0f) {
        
        aValue = 1.0f;
    }
    
    if (aValue != currencyRelativeFontSize_) {
        
        currencyRelativeFontSize_ = aValue;
        
        [self layoutLabels];
        
    }
    
}

/**
 * Sets the new currency to the left flag and sets the view to layout again
 *
 * @param aValue The new currency to the left flag
 */
- (void)setIsCurrencyToTheLeft:(BOOL)aValue {
    
    if (aValue != currencyToTheLeft_) {
        
        currencyToTheLeft_ = aValue;
        
        [self layoutLabels];
        
    }
    
}

/*
 * Sets the new text color for both labels
 *
 * @param aValue The new text color
 */
- (void)setTextColor:(UIColor *)aValue {
    
    if (aValue != textColor_) {
        
        [aValue retain];
        [textColor_ release];
        textColor_ = aValue;
        
        amountLabel_.textColor = aValue;
        currencyLabel_.textColor = aValue;
        
    }
    
}

/*
 * Sets the new highlighted text color for both labels
 *
 * @param aValue The new highlighted text color
 */
- (void)setHighlightedTextColor:(UIColor *)aValue {
    
    if (aValue != highlightedTextColor_) {

        [aValue retain];
        [highlightedTextColor_ release];
        highlightedTextColor_ = aValue;
        
        amountLabel_.highlightedTextColor = aValue;
        currencyLabel_.highlightedTextColor = aValue;
        
    }
    
}

/*
 * Returns the highlighted property
 *
 * @return The highlighted property
 */
- (BOOL)highlighted {
    
    return amountLabel_.highlighted;
    
}

/*
 * Sets the new highlighted property for both labels
 *
 * @param aValue The new highlighted property
 */
- (void)setHighlighted:(BOOL)aValue {
    
    amountLabel_.highlighted = aValue;
    currencyLabel_.highlighted = aValue;
    
}

/**
 * Sets the maximum size to resize and sets the view to layout again
 *
 * @param aValue The new masimum size to resize
 */
- (void)setMaximumSize:(CGSize)aValue {
    
    if (!CGSizeEqualToSize(aValue, maximumSize_)) {
        
        maximumSize_ = aValue;
        
        [self layoutLabels];
        
    }
    
}

/**
 * Sets the control font and sets the view to layout again. New font must be not nil, or it will be discarded
 *
 * @param aValue The new control font
 */
- (void)setFont:(UIFont *)aValue {
    
    if ((aValue != nil) && (aValue != font_)) {
        
        [aValue retain];
        [font_ release];
        font_ = aValue;
        
        [self layoutLabels];
        
    }
    
}

/**
 * Sets the new text alignment and relocates the labels to fit the new text alignment
 *
 * @param aValue the new text alignment
 */
- (void)setTextAlignment:(UITextAlignment)aValue {
    
    if (aValue != textAlignment_) {
        
        textAlignment_ = aValue;
        
        [self alignLabels];
        
    }
    
}

/**
 * Sets the new can resize horizontally flag and if necessary resizes the control
 *
 * @param aValue the new can resize horizontally flag
 */
- (void)setCanResizeHorizontally:(BOOL)aValue {
    
    if (aValue != canResizeHorizontally_) {
        
        canResizeHorizontally_ = aValue;
        
        [self resizeControl];
        
    }
    
}

/**
 * Sets the new can resize vertically flag and if necessary resizes the control
 *
 * @param aValue the new can resize vertically flag
 */
- (void)setCanResizeVertically:(BOOL)aValue {
    
    if (aValue != canResizeVertically_) {
        
        canResizeVertically_ = aValue;
        
        [self resizeControl];
        
    }
    
}

@end
