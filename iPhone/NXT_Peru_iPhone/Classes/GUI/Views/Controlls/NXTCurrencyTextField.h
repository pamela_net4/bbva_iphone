/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import "NXTTextField.h"

@class NXTCurrencyTextFieldDelegate;

/**
 * Custom text field with a currency symbol and currency behaviour 
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTCurrencyTextField : NXTTextField {
@private    
    /**
     * Currency string
     */
    NSString *currencySymbol_;
    
    /**
     * Flag that indicates if the text field can contain cents
     */
    BOOL canContainCents_;
    
    /**
     * Flag that indicates if the text field can contain cents
     */
    BOOL currencyShowAlways_;
    
    /**
     * Max length of this text field
     */
    NSInteger maxLength_;
    
    /**
     * Delegate of this text field
     */
    NXTCurrencyTextFieldDelegate *currencyTextFieldDelegate_;
    
    /**
     * Maximum number of decimals
     */
    NSInteger maxDecimalNumbers_;
}

/**
 * Provides readwrite access to the maxLength
 */
@property (nonatomic, readwrite, assign) NSInteger maxLength;

/**
 * Provides readwrite access to the maxDecimalNumbers
 */
@property (nonatomic, readwrite, assign) NSInteger maxDecimalNumbers;

/**
 * Provides readwrite access to the canContainCents flag
 */
@property (nonatomic, readwrite, assign) BOOL canContainCents;

/**
 * Provides readwrite access to the currencyShowAlways flag
 */
@property (nonatomic, readwrite, assign) BOOL currencyShowAlways;

/**
 * Provides read-write access to the currency string
 */
@property (nonatomic, readwrite, copy) NSString *currencySymbol;

/**
 * Indicates if the text field has a value
 */
- (BOOL)hasValue;

/**
 * Clears the text field
 */
- (void)clear;

@end
