/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

/**
 * Section header view (either for tables or for plain views)
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface SectionHeader : UIView {
    
@private
    
    /**
     * Left text label
     */
    UILabel *leftTextLabel_;
    
    /**
     * Right text label
     */
    UILabel *rightTextLabel_;
    
    /**
     * Bottom separator line image view
     */
    UIImageView *bottomSeparatorImageView_;
    
    /**
     * Top separator line image view
     */
    UIImageView *topSeparatorImageView_;
    
    /**
     * Flag to indicate that the text is gray (and aligment is centered). If NO, text is blue and text is left aligned
     */
    BOOL grayCenteredText_;
    
}


/**
 * Provides readwrite access to the left text label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *leftTextLabel;

/**
 * Provides readwrite access to the right text label and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UILabel *rightTextLabel;

/**
 * Provides readwrite access to the bottom line separator image view and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *bottomSeparatorImageView;

/**
 * Provides readwrite access to the topSeparator. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet UIImageView *topSeparatorImageView;

/**
 * Provides readwrite access to the leftText
 */
@property (nonatomic, readwrite, copy) NSString *leftText;

/**
 * Provides readwrite access to the leftText
 */
@property (nonatomic, readwrite, copy) NSString *rightText;

/**
 * Provides readwrite access to the grayCenteredText
 */
@property (nonatomic, readwrite, assign) BOOL grayCenteredText;

/**
 * Creates and returns aa autoreleased SectionHeader constructed from a NIB file
 *
 * @return The autoreleased SectionHeader constructed from a NIB file
 */
+ (SectionHeader *)sectionHeader;

/**
 * Returns the height of the section header
 */
+ (CGFloat)height;


/**
 * Styles the right text with the correct color for the given amount
 *
 * @param amount The amount to set the right text color
 */
- (void)styleRightColorForAmount:(NSDecimalNumber *)amount;

@end
