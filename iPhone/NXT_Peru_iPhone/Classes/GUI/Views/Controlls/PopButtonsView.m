/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "PopButtonsView.h"
#import "StringKeys.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "Constants.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "UIColor+BBVA_Colors.h"


/**
 * Define the PopButtonsView NIB file name
 */
#define NIB_FILE_NAME                                               @"PopButtonsView"

/**
 * Defines the buttons title size
 */
#define BUTTONS_TITTLE_SIZE                                             14.0f


#pragma mark -

/**
 * Pop buttons view provider private category
 */
@interface PopButtonsViewProvider(private)

/**
 * Returns the singleton only instance
 *
 * @return Singleton only instance
 */
+ (PopButtonsViewProvider *)getInstance;

/**
 * Creates and returns an autoreleased PopButtonsView constructed from a NIB file
 *
 * @return The autoreleased PopButtonsView constructed from a NIB file
 */
- (PopButtonsView *)popButtonsView;

@end


#pragma mark -

@implementation PopButtonsViewProvider

#pragma mark -
#pragma mark Properties

@synthesize auxView = auxView_;

#pragma mark -
#pragma mark Static attributes

/**
 * Pop button view provier singleton only instance
 */
static PopButtonsViewProvider *popButtonsViewProviderInstance_ = nil;

#pragma mark -
#pragma mark Singleton methods

/**
 * The allocation returns the singleton only instance
 */
+ (id)allocWithZone:(NSZone *)zone {
    
    @synchronized([PopButtonsViewProvider class]) {
        
        if (popButtonsViewProviderInstance_ == nil) {
            
            popButtonsViewProviderInstance_ = [super allocWithZone:zone];
            return popButtonsViewProviderInstance_;
            
        }
        
    }
    
    return nil;
    
}

/*
 * Returns the singleton only instance
 */
+ (PopButtonsViewProvider *)getInstance {
    
    if (popButtonsViewProviderInstance_ == nil) {
        
        @synchronized([PopButtonsViewProvider class]) {
            
            if (popButtonsViewProviderInstance_ == nil) {
                
                popButtonsViewProviderInstance_ = [[PopButtonsViewProvider alloc] init];
                
            }
            
        }
        
    }
    
    return popButtonsViewProviderInstance_;
    
}

#pragma mark -
#pragma mark PopButtonsView creation

/*
 * Creates and returns aa autoreleased PopButtonsView constructed from a NIB file
 */
- (PopButtonsView *)popButtonsView {
    
    [[NSBundle mainBundle] loadNibNamed:NIB_FILE_NAME owner:self options:nil];
    
    PopButtonsView *result = auxView_;
    auxView_ = nil;
    
    return result;
    
}

@end


#pragma mark -

@implementation PopButtonsView

#pragma mark -
#pragma mark Properties

@synthesize backgroundImage = backgroundImage_;
@synthesize previousButton = previousButton_;
@synthesize nextButton = nextButton_;
@synthesize showAvailableTextsButton = showAvailableTextsButton_;
@synthesize okButton = okButton_;
@synthesize delegate = delegate_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [backgroundImage_ release];
    backgroundImage_ = nil;
    
    [previousButton_ release];
    previousButton_ = nil;
    
    [nextButton_ release];
    nextButton_ = nil;
    
    [showAvailableTextsButton_ release];
    showAvailableTextsButton_ = nil;
    
    [okButton_ release];
    okButton_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Invoked by framework when view awakes from a NIB file. Buttons background images are fixed to the correct stretchable versions
 */
- (void)awakeFromNib {
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    [imagesCache setStretchableLeftCapWidth:5 andTopCapHeight:0 forImageName:PREVIOUS_BUTTON_BACKGROUND_IMAGE_FILE];
    [imagesCache setStretchableLeftCapWidth:5 andTopCapHeight:0 forImageName:NEXT_BUTTON_BACKGROUND_IMAGE_FILE];
    [imagesCache setStretchableLeftCapWidth:5 andTopCapHeight:0 forImageName:SHOW_AVAILABLE_TEXTS_BUTTON_BACKGROUND_IMAGE_FILE];

    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        UIImage *background = [imagesCache imageNamed:POP_BUTTONS_VIEW_BACKGROUND_IMAGE];
        backgroundImage_.image = background;
        
        background = [imagesCache imageNamed:PREVIOUS_BUTTON_BACKGROUND_IMAGE_FILE];
        [NXT_Peru_iPhoneStyler styleButton:previousButton_ withWhiteFontSize:BUTTONS_TITTLE_SIZE background:background];
        
        background = [imagesCache  imageNamed:NEXT_BUTTON_BACKGROUND_IMAGE_FILE];
        [NXT_Peru_iPhoneStyler styleButton:nextButton_ withWhiteFontSize:BUTTONS_TITTLE_SIZE background:background];
        
        background = [imagesCache  imageNamed:SHOW_AVAILABLE_TEXTS_BUTTON_BACKGROUND_IMAGE_FILE];
        [NXT_Peru_iPhoneStyler styleButton:showAvailableTextsButton_ withWhiteFontSize:BUTTONS_TITTLE_SIZE background:background];
        
        background = [imagesCache imageNamed:BLUE_BUTTON_BACKGROUND_IMAGE_FILE_NAME];
        [NXT_Peru_iPhoneStyler styleButton:okButton_ withWhiteFontSize:BUTTONS_TITTLE_SIZE background:background];

        
    }else {
        backgroundImage_.backgroundColor = [UIColor BBVAGreyToneFiveColor];
        previousButton_.layer.borderWidth=1.0f;
        previousButton_.layer.borderColor=[UIColor BBVABlueSpectrumColor].CGColor;
        previousButton_.layer.cornerRadius=2.5f;
        nextButton_.layer.borderWidth=1.0f;
        nextButton_.layer.borderColor=[UIColor BBVABlueSpectrumColor].CGColor;
        nextButton_.layer.cornerRadius=2.5f;

        [NXT_Peru_iPhoneStyler styleButton:showAvailableTextsButton_ withBoldFont:YES fontSize:BUTTONS_TITTLE_SIZE color:[UIColor BBVABlueSpectrumColor] disabledColor:[UIColor BBVAGreyToneThreeColor] andShadowColor:[UIColor colorWithWhite:0.5f alpha:0.3f]];
        [NXT_Peru_iPhoneStyler styleButton:okButton_ withBoldFont:YES fontSize:BUTTONS_TITTLE_SIZE color:[UIColor BBVABlueSpectrumColor] disabledColor:[UIColor BBVAGreyToneThreeColor] andShadowColor:[UIColor colorWithWhite:0.5f alpha:0.3f]];
        [NXT_Peru_iPhoneStyler styleButton:previousButton_ withBoldFont:NO fontSize:BUTTONS_TITTLE_SIZE color:[UIColor BBVABlueSpectrumColor] disabledColor:[UIColor BBVAGreyToneThreeColor] andShadowColor:[UIColor colorWithWhite:0.5f alpha:0.3f]];
        [NXT_Peru_iPhoneStyler styleButton:nextButton_ withBoldFont:NO fontSize:BUTTONS_TITTLE_SIZE color:[UIColor BBVABlueSpectrumColor] disabledColor:[UIColor BBVAGreyToneThreeColor] andShadowColor:[UIColor colorWithWhite:0.5f alpha:0.3f]];
       
    }
    
    
    NSString* auxString = NSLocalizedString(POP_BUTTONS_PREVIOUS_KEY, nil);
    [previousButton_ setTitle:auxString forState:UIControlStateNormal];
    
    auxString = NSLocalizedString(POP_BUTTONS_NEXT_KEY, nil);
    [nextButton_ setTitle:auxString forState:UIControlStateNormal];
    
    auxString = NSLocalizedString(POP_BUTTONS_LIST_KEY, nil);
    [showAvailableTextsButton_ setTitle:auxString forState:UIControlStateNormal];
    
    auxString = NSLocalizedString(POP_BUTTONS_OK_KEY, nil);
    [okButton_ setTitle:auxString forState:UIControlStateNormal];
    
    enableEventForwarding_ = YES;
}

/*
 * Creates and returns an autoreleased PopButtonsView constructed from a NIB file
 */
+ (PopButtonsView *)popButtonsView {
    
    return [[PopButtonsViewProvider getInstance] popButtonsView];
    
}

#pragma mark -
#pragma mark Buttons enabling and disabling

/*
 * Enables or disables the next responder button, depending on the provided parameter
 */
- (void)enableNextResponderButton:(BOOL)anEnableFlag {
    
    nextButton_.enabled = anEnableFlag;
    
}

/*
 * Enables or disables the OK button, depending on the provided parameter
 */
- (void)enableOKResponderButton:(BOOL)anEnableFlag {
    
    okButton_.enabled = anEnableFlag;
    
}

/*
 * Enables or disables the previous responder button, depending on the provided parameter
 */
- (void)enablePreviousResponderButton:(BOOL)anEnableFlag {
    
    previousButton_.enabled = anEnableFlag;
    
}

/*
 * Shows or hides the previous responder and next respoder buttons depending on the provided parameter
 */
- (void)showPreviousAndNextResponderButtons:(BOOL)aShowFlag {
    
    previousButton_.hidden = !aShowFlag;
    nextButton_.hidden = !aShowFlag;
    
}

/*
 * Shows or hides the show available texts button depending on the provided parameter
 */
- (void)showShowAvailableTextsButton:(BOOL)aShowFlag {

    if (aShowFlag) {
        showAvailableTextsButton_.hidden = NO;
    } else {
        showAvailableTextsButton_.hidden = YES;
    }


}

#pragma mark -
#pragma mark Buttons clicking

/*
 * Invoked by framework when next responder button is clicked. Delegate is notified that next responder button was clicked
 */
- (IBAction)nextButtonClicked:(UIButton *)aSender {
    
    if ((enableEventForwarding_ == YES) &&
        ([(NSObject *)delegate_ respondsToSelector:@selector(nextButtonClickedInPopButtonsView:)] == YES)) {
        
        [delegate_ nextButtonClickedInPopButtonsView:self];
        
    }
    
}

/*
 * Invoked by framework when Ok button is clicked. Delegate is notified that OK button was clicked
 */
- (IBAction)okButtonClicked:(UIButton *)aSender {
    
    if ((enableEventForwarding_ == YES) &&
        ([(NSObject *)delegate_ respondsToSelector:@selector(okButtonClickedInPopButtonsView:)] == YES)) {
        
        [delegate_ okButtonClickedInPopButtonsView:self];
        
    }
    
}

/*
 * Invoked by framework when previous responder button is clicked. Delegate is notified that previous responder button was clicked
 */
- (IBAction)previousButtonClicked:(UIButton *)aSender {
    
    if ((enableEventForwarding_ == YES) &&
        ([(NSObject *)delegate_ respondsToSelector:@selector(previousButtonClickedInPopButtonsView:)] == YES)) {
        
        [delegate_ previousButtonClickedInPopButtonsView:self];
        
    }
    
}

/*
 * Invoked by framework when show available texts button is clicked. Delegate is notified that
 * show available texts buttons was clicked
 */
- (IBAction)showAvailableTextButtonClicked:(UIButton *)aSender {
    
    if ((enableEventForwarding_ == YES) &&
        ([(NSObject *)delegate_ respondsToSelector:@selector(showAvailableTextsButtonClickedInPopButtonsView:)] == YES)) {
        
        [delegate_ showAvailableTextsButtonClickedInPopButtonsView:self];
        
    }
    
}

#pragma mark -
#pragma mark Predictive text management

/*
 * Shows the "Lista" text or the "Teclado" text in available texts button depending on the
 * flat provided
 */
- (void)showListText:(BOOL)aShowListText {
    
    NSString *auxString = nil;
    
    if (aShowListText == YES) {
        auxString = NSLocalizedString(POP_BUTTONS_LIST_KEY, nil);
    } else {
        auxString = NSLocalizedString(POP_BUTTONS_KEYBOARD_KEY, nil);
    }
    
    [showAvailableTextsButton_ setTitle: auxString forState: UIControlStateNormal];
    
}

#pragma mark -
#pragma mark Event forwarding management

/*
 * Enables and disables button events fowarding to delegate
 */
- (void)enableEventForwarding:(BOOL)anEnabledState {
    
    enableEventForwarding_ = anEnabledState;
    
}

@end
