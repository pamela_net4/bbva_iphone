/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "UIBBVANavigationBar.h"


#pragma mark -

@implementation UIBBVANavigationBar

#pragma mark -
#pragma mark Instance initialization

/**
 * Object has been initialized from a NIB file. Navigation bar tint is set
 */
- (void) awakeFromNib {
    
    self.tintColor = [UIColor colorWithRed:0.23529f green:0.35294 blue:0.50980 alpha:1.0f];
    
}
                      
@end
