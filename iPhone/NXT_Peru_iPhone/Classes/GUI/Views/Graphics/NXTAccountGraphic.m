/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTAccountGraphic.h"
#import "NXTAccountGraphicInformation.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTGraphicsCommon.h"
#import "Tools.h"


/**
 * Defines the nominal graphic width
 */
#define NOMINAL_GRAPHIC_WIDTH                                                               480.0f

/**
 * Defines the nominal graphic height
 */
#define NOMINAL_GRAPHIC_HEIGHT                                                              320.0f

/**
 * Defines the nominal balance call-out rectangle width
 */
#define NOMINAL_BALANCE_CALLOUT_RECTANGLE_WIDTH                                             137.0f

/**
 * Defines the nominal balance call-out rectangle height
 */
#define NOMINAL_BALANCE_CALLOUT_RECTANGLE_HEIGHT                                            65.0f

/**
 * Defines the nominal balalance down arrow height
 */
#define NOMINAL_BALANCE_CALLOUT_DOWN_ARROW_HEIGHT                                           7.0f

/**
 * Defines the nominal balance call-out height
 */
#define NOMINAL_BALANCE_CALLOUT_HEIGHT                                                      (NOMINAL_BALANCE_CALLOUT_DOWN_ARROW_HEIGHT + NOMINAL_BALANCE_CALLOUT_RECTANGLE_HEIGHT)

/**
 * Defines the nominal balance call-out border width
 */
#define NOMINAL_BALANCE_CALLOUT_BORDER_WIDTH                                                2.0f

/**
 * Defines the nominal balance call-out shadow offset (used both as vertical and horizontal offset
 */
#define NOMINAL_BALANCE_CALLOUT_SHADOW_OFFSET                                               2.0f

/**
 * Defines the nominal balance call-out corner rounding radius
 */
#define NOMINAL_BALANCE_CALLOUT_ROUNDING_RADIUS                                             5.0f

/**
 * Defines the nominal balance call-out rectangle width
 */
#define NOMINAL_TRANSACTIONS_CALLOUT_RECTANGLE_WIDTH                                        137.0f

/**
 * Defines the nominal balance call-out rectangle height
 */
#define NOMINAL_TRANSACTIONS_CALLOUT_RECTANGLE_HEIGHT                                       65.0f

/**
 * Defines the nominal balalance down arrow height
 */
#define NOMINAL_TRANSACTIONS_CALLOUT_DOWN_ARROW_HEIGHT                                      7.0f

/**
 * Defines the nominal balance call-out height
 */
#define NOMINAL_TRANSACTIONS_CALLOUT_HEIGHT                                                 (NOMINAL_TRANSACTIONS_CALLOUT_DOWN_ARROW_HEIGHT + NOMINAL_TRANSACTIONS_CALLOUT_RECTANGLE_HEIGHT)

/**
 * Defines the nominal balance call-out border width
 */
#define NOMINAL_TRANSACTIONS_CALLOUT_BORDER_WIDTH                                           2.0f

/**
 * Defines the nominal balance call-out shadow offset (used both as vertical and horizontal offset
 */
#define NOMINAL_TRANSACTIONS_CALLOUT_SHADOW_OFFSET                                          2.0f

/**
 * Defines the nominal transactions call-out corner rounding radius
 */
#define NOMINAL_TRANSACTIONS_CALLOUT_ROUNDING_RADIUS                                        5.0f

/**
 * Defines the nominal transactions call-out white arrow mid height
 */
#define NOMINAL_TRANSACTIONS_CALLOUT_RIGHT_ARROW_HALF_HEIGHT                                5.0f

/**
 * Defines the nominal transactions call-out white arrow width
 */
#define NOMINAL_TRANSACTIONS_CALLOUT_RIGHT_ARROW_WIDTH                                      5.0f

/**
 * Defines the nominal transactions call-out white arrow line width
 */
#define NOMINAL_TRANSACTIONS_CALLOUT_RIGHT_ARROW_LINE_WIDTH                                 3.0f

/**
 * Defines the nominal distance between the amount and the currency inside the call-out
 */
#define NOMINAL_CALLOUT_AMOUNT_TO_CURRENCY_GAP                                              2.0f

/**
 * Defines the nominal call-out amount text size
 */
#define NOMINAL_CALLOUT_AMOUNT_TEXT_SIZE                                                    23.0f

/**
 * Defines the nominal call-out currency text size
 */
#define NOMINAL_CALLOUT_CURRENCY_TEXT_SIZE                                                  15.0f

/**
 * Defines the nominal call-out date text size
 */
#define NOMINAL_CALLOUT_DATE_TEXT_SIZE                                                      10.0f

/**
 * Defines the nominal call-out offsets
 */
#define NOMINAL_CALLOUT_OFFSETS                                                             15.0f

/**
 * Defines the nominal call-out first line base line
 */
#define NOMINAL_CALLOUT_FIRST_LINE_BASE_LINE                                                36.0f

/**
 * Defines the nominal call-out second line base line
 */
#define NOMINAL_CALLOUT_SECOND_LINE_BASE_LINE                                               50.0f

/**
 * Defines the maximum height for the transactions grand total
 */
#define MAX_TRANSACTIONS_GRAND_TOTAL_HEIGHT                                                 40.0f

/**
 * Defines the top and bottom padding (no element can be within that distance to the top or bottom borders)
 */
#define TOP_AND_BOTTOM_PADDING                                                              10.0f

/**
 * Defines the balance day circle radius
 */
#define BALANCE_DAY_CIRCLE_RADIUS                                                           6.0f

/**
 * Defines the nominal day text horizontal offset from any limit (day left or right border, view left or right border)
 */
#define NOMINAL_DAY_TEXT_HORIONTAL_OFFSET                                                   5.0f

/**
 * Defines the nominal day text vertical offset from the view top
 */
#define NOMINAL_DAY_TEXT_VERTICAL_OFFSET                                                    5.0f

/**
 * Day text nominal size
 */
#define NOMINAL_DAY_TEXT_SIZE                                                               10.0f

/**
 * Amount text nominal size
 */
#define NOMINAL_AMOUNT_TEXT_SIZE                                                            10.0f

/**
 * Amount border nominal horizontal offset from the right limit
 */
#define NOMINAL_AMOUNT_BORDER_HORIZONTAL_OFFSET                                             3.0f

/**
 * Amount text border nominal offset from associated line
 */
#define NOMINAL_AMOUNT_BORDER_VERTICAL_OFFSET                                               5.0f

/**
 * Amount text border nominal height
 */
#define NOMINAL_AMOUNT_BORDER_HEIGHT                                                        12.0f

/**
 * Day circle nominal diameter
 */
#define NOMINAL_DAY_CIRCLE_DIAMETER                                                         13.0f

/**
 * Defines the day tap detection semiwidth
 */
#define DAY_TAP_DETECTION_SEMIWIDTH                                                         20.0f


/**
 * Defines the grid dash line color
 */
#define GRID_DASH_LINE_COLOR(aColorAlpha)                                                   [UIColor colorWithWhite:0.6431f alpha:(aColorAlpha)]

/**
 * Defines the grid not main lines color
 */
#define GRID_NOT_MAIN_LINES_COLOR(aColorAlpha)                                              [UIColor colorWithWhite:0.9176f alpha:(aColorAlpha)]

/**
 * Defines the grid day start color
 */
#define GRID_DAY_START_COLOR                                                                [UIColor colorWithWhite:0.8275f alpha:1.0f]

/**
 * Defines the date text color
 */
#define DATE_TEXT_COLOR                                                                     [UIColor colorWithWhite:0.6196f alpha:1.0f]

/**
 * Defines the amount text color
 */
#define AMOUNT_TEXT_COLOR                                                                   [UIColor colorWithWhite:0.8941f alpha:1.0f]

/**
 * Defines the amount text border color
 */
#define AMOUNT_TEXT_BORDER_COLOR                                                            [UIColor colorWithWhite:0.6431f alpha:1.0f]

/**
 * Defines the graphic gradient top color as an RGBA array
 */
#define GRAPHIC_GRADIENT_TOP_COLOR_ARRAY                                                    0.65f, 0.65f, 0.65f, 0.75f

/**
 * Defines the graphic gradient bottom color as an RGBA array
 */
#define GRAPHIC_GRADIENT_BOTTOM_COLOR_ARRAY                                                 0.9137f, 0.9137f, 0.9137f, 1.0f

/**
 * Defines the day circle fill color
 */
#define DAY_CIRCLE_FILL_COLOR                                                               [UIColor whiteColor]

/**
 * Defines the day selected circle fill color
 */
#define DAY_SELECTED_CIRCLE_FILL_COLOR                                                      [UIColor colorWithRed:0.0039f green:0.2549f blue:0.5451 alpha:1.0f]

/**
 * Defines the day selected circle line color
 */
#define DAY_SELECTED_CIRCLE_LINE_COLOR                                                      [UIColor whiteColor]

/**
 * Defines the graphic lines color (both graphic line and day circle border)
 */
#define GRAPHIC_LINES_COLOR                                                                 [UIColor colorWithWhite:0.6196f alpha:1.0f]

/**
 * Defines the positive transactions color
 */
#define POSITIVE_TRANSACTIONS_COLOR                                                         [UIColor whiteColor]

/**
 * Defines the negative transactions color
 */
#define NEGATIVE_TRANSACTIONS_COLOR                                                         [UIColor colorWithWhite:0.6196f alpha:1.0f]

/**
 * Defines the selected transactions color
 */
#define SELECTED_TRANSACTIONS_COLOR                                                         [UIColor colorWithRed:0.1294f green:0.4784f blue:0.7608f alpha:1.0f]

/**
 * Defines the callout top border color as an array
 */
#define CALLOUT_TOP_BORDER_COLOR_ARRAY                                                      0.2235f, 0.5333f, 0.7843f, 1.0f

/**
 * Defines the callout bottom border color as an array
 */
#define CALLOUT_BOTTOM_BORDER_COLOR_ARRAY                                                   0.0157f, 0.2902f, 0.5961f, 1.0f

/**
 * Defines the callout border color
 */
#define CALLOUT_BORDER_COLOR                                                                [UIColor whiteColor]

/**
 * Defines the callout amount text color
 */
#define CALLOUT_AMOUNT_TEXT_COLOR                                                           [UIColor whiteColor]

/**
 * Defines the callout date text color
 */
#define CALLOUT_DATE_TEXT_COLOR                                                             [UIColor colorWithRed:0.8157f green:0.8863f blue:0.9647f alpha:1.0f]

/**
 * Defines the callout shadow color
 */
#define CALLOUT_SHADOW_COLOR                                                                [UIColor colorWithWhite:0.5f alpha:1.0f]


/**
 * Defines the activity indicator horizontal distance to the left border
 */
#define ACTIVITY_INDICATOR_DISTANCE_TO_LEFT_BORDER                                          50.0f

/**
 * Left scroll needed to notify the delegate to download more information. It is measured as points from the left border
 */
#define LEFT_SCROLL_TO_NOTIFY_DOWNLOAD                                                      -100.0f


#pragma mark -

/**
 * View to detect touches and forward them to the NXTAcountGraphic
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountGraphicTouchDetectorView: UIView {
    
@private
    
    /**
     * Associated NXTAccountGraphic
     */
    NXTAccountGraphic *accountGraphic_;
    
}

/**
 * Provides read-write access to the associated NXTAccountGraphic
 */
@property (nonatomic, readwrite, assign) NXTAccountGraphic *accountGraphic;

@end


#pragma mark -

/**
 * NXTAccountGraphic private category
 */
@interface NXTAccountGraphic(private)

/**
 * Initializes the instance information
 */
- (void)initializeElements;

/**
 * Caculates the graphic layout from the stored information
 */
- (void)calculateGraphicLayout;

/**
 * Calculates the multiplying factors
 */
- (void)calculateMultiplyingFactors;

/**
 * Obtains and stores the information relevant valules
 */
- (void)analyzeAccountInformationToDisplay;

/**
 * Calculates the graphic parameters (anchor points positions, and widths)
 */
- (void)calculateGraphicParameters;

/**
 * Sets the scroll parameters, once they are calculated
 */
- (void)setScrollParameters;

/**
 * Sets the tap view frame
 */
- (void)setTapViewFrame;

/**
 * Draws the grid into the given context
 *
 * @param aContext The context to draw the grid into
 * @param aFirstDayXPosition The first day X position (measured from the day left most position to the view left border)
 * @param aControlWidth The control width
 * @param aControlHeight The control height
 */
- (void)drawGridIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
               controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight;

/**
 * Draws the first and last dates displayed into the given context
 *
 * @param aContext The context to draw the date texts into
 * @param aFirstDayXPosition The first day X position (measured from the day left most position to the view left border)
 * @param aControlWidth The control width
 * @param aControlHeight The control height
 */
- (void)drawDaysIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
               controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight;

/**
 * Draws the amounts on the graphic right hand side
 *
 * @param aContext The context to draw the amounts into
 * @param aFirstDayXPosition The first day X position (measured from the day left most position to the view left border)
 * @param aControlWidth The control width
 * @param aControlHeight The control height
 */
- (void)drawAmountsIntoContext:(CGContextRef)aContext
         withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                  controlWidth:(CGFloat)aControlWidth
                 controlHeight:(CGFloat)aControlHeight;

/**
 * Draws the balance graphic background into the provided context
 *
 * @param aContext The context to draw the graphic into
 * @param aFirstDayXPosition The first day X position (measured from the day left most position to the view left border)
 * @param aControlWidth The control width
 * @param aControlHeight The control height
 */
- (void)drawBalanceBackgroundIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                            controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight;

/**
 * Draws the balance graphic lines into the provided context
 *
 * @param aContext The context to draw the graphic into
 * @param aFirstDayXPosition The first day X position (measured from the day left most position to the view left border)
 * @param aControlWidth The control width
 * @param aControlHeight The control height
 */
- (void)drawBalanceLinesIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                       controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight;

/**
 * Draws the graphic smoothing gradient into the provided context
 *
 * @param aContext The context to draw the graphic smoothing gradient callout into
 * @param aFirstDayXPosition The first day X position (measured from the day left most position to the view left border)
 * @param aControlWidth The control width
 * @param aControlHeight The control height
 */
- (void)drawGraphicSmoothingGradientIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                                   controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight;

/**
 * Draws the transactions bars into the provided context
 *
 * @param aContext The context to draw the transactions into
 * @param aFirstDayXPosition The first day X position (measured from the day left most position to the view left border)
 * @param aControlWidth The control width
 * @param aControlHeight The control height
 */
- (void)drawTransactionsIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                       controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight;

/**
 * Draws the transaction callout into the provided context
 *
 * @param aContext The context to draw the transaction callout into
 * @param aFirstDayXPosition The first day X position (measured from the day left most position to the view left border)
 * @param aControlWidth The control width
 * @param aControlHeight The control height
 */
- (void)drawTransactionCallOutIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                              controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight;

/**
 * Draws the selected day callout into the provided context
 *
 * @param aContext The context to draw the selected day callout into
 * @param aFirstDayXPosition The first day X position (measured from the day left most position to the view left border)
 * @param aControlWidth The control width
 * @param aControlHeight The control height
 */
- (void)drawSelectedDayCallOutIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                             controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight;

/**
 * Draws a callout texts into the provided context. It assumes the context is translated to the callout top left corner
 *
 * @param aContext The context to draw the transaction callout into
 * @param anAmount The amount to display in the first line, together with the currency symbol
 * @param aDateOffset The day offset from the information base date to display in the second line
 * @param anAmountAvaliableWidth The available width to display the amount and currency symbols
 */
- (void)drawCalloutStringsIntoContext:(CGContextRef)aContext
                           withAmount:(NSDecimalNumber *)anAmount
                           dateOffset:(NSInteger)aDateOffset
                 amountAvailableWidth:(CGFloat)anAmountAvaliableWidth;

/**
 * Creates the grid vertical line image
 */
- (void)createGridVerticalLineImage;

/**
 * Creates the amount strings information
 */
- (void)createAmountStringsInformation;

/**
 * Creates the graphic gradient
 */
- (void)createGraphicGradient;

/**
 * Creates the graphic limits smoothing gradient
 */
- (void)createGraphicLimitsSmoothingGradient;

/**
 * Creates the callouts gradient
 */
- (void)createCalloutGradient;

/**
 * Creates the graphic paths (both the line path and the fill path)
 *
 * @param aFirstDayXPosition The first day X position (measured from the day left most position to the view left border)
 * @param aControlWidth The control width
 * @param aControlHeight The control height
 */
- (void)createGraphicPathsWithFirstDayXPosition:(CGFloat)aFirstDayXPosition
                                   controlWidth:(CGFloat)aControlWidth
                                  controlHeight:(CGFloat)aControlHeight;

/**
 * Creates the transactions callout path
 */
- (void)createTransactionsCalloutPath;

/**
 * Returns the graphic date string for the day at the given lapse from the reference date
 *
 * @param aDayLapse The day lapse from the referecen date (measured in days)
 * @param aReferenceDate The reference date (represented as time interval from Jan, 1 1970, measured in seconds)
 * @return The new graphic date string calculated
 */
- (NSString *)graphicStringForDateAtDays:(NSInteger)aDayLapse fromReference:(NSTimeInterval)aReferenceDate;

/**
 * Returns the callout date string for the day at the given lapse form the reference date
 *
 * @param aDayLapse The day lapse from the referecen date (measured in days)
 * @param aReferenceDate The reference date (represented as time interval from Jan, 1 1970, measured in seconds)
 * @return The new callout date string calculated
 */
- (NSString *)calloutStringForDateAtDays:(NSInteger)aDayLapse fromReference:(NSTimeInterval)aReferenceDate;

/**
 * The view has detected that it has been tapped inside
 *
 * @param aPoint The point where the graphic was tapped
 */
- (void)viewTappedAtPoint:(CGPoint)aPoint;

/**
 * Checks whether the view as tapped on the transactions callout
 *
 * @param aTapPoint The poit where the view was tapped
 * @return YES when the view was tapped on the transactions callout, NO otherwise
 */
- (BOOL)checkViewTappedOnTransactionsCalloutAtPoint:(CGPoint)aTapPoint;

/**
 * Checks whether view was tapped on a day balance circle
 *
 * @param aTapPoint The poit where the view was tapped
 * @param aFirstDayXPosition The first day X position (measured from the day left most position to the view left border)
 * @param aControlWidth The control width
 * @return The day where the view was tapped, or -1 if no day circle was tapped
 */
- (NSInteger)checkViewTappedOnDayBalanceCircleAtPoint:(CGPoint)aTapPoint withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                                         controlWidth:(CGFloat)aControlWidth;

/**
 * Checks whether the view as tapped on the transactions band
 *
 * @param aTapPoint The poit where the view was tapped
 * @return YES when the view was tapped on the transactions band, NO otherwise
 */
- (BOOL)checkViewTappedOnTransactionsBandAtPoint:(CGPoint)aTapPoint;

/**
 * Returns the central day as an NSDate. If the date is out of range, it returns nil
 *
 * @return The central day as an NSDate
 */
- (NSDate *)centralDayAsNSDate;

@end


#pragma mark -

@implementation AccountGraphicTouchDetectorView

#pragma mark -
#pragma mark Properties

@synthesize accountGraphic = accountGraphic_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    accountGraphic_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. It stablishes the view background color to transparent
 *
 * @param frame The view original frame
 * @return The initilized AccountGraphicTouchDetectorView instance
 */
- (id)initWithFrame:(CGRect)frame {

    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.0f];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Manages the touches events

/**
 * Tells the receiver when one or more fingers are raised from a view or window. When a single tap is detected, account graphic is notified
 *
 * @param touches A set of UITouch instances that represent the touches for the ending phase of the event represented by event
 * @param event An object representing the event to which the touches belong
 */
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {

    //DLog(@"touch ends");
    
    if (accountGraphic_ != nil) {
            
        if ([touches count] == 1) {
            
            UITouch *touch = [touches anyObject];
            
            if (touch.tapCount == 1) {
                
                CGPoint viewPoint = [touch locationInView:accountGraphic_];
                
                [accountGraphic_ viewTappedAtPoint:viewPoint];
                
            }
            
            
        }
    
    }
    
}

@end


#pragma mark -

@implementation NXTAccountGraphic

#pragma mark -
#pragma mark Properties

@synthesize delegate = delegate_;
@synthesize selectedDay = selectedDay_;
@synthesize transactionsCalloutVisible = transactionsCalloutVisible_;
@synthesize canRotate = canRotate_;
@dynamic firstDayDisplayed;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [accountGraphicInformation_ release];
    accountGraphicInformation_ = nil;
    
    [currencySymbol_ release];
    currencySymbol_ = nil;
    
    [scrollView_ release];
    scrollView_ = nil;
    
    [touchDetectorView_ release];
    touchDetectorView_ = nil;
    
    [downloadDataView_ release];
    downloadDataView_ = nil;
    
    delegate_ = nil;
    
    [gridVerticalDayLineImage_ release];
    gridVerticalDayLineImage_ = nil;
    
    [gridVerticalSubdayLineImage_ release];
    gridVerticalSubdayLineImage_ = nil;
    
    [calendar_ release];
    calendar_ = nil;
    
    [monthsStrings_ release];
    monthsStrings_ = nil;
    
    [amountStringsInformationArray_ release];
    amountStringsInformationArray_ = nil;
    
    [amountTextFont_ release];
    amountTextFont_ = nil;
    
    CGPathRelease(amountBorderPath_);
    amountBorderPath_ = NULL;
    
    CGGradientRelease(graphicGradient_);
    graphicGradient_ = NULL;
    
    CGGradientRelease(graphicLimitsSmoothGradient_);
    graphicLimitsSmoothGradient_ = NULL;
    
    CGPathRelease(graphicLinePath_);
    graphicLinePath_ = nil;
    
    CGPathRelease(graphicFillPath_);
    graphicFillPath_ = nil;
    
    CGGradientRelease(calloutGradient_);
    calloutGradient_ = NULL;
    
    CGPathRelease(transactionsCalloutPath_);
    transactionsCalloutPath_ = NULL;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file. Set the graphic style
 */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    CGRect boundsScreen = [[UIScreen mainScreen] bounds];
    
    [self setFrame:boundsScreen];
    
    [self initializeElements];
    
}

/**
 * Designated initializer. It applies the graphic style
 *
 * @param frame The view initial frame
 * @return The initialized NXTAccountGraphic instance
 */
- (id)initWithFrame:(CGRect)frame {

    if (self = [super initWithFrame:frame]) {
        
        [self initializeElements];
        
    }
    
    return self;
    
}

/*
 * Initializes the instance information
 */
- (void)initializeElements {
    
    [amountStringsInformationArray_ release];
    amountStringsInformationArray_ = [[NSMutableArray alloc] init];
    
    selectedDay_ = -1;
    
    [NXT_Peru_iPhoneStyler styleAccountGraphic:self];
    CGRect frame = self.frame;
    frame.origin.x = 0.0f;
    frame.origin.y = 0.0f;
    
    [scrollView_ release];
    scrollView_ = [[UIScrollView alloc] initWithFrame:frame];
    scrollView_.delegate = self;
    scrollView_.showsVerticalScrollIndicator = NO;
    scrollView_.showsHorizontalScrollIndicator = NO;
    scrollView_.backgroundColor = [UIColor clearColor];
    [self addSubview:scrollView_];
    
    [touchDetectorView_ release];
    touchDetectorView_ = [[AccountGraphicTouchDetectorView alloc] initWithFrame:frame];
    touchDetectorView_.accountGraphic = self;
    [scrollView_ addSubview:touchDetectorView_];
    
    [downloadDataView_ release];
    downloadDataView_ = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    [self createGraphicGradient];
    [self createCalloutGradient];
     
    [self calculateMultiplyingFactors];
    [self calculateGraphicParameters];
    [self createTransactionsCalloutPath];
	
	canNotifyDownload_ = YES;
    
}

#pragma mark -
#pragma mark Drawing the graphic

/**
 * Draws the graphic in the view frame
 *
 * @param rect A rectangle defining the area to restrict drawing to
 */
- (void)drawRect:(CGRect)rect {
    
#ifdef DEBUG
    static NSUInteger counter = 0;

    static NSTimeInterval step1Time = 0;
    static NSTimeInterval step2Time = 0;
    static NSTimeInterval step3Time = 0;
    static NSTimeInterval step4Time = 0;
    static NSTimeInterval step5Time = 0;
    static NSTimeInterval step6Time = 0;
    static NSTimeInterval step7Time = 0;
    static NSTimeInterval step8Time = 0;
#endif //DEBUG
    
    CGPoint contentOffset = scrollView_.contentOffset;
    CGRect frame = self.frame;
    CGFloat width = CGRectGetMaxX(frame) - CGRectGetMinX(frame);
    CGFloat height = CGRectGetMaxY(frame) - CGRectGetMinY(frame);
    CGFloat firstDayXPosition = ((width - dayWidth_) / 2.0f) - contentOffset.x;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);

#ifdef DEBUG
    NSTimeInterval beforeTime = [NSDate timeIntervalSinceReferenceDate];
#endif //DEBUG
    
    [self drawGridIntoContext:context withFirstDayXPosition:firstDayXPosition controlWidth:width controlHeight:height];
    
#ifdef DEBUG
    NSTimeInterval afterTime = [NSDate timeIntervalSinceReferenceDate];
    step1Time += ((afterTime - beforeTime) * 1000);
    
    beforeTime = afterTime;
#endif //DEBUG
    
    [self drawDaysIntoContext:context withFirstDayXPosition:firstDayXPosition controlWidth:width controlHeight:height];
    
#ifdef DEBUG
    afterTime = [NSDate timeIntervalSinceReferenceDate];
    step2Time += ((afterTime - beforeTime) * 1000);

    beforeTime = afterTime;
#endif //DEBUG
    
    [self drawBalanceBackgroundIntoContext:context withFirstDayXPosition:firstDayXPosition controlWidth:width controlHeight:height];

#ifdef DEBUG
    afterTime = [NSDate timeIntervalSinceReferenceDate];
    step3Time += ((afterTime - beforeTime) * 1000);

    beforeTime = afterTime;
#endif //DEBUG
    
    [self drawTransactionsIntoContext:context withFirstDayXPosition:firstDayXPosition controlWidth:width controlHeight:height];
    
#ifdef DEBUG
    afterTime = [NSDate timeIntervalSinceReferenceDate];
    step4Time += ((afterTime - beforeTime) * 1000);

    beforeTime = afterTime;
#endif //DEBUG
    
    [self drawBalanceLinesIntoContext:context withFirstDayXPosition:firstDayXPosition controlWidth:width controlHeight:height];
    
#ifdef DEBUG
    afterTime = [NSDate timeIntervalSinceReferenceDate];
    step5Time += ((afterTime - beforeTime) * 1000);
    
    beforeTime = afterTime;
#endif //DEBUG
    
    [self drawAmountsIntoContext:context withFirstDayXPosition:firstDayXPosition controlWidth:width controlHeight:height];
    
#ifdef DEBUG
    afterTime = [NSDate timeIntervalSinceReferenceDate];
    step6Time += ((afterTime - beforeTime) * 1000);
    
    beforeTime = afterTime;
#endif //DEBUG
    
    [self drawSelectedDayCallOutIntoContext:context withFirstDayXPosition:firstDayXPosition controlWidth:width controlHeight:height];
    
#ifdef DEBUG
    afterTime = [NSDate timeIntervalSinceReferenceDate];
    step7Time += ((afterTime - beforeTime) * 1000);

    beforeTime = afterTime;
#endif //DEBUG
    
    [self drawTransactionCallOutIntoContext:context withFirstDayXPosition:firstDayXPosition controlWidth:width controlHeight:height];
    
#ifdef DEBUG
    afterTime = [NSDate timeIntervalSinceReferenceDate];
    step8Time += ((afterTime - beforeTime) * 1000);
    
    counter++;
    
    if (counter > 100) {
        counter = 0;
        //DLog(@"Elapsed time for step1 in ms: %f", step1Time);
        //DLog(@"Elapsed time for step2 in ms: %f", step2Time);
        //DLog(@"Elapsed time for step3 in ms: %f", step3Time);
        //DLog(@"Elapsed time for step4 in ms: %f", step4Time);
        //DLog(@"Elapsed time for step5 in ms: %f", step5Time);
        //DLog(@"Elapsed time for step6 in ms: %f", step6Time);
        //DLog(@"Elapsed time for step7 in ms: %f", step7Time);
        //DLog(@"Elapsed time for step8 in ms: %f", step8Time);

        step1Time = 0;
        step2Time = 0;
        step3Time = 0;
        step4Time = 0;
        step5Time = 0;
        step6Time = 0;
        step7Time = 0;
        step8Time = 0;
    }
#endif //DEBUG
    
    CGContextRestoreGState(context);
    
}

/*
 * Draws the grid into the given context
 */
- (void)drawGridIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
               controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight {
    
    CGContextSaveGState(aContext);
    
    CGFloat curDayXPos = aFirstDayXPosition;
    
    if (aFirstDayXPosition > 0.0f) {
		
        curDayXPos = aFirstDayXPosition + floor(-aFirstDayXPosition / dayWidth_) * dayWidth_;
        
    }
    
    CGFloat nextDayXPos = curDayXPos + dayWidth_;
    CGFloat interdayXDivision = dayWidth_ / 4.0f;
    CGFloat currentXPosition;
    
    if (nextDayXPos < 0.0f) {
        
        nextDayXPos = ((floor(-aFirstDayXPosition / dayWidth_) + 1.0f) * dayWidth_) + aFirstDayXPosition;
        curDayXPos = nextDayXPos - dayWidth_;
        
    }
    
    CGContextSetLineWidth(aContext, 1.0f);
    
    if (curDayXPos >= 0.0f) {
        
        CGContextDrawImage(aContext, CGRectMake(curDayXPos, 0.0f, 1.0f, aControlHeight), gridVerticalDayLineImage_.CGImage);
        
    }
	
    CGFloat curDayXPosBackup = curDayXPos;
    CGFloat nextDayXPosBackup = nextDayXPos;
	
	CGContextBeginPath(aContext);
	
    while (curDayXPos < aControlWidth) {
		
        currentXPosition = curDayXPos + interdayXDivision;
        
        CGContextDrawImage(aContext, CGRectMake(currentXPosition, 0.0f, 1.0f, aControlHeight), gridVerticalSubdayLineImage_.CGImage);
        
        currentXPosition += interdayXDivision;
        CGContextDrawImage(aContext, CGRectMake(currentXPosition, 0.0f, 1.0f, aControlHeight), gridVerticalSubdayLineImage_.CGImage);
        
        currentXPosition += interdayXDivision;
        CGContextDrawImage(aContext, CGRectMake(currentXPosition, 0.0f, 1.0f, aControlHeight), gridVerticalSubdayLineImage_.CGImage);
        
        curDayXPos = nextDayXPos;
        nextDayXPos += dayWidth_;
        
    }
	
	curDayXPos = curDayXPosBackup;
	nextDayXPos = nextDayXPosBackup;
	
	CGContextBeginPath(aContext);
	
    while (curDayXPos < aControlWidth) {
		
        CGContextDrawImage(aContext, CGRectMake(curDayXPos, 0.0f, 1.0f, aControlHeight), gridVerticalDayLineImage_.CGImage);
        
        curDayXPos = nextDayXPos;
        nextDayXPos +=dayWidth_;
        
    }
	
    CGFloat curAmount = 0.0f;
    CGFloat nextAmount = mainDivision_;
    CGFloat curYAmount = AMOUNT_TO_Y(curAmount, yAxisZeroPosition_, 0.0f, graphicScale_);
    CGFloat nextYAmount = AMOUNT_TO_Y(nextAmount, yAxisZeroPosition_, 0.0f, graphicScale_);
    CGFloat interAmountsYDivision = (nextYAmount - curYAmount) / 3.0f;
    CGFloat currentYPosition;
    CGFloat dashLine[2] = { 1.0f, 1.0f };
    
    CGContextBeginPath(aContext);
    CGContextMoveToPoint(aContext, 0.0f, yAxisZeroPosition_);
    CGContextAddLineToPoint(aContext, aControlWidth, yAxisZeroPosition_);
    CGContextSaveGState(aContext);
    CGContextSetLineDash(aContext, 0.0f, dashLine, 2);
    CGContextSetStrokeColorWithColor(aContext, GRID_DASH_LINE_COLOR(yAxisZeroPosition_ / aControlHeight).CGColor);
    CGContextStrokePath(aContext);
    CGContextRestoreGState(aContext);
	
	
    CGFloat nextAmountBackup = nextAmount;
	CGFloat curYAmountBackup = curYAmount;
    CGFloat nextYAmountBackup = nextYAmount;
	
    while (curYAmount > 0.0f) {
		
        currentYPosition = curYAmount + interAmountsYDivision;
		
        CGContextBeginPath(aContext);
        CGContextSetStrokeColorWithColor(aContext, GRID_NOT_MAIN_LINES_COLOR(ALPHA_FOR_PERCENTAJE_POSITION((currentYPosition / aControlHeight), maxGridGradientPercentage_)).CGColor);
        CGContextMoveToPoint(aContext, 0.0f, currentYPosition);
        CGContextAddLineToPoint(aContext, aControlWidth, currentYPosition);
        CGContextStrokePath(aContext);
        currentYPosition += interAmountsYDivision;
		
        CGContextBeginPath(aContext);
        CGContextSetStrokeColorWithColor(aContext, GRID_NOT_MAIN_LINES_COLOR(ALPHA_FOR_PERCENTAJE_POSITION((currentYPosition / aControlHeight), maxGridGradientPercentage_)).CGColor);
        CGContextMoveToPoint(aContext, 0.0f, currentYPosition);
        CGContextAddLineToPoint(aContext, aControlWidth, currentYPosition);
        CGContextStrokePath(aContext);
        
        CGContextBeginPath(aContext);
        CGContextSetStrokeColorWithColor(aContext, GRID_NOT_MAIN_LINES_COLOR(ALPHA_FOR_PERCENTAJE_POSITION((nextYAmount / aControlHeight), maxGridGradientPercentage_)).CGColor);
		CGContextMoveToPoint(aContext, 0.0f, nextYAmount);
        CGContextAddLineToPoint(aContext, aControlWidth, nextYAmount);
        CGContextStrokePath(aContext);
		
        curYAmount = nextYAmount;
        nextAmount += mainDivision_;
        nextYAmount = AMOUNT_TO_Y(nextAmount, yAxisZeroPosition_, 0.0f, graphicScale_);
		
    }
	
    nextAmount = nextAmountBackup;
	curYAmount = curYAmountBackup;
    nextYAmount = nextYAmountBackup;
	
	CGContextSaveGState(aContext);
	CGContextSetLineDash(aContext, 0.0f, dashLine, 2);

	while (curYAmount > 0.0f) {
        
        CGContextBeginPath(aContext);
        CGContextSetStrokeColorWithColor(aContext, GRID_DASH_LINE_COLOR(ALPHA_FOR_PERCENTAJE_POSITION((nextYAmount / aControlHeight), maxGridGradientPercentage_)).CGColor);
        CGContextMoveToPoint(aContext, 0.0f, nextYAmount);
        CGContextAddLineToPoint(aContext, aControlWidth, nextYAmount);
        CGContextStrokePath(aContext);
		
        curYAmount = nextYAmount;
        nextAmount += mainDivision_;
        nextYAmount = AMOUNT_TO_Y(nextAmount, yAxisZeroPosition_, 0.0f, graphicScale_);
        
    }
	
	CGContextRestoreGState(aContext);
	
    curAmount = 0.0f;
    nextAmount = -mainDivision_;
    curYAmount = AMOUNT_TO_Y(curAmount, yAxisZeroPosition_, 0.0f, graphicScale_);
    nextYAmount = AMOUNT_TO_Y(nextAmount, yAxisZeroPosition_, 0.0f, graphicScale_);
    
    nextAmountBackup = nextAmount;
	curYAmountBackup = curYAmount;
    nextYAmountBackup = nextYAmount;
	
    while (curYAmount <= aControlHeight) {
        
        currentYPosition = curYAmount - interAmountsYDivision;
        
        CGContextBeginPath(aContext);
        CGContextSetStrokeColorWithColor(aContext, GRID_NOT_MAIN_LINES_COLOR(ALPHA_FOR_PERCENTAJE_POSITION((currentYPosition / aControlHeight), maxGridGradientPercentage_)).CGColor);
        CGContextMoveToPoint(aContext, 0.0f, currentYPosition);
        CGContextAddLineToPoint(aContext, aControlWidth, currentYPosition);
        CGContextStrokePath(aContext);
        currentYPosition -= interAmountsYDivision;
    
        CGContextBeginPath(aContext);
        CGContextSetStrokeColorWithColor(aContext, GRID_NOT_MAIN_LINES_COLOR(ALPHA_FOR_PERCENTAJE_POSITION((currentYPosition / aControlHeight), maxGridGradientPercentage_)).CGColor);
		CGContextMoveToPoint(aContext, 0.0f, currentYPosition);
        CGContextAddLineToPoint(aContext, aControlWidth, currentYPosition);
        CGContextStrokePath(aContext);
        
        CGContextBeginPath(aContext);
        CGContextSetStrokeColorWithColor(aContext, GRID_NOT_MAIN_LINES_COLOR(ALPHA_FOR_PERCENTAJE_POSITION((nextYAmount / aControlHeight), maxGridGradientPercentage_)).CGColor);
		CGContextMoveToPoint(aContext, 0.0f, nextYAmount);
        CGContextAddLineToPoint(aContext, aControlWidth, nextYAmount);
        CGContextStrokePath(aContext);
        
        curYAmount = nextYAmount;
        nextAmount -= mainDivision_;
        nextYAmount = AMOUNT_TO_Y(nextAmount, yAxisZeroPosition_, 0.0f, graphicScale_);
        
    }
	
    nextAmount = nextAmountBackup;
	curYAmount = curYAmountBackup;
    nextYAmount = nextYAmountBackup;
	
	CGContextSaveGState(aContext);
	CGContextSetLineDash(aContext, 0.0f, dashLine, 2);
    
	while (curYAmount <= aControlHeight) {
        
        CGContextBeginPath(aContext);
        CGContextSetStrokeColorWithColor(aContext, GRID_DASH_LINE_COLOR(ALPHA_FOR_PERCENTAJE_POSITION((nextYAmount / aControlHeight), maxGridGradientPercentage_)).CGColor);
        CGContextMoveToPoint(aContext, 0.0f, nextYAmount);
        CGContextAddLineToPoint(aContext, aControlWidth, nextYAmount);
        CGContextStrokePath(aContext);
        
        curYAmount = nextYAmount;
        nextAmount -= mainDivision_;
        nextYAmount = AMOUNT_TO_Y(nextAmount, yAxisZeroPosition_, 0.0f, graphicScale_);
        
    }
    
	CGContextRestoreGState(aContext);

    CGContextRestoreGState(aContext);
    
}


/*
 * Draws the first and last days displayed into the given context
 */
- (void)drawDaysIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
               controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight {
    
    CGContextSaveGState(aContext);
    
    CGContextSetStrokeColorWithColor(aContext, DATE_TEXT_COLOR.CGColor);
    CGContextSetFillColorWithColor(aContext, DATE_TEXT_COLOR.CGColor);

    NSInteger totalDays = accountGraphicInformation_.daysSpan - 1;
    
    NSInteger dayAnalyzed = 0;
    
    if (aFirstDayXPosition < 0.0f) {
        
        dayAnalyzed = (NSInteger)(floor(-aFirstDayXPosition / dayWidth_));
        
    }
    
    CGFloat multiplyingFactor = (multiplyingHorizontalFactor_ < multiplyingVerticalFactor_) ? multiplyingHorizontalFactor_ : multiplyingVerticalFactor_;
    CGFloat textFontSize = NOMINAL_DAY_TEXT_SIZE * multiplyingFactor;
    CGFloat textOffset = NOMINAL_DAY_TEXT_HORIONTAL_OFFSET * multiplyingFactor;
    UIFont *font = [NXT_Peru_iPhoneStyler boldFontWithSize:textFontSize];

    CGFloat currentDayLeftMostXPos = aFirstDayXPosition + (dayAnalyzed * dayWidth_);
    CGFloat currentDayRightMostXPos = currentDayLeftMostXPos + dayWidth_;
    NSTimeInterval firstDay = accountGraphicInformation_.firstDay;
    NSString *firstDayString;
    CGSize textSize;
    CGFloat textWidth;
    CGFloat textHeight;
    CGFloat totalSizeNeeded;
    CGFloat availableWidth;
    CGFloat textLeftPosition;
    CGFloat textTopPosition = NOMINAL_DAY_TEXT_VERTICAL_OFFSET * multiplyingFactor;
    CGFloat maxTextHeight = -CGFLOAT_MAX;
        
    while (dayAnalyzed < totalDays) {
    
        firstDayString = [self graphicStringForDateAtDays:dayAnalyzed fromReference:firstDay];
        textSize = [firstDayString sizeWithFont:font];
        textWidth = textSize.width;
        textHeight = textSize.height;
        totalSizeNeeded = textWidth + (2.0f * textOffset);
        availableWidth = (currentDayLeftMostXPos < 0.0f) ? (currentDayRightMostXPos - 0.0f) : (currentDayRightMostXPos - currentDayLeftMostXPos);
        
        if (totalSizeNeeded <= availableWidth) {
            
            textLeftPosition = (currentDayLeftMostXPos < 0.0f) ? textOffset : (currentDayLeftMostXPos + textOffset);
            [firstDayString drawAtPoint:CGPointMake(textLeftPosition, textTopPosition) withFont:font];

            if (textHeight > maxTextHeight) {
            
                maxTextHeight = textHeight;
                
            }
            
            break;
            
        }
        
        dayAnalyzed++;
        currentDayLeftMostXPos = aFirstDayXPosition + (dayAnalyzed * dayWidth_);
        currentDayRightMostXPos = currentDayLeftMostXPos + dayWidth_;
        
    }
    
    dayAnalyzed = (NSInteger)(floor((aControlWidth - aFirstDayXPosition) / dayWidth_));
    dayAnalyzed = (dayAnalyzed <= totalDays) ? dayAnalyzed : totalDays;

    currentDayLeftMostXPos = aFirstDayXPosition + (dayAnalyzed * dayWidth_);
    currentDayRightMostXPos = currentDayLeftMostXPos + dayWidth_;

    while (dayAnalyzed > 0) {
        
        firstDayString = [self graphicStringForDateAtDays:dayAnalyzed fromReference:firstDay];
        textSize = [firstDayString sizeWithFont:font];
        textWidth = textSize.width;
        textHeight = textSize.height;
        totalSizeNeeded = textWidth + (2.0f * textOffset);
        availableWidth = (currentDayRightMostXPos > aControlWidth) ? (aControlWidth - currentDayLeftMostXPos) : (currentDayRightMostXPos - currentDayLeftMostXPos);
        
        if (totalSizeNeeded <= availableWidth) {
            
            textLeftPosition = (currentDayRightMostXPos > aControlWidth) ? (aControlWidth - textWidth - textOffset) : (currentDayRightMostXPos - textWidth - textOffset);
            [firstDayString drawAtPoint:CGPointMake(textLeftPosition, textTopPosition) withFont:font];
            
            if (textHeight > maxTextHeight) {
            
                maxTextHeight = textHeight;
                
            }

            break;
            
        }
        
        dayAnalyzed--;
        currentDayLeftMostXPos = aFirstDayXPosition + (dayAnalyzed * dayWidth_);
        currentDayRightMostXPos = currentDayLeftMostXPos + dayWidth_;
        
    }
    
    CGContextRestoreGState(aContext);
    
    if (maxTextHeight > 0.0f) {
        
        dayLineBottomPosition_ = maxTextHeight + textTopPosition + (NOMINAL_DAY_TEXT_VERTICAL_OFFSET * multiplyingFactor);
        
    } else {
        
        dayLineBottomPosition_ = 0.0f;
        
    }
    
}

/*
 * Draws the amounts on the graphic right hand side
 */
- (void)drawAmountsIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                  controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight {
    
    CGFloat multiplyingFactor = (multiplyingHorizontalFactor_ < multiplyingVerticalFactor_) ? multiplyingHorizontalFactor_ : multiplyingVerticalFactor_;
    
    CGFloat borderHorizontalOffset = NOMINAL_AMOUNT_BORDER_HORIZONTAL_OFFSET * multiplyingFactor;
    CGFloat borderRightPosition = aControlWidth - borderHorizontalOffset;
    
    CGFloat borderWidth;
    CGFloat borderHeight;
    CGFloat borderTop;
    CGFloat borderLeft;
    CGFloat stringWidth;
    CGFloat stringHeight;
    NSString *string;
    CGColorRef borderColor = AMOUNT_TEXT_BORDER_COLOR.CGColor;
    CGColorRef textColor = AMOUNT_TEXT_COLOR.CGColor;
    
    for (NXTGraphicAmountStringInformation *information in amountStringsInformationArray_) {
        
        borderWidth = information.borderWidth;
        borderHeight = information.borderHeight;
        
        borderTop = information.borderTopPosition;
        borderLeft = borderRightPosition - borderWidth;
        
        if (borderTop > dayLineBottomPosition_) {

            stringWidth = information.stringWidth;
            stringHeight = information.stringHeight;
            string = information.amountString;
            
            CGContextSaveGState(aContext);
            CGContextSetFillColorWithColor(aContext, borderColor);
            CGContextSetStrokeColorWithColor(aContext, borderColor);
            
            CGContextTranslateCTM(aContext, borderLeft, borderTop);
            CGContextAddPath(aContext, amountBorderPath_);
            CGContextFillPath(aContext);
            
            CGContextTranslateCTM(aContext, borderWidth - stringWidth - multiplyingFactor - (borderHeight / 2.0f), (borderHeight - stringHeight) / 2.0f);
            
            CGContextSetFillColorWithColor(aContext, textColor);
            CGContextSetStrokeColorWithColor(aContext, textColor);
            
            [string drawAtPoint:CGPointMake(0.0f, 0.0f) withFont:amountTextFont_];
            
            CGContextRestoreGState(aContext);
        
        }
        
    }
    
}

/*
 * Draws the balance graphic into the provided context
 */
- (void)drawBalanceBackgroundIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                  controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight {
    
    [self createGraphicPathsWithFirstDayXPosition:aFirstDayXPosition controlWidth:aControlWidth controlHeight:aControlHeight];
    
    CGContextSaveGState(aContext);
    CGContextBeginTransparencyLayer(aContext, NULL);
    
    CGContextAddPath(aContext, graphicFillPath_);
    CGContextClip(aContext);
    CGContextDrawLinearGradient(aContext, graphicGradient_, CGPointMake(0.0f, 0.0f), CGPointMake(0.0f, aControlHeight), 0);

    [self drawGraphicSmoothingGradientIntoContext:aContext withFirstDayXPosition:aFirstDayXPosition controlWidth:aControlWidth controlHeight:aControlHeight];
    CGContextEndTransparencyLayer(aContext);
    CGContextRestoreGState(aContext);
    
}

/*
 * Draws the balance graphic lines into the provided context
 */
- (void)drawBalanceLinesIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                       controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight {
    
    NSInteger currentDay = 0;
    
    if (aFirstDayXPosition < 0.0f) {
        
        currentDay = (NSInteger)(floor(-aFirstDayXPosition / dayWidth_));
        
    }
    
    CGFloat multiplyingFactor = (multiplyingHorizontalFactor_ < multiplyingVerticalFactor_) ? multiplyingHorizontalFactor_ : multiplyingVerticalFactor_;
    CGFloat circleDiameter = NOMINAL_DAY_CIRCLE_DIAMETER * multiplyingFactor;
    CGFloat circleRadius = circleDiameter / 2.0f;
    NSUInteger daysSpan = accountGraphicInformation_.daysSpan;
    CGFloat currentDayXLeftMostPosition = aFirstDayXPosition + (currentDay * dayWidth_);
    NXTAccountGraphicDay *accountGraphicDay;
    NSDecimalNumber *dayBalanceNumber;
    CGFloat dayBalanceYPosition;
    CGColorRef dayCircleLineColor = GRAPHIC_LINES_COLOR.CGColor;
    CGColorRef dayCircleFillColor = DAY_CIRCLE_FILL_COLOR.CGColor;
    CGColorRef selectedDayCircleLineColor = DAY_SELECTED_CIRCLE_LINE_COLOR.CGColor;
    CGColorRef selectedDayCircleFillColor = DAY_SELECTED_CIRCLE_FILL_COLOR.CGColor;
    
    CGContextSaveGState(aContext);

    CGContextBeginTransparencyLayer(aContext, NULL);
    CGContextAddPath(aContext, graphicLinePath_);
    CGContextSetStrokeColorWithColor(aContext, GRAPHIC_LINES_COLOR.CGColor);
    CGContextSetLineWidth(aContext, 1.0f);
    CGContextSetLineJoin(aContext, kCGLineJoinRound);
    CGContextStrokePath(aContext);
    
    while ((currentDay < daysSpan) && (currentDayXLeftMostPosition <= (aControlWidth + circleRadius))) {
        
        currentDayXLeftMostPosition = aFirstDayXPosition + (currentDay * dayWidth_);
        
        accountGraphicDay = [accountGraphicInformation_ accountGraphicDayAtIndex:currentDay];
        dayBalanceNumber = accountGraphicDay.accountBalance;
        dayBalanceYPosition = AMOUNT_TO_Y([Tools convertDecimalNumber:dayBalanceNumber], yAxisZeroPosition_, 0.0f, graphicScale_);
        
        CGContextBeginPath(aContext);
        
        if (currentDay > 0) {

            CGContextAddEllipseInRect(aContext, CGRectMake(currentDayXLeftMostPosition - circleRadius, dayBalanceYPosition - circleRadius, circleDiameter, circleDiameter));
            
        }
        
        if (currentDay == selectedDay_) {
            
            CGContextSetStrokeColorWithColor(aContext, selectedDayCircleLineColor);
            CGContextSetFillColorWithColor(aContext, selectedDayCircleFillColor);
            
        } else {
            
            CGContextSetStrokeColorWithColor(aContext, dayCircleLineColor);
            CGContextSetFillColorWithColor(aContext, dayCircleFillColor);
            
        }
        
        CGContextDrawPath(aContext, kCGPathFillStroke);
        
        if (currentDay == (daysSpan - 1)) {
            
//            NSDecimalNumber *transactions = accountGraphicDay.transactionsGrandTotal;
//            NSDecimalNumber *finalBalance = [dayBalanceNumber decimalNumberByAdding:transactions];
            currentDayXLeftMostPosition = aFirstDayXPosition + (daysSpan * dayWidth_);
//            dayBalanceYPosition = AMOUNT_TO_Y([self convertDecimalNumber:finalBalance]);
            
        }
        
        currentDay ++;
        
    }

    [self drawGraphicSmoothingGradientIntoContext:aContext withFirstDayXPosition:aFirstDayXPosition controlWidth:aControlWidth controlHeight:aControlHeight];
    CGContextEndTransparencyLayer(aContext);
    CGContextRestoreGState(aContext);
        
}

/*
 * Draws the graphic smoothing gradient into the provided context
 */
- (void)drawGraphicSmoothingGradientIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                                   controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight {

    if (graphicLimitsSmoothGradient_ == NULL) {
        
        [self createGraphicLimitsSmoothingGradient];
        
    }
    
    CGFloat gradientWidth = dayWidth_ * 0.75f;
    
    CGFloat leftLimitGradientLeft = aFirstDayXPosition;
    CGFloat leftLimitGradientRight = leftLimitGradientLeft + gradientWidth;
    
    if ((leftLimitGradientRight >= 0.0f) && (leftLimitGradientLeft <= aControlWidth)) {
        
        CGContextSaveGState(aContext);
        CGContextClipToRect(aContext, CGRectMake(leftLimitGradientLeft, 0.0f, leftLimitGradientRight, aControlHeight));
        CGContextSetBlendMode(aContext, kCGBlendModeDestinationOut);
        CGContextDrawLinearGradient(aContext, graphicLimitsSmoothGradient_, CGPointMake(leftLimitGradientLeft, 0.0f), CGPointMake(leftLimitGradientRight, 0.0f), 0);
        CGContextRestoreGState(aContext);
        
    }
    
    CGFloat rightLimitGradientRight = aFirstDayXPosition + (dayWidth_ * [accountGraphicInformation_ daysSpan]);
    CGFloat rightLimitGradientLeft = rightLimitGradientRight - gradientWidth;
    
    if ((rightLimitGradientRight >= 0.0f) && (rightLimitGradientLeft <= aControlWidth)) {
        
        CGContextSaveGState(aContext);
        CGContextClipToRect(aContext, CGRectMake(rightLimitGradientLeft, 0.0f, rightLimitGradientRight, aControlHeight));
        CGContextSetBlendMode(aContext, kCGBlendModeDestinationIn);
        CGContextDrawLinearGradient(aContext, graphicLimitsSmoothGradient_, CGPointMake(rightLimitGradientLeft, 0.0f), CGPointMake(rightLimitGradientRight, 0.0f), 0);
        CGContextRestoreGState(aContext);
        
    }
    
}

/*
 * Draws the transactions bars into the provided context
 */
- (void)drawTransactionsIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                       controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight {
    
    CGFloat controlMidWidth = aControlWidth / 2.0f;
    
    NSInteger currentDay = 0;
    
    if (aFirstDayXPosition < 0.0f) {
        
        currentDay = (NSInteger)(floor(-aFirstDayXPosition / dayWidth_));
        
    }
    
    NSUInteger daysSpan = accountGraphicInformation_.daysSpan;
    CGFloat currentDayXLeftMostPosition = aFirstDayXPosition + (currentDay * dayWidth_);
    CGFloat currentDayXRightMostPosition = currentDayXLeftMostPosition + dayWidth_;
    CGFloat maxTransactionsGrandTotalHeight = MAX_TRANSACTIONS_GRAND_TOTAL_HEIGHT * multiplyingVerticalFactor_;
    NXTAccountGraphicDay *accountGraphicDay;
    CGFloat dayTransactionsTotal;
    CGFloat transactionHeight;
    CGFloat rectTop = 0.0f;
    CGColorRef positiveTransactionsFillColor = POSITIVE_TRANSACTIONS_COLOR.CGColor;
    CGColorRef negativeTransactionsFillColor = NEGATIVE_TRANSACTIONS_COLOR.CGColor;
    CGColorRef selectedTransactionsFillColor = SELECTED_TRANSACTIONS_COLOR.CGColor;
    
    while ((currentDay < daysSpan) && (currentDayXLeftMostPosition < aControlWidth)) {
        
        accountGraphicDay = [accountGraphicInformation_ accountGraphicDayAtIndex:currentDay];
        dayTransactionsTotal = [Tools convertDecimalNumber:accountGraphicDay.transactionsGrandTotal];
        
        if (maxTransactionsGrandTotalRange_ != 0.0f) {
            
            transactionHeight = dayTransactionsTotal / maxTransactionsGrandTotalRange_ * maxTransactionsGrandTotalHeight;
            ////DLog(@"day total %f - max total range %f - max total height", dayTransactionsTotal, maxTransactionsGrandTotalRange_, maxTransactionsGrandTotalHeight);
            
        } else {
            
            transactionHeight = 0.0f;
            
        }
        
        if (transactionHeight >= 0.0f) {
            
            if (transactionHeight < 1.0f) {

                transactionHeight = 1.0f;
                
            }

            rectTop = yAxisZeroPosition_ - transactionHeight;
            
        } else if (transactionHeight < 0.0f) {
            
            if (transactionHeight > -1.0f) {

                transactionHeight = -1.0f;
                
            }

            rectTop = yAxisZeroPosition_;
            
        }
        
        if ((transactionsCalloutVisible_) && (currentDayXLeftMostPosition <= controlMidWidth) && (currentDayXRightMostPosition > controlMidWidth)) {
            
            CGContextSetFillColorWithColor(aContext, selectedTransactionsFillColor);
            
        } else if (transactionHeight >= 0.0f) {
            
            CGContextSetFillColorWithColor(aContext, positiveTransactionsFillColor);

        } else {
            
            CGContextSetFillColorWithColor(aContext, negativeTransactionsFillColor);
            
        }
        
        CGContextFillRect(aContext, CGRectMake(currentDayXLeftMostPosition + 1.0f, rectTop, dayWidth_ - 2.0f, fabs(transactionHeight)));
        
        currentDay++;
        currentDayXLeftMostPosition = aFirstDayXPosition + (currentDay * dayWidth_);
        currentDayXRightMostPosition = currentDayXLeftMostPosition + dayWidth_;
        
    }
    
}

/*
 * Draws the transaction callout into the provided context
 */
- (void)drawTransactionCallOutIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                              controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight {
    
    BOOL transactionsCalloutDisplayed = NO;
    
    if (transactionsCalloutVisible_) {
        
        CGFloat controlMidWidth = aControlWidth / 2.0f;
        NSUInteger daysSpan = accountGraphicInformation_.daysSpan;
        NSInteger centerDay = (NSInteger)(floor((controlMidWidth - aFirstDayXPosition) / dayWidth_));
        
        if ((centerDay >= 0) && (centerDay < daysSpan)) {
            
            CGFloat multiplyingFactor = (multiplyingHorizontalFactor_ < multiplyingVerticalFactor_) ? multiplyingHorizontalFactor_ : multiplyingVerticalFactor_;
            NXTAccountGraphicDay *accountGraphicDay = [accountGraphicInformation_ accountGraphicDayAtIndex:centerDay];
            
            if (accountGraphicDay.hasTransactions) {
                
                NSDecimalNumber *transactionsGrandTotal = accountGraphicDay.transactionsGrandTotal;
                CGFloat transactionsGrandTotalFloat = [Tools convertDecimalNumber:transactionsGrandTotal];
                
                CGFloat calloutWidth = NOMINAL_TRANSACTIONS_CALLOUT_RECTANGLE_WIDTH * multiplyingFactor;
                CGFloat calloutMidWidth = calloutWidth / 2.0f;
                CGFloat calloutHeight = NOMINAL_TRANSACTIONS_CALLOUT_HEIGHT * multiplyingFactor;
                CGFloat calloutBorderWidth = NOMINAL_TRANSACTIONS_CALLOUT_BORDER_WIDTH * multiplyingFactor;
                CGFloat calloutShadowOffset = NOMINAL_TRANSACTIONS_CALLOUT_SHADOW_OFFSET * multiplyingFactor;
                
                CGFloat calloutLeftPos = controlMidWidth - calloutMidWidth;
                CGFloat calloutTopPos = yAxisZeroPosition_ - calloutHeight;
                
                if (transactionsGrandTotalFloat >= 0.0f) {
                    
                    CGFloat maxTransactionsGrandTotalHeight = MAX_TRANSACTIONS_GRAND_TOTAL_HEIGHT * multiplyingVerticalFactor_;
                    CGFloat transactionHeight;
                    
                    if (maxTransactionsGrandTotalRange_ != 0.0f) {
                        
                        transactionHeight = transactionsGrandTotalFloat / maxTransactionsGrandTotalRange_ * maxTransactionsGrandTotalHeight;
                        
                    } else {
                        
                        transactionHeight = 0.0f;
                        
                    }
                    
                    if (transactionHeight < 1.0f) {
                        
                        transactionHeight = 1.0f;
                        
                    }
                    
                    calloutTopPos -= transactionHeight;
                    
                }
                
                CGContextSaveGState(aContext);
                
                CGContextTranslateCTM(aContext, calloutLeftPos, calloutTopPos);
                CGContextSaveGState(aContext);
                CGContextAddPath(aContext, transactionsCalloutPath_);
                CGContextClip(aContext);
                CGContextDrawLinearGradient(aContext, calloutGradient_, CGPointMake(0.0f, 0.0f), CGPointMake(0.0f, calloutHeight), 0);
                CGContextRestoreGState(aContext);
                
                CGContextAddPath(aContext, transactionsCalloutPath_);
                CGContextSetLineWidth(aContext, calloutBorderWidth);
                CGContextSetStrokeColorWithColor(aContext, CALLOUT_BORDER_COLOR.CGColor);
                CGContextSetShadowWithColor(aContext, CGSizeMake(-calloutShadowOffset, calloutShadowOffset), calloutShadowOffset, CALLOUT_SHADOW_COLOR.CGColor);
                CGContextStrokePath(aContext);
                
                if (accountGraphicDay.hasTransactions) {
                    
                    CGContextSetStrokeColorWithColor(aContext, CALLOUT_BORDER_COLOR.CGColor);
                    CGContextSetLineWidth(aContext, NOMINAL_TRANSACTIONS_CALLOUT_RIGHT_ARROW_LINE_WIDTH * multiplyingFactor);
                    CGContextSetLineJoin(aContext, kCGLineJoinRound);
                    
                    CGFloat arrowRight = calloutWidth - (NOMINAL_CALLOUT_OFFSETS * multiplyingFactor);
                    CGFloat arrowLeft = arrowRight - (NOMINAL_TRANSACTIONS_CALLOUT_RIGHT_ARROW_WIDTH * multiplyingFactor);
                    CGFloat arrowCenterHeight = NOMINAL_TRANSACTIONS_CALLOUT_RECTANGLE_HEIGHT * multiplyingFactor / 2.0f;
                    CGFloat arrowMidHeidth = NOMINAL_TRANSACTIONS_CALLOUT_RIGHT_ARROW_HALF_HEIGHT * multiplyingFactor;
                    
                    CGContextBeginPath(aContext);
                    CGContextMoveToPoint(aContext, arrowLeft, arrowCenterHeight - arrowMidHeidth);
                    CGContextAddLineToPoint(aContext, arrowRight, arrowCenterHeight);
                    CGContextAddLineToPoint(aContext, arrowLeft, arrowCenterHeight + arrowMidHeidth);
                    
                    CGContextStrokePath(aContext);
                    
                    transactionsCalloutRect_ = CGRectMake(calloutLeftPos, calloutTopPos, calloutWidth, calloutHeight);
                    transactionsCalloutDisplayed = YES;
                    
                }
                
                CGFloat availableWidth = calloutWidth - (((3.0f * NOMINAL_CALLOUT_OFFSETS) + NOMINAL_TRANSACTIONS_CALLOUT_RIGHT_ARROW_WIDTH) * multiplyingFactor);

                [self drawCalloutStringsIntoContext:aContext withAmount:transactionsGrandTotal dateOffset:centerDay amountAvailableWidth:availableWidth];
                    
            }
            
        }
        
    }
    
    if (!transactionsCalloutDisplayed) {
        
        transactionsCalloutRect_ = CGRectMake(-100.0f, -100.0f, 1.0f, 1.0f);
        
    }
    
}

/*
 * Draws the selected day callout into the provided context
 */
- (void)drawSelectedDayCallOutIntoContext:(CGContextRef)aContext withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                             controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight {
    
    NSInteger daysSpan = accountGraphicInformation_.daysSpan;
    
    if ((selectedDay_ >= 0) && (selectedDay_ < daysSpan)) {

        CGFloat multiplyingFactor = (multiplyingVerticalFactor_ < multiplyingHorizontalFactor_) ? multiplyingVerticalFactor_ : multiplyingHorizontalFactor_;
        CGFloat calloutRectangleHeight = NOMINAL_BALANCE_CALLOUT_RECTANGLE_HEIGHT * multiplyingFactor;
        CGFloat calloutRectangleWidth = NOMINAL_BALANCE_CALLOUT_RECTANGLE_WIDTH * multiplyingFactor;
        CGFloat downArrowHeight = NOMINAL_BALANCE_CALLOUT_DOWN_ARROW_HEIGHT * multiplyingFactor;
        CGFloat roundingCorners = NOMINAL_BALANCE_CALLOUT_ROUNDING_RADIUS * multiplyingFactor;
        CGFloat offset = NOMINAL_CALLOUT_OFFSETS * multiplyingFactor;
        
        CGFloat minDayXPosition = roundingCorners + downArrowHeight + offset;
        CGFloat maxDayXPosition = aControlWidth - minDayXPosition;
        
        CGFloat selectedDayXPosition = aFirstDayXPosition + (selectedDay_ * dayWidth_);
        
        if ((minDayXPosition <= selectedDayXPosition) && (maxDayXPosition >= selectedDayXPosition)) {
                
            CGFloat rectangleMidWidth = calloutRectangleWidth / 2.0f;
            CGFloat rightMostNotRounded = calloutRectangleWidth - roundingCorners;
            CGFloat leftMostNotRounded = roundingCorners;
            CGFloat topMostNotRounded = roundingCorners;
            CGFloat bottomMostNotRounded = calloutRectangleHeight - roundingCorners;
            CGFloat arrowMidPoint = rectangleMidWidth;
            CGFloat calloutHeight = NOMINAL_BALANCE_CALLOUT_HEIGHT * multiplyingFactor;
            CGFloat calloutLeftPos = selectedDayXPosition - rectangleMidWidth;
            CGFloat calloutBorderWidth = NOMINAL_TRANSACTIONS_CALLOUT_BORDER_WIDTH * multiplyingFactor;
            CGFloat calloutShadowOffset = NOMINAL_TRANSACTIONS_CALLOUT_SHADOW_OFFSET * multiplyingFactor;

            CGFloat leftBorderDistance = selectedDayXPosition;
            CGFloat rightBorderDistance = aControlWidth - selectedDayXPosition;
            
            if (leftBorderDistance < rectangleMidWidth) {
                
                arrowMidPoint = leftBorderDistance;
                calloutLeftPos = 0.0f;
                
            } else if (rightBorderDistance < rectangleMidWidth) {
                
                arrowMidPoint = calloutRectangleWidth - rightBorderDistance;
                calloutLeftPos = aControlWidth - calloutRectangleWidth;
                
            }
            
            CGFloat arrowLeftInsertion = arrowMidPoint - downArrowHeight;
            CGFloat arrowRightInsertion = arrowMidPoint + downArrowHeight;
            
            NXTAccountGraphicDay *accountDay = [accountGraphicInformation_ accountGraphicDayAtIndex:selectedDay_];
            NSDecimalNumber *balance = accountDay.accountBalance;
            CGFloat balanceFloat = [Tools convertDecimalNumber:balance];
            CGFloat balanceYPos = AMOUNT_TO_Y(balanceFloat, yAxisZeroPosition_, 0.0f, graphicScale_);
            
            CGFloat balanceCircleDiameter = NOMINAL_DAY_CIRCLE_DIAMETER * multiplyingFactor;
            CGFloat calloutTopPos = balanceYPos - balanceCircleDiameter - calloutHeight;
            
            CGMutablePathRef selectedDayPath = CGPathCreateMutable();
            
            CGPathMoveToPoint(selectedDayPath, NULL, rightMostNotRounded, 0.0f);
            CGPathAddArcToPoint(selectedDayPath, NULL, calloutRectangleWidth, 0.0f, calloutRectangleWidth, topMostNotRounded, roundingCorners);
            CGPathAddLineToPoint(selectedDayPath, NULL, calloutRectangleWidth, bottomMostNotRounded);
            CGPathAddArcToPoint(selectedDayPath, NULL, calloutRectangleWidth, calloutRectangleHeight, rightMostNotRounded, calloutRectangleHeight, roundingCorners);
            CGPathAddLineToPoint(selectedDayPath, NULL, arrowRightInsertion, calloutRectangleHeight);
            CGPathAddLineToPoint(selectedDayPath, NULL, arrowMidPoint, calloutRectangleHeight + downArrowHeight);
            CGPathAddLineToPoint(selectedDayPath, NULL, arrowLeftInsertion, calloutRectangleHeight);
            CGPathAddLineToPoint(selectedDayPath, NULL, leftMostNotRounded, calloutRectangleHeight);
            CGPathAddArcToPoint(selectedDayPath, NULL, 0.0f, calloutRectangleHeight, 0.0f, bottomMostNotRounded, roundingCorners);
            CGPathAddLineToPoint(selectedDayPath, NULL, 0.0f, topMostNotRounded);
            CGPathAddArcToPoint(selectedDayPath, NULL, 0.0f, 0.0f, leftMostNotRounded, 0.0f, roundingCorners);
            CGPathCloseSubpath(selectedDayPath);
            

            CGContextSaveGState(aContext);
            
            CGContextTranslateCTM(aContext, calloutLeftPos, calloutTopPos);
            CGContextSaveGState(aContext);
            CGContextAddPath(aContext, selectedDayPath);
            CGContextClip(aContext);
            CGContextDrawLinearGradient(aContext, calloutGradient_, CGPointMake(0.0f, 0.0f), CGPointMake(0.0f, calloutHeight), 0);
            CGContextRestoreGState(aContext);
            
            CGContextAddPath(aContext, selectedDayPath);
            CGContextSetLineWidth(aContext, calloutBorderWidth);
            CGContextSetStrokeColorWithColor(aContext, CALLOUT_BORDER_COLOR.CGColor);
            CGContextSetShadowWithColor(aContext, CGSizeMake(-calloutShadowOffset, calloutShadowOffset), calloutShadowOffset, CALLOUT_SHADOW_COLOR.CGColor);
            CGContextStrokePath(aContext);
            
            CGPathRelease(selectedDayPath);
        
            CGFloat availableWidth = calloutRectangleWidth - ((2.0f * NOMINAL_CALLOUT_OFFSETS) * multiplyingFactor);
            
            [self drawCalloutStringsIntoContext:aContext withAmount:balance dateOffset:selectedDay_ amountAvailableWidth:availableWidth];

        }
        
    }
    
}

/*
 * Draws a callout texts into the provided context. It assumes the context is translated to the callout top left corner
 */
- (void)drawCalloutStringsIntoContext:(CGContextRef)aContext withAmount:(NSDecimalNumber *)anAmount
                           dateOffset:(NSInteger)aDateOffset amountAvailableWidth:(CGFloat)anAmountAvaliableWidth {
    
    NSString *amountString = [Tools formatAmount:anAmount withDecimalCount:2 plusSign:NO];
    
    CGFloat multiplyingFactor = (multiplyingHorizontalFactor_ < multiplyingVerticalFactor_) ? multiplyingHorizontalFactor_ : multiplyingVerticalFactor_;
    CGFloat amountTextSize = NOMINAL_CALLOUT_AMOUNT_TEXT_SIZE * multiplyingFactor;
    CGFloat currencyTextSize = NOMINAL_CALLOUT_CURRENCY_TEXT_SIZE * multiplyingFactor;
    UIFont *amountFont = [NXT_Peru_iPhoneStyler boldFontWithSize:amountTextSize];
    UIFont *currencyFont = [NXT_Peru_iPhoneStyler normalFontWithSize:currencyTextSize];
    
    CGFloat gap = NOMINAL_CALLOUT_AMOUNT_TO_CURRENCY_GAP * multiplyingFactor;
    
    CGSize textSize = [amountString sizeWithFont:amountFont];
    CGFloat usedWidth = textSize.width;
    textSize = [currencySymbol_ sizeWithFont:currencyFont];
    usedWidth += textSize.width;
    usedWidth += gap;

    CGFloat textMultiplyingFactor = 1.0f;
    
    if ((usedWidth > anAmountAvaliableWidth) && (usedWidth > 0.0f)) {
        
        textMultiplyingFactor = anAmountAvaliableWidth / usedWidth;
        
    }
    
    CGFloat baseLine = NOMINAL_CALLOUT_FIRST_LINE_BASE_LINE * multiplyingFactor;
    CGFloat leftPos = NOMINAL_CALLOUT_OFFSETS * multiplyingFactor;
    
    amountTextSize = amountTextSize * textMultiplyingFactor;
    currencyTextSize = currencyTextSize * textMultiplyingFactor;
    amountFont = [NXT_Peru_iPhoneStyler boldFontWithSize:amountTextSize];
    currencyFont = [NXT_Peru_iPhoneStyler normalFontWithSize:currencyTextSize];
    
    textSize = [amountString sizeWithFont:amountFont];
    CGFloat amountWidth = textSize.width;
    CGFloat amountHeight = textSize.height;
    
    textSize = [@"0" sizeWithFont:amountFont];
    CGFloat auxHeight = textSize.height;
    
    amountHeight = (amountHeight < auxHeight) ? amountHeight : auxHeight;
    
    textSize = [currencySymbol_ sizeWithFont:currencyFont];
    CGFloat currencyWidth = textSize.width;
    CGFloat currencyHeight = textSize.height;
    
    textSize = [@"0" sizeWithFont:currencyFont];
    auxHeight = textSize.height;
    
    currencyHeight = (currencyHeight < auxHeight) ? currencyHeight : auxHeight;
    
    gap = gap * textMultiplyingFactor;
    
    CGFloat currencyLeft;
    CGFloat amountLeft;
    
    if (currencyFollows_) {
        
        amountLeft = leftPos;
        currencyLeft = amountLeft + amountWidth + gap;
        
    } else {
        
        currencyLeft = leftPos;
        amountLeft = currencyLeft + currencyWidth + gap;
        
    }
    
    CGContextSetStrokeColorWithColor(aContext, CALLOUT_AMOUNT_TEXT_COLOR.CGColor);
    CGContextSetFillColorWithColor(aContext, CALLOUT_AMOUNT_TEXT_COLOR.CGColor);
    
    [amountString drawAtPoint:CGPointMake(amountLeft, baseLine - amountHeight) withFont:amountFont];
    [currencySymbol_ drawAtPoint:CGPointMake(currencyLeft, baseLine - currencyHeight + amountFont.descender - currencyFont.descender) withFont:currencyFont];
    
    baseLine = NOMINAL_CALLOUT_SECOND_LINE_BASE_LINE * multiplyingFactor;
    UIFont *dateFont = [NXT_Peru_iPhoneStyler normalFontWithSize:(NOMINAL_CALLOUT_DATE_TEXT_SIZE * multiplyingFactor)];
    NSString *dateString = [self calloutStringForDateAtDays:aDateOffset fromReference:accountGraphicInformation_.firstDay];
    textSize = [dateString sizeWithFont:dateFont];
    
    CGContextSetStrokeColorWithColor(aContext, CALLOUT_DATE_TEXT_COLOR.CGColor);
    CGContextSetFillColorWithColor(aContext, CALLOUT_DATE_TEXT_COLOR.CGColor);
    [dateString drawAtPoint:CGPointMake(leftPos, baseLine - textSize.height) withFont:dateFont];
    
    CGContextRestoreGState(aContext);
    
}

/*
 * Creates the amount strings information
 */
- (void)createAmountStringsInformation {
    
    CGRect frame = self.frame;
    CGFloat height = CGRectGetMaxY(frame) - CGRectGetMinY(frame);
    
    CGFloat multiplyingFactor = (multiplyingHorizontalFactor_ < multiplyingVerticalFactor_) ? multiplyingHorizontalFactor_ : multiplyingVerticalFactor_;
    CGFloat fontSize = NOMINAL_AMOUNT_TEXT_SIZE * multiplyingFactor;
    
    [amountTextFont_ release];
    amountTextFont_ = nil;
    amountTextFont_ = [[NXT_Peru_iPhoneStyler normalFontWithSize:fontSize] retain];
    
    [amountStringsInformationArray_ removeAllObjects];
    
    CGFloat borderHeight = NOMINAL_AMOUNT_BORDER_HEIGHT * multiplyingFactor;
    CGFloat borderOffset = NOMINAL_AMOUNT_BORDER_VERTICAL_OFFSET * multiplyingFactor;
    
    NSString *amountFormat = @"";
    
    if (currencyFollows_) {
        
        if (mainDivision_ > 1.0f) {
            
            amountFormat = @"%.0f%@";
            
        } else {

            amountFormat = @"%1.3f%@";

        }
        
    } else {

        if (mainDivision_ > 1.0f) {

            amountFormat = @"%@%.0f";

        } else {
            
            amountFormat = @"%@%1.3f";
            
        }
        
    }
    
    CGFloat amount = 0.0f;
    CGFloat amountYPosition = AMOUNT_TO_Y(amount, yAxisZeroPosition_, 0.0f, graphicScale_);
    NSString *amountString;
    CGSize textSize;
    CGFloat textWidth;
    CGFloat textHeight;
    CGFloat borderBottom;
    CGFloat borderTop;
    CGFloat maxTextWidth = -CGFLOAT_MAX;
    
    while (amountYPosition > 0.0f) {
        
        borderBottom = amountYPosition - borderOffset;
        borderTop = borderBottom - borderHeight;
        
        if ((borderTop - borderOffset) >= 0.0f) {
            
            if (currencyFollows_) {

                amountString = [NSString stringWithFormat:amountFormat, amount, currencySymbol_];
                
            } else {

                amountString = [NSString stringWithFormat:amountFormat, currencySymbol_, amount];
                
            }
            
            textSize = [amountString sizeWithFont:amountTextFont_];
            textWidth = textSize.width;
            textHeight = textSize.height;
            
            if (textWidth > maxTextWidth) {
                
                maxTextWidth = textWidth;
                
            }
            
            [amountStringsInformationArray_ addObject:[[[NXTGraphicAmountStringInformation alloc] initWithAmountString:amountString
                                                                                                           stringWidth:textWidth
                                                                                                          stringHeight:textHeight
                                                                                                     borderTopPosition:borderTop
                                                                                                           borderWidth:0.0f
                                                                                                          borderHeight:borderHeight] autorelease]];
            
        } else {
            
            break;
            
        }
        
        amount +=mainDivision_;
        amountYPosition = AMOUNT_TO_Y(amount, yAxisZeroPosition_, 0.0f, graphicScale_);
        
    }
    
    amount = -mainDivision_;
    amountYPosition = AMOUNT_TO_Y(amount, yAxisZeroPosition_, 0.0f, graphicScale_);
    
    while (amountYPosition <= height) {
        
        borderBottom = amountYPosition - borderOffset;
        borderTop = borderBottom - borderHeight;
            
        if (currencyFollows_) {
            
            amountString = [NSString stringWithFormat:amountFormat, amount, currencySymbol_];
            
        } else {
            
            amountString = [NSString stringWithFormat:amountFormat, currencySymbol_, amount];
            
        }
        
        textSize = [amountString sizeWithFont:amountTextFont_];
        textWidth = textSize.width;
        textHeight = textSize.height;
        
        if (textWidth > maxTextWidth) {
            
            maxTextWidth = textWidth;
            
        }
        
        [amountStringsInformationArray_ addObject:[[[NXTGraphicAmountStringInformation alloc] initWithAmountString:amountString
                                                                                                       stringWidth:textWidth
                                                                                                      stringHeight:textHeight
                                                                                                 borderTopPosition:borderTop
                                                                                                       borderWidth:0.0f
                                                                                                      borderHeight:borderHeight] autorelease]];
        
        amount -=mainDivision_;
        amountYPosition = AMOUNT_TO_Y(amount, yAxisZeroPosition_, 0.0f, graphicScale_);
        
    }
    
    CGFloat borderWidth = maxTextWidth + ((NOMINAL_AMOUNT_BORDER_HEIGHT + 2.0f) * multiplyingFactor);

    for (NXTGraphicAmountStringInformation *amountStringInfo in amountStringsInformationArray_) {
        
        amountStringInfo.borderWidth = borderWidth;
        
    }
    
    CGFloat borderHalfHeight = borderHeight / 2.0f;
    CGFloat borderMinXStraightLine = borderHalfHeight;
    CGFloat borderMaxXStraightLine = borderWidth - borderHalfHeight;

    CGPathRelease(amountBorderPath_);
    amountBorderPath_ = NULL;
    amountBorderPath_ = CGPathCreateMutable();
    
    CGPathMoveToPoint(amountBorderPath_, NULL, borderMaxXStraightLine, 0.0f);
    CGPathAddArcToPoint(amountBorderPath_, NULL, borderWidth, 0.0f, borderWidth, borderHalfHeight, borderHalfHeight);
    CGPathAddArcToPoint(amountBorderPath_, NULL, borderWidth, borderHeight, borderMaxXStraightLine, borderHeight, borderHalfHeight);
    CGPathAddLineToPoint(amountBorderPath_, NULL, borderMinXStraightLine, borderHeight);
    CGPathAddArcToPoint(amountBorderPath_, NULL, 0.0f, borderHeight, 0.0f, borderHalfHeight, borderHalfHeight);
    CGPathAddArcToPoint(amountBorderPath_, NULL, 0.0f, 0.0f, borderMinXStraightLine, 0.0f, borderHalfHeight);
    CGPathCloseSubpath(amountBorderPath_);
    
}

/*
 * Creates the graphic gradient
 */
- (void)createGraphicGradient {
    
    CGGradientRelease(graphicGradient_);
    graphicGradient_ = NULL;
    
    CGColorSpaceRef rgbColorspace = CGColorSpaceCreateDeviceRGB();

	CGFloat colors[8] = { 
		GRAPHIC_GRADIENT_TOP_COLOR_ARRAY,
        GRAPHIC_GRADIENT_BOTTOM_COLOR_ARRAY
	};
    
	graphicGradient_ = CGGradientCreateWithColorComponents(rgbColorspace, colors, NULL, 2);
    
    CGColorSpaceRelease(rgbColorspace);
        
}


/**
 * Creates the graphic limits smoothing gradient
 */
- (void)createGraphicLimitsSmoothingGradient {
    
    CGGradientRelease(graphicLimitsSmoothGradient_);
    graphicLimitsSmoothGradient_ = NULL;
    
    CGColorSpaceRef rgbColorspace = CGColorSpaceCreateDeviceRGB();
    
	CGFloat colors[8] = { 
		1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 0.0f
	};
    
	graphicLimitsSmoothGradient_ = CGGradientCreateWithColorComponents(rgbColorspace, colors, NULL, 2);
    
    CGColorSpaceRelease(rgbColorspace);
    
}

/*
 * Creates the callouts gradient
 */
- (void)createCalloutGradient {
    
    CGGradientRelease(calloutGradient_);
    calloutGradient_ = NULL;
    
    CGColorSpaceRef rgbColorspace = CGColorSpaceCreateDeviceRGB();
    
	CGFloat colors[8] = { 
		CALLOUT_TOP_BORDER_COLOR_ARRAY,
        CALLOUT_BOTTOM_BORDER_COLOR_ARRAY
	};
    
	calloutGradient_ = CGGradientCreateWithColorComponents(rgbColorspace, colors, NULL, 2);
    
    CGColorSpaceRelease(rgbColorspace);
    
}

/*
 * Creates the graphic paths (both the line path and the fill path)
 */
- (void)createGraphicPathsWithFirstDayXPosition:(CGFloat)aFirstDayXPosition
                                   controlWidth:(CGFloat)aControlWidth controlHeight:(CGFloat)aControlHeight {
    
    CGPathRelease(graphicLinePath_);
    graphicLinePath_ = nil;
    
    CGPathRelease(graphicFillPath_);
    graphicFillPath_ = nil;

    graphicLinePath_ = CGPathCreateMutable();
    
    NSInteger firstDayDisplayed = 0;
    
    if (aFirstDayXPosition < 0.0f) {
        
        firstDayDisplayed = (NSInteger)(floor(-aFirstDayXPosition / dayWidth_));
        
    }
    
    NSUInteger daysSpan = accountGraphicInformation_.daysSpan;
    NSInteger currentDay = firstDayDisplayed;
    CGFloat currentDayXLeftMostPosition = aFirstDayXPosition + (currentDay * dayWidth_);
    CGFloat firstDayXLeftMostPosition = currentDayXLeftMostPosition;
    NXTAccountGraphicDay *accountGraphicDay;
    NSDecimalNumber *dayBalanceNumber;
    CGFloat dayBalanceYPosition;
    
    while ((currentDay < daysSpan) && (currentDayXLeftMostPosition <= aControlWidth)) {
        
        currentDayXLeftMostPosition = aFirstDayXPosition + (currentDay * dayWidth_);
        
        accountGraphicDay = [accountGraphicInformation_ accountGraphicDayAtIndex:currentDay];
        dayBalanceNumber = accountGraphicDay.accountBalance;
        dayBalanceYPosition = AMOUNT_TO_Y([Tools convertDecimalNumber:dayBalanceNumber], yAxisZeroPosition_, 0.0f, graphicScale_);
        
        if (currentDay == firstDayDisplayed) {
            
            CGPathMoveToPoint(graphicLinePath_, NULL, currentDayXLeftMostPosition, dayBalanceYPosition);
            
        } else {
            
            CGPathAddLineToPoint(graphicLinePath_, NULL, currentDayXLeftMostPosition, dayBalanceYPosition);
            
        }
        
        if (currentDay == (daysSpan - 1)) {
            
            NSDecimalNumber *transactions = accountGraphicDay.transactionsGrandTotal;
            NSDecimalNumber *finalBalance = [dayBalanceNumber decimalNumberByAdding:transactions];
            currentDayXLeftMostPosition = aFirstDayXPosition + (daysSpan * dayWidth_);
            dayBalanceYPosition = AMOUNT_TO_Y([Tools convertDecimalNumber:finalBalance], yAxisZeroPosition_, 0.0f, graphicScale_);
            CGPathAddLineToPoint(graphicLinePath_, NULL, currentDayXLeftMostPosition, dayBalanceYPosition);

            
        }
        
        currentDay ++;
        
    }
    
    graphicFillPath_ = CGPathCreateMutableCopy(graphicLinePath_);
    CGPathAddLineToPoint(graphicFillPath_, NULL, currentDayXLeftMostPosition, aControlHeight);
    CGPathAddLineToPoint(graphicFillPath_, NULL, firstDayXLeftMostPosition, aControlHeight);
    CGPathCloseSubpath(graphicFillPath_);
    
}

/*
 * Creates the transactions callout path
 */
- (void)createTransactionsCalloutPath {
    
    CGPathRelease(transactionsCalloutPath_);
    transactionsCalloutPath_ = NULL;
    
    transactionsCalloutPath_ = CGPathCreateMutable();
    
    CGFloat multiplyingFactor = (multiplyingVerticalFactor_ < multiplyingHorizontalFactor_) ? multiplyingVerticalFactor_ : multiplyingHorizontalFactor_;
    CGFloat calloutRectangleHeight = NOMINAL_TRANSACTIONS_CALLOUT_RECTANGLE_HEIGHT * multiplyingFactor;
    CGFloat calloutRectangleWidth = NOMINAL_TRANSACTIONS_CALLOUT_RECTANGLE_WIDTH * multiplyingFactor;
    CGFloat downArrowHeight = NOMINAL_TRANSACTIONS_CALLOUT_DOWN_ARROW_HEIGHT * multiplyingFactor;
    CGFloat roundingCorners = NOMINAL_TRANSACTIONS_CALLOUT_ROUNDING_RADIUS * multiplyingFactor;
    
    CGFloat rightMostNotRounded = calloutRectangleWidth - roundingCorners;
    CGFloat leftMostNotRounded = roundingCorners;
    CGFloat topMostNotRounded = roundingCorners;
    CGFloat bottomMostNotRounded = calloutRectangleHeight - roundingCorners;
    CGFloat rectangleMidWidth = calloutRectangleWidth / 2.0f;
    CGFloat arrowLeftInsertion = rectangleMidWidth - downArrowHeight;
    CGFloat arrowRightInsertion = rectangleMidWidth + downArrowHeight;
    
    CGPathMoveToPoint(transactionsCalloutPath_, NULL, rightMostNotRounded, 0.0f);
    CGPathAddArcToPoint(transactionsCalloutPath_, NULL, calloutRectangleWidth, 0.0f, calloutRectangleWidth, topMostNotRounded, roundingCorners);
    CGPathAddLineToPoint(transactionsCalloutPath_, NULL, calloutRectangleWidth, bottomMostNotRounded);
    CGPathAddArcToPoint(transactionsCalloutPath_, NULL, calloutRectangleWidth, calloutRectangleHeight, rightMostNotRounded, calloutRectangleHeight, roundingCorners);
    CGPathAddLineToPoint(transactionsCalloutPath_, NULL, arrowRightInsertion, calloutRectangleHeight);
    CGPathAddLineToPoint(transactionsCalloutPath_, NULL, rectangleMidWidth, calloutRectangleHeight + downArrowHeight);
    CGPathAddLineToPoint(transactionsCalloutPath_, NULL, arrowLeftInsertion, calloutRectangleHeight);
    CGPathAddLineToPoint(transactionsCalloutPath_, NULL, leftMostNotRounded, calloutRectangleHeight);
    CGPathAddArcToPoint(transactionsCalloutPath_, NULL, 0.0f, calloutRectangleHeight, 0.0f, bottomMostNotRounded, roundingCorners);
    CGPathAddLineToPoint(transactionsCalloutPath_, NULL, 0.0f, topMostNotRounded);
    CGPathAddArcToPoint(transactionsCalloutPath_, NULL, 0.0f, 0.0f, leftMostNotRounded, 0.0f, roundingCorners);
    CGPathCloseSubpath(transactionsCalloutPath_);
    
}

/*
 * Returns the graphic date string for the day at the given lapse from the reference date
 */
- (NSString *)graphicStringForDateAtDays:(NSInteger)aDayLapse fromReference:(NSTimeInterval)aReferenceDate {

    NSTimeInterval newDay = aReferenceDate + (aDayLapse * 86400);
    NSDate *finalDate = [NSDate dateWithTimeIntervalSince1970:newDay];
    
    if (calendar_ == nil) {
        
        calendar_ = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [calendar_ setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        
    }
    
    if (monthsStrings_ == nil) {
        
        NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
        NSLocale *locale = [NSLocale currentLocale];
        [dateFormatter setLocale:locale];
        
        [monthsStrings_ release];
        monthsStrings_ = [[dateFormatter shortMonthSymbols] retain];
        
    }
    
    NSDateComponents *dateComponents = [calendar_ components:(NSMonthCalendarUnit |  NSDayCalendarUnit) fromDate:finalDate];
    NSInteger month = dateComponents.month - 1;
    NSString *monthString = nil;
    
    if ((month >= 0) && (month < [monthsStrings_ count])) {

        monthString = [[monthsStrings_ objectAtIndex:month] uppercaseString];
        
    }
    
    return [NSString stringWithFormat:@"%02d%@", dateComponents.day, monthString];
    
}

/*
 * Returns the callout date string for the day at the given lapse form the reference date
 */
- (NSString *)calloutStringForDateAtDays:(NSInteger)aDayLapse fromReference:(NSTimeInterval)aReferenceDate {
    
    NSTimeInterval newDay = aReferenceDate + (aDayLapse * 86400);
    NSDate *finalDate = [NSDate dateWithTimeIntervalSince1970:newDay];
    
    if (calendar_ == nil) {
        
        calendar_ = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [calendar_ setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        
    }
    
    if (monthsStrings_ == nil) {
        
        NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
        NSLocale *locale = [NSLocale currentLocale];
        [dateFormatter setLocale:locale];
        
        [monthsStrings_ release];
        monthsStrings_ = [[dateFormatter shortMonthSymbols] retain];
        
    }
    
    NSDateComponents *dateComponents = [calendar_ components:(NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit) fromDate:finalDate];
    
    return [NSString stringWithFormat:@"%02d/%02d/%02d", dateComponents.day, dateComponents.month, dateComponents.year % 100];

}

/*
 * Creates the grid vertical line image
 */
- (void)createGridVerticalLineImage {
    
    [gridVerticalDayLineImage_ release];
    gridVerticalDayLineImage_ = nil;
    
    [gridVerticalSubdayLineImage_ release];
    gridVerticalSubdayLineImage_ = nil;
    
    CGRect frame = self.frame;
    CGFloat height = CGRectGetMaxY(frame) - CGRectGetMinY(frame);
    CGSize lineSize = CGSizeMake(1.0f, height);
    
    UIGraphicsBeginImageContext(lineSize);
    CGContextRef dayContext = UIGraphicsGetCurrentContext();
    
    if (dayContext != NULL) {
        
        CGRect lineRect = CGRectMake(0.0f, 0.0f, 1.0f, height);

        maxGridGradientPercentage_ = mainDivision_ / height;
        maxGridGradientPercentage_ = (maxGridGradientPercentage_ < 0.2f) ? maxGridGradientPercentage_ : 0.2f;
        
        CGContextSetFillColorWithColor(dayContext, GRID_DAY_START_COLOR.CGColor);
        CGContextFillRect(dayContext, lineRect);

        CGColorSpaceRef rgbColorspace = CGColorSpaceCreateDeviceRGB();
        CGFloat colors[20] = { 
            1.0f, 1.0f, 1.0f, ALPHA_FOR_PERCENTAJE_POSITION(0.0f, maxGridGradientPercentage_),
            1.0f, 1.0f, 1.0f, ALPHA_FOR_PERCENTAJE_POSITION(maxGridGradientPercentage_, maxGridGradientPercentage_),
            1.0f, 1.0f, 1.0f, ALPHA_FOR_PERCENTAJE_POSITION(0.5f, maxGridGradientPercentage_),
            1.0f, 1.0f, 1.0f, ALPHA_FOR_PERCENTAJE_POSITION((1.0f - maxGridGradientPercentage_), maxGridGradientPercentage_),
            1.0f, 1.0f, 1.0f, ALPHA_FOR_PERCENTAJE_POSITION(1.0f, maxGridGradientPercentage_)
        };

        CGFloat points[5] = {
            0.0f, maxGridGradientPercentage_, 0.5f, 1.0f - maxGridGradientPercentage_, 1.0f
        };

        CGGradientRef gridGradient = CGGradientCreateWithColorComponents(rgbColorspace, colors, points, 5);
        
        CGContextSetBlendMode(dayContext, kCGBlendModeDestinationIn);
        CGContextDrawLinearGradient(dayContext, gridGradient, CGPointMake(0.0f, 0.0f), CGPointMake(0.0f, height), 0);
        
        [gridVerticalDayLineImage_ release];
        gridVerticalDayLineImage_ = [UIGraphicsGetImageFromCurrentImageContext() retain];
        
        CGContextSetBlendMode(dayContext, kCGBlendModeNormal);
        CGContextSetFillColorWithColor(dayContext, GRID_NOT_MAIN_LINES_COLOR(1.0f).CGColor);
        CGContextFillRect(dayContext, lineRect);
        
        CGContextSetBlendMode(dayContext, kCGBlendModeDestinationIn);
        CGContextDrawLinearGradient(dayContext, gridGradient, CGPointMake(0.0f, 0.0f), CGPointMake(0.0f, height), 0);
        
        [gridVerticalSubdayLineImage_ release];
        gridVerticalSubdayLineImage_ = [UIGraphicsGetImageFromCurrentImageContext() retain];
        UIGraphicsEndImageContext();
        
        CGColorSpaceRelease(rgbColorspace);
        CGGradientRelease(gridGradient);

    }    
    
}

#pragma mark -
#pragma mark Setting the information to display

/*
 * Displays the graphic for the given graphic information. The information stored in the instance is copied
 */
- (void)displayGraphicForInformation:(NXTAccountGraphicInformation *)anAccountGraphicInformation withCurrency:(NSString *)aCurrencySymbol
                     currencyFollows:(BOOL)currencyFollows {
    
    selectedDay_ = anAccountGraphicInformation.daysSpan - 1;
    transactionsCalloutVisible_ = YES;
    
    [accountGraphicInformation_ release];
    accountGraphicInformation_ = nil;
    accountGraphicInformation_ = [anAccountGraphicInformation copyWithZone:self.zone];
    
    [currencySymbol_ release];
    currencySymbol_ = nil;
    currencySymbol_ = [aCurrencySymbol copy];
    
    currencyFollows_ = currencyFollows;
    
    [self calculateGraphicLayout];
    [self setNeedsDisplay];
	
	canNotifyDownload_ = YES;
    
}


/*
 * Adds graphic information provided to the given graphic information and displays it
 */
- (void)addGraphicInformationToGraphic:(NXTAccountGraphicInformation *)anAccountGraphicInformation {
    
	if (anAccountGraphicInformation != nil) {

		CGRect bounds = scrollView_.bounds;
		CGSize contentSize = scrollView_.contentSize;
		NSInteger selectedDayToTimeSpan = -1;
		
		if (selectedDay_ > -1) {
			
			selectedDayToTimeSpan = accountGraphicInformation_.daysSpan - selectedDay_;
			
		}
		
		CGFloat distanceToRightBorder = contentSize.width - CGRectGetMinX(bounds);
		
		[accountGraphicInformation_ addAccountGraphicInformation:anAccountGraphicInformation];
		
		[self calculateGraphicLayout];
		
		contentSize = scrollView_.contentSize;
		bounds = scrollView_.bounds;
		scrollView_.bounds = CGRectMake(contentSize.width - distanceToRightBorder, bounds.origin.y, bounds.size.width, bounds.size.height);
		
		if (selectedDayToTimeSpan > -1) {
			
			selectedDay_ = accountGraphicInformation_.daysSpan - selectedDayToTimeSpan;
			
		}
		
		[self setNeedsDisplay];
		
	}
	
	canNotifyDownload_ = YES;
    
}

#pragma mark -
#pragma mark Graphic maths

/*
 * Caculates the graphic layout from the stored information
 */
- (void)calculateGraphicLayout {
    
    [self analyzeAccountInformationToDisplay];
    [self calculateGraphicParameters];
    [self setScrollParameters];
    
}

/*
 * Calculates the multiplying factors
 */
- (void)calculateMultiplyingFactors {
    
    CGRect frame = self.frame;
    CGFloat width = CGRectGetMaxX(frame) - CGRectGetMinX(frame);
    CGFloat height = CGRectGetMaxY(frame) - CGRectGetMinY(frame);
    
    multiplyingHorizontalFactor_ = width / NOMINAL_GRAPHIC_WIDTH;
    multiplyingVerticalFactor_ = height / NOMINAL_GRAPHIC_HEIGHT;
    
    multiplyingVerticalFactor_ = (multiplyingVerticalFactor_ < 1.0f) ? multiplyingVerticalFactor_ : 1.0f;
    multiplyingHorizontalFactor_ = (multiplyingHorizontalFactor_ < 1.0f) ? multiplyingHorizontalFactor_ : 1.0f;
    
}

/*
 * Obtains and stores the information relevant valules
 */
- (void)analyzeAccountInformationToDisplay {
    
    NSUInteger daysSpan = accountGraphicInformation_.daysSpan;
    
    if (daysSpan > 0) {
        
        CGFloat maxBalance = -CGFLOAT_MAX;
        CGFloat minBalance = CGFLOAT_MAX;
        
        CGFloat maxTransactionsGrandTotal = -CGFLOAT_MAX;
        CGFloat minTransactionsGrandTotal = CGFLOAT_MAX;
        
        CGFloat balance;
        CGFloat transactionsGrandTotal;
        
        for (NSUInteger i = 0; i < daysSpan; i++) {
            
            NXTAccountGraphicDay *graphicDay = [accountGraphicInformation_ accountGraphicDayAtIndex:i];
            
            balance = [Tools convertDecimalNumber:graphicDay.accountBalance];
            transactionsGrandTotal = [Tools convertDecimalNumber:graphicDay.transactionsGrandTotal];
            
            if (balance > maxBalance) {
                
                maxBalance = balance;
                
            }
            
            if (balance < minBalance) {
                
                minBalance = balance;
                
            }
            
            if (transactionsGrandTotal > maxTransactionsGrandTotal) {
                
                maxTransactionsGrandTotal = transactionsGrandTotal;
                
            }
            
            if (transactionsGrandTotal < minTransactionsGrandTotal) {
                
                minTransactionsGrandTotal = transactionsGrandTotal;
                
            }
            
        }
        
        minBalance_ = (minBalance < 0.0f) ? minBalance : 0.0f;
        maxBalance_ = (maxBalance > 0.0f) ? maxBalance : 0.0f;
        
        minTransactionsGrandTotal = (CGFloat)fabs(minTransactionsGrandTotal);
        maxTransactionsGrandTotalRange_ = (maxTransactionsGrandTotal > minTransactionsGrandTotal) ? maxTransactionsGrandTotal : minTransactionsGrandTotal;
        
    } else {
        
        minBalance_ = 0.0f;
        maxBalance_ = 0.0f;
        maxTransactionsGrandTotalRange_ = 0.0f;
        
    }
    
}

/*
 * Calculates the graphic parameters (anchor points positions, and widths)
 */
- (void)calculateGraphicParameters {
    
    CGRect frame = self.frame;
    CGFloat width = CGRectGetMaxX(frame) - CGRectGetMinX(frame);
    CGFloat height = CGRectGetMaxY(frame) - CGRectGetMinY(frame);
    
    dayWidth_ = width / 8.0f;

    NSInteger totalDays = accountGraphicInformation_.daysSpan - 1;
    
    if (totalDays < 1) {
        
        totalDays = 1;
        
    }
    
    scrollContentWidth_ = (dayWidth_ * totalDays) + width;
    
    CGFloat generalMultiplyingFactor = (multiplyingVerticalFactor_ < multiplyingHorizontalFactor_) ? multiplyingVerticalFactor_ : multiplyingHorizontalFactor_;
    CGFloat balanceCallOutHeight = NOMINAL_BALANCE_CALLOUT_HEIGHT * generalMultiplyingFactor;
    CGFloat transactionsCallOutHeight = NOMINAL_TRANSACTIONS_CALLOUT_HEIGHT * generalMultiplyingFactor;
    CGFloat balancePointRadius = BALANCE_DAY_CIRCLE_RADIUS * generalMultiplyingFactor;
    CGFloat topAndBottomPadding = TOP_AND_BOTTOM_PADDING * multiplyingVerticalFactor_;
    CGFloat maxTransactionsGrandTotalHeight = MAX_TRANSACTIONS_GRAND_TOTAL_HEIGHT * multiplyingVerticalFactor_;
    
    //The zero position limited by the transactions grand total graphic element
    CGFloat minTransactionsZeroPosition = maxTransactionsGrandTotalHeight + topAndBottomPadding;
    CGFloat maxTransactionsZeroPosition = height - maxTransactionsGrandTotalHeight - transactionsCallOutHeight - topAndBottomPadding;
    
    CGFloat maxBalanceTop = height - balanceCallOutHeight - balancePointRadius - topAndBottomPadding;
    CGFloat minBalanceTop = topAndBottomPadding + balancePointRadius;
    CGFloat balanceAvailableHeight = maxBalanceTop - minBalanceTop;
    
    CGFloat balanceRange = maxBalance_ - minBalance_;
    
    if (balanceRange == 0.0f) {
        
        yAxisZeroPosition_ = (maxBalance_ >= 0.0f) ? (height - minTransactionsZeroPosition) : (height - maxTransactionsZeroPosition);
        
        if (maxBalance_ != 0.0f) {

            maxBalanceYPosition_ = height - (balanceAvailableHeight / 2.0f);
            graphicScale_ = (yAxisZeroPosition_ - maxBalanceYPosition_) / maxBalance_;
            minBalanceYPosition_ = maxBalanceYPosition_;
            CGFloat auxMainDivision = (maxBalance_ > 0.0f) ? (yAxisZeroPosition_ / graphicScale_ / 5.0f) : (maxTransactionsZeroPosition / graphicScale_ / 5.0f);
            mainDivision_ = [Tools roundToOneNonZeroDigit:auxMainDivision];
            
        } else {
            
            maxBalanceYPosition_ = yAxisZeroPosition_;
            minBalanceYPosition_ = maxBalanceYPosition_;
            graphicScale_ = yAxisZeroPosition_ / 10.0f;
            mainDivision_ = 2.0f;
            
        }
        
        
    } else {
        
        CGFloat maxBalanceToZeroHeight = (maxBalance_ * balanceAvailableHeight) / balanceRange;
        CGFloat balanceZeroPosition = maxBalanceTop - maxBalanceToZeroHeight;
        
        if (balanceZeroPosition < minTransactionsZeroPosition) {
            
            yAxisZeroPosition_ = height - minTransactionsZeroPosition;
            maxBalanceYPosition_ = height - maxBalanceTop;
            graphicScale_ = (yAxisZeroPosition_ - maxBalanceYPosition_) / maxBalance_;
            minBalanceYPosition_ = maxBalanceYPosition_ + (graphicScale_ * balanceRange);
            mainDivision_ = [Tools roundToOneNonZeroDigit:(yAxisZeroPosition_ / graphicScale_ / 5.0f)];
            
        } else if (balanceZeroPosition > maxTransactionsZeroPosition) {
            
            yAxisZeroPosition_ = height - maxTransactionsZeroPosition;
            minBalanceYPosition_ = height - minBalanceTop;
            graphicScale_ = (minBalanceYPosition_ - yAxisZeroPosition_) / fabs(minBalance_);
            maxBalanceYPosition_ = minBalanceYPosition_ - (graphicScale_ * balanceRange);
            mainDivision_ = [Tools roundToOneNonZeroDigit:(maxTransactionsZeroPosition / graphicScale_ / 5.0f)];
            
        } else {
            
            yAxisZeroPosition_ = height - balanceZeroPosition;
            maxBalanceYPosition_ = height - maxBalanceTop;
            minBalanceYPosition_ = height - minBalanceTop;
            graphicScale_ = balanceAvailableHeight / balanceRange;
            mainDivision_ = [Tools roundToOneNonZeroDigit:(balanceRange / 5.0f)];
            
        }
        
    }
    
    [gridVerticalDayLineImage_ release];
    gridVerticalDayLineImage_ = nil;
    [gridVerticalSubdayLineImage_ release];
    gridVerticalSubdayLineImage_ = nil;
    [self createGridVerticalLineImage];
    [self createAmountStringsInformation];
    
}

/*
 * Sets the scroll parameters, once they are calculated
 */
- (void)setScrollParameters {
    
    CGRect frame = self.frame;
    CGFloat controlWidth = CGRectGetMaxX(frame) - CGRectGetMinX(frame);
    CGFloat height = CGRectGetMaxY(frame) - CGRectGetMinY(frame);
    CGSize contentSize = CGSizeMake(scrollContentWidth_, height);
    scrollView_.contentSize = contentSize;
    scrollView_.contentOffset = CGPointMake(contentSize.width - controlWidth - (controlWidth / 2) + (controlWidth / 16), 0.0f);
    
}

/*
 * Sets the tap view frame
 */
- (void)setTapViewFrame {
    
    CGRect frame = self.frame;
    CGRect scrollBounds = scrollView_.bounds;
    frame.origin.x = CGRectGetMinX(scrollBounds);
    frame.origin.y = 0.0f;
    touchDetectorView_.frame = frame;
    
}

/*
 * Returns the central day as an NSDate. If the date is out of range, it returns nil
 */
- (NSDate *)centralDayAsNSDate {
    
    NSDate *result = nil;

    CGPoint contentOffset = scrollView_.contentOffset;
    CGRect frame = self.frame;
    CGFloat width = CGRectGetMaxX(frame) - CGRectGetMinX(frame);
    CGFloat firstDayXPosition = ((width - dayWidth_) / 2.0f) - contentOffset.x;
    CGFloat controlMidWidth = width / 2.0f;
    NSUInteger daysSpan = accountGraphicInformation_.daysSpan;
    NSInteger centerDay = (NSInteger)(floor((controlMidWidth - firstDayXPosition) / dayWidth_));
    
    if ((centerDay >= 0) && (centerDay < daysSpan)) {
        
        NSTimeInterval dayTimeInterval = accountGraphicInformation_.firstDay + (centerDay * 86400.0f);
        result = [NSDate dateWithTimeIntervalSince1970:dayTimeInterval];
        
    }
    
    return result;

}

#pragma mark -
#pragma mark User interactions

/*
 * The view has detected that it has been tapped inside
 */
- (void)viewTappedAtPoint:(CGPoint)aPoint {
    
    BOOL continueChecking = YES;

    if ([self checkViewTappedOnTransactionsCalloutAtPoint:aPoint]) {
        
        NSDate *centralDate = [self centralDayAsNSDate];
        
        if (centralDate != nil) {
            
            [delegate_ accountGraphic:self transactionCalloutTappedOnDate:centralDate];
            continueChecking = NO;
            
        }
        
    }
    
    if (continueChecking) {
        
        CGRect frame = self.frame;
        CGFloat width = CGRectGetMaxX(frame) - CGRectGetMinX(frame);
        CGPoint contentOffset = scrollView_.contentOffset;
        CGFloat firstDayXPosition = ((width - dayWidth_) / 2.0f) - contentOffset.x;
        
        NSInteger selectedDay = [self checkViewTappedOnDayBalanceCircleAtPoint:aPoint withFirstDayXPosition:firstDayXPosition
                                                                  controlWidth:width];
        
        if (selectedDay != -1) {
            
            if (self.selectedDay == selectedDay) {
                
                self.selectedDay = -1;
                
            } else {
                
                self.selectedDay = selectedDay;
                
            }
            
        } else if ([self checkViewTappedOnTransactionsBandAtPoint:aPoint]) {
            
            self.transactionsCalloutVisible = !self.transactionsCalloutVisible;
            
        }
        
    }
    
}

/*
 * Checks whether the view as tapped on the transactions callout
 */
- (BOOL)checkViewTappedOnTransactionsCalloutAtPoint:(CGPoint)aTapPoint {
    
    BOOL result = NO;
    
    if (transactionsCalloutVisible_) {
        
        CGFloat pointX = aTapPoint.x;
        CGFloat pointY = aTapPoint.y;
        
        if ((CGRectGetMaxX(transactionsCalloutRect_) >= pointX) && (CGRectGetMinX(transactionsCalloutRect_) <= pointX) &&
            (CGRectGetMaxY(transactionsCalloutRect_) >= pointY) && (CGRectGetMinY(transactionsCalloutRect_) <= pointY)) {
            
            result = YES;
            
        }
        
    }
    
    return result;
    
}

/*
 * Checks whether view was tapped on a day balance circle
 */
- (NSInteger)checkViewTappedOnDayBalanceCircleAtPoint:(CGPoint)aTapPoint withFirstDayXPosition:(CGFloat)aFirstDayXPosition
                                         controlWidth:(CGFloat)aControlWidth {
    
    NSInteger result = -1;

    NSInteger currDay = 0;
    
    if (aFirstDayXPosition < 0.0f) {
        
        currDay = (NSInteger)floor(-aFirstDayXPosition / dayWidth_);
        
    }
    
    CGFloat pointX = aTapPoint.x;
    CGFloat pointY = aTapPoint.y;
    
    NSInteger daysSpan = accountGraphicInformation_.daysSpan;
    
    CGFloat currentDayXPos = aFirstDayXPosition + (currDay * dayWidth_);
    CGFloat balance;
    CGFloat balanceYPos;
    NXTAccountGraphicDay *accountGraphicDay;
    
    while ((currentDayXPos < aControlWidth) && (currDay < daysSpan)) {
        
        if (((currentDayXPos - DAY_TAP_DETECTION_SEMIWIDTH) <= pointX) && ((currentDayXPos + DAY_TAP_DETECTION_SEMIWIDTH) >= pointX)) {
                
            accountGraphicDay = [accountGraphicInformation_ accountGraphicDayAtIndex:currDay];
            balance = [Tools convertDecimalNumber:accountGraphicDay.accountBalance];
            balanceYPos = AMOUNT_TO_Y(balance, yAxisZeroPosition_, 0.0f, graphicScale_);
            
            if (((balanceYPos - DAY_TAP_DETECTION_SEMIWIDTH) <= pointY) && ((balanceYPos + DAY_TAP_DETECTION_SEMIWIDTH) > pointY)) {
                
                if (currDay > 0) {
                
                    result = currDay;
                    
                }

                break;
                
            }
            
        }
        
        currDay++;
        currentDayXPos = aFirstDayXPosition + (currDay * dayWidth_);
        
    }
    
    if (result < 0) {
        
        CGFloat maxTransactionsGrandTotalHeight = MAX_TRANSACTIONS_GRAND_TOTAL_HEIGHT * multiplyingVerticalFactor_;
        
        if ((yAxisZeroPosition_ - maxTransactionsGrandTotalHeight) > pointY) {
            
            result = selectedDay_;
            
        }
        
    }
    
    return result;
    
}

/*
 * Checks whether the view as tapped on the transactions band
 */
- (BOOL)checkViewTappedOnTransactionsBandAtPoint:(CGPoint)aTapPoint {
    
    BOOL result = NO;
    
    CGFloat pointY = aTapPoint.y;
    
    CGFloat maxTransactionsGrandTotalHeight = MAX_TRANSACTIONS_GRAND_TOTAL_HEIGHT * multiplyingVerticalFactor_;

    if (((yAxisZeroPosition_ - maxTransactionsGrandTotalHeight) <= pointY) && ((yAxisZeroPosition_ + maxTransactionsGrandTotalHeight) >= pointY)) {
        
        result = YES;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Activity indicator management

/*
 * Displays the activity indicator
 */
- (void)displayActivityIndicator {
    
    if (downloadDataView_.superview != nil) {
        
        [downloadDataView_ removeFromSuperview];
        
    }
    
    CGRect viewFrame = self.frame;
    downloadDataView_.center = CGPointMake(ACTIVITY_INDICATOR_DISTANCE_TO_LEFT_BORDER,
                                           (CGRectGetMaxY(viewFrame) - CGRectGetMinY(viewFrame)) / 2.0f);
    [scrollView_ addSubview:downloadDataView_];
    [downloadDataView_ startAnimating];
    
}

/*
 * Hides the activirt indicator
 */
- (void)hideActivityIndicator {

    [downloadDataView_ stopAnimating];
    [downloadDataView_ removeFromSuperview];
    
}

#pragma mark -
#pragma mark UIView methods

/**
 * Sets the new view frame
 *
 * @param frame The view frame to set
 */
- (void)setFrame:(CGRect)frame {
    
    [super setFrame:frame];
    
    CGFloat width = CGRectGetMaxX(frame) - CGRectGetMinX(frame);
    CGFloat height = CGRectGetMaxY(frame) - CGRectGetMinY(frame);
    [scrollView_ setFrame:CGRectMake(0.0f, 0.0f, width, height)];
    [self calculateMultiplyingFactors];
    [self calculateGraphicParameters];
    [self setScrollParameters];
    [self setTapViewFrame];
    [self createTransactionsCalloutPath];
    [self setNeedsDisplay];
    
}

#pragma mark -
#pragma mark UIScrollViewDelegate methods

/**
 * Tells the delegate when the scroll view is about to start scrolling the content.
 *
 * @param scrollView The scroll-view object that is decelerating the scrolling of the content view.
 */
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    canRotate_ = NO;
}

/**
 * Tells the delegate that the scroll view has ended decelerating the scrolling movement.
 *
 * @param scrollView The scroll-view object that is decelerating the scrolling of the content view.
 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    canRotate_ = YES;
}

/**
 * Tells the delegate when dragging ended in the scroll view.
 *
 * @param scrollView The scroll-view object that is decelerating the scrolling of the content view.
 * @param decelerate The decelerate flag.
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//    if (!decelerate) {
        canRotate_ = YES;
//    }
}

/**
 * Tells the delegate when the user scrolls the content view within the receiver. The control is redrawn
 *
 * @param scrollView The scroll-view object in which the scrolling occurred
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView.bounds.origin.x < LEFT_SCROLL_TO_NOTIFY_DOWNLOAD) {
        
        if (canNotifyDownload_) {
            
            [delegate_ startAdditionalDownloadForAccountGraphic:self];
            canNotifyDownload_ = NO;
            
        }
        
    } else {
        
//        canNotifyDownload_ = YES;
        
    }
    
    [self setTapViewFrame];
    [self setNeedsDisplay];
    
}

#pragma mark -
#pragma mark Properties methods

/**
 * Sets the new selected day. When new selected day is different to current selected day, the control is marked to be redrawn
 *
 * @param aSelectedDay The new selected day
 */
- (void)setSelectedDay:(NSInteger)aSelectedDay {
    
    if (aSelectedDay != selectedDay_) {
        
        selectedDay_ = aSelectedDay;
        [self setNeedsDisplay];
        
    }
    
}

/**
 * Sets the new transactions callout visible flag. When new value is different to current value, the control is marked to be redrawn
 *
 * @param aValue The new transactions callout visible flag
 */
- (void)setTransactionsCalloutVisible:(BOOL)aValue {

    if (aValue != transactionsCalloutVisible_) {
        
        transactionsCalloutVisible_ = aValue;
        [self setNeedsDisplay];
        
    }
    
}

/*
 * Disables the scrolling. Used when the user rotates the device.
 * Cancels the drawing of the view
 */
- (void)disableScroll {
    self.userInteractionEnabled = NO;
    scrollView_.userInteractionEnabled = NO;
    [scrollView_ scrollRectToVisible:scrollView_.frame animated:NO];
}

/*
 * Enables scroll
 */
- (void)enableScroll {
    canRotate_ = YES;
    self.userInteractionEnabled = YES;
    scrollView_.userInteractionEnabled = YES;
}

/*
 * Returns the fist day displayed
 *
 * @return The first day displayed
 */
- (NSTimeInterval)firstDayDisplayed {

    return accountGraphicInformation_.firstDay;
    
}

/*
 * Returns the graphic initial balance
 *
 * @return The graphic initial balance
 */
- (NSDecimalNumber *)initialBalance {
    
    NSDecimalNumber *result = [NSDecimalNumber zero];
    
    NXTAccountGraphicDay *graphicDay = [accountGraphicInformation_ accountGraphicDayAtIndex:0];
    
    if (graphicDay != nil) {
        
        result = graphicDay.accountBalance;
        
    }
    
    return result;
    
}

@end
