/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <UIKit/UIKit.h>


//Forward declarations
@class NXTAccountGraphic;
@class NXTAccountGraphicInformation;
@class AccountGraphicTouchDetectorView;


/**
 * NXT account graphic delegate
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol NXTAccountGraphicDelegate

/**
 * Notifies the delegate that the transactions callout was tapped for the given date
 *
 * @param anAccountGraphic The NXTAccountGraphic that triggered the event
 * @param aDate The date where the callout was over
 */
- (void)accountGraphic:(NXTAccountGraphic *)anAccountGraphic transactionCalloutTappedOnDate:(NSDate *)aDate;

/**
 * Notifies the delegate that an additional download was requested
 *
 * @param anAccountGraphic The NXTAccountGraphic triggering the event
 */
- (void)startAdditionalDownloadForAccountGraphic:(NXTAccountGraphic *)anAccountGraphic;

@end



/**
 * Displays an scrollable graphic representing a dayly account balance
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTAccountGraphic : UIView <UIScrollViewDelegate> {

@private
    
    /**
     * Information to display
     */
    NXTAccountGraphicInformation *accountGraphicInformation_;
    
    /**
     * Currency symbol
     */
    NSString *currencySymbol_;
    
    /**
     * Currency follows the amount flag. If YES, the currency symbol is displayed after the amount (i.e. 10€), otherwise, the currency symbol is displayed before the amount (i.e. $10)
     */
    BOOL currencyFollows_;
    
    /**
     * Scroll view that notifies the scrolling events
     */
    UIScrollView *scrollView_;
    
    /**
     * View to detect touches
     */
    AccountGraphicTouchDetectorView *touchDetectorView_;
    
    /**
     * Activity indicator to display when downloading more information
     */
    UIActivityIndicatorView *downloadDataView_;
    
    /**
     * Account graphic delegate
     */
    id<NXTAccountGraphicDelegate> delegate_;
    
    /**
     * Multiplying vertical factor. When the current width is equal to the nominal width, the multiplying factor is 1.0f. When the current width is smaller than
     * the nominal width, the multiplying vertical factor is the ratio between the current and nominal widths
     */
    CGFloat multiplyingVerticalFactor_;
    
    /**
     * Multiplying horizontal factor. When the current height is equal to the nominal height, the multiplying factor is 1.0f. When the current height is smaller than
     * the nominal height, the multiplying horizontal factor is the ratio between the current and nominal heights
     */
    CGFloat multiplyingHorizontalFactor_;
    
    /**
     * Graphic information maximum balance
     */
    CGFloat maxBalance_;
    
    /**
     * Graphic information minimum balance
     */
    CGFloat minBalance_;
    
    /**
     * Transfers maximum range. The original information could be a positive or negative amount, but the information stored is a positive value
     */
    CGFloat maxTransactionsGrandTotalRange_;
    
    /**
     * Y-axis zero position (measured in points from the view top)
     */
    CGFloat yAxisZeroPosition_;
    
    /**
     * Graphic scale. It stores the pixels per information unit
     */
    CGFloat graphicScale_;
    
    /**
     * Maximum balance y-axis position (measured in points from the view top)
     */
    CGFloat maxBalanceYPosition_;
    
    /**
     * Minimum balance y-axis position (measured in points from the view top)
     */
    CGFloat minBalanceYPosition_;
    
    /**
     * Day width (measured in points)
     */
    CGFloat dayWidth_;
    
    /**
     * Scroll content width
     */
    CGFloat scrollContentWidth_;
    
    /**
     * Amount main division (measured in the information magnitude). Main division lines are represented with a dark gray dotted line and they are separated by mainDivision_ magnitudes from the next and previous
     * division lines
     */
    CGFloat mainDivision_;
    
    /**
     * Contains the grid day vertical line image. This image has a 1 point with and a height equal to the screen height in landscape
     */
    UIImage *gridVerticalDayLineImage_;
    
    /**
     * Contains the grid sub-day vertical line image. This image has a 1 point with and a height equal to the screen height in landscape
     */
    UIImage *gridVerticalSubdayLineImage_;
    
    /**
     * View height percentage where grid gradient is more intense. This percentage is applied both at the view top and at the view bottom
     */
    CGFloat maxGridGradientPercentage_;
    
    /**
     * Calendar to obtain the days components
     */
    NSCalendar *calendar_;
    
    /**
     * Months short strings
     */
    NSArray *monthsStrings_;
    
    /**
     * Array containing the amount texts to display, along with their width and top border position information
     */
    NSMutableArray *amountStringsInformationArray_;
    
    /**
     * Day line bottom position, including bottom offset, measured in pixes from the view top
     */
    CGFloat dayLineBottomPosition_;
    
    /**
     * Amount text font
     */
    UIFont *amountTextFont_;
    
    /**
     * Amount border path
     */
    CGMutablePathRef amountBorderPath_;
    
    /**
     * Graphic gradient used for fill the graphic
     */
    CGGradientRef graphicGradient_;
    
    /**
     * Graphic limiting dates smoothing gradient
     */
    CGGradientRef graphicLimitsSmoothGradient_;
    
    /**
     * Path to draw the graphic line
     */
    CGMutablePathRef graphicLinePath_;
    
    /**
     * Path to fill the graphic
     */
    CGMutablePathRef graphicFillPath_;
    
    /**
     * Callouts fill gradient
     */
    CGGradientRef calloutGradient_;
    
    /**
     * Transactions callout path
     */
    CGMutablePathRef transactionsCalloutPath_;
    
    /**
     * Transactions callout rectangle. When no callout is displayed, or the day has no associated transactions, the rect is set outside the view
     */
    CGRect transactionsCalloutRect_;
    
    /**
     * Selected day balance from the graphic first day. If no day balance is selected, the value stored is -1
     */
    NSInteger selectedDay_;
    
    /**
     * Transactions callout displayed flag. When YES, the transactions callout is displayed and the central transaction element is displayed in blue color
     */
    BOOL transactionsCalloutVisible_;
    
    /**
     * Flag that indicates if the view can be rotated
     */
    BOOL canRotate_;
    
    /**
     * Can notify an additional download flag. YES when an additional download can be notified, NO otherwise. This flag is used
     * for sending only one additional download notification to the delegate while the scroller is located in the notification region
     */
    BOOL canNotifyDownload_;
    
}

/**
 * Provides read-write access to the flag that indicates if the view can be rotated
 */
@property (nonatomic, readwrite, assign) BOOL canRotate;

/**
 * Provides read-write access to the account graphic delegate and exports it for Interface Builder
 */
@property (nonatomic, readwrite, assign) IBOutlet id<NXTAccountGraphicDelegate> delegate;

/**
 * Provides read-write access to the selected day
 */
@property (nonatomic, readwrite) NSInteger selectedDay;

/**
 * Provides read-write access to the transactions callout displayed flag
 */
@property (nonatomic, readwrite) BOOL transactionsCalloutVisible;

/**
 * Provides read-only access to the first day displayed, represented as seconds from 01/01/1970
 */
@property (nonatomic, readonly) NSTimeInterval firstDayDisplayed;

/**
 * Provides read-only access to the initial balance
 */
@property (nonatomic, readonly) NSDecimalNumber *initialBalance;


/**
 * Displays the graphic for the given graphic information. The information stored in the instance is copied
 *
 * @param anAccountGraphicInformation The account graphic information to display
 * @param aCurrencySymbol The currency symbol to display with all the amounts
 * @param currencyFollows YES to display the currency to the amount right (i.e. 10,00€), NO to display the currency to the amount left (i.e. $10.00)
 */
- (void)displayGraphicForInformation:(NXTAccountGraphicInformation *)anAccountGraphicInformation withCurrency:(NSString *)aCurrencySymbol
                     currencyFollows:(BOOL)currencyFollows;

/**
 * Adds graphic information provided to the given graphic information and displays it
 *
 * @param anAccountGraphicInformation The account graphic information to display
 */
- (void)addGraphicInformationToGraphic:(NXTAccountGraphicInformation *)anAccountGraphicInformation;

/**
 * Disables the scrolling. Used when the user rotates the device.
 * Cancels the drawing of the view
 */
- (void)disableScroll;

/**
 * Enables scroll
 */
- (void)enableScroll;

/**
 * Displays the activity indicator
 */
- (void)displayActivityIndicator;

/**
 * Hides the activirt indicator
 */
- (void)hideActivityIndicator;

@end
