/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXTGraphicsCommon.h"


#pragma mark -

@implementation NXTGraphicAmountStringInformation

#pragma mark -
#pragma mark Properties

@synthesize amountString = amountString_;
@synthesize stringWidth = stringWidth_;
@synthesize stringHeight = stringHeight_;
@synthesize borderTopPosition = borderTopPosition_;
@synthesize borderWidth = borderWidth_;
@synthesize borderHeight = borderHeight_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [amountString_ release];
    amountString_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. Initializes an empty NXTAccountGraphicAccountStringInformation
 *
 * @return The initialized NXTAccountGraphicAccountStringInformation
 */
- (id)init {
    
    return [self initWithAmountString:nil stringWidth:0.0f stringHeight:0.0f borderTopPosition:0.0f borderWidth:0.0f borderHeight:0.0f];
    
}

/*
 * Designated initializer. Initializes an NXTAccountGraphicAccountStringInformation with the provided information
 */
- (id)initWithAmountString:(NSString *)anAmountString stringWidth:(CGFloat)aStringWidth
              stringHeight:(CGFloat)aStringHeight borderTopPosition:(CGFloat)aBorderTopPosition
               borderWidth:(CGFloat)aBorderWidth borderHeight:(CGFloat)aBorderHeight {
    
    if (self = [super init]) {
        
        [amountString_ release];
        amountString_ = [anAmountString copy];
        
        stringWidth_ = aStringWidth;
        stringHeight_ = aStringHeight;
        borderTopPosition_ = aBorderTopPosition;
        borderWidth_ = aBorderWidth;
        borderHeight_ = aBorderHeight;
        
    }
    
    return self;
    
}

@end
