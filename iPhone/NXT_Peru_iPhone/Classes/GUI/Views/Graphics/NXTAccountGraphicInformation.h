/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>


/**
 * Class to contain the information related to an account graphic single day
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTAccountGraphicDay : NSObject <NSCopying> {
    
@private
    
    /**
     * Graphic element day represented as the time interval since Jan 1 1970 (measured in seconds)
     */
    NSTimeInterval dayTimeInterval_;
    
    /**
     * Account day balance
     */
    NSDecimalNumber *accountBalance_;
    
    /**
     * Account day transactions grand total
     */
    NSDecimalNumber *transactionsGrandTotal_;
    
    /**
     * Has transactions flag. YES to denote that the account graphic day has transaccions associated
     */
    BOOL hasTransactions_;
    
}

/**
 * Provides read-only access to the graphic element day represented as the time interval since Jan 1 1970 (measured in seconds)
 */
@property (nonatomic, readonly) NSTimeInterval dayTimeInterval;

/**
 * Provides read-only access to the account day balance
 */
@property (nonatomic, readonly, retain) NSDecimalNumber *accountBalance;

/**
 * Provides read-only access to the account day transactions grand total
 */
@property (nonatomic, readonly, retain) NSDecimalNumber *transactionsGrandTotal;

/**
 * Provides read-only access to the end of day balance (balance and transactions grand total addition)
 */
@property (nonatomic, readonly, retain) NSDecimalNumber *endOfDayAccountBalance;

/**
 * Provides read-only access to the has transactions flag
 */
@property (nonatomic, readonly) BOOL hasTransactions;

@end


/**
 * Contains the information needed to render the account graphic
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTAccountGraphicInformation : NSObject <NSCopying> {

@private
    
    /**
     * Array containing all information objects ordered in date order
     */
    NSMutableArray *informationArray_;
    
}

/**
 * Provides read-only access to the days span stored. That is, the number of days between the first and the last day stored
 */
@property (nonatomic, readonly) NSUInteger daysSpan;

/**
 * Provides read-only access to the first day stored (represented as a time interval since Jan, 1 1970 measured in seconds)
 */
@property (nonatomic, readonly) NSTimeInterval firstDay;

/**
 * Provides read-only access to the last day stored (represented as a time interval since Jan, 1 1970 measured in seconds)
 */
@property (nonatomic, readonly) NSTimeInterval lastDay;

/**
 * Returns the graphic information day at the provided position. The index must be 0 <= index < daysCount, or a nil instance will be returned
 *
 * @param index The graphic information day index
 * @return The graphic information day at the provided position or nil if out of bounds
 */
- (NXTAccountGraphicDay *)accountGraphicDayAtIndex:(NSUInteger)index;

/**
 * Adds a graphic information day to the array. When the provided day is already stored, the information is updated
 *
 * @param aDayTimeInterval The day (represented as a time interval since Jan, 1 1970 measured in seconds) the graphic information refers to
 * @param anAccountBalance The account balance for this day
 * @param aTransactionsGrandTotal The transactions grand total for this day
 * @param hasTransactions The has transactions flag. YES when that day has transactions
 */
- (void)addInformationForDayTimeInterval:(NSTimeInterval)aDayTimeInterval withBalance:(NSDecimalNumber *)anAccountBalance transactionsGrandTotal:(NSDecimalNumber *)aTransactionsGrandTotal
                         hasTransactions:(BOOL)hasTransactions;

/**
 * Adds the information contained in another NXTAccountGraphicInformation instance to the receiver instance
 *
 * @param aGraphicInformation The NXTAccountGraphicInformtion to add
 */
- (void)addAccountGraphicInformation:(NXTAccountGraphicInformation *)aGraphicInformation;

@end
