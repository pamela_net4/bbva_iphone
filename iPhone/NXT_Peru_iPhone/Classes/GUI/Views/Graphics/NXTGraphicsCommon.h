/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>


/**
 * Defines the operation to convert an amount into an Y coordinate (measured in points from the view top)
 */
#define AMOUNT_TO_Y(anAmount, anYMinPosition, anAmountMin, aGraphicScale)                   (anYMinPosition - ((anAmount - anAmountMin) * aGraphicScale))

/**
 * Calculates the grid line alpha for a given percentaje position
 */
#define ALPHA_FOR_PERCENTAJE_POSITION(aPositionPercentaje, aMaxGridGradientPercentage)      (((aPositionPercentaje) < 0.0f) ? 0.0f : \
                                                                                            (((aPositionPercentaje) < aMaxGridGradientPercentage) ? ((aPositionPercentaje) * 0.85 / aMaxGridGradientPercentage) : \
                                                                                            (((aPositionPercentaje) < 0.5f) ? \
                                                                                            (0.85 + (0.15f * ((aPositionPercentaje) - aMaxGridGradientPercentage) / (0.5f - aMaxGridGradientPercentage))) : \
                                                                                            (((aPositionPercentaje) < (1.0f - aMaxGridGradientPercentage)) ? \
                                                                                            (0.85 + (0.15f * (1.0f - (aPositionPercentaje) - aMaxGridGradientPercentage) / (0.5f - aMaxGridGradientPercentage))) : \
                                                                                            (((aPositionPercentaje) < 1.0f) ? ((1.0f - (aPositionPercentaje)) * 0.85 / aMaxGridGradientPercentage) : 0.0f)))))


/**
 * Stores the information needed to display the amount strings into the graphic
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXTGraphicAmountStringInformation : NSObject {
    
@private
    
    /**
     * String to display
     */
    NSString *amountString_;
    
    /**
     * String width measured in points using its display font
     */
    CGFloat stringWidth_;
    
    /**
     * String height measured in points using its display font
     */
    CGFloat stringHeight_;
    
    /**
     * Border top position measured in points from the graphic top
     */
    CGFloat borderTopPosition_;
    
    /**
     * Border width measured in points
     */
    CGFloat borderWidth_;
    
    /**
     * Border height measured in points
     */
    CGFloat borderHeight_;
    
}

/**
 * Provides read-only access to the string to display
 */
@property (nonatomic, readonly, copy) NSString *amountString;

/**
 * Provides read-write access to the string width measured in points using its display font
 */
@property (nonatomic, readwrite) CGFloat stringWidth;

/**
 * Provides read-write access to the string height measured in points using its display font
 */
@property (nonatomic, readwrite) CGFloat stringHeight;

/**
 * Provides read-write access to the border top position measured in points from the graphic top
 */
@property (nonatomic, readwrite) CGFloat borderTopPosition;

/**
 * Provides read-write access to the border width measured in points
 */
@property (nonatomic, readwrite) CGFloat borderWidth;

/**
 * Provides read-write access to the border height measured in points
 */
@property (nonatomic, readwrite) CGFloat borderHeight;


/**
 * Designated initializer. Initializes an NXTAccountGraphicAccountStringInformation with the provided information
 *
 * @param anAmountString The string to render
 * @param aStringWidth The string width once rendered
 * @param aStringHeight The string height once rendered
 * @param aBorderTopPosition The border top position to store
 * @param aBorderWidth The border width to store
 * @param aBorderHeight The border height to store
 * @return The initialized NXTAccountGraphicAccountStringInformation instance
 */
- (id)initWithAmountString:(NSString *)anAmountString stringWidth:(CGFloat)aStringWidth
              stringHeight:(CGFloat)aStringHeight borderTopPosition:(CGFloat)aBorderTopPosition
               borderWidth:(CGFloat)aBorderWidth borderHeight:(CGFloat)aBorderHeight;

@end
