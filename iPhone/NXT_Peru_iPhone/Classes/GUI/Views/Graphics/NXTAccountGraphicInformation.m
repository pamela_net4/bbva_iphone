/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "NXTAccountGraphicInformation.h"


#pragma mark -

/**
 * NXTAccountGraphicDay private extension
 */
@interface NXTAccountGraphicDay()

/**
 * Provides read-write access to the account day balance
 */
@property (nonatomic, readwrite, retain) NSDecimalNumber *accountBalance;

/**
 * Provides read-write access to the account day transactions grand total
 */
@property (nonatomic, readwrite, retain) NSDecimalNumber *transactionsGrandTotal;

/**
 * Provides read-write access to the has transactions flag
 */
@property (nonatomic, readwrite) BOOL hasTransactions;

/**
 * Designated initializer. It initializes an NXTGraphicDay with the information provided
 *
 * @param aDayTimeInterval The graphic day represented as the time interval since Jan 1 1970 (measured in seconds)
 * @param aBalance The account balance for the day
 * @param aTransactionsGradTotal The transactions grand total for the day
 * @param hasTransactions The has transactions flag. YES when that day has transactions
 * @return The initialized NXTGraphicDay
 */
- (id)initWithDayTimeInterval:(NSTimeInterval)aDayTimeInterval balance:(NSDecimalNumber *)aBalance transactionsGrandTotal:(NSDecimalNumber *)aTransactionsGradTotal
              hasTransactions:(BOOL)hasTransactions;

@end


//#pragma mark -
//
///**
// * NXTAccountGraphicInformation private category
// */
//@interface NXTAccountGraphicInformation(private)
//
///**
// * Fills the gaps in the information array
// */
//- (void)fillGaps;
//
//@end
//
//
#pragma mark -

@implementation NXTAccountGraphicDay

#pragma mark -
#pragma mark Properties

@synthesize dayTimeInterval = dayTimeInterval_;
@synthesize accountBalance = accountBalance_;
@synthesize transactionsGrandTotal = transactionsGrandTotal_;
@dynamic endOfDayAccountBalance;
@synthesize hasTransactions = hasTransactions_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [accountBalance_ release];
    accountBalance_ = nil;
    
    [transactionsGrandTotal_ release];
    transactionsGrandTotal_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It call the designated initializer to construct an empty NXTAccountGraphicDay
 *
 * @return The initialized NXTAccountGraphicDay instance
 */
- (id)init {
    
    return [self initWithDayTimeInterval:0.0f balance:nil transactionsGrandTotal:nil hasTransactions:NO];
    
}

/*
 * Designated initializer. It initializes an NXTGraphicDay with the information provided
 */
- (id)initWithDayTimeInterval:(NSTimeInterval)aDayTimeInterval balance:(NSDecimalNumber *)aBalance transactionsGrandTotal:(NSDecimalNumber *)aTransactionsGradTotal
              hasTransactions:(BOOL)hasTransactions {
 
    if (self = [super init]) {
        
        dayTimeInterval_ = aDayTimeInterval;
        accountBalance_ = [aBalance retain];
        transactionsGrandTotal_ = [aTransactionsGradTotal retain];
        hasTransactions_ = hasTransactions;
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark NSCopying selectors

/**
 * Returns a new instance that’s a copy of the receiver. The attributes are copied to the new NXTAccountGraphicDay instance
 *
 * @param zone The zone identifies an area of memory from which to allocate for the new instance
 * @return The new NXTAccountGraphicDay instance with the copied information
 */
- (id)copyWithZone:(NSZone *)zone {
    
    return [[NXTAccountGraphicDay allocWithZone:zone] initWithDayTimeInterval:self.dayTimeInterval
                                                                      balance:self.accountBalance
                                                       transactionsGrandTotal:self.transactionsGrandTotal
                                                              hasTransactions:self.hasTransactions];
    
}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the end of day balance
 */
- (NSDecimalNumber *)endOfDayAccountBalance {
    
    return [accountBalance_ decimalNumberByAdding:transactionsGrandTotal_];
    
}

@end


#pragma mark -

@implementation NXTAccountGraphicInformation

#pragma mark -
#pragma mark Properties

@dynamic daysSpan;
@dynamic firstDay;
@dynamic lastDay;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [informationArray_ release];
    informationArray_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes the elements needed to manage the instance
 *
 * @return The initializede NXTAccountGraphicInformation
 */
- (id)init {
    
    if (self = [super init]) {
        
        informationArray_ = [[NSMutableArray alloc] init];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information access

/*
 * Returns the graphic information day at the provided position. The index must be 0 <= index < daysCount, or a nil instance will be returned
 */
- (NXTAccountGraphicDay *)accountGraphicDayAtIndex:(NSUInteger)index {
    
    NXTAccountGraphicDay *result = nil;
    
    if (index < [informationArray_ count]) {
        
        result = [informationArray_ objectAtIndex:index];
        
    }
    
    return result;
    
}

/*
 * Adds a graphic information day to the array. When the provided day is already stored, the information is updated
 */
- (void)addInformationForDayTimeInterval:(NSTimeInterval)aDayTimeInterval withBalance:(NSDecimalNumber *)anAccountBalance transactionsGrandTotal:(NSDecimalNumber *)aTransactionsGrandTotal
                         hasTransactions:(BOOL)hasTransactions {
    
    NSTimeInterval normalizedDayTimeInterval = (NSTimeInterval)(trunc(aDayTimeInterval / 86400.0f) * 86400.0f);
          
//    BOOL informationInserted = NO;
    NSUInteger storedDays = [informationArray_ count];
    NSTimeInterval firstDay = [self firstDay];
    NSInteger dayPosition = (NSInteger)(trunc((normalizedDayTimeInterval - firstDay) / 86400.0f));
    
    if ((dayPosition >= 0) && (dayPosition < storedDays)) {
        
        NXTAccountGraphicDay *storedDayInformation = [informationArray_ objectAtIndex:dayPosition];
        storedDayInformation.accountBalance = anAccountBalance;
        storedDayInformation.transactionsGrandTotal = aTransactionsGrandTotal;
        storedDayInformation.hasTransactions = hasTransactions;
        
    } else if (storedDays == 0) {
        
        [informationArray_ addObject:[[[NXTAccountGraphicDay alloc] initWithDayTimeInterval:normalizedDayTimeInterval
                                                                                    balance:anAccountBalance
                                                                     transactionsGrandTotal:aTransactionsGrandTotal
                                                                            hasTransactions:hasTransactions] autorelease]];
        
    } else if (dayPosition < 0) {
        
        NXTAccountGraphicDay *firstGraphicDay = [informationArray_ objectAtIndex:0];
        NSDecimalNumber *balance = firstGraphicDay.accountBalance;
        NSDecimalNumber *grandTotal = [NSDecimalNumber zero];
        
        for (; dayPosition < 0; dayPosition++) {
            
            firstDay -= 86400.0f;
            [informationArray_ insertObject:[[[NXTAccountGraphicDay alloc] initWithDayTimeInterval:firstDay
                                                                                           balance:balance
                                                                            transactionsGrandTotal:grandTotal
                                                                                   hasTransactions:NO] autorelease]
                                    atIndex:0];
            
        }
        
        NXTAccountGraphicDay *graphicDay = [informationArray_ objectAtIndex:0];
        graphicDay.accountBalance = anAccountBalance;
        graphicDay.transactionsGrandTotal = aTransactionsGrandTotal;
        graphicDay.hasTransactions = hasTransactions;
        
    } else {
        
        NXTAccountGraphicDay *lastGraphicDay = [informationArray_ objectAtIndex:(storedDays - 1)];
        NSTimeInterval lastDay = lastGraphicDay.dayTimeInterval;
        NSDecimalNumber *balance = [lastGraphicDay.accountBalance decimalNumberByAdding:lastGraphicDay.transactionsGrandTotal];
        NSDecimalNumber *grandTotal = [NSDecimalNumber zero];
        
        for (; dayPosition > storedDays; dayPosition--) {
            
            lastDay += 86400.0f;
            [informationArray_ addObject:[[[NXTAccountGraphicDay alloc] initWithDayTimeInterval:lastDay
                                                                                        balance:balance
                                                                         transactionsGrandTotal:grandTotal
                                                                                hasTransactions:NO] autorelease]];
            
        }
        
        storedDays = [informationArray_ count];
        NXTAccountGraphicDay *graphicDay = [informationArray_ objectAtIndex:(storedDays - 1)];
        graphicDay.accountBalance = anAccountBalance;
        graphicDay.transactionsGrandTotal = aTransactionsGrandTotal;
        graphicDay.hasTransactions = hasTransactions;
        
    }
    
}


/*
 * Adds the information contained in another NXTAccountGraphicInformation instance to the receiver instance
 */
- (void)addAccountGraphicInformation:(NXTAccountGraphicInformation *)aGraphicInformation {
    
    for (NXTAccountGraphicDay *graphicDay in aGraphicInformation->informationArray_) {
        
        [self addInformationForDayTimeInterval:graphicDay.dayTimeInterval
                                   withBalance:graphicDay.accountBalance
                        transactionsGrandTotal:graphicDay.transactionsGrandTotal
                               hasTransactions:graphicDay.hasTransactions];
        
    }
    
}
//
///*
// * Fills the gaps in the information array
// */
//- (void)fillGaps {
//    
//    NSUInteger daysStored = [informationArray_ count];
//    
//    if (daysStored > 1) {
//        
//        NXTAccountGraphicDay *nextDay = [informationArray_ objectAtIndex:(daysStored - 1)];
//        NSTimeInterval previousTimeInterval;
//        NSTimeInterval nextTimeInterval;
//        NSTimeInterval dayTimeInterval = 86400.0f;
//        NSDecimalNumber *nextBalance;
//        NSInteger daysGap;
//        NXTAccountGraphicDay *newDay;
//        NSTimeInterval newTimeInterval;
//        NSDecimalNumber *zeroTransactions = [NSDecimalNumber zero];
//        
//        for (NSInteger i = daysStored - 2; i >= 0; i--) {
//            
//            NXTAccountGraphicDay *previousDay = [informationArray_ objectAtIndex:i];
//            
//            previousTimeInterval = previousDay.dayTimeInterval;
//            nextTimeInterval = nextDay.dayTimeInterval;
//            
//            daysGap = (NSInteger)(round(nextTimeInterval - previousTimeInterval) / dayTimeInterval) - 1;
//            
//            if (daysGap > 0) {
//                
//                nextBalance = nextDay.accountBalance;
//                
//                for (NSUInteger j = daysGap; j > 0; j--) {
//                    
//                    newTimeInterval = nextTimeInterval - (j * dayTimeInterval);
//                    newDay = [[[NXTAccountGraphicDay alloc] initWithDayTimeInterval:newTimeInterval balance:nextBalance transactionsGrandTotal:zeroTransactions
//                                                                    hasTransactions:NO] autorelease];
//                    [informationArray_ insertObject:newDay atIndex:(i + 1)];
//                    
//                }
//                
//            }
//            
//            nextDay = previousDay;
//            
//        }
//        
//    }
//    
//}

#pragma mark -
#pragma mark NSCopying selectors

/**
 * Returns a new instance that’s a copy of the receiver. The attributes are copied to the new NXTAccountGraphicInformation instance
 *
 * @param zone The zone identifies an area of memory from which to allocate for the new instance
 * @return The new NXTAccountGraphicInformation instance with the copied information
 */
- (id)copyWithZone:(NSZone *)zone {
    
    NXTAccountGraphicInformation *result = [[NXTAccountGraphicInformation allocWithZone:zone] init];
    
    for (NXTAccountGraphicDay *graphicDay in informationArray_) {
        
        [result addInformationForDayTimeInterval:graphicDay.dayTimeInterval
                                     withBalance:graphicDay.accountBalance
                          transactionsGrandTotal:graphicDay.transactionsGrandTotal
                                 hasTransactions:graphicDay.hasTransactions];
        
    }
    
    return result;
    
}
    
#pragma mark -
#pragma mark Properties methods

/*
 * Returns the days span stored
 *
 * @return The days span stored
 */
- (NSUInteger)daysSpan {

    return [informationArray_ count];
    
}

/*
 * Returns the first day stored in the information
 *
 * @return The first day stored in the information
 */
- (NSTimeInterval)firstDay {

    NSTimeInterval result = 0.0f;
    
    if ([informationArray_ count] > 0) {
        
        NXTAccountGraphicDay *storedGrahpicDay = [informationArray_ objectAtIndex:0];
        result = storedGrahpicDay.dayTimeInterval;
        
    }
    
    return result;
}

/*
 * Returns the last day stored in the information
 *
 * @return The last day stored in the information
 */
- (NSTimeInterval)lastDay {
    
    NSTimeInterval result = 0.0f;
    
    NSUInteger graphicDaysCount = [informationArray_ count];
    
    if (graphicDaysCount > 0) {
        
        NXTAccountGraphicDay *storedGrahpicDay = [informationArray_ objectAtIndex:(graphicDaysCount - 1)];
        result = storedGrahpicDay.dayTimeInterval;
        
    }
    
    return result;
    
}

@end
