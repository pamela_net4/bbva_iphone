/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXT_Peru_iPhoneStyler.h"

#import <QuartzCore/QuartzCore.h>

#import "AlignableButton.h"
#import "AmountAndCurrencyLabel.h"
#import "Constants.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MOKButton.h"
#import "MOKStringListSelectionButton.h"
#import "MOKTextField.h"
#import "NXTAccountGraphic.h"
#import "NXTTextField.h"
#import "StringKeys.h"
#import "UIColor+BBVA_Colors.h"
#import "NXTCurrencyTextField.h"

/**
 * Defines the MoreTabViewCell label text size
 */
#define MORE_TAB_VIEW_CELL_LABEL_TEXT_SIZE                          15.0f

/**
 * Defines the standard blue button title text size
 */
#define STANDARD_BLUE_BUTTON_TITLE_TEXT_SIZE                        15.0f

/**
 * Defines the standard blue button title text size
 */
#define STANDARD_GRAY_BUTTON_TITLE_TEXT_SIZE                        14.0f

/**
 * Defines the bevel button title text size
 */
#define BEVEL_BUTTON_TITLE_TEXT_SIZE                                12.0f


/**
 * Defines the icon action cell label text size
 */
#define ICON_ACTION_CELL_LABEL_TEXT_SIZE                            15.0f

/**
 * Defines the login information cell label text size
 */
#define LOGIN_INFO_CELL_LABEL_TEXT_SIZE                             13.0f

/**
 * Defines the login information cell label text size
 */
#define LOGIN_INFO_CELL_LABEL_TEXT_SMALL_SIZE                       11.0f

/**
 * Defines the padding to apply to the NXT standard text field
 */
#define NXT_TEXT_FIELD_PADDING                                      5.0f


#pragma mark -

/**
 * NXTStyle private extension
 */
@interface NXT_Peru_iPhoneStyler()

/**
 * Sets a white shadow to the provided label
 *
 * @param aLabel The label to receive the shadow effect
 */
+ (void)setWhiteShadowToLabel:(UILabel *)aLabel;

/**
 * Sets a black shadow to the provided label
 *
 * @param aLabel The label to receive the shadow effect
 */
+ (void)setBlackShadowToLabel:(UILabel *)aLabel;

/**
 * Sets a white font to the provided button using the given font size and colors
 *
 * @param aButton The button to apply style to
 * @param aBoldFont A flag to use a bold font or a normal one (YES for a bold font, NO for a normal one)
 * @param aSize The font size
 * @param anEnabledTitleColor The title color when button is enabled
 * @param aDisabledTitleColor The title color when button is disabled
 * @param aShadowColor The title shadow color
 */
+ (void)styleButton:(UIButton *)aButton withBoldFont:(BOOL)aBoldFont fontSize:(CGFloat)aSize color:(UIColor *)anEnabledTitleColor
      disabledColor:(UIColor *)aDisabledTitleColor andShadowColor:(UIColor *)aShadowColor;

@end


#pragma mark -

@implementation NXT_Peru_iPhoneStyler

#pragma mark -
#pragma mark Button styling

/*
 * Styles a route modal view button
 */
+ (void)styleRouteModalViewButton:(UIButton *)button {
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    [button setBackgroundImage:[imagesCache imageNamed:ROUTE_MODAL_VIEW_CONTROLLER_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                      forState:UIControlStateNormal];
    
    UILabel *titleLabel = button.titleLabel;
    titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    [button setTitleColor:[UIColor whiteColor]
                 forState:UIControlStateNormal];
    
}

#pragma mark -
#pragma mark Button styling

/*
 * Sets a white font to the provided button using the given font size and colors
 */
+ (void)styleButton:(UIButton *)aButton withBoldFont:(BOOL)aBoldFont fontSize:(CGFloat)aSize color:(UIColor *)anEnabledTitleColor
      disabledColor:(UIColor *)aDisabledTitleColor andShadowColor:(UIColor *)aShadowColor {
    
    UIFont *font;
    
    if (aBoldFont) {
        
        font = [self boldFontWithSize:aSize];
        
    } else {
        
        font = [self normalFontWithSize: aSize];
        
    }
    
    UILabel *titleLabel = [aButton titleLabel];
    titleLabel.font = font;
    titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    
    [aButton setTitleColor:anEnabledTitleColor forState:UIControlStateNormal];
    [aButton setTitleColor:anEnabledTitleColor forState:UIControlStateHighlighted];
    [aButton setTitleColor:aDisabledTitleColor forState:UIControlStateDisabled];
    
    [aButton setTitleShadowColor:aShadowColor forState:UIControlStateNormal];
    
    aButton.showsTouchWhenHighlighted = NO;
    
}

/*
 * Styles a button with white text and given background
 */
+ (void)styleButton:(UIButton *)aButton withWhiteFontSize:(CGFloat)aSize background:(UIImage *)aBackground {
    
    [NXT_Peru_iPhoneStyler styleButton:aButton withBoldFont:NO fontSize:aSize color:[UIColor whiteColor]
                         disabledColor:[UIColor grayColor] andShadowColor:[UIColor colorWithWhite:0.5f alpha:0.3f]];
    
    [aButton setBackgroundImage:aBackground forState:UIControlStateNormal];
    
}

/*
 * Styles a button with white text and given background
 */

+ (void)styleButton:(UIButton *)aButton withWhiteFontSize:(CGFloat)aSize backgroundColor:(UIColor *)aBackground
{
    [NXT_Peru_iPhoneStyler styleButton:aButton withBoldFont:NO fontSize:aSize color:[UIColor whiteColor]
                         disabledColor:[UIColor grayColor] andShadowColor:[UIColor colorWithWhite:0.5f alpha:0.3f]];
    
    [aButton setBackgroundColor:aBackground];

}

/*
 * Styles an application standard blue button
 */
+ (void)styleBlueButton:(UIButton *)aButton {
    
    [NXT_Peru_iPhoneStyler styleButton:aButton withBoldFont:NO fontSize:STANDARD_BLUE_BUTTON_TITLE_TEXT_SIZE color:[UIColor whiteColor]
                         disabledColor:[UIColor grayColor] andShadowColor:[UIColor clearColor]];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        ImagesCache *imagesCache = [ImagesCache getInstance];
        UIImage *backgroundImage = [imagesCache imageNamed:BLUE_BUTTON_BACKGROUND_IMAGE_FILE_NAME];
        [aButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    
    }else {

        [aButton setBackgroundColor:[UIColor BBVABlueSpectrumColor]];
    }
}

/*
 * Styles an application standard white button
 */
+ (void)styleWhiteButton:(UIButton *)aButton {
    
    [NXT_Peru_iPhoneStyler styleButton:aButton withBoldFont:NO fontSize:STANDARD_BLUE_BUTTON_TITLE_TEXT_SIZE color:[UIColor BBVABlueSpectrumColor]
                         disabledColor:[UIColor BBVAGreyToneFourColor] andShadowColor:[UIColor clearColor]];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        ImagesCache *imagesCache = [ImagesCache getInstance];
        UIImage *backgroundImage = [imagesCache imageNamed:WHITE_BUTTON_BACKGROUND_IMAGE_FILE_NAME];
        [aButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
        
    }else {
        
        [aButton setBackgroundColor:[UIColor BBVAWhiteColor]];
    }

    
    
    
}

/*
 * Styles an application standard white button
 */
+ (void)styleBoldWhiteButton:(UIButton *)aButton {
    
    [NXT_Peru_iPhoneStyler styleButton:aButton withBoldFont:YES fontSize:STANDARD_BLUE_BUTTON_TITLE_TEXT_SIZE color:[UIColor BBVABlueSpectrumColor]
                         disabledColor:[UIColor BBVAGreyToneFourColor] andShadowColor:[UIColor clearColor]];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        ImagesCache *imagesCache = [ImagesCache getInstance];
        UIImage *backgroundImage = [imagesCache imageNamed:WHITE_BUTTON_BACKGROUND_IMAGE_FILE_NAME];
        [aButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
        
    }else {
        
        [aButton setBackgroundColor:[UIColor BBVAWhiteColor]];
    }
    
    
    
    
}

/*
 * Styles an application standard gray button
 *
 * @param aButton The button to style
 */
+ (void)styleGrayButton:(UIButton *)aButton {
    
    [NXT_Peru_iPhoneStyler styleButton:aButton withBoldFont:NO fontSize:STANDARD_GRAY_BUTTON_TITLE_TEXT_SIZE color:[UIColor whiteColor]
                         disabledColor:[UIColor grayColor] andShadowColor:[UIColor clearColor]];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        ImagesCache *imagesCache = [ImagesCache getInstance];
        UIImage *backgroundImage = [imagesCache imageNamed:GRAY_BUTTON_BACKGROUND_IMAGE_FILE_NAME];
        
        [aButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
        
    }else {
        
        [aButton setBackgroundColor:[UIColor BBVAGreyToneTwoColor]];
    }
    
}

/*
 * Styles an application standard gray button
 *
 * @param aButton The button to style
 */
+ (void)styleGrayButtonTextLight:(UIButton *)aButton {
    
    [NXT_Peru_iPhoneStyler styleButton:aButton withBoldFont:NO fontSize:STANDARD_GRAY_BUTTON_TITLE_TEXT_SIZE color:[UIColor whiteColor]
                         disabledColor:[UIColor whiteColor] andShadowColor:[UIColor clearColor]];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        ImagesCache *imagesCache = [ImagesCache getInstance];
        UIImage *backgroundImage = [imagesCache imageNamed:GRAY_BUTTON_BACKGROUND_IMAGE_FILE_NAME];
        
        [aButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
        
    }else {
        
        [aButton setBackgroundColor:[UIColor BBVAGreyToneTwoColor]];
    }
    
}

/*
 * Styles an application standard gray button
 *
 * @param aButton The button to style
 */
+ (void)styleLightGrayButton:(UIButton *)aButton {
    
    [NXT_Peru_iPhoneStyler styleButton:aButton withBoldFont:NO fontSize:STANDARD_GRAY_BUTTON_TITLE_TEXT_SIZE color:[UIColor whiteColor]
                         disabledColor:[UIColor grayColor] andShadowColor:[UIColor clearColor]];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        ImagesCache *imagesCache = [ImagesCache getInstance];
        UIImage *backgroundImage = [imagesCache imageNamed:GRAY_BUTTON_BACKGROUND_IMAGE_FILE_NAME];
        
        [aButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
        
    }else {
        
        [aButton setBackgroundColor:[UIColor BBVABlueColor]];
    }
    
}

/*
 * Styles a button with a bevel background and light blue text
 */
+ (void)styleBevelButton:(UIButton *)aButton {
    
    [NXT_Peru_iPhoneStyler styleButton:aButton
                          withBoldFont:NO
                              fontSize:BEVEL_BUTTON_TITLE_TEXT_SIZE
                                 color:[UIColor BBVABlueSpectrumColor]
                         disabledColor:[UIColor grayColor]
                        andShadowColor:[UIColor clearColor]];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    UIImage *backgroundImage = [imagesCache imageNamed:BEVEL_BACKGROUND_2_IMAGE_FILE_NAME];
    
    [aButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    
}

/*
 * Styles an action icon button (contains only an icon as the button image)
 */
+ (void)styleActionIconButton:(UIButton *)aButton {
    
    aButton.showsTouchWhenHighlighted = YES;
    
}

/*
 * Styles a button as a cell option button
 */
+ (void)styleCellOptionButton:(UIButton *)aButton withTitle:(NSString *)title {
    
    [aButton setBackgroundColor:[UIColor BBVAGreenLemonToneThreeColor]];
    aButton.autoresizesSubviews = YES;
    
    UILabel *customTitleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(5.0f, aButton.frame.size.height - 25.0f, aButton.frame.size.width - 10.0f, 20.0f)] autorelease];
    customTitleLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    
    customTitleLabel.text = title;
    customTitleLabel.textAlignment = UITextAlignmentCenter;
    customTitleLabel.backgroundColor = [UIColor clearColor];
    //    customTitleLabel.tag = CELL_OPTION_BUTTON_TITLE_TAG;
    
    customTitleLabel.textColor = [UIColor colorWithRed:0.2823f green:0.3921f blue:0.4784f alpha:1.0f];
    customTitleLabel.font = [UIFont systemFontOfSize:12.0f];
    //[NXTStyler styleLabel:customTitleLabel withAnotherBlueColorBoldFontSize:12.0f];
    
    [aButton addSubview:customTitleLabel];
    
    [aButton setImageEdgeInsets:UIEdgeInsetsMake(0.0f, 0.0f, 15.0f, 0.0f)];
}

/*
 * Styles a button as a bottom bar button
 */
+ (void)styleBottomBarButton:(UIButton *)aButton withTitle:(NSString *)title {
    
    [aButton setBackgroundColor:[UIColor clearColor]];
    
    UILabel *customTitleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0.0f, aButton.frame.size.height - 21.0f, aButton.frame.size.width, 20.0f)] autorelease];
    customTitleLabel.text = title;
    customTitleLabel.textAlignment = UITextAlignmentCenter;
    customTitleLabel.font = [UIFont boldSystemFontOfSize:11.0f];
    customTitleLabel.textColor = [UIColor BBVAGreyToneOneColor];
    customTitleLabel.backgroundColor = [UIColor clearColor];
    customTitleLabel.shadowColor = [UIColor whiteColor];
    customTitleLabel.shadowOffset = CGSizeMake(0, 1);
    
    [NXT_Peru_iPhoneStyler styleLabel:customTitleLabel withBoldFontSize:11.0f color:[UIColor BBVAGreyToneOneColor]];
    
    [aButton addSubview:customTitleLabel];
    
    [aButton setImageEdgeInsets:UIEdgeInsetsMake(0.0f, 0.0f, 15.0f, 0.0f)];
}

/*
 * Styles a button with a gray background, white font black shaded text and a disclosure arrow on the right side
 */
+ (void)styleComboButton:(UIButton *)aButton {
    [self styleGrayButton:aButton];
    
    UIImage *disclosureImage = [[ImagesCache getInstance] imageNamed:COMBO_DISCLOSURE_ARROW_IMAGE_FILE_NAME];
    [aButton setImage:disclosureImage forState:UIControlStateNormal];
    
    
    
    
    [aButton setImageEdgeInsets:UIEdgeInsetsMake(0.0f, aButton.frame.size.width - disclosureImage.size.width - 10.0f, 0.0f, 0.0f)];
    [aButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [aButton setShowsTouchWhenHighlighted:NO];
    
    [NXT_Peru_iPhoneStyler setBlackShadowToLabel:aButton.titleLabel];
    
}

/*
 * Styles a button with a gray background, white font black shaded text and a disclosure arrow on the right side
 */
+ (void)styleComboButton:(UIButton *)aButton  isNewDesign:(BOOL)isNewDesign{
    if(isNewDesign){
        [self styleGrayButtonTextLight:aButton];
    }
    else{
        [self styleGrayButton:aButton];
    }
    
    UIImage *disclosureImage = [[ImagesCache getInstance] imageNamed:COMBO_DISCLOSURE_ARROW_IMAGE_FILE_NAME];
    [aButton setImage:disclosureImage forState:UIControlStateNormal];
    
    
    
    
    [aButton setImageEdgeInsets:UIEdgeInsetsMake(0.0f, aButton.frame.size.width - disclosureImage.size.width - 10.0f, 0.0f, 0.0f)];
    [aButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [aButton setShowsTouchWhenHighlighted:NO];
    
    [NXT_Peru_iPhoneStyler setBlackShadowToLabel:aButton.titleLabel];
    
}

/*
 * Styles a button with a gray background, white font black shaded text and a disclosure arrow on the right side
 */
+ (void)styleComboButtonLogin:(UIButton *)aButton {
    [self styleGrayButton:aButton];
    
    UIImage *disclosureImage = [[ImagesCache getInstance] imageNamed:COMBO_DISCLOSURE_ARROW_IMAGE_FILE_NAME];
    [aButton setImage:disclosureImage forState:UIControlStateNormal];
    
    [aButton setImageEdgeInsets:UIEdgeInsetsMake(0.0f, aButton.frame.size.width - disclosureImage.size.width - 10.0f, 0.0f, 0.0f)];
    [aButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [aButton setShowsTouchWhenHighlighted:NO];
    
    [NXT_Peru_iPhoneStyler setBlackShadowToLabel:aButton.titleLabel];
    
}


/*
 * Styles a button with a gray background, white font black shaded text and a disclosure arrow on the right side
 */
+ (void)styleStringListButton:(MOKStringListSelectionButton *)aButton {
    
    [self styleGrayButton:aButton];
    [aButton setImageOnTheRight:YES];
    UIImage *disclosureImage = [[ImagesCache getInstance] imageNamed:COMBO_DISCLOSURE_ARROW_IMAGE_FILE_NAME];
    [aButton setImage:disclosureImage forState:UIControlStateNormal];
    
    [aButton setImageEdgeInsets:UIEdgeInsetsMake(0.0f, aButton.frame.size.width - disclosureImage.size.width - 10.0f, 0.0f, 0.0f)];
    [aButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [aButton setShowsTouchWhenHighlighted:NO];
    
    [NXT_Peru_iPhoneStyler setBlackShadowToLabel:aButton.titleLabel];
    
}

/**
 * Styles a button used to select an account with a gray background, white font black shaded text and a disclosure arrow on the right side
 *
 * @param aButton The button to style
 */
+ (void)styleAccountSelectionButton:(MOKStringListSelectionButton *)aButton {
    
    [NXT_Peru_iPhoneStyler styleStringListButton:aButton];
    
    UILabel *titleLabel = [aButton titleLabel];
    [titleLabel setFont:[NXT_Peru_iPhoneStyler normalFontWithSize:10.0f]];
    
}

/*
 * Styles a button with segmented aspect(left side)
 */
+ (void)styleLeftSegmentedButton:(UIButton *)aButton {
	[NXT_Peru_iPhoneStyler styleButton:aButton
                          withBoldFont:NO
                              fontSize:STANDARD_GRAY_BUTTON_TITLE_TEXT_SIZE
                                 color:[UIColor BBVACoolGreyColor]
                         disabledColor:[UIColor grayColor]
                        andShadowColor:[UIColor clearColor]];
	
    ImagesCache *imagesCache = [ImagesCache getInstance];
    UIImage *backgroundImage = [imagesCache imageNamed:STOCK_BUTTON_NORMAL_BACKGROUND_IMAGE_FILE_NAME];
    [aButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
	
	backgroundImage = [imagesCache imageNamed:STOCK_BUTTON_SELECTED_BACKGROUND_IMAGE_FILE_NAME];
    [aButton setBackgroundImage:backgroundImage forState:UIControlStateHighlighted];
}

/*
 * Styles a button with segmented aspect(right side)
 */
+ (void)styleRightSegmentedButton:(UIButton *)aButton {
    
	[NXT_Peru_iPhoneStyler styleButton:aButton
                          withBoldFont:NO
                              fontSize:STANDARD_GRAY_BUTTON_TITLE_TEXT_SIZE
                                 color:[UIColor BBVACoolGreyColor]
                         disabledColor:[UIColor grayColor]
                        andShadowColor:[UIColor clearColor]];
	
    ImagesCache *imagesCache = [ImagesCache getInstance];
    UIImage *backgroundImage = [imagesCache imageNamed:ORDER_BUTTON_NORMAL_BACKGROUND_IMAGE_FILE_NAME];
    [aButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
	
	backgroundImage = [imagesCache imageNamed:ORDER_BUTTON_SELECTED_BACKGROUND_IMAGE_FILE_NAME];
    [aButton setBackgroundImage:backgroundImage forState:UIControlStateHighlighted];
}


/*
 * Styles a stock graphic type button
 */
+ (void)styleStockGraphicTypeButton:(UIButton *)aButton {
    
    [NXT_Peru_iPhoneStyler styleButton:aButton
                          withBoldFont:NO
                              fontSize:STANDARD_GRAY_BUTTON_TITLE_TEXT_SIZE
                                 color:[UIColor BBVACoolGreyColor]
                         disabledColor:[UIColor BBVACoolGreyColor]
                        andShadowColor:[UIColor clearColor]];
    
    [aButton setTitleColor:[UIColor BBVACoolGreyColor]
                  forState:UIControlStateNormal];
    [aButton setTitleColor:[UIColor whiteColor]
                  forState:UIControlStateSelected];
    
    [aButton setBackgroundImage:[[ImagesCache getInstance] imageNamed:SELECTED_STOCK_GRAPHIC_TYPE_BUTTON_BACKGROUND_IMAGE_FILE_NAME]
                       forState:UIControlStateSelected];
    
}

/*
 * Styles the given button as the Exit button
 */
+ (void)styleExitButton:(UIButton *)exitButton {
    [exitButton setTitle:NSLocalizedString(EXIT_BUTTON_TITLE_KEY, nil) forState:UIControlStateNormal];
    [exitButton setBackgroundColor:[UIColor colorWithRed:0.7882f green:0.8627f blue:0.9294f alpha:1.0f]];
    exitButton.showsTouchWhenHighlighted = YES;
    [exitButton setTitleColor:[UIColor BBVABlueColor] forState:UIControlStateNormal];
    exitButton.titleLabel.font = [UIFont boldSystemFontOfSize:13.0f];
    exitButton.layer.cornerRadius = 5;
}

/*
 * Styles the given button as a POI telephone button
 */
+ (void)stylePOITelephoneButton:(UIButton *)button {
    [button setBackgroundImage:[[ImagesCache getInstance] imageNamed:GRAY_PLAIN_BUTTON_BACKGROUND_IMAGE_FILE_NAME] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor BBVACoolGreyColor] forState:UIControlStateNormal];
    [button setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
	//[button setTitleShadowOffset:CGSizeMake(1.0f, 1.0f)];
	UILabel *titleLabel = button.titleLabel;
	titleLabel.shadowOffset = CGSizeMake(1.0f, 1.0f);
    titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    button.showsTouchWhenHighlighted = YES;
}

/*
 * Styles the given button as a transction type button
 */
+ (void)styleTransactionTypeButton:(UIButton *)button {
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:11.0f];
    button.enabled = NO;
    button.layer.cornerRadius = 6;
}

/*
 * Styles the given button as a white button with an arrow on the right
 */
+ (void)styleWhiteButtonWithArrow:(UIButton *)button {
    [button setImage:[[ImagesCache getInstance] imageNamed:ARROW_ICON_IMAGE_FILE_NAME] forState:UIControlStateNormal];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0.0f, button.frame.size.width - 30.0f, 0.0f, 0.0f)];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 30.0f)];
}

/*
 * Styles the given button as a white button with the text on the left and an arrow on the right
 */
+ (void)styleWhiteButtonWithTextOnTheLeftAndArrow:(UIButton *)button withFontSize:(CGFloat)fontSize {
    button.titleLabel.numberOfLines = 0;
    button.titleLabel.lineBreakMode = UILineBreakModeWordWrap;
    button.titleLabel.minimumFontSize = 10.0f;
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    button.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setImage:[[ImagesCache getInstance] imageNamed:ARROW_ICON_IMAGE_FILE_NAME] forState:UIControlStateNormal];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0.0f, button.frame.size.width - 30.0f, 0.0f, 0.0f)];
    
    CGSize textSize = [button.currentTitle sizeWithFont:button.titleLabel.font constrainedToSize:CGSizeMake(260.0f, 40.0f)];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, -(button.frame.size.width - 40.0f - textSize.width), 0.0f, 30.0f)];
}

#pragma mark -
#pragma mark Text fields styling

/**
 * Styles a text field with gray text
 *
 * @param aTextField: The text field to apply style to
 * @param aSize: The font size
 * @param aColor: The color to text field.
 */
+ (void)styleTextField:(NXTTextField *)aTextField
          withFontSize:(CGFloat)aSize
              andColor:(UIColor *)aColor {
    
    UIFont *font = [self normalFontWithSize:aSize];
    
    aTextField.font = font;
    aTextField.textColor = aColor;
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    aTextField.background = [imagesCache imageNamed:TEXT_FIELD_BACKGROUND_IMAGE_FILE_NAME];
    aTextField.textPadding = NXT_TEXT_FIELD_PADDING;
    
    UIView * leftView = aTextField.leftView;
    if(leftView && [[leftView class] isSubclassOfClass:[UILabel class]]){
        UILabel * label = (UILabel*)leftView;
        label.textColor = aColor;
    }
    
}

/**
 * Styles a text field with gray text
 *
 * @param aTextField: The text field to apply style to
 * @param aSize: The font size
 * @param aColor: The color to text field.
 */
+ (void)styleTextFieldError:(NXTTextField *)aTextField
          withFontSize:(CGFloat)aSize
              andColor:(UIColor *)aColor {
    
    UIFont *font = [self normalFontWithSize:aSize];
    
    aTextField.font = font;
    aTextField.textColor = aColor;
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    aTextField.background = [imagesCache imageNamed:TEXT_FIELD_BACKGROUND_IMAGE_FILE_NAME];
    aTextField.textPadding = NXT_TEXT_FIELD_PADDING;
    
    UIView * bgError = nil;
    for (UIView * subview in aTextField.subviews) {
        if(subview.tag == 555){
            bgError = subview;
        }
    }
    if(!bgError){
        bgError = [[UIView alloc] init];
        [aTextField addSubview:bgError];
    }
    bgError.frame = CGRectMake(0, 0, CGRectGetWidth(aTextField.frame), CGRectGetHeight(aTextField.frame));
    bgError.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:0.7];
    bgError.layer.borderWidth = 1;
    bgError.layer.borderColor = [[UIColor colorWithRed:241.0/255.0 green:209.0/255.0 blue:222.0/255.0 alpha:1.0] CGColor];
    bgError.layer.cornerRadius = 6;
    bgError.tag = 555;
    bgError.layer.masksToBounds = YES;
    bgError.userInteractionEnabled = NO;
    
    UIView * leftView = aTextField.leftView;
    if(leftView && [[leftView class] isSubclassOfClass:[UILabel class]]){
        UILabel * label = (UILabel*)leftView;
        label.textColor = aColor;
    }
    
    
}

+(void)addError:(UIView*)aView{
    UIView * bgError = nil;
    for (UIView * subview in aView.subviews) {
        if(subview.tag == 555){
            bgError = subview;
        }
    }
    if(!bgError){
        bgError = [[UIView alloc] init];
        [aView addSubview:bgError];
    }
    bgError.frame = CGRectMake(0, 0, CGRectGetWidth(aView.frame), CGRectGetHeight(aView.frame));
    bgError.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:0.7];
    bgError.layer.borderWidth = 1;
    bgError.layer.borderColor = [[UIColor colorWithRed:241.0/255.0 green:209.0/255.0 blue:222.0/255.0 alpha:1.0] CGColor];
    bgError.layer.cornerRadius = 6;
    bgError.tag = 555;
    bgError.layer.masksToBounds = YES;
    bgError.userInteractionEnabled = NO;
}

+(void)removeError:(UIView *)aView{
    for (UIView * subview in aView.subviews) {
        if(subview.tag == 555){
            [subview removeFromSuperview];
        }
    }
}


/**
 * Styles a text field with gray text
 *
 * @param aTextField: The text field to apply style to
 * @param aSize: The font size
 * @param aColor: The color to text field.
 */
+ (void)styleMokTextField:(MOKTextField *)aTextField
             withFontSize:(CGFloat)aSize
                 andColor:(UIColor *)aColor {
    
    UIFont *font = [self normalFontWithSize:aSize];
    
    aTextField.font = font;
    aTextField.textColor = aColor;
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    aTextField.background = [imagesCache imageNamed:TEXT_FIELD_BACKGROUND_IMAGE_FILE_NAME];
    aTextField.textPadding = NXT_TEXT_FIELD_PADDING;
    
}

#pragma mark -
#pragma mark TextView styling

/*
 * Styles a text view with gray text
 */
+ (void)styleTextView:(UITextView *)textField withFontSize:(CGFloat)size andColor:(UIColor *)aColor {
    
    UIFont *font = [self normalFontWithSize:size];
    
    textField.font = font;
    textField.textColor = aColor;
    
}

#pragma mark -
#pragma mark Label styling

/*
 * Styles a label with a given font size and text color
 */
+ (void)styleLabel:(UILabel *)label
      withFontSize:(CGFloat)fontSize
             color:(UIColor *)textColor {
    
    UIFont *font = [NXT_Peru_iPhoneStyler normalFontWithSize:fontSize];
    label.font = font;
    label.textColor = textColor;
    label.highlightedTextColor = textColor;
    
}


/*
 * Styles a label with a given bold font size and text color
 */
+ (void)styleLabel:(UILabel *)label
  withBoldFontSize:(CGFloat)fontSize
             color:(UIColor *)textColor {
    
    UIFont *font = [NXT_Peru_iPhoneStyler boldFontWithSize:fontSize];
    label.font = font;
    label.textColor = textColor;
    label.highlightedTextColor = textColor;
    
}

/*
 * Sets a black shadow to the provided label
 */
+ (void)setBlackShadowToLabel:(UILabel *)aLabel {
    
    aLabel.shadowColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.75f];
    aLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    
}

/*
 * Sets a white shadow to the provided label
 */
+ (void)setWhiteShadowToLabel:(UILabel *)aLabel {
    
    aLabel.shadowColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.75f];
    aLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    
}

/*
 * Styles a label giving it a blue bold font and white shadow with the provided font size
 */
+ (void)styleLabel:(UILabel *)aLabel withBlueWhiteShadedBoldFontSize:(CGFloat)aFontSize {
    
    UIFont *font = [self boldFontWithSize:aFontSize];
    
    aLabel.font = font;
    aLabel.textColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.5f alpha:1.0f];
    [NXT_Peru_iPhoneStyler setWhiteShadowToLabel:aLabel];
    
}

/*
 * Styles a label inside a MoreTabViewCell
 */
+ (void)styleMoreTabViewCellLabel:(UILabel *)aLabel {
    
    UIFont *font = [self boldFontWithSize:MORE_TAB_VIEW_CELL_LABEL_TEXT_SIZE];
    
    aLabel.font = font;
    aLabel.textColor = [UIColor blackColor];
    
}

/*
 * Styles a label inside the IconActionCell
 */
+ (void)styleIconActionCellLabel:(UILabel *)aLabel {
    
    UIFont *font = [self boldFontWithSize:ICON_ACTION_CELL_LABEL_TEXT_SIZE];
    
    aLabel.font = font;
    aLabel.textColor = [UIColor BBVABlueColor];
    
}

/*
 * Styles a label inside the LoginInfoCell
 */
+ (void)styleLoginInfoCellLabel:(UILabel *)aLabel {
    
    UIFont *font = [self boldFontWithSize:LOGIN_INFO_CELL_LABEL_TEXT_SIZE];
    
    aLabel.font = font;
    aLabel.textColor = [UIColor BBVACoolGreyColor];
    
}

/*
 * Styles a label inside the LoginInfoCell
 */
+ (void)styleLoginInfoCellLabelSmall:(UILabel *)aLabel {
    
    UIFont *font = [self boldFontWithSize:LOGIN_INFO_CELL_LABEL_TEXT_SMALL_SIZE];
    
    aLabel.font = font;
    aLabel.textColor = [UIColor BBVACoolGreyColor];
    
}

/*
 * Styles the given label with the correct color due to value of the given amount
 */
+ (void)styleLabelColor:(UILabel *)label forAmount:(NSDecimalNumber *)amount {
    
    if ([amount compare:[NSDecimalNumber zero]] != NSOrderedAscending) {
        
        label.textColor = [UIColor BBVATurquoiseToneFourColor];
        
    } else {
        
        label.textColor = [UIColor BBVAMagentaToneOneColor];
        
    }
    
}

/**
 * Styles a label as a picker label
 *
 * @param aLabel The label to style
 */
+ (void)stylePickerLabel:(UILabel *)aLabel {
    
    aLabel.backgroundColor = [UIColor clearColor];
    aLabel.minimumFontSize = 12.0f;
    aLabel.adjustsFontSizeToFitWidth = YES;
    aLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    aLabel.lineBreakMode = UILineBreakModeHeadTruncation;
    
}

/*
 * Styles an amount and currency label in a transaction detail cell
 */
+ (void)styleAmountAndCurrencyLabelInTransactionDetailCell:(AmountAndCurrencyLabel *)anAmountAndCurrencyLabel {
    
    CGFloat nominalFontSize = 16.0f;
    CGFloat currencyFontSize = 11.0f;
    UIFont *font = [self boldFontWithSize:nominalFontSize];
    
    anAmountAndCurrencyLabel.font = font;
    anAmountAndCurrencyLabel.nominalFontSize = nominalFontSize;
    anAmountAndCurrencyLabel.currencyRelativeFontSize = currencyFontSize / nominalFontSize;
    anAmountAndCurrencyLabel.textColor = [UIColor BBVACoolGreyColor];
    anAmountAndCurrencyLabel.highlightedTextColor = [UIColor BBVACoolGreyColor];
    anAmountAndCurrencyLabel.isCurrencyToTheLeft = YES;
    
}

/*
 * Styles an amount and currency label in a product cell
 */
+ (void)styleAmountAndCurrencyLabelInProductCell:(AmountAndCurrencyLabel *)anAmountAndCurrencyLabel {
    
    CGFloat nominalFontSize = 30.0f;
    CGFloat currencyFontSize = 16.0f;
    UIFont *font = [self normalFontWithSize:nominalFontSize];
    
    anAmountAndCurrencyLabel.font = font;
    anAmountAndCurrencyLabel.nominalFontSize = nominalFontSize;
    anAmountAndCurrencyLabel.currencyRelativeFontSize = currencyFontSize / nominalFontSize;
    anAmountAndCurrencyLabel.textColor = [UIColor BBVATurquoiseToneFourColor];
    anAmountAndCurrencyLabel.highlightedTextColor = [UIColor BBVACoolGreyColor];
    anAmountAndCurrencyLabel.isCurrencyToTheLeft = YES;
    
}

/*
 * Styles an amount and currency label in a transaction cell
 */
+ (void)styleAmountAndCurrencyLabelInTransactionCell:(AmountAndCurrencyLabel *)anAmountAndCurrencyLabel {
    
    CGFloat nominalFontSize = 16.0f;
    CGFloat currencyFontSize = 11.0f;
    UIFont *font = [self boldFontWithSize:nominalFontSize];
    
    anAmountAndCurrencyLabel.font = font;
    anAmountAndCurrencyLabel.nominalFontSize = nominalFontSize;
    anAmountAndCurrencyLabel.currencyRelativeFontSize = currencyFontSize / nominalFontSize;
    anAmountAndCurrencyLabel.textColor = [UIColor BBVACoolGreyColor];
    anAmountAndCurrencyLabel.highlightedTextColor = [UIColor BBVACoolGreyColor];
    anAmountAndCurrencyLabel.isCurrencyToTheLeft = YES;
    
}

/*
 * Styles an ITF amount and currency label in a transaction cell
 */
+ (void)styleITFAmountAndCurrencyLabelInTransactionCell:(AmountAndCurrencyLabel *)anITFAmountAndCurrencyLabel {
    
    CGFloat nominalFontSize = 12.0f;
    CGFloat currencyFontSize = 8.0f;
    UIFont *font = [self normalFontWithSize:nominalFontSize];
    
    anITFAmountAndCurrencyLabel.font = font;
    anITFAmountAndCurrencyLabel.nominalFontSize = nominalFontSize;
    anITFAmountAndCurrencyLabel.currencyRelativeFontSize = currencyFontSize / nominalFontSize;
    anITFAmountAndCurrencyLabel.textColor = [UIColor BBVATurquoiseToneFourColor];
    anITFAmountAndCurrencyLabel.highlightedTextColor = [UIColor BBVAGreyToneTwoColor];
    anITFAmountAndCurrencyLabel.isCurrencyToTheLeft = YES;
    
}

/*
 * Styles an amount and currency label in a global position cell
 */
+ (void)styleAmountAndCurrencyLabelInGlobalPositionCell:(AmountAndCurrencyLabel *)anAmountAndCurrencyLabel {
    
    CGFloat nominalFontSize = 13.0f;
    CGFloat currencyFontSize = 9.0f;
    UIFont *font = [self normalFontWithSize:nominalFontSize];
    
    anAmountAndCurrencyLabel.font = font;
    anAmountAndCurrencyLabel.nominalFontSize = nominalFontSize;
    anAmountAndCurrencyLabel.currencyRelativeFontSize = currencyFontSize / nominalFontSize;
    anAmountAndCurrencyLabel.textColor = [UIColor BBVACoolGreyColor];
    anAmountAndCurrencyLabel.isCurrencyToTheLeft = YES;
    anAmountAndCurrencyLabel.canResizeVertically = NO;
    anAmountAndCurrencyLabel.canResizeHorizontally = NO;
    anAmountAndCurrencyLabel.textAlignment = UITextAlignmentRight;
    
}

#pragma mark -
#pragma mark Table cell styling

/*
 * Styles a cell providing it with a background for unselected and selected state
 */
+ (void)styleCell:(UITableViewCell *)aCellView withSelectedBackground:(UIImage *)aSelectedBackground unselectedBackground:(UIImage *)anUnselectedBackground {
    
    aCellView.backgroundView = [[[UIImageView alloc] initWithImage:anUnselectedBackground] autorelease];
    aCellView.selectedBackgroundView = [[[UIImageView alloc] initWithImage:aSelectedBackground] autorelease];
    
}

/*
 * Styles the login information cell
 */
+ (void)styleLoginInformationCell:(UITableViewCell *)aCell {
    
    UIView *backgroundView = [[[UIView alloc] init] autorelease];
    backgroundView.backgroundColor = [UIColor BBVAGreyToneFiveColor];
    aCell.backgroundView = backgroundView;
}

/*
 * Styles the favourites category cell
 */
+ (void)styleFavouritesCategoryCell:(UITableViewCell *)aCell {
    
    UIView *backgroundView = [[[UIView alloc] init] autorelease];
	backgroundView.backgroundColor = [UIColor whiteColor];
    
    aCell.backgroundView = backgroundView;
    
}

/**
 * Styles the buttons cell
 */
+ (void)styleButtonsCell:(UITableViewCell *)aCell {
	UIView *backgroundView = [[[UIView alloc] init] autorelease];
    backgroundView.backgroundColor = [UIColor BBVATurquoiseToneThreeColor];
    
    aCell.backgroundView = backgroundView;
}

+(void)styleGroupedTableViewForiOS7:(UITableView *)aTableView Cell:(UITableViewCell *)aCell IndexPath:(NSIndexPath *)aIndexPath{
    
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        
        return;
    
    CGFloat cornerRadius = 5.f;
    
    aCell.backgroundColor = UIColor.clearColor;
    
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
    
    CGMutablePathRef pathRef = CGPathCreateMutable();
    
    CGRect bounds = CGRectInset(aCell.bounds, 8, 0);
    
    BOOL addLine = NO;
    
    if (aIndexPath.row == 0 && aIndexPath.row == [aTableView numberOfRowsInSection:aIndexPath.section]-1) {
        
        CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
        
    } else if (aIndexPath.row == 0) {
        
        CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
        
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
        
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
        
        CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
        
        addLine = YES;
        
    } else if (aIndexPath.row == [aTableView numberOfRowsInSection:aIndexPath.section]-1) {
        
        CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
        
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
        
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
        
        CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
        
    } else {
        
        CGPathAddRect(pathRef, nil, bounds);
        
        addLine = YES;
        
    }
    
    layer.path = pathRef;
    
    CFRelease(pathRef);
    
    layer.fillColor = [UIColor colorWithWhite:1.f alpha:1.f].CGColor;
    
    
    
    if (addLine == YES) {
        
        CALayer *lineLayer = [[CALayer alloc] init];
        
        CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
        
        lineLayer.frame = CGRectMake(CGRectGetMinX(bounds), bounds.size.height-lineHeight, bounds.size.width, lineHeight);
        
        lineLayer.backgroundColor = [UIColor grayColor].CGColor;
        
        [layer addSublayer:lineLayer];
        
    }
    
    layer.lineWidth=1;
    
    layer.strokeColor=[UIColor grayColor].CGColor;
    
    layer.frame=CGRectMake(layer.frame.origin.x, layer.frame.origin.y, layer.frame.size.width-2, layer.frame.size.height);
    
    
    
    UIView *testView = [[UIView alloc] initWithFrame:bounds];
    
    [testView.layer insertSublayer:layer atIndex:0];
    
    testView.backgroundColor = UIColor.clearColor;
    
    
    
    aCell.backgroundView = testView;
    
    
    
    
    
}

#pragma mark -
#pragma mark Table view styling

/*
 * Styles a table view providing it its background color and the cell separator
 */
+ (void)styleTableView:(UITableView *)aTableView {
    
    aTableView.backgroundColor = [UIColor BBVAGreyToneFiveColor];
    
    if ([aTableView respondsToSelector:@selector(setBackgroundView:)]) {
        
        [aTableView setBackgroundView:nil];
        
    }
    
    aTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

#pragma mark -
#pragma mark View styling

/*
 * Styles an NXT view prividing it with the default background color
 */
+ (void)styleNXTView:(UIView *)aView {
    
    aView.backgroundColor = [UIColor BBVAGreyToneFiveColor];
    
}

#pragma mark -
#pragma mark Custom graphics

/*
 * Styles an account graphic providing it with the background color
 */
+ (void)styleAccountGraphic:(NXTAccountGraphic *)anAccountGraphic {
    
    anAccountGraphic.backgroundColor = [UIColor colorWithWhite:0.9490f alpha:1.0f];
    
}

#pragma mark -
#pragma mark Navigation bar

/*
 * Styles the navigation bar with the colors of BBVA
 */
+ (void)styleNavigationBar:(UINavigationBar *)navBar {
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)
    {
        navBar.tintColor = [UIColor BBVABlueSpectrumColor];
    }
    else{
        navBar.tintColor = [UIColor BBVAWhiteColor];
    }
}

#pragma mark -
#pragma mark Toolbar

/*
 * Styles the toolbar bar with the colors of BBVA
 */
+ (void)styleToolbar:(UIToolbar *)toolbar {
    toolbar.tintColor = [UIColor BBVABlueColor];
}

#pragma mark -
#pragma mark Fonts

/*
 * Returns the normal font with a given font size
 */
+ (UIFont *)normalFontWithSize:(CGFloat)aFontSize {
    
    return [UIFont systemFontOfSize:aFontSize];
    
}


/*
 * Returns the bold font with a given font size
 */
+ (UIFont *)boldFontWithSize:(CGFloat)aFontSize {
    
    return [UIFont boldSystemFontOfSize:aFontSize];
    
}

+(void) styleSwitchForiOS7:(UISwitch *)aSwitch {
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [aSwitch setOnTintColor:[UIColor BBVABlueSpectrumColor]];
        [aSwitch setTintColor:[UIColor BBVABlueSpectrumColor]];
    }
}

@end
