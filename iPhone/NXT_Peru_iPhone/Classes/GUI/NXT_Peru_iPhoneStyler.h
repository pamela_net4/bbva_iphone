/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>

@class AlignableButton;
@class MOKTextField;
@class NXTTextField;
@class NXTAccountGraphic;
@class AmountAndCurrencyLabel;
@class MOKStringListSelectionButton;

/**
 * NXT_Peru_iPhone styler class provides static selectors to style different application interface elements. This is
 * a centralized way to change application style when needed
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXT_Peru_iPhoneStyler : NSObject {

}

/**
 * Styles a route modal view button
 *
 * @param button The route modal view button to style
 */
+ (void)styleRouteModalViewButton:(UIButton *)button;

/**
 * Styles a text field with gray text
 *
 * @param aTextField: The text field to apply style to
 * @param aSize: The font size
 * @param aColor: The color to text field.
 */
+ (void)styleTextField:(NXTTextField *)aTextField
          withFontSize:(CGFloat)aSize 
              andColor:(UIColor *)aColor;

/**
 * Styles a text field with gray text
 *
 * @param aTextField: The text field to apply style to
 * @param aSize: The font size
 * @param aColor: The color to text field.
 */
+ (void)styleTextFieldError:(NXTTextField *)aTextField
               withFontSize:(CGFloat)aSize
                   andColor:(UIColor *)aColor;

+(void)addError:(UIView*)aView;

+(void)removeError:(UIView *)aView;

/**
 * Styles a text field with gray text
 *
 * @param aTextField: The text field to apply style to
 * @param aSize: The font size
 * @param aColor: The color to text field.
 */
+ (void)styleMokTextField:(MOKTextField *)aTextField
             withFontSize:(CGFloat)aSize 
                 andColor:(UIColor *)aColor;

/**
 * Styles a text view with gray text
 *
 * @param textField: The text field to change theme.
 * @param size: The size of font.
 * @param aColor: The color to text field.
 */
+ (void)styleTextView:(UITextView *)textField 
         withFontSize:(CGFloat)size
             andColor:(UIColor *)aColor;

/**
 * Styles a label with a given font size and text color
 *
 * @param label The label to style
 * @param fontSize The font size
 * @param textColor The text color
 */
+ (void)styleLabel:(UILabel *)label
      withFontSize:(CGFloat)fontSize
             color:(UIColor *)textColor;

/**
 * Styles a label with a given bold font size and text color
 *
 * @param label The label to style
 * @param fontSize The font size
 * @param textColor The text color
 */
+ (void)styleLabel:(UILabel *)label
  withBoldFontSize:(CGFloat)fontSize
             color:(UIColor *)textColor;

/**
 * Styles the given label with the correct color due to value of the given amount
 *
 * @param label to style
 * @param amount used to check
 */
+ (void)styleLabelColor:(UILabel *)label forAmount:(NSDecimalNumber *)amount;

/**
 * Styles a label as a picker label
 *
 * @param aLabel The label to style
 */
+ (void)stylePickerLabel:(UILabel *)aLabel;

/**
 * Styles a label inside a MoreTabViewCell
 *
 * @param aLabel The label to style
 */
+ (void)styleMoreTabViewCellLabel:(UILabel *)aLabel;

/**
 * Styles a label inside the IconActionCell
 *
 * @param aLabel The label to style
 */
+ (void)styleIconActionCellLabel:(UILabel *)aLabel;

/**
 * Styles a label inside the LoginInfoCell
 *
 * @param aLabel The label to style
 */
+ (void)styleLoginInfoCellLabel:(UILabel *)aLabel;


/**
 * Styles a label inside the LoginInfoCell Small
 *
 * @param aLabel The label to style
 */
+ (void)styleLoginInfoCellLabelSmall:(UILabel *)aLabel;

/**
 * Styles a table view providing it its background color and the cell separator
 *
 * @param aTableView The table view to style
 */
+ (void)styleTableView:(UITableView *)aTableView;

/**
 * Styles a button with white text and given background
 *
 * @param aButton The button to apply style to
 * @param aSize The button title font size
 * @param aBackground The image used as background
 */
+ (void)styleButton:(UIButton *)aButton withWhiteFontSize:(CGFloat)aSize background:(UIImage *)aBackground;

/**
 * Styles a button with white text and given background
 *
 * @param aButton The button to apply style to
 * @param aSize The button title font size
 * @param aBackground The color used as background
 */
+ (void)styleButton:(UIButton *)aButton withWhiteFontSize:(CGFloat)aSize backgroundColor:(UIColor *)aBackground;

/**
 * Styles an application standard blue button
 *
 * @param aButton The button to style
 */
+ (void)styleBlueButton:(UIButton *)aButton;

/**
 * Styles an application standard white button
 * 
 * @param aButton The button to style
 */
+ (void)styleWhiteButton:(UIButton *)aButton;

+ (void)styleBoldWhiteButton:(UIButton *)aButton ;

/**
 * Styles an application standard gray button
 *
 * @param aButton The button to style
 */
+ (void)styleGrayButton:(UIButton *)aButton;

+ (void)styleGrayButtonTextLight:(UIButton *)aButton;

+ (void)styleLightGrayButton:(UIButton *)aButton;

/**
 * Styles a button with a bevel background and light blue text
 *
 * @param aButton The button to style
 */
+ (void)styleBevelButton:(UIButton *)aButton;

/**
 * Styles a button with a gray background, white font black shaded text and a disclosure arrow on the right side
 *
 * @param aButton The button to style
 */
+ (void)styleComboButton:(UIButton *)aButton;


+ (void)styleComboButton:(UIButton *)aButton isNewDesign:(BOOL)isNewDesign;

/**
 * Styles a button with a gray background, white font black shaded text and a disclosure arrow on the right side
 *
 * @param aButton The button to style
 */
+ (void)styleStringListButton:(MOKStringListSelectionButton *)aButton;

/**
 * Styles a button used to select an account with a gray background, white font black shaded text and a disclosure arrow on the right side
 *
 * @param aButton The button to style
 */
+ (void)styleAccountSelectionButton:(MOKStringListSelectionButton *)aButton;

/**
 * Styles a button as a cell option button
 *
 * @param aButton The button to style
 * @param title of the button
 */
+ (void)styleCellOptionButton:(UIButton *)aButton withTitle:(NSString *)title;

/**
 * Styles a button as a bottom bar button
 *
 * @param aButton The button to style
 * @param title of the button
 */
+ (void)styleBottomBarButton:(UIButton *)aButton withTitle:(NSString *)title;

/**
 * Styles an action icon button (contains only an icon as the button image)
 *
 * @param aButton The button to style
 */
+ (void)styleActionIconButton:(UIButton *)aButton;

/**
 * Styles a button with segmented aspect(left side)
 *
 * @param aButton The button to style
 */
+ (void)styleLeftSegmentedButton:(UIButton *)aButton;

/**
 * Styles a button with segmented aspect(right side)
 *
 * @param aButton The button to style
 */
+ (void)styleRightSegmentedButton:(UIButton *)aButton;

/**
 * Styles a stock graphic type button
 *
 * @param aButton The button to style
 */
+ (void)styleStockGraphicTypeButton:(UIButton *)aButton;

/**
 * Styles a cell providing it with a background for unselected and selected state
 *
 * @param aCellView The cell view to apply style to
 * @param aSelectedBackground selected background image
 * @param anUnselectedBackground unselected background image
 */
+ (void)styleCell:(UITableViewCell *)aCellView withSelectedBackground:(UIImage *)aSelectedBackground unselectedBackground:(UIImage *)anUnselectedBackground;

/**
 * Styles an NXT view prividing it with the default background color
 *
 * @param aView The view to style
 */
+ (void)styleNXTView:(UIView *)aView;

/**
 * Styles the login information cell
 *
 * @param aCell The login information cell to style
 */
+ (void)styleLoginInformationCell:(UITableViewCell *)aCell;

/**
 * Styles the favourites category cell
 *
 * @param aCell The favourites category cell to style
 */
+ (void)styleFavouritesCategoryCell:(UITableViewCell *)aCell;

/**
 * Styles the buttons cell
 *
 * @param aCell The buttons cell to style
 */
+ (void)styleButtonsCell:(UITableViewCell *)aCell;

/**
 * Styles an account graphic providing it with the background color
 *
 * @param anAccountGraphic The account graphic to style
 */
+ (void)styleAccountGraphic:(NXTAccountGraphic *)anAccountGraphic;

/**
 * Returns the normal font with a given font size
 *
 * @param aFontSize The font size to use
 * @return The normal font with a given font size
 */
+ (UIFont *)normalFontWithSize:(CGFloat)aFontSize;

/**
 * Returns the bold font with a given font size
 *
 * @param aFontSize The font size to use
 * @return The bold font with a given font size
 */
+ (UIFont *)boldFontWithSize:(CGFloat)aFontSize;

/**
 * Styles the navigation bar with the colors of BBVA
 *
 * @param navBar to style
 */
+ (void)styleNavigationBar:(UINavigationBar *)navBar;

/**
 * Styles the toolbar bar with the colors of BBVA
 *
 * @param toolbar The UIToolbar to tyle
 */
+ (void)styleToolbar:(UIToolbar *)toolbar;

/**
 * Styles the given button as the Exit button
 *
 * exitButton to apply style
 */
+ (void)styleExitButton:(UIButton *)exitButton;

/**
 * Styles the given button as a POI telephone button
 *
 * @param button to apply style
 */
+ (void)stylePOITelephoneButton:(UIButton *)button;

/**
 * Styles the given button as a transction type button
 *
 * @param button to apply style
 */
+ (void)styleTransactionTypeButton:(UIButton *)button;

/**
 * Styles the given button as a white button with an arrow on the right
 *
 * @param button to set style
 */
+ (void)styleWhiteButtonWithArrow:(UIButton *)button;

/*
 * Styles the given button as a white button with the text on the left and an arrow on the right
 *
 * @param button to set style
 */
+ (void)styleWhiteButtonWithTextOnTheLeftAndArrow:(UIButton *)button withFontSize:(CGFloat)fontSize;


/**
 * Styles an amount and currency label in a transaction detail cell
 *
 * @param anAmountAndCurrencyLabel The amount and currency label to style
 */
+ (void)styleAmountAndCurrencyLabelInTransactionDetailCell:(AmountAndCurrencyLabel *)anAmountAndCurrencyLabel;

/**
 * Styles an amount and currency label in a product cell
 *
 * @param anAmountAndCurrencyLabel The amount and currency label to style
 */
+ (void)styleAmountAndCurrencyLabelInProductCell:(AmountAndCurrencyLabel *)anAmountAndCurrencyLabel;

/**
 * Styles an amount and currency label in a transaction cell
 *
 * @param anAmountAndCurrencyLabel The amount and currency label to style
 */
+ (void)styleAmountAndCurrencyLabelInTransactionCell:(AmountAndCurrencyLabel *)anAmountAndCurrencyLabel;

/**
 * Styles an ITF amount and currency label in a transaction cell
 *
 * @param anITFAmountAndCurrencyLabel The amount and currency label to style
 */
+ (void)styleITFAmountAndCurrencyLabelInTransactionCell:(AmountAndCurrencyLabel *)anITFAmountAndCurrencyLabel;

/**
 * Styles an amount and currency label in a global position cell
 *
 * @param anAmountAndCurrencyLabel The amount and currency label to style
 */
+ (void)styleAmountAndCurrencyLabelInGlobalPositionCell:(AmountAndCurrencyLabel *)anAmountAndCurrencyLabel;

/*
 * Styles a label giving it a blue bold font and white shadow with the provided font size
 */
+ (void)styleLabel:(UILabel *)aLabel withBlueWhiteShadedBoldFontSize:(CGFloat)aFontSize;

/**
 *
 * Support for old style grouped TableView in iOS7
 * @param  atableView TableView which contain the cell
 * @param  acell Cell which style is going to be supported
 * @param  aindexPath current position of the cell in the tableView
 *
 */
+(void)styleGroupedTableViewForiOS7:(UITableView *)aTableView Cell:(UITableViewCell *)aCell IndexPath:(NSIndexPath *)aIndexPath;

/**
 * Sets a white font to the provided button using the given font size and colors
 *
 * @param aButton The button to apply style to
 * @param aBoldFont A flag to use a bold font or a normal one (YES for a bold font, NO for a normal one)
 * @param aSize The font size
 * @param anEnabledTitleColor The title color when button is enabled
 * @param aDisabledTitleColor The title color when button is disabled
 * @param aShadowColor The title shadow color
 */
+ (void)styleButton:(UIButton *)aButton withBoldFont:(BOOL)aBoldFont fontSize:(CGFloat)aSize color:(UIColor *)anEnabledTitleColor
      disabledColor:(UIColor *)aDisabledTitleColor andShadowColor:(UIColor *)aShadowColor;

+(void) styleSwitchForiOS7:(UISwitch *)aSwitch;

@end
