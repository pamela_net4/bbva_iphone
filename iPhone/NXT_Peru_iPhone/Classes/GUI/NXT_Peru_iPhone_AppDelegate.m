/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "NXT_Peru_iPhone_AppDelegate.h"

#import "AccountList.h"
#import "BBVATabBarTabInformation.h"
#import "BBVATabBarViewController.h"
#import "BrowserViewController.h"
#import "Constants.h"
#import "DateLegalTermsResponse.h"
#import "DirConfiguration.h"
#import "FrequentOperationListViewController.h"
#import "GeoServerManager.h"
#import "GlobalPositionViewController.h"
#import "ImagesCache.h"
#import "ImagesFileNames.h"
#import "MATConfigurationResponse.h"
#import "MATLogNames.h"
#import "MCBConfiguration.h"
#import "MCBFacade.h"
#import "MoreTabViewController.h"
#import "NXTActivityIndicator.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "NXTWindow.h"
#import "PaymentsListViewController.h"
#import "POIMapViewController.h"
#import "Session.h"
#import "StringKeys.h"
#import "SplashViewController.h"
#import "TransfersListViewController.h"
#import "Tools.h"
#import "UIColor+BBVA_Colors.h"
#import "Updater.h"
#import "NSData+AES256.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "ACTReporter.h"

#define BBVA_LINE_DIALOG_TAG            1111
#define EXIT_DIALOG_TAG                 2222
#define SESSION_EXPIRED_TAG             3333

#define kSplashScreenAnimation          @"kSplashScreenAnimation"
#define kLegalTermsAnimation            @"kLegalTermsAnimation"
#define kEncryptionRadioKey             @"k3ncr1pt10nR4d10K3y"
#define kRadioKey                       @"gBbMmwNwJtqDCKSaUCd4+bEVBNjNshSKpEdm5bRBXaI="

/**
 * Defines the global position tab identifier
 */
NSString * const kGlobalPositionTabIdentifier = @"globalPositionTab";

/**
 * Defines the transfer list tab identifier
 */
NSString * const kTransferPositionTabIdentifier = @"transferPositionTab";

/**
 * Defines the payment list tab identifier
 */
NSString * const kPaymentPositionTabIdentifier = @"paymentPositionTab";

/**
 * Defines the Frequent Operations list tab identifier
 */
NSString * const kFOPositionTabIdentifier = @"FreqOperationsPositionTab";

/**
 * Defines the location map tab identifier
 */
NSString * const kPoiMapPositionTabIdentifier = @"poiMapPositionTab";

/**
 * Defines the transfers tab identifier
 */
NSString * const kTransfersTabIdentifier = @"transfersTab";

/**
 * Defines the favourites tab identifier
 */
NSString * const kFavouritesTabIdentifier = @"favouritesTab";

/**
 * Defines the accounts tab identifier
 */
NSString * const kAccountsTabIdentifier = @"accountsTab";

/**
 * Defines the cards tab identifier
 */
NSString * const kCardsTabIdentifier = @"cardsTab";

/**
 * Defines the personal area tab identifier
 */
NSString * const kPersonalAreaTabIdentifier = @"personalAreaTab";

/**
 * Defines the mobile phone charge tab identifier
 */
NSString * const kMobilePhoneChargeTabIdentifier = @"mobilePhoneChargeTab";

/**
 * Defines the ATM and branch locator tab identifier
 */
NSString * const kATMAndBranchLocatorTabIdentifier = @"atmAndBranchLocatorTab";

/**
 * Defines the legal notice tab identifier
 */
NSString * const kLegalNoticeTabIdentifier = @"legalNoticeTab";

/**
 * Defines the QR codes tab identifier
 */
NSString * const kQRCodesTabIdentifier = @"qrCodesTab";

/**
 * Defines the help tab identifier
 */
NSString * const kHelpTabIdentifier = @"helpTab";

/**
 * Defines the remote manager tab identifier
 */
NSString * const kRemoteManagerTabIdentifier = @"remoteManagerTab";

/**
 * Defines the call BBVA line tab identifier
 */
NSString * const kCallBBVALineTabInformation = @"callBBVALineTab";

/**
 * Defines the tab identifers order array user preferences key
 */
NSString * const kTabIdentifiersOrderArrayUserPreferencesKey = @"net.movilok.NXTAppDelegate.tabIdentifiersOrderArrayUserPreferencesKey";

/**
 * Defines the FAQ URL user preferences key
 */
NSString * const kRemoteFaqURLPreferencesKey = @"net.movilok.NXTAppDelegate.remoteFaqURLPreferencesKey";


@implementation PhoneCallManager

#pragma mark -
#pragma mark Properties

@synthesize phoneNumber = phoneNumber_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [phoneNumber_ release];
    phoneNumber_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. A nil PhoneCallManager instance is returned, as it cannot be created without a phone number
 *
 * @return A nil PhoneCallManager instance
 */
- (id)init {
    
    [self autorelease];
    
    return nil;
    
}

/*
 * Designated initializer. Initializes a PhoneCallManager instance with the provided phone number
 */
- (id)initWithPhoneNumber:(NSString *)aPhoneNumber {
    
    if ((self = [super init])) {
        
        phoneNumber_ = [aPhoneNumber copyWithZone:[self zone]];
    }
    
    return self;
    
}

/*
 * Shows the dialog
 */
- (void)show {
    
    [self showWithTitle:NSLocalizedString(BBVA_LINE_DIALOG_TITLE_KEY,nil) andMessage:NSLocalizedString(BBVA_LINE_DIALOG_MESSAGE_KEY, nil)];
}

/*
 * Shows the dialog
 */
- (void)showWithTitle:(NSString *)title andMessage:(NSString *)message {
    
    UIAlertView *dialog = [[[UIAlertView alloc] initWithTitle:title
                                                      message:message
                                                     delegate:self
                                            cancelButtonTitle:NSLocalizedString(NO_TEXT_KEY, nil)
                                            otherButtonTitles:nil] autorelease];
    
    NSString *buttonText = NSLocalizedString(YES_TEXT_KEY, nil);
    [dialog addButtonWithTitle:buttonText];
    dialog.tag = BBVA_LINE_DIALOG_TAG;
    
    [dialog show];
    
}

#pragma mark -
#pragma mark UIAlertViewDelegate protocol selectors

/**
 * Sent to the delegate when the user clicks a button on an alert view. When OK button is clicked, the phone call is started
 *
 * @param alertView The alert view containing the button
 * @param buttonIndex The position of the clicked button. The button indices start at 0
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
    	[Tools startPhoneCallToNumber:phoneNumber_];        
    }
}

@end

#pragma mark -

/**
 * NXTAppDelegate private extension
 */
@interface NXT_Peru_iPhone_AppDelegate()

/**
 * Initializes the images stretching. Provides the ImagesCache singleton with the stretching information to every image that needs it
 *
 * @private
 */
- (void)initializeImagesStretching;

/**
 * Initializes the connected tab bar buttons with the needed view controllers
 *
 * @private
 */
- (void)initializeConnectedTabBarButtons;

/**
 * Load the user preferences
 *
 * @private
 */
- (void)loadUserPreferences;

/**
 * Close response received
 *
 * @private
 */
- (void)closeResponseReceived:(NSNotification *)notification;

/**
 * Logout response received
 *
 * @private
 */
- (void)logoutResponseReceived:(NSNotification *)notification;

/**
 * Perform close animations
 *
 * @private
 */
- (void)doCloseAnimations;

/**
 * Legal conditions has been accepted by the user. The initial view is displayed, and the associated flag is updated
 *
 * @private
 */
- (void)legalConditionsAccepted;

/**
 * Window notifies it has been tapped. The automatic log-out event is reseted
 *
 * @param aNotification The notification object sent by the notification center
 * @private
 */
- (void)windowTappedNotification:(NSNotification *)aNotification;

/**
 * Resets the window tapped time event
 *
 * @param aDelay The delay to set the new invocation
 * @private
 */
- (void)resetWindowTappedTimeEventWithDelay:(NSTimeInterval)aDelay;

/**
 * Checks the session expiration
 *
 * @private
 */
- (void)checkSessionExpiration;

/**
 * Releases the tab bars information instances. First action performed is ask for the tab view controller for every tab so the original navigation controller is loaded with the view controller
 *
 * @private
 */
- (void)releaseTabBarsInformation;

/**
 * Populates the current tabs order array with the preferences information
 *
 * @private
 */
- (void)populateCurrentTabsOrderArray;

/**
 * The datelegal terms reponse has been received
 *
 * @private
 */
- (void)dateLegalTermsResponseReceived:(NSNotification *)notification;

/**
 * The MAT configuration reponse has been received
 *
 * @private
 */
- (void)MATConfigurationResponseReceived:(NSNotification *)notification;

/**
 * The close animations has been finished
 *
 * @private
 */
- (void)closeAnimationEnded;

/**
 * Show dialog with bbva line.
 *
 * @private
 */
- (void)showCallBBVALineDialog;

@end


#pragma mark -

@implementation NXT_Peru_iPhone_AppDelegate

#pragma mark -
#pragma mark Properties

@synthesize window = window_;
@synthesize disconnectedNavigationController = disconectedNavigationController_;
@synthesize connectedTabBarViewController = connectedTabBarViewController_;
@synthesize splashViewController = splashViewController_;
@synthesize initialLegalConditionsViewController = initialLegalConditionsViewController_;
@synthesize initialLegalConditionsNavigationController = initialLegalConditionsNavigationController_;
@synthesize smsConfigurationMessageShown = smsConfigurationMessageShown_;
@synthesize lastUsernameLogged = lastUsernameLogged_;
@synthesize accountGraphicDisplayed = accountGraphicDisplayed_;
@synthesize stockGraphicDisplayed = stockGraphicDisplayed_;
@synthesize firstRun = firstRun_;
@dynamic onDisconnectedViews;
@synthesize hideGPChartAdvice = hideGPChartAdvice_;
@synthesize gpChartAdviceShown = gpChartAdviceShown_;
@synthesize hideStockBuySellAdvice = hideStockBuySellAdvice_;
@synthesize stockBuySellAdviceShown = stockBuySellAdviceShown_;
@synthesize poiMapNetworksFilter = poiMapNetworksFilter_;
@synthesize modelObjectToNavigate = modelObjectToNavigate_;
@synthesize dateLegalTerms = dateLegalTerms_;
@synthesize faqURL = faqURL_;
@synthesize legalContitionsAccepted = legalContitionsAccepted_;
@synthesize hideStockAccountChartAdvice = hideStockAccountChartAdvice_;
@synthesize stockAccountChartAdviceShown = stockAccountChartAdviceShown_;
@synthesize radioStreamManager = radioStreamManager_;
@synthesize deeplinkOption = deeplinkOption_;

#pragma mark -
#pragma mark Application lifecycle

/**
 * Tells the delegate when the application has finished launching
 *
 * @param application The delegating application object
 * @param launchOptions A dictionary indicating the reason the application was launched (if any)
 * @return NO if the application cannot handle the URL resource, otherwise return YES
 */
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"vienedeFrecuente"];
    
    // BBVA IOS - Iphone
    // Google iOS first open tracking snippet
    // Add this code to your application delegate's
    // application:didFinishLaunchingWithOptions: method.
    
    // Enable automated usage reporting.
    [ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:@"1019552767"];
    
    [ACTConversionReporter reportWithConversionID:@"977810892" label:@"GHc8CNDjn2UQzOug0gM" value:@"1.00" isRepeatable:NO];
    
    [self fnInsertarBaseDatosADocumentos];
    [self fnDeleteCacheIfExists];
    
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:kRadioKey options:0];
    NSData *decryptedData = [decodedData AES256DecryptWithKey:kEncryptionRadioKey];
    NSString *decryptedString = [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
   
    radioStreamManager_ = [[RadioStreamController alloc] initWithURL:decryptedString];
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1){
        
        //[[UINavigationBar appearance] setBarTintColor:[UIColor BBVABlueSpectrumColor]];
        //[[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    
    
#if defined(NOT_MAT_LOGGING_ENABLED)
    [MCBFacade setEnabled:NO];
#endif //NOT_MAT_LOGGING_ENABLED
    
    [MCBFacade setMATServerSchemeName:@"https://"];
    [MCBFacade setMATServerAuthority:MCB_AUTHORITY_SERVER];
    
    [MCBFacade invokeLogAppDetailsWithAppName:MCB_APPLICATION_NAME
                                       appKey:MCB_APPLICATION_KEY
                                   appCountry:NXT_APPLICATION_COUNTRY
                                   appVersion:APP_VERSION_STRING
                                     latitude:0.0f
                                    longitude:0.0f
                                validLocation:NO];
    
    NSMutableArray *matNamesToLog = [NSMutableArray arrayWithObjects:MAT_LOG_LOGIN,
                                     MAT_LOG_VALIDATE_COORDINATES,
                                     MAT_LOG_LOGOUT,
                                     MAT_LOG_SESSION_EXPIRED,
                                     MAT_LOG_GLOBAL_POSITION,
                                     MAT_LOG_ACCOUNTS_TRANSACTIONS,
                                     MAT_LOG_CARDS_TRANSACTIONS,
                                     MAT_LOG_RETENTIONS,
                                     MAT_LOG_SEND_TRANSACTION,
                                     MAT_LOG_SEND_TRANSACTION_CONFIRMATION,
                                     MAT_LOG_OWN_ACCOUNTS_TRANSFER_SEND,
                                     MAT_LOG_OWN_ACCOUNTS_TRANSFER_CONFIRMATION,
                                     MAT_LOG_OWN_ACCOUNTS_TRANSFER_SUCCESS,
                                     MAT_LOG_OTHER_BANK_CUSTOMER_TRANSFER_SEND,
                                     MAT_LOG_OTHER_BANK_CUSTOMER_TRANSFER_CONFIRMATION,
                                     MAT_LOG_OTHER_BANK_CUSTOMER_TRANSFER_SUCCESS,
                                     MAT_LOG_OTHER_BANK_TRANSFER_SEND,
                                     MAT_LOG_OTHER_BANK_TRANSFER_CONFIRMATION,
                                     MAT_LOG_OTHER_BANK_TRANSFER_SUCCESS,
                                     MAT_LOG_PAYMENT_START,
                                     MAT_LOG_PAYMENT_LUZ_DEL_SUR_LIST,
                                     MAT_LOG_PAYMENT_LUZ_DEL_SUR_DATA,
                                     MAT_LOG_PAYMENT_LUZ_DEL_SUR_CONFIRMATION,
                                     MAT_LOG_PAYMENT_LUZ_DEL_SUR_SUCCESS,
                                     MAT_LOG_PAYMENT_EDELNOR_LIST,
                                     MAT_LOG_PAYMENT_EDELNOR_DATA,
                                     MAT_LOG_PAYMENT_EDELNOR_CONFIRMATION,
                                     MAT_LOG_PAYMENT_EDELNOR_SUCCESS,
                                     MAT_LOG_PAYMENT_SEDAPAL_LIST,
                                     MAT_LOG_PAYMENT_SEDAPAL_DATA,
                                     MAT_LOG_PAYMENT_SEDAPAL_CONFIRMATION,
                                     MAT_LOG_PAYMENT_SEDAPAL_SUCCESS,
                                     MAT_LOG_PAYMENT_TELEFONICA_LANDLINE_LIST,
                                     MAT_LOG_PAYMENT_TELEFONICA_LANDLINE_DATA,
                                     MAT_LOG_PAYMENT_TELEFONICA_LANDLINE_CONFIRMATION,
                                     MAT_LOG_PAYMENT_TELEFONICA_LANDLINE_SUCCESS,
                                     MAT_LOG_PAYMENT_MOVISTAR_CELULAR_LIST,
                                     MAT_LOG_PAYMENT_MOVISTAR_CELULAR_DATA,
                                     MAT_LOG_PAYMENT_MOVISTAR_CELULAR_CONFIRMATION,
                                     MAT_LOG_PAYMENT_MOVISTAR_CELULAR_SUCCESS,
                                     MAT_LOG_PAYMENT_CLARO_CELULAR_LIST,
                                     MAT_LOG_PAYMENT_CLARO_CELULAR_DATA,
                                     MAT_LOG_PAYMENT_CLARO_CELULAR_CONFIRMATION,
                                     MAT_LOG_PAYMENT_CLARO_CELULAR_SUCCESS,
                                     MAT_LOG_PAYMENT_OWN_CARDS_LIST,
                                     MAT_LOG_PAYMENT_OWN_CARDS_DATA,
                                     MAT_LOG_PAYMENT_OWN_CARDS_CONFIRMATION,
                                     MAT_LOG_PAYMENT_OWN_CARDS_SUCCESS,
                                     MAT_LOG_PAYMENT_OTHERS_CARDS_DATA,
                                     MAT_LOG_PAYMENT_OTHERS_CARDS_CONFIRMATION,
                                     MAT_LOG_PAYMENT_OTHERS_CARDS_SUCCESS,
                                     MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_LIST,
                                     MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_DATA,
                                     MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_CONFIRMATION,
                                     MAT_LOG_PAYMENT_OTHER_BANKS_CARDS_SUCCESS,
                                     MAT_LOG_PAYMENT_PREPAY_DATA,
                                     MAT_LOG_PAYMENT_PREPAY_CONFIRMATION,
                                     MAT_LOG_PAYMENT_PREPAY_TELEFONICA_SUCCESS,
                                     MAT_LOG_PAYMENT_PREPAY_CLARO_SUCCESS,
                                     MAT_LOG_SUSCRIPTION_CLIENT_IN,
                                     MAT_LOG_CHANGE_CLIENT_IN,
                                     MAT_LOG_SUSCRIPTION_CLIENT_VERIFY,
                                     MAT_LOG_CHANGE_CLIENT_VERIFY,
                                     MAT_LOG_SUSCRIPTION_CLIENT_DO,
                                     MAT_LOG_CHANGE_CLIENT_DO,
                                     MAT_LOG_SUSCRIPTION_CLIENT_TERMS,
                                     MAT_LOG_CHANGE_CLIENT_DO_TERMS,
                                     MAT_LOG_RADIO_BBVA_OPERATION,
                                     MAT_LOG_CAMPAIGN_LIST_OPERATION,
                                     MAT_LOG_INCREMENT_OF_LINE_STEP_ONE_OPERATION,
                                     MAT_LOG_INCREMENT_OF_LINE_STEP_TWO_OPERATION,
                                     MAT_LOG_INCREMENT_OF_LINE_STEP_THREE_OPERATION,
                                     MAT_LOG_FAST_LOAN_STEP_ONE_OPERATION,
                                     MAT_LOG_FAST_LOAN_STEP_TWO_OPERATION,
                                     MAT_LOG_FAST_LOAN_STEP_THREE_OPERATION,
                                     MAT_LOG_FAST_LOAN_TERM_OPERATION,
                                     MAT_LOG_FAST_LOAN_SIMULATION_OPERATION,
                                     MAT_LOG_FAST_LOAN_SCHEDULE_OPERATION,
                                     MAT_LOG_SALARY_ADVANCE_AFFILIATION_OPERATION,
                                     MAT_LOG_SALARY_ADVANCE_RECEIVE_OPERATION,
                                     MAT_LOG_SALARY_ADVANCE_SUMMARY_OPERATION,
                                     MAT_LOG_SALARY_ADVANCE_AFFILIATION_CONFIRM_OPERATION,
                                     nil];
    
    MCBConfiguration *matConfiguration = [[[MCBConfiguration alloc] init] autorelease];
    [matConfiguration setMethods:matNamesToLog];
    [MCBFacade setMATConfiguration:matConfiguration];

    [window_ setFrame:[[UIScreen mainScreen] bounds]];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self selector:@selector(MATConfigurationResponseReceived:) name:kNotificationMATConfigurationReceived object:nil];
    [notificationCenter addObserver:self selector:@selector(dateLegalTermsResponseReceived:) name:kNotificationDateLegalTermsReceived object:nil];
    [notificationCenter addObserver:self selector:@selector(closeResponseReceived:) name:kNotificationCloseEnds object:nil];
    [notificationCenter addObserver:self selector:@selector(logoutResponseReceived:) name:kNotificationLogoutEnds object:nil];
    
    [self loadUserPreferences];
    [self initializeImagesStretching];
    [DirConfiguration createDirectoryStructure];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    UIImage *moreTabUnselectedImage = [imagesCache imageNamed:MORE_TAB_UNSELECTED_ICON];
    connectedTabBarViewController_.delegate = self;
    connectedTabBarViewController_.navigationBarColor = [UIColor BBVATurquoiseColor];
    connectedTabBarViewController_.moreTabText = NSLocalizedString(MORE_TABS_TAB_TEXT_KEY, nil);
    connectedTabBarViewController_.moreTabReferenceImage = moreTabUnselectedImage;
    connectedTabBarViewController_.moreTabUnselectedImage = moreTabUnselectedImage;
    connectedTabBarViewController_.moreTabBackgroundColor = [UIColor BBVATurquoiseToneTwoColor];
    connectedTabBarViewController_.moreTabTableBackgroundColor = [UIColor BBVATurquoiseToneTwoColor];
    connectedTabBarViewController_.cellArrowImage = [imagesCache imageNamed:ARROW_ICON_IMAGE_FILE_NAME];
    connectedTabBarViewController_.cellSeparatorImage = [imagesCache imageNamed:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    connectedTabBarViewController_.moreTabCellIsNarrow = YES;
    connectedTabBarViewController_.moreTabElementsOrdered = YES;
    connectedTabBarViewController_.relocateHeaderBackgroundImage = [imagesCache imageNamed:HEADER_BACKGROUND_IMAGE_FILE_NAME];
    connectedTabBarViewController_.relocateTabDoneText = NSLocalizedString(OK_TEXT_KEY, nil);
    connectedTabBarViewController_.relocateNavigationBarClass = [UINavigationBar class];
    
    
    [NXT_Peru_iPhoneStyler styleNXTView:window_];
    [window_ setBackgroundColor:[UIColor BBVABlueSpectrumColor]];
    [window_ setAutoresizesSubviews:YES];
    [window_ setRootViewController:splashViewController_];
	currentViewController_ = splashViewController_;
    [window_ makeKeyAndVisible];
    
    [self.window setRootViewController:currentViewController_];
    
    initialLegalConditionsViewController_.showActionButton = YES;
    initialLegalConditionsViewController_.showAcceptButton = YES;
    initialLegalConditionsViewController_.mailSubject = NSLocalizedString(DISCLAIMER_EMAIL_SUBJECT_KEY, nil);
    initialLegalConditionsViewController_.mailBodyInitialText = NSLocalizedString(DISCLAIMER_EMAIL_BODY_KEY, nil);
    initialLegalConditionsViewController_.url = BROWSER_URL_DISCLAIMER;
    initialLegalConditionsViewController_.title = NSLocalizedString(DISCLAIMER_TITLE_KEY, nil);
    initialLegalConditionsNavigationController_.viewControllers = [NSArray arrayWithObject:initialLegalConditionsViewController_];
    
    gpChartAdviceShown_ = NO;
    stockAccountChartAdviceShown_ = NO;
    stockBuySellAdviceShown_ = NO;
    
	sessionPingInvocationRequired_ = NO;
    
    if (defaultOrderTabsArray_ == nil) {
        
        defaultOrderTabsArray_ = [[NSMutableArray alloc] init];
        
    }
    
    if (currentOrderTabsArray_ == nil) {
        
        currentOrderTabsArray_ = [[NSMutableArray alloc] init];
        
    }
    
    if (tabsDictionary_ == nil) {
        
        tabsDictionary_ = [[NSMutableDictionary alloc] init];
        
    }
    
    [connectedTabBarViewController_ setMoreTabLeftBarButton:nil
                                             rightBarButton:nil];
    
    obtainingLegalTermsFromBackground_ = NO;
	
	[splashViewController_ setDelegate:self];
    
    //Clear cache file with old registers
    BOOL isNotFirstTimeRun = [[NSUserDefaults standardUserDefaults] boolForKey:@"firstTimeRun"];
    if (!isNotFirstTimeRun) {
        // Clear the cache if this is a first time run
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        // Set the flag to true and synchronize the user defaults
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:YES forKey: @"firstTimeRun"];
        [defaults synchronize];
    }
    

    deeplinkOption_ = DEEPLINK_NONE;
    
    if (launchOptions[UIApplicationLaunchOptionsURLKey] == nil) {
        [FBSDKAppLinkUtility fetchDeferredAppLink:^(NSURL *url, NSError *error) {
            if (error) {
                DLog(@"Received error while fetching deferred app link %@", error);
            }
            if (url) {
                
                if([[url scheme] isEqualToString:@"bbvacontinentalapp"] && [[url host] isEqualToString:@"op=radiobbva"]){
                
                    deeplinkOption_ = DEEPLINK_RADIO_BBVA;
                }
                
            }
        }];
    }
    
    return YES;

}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    return YES;

}


/**
 * The deleate is notified that the SplashViewController instance finished its presentation and it is OK to hide it
 *
 * @param splashViewController The SplashViewController instance triggering the event
 */
- (void)splashViewControllerFinishedPresentation:(SplashViewController *)splashViewController {
    
    UIView *nextView = initialLegalConditionsNavigationController_.view;
    
    if (legalContitionsAccepted_) {
        
        nextView = disconectedNavigationController_.view;
        
    } else {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(legalConditionsAccepted)
                                                     name:BROWSER_VIEW_CONTROLLER_ACCEPT_BUTTON_TAPPED
                                                   object:initialLegalConditionsViewController_];
        
    }
    
    [window_ addSubview:nextView];
	currentViewController_ = (legalContitionsAccepted_) ? disconectedNavigationController_ : initialLegalConditionsNavigationController_;
    nextView.alpha = 0.0f;
    
	[UIView beginAnimations:kSplashScreenAnimation context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.5];
    
    splashViewController_.view.transform = CGAffineTransformMakeScale(1.5f, 1.5f); 
    splashViewController_.view.alpha = 0.0f;
    nextView.alpha = 1.0f;
    
	[UIView commitAnimations];	
    
    [activityIndicator_ release];
    activityIndicator_ = [[NXTActivityIndicator NXTActivityIndicator] retain];
    activityIndicator_.showBackgroundLayer = YES;
    activityIndicator_.frame = CGRectMake(window_.frame.size.width / 2 - [NXTActivityIndicator width] / 2, 
                                          window_.frame.size.height / 2 - [NXTActivityIndicator height] / 2, 
                                          [NXTActivityIndicator width], [NXTActivityIndicator height]);
    [window_ addSubview:activityIndicator_];
    
    if (legalContitionsAccepted_) {
        
        [self initializeConnectedTabBarButtons];
        
    }
    
}



/**
 * Tells the delegate when the application receives a memory warning from the system.
 * Images stored in the image cache are released
 *
 * @param application The delegating application object
 */
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    
    [[ImagesCache getInstance] removeCachedImages];
    
}

/**
 * Tells the delegate when the application is about to terminate
 *
 * @param application The delegating application object
 */
- (void)applicationWillTerminate:(UIApplication *)application {
    
    [radioStreamManager_ stopFinalRadioStream];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationMATConfigurationReceived object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationDateLegalTermsReceived object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationCloseEnds object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationLogoutEnds object:nil];
  
}

#pragma mark -
#pragma mark Animations delegate

/*
 * The animation has ended
 */
- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {   
    
    if ([animationID isEqualToString:kSplashScreenAnimation]) {
        
        [splashViewController_.view removeFromSuperview];
        [splashViewController_ release];
        splashViewController_ = nil;
        
    } else if ([animationID isEqualToString:kLegalTermsAnimation]) {
        
        initialLegalConditionsNavigationController_.view.transform = CGAffineTransformMakeScale(1.0f, 1.0f); 
    }
}

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:WINDOWS_TAPPED_NOTIFICATION
                                                  object:window_];
    
    [window_ release];
    window_ = nil;
    
    [disconectedNavigationController_ release];
    disconectedNavigationController_ = nil;
    
    [connectedTabBarViewController_ release];
    connectedTabBarViewController_ = nil;
    
    [splashViewController_ release];
    splashViewController_ = nil;
    
    [currentViewController_ release];
    currentViewController_ = nil;
    
    [globalPositionViewController_ release];
    globalPositionViewController_ = nil;
    
    [transfersListViewController_ release];
    transfersListViewController_ = nil;
    
    [paymentsListViewController_ release];
    paymentsListViewController_ = nil;
    
    [freOperationsListViewController_ release];
    freOperationsListViewController_ = nil;
    
    [poiMapViewController_ release];
    poiMapViewController_ = nil;
    
    [initialLegalConditionsViewController_ release];
    initialLegalConditionsViewController_ = nil;
    
    [initialLegalConditionsNavigationController_ release];
    initialLegalConditionsNavigationController_ = nil;
    
    [activityIndicator_ release];
    activityIndicator_ = nil;
    
    [legalTermsViewController_ release];
    legalTermsViewController_ = nil;
    
    [self releaseTabBarsInformation];
    
    [defaultOrderTabsArray_ release];
    defaultOrderTabsArray_ = nil;
    
    [currentOrderTabsArray_ release];
    currentOrderTabsArray_ = nil;
    
    [tabsDictionary_ release];
    tabsDictionary_ = nil;
    
    [lastUsernameLogged_ release];
    lastUsernameLogged_ = nil;
	
	[lastErrorMessage_ release];
	lastErrorMessage_ = nil;
    
    [poiMapNetworksFilter_ release];
    poiMapNetworksFilter_ = nil;
    
    [modelObjectToNavigate_ release];
    modelObjectToNavigate_ = nil;
    
    [dateLegalTerms_ release];
    dateLegalTerms_ = nil;
    
    [faqURL_ release];
    faqURL_ = nil;
    
    [tabsIdentifiersOrderArray_ release];
    tabsIdentifiersOrderArray_ = nil;
    
    [phoneCallManager_ release];
    phoneCallManager_ = nil;
    
    [radioStreamManager_ release];
    radioStreamManager_ = nil;
    
    [super dealloc];
    
}

/*
 * Releases the tab bars information instances. First action performed is ask for the tab view controller for every tab so the original navigation controller is loaded with the view controller
 */
- (void)releaseTabBarsInformation {
    
    [globalPositionTabInformation_ release];
    globalPositionTabInformation_ = nil;
    
    [transfersListTabInformation_ release];
    transfersListTabInformation_ = nil;
    
    [paymentsListTabInformation_ release];
    paymentsListTabInformation_ = nil;
    
    [FreqOperationsListTabInformation_ release];
    FreqOperationsListTabInformation_ = nil;
    
    [poiMapTabInformation_ release];
    poiMapTabInformation_ = nil;
    
    [defaultOrderTabsArray_ removeAllObjects];
    [currentOrderTabsArray_ removeAllObjects];
    [tabsDictionary_ removeAllObjects];
    
}

#pragma mark -
#pragma mark Initialization

/*
 * Initializes the images stretching. Provides the ImagesCache singleton with the stretching information to every image that needs it
 */
- (void)initializeImagesStretching {
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    [imagesCache setStretchableLeftCapWidth:6 andTopCapHeight:0 forImageName:BLUE_BUTTON_BACKGROUND_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:6 andTopCapHeight:0 forImageName:GRAY_BUTTON_BACKGROUND_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:6 andTopCapHeight:0 forImageName:GRAY_PLAIN_BUTTON_BACKGROUND_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:7 andTopCapHeight:6 forImageName:BEVEL_BACKGROUND_1_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:8 andTopCapHeight:8 forImageName:BEVEL_BACKGROUND_2_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:2 andTopCapHeight:0 forImageName:AD_BLUE_BACKGROUND_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:1 andTopCapHeight:0 forImageName:HORIZONTAL_DIVISION_LINE_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:6 andTopCapHeight:6 forImageName:BLUE_BALLOON_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:20 andTopCapHeight:20 forImageName:ALERT_BOX_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:10 andTopCapHeight:0 forImageName:SELECTED_STOCK_GRAPHIC_TYPE_BUTTON_BACKGROUND_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:1 andTopCapHeight:1 forImageName:HEADER_BACKGROUND_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:14.0f andTopCapHeight:14.0f forImageName:TEXT_FIELD_BACKGROUND_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:10 andTopCapHeight:10 forImageName:WHITE_BUTTON_BACKGROUND_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:10 andTopCapHeight:0 forImageName:BOTTOM_BUTTON_BAR_BACKGROUND_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:10.0f andTopCapHeight:10.0f forImageName:ROUTE_MODAL_VIEW_CONTROLLER_BACKGROUND_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:10.0f andTopCapHeight:10.0f forImageName:ROUTE_MODAL_VIEW_CONTROLLER_BUTTON_BACKGROUND_IMAGE_FILE_NAME];
    [imagesCache setStretchableLeftCapWidth:10.0f andTopCapHeight:0.0f forImageName:TRANSFER_DONE_IMAGE_FILE_NAME];
//    [imagesCache setStretchableLeftCapWidth:1 andTopCapHeight:0 forImageName:BLUE_ICON_ALERT_IMAGE_FILE_NAME];

}

/*
 * Initializes the connected tab bar buttons with the needed view controllers
 */
- (void)initializeConnectedTabBarButtons {
    
    [self releaseTabBarsInformation];
    
    ImagesCache *imagesCache = [ImagesCache getInstance];
    
    UINavigationController *navigationController = nil;
    
    if (globalPositionViewController_ == nil) {
        
        globalPositionViewController_ = [[GlobalPositionViewController globalPositionViewController] retain];
        navigationController = [[[UINavigationController alloc] initWithRootViewController:globalPositionViewController_] autorelease];
        
    } else {
        
        navigationController = [globalPositionViewController_ navigationController];
        
    }
    //accounts
    UIImage *unselectedImage = [imagesCache imageNamed:GLOBAL_POSITION_TAB_BAR_UNSELECTED_BUTTON_ICON_FILE_NAME];
    NSString *tabText = NSLocalizedString(GLOBAL_POSITION_TAB_BAR_BUTTON_TEXT_KEY, nil);
    globalPositionTabInformation_ = [[BBVATabBarTabInformation alloc] initWithTabIdentifier:kGlobalPositionTabIdentifier
                                                                             viewController:navigationController
                                                                               actionTarget:nil
                                                                             actionSelector:nil
                                                                         referenceImageIcon:unselectedImage
                                                                          selectedImageIcon:nil
                                                                         selectedImageColor:nil
                                                                        unselectedImageIcon:unselectedImage
                                                                       unselectedImageColor:nil
                                                                              moreImageIcon:nil
                                                                             moreImageColor:nil
                                                                          disabledImageIcon:nil
                                                                         disabledImageColor:nil
                                                                                    tabText:tabText
                                                                           moreCellMainText:nil
                                                                      moreCellSecondaryText:nil
                                                                           canShowOnMoreTab:YES
                                                                            canShowOnTabBar:YES];
    
    if (globalPositionTabInformation_ != nil) {
        
        [tabsDictionary_ setObject:globalPositionTabInformation_
                            forKey:kGlobalPositionTabIdentifier];
        [defaultOrderTabsArray_ addObject:globalPositionTabInformation_];
        
    }
    
    //Transfer
    if (transfersListViewController_ == nil) {
        
        transfersListViewController_ = [[TransfersListViewController transfersListViewController] retain];
        navigationController = [[[UINavigationController alloc] initWithRootViewController:transfersListViewController_] autorelease];
        
    } else {
        
        navigationController = [transfersListViewController_ navigationController];
        
    }
    unselectedImage = [imagesCache imageNamed:TRANSFER_POSITION_TAB_BAR_UNSELECTED_BUTTON_ICON_FILE_NAME];
    tabText = NSLocalizedString(TRANSFER_TITLE_TEXT_KEY, nil);
    transfersListTabInformation_ = [[BBVATabBarTabInformation alloc] initWithTabIdentifier:kTransferPositionTabIdentifier
                                                                             viewController:navigationController
                                                                               actionTarget:nil
                                                                             actionSelector:nil
                                                                         referenceImageIcon:unselectedImage
                                                                          selectedImageIcon:nil
                                                                         selectedImageColor:nil
                                                                        unselectedImageIcon:unselectedImage
                                                                       unselectedImageColor:nil
                                                                              moreImageIcon:nil
                                                                             moreImageColor:nil
                                                                          disabledImageIcon:nil
                                                                         disabledImageColor:nil
                                                                                    tabText:tabText
                                                                           moreCellMainText:nil
                                                                      moreCellSecondaryText:nil
                                                                           canShowOnMoreTab:YES
                                                                            canShowOnTabBar:YES];
    
    if (transfersListTabInformation_ != nil) {
        
        [tabsDictionary_ setObject:transfersListTabInformation_
                            forKey:kTransferPositionTabIdentifier];
        [defaultOrderTabsArray_ addObject:transfersListTabInformation_];
        
    }
    
    //Payments
    if (paymentsListViewController_ == nil) {
        
        paymentsListViewController_ = [[PaymentsListViewController paymentsListViewController] retain];
        navigationController = [[[UINavigationController alloc] initWithRootViewController:paymentsListViewController_] autorelease];
        
    } else {
        
        navigationController = [paymentsListViewController_ navigationController];
        
    }
    
    unselectedImage = [imagesCache imageNamed:TAB_PAYMENT_UNSELETED_FILE_NAME];
    tabText = NSLocalizedString(PAYMENT_TITLE_TEXT_KEY, nil);
    paymentsListTabInformation_ = [[BBVATabBarTabInformation alloc] initWithTabIdentifier:kPaymentPositionTabIdentifier
                                                                           viewController:navigationController
                                                                             actionTarget:nil
                                                                           actionSelector:nil
                                                                       referenceImageIcon:unselectedImage
                                                                        selectedImageIcon:nil
                                                                       selectedImageColor:nil
                                                                      unselectedImageIcon:unselectedImage
                                                                     unselectedImageColor:nil
                                                                            moreImageIcon:nil
                                                                           moreImageColor:nil
                                                                        disabledImageIcon:nil
                                                                       disabledImageColor:nil
                                                                                  tabText:tabText
                                                                         moreCellMainText:nil
                                                                    moreCellSecondaryText:nil
                                                                         canShowOnMoreTab:YES
                                                                          canShowOnTabBar:YES];
    
    if (paymentsListTabInformation_ != nil) {
        
        [tabsDictionary_ setObject:paymentsListTabInformation_
                            forKey:kPaymentPositionTabIdentifier];
        [defaultOrderTabsArray_ addObject:paymentsListTabInformation_];
        
    }
    
    //Frequent Operation
    if (freOperationsListViewController_ == nil) {
        
        freOperationsListViewController_ = [[FrequentOperationListViewController frequentOperationListViewController] retain];
        navigationController = [[[UINavigationController alloc] initWithRootViewController:freOperationsListViewController_] autorelease];
        
    } else {
        
        navigationController = [freOperationsListViewController_ navigationController];
        
    }
    [freOperationsListViewController_ setHasForward:FALSE];
    
    unselectedImage = [imagesCache imageNamed:FREQUENTS_POSITION_TAB_BAR_UNSELECTED_BUTTON_ICON_FILE_NAME];
    tabText = NSLocalizedString(FO_TITLE_TEXT_KEY, nil);
    FreqOperationsListTabInformation_ = [[BBVATabBarTabInformation alloc] initWithTabIdentifier:kFOPositionTabIdentifier
                                                                            viewController:navigationController
                                                                              actionTarget:nil
                                                                            actionSelector:nil
                                                                        referenceImageIcon:unselectedImage
                                                                         selectedImageIcon:nil
                                                                        selectedImageColor:nil
                                                                       unselectedImageIcon:unselectedImage
                                                                      unselectedImageColor:nil
                                                                             moreImageIcon:nil
                                                                            moreImageColor:nil
                                                                         disabledImageIcon:nil
                                                                        disabledImageColor:nil
                                                                                   tabText:tabText
                                                                          moreCellMainText:nil
                                                                     moreCellSecondaryText:nil
                                                                          canShowOnMoreTab:YES
                                                                           canShowOnTabBar:YES];
    
    if (FreqOperationsListTabInformation_ != nil) {
        
        [tabsDictionary_ setObject:FreqOperationsListTabInformation_
                            forKey:kFOPositionTabIdentifier];
        [defaultOrderTabsArray_ addObject:FreqOperationsListTabInformation_];
        
    }

    //Localizable
    if (poiMapViewController_ == nil) {
        
        poiMapViewController_ = [[POIMapViewController poiMapViewController] retain];
        navigationController = [[[UINavigationController alloc] initWithRootViewController:poiMapViewController_] autorelease];
        
    } else {
        
        navigationController = [poiMapViewController_ navigationController];
        
    }
    
    
    
    
    unselectedImage = [imagesCache imageNamed:LOCALIZABLE_POSITION_TAB_BAR_UNSELECTED_BUTTON_ICON_FILE_NAME];
    tabText = NSLocalizedString(POI_MAP_POSITION_TAB_BAR_BUTTON_TEXT_KEY, nil);
    poiMapTabInformation_ = [[BBVATabBarTabInformation alloc] initWithTabIdentifier:kPoiMapPositionTabIdentifier
                                                                     viewController:navigationController
                                                                       actionTarget:nil
                                                                     actionSelector:nil
                                                                 referenceImageIcon:unselectedImage
                                                                  selectedImageIcon:nil
                                                                 selectedImageColor:nil
                                                                unselectedImageIcon:unselectedImage
                                                               unselectedImageColor:nil
                                                                      moreImageIcon:nil
                                                                     moreImageColor:nil
                                                                  disabledImageIcon:nil
                                                                 disabledImageColor:nil
                                                                            tabText:tabText
                                                                   moreCellMainText:nil
                                                              moreCellSecondaryText:nil
                                                                   canShowOnMoreTab:YES
                                                                    canShowOnTabBar:YES];
    
    if (poiMapTabInformation_ != nil) {
        
        [tabsDictionary_ setObject:poiMapTabInformation_
                            forKey:kPoiMapPositionTabIdentifier];
        [defaultOrderTabsArray_ addObject:poiMapTabInformation_];
        
    }
    
    [self populateCurrentTabsOrderArray];
    [connectedTabBarViewController_ setDefaultTabsOrderArray:defaultOrderTabsArray_
                                       currentTabsOrderArray:currentOrderTabsArray_];
    
}

/*
 * Reload and check that tab item is visible
 */
- (void)reloadVisibleTabBarItems {
    
    NSMutableArray *tabsArray = [NSMutableArray array];
    
    [tabsArray addObject:globalPositionTabInformation_];
    
    [connectedTabBarViewController_ setDefaultTabsOrderArray:defaultOrderTabsArray_
                                       currentTabsOrderArray:tabsArray];
    
}


#pragma mark -
#pragma mark User interaction

/*
 * Shows activity indicator and disables user interaction
 */
- (void)showActivityIndicator:(PlaceOfActivityIndicator)place {
    
    [self showActivityIndicator:place withMessage:nil];
    
}

/*
 * Shows activity indicator and disables user interaction
 */
- (void)showActivityIndicator:(PlaceOfActivityIndicator)place withMessage:(NSString *)message {
    
    if (place == poai_StatusBar || place == poai_Both) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    }
    
    if (place == poai_Screen || place == poai_Both) {
        
        [window_ setUserInteractionEnabled:NO];
        //        [activityIndicator_ showIndicatorWithMessage:message];
        [activityIndicator_ showIndicatorWithMessage:nil];
        //        [window_ addSubview:activityIndicator_];
        
    }
}

/*
 * Hides activity indicator and enables user interaction
 */
- (void)hideActivityIndicator {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [activityIndicator_ hideIndicator];
    //    [activityIndicator_ performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.25];
    [window_ setUserInteractionEnabled:YES];
    
}

#pragma mark -
#pragma mark Tabs management

/*
 * Selects the global position tab when the connected tab bar is being displayed
 */
- (BOOL)displayGlobalPositionTab {
    
    BOOL result = NO;
    
    if (currentViewController_ == connectedTabBarViewController_) {
        
        result = [connectedTabBarViewController_ selectViewController:globalPositionTabInformation_.viewController];
        
    }
    
    return result;
    
}

/*
 * Starts editing the tabs order
 */
- (void)editTabsOrder {
    
    if (currentViewController_ == connectedTabBarViewController_) {
        
        [connectedTabBarViewController_ relocateTabs];
        
    }
    
}

/*
 * Set tabBar visibility if currentViewController is BBVATabBarViewController
 */
- (BOOL)setTabBarVisibility:(BOOL)visible
                   animated:(BOOL)animated {
    
    BOOL result = NO;
    
    if (currentViewController_ == connectedTabBarViewController_) {
        
        [connectedTabBarViewController_ setTabBarVisibility:visible animated:animated];
        
        result = YES;
        
    }
    
    return result;
    
}

/*
 * Populates the current tabs order array with the preferences informatio
 */
- (void)populateCurrentTabsOrderArray {
    
    [self loadUserPreferences];
    
    [currentOrderTabsArray_ removeAllObjects];
    
    if (tabsIdentifiersOrderArray_ == nil) {
        
        [currentOrderTabsArray_ addObjectsFromArray:defaultOrderTabsArray_];
        
    } else {
        
        BBVATabBarTabInformation *tabInformation;
        
        for (NSString *tabIdentifier in tabsIdentifiersOrderArray_) {
            
            tabInformation = [tabsDictionary_ objectForKey:tabIdentifier];
            
            if (tabInformation != nil) {
                
                [currentOrderTabsArray_ addObject:tabInformation];
                
            }
            
        }
        
    }
    
}

/*
 * Present a modal view controller with all views in application.
 */
- (void)presentModalViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    [currentViewController_ presentModalViewController:viewController animated:animated];
    
}

/*
 * Dismiss modal view controller.
 */
- (void)dismissModalViewControllerAnimated:(BOOL)animated {
    
    [currentViewController_ dismissModalViewControllerAnimated:animated];
    
}

#pragma mark -
#pragma mark Login methods

/*
 * Turns from the disconnected views to the connected views once the login is correct
 */
- (void)loginPerformed {
    
	[Session getInstance].isLogged = YES;
	
    trackWindowTap_ = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(windowTappedNotification:) name:WINDOWS_TAPPED_NOTIFICATION object:window_];
    [self resetWindowTappedTimeEventWithDelay:AUTOMATIC_LOGOUT_TIMEOUT];
	
	[globalPositionViewController_ setShouldBeCleaned:YES];
    
    UIViewController *globalPositionViewController = globalPositionTabInformation_.viewController;
    [connectedTabBarViewController_ selectViewController:globalPositionViewController];
    
    if ([globalPositionViewController isKindOfClass:[UINavigationController class]]) {
        
        UINavigationController *globalPositionNavigationController = (UINavigationController *)globalPositionViewController;
        
        [globalPositionNavigationController popToRootViewControllerAnimated:NO];
        
    }
    
    [connectedTabBarViewController_ setTabBarVisibility:YES
                                               animated:NO];
    
    UIView *previousView = disconectedNavigationController_.view;
    UIView *nextView = connectedTabBarViewController_.view;
    //    nextView.frame = [[UIScreen mainScreen] applicationFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:window_ cache:NO];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:1.0];
    
    [window_ addSubview:nextView];
	currentViewController_ = connectedTabBarViewController_;
    
    [UIView commitAnimations];
    
    [window_ addSubview:activityIndicator_];
    [previousView removeFromSuperview];
    
}

/**
 * Disconnects the user and returns to Home view
 */
- (void)disconnect {  
    UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(WARNING_MESSAGE_TITLE_KEY, nil)
                                                         message:NSLocalizedString(EXIT_DIALOG_MESSAGE_TEXT_KEY, nil)
                                                        delegate:self 
                                               cancelButtonTitle:NSLocalizedString(BBVA_LINE_DIALOG_CANCEL_BUTTON_TITLE_KEY, nil)
                                               otherButtonTitles:NSLocalizedString(BBVA_LINE_DIALOG_OK_BUTTON_TITLE_KEY, nil), nil] autorelease];
    alertView.tag = EXIT_DIALOG_TAG;
    [alertView show];
}

/*
 * Force the close and logout of the server session
 */
- (void)closeSessionAndLogout {
    
	[Session getInstance].isLogged = NO;
    
    gpChartAdviceShown_ = NO;
    stockAccountChartAdviceShown_ = NO;
    stockBuySellAdviceShown_ = NO;
    
    
    [[Updater getInstance] logout];
	[[Session getInstance] removeData];
    [[Updater getInstance] close];
    
}

/*
 * Inform user of a session expired
 */
- (void)sessionExpired {
    
	if ([Session getInstance].isLogged) {
        
		[Session getInstance].isLogged = NO;
        
        [MCBFacade invokeLogInvokedAppMethodWithAppName:MCB_APPLICATION_NAME
                                                 appKey:MCB_APPLICATION_KEY
                                             appCountry:APP_COUNTRY
                                             appVersion:APP_VERSION_STRING
                                               latitude:0.0f
                                              longitude:0.0f
                                          validLocation:NO
                                              appMethod:MAT_LOG_SESSION_EXPIRED];

		if (lastErrorMessage_ != nil) {
			
			[currentViewController_ dismissModalViewControllerAnimated:NO];
			
			[lastErrorMessage_ release];
			lastErrorMessage_ = nil;
			
		}
		
		gpChartAdviceShown_ = NO;
		stockAccountChartAdviceShown_ = NO;
		stockBuySellAdviceShown_ = NO;
		trackWindowTap_ = NO;
		
		[self hideActivityIndicator];
		
		
		

		[[Session getInstance] removeData];
		
		[self showActivityIndicator:poai_Both];
		
		//	// Server will fail no next login so the proposal from BBVA is to perform a fake logon on session expiration
		//	[[Updater getInstance] logon];
		
		[self closeSessionAndLogout];
        
        UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(WARNING_MESSAGE_TITLE_KEY, nil)
															message:NSLocalizedString(SESSION_EXPIRED_ERROR_KEY, nil)
														   delegate:self
												  cancelButtonTitle:NSLocalizedString(OK_TEXT_KEY, nil)
												  otherButtonTitles:nil] autorelease];
		[alertView setTag:SESSION_EXPIRED_TAG];
		[alertView show];
        
        
        //[[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLogoutEnds object:nil];
        
	}
	
}

/*
 * Perform close animations
 */
- (void)doCloseAnimations {
    
    [connectedTabBarViewController_ stopRelocateTabs];
    
    UIApplication *application = [UIApplication sharedApplication];
    application.statusBarHidden = NO;
    application.statusBarOrientation = UIInterfaceOrientationPortrait;
    
    CGRect frame = [[UIScreen mainScreen] applicationFrame];
    connectedTabBarViewController_.view.frame = frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:window_ cache:NO];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(closeAnimationEnded)];
    [UIView setAnimationDuration:1.0];
    
    [window_ addSubview:disconectedNavigationController_.view];
	currentViewController_ = disconectedNavigationController_;
	
    [UIView commitAnimations];
    
    [window_ addSubview:activityIndicator_];
    [connectedTabBarViewController_.view removeFromSuperview];
    
}

/*
 * The close animations has been finished
 */
- (void)closeAnimationEnded {
    
    // This only happens if we are returning from background and session expired. Then, we wait until
    // animations ends and then request legal terms.
    
    if (obtainingLegalTermsFromBackground_) {
        
        [self showActivityIndicator:poai_Both];

    }
    
}

#pragma mark -
#pragma mark Dialogs

/*
 * Show dialog with bbva line.
 */
- (void)showCallBBVALineDialog {
    
    [self showCallDialogWithPhone:BBVA_LINE_PHONE];
    
}

/*
 * Shows the confirmation dialog to call a telephone
 */
- (void)showCallDialogWithPhone:(NSString *)aPhone {
    
    NSString *messageError = [NSString stringWithFormat:NSLocalizedString(BBVA_LINE_DIALOG_TEXT_KEY, nil), BBVA_LINE_PHONE];
    
    [self showCallDialogWithPhone:aPhone withTitle:NSLocalizedString(BBVA_LINE_DIALOG_TITLE_KEY,nil) andMessage:NSLocalizedString(BBVA_LINE_DIALOG_MESSAGE_KEY, nil) andMessageError:messageError];
    
}

/*
 * Shows the confirmation dialog to call a telephone with title and message.
 */
- (void)showCallDialogWithPhone:(NSString *)aPhone withTitle:(NSString *)title andMessage:(NSString *)message andMessageError:(NSString *)messageError {
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel:"]]) {
        
        if (phoneCallManager_ == nil) {
            
            
            phoneCallManager_ = [[PhoneCallManager alloc] initWithPhoneNumber:aPhone];
            
        } else {
            
            phoneCallManager_.phoneNumber = aPhone;
            
        }
        
        [phoneCallManager_ showWithTitle:title andMessage:message];
        
    } else {
        
        [Tools showAlertWithMessage:messageError title:title];
        
    }
    
}

/*
 * Shows an error alert view
 */
- (void)showErrorWithMessage:(NSString *)anErrorMessage {
    
    if ([anErrorMessage length] > 0) {
		
		[lastErrorMessage_ release];
		lastErrorMessage_ = [anErrorMessage copy];
        
        UIAlertView *dialog = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(INFO_MESSAGE_TITLE_KEY,nil)
                                                          message:anErrorMessage
                                                         delegate:self 
                                                cancelButtonTitle:NSLocalizedString(OK_TEXT_KEY, nil)
                                                otherButtonTitles:nil] autorelease];
        
        [dialog show];
		
	}
    
}


#pragma mark -
#pragma mark UIAlertViewDelegate methods

/**
 * Invoked when the user discards an alert view by clicking a button. The only alert view in the application is the customer center call confirmation, so when the user
 * accepts the call, it is performed
 *
 * @param alertView The alert view triggering the event
 * @param buttonIndex The button index the user clicked
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == EXIT_DIALOG_TAG) {
        if (buttonIndex == 1) {
            obtainingLegalTermsFromBackground_ = NO;
            [self showActivityIndicator:poai_Both];
            [self closeSessionAndLogout];
        }
        
    }
    
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate selector

/**
 * Tells the delegate that the user wants to dismiss the mail composition view.
 *
 * @param controller The view controller object managing the mail composition view.
 * @param result The result of the user’s action.
 * @param error If an error occurred, this parameter contains an error object with information about the type of failure.
 */
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    [currentViewController_ dismissModalViewControllerAnimated:YES];
    
	[lastErrorMessage_ release];
	lastErrorMessage_ = nil;
    
}

#pragma mark -
#pragma mark User preferences

/*
 * Load the user preferences
 */
- (void)loadUserPreferences {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    smsConfigurationMessageShown_ = [userDefaults boolForKey:@"showSMSConfigurationMessage"];
    accountGraphicDisplayed_ = [userDefaults boolForKey:@"accountGraphicDisplayed"];
    stockGraphicDisplayed_ = [userDefaults boolForKey:@"stockGraphicDisplayed"];
	firstRun_ = ![userDefaults boolForKey:@"notFirstRun"];
    
    [dateLegalTerms_ release];
    dateLegalTerms_ = [[NSDate dateWithTimeIntervalSince1970:[userDefaults doubleForKey:@"dateLegalTerms"]] retain];
    
	if (firstRun_) {
		[userDefaults setBool:YES forKey:@"notFirstRun"];
	}
    
    [faqURL_ release];
    faqURL_ = nil;
    faqURL_ = [[userDefaults stringForKey:kRemoteFaqURLPreferencesKey] copy];
    
    //TODO: Uncomment to allow to show legal terms on startup
//    legalContitionsAccepted_ = [userDefaults boolForKey:@"legalConditionsAccepted"];
    legalContitionsAccepted_ = YES;

    //TODO: Uncomment to use last user name logged
//	[lastUsernameLogged_ release];
//    lastUsernameLogged_ = [[Tools decipherString:[userDefaults stringForKey:@"lastUsernameLogged"]] copy];

    hideGPChartAdvice_ = [userDefaults boolForKey:@"hideGPChartAdvice"];
    hideStockBuySellAdvice_ = [userDefaults boolForKey:@"hideStockBuySellAdvice"];
    hideStockAccountChartAdvice_ = [userDefaults boolForKey:@"hideStockAccountChartAdvice"];
    
    NSArray *poiMapNetworksFilter = [userDefaults objectForKey:@"poiMapNetworksFilter"];
    
    if (poiMapNetworksFilter_ == nil) {
        
        poiMapNetworksFilter_ = [[NSMutableArray alloc] init];
        
    } else {
        
        [poiMapNetworksFilter_ removeAllObjects];
        
    }
    
    if (poiMapNetworksFilter == nil) {
        
        [poiMapNetworksFilter_ addObject:BRANCH_TYPE];
        [poiMapNetworksFilter_ addObject:ATM_TYPE];
        
    } else {
        
        [poiMapNetworksFilter_ addObjectsFromArray:poiMapNetworksFilter];
        
    }
    
    if (![poiMapNetworksFilter_ containsObject:BRANCH_TYPE]) {
        
        [poiMapNetworksFilter_ addObject:BRANCH_TYPE];
        
    }
    
    if (![poiMapNetworksFilter_ containsObject:ATM_TYPE]) {
        
        [poiMapNetworksFilter_ addObject:ATM_TYPE];
        
    }
    
    [tabsIdentifiersOrderArray_ release];
    tabsIdentifiersOrderArray_ = [[userDefaults objectForKey:kTabIdentifiersOrderArrayUserPreferencesKey] retain];
}

/*
 * Save user preferences
 */
- (void)saveUserPreferences {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:smsConfigurationMessageShown_ forKey:@"showSMSConfigurationMessage"];
    [prefs setBool:accountGraphicDisplayed_ forKey:@"accountGraphicDisplayed"];
    [prefs setBool:stockGraphicDisplayed_ forKey:@"stockGraphicDisplayed"];
    [prefs setBool:legalContitionsAccepted_ forKey:@"legalConditionsAccepted"];
    [prefs setBool:hideGPChartAdvice_ forKey:@"hideGPChartAdvice"];
    [prefs setBool:hideStockAccountChartAdvice_ forKey:@"hideStockAccountChartAdvice"];
    [prefs setBool:hideStockBuySellAdvice_ forKey:@"hideStockBuySellAdvice"];
    [prefs setObject:poiMapNetworksFilter_ forKey:@"poiMapNetworksFilter"];
    
    //TODO:Uncomment to use last user name logged
//	if (lastUsernameLogged_ != nil) {
//		[prefs setObject:[Tools cipherString:lastUsernameLogged_] forKey:@"lastUsernameLogged"];
//	}
    
    [prefs setObject:tabsIdentifiersOrderArray_
              forKey:kTabIdentifiersOrderArrayUserPreferencesKey];
    
    if (![prefs synchronize]) {
        //DLog(@"Error guardando preferencias del sistema");
    }
}

#pragma mark -
#pragma mark Notifications

/*
 * Close response received
 */
- (void)closeResponseReceived:(NSNotification *)notification {
    
    trackWindowTap_ = NO;
    [self resetWindowTappedTimeEventWithDelay:AUTOMATIC_LOGOUT_TIMEOUT];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:WINDOWS_TAPPED_NOTIFICATION
                                                  object:window_];
    
    [[Updater getInstance] logout];
}

/*
 * Logout response received
 */
- (void)logoutResponseReceived:(NSNotification *)notification {
    
    [self hideActivityIndicator];

	if (currentViewController_ != disconectedNavigationController_) {
        
		[self doCloseAnimations];
        
	}
    
}

/*
 * Legal conditions has been accepted by the user. The initial view is displayed, and the associated flag is updated
 */
- (void)legalConditionsAccepted {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BROWSER_VIEW_CONTROLLER_ACCEPT_BUTTON_TAPPED object:initialLegalConditionsViewController_];
    legalContitionsAccepted_ = YES;
    [self saveUserPreferences];
    
    UIView *previousView = initialLegalConditionsNavigationController_.view;
    UIView *nextView = disconectedNavigationController_.view;
    
    [window_ addSubview:nextView];
	currentViewController_ = disconectedNavigationController_;
    nextView.alpha = 0.0f;
    
	[UIView beginAnimations:kLegalTermsAnimation context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.5];
    
    previousView.transform = CGAffineTransformMakeScale(1.5f, 1.5f); 
    previousView.alpha = 0.0f;
    nextView.alpha = 1.0f;
    
	[UIView commitAnimations];	
    
    [activityIndicator_ release];
    activityIndicator_ = [[NXTActivityIndicator NXTActivityIndicator] retain];
    activityIndicator_.showBackgroundLayer = YES;
    activityIndicator_.frame = CGRectMake(window_.frame.size.width / 2 - [NXTActivityIndicator width] / 2, 
                                          window_.frame.size.height / 2 - [NXTActivityIndicator height] / 2, 
                                          [NXTActivityIndicator width], [NXTActivityIndicator height]);
    [window_ addSubview:activityIndicator_];
    
    [self initializeConnectedTabBarButtons];
    
}

/*
 * Window notifies it has been tapped. The automatic log-out event is reseted
 */
- (void)windowTappedNotification:(NSNotification *)aNotification {
    
    nextExpirationTime_ = [NSDate timeIntervalSinceReferenceDate] + AUTOMATIC_LOGOUT_TIMEOUT;
    
    [self resetWindowTappedTimeEventWithDelay:AUTOMATIC_LOGOUT_TIMEOUT];
    
}

/*
 * Resets the window tapped time event
 */
- (void)resetWindowTappedTimeEventWithDelay:(NSTimeInterval)aDelay {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(sessionExpired)
                                               object:nil];
    
    if (trackWindowTap_) {
        
        [self performSelector:@selector(sessionExpired)
                   withObject:nil
                   afterDelay:AUTOMATIC_LOGOUT_TIMEOUT];
        
    }
    
}

/*
 * Checks the session expiration
 */
- (void)checkSessionExpiration {
    
    if (trackWindowTap_) {
        
        NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
        
        if (currentTime > nextExpirationTime_) {
            
            [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                     selector:@selector(sessionExpired)
                                                       object:nil];
            
            [self sessionExpired];
            
        } else {
            
            [self resetWindowTappedTimeEventWithDelay:(nextExpirationTime_ - currentTime)];
            
        }
        
    }
    
}

/*
 * The MAT configuration reponse has been received
 */
- (void)MATConfigurationResponseReceived:(NSNotification *)notification {
    MATConfigurationResponse *response = [notification object];
    
    if (![response isError]) {
        
        MCBConfiguration *configuration = [[MCBConfiguration alloc] init];
        configuration.methods = response.methodsList;
        
        [configuration release];
    }
    
}

/*
 * The date legal terms reponse has been received
 */
- (void)dateLegalTermsResponseReceived:(NSNotification *)notification {
    
    DateLegalTermsResponse *response = [notification object];
    
    BOOL showModal = NO;
    
    if (![response isError]) {
        
        NSTimeInterval currentLegalInterval = [[self dateLegalTerms] timeIntervalSince1970];
        
        NSTimeInterval receivedLegelInterval = [response.dateLegalTerms doubleValue];
        
        if (receivedLegelInterval > currentLegalInterval){
            
            showModal = YES;
            
            [self setLegalContitionsAccepted:NO];
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            
            [userDefaults setValue:response.dateLegalTerms forKey:@"dateLegalTerms"];
            
            if (![userDefaults synchronize]) {
                //DLog(@"Error guardando preferencias del sistema");
            }
        }
        
        NSString *remoteFAQURL = response.faqURL;
        
        if (remoteFAQURL != faqURL_) {
            
            [faqURL_ release];
            faqURL_ = nil;
            faqURL_ = [remoteFAQURL copy];
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:faqURL_
                            forKey:kRemoteFaqURLPreferencesKey];
            [userDefaults synchronize];
            
        }
        
    }
    
    if (obtainingLegalTermsFromBackground_) {
        
        obtainingLegalTermsFromBackground_ = NO;
        
        [self hideActivityIndicator];
        
        if (showModal) {
            [self splashViewControllerFinishedPresentation:nil];
        }
        
    } else {
        
        [self splashViewControllerFinishedPresentation:nil];
    }
}

#pragma mark -
#pragma mark BBVATabBarViewControllerDelegate protocol selectors

/**
 * Asks the delegate confirmation to select or not a given view controller. All view controllers are allowed to be selected
 *
 * @param aBBVATabBarViewController The BBVA tab bar view controller asking for confirmation
 * @param aViewController The view controller that is going to be selected
 * @return YES if the view controller can be selected, NO otherwise
 */
- (BOOL)tabBarViewController:(BBVATabBarViewController *)aBBVATabBarViewController shouldSelectViewController:(UIViewController *)aViewController {
    
    BOOL result = YES;
    
    if (aViewController == [transfersListTabInformation_ viewController]) {
        
        NSArray *accounts = [[[Session getInstance] accountList] accountList];
        
        if ([accounts count] == 0) {
            
            result = NO;
            
            [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_NO_ACCOUNT_AVAILABLE_KEY, nil)];
            
        }
        
    }
    
    return result;
    
}

/**
 * Notifies a new view controller will be selected
 *
 * @param bbvaTabBarViewController The BBVA tab bar view controller triggering the event
 * @param viewController The view controller being selected
 */
- (void)tabBarViewController:(BBVATabBarViewController *)bbvaTabBarViewController
    willSelectViewController:(UIViewController *)viewController {
    
}

/**
 * Notifies a new view controller has been selected. Session state is updated, or operations are launched, depending on the tab selected
 *
 * @param aBBVATabBarViewController The BBVA tab bar view controller triggering the event
 * @param aViewController The view controller being selected information
 */
- (void)tabBarViewController:(BBVATabBarViewController *)aBBVATabBarViewController viewControllerSelected:(UIViewController *)aViewController {
    
}

/**
 * Notifies the delegate that the tab bar view did disappear. Do nothing
 *
 * @param aBBVATabBarViewController The BBVA tab bar view controller triggering the event
 */
- (void)tabBarViewControllerTabBarDidDisappear:(BBVATabBarViewController *)aBBVATabBarViewController {
    
}

/**
 * Notifies the delegate that the tab bar view did appear. Do nothing
 *
 * @param aBBVATabBarViewController The BBVA tab bar view controller triggering the event
 */
- (void)tabBarViewControllerTabBarDidAppear:(BBVATabBarViewController *)aBBVATabBarViewController {
    
}

/**
 * Asks the delegate whether a given view controller can be relocated. Only dashboard view controller cannot be relocated
 *
 * @param bbvaTabBarViewController The BBVA tab bar view controller asking for confirmation
 * @param tabBarInformation The information object representing the BBVA tab bar to check about
 * @return YES when the view controller can be selected, NO otherwise
 */
- (BOOL)tabBarViewController:(BBVATabBarViewController *)bbvaTabBarViewController
              canRelocateTab:(BBVATabBarTabInformation *)tabBarInformation {
    
    BOOL result = NO;
    
    if (bbvaTabBarViewController == connectedTabBarViewController_) {
        
        if (tabBarInformation != globalPositionTabInformation_) {
            
            result = YES;
            
        }
        
    }
    
    return result;
    
}

/**
 * Notifies the delegate that the tab bars where reordered. Stores the current tab order
 *
 * @param bbvaTabBarViewController The BBVA tab bar view controller triggering the event
 * @param tabsList The new tabs order. All objects are BBVATabBarTabInformation instances
 * @param tabBarCount The number of tabs inside the tab bar (not including the "more" tab). This parameter has meaning in iPad version
 */
- (void)tabBarViewcontroller:(BBVATabBarViewController *)bbvaTabBarViewController
               tabsReordered:(NSArray *)tabsList
                 tabBarCount:(NSUInteger)tabBarCount {
    
    if (bbvaTabBarViewController == connectedTabBarViewController_) {
        
        [currentOrderTabsArray_ removeAllObjects];
        [currentOrderTabsArray_ addObjectsFromArray:tabsList];
        
        NSMutableArray *tabsIdentifiersOrder = [NSMutableArray array];
        NSString *tabIdentifier =  nil;
        
        for (BBVATabBarTabInformation *tabInformation in currentOrderTabsArray_) {
            
            tabIdentifier = tabInformation.tabIdentifier;
            
            if (tabIdentifier != nil) {
                
                [tabsIdentifiersOrder addObject:tabIdentifier];
                
            }
            
        }
        
        [tabsIdentifiersOrderArray_ release];
        tabsIdentifiersOrderArray_ = [[NSArray alloc] initWithArray:tabsIdentifiersOrder];
        
        [self saveUserPreferences];
        
    }
    
}

/**
 * Notifies the delegate that the logo image was tapped. Does nothing.
 *
 * @param bbvaTabBarViewController The BBVA tab bar view controller triggering the event
 */
- (void)tabBarViewControllerLogoImageTapped:(BBVATabBarViewController *)bbvaTabBarViewController {
    
}

#pragma mark -
#pragma mark Action sheet

/*
 * Shows the given UIActionSheet in the currentViewController_'s view
 */
- (void)showActionSheet:(UIActionSheet *)sheet {
    
    [sheet showInView:currentViewController_.view];
    
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Returns a flag indicating if the disconnected controller is showing
 */
- (BOOL)onDisconnectedViews {
    
    if ([window_.subviews containsObject:disconectedNavigationController_.view]) {
        
        return YES;
        
    } else {
        
        return NO;
        
    }
    
}

#pragma mark -
#pragma mark Util bd

-(void)fnInsertarBaseDatosADocumentos
{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if (![user boolForKey:@"copiarBD"]) {
        
        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"nxt_peru" ofType:@"sqlite"];
        
        NSString *nombreBaseDatosString = @"nxt_peru.sqlite";
        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDir = [documentPaths objectAtIndex:0];
        NSString *folderPath = [documentsDir stringByAppendingPathComponent:nombreBaseDatosString];
        
        //DLog(@"creado database : %@", folderPath);
        
        NSError *error = nil;
        
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath
                                                toPath:folderPath
                                                 error:&error];
        [user setBool:TRUE forKey:@"copiarBD"];
    }
}

-(void)fnDeleteCacheIfExists{
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDir = [documentPaths objectAtIndex:0];
    
    NSString *bundleIdentifierApp = [[NSBundle mainBundle] bundleIdentifier];
    cacheDir = [cacheDir stringByAppendingPathComponent:bundleIdentifierApp];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    BOOL fileExists = [fileManager fileExistsAtPath:cacheDir];
    
    if (fileExists)
    {
        BOOL success = [fileManager removeItemAtPath:cacheDir error:&error];
        if (!success) NSLog(@"Error: %@", [error localizedDescription]);
        
    }
}


//call a void 5 minutes after go to background
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    UIApplication *app = [UIApplication sharedApplication];
    
    //create new uiBackgroundTask
    __block UIBackgroundTaskIdentifier bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
    
    //and create new timer with async call:
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //run function methodRunAfterBackground
        NSTimer* t = [NSTimer scheduledTimerWithTimeInterval:AUTOMATIC_LOGOUT_TIMEOUT target:self selector:@selector(methodRunAfterBackground) userInfo:nil repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:t forMode:NSDefaultRunLoopMode];
        [[NSRunLoop currentRunLoop] run];
    });
}

-(void)methodRunAfterBackground
{
    [self checkSessionExpiration];

}
#pragma mark Image for Multitasking
-(void)applicationWillResignActive:(UIApplication *)application
{
    imageView = [[UIImageView alloc]initWithFrame:[self.window frame]];
    [imageView setImage:[UIImage imageNamed:@"Default-568h"]];
    [self.window addSubview:imageView];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSDKAppEvents activateApp];
    
    if(imageView != nil) {
        [imageView removeFromSuperview];
        imageView = nil;
    }
}

@end
