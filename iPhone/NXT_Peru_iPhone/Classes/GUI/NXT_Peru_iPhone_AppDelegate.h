/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "BBVATabBarViewController.h"
#import "SplashViewController.h"
#import "RadioStreamController.h"
#import <AVFoundation/AVFoundation.h>

//Forward declarations
@class BBVATabBarViewController;
@class BrowserViewController;
@class FrequentOperationListViewController;
@class GlobalPositionViewController;
@class NXTActivityIndicator;
@class POIMapViewController;
@class PaymentsListViewController;
@class PhoneCallManager;
@class SplashViewController;
@class TransfersListViewController;

typedef enum {
    poai_StatusBar = 0,
    poai_Screen,
    poai_Both
} PlaceOfActivityIndicator;


/**
 * Phone call alert view delegate to manage a phone call
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PhoneCallManager : NSObject <UIAlertViewDelegate> {
    
@private
    
    /**
     * Phone number to perform the call to
     */
    NSString *phoneNumber_;
    
    
}

/**
 * Provides read-only access to the atored phone number
 */
@property (nonatomic, readwrite, copy) NSString *phoneNumber;

/**
 * Designated initializer. Initializes a PhoneCallManager instance with the provided phone number
 *
 * @param aPhoneNumber The phone number to store
 * @return The initialized PhoneCallManager instance
 */
- (id)initWithPhoneNumber:(NSString *)aPhoneNumber;

/**
 * Shows the dialog
 */
- (void)show;

/**
 * Shows the dialog
 *
 * @param title: Title to show in alertview.
 * @param message: Message to show information.
 */
- (void)showWithTitle:(NSString *)title andMessage:(NSString *)message;

@end


/**
 * NXT application delegate. Manages the application behaviour and the initial navigations
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface NXT_Peru_iPhone_AppDelegate : NSObject <UIApplicationDelegate, BBVATabBarViewControllerDelegate, MFMailComposeViewControllerDelegate, SplashViewControllerDelegate> {
    
@private
    
    /**
     * Application window. All views are child to this window
     */
    UIWindow *window_;
    UIImageView *imageView;
    /**
     * Navigation controller to contain disconected views
     */
    UINavigationController *disconectedNavigationController_;
    
    /**
     * BBVA tab bar controller to show connected views
     */
    BBVATabBarViewController *connectedTabBarViewController_;
    
    /**
     * Splash view controller
     */
    SplashViewController *splashViewController_;
    
    /**
     * Current view controller
     */
    UIViewController *currentViewController_;
    
    /**
     * Legal conditions initial web browser
     */
    BrowserViewController *initialLegalConditionsViewController_;
    
    /**
     * The global position view controller.
     */
    GlobalPositionViewController *globalPositionViewController_;
    
    /**
     * The transfer position view controller.
     */
    TransfersListViewController *transfersListViewController_;

    /**
     * The transfer position view controller.
     */
    PaymentsListViewController *paymentsListViewController_;
    
    /**
     * The Frequen Operations position view controller.
     */
    FrequentOperationListViewController *freOperationsListViewController_;
    
    /**
     * The map position view controller.
     */
    POIMapViewController *poiMapViewController_;
    
    /**
     * Navigation controller to contain the initial legal conditions view controller
     */
    UINavigationController *initialLegalConditionsNavigationController_;
    
    /**
     * The activity indicator
     */
    NXTActivityIndicator *activityIndicator_;
    
    /**
     * Legal terms view controller
     */
    BrowserViewController *legalTermsViewController_;
    
    /**
     * Global position tab information
     */
    BBVATabBarTabInformation *globalPositionTabInformation_;
    
    /**
     * Transfer position tab information
     */
    BBVATabBarTabInformation *transfersListTabInformation_;

    /**
     * Payment position tab information.
     */
    BBVATabBarTabInformation *paymentsListTabInformation_;
    
    /**
     * Frequent operations position tab information
     */
    BBVATabBarTabInformation *FreqOperationsListTabInformation_;
    
    /**
     * Poi map position tab information.
     */
    BBVATabBarTabInformation *poiMapTabInformation_;
    
    /**
     * Default tabs order. Both visible and invisible tabs are displayed
     */
    NSMutableArray *defaultOrderTabsArray_;
    
    /**
     * Current tabs order. Both visible and invisible tabs are displayed
     */
    NSMutableArray *currentOrderTabsArray_;
    
    /**
     * Tabs dictionary. Contains all the BBVATabBarTabInformation insntaces as the values associated to the identifier key
     */
    NSMutableDictionary *tabsDictionary_;

    /**
     * Flag that indicates if we must show the SMS Configuration message
     */
    BOOL smsConfigurationMessageShown_;
    
    /**
     * Last username succesfully logged
     */
    NSString *lastUsernameLogged_;
    
    /**
     * Account graphic displayed flag. YES when account graphic has been displayed at least once, NO otherwise
     */
    BOOL accountGraphicDisplayed_;
    
    /**
     * Stock graphic displayed flag. YES when stock graphic has been displayed at least once, NO otherwise
     */
    BOOL stockGraphicDisplayed_;
	
    /**
     * First run flag. YES when is the application first run, NO otherwise
     */
    BOOL firstRun_;
    
    /**
     * Legal conditions accepted flag. YES when legal conditions were accepted, NO otherwise
     */
    BOOL legalContitionsAccepted_;
    
    /**
     * Track window tap flag. YES when the tapping on window must be tracked to automatically log-out user, NO otherwise
     */
    BOOL trackWindowTap_;
    
    /**
     * Time interval used to have the last legal terms
     */
    NSDate *dateLegalTerms_;
    
    /**
     * FAQ URL
     */
    NSString *faqURL_;
    
    /**
     * Tabs order identifiers array. Contains the tabs identifiers in the order they where stored
     */
    NSArray *tabsIdentifiersOrderArray_;
    
    /**
     * Next time the session will expire, measured as seconds from Jan 1, 2001
     */
    NSTimeInterval nextExpirationTime_;
	
    /**
     * Last error message
     */
    NSString *lastErrorMessage_;
    
    /**
     * Flag to indicate that the gloabl position chart advice must be shown
     */
    BOOL hideGPChartAdvice_;
    
    /**
     * Flag to indicate that the gloabl position char advice has been shown in this session
     */
    BOOL gpChartAdviceShown_;
    
    /**
     * Flag to indicate that the buy/sell stocksadvice must be shown
     */
    BOOL hideStockBuySellAdvice_;
    
    /**
     * Flag to indicate that the buy/sell stocksadvice has been shown in this session
     */
    BOOL stockBuySellAdviceShown_;
    
    /**
     * POI map filter array. If a network is into the array, then the switch value is YES. Otherwise, NO
     */
    NSMutableArray *poiMapNetworksFilter_;
    /**
     * Flag to indicate that the stock account chart advice must be shown
     */
    BOOL hideStockAccountChartAdvice_;
    
    /**
     * Flag to indicate that the stock account char advice has been shown in this session
     */
    BOOL stockAccountChartAdviceShown_;

    /**
     * Flag to indicate that a session ping invocation is required
     */
    BOOL sessionPingInvocationRequired_;

    /**
     * Flag to indicate that we are retrieving the date of legal terms due to a returning from background
     */
    BOOL obtainingLegalTermsFromBackground_;
    
    /**
     * Manager to begin a phone call
     */
    PhoneCallManager *phoneCallManager_;
    
    RadioStreamController *radioStreamManager_;
    
    int32_t deeplinkOption_;
}
/**
 * Provides read-write access to the hideStockAccountChartAdvice
 */
@property (nonatomic, readwrite, assign) BOOL hideStockAccountChartAdvice;

/**
 * Provides read-write access to the stockAccountChartAdviceShown
 */
@property (nonatomic, readwrite, assign) BOOL stockAccountChartAdviceShown;

/**
 * Provides read-write access to the modelObjectToNavigate
 */
@property (nonatomic, readwrite, retain) NSObject *modelObjectToNavigate;

/**
 * Provides read-write access to the poiMapNetworksFilter
 */
@property (nonatomic, readwrite, retain) NSMutableArray *poiMapNetworksFilter;

/**
 * Provides read-write access to the hideGPChartAdvice
 */
@property (nonatomic, readwrite, assign) BOOL hideGPChartAdvice;

/**
 * Provides read-write access to the hideGPChartAdvice
 */
@property (nonatomic, readwrite, assign) BOOL gpChartAdviceShown;

/**
 * Provides read-write access to the stockBuySellAdviceShown
 */
@property (nonatomic, readwrite, assign) BOOL stockBuySellAdviceShown;

/**
 * Provides read-write access to the hideStockBuySellAdvice
 */
@property (nonatomic, readwrite, assign) BOOL hideStockBuySellAdvice;

/**
 * Provides read-write access to the application window and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UIWindow *window;

/**
 * Provides read-write access to the navigation controller and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UINavigationController *disconnectedNavigationController;

/**
 * BBVA tab bar controller
 */
@property (nonatomic, readwrite, retain) IBOutlet BBVATabBarViewController *connectedTabBarViewController;

/**
 * Splash controller
 */
@property (nonatomic, readwrite, retain) IBOutlet SplashViewController *splashViewController;

/**
 * Provides read-write access to the legal conditions initial web browser and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet BrowserViewController *initialLegalConditionsViewController;

/**
 * Provides read-write access to the avigation controller to contain the initial legal conditions view controller and exports it for Interface Builder
 */
@property (nonatomic, readwrite, retain) IBOutlet UINavigationController *initialLegalConditionsNavigationController;

/**
 * Provides readwrite access to the smsConfigurationMessageShown flag
 */
@property (nonatomic, readwrite, assign) BOOL smsConfigurationMessageShown;

/**
 * Provides readwrite access to the onDisconnectedViews flag
 */
@property (nonatomic, readwrite, assign) BOOL onDisconnectedViews;

/**
 * Provides readwrite access to the last username succesfully logged
 */
@property (nonatomic, readwrite, copy) NSString *lastUsernameLogged;

/**
 * Provides read-write access to the account graphic displayed flag
 */
@property (nonatomic, readwrite, assign) BOOL accountGraphicDisplayed;

/**
 * Provides readwrite access to the stock graphic displayed flag
 */
@property (nonatomic, readwrite, assign) BOOL stockGraphicDisplayed;

/**
 * Provides readonly access to the first run flag
 */
@property (nonatomic, readonly, assign) BOOL firstRun;

/**
 * Provides readonly access to the date Legal Terms
 */
@property (nonatomic, readonly, copy) NSDate *dateLegalTerms;

/**
 * Provides readonly access to the FAQ URL
 */
@property (nonatomic, readonly, copy) NSString *faqURL;

/**
 * Provides readonly access to the legal Contitions Accepted
 */
@property (nonatomic, readwrite, assign) BOOL legalContitionsAccepted;


/**
 * Radio Stream  controller
 */
@property (nonatomic, readwrite, retain) RadioStreamController *radioStreamManager;

/**
 * Provides read-write access to the deeplinkOption
 */
@property (nonatomic, readwrite, assign) int32_t deeplinkOption;

/**
 * Shows the confirmation dialog to call a telephone
 *
 * @param aPhone to call
 */
- (void)showCallDialogWithPhone:(NSString *)aPhone;

/**
 * Shows the confirmation dialog to call a telephone with title and message.
 *
 * @param aPhone: Phone number to call.
 * @param title: A title to show message.
 * @param message: The message information.
 * @param messageError: The messsage error if can't call.
 */
- (void)showCallDialogWithPhone:(NSString *)aPhone withTitle:(NSString *)title andMessage:(NSString *)message andMessageError:(NSString *)messageError;

/**
 * Shows activity indicator and disables user interaction
 *
 * @param place of which we want to show the activity indicator
 */
- (void)showActivityIndicator:(PlaceOfActivityIndicator)place;

/**
 * Shows activity indicator and disables user interaction
 *
 * @param place of which we want to show the activity indicator
 * @param message to show (optional)
 */
- (void)showActivityIndicator:(PlaceOfActivityIndicator)place withMessage:(NSString *)message;

/**
 * Shows an error alert view
 *
 * @param anErrorMessage The error alert view message
 */
- (void)showErrorWithMessage:(NSString *)anErrorMessage;

/**
 * Hides activity indicator and enables user interaction
 */
- (void)hideActivityIndicator;

/**
 * Turns from the disconnected views to the connected views once the login is correct
 */
- (void)loginPerformed;

/**
 * Disconnects the user and returns to Home view
 */
- (void)disconnect;

/**
 * Save user preferences
 */
- (void)saveUserPreferences;

/**
 * Force the close and logout of the server session
 */
- (void)closeSessionAndLogout;

/**
 * Inform user of a session expired
 */
- (void)sessionExpired;

/**
 * Selects the global position tab when the connected tab bar is being displayed
 *
 * @return YES when the global position tab can be displayed
 */
- (BOOL)displayGlobalPositionTab;

/**
 * Starts editing the tabs order
 */
- (void)editTabsOrder;

/**
 * Set tabBar visibility if currentViewController is BBVATabBarViewController
 *
 * @param visible YES to show tabBar, NO to hide tabBar
 * @param animated YES to show or hide tabBar with effect, NO to show or hide tabBar without effect
 * @return YES if connected view controller is being displayed NO otherwise
 */
- (BOOL)setTabBarVisibility:(BOOL)visible
                   animated:(BOOL)animated;

/**
 * Present a modal view controller with all views in application.
 *
 * @param viewController: UIViewController to pressent modal view.
 * @param animated: YES to show modalView with effect. NO otherwise.
 */
- (void)presentModalViewController:(UIViewController *)viewController
                          animated:(BOOL)animated;

/**
 * Dismiss modal view controller.
 *
 * @param animated: YES to dismiss modal view with effect. NO otherwise.
 */
- (void)dismissModalViewControllerAnimated:(BOOL)animated;

/**
 * Shows the given UIActionSheet in the currentViewController_'s view
 *
 * @param sheet The action sheet
 */
- (void)showActionSheet:(UIActionSheet *)sheet;

/**
 * Reload and check that tab item is visible
 */
- (void)reloadVisibleTabBarItems;

@end

