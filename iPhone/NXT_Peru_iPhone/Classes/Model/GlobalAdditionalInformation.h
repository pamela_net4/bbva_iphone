/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"

/**
 * Defines the usage of OTP
 */
typedef enum {
    
    otp_UsageUnknown = -1,  //!< OTP usage is not defined
    otp_UsageOTP = 1,       //!< OTP usage with SMS
    otp_UsageTC             //!< OTP usage with TC (Traveller mode)
    
}OTP_Usage;

/**
 * Contains additional information inside the global position
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GlobalAdditionalInformation : StatusEnabledResponse {

@private
    
    /**
     * Customer name
     */
    NSString *customer_;
    
    /**
     * Last access
     */
    NSString *lastAccess_;
    
    /**
     * Accumulated points
     */
    NSString *accumulatedPoints_;
    
    /**
     * Executive name
     */
    NSString *executiveName_;
    
    /**
     * Purchase exchange rate
     */
    NSString *purchaseExchangeRate_;
    
    /**
     * Sale exchange rate
     */
    NSString *saleExchangeRate_;
    
    /**
     * E-mail
     */
    NSString *eMail_;
    
    /**
     * Executive e-mail
     */
    NSString *executiveEMail_;
    
    /**
     * Customer type
     */
    NSString *customerType_;
    
    /**
     * Customer branch
     */
    NSString *branch_;
    
    /**
     * Customer mobile phone number
     */
    NSString *mobilePhoneNumber_;
    
    /**
     * Customer mobile phone carrier
     */
    NSString *carrier_;
    
    /**
     * Message for group code type 1
     */
    NSString *messageCTS1_;
    
    /**
     * Message for group code type 2
     */
    NSString *messageCTS2_;
    
    /**
     * Subscribed to OTP service
     */
    BOOL otpActive_;
    
    /**
     * OPT usage: OTP o TC(viajero)
     */
    OTP_Usage otpUsage_;

    /**
     * Titular name
     */
    NSString *titularName_;


}

/**
 * Provides read-only access to the customer name
 */
@property (nonatomic, readonly, copy) NSString *customer;

/**
 * Provides read-only access to the last access
 */
@property (nonatomic, readonly, copy) NSString *lastAccess;

/**
 * Provides read-only access to the accumulated points
 */
@property (nonatomic, readonly, copy) NSString *accumulatedPoints;

/**
 * Provides read-only access to the executive name
 */
@property (nonatomic, readonly, copy) NSString *executiveName;

/**
 * Provides read-only access to the purchase exchange rate
 */
@property (nonatomic, readonly, copy) NSString *purchaseExchangeRate;

/**
 * Provides read-only access to the sale exchange rate
 */
@property (nonatomic, readonly, copy) NSString *saleExchangeRate;

/**
 * Provides read-only access to the e-mail
 */
@property (nonatomic, readonly, copy) NSString *eMail;

/**
 * Provides read-only access to the executive e-mail
 */
@property (nonatomic, readonly, copy) NSString *executiveEMail;

/**
 * Provides read-only access to the customer type
 */
@property (nonatomic, readonly, copy) NSString *customerType;

/**
 * Provides read-only access to the customer branch
 */
@property (nonatomic, readonly, copy) NSString *branch;

/**
 * Provides read-only access to the customer mobile phone number
 */
@property (nonatomic, readonly, copy) NSString *mobilePhoneNumber;

/**
 * Provides read-only access to the customer mobile phone carrier
 */
@property (nonatomic, readonly, copy) NSString *carrier;

/**
 * Provides read-only access to the Message for group code type 1
 */
@property (nonatomic, readonly, copy) NSString *messageCTS1;

/**
 * Provides read-only access to the Message for group code type 2
 */
@property (nonatomic, readonly, copy) NSString *messageCTS2;

/**
 * Provides read-only access to the activation of opt service
 */
@property (nonatomic, readonly, assign) BOOL otpActive;

/**
 * Provides read-only access to the otp usage
 */
@property (nonatomic, readonly, assign) OTP_Usage otpUsage;

/**
 * Provides read-only access to the name of the titular
 */
@property (nonatomic, readonly, copy) NSString *titularName;

/**
 * Updates the global position additiona information from another global position additional information
 *
 * @param anAdditionalInformation The global position additional information to update from
 */
- (void)updateFrom:(GlobalAdditionalInformation *)anAdditionalInformation;

/**
 * Remove the contained data
 */
- (void)removeData;

@end
