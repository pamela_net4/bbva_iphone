/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"

/**
 * BaseXMLParserObject protected category
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BaseXMLParserObject(protected)

/**
 * Provides read-only access to the auxiliary string to contruct integer values
 */
@property (nonatomic, readonly, copy) NSString *elementString;

/**
 * Switch parsing from one object to another
 *
 * @param aParser The parser parsing the document
 * @param anXMLParserObject The XML parser object parsing one element
 * @param anOpeningTag The element opening tag
 * @protected
 */
- (void)switchParser:(NSXMLParser *)aParser toParserObject:(BaseXMLParserObject *)anXMLParserObject withOpeningTag:(NSString *)anOpeningTag;

@end
