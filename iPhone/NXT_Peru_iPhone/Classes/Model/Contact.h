/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "ParseableObject.h"

/**
 * Address book contact information
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Contact : ParseableObject {
    
@private
    
    /**
     * Contact name
     */
    NSString *name_;
    
    /**
     * Contact email
     */
    NSString *email_;
    
    /**
     * Contact photo
     */
    UIImage *photo_;
    
    /**
     * Contact phone number
     */
    NSString *phoneNumber_;
    
    /**
     * Contact telephony carrier
     */
    NSString *carrier_;
    
    /**
     * Send SMS flag
     */
    BOOL sendSMS_;
    
    /**
     * Send mail flag
     */
    BOOL sendMail_;
    
    /**
     * Secondary mail, if we enter it manually
     */
    NSString* secondEmail_;
    
    /**
     * List of telephones
     */
    NSMutableArray *telephoneList_;
    
    /**
     * List of emails
     */
    NSMutableArray *emailList_;
    
}

/**
 * Provides read-only access to the emailList
 */
@property (nonatomic, readwrite, retain) NSMutableArray *emailList;

/**
 * Provides read-only access to the telephoneList
 */
@property (nonatomic, readwrite, retain) NSMutableArray *telephoneList;

/**
 * Provides read-only access to the contact email
 */
@property (nonatomic, readwrite, copy) NSString *secondEmail;

/**
 * Provides read-only access to the contact email
 */
@property (nonatomic, readwrite, copy) NSString *email;

/**
 * Provides read-only access to the contact photo image
 */
@property (nonatomic, readwrite, retain) UIImage *photo;

/**
 * Provides read-only access to the contact name
 */
@property (nonatomic, readwrite, copy) NSString *name;

/**
 * Provides read-only access to the contact phone number
 */
@property (nonatomic, readwrite, copy) NSString *phoneNumber;

/**
 * Provides read-only access to the contact telephony carrier
 */
@property (nonatomic, readwrite, copy) NSString *carrier;

/**
 * Provides read only access to the send SMS flag
 */
@property (nonatomic, readwrite) BOOL sendSMS;

/**
 * Provides read only access to the send mail flag
 */
@property (nonatomic, readwrite) BOOL sendMail;


/**
 * Initializes a contact instance given its name
 *
 * @param aName The contact name
 * @return The initialized Contact instance
 */
- (id)initWithName:(NSString *)aName;

/**
 * Designated initializer. Initializes a contact instance given its name and its phone number
 *
 * @param aName The contact name
 * @param aPhoneNumber The contact phone number
 * @return The initialized Contact instance
 */
- (id)initWithName:(NSString *)aName phoneNumber:(NSString *)aPhoneNumber;

@end
