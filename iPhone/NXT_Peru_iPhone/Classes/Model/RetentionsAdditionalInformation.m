/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "RetentionsAdditionalInformation.h"
#import "Tools.h"


/**
 * Enumerates the analysis states
 */
typedef enum {
    
    raixeas_AnalyzingCurrency = serxeas_ElementsCount, //!<Analyzing the currency
	raixeas_AnalyzingTotalRetention //!<Analyzing the total retention
    
} RetentionAdditionalInformationXMLElementAnalyzerState;


#pragma mark -

/**
 * RetentionsAddtionalInformation private category
 */
@interface RetentionsAdditionalInformation(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearRetentionsAdditionalInformationData;

@end


#pragma mark -

@implementation RetentionsAdditionalInformation

#pragma mark -
#pragma mark Properties

@synthesize currency = currency_;
@synthesize totalRetentionString = totalRetentionString_;
@dynamic totalRetention;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearRetentionsAdditionalInformationData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearRetentionsAdditionalInformationData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"divisa"]) {
            
            xmlAnalysisCurrentValue_ = raixeas_AnalyzingCurrency;
            
        } else if ([lname isEqualToString:@"totalretenido"]) {
            
            xmlAnalysisCurrentValue_ = raixeas_AnalyzingTotalRetention;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == raixeas_AnalyzingCurrency) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == raixeas_AnalyzingTotalRetention) {
            
            [totalRetentionString_ release];
            totalRetentionString_ = nil;
            totalRetentionString_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the total retentions, calculating it from the string representation if necessary
 *
 * @return The total retentions
 */
- (NSDecimalNumber *)totalRetention {
    
    NSDecimalNumber *result = totalRetention_;
    
    if (result == nil) {
        
        if ([totalRetentionString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:totalRetentionString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                totalRetention_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation RetentionsAdditionalInformation(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearRetentionsAdditionalInformationData {
    
    [currency_ release];
    currency_ = nil;
    
    [totalRetentionString_ release];
    totalRetentionString_ = nil;
    
    [totalRetention_ release];
    totalRetention_ = nil;
    
}

@end
