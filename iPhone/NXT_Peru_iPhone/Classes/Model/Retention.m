/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "Retention.h"
#import "Tools.h"


/**
 * Enumerates the analysis states
 */
typedef enum {
    
    rexeas_AnalyzingDischargeDate = serxeas_ElementsCount, //!<Analyzing the retention discharge date
	rexeas_AnalyzingDueDate, //!<Analyzing the retention due date
    rexeas_AnalyzingNumber, //!<Analyzing the retention number
    rexeas_AnalyzingRemarks, //!<Analyzing the retention remarks
    rexeas_AnalyzingCurrentAmount //!<Analyzing the retention current amount
    
} RetentionXMLElementAnalyzerState;


#pragma mark -

/**
 * Retention private category
 */
@interface Retention(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearRetentionData;

@end


#pragma mark -

@implementation Retention

#pragma mark -
#pragma mark Properties

@synthesize dischargeDateString = dischargeDateString_;
@dynamic dischargeDate;
@synthesize dueDateString = dueDateString_;
@dynamic dueDate;
@synthesize number = number_;
@synthesize remarks = remarks_;
@synthesize currentAmountString = currentAmountString_;
@dynamic currentAmount;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearRetentionData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearRetentionData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"fechaalta"]) {
            
            xmlAnalysisCurrentValue_ = rexeas_AnalyzingDischargeDate;
            
        } else if ([lname isEqualToString:@"fechavenciomiento"]) {
            
            xmlAnalysisCurrentValue_ = rexeas_AnalyzingDueDate;
            
        } else if ([lname isEqualToString:@"numero"]) {
            
            xmlAnalysisCurrentValue_ = rexeas_AnalyzingNumber;
            
        } else if ([lname isEqualToString:@"observacion"]) {
            
            xmlAnalysisCurrentValue_ = rexeas_AnalyzingRemarks;
            
        } else if ([lname isEqualToString:@"importeactual"]) {
            
            xmlAnalysisCurrentValue_ = rexeas_AnalyzingCurrentAmount;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == rexeas_AnalyzingDischargeDate) {
            
            [dischargeDateString_ release];
            dischargeDateString_ = nil;
            dischargeDateString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == rexeas_AnalyzingDueDate) {
            
            [dueDateString_ release];
            dueDateString_ = nil;
            dueDateString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == rexeas_AnalyzingNumber) {
            
            [number_ release];
            number_ = nil;
            number_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == rexeas_AnalyzingRemarks) {
            
            [remarks_ release];
            remarks_ = nil;
            remarks_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == rexeas_AnalyzingCurrentAmount) {
            
            [currentAmountString_ release];
            currentAmountString_ = nil;
            currentAmountString_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the discharge date, calculating it from the string representation if necessary
 *
 * @return The discharge date
 */
- (NSDate *)dischargeDate {
    
    NSDate *result = dischargeDate_;
    
    if (result == nil) {
        
        dischargeDate_ = [[Tools dateFromServerString:dischargeDateString_] retain];
        result = dischargeDate_;
        
    }
    
    return result;
    
}

/*
 * Returns the due date, calculating it from the string representation if necessary
 *
 * @return The due date
 */
- (NSDate *)dueDate {
    
    NSDate *result = dueDate_;
    
    if (result == nil) {
        
        dueDate_ = [[Tools dateFromServerString:dueDateString_] retain];
        result = dueDate_;
        
    }
    
    return result;
    
}

/*
 * Returns the current amount, calculating it from the string representation if necessary
 *
 * @return The current amount
 */
- (NSDecimalNumber *)currentAmount {
    
    NSDecimalNumber *result = currentAmount_;
    
    if (result == nil) {
        
        if ([currentAmountString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:currentAmountString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                currentAmount_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation Retention(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearRetentionData {
    
    [dischargeDateString_ release];
    dischargeDateString_ = nil;
    
    [dischargeDate_ release];
    dischargeDate_ = nil;
    
    [dueDateString_ release];
    dueDateString_ = nil;
    
    [dueDate_ release];
    dueDate_ = nil;
    
    [number_ release];
    number_ = nil;
    
    [remarks_ release];
    remarks_ = nil;
    
    [currentAmountString_ release];
    currentAmountString_ = nil;
    
    [currentAmount_ release];
    currentAmount_ = nil;
    
}

@end
