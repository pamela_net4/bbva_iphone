/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>
#import "Constants.h"


/**
 * Contains information about an object that is part of the model and can be parsed
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ParseableObject : NSObject <NSXMLParserDelegate> {
@protected
	
	/**
	 * Parent parseable object to returns control
	 */
	ParseableObject* parentParseableObject_;
	
	/**
	 * The current value being analyzed
	 */
	NSInteger xmlAnalysisCurrentValue_;
	
	/**
	 * Flag that indicates if the information is updated
	 */
	StateOfInformationEnumeration informationUpdated_;
    
    /**
     * Notification to post when parsing ends
     */
    NSString *notificationToPost_;
	
    /**
     * Notification to post when parsing ends
     */
    NSString *downloadFile_;
	
@private
	
	/**
	 * Tag that opened the parseable object element. The same tag must be found to close the element
	 */
	NSString *openingTag_;
	
	/**
	 * Auxiliary string to contruct integer values
	 */
	NSMutableString *auxString_;

}

/**
 * Provides read access to the notificationToPost
 */
@property (nonatomic, readwrite, copy) NSString *notificationToPost;

/**
 * Provides read write access to the related file
 */
@property (nonatomic, readwrite, copy) NSString *downloadFile;

/**
 * Provides readwrite access to the informationUpdated
 */
@property (nonatomic, readwrite, assign) StateOfInformationEnumeration informationUpdated;

/**
 * Provides readwrite access to the informationUpdated
 */
@property (nonatomic, readwrite, assign) ParseableObject *parentParseableObject;

/**
 * Provides read-write access to the tag that opened the parseable object element
 */
@property (nonatomic, readwrite, copy) NSString *openingTag;

/**
 * Provides read-only access to the auxiliary string to contruct integer values
 */
@property (nonatomic, readonly, copy) NSString *elementString;


/**
 * Returns YES if information is ready to be used
 */
- (BOOL) isInformationReady;

@end
