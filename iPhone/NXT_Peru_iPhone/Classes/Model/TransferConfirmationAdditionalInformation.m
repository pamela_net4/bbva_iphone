/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "TransferConfirmationAdditionalInformation.h"


/**
 * Enumerates the analysis states
 */
typedef enum {
    
    tcaixeas_AnalyzingMarketPlaceCommision = serxeas_ElementsCount, //!<Analyzing the market place commision
    tcaixeas_AnalyzingMarketPlaceCommisionCurrency, //!<Analyzing the market place commision currency
    tcaixeas_AnalyzingHolder, //!<Analyzing the holder
    tcaixeas_AnalyzingBeneficiary, //!<Analyzing the beneficiary
    tcaixeas_AnalyzingBeneficiaryAccount, //!<Analyzing the beneficiary account
    tcaixeas_AnalyzingBeneficiaryAccountCurrency, //!<Analyzing the beneficiary
    tcaixeas_AnalyzingOperation, //!<Analyzing the operation
    tcaixeas_AnalyzingDisclaimer, //!<Analyzing the disclaimer
	tcaixeas_AnalyzingCoordinate, //!<Analyzing the coordinate
	tcaixeas_AnalyzingSeal, //!<Analyzing the seal
	tcaixeas_AnalyzingDestinationBank, //!<Analyzing the destination bank
	tcaixeas_AnalyzingAmountToTransfer, //!<Analyzing the amount to transfer
	tcaixeas_AnalyzingAmountCurrencyToTransfer, //!<Analyzing the amount to transfer currency
	tcaixeas_AnalyzingCommission, //!<Analyzing the commission
	tcaixeas_AnalyzingCurrencyCommission, //!<Analyzing the commission currency
	tcaixeas_AnalyzingNetworkUse, //!<Analyzing the network use
	tcaixeas_AnalyzingCurrencyNetworkUse, //!<Analyzing the network use currency
	tcaixeas_AnalyzingAmountToPay, //!<Analyzing the amount to pay
	tcaixeas_AnalyzingCurrencyAmountToPay, //!<Analyzing the currency amount to pay
	tcaixeas_AnalyzingITFMessage, //!<Analyzing the ITF message
    tcaixeas_AnalyzingITFAmount, //!<Analyzing the ITF amount
    tcaixeas_AnalyzingITFSymbol, //!<Analyzing the ITF symbol
	tcaixeas_AnalyzingBeneficiaryDocument, //!<Analyzing the beneficiary document
    tcaixeas_AnalyzingBeneficiaryDocumentType, //!<Analyzing the beneficiary document type
    tcaixeas_AnalyzingBeneficiaryDocumentTypeInd, //!<Analyzing the beneficiary document type indicator
	tcaixeas_AnalyzingTipoCambio, //!<Analyzing the tipo campo element
	tcaixeas_AnalyzingMonedaTipoCambio, //!<Analyzing the moneda tipo campo element
    tcaixeas_AnalyzingModuloActivo
	
} TransferConfirmationAdditionalInformationXMLElementAnalyzerState;


#pragma mark -

/**
 * TransferConfirmationAdditionalInformation
 */
@interface TransferConfirmationAdditionalInformation(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearTransferConfirmationAdditionalInformationData;

@end


#pragma mark -

@implementation TransferConfirmationAdditionalInformation

#pragma mark -
#pragma mark Properties

@synthesize marketPlaceCommision = marketPlaceCommision_;
@synthesize marketPlaceCommisionCurrency = marketPlaceCommisionCurrency_;
@synthesize coordinate = coordinate_;
@synthesize seal = seal_;
@synthesize operation = operation_;
@synthesize holder = holder_;
@synthesize beneficiary = beneficiary_;
@synthesize beneficiaryAccount = beneficiaryAccount_;
@synthesize beneficiaryAccountCurrency = beneficiaryAccountCurrency_;
@synthesize disclaimer = disclaimer_;
@synthesize destinationBank = destinationBank_;
@synthesize amountToTransfer = amountToTransfer_;
@synthesize amountCurrencyToTransfer = amountCurrencyToTransfer_;
@synthesize commission = commission_;
@synthesize currencyCommission = currencyCommission_;
@synthesize networkUse = networkUse_;
@synthesize currencyNetworkUse = currencyNetworkUse_;
@synthesize amountToPay = amountToPay_;
@synthesize amountCurrencyToPay = amountCurrencyToPay_;
@synthesize itfMessage = itfMessage_;
@synthesize itfAmount = itfAmount_;
@synthesize itfSymbol = itfSymbol_;
@synthesize beneficiaryDocument = beneficiaryDocument_;
@synthesize tipoCambio = tipoCambio_;
@synthesize monedaTipoCambio = monedaTipoCambio_;
@synthesize moduleActive = moduleActive_;
@synthesize beneficiaryDocumentType = beneficiaryDocumentType_;
@synthesize beneficiaryDocumentTypeId = beneficiaryDocumentTypeId_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearTransferConfirmationAdditionalInformationData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearTransferConfirmationAdditionalInformationData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"comisionplaza"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingMarketPlaceCommision;
            
        } else if ([lname isEqualToString:@"monedacomisionplaza"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingMarketPlaceCommisionCurrency;
            
        } else if ([lname isEqualToString:@"coordenada"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingCoordinate;
            
        } else if ([lname isEqualToString:@"sello"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingSeal;
            
        } else if ([lname isEqualToString:@"operacion"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingOperation;
            
        } else if ([lname isEqualToString:@"titular"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingHolder;
            
        } else if ([lname isEqualToString:@"beneficiario"] || [lname isEqualToString:@"nombrebeneficiario"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingBeneficiary;
            
        } else if ([lname isEqualToString:@"cuentabeneficiario"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingBeneficiaryAccount;
            
        } else if ([lname isEqualToString:@"monedacuentabeneficiario"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingBeneficiaryAccountCurrency;
            
        } else if ([lname isEqualToString:@"disclaimer"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingDisclaimer;
            
        } else if ([lname isEqualToString:@"bancodestino"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingDestinationBank;
            
        } else if ([lname isEqualToString:@"importetransferir"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingAmountToTransfer;
            
        } else if ([lname isEqualToString:@"monedaimportetransferir"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingAmountCurrencyToTransfer;
            
        } else if ([lname isEqualToString:@"comision"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingCommission;
            
        } else if ([lname isEqualToString:@"monedacomision"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingCurrencyCommission;
            
        } else if ([lname isEqualToString:@"usored"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingNetworkUse;
            
        } else if ([lname isEqualToString:@"monedausored"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingCurrencyNetworkUse;
            
        } else if ([lname isEqualToString:@"importecargar"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingAmountToPay;
            
        }  else if ([lname isEqualToString:@"monedaimportecargar"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingCurrencyAmountToPay;
            
        } else if ([lname isEqualToString:@"mensajeitf"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingITFMessage;
            
        } else if ([lname isEqualToString:@"monedaitf"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingITFSymbol;
            
        }else if ([lname isEqualToString:@"importeitf"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingITFAmount;
            
        }else if ([lname isEqualToString:@"documentobeneficiario"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingBeneficiaryDocument;
            
        } else if ([lname isEqualToString:@"indtipodocumento"]){
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingBeneficiaryDocumentTypeInd;
            
        }else if ([lname isEqualToString:@"tipodocumento"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingBeneficiaryDocumentType;
            
        }else if ([lname isEqualToString:@"tipocambio"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingTipoCambio;
            
        } else if ([lname isEqualToString:@"monedatipocambio"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingMonedaTipoCambio;
            
        } else if ([lname isEqualToString:@"moduloactivo"]) {
            
            xmlAnalysisCurrentValue_ = tcaixeas_AnalyzingModuloActivo;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingMarketPlaceCommision) {
            
            [marketPlaceCommision_ release];
            marketPlaceCommision_ = nil;
            
            NSString *auxmarketPlaceCommision= [[elementString copyWithZone:self.zone]autorelease];
            marketPlaceCommision_ =[auxmarketPlaceCommision retain];
            
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingMarketPlaceCommisionCurrency) {
            
            [marketPlaceCommisionCurrency_ release];
            marketPlaceCommisionCurrency_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            marketPlaceCommisionCurrency_ = [aux retain];
            
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingCoordinate) {
            
            [coordinate_ release];
            coordinate_ = nil;
            
            NSString *auxCoord = [[elementString copyWithZone:self.zone]autorelease];
            coordinate_ =[auxCoord retain];
            
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingSeal) {
            
            [seal_ release];
            seal_ = nil;
            
            NSString *auxSeal = [[elementString copyWithZone:self.zone] autorelease];
            auxSeal = [auxSeal stringByReplacingOccurrencesOfString:@"\r" withString:@""];
            auxSeal = [auxSeal stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            seal_ = [auxSeal retain];
            
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingOperation) {
            
            [operation_ release];
            operation_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            operation_ = [aux retain];
            
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingHolder) {
            
            [holder_ release];
            holder_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            holder_ = [aux retain];
            
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingBeneficiary) {
            
            [beneficiary_ release];
            beneficiary_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            beneficiary_ = [aux retain];
            
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingBeneficiaryAccount) {
            
            [beneficiaryAccount_ release];
            beneficiaryAccount_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            beneficiaryAccount_ = [aux retain];
            
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingBeneficiaryAccountCurrency) {
            
            [beneficiaryAccountCurrency_ release];
            beneficiaryAccountCurrency_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            beneficiaryAccountCurrency_ = [aux retain];
            
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingDisclaimer) {
            
            [disclaimer_ release];
            disclaimer_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            disclaimer_ = [aux retain];
            
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingDestinationBank) {
            
            [destinationBank_ release];
            destinationBank_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            destinationBank_ = [aux retain];
			
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingAmountToTransfer) {
            
            [amountToTransfer_ release];
            amountToTransfer_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            amountToTransfer_ = [aux retain];
			
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingAmountCurrencyToTransfer) {
            
            [amountCurrencyToTransfer_ release];
            amountCurrencyToTransfer_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            amountCurrencyToTransfer_ = [aux retain];
			
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingCommission) {
            
            [commission_ release];
            commission_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            commission_ = [aux retain];
			
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingCurrencyCommission) {
            
            [currencyCommission_ release];
            currencyCommission_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            currencyCommission_ = [aux retain];
			
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingNetworkUse) {
            
            [networkUse_ release];
            networkUse_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            networkUse_ = [aux retain];
			
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingCurrencyNetworkUse) {
            
            [currencyNetworkUse_ release];
            currencyNetworkUse_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            currencyNetworkUse_ = [aux retain];
			
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingAmountToPay) {
            
            [amountToPay_ release];
            amountToPay_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            amountToPay_ = [aux retain];
			
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingCurrencyAmountToPay) {
            
            [amountCurrencyToPay_ release];
            amountCurrencyToPay_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            amountCurrencyToPay_ = [aux retain];
			
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingITFMessage) {
            
            [itfMessage_ release];
            itfMessage_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            itfMessage_ = [aux retain];
			
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingITFSymbol) {
            
            [itfSymbol_ release];
            itfSymbol_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            itfSymbol_ = [aux retain];
			
        }else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingITFAmount) {
            
            [itfAmount_ release];
            itfAmount_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            itfAmount_ = [aux retain];
			
        }else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingBeneficiaryDocument) {
            
            [beneficiaryDocument_ release];
            beneficiaryDocument_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            beneficiaryDocument_ = [aux retain];
			
        }else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingBeneficiaryDocumentTypeInd) {
            
            [beneficiaryDocumentTypeId_ release];
            beneficiaryDocumentTypeId_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            beneficiaryDocumentTypeId_ = [aux retain];
            
        }else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingBeneficiaryDocumentType) {
            
            [beneficiaryDocumentType_ release];
            beneficiaryDocumentType_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            beneficiaryDocumentType_ = [aux retain];
            
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingTipoCambio) {
            
            [tipoCambio_ release];
            tipoCambio_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            tipoCambio_ = [aux retain];
			
        } else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingMonedaTipoCambio) {
            
            [monedaTipoCambio_ release];
            monedaTipoCambio_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];          
            monedaTipoCambio_ = [aux retain];
			
        }else if (xmlAnalysisCurrentValue_ == tcaixeas_AnalyzingModuloActivo) {
            
            [moduleActive_ release];
            moduleActive_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            moduleActive_ = [aux retain];
            
        }
        
        //
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end


#pragma mark -

@implementation TransferConfirmationAdditionalInformation(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearTransferConfirmationAdditionalInformationData {
    
    [marketPlaceCommision_ release];
    marketPlaceCommision_ = nil;
    
    [marketPlaceCommisionCurrency_ release];
    marketPlaceCommisionCurrency_ = nil;
    
    [coordinate_ release];
    coordinate_ = nil;
    
    [seal_ release];
    seal_ = nil;
    
    [operation_ release];
    operation_ = nil;
    
    [holder_ release];
    holder_ = nil;
    
    [beneficiary_ release];
    beneficiary_ = nil;
    
    [beneficiaryAccount_ release];
    beneficiaryAccount_ = nil;
    
    [beneficiaryAccountCurrency_ release];
    beneficiaryAccountCurrency_ = nil;
    
    [disclaimer_ release];
    disclaimer_ = nil;
	
	[destinationBank_ release];
	destinationBank_ = nil;
	
	[amountToTransfer_ release];
	amountToTransfer_ = nil;
	
	[amountCurrencyToTransfer_ release];
	amountCurrencyToTransfer_ = nil;
	
	[commission_ release];
	commission_ = nil;
	
	[currencyCommission_ release];
	currencyCommission_ = nil;
	
	[networkUse_ release];
	networkUse_ = nil;
	
	[currencyNetworkUse_ release];
	currencyNetworkUse_ = nil;
	
	[amountToPay_ release];
	amountToPay_ = nil;
	
	[amountCurrencyToPay_ release];
	amountCurrencyToPay_ = nil;
	
	[itfMessage_ release];
	itfMessage_ = nil;
    
    [itfSymbol_ release];
	itfSymbol_ = nil;
	
    [itfAmount_ release];
	itfAmount_ = nil;
    
	[beneficiaryDocument_ release];
	beneficiaryDocument_ = nil;
    
    [beneficiaryDocumentType_ release];
    beneficiaryDocumentType_ = nil;
    
    [beneficiaryDocumentTypeId_ release];
    beneficiaryDocumentTypeId_ = nil;

    
    [tipoCambio_ release];
    tipoCambio_ = nil;
    
    [monedaTipoCambio_ release];
    monedaTipoCambio_ = nil;
    
    [moduleActive_ release];
    moduleActive_ = nil;
	
}

@end
