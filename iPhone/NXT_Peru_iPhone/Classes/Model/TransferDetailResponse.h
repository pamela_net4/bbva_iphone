/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import "StatusEnabledResponse.h"

//Forward declarations
@class TransferDetailAdditionalInformation;

@interface TransferDetailResponse : StatusEnabledResponse{
@private
    /**
     * Additional information
     */
    TransferDetailAdditionalInformation *auxAdditionalInformation_;
    
    /**
     * Array With transaction details
     */
    NSMutableArray *additionalInformationArray_;
    
}

/**
 * Provides read-only access to the array of aditional information
 */
@property (nonatomic, readonly, retain) NSMutableArray *additionalInformationArray;


@end
