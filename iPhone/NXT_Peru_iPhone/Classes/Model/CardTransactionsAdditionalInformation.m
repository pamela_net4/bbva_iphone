/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "CardTransactionsAdditionalInformation.h"
#import "Tools.h"


/**
 * Enumerates the analysis state
 */
typedef enum {
    
    ctaixeas_AnalyzingClosingDate = serxeas_ElementsCount, //!<Analyzing the closing date
    ctaixeas_AnalyzingLastPayedDay, //!<Analyzing the last payed day
    ctaixeas_AnalyzingMinimumPaymentML, //!<Analyzing the minimum payment ML
    ctaixeas_AnalyzingMinimumPaymentME, //!<Analyzing the minimum payment ME
    ctaixeas_AnalyzingTotalPaymentML, //!<Analyzing the total payment ML
    ctaixeas_AnalyzingTotalPaymentME, //!<Analyzing the total payment ME
    ctaixeas_AnalyzingDelayedPaymentML, //!<Analyzing the delayed payment ML
    ctaixeas_AnalyzingDelayedPaymentME, //!<Analyzing the delayed payment ME
    ctaixeas_AnalyzingMoreData, //!<Analyzing the more data element
	ctaixeas_AnalyzingIndex //!<Analyzing the index
    
} CardTransactionsAdditionalInformationXMLElementAnalyzerState;


#pragma mark -

/**
 * CardTransactionsAdditionalInformation private category
 */
@interface CardTransactionsAdditionalInformation(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearCardTransactionsAdditionalInformationData;

@end


#pragma mark -

@implementation CardTransactionsAdditionalInformation

#pragma mark -
#pragma mark Properties

@synthesize closingDateString = closingDateString_;
@dynamic closingDate;
@synthesize lastPaymentDayString = lastPaymentDayString_;
@dynamic lastPaymentDay;
@synthesize minimumPaymentMLString = minimumPaymentMLString_;
@dynamic minimumPaymentML;
@synthesize minimumPaymentMEString = minimumPaymentMEString_;
@dynamic minimumPaymentME;
@synthesize totalPaymentMLString = totalPaymentMLString_;
@dynamic totalPaymentML;
@synthesize totalPaymentMEString = totalPaymentMEString_;
@dynamic totalPaymentME;
@synthesize delayedPaymentMLString = delayedPaymentMLString_;
@dynamic delayedPaymentML;
@synthesize delayedPaymentMEString = delayedPaymentMEString_;
@dynamic delayedPaymentME;
@synthesize moreData = moreData_;
@synthesize index = index_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearCardTransactionsAdditionalInformationData];
	
	[super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initialized. Initializes a CardTransactionAdditionalnformation instance
 *
 * @return The initialized CardTransactionAdditionalnformation instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationCardTransactionsAdditionalnformationUpdated;
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates this instance with the given card transaction additional information
 */
- (void)updateFrom:(CardTransactionsAdditionalInformation *)aCardTransactionsAdditionalInformation {
    
    NSZone *zone = self.zone;
    
    [closingDateString_ release];
    closingDateString_ = nil;
    closingDateString_ = [aCardTransactionsAdditionalInformation.closingDateString copyWithZone:zone];
    
    [closingDate_ release];
    closingDate_ = nil;
    
    [lastPaymentDayString_ release];
    lastPaymentDayString_ = nil;
    lastPaymentDayString_ = [aCardTransactionsAdditionalInformation.lastPaymentDayString copyWithZone:zone];
    
    [lastPaymentDay_ release];
    lastPaymentDay_ = nil;
    
    [minimumPaymentMLString_ release];
    minimumPaymentMLString_ = nil;
    minimumPaymentMLString_ = [aCardTransactionsAdditionalInformation.minimumPaymentMLString copyWithZone:zone];
    
    [minimumPaymentML_ release];
    minimumPaymentML_ = nil;
    
    [minimumPaymentMEString_ release];
    minimumPaymentMEString_ = nil;
    minimumPaymentMEString_ = [aCardTransactionsAdditionalInformation.minimumPaymentMEString copyWithZone:zone];
    
    [minimumPaymentME_ release];
    minimumPaymentME_ = nil;
    
    [totalPaymentMLString_ release];
    totalPaymentMLString_ = nil;
    totalPaymentMLString_ = [aCardTransactionsAdditionalInformation.totalPaymentMLString copyWithZone:zone];
    
    [totalPaymentML_ release];
    totalPaymentML_ = nil;
    
    [totalPaymentMEString_ release];
    totalPaymentMEString_ = nil;
    totalPaymentMEString_ = [aCardTransactionsAdditionalInformation.totalPaymentMEString copyWithZone:zone];
    
    [totalPaymentME_ release];
    totalPaymentME_ = nil;
    
    [delayedPaymentMLString_ release];
    delayedPaymentMLString_ = nil;
    delayedPaymentMLString_ = [aCardTransactionsAdditionalInformation.delayedPaymentMLString copyWithZone:zone];
    
    [delayedPaymentML_ release];
    delayedPaymentML_ = nil;
    
    [delayedPaymentMEString_ release];
    delayedPaymentMEString_ = nil;
    delayedPaymentMEString_ = [aCardTransactionsAdditionalInformation.delayedPaymentMEString copyWithZone:zone];
    
    [delayedPaymentME_ release];
    delayedPaymentME_ = nil;
    
    [moreData_ release];
    moreData_ = nil;
    moreData_ = [aCardTransactionsAdditionalInformation.moreData copyWithZone:zone];
    
    [index_ release];
    index_ = nil;
    index_ = [aCardTransactionsAdditionalInformation.index copyWithZone:zone];
    
    [self updateFromStatusEnabledResponse:aCardTransactionsAdditionalInformation];
	
	self.informationUpdated = aCardTransactionsAdditionalInformation.informationUpdated;
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearCardTransactionsAdditionalInformationData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"fechacierre"]) {
            
            xmlAnalysisCurrentValue_ = ctaixeas_AnalyzingClosingDate;
            
        } else if ([lname isEqualToString: @"ultimodiapago"]) {
            
            xmlAnalysisCurrentValue_ = ctaixeas_AnalyzingLastPayedDay;
            
        } else if ([lname isEqualToString: @"pagominimoml"]) {
            
            xmlAnalysisCurrentValue_ = ctaixeas_AnalyzingMinimumPaymentML;
            
        } else if ([lname isEqualToString: @"pagominimome"]) {
            
            xmlAnalysisCurrentValue_ = ctaixeas_AnalyzingMinimumPaymentME;
            
        } else if ([lname isEqualToString: @"pagototalml"]) {
            
            xmlAnalysisCurrentValue_ = ctaixeas_AnalyzingTotalPaymentML;
            
        } else if ([lname isEqualToString: @"pagototalme"]) {
            
            xmlAnalysisCurrentValue_ = ctaixeas_AnalyzingTotalPaymentME;
            
        } else if ([lname isEqualToString: @"atrasosml"]) {
            
            xmlAnalysisCurrentValue_ = ctaixeas_AnalyzingDelayedPaymentML;
            
        } else if ([lname isEqualToString: @"atrasosme"]) {
            
            xmlAnalysisCurrentValue_ = ctaixeas_AnalyzingDelayedPaymentME;
            
        } else if ([lname isEqualToString: @"indmasdatos"]) {
            
            xmlAnalysisCurrentValue_ = ctaixeas_AnalyzingMoreData;
            
        } else if ([lname isEqualToString: @"indindice"]) {
            
            xmlAnalysisCurrentValue_ = ctaixeas_AnalyzingIndex;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == ctaixeas_AnalyzingClosingDate) {
            
            [closingDateString_ release];
            closingDateString_ = nil;
            closingDateString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctaixeas_AnalyzingLastPayedDay) {
            
            [lastPaymentDayString_ release];
            lastPaymentDayString_ = nil;
            lastPaymentDayString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctaixeas_AnalyzingMinimumPaymentML) {
            
            [minimumPaymentMLString_ release];
            minimumPaymentMLString_ = nil;
            minimumPaymentMLString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctaixeas_AnalyzingMinimumPaymentME) {
            
            [minimumPaymentMEString_ release];
            minimumPaymentMEString_ = nil;
            minimumPaymentMEString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctaixeas_AnalyzingTotalPaymentML) {
            
            [totalPaymentMLString_ release];
            totalPaymentMLString_ = nil;
            totalPaymentMLString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctaixeas_AnalyzingTotalPaymentME) {
            
            [totalPaymentMEString_ release];
            totalPaymentMEString_ = nil;
            totalPaymentMEString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctaixeas_AnalyzingDelayedPaymentML) {
            
            [delayedPaymentMLString_ release];
            delayedPaymentMLString_ = nil;
            delayedPaymentMLString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctaixeas_AnalyzingDelayedPaymentME) {
            
            [delayedPaymentMEString_ release];
            delayedPaymentMEString_ = nil;
            delayedPaymentMEString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctaixeas_AnalyzingMoreData) {
            
            [moreData_ release];
            moreData_ = nil;
            moreData_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctaixeas_AnalyzingIndex) {
            
            [index_ release];
            index_ = nil;
            index_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the closing date, creating it from the string representation if necessary
 *
 * @return The closing date
 */
- (NSDate *)closingDate {
    
    NSDate *result = closingDate_;
    
    if (result == nil) {
        
        closingDate_ = [[Tools dateFromServerString:closingDateString_] retain];
        result = closingDate_;
        
    }
    
    return result;
    
}

/*
 * Returns the last payment date, creating it from the string representation if necessary
 *
 * @return The last payment date
 */
- (NSDate *)lastPaymentDay {
    
    NSDate *result = lastPaymentDay_;
    
    if (result == nil) {
        
        lastPaymentDay_ = [[Tools dateFromServerString:lastPaymentDayString_] retain];
        result = lastPaymentDay_;
        
    }
    
    return result;
    
}

/*
 * Returns the minimum payment ML, creatin it from the string representation if necessary
 *
 * @return The minimum paymnet ML
 */
- (NSDecimalNumber *)minimumPaymentML {
    
    NSDecimalNumber *result = minimumPaymentML_;
    
    if (result == nil) {
        
        if ([minimumPaymentMLString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:minimumPaymentMLString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
				[minimumPaymentML_ release];
                minimumPaymentML_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the minimum payment ME, creatin it from the string representation if necessary
 *
 * @return The minimum paymnet ME
 */
- (NSDecimalNumber *)minimumPaymentME {
    
    NSDecimalNumber *result = minimumPaymentME_;
    
    if (result == nil) {
        
        if ([minimumPaymentMEString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:minimumPaymentMEString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
				[minimumPaymentME_ release];
                minimumPaymentME_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the total payment ML, creatin it from the string representation if necessary
 *
 * @return The total paymnet ML
 */
- (NSDecimalNumber *)totalPaymentML {
    
    NSDecimalNumber *result = totalPaymentML_;
    
    if (result == nil) {
        
        if ([totalPaymentMLString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:totalPaymentMLString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
				[totalPaymentML_ release];
                totalPaymentML_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the total payment ME, creatin it from the string representation if necessary
 *
 * @return The total paymnet ME
 */
- (NSDecimalNumber *)totalPaymentME {
    
    NSDecimalNumber *result = totalPaymentME_;
    
    if (result == nil) {
        
        if ([totalPaymentMEString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:totalPaymentMEString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
				[totalPaymentME_ release];
                totalPaymentME_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the delayed payment ML, creatin it from the string representation if necessary
 *
 * @return The delayed paymnet ML
 */
- (NSDecimalNumber *)delayedPaymentML {
    
    NSDecimalNumber *result = delayedPaymentML_;
    
    if (result == nil) {
        
        if ([delayedPaymentMLString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:delayedPaymentMLString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
				[delayedPaymentML_ release];
                delayedPaymentML_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the delayed payment ME, creatin it from the string representation if necessary
 *
 * @return The delayed paymnet ME
 */
- (NSDecimalNumber *)delayedPaymentME {
    
    NSDecimalNumber *result = delayedPaymentME_;
    
    if (result == nil) {
        
        if ([delayedPaymentMEString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:delayedPaymentMEString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
				[delayedPaymentME_ release];
                delayedPaymentME_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation CardTransactionsAdditionalInformation(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearCardTransactionsAdditionalInformationData {
    
    [closingDateString_ release];
    closingDateString_ = nil;
    
    [closingDate_ release];
    closingDate_ = nil;
    
    [lastPaymentDayString_ release];
    lastPaymentDayString_ = nil;
    
    [lastPaymentDay_ release];
    lastPaymentDay_ = nil;
    
    [minimumPaymentMLString_ release];
    minimumPaymentMLString_ = nil;
    
    [minimumPaymentML_ release];
    minimumPaymentML_ = nil;
    
    [minimumPaymentMEString_ release];
    minimumPaymentMEString_ = nil;
    
    [minimumPaymentME_ release];
    minimumPaymentME_ = nil;
    
    [totalPaymentMLString_ release];
    totalPaymentMLString_ = nil;
    
    [totalPaymentML_ release];
    totalPaymentML_ = nil;
    
    [totalPaymentMEString_ release];
    totalPaymentMEString_ = nil;
    
    [totalPaymentME_ release];
    totalPaymentME_ = nil;
    
    [delayedPaymentMLString_ release];
    delayedPaymentMLString_ = nil;
    
    [delayedPaymentML_ release];
    delayedPaymentML_ = nil;
    
    [delayedPaymentMEString_ release];
    delayedPaymentMEString_ = nil;
    
    [delayedPaymentME_ release];
    delayedPaymentME_ = nil;
    
    [moreData_ release];
    moreData_ = nil;
    
    [index_ release];
    index_ = nil;
    
}

@end
