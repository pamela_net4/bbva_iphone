/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "Deposit.h"
#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
    dxeas_AnalyzingAccountNumber = serxeas_ElementsCount, //!<Analyzing the deposit account number
    dxeas_AnalyzingCurrentBalance, //!<Analyzing the current balance
	dxeas_AnalyzingAvailableBalance, //!<Analyzing the available balance
    dxeas_AnalyzingCurrency, //!<Analyzing the currency
    dxeas_AnalyzingSubject, //!<Analyzing the subject
    dxeas_AnalyzingBalanceInCountryCurrency, //!<Analyzing the balance in country currency
    dxeas_AnalyzingCurrencyAvailable, //!<Analyzing the currency available
    dxeas_AnalyzingCTSIndicator, //!<Analyzing the Cts indicator
    dxeas_AnalyzingGroupingCode //!<Analyzing the grouping code
    
} DepositXMLElementAnalyzerState;


#pragma mark -

/**
 * Deposit private category
 */
@interface Deposit(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearDepositData;

@end


#pragma mark -

@implementation Deposit

#pragma mark -
#pragma mark Properties

@synthesize accountNumber = accountNumber_;
@synthesize currentBalanceString = currentBalanceString_;
@dynamic currentBalance;
@synthesize availableBalanceString = availableBalanceString_;
@dynamic availableBalance;
@synthesize currency = currency_;
@synthesize subject = subject_;
@synthesize balanceInCountryCurrencyString = balanceInCountryCurrencyString_;
@synthesize currencyAvailable = currencyAvailable_;
@synthesize ctsIndicator = ctsIndicator_;
@synthesize groupingCode = groupingCode_;

@dynamic balanceInCountryCurrency;

#pragma mark -
#pragma mark Memory management
/**
 * Deallocates used memory
 */
- (void)dealloc {
	
    [self clearDepositData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Title texts management

/*
 * Returns the deposit list display name. It can be used whenever a deposit name must be displayed inside a list
 */
- (NSString *)depositListName {
    
    return [NSString stringWithFormat:@"CTS %@", [Tools obfuscateAccountNumber:accountNumber_]];
    
}

/*
 * Returns the obfuscated deposit account number
 */
- (NSString *)obfuscatedDepositAccountNumber {
    
    return [Tools obfuscateAccountNumber:accountNumber_];
    
}

//-(void) setGroupingCode:(NSString *)code
//{
//    groupingCode_ = code;
//}
//
//-(void) setAccountNumber:(NSString *)number
//{
//    accountNumber_ = number;
//}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearDepositData];
	[groupingCode_ release];
	groupingCode_ = [@"" retain];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if ([lname isEqualToString: @"numerocuenta"]) {
            
            xmlAnalysisCurrentValue_ = dxeas_AnalyzingAccountNumber;
            
        } else if ([lname isEqualToString: @"saldoactual"]) {
            
            xmlAnalysisCurrentValue_ = dxeas_AnalyzingCurrentBalance;
            
        } else if ([lname isEqualToString: @"saldodisponible"]) {
            
            xmlAnalysisCurrentValue_ = dxeas_AnalyzingAvailableBalance;
            
        } else if ([lname isEqualToString: @"moneda"]) {
            
            xmlAnalysisCurrentValue_ = dxeas_AnalyzingCurrency;
            
        } else if ([lname isEqualToString: @"asunto"]) {
            
            xmlAnalysisCurrentValue_ = dxeas_AnalyzingSubject;
            
        } else if ([lname isEqualToString:@"importeconvertido"]) {
            
            xmlAnalysisCurrentValue_ = dxeas_AnalyzingBalanceInCountryCurrency;
            
        }else if ([lname isEqualToString:@"monedadisponible"]) {
            
            xmlAnalysisCurrentValue_ = dxeas_AnalyzingCurrencyAvailable;
            
        }else if ([lname isEqualToString:@"indicadorcts"]) {
            
            xmlAnalysisCurrentValue_ = dxeas_AnalyzingCTSIndicator;
            
        }else if ([lname isEqualToString:@"codigoagrupacion"]) {
            
            xmlAnalysisCurrentValue_ = dxeas_AnalyzingGroupingCode;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSZone *zone = self.zone;
        
        if (xmlAnalysisCurrentValue_ == dxeas_AnalyzingAccountNumber) {
            
            [accountNumber_ release];
            accountNumber_ = nil;
            accountNumber_ = [elementString copyWithZone:zone];
            
        } else if (xmlAnalysisCurrentValue_ == dxeas_AnalyzingCurrentBalance) {
            
            [currentBalanceString_ release];
            currentBalanceString_ = nil;
            currentBalanceString_ = [elementString copyWithZone:zone];
            
        } else if (xmlAnalysisCurrentValue_ == dxeas_AnalyzingAvailableBalance) {
            
            [availableBalanceString_ release];
            availableBalanceString_ = nil;
            availableBalanceString_ = [elementString copyWithZone:zone];
            
        } else if (xmlAnalysisCurrentValue_ == dxeas_AnalyzingCurrency) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:zone];
            
        } else if (xmlAnalysisCurrentValue_ == dxeas_AnalyzingSubject) {
            
            [subject_ release];
            subject_ = nil;
            subject_ = [elementString copyWithZone:zone];
            
        } else if (xmlAnalysisCurrentValue_ == dxeas_AnalyzingBalanceInCountryCurrency) {
            
            [balanceInCountryCurrencyString_ release];
            balanceInCountryCurrencyString_ = nil;
            balanceInCountryCurrencyString_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == dxeas_AnalyzingCurrencyAvailable) {
            
            [currencyAvailable_ release];
            currencyAvailable_ = nil;
            currencyAvailable_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == dxeas_AnalyzingCTSIndicator) {
            
            [ctsIndicator_ release];
            ctsIndicator_ = nil;
            ctsIndicator_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == dxeas_AnalyzingGroupingCode) {
            
            [groupingCode_ release];
            groupingCode_ = nil;
            groupingCode_ = (elementString != nil) ? [elementString copyWithZone:self.zone] : [@"" retain];
            
        }
         
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the current balance, calculating it from the string representation in case it is necessary
 *
 * @return The current balance
 */
- (NSDecimalNumber *)currentBalance {
    
    NSDecimalNumber *result = currentBalance_;
    
    if (result == nil) {
        
        if ([currentBalanceString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:currentBalanceString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
				[currentBalance_ release];
                currentBalance_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the available balance, calculating it from the string representation in case it is necessary
 *
 * @return The available balance
 */
- (NSDecimalNumber *)availableBalance {
    
    NSDecimalNumber *result = availableBalance_;
    
    if (result == nil) {
        
        if ([availableBalanceString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:availableBalanceString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
				[availableBalance_ release];
                availableBalance_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the balance converted into the country currency, calculating ir from string representation if needed
 *
 * @return The balance converted into country curency
 */
- (NSDecimalNumber *)balanceInCountryCurrency {
    
    NSDecimalNumber *result = balanceInCountryCurrency_;
    
    if (result == nil) {
        
        if ([balanceInCountryCurrencyString_ length] == 0) {
            
            if ([availableBalanceString_ length] > 0) {
            
                result = [Tools decimalFromServerString:availableBalanceString_];
                
            } else {
                
                result = [NSDecimalNumber notANumber];

            }
            
        } else {
            
            result = [Tools decimalFromServerString:balanceInCountryCurrencyString_];
            
        }
        
        if (![result compare:[NSDecimalNumber notANumber]]) {
            
			[balanceInCountryCurrency_ release];
            balanceInCountryCurrency_ = [result retain];
            
        }
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation Deposit(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearDepositData {
    
    [accountNumber_ release];
    accountNumber_ = nil;
    
    [currentBalanceString_ release];
    currentBalanceString_ = nil;
    
    [currentBalance_ release];
    currentBalance_ = nil;
    
    [availableBalanceString_ release];
    availableBalanceString_ = nil;
    
    [availableBalance_ release];
    availableBalance_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [subject_ release];
    subject_ = nil;
    
    [balanceInCountryCurrencyString_ release];
    balanceInCountryCurrencyString_ = nil;
    
    [balanceInCountryCurrency_ release];
    balanceInCountryCurrency_ = nil;  
    
    [currencyAvailable_ release];
    currencyAvailable_ = nil;
    
    [ctsIndicator_ release];
    ctsIndicator_ = nil;
    
    [groupingCode_ release];
    groupingCode_ = nil;
    
}

@end
