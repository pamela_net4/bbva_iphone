/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "StatusEnabledResponse.h"


/**
 * Transfer success response additional information information
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferSuccessAdditionalInformation : StatusEnabledResponse {
    
@private
    /**
     * Operation number
     */
    NSString *operationState_;
    /**
     * Operation number
     */
    NSString *operationNumber_;
    
    /**
     * Operation
     */
    NSString *operation_;
    
    /**
     * Holder
     */
    NSString *holder_;
    
    /**
     * Amount charged string
     */
    NSString *amountChargedString_;
	
	/**
     * Amount charged currency string
     */
	NSString *amountChargedCurrencyString_;
    
    /**
     * Amount charged
     */
    NSDecimalNumber *amountCharged_;
    
    /**
     * Amount paid string
     */
    NSString *amountPaidString_;
    
    /**
     * Amount paid
     */
    NSDecimalNumber *amountPaid_;
    
	/**
     * Amount paid string
     */
    NSString *amountPaidCurrencyString_;
	
    /**
     * Exchange rate
     */
    NSString *exchangeRate_;
    
    /**
     * Transfer date string
     */
    NSString *dateString_;
    
    /**
     * Transfer date
     */
    NSDate *date_;
	
	/**
     * Transfer hour
     */
    NSString *hour_;
    
    /**
     * Transfer expiration date
     */
    NSString *expirationDate_;
	
	/**
     * Transfer expiration hour
     */
    NSString *expirationHour_;
	
	/**
     * Transfer currency exchange rate
     */
    NSString *currencyExchangeRate_;
    
	/**
     * Transfer commission
     */
    NSString *commission_;
	
	/**
     * Transfer currency commission
     */
    NSString *currencyCommission_;
	
	/**
     * Transfer network use
     */
    NSString *networkUse_;
	
	/**
     * Transfer currency network use
     */
    NSString *currencyNetworkUse_;
	
	/**
     * Transfer amount to pay
     */
    NSString *amountToPay_;
	
	/**
     * Transfer currency amount to pay
     */
    NSString *currencyAmountToPay_;
    
	/**
     * Transfer amount to pay
     */
    NSString *totalAmountToPay_;
	
	/**
     * Transfer currency total amount to pay
     */
    NSString *currencyTotalAmountToPay_;
    
    /**
     * Transfer itf currency
     */
    NSString *currencyItf_;
    
    /**
     * Transfer itf amount
     */
    NSString *itfAmount_;
    
	/**
     * Transfer itf message
     */
    NSString *itfMessage_;
    
	/**
     * Transfer confirmation message
     */
    NSString *confirmationMessage_;
	
	/**
	 * Market place commision
	 */
	NSString *marketPlaceCommisionString_;
	
	/**
     * Market place commision
     */
    NSDecimalNumber *marketPlaceCommision_;
	
	/**
	 * Market place commision currency
	 */
	NSString *marketPlaceCommisionCurrencyString_;
	
	/**
	 * Beneficiary document
	 */
	NSString *beneficiaryDocument_;
    
    /**
     * Beneficiary document
     */
    NSString *beneficiaryDocumentType_;
    
    /**
     * Beneficiary document
     */
    NSString *beneficiaryDocumentTypeId_;

    
    /**
	 * Transfer State
	 */
	NSString *transferState_;
	
    /**
	 * Notification Message
	 */
	NSString *notificationMessage_;
    
    NSString *amountToTransfer_;
    
}


/**
 * Provides read-only access to the operation number
 */
@property (nonatomic, readonly, copy) NSString *operationState;
/**
 * Provides read-only access to the operation number
 */
@property (nonatomic, readonly, copy) NSString *operationNumber;

/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to the holder
 */
@property (nonatomic, readonly, copy) NSString *holder;

/**
 * Provides read-only access to the amount charged string
 */
@property (nonatomic, readonly, copy) NSString *amountChargedString;

/**
 * Provides read-only access to the amount charged string
 */
@property (nonatomic, readonly, copy) NSString *amountChargedCurrencyString;

/**
 * Provides read-only access to the amount charged
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *amountCharged;

/**
 * Provides read-only access to the amount paid string
 */
@property (nonatomic, readonly, copy) NSString *amountPaidString;

/**
 * Provides read-only access to the amount paid
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *amountPaid;

/**
 * Provides read-only access to the amount paid currency string
 */
@property (nonatomic, readonly, copy) NSString *amountPaidCurrencyString;

/**
 * Provides read-only access to the exchange rate
 */
@property (nonatomic, readonly, copy) NSString *exchangeRate;

/**
 * Provides read-only access to the transfer date string
 */
@property (nonatomic, readonly, copy) NSString *dateString;

/**
 * Provides read-only access to the transfer expiration date string
 */
@property (nonatomic, readonly, copy) NSString *expirationDateString;

/**
 * Provides read-only access to the transfer expiration hour string
 */
@property (nonatomic, readonly, copy) NSString *expirationHourString;


/**
 * Provides read-only access to the transfer date
 */
@property (nonatomic, readonly, copy) NSDate *date;

/**
 * Provides read-only access to the transfer hour string
 */
@property (nonatomic, readonly, copy) NSString *hour;

/**
 * Provides read-only access to the transfer currencyExchangeRate string
 */
@property (nonatomic, readonly, copy) NSString *currencyExchangeRate;

/**
 * Provides read-only access to the transfer commission string
 */
@property (nonatomic, readonly, copy) NSString *commission;

/**
 * Provides read-only access to the transfer currencyCommission string
 */
@property (nonatomic, readonly, copy) NSString *currencyCommission;

/**
 * Provides read-only access to the transfer networkUse string
 */
@property (nonatomic, readonly, copy) NSString *networkUse;

/**
 * Provides read-only access to the transfer currencyNetworkUse string
 */
@property (nonatomic, readonly, copy) NSString *currencyNetworkUse;

/**
 * Provides read-only access to the transfer amountToPay string
 */
@property (nonatomic, readonly, copy) NSString *amountToPay;

/**
 * Provides read-only access to the transfer amountToTransfer string
 */
@property (nonatomic, readonly, copy) NSString *amountToTransfer;
/**
 * Provides read-only access to the transfer currencyAmountToPay string
 */
@property (nonatomic, readonly, copy) NSString *currencyAmountToPay;

/**
 * Provides read-only access to the transfer totalAmountToPay string
 */
@property (nonatomic, readonly, copy) NSString *totalAmountToPay;

/**
 * Provides read-only access to the transfer currencyTotalAmountToPay string
 */
@property (nonatomic, readonly, copy) NSString *currencyTotalAmountToPay;

/**
 * Provides read-only access to the transfer currencyItf string
 */
@property (nonatomic, readonly, copy) NSString *currencyItf;

/**
 * Provides read-only access to the transfer itfAmount string
 */
@property (nonatomic, readonly, copy) NSString *itfAmount;

/**
 * Provides read-only access to the transfer itfMessage string
 */
@property (nonatomic, readonly, copy) NSString *itfMessage;

/**
 * Provides read-only access to the transfer confirmationMessage string
 */
@property (nonatomic, readonly, copy) NSString *confirmationMessage;

/**
 * Provides read-only access to the transfer marketPlaceCommisionString string
 */
@property (nonatomic, readonly, copy) NSString *marketPlaceCommisionString;

/**
 * Provides read-only access to the transfer marketPlaceCommision charged
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *marketPlaceCommision;

/**
 * Provides read-only access to the transfer marketPlaceCommisionCurrency string
 */
@property (nonatomic, readonly, copy) NSString *marketPlaceCommisionCurrencyString;

/**
 * Provides read-only access to beneficiary document
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryDocument;



/**
 * Provides read-only access to beneficiary document
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryDocumentType;

/**
 * Provides read-only access to beneficiary document
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryDocumentTypeId;

/**
 * Provides read-only access to transfer state
 */
@property (nonatomic, readonly, copy) NSString *transferState;

/**
 * Provides read-only access to notification message
 */
@property (nonatomic, readonly, copy) NSString *notificationMessage;

@end
