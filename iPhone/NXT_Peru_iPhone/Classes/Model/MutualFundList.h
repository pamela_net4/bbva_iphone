/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class MutualFund;


/**
 * Contains the list of mutual funds obtained from the server
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MutualFundList : StatusEnabledResponse {

@private
	
	/**
	 * Mutual funds array list
	 */
	NSMutableArray *mutualFundsList_;
    
	/**
	 * Auxiliar mutual fund instance for parsing
	 */
	MutualFund *auxMutualFund_;
    
}

/**
 * Provides read-only access to the mutual funds array list
 */
@property (nonatomic, readonly, retain) NSArray *mutualFundsList;

/**
 * Provides read access to the number of mutual fund stored
 */
@property (nonatomic, readonly, assign) NSUInteger mutualFundCount;


/**
 * Returns the Mutual fund located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the Mutual fund is located
 * @return Mutual fund located at the given position, or nil if position is not valid
 */
- (MutualFund *)mutualFundAtPosition:(NSUInteger)aPosition;

/**
 * Returns the mutual fund with a mutual fund Number, or nil if not valid
 *
 * @param aMutualFundNumber The mutual fund number
 * @return The mutual fund with this number, or nil if not valid
 */
- (MutualFund *)mutualFundFromMutualFundNumber:(NSString *)aMutualFundNumber;

/**
 * Returns the Mutual fund position inside the list
 *
 * @param  aMutualFund Mutual fund object which position we are looking for
 * @return Mutual fund position inside the list
 */
- (NSUInteger)mutualFundPosition:(MutualFund *)aMutualFund;


/**
 * Updates the mutual funds list from another mutual funds list
 *
 * @param aMutualFundList The MutualFundList instance to update from
 */
- (void)updateFrom:(MutualFundList *)aMutualFundList;

/**
 * Remove the contained data
 */
- (void)removeData;

@end
