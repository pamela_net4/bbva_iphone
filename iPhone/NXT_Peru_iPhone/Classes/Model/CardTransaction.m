/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "CardTransaction.h"
#import "Tools.h"


/**
 * Enumerates the analysis states
 */
typedef enum {
    
    ctxeas_AnalyzingOperationDate = serxeas_ElementsCount, //!<Analyzing the transaction operation date
    ctxeas_AnalyzingOperationType, //!<Analyzing the transaction operation type
	ctxeas_AnalyzingUse, //!<Analyzing the transaction use
	ctxeas_AnalyzingConcept, //!<Analyzing the transaction concept
	ctxeas_AnalyzingOperationsML, //!<Analyzing the transaction operations ML
	ctxeas_AnalyzingOperationsME, //!<Analyzing the transaction operations ME
	ctxeas_AnalyzingCurrency, //!<Analyzing the currency
	ctxeas_AnalyzingAmount, //!<Analyzing the transaction amount
	ctxeas_AnalyzingSubject //!<Analyzing the subject
    
} CardTransactionXMLElementAnalyzerState;


#pragma mark -

/**
 * CardTransaction private extension
 */
@interface CardTransaction()

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearCardTransactionData;

@end


#pragma mark -

@implementation CardTransaction

#pragma mark -
#pragma mark Properties

@synthesize operationDateString = operationDateString_;
@dynamic operationDate;
@synthesize operationType = operationType_;
@synthesize use = use_;
@synthesize concept = concept_;
@synthesize operationsMLString = operationsMLString_;
@dynamic operationsML;
@synthesize operationsMEString = operationsMEString_;
@dynamic operationsME;
@synthesize currency = currency_;
@synthesize amount = amount_;
@synthesize subject = subject_;
@synthesize amountString = amountString_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearCardTransactionData];
	
	[super dealloc];
    
}

/*
 * Clears the object data
 */
- (void)clearCardTransactionData {
    
    [operationDateString_ release];
    operationDateString_ = nil;
    
    [operationDate_ release];
    operationDate_ = nil;
    
    [operationType_ release];
    operationType_ = nil;
    
    [use_ release];
    use_ = nil;
    
    [concept_ release];
    concept_ = nil;
    
    [operationsMLString_ release];
    operationsMLString_ = nil;
    
    [operationsML_ release];
    operationsML_ = nil;
    
    [operationsMEString_ release];
    operationsMEString_ = nil;
    
    [operationsME_ release];
    operationsME_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [amountString_ release];
    amountString_ = nil;
    
    [amount_ release];
    amount_ = nil;
    
    [subject_ release];
    subject_ = nil;
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initialized. Initializes a CardTransaction instance
 *
 * @return The initialized CardTransaction instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationCardTransactionUpdated;
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates this instance with the given card transaction
 */
- (void)updateFrom:(CardTransaction *)aCardTransaction {
    
    NSZone *zone = self.zone;
    
    [operationDateString_ release];
    operationDateString_ = nil;
    operationDateString_ = [aCardTransaction.operationDateString copyWithZone:zone];
    
    [operationDate_ release];
    operationDate_ = nil;
    
    [operationType_ release];
    operationType_ = nil;
    operationType_ = [aCardTransaction.operationType copyWithZone:zone];
    
    [use_ release];
    use_ = nil;
    use_ = [aCardTransaction.use copyWithZone:zone];
    
    [concept_ release];
    concept_ = nil;
    concept_ = [aCardTransaction.concept copyWithZone:zone];
    
    [operationsMLString_ release];
    operationsMLString_ = nil;
    operationsMLString_ = [aCardTransaction.operationsMLString copyWithZone:zone];
    
    [operationsML_ release];
    operationsML_ = nil;
    
    [operationsMEString_ release];
    operationsMEString_ = nil;
    operationsMEString_ = [aCardTransaction.operationsMEString copyWithZone:zone];
    
    [operationsME_ release];
    operationsME_ = nil;
    
    [currency_ release];
    currency_ = nil;
    currency_ = [aCardTransaction.currency copyWithZone:zone];
    
    [amountString_ release];
    amountString_ = nil;
    amountString_ = [aCardTransaction.amountString copyWithZone:zone];
    
    [amount_ release];
    amount_ = nil;
    
    [subject_ release];
    subject_ = nil;
    subject_ = [aCardTransaction.subject copyWithZone:zone];
    
    [self updateFromStatusEnabledResponse:aCardTransaction];
	
	self.informationUpdated = aCardTransaction.informationUpdated;
    
}

#pragma mark -
#pragma mark Comparing selectors

/*
 * Compares the card transaction with another to produce an ordered list where older transactions are the last items
 */
- (NSComparisonResult)compareForDescendingDateOrdering:(CardTransaction *)aCardTransaction {
    
    NSComparisonResult result = NSOrderedAscending;
    
    if (aCardTransaction != nil) {
        
        NSDate *otherDate = aCardTransaction.operationDate;
        NSDate *date = self.operationDate;
        
        result = [date compare:otherDate];
        
        if (result == NSOrderedAscending) {
            
            result = NSOrderedDescending;
            
        } else if (result == NSOrderedDescending) {
            
            result = NSOrderedAscending;
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearCardTransactionData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"fechaoperacion"]) {
            
            xmlAnalysisCurrentValue_ = ctxeas_AnalyzingOperationDate;
            
        } else if ([lname isEqualToString: @"tipooperacion"]) {
            
            xmlAnalysisCurrentValue_ = ctxeas_AnalyzingOperationType;
            
        } else if ([lname isEqualToString: @"uso"]) {
            
            xmlAnalysisCurrentValue_ = ctxeas_AnalyzingUse;
            
        } else if ([lname isEqualToString: @"concepto"]) {
            
            xmlAnalysisCurrentValue_ = ctxeas_AnalyzingConcept;
            
        } else if ([lname isEqualToString: @"operacionesml"]) {
            
            xmlAnalysisCurrentValue_ = ctxeas_AnalyzingOperationsML;
            
        } else if ([lname isEqualToString: @"operacionesme"]) {
            
            xmlAnalysisCurrentValue_ = ctxeas_AnalyzingOperationsME;
            
        } else if ([lname isEqualToString: @"divisa"]) {
            
            xmlAnalysisCurrentValue_ = ctxeas_AnalyzingCurrency;
            
        } else if ([lname isEqualToString: @"importe"]) {
            
            xmlAnalysisCurrentValue_ = ctxeas_AnalyzingAmount;
            

        } else if ([lname isEqualToString: @"asunto"]) {
            
            xmlAnalysisCurrentValue_ = ctxeas_AnalyzingSubject;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == ctxeas_AnalyzingOperationDate) {
            
            [operationDateString_ release];
            operationDateString_ = nil;
            operationDateString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctxeas_AnalyzingOperationType) {
            
            [operationType_ release];
            operationType_ = nil;
            operationType_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctxeas_AnalyzingUse) {
            
            [use_ release];
            use_ = nil;
            use_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctxeas_AnalyzingConcept) {
            
            [concept_ release];
            concept_ = nil;
            concept_ = [[elementString capitalizedString] copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctxeas_AnalyzingOperationsML) {
            
            [operationsMLString_ release];
            operationsMLString_ = nil;
            operationsMLString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctxeas_AnalyzingOperationsME) {
            
            [operationsMEString_ release];
            operationsMEString_ = nil;
            operationsMEString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctxeas_AnalyzingCurrency) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctxeas_AnalyzingAmount) {
            
            [amountString_ release];
            amountString_ = nil;
            amountString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ctxeas_AnalyzingSubject) {
            
            [subject_ release];
            subject_ = nil;
            subject_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the operation date
 *
 * @return The operation date
 */
- (NSDate *)operationDate{
    
    NSDate *result = operationDate_;
    
    if (result == nil) {
        
        operationDate_ = [[Tools dateFromServerString:operationDateString_] retain];
        result = operationDate_;
        
    }
    
    return result;
    
}
/*
 * Returns the account transaction amount
 *
 * @return The account transaction amount
 */
- (NSDecimalNumber *)amount {
    
    NSDecimalNumber *result = amount_;
    
    if (result == nil) {
        
        if ([amountString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:amountString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
				[amount_ release];
                amount_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the operations ML
 *
 * @return The operations ML
 */
- (NSDecimalNumber *)operationsML {
    
    NSDecimalNumber *result = operationsML_;
    
    if (result == nil) {
        
        if ([operationsMLString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:operationsMLString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
				[operationsML_ release];
                operationsML_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the operations ME
 *
 * @return The operations ME
 */
- (NSDecimalNumber *)operationsME {
    
    NSDecimalNumber *result = operationsME_;
    
    if (result == nil) {
        
        if ([operationsMEString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:operationsMEString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
				[operationsME_ release];
                operationsME_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

@end
