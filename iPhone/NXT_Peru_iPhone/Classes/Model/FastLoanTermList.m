/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "FastLoanTermList.h"
#import "FastLoanTerm.h"


#pragma mark -

/**
 * FastLoanTermList private category
 */
@interface FastLoanTermList (private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFastLoanTermListData;

@end


#pragma mark -

@implementation FastLoanTermList

@dynamic fastloantermList;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [self clearFastLoanTermListData];

	[fastloantermList_ release];
	fastloantermList_ = nil;

	[super dealloc];

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a FastLoanTermList instance creating the associated array
 *
 * @return The initialized FastLoanTermList instance
 */
- (id)init {

    if (self = [super init]) {

        fastloantermList_ = [[NSMutableArray alloc] init];

    }

    return self;

}

#pragma mark -
#pragma mark Information management

/*
 * Updates the FastLoanTerm list from another FastLoanTermList instance
 */
- (void)updateFrom:(FastLoanTermList *)aFastLoanTermList {

    NSArray *otherFastLoanTermList = aFastLoanTermList.fastloantermList;

	[self clearFastLoanTermListData];

    for (FastLoanTerm *fastloanterm in otherFastLoanTermList) {

        [fastloantermList_ addObject:fastloanterm];

    }

    [self updateFromStatusEnabledResponse:aFastLoanTermList];

	self.informationUpdated = aFastLoanTermList.informationUpdated;

}

/*
 * Remove object data
 */
- (void)removeData {

    [self clearFastLoanTermListData];

}

#pragma mark -
#pragma mark Getters and setters

/**
 * Provides read access to the number of FastLoanTerm stored
 *
 * @return The count
 */
- (NSUInteger) fastloantermCount {
	return [fastloantermList_ count];
}

/*
 * Returns the FastLoanTerm located at the given position, or nil if position is not valid
 */
- (FastLoanTerm*) fastloantermAtPosition: (NSUInteger) aPosition {
	FastLoanTerm* result = nil;

	if (aPosition < [fastloantermList_ count]) {
		result = [fastloantermList_ objectAtIndex: aPosition];
	}

	return result;
}

/*
 * Returns the FastLoanTerm position inside the list
 */
- (NSUInteger) fastloantermPosition: (FastLoanTerm*) aFastLoanTerm {

	return [fastloantermList_ indexOfObject:aFastLoanTerm];

}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {

    [super parserDidStartDocument:parser];

	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFastLoanTermListData];

}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {

    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];

	if (auxFastLoanTerm_ != nil) {

		[fastloantermList_ addObject:auxFastLoanTerm_];
		[auxFastLoanTerm_ release];
		auxFastLoanTerm_ = nil;

	}

	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        NSString* lname = [elementName lowercaseString];

        if ([lname isEqualToString: @"e"]) {

            auxFastLoanTerm_ = [[FastLoanTerm alloc] init];
            [auxFastLoanTerm_ setParentParseableObject:self];
            auxFastLoanTerm_.openingTag = lname;
            [parser setDelegate:auxFastLoanTerm_];
            [auxFastLoanTerm_ parserDidStartDocument: parser];

        } else {

            xmlAnalysisCurrentValue_ = serxeas_Nothing;

        }

    }

}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {

    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];

    if (auxFastLoanTerm_ != nil) {

		[fastloantermList_ addObject:auxFastLoanTerm_];
		[auxFastLoanTerm_ release];
		auxFastLoanTerm_ = nil;

	}

    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        informationUpdated_ = soie_InfoAvailable;

    }

    xmlAnalysisCurrentValue_ = serxeas_Nothing;

}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the fastloanterm list array
 *
 * @return The fastloanterm list array
 */
- (NSArray *)fastloantermList {

    return [NSArray arrayWithArray:fastloantermList_];

}

/**
 * Returns the FastLoanTerm with an FastLoanTerm id, or nil if not valid
 *//*
- (FastLoanTerm *)fastloantermFromFastLoanTermId:(NSString *)aFastLoanTermId {

	FastLoanTerm *fastloanterm = nil;

	if (aFastLoanTermId != nil) {
		FastLoanTerm *target;
		NSUInteger size = [fastloantermList_ count];
		BOOL found = NO;
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
			target = [fastloantermList_ objectAtIndex:i];
			if ([aFastLoanTermId isEqualToString:target.id]) {
				fastloanterm = target;
				found = YES;
			}
		}
	}

	return fastloanterm;

}*/
@end


#pragma mark -

@implementation FastLoanTermList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFastLoanTermListData {

    [fastloantermList_ removeAllObjects];

	informationUpdated_ = soie_NoInfo;

}

@end

