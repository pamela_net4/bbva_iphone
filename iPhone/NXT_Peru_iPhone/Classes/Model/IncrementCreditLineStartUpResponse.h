//
//  IncrementCreditLineStartUpResponse.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
@class EmailInfoList;
@class CardForIncrementList;


@interface IncrementCreditLineStartUpResponse : StatusEnabledResponse{
    @private

   /**
     * IncrementCreditLineStartUpResponse amountOffer
     */
    NSString *amountOffer_;


   /**
     * IncrementCreditLineStartUpResponse emailInfoList
     */
    EmailInfoList *emailInfoList_;


   /**
     * IncrementCreditLineStartUpResponse cardForIncrementList
     */
    CardForIncrementList *cardForIncrementList_;




}

/**
 * Provides read-only access to the IncrementCreditLineStartUpResponse amountOffer
 */
@property (nonatomic, readonly, copy) NSString * amountOffer;


/**
 * Provides read-only access to the IncrementCreditLineStartUpResponse emailInfoList
 */
@property (nonatomic, readonly, copy) EmailInfoList * emailInfoList;


/**
 * Provides read-only access to the IncrementCreditLineStartUpResponse cardForIncrementList
 */
@property (nonatomic, readonly, copy) CardForIncrementList * cardForIncrementList;




@end

