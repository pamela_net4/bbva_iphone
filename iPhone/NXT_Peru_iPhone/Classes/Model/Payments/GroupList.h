//
//  GroupList.h
//  NXT_Peru_iPad
//
//  Created by Ricardo on 11/11/13.
//
//

#import "StructuredXMLParser.h"

/**
 * Defines the GroupList element tag
 */
extern NSString * const kGroupListElement;

/**
 * Analyzes a group list
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface GroupList : StructuredXMLParser

/**
 * Provides read-only access to the group list
 */
@property (nonatomic, readwrite, retain) NSArray *groupList;

@end
