/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PaymentCCOtherBanksInitialResponse.h"

#import "CardTypeList.h"

/**
 * Defines the additional information element tag
 */
NSString * const kPaymentCCOtherBanksInitialResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";

/**
 * Defines the msg-s element tag
 */
NSString * const kPaymentCCOtherBanksInitialResponseMsgElement = @"MSG-S";

/**
 * Defines the card list element tag
 */
NSString * const kPaymentCCOtherBanksInitialResponseCardTypeListElement = @"TIPOTARJETA";


#pragma mark -
#pragma mark Properties

@implementation PaymentCCOtherBanksInitialResponse

@dynamic cardTypeList;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPaymentCCOtherBanksInitialResponseMsgElement]) ||
            ([element isEqualToString:kPaymentCCOtherBanksInitialResponseAdditionalInfoElement])){
            
            result = BXPOElementIdle;
        
        } else if ([element isEqualToString:kPaymentCCOtherBanksInitialResponseCardTypeListElement]) {
            
            result = BXPOElementCustom;
            
        }
    }
    
    return result;
    
}

/**
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Returns the corresponing parser for the transaction list and nil for the rest
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element parser
 */
- (StructuredXMLParser *)parserForElement:(NSString *)element
                             namespaceURI:(NSString *)namespaceURI {
    
    StructuredXMLParser *result = [super parserForElement:element
                                             namespaceURI:namespaceURI];
    
    if (result == nil) {
        
        if ([element isEqualToString:kPaymentCCOtherBanksInitialResponseCardTypeListElement]) {
            
            result = [[[CardTypeList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                    ownNamespacesPrefixesDictionary:nil] autorelease];
            
        }
        
    }
    
    return result;
    
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the card type list
 *
 * @return The card type list
 */
- (CardTypeList *)cardTypeList {
    
    CardTypeList *result = (CardTypeList *)[self objectForElement:kPaymentCCOtherBanksInitialResponseCardTypeListElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

@end
