/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "Carrier.h"

#import "StringKeys.h"
#import "Tools.h"


/*
 * Defines the Movistar carrier code (in lower case)
 */
NSString * const kCarrierMovistarCode = @"movi";

/*
 * Defines the Claro carrier code (in lower case)
 */
NSString * const kCarrierClaroCode = @"clar";


/*
 * Defines the Movistar carrier code for notifications.
 */
NSString * const kCarrierMovistarCodeForNotification = @"TELE";

/*
 * Defines the Claro carrier code for notifications.
 */
NSString * const kCarrierClaroCodeForNotification = @"CLAR";

/*
 * Defines the Nextel carrier code for notifications.
 */
NSString * const kCarrierNextelCodeForNotification = @"NEXT";


/**
 * Defines the carrier element tag
 */
NSString * const kCarrierCarrierElement = @"OPERADOR";

/**
 * Defines the carrier element tag
 */
NSString * const kCarrierCodeElement = @"CODIGO";

/**
 * Defines the max element tag
 */
NSString * const kCarrierMaxElement = @"MAXIMO";

/**
 * Defines the max element tag
 */
NSString * const kCarrierMinElement = @"MINIMO";

/**
 * Defines the max element tag
 */
NSString * const kCarrierMessageElement = @"MENSAJERANGO";

/**
 * Defines the max element tag
 */
NSString * const kCarrierDescriptionElement = @"DESCRIPCION";


#pragma mark -


@implementation Carrier

#pragma mark -
#pragma mark Properties

@dynamic carrier;
@dynamic carrierCode;
@dynamic max;
@dynamic min;
@dynamic message;
@dynamic carrierDisplayName;
@dynamic description;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kCarrierCarrierElement]) ||
            ([element isEqualToString:kCarrierCodeElement]) ||
            ([element isEqualToString:kCarrierMaxElement]) ||
            ([element isEqualToString:kCarrierMinElement]) ||
            ([element isEqualToString:kCarrierDescriptionElement]) ||
            ([element isEqualToString:kCarrierMessageElement])){
            
            result = BXPOElementString;
        
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the carrier
 *
 * @return The carrier
 */
- (NSString *)carrier {
    
    NSString *result = (NSString *)[self objectForElement:kCarrierCarrierElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the carrier code
 *
 * @return The carrier code
 */
- (NSString *)carrierCode {
    
    NSString *result = (NSString *)[self objectForElement:kCarrierCodeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the max 
 *
 * @return The max 
 */
- (NSDecimalNumber *)max {
    
    NSDecimalNumber *result;
    
    NSString *value = (NSString *)[self objectForElement:kCarrierMaxElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    result = [Tools decimalFromServerString:value];
    
    return result;
    
}

/*
 * Returns the min 
 *
 * @return The min 
 */
- (NSDecimalNumber *)min {
    
    NSDecimalNumber *result;
    
    NSString *value = (NSString *)[self objectForElement:kCarrierMinElement
                                            namespaceURI:@""
                                                 atIndex:0];
    
    result = [Tools decimalFromServerString:value];
    
    return result;
    
}

/*
 * Returns the message
 *
 * @return The message
 */
- (NSString *)message {
    
    NSString *result = (NSString *)[self objectForElement:kCarrierMessageElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the description
 *
 * @return The description
 */
- (NSString *)description {
    
    NSString *result = (NSString *)[self objectForElement:kCarrierDescriptionElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the carrier display name.
 *
 * @return The carrier display name.
 */
- (NSString *)carrierDisplayName {
    
    NSString *result = nil;
    
    NSString *carrierCode = [[self carrier] lowercaseString];
    
    if ([kCarrierClaroCode isEqualToString:carrierCode]) {
        
        result = NSLocalizedString(CLARO_CARRIER_TEXT_KEY, nil);
        
    } else if ([kCarrierMovistarCode isEqualToString:carrierCode]) {
        
        result = NSLocalizedString(MOVISTAR_CARRIER_TEXT_KEY, nil);
        
    }
    
    return result;
    
}

@end
