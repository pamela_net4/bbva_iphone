//
//  PaymentInstitutionAndCompaniesSuccessConfirmationResponse.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/27/13.
//
//

#import "StructuredStatusEnabledResponse.h"

@class OcurrencyList;
@class PendingDocumentList;

/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseAdditionalInfoElement;

/**
 * Defines the msg-s element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseMsgElement;

/**
 * Defines the pay type element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponsePayTypeElement;

/**
 * Defines the pay type element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseOperationNumberElement;

/**
 * Defines the pay amount element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseDateElement;

/**
 * Defines the pay amount currency element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseTimeElement;

/**
 * Defines the operation element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseOperationElement;

/**
 * Defines the account type element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseAccountTypeElement;

/**
 * Defines the currency element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseCurrencyElement;

/**
 * Defines the subject account element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseSubjectElement;

/**
 * Defines the Companie Name element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseCompanieNameElement;

/**
 * Defines the Ocurrency list element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseOcurrencyListElement;

/**
 * Defines the holder Name element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseHolderName;

/**
 * Defines the Doc Number element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseDocumentNumber;

/**
 * Defines the Document List element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseDocumentList;

/**
 * Defines the Coordinates element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseChangeType;

/**
 * Defines the Coordinates element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseChangeTypeCurrency;

/**
 * Defines the Seal element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseAmountPayed;

/**
 * Defines the Disclaimer element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseAmountPayedCurrency;

/**
 * Defines the Seal element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseAmountCustomPayed;

/**
 * Defines the Disclaimer element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseAmountCustomPayedCurrency;

/**
 * Defines the Disclaimer element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseChargedAmount;

/**
 * Defines the Disclaimer element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseChargedAmountCurrency;

/**
 * Defines the Disclaimer element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseMessage;
/**
 * Defines the Schedule ampliation element tag
 */
extern NSString * const kPaymentInstitutionsSuccessResponseScheduleAmpl;


/**
 * The payment Institution success confirmation response.
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface PaymentInstitutionAndCompaniesSuccessConfirmationResponse : StructuredStatusEnabledResponse

/**
 * Provides read-only access to the Pay type
 */
@property (nonatomic, readonly, retain) NSString *payType;

/**
 * Provides read-only access to the Pay type code 
 */
@property (nonatomic, readonly, retain) NSString *payTypeCode;

/**
 * Provides read-only access to the Pay amount
 */
@property (nonatomic, readonly, retain) NSString *operationNumber;

/**
 * Provides read-only access to the Pay amount currency
 */
@property (nonatomic, readonly, retain) NSString *date;

/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, retain) NSString *time;

/**
 * Provides read-only access to the Account type
 */
@property (nonatomic, readonly, retain) NSString *operation;

/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, retain) NSString *accountType;

/**
 * Provides read-only access to the subject
 */
@property (nonatomic, readonly, retain) NSString *currency;

/**
 * Provides read-only access to the companie Name
 */
@property (nonatomic, readonly, retain) NSString *subject;

/**
 * Provides read-only access to the companie Name
 */
@property (nonatomic, readonly, retain) NSString *companieName;

/**
 * Provides read-only access to the ocurrency list
 */
@property (nonatomic, readonly, retain) OcurrencyList *ocurrencyList;

/**
 * Provides read-only access to the holder Name
 */
@property (nonatomic, readonly, retain) NSString *holderName;

/**
 * Provides read-only access to the doc number
 */
@property (nonatomic, readonly, retain) NSString *docNumber;

/**
 * Provides read-only access to the Pay type
 */
@property (nonatomic, readonly, retain) PendingDocumentList *docList;

/**
 * Provides read-only access to the Coordinate
 */
@property (nonatomic, readonly, retain) NSString *changeType;

/**
 * Provides read-only access to the Seal
 */
@property (nonatomic, readonly, retain) NSString *changeTypeCurrency;

/**
 * Provides read-only access to the Disclaimer
 */
@property (nonatomic, readonly, retain) NSString *payedAmount;

/**
 * Provides read-only access to the Disclaimer
 */
@property (nonatomic, readonly, retain) NSString *payedAmountCurrency;

/**
 * Provides read-only access to the Disclaimer
 */
@property (nonatomic, readonly, retain) NSString *chargedAmount;

/**
 * Provides read-only access to the Disclaimer
 */
@property (nonatomic, readonly, retain) NSString *chargedAmountCurrency;

/**
 * Provides read-only access to the Disclaimer
 */
@property (nonatomic, readonly, retain) NSString *message;

/**
 * Provides read-only access to the Schedule ampliation
 */
@property (nonatomic, readonly, retain) NSString *scheduleAmpl;

@end
