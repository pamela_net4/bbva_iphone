/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "StructuredXMLParser.h"

/**
 * Defines the CardTypeList element tag
 */
extern NSString * const kCarrierListElement;

/**
 * Analyzes a payment list
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CarrierList : StructuredXMLParser


/**
 * Provides read-only access to the carrier list
 */
@property (nonatomic, readwrite, retain) NSArray *carrierList;

@end
