/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "ServiceResponse.h"

#import "ServiceList.h"

/**
 * Defines the additional information element tag
 */
NSString * const kServiceResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";

/**
 * Defines the msg element tag
 */
NSString * const kServiceResponseMsgElement = @"MSG-S";

/**
 * Defines the msg element tag
 */
NSString * const kServiceResponseServicesListElement = @"LISTASERVICIOS";

#pragma mark -
#pragma mark Properties

@implementation ServiceResponse

@dynamic serviceList;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kServiceResponseMsgElement]) ||
            ([element isEqualToString:kServiceResponseAdditionalInfoElement])){
            
            result = BXPOElementIdle;
        
        } else if ([element isEqualToString:kServiceResponseServicesListElement]) {
            
            result = BXPOElementCustom;
            
        }
    }
    
    return result;
    
}

/**
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Returns the corresponing parser for the transaction list and nil for the rest
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element parser
 */
- (StructuredXMLParser *)parserForElement:(NSString *)element
                             namespaceURI:(NSString *)namespaceURI {
    
    StructuredXMLParser *result = [super parserForElement:element
                                             namespaceURI:namespaceURI];
    
    if (result == nil) {
        
        if ([element isEqualToString:kServiceResponseServicesListElement]) {
            
            result = [[[ServiceList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                  ownNamespacesPrefixesDictionary:nil] autorelease];
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the service list
 *
 * @return The service list
 */
- (ServiceList *)serviceList {
    
    ServiceList *result = (ServiceList *)[self objectForElement:kServiceResponseServicesListElement
                                                   namespaceURI:@""
                                                        atIndex:0];
    return result;
    
}


@end
