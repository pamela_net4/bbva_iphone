/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "Locality.h"

/**
 * Defines the name element 
 */
NSString * const kLocalityNameElement = @"NL";

/**
 * Defines the code element
 */
NSString * const kLocalityCodeElement = @"CL";

#pragma mark -
#pragma mark Properties

@implementation Locality

@dynamic localityName;
@dynamic code;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kLocalityNameElement]) ||
            ([element isEqualToString:kLocalityCodeElement])) {
            
            result = BXPOElementString;
        
        } 
        
    }
    
    return result;
    
}



#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the locality name 
 *
 * @return The locality name 
 */
- (NSString *)localityName {
    
    NSString *result = (NSString *)[self objectForElement:kLocalityNameElement
                                             namespaceURI:@""
                                                  atIndex:0];
        
    return result;
    
}

/*
 * Returns the code 
 *
 * @return The code 
 */
- (NSString *)code {
    
    NSString *result = (NSString *)[self objectForElement:kLocalityCodeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}



@end
