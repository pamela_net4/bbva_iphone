//
//  Ocurrency.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/22/13.
//
//

#import "StructuredXMLParser.h"

/**
 * Defines the description element tag
 */
extern NSString * const kOcurrencydDescriptionElement;

/**
 * Defines the Logic for the value element tag
 */
extern NSString * const kOcurrencyValueElement;

/**
 * Defines the Logic for the value element tag
 */
extern NSString * const kOcurrencyValueCurrencyElement;

/**
 * Analyzes a ocurrency
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface Ocurrency : StructuredXMLParser

/**
 * Provides read-only access to the description of the ocurrency
 */
@property (nonatomic, readonly, copy) NSString *description;

/**
 * Provides read-only access to the value of the ocurrency
 */
@property (nonatomic, readonly, copy) NSString *value;

/**
 * Provides read-only access to the value
 */
@property (nonatomic, readonly, copy) NSString *valueCurrency;


@end
