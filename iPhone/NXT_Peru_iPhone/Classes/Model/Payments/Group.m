//
//  Group.m
//  NXT_Peru_iPad
//
//  Created by Ricardo on 11/11/13.
//
//

#import "Group.h"

/**
 * Defines the group element tag
 */
NSString * const kGroupGroupElement = @"CODIGOGRUPO";

/**
 * Defines the description element tag
 */
NSString * const kGroupDescriptionElement = @"DESCRIPCIONGRUPO";

@implementation Group

@dynamic groupCod;
@dynamic description;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kGroupGroupElement]) ||
            ([element isEqualToString:kGroupDescriptionElement])){
            
            result = BXPOElementString;
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the code
 *
 * @return The code
 */
- (NSString *)groupCod {
    
    NSString *result = (NSString *)[self objectForElement:kGroupGroupElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the description
 *
 * @return The description
 */
- (NSString *)description {
    
    NSString *result = (NSString *)[self objectForElement:kGroupDescriptionElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

@end
