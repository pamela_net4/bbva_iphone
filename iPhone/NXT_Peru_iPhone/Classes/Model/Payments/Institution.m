//
//  Institution.m
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/17/13.
//
//

#import "Institution.h"

/**
 * Defines the Entity element tag
 */
NSString * const kInstitutionEntityElement = @"ENTIDAD";

/**
 * Defines the Code element tag
 */
NSString * const kInstitutionCodeElement = @"CODIGO";

/**
 * Defines the description element tag
 */
NSString * const kInstitutionDescriptionElement = @"DESCRIPCION";

/**
 * Defines the Class element tag
 */
NSString * const kInstitutionClassElement = @"CLASE";

/**
 * Defines the Flag of BD element tag
 */
NSString * const kInstitutionBDElement = @"BD";

/**
 * Defines the Parcial Val element tag
 */
NSString * const kInstitutionValParcialElement = @"VALPARCIAL";

/**
 * Defines the Indicator element tag
 */
NSString * const kInstitutionCardIndicatorElement = @"INDICADORTARJETA";

/**
 * Defines the Currency element tag
 */
NSString * const kInstitutionCurrencyElement = @"DIVISA";

/**
 * Defines the first Indicator element tag
 */
NSString * const kInstitutionIndicator1Element = @"INDICADOR1";

/**
 * Defines the indicator PF element tag
 */
NSString * const kInstitutionIndicatorPFElement = @"INDICADORPF";

/**
 * Defines the Sub Class Index element tag
 */
NSString * const kInstitutionSubClasssIndexElement = @"INDICESUBCLASE";

/**
 * Defines the Parameter element tag
 */
NSString * const kInstitutionParamElement = @"PARAMETROS";

@implementation Institution

@dynamic institutionCod;
@dynamic institutionDescription;
@dynamic institutionEntity;
@dynamic institutionClass;
@dynamic institutionHaveBD;
@dynamic institutionValParcial;
@dynamic institutionCardIndicator;
@dynamic institutionCurrency;
@dynamic institutionIndicator1;
@dynamic institutionIndicatorPF;
@dynamic institutionSubClassIndex;
@dynamic institutionParameter;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kInstitutionEntityElement]) ||
            ([element isEqualToString:kInstitutionCodeElement]) ||
            ([element isEqualToString:kInstitutionDescriptionElement]) ||
            ([element isEqualToString:kInstitutionClassElement]) ||
            ([element isEqualToString:kInstitutionBDElement]) ||
            ([element isEqualToString:kInstitutionValParcialElement]) ||
            ([element isEqualToString:kInstitutionCardIndicatorElement]) ||
            ([element isEqualToString:kInstitutionCurrencyElement]) ||
            ([element isEqualToString:kInstitutionIndicator1Element]) ||
            ([element isEqualToString:kInstitutionIndicatorPFElement]) ||
            ([element isEqualToString:kInstitutionSubClasssIndexElement]) ||
            ([element isEqualToString:kInstitutionParamElement])
            ){
            
            result = BXPOElementString;
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the carrier
 *
 * @return The carrier
 */
- (NSString *)institutionCod; {
    
    NSString *result = (NSString *)[self objectForElement:kInstitutionCodeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the description
 *
 * @return The description
 */
- (NSString *)institutionDescription {
    
    NSString *result = (NSString *)[self objectForElement:kInstitutionDescriptionElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the entity
 *
 * @return The entity
 */
- (NSString *)institutionEntity {
    
    NSString *result = (NSString *)[self objectForElement:kInstitutionEntityElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the class
 *
 * @return The class
 */
- (NSString *)institutionClass {
    
    NSString *result = (NSString *)[self objectForElement:kInstitutionClassElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the Flag BD
 *
 * @return The Flag BD
 */
- (NSString *)institutionHaveBD {
    
    NSString *result = (NSString *)[self objectForElement:kInstitutionBDElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the institutions partial val
 *
 * @return The institutions partial val
 */
- (NSString *)institutionValParcial {
    
    NSString *result = (NSString *)[self objectForElement:kInstitutionValParcialElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the institutions card indicator
 *
 * @return The institutions card indicator
 */
- (NSString *)institutionCardIndicator {
    
    NSString *result = (NSString *)[self objectForElement:kInstitutionCardIndicatorElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the institutions currency
 *
 * @return The institutions currency
 */
- (NSString *)institutionCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kInstitutionCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the institutions first indicator
 *
 * @return The institutions first indicator
 */
- (NSString *)institutionIndicator1 {
    
    NSString *result = (NSString *)[self objectForElement:kInstitutionIndicator1Element
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the institutions pf indicator
 *
 * @return The institutions pf indicator
 */
- (NSString *)institutionIndicatorPF {
    
    NSString *result = (NSString *)[self objectForElement:kInstitutionIndicatorPFElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the institutions sub class index
 *
 * @return The institutions sub class index
 */
- (NSString *)institutionSubClassIndex {
    
    NSString *result = (NSString *)[self objectForElement:kInstitutionSubClasssIndexElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the institutions Parameter
 *
 * @return The institutions Parameter
 */
- (NSString *)institutionParameter {
    
    NSString *result = (NSString *)[self objectForElement:kInstitutionParamElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

@end
