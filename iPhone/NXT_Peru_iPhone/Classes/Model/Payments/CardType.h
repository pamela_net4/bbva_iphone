/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "StructuredXMLParser.h"

@class BankList;

/**
 * Defines the category element tag 
 */
extern NSString * const kCardTypeCategoryElement;

/**
 * Defines the banks element tag
 */
extern NSString * const kCardTypeBanksElement;

/**
 * Defines the num element tag
 */
extern NSString * const kCardTypeNumElement;

/**
 * Defines the type element tag
 */
extern NSString * const kCardTypeTypeElement;

/**
 * Defines the currency element tag
 */
extern NSString * const kCardTypeCurrencyElement;

/**
 * Defines the param tag
 */
extern NSString * const kCardTypeParamCardElement;


/**
 * Analyzes a tag and value
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardType : StructuredXMLParser

/**
 * Provides read-only access to the tag
 */
@property (nonatomic, readonly, retain) BankList *bankList;

/**
 * Provides read-only access to the category
 */
@property (nonatomic, readonly, copy) NSString *category;

/**
 * Provides read-only access to the num
 */
@property (nonatomic, readonly, copy) NSString *num;

/**
 * Provides read-only access to the subject
 */
@property (nonatomic, readonly, copy) NSString *subject;

/**
 * Provides read-only access to the number
 */
@property (nonatomic, readonly, copy) NSString *number;

/**
 * Provides read-only access to the type
 */
@property (nonatomic, readonly, copy) NSString *type;

/**
 * Provides read-only access to the account type
 */
@property (nonatomic, readonly, copy) NSString *accountType;

/**
 * Provides read-only access to the disponible Amount
 */
@property (nonatomic, readonly, copy) NSString *availableBalanceString;

/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the Param
 */
@property (nonatomic, readonly, copy) NSString *param;



@end

