//
//  InstitutionList.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/17/13.
//
//

#import "StructuredXMLParser.h"

/**
 * Defines the GroupList element tag
 */
extern NSString * const kInstitutionListElement;

/**
 * Analyzes a payment list
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface InstitutionList : StructuredXMLParser

/**
 * Provides read-only access to the institution list
 */
@property (nonatomic, readwrite, retain) NSArray *institutionList;

@end
