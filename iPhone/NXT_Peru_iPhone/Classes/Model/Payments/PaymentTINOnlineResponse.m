//
//  PaymentTINOnlineResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 10/13/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "PaymentTINOnlineResponse.h"

#import "PaymentConfirmationResponse.h"


/**
 * Defines the msg-s element tag
 */
NSString * const kPaymentTINResponseMsgElement = @"MSG-S";

/**
 * Defines the additional information element tag
 */
NSString * const kPaymentTINResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";


/**
 * Defines the additional information element tag
 */
NSString * const kPaymentTINConfirmationResponseAdditionalInfoElement = @"CONFIRMACIONPAGO";

/**
 * Defines the type flow element tag
 */
NSString * const kPaymentTINResponseTypeFlowInfoElement = @"TIPOFLUJO";

/**
 * Defines the type flow element tag
 */
NSString * const kPaymentTINResponseComissionNInfoElement = @"H_COMISION";

/**
 * Defines the type flow element tag
 */
NSString * const kPaymentTINResponseComissionLInfoElement = @"I_COMISION";

/**
 * Defines the type flow element tag
 */
NSString * const kPaymentTINResponseTotalPaymentLInfoElement = @"I_TOT_PAGAR";

/**
 * Defines the type flow element tag
 */
NSString * const kPaymentTINResponseTotalPaymentNInfoElement = @"H_TOT_PAGAT";

NSString * const kPaymentTINResponseDisclaimerInfoElement = @"DISCLAIMER";

@interface PaymentTINOnlineResponse()


@end

@implementation PaymentTINOnlineResponse
@dynamic typeFlow;
@dynamic commissionN;
@dynamic commissionL;
@dynamic totalPayL;
@dynamic totalPayN ;
@dynamic additionalInformation;


#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPaymentConfirmationResponseMsgElement]) ||
            ([element isEqualToString:kPaymentTINResponseAdditionalInfoElement])){
            
            result = BXPOElementIdle;
            
        } else if (([element isEqualToString:kPaymentTINResponseTypeFlowInfoElement]) ||
                   ([element isEqualToString:kPaymentTINResponseComissionNInfoElement]) ||
                   ([element isEqualToString:kPaymentTINResponseComissionLInfoElement]) ||
                   ([element isEqualToString:kPaymentTINResponseTotalPaymentLInfoElement]) ||
                   ([element isEqualToString:kPaymentTINResponseTotalPaymentNInfoElement]) ||
                   ([element isEqualToString:kPaymentTINResponseDisclaimerInfoElement]) ) {
            
            result = BXPOElementString;
            
        }else if([element isEqualToString:kPaymentTINConfirmationResponseAdditionalInfoElement]){
            result = BXPOElementCustom;

        }
    }
    
    return result;
    
}

/*
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Default implementation returns nil
 */
- (StructuredXMLParser *)parserForElement:(NSString *)anElement
                             namespaceURI:(NSString *)namespaceURI {
    
    StructuredXMLParser *result = [super parserForElement:anElement namespaceURI:namespaceURI];
    if (result == nil) {
        
        if ([anElement isEqualToString:kPaymentTINConfirmationResponseAdditionalInfoElement]) {
            result = [[[PaymentConfirmationResponse alloc] initWithParentNamespacePrefixesDictionary:[self wholeNamespacesPrefixesDictionary]
                                                                     ownNamespacesPrefixesDictionary:nil] autorelease];
        }
    }
    
     return result;
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the coordinate
 *
 * @return The coordinate
 */
- (PaymentConfirmationResponse *)additionalInformation {
    
    PaymentConfirmationResponse *result = (PaymentConfirmationResponse *)[self objectForElement:kPaymentTINConfirmationResponseAdditionalInfoElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the coordinate
 *
 * @return The coordinate
 */
- (NSString *)typeFlow {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentTINResponseTypeFlowInfoElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the coordinate
 *
 * @return The coordinate
 */
- (NSString *)commissionN {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentTINResponseComissionNInfoElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the coordinate
 *
 * @return The coordinate
 */
- (NSString *)commissionL {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentTINResponseComissionLInfoElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the coordinate
 *
 * @return The coordinate
 */
- (NSString *)totalPayL {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentTINResponseTotalPaymentLInfoElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the coordinate
 *
 * @return The coordinate
 */
- (NSString *)totalPayN {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentTINResponseTotalPaymentNInfoElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the coordinate
 *
 * @return The coordinate
 */
- (NSString *)disclaimer {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentTINResponseDisclaimerInfoElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}



@end
