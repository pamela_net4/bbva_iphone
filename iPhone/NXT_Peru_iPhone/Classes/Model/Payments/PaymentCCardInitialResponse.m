/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PaymentCCardInitialResponse.h"
#import "StringList.h"

/**
 * Defines the additional information element tag
 */
NSString * const kPaymentCCardInitialResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";

/**
 * Defines the msg-s element tag
 */
NSString * const kPaymentCCardInitialResponseMsgElement = @"MSG-S";

/**
 * Defines the card list element tag
 */
NSString * const kPaymentCCardInitialResponseCardsListElement = @"TARJETASHABILITADAS";


#pragma mark -
#pragma mark Properties

@implementation PaymentCCardInitialResponse

@dynamic numberCardList;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPaymentCCardInitialResponseAdditionalInfoElement]) ||
            ([element isEqualToString:kPaymentCCardInitialResponseMsgElement])){
            
            result = BXPOElementIdle;
        
        } else if ([element isEqualToString:kPaymentCCardInitialResponseCardsListElement]) {
            
            result = BXPOElementCustom;
            
        }
    }
    
    return result;
    
}

/*
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Default implementation returns nil
 */
- (StructuredXMLParser *)parserForElement:(NSString *)anElement
                             namespaceURI:(NSString *)namespaceURI {
	
	StructuredXMLParser *result = [super parserForElement:anElement namespaceURI:namespaceURI];
    
	if (result == nil) {
		
		if ([anElement isEqualToString:kPaymentCCardInitialResponseCardsListElement]) {
			
			result = [[[StringList alloc] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                    ownNamespacesPrefixesDictionary:nil 
																	   stringToFind:@"NUMEROTARJETA"] autorelease];
			
		}
		
	}
	
    return result;
    
}


#pragma mark -
#pragma mark Properties selectors

/**
 * Returns the numberCardList value.
 *
 * @return The numberCardList value.
 */
- (StringList *)numberCardList {
	
	StringList *result = (StringList *)[self objectForElement:kPaymentCCardInitialResponseCardsListElement
                                                 namespaceURI:@""
                                                      atIndex:0];
    
    return result;
	
}

@end
