//
//  PaymentInstitutionsAndCompaniesDetailResponse.h
//  NXT_Peru_iPad
//
//  Created by Ricardo on 11/18/13.
//
//

#import "StructuredStatusEnabledResponse.h"

@class FieldList;

/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentInstitutionsDetailResponseAdditionalInfoElement;

/**
 * Defines the msg-s element tag
 */
extern NSString * const kPaymentInstitutionsDetailResponseMsgElement;

/**
 * Defines the agreement element tag
 */
extern NSString * const kPaymentInstitutionsDetailResponseAgreementElement;

/**
 * Defines the pay type element tag
 */
extern NSString * const kPaymentInstitutionsDetailResponsePayTypeElement;

/**
 * Defines the institution element tag
 */
extern NSString * const kPaymentInstitutionsDetailResponseInstitutionElement;

/**
 * Defines the camps element tag
 */
extern NSString * const kPaymentInstitutionsDetailResponseFieldsElement;

/**
 * Defines the Array Title element tag
 */
extern NSString * const kPaymentInstitutionsDetailResponseArrayTitleElement;

/**
 * Defines the Data Validation element tag
 */
extern NSString * const kPaymentInstitutionsDetailResponseDataValidationElement;

/**
 * Defines the Module Flag element tag
 */
extern NSString * const kPaymentInstitutionsDetailResponseModuleElement;

/**
 * Defines the Schedule ampliation element tag
 */
extern NSString * const kPaymentInstitutionsDetailResponseScheduleAmpl;


/**
 * The payment Institution Detail response.
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface PaymentInstitutionsAndCompaniesDetailResponse : StructuredStatusEnabledResponse

/**
 * Provides read-only access to the group list
 */
@property (nonatomic, readonly, retain) NSString *payType;

/**
 * Provides read-only access to the group list
 */
@property (nonatomic, readonly, retain) NSString *payTypeCode;

/**
 * Provides read-only access to the institution list
 */
@property (nonatomic, readonly, retain) NSString *institutionName;

/**
 * Provides read-only access to the institution list
 */
@property (nonatomic, readonly, retain) NSString *arrayTitle;

/**
 * Provides read-only access to the Data Validation
 */
@property (nonatomic, readonly, retain) NSString *dataValidation;

/**
 * Provides read-only access to the Module Flag
 */
@property (nonatomic, readonly, retain) NSString *moduleFlag;

/**
 * Provides read-only access to the Field list
 */
@property (nonatomic, readonly, retain) FieldList *fieldList;

/**
 * Provides read-only access to the Schedule ampliation
 */
@property (nonatomic, readonly, retain) NSString *scheduleAmpl;


@end
