/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "StructuredXMLParser.h"

/**
 * Defines the tag element 
 */
extern NSString * const kLocalityNameElement;

/**
 * Defines the value element
 */
extern NSString * const kLocalityCodeElement;


/**
 * Analyzes a tag and value
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Locality : StructuredXMLParser

/**
 * Provides read-only access to the tag
 */
@property (nonatomic, readonly, copy) NSString *localityName;

/**
 * Provides read-only access to the value
 */
@property (nonatomic, readonly, copy) NSString *code;

@end

