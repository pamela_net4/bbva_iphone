//
//  FieldList.h
//  NXT_Peru_iPad
//
//  Created by Ricardo on 11/18/13.
//
//

#import "StructuredXMLParser.h"

/**
 * Defines the GroupList element tag
 */
extern NSString * const kFieldListElement;

/**
 * Analyzes a Field list
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface FieldList : StructuredXMLParser

/**
 * Provides read-only access to the group list
 */
@property (nonatomic, readwrite, retain) NSArray *fieldList;

@end
