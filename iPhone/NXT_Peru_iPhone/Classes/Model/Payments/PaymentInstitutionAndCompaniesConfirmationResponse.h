//
//  PaymentInstitutionAndCompaniesConfirmationResponse.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/21/13.
//
//

#import "StructuredStatusEnabledResponse.h"

@class CardTypeList;
@class accountPayList;
@class CarrierList;
@class OcurrencyList;
@class PendingDocumentList;


/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentInstitutionsconfirmationResponseAdditionalInfoElement;

/**
 * Defines the msg-s element tag
 */
extern NSString * const kPaymentInstitutionsconfirmationResponseMsgElement;

/**
 * Defines the pay type element tag
 */
extern NSString * const kPaymentInstitutionsconfirmationResponsePayTypeElement;

/**
 * Defines the companie name element tag
 */
extern NSString * const kPaymentInstitutionsconfirmationResponseCompanieNameElement;

/**
 * Defines the Ocurrency list element tag
 */
extern NSString * const kPaymentInstitutionsconfirmationResponseOcurrencyListElement;

/**
 * Defines the Pending Holder Name element tag
 */
extern NSString * const kPaymentInstitutionsconfirmationResponseHolderNameElement;

/**
 * Defines the Pending docs list element tag
 */
extern NSString * const kPaymentInstitutionsconfirmationResponsePendingDocsListElement;

/**
 * Defines the Partial Val element tag
 */
extern NSString * const kPaymentInstitutionsconfirmationResponsePartialValElement;


/**
 * Defines the card indication payment element tag
 */
extern NSString * const kPaymentInstitutionsconfirmationResponseCardIndPaymentElement;

/**
 * Defines the Card List element tag
 */
extern NSString * const kPaymentInstitutionsconfirmationResponseCardListElement;

/**
 * Defines the Account List element tag
 */
extern NSString * const kPaymentInstitutionsconfirmationResponseAccountListElement;

/**
 * Defines the Carrier list element tag
 */
extern NSString * const kPaymentInstitutionsconfirmationResponseCarrierListElement;

/**
 * Defines the Schedule ampliation element tag
 */
extern NSString * const kPaymentInstitutionsconfirmationResponseScheduleAmpl;


/**
 * The payment Institution confirmation response.
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface PaymentInstitutionAndCompaniesConfirmationResponse : StructuredStatusEnabledResponse


/**
 * Provides read-only access to the card list
 */
@property (nonatomic, readonly, retain) CardTypeList *cardList;

/**
 * Provides read-only access to the account list
 */
@property (nonatomic, readonly, retain) accountPayList *accountList;

/**
 * Provides read-only access to the carrier list
 */
@property (nonatomic, readonly, retain) CarrierList *carrierList;

/**
 * Provides read-only access to the ocurrency list
 */
@property (nonatomic, readonly, retain) OcurrencyList *ocurrencyList;

/**
 * Provides read-only access to the ocurrency list
 */
@property (nonatomic, readonly, retain) PendingDocumentList *pendingDocumentList;

/**
 * Provides read-only access to the Pay type
 */
@property (nonatomic, readonly, retain) NSString *payType;

/**
 * Provides read-only access to the Pay type
 */
@property (nonatomic, readonly, retain) NSString *payTypeCode;

/**
 * Provides read-only access to the Companie Name
 */
@property (nonatomic, readonly, retain) NSString *companieName;

/**
 * Provides read-only access to the holder name
 */
@property (nonatomic, readonly, retain) NSString *holderName;

/**
 * Provides read-only access to the partial value
 */
@property (nonatomic, readonly, assign) BOOL partialVal;

/**
 * Provides read-only access to the pay indication of the card
 */
@property (nonatomic, readonly, retain) NSString *indCardPay;

/**
 * Provides read-only access to the Schedule ampliation
 */
@property (nonatomic, readonly, retain) NSString *scheduleAmpl;


@end
