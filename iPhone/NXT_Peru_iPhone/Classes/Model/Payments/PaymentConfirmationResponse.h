/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "StructuredStatusEnabledResponse.h"

/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentConfirmationResponseAdditionalInfoElement;

/**
 * Defines the msg-s element tag
 */
extern NSString * const kPaymentConfirmationResponseMsgElement;

/**
 * Defines the coordinate element tag
 */
extern NSString * const kPaymentConfirmationResponseCoordElement;

/**
 * Defines the seal element tag
 */
extern NSString * const kPaymentConfirmationResponseSealElement;

/**
 * Defines the amount to charge element tag
 */
extern NSString * const kPaymentConfirmationResponseAmountToChargeElement;

/**
 * Defines the amount to charge currency element tag
 */
extern NSString * const kPaymentConfirmationResponseAmountToChargeCurrencyElement;

/**
 * Defines the disclaimer element tag
 */
extern NSString * const kPaymentConfirmationResponseDisclaimerElement;

/**
 * Defines the amount charged element tag
 */
extern NSString * const kPaymentConfirmationResponseAmountChargedElement;

/**
 * Defines the amount charged currency element tag
 */
extern NSString * const kPaymentConfirmationResponseAmountChargedCurrencyElement;

/**
 * Defines the amount to pay element tag
 */
extern NSString * const kPaymentConfirmationResponseAmountPaidElement;

/**
 * Defines the amount to pay currency element tag
 */
extern NSString * const kPaymentConfirmationResponseAmountPaidCurrencyElement;

/**
 * Defines the network use element tag
 */
extern NSString * const kPaymentConfirmationResponseNetworkUseElement;

/**
 * Defines the network use currency element tag
 */
extern NSString * const kPaymentConfirmationResponseNetworkUseCurrencyElement;

/**
 * Defines the tax element tag
 */
extern NSString * const kPaymentConfirmationResponseTaxElement;

/**
 * Defines the tax currency element tag
 */
extern NSString * const kPaymentConfirmationResponseTaxCurrencyElement;

/**
 * Defines the tax currency element tag
 */
extern NSString * const kPaymentConfirmationResponseTypeChangeElement;

/**
 * Defines the operation element tag
 */
extern NSString * const kPaymentConfirmationResponseOperationElement;

/**
 * Defines the amount element tag
 */
extern NSString * const kPaymentConfirmationResponseAmountChargeElement;

/**
 * Defines the currency element tag
 */
extern NSString * const kPaymentConfirmationResponseAmountChargeCurrencyElement;

/**
 * Defines the supply element tag
 */
extern NSString * const kPaymentConfirmationResponseSupplyElement;

/**
 * Defines the supply number element tag
 */
extern NSString * const kPaymentConfirmationResponseBeneficiaryElement;

/**
 * Defines the supply number element tag
 */
extern NSString * const kPaymentConfirmationResponseBankNameElement;;

/**
 * Defines the supply number element tag
 */
extern NSString * const kPaymentConfirmationResponseCardPaymentElement;
/**
 * Analyzes a service list
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentConfirmationResponse : StructuredStatusEnabledResponse

/**
 * Provides read-only access to the coordinate
 */
@property (nonatomic, readonly, copy) NSString *coordinate;

/**
 * Provides read-only access to the seal
 */
@property (nonatomic, readonly, copy) NSString *seal;

/**
 * Provides read-only access to the amount to charge
 */
@property (nonatomic, readonly, copy) NSString *amountToCharge;

/**
 * Provides read-only access to the amount to charge currency
 */
@property (nonatomic, readonly, copy) NSString *amountToChargeCurrency;

/**
 * Provides read-only access to the amount to charge
 */
@property (nonatomic, readonly, copy) NSString *amountToRecharge;

/**
 * Provides read-only access to the amount to charge currency
 */
@property (nonatomic, readonly, copy) NSString *amountToRechargeCurrency;

/**
 * Provides read-only access to the payment list
 */
@property (nonatomic, readonly, copy) NSString *disclaimer;

/**
 * Provides read-only access to the amount charged
 */
@property (nonatomic, readonly, copy) NSString *amountCharged;

/**
 * Provides read-only access to the amount charged currency
 */
@property (nonatomic, readonly, copy) NSString *amountChargedCurrency;

/**
 * Provides read-only access to the amount paid
 */
@property (nonatomic, readonly, copy) NSString *amountPaid;

/**
 * Provides read-only access to the amount paid currency
 */
@property (nonatomic, readonly, copy) NSString *amountPaidCurrency;

/**
 * Provides read-only access to the network use
 */
@property (nonatomic, readonly, copy) NSString *networkUse;

/**
 * Provides read-only access to the network use currency
 */
@property (nonatomic, readonly, copy) NSString *networkUseCurrency;

/**
 * Provides read-only access to the tax
 */
@property (nonatomic, readonly, copy) NSString *tax;

/**
 * Provides read-only access to the tax currency
 */
@property (nonatomic, readonly, copy) NSString *taxCurrency;

/**
 * Provides read-only access to the type change.
 */
@property (nonatomic, readonly, copy) NSString *typeChange;

/**
 * Provides read-only access to the operation.
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to the amount charge
 */
@property (nonatomic, readonly, copy) NSString *amountCharge;

/**
 * Provides read-only access to the amount charge currency
 */
@property (nonatomic, readonly, copy) NSString *amountChargeCurrency;

/**
 * Provides read-only access to the supply
 */
@property (nonatomic, readonly, copy) NSString *supply;


/**
 * Provides read-only access to the beneficiary
 */
@property (nonatomic, readonly, copy) NSString *beneficiary;


/**
 * Provides read-only access to the beneficiary
 */
@property (nonatomic, readonly, copy) NSString *bankName;


/**
 * Provides read-only access to the beneficiary
 */
@property (nonatomic, readonly, copy) NSString *cardPayment;

@end