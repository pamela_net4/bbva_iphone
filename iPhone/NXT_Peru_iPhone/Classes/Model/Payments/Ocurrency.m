//
//  Ocurrency.m
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/22/13.
//
//

#import "Ocurrency.h"

/**
 * Defines the description element tag
 */
NSString * const kOcurrencydDescriptionElement = @"DESCRIPCION";

/**
 * Defines the Logic for the value element tag
 */
NSString * const kOcurrencyValueElement = @"VALOR";

/**
 * Defines the Logic for the value currency element tag
 */
NSString * const kOcurrencyValueCurrencyElement = @"DIVVALOR";

@implementation Ocurrency

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kOcurrencydDescriptionElement]) ||
            ([element isEqualToString:kOcurrencyValueElement]) ||
            ([element isEqualToString:kOcurrencyValueCurrencyElement])
            ){
            
            result = BXPOElementString;
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the description
 *
 * @return The description
 */
- (NSString *)description {
    
    NSString *result = (NSString *)[self objectForElement:kOcurrencydDescriptionElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the logic for the field
 *
 * @return The value
 */
- (NSString *)value {
    
    NSString *result = (NSString *)[self objectForElement:kOcurrencyValueElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the value
 *
 * @return The value currency
 */
- (NSString *)valueCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kOcurrencyValueCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

@end
