//
//  accountPay.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/22/13.
//
//

#import "StructuredXMLParser.h"

/**
 * Defines the identification element tag
 */
extern NSString * const kAccountPayCurrencyElement;

/**
 * Defines the bank element tag
 */
extern NSString * const kAccountPayAccountTypeElement;

/**
 * Defines the agreement element tag
 */
extern NSString * const kAccountPaySubjectElement;

/**
 * Defines the agreement parameters element tag
 */
extern NSString * const kAccountPayDispAmountElement;

/**
 * Defines the agreement parameters element tag
 */
extern NSString * const kAccountPayDispAmountCurrencyElement;

/**
 * Defines the agreement parameters element tag
 */
extern NSString * const kAccountPayDispAccountParamElement;

/**
 * Analyzes a AccountPay element
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface accountPay : StructuredXMLParser

/**
 * Provides read-only access to the currency tag
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the accountType
 */
@property (nonatomic, readonly, copy) NSString *accountType;

/**
 * Provides read-only access to the subject
 */
@property (nonatomic, readonly, copy) NSString *subject;

/**
 * Provides read-only access to the number
 */
@property (nonatomic, readonly, copy) NSString *number;

/**
 * Provides read-only access to the disponible Amount
 */
@property (nonatomic, readonly, copy) NSString *availableBalanceString;

/**
 * Provides read-only access to the disponible Amont Currency
 */
@property (nonatomic, readonly, copy) NSString *dispAmountCurrency;

/**
 * Provides read-only access to the account Param
 */
@property (nonatomic, readonly, copy) NSString *accountParam;

/**
 * Provides read-only access to the account display format
 */
@property (nonatomic, readonly, copy) NSString *accountIdAndDescription;

@end
