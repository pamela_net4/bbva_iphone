/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "StructuredStatusEnabledResponse.h"

@class PaymentList;
@class CardList;
@class LocalityList;
@class StringList;

/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentDataResponseAdditionalInfoElement;

/**
 * Defines the msg-s element tag
 */
extern NSString * const kPaymentDataResponseMsgElement;

/**
 * Defines the holder element tag
 */
extern NSString * const kPaymentDataResponseHolderElement;

/**
 * Defines the locality element tag
 */
extern NSString * const kPaymentDataResponseLocalityElement;

/**
 * Defines the currency element tag
 */
extern NSString * const kPaymentDataResponseCurrencyElement;

/**
 * Defines the phone number element tag
 */
extern NSString * const kPaymentDataResponsePhoneNumberElement;

/**
 * Defines the cards flag element tag
 */
extern NSString * const kPaymentDataResponseCardsFlagElement;

/**
 * Defines the payments list element tag
 */
extern NSString * const kPaymentDataResponsePaymentsListElement;

/**
 * Defines the accounts list element tag
 */
extern NSString * const kPaymentDataResponseAccountsListElement;

/**
 * Defines the accounts list element tag
 */
extern NSString * const kPaymentDataResponseCardsListElement;

/**
 * Defines the supply number element tag
 */
extern NSString *const kPaymentDataResponseSupplyElement;

/**
 * Defines the client name element tag
 */
extern NSString * const kPaymentDataResponseClientNameElement;

/**
 * Defines the card currency element tag
 */
extern NSString * const kPaymentDataResponseCardCurrencyElement;

/**
 * Defines the card type element tag
 */
extern NSString * const kPaymentDataResponseCardTypeElement;

/**
 * Defines the card number element tag
 */
extern NSString * const kPaymentDataResponseCardNumberElement;

/**
 * Defines the credit limit element tag
 */
extern NSString * const kPaymentDataResponseCreditLimitElement;

/**
 * Defines the available element tag
 */
extern NSString * const kPaymentDataResponseAvailableElement;

/**
 * Defines the total debt soles element tag
 */
extern NSString * const kPaymentDataResponseTotalDebtSolesElement;

/**
 * Defines the total debt dolars element tag
 */
extern NSString * const kPaymentDataResponseTotalDebeDolarsElement;

/**
 * Defines the last payment date element tag
 */
extern NSString * const kPaymentDataResponseLastPaymentDateElement;

/**
 * Defines the minimum payment element tag
 */
extern NSString * const kPaymentDataResponseMinPaymentElement;

/**
 * Defines the month instalment element tag
 */
extern NSString * const kPaymentDataResponseMonthInstalmentElement;

/**
 * Defines the month total element tag
 */
extern NSString * const kPaymentDataResponseMonthTotalElement;

/**
 * Defines the minimum payment dolar element tag
 */
extern NSString * const kPaymentDataResponseMinPaymentDolarElement;

/**
 * Defines the month instalment dolar element tag
 */
extern NSString * const kPaymentDataResponseMonthInstalmentDolarElement;

/**
 * Defines the month total dolar element tag
 */
extern NSString * const kPaymentDataResponseMonthTotalDolarElement;

/**
 * Defines the beneficiary card element tag
 */
extern NSString * const kPaymentDataResponseBeneficiaryCardElement;

/**
 * Defines the bank name element tag
 */
extern NSString * const kPaymentDataResponseBankNameElement;

/**
 * Defines the authorized accounts element tag
 */
extern NSString * const kPaymentDataResponseAuthorizedAccountsElement;

/**
 * Defines the emision place element tag
 */
extern NSString * const kPaymentDataResponseEmisionPlaceElement;

/**
 * Defines the max element tag
 */
extern NSString * const kPaymentDataResponseMaxElement;

/**
 * Defines the min element tag
 */
extern NSString * const kPaymentDataResponseMinElement;

/**
 * Defines the message range element tag
 */
extern NSString * const kPaymentDataResponseMessageRangeElement;


/**
 * Analyzes a service list
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentDataResponse : StructuredStatusEnabledResponse

/**
 * Provides read-only access to the holder
 */
@property (nonatomic, readonly, copy) NSString *holder;

/**
 * Provides read-only access to the locality
 */
@property (nonatomic, readonly, copy) NSString *locality;

/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the phone number
 */
@property (nonatomic, readonly, copy) NSString *phoneNumber;

/**
 * Provides read-only access to the holder
 */
@property (nonatomic, readonly, assign) BOOL cardsFlag;

/**
 * Provides read-only access to the payment list
 */
@property (nonatomic, readonly, retain) PaymentList *paymentList;

/**
 * Provides read-only access to the account list
 */
@property (nonatomic, readonly, retain) StringList *numberAccountList;

/**
 * Provides read-only access to the account enable list.
 */
@property (nonatomic, readonly, retain) StringList *numberAccountEnabledList;

/**
 * Provides read-only access to the account list
 */
@property (nonatomic, readonly, retain) StringList *numberCardList;

/**
 * Provides read-only access to the client name
 */
@property (nonatomic, readonly, copy) NSString *clientName;

/**
 * Provides read-only access to the card currency
 */
@property (nonatomic, readonly, copy) NSString *cardCurrency;

/**
 * Provides read-only access to the card type
 */
@property (nonatomic, readonly, copy) NSString *cardType;

/**
 * Provides read-only access to the card number
 */
@property (nonatomic, readonly, copy) NSString *cardNumber;

/**
 * Provides read-only access to the creditLimit
 */
@property (nonatomic, readonly, copy) NSString *creditLimit;

/**
 * Provides read-only access to the available amount
 */
@property (nonatomic, readonly, copy) NSString *availableAmount;

/**
 * Provides read-only access to the total debt in soles
 */
@property (nonatomic, readonly, copy) NSString *totalDebtSoles;

/**
 * Provides read-only access to the total debt in dolars
 */
@property (nonatomic, readonly, copy) NSString *totalDebtDolars;

/**
 * Provides read-only access to the last payment date
 */
@property (nonatomic, readonly, copy) NSString *lastPaymentDate;

/**
 * Provides read-only access to the minimum payment
 */
@property (nonatomic, readonly, copy) NSString *minPayment;

/**
 * Provides read-only access to the month instalment
 */
@property (nonatomic, readonly, copy) NSString *monthInstalment;

/**
 * Provides read-only access to the month total
 */
@property (nonatomic, readonly, copy) NSString *monthTotal;

/**
 * Provides read-only access to the minimum payment in dolars
 */
@property (nonatomic, readonly, copy) NSString *minPaymentDolar;

/**
 * Provides read-only access to the month instalment in dolars
 */
@property (nonatomic, readonly, copy) NSString *monthInstalmentDolar;

/**
 * Provides read-only access to the month total in dolars
 */
@property (nonatomic, readonly, copy) NSString *monthTotalDolar;

/**
 * Provides read-only access to the beneficiary card
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryCard;

/**
 * Provides read-only access to the bankName
 */
@property (nonatomic, readonly, copy) NSString *bankName;

/**
 * Provides read-only access to the beneficiary card
 */
@property (nonatomic, readonly, retain) LocalityList *emisionPlaces;

/**
 * Provides read-only access to the max
 */
@property (nonatomic, readonly, assign) NSInteger max;

/**
 * Provides read-only access to the max
 */
@property (nonatomic, readonly, assign) NSInteger min;

/**
 * Provides read-only access to the message range
 */
@property (nonatomic, readonly, copy) NSString *messageRange;

/**
 * Provides read-only access to the divisa
 */
@property (nonatomic, readonly, copy) NSString *divisa;

/**
 * Provides read-only access to the supply number
 */
@property (nonatomic, readonly, copy) NSString *supply;


@end