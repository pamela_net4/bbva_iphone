//
//  Field.h
//  NXT_Peru_iPad
//
//  Created by Ricardo on 11/18/13.
//
//

#import "StructuredXMLParser.h"

/**
 * Defines the description element tag
 */
extern NSString * const kFieldDescriptionElement;

/**
 * Defines the Logic for the field element tag
 */
extern NSString * const kFieldLogCampElement;

/**
 * Defines the Logic for the field element tag
 */
extern NSString * const kFieldAlignmentElement;

/**
 * Defines the Logic for the field element tag
 */
extern NSString * const kFieldFullCaracElement;

/**
 * Defines the Logic for the field element tag
 */
extern NSString * const kFieldInitialPosElement;

/**
 * Defines the Logic for the field element tag
 */
extern NSString * const kFieldIndPassFieldElement;

/**
 * Defines the Logic for the field element tag
 */
extern NSString * const kFieldValidationTypeFieldElement;

/**
 * Defines the Logic for the field element tag
 */
extern NSString * const kFieldAuxLabelElement;

/**
 * Analyzes a dynamic field
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface Field : StructuredXMLParser

/**
 * Provides read-only access to the logiField
 */
@property (nonatomic, readonly, copy) NSString *logiField;

/**
 * Provides read-only access to the description of the field
 */
@property (nonatomic, readonly, copy) NSString *description;

/**
 * Provides read-only access to the alignment of the field
 */
@property (nonatomic, readonly, copy) NSString *alignment;

/**
 * Provides read-only access to the max number of the field
 */
@property (nonatomic, readonly, copy) NSString *fullCarac;

/**
 * Provides read-only access to the initial position of the field
 */
@property (nonatomic, readonly, copy) NSString *initialPosition;

/**
 * Provides read-only access to the pass flag
 */
@property (nonatomic, readonly) BOOL indPassField;

/**
 * Provides read-only access to the validation type
 */
@property (nonatomic, readonly, copy) NSString *validationType;

/**
 * Provides read-only access to the auxiliar label
 */
@property (nonatomic, readonly, copy) NSString *auxLabel;

@end
