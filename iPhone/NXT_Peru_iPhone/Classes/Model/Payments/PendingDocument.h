//
//  PendingDocument.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/22/13.
//
//

#import "StructuredXMLParser.h"

/**
 * Defines the description element tag
 */
extern NSString * const kPendingDocDescriptionElement;

/**
 * Defines the expiration date element tag
 */
extern NSString * const kPendingDocExpirationDateElement;

/**
 * Defines the expiration date element tag
 */
extern NSString * const kPendingDocExpirationDateCustomElement;

/**
 * Defines the Currency element tag
 */
extern NSString * const kPendingDocCurrencyElement;

/**
 * Defines the Currency element tag
 */
extern NSString * const kPendingDocMinAmountElement;

/**
 * Defines the Currency element tag
 */
extern NSString * const kPendingDocMinAmountCustomElement;

/**
 * Defines the Min Amount Currency element tag
 */
extern NSString * const kPendingDocMinAmountCurrencyElement;

/**
 * Defines the Min Amount Currency element tag
 */
extern NSString * const kPendingDocMinAmountCurrencyCustomElement;

/**
 * Defines the Max Amount element tag
 */
extern NSString * const kPendingDocMaxAmountElement;

/**
 * Defines the Max Amount element tag
 */
extern NSString * const kPendingDocMaxAmountCustomElement;

/**
 * Defines the Max Amount Currency element tag
 */
extern NSString * const kPendingDocMaxAmountCurrencyElement;

/**
 * Defines the Max Amount Currency element tag
 */
extern NSString * const kPendingDocMaxAmountCurrencyCustomElement;


/**
 * Defines the Total Amount element tag
 */
extern NSString * const kPendingDocTotalAmountElement;

/**
 * Defines the Total Amount Currency element tag
 */
extern NSString * const kPendingDocTotalAmountCurrencyElement;

/**
 * Analyzes a tag and value
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface PendingDocument : StructuredXMLParser

/**
 * Provides read-only access to the document description
 */
@property (nonatomic, readonly, copy) NSString *description;

/**
 * Provides read-only access to the document expiration date
 */
@property (nonatomic, readonly, copy) NSString *expirationDate;

/**
 * Provides read-only access to the document currency
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the Min amount
 */
@property (nonatomic, readonly, copy) NSString *minAmount;

/**
 * Provides read-only access to the min amount currency
 */
@property (nonatomic, readonly, copy) NSString *minAmountCurrency;

/**
 * Provides read-only access to the max amount
 */
@property (nonatomic, readonly, copy) NSString *maxAmount;

/**
 * Provides read-only access to the max amount currency
 */
@property (nonatomic, readonly, copy) NSString *maxAmountCurrency;

/**
 * Provides read-only access to the total amount
 */
@property (nonatomic, readonly, copy) NSString *totalAmount;

/**
 * Provides read-only access to the total amount currency
 */
@property (nonatomic, readonly, copy) NSString *totalAmountCurrency;

@end
