/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PaymentDataResponse.h"

#import "CardList.h"
#import "PaymentList.h"
#import "LocalityList.h"
#import "StringList.h"

/**
 * Defines the additional information element tag
 */
NSString * const kPaymentDataResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";

/**
 * Defines the msg-s element tag
 */
NSString * const kPaymentDataResponseMsgElement = @"MSG-S";

/**
 * Defines the holder element tag
 */
NSString * const kPaymentDataResponseHolderElement = @"TITULAR";

/**
 * Defines the locality element tag
 */
NSString * const kPaymentDataResponseLocalityElement = @"LOCALIDAD";

/**
 * Defines the currency element tag
 */
NSString * const kPaymentDataResponseCurrencyElement = @"MONEDA";

/**
 * Defines the phone number element tag
 */
NSString * const kPaymentDataResponsePhoneNumberElement = @"TELEFONO";

/**
 * Defines the cards flag element tag
 */
NSString * const kPaymentDataResponseCardsFlagElement = @"INDICADORPAGOTARJETA";

/**
 * Defines the payments list element tag
 */
NSString * const kPaymentDataResponsePaymentsListElement = @"PAGOS";

/**
 * Defines the accounts list element tag
 */
NSString * const kPaymentDataResponseAccountsListElement = @"CUENTAS";

/**
 * Defines the account enable element tag
 */
NSString * const kPaymentDataResponseAccountsEnableListElement = @"CUENTASHABILITADAS";

/**
 * Defines the accounts list element tag
 */
NSString * const kPaymentDataResponseCardsListElement = @"TARJETAS";

/**
 * Defines the supply number element tag
 */
NSString *const kPaymentDataResponseSupplyElement = @"SUMINISTRO";

/**
 * Defines the client name element tag
 */
NSString * const kPaymentDataResponseClientNameElement = @"NOMBRECLIENTE";

/**
 * Defines the card currency element tag
 */
NSString * const kPaymentDataResponseCardCurrencyElement = @"DIVISATARJETA";

/**
 * Defines the card element tag
 */
NSString * const kPaymentDataResponseDivisaElement = @"DIVISA";

/**
 * Defines the card type element tag
 */
NSString * const kPaymentDataResponseCardTypeElement = @"TIPOTARJETA";

/**
 * Defines the card number element tag
 */
NSString * const kPaymentDataResponseCardNumberElement = @"NUMEROTARJETA";

/**
 * Defines the credit limit element tag
 */
NSString * const kPaymentDataResponseCreditLimitElement = @"LIMITECREDITO";

/**
 * Defines the available element tag
 */
NSString * const kPaymentDataResponseAvailableElement = @"DISPONIBLE";

/**
 * Defines the total debt soles element tag
 */
NSString * const kPaymentDataResponseTotalDebtSolesElement = @"DEUDATOTALSOLES";

/**
 * Defines the total debt dolars element tag
 */
NSString * const kPaymentDataResponseTotalDebeDolarsElement = @"DEUDATOTALDOLARES";

/**
 * Defines the last payment date element tag
 */
NSString * const kPaymentDataResponseLastPaymentDateElement = @"ULTIMODIAPAGO";

/**
 * Defines the minimum payment element tag
 */
NSString * const kPaymentDataResponseMinPaymentElement = @"PAGOMINIMO";

/**
 * Defines the month instalment element tag
 */
NSString * const kPaymentDataResponseMonthInstalmentElement = @"CUOTAMES";

/**
 * Defines the month total element tag
 */
NSString * const kPaymentDataResponseMonthTotalElement = @"TOTALMES";

/**
 * Defines the minimum payment dolar element tag
 */
NSString * const kPaymentDataResponseMinPaymentDolarElement = @"PAGOMINIMOUSD";

/**
 * Defines the month instalment dolar element tag
 */
NSString * const kPaymentDataResponseMonthInstalmentDolarElement = @"CUOTAMESUSD";

/**
 * Defines the month total dolar element tag
 */
NSString * const kPaymentDataResponseMonthTotalDolarElement = @"TOTALMESUSD";

/**
 * Defines the beneficiary card element tag
 */
NSString * const kPaymentDataResponseBeneficiaryCardElement = @"TARJETABENEFICIARIA";

/**
 * Defines the bank name element tag
 */
NSString * const kPaymentDataResponseBankNameElement = @"NOMBREBANCO";

/**
 * Defines the authorized accounts element tag
 */
NSString * const kPaymentDataResponseAuthorizedAccountsElement = @"CUENTASHABILIDATAS";

/**
 * Defines the emision place element tag
 */
NSString * const kPaymentDataResponseEmisionPlaceElement = @"LUGAREMISION";

/**
 * Defines the max element tag
 */
NSString * const kPaymentDataResponseMaxElement = @"MAXIMO";

/**
 * Defines the min element tag
 */
NSString * const kPaymentDataResponseMinElement = @"MINIMO";

/**
 * Defines the message range element tag
 */
NSString * const kPaymentDataResponseMessageRangeElement = @"MENSAJERANGO";

@implementation PaymentDataResponse

#pragma mark -
#pragma mark Properties

@dynamic holder;
@dynamic locality;
@dynamic currency;
@dynamic phoneNumber;
@dynamic cardsFlag;
@dynamic paymentList;
@dynamic numberAccountList;
@dynamic numberAccountEnabledList;
@dynamic numberCardList;
@dynamic clientName;
@dynamic cardCurrency;
@dynamic cardType;
@dynamic cardNumber;
@dynamic creditLimit;
@dynamic availableAmount;
@dynamic totalDebtSoles;
@dynamic totalDebtDolars;
@dynamic lastPaymentDate;
@dynamic minPayment;
@dynamic monthInstalment;
@dynamic monthTotal;
@dynamic minPaymentDolar;
@dynamic monthInstalmentDolar;
@dynamic monthTotalDolar;
@dynamic beneficiaryCard;
@dynamic bankName;
@dynamic emisionPlaces;
@dynamic max;
@dynamic min;
@dynamic messageRange;
@dynamic divisa;
@dynamic supply;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPaymentDataResponseAdditionalInfoElement]) ||
            ([element isEqualToString:kPaymentDataResponseMsgElement])){
            
            result = BXPOElementIdle;
        
        } else if (([element isEqualToString:kPaymentDataResponseHolderElement]) ||
                   ([element isEqualToString:kPaymentDataResponseCardsFlagElement]) ||
                   ([element isEqualToString:kPaymentDataResponseClientNameElement]) ||
                   ([element isEqualToString:kPaymentDataResponseCardCurrencyElement]) ||
                   ([element isEqualToString:kPaymentDataResponseCardTypeElement]) ||
                   ([element isEqualToString:kPaymentDataResponseCardNumberElement]) ||
                   ([element isEqualToString:kPaymentDataResponseCreditLimitElement]) ||
                   ([element isEqualToString:kPaymentDataResponseAvailableElement]) ||
                   ([element isEqualToString:kPaymentDataResponseTotalDebtSolesElement]) ||
                   ([element isEqualToString:kPaymentDataResponseTotalDebeDolarsElement]) ||
                   ([element isEqualToString:kPaymentDataResponseLastPaymentDateElement]) ||
                   ([element isEqualToString:kPaymentDataResponseMinPaymentElement]) ||
                   ([element isEqualToString:kPaymentDataResponseMonthInstalmentElement]) ||
                   ([element isEqualToString:kPaymentDataResponseMonthTotalElement]) ||
                   ([element isEqualToString:kPaymentDataResponseMinPaymentDolarElement]) ||
                   ([element isEqualToString:kPaymentDataResponseMonthInstalmentDolarElement]) ||
                   ([element isEqualToString:kPaymentDataResponseMonthTotalDolarElement]) ||
                   ([element isEqualToString:kPaymentDataResponseBeneficiaryCardElement]) ||
                   ([element isEqualToString:kPaymentDataResponseBankNameElement]) ||
                   ([element isEqualToString:kPaymentDataResponseMaxElement]) ||
                   ([element isEqualToString:kPaymentDataResponseMinElement]) ||
                   ([element isEqualToString:kPaymentDataResponseMessageRangeElement]) || 
                   ([element isEqualToString:kPaymentDataResponseDivisaElement]) || 
                   ([element isEqualToString:kPaymentDataResponseCurrencyElement]) ||
                   ([element isEqualToString:kPaymentDataResponseSupplyElement])) {
            
            result = BXPOElementString;
            
        } else if (([element isEqualToString:kPaymentDataResponsePaymentsListElement]) ||
				   ([element isEqualToString:kPaymentDataResponseAccountsListElement]) ||
                   ([element isEqualToString:kPaymentDataResponseCardsListElement]) ||
                   ([element isEqualToString:kPaymentDataResponseEmisionPlaceElement]) ||
                   ([element isEqualToString:kPaymentDataResponseAuthorizedAccountsElement]) ||
                   ([element isEqualToString:kPaymentDataResponseAccountsEnableListElement])) {
            
            result = BXPOElementCustom;
            
        }
    }
    
    return result;
    
}

/*
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Default implementation returns nil
 */
- (StructuredXMLParser *)parserForElement:(NSString *)anElement
                             namespaceURI:(NSString *)namespaceURI {
	
	StructuredXMLParser *result = [super parserForElement:anElement namespaceURI:namespaceURI];
    
	if (result == nil) {
		
		if ([anElement isEqualToString:kPaymentDataResponseAccountsListElement]) {
			
			result = [[[StringList alloc] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
													 ownNamespacesPrefixesDictionary:nil 
																	   stringToFind:@"NUMCUENTA"] autorelease];
			
		} else if ([anElement isEqualToString:kPaymentDataResponseCardsListElement]) {
			
			result = [[[StringList alloc] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                    ownNamespacesPrefixesDictionary:nil 
																	   stringToFind:@"NUMTARJETA"] autorelease];
			
		} else  if ([anElement isEqualToString:kPaymentDataResponsePaymentsListElement]) {
            
            result = [[[PaymentList alloc] initWithParentNamespacePrefixesDictionary:[self wholeNamespacesPrefixesDictionary] 
													 ownNamespacesPrefixesDictionary:nil] autorelease];
            
		} else if ([anElement isEqualToString:kPaymentDataResponseAccountsEnableListElement]) {
            
            result = [[[StringList alloc] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                    ownNamespacesPrefixesDictionary:nil 
																	   stringToFind:@"NUMEROCUENTA"] autorelease];
            
        } else if ([anElement isEqualToString:kPaymentDataResponseEmisionPlaceElement]) {
        
            result = [[[LocalityList alloc] initWithParentNamespacePrefixesDictionary:[self wholeNamespacesPrefixesDictionary] 
                                                      ownNamespacesPrefixesDictionary:nil] autorelease];
        }
		
	}
	
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the company
 *
 * @return The company
 */
- (NSString *)holder {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseHolderElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the currency
 *
 * @return The currency
 */
- (NSString *)currency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the locality
 *
 * @return The locality
 */
- (NSString *)locality {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseLocalityElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the phone Number
 *
 * @return The phone Number
 */
- (NSString *)phoneNumber {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponsePhoneNumberElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the agreement
 *
 * @return The agreement
 */
- (BOOL)cardsFlag {
    
    BOOL result = NO;
    
    NSString *value = (NSString *)[self objectForElement:kPaymentDataResponseCardsFlagElement
                                            namespaceURI:@""
                                                 atIndex:0];
    
    if ([value isEqualToString:@"S"]) {
        result = YES;
    }
    
    return result;
    
}

/*
 * Returns the payment list
 *
 * @return The payment list
 */
- (PaymentList *)paymentList {
        
    PaymentList *result = (PaymentList *)[self objectForElement:kPaymentDataResponsePaymentsListElement
                                                  namespaceURI:@""
                                                       atIndex:0];
    
    return result;
    
}

/**
 * Returns the numberAccountList value.
 *
 * @return The numberAccountList value.
 */
- (StringList *)numberAccountList {
	
	StringList *result = (StringList *)[self objectForElement:kPaymentDataResponseAccountsListElement
												   namespaceURI:@""
														atIndex:0];
    
    return result;
	
}

/**
 * Returns the numberAccountList value.
 *
 * @return The numberAccountList value.
 */
- (StringList *)numberAccountEnabledList {
	
	StringList *result = (StringList *)[self objectForElement:kPaymentDataResponseAccountsEnableListElement
                                                 namespaceURI:@""
                                                      atIndex:0];
    
    return result;
	
}

/**
 * Returns the numberCardList value.
 *
 * @return The numberCardList value.
 */
- (StringList *)numberCardList {
	
	StringList *result = (StringList *)[self objectForElement:kPaymentDataResponseCardsListElement
                                                 namespaceURI:@""
                                                      atIndex:0];
    
    return result;
	
}

/*
 * Returns the client name
 *
 * @return The client name
 */
- (NSString *)clientName {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseClientNameElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the card currency
 *
 * @return The card currency
 */
- (NSString *)cardCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseCardCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the card type
 *
 * @return The card type
 */
- (NSString *)cardType {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseCardTypeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the card number
 *
 * @return The card number
 */
- (NSString *)cardNumber {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseCardNumberElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the credit limit
 *
 * @return The credit limit
 */
- (NSString *)creditLimit {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseCreditLimitElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the available amount
 *
 * @return The available amount
 */
- (NSString *)availableAmount {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseAvailableElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the total debt in soles
 *
 * @return The total debt in soles
 */
- (NSString *)totalDebtSoles {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseTotalDebtSolesElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the total debt in dolars
 *
 * @return The total debt in dolars
 */
- (NSString *)totalDebtDolars {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseTotalDebeDolarsElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the last payment date
 *
 * @return The last payment date
 */
- (NSString *)lastPaymentDate {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseLastPaymentDateElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the minimum payment
 *
 * @return The minimum payment
 */
- (NSString *)minPayment {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseMinPaymentElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the month instalment
 *
 * @return The month instalment
 */
- (NSString *)monthInstalment {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseMonthInstalmentElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the month total
 *
 * @return The month total
 */
- (NSString *)monthTotal {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseMonthTotalElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}


/*
 * Returns the min payment dolar
 *
 * @return The min payment dolar
 */
- (NSString *)minPaymentDolar {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseMinPaymentDolarElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the month instalment in dolar
 *
 * @return The month instalment in dolar
 */
- (NSString *)monthInstalmentDolar {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseMonthInstalmentDolarElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the month total in dolar
 *
 * @return The month total in dolar
 */
- (NSString *)monthTotalDolar {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseMonthTotalDolarElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the beneficiary card
 *
 * @return The beneficiary card
 */
- (NSString *)beneficiaryCard {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseBeneficiaryCardElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the bank name 
 *
 * @return The bank name
 */
- (NSString *)bankName {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseBankNameElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

///*
// * Returns the loyalty list
// *
// * @return The loyalty list
// */
//- (LocalityList *)loyaltyList {
//    
//    LocalityList *result = (LocalityList *)[self objectForElement:kPaymentDataResponseEmisionPlaceElement
//                                                   namespaceURI:@""
//                                                        atIndex:0];
//    
//    return result;
//    
//}

/*
 * Returns the loyalty list
 *
 * @return The loyalty list
 */
- (LocalityList *)emisionPlaces {
    
    LocalityList *result = (LocalityList *)[self objectForElement:kPaymentDataResponseEmisionPlaceElement
                                                     namespaceURI:@""
                                                          atIndex:0];
    
    return result;
    
}

/*
 * Returns the max
 *
 * @return The max
 */
- (NSInteger)max {
    
    NSInteger result = 0;
    
    NSString *value = (NSString *)[self objectForElement:kPaymentDataResponseMaxElement
                                            namespaceURI:@""
                                                 atIndex:0];
    result = [value integerValue];
    
    return result;
    
}

/*
 * Returns the min
 *
 * @return The min
 */
- (NSInteger)min {
    
    NSInteger result = 0;
    
    NSString *value = (NSString *)[self objectForElement:kPaymentDataResponseMinElement
                                            namespaceURI:@""
                                                 atIndex:0];
    result = [value integerValue];

    return result;
    
}

/*
 * Returns the message range
 *
 * @return The message range
 */
- (NSString *)messageRange {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseMessageRangeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}


/*
 * Returns the divisa
 *
 * @return The divisa
 */
- (NSString *)divisa {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseDivisaElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/**
 * Returns the supply number
 *
 * @return The supply number
 */
- (NSString *)supply {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDataResponseSupplyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}


@end
