//
//  PendingDocument.m
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/22/13.
//
//

#import "PendingDocument.h"

/**
 * Defines the description element tag
 */
NSString * const kPendingDocDescriptionElement = @"DESCRIPCION";

/**
 * Defines the expiration date element tag
 */
NSString * const kPendingDocExpirationDateElement = @"FECHAVENCTO";

/**
 * Defines the expiration date element tag
 */
NSString * const kPendingDocExpirationDateCustomElement = @"FECVCTO";

/**
 * Defines the Currency element tag
 */
NSString * const kPendingDocCurrencyElement = @"MONEDA";

/**
 * Defines the Currency element tag
 */
NSString * const kPendingDocMinAmountElement = @"MTOMINIMO";

/**
 * Defines the Currency element tag
 */
NSString * const kPendingDocMinAmountCustomElement = @"MONTOMINIMO";

/**
 * Defines the Min Amount Currency element tag
 */
NSString * const kPendingDocMinAmountCurrencyElement = @"DIVMTOMINIMO";

/**
 * Defines the Min Amount Currency element tag
 */
NSString * const kPendingDocMinAmountCurrencyCustomElement = @"DIVMONTOMINIMO";

/**
 * Defines the Max Amount element tag
 */
NSString * const kPendingDocMaxAmountElement = @"MTOMAXIMO";

/**
 * Defines the Max Amount element tag
 */
NSString * const kPendingDocMaxAmountCustomElement = @"MONTOMAXIMO";

/**
 * Defines the Max Amount Currency element tag
 */
NSString * const kPendingDocMaxAmountCurrencyElement = @"DIVMTOMAXIMO";

/**
 * Defines the Max Amount Currency element tag
 */
NSString * const kPendingDocMaxAmountCurrencyCustomElement = @"DIVMONTOMAXIMO";

/**
 * Defines the Total Amount element tag
 */
NSString * const kPendingDocTotalAmountElement = @"MONTO";

/**
 * Defines the Total Amount Currency element tag
 */
NSString * const kPendingDocTotalAmountCurrencyElement = @"DIVMONTO";


@implementation PendingDocument
#pragma mark -
#pragma mark properties
@dynamic currency;
@dynamic description;
@dynamic expirationDate;
@dynamic maxAmount;
@dynamic maxAmountCurrency;
@dynamic minAmount;
@dynamic minAmountCurrency;
@dynamic totalAmount;
@dynamic totalAmountCurrency;


#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPendingDocCurrencyElement]) ||
            ([element isEqualToString:kPendingDocDescriptionElement]) ||
            ([element isEqualToString:kPendingDocExpirationDateElement]) ||
            ([element isEqualToString:kPendingDocExpirationDateCustomElement]) ||
            ([element isEqualToString:kPendingDocMaxAmountCurrencyElement]) ||
            ([element isEqualToString:kPendingDocMaxAmountElement]) ||
            ([element isEqualToString:kPendingDocMinAmountCurrencyElement]) ||
            ([element isEqualToString:kPendingDocMinAmountElement]) ||
            ([element isEqualToString:kPendingDocMaxAmountCurrencyCustomElement]) ||
            ([element isEqualToString:kPendingDocMaxAmountCustomElement]) ||
            ([element isEqualToString:kPendingDocMinAmountCurrencyCustomElement]) ||
            ([element isEqualToString:kPendingDocMinAmountCustomElement]) ||
            ([element isEqualToString:kPendingDocTotalAmountElement]) ||
            ([element isEqualToString:kPendingDocTotalAmountCurrencyElement])
            ){
            
            result = BXPOElementString;
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the description
 *
 * @return The description
 */
- (NSString *)description {
    
    NSString *result = (NSString *)[self objectForElement:kPendingDocDescriptionElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the expiration date
 *
 * @return The expirationdate
 */
- (NSString *)expirationDate {
    
    NSString *result = (NSString *)[self objectForElement:kPendingDocExpirationDateElement
                                             namespaceURI:@""
                                                  atIndex:0];
    if (result == nil ||[@"" isEqualToString:result]) {
        result = (NSString *)[self objectForElement:kPendingDocExpirationDateCustomElement
                                       namespaceURI:@""
                                            atIndex:0];
    }
    
    return result;
    
}

/*
 * Returns the currency
 *
 * @return The currency
 */
- (NSString *)currency {
    
    NSString *result = (NSString *)[self objectForElement:kPendingDocCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the minimum amount
 *
 * @return The minAmount
 */
- (NSString *)minAmount {
    
    NSString *result = (NSString *)[self objectForElement:kPendingDocMinAmountElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    if (result == nil || [@"" isEqualToString:result]) {
        result = (NSString *)[self objectForElement:kPendingDocMinAmountCustomElement
                                       namespaceURI:@""
                                            atIndex:0];
    }
    
    return result;
    
}

/*
 * Returns the minimum amount currency
 *
 * @return The minAmountCurrency
 */
- (NSString *)minAmountCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPendingDocMinAmountCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    if (result == nil || [@"" isEqualToString:result]) {
        result = (NSString *)[self objectForElement:kPendingDocMinAmountCurrencyCustomElement
                                       namespaceURI:@""
                                            atIndex:0];
    }
    
    return result;
    
}

/*
 * Returns the max amount
 *
 * @return The maxAmount
 */
- (NSString *)maxAmount {
    
    NSString *result = (NSString *)[self objectForElement:kPendingDocMaxAmountElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    if (result == nil || [@"" isEqualToString:result]) {
        result = (NSString *)[self objectForElement:kPendingDocMaxAmountCustomElement
                                       namespaceURI:@""
                                            atIndex:0];
    }
    
    return result;
    
}

/*
 * Returns the max amount currency
 *
 * @return The maxAmountCurrency
 */
- (NSString *)maxAmountCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPendingDocMaxAmountCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    if (result == nil || [@"" isEqualToString:result]) {
        result = (NSString *)[self objectForElement:kPendingDocMaxAmountCurrencyCustomElement
                                       namespaceURI:@""
                                            atIndex:0];
    }
    
    return result;
    
}

/*
 * Returns the total amount
 *
 * @return The totalAmount
 */
- (NSString *)totalAmount {
    
    NSString *result = (NSString *)[self objectForElement:kPendingDocTotalAmountElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the total amount currency
 *
 * @return The totalAmountCurrency
 */
- (NSString *)totalAmountCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPendingDocTotalAmountCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}


@end
