/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "StructuredXMLParser.h"

/**
 * Defines the identification element tag
 */
extern NSString * const kBankIdentificationElement;

/**
 * Defines the bank element tag
 */
extern NSString * const kBankBankElement;

/**
 * Defines the agreement element tag
 */
extern NSString * const kBankAgreementElement;

/**
 * Defines the agreement parameters element tag
 */
extern NSString * const kBankAgreementParametersElement;


/**
 * Analyzes a tag and value
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Bank : StructuredXMLParser

/**
 * Provides read-only access to the tag
 */
@property (nonatomic, readonly, copy) NSString *identification;

/**
 * Provides read-only access to the value
 */
@property (nonatomic, readonly, copy) NSString *bank;

/**
 * Provides read-only access to the value
 */
@property (nonatomic, readonly, assign) BOOL agreement;

/**
 * Provides read-only access to the value
 */
@property (nonatomic, readonly, copy) NSString *agreementParameters;

@end

