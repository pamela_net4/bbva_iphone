//
//  Group.h
//  NXT_Peru_iPad
//
//  Created by Ricardo on 11/11/13.
//
//

#import "StructuredXMLParser.h"

/**
 * Defines the Group element tag
 */
extern NSString * const kGroupGroupElement;

/**
 * Defines the description element tag
 */
extern NSString * const kGroupDescriptionElement;

/**
 * Analyzes a tag and value
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface Group : StructuredXMLParser

/**
 * Provides read-only access to the cod of the group
 */
@property (nonatomic, readonly, copy) NSString *groupCod;

/**
 * Provides read-only access to the description of the group
 */
@property (nonatomic, readonly, copy) NSString *description;

@end
