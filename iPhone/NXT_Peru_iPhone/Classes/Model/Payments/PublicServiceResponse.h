/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "PaymentsConstants.h"
#import "StructuredStatusEnabledResponse.h"


/**
 * Defines the additional information element tag
 */
extern NSString * const kPublicServiceResponseAdditionalInfoElement;

/**
 * Defines the msg-s element tag
 */
extern NSString * const kPublicServiceResponseMsgElement;

/**
 * Defines the company element tag
 */
extern NSString * const kPublicServiceResponseCompanyElement;

/**
 * Defines the service element tag
 */
extern NSString * const kPublicServiceResponseServiceElement;

/**
 * Defines the agreement element tag
 */
extern NSString * const kPublicServiceResponseAgreementElement;

/**
 * Defines the max longitude element tag
 */
extern NSString * const kPublicServiceResponseMaxLongElement;

/**
 * Defines the min longitude element tag
 */
extern NSString * const kPublicServiceResponseMinLongElement;

/**
 * Analyzes a service list
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PublicServiceResponse : StructuredStatusEnabledResponse


/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, copy) NSString *company;

/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, copy) NSString *service;

/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, assign) BOOL agreement;

/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, assign) NSInteger maxLong;

/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, assign) NSInteger minLong;

/**
 * Provides read-only access to the payment operation type
 */
@property (nonatomic, readwrite, assign) PaymentTypeEnum paymentOperationType;


@end