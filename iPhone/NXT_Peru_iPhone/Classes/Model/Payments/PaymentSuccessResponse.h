/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "StructuredStatusEnabledResponse.h"

/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentSuccessResponseAdditionalInfoElement;

/**
 * Defines the msg-s element tag
 */
extern NSString * const kPaymentSuccessResponseMsgElement;

/**
 * Defines the operation number element tag
 */
extern NSString * const kPaymentSuccessResponseOperationStateElement;

/**
 * Defines the operation number element tag
 */
extern NSString * const kPaymentSuccessResponseOperationNumberElement;

/**
 * Defines the operation element tag
 */
extern NSString * const kPaymentSuccessResponseOperationElement;

/**
 * Defines the change type element tag
 */
extern NSString * const kPaymentSuccessResponseChangeTypeElement;

/**
 * Defines the change type currency element tag
 */
extern NSString * const kPaymentSuccessResponseChangeTypeCurrencyElement;

/**
 * Defines the total charged element tag
 */
extern NSString * const kPaymentSuccessResponseTotalChargedElement;

/**
 * Defines the total charged currency element tag
 */
extern NSString * const kPaymentSuccessResponseTotalChargedCurrencyElement;

/**
 * Defines the total paid element tag
 */
extern NSString * const kPaymentSuccessResponseTotalPaidElement;

/**
 * Defines the total paid currency element tag
 */
extern NSString * const kPaymentSuccessResponseTotalPaidCurrencyElement;

/**
 * Defines the operation date element tag
 */
extern NSString * const kPaymentSuccessResponseOperationDateElement;

/**
 * Defines the value date element tag
 */
extern NSString * const kPaymentSuccessResponseValueDateElement;

/**
 * Defines the message element tag
 */
extern NSString * const kPaymentSuccessResponseMessageElement;

/**
 * Defines the amount to charge element tag
 */
extern NSString * const kPaymentSuccessResponseAmountToChargeElement;

/**
 * Defines the amount to charge currency element tag
 */
extern NSString * const kPaymentSuccessResponseAmountToChargeCurrencyElement;

/**
 * Defines the amount to charge element tag
 */
extern NSString * const kPaymentSuccessResponseDestinationBankElement;

/**
 * Defines the amount to pay element tag
 */
extern NSString * const kPaymentSuccessResponseAmountToPayElement;

/**
 * Defines the amount to pay currency element tag
 */
extern NSString * const kPaymentSuccessResponseAmountToPayCurrencyElement;

/**
 * Defines the amount turned element tag
 */
extern NSString * const kPaymentSuccessResponseAmountTurnedElement;

/**
 * Defines the amount turned currency element tag
 */
extern NSString * const kPaymentSuccessResponseAmountTurnedCurrencyElement;

/**
 * Defines the service element tag
 */
extern NSString * const kPaymentSuccessResponseServiceElement;

/**
 * Defines the amount turned element tag
 */
extern NSString * const kPaymentSuccessResponseAmountRechargeElement;

/**
 * Defines the amount turned currency element tag
 */
extern NSString * const kPaymentSuccessResponseAmountRechargeCurrencyElement;

/**
 * Defines the service element tag
 */
extern NSString * const kPaymentSuccessResponseCustomerServiceElement;

/**
 * Defines the service element tag
 */
extern NSString * const kPaymentSuccessResponseBreakdownElement;

/**
 * Defines the service element tag
 */
extern NSString * const kPaymentSuccessResponseBalanceConsultElement;

/**
 * Defines the service element tag
 */
extern NSString * const kPaymentSuccessResponseClaimElement;

/**
 * Defines the service element tag
 */
extern NSString * const kPaymentSuccessResponseDaysUseElement;

/**
 * Defines the Account Owner element tag
 */
extern NSString * const kPaymentSuccessResponseAccountOwnerElement;

/**
 * Defines the amount payed element tag
 */
extern NSString * const kPaymentSuccessResponseAmountPayedElement;

/**
 * Defines the transaction date tag
 */
extern NSString * const kPaymentSuccessResponseTransactionDateElement;

/**
 * Defines the transaction hour tag
 */
extern NSString * const kPaymentSuccessResponseTransactionHourElement;

/**
 * Defines the supply number tag
 */
extern NSString *const kPaymentSuccessResponseResponseSupplyElement;

/**
 * Analyzes a service list
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentSuccessResponse : StructuredStatusEnabledResponse


/**
 * Provides read-only access to the operation state
 */
@property (nonatomic, readonly, copy) NSString *operationState;
/**
 * Provides read-only access to the operation number
 */
@property (nonatomic, readonly, copy) NSString *operationNumber;

/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to the change type
 */
@property (nonatomic, readonly, copy) NSString *changeType;

/**
 * Provides read-only access to the change type currency
 */
@property (nonatomic, readonly, copy) NSString *changeTypeCurrency;

/**
 * Provides read-only access to the total charged
 */
@property (nonatomic, readonly, copy) NSString *totalCharged;

/**
 * Provides read-only access to the total charged currency
 */
@property (nonatomic, readonly, copy) NSString *totalChargedCurrency;

/**
 * Provides read-only access to the total paid
 */
@property (nonatomic, readonly, copy) NSString *totalPaid;

/**
 * Provides read-only access to the total paid currency
 */
@property (nonatomic, readonly, copy) NSString *totalPaidCurrency;

/**
 * Provides read-only access to the operation date
 */
@property (nonatomic, readonly, copy) NSString *operationDate;

/**
 * Provides read-only access to the value date
 */
@property (nonatomic, readonly, copy) NSString *valueDate;

/**
 * Provides read-only access to the message
 */
@property (nonatomic, readonly, copy) NSString *message;

/**
 * Provides read-only access to the notification message
 */
@property (nonatomic, readonly, copy) NSString *notificationMessage;

/**
 * Provides read-only access to the amount to change
 */
@property (nonatomic, readonly, copy) NSString *amountToCharge;

/**
 * Provides read-only access to the amount to change currency
 */
@property (nonatomic, readonly, copy) NSString *amountToChargeCurrency;

/**
 * Provides read-only access to the amount changed
 */
@property (nonatomic, readonly, copy) NSString *amountCharged;

/**
 * Provides read-only access to the amount changed currency
 */
@property (nonatomic, readonly, copy) NSString *amountChargedCurrency;

/**
 * Provides read-only access to the message
 */
@property (nonatomic, readonly, copy) NSString *destinationBank;

/**
 * Provides read-only access to the amount to pay
 */
@property (nonatomic, readonly, copy) NSString *amountToPay;

/**
 * Provides read-only access to the amount to pay currency
 */
@property (nonatomic, readonly, copy) NSString *amountToPayCurrency;

/**
 * Provides read-only access to the amount payed
 */
@property (nonatomic, readonly, copy) NSString *amountPayed;

/**
 * Provides read-only access to the amount payed currency
 */
@property (nonatomic, readonly, copy) NSString *amountPayedCurrency;

/**
 * Provides read-only access to the amount turned
 */
@property (nonatomic, readonly, copy) NSString *amountTurned;

/**
 * Provides read-only access to the amount turned currency
 */
@property (nonatomic, readonly, copy) NSString *amountTurnedCurrency;

/**
 * Provides read-only access to the service
 */
@property (nonatomic, readonly, copy) NSString *service;

/**
 * Provides read-only access to the amount recharge
 */
@property (nonatomic, readonly, copy) NSString *amountRecharge;

/**
 * Provides read-only access to the amount recharge currency
 */
@property (nonatomic, readonly, copy) NSString *amountRechargeCurrency;

/**
 * Provides read-only access to the customer service
 */
@property (nonatomic, readonly, copy) NSString *customerService;

/**
 * Provides read-only access to the claroBreakdown
 */
@property (nonatomic, readonly, copy) NSString *claroBreakdown;

/**
 * Provides read-only access to the breakdown
 */
@property (nonatomic, readonly, copy) NSString *breakdown;

/**
 * Provides read-only access to the balance consult
 */
@property (nonatomic, readonly, copy) NSString *balanceConsult;

/**
 * Provides read-only access to the balance consult
 */
@property (nonatomic, readonly, copy) NSString *claroBalanceConsult;

/**
 * Provides read-only access to the claim
 */
@property (nonatomic, readonly, copy) NSString *claim;

/**
 * Provides read-only access to the days use
 */
@property (nonatomic, readonly, copy) NSString *daysUse;


/**
 * Provides read-only access to the network Use
 */
@property (nonatomic, readonly, copy) NSString *networkUse;


/**
 * Provides read-only access to the network use currency
 */
@property (nonatomic, readonly, copy) NSString *networkUseCurrency;

/**
 * Provides read-only access to the tax
 */
@property (nonatomic, readonly, copy) NSString *tax;

/**
 * Provides read-only access to the tax currency
 */
@property (nonatomic, readonly, copy) NSString *taxCurrency;


/**
 * Provides read-only access to the claroDaysUse
 */
@property (nonatomic, readonly, copy) NSString *claroDaysUse;

/**
 * Provides read-only access to the days use
 */
@property (nonatomic, readonly, copy) NSString *accountOwner;


/**
 * Provides read-only access to the days use
 */
@property (nonatomic, readonly, copy) NSString *cardHolder;
/**
 * Provides read-only access to the transaction date
 */
@property (nonatomic, readonly, copy) NSString *transactionDate;

/**
 * Provides read-only access to the transaction hour
 */
@property (nonatomic, readonly, copy) NSString *transactionHour;

/**
 * Provides read-only access to the supplies
 */
@property (nonatomic, readonly, copy) NSString *supply;

@end