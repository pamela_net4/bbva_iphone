/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "StringList.h"

/**
 * Defines the string element tag
 */
NSString * const kElement = @"E";

@implementation StringList

#pragma mark -
#pragma mark Properties

@dynamic stringList;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Designated intialized. It initializes a StructuredXMLParser instance with its parent namespace prefixes dictionary
 */
- (id)initWithParentNamespacePrefixesDictionary:(NSDictionary *)aParentNamespacesPrefixesDictionary
ownNamespacesPrefixesDictionary:(NSDictionary *)ownNamespacesPrefixesDictionary stringToFind:(NSString *)stringToFind {
    
	self = [super initWithParentNamespacePrefixesDictionary:aParentNamespacesPrefixesDictionary 
							ownNamespacesPrefixesDictionary:ownNamespacesPrefixesDictionary];
	
	if (self) {
		
		stringElement_ = [stringToFind copy];
		
	}
	
	return self;
    
}

#pragma mark -
#pragma mark BaseXMLParserObject selectors


/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
				  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if ([element isEqualToString:kElement]) {
            
            result = BXPOElementIdle;
            
        } else if ([element isEqualToString:stringElement_]) {
            
            result = BXPOElementString;
            
		}
		
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the string list
 *
 * @return The string list
 */
- (NSArray *)stringList {
    
    NSArray *result = [self arrayForElement:stringElement_
                               namespaceURI:@""];
	
    return result;
    
}

@end
