/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "ServiceList.h"

#import "Service.h"

/**
 * Defines the service element tag
 */
NSString * const kServiceListServiceElement = @"E";

#pragma mark -
#pragma mark Properties

@implementation ServiceList

@dynamic serviceList;

#pragma mark -
#pragma mark BaseXMLParserObject selectors


/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if ([element isEqualToString:kServiceListServiceElement]) {
            
            result = BXPOElementCustom;
            
        }
		
    }
    
    return result;
    
}

/**
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Returns the corresponing parser for the transaction list and nil for the rest
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element parser
 */
- (StructuredXMLParser *)parserForElement:(NSString *)element
                             namespaceURI:(NSString *)namespaceURI {
    
    StructuredXMLParser *result = [super parserForElement:element
                                             namespaceURI:namespaceURI];
    
    if (result == nil) {
        
        if ([element isEqualToString:kServiceListServiceElement]) {
            
            result = [[[Service allocWithZone:[self zone]] initWithParentNamespacePrefixesDictionary:[self wholeNamespacesPrefixesDictionary]
                                                                  ownNamespacesPrefixesDictionary:nil] autorelease];
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the service list
 *
 * @return The service list
 */
- (NSArray *)serviceList {
    
    NSArray *result = [self arrayForElement:kServiceListServiceElement
                               namespaceURI:@""];
        
    return result;
    
}


@end
