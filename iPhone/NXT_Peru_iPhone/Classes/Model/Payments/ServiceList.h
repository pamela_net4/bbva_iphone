/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "StructuredXMLParser.h"

/**
 * Defines the additional information element tag
 */
extern NSString * const kServiceListAdditionalInfoElement;

/**
 * Defines the serviceList element tag
 */
extern NSString * const kServiceListElement;

/**
 * Defines the serviceList element tag
 */
extern NSString * const kServiceListServiceElement;

/**
 * Analyzes a service list
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ServiceList : StructuredXMLParser


/**
 * Provides read-only access to the service list
 */
@property (nonatomic, readwrite, retain) NSArray *serviceList;

@end
