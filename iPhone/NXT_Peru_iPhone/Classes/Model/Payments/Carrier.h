/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>

#import "StructuredXMLParser.h"


/**
 * Defines the Movistar carrier code (in lower case)
 */
extern NSString * const kCarrierMovistarCode;

/**
 * Defines the Claro carrier code (in lower case)
 */
extern NSString * const kCarrierClaroCode;


/**
 * Defines the Movistar carrier code for notifications.
 */
extern NSString * const kCarrierMovistarCodeForNotification;

/**
 * Defines the Claro carrier code for notifications.
 */
extern NSString * const kCarrierClaroCodeForNotification;

/**
 * Defines the Nextel carrier code for notifications.
 */
extern NSString * const kCarrierNextelCodeForNotification;


/**
 * Analyzes a tag and value
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Carrier : StructuredXMLParser

/**
 * Provides read-only access to the category
 */
@property (nonatomic, readonly, copy) NSString *carrier;

/**
 * Provides read-only access to the code
 */
@property (nonatomic, readonly, copy) NSString *carrierCode;

/**
 * Provides read-only access to the max
 */
@property (nonatomic, readonly, retain) NSDecimalNumber *max;

/**
 * Provides read-only access to the min
 */
@property (nonatomic, readonly, retain) NSDecimalNumber *min;

/**
 * Provides read-only access to the message
 */
@property (nonatomic, readonly, copy) NSString *message;

/**
 * Provides read-only access to the description
 */
@property (nonatomic, readonly, copy) NSString *description;

/**
 * Provides read-only access to the carrier display name.
 */
@property (nonatomic, readonly, copy) NSString *carrierDisplayName;

@end

