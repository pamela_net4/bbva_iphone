//
//  PaymentInstitutionAndCompaniesConfirmationInformationResponse.m
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/27/13.
//
//

#import "PaymentInstitutionAndCompaniesConfirmationInformationResponse.h"
#import "OcurrencyList.h"
#import "PendingDocumentList.h"
#import "Tools.h"

/**
 * Defines the additional information element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";

/**
 * Defines the msg-s element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseMsgElement = @"MSG-S";

/**
 * Defines the pay type element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponsePayTypeElement = @"TIPOPAGO";
/**
 * Defines the pay type code element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponsePayTypeCodeElement = @"TIPOPAGOCOD";

/**
 * Defines the pay amount element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponsePayAmountElement = @"MTOPAGAR";

/**
 * Defines the pay amount currency element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponsePayAmountCurrencyElement = @"DIVMTOPAGAR";

/**
 * Defines the operation element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseOperationElement = @"OPERACION";

/**
 * Defines the account type element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseAccountPayTypeElement = @"TIPOCTA";

/**
 * Defines the currency element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseCurrencyElement = @"MONEDA";

/**
 * Defines the subject account element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseSubjectElement = @"ASUNTO";

/**
 * Defines the Companie Name element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseCompanieNameElement = @"NOMEMPRESA";

/**
 * Defines the Ocurrency list element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseOcurrencyListElement = @"LISTAOCURRENCIA";

/**
 * Defines the holder Name element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseHolderNameElement = @"NOMTITULAR";

/**
 * Defines the Doc Number element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseDocNumberElement = @"CANTDOCUMENTO";

/**
 * Defines the Document List element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseDocListElement = @"LISTADOCUMENTO";

/**
 * Defines the Coordinates element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseCoordinatesElement = @"COORDENADA";

/**
 * Defines the Seal element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseSealElement = @"SELLO";

/**
 * Defines the Disclaimer element tag
 */
NSString * const kPaymentInstitutionsConfirmationInformationResponseDisclaimerElement = @"DISCLAIMER";

@implementation PaymentInstitutionAndCompaniesConfirmationInformationResponse

@dynamic disclaimer;
@dynamic seal;
@dynamic coordinate;
@dynamic docNumber;
@dynamic payAmount;
@dynamic payAmountCurrency;
@dynamic currency;
@dynamic accountType;
@dynamic companieName;
@dynamic docList;
@dynamic holderName;
@dynamic ocurrencyList;
@dynamic operation;
@dynamic payType;
@dynamic subject;
@dynamic payTypeCode;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseAdditionalInfoElement]) ||
            ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseMsgElement])
            ) {
            
            result = BXPOElementIdle;
			
        } else if (
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseAccountPayTypeElement])  ||
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseCompanieNameElement])  ||
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseCoordinatesElement])||
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseCurrencyElement])||
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseDisclaimerElement])||
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseDocNumberElement])||
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseHolderNameElement])||
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseOperationElement])||
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponsePayAmountCurrencyElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponsePayAmountElement ]) ||
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponsePayTypeElement ]) ||
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseSealElement ]) ||
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseSubjectElement ] || [element isEqualToString:kPaymentInstitutionsConfirmationInformationResponsePayTypeCodeElement])
                   
                   ) {
            
            result = BXPOElementString;
            
        } else if ( ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseDocListElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsConfirmationInformationResponseOcurrencyListElement])
                   ) {
            
            result = BXPOElementCustom;
            
		}
		
    }
    
    return result;
    
}

/*
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Default implementation returns nil
 */
- (StructuredXMLParser *)parserForElement:(NSString *)anElement
                             namespaceURI:(NSString *)namespaceURI {
	
	StructuredXMLParser *result = [super parserForElement:anElement namespaceURI:namespaceURI];
    
	if (result == nil) {
		
		if ([anElement isEqualToString:kPaymentInstitutionsConfirmationInformationResponseDocListElement]) {
            
            result = [[[PendingDocumentList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                               ownNamespacesPrefixesDictionary:nil] autorelease];
            
        } else if ([anElement isEqualToString:kPaymentInstitutionsConfirmationInformationResponseOcurrencyListElement]) {
            
            result = [[[OcurrencyList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                        ownNamespacesPrefixesDictionary:nil] autorelease];
            
        } 
		
	}
	
    return result;
    
}

#pragma mark -
#pragma mark access methods
/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (OcurrencyList *) ocurrencyList{
    
    OcurrencyList *result = (OcurrencyList *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponseOcurrencyListElement
                                                       namespaceURI:@""
                                                            atIndex:0];
    return result;
    
}

/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (NSString *) payType{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponsePayTypeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (NSString *) payTypeCode{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponsePayTypeCodeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}


/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (NSString *) payAmount{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponsePayAmountElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (NSString *) payAmountCurrency{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponsePayAmountCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (NSString *) operation{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponseOperationElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return [Tools capitalizeFirstWordOnly:result];
    
}

/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (NSString *) accountType{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponseAccountPayTypeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (NSString *) currency{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponseCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (NSString *) subject{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponseSubjectElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (NSString *) companieName{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponseCompanieNameElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (PendingDocumentList *) docList{
    
    PendingDocumentList *result = (PendingDocumentList *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponseDocListElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (NSString *) holderName{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponseHolderNameElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (NSString *) docNumber{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponseDocNumberElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the coordinate
 *
 * @return The coordinate
 */
- (NSString *) coordinate{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponseCoordinatesElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the seal
 *
 * @return The seal
 */
- (NSString *) seal{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponseSealElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the disclaimer
 *
 * @return The disclaimer
 */
- (NSString *) disclaimer{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsConfirmationInformationResponseDisclaimerElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}


@end
