//
//  accountPayList.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/22/13.
//
//

#import "StructuredXMLParser.h"

/**
 * Defines the AccountPay element tag
 */
extern NSString * const kAccountPayListElement;

/**
 * Analyzes a AccountPay list
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface accountPayList : StructuredXMLParser

/**
 * Provides read-only access to the account list
 */
@property (nonatomic, readwrite, retain) NSArray *accountPayList;

@end
