//
//  PendingDocumentList.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/22/13.
//
//

#import "StructuredXMLParser.h"

/**
 * Defines the GroupList element tag
 */
extern NSString * const kPendingDocumentListElement;

/**
 * Analyzes a Pending document list
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface PendingDocumentList : StructuredXMLParser

/**
 * Provides read-only access to the group list
 */
@property (nonatomic, readwrite, retain) NSArray *pendingDocumentList;

@end
