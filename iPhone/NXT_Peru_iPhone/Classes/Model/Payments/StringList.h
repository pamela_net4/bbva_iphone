/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "StructuredStatusEnabledResponse.h"

/**
 * Parse a string list of data model.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface StringList : StructuredXMLParser {
	
@private
	
	/**
	 * The string element to find in parser.
	 */
	NSString *stringElement_;
	
}

/**
 * Provides read-only access to the string list
 */
@property (nonatomic, readwrite, retain) NSArray *stringList;

/**
 * Designated intialized. It initializes a StructuredXMLParser instance with its parent namespace prefixes dictionary
 *
 * @param aParentNamespacesPrefixesDictionary The parent namespaces prefixes dictionary to store
 * @param ownNamespacesPrefixesDictionary The initial own namespaces prefixes dictionary to store
 * @param stringToFind: The string to find in data model.
 * @return The initialized StructuredXMLParser instance
 */
- (id)initWithParentNamespacePrefixesDictionary:(NSDictionary *)aParentNamespacesPrefixesDictionary
				ownNamespacesPrefixesDictionary:(NSDictionary *)ownNamespacesPrefixesDictionary
                                   stringToFind:(NSString *)stringToFind;

@end
