/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "StructuredXMLParser.h"

@class ServiceDetail;

/**
 * Defines the service element tag
 */
extern NSString * const kServiceServiceElement;

/**
 * Defines the description element tag
 */
extern NSString * const kServiceDescriptionElement;

/**
 * Defines the service detail element tag
 */
extern NSString * const kServiceServiceDetailElement;

/**
 * Defines the service code element tag
 */
extern NSString * const kServiceServiceCodeElement;


/**
 * Analyzes a service list
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Service : StructuredXMLParser

/**
 * Provides read-only access to the service detail
 */
@property (nonatomic, readonly, copy) NSString *service;

/**
 * Provides read-only access to the service detail
 */
@property (nonatomic, readonly, copy) NSString *description;

/**
 * Provides read-only access to the service code
 */
@property (nonatomic, readonly, copy) NSString *serviceCode;

/**
 * Provices read-only access to the service detail list.
 */
@property (nonatomic, readonly, retain) NSArray *serviceDetailList;

@end
