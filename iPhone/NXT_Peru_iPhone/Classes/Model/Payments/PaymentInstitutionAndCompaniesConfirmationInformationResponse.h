//
//  PaymentInstitutionAndCompaniesConfirmationInformationResponse.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/27/13.
//
//

#import "StructuredStatusEnabledResponse.h"

@class OcurrencyList;
@class PendingDocumentList;

/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseAdditionalInfoElement;

/**
 * Defines the msg-s element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseMsgElement;

/**
 * Defines the pay type element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponsePayTypeElement;

/**
 * Defines the pay amount element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponsePayAmountElement;

/**
 * Defines the pay amount currency element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponsePayAmountCurrencyElement;

/**
 * Defines the operation element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseOperationElement;

/**
 * Defines the account type element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseAccountPayTypeElement;

/**
 * Defines the currency element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseCurrencyElement;

/**
 * Defines the subject account element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseSubjectElement;

/**
 * Defines the Companie Name element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseCompanieNameElement;

/**
 * Defines the Ocurrency list element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseOcurrencyListElement;

/**
 * Defines the holder Name element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseHolderNameElement;

/**
 * Defines the Doc Number element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseDocNumberElement;

/**
 * Defines the Document List element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseDocListElement;

/**
 * Defines the Coordinates element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseCoordinatesElement;

/**
 * Defines the Seal element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseSealElement;

/**
 * Defines the Disclaimer element tag
 */
extern NSString * const kPaymentInstitutionsConfirmationInformationResponseDisclaimerElement;

/**
 * The payment Institution confirmation response.
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface PaymentInstitutionAndCompaniesConfirmationInformationResponse : StructuredStatusEnabledResponse

/**
 * Provides read-only access to the Pay type
 */
@property (nonatomic, readonly, retain) NSString *payType;

/**
 * Provides read-only access to the Pay type code
 */
@property (nonatomic, readonly, retain) NSString *payTypeCode;

/**
 * Provides read-only access to the Pay amount
 */
@property (nonatomic, readonly, retain) NSString *payAmount;

/**
 * Provides read-only access to the Pay amount currency
 */
@property (nonatomic, readonly, retain) NSString *payAmountCurrency;

/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, retain) NSString *operation;

/**
 * Provides read-only access to the Account type
 */
@property (nonatomic, readonly, retain) NSString *accountType;

/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, retain) NSString *currency;

/**
 * Provides read-only access to the subject
 */
@property (nonatomic, readonly, retain) NSString *subject;

/**
 * Provides read-only access to the companie Name
 */
@property (nonatomic, readonly, retain) NSString *companieName;

/**
 * Provides read-only access to the ocurrency list
 */
@property (nonatomic, readonly, retain) OcurrencyList *ocurrencyList;

/**
 * Provides read-only access to the holder Name
 */
@property (nonatomic, readonly, retain) NSString *holderName;

/**
 * Provides read-only access to the doc number
 */
@property (nonatomic, readonly, retain) NSString *docNumber;

/**
 * Provides read-only access to the Pay type
 */
@property (nonatomic, readonly, retain) PendingDocumentList *docList;

/**
 * Provides read-only access to the Coordinate
 */
@property (nonatomic, readonly, retain) NSString *coordinate;

/**
 * Provides read-only access to the Seal
 */
@property (nonatomic, readonly, retain) NSString *seal;

/**
 * Provides read-only access to the Disclaimer
 */
@property (nonatomic, readonly, retain) NSString *disclaimer;



@end
