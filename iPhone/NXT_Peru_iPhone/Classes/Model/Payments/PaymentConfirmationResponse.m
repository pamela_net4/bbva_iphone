/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PaymentConfirmationResponse.h"

/**
 * Defines the msg-s element tag
 */
NSString * const kPaymentConfirmationResponseMsgElement = @"MSG-S";

/**
 * Defines the additional information element tag
 */
NSString * const kPaymentConfirmationResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";

/**
 * Defines the coordinate element tag
 */
NSString * const kPaymentConfirmationResponseCoordElement = @"COORDENADA";

/**
 * Defines the seal element tag
 */
NSString * const kPaymentConfirmationResponseSealElement = @"SELLO";

/**
 * Defines the amount element tag
 */
NSString * const kPaymentConfirmationResponseAmountToRechargeElement = @"IMPORTERECARGAR";

/**
 * Defines the currency element tag
 */
NSString * const kPaymentConfirmationResponseAmountToRechargeCurrencyElement = @"MONEDAIMPORTERECARGAR";

/**
 * Defines the amount element tag
 */
NSString * const kPaymentConfirmationResponseAmountToChargeElement = @"IMPORTECARGAR";

/**
 * Defines the currency element tag
 */
NSString * const kPaymentConfirmationResponseAmountToChargeCurrencyElement = @"MONEDAIMPORTECARGAR";

/**
 * Defines the disclaimer element tag
 */
NSString * const kPaymentConfirmationResponseDisclaimerElement = @"DISCLAIMER";

/**
 * Defines the amount charged element tag
 */
NSString * const kPaymentConfirmationResponseAmountChargedElement = @"IMPORTECARGADO";

/**
 * Defines the amount charged currency element tag
 */
NSString * const kPaymentConfirmationResponseAmountChargedCurrencyElement = @"MONEDAIMPORTECARGADO";

/**
 * Defines the amount to pay element tag
 */
NSString * const kPaymentConfirmationResponseAmountPaidElement = @"IMPORTEPAGADO";

/**
 * Defines the amount to pay currency element tag
 */
NSString * const kPaymentConfirmationResponseAmountPaidCurrencyElement = @"MONEDAIMPORTEPAGADO";

/**
 * Defines the network use element tag
 */
NSString * const kPaymentConfirmationResponseNetworkUseElement = @"USORED";

/**
 * Defines the network use currency element tag
 */
NSString * const kPaymentConfirmationResponseNetworkUseCurrencyElement = @"MONEDAUSORED";

/**
 * Defines the tax element tag
 */
NSString * const kPaymentConfirmationResponseTaxElement = @"COMISION";

/**
 * Defines the tax currency element tag
 */
NSString * const kPaymentConfirmationResponseTaxCurrencyElement = @"MONEDACOMISION";

/**
 * Defines the type change element tag
 */
NSString * const kPaymentConfirmationResponseTypeChangeElement = @"TIPOCAMBIO";

/**
 * Defines the operation element tag
 */
NSString * const kPaymentConfirmationResponseOperationElement = @"OPERACION";

/**
 * Defines the amount element tag
 */
NSString * const kPaymentConfirmationResponseAmountChargeElement = @"IMPORTECARGA";

/**
 * Defines the currency element tag
 */
NSString * const kPaymentConfirmationResponseAmountChargeCurrencyElement = @"MONEDAIMPORTECARGA";

/**
 * Defines the supply element tag
 */
NSString * const kPaymentConfirmationResponseSupplyElement = @"SUMINISTRO";


/**
 * Defines the supplie element tag
 */
NSString * const kPaymentConfirmationResponseBeneficiaryElement = @"TITULAR";


/**
 * Defines the supplie element tag
 */
NSString * const kPaymentConfirmationResponseBankNameElement= @"BANCO_DESTINO";


/**
 * Defines the supplie element tag
 */
NSString * const kPaymentConfirmationResponseCardPaymentElement = @"TARJ_ABONO";


@implementation PaymentConfirmationResponse

#pragma mark -
#pragma mark Properties

@dynamic coordinate;
@dynamic seal;
@dynamic amountToCharge;
@dynamic amountToChargeCurrency;
@dynamic disclaimer;
@dynamic amountCharged;
@dynamic amountChargedCurrency;
@dynamic amountPaid;
@dynamic amountPaidCurrency;
@dynamic networkUse;
@dynamic networkUseCurrency;
@dynamic tax;
@dynamic taxCurrency;
@dynamic operation;
@dynamic amountCharge;
@dynamic amountChargeCurrency;
@dynamic supply;
@dynamic beneficiary;
@dynamic bankName;
@dynamic cardPayment;
#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPaymentConfirmationResponseMsgElement]) ||
            ([element isEqualToString:kPaymentConfirmationResponseAdditionalInfoElement])){
            
            result = BXPOElementIdle;
            
        } else if (([element isEqualToString:kPaymentConfirmationResponseCoordElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseSealElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseAmountToChargeElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseAmountToChargeCurrencyElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseDisclaimerElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseAmountChargedElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseAmountChargedCurrencyElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseAmountPaidElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseAmountPaidCurrencyElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseNetworkUseElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseNetworkUseCurrencyElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseTaxElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseTaxCurrencyElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseTypeChangeElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseOperationElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseAmountChargeElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseAmountChargeCurrencyElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseAmountToRechargeElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseAmountToRechargeCurrencyElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseSupplyElement])||
                   ([element isEqualToString:kPaymentConfirmationResponseBeneficiaryElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseBankNameElement]) ||
                   ([element isEqualToString:kPaymentConfirmationResponseCardPaymentElement])) {
            
            result = BXPOElementString;
            
        }
    }
    
    return result;
    
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the coordinate
 *
 * @return The coordinate
 */
- (NSString *)coordinate {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseCoordElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the seal
 *
 * @return The seal
 */
- (NSString *)seal {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseSealElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount
 *
 * @return The amount
 */
- (NSString *)amountToCharge {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseAmountToChargeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the currency
 *
 * @return The currency
 */
- (NSString *)amountToChargeCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseAmountToChargeCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount
 *
 * @return The amount
 */
- (NSString *)amountToRecharge {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseAmountToRechargeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the currency
 *
 * @return The currency
 */
- (NSString *)amountToRechargeCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseAmountToRechargeCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the disclaimer
 *
 * @return The disclaimer
 */
- (NSString *)disclaimer {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseDisclaimerElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount charged
 *
 * @return The amount charged
 */
- (NSString *)amountCharged {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseAmountChargedElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount charged currency
 *
 * @return The amount charged currency
 */
- (NSString *)amountChargedCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseAmountChargedCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount paid
 *
 * @return The amount paid
 */
- (NSString *)amountPaid {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseAmountPaidElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount paid currency
 *
 * @return The amount paid currency
 */
- (NSString *)amountPaidCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseAmountPaidCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the network use
 *
 * @return The network use
 */
- (NSString *)networkUse {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseNetworkUseElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the network use currency
 *
 * @return The network use currency
 */
- (NSString *)networkUseCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseNetworkUseCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the tax
 *
 * @return The tax
 */
- (NSString *)tax {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseTaxElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the tax currency
 *
 * @return The tax currency
 */
- (NSString *)taxCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseTaxCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the type change
 *
 * @return The type change
 */
- (NSString *)typeChange {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseTypeChangeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the operation
 *
 * @return The operation
 */
- (NSString *)operation {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseOperationElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount
 *
 * @return The amount
 */
- (NSString *)amountCharge {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseAmountChargeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the currency
 *
 * @return The currency
 */
- (NSString *)amountChargeCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseAmountChargeCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/**
 * Returns the supply
 *
 * @return The supply
 */
- (NSString *)supply {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseSupplyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/**
 * Return the supply number
 *
 * @return The supply number
 */
- (NSString *)beneficiary{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseBeneficiaryElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}


/**
 * Return the supply number
 *
 * @return The supply number
 */
- (NSString *)bankName{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseBankNameElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}


/**
 * Return the supply number
 *
 * @return The supply number
 */
- (NSString *)cardPayment{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentConfirmationResponseCardPaymentElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}


@end
