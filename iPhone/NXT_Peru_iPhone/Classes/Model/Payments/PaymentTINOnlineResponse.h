//
//  PaymentTINOnlineResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 10/13/15.
//  Copyright © 2015 Movilok. All rights reserved.
//
#import "StructuredStatusEnabledResponse.h"

@class PaymentConfirmationResponse;



/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentTINConfirmationResponseAdditionalInfoElement;
/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentTINResponseTypeFlowInfoElement;

/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentTINResponseComissionNInfoElement;

/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentTINResponseComissionLInfoElement;

/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentTINResponseTotalPaymentLInfoElement;

/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentTINResponseTotalPaymentNInfoElement;


/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentTINResponseDisclaimerInfoElement;


@interface PaymentTINOnlineResponse : StructuredStatusEnabledResponse

/**
 * Provides read-only access to the type flow
 */
@property (nonatomic, readonly, copy) NSString *typeFlow;

@property (nonatomic, readonly, copy) NSString *commissionN;

@property (nonatomic, readonly, copy) NSString *commissionL;

@property (nonatomic, readonly, copy) NSString *totalPayN;

@property (nonatomic, readonly, copy) NSString *totalPayL;

@property (nonatomic, readonly, copy) NSString *disclaimer;


@property (nonatomic, readonly, copy) PaymentConfirmationResponse *additionalInformation;
@end
