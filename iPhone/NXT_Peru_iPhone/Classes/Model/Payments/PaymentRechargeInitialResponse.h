/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "StructuredStatusEnabledResponse.h"

@class StringList;
@class CarriersList;

/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentRechargeResponseAdditionalInfoElement;

/**
 * Defines the msg-s element tag
 */
extern NSString * const kPaymentRechargeResponseMsgElement;

/**
 * Defines the agreement element tag
 */
extern NSString * const kPaymentRechargeResponseAgreementElement;

/**
 * Defines the max longitude element tag
 */
extern NSString * const kPaymentRechargeResponseMaxLongElement;

/**
 * Defines the min longitude element tag
 */
extern NSString * const kPaymentRechargeResponseMinLongElement;

/**
 * Defines the currency element tag
 */
extern NSString * const kPaymentRechargeResponseCurrencyElement;

/**
 * Defines the accounts element tag
 */
extern NSString * const kPaymentRechargeResponseAccountsElement;

/**
 * Defines the carriers element tag
 */
extern NSString * const kPaymentRechargeResponseCarriersElement;

/**
 * The payment recharge initial response.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentRechargeInitialResponse : StructuredStatusEnabledResponse

/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, assign) BOOL agreement;

/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, assign) NSInteger maxLong;

/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, assign) NSInteger minLong;

/**
 * Provides read-only access to the account list
 */
@property (nonatomic, readonly, retain) StringList *numberAccountList;

/**
 * Provides read-only access to the carrier list
 */
@property (nonatomic, readonly, retain) CarriersList *carrierList;

@end
