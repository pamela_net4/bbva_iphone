/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "CardType.h"

#import "BankList.h"

/**
 * Defines the category element tag 
 */
NSString * const kCardTypeCategoryElement = @"CATEGORIA";

/**
 * Defines the banks element tag
 */
NSString * const kCardTypeBanksElement = @"BANCOS";

/**
 * Defines the num element tag
 */
NSString * const kCardTypeNumElement = @"NUMEROTARJETA";

/**
 * Defines the type element tag
 */
NSString * const kCardTypeTypeElement = @"TIPOTARJETA";

/**
 * Defines the currency element tag
 */
NSString * const kCardTypeCurrencyElement = @"MONEDA";

/**
 * Defines the param tag
 */
NSString * const kCardTypeParamCardElement = @"PARAMTARJETA";

#pragma mark -
#pragma mark Properties

@implementation CardType

@dynamic category;
@dynamic bankList;
@dynamic currency;
@dynamic num;
@dynamic param;
@dynamic type;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if ([element isEqualToString:kCardTypeCategoryElement] ||
            [element isEqualToString:kCardTypeCurrencyElement] ||
            [element isEqualToString:kCardTypeNumElement] ||
            [element isEqualToString:kCardTypeParamCardElement] ||
            [element isEqualToString:kCardTypeTypeElement]
            ) {
            
            result = BXPOElementString;
        
        } else if ([element isEqualToString:kCardTypeBanksElement]) {
        
            result = BXPOElementCustom;
            
        }
        
    }
    
    return result;
    
}

/**
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Returns the corresponing parser for the transaction list and nil for the rest
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element parser
 */
- (StructuredXMLParser *)parserForElement:(NSString *)element
                             namespaceURI:(NSString *)namespaceURI {
    
    StructuredXMLParser *result = [super parserForElement:element
                                             namespaceURI:namespaceURI];
    
    if (result == nil) {
        
        if ([element isEqualToString:kCardTypeBanksElement]) {
            
            result = [[[BankList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                    ownNamespacesPrefixesDictionary:nil] autorelease];
            
        }
        
    }
    
    return result;
    
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the category 
 *
 * @return The category 
 */
- (NSString *)category {
    
    NSString *result = (NSString *)[self objectForElement:kCardTypeCategoryElement
                                             namespaceURI:@""
                                                  atIndex:0];
        
    return result;
    
}

/*
 * Returns the bankList 
 *
 * @return The bankList 
 */
- (BankList *)bankList {

    BankList *result = (BankList *)[self objectForElement:kCardTypeBanksElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the num
 *
 * @return The num
 */
- (NSString *)num {
    
    NSString *result = (NSString *)[self objectForElement:kCardTypeNumElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the type
 *
 * @return The type
 */
- (NSString *)type {
    
    NSString *result = (NSString *)[self objectForElement:kCardTypeTypeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the currency
 *
 * @return The currency
 */
- (NSString *)currency {
    
    NSString *result = (NSString *)[self objectForElement:kCardTypeCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the param
 *
 * @return The param
 */
- (NSString *)param {
    
    NSString *result = (NSString *)[self objectForElement:kCardTypeParamCardElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the account Type
 *
 * @return The accountType
 */
- (NSString *)accountType {
    
    NSString *result = (NSString *)[self objectForElement:kCardTypeTypeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the subject
 *
 * @return The accountsubjectType
 */
- (NSString *)subject {
    
    NSString *result = (NSString *)[self objectForElement:kCardTypeNumElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the dispo Amount
 *
 * @return The dispoAmount
 */
- (NSString *)availableBalanceString {
    
    NSString *result = (NSString *)[self objectForElement:kCardTypeParamCardElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the account number
 *
 * @return The number
 */
- (NSString *)number {
    
    NSString *result = (NSString *)[self objectForElement:kCardTypeNumElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}




@end
