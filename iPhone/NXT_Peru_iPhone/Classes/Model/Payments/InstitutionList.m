//
//  InstitutionList.m
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/17/13.
//
//

#import "InstitutionList.h"
#import "Institution.h"

/**
 * Defines the institution list element tag
 */
NSString * const kInstitutionListElement = @"E";

@implementation InstitutionList

#pragma mark -
#pragma mark Properties
@dynamic institutionList;

#pragma mark -
#pragma mark BaseXMLParserObject selectors


/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if ([element isEqualToString:kInstitutionListElement]) {
            
            result = BXPOElementCustom;
            
        }
    }
    
    return result;
    
}

/**
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Returns the corresponing parser for the transaction list and nil for the rest
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element parser
 */
- (StructuredXMLParser *)parserForElement:(NSString *)element
                             namespaceURI:(NSString *)namespaceURI {
    
    StructuredXMLParser *result = [super parserForElement:element
                                             namespaceURI:namespaceURI];
    
    if (result == nil) {
        
        if ([element isEqualToString:kInstitutionListElement]) {
            
            result = [[[Institution allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                 ownNamespacesPrefixesDictionary:nil] autorelease];
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the institution list
 *
 * @return The institution list
 */
- (NSArray *)institutionList {
    
    NSArray *result = [self arrayForElement:kInstitutionListElement
                               namespaceURI:@""];
    
    return result;
    
}

@end
