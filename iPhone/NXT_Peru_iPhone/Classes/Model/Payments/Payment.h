/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "StructuredXMLParser.h"

@class TagAndValue;

/**
 * Defines the id element tag
 */
extern NSString * const kPaymentIdElement;

/**
 * Defines the receipt element tag
 */
extern NSString * const kPaymentReceiptElement;

/**
 * Defines the date element tag
 */
extern NSString * const kPaymentDateElement;

/**
 * Defines the state element tag
 */
extern NSString * const kPaymentStateElement;

/**
 * Defines the amount element tag
 */
extern NSString * const kPaymenAmountElement;

/**
 * Defines the type element tag
 */
extern NSString * const kPaymentTypeElement;

/**
 * Defines the due date element tag
 */
extern NSString * const kPaymentDueDateElement;

/**
 * Defines the ticket element tag
 */
extern NSString * const kPaymentTicketElement;


/**
 * Analyzes a service list
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Payment : StructuredXMLParser

/**
 * Provides read-only access to the idPayment
 */
@property (nonatomic, readonly, copy) NSString *idPayment;

/**
 * Provides read-only access to the receipt
 */
@property (nonatomic, readonly, retain) TagAndValue *receipt;

/**
 * Provides read-only access to the date
 */
@property (nonatomic, readonly, retain) TagAndValue *date;

/**
 * Provides read-only access to the date as a date.
 */
@property (nonatomic, readonly, retain) NSDate *dateAsDate;

/**
 * Provides read-only access to the state
 */
@property (nonatomic, readonly, copy) NSString *state;

/**
 * Provides read-only access to the amount
 */
@property (nonatomic, readonly, copy) NSString *amount;

/**
 * Provides read-only access to the type
 */
@property (nonatomic, readonly, copy) NSString *type;

/**
 * Provides read-only access to the dueDate
 */
@property (nonatomic, readonly, copy) NSString *dueDate;

/**
 * Provides read-only access to the due date as a string.
 */
@property (nonatomic, readonly, copy) NSDate *dueDateAsDate;

/**
 * Provides read-only access to the ticket
 */
@property (nonatomic, readonly, copy) NSString *ticket;


/**
 * Compares two payments taking into account their dates. Payment date is compared, taking into account that it can be
 * a dd/MM/yy date format or a dd/MM/yyyy format.
 *
 * @param otherPayment The other payment to compare to.
 * @return NSOrderedAscending when receiving date is earlier than the other payment, NSOrderedSame when they have both
 * the same date, NSOrderedDescending otherwise.
 */
- (NSComparisonResult)compareDateWithPayment:(NSObject *)otherPayment;

/**
 * Compares two payments taking into account their due dates. Payment due date is compared, taking into account that it can be
 * a dd/MM/yy date format or a dd/MM/yyyy format.
 *
 * @param otherPayment The other payment to compare to.
 * @return NSOrderedAscending when receiving due date is earlier than the other payment, NSOrderedSame when they have both
 * the same due date, NSOrderedDescending otherwise.
 */
- (NSComparisonResult)compareDueDateWithPayment:(NSObject *)otherPayment;

@end
