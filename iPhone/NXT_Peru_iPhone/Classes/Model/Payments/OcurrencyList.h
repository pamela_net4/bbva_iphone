//
//  OcurrencyList.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/22/13.
//
//

#import "StructuredXMLParser.h"

/**
 * Defines the OcurrencyList element tag
 */
extern NSString * const kOcurrencyListElement;

/**
 * Analyzes a ocurrency list
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface OcurrencyList : StructuredXMLParser

/**
 * Provides read-only access to the ocurrency list
 */
@property (nonatomic, readwrite, retain) NSArray *ocurrencyList;

@end
