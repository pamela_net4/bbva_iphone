/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "Payment.h"

#import "TagAndValue.h"
#import "Tools.h"


/**
 * Defines the id element tag
 */
NSString * const kPaymentIdElement = @"IDPAGO";

/**
 * Defines the receipt element tag
 */
NSString * const kPaymentReceiptElement = @"RECIBO";

/**
 * Defines the date element tag
 */
NSString * const kPaymentDateElement = @"FECHA";

/**
 * Defines the service detail element tag
 */
NSString * const kPaymentStateElement = @"MONEDA";

/**
 * Defines the service detail element tag
 */
NSString * const kPaymenAmountElement = @"IMPORTE";

/**
 * Defines the type element tag
 */
NSString * const kPaymentTypeElement = @"TIPO";

/**
 * Defines the due date element tag
 */
NSString * const kPaymentDueDateElement = @"FECHAVENCIMIENTO";

/**
 * Defines the ticket element tag
 */
NSString * const kPaymentTicketElement = @"BOLETA";

#pragma mark -
#pragma mark Properties

@implementation Payment

@dynamic idPayment;
@dynamic receipt;
@dynamic date;
@dynamic dateAsDate;
@dynamic state;
@dynamic amount;
@dynamic type;
@dynamic dueDate;
@dynamic dueDateAsDate;
@dynamic ticket;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPaymentIdElement]) ||
            ([element isEqualToString:kPaymentStateElement]) ||
            ([element isEqualToString:kPaymenAmountElement]) ||
            ([element isEqualToString:kPaymentTypeElement]) ||
            ([element isEqualToString:kPaymentDueDateElement]) ||
            ([element isEqualToString:kPaymentTicketElement])) {
            
            result = BXPOElementString;
        
        } else if (([element isEqualToString:kPaymentReceiptElement]) ||
                   ([element isEqualToString:kPaymentDateElement])) {
            
            result = BXPOElementCustom;
            
        }
    }
    
    return result;
    
}

/**
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Returns the corresponing parser for the transaction list and nil for the rest
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element parser
 */
- (StructuredXMLParser *)parserForElement:(NSString *)element
                             namespaceURI:(NSString *)namespaceURI {
    
    StructuredXMLParser *result = [super parserForElement:element
                                             namespaceURI:namespaceURI];
    
    if (result == nil) {
        
        if (([element isEqualToString:kPaymentReceiptElement]) ||
            ([element isEqualToString:kPaymentDateElement])) {
            
            result = [[[TagAndValue allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                  ownNamespacesPrefixesDictionary:nil] autorelease];
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Comparison selectors

/*
 * Compares two payments taking into account their dates. Payment date is compared, taking into account that it can be
 * a dd/MM/yy date format or a dd/MM/yyyy format.
 */
- (NSComparisonResult)compareDateWithPayment:(NSObject *)otherPayment {
    
    NSComparisonResult result = NSOrderedAscending;
    
    if ((otherPayment != nil) && ([otherPayment isKindOfClass:[Payment class]])) {
        
        NSString *ownDateString = [[self date] value];
        NSString *otherDateString = [[(Payment *)otherPayment date] value];
        
        NSDate *ownDate = [Tools dateFromServerString:ownDateString];
        NSDate *otherDate = [Tools dateFromServerString:otherDateString];
        
        if ((ownDate == nil) && (otherDate == nil)) {
            
            result = NSOrderedSame;
            
        } else if ((ownDate == nil) && (otherDate != nil)) {
            
            result = NSOrderedDescending;
            
        } else {
            
            result = [ownDate compare:otherDate];
            
        }
        
    }
    
    return result;
    
}

/*
 * Compares two payments taking into account their due dates. Payment due date is compared, taking into account that it can be
 * a dd/MM/yy date format or a dd/MM/yyyy format.
 */
- (NSComparisonResult)compareDueDateWithPayment:(NSObject *)otherPayment {
    
    NSComparisonResult result = NSOrderedAscending;
    
    if ((otherPayment != nil) && ([otherPayment isKindOfClass:[Payment class]])) {
        
        NSString *ownDueDateString = [self dueDate];
        NSString *otherDueDateString = [(Payment *)otherPayment dueDate];
        
        NSDate *ownDueDate = [Tools dateFromServerString:ownDueDateString];
        NSDate *otherDueDate = [Tools dateFromServerString:otherDueDateString];
        
        if ((ownDueDate == nil) && (otherDueDate == nil)) {
            
            result = NSOrderedSame;
            
        } else if ((ownDueDate == nil) && (otherDueDate != nil)) {
            
            result = NSOrderedDescending;
            
        } else {
            
            result = [ownDueDate compare:otherDueDate];
            
        }

    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the idPayment 
 *
 * @return The idPayment 
 */
- (NSString *)idPayment {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentIdElement
                                             namespaceURI:@""
                                                  atIndex:0];
        
    return result;
    
}

/*
 * Returns the receipt 
 *
 * @return The receipt 
 */
- (TagAndValue *)receipt {
    
    TagAndValue *result = (TagAndValue *)[self objectForElement:kPaymentReceiptElement
                                                   namespaceURI:@""
                                                        atIndex:0];
    return result;
    
}

/*
 * Returns the date
 *
 * @return The date
 */
- (TagAndValue *)date {
    
    TagAndValue *result = (TagAndValue *)[self objectForElement:kPaymentDateElement
                                                   namespaceURI:@""
                                                        atIndex:0];
    
    return result;
    
}

/*
 * Returns the date as a date.
 *
 * @return The date as date.
 */
- (NSDate *)dateAsDate {
    
    NSDate *result = nil;
    
    NSString *dateAsString = [[self date] value];
    
    result = [Tools dateFromServerString:dateAsString];
    
    return result;
    
}

/*
 * Returns the amount 
 *
 * @return The amount 
 */
- (NSString *)amount {
    
    NSString *result = (NSString *)[self objectForElement:kPaymenAmountElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the state 
 *
 * @return The state 
 */
- (NSString *)state {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentStateElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the type 
 *
 * @return The type 
 */
- (NSString *)type {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentTypeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the dueDate 
 *
 * @return The dueDate 
 */
- (NSString *)dueDate {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentDueDateElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the due date as a date.
 *
 * @return The due date as a date.
 */
- (NSDate *)dueDateAsDate {
    
    NSDate *result = nil;
    
    NSString *dueDateAsString = [self dueDate];
    
    result = [Tools dateFromServerString:dueDateAsString];
    
    return result;

}

/*
 * Returns the ticket 
 *
 * @return The ticket 
 */
- (NSString *)ticket {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentTicketElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

@end
