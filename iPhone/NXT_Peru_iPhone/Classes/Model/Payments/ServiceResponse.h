/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "StructuredStatusEnabledResponse.h"

@class ServiceList;

/**
 * Defines the additional information element tag
 */
extern NSString * const kServiceResponseAdditionalInfoElement;

/**
 * Defines the msg-s element tag
 */
extern NSString * const kServiceResponseMsgElement;

/**
 * Analyzes a service list
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ServiceResponse : StructuredStatusEnabledResponse


/**
 * Provides read-only access to the service list
 */
@property (nonatomic, readonly, retain) ServiceList *serviceList;

@end
