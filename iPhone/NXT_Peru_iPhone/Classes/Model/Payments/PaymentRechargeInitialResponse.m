/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PaymentRechargeInitialResponse.h"

#import "CarrierList.h"
#import "StringList.h"

/**
 * Defines the additional information element tag
 */
NSString * const kPaymentRechargeResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";

/**
 * Defines the msg-s element tag
 */
NSString * const kPaymentRechargeResponseMsgElement = @"MSG-S";

/**
 * Defines the agreement element tag
 */
NSString * const kPaymentRechargeResponseAgreementElement = @"ESCONVENIO";

/**
 * Defines the max longitude element tag
 */
NSString * const kPaymentRechargeResponseMaxLongElement = @"LONGMAXSUMI";

/**
 * Defines the min longitude element tag
 */
NSString * const kPaymentRechargeResponseMinLongElement = @"LONGMINSUMI";

/**
 * Defines the Currency element tag
 */
NSString * const kPaymentRechargeResponseCurrencyElement = @"MONEDA";

/**
 * Defines the accounts element tag
 */
NSString * const kPaymentRechargeResponseAccountsElement = @"CUENTAS";

/**
 * Defines the carriers element tag
 */
NSString * const kPaymentRechargeResponseCarriersElement = @"OPERADORES";

#pragma mark -
#pragma mark Properties

@implementation PaymentRechargeInitialResponse

@dynamic agreement;
@dynamic maxLong;
@dynamic minLong;
@dynamic currency;
@dynamic carrierList;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPaymentRechargeResponseMsgElement]) ||
            ([element isEqualToString:kPaymentRechargeResponseAdditionalInfoElement])) {
            
            result = BXPOElementIdle;
			
        } else if (([element isEqualToString:kPaymentRechargeResponseAgreementElement]) ||
                   ([element isEqualToString:kPaymentRechargeResponseMaxLongElement]) ||
                   ([element isEqualToString:kPaymentRechargeResponseMinLongElement]) ||
				   ([element isEqualToString:kPaymentRechargeResponseCurrencyElement])) {
            
            result = BXPOElementString;
            
        } else if (([element isEqualToString:kPaymentRechargeResponseAccountsElement]) ||
                   ([element isEqualToString:kPaymentRechargeResponseCarriersElement])) {
            
            result = BXPOElementCustom;
            
		}
		
    }
    
    return result;
    
}

/*
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Default implementation returns nil
 */
- (StructuredXMLParser *)parserForElement:(NSString *)anElement
                             namespaceURI:(NSString *)namespaceURI {
	
	StructuredXMLParser *result = [super parserForElement:anElement namespaceURI:namespaceURI];
    
	if (result == nil) {
		
		if ([anElement isEqualToString:kPaymentRechargeResponseAccountsElement]) {
			
			result = [[[StringList alloc] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
													ownNamespacesPrefixesDictionary:nil 
																	   stringToFind:@"NUMCUENTA"] autorelease];
			
		} else if ([anElement isEqualToString:kPaymentRechargeResponseCarriersElement]) {
            
            result = [[[CarrierList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                    ownNamespacesPrefixesDictionary:nil] autorelease];
            
        }
		
	}
	
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the currency
 *
 * @return The currency
 */
- (NSString *)currency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentRechargeResponseCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the agreement
 *
 * @return The agreement
 */
- (BOOL)agreement {
    
    BOOL result = NO;
    
    NSString *value = (NSString *)[self objectForElement:kPaymentRechargeResponseAgreementElement
                                            namespaceURI:@""
                                                 atIndex:0];
    
    if ([value isEqualToString:@"S"]) {
        result = YES;
    }
    
    return result;
    
}

/*
 * Returns the max long
 *
 * @return The max long
 */
- (NSInteger)maxLong {
    
    NSInteger result = 0;
    
    NSString *value = (NSString *)[self objectForElement:kPaymentRechargeResponseMaxLongElement
                                            namespaceURI:@""
                                                 atIndex:0];
    result = [value integerValue];
    
    return result;
    
}

/*
 * Returns the min long
 *
 * @return The min long
 */
- (NSInteger)minLong {
    
    NSInteger result = 0;
    
    NSString *value = (NSString *)[self objectForElement:kPaymentRechargeResponseMinLongElement
                                            namespaceURI:@""
                                                 atIndex:0];
    result = [value integerValue];
    
    return result;
    
}


/**
 * Returns the numberAccountList value.
 *
 * @return The numberAccountList value.
 */
- (StringList *)numberAccountList {
	
	StringList *result = (StringList *)[self objectForElement:kPaymentRechargeResponseAccountsElement
												 namespaceURI:@""
													  atIndex:0];
    
    return result;
	
}

/**
 * Returns the carrierList value.
 *
 * @return The carrierList value.
 */
- (CarrierList *)carrierList {
	
	CarrierList *result = (CarrierList *)[self objectForElement:kPaymentRechargeResponseCarriersElement
												 namespaceURI:@""
													  atIndex:0];
    
    return result;
	
}

@end
