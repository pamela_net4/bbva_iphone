//
//  Institution.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/17/13.
//
//

#import "StructuredXMLParser.h"

/**
 * Defines the Entity element tag
 */
extern NSString * const kInstitutionEntityElement;

/**
 * Defines the Code element tag
 */
extern NSString * const kInstitutionCodeElement;

/**
 * Defines the description element tag
 */
extern NSString * const kInstitutionDescriptionElement;

/**
 * Defines the Class element tag
 */
extern NSString * const kInstitutionClassElement;

/**
 * Defines the Flag of BD element tag
 */
extern NSString * const kInstitutionBDElement;

/**
 * Defines the Parcial Val element tag
 */
extern NSString * const kInstitutionValParcialElement;

/**
 * Defines the card indicator element tag
 */
extern NSString * const kInstitutionCardIndicatorElement;

/**
 * Defines the Currency element tag
 */
extern NSString * const kInstitutionCurrencyElement;

/**
 * Defines the first Indicator element tag
 */
extern NSString * const kInstitutionIndicator1Element;

/**
 * Defines the indicator PF element tag
 */
extern NSString * const kInstitutionIndicatorPFElement;

/**
 * Defines the Sub Class Index element tag
 */
extern NSString * const kInstitutionSubClasssIndexElement;

/**
 * Defines the Parameter element tag
 */
extern NSString * const kInstitutionParamElement;

/**
 * Analyzes a tag and value
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface Institution : StructuredXMLParser

/**
 * Provides read-only access to the cod of the institution
 */
@property (nonatomic, readonly, copy) NSString *institutionCod;

/**
 * Provides read-only access to the description of the institution
 */
@property (nonatomic, readonly, copy) NSString *institutionDescription;

/**
 * Provides read-only access to the entity of the institution
 */
@property (nonatomic, readonly, copy) NSString *institutionEntity;

/**
 * Provides read-only access to the class of the institution
 */
@property (nonatomic, readonly, copy) NSString *institutionClass;

/**
 * Provides read-only access to the flag of the institution BD
 */
@property (nonatomic, readonly, copy) NSString *institutionHaveBD;

/**
 * Provides read-only access to the partial value of the institution
 */
@property (nonatomic, readonly, copy) NSString *institutionValParcial;

/**
 * Provides read-only access to the card indicator of the institution
 */
@property (nonatomic, readonly, copy) NSString *institutionCardIndicator;

/**
 * Provides read-only access to the currency of the institution
 */
@property (nonatomic, readonly, copy) NSString *institutionCurrency;

/**
 * Provides read-only access to the first indicator of the institution
 */
@property (nonatomic, readonly, copy) NSString *institutionIndicator1;

/**
 * Provides read-only access to the indicator PF of the institution
 */
@property (nonatomic, readonly, copy) NSString *institutionIndicatorPF;

/**
 * Provides read-only access to the sub class index of the institution
 */
@property (nonatomic, readonly, copy) NSString *institutionSubClassIndex;

/**
 * Provides read-only access to the parameter of the institution
 */
@property (nonatomic, readonly, copy) NSString *institutionParameter;

@end
