//
//  PaymentInstitutionsAndCompaniesInitialResponse.m
//  NXT_Peru_iPad
//
//  Created by Ricardo on 11/11/13.
//
//

#import "PaymentInstitutionsAndCompaniesInitialResponse.h"

#import "GroupList.h"
#import "InstitutionList.h"

/**
 * Defines the additional information element tag
 */
NSString * const kPaymentInstitutionsResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";

/**
 * Defines the msg-s element tag
 */
NSString * const kPaymentInstitutionsResponseMsgElement = @"MSG-S";

/**
 * Defines the groups element tag
 */
NSString * const kPaymentInstitutionsResponseGroupsElement = @"AGRUPACION";

/**
 * Defines the Institutions element tag
 */
NSString * const kPaymentInstitutionsResponseInstitutionsElement = @"CONVENIOS";

/**
 * Defines the Next element tag
 */
NSString * const kPaymentInstitutionsResponseNextElement = @"SIGUIENTE";

/**
 * Defines the Next Movement element tag
 */
NSString * const kPaymentInstitutionsResponseNextMovementElement = @"SIGUIENTEMOVIMIENTO";

/**
 * Defines the Pagination indicator element tag
 */
NSString * const kPaymentInstitutionsResponsePagIndicatorElement = @"INDPAGINACION";

/**
 * Defines the Search Argument element tag
 */
NSString * const kPaymentInstitutionsResponseSearchArgElement = @"ARGBUSQUEDA";

/**
 * Defines the Last Description element tag
 */
NSString * const kPaymentInstitutionsResponseLastDescriptionElement = @"ULTDESCRIPCION";

/**
 * Defines the Next element back tag
 */
NSString * const kPaymentInstitutionsResponseBackElement = @"ATRAS";

/**
 * Defines the Back Next Movement element tag
 */
NSString * const kPaymentInstitutionsResponseNextMovementToBackElement = @"SIGUIENTEMOVIMIENTOATRAS";

/**
 * Defines the Back Pagination indicator element tag
 */
NSString * const kPaymentInstitutionsResponseBackPagIndicatorElement = @"INDPAGINACIONATRAS";

/**
 * Defines the Back Search Argument element tag
 */
NSString * const kPaymentInstitutionsResponseBackSearchArgElement = @"ARGBUSQUEDAATRAS";

/**
 * Defines the Back Last Description element tag
 */
NSString * const kPaymentInstitutionsResponseBackLastDescriptionElement = @"ULTDESCRIPCIONATRAS";


@implementation PaymentInstitutionsAndCompaniesInitialResponse


#pragma mark -
#pragma mark Properties

@dynamic groupList;
@dynamic institutionList;
@dynamic hasNext;
@dynamic nextMovement;
@dynamic pagIndicator;
@dynamic searchArg;
@dynamic lastDescription;
@dynamic hasBack;
@dynamic backNextMovement;
@dynamic backPagIndicator;
@dynamic backSearchArg;
@dynamic backLastDescription;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPaymentInstitutionsResponseMsgElement]) ||
            ([element isEqualToString:kPaymentInstitutionsResponseAdditionalInfoElement])) {
            
            result = BXPOElementIdle;
			
        } else if ( ([element isEqualToString:kPaymentInstitutionsResponseGroupsElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsResponseInstitutionsElement])) {
            
            result = BXPOElementCustom;
            
		} else if (([element isEqualToString:kPaymentInstitutionsResponseNextElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsResponseNextMovementElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsResponsePagIndicatorElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsResponseSearchArgElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsResponseLastDescriptionElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsResponseBackElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsResponseNextMovementToBackElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsResponseBackPagIndicatorElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsResponseBackSearchArgElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsResponseBackLastDescriptionElement])
                   ){
            result = BXPOElementString;
        }
		
    }
    
    return result;
    
}

/*
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Default implementation returns nil
 */
- (StructuredXMLParser *)parserForElement:(NSString *)anElement
                             namespaceURI:(NSString *)namespaceURI {
	
	StructuredXMLParser *result = [super parserForElement:anElement namespaceURI:namespaceURI];
    
	if (result == nil) {
		
		if ([anElement isEqualToString:kPaymentInstitutionsResponseGroupsElement]) {
            
            result = [[[GroupList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                     ownNamespacesPrefixesDictionary:nil] autorelease];
            
        }
        if ([anElement isEqualToString:kPaymentInstitutionsResponseInstitutionsElement]) {
            
            result = [[[InstitutionList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                     ownNamespacesPrefixesDictionary:nil] autorelease];
            
        }
		
	}
	
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors


/**
 * Returns the GroupList value.
 *
 * @return The groupList value.
 */
- (GroupList *)groupList {
	
	GroupList *result = (GroupList *)[self objectForElement:kPaymentInstitutionsResponseGroupsElement
                                               namespaceURI:@""
                                                    atIndex:0];
    
    return result;
	
}

/**
 * Returns the InstitutionList value.
 *
 * @return The InstitutionList value.
 */
- (InstitutionList *)institutionList {
	
	InstitutionList *result = (InstitutionList *)[self objectForElement:kPaymentInstitutionsResponseInstitutionsElement
                                               namespaceURI:@""
                                                    atIndex:0];
    
    return result;
	
}

/*
 * Returns the next
 *
 * @return The next
 */
- (BOOL)hasNext{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsResponseNextElement                                             namespaceURI:@""
                                                  atIndex:0];
    BOOL resultValue = FALSE;
    
    if (result != nil &&
        ([@"s" isEqualToString:[result lowercaseString]] || [@"si" isEqualToString:[result lowercaseString]])) {
        resultValue = TRUE;
    }
    
    return result;
    
}

/*
 * Returns the next Movement
 *
 * @return The nextMovement
 */
- (NSString *)nextMovement {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsResponseNextMovementElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the pagination indicator
 *
 * @return The pagIndicator
 */
- (NSString *)pagIndicator {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsResponsePagIndicatorElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the Search argument
 *
 * @return The searchArg
 */
- (NSString *)searchArg {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsResponseSearchArgElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the last description of the search
 *
 * @return The lastDescription
 */
- (NSString *)lastDescription {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsResponseLastDescriptionElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the back
 *
 * @return The back
 */
- (BOOL)hasBack{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsResponseBackElement                                             namespaceURI:@""
                                                  atIndex:0];
    BOOL resultValue = FALSE;
    
    if (result != nil &&
        ([@"s" isEqualToString:[result lowercaseString]] || [@"si" isEqualToString:[result lowercaseString]])) {
        resultValue = TRUE;
    }
    
    return result;
    
}

/*
 * Returns the next Movement
 *
 * @return The nextMovement
 */
- (NSString *)backNextMovement {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsResponseNextMovementToBackElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the pagination indicator
 *
 * @return The pagIndicator
 */
- (NSString *)backPagIndicator {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsResponseBackPagIndicatorElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the Search argument
 *
 * @return The searchArg
 */
- (NSString *)backSearchArg {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsResponseBackSearchArgElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the last description of the search
 *
 * @return The lastDescription
 */
- (NSString *)backLastDescription {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsResponseBackLastDescriptionElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

@end
