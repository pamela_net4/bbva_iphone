/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "Bank.h"

/**
 * Defines the identification element tag
 */
NSString * const kBankIdentificationElement = @"ID";

/**
 * Defines the bank element tag
 */
NSString * const kBankBankElement = @"BANCO";

/**
 * Defines the agreement element tag
 */
NSString * const kBankAgreementElement = @"ESCONVENIO";

/**
 * Defines the agreement parameters element tag
 */
NSString * const kBankAgreementParametersElement = @"PARAMETROSCONVENIO";

@implementation Bank

#pragma mark -
#pragma mark Properties

@dynamic identification;
@dynamic bank;
@dynamic agreement;
@dynamic agreementParameters;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kBankIdentificationElement]) ||
            ([element isEqualToString:kBankBankElement]) ||
            ([element isEqualToString:kBankAgreementElement]) ||
            ([element isEqualToString:kBankAgreementParametersElement])) {
            
            result = BXPOElementString;
        
        } 
        
    }
    
    return result;
    
}



#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the identification 
 *
 * @return The identification 
 */
- (NSString *)identification {
    
    NSString *result = (NSString *)[self objectForElement:kBankIdentificationElement
                                             namespaceURI:@""
                                                  atIndex:0];
        
    return result;
    
}

/*
 * Returns the bank 
 *
 * @return The bank 
 */
- (NSString *)bank {
    
    NSString *result = (NSString *)[self objectForElement:kBankBankElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the agreement 
 *
 * @return The agreement 
 */
- (BOOL)agreement {
    
    NSString *result = (NSString *)[self objectForElement:kBankAgreementElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    BOOL value = NO;
    
    if ([result isEqualToString:@"S"]) {
        value = YES;
    }
    
    return value;
    
}

/*
 * Returns the agreement parameters 
 *
 * @return The agreement parameters 
 */
- (NSString *)agreementParameters {
    
    NSString *result = (NSString *)[self objectForElement:kBankAgreementElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}


@end
