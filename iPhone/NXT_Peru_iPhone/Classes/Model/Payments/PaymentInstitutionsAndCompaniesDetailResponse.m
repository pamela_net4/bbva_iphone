//
//  PaymentInstitutionsAndCompaniesDetailResponse.m
//  NXT_Peru_iPad
//
//  Created by Ricardo on 11/18/13.
//
//

#import "PaymentInstitutionsAndCompaniesDetailResponse.h"

#import "FieldList.h"
/**
 * Defines the additional information element tag
 */
NSString * const kPaymentInstitutionsDetailResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";

/**
 * Defines the msg-s element tag
 */
NSString * const kPaymentInstitutionsDetailResponseMsgElement = @"MSG-S";

/**
 * Defines the agreement element tag
 */
NSString * const kPaymentInstitutionsDetailResponseAgreementElement = @"CONVENIO";

/**
 * Defines the pay type element tag
 */
NSString * const kPaymentInstitutionsDetailResponsePayTypeElement = @"TIPOPAGO";

/**
 * Defines the pay type element code tag
 */
NSString * const kPaymentInstitutionsDetailResponsePayTypeCodeElement = @"TIPOPAGOCOD";

/**
 * Defines the institution element tag
 */
NSString * const kPaymentInstitutionsDetailResponseInstitutionElement = @"INSTITUCION";

/**
 * Defines the camps element tag
 */
NSString * const kPaymentInstitutionsDetailResponseFieldsElement = @"CAMPOSINGRESO";

/**
 * Defines the Array Title element tag
 */
NSString * const kPaymentInstitutionsDetailResponseArrayTitleElement = @"TITULOARREGLO";

/**
 * Defines the Data Validation element tag
 */
NSString * const kPaymentInstitutionsDetailResponseDataValidationElement = @"DATAVALIDACION";

/**
 * Defines the Module Flag element tag
 */
NSString * const kPaymentInstitutionsDetailResponseModuleElement = @"FLAGMODULO";

/**
 * Defines the Schedule ampliation element tag
 */
NSString * const kPaymentInstitutionsDetailResponseScheduleAmpl = @"AMPLIACIONHORARIOS";


@implementation PaymentInstitutionsAndCompaniesDetailResponse

#pragma mark -
#pragma mark Properties
@dynamic payType;
@dynamic institutionName;
@dynamic arrayTitle;
@dynamic moduleFlag;
@dynamic dataValidation;
@dynamic fieldList;
@dynamic scheduleAmpl;
@dynamic payTypeCode;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPaymentInstitutionsDetailResponseMsgElement]) ||
            ([element isEqualToString:kPaymentInstitutionsDetailResponseAdditionalInfoElement]) ||
            ([element isEqualToString:kPaymentInstitutionsDetailResponseAgreementElement]) ) {
            
            result = BXPOElementIdle;
			
        } else if (([element isEqualToString:kPaymentInstitutionsDetailResponseArrayTitleElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsDetailResponseInstitutionElement]) ||
				   ([element isEqualToString:kPaymentInstitutionsDetailResponsePayTypeElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsDetailResponseModuleElement])  ||
                   ([element isEqualToString:kPaymentInstitutionsDetailResponseDataValidationElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsDetailResponseScheduleAmpl] ||
                    [element isEqualToString:kPaymentInstitutionsDetailResponsePayTypeCodeElement]
                    )
                   ) {
            
            result = BXPOElementString;
            
        } else if ( ([element isEqualToString:kPaymentInstitutionsDetailResponseFieldsElement]) ) {
            
            result = BXPOElementCustom;
            
		}
		
    }
    
    return result;
    
}

/*
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Default implementation returns nil
 */
- (StructuredXMLParser *)parserForElement:(NSString *)anElement
                             namespaceURI:(NSString *)namespaceURI {
	
	StructuredXMLParser *result = [super parserForElement:anElement namespaceURI:namespaceURI];
    
	if (result == nil) {
		
		if ([anElement isEqualToString:kPaymentInstitutionsDetailResponseFieldsElement]) {
            
            result = [[[FieldList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                     ownNamespacesPrefixesDictionary:nil] autorelease];
            
        }
		
	}
	
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the institutionName
 *
 * @return The institutionName
 */
- (NSString *)institutionName {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsDetailResponseInstitutionElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the arrayTitle
 *
 * @return The arrayTitle
 */
- (NSString *)arrayTitle {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsDetailResponseArrayTitleElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the payType
 *
 * @return The payType
 */
- (NSString *)payType {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsDetailResponsePayTypeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the payType
 *
 * @return The payType
 */
- (NSString *)payTypeCode {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsDetailResponsePayTypeCodeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the moduleFlag
 *
 * @return The moduleFlag
 */
- (NSString *)moduleFlag {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsDetailResponseModuleElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the dataValidation
 *
 * @return The dataValidation
 */
- (NSString *)dataValidation {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsDetailResponseDataValidationElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/**
 * Returns the FieldList value.
 *
 * @return The fieldList value.
 */
- (FieldList *)fieldList {
	
	FieldList *result = (FieldList *)[self objectForElement:kPaymentInstitutionsDetailResponseFieldsElement
                                               namespaceURI:@""
                                                    atIndex:0];
    
    return result;
	
}

/*
 * Returns the scheduleAmpl
 *
 * @return The scheduleAmpl
 */
- (NSString *)scheduleAmpl {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsDetailResponseScheduleAmpl
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}


@end
