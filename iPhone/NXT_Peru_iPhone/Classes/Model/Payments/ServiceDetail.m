/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "ServiceDetail.h"


/**
 * Defines the service element tag
 */
NSString * const kServiceDetailCompanyElement = @"EMPRESA";

/**
 * Defines the description element tag
 */
NSString * const kServiceDetailCodeElement = @"CODIGO";

/**
 * Defines the service detail element tag
 */
NSString * const kServiceDetailFlowElement = @"FLUJO";

/**
 * Defines the param element tag
 */
NSString * const kServiceDetailParamElement = @"PARAMETROS";

#pragma mark -
#pragma mark Properties

@implementation ServiceDetail

@dynamic company;
@dynamic code;
@dynamic flow;
@dynamic param;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kServiceDetailCompanyElement]) ||
            ([element isEqualToString:kServiceDetailCodeElement]) ||
            ([element isEqualToString:kServiceDetailFlowElement]) ||
            ([element isEqualToString:kServiceDetailParamElement])
            ) {
            
            result = BXPOElementString;
        
        } 
    }
    
    return result;
    
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the company 
 *
 * @return The company 
 */
- (NSString *)company {
    
    NSString *result = (NSString *)[self objectForElement:kServiceDetailCompanyElement
                                             namespaceURI:@""
                                                  atIndex:0];
        
    return result;
    
}

/*
 * Returns the code 
 *
 * @return The code 
 */
- (NSString *)code {
    
    NSString *result = (NSString *)[self objectForElement:kServiceDetailCodeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the flow
 *
 * @return The flow
 */
- (NSString *)flow {
    
    NSString *result = (NSString *)[self objectForElement:kServiceDetailFlowElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the parameter
 *
 * @return The parameter
 */
- (NSString *)param {
    
    NSString *result = (NSString *)[self objectForElement:kServiceDetailParamElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}


@end
