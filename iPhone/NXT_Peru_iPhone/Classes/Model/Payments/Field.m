//
//  Field.m
//  NXT_Peru_iPad
//
//  Created by Ricardo on 11/18/13.
//
//

#import "Field.h"


/**
 * Defines the description element tag
 */
NSString * const kFieldDescriptionElement = @"DESCRIPCION";

/**
 * Defines the Logic for the field element tag
 */
NSString * const kFieldLogCampElement = @"LOGICAMPO";

/**
 * Defines the Logic for the field element tag
 */
NSString * const kFieldAlignmentElement = @"ALINEACION";

/**
 * Defines the Logic for the field element tag
 */
NSString * const kFieldFullCaracElement = @"CARACRELLENO";

/**
 * Defines the Logic for the field element tag
 */
NSString * const kFieldInitialPosElement = @"POSIINICIO";

/**
 * Defines the Logic for the field element tag
 */
NSString * const kFieldIndPassFieldElement = @"INDCAMPOCLAVE";

/**
 * Defines the Logic for the field element tag
 */
NSString * const kFieldValidationTypeFieldElement = @"TIPOVALIDACION";

/**
 * Defines the Logic for the field element tag
 */
NSString * const kFieldAuxLabelElement = @"ETIQUETAAUXILIAR";


@implementation Field

@dynamic alignment;
@dynamic auxLabel;
@dynamic description;
@dynamic fullCarac;
@dynamic validationType;
@dynamic indPassField;
@dynamic initialPosition;
@dynamic logiField;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kFieldFullCaracElement]) ||
            ([element isEqualToString:kFieldAlignmentElement]) ||
            ([element isEqualToString:kFieldAuxLabelElement]) ||
            ([element isEqualToString:kFieldDescriptionElement]) ||
            ([element isEqualToString:kFieldIndPassFieldElement]) ||
            ([element isEqualToString:kFieldInitialPosElement]) ||
            ([element isEqualToString:kFieldLogCampElement]) ||
            ([element isEqualToString:kFieldValidationTypeFieldElement])
            ){
            
            result = BXPOElementString;
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the description
 *
 * @return The description
 */
- (NSString *)description {
    
    NSString *result = (NSString *)[self objectForElement:kFieldDescriptionElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the logic for the field
 *
 * @return The logiField
 */
- (NSString *)logiField {
    
    NSString *result = (NSString *)[self objectForElement:kFieldLogCampElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the alignment
 *
 * @return The alignment
 */
- (NSString *)alignment {
    
    NSString *result = (NSString *)[self objectForElement:kFieldAlignmentElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the max allowed number for the field
 *
 * @return The fullCarac
 */
- (NSString *)fullCarac {
    
    NSString *result = (NSString *)[self objectForElement:kFieldFullCaracElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the initialPosition
 *
 * @return The initialPosition
 */
- (NSString *)initialPosition {
    
    NSString *result = (NSString *)[self objectForElement:kFieldInitialPosElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the password flag
 *
 * @return The indPassField
 */
- (BOOL)indPassField {
    
    NSString *result = (NSString *)[self objectForElement:kFieldIndPassFieldElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    
    if ([@"S" isEqualToString:result]) {
        return YES;
    }
    
    return NO;
    
}

/*
 * Returns the validation type
 *
 * @return The validationType
 */
- (NSString *)validationType {
    
    NSString *result = (NSString *)[self objectForElement:kFieldValidationTypeFieldElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the auxiliar label
 *
 * @return The auxLabel
 */
- (NSString *)auxLabel {
    
    NSString *result = (NSString *)[self objectForElement:kFieldAuxLabelElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}
@end
