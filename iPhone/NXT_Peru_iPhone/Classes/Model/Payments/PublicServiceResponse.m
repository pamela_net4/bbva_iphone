/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PublicServiceResponse.h"

/**
 * Defines the additional information element tag
 */
NSString * const kPublicServiceResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";

/**
 * Defines the msg-s element tag
 */
NSString * const kPublicServiceResponseMsgElement = @"MSG-S";

/**
 * Defines the company element tag
 */
NSString * const kPublicServiceResponseCompanyElement = @"EMPRESA";

/**
 * Defines the service element tag
 */
NSString * const kPublicServiceResponseServiceElement = @"SERVICIO";

/**
 * Defines the agreement element tag
 */
NSString * const kPublicServiceResponseAgreementElement = @"ESCONVENIO";

/**
 * Defines the max longitude element tag
 */
NSString * const kPublicServiceResponseMaxLongElement = @"LONGMAXSUMI";

/**
 * Defines the min longitude element tag
 */
NSString * const kPublicServiceResponseMinLongElement = @"LONGMINSUMI";

#pragma mark -
#pragma mark Properties

@implementation PublicServiceResponse

@dynamic company;
@dynamic service;
@dynamic agreement;
@dynamic maxLong;
@dynamic minLong;
@synthesize paymentOperationType;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPublicServiceResponseMsgElement]) ||
            ([element isEqualToString:kPublicServiceResponseAdditionalInfoElement])){
            
            result = BXPOElementIdle;
        
        } else if (([element isEqualToString:kPublicServiceResponseCompanyElement]) ||
                   ([element isEqualToString:kPublicServiceResponseServiceElement]) ||
                   ([element isEqualToString:kPublicServiceResponseAgreementElement]) ||
                   ([element isEqualToString:kPublicServiceResponseMaxLongElement]) ||
                   ([element isEqualToString:kPublicServiceResponseMinLongElement])) {
            
            result = BXPOElementString;
            
        }
    }
    
    return result;
    
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the company
 *
 * @return The company
 */
- (NSString *)company {
    
    NSString *result = (NSString *)[self objectForElement:kPublicServiceResponseCompanyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the service
 *
 * @return The service
 */
- (NSString *)service {
    
    NSString *result = (NSString *)[self objectForElement:kPublicServiceResponseServiceElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the agreement
 *
 * @return The agreement
 */
- (BOOL)agreement {
    
    BOOL result = NO;
    
    NSString *value = (NSString *)[self objectForElement:kPublicServiceResponseAgreementElement
                                            namespaceURI:@""
                                                 atIndex:0];
    
    if ([value isEqualToString:@"S"]) {
        result = YES;
    }
    
    return result;
    
}

/*
 * Returns the max long
 *
 * @return The max long
 */
- (NSInteger)maxLong {
    
    NSInteger result = 0;
    
    NSString *value = (NSString *)[self objectForElement:kPublicServiceResponseMaxLongElement
                                            namespaceURI:@""
                                                 atIndex:0];
    result = [value integerValue];
    
    return result;
    
}

/*
 * Returns the min long
 *
 * @return The min long
 */
- (NSInteger)minLong {
    
    NSInteger result = 0;
    
    NSString *value = (NSString *)[self objectForElement:kPublicServiceResponseMinLongElement
                                            namespaceURI:@""
                                                 atIndex:0];
    result = [value integerValue];
    
    return result;
    
}

@end
