/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "StructuredXMLParser.h"

/**
 * Defines the company element tag
 */
extern NSString * const kServiceDetailCompanyElement;

/**
 * Defines the code element tag
 */
extern NSString * const kServiceDetailCodeElement;

/**
 * Defines the flow element tag
 */
extern NSString * const kServiceDetailFlowElement;

/**
 * Defines the param element tag
 */
extern NSString * const kServiceDetailParamElement;


/**
 * Analyzes a service detail
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface ServiceDetail : StructuredXMLParser

/**
 * Provides read-only access to the company
 */
@property (nonatomic, readonly, copy) NSString *company;

/**
 * Provides read-only access to the code
 */
@property (nonatomic, readonly, copy) NSString *code;

/**
 * Provides read-only access to the flow
 */
@property (nonatomic, readonly, copy) NSString *flow;

/**
 * Provides read-only access to the param
 */
@property (nonatomic, readonly, copy) NSString *param;

@end
