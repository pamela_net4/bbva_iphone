/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "Service.h"

#import "ServiceDetail.h"


/**
 * Defines the service element tag
 */
NSString * const kServiceServiceElement = @"SERVICIO";

/**
 * Defines the description element tag
 */
NSString * const kServiceDescriptionElement = @"DESCRIPCION";

/**
 * Defines the service code element tag
 */
NSString * const kServiceServiceCodeElement = @"CODIGO";

/**
 * Defines the service detail element tag
 */
NSString * const kServiceServiceDetailElement = @"E";

#pragma mark -
#pragma mark Properties

@implementation Service

@dynamic service;
@dynamic description;
@dynamic serviceDetailList;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kServiceServiceElement]) ||
            ([element isEqualToString:kServiceDescriptionElement]) ||
            ([element isEqualToString:kServiceServiceCodeElement])) {
            
            result = BXPOElementString;
            
        } else if ([element isEqualToString:kServiceServiceDetailElement]) {
            
            result = BXPOElementCustom;
            
        }
    }
    
    return result;
    
}

/**
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Returns the corresponing parser for the transaction list and nil for the rest
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element parser
 */
- (StructuredXMLParser *)parserForElement:(NSString *)element
                             namespaceURI:(NSString *)namespaceURI {
    
    StructuredXMLParser *result = [super parserForElement:element
                                             namespaceURI:namespaceURI];
    
    if (result == nil) {
        
        if ([element isEqualToString:kServiceServiceDetailElement]) {
            
            result = [[[ServiceDetail allocWithZone:[self zone]] initWithParentNamespacePrefixesDictionary:[self wholeNamespacesPrefixesDictionary]
                                                                           ownNamespacesPrefixesDictionary:nil] autorelease];
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the service
 *
 * @return The service
 */
- (NSString *)service {
    
    NSString *result = (NSString *)[self objectForElement:kServiceServiceElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the description
 *
 * @return The description
 */
- (NSString *)description {
    
    NSString *result = (NSString *)[self objectForElement:kServiceDescriptionElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/**
 * Returns the service code
 *
 * @return The code
 */
- (NSString *)serviceCode {
    
    NSString *result = (NSString *)[self objectForElement:kServiceServiceCodeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the service detail
 *
 * @return The service detail
 */
- (NSArray *)serviceDetailList {
	
	NSArray *result = [self arrayForElement:kServiceServiceDetailElement
                               namespaceURI:@""];
	
	return result;
    
}


@end
