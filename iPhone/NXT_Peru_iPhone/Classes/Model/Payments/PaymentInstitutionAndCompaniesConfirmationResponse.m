//
//  PaymentInstitutionAndCompaniesConfirmationResponse.m
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/21/13.
//
//

#import "PaymentInstitutionAndCompaniesConfirmationResponse.h"

#import "PendingDocumentList.h"
#import "accountPayList.h"
#import "CarrierList.h"
#import "CardTypeList.h"
#import "OcurrencyList.h"

/**
 * Defines the additional information element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";

/**
 * Defines the msg-s element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponseMsgElement = @"MSG-S";

/**
 * Defines the pay type element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponsePayTypeElement = @"TIPOPAGO";

/**
 * Defines the pay type code element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponsePayTypeCodeElement = @"TIPOPAGOCOD";

/**
 * Defines the companie name element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponseCompanieNameElement = @"NOMEMPRESA";

/**
 * Defines the Ocurrency list element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponseOcurrencyListElement = @"LISTAOCURRENCIA";

/**
 * Defines the Pending Holder Name element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponseHolderNameElement = @"NOMTITULAR";

/**
 * Defines the Pending docs list element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponsePendingDocsListElement = @"LISTADOCSPENDIENTE";

/**
 * Defines the Partial Val element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponsePartialValElement = @"VALPARCIAL";

/**
 * Defines the card indication payment element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponseCardIndPaymentElement = @"INDICADORPAGOTARJETA";

/**
 * Defines the Card List element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponseCardListElement = @"LISTATARJETA";

/**
 * Defines the Account List element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponseAccountListElement = @"LISTACUENTA";

/**
 * Defines the Carrier list element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponseCarrierListElement = @"OPERADORAS";

/**
 * Defines the Schedule ampliation element tag
 */
NSString * const kPaymentInstitutionsconfirmationResponseScheduleAmpl = @"AMPLIACIONHORARIOS";

/**
 * The payment Institution Detail response.
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@implementation PaymentInstitutionAndCompaniesConfirmationResponse

@dynamic ocurrencyList;
@dynamic cardList;
@dynamic carrierList;
@dynamic accountList;
@dynamic companieName;
@dynamic payType;
@dynamic partialVal;
@dynamic holderName;
@dynamic indCardPay;
@dynamic pendingDocumentList;
@dynamic scheduleAmpl;
@dynamic payTypeCode;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPaymentInstitutionsconfirmationResponseMsgElement]) ||
            ([element isEqualToString:kPaymentInstitutionsconfirmationResponseAdditionalInfoElement])
             ) {
            
            result = BXPOElementIdle;
			
        } else if (([element isEqualToString:kPaymentInstitutionsconfirmationResponseCardIndPaymentElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsconfirmationResponsePayTypeElement])  ||
                   ([element isEqualToString:kPaymentInstitutionsconfirmationResponseHolderNameElement])  ||
                   ([element isEqualToString:kPaymentInstitutionsconfirmationResponseCompanieNameElement])||
                   ([element isEqualToString:kPaymentInstitutionsconfirmationResponsePartialValElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsconfirmationResponseScheduleAmpl] || [element isEqualToString:kPaymentInstitutionsconfirmationResponsePayTypeCodeElement])
                   ) {
            
            result = BXPOElementString;
            
        } else if ( ([element isEqualToString:kPaymentInstitutionsconfirmationResponseAccountListElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsconfirmationResponseCardListElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsconfirmationResponseOcurrencyListElement])  ||
                   ([element isEqualToString:kPaymentInstitutionsconfirmationResponseCarrierListElement])  ||
                   ([element isEqualToString:kPaymentInstitutionsconfirmationResponsePendingDocsListElement])
                   
                   ) {
            
            result = BXPOElementCustom;
            
		}
		
    }
    
    return result;
    
}

/*
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Default implementation returns nil
 */
- (StructuredXMLParser *)parserForElement:(NSString *)anElement
                             namespaceURI:(NSString *)namespaceURI {
	
	StructuredXMLParser *result = [super parserForElement:anElement namespaceURI:namespaceURI];
    
	if (result == nil) {
		
		if ([anElement isEqualToString:kPaymentInstitutionsconfirmationResponseAccountListElement]) {
            
            result = [[[accountPayList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                        ownNamespacesPrefixesDictionary:nil] autorelease];
            
        } else if ([anElement isEqualToString:kPaymentInstitutionsconfirmationResponseCardListElement]) {
            
            result = [[[CardTypeList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                        ownNamespacesPrefixesDictionary:nil] autorelease];
            
        } else if ([anElement isEqualToString:kPaymentInstitutionsconfirmationResponseCarrierListElement]) {
            
            result = [[[CarrierList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                            ownNamespacesPrefixesDictionary:nil] autorelease];
            
        } else if ([anElement isEqualToString:kPaymentInstitutionsconfirmationResponsePendingDocsListElement]) {
            
            result = [[[PendingDocumentList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                               ownNamespacesPrefixesDictionary:nil] autorelease];
            
        } else if ([anElement isEqualToString:kPaymentInstitutionsconfirmationResponseOcurrencyListElement]) {
            
            result = [[[OcurrencyList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                               ownNamespacesPrefixesDictionary:nil] autorelease];
            
        }
		
	}
	
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the card list
 *
 * @return The cardlist
 */
- (CardTypeList *)cardList {
    
    CardTypeList *result = (CardTypeList *)[self objectForElement:kPaymentInstitutionsconfirmationResponseCardListElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the accountList
 *
 * @return The accountList
 */
- (accountPayList *) accountList{
    
    accountPayList *result = (accountPayList *)[self objectForElement:kPaymentInstitutionsconfirmationResponseAccountListElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the ocurrencyList
 *
 * @return The ocurrencyList
 */
- (OcurrencyList *) ocurrencyList{
    
    OcurrencyList *result = (OcurrencyList *)[self objectForElement:kPaymentInstitutionsconfirmationResponseOcurrencyListElement
                                                         namespaceURI:@""
                                                              atIndex:0];
    return result;
    
}

/*
 * Returns the carrierList
 *
 * @return The carrierList
 */
- (CarrierList *) carrierList{
    
    CarrierList *result = (CarrierList *)[self objectForElement:kPaymentInstitutionsconfirmationResponseCarrierListElement
                                                   namespaceURI:@""
                                                        atIndex:0];
    return result;
    
}

/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) companieName{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsconfirmationResponseCompanieNameElement
                                                   namespaceURI:@""
                                                        atIndex:0];
    return result;
    
}

/*
 * Returns the payType
 *
 * @return The payType
 */
- (NSString *) payType{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsconfirmationResponsePayTypeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the payType
 *
 * @return The payType
 */
- (NSString *) payTypeCode{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsconfirmationResponsePayTypeCodeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the partialVal
 *
 * @return The partialVal
 */
- (BOOL) partialVal{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsconfirmationResponsePartialValElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    if ([@"S" isEqualToString:[result uppercaseString]]) {
        return TRUE;
    }
    
    return FALSE;
    
}

/*
 * Returns the holderName
 *
 * @return The holderName
 */
- (NSString *) holderName{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsconfirmationResponseHolderNameElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the indCardPay
 *
 * @return The indCardPay
 */
- (NSString *) indCardPay{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsconfirmationResponseCardIndPaymentElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the pendingDocumentList
 *
 * @return The pendingDocumentList
 */
- (PendingDocumentList *) pendingDocumentList{
    
    PendingDocumentList *result = (PendingDocumentList *)[self objectForElement:kPaymentInstitutionsconfirmationResponsePendingDocsListElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the scheduleAmpl
 *
 * @return The scheduleAmpl
 */
- (NSString *)scheduleAmpl {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsconfirmationResponseScheduleAmpl
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

@end
