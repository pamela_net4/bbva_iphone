//
//  PaymentInstitutionsAndCompaniesInitialResponse.h
//  NXT_Peru_iPad
//
//  Created by Ricardo on 11/11/13.
//
//

#import "StructuredStatusEnabledResponse.h"

@class GroupList;
@class InstitutionList;

/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentInstitutionsResponseAdditionalInfoElement;

/**
 * Defines the msg-s element tag
 */
extern NSString * const kPaymentInstitutionsResponseMsgElement;

/**
 * Defines the groups element tag
 */
extern NSString * const kPaymentInstitutionsResponseGroupsElement;

/**
 * Defines the Institutions element tag
 */
extern NSString * const kPaymentInstitutionsResponseInstitutionsElement;

/**
 * Defines the Next element tag
 */
extern NSString * const kPaymentInstitutionsResponseNextElement;

/**
 * Defines the Next Movement element tag
 */
extern NSString * const kPaymentInstitutionsResponseNextMovementElement;

/**
 * Defines the Pagination indicator element tag
 */
extern NSString * const kPaymentInstitutionsResponsePagIndicatorElement;

/**
 * Defines the Search Argument element tag
 */
extern NSString * const kPaymentInstitutionsResponseSearchArgElement;

/**
 * Defines the Last Description element tag
 */
extern NSString * const kPaymentInstitutionsResponseLastDescriptionElement;

/**
 * Defines the Next element back tag
 */
extern NSString * const kPaymentInstitutionsResponseBackElement;

/**
 * Defines the Back Next Movement element tag
 */
extern NSString * const kPaymentInstitutionsResponseNextMovementToBackElement;

/**
 * Defines the Back Pagination indicator element tag
 */
extern NSString * const kPaymentInstitutionsResponseBackPagIndicatorElement;

/**
 * Defines the Back Search Argument element tag
 */
extern NSString * const kPaymentInstitutionsResponseBackSearchArgElement;

/**
 * Defines the Back Last Description element tag
 */
extern NSString * const kPaymentInstitutionsResponseBackLastDescriptionElement;

/**
 * The payment Institution initial response.
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@interface PaymentInstitutionsAndCompaniesInitialResponse : StructuredStatusEnabledResponse


/**
 * Provides read-only access to the group list
 */
@property (nonatomic, readonly, retain) GroupList *groupList;

/**
 * Provides read-only access to the institution list
 */
@property (nonatomic, readonly, retain) InstitutionList *institutionList;

/**
 * Provides read-only access to the Next element
 */
@property (nonatomic, readonly) BOOL hasNext;

/**
 * Provides read-only access to the Next Movement element
 */
@property (nonatomic, readonly, retain) NSString *nextMovement;

/**
 * Provides read-only access to the pagination indicator element
 */
@property (nonatomic, readonly, retain) NSString *pagIndicator;

/**
 * Provides read-only access to the Search Argument
 */
@property (nonatomic, readonly, retain) NSString *searchArg;

/**
 * Provides read-only access to the Next element
 */
@property (nonatomic, readonly, retain) NSString *lastDescription;

/**
 * Provides read-only access to the back element
 */
@property (nonatomic, readonly) BOOL hasBack;

/**
 * Provides read-only access to the Next Movement element
 */
@property (nonatomic, readonly, retain) NSString *backNextMovement;

/**
 * Provides read-only access to the pagination indicator element
 */
@property (nonatomic, readonly, retain) NSString *backPagIndicator;

/**
 * Provides read-only access to the Search Argument
 */
@property (nonatomic, readonly, retain) NSString *backSearchArg;

/**
 * Provides read-only access to the Next element
 */
@property (nonatomic, readonly, retain) NSString *backLastDescription;

@end
