/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "TagAndValue.h"

/**
 * Defines the tag element 
 */
NSString * const kTagAndValueTagElement = @"ETIQUETA";

/**
 * Defines the value element
 */
NSString * const kTagAndValueVulueElement = @"VALOR";

#pragma mark -
#pragma mark Properties

@implementation TagAndValue

@dynamic tag;
@dynamic value;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kTagAndValueTagElement]) ||
            ([element isEqualToString:kTagAndValueVulueElement])) {
            
            result = BXPOElementString;
        
        } 
        
    }
    
    return result;
    
}



#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the tag 
 *
 * @return The tag 
 */
- (NSString *)tag {
    
    NSString *result = (NSString *)[self objectForElement:kTagAndValueTagElement
                                             namespaceURI:@""
                                                  atIndex:0];
        
    return result;
    
}

/*
 * Returns the value 
 *
 * @return The value 
 */
- (NSString *)value {
    
    NSString *result = (NSString *)[self objectForElement:kTagAndValueVulueElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}



@end
