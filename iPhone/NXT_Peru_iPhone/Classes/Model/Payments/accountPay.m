//
//  accountPay.m
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/22/13.
//
//

#import "accountPay.h"
#import "Tools.h"


/**
 * Defines the identification element tag
 */
NSString * const kAccountPayCurrencyElement = @"MONEDA";

/**
 * Defines the bank element tag
 */
NSString * const kAccountPayAccountTypeElement  = @"TIPOCTA";;

/**
 * Defines the agreement element tag
 */
NSString * const kAccountPaySubjectElement = @"ASUNTO";

/**
 * Defines the agreement element tag
 */
NSString * const kAccountPayNumberElement = @"NUMERO";

/**
 * Defines the agreement parameters element tag
 */
NSString * const kAccountPayDispAmountElement = @"SALDODISPONIBLE";;

/**
 * Defines the agreement parameters element tag
 */
NSString * const kAccountPayDispAmountCurrencyElement = @"DIVSALDODISPONIBLE";

/**
 * Defines the agreement parameters element tag
 */
NSString * const kAccountPayDispAccountParamElement = @"PARAMCUENTA";

@implementation accountPay

#pragma mark -
#pragma mark Properties

@dynamic availableBalanceString;
@dynamic subject;
@dynamic number;
@dynamic accountType;
@dynamic currency;
@dynamic accountParam;
@dynamic dispAmountCurrency;
@dynamic accountIdAndDescription;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kAccountPayAccountTypeElement]) ||
            ([element isEqualToString:kAccountPayCurrencyElement]) ||
            ([element isEqualToString:kAccountPayDispAccountParamElement]) ||
            ([element isEqualToString:kAccountPayDispAmountCurrencyElement]) ||
            ([element isEqualToString:kAccountPayDispAmountElement]) ||
            ([element isEqualToString:kAccountPayNumberElement]) ||
            ([element isEqualToString:kAccountPaySubjectElement]) ) {
            
            result = BXPOElementString;
            
        }
        
    }
    
    return result;
    
}



#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the currency
 *
 * @return The currency
 */
- (NSString *)currency {
    
    NSString *result = (NSString *)[self objectForElement:kAccountPayCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return result;
    
}

/*
 * Returns the account Type
 *
 * @return The accountType
 */
- (NSString *)accountType {
    
    NSString *result = (NSString *)[self objectForElement:kAccountPayAccountTypeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the subject
 *
 * @return The accountsubjectType
 */
- (NSString *)subject {
    
    NSString *result = (NSString *)[self objectForElement:kAccountPaySubjectElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the dispo Amount
 *
 * @return The dispoAmount
 */
- (NSString *)availableBalanceString {
    
    NSString *result = (NSString *)[self objectForElement:kAccountPayDispAmountElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the disp Amount Currency
 *
 * @return The dispAmountCurrency
 */
- (NSString *)dispAmountCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kAccountPayDispAmountCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the account Param
 *
 * @return The accountParam
 */
- (NSString *)accountParam {
    
    NSString *result = (NSString *)[self objectForElement:kAccountPayDispAccountParamElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the account number
 *
 * @return The number
 */
- (NSString *)number {
    
    NSString *result = (NSString *)[self objectForElement:kAccountPayNumberElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the account identification plus the description
 *
 * @return The account identification plus the description
 */
- (NSString *)accountIdAndDescription {
    
    NSString *result = @"";
    
    NSString *accountNumber = [self subject];
    
    result = [NSString stringWithFormat:@"%@ %@ - Disp. %@ %@", [self accountType], [Tools obfuscateAccountNumber:accountNumber], [Tools getCurrencySimbol:[self currency]], [self availableBalanceString]];
    
    return result;
    
}

@end
