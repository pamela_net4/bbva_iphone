//
//  PaymentInstitutionAndCompaniesSuccessConfirmationResponse.m
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 11/27/13.
//
//

#import "PaymentInstitutionAndCompaniesSuccessConfirmationResponse.h"
#import "OcurrencyList.h"
#import "PendingDocumentList.h"
#import "Tools.h"

/**
 * Defines the additional information element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";

/**
 * Defines the msg-s element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseMsgElement = @"MSG-S";

/**
 * Defines the pay type element tag
 */
NSString * const kPaymentInstitutionsSuccessResponsePayTypeElement = @"TIPOPAGO";

/**
 * Defines the pay type code element tag
 */
NSString * const kPaymentInstitutionsSuccessResponsePayTypeCodeElement = @"TIPOPAGOCOD";

/**
 * Defines the pay type element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseOperationNumberElement = @"NUMOPERACION";

/**
 * Defines the pay amount element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseDateElement = @"FECHA";

/**
 * Defines the pay amount currency element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseTimeElement = @"HORA";

/**
 * Defines the operation element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseOperationElement = @"OPERACION";

/**
 * Defines the account type element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseAccountTypeElement = @"TIPOCTA";

/**
 * Defines the currency element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseCurrencyElement = @"MONEDA";

/**
 * Defines the subject account element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseSubjectElement = @"ASUNTO";

/**
 * Defines the Companie Name element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseCompanieNameElement = @"NOMEMPRESA";

/**
 * Defines the Ocurrency list element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseOcurrencyListElement = @"LISTAOCURRENCIA";

/**
 * Defines the holder Name element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseHolderName = @"NOMTITULAR";

/**
 * Defines the Doc Number element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseDocumentNumber = @"CANTDOCUMENTO";

/**
 * Defines the Document List element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseDocumentList = @"LISTADOCUMENTO";

/**
 * Defines the Coordinates element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseChangeType = @"TIPOCAMBIO";

/**
 * Defines the Coordinates element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseChangeTypeCurrency = @"DIVTIPOCAMBIO";

/**
 * Defines the Seal element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseAmountPayed = @"MTOPAGADO";

/**
 * Defines the Disclaimer element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseAmountPayedCurrency = @"DIVMTOPAGADO";

/**
 * Defines the Seal element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseAmountCustomPayed = @"MONTOPAGADO";

/**
 * Defines the Disclaimer element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseAmountCustomPayedCurrency = @"DIVMONTOPAGADO";

/**
 * Defines the Disclaimer element tag 
 */
NSString * const kPaymentInstitutionsSuccessResponseChargedAmount = @"MTOCARGADO";

/**
 * Defines the Disclaimer element tag 
 */
NSString * const kPaymentInstitutionsSuccessResponseChargedAmountCurrency = @"DIVMTOCARGADO";

/**
 * Defines the Disclaimer element tag 
 */
NSString * const kPaymentInstitutionsSuccessResponseMessage = @"MENSAJE";

/**
 * Defines the Schedule ampliation element tag
 */
NSString * const kPaymentInstitutionsSuccessResponseScheduleAmpl = @"AMPLIACIONHORARIOS";


/**
 * The payment Institution confirmation response.
 *
 * @author <a href="http://www.mdp.com.pe">MDP Consulting</a>
 */
@implementation PaymentInstitutionAndCompaniesSuccessConfirmationResponse
@dynamic accountType;
@dynamic changeType;
@dynamic changeTypeCurrency;
@dynamic chargedAmount;
@dynamic chargedAmountCurrency;
@dynamic companieName;
@dynamic currency;
@dynamic date;
@dynamic docList;
@dynamic docNumber;
@dynamic holderName;
@dynamic message;
@dynamic ocurrencyList;
@dynamic operation;
@dynamic operationNumber;
@dynamic payedAmount;
@dynamic payedAmountCurrency;
@dynamic payType;
@dynamic subject;
@dynamic time;
@dynamic scheduleAmpl;
@dynamic payTypeCode;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPaymentInstitutionsSuccessResponseAdditionalInfoElement]) ||
            ([element isEqualToString:kPaymentInstitutionsSuccessResponseMsgElement])
            ) {
            
            result = BXPOElementIdle;
			
        } else if (([element isEqualToString:kPaymentInstitutionsSuccessResponseAccountTypeElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseAmountPayed])  ||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseAmountPayedCurrency])  ||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseAmountCustomPayed])  ||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseAmountCustomPayedCurrency])  ||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseChangeType])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseChangeTypeCurrency])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseChargedAmount])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseChargedAmountCurrency])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseCompanieNameElement])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseCurrencyElement])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseDateElement])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseDocumentNumber])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseHolderName])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseMessage])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseOperationElement])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseOperationNumberElement])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponsePayTypeElement])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseSubjectElement])||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseTimeElement]) ||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseScheduleAmpl] ||
                    ([element isEqualToString:
                    kPaymentInstitutionsSuccessResponsePayTypeCodeElement])
                    )
                   ) {
            
            result = BXPOElementString;
            
        } else if ( ([element isEqualToString:kPaymentInstitutionsSuccessResponseDocumentList]) ||
                   ([element isEqualToString:kPaymentInstitutionsSuccessResponseOcurrencyListElement])
                   
                   ) {
            
            result = BXPOElementCustom;
            
		}
		
    }
    
    return result;
    
}

/*
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Default implementation returns nil
 */
- (StructuredXMLParser *)parserForElement:(NSString *)anElement
                             namespaceURI:(NSString *)namespaceURI {
	
	StructuredXMLParser *result = [super parserForElement:anElement namespaceURI:namespaceURI];
    
	if (result == nil) {
		
		if ([anElement isEqualToString:kPaymentInstitutionsSuccessResponseOcurrencyListElement]) {
            
            result = [[[OcurrencyList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                          ownNamespacesPrefixesDictionary:nil] autorelease];
            
        } else if ([anElement isEqualToString:kPaymentInstitutionsSuccessResponseDocumentList]) {
            
            result = [[[PendingDocumentList allocWithZone:self.zone] initWithParentNamespacePrefixesDictionary:self.wholeNamespacesPrefixesDictionary
                                                                        ownNamespacesPrefixesDictionary:nil] autorelease];
            
        } 
		
	}
	
    return result;
    
}

/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) accountType{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseAccountTypeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) changeType{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseChangeType
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) changeTypeCurrency{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseChangeTypeCurrency
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) chargedAmount{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseChargedAmount
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) chargedAmountCurrency{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseChargedAmountCurrency
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) companieName{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseCompanieNameElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) currency{    
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) date{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseDateElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) docNumber{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseDocumentNumber
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) holderName{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseHolderName
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) message{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseMessage
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) operation{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseOperationElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return [Tools capitalizeFirstWordOnly:result];
    
}

/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) operationNumber{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseOperationNumberElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) payedAmount{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseAmountPayed
                                             namespaceURI:@""
                                                  atIndex:0];
    
    if (result == nil || [@"" isEqualToString:result]) {
        result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseAmountCustomPayed
                                       namespaceURI:@""
                                            atIndex:0];
    }
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) payedAmountCurrency{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseAmountPayedCurrency
                                             namespaceURI:@""
                                                  atIndex:0];
    if (result == nil || [@"" isEqualToString:result]) {
        result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseAmountCustomPayedCurrency
                                       namespaceURI:@""
                                            atIndex:0];
    }
    return result;
    
}
/*
 * Returns the payType
 *
 * @return The payType
 */
- (NSString *) payType{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponsePayTypeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the payType Code
 *
 * @return The payType Code
 */
- (NSString *) payTypeCode{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponsePayTypeCodeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the subject
 *
 * @return The subject
 */
- (NSString *) subject{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseSubjectElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (NSString *) time{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseTimeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (PendingDocumentList *)docList {
    
    PendingDocumentList *result = (PendingDocumentList *)[self objectForElement:kPaymentInstitutionsSuccessResponseDocumentList
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}
/*
 * Returns the companieName
 *
 * @return The companieName
 */
- (OcurrencyList *) ocurrencyList{
    
    OcurrencyList *result = (OcurrencyList *)[self objectForElement:kPaymentInstitutionsSuccessResponseOcurrencyListElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the scheduleAmpl
 *
 * @return The scheduleAmpl
 */
- (NSString *)scheduleAmpl {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentInstitutionsSuccessResponseScheduleAmpl
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

@end
