/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "PaymentSuccessResponse.h"
#import "Tools.h"

/**
 * Defines the msg-s element tag
 */
NSString * const kPaymentSuccessResponseMsgElement = @"MSG-S";

/**
 * Defines the additional information element tag
 */
NSString * const kPaymentSuccessResponseAdditionalInfoElement = @"INFORMACIONADICIONAL";

/**
 * Defines the operation number element tag
 */
NSString * const kPaymentSuccessResponseOperationNumberElement = @"NUMEROOPERACION";


/**
 * Defines the operation number element tag
 */
NSString * const kPaymentSuccessResponseOperationStateElement = @"ESTADOOPERACION";

/**
 * Defines the operation element tag
 */
NSString * const kPaymentSuccessResponseOperationElement = @"OPERACION";

/**
 * Defines the change type element tag
 */
NSString * const kPaymentSuccessResponseChangeTypeElement = @"TIPOCAMBIO";

/**
 * Defines the change type currency element tag
 */
NSString * const kPaymentSuccessResponseChangeTypeCurrencyElement = @"MONEDATIPOCAMBIO";

/**
 * Defines the total charged element tag
 */
NSString * const kPaymentSuccessResponseTotalChargedElement = @"TOTALCARGADO";

/**
 * Defines the total charged currency element tag
 */
NSString * const kPaymentSuccessResponseTotalChargedCurrencyElement = @"MONEDATOTALCARGADO";

/**
 * Defines the total paid element tag
 */
NSString * const kPaymentSuccessResponseTotalPaidElement = @"TOTALABONADO";

/**
 * Defines the total paid currency element tag
 */
NSString * const kPaymentSuccessResponseTotalPaidCurrencyElement = @"MONEDATOTALABONADO";

/**
 * Defines the operation date element tag
 */
NSString * const kPaymentSuccessResponseOperationDateElement = @"FECHAOPERACION";

/**
 * Defines the value date element tag
 */
NSString * const kPaymentSuccessResponseValueDateElement = @"FECHAVALOR";

/**
 * Defines the message element tag
 */
NSString * const kPaymentSuccessResponseMessageElement = @"MENSAJECONFIMACION";

/**
 * Defines the message element tag
 */
NSString * const kPaymentSuccessResponseNotificationMessageElement = @"MENSAJENOTIFICACION";

/**
 * Defines the amount to charge element tag
 */
NSString * const kPaymentSuccessResponseAmountToChargeElement = @"IMPORTECARGAR";

/**
 * Defines the amount to charge currency element tag
 */
NSString * const kPaymentSuccessResponseAmountToChargeCurrencyElement = @"MONEDAIMPORTECARGAR";

/**
 * Defines the amount charged element tag
 */
NSString * const kPaymentSuccessResponseAmountChargedElement = @"IMPORTECARGADO";

/**
 * Defines the amount charged currency element tag
 */
NSString * const kPaymentSuccessResponseAmountChargedCurrencyElement = @"MONEDAIMPORTECARGADO";

/**
 * Defines the amount to charge element tag
 */
NSString * const kPaymentSuccessResponseDestinationBankElement = @"BANCODESTINO";


/**
 * Defines the amount to charge element tag
 */
NSString * const kPaymentSuccessResponseDestinationBank2Element = @"BCODESTINO";

/**
 * Defines the amount to pay element tag
 */
NSString * const kPaymentSuccessResponseAmountToPayElement = @"IMPORTEPAGAR";

/**
 * Defines the amount to pay currency element tag
 */
NSString * const kPaymentSuccessResponseAmountToPayCurrencyElement = @"MONEDAIMPORTEPAGAR";

/**
 * Defines the amount payed element tag
 */
NSString * const kPaymentSuccessResponseAmountPayedElement = @"IMPORTEPAGADO";

/**
 * Defines the amount payed currency element tag
 */
NSString * const kPaymentSuccessResponseAmountPayedCurrencyElement = @"MONEDAIMPORTEPAGADO";

/**
 * Defines the amount turned element tag
 */
NSString * const kPaymentSuccessResponseAmountTurnedElement = @"IMPORTECONVERTIDO";

/**
 * Defines the amount turned currency element tag
 */
NSString * const kPaymentSuccessResponseAmountTurnedCurrencyElement = @"MONEDAIMPORTECONVERTIDO";

/**
 * Defines the service element tag
 */
NSString * const kPaymentSuccessResponseServiceElement = @"SERVICE";

/**
 * Defines the amount turned element tag
 */
NSString * const kPaymentSuccessResponseAmountRechargeElement = @"IMPORTERECARGA";

/**
 * Defines the amount turned currency element tag
 */
NSString * const kPaymentSuccessResponseAmountRechargeCurrencyElement = @"MONEDAIMPORTERECARGA";

/**
 * Defines the service element tag
 */
NSString * const kPaymentSuccessResponseCustomerServiceElement = @"ATENCIONCLIENTE";

/**
 * Defines the service element tag
 */
NSString * const kPaymentSuccessResponseBreakdownElement = @"AVERIAS";

/**
 * Defines the service element tag
 */
NSString * const kPaymentSuccessResponseClaroBreakdownElement = @"MENSAJEASISTENCIA";

/**
 * Defines the service element tag
 */
NSString * const kPaymentSuccessResponseBalanceConsultElement = @"CONSULTASSALDO";

/**
 * Defines the service element tag
 */
NSString * const kPaymentSuccessResponseClaroBalanceConsultElement = @"MENSAJESALDO";

/**
 * Defines the service element tag
 */
NSString * const kPaymentSuccessResponseClaimElement = @"RECLAMOS";

/**
 * Defines the service element tag
 */
NSString * const kPaymentSuccessResponseDaysUseElement = @"DIASUSO";

/**
 * Defines the service element tag
 */
NSString * const kPaymentSuccessResponseClaroDaysUseElement = @"MENSAJEDIASUSO";

/**
 * Defines the service element tag
 */
NSString * const kPaymentSuccessResponseAccountOwnerElement = @"TITULARCUENTA";


/**
 * Defines the service element tag
 */
NSString * const kPaymentSuccessResponseCardHolderElement = @"TITULARTARJETA";

/**
 * Defines the amount payed element tag
 */
NSString * const kPaymentSuccessResponseTotalAmountChargedElement = @"TOTALCARGADO";

/**
 * Defines the transaction date tag
 */
NSString * const kPaymentSuccessResponseTransactionDateElement = @"FECHATRANSACCION";

/**
 * Defines the transaction hour tag
 */
NSString * const kPaymentSuccessResponseTransactionHourElement = @"HORATRANSACCION";

/**
 * Defines the supply number tag
 */
NSString *const kPaymentSuccessResponseResponseSupplyElement = @"SUMINISTRO";

/**
 * Defines the service type  element tag
 */
NSString * const kPaymentSuccessResponseNetworkUseElement = @"USODERED";



/**
 * Defines the network use currency element tag
 */
NSString * const kPaymentSuccessResponseNetworkUseCurrencyElement = @"MONEDAUSORED";

/**
 * Defines the tax element tag
 */
NSString * const kPaymentSuccessResponseTaxElement = @"COMISION";

/**
 * Defines the tax currency element tag
 */
NSString * const kPaymentSuccessResponseTaxCurrencyElement = @"MONEDACOMISION";



@implementation PaymentSuccessResponse

#pragma mark -
#pragma mark Properties

@dynamic operationNumber;
@dynamic operation;
@dynamic changeType;
@dynamic changeTypeCurrency;
@dynamic totalCharged;
@dynamic totalChargedCurrency;
@dynamic totalPaid;
@dynamic totalPaidCurrency;
@dynamic operationDate;
@dynamic valueDate;
@dynamic message;
@dynamic notificationMessage;
@dynamic amountToCharge;
@dynamic amountToChargeCurrency;
@dynamic amountCharged;
@dynamic amountChargedCurrency;
@dynamic destinationBank;
@dynamic amountToPay;
@dynamic amountToPayCurrency;
@dynamic amountPayed;
@dynamic amountPayedCurrency;
@dynamic amountTurned;
@dynamic amountTurnedCurrency;
@dynamic service;
@dynamic amountRecharge;
@dynamic amountRechargeCurrency;
@dynamic customerService;
@dynamic breakdown;
@dynamic balanceConsult;
@dynamic claim;
@dynamic daysUse;
@dynamic supply;
@dynamic cardHolder;
@dynamic networkUse;

#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementCustom for the known elements and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kPaymentSuccessResponseAdditionalInfoElement]) ||
            ([element isEqualToString:kPaymentSuccessResponseMsgElement])){
            
            result = BXPOElementIdle;
        
        } else if (([element isEqualToString:kPaymentSuccessResponseOperationStateElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseOperationNumberElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseOperationElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseChangeTypeElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseChangeTypeCurrencyElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseTotalChargedElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseTotalChargedCurrencyElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseTotalPaidElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseTotalPaidCurrencyElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseOperationDateElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseValueDateElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseMessageElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseAmountToChargeElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseAmountToChargeCurrencyElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseAmountChargedElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseAmountChargedCurrencyElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseDestinationBankElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseAmountToPayElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseAmountToPayCurrencyElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseAmountPayedElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseAmountPayedCurrencyElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseAmountTurnedElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseAmountTurnedCurrencyElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseServiceElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseAmountRechargeElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseAmountRechargeCurrencyElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseCustomerServiceElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseBreakdownElement]) ||
				   ([element isEqualToString:kPaymentSuccessResponseClaroBreakdownElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseBalanceConsultElement]) ||
				   ([element isEqualToString:kPaymentSuccessResponseClaroBalanceConsultElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseClaimElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseDaysUseElement]) ||
				   ([element isEqualToString:kPaymentSuccessResponseClaroDaysUseElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseAccountOwnerElement] ||
                    ([element isEqualToString:kPaymentSuccessResponseTotalAmountChargedElement])) ||
                   ([element isEqualToString:kPaymentSuccessResponseTransactionDateElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseTransactionHourElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseResponseSupplyElement])||
                   ([element isEqualToString:kPaymentSuccessResponseNotificationMessageElement])||
                   ([element isEqualToString:kPaymentSuccessResponseNetworkUseElement])||
                   ([element isEqualToString:kPaymentSuccessResponseNetworkUseCurrencyElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseTaxElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseTaxCurrencyElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseCardHolderElement]) ||
                   ([element isEqualToString:kPaymentSuccessResponseDestinationBank2Element])) {
            
            result = BXPOElementString;
            
        } 
    }
    
    return result;
    
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the operationNumber
 *
 * @return The operationNumber
 */
- (NSString *)operationNumber {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseOperationNumberElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the operation
 *
 * @return The operation
 */
- (NSString *)operation {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseOperationElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return [Tools capitalizeFirstWordOnly:result];

    
}

/*
 * Returns the operation
 *
 * @return The operation
 */
- (NSString *)operationState {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseOperationStateElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return [Tools capitalizeFirstWordOnly:result];
    
    
}


/*
 * Returns the changeType
 *
 * @return The changeType
 */
- (NSString *)changeType {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseChangeTypeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the changeTypeCurrency
 *
 * @return The changeTypeCurrency
 */
- (NSString *)changeTypeCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseChangeTypeCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the totalCharged
 *
 * @return The totalCharged
 */
- (NSString *)totalCharged {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseTotalChargedElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the total Charged Currency
 *
 * @return The total Charged Currency
 */
- (NSString *)totalChargedCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseTotalChargedCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the total Paid
 *
 * @return The total Paid
 */
- (NSString *)totalPaid {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseTotalPaidElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the total Paid Currency
 *
 * @return The total Paid Currency
 */
- (NSString *)totalPaidCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseTotalPaidCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the operation Date
 *
 * @return The operation Date
 */
- (NSString *)operationDate {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseOperationDateElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the value Date
 *
 * @return The value Date
 */
- (NSString *)valueDate {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseValueDateElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the message
 *
 * @return The message
 */
- (NSString *)message {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseMessageElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    return [result stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    
}

/*
 * Returns the message
 *
 * @return The message
 */
- (NSString *)notificationMessage {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseNotificationMessageElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    
    
    return [result stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    
}
/*
 * Returns the amount To Charge
 *
 * @return The amount To Charge
 */
- (NSString *)amountToCharge {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAmountToChargeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount To Charge currency
 *
 * @return The amount To Charge currency
 */
- (NSString *)amountToChargeCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAmountToChargeCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount charge
 *
 * @return The amount charge
 */
- (NSString *)amountCharged {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAmountChargedElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount charged currency
 *
 * @return The amount charged currency
 */
- (NSString *)amountChargedCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAmountChargedCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the destination Bank
 *
 * @return The destination Bank
 */
- (NSString *)destinationBank {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseDestinationBankElement
                                             namespaceURI:@""
                                                  atIndex:0];
    if([result length]==0){
        
        result = (NSString *)[self objectForElement:kPaymentSuccessResponseDestinationBank2Element
                                        namespaceURI:@""
                                             atIndex:0];
        
    }
    
    return result;
    
}

/*
 * Returns the amount To pay
 *
 * @return The amount To pay
 */
- (NSString *)amountToPay {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAmountToPayElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount to pay currency
 *
 * @return The amount to pay currency
 */
- (NSString *)amountToPayCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAmountToPayCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount payed
 *
 * @return The amount payed
 */
- (NSString *)amountPayed {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAmountPayedElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount payed currency
 *
 * @return The amount payed currency
 */
- (NSString *)amountPayedCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAmountPayedCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount To pay currency
 *
 * @return The amount To pay currency
 */
- (NSString *)amountToChargePay {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAmountToChargeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount turned
 *
 * @return The amount turned
 */
- (NSString *)amountTurned {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAmountTurnedElement
                                             namespaceURI:@""
                                                  atIndex:0];
	
	if (result == nil || [result isEqualToString:@""]) {
		
		result = (NSString *)[self objectForElement:kPaymentSuccessResponseTotalAmountChargedElement
									   namespaceURI:@""
											atIndex:0];
		
	}

    return result;
    
}

/*
 * Returns the amount turned currency
 *
 * @return The amount turned currency
 */
- (NSString *)amountTurnedCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAmountTurnedCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    
    if (result == nil || [result isEqualToString:@""]) {
		
		result = (NSString *)[self objectForElement:kPaymentSuccessResponseTotalChargedCurrencyElement
									   namespaceURI:@""
											atIndex:0];
		
	}
    
    return result;
    
}

/*
 * Returns the service
 *
 * @return The service
 */
- (NSString *)service {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseServiceElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount recharge
 *
 * @return The amount recharge
 */
- (NSString *)amountRecharge {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAmountRechargeElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the amount recharge currency
 *
 * @return The amount recharge currency
 */
- (NSString *)amountRechargeCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAmountRechargeCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the customerService service
 *
 * @return The customerService service
 */
- (NSString *)customerService {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseCustomerServiceElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the breakdown
 *
 * @return The breakdown
 */
- (NSString *)breakdown {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseBreakdownElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the network use currency
 *
 * @return The network use currency
 */
- (NSString *)networkUseCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseNetworkUseCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the tax
 *
 * @return The tax
 */
- (NSString *)tax {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseTaxElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the tax currency
 *
 * @return The tax currency
 */
- (NSString *)taxCurrency {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseTaxCurrencyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}


/*
 * Returns the network use
 *
 * @return The network use
 */
- (NSString *)networkUse {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseNetworkUseElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the claroBreakdown
 *
 * @return The claroBreakdown
 */
- (NSString *)claroBreakdown {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseClaroBreakdownElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the balance consult
 *
 * @return The balance consult
 */
- (NSString *)balanceConsult {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseBalanceConsultElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the claroBalanceConsult consult
 *
 * @return The claroBalanceConsult consult
 */
- (NSString *)claroBalanceConsult {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseClaroBalanceConsultElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the claim
 *
 * @return The claim
 */
- (NSString *)claim {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseClaimElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the days use
 *
 * @return The days use
 */
- (NSString *)daysUse {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseDaysUseElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the claroDaysUse
 *
 * @return The claroDaysUse
 */
- (NSString *)claroDaysUse {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseClaroDaysUseElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the days use
 *
 * @return The days use
 */
- (NSString *)accountOwner {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseAccountOwnerElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the days use
 *
 * @return The days use
 */
- (NSString *)cardHolder {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseCardHolderElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the transactionDate
 *
 * @return The transactionDate
 */
- (NSString *)transactionDate {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseTransactionDateElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/*
 * Returns the transactionHour
 *
 * @return The transactionHour
 */
- (NSString *)transactionHour {
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseTransactionHourElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

/**
 * Return the supply number
 *
 * @return The supply number
 */
- (NSString *)supply{
    
    NSString *result = (NSString *)[self objectForElement:kPaymentSuccessResponseResponseSupplyElement
                                             namespaceURI:@""
                                                  atIndex:0];
    return result;
    
}

@end
