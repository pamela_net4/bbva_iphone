/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import <Foundation/Foundation.h>

#import "StructuredStatusEnabledResponse.h"

@class StringList;

/**
 * Defines the additional information element tag
 */
extern NSString * const kPaymentCCardInitialResponseAdditionalInfoElement;

/**
 * Defines the msg-s element tag
 */
extern NSString * const kPaymentCCardInitialResponseMsgElement;

/**
 * Defines the company element tag
 */
extern NSString * const kPaymentCCardInitialResponseCardsListElement;


/**
 * Analyzes a service list
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentCCardInitialResponse : StructuredStatusEnabledResponse


/**
 * Provides read-only access to the numberCardList
 */
@property (nonatomic, readonly, retain) StringList *numberCardList;

@end