//
//  CampaignList.m
//  NXT_Peru_iPhone
//
//  Created by Estefany on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "CampaignList.h"

@interface CampaignList(private)

- (void) clearData;

@end


@implementation CampaignList

@synthesize campaignList = campaignList_;

- (void)dealloc
{
    [self clearData];
    
    [campaignList_ release];
    campaignList_ = nil;
    
    [super dealloc];
}

- (id) init {
    if (self = [super init]) {
        campaignList_ = [[NSMutableArray alloc] init];
       // self.notificationToPost = kNotificationCampaignListReceived;
    }
    return self;
}

- (void) removeData {
    [self clearData];
}

- (void) clearData {
    [campaignList_ removeAllObjects];
    
    [campaignAux_ release];
    campaignAux_ = nil;
}

- (Campaign *) campaignAtPosition: (NSUInteger) aPosition {
    
    Campaign *result = nil;
    if(aPosition < [campaignList_ count]){
        result = [campaignList_ objectAtIndex:aPosition];
    }
    return result;
}

- (NSArray *) campaignList {
    return [NSArray arrayWithArray:campaignList_];
}

- (void) updateFrom:(CampaignList *) campaignListOrigin {
    NSArray * campaignList = campaignListOrigin.campaignList;
    [self clearData];
    
    if (campaignList != nil) {
        for (Campaign *aux in campaignList) {
            [campaignList_ addObject:aux];
        }
    }
    
    [self updateFromStatusEnabledResponse: campaignListOrigin];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if(campaignAux_ != nil){
        [campaignList_ addObject: campaignAux_];
        
        [campaignAux_ release];
        campaignAux_ = nil;
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString *lname = [elementName lowercaseString];
        
        if([lname isEqualToString:@"e"]){
            [campaignAux_ release];
            campaignAux_ = [[Campaign alloc] init];
            [campaignAux_ setParentParseableObject:self];
            campaignAux_.openingTag = lname;
            [parser setDelegate:campaignAux_];
            
            [campaignAux_ parserDidStartDocument:parser];
        } else {
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
    }
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (campaignAux_ != nil) {
        [campaignList_ addObject:campaignAux_];
        
        [campaignAux_ release];
        campaignAux_ = nil;
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        informationUpdated_ = soie_InfoAvailable;
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
}

@end
