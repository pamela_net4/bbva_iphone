/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StockMarketAccountList.h"
#import "StockMarketAccount.h"


#pragma mark -

/**
 * StockMarketAccountList private category
 */
@interface StockMarketAccountList(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearStockMarketAccountListData;

@end


#pragma mark -

@implementation StockMarketAccountList

#pragma mark -
#pragma mark Properties

@dynamic stockMarketAccountList;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearStockMarketAccountListData];
    
    [stockMarketAccountsList_ release];
    stockMarketAccountsList_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a StockMarketAccountList instance creating the associated array
 *
 * @return The initialized StockMarketAccountList instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        stockMarketAccountsList_ = [[NSMutableArray alloc] init];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates the stock market account list from another stock market account list
 */
- (void)updateFrom:(StockMarketAccountList *)aStockMarketAccountList {
	
	[self clearStockMarketAccountListData];
    
	if (aStockMarketAccountList != nil) {
		
		NSArray *otherStockMarketAccountListArray = aStockMarketAccountList->stockMarketAccountsList_;
		
		for (StockMarketAccount *stockMarketAccount in otherStockMarketAccountListArray) {
			
			[stockMarketAccountsList_ addObject:stockMarketAccount];
			
		}
		
		[self updateFromStatusEnabledResponse:aStockMarketAccountList];
		
		self.informationUpdated = aStockMarketAccountList.informationUpdated;
		
	}

}

/*
 * Remove the contained data
 */
- (void)removeData {
    
    [self clearStockMarketAccountListData];

}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearStockMarketAccountListData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (auxStockMarketAccount_ != nil) {
        
        [stockMarketAccountsList_ addObject:auxStockMarketAccount_];
        [auxStockMarketAccount_ release];
        auxStockMarketAccount_ = nil;
        
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
            auxStockMarketAccount_ = [[StockMarketAccount alloc] init];
            [auxStockMarketAccount_ setParentParseableObject:self];
            auxStockMarketAccount_.openingTag = lname;
            [parser setDelegate:auxStockMarketAccount_];
            [auxStockMarketAccount_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (auxStockMarketAccount_ != nil) {
        
        [stockMarketAccountsList_ addObject:auxStockMarketAccount_];
        [auxStockMarketAccount_ release];
        auxStockMarketAccount_ = nil;
        
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the stock market account list
 *
 * @return The stock market account list
 */
- (NSArray *)stockMarketAccountList {
    
    return [NSArray arrayWithArray:stockMarketAccountsList_];
    
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Provides read access to the number of StockAccount stored
 *
 * @return The count
 */
- (NSUInteger) stockMarketAccountCount {
	return [stockMarketAccountsList_ count];
}

/*
 * Returns the StockAccount located at the given position, or nil if position is not valid
 */
- (StockMarketAccount*) stockMarketAccountAtPosition: (NSUInteger) aPosition {
	StockMarketAccount* result = nil;
	
	if (aPosition < [stockMarketAccountsList_ count]) {
		result = [stockMarketAccountsList_ objectAtIndex: aPosition];
	}
	
	return result;
}

/*
 * Returns the StockAccount position inside the list
 */
- (NSUInteger) stockMarketAccountPosition: (StockMarketAccount*) aStockMarketAccount {
    
	return [stockMarketAccountsList_ indexOfObject:aStockMarketAccount];
	
}

/*
 * Returns the stock market account with a value account number, or nil if not valid
 */
- (StockMarketAccount *)stockMarketAccountFromValueAccount:(NSString *)aStockMarketAccountValueAccount {
    
	StockMarketAccount *result = nil;
    
	if (aStockMarketAccountValueAccount != nil) {
        
		StockMarketAccount *target;
		NSUInteger size = [stockMarketAccountsList_ count];
		BOOL found = NO;
        
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
            
			target = [stockMarketAccountsList_ objectAtIndex:i];
            
			if ([aStockMarketAccountValueAccount isEqualToString:target.valueAccount]) {
                
				result = target;
				found = YES;
                
			}
            
		}
        
	}
    
	return result;
    
}

@end


#pragma mark -

@implementation StockMarketAccountList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearStockMarketAccountListData {
    
    [stockMarketAccountsList_ removeAllObjects];
    
    [auxStockMarketAccount_ release];
    auxStockMarketAccount_ = nil;
    
	informationUpdated_ = soie_NoInfo;
    
}

@end
