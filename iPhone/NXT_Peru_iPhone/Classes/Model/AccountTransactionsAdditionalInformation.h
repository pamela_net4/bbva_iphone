/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


/**
 * Stores one account transactions additional information element
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountTransactionsAdditionalInformation : StatusEnabledResponse {

@private
    
    /**
     * More date indicator
     */
    NSString *moreData_;
    
    /**
     * Index
     */
    NSString *index_;
    
}


/**
 * Provides read-write access to the more date indicator
 */
@property (nonatomic, readwrite, copy) NSString *moreData;

/**
 * Provides read-write access to the index
 */
@property (nonatomic, readwrite, copy) NSString *index;


/**
 * Updates this instance with the given account transactions additional information
 *
 * @param anAccountTransactionsAdditionalInformation The accounts transaction additional information to update from
 */
- (void)updateFrom:(AccountTransactionsAdditionalInformation *)anAccountTransactionsAdditionalInformation;

@end
