//
//  CuentaBanco.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 13/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "CuentaBanco.h"

typedef enum {
    axeas_AnalyzingNumeroCuenta = serxeas_ElementsCount,
    axeas_AnalyzingMoneda,
    axeas_AnalyzingSaldoContable
} CuentaXMLElementAnalyserState;

@interface CuentaBanco(private)

-(void) clearCuentaBanco;

@end

@implementation CuentaBanco

@synthesize numeroCuenta = numeroCuenta_;
@synthesize moneda = moneda_;
@synthesize saldoContable = saldoContable_;

- (void) updateFrom:(CuentaBanco *)cuentaOrigen {

    NSZone *zone = self.zone;
    
    [numeroCuenta_ release];
    numeroCuenta_ = nil;
    numeroCuenta_ = [cuentaOrigen.numeroCuenta copyWithZone:zone];
    
    [moneda_ release];
    moneda_ = nil;
    moneda_ = [cuentaOrigen.moneda copyWithZone:zone];
    
    [saldoContable_ release];
    saldoContable_ = nil;
    saldoContable_ = [cuentaOrigen.saldoContable copyWithZone:zone];
    
    [self updateFromStatusEnabledResponse:cuentaOrigen];
    
    self.informationUpdated = cuentaOrigen.informationUpdated;
}

- (void) clearCuentaBanco {
    [numeroCuenta_ release];
    numeroCuenta_ = nil;
    
    [moneda_ release];
    moneda_ = nil;
    
    [saldoContable_ release];
    saldoContable_ = nil;
}

- (void)dealloc
{
    [self clearCuentaBanco];
    [super dealloc];
}

- (id)init {
    if (self = [super init]){
        self.notificationToPost = kNotificationCuentaBancoUpdated;
    }
    return self;
}

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearCuentaBanco];
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    [super parser: parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName: qName attributes:attributeDict];
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString* lname = [elementName lowercaseString];
        
        if([lname
            isEqualToString:@"numerocuenta"]) {
            xmlAnalysisCurrentValue_ = axeas_AnalyzingNumeroCuenta;
        } else if ([lname isEqualToString:@"moneda"]) {
            xmlAnalysisCurrentValue_ = axeas_AnalyzingMoneda;
        } else if ([lname
                    isEqualToString:@"saldocontable"]) {
            xmlAnalysisCurrentValue_ = axeas_AnalyzingSaldoContable;
        } else {
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
        
    }

}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
    
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(xmlAnalysisCurrentValue_ == axeas_AnalyzingNumeroCuenta) {
            [numeroCuenta_ release];
            numeroCuenta_ = nil;
            numeroCuenta_ = [elementString copyWithZone:self.zone];
        } else if(xmlAnalysisCurrentValue_ == axeas_AnalyzingSaldoContable) {
            [saldoContable_ release];
            saldoContable_ = nil;
            saldoContable_ = [elementString copyWithZone:self.zone];
        } else if(xmlAnalysisCurrentValue_ == axeas_AnalyzingMoneda) {
            [moneda_ release];
            moneda_ = nil;
            moneda_ = [elementString copyWithZone:self.zone];
        }
    
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end
