/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GeneralLoginResponse.h"


/**
 * Contains a coordinate response, which can be either a general login response with an error code, or
 * an OK status
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CoordinateResponse : GeneralLoginResponse {

@private
    
    /**
     * Status
     */
   // NSString *status_;
    
}


///**
// * Provides read-only access to the status
// */
//@property (nonatomic, readonly, copy) NSString *status;

@end
