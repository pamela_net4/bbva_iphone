/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GeographicCoordinate.h"


#pragma mark -

@implementation GeographicCoordinate

#pragma mark -
#pragma mark Properties

@synthesize coordinate = coordinate_;

@end
