//
//  CuentaBanco.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 13/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"


@interface CuentaBanco : StatusEnabledResponse {
@private
    NSString *numeroCuenta_;
    NSDecimalNumber *saldoContable_;
    NSString *moneda_;
}

@property (nonatomic, readwrite, copy) NSString *numeroCuenta;
@property (nonatomic, readwrite, copy) NSDecimalNumber *saldoContable;
@property (nonatomic, readwrite, copy) NSString *moneda;

- (void) updateFrom: (CuentaBanco *) cuentaOrigen;

@end
