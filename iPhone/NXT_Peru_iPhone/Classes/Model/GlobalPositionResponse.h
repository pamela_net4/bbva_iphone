/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class AccountList;
@class CardList;
@class DepositList;
@class LoanList;
@class StockMarketAccountList;
@class MutualFundList;
@class GlobalAdditionalInformation;


/**
 * Contains the global position information obtained from the server
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GlobalPositionResponse : StatusEnabledResponse {
    
@private
	
    /**
     * Account list
     */
    AccountList *accountList_;
    
    /**
     * Credit card list
     */
    CardList *cardList_;
    
    /**
     * List of deposits
     */
    DepositList *depositsList_;
    
    /**
     * List of loans
     */
    LoanList *loansList_;
	
	/**
     * List of stock market accounts
     */
	StockMarketAccountList *stockMarketAccountsList_;
    
    /**
     * List of mutual funds
     */
    MutualFundList *mutualFundsList_;
    
    /**
     * Additional information
     */
    GlobalAdditionalInformation *additionalInformation_;

}

/**
 * Provides read-only access to the account list
 */
@property (nonatomic, readonly, retain) AccountList *accountList;

/**
 * Provides read-only access to the credit card list
 */
@property (nonatomic, readonly, retain) CardList *cardList;

/**
 * Provides read-only access to the deposits list
 */
@property (nonatomic, readonly, retain) DepositList *depositsList;

/**
 * Provides read-only access to the loans list
 */
@property (nonatomic, readonly, retain) LoanList *loansList;

/**
 * Provides read-only access to the stock market accounts list
 */
@property (nonatomic, readonly, retain) StockMarketAccountList *stockMarketAccountsList;

/**
 * Provides read-only access to the mutual funds list
 */
@property (nonatomic, readonly, retain) MutualFundList *mutualFundsList;

/**
 * Provides read-only access to the additional information
 */
@property (nonatomic, readonly, retain) GlobalAdditionalInformation *additionalInformation;

@end
