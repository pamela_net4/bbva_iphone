/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "CardForIncrementList.h"
#import "CardForIncrement.h"


#pragma mark -

/**
 * CardForIncrementList private category
 */
@interface CardForIncrementList (private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearCardForIncrementListData;

@end


#pragma mark -

@implementation CardForIncrementList

@dynamic cardforincrementList;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [self clearCardForIncrementListData];

	[cardforincrementList_ release];
	cardforincrementList_ = nil;

	[super dealloc];

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a CardForIncrementList instance creating the associated array
 *
 * @return The initialized CardForIncrementList instance
 */
- (id)init {

    if (self = [super init]) {

        cardforincrementList_ = [[NSMutableArray alloc] init];

    }

    return self;

}

#pragma mark -
#pragma mark Information management

/*
 * Updates the CardForIncrement list from another CardForIncrementList instance
 */
- (void)updateFrom:(CardForIncrementList *)aCardForIncrementList {

    NSArray *otherCardForIncrementList = aCardForIncrementList.cardforincrementList;

	[self clearCardForIncrementListData];

    for (CardForIncrement *cardforincrement in otherCardForIncrementList) {

        [cardforincrementList_ addObject:cardforincrement];

    }

    [self updateFromStatusEnabledResponse:aCardForIncrementList];

	self.informationUpdated = aCardForIncrementList.informationUpdated;

}

/*
 * Remove object data
 */
- (void)removeData {

    [self clearCardForIncrementListData];

}

#pragma mark -
#pragma mark Getters and setters

/**
 * Provides read access to the number of CardForIncrement stored
 *
 * @return The count
 */
- (NSUInteger) cardforincrementCount {
	return [cardforincrementList_ count];
}

/*
 * Returns the CardForIncrement located at the given position, or nil if position is not valid
 */
- (CardForIncrement*) cardforincrementAtPosition: (NSUInteger) aPosition {
	CardForIncrement* result = nil;

	if (aPosition < [cardforincrementList_ count]) {
		result = [cardforincrementList_ objectAtIndex: aPosition];
	}

	return result;
}

/*
 * Returns the CardForIncrement position inside the list
 */
- (NSUInteger) cardforincrementPosition: (CardForIncrement*) aCardForIncrement {

	return [cardforincrementList_ indexOfObject:aCardForIncrement];

}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {

    [super parserDidStartDocument:parser];

	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearCardForIncrementListData];

}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {

    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];

	if (auxCardForIncrement_ != nil) {

		[cardforincrementList_ addObject:auxCardForIncrement_];
		[auxCardForIncrement_ release];
		auxCardForIncrement_ = nil;

	}

	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        NSString* lname = [elementName lowercaseString];

        if ([lname isEqualToString: @"tarjeta"]) {

            auxCardForIncrement_ = [[CardForIncrement alloc] init];
            [auxCardForIncrement_ setParentParseableObject:self];
            auxCardForIncrement_.openingTag = lname;
            [parser setDelegate:auxCardForIncrement_];
            [auxCardForIncrement_ parserDidStartDocument: parser];

        } else {

            xmlAnalysisCurrentValue_ = serxeas_Nothing;

        }

    }

}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {

    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];

    if (auxCardForIncrement_ != nil) {

		[cardforincrementList_ addObject:auxCardForIncrement_];
		[auxCardForIncrement_ release];
		auxCardForIncrement_ = nil;

	}

    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        informationUpdated_ = soie_InfoAvailable;

    }

    xmlAnalysisCurrentValue_ = serxeas_Nothing;

}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the cardforincrement list array
 *
 * @return The cardforincrement list array
 */
- (NSArray *)cardforincrementList {

    return [NSArray arrayWithArray:cardforincrementList_];

}

/**
 * Returns the CardForIncrement with an CardForIncrement id, or nil if not valid
 *//*
- (CardForIncrement *)cardforincrementFromCardForIncrementId:(NSString *)aCardForIncrementId {

	CardForIncrement *cardforincrement = nil;

	if (aCardForIncrementId != nil) {
		CardForIncrement *target;
		NSUInteger size = [cardforincrementList_ count];
		BOOL found = NO;
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
			target = [cardforincrementList_ objectAtIndex:i];
			if ([aCardForIncrementId isEqualToString:target.id]) {
				cardforincrement = target;
				found = YES;
			}
		}
	}

	return cardforincrement;

}*/
@end


#pragma mark -

@implementation CardForIncrementList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearCardForIncrementListData {

    [cardforincrementList_ removeAllObjects];

	informationUpdated_ = soie_NoInfo;

}

@end

