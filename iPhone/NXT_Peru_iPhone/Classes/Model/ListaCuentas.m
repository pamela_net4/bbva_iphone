//
//  ListaCuentas.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 13/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "ListaCuentas.h"

@interface ListaCuentas(private)

- (void) clearListaCuentasData;

@end

@implementation ListaCuentas

@dynamic listaCuentas;

- (void)dealloc
{
    [self clearListaCuentasData];
    
    [listaCuentas_ release];
    listaCuentas_ = nil;
    
    [super dealloc];
}

- (id) init {
    if (self = [super init]) {
        listaCuentas_ = [[NSMutableArray alloc] init];
        self.notificationToPost = kNotificationListaCuentasReceived;
    }
    return self;
}

- (void) removeData {
    [self clearListaCuentasData];
}

- (void) clearListaCuentasData {
    [listaCuentas_ removeAllObjects];
    [cuentaBancoAux_ release];
    cuentaBancoAux_ = nil;
}

- (CuentaBanco *) cuentaAtPosition:(NSUInteger)aPosition {

    CuentaBanco *result = nil;
    if(aPosition < [listaCuentas_ count]){
        result = [listaCuentas_ objectAtIndex:aPosition];
    }
    return result;
}

- (NSArray *) listaCuentas {
    return [NSArray arrayWithArray:listaCuentas_];
}

- (void) updateFrom:(ListaCuentas *)listaCuentasOrigen {
    NSArray *cuentas = listaCuentasOrigen.listaCuentas;
    [self clearListaCuentasData];
    
    if (cuentas != nil) {
        for (CuentaBanco *cuenta in cuentas) {
            [listaCuentas_ addObject:cuenta];
        }
    }
    
    [self updateFromStatusEnabledResponse: listaCuentasOrigen];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearListaCuentasData];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if(cuentaBancoAux_ != nil){
        [listaCuentas_ addObject: cuentaBancoAux_];
        
        [cuentaBancoAux_ release];
        cuentaBancoAux_ = nil;
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        NSString *lname = [elementName lowercaseString];
        
        if([lname isEqualToString:@"e"]){
            [cuentaBancoAux_ release];
            cuentaBancoAux_ = [[CuentaBanco alloc] init];
            [cuentaBancoAux_ setParentParseableObject:self];
            cuentaBancoAux_.openingTag = lname;
            [parser setDelegate:cuentaBancoAux_];
            [cuentaBancoAux_ parserDidStartDocument:parser];
        } else {
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
    }

}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (cuentaBancoAux_ != nil) {
        [listaCuentas_ addObject:cuentaBancoAux_];
        
        [cuentaBancoAux_ release];
        cuentaBancoAux_ = nil;
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        informationUpdated_ = soie_InfoAvailable;
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
}

@end
