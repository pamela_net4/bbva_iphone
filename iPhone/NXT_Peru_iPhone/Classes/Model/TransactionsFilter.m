/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "TransactionsFilter.h"
#import "Tools.h"


#pragma mark -

@implementation TransactionsFilter

#pragma mark -

@synthesize lastDays = lastDays_;
@synthesize showIncomes = showIncomes_;
@synthesize showPayments = showPayments_;
@synthesize uniqueDay = uniqueDay_;
@synthesize selectedMonth = selectedMonth_;
@dynamic isEmptyFilter;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [uniqueDay_ release];
    uniqueDay_ = nil;
	
    [selectedMonth_ release];
    selectedMonth_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designates initializer. Initializes an empty TransactionsFilter instance
 *
 * @return An empty TransactionsFilter instance
 */
- (id)init {
    
    return [self initWithFilter:nil];
    
}

/*
 * Designated initializer. Initializes a TransactionsFilter instance with another filter options
 */
- (id)initWithFilter:(TransactionsFilter *)filterOptions {
    
    if ((self = [super init])) {
        
        if (filterOptions == nil) {
            
            lastDays_ = lde_All;
            uniqueDay_ = nil;
            selectedMonth_ = nil;
            showIncomes_ = YES;
            showPayments_ = YES;

        } else {
            
            lastDays_ = filterOptions.lastDays;
            
            if (filterOptions.uniqueDay == nil) {
                
                [uniqueDay_ release];
                uniqueDay_ = nil;
                
            } else {
                
                [uniqueDay_ release];
                uniqueDay_ = [[Tools getDateWithTimeToZero:filterOptions.uniqueDay] retain];
                
            }
            
            if (filterOptions.selectedMonth == nil) {
                
                [selectedMonth_ release];
                selectedMonth_ = nil;
                
            } else {
                
                [selectedMonth_ release];
                selectedMonth_ = [[Tools getDateWithTimeToZero:filterOptions.selectedMonth] retain];
                
            }
            
            showIncomes_ = filterOptions.showIncomes;
            showPayments_ = filterOptions.showPayments;
            
        }
        
	}
    
    return self;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the unique day
 * 
 * @param uniqueDay The date to set
 */
- (void)setUniqueDay:(NSDate *)uniqueDay {
    
    if (uniqueDay != uniqueDay_) {
        
        [uniqueDay_ release];
        
        if (uniqueDay != nil) {
            
            uniqueDay_ = [[Tools getDateWithTimeToZero:uniqueDay] retain];
            
            lastDays_ = lde_None;
            
            [selectedMonth_ release];
            selectedMonth_ = nil;
            
        } else {
            
            uniqueDay_ = nil;
            
        }
        
    }
    
}

/*
 * Sets the unique day
 * 
 * @param selectedMonth The month
 */
- (void)setSelectedMonth:(NSDate *)aMonth {
    
 	[selectedMonth_ release];
    
	if (aMonth != nil) {
        
		selectedMonth_ = [[Tools getDateWithTimeToZero:aMonth] retain];
        
	    lastDays_ = lde_None;
		
		[uniqueDay_ release];
		uniqueDay_ = nil;
		
	} else {
        
		selectedMonth_ = nil;
        
	}
}

/*
 * Returns the is empty filter flag
 *
 * @return The is empty filter flag
 */
- (BOOL)isEmptyFilter {
    
    BOOL result = NO;
    
    if ((lastDays_ == lde_All) && (showIncomes_) && (showPayments_)) {
        
        result = YES;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark NSCopying protocol selectors

/**
 * Returns a new instance that’s a copy of the receiver.
 *
 * @param zone The zone identifies an area of memory from which to allocate for the new instance
 * @return The new instance with the copied information
 */
- (id)copyWithZone:(NSZone *)zone {
    
    TransactionsFilter *copy = [[[self class] allocWithZone:zone] init];
    
    copy->lastDays_ = lastDays_;
    copy->showIncomes_ = showIncomes_;
    copy->showPayments_ = showPayments_;
    
    if (uniqueDay_ != nil) {
        
        copy->uniqueDay_ = [uniqueDay_ copy];
        
    }
    
    if (selectedMonth_ != nil) {
        
        copy->selectedMonth_ = [selectedMonth_ copy];
        
    }
    
    return copy;
    
}

@end
