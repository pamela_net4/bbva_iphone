//
//  IncrementCreditLineResultResponse.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"


@interface IncrementCreditLineResultResponse : StatusEnabledResponse{
    @private

   /**
     * IncrementCreditLineResultResponse clientName
     */
    NSString *clientName_;


   /**
     * IncrementCreditLineResultResponse contract
     */
    NSString *contract_;


   /**
     * IncrementCreditLineResultResponse cardNumber
     */
    NSString *cardNumber_;


   /**
     * IncrementCreditLineResultResponse cardName
     */
    NSString *cardName_;


   /**
     * IncrementCreditLineResultResponse currency
     */
    NSString *currency_;


   /**
     * IncrementCreditLineResultResponse amountCreditPrevious
     */
    NSString *amountCreditPrevious_;


   /**
     * IncrementCreditLineResultResponse amountCredit
     */
    NSString *amountCredit_;


   /**
     * IncrementCreditLineResultResponse email
     */
    NSString *email_;


   /**
     * IncrementCreditLineResultResponse date
     */
    NSString *date_;


   /**
     * IncrementCreditLineResultResponse hour
     */
    NSString *hour_;


   /**
     * IncrementCreditLineResultResponse confirmationMessage
     */
    NSString *confirmationMessage_;




}

/**
 * Provides read-only access to the IncrementCreditLineResultResponse clientName
 */
@property (nonatomic, readonly, copy) NSString * clientName;


/**
 * Provides read-only access to the IncrementCreditLineResultResponse contract
 */
@property (nonatomic, readonly, copy) NSString * contract;


/**
 * Provides read-only access to the IncrementCreditLineResultResponse cardNumber
 */
@property (nonatomic, readonly, copy) NSString * cardNumber;


/**
 * Provides read-only access to the IncrementCreditLineResultResponse cardName
 */
@property (nonatomic, readonly, copy) NSString * cardName;


/**
 * Provides read-only access to the IncrementCreditLineResultResponse currency
 */
@property (nonatomic, readonly, copy) NSString * currency;


/**
 * Provides read-only access to the IncrementCreditLineResultResponse amountCreditPrevious
 */
@property (nonatomic, readonly, copy) NSString * amountCreditPrevious;


/**
 * Provides read-only access to the IncrementCreditLineResultResponse amountCredit
 */
@property (nonatomic, readonly, copy) NSString * amountCredit;


/**
 * Provides read-only access to the IncrementCreditLineResultResponse email
 */
@property (nonatomic, readonly, copy) NSString * email;


/**
 * Provides read-only access to the IncrementCreditLineResultResponse date
 */
@property (nonatomic, readonly, copy) NSString * date;


/**
 * Provides read-only access to the IncrementCreditLineResultResponse hour
 */
@property (nonatomic, readonly, copy) NSString * hour;


/**
 * Provides read-only access to the IncrementCreditLineResultResponse confirmationMessage
 */
@property (nonatomic, readonly, copy) NSString * confirmationMessage;




@end

