//
//  ThirdAccountList.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/14/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "ThirdAccountList.h"
#import "ThirdAccount.h"

#pragma mark -

/**
 * ThirdAccountList private category
 */
@interface ThirdAccountList(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearAccountListData;


@end


@implementation ThirdAccountList


#pragma mark -
#pragma mark Properties

@dynamic thirdAccountList;
@dynamic thirdAccountCount;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearAccountListData];
    
    [thirdAccountList_ release];
    thirdAccountList_ = nil;
    
    [auxThirdAccount_ release];
    auxThirdAccount_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Initialization

/**
 * Object initializer
 *
 * @return An initialized instance
 */
- (id) init {
    
    if (self = [super init]) {
        
        thirdAccountList_ = [[NSMutableArray alloc] init];
        informationUpdated_ = soie_NoInfo;
        self.notificationToPost = kNotificationThirdAccountListReceived;
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates the account transaction list from another account transaction list
 */
- (void)updateFrom:(ThirdAccountList *)anAccountTransactionList {
    
    NSArray *accounts = anAccountTransactionList.thirdAccountList;
    
    [self clearAccountListData];
    
    if (accounts != nil) {
        
        for (ThirdAccount *account in accounts) {
            
            [thirdAccountList_ addObject:account];
            
        }
        
    }
    
    [self updateFromStatusEnabledResponse:anAccountTransactionList];
    
    self.informationUpdated = anAccountTransactionList.informationUpdated;
    
}

/*
 * Remove the contained data
 */
- (void)removeData {
    
    [self clearAccountListData];
    
}

/*
 * Returns the AccountTransaction located at the given position, or nil if position is not valid
 */
- (ThirdAccount *)thirdAccountAtPosition:(NSUInteger)aPosition {
    
    ThirdAccount *result = nil;
    
    if (aPosition < [thirdAccountList_ count]) {
        
        result = [thirdAccountList_ objectAtIndex:aPosition];
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearAccountListData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (auxThirdAccount_ != nil) {
        
        [thirdAccountList_ addObject:auxThirdAccount_];
        [auxThirdAccount_ release];
        auxThirdAccount_ = nil;
        
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
            [auxThirdAccount_ release];
            auxThirdAccount_ = [[ThirdAccount alloc] init];
            [auxThirdAccount_ setParentParseableObject:self];
            auxThirdAccount_.openingTag = lname;
            [parser setDelegate:auxThirdAccount_];
            [auxThirdAccount_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (auxThirdAccount_ != nil) {
        
        [thirdAccountList_ addObject:auxThirdAccount_];
        [auxThirdAccount_ release];
        auxThirdAccount_ = nil;
        
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the account transaction list
 *
 * @return The account transaction list
 */
- (NSArray *)thirdAccountList {
    
    return thirdAccountList_;
    
}

/*
 * Returns the account transactions count
 *
 * @return The account transactions count
 */
- (NSUInteger)thirdAccountCount {
    
    return [thirdAccountList_ count];
    
}


@end



#pragma mark -

@implementation ThirdAccountList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearAccountListData {
    
    [thirdAccountList_ removeAllObjects];
    
}





@end
