//
//  RegistrationThirdAccountSuccessResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/14/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
#import "ThirdAccountConfirmationAdditionalInformation.h"

typedef enum {
    rtasxeas_AnalyzingThirdAccountResponseConfirmation,
    rtasxeas_AnalyzingAdditionalInformation,
} rtasxeas_AnalyzingThirdAccountConfirmationResponseXMLElementAnalyzerState;


@interface RegistrationThirdAccountSuccessResponse : StatusEnabledResponse
{
@private
    ThirdAccountConfirmationAdditionalInformation *additionalInformation_;
    NSError *parseError_;
}
@property (nonatomic, readonly, retain) ThirdAccountConfirmationAdditionalInformation *additionalInformation;
@property (nonatomic, readonly, assign) NSError *parseError;
@end
