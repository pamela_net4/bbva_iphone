//
//  ThirdAccountDetailsAdditionalInformation.m
//  NXT_Peru_iPad
//
//  Created by Jose Contreras Sanchez on 5/26/15.
//
//

#import "ThirdAccountDetailsAdditionalInformation.h"

@interface ThirdAccountDetailsAdditionalInformation (private)

- (void) clearThirdAccountDetailsAdditionalInformationData;

@end

@implementation ThirdAccountDetailsAdditionalInformation

#pragma mark -
#pragma mark Properties
@synthesize accountDetail = accountDetail_;
@synthesize operationTitle = operationTitle_;
@synthesize seal = seal_;
@synthesize coordinate = coordinate_;
@synthesize parseError = parseError_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearThirdAccountDetailsAdditionalInformationData];
    
    [super dealloc];
    
}

- (void) removeData {
    
    [self clearThirdAccountDetailsAdditionalInformationData];
    
}
- (void) updateFrom:(ThirdAccountDetailsAdditionalInformation *)anAdditionalInformation{
    
    [operationTitle_ release];
    operationTitle_ = nil;
    operationTitle_ = [anAdditionalInformation.operationTitle copyWithZone:self.zone];
    
    [coordinate_ release];
    coordinate_ = nil;
    coordinate_ = [anAdditionalInformation.coordinate copyWithZone:self.zone];
    
    [seal_ release];
    seal_ = nil;
    seal_ = [anAdditionalInformation.seal copyWithZone: self.zone];
    
    [accountDetail_ release];
    accountDetail_ = nil;
    accountDetail_ = [ThirdAccount alloc];
    [accountDetail_ updateFrom: [anAdditionalInformation accountDetail]];
}


#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
-(void) parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearThirdAccountDetailsAdditionalInformationData];
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing){
        
        NSString *lname = [elementName lowercaseString];
        
        
        if([lname isEqualToString: @"operacion"]){
            xmlAnalysisCurrentValue_ = tadaixeas_AnalyzingOperationTitle;
        }
        if([lname isEqualToString: @"cuenta"]){
            xmlAnalysisCurrentValue_ = tadaixeas_AnalyzingThirdAccountResponseDetail;
            [accountDetail_ release];
            accountDetail_ = nil;
            accountDetail_ = [[ThirdAccount alloc] init];
            accountDetail_.openingTag = lname;
            [accountDetail_ setParentParseableObject:self];
            [parser setDelegate:accountDetail_];
            [accountDetail_ parserDidStartDocument:parser];
        }
        
        if([lname isEqualToString: @"coordenada"]){
            xmlAnalysisCurrentValue_ = tadaixeas_AnalyzingCoordinate;
        }
        if([lname isEqualToString: @"sello"]){
            xmlAnalysisCurrentValue_ = tadaixeas_AnalyzingSeal;
        }
        
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if(xmlAnalysisCurrentValue_ != serxeas_Nothing){
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if([lname isEqualToString: @"operacion"]){
            
            [operationTitle_ release];
            operationTitle_ = nil;
            operationTitle_ = [elementString copyWithZone:self.zone];
            
        } else if([lname isEqualToString: @"coordenada"]){
            
            [coordinate_ release];
            coordinate_ = nil;
            coordinate_ = [elementString copyWithZone:self.zone];
            
        } else if([lname isEqualToString: @"sello"]){
            
            [seal_ release];
            seal_ = nil;
            
            //seal_ = [[elementString copyWithZone:self.zone] stringByReplacingOccurrencesOfString:@"\r" withString:@""];
            //seal_ = [seal_ stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            seal_ = [elementString copyWithZone:self.zone];
        }
        
    }
    
}
- (void) parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    [super parser:parser parseErrorOccurred:parseError];
    parseError_ = parseError;
    [self.parentParseableObject parser:parser parseErrorOccurred:parseError];
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void) clearThirdAccountDetailsAdditionalInformationData {
    [operationTitle_ release];
    operationTitle_ = nil;
    
    [coordinate_ release];
    coordinate_ = nil;
    
    [seal_ release];
    seal_ = nil;
    
    [accountDetail_ release];
    accountDetail_ = nil;
}

@end