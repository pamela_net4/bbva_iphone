//
//  ThirdAccount.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/14/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "ThirdAccount.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    taxeas_AnalyzingOperationDate = serxeas_ElementsCount, //!<Analyzing the transaction operation date
    taxeas_AnalyzingAccountNumber, //!<Analyzing the account number
    taxeas_AnalyzingOperationType, //!<Analyzing the account type
    taxeas_AnalyzingOperationTypeId, //!<Analyzing the account type id
    taxeas_AnalyzingNickname, //!<Analyzing the nickname account
    taxeas_AnalyzingOpDate, //!<Analyzing the date creation
    
} ThirdAccountXMLElementAnalyzerState;


#pragma mark -

/**
 * ThirdAccount private category
 */
@interface ThirdAccount(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearAccountTransactionData;

@end

@implementation ThirdAccount

#pragma mark -
#pragma mark Properties
@synthesize accountNumber = accountNumber_;
@synthesize operationType = operationType_;
@synthesize nickName = nickName_;
@synthesize operationDate = operationDate_;
@synthesize operationTypeId = operationTypeId_;


#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearAccountTransactionData];
    
    [super dealloc];
    
}


#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initialized. Initializes an AccountTransaction instance
 *
 * @return The initialized AccountTransaction instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        self.notificationToPost = kNotificationThirdAccountUpdated;
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates this instance with the given account transaction
 */
- (void)updateFrom:(ThirdAccount *)aThirdAccount {
    
    NSZone *zone = self.zone;
    
    [accountNumber_ release];
    accountNumber_ = nil;
    accountNumber_ = [aThirdAccount.accountNumber copyWithZone:zone];

    [operationType_ release];
    operationType_ = nil;
    operationType_ = [aThirdAccount.operationType copyWithZone:zone];
    
    [operationTypeId_ release];
    operationTypeId_ = nil;
    operationTypeId_ = [aThirdAccount.operationTypeId copyWithZone:zone];

    
    [nickName_ release];
    nickName_ = nil;
    nickName_ = [aThirdAccount.nickName copyWithZone:zone];
    
    [operationDate_ release];
    operationDate_ = nil;
    operationDate_ = [aThirdAccount.operationDate copyWithZone:zone];

    
    [self updateFromStatusEnabledResponse:aThirdAccount];
    
    self.informationUpdated = aThirdAccount.informationUpdated;
}


#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearAccountTransactionData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"nrocuenta"]) {
            
            xmlAnalysisCurrentValue_ = taxeas_AnalyzingAccountNumber;
            
        } else if ([lname isEqualToString: @"tipoperacion"]) {
            
            xmlAnalysisCurrentValue_ = taxeas_AnalyzingOperationType;
            
        } else if ([lname isEqualToString: @"idtipoperacion"]) {
            
            xmlAnalysisCurrentValue_ = taxeas_AnalyzingOperationTypeId;
            
        } else if ([lname isEqualToString: @"alias"]) {
            
            xmlAnalysisCurrentValue_ = taxeas_AnalyzingNickname;
            
        } else if ([lname isEqualToString: @"fecha"]) {
            
            xmlAnalysisCurrentValue_ = taxeas_AnalyzingOpDate;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == taxeas_AnalyzingAccountNumber) {
            
            [accountNumber_ release];
            accountNumber_ = nil;
            accountNumber_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == taxeas_AnalyzingOperationType) {
            
            [operationType_ release];
            operationType_ = nil;
            operationType_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == taxeas_AnalyzingOperationTypeId) {
            
            [operationTypeId_ release];
            operationTypeId_ = nil;
            operationTypeId_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == taxeas_AnalyzingNickname) {
            
            [nickName_ release];
            nickName_ = nil;
            nickName_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == taxeas_AnalyzingOpDate) {
            
            [operationDate_ release];
            operationDate_ = nil;
            operationDate_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

@implementation ThirdAccount (private)


/**
 * Clears the object data
 *
 * @private
 */
- (void)clearAccountTransactionData
{
    
}

@end
