//
//  NicknameValidationResponse.h
//  NXT_Peru_iPad
//
//  Created by Jose Contreras Sanchez on 5/26/15.
//
//

#import "StatusEnabledResponse.h"

@interface NicknameValidationAdditionalInformation : StatusEnabledResponse
{
    /*
     indicator of validation
     */
    NSString *indicatorNickname_;
    /*
     message alert
     */
    NSString *messageValidation_;
}
/**
 * Provides read-only access to indicator
 */
@property (nonatomic, readonly, retain) NSString *indicatorNickname;

/**
 * Provides read-only access to indicator
 */
@property (nonatomic, readonly, retain) NSString *messageValidation;

@end
