//
//  ThirdAccountConfirmationAdditionalInformation.h
//  NXT_Peru_iPad
//
//  Created by Jose Contreras Sanchez on 5/26/15.
//
//

#import "StatusEnabledResponse.h"
#import "ThirdAccount.h"

typedef enum {
    tacaixeas_AnalyzingThirdAccountResponseConfirmationDetail,
    tacaixeas_AnalyzingOperationTitle,
    tacaixeas_AnalyzingWaitNumber
} tacaixeas_AnalyzingThirdAccountRegistrationResponseXMLElementAnalyzerState;

@interface ThirdAccountConfirmationAdditionalInformation : StatusEnabledResponse
{
@private
    NSString *operationTitle_;
    ThirdAccount *accountDetail_;
    NSString *waitNumber_;
    NSError *parseError_;
}

@property (nonatomic, readonly, copy) NSString *operationTitle;
@property (nonatomic, readonly, copy) ThirdAccount *accountDetail;
@property (nonatomic, readonly, copy) NSString *waitNumber;
@property (nonatomic, readonly, copy) NSError *parseError;

- (void) updateFrom :(ThirdAccountConfirmationAdditionalInformation *) anAdditionalInformation ;
- (void) removeData;
@end
