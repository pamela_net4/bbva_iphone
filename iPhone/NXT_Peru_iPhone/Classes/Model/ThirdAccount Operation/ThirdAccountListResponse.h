//
//  ThirdAccountListResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/14/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@class ThirdAccountList;

@interface ThirdAccountListResponse : StatusEnabledResponse
{
@private
    /*
     Third Account List
     */
    ThirdAccountList *thirdAccountList_;
    /*
     more data
     */
    NSString *moreData_;
    /*
     next page
     */
    NSString *nextPage_;
    /*
     back page
     */
    NSString *backPage_;
}
/**
 * Provides read-only access to the Third Account List
 */
@property (nonatomic, readonly, retain) ThirdAccountList *thirdAccountList;
/**
 * Provides read-only access to the more date
 */
@property (nonatomic, readonly, retain) NSString *moreData;
/**
 * Provides read-only access to the next page
 */
@property (nonatomic, readonly, retain) NSString *nextPage;
/**
 * Provides read-only access to the back page
 */
@property (nonatomic, readonly, retain) NSString *backPage;

@property (nonatomic, readonly, assign) BOOL hasMoreData;

@end
