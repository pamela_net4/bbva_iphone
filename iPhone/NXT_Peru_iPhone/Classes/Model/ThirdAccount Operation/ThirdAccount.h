//
//  ThirdAccount.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/14/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//
#import "StatusEnabledResponse.h"

@interface ThirdAccount :StatusEnabledResponse {
    
@private
    
    NSString *accountNumber_;
    
    NSString *operationType_;
    
    NSString *operationTypeId_;
    
    NSString *nickName_;
    
    NSString *operationDate_;

}

@property (nonatomic, readonly, retain) NSString *accountNumber;

@property (nonatomic, readonly, retain) NSString *operationType;


@property (nonatomic, readonly, retain) NSString *operationTypeId;

@property (nonatomic, readonly, retain) NSString *nickName;

@property (nonatomic, readonly, retain) NSString *operationDate;

/**
 * Provides read-only access to the account transaction list array
 */
@property (nonatomic, readonly, retain) NSArray *accountTransactionList;

/**
 * Provides read-only access to the account transactions count
 */
@property (nonatomic, readonly, assign) NSUInteger accountTransactionCount;


/**
 * Updates this instance with the given account transaction
 *
 * @param anAccountTransaction The account transaction to update from
 */
- (void)updateFrom:(ThirdAccount *)aThirdAccount;

@end
