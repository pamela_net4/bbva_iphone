//
//  ThirdAccountListResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/14/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "ThirdAccountList.h"

#import "ThirdAccountListResponse.h"
/**
 * Enumerates the analysis state
 */
typedef enum {
    
    talrxeas_AnalyzingFrequentOperationList = serxeas_ElementsCount, //!<Analyzing the account list
    talrxeas_AnalyzingMoreData,
    talrxeas_AnalyzingNextPage,
    talrxeas_AnalyzingBackPage
    
} ThirdAccountListResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * FOPInstitutionPaymentStepOneResponse private category
 */
@interface ThirdAccountListResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearThirdAccountListResponseData;

@end


@implementation ThirdAccountListResponse

#pragma mark -
#pragma mark Properties
@synthesize thirdAccountList = thirdAccountList_;
@synthesize moreData = moreData_;
@synthesize nextPage = nextPage_;
@synthesize backPage = backPage_;
@dynamic hasMoreData;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearThirdAccountListResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOGeneralMenuStepOneResponse instance providing the associated notification
 *
 * @return The initialized FOGeneralMenuStepOneResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        self.notificationToPost =  kNotificationThirdAccountListResponse;
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearThirdAccountListResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"cuentasbeneficiarias"]) {
            
            xmlAnalysisCurrentValue_ = talrxeas_AnalyzingFrequentOperationList;
            [thirdAccountList_ release];
            thirdAccountList_ = nil;
            thirdAccountList_ = [[ThirdAccountList alloc] init];
            thirdAccountList_.openingTag = lname;
            [thirdAccountList_ setParentParseableObject:self];
            [parser setDelegate:thirdAccountList_];
            [thirdAccountList_ parserDidStartDocument:parser];
            
            
        } else if ([lname isEqualToString: @"masdatos"]) {
            
            xmlAnalysisCurrentValue_ = talrxeas_AnalyzingMoreData;
            
        } else if ([lname isEqualToString: @"nextpage"]) {
            
            xmlAnalysisCurrentValue_ = talrxeas_AnalyzingNextPage;
            
        }else if ([lname isEqualToString: @"backpage"]) {
            
            xmlAnalysisCurrentValue_ = talrxeas_AnalyzingBackPage;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == talrxeas_AnalyzingMoreData)
        {
            [moreData_ release];
            moreData_ = nil;
            moreData_ = [elementString copyWithZone:self.zone];
        }
        else if(xmlAnalysisCurrentValue_ == talrxeas_AnalyzingNextPage)
        {
            [nextPage_ release];
            nextPage_ = nil;
            nextPage_ = [elementString copyWithZone:self.zone];;
        }
        else if(xmlAnalysisCurrentValue_ == talrxeas_AnalyzingBackPage)
        {
            [backPage_ release];
            backPage_ = nil;
            backPage_ = [elementString copyWithZone:self.zone];
        }
        else if ([elementString isEqualToString: @"msg-s"]) {
            
            informationUpdated_ = soie_InfoAvailable;
            [parser setDelegate:parentParseableObject_];
            parentParseableObject_ = nil;
            
        }
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
    }
    
}

#pragma mark -
#pragma mark properties implementation
- (BOOL)hasMoreData{
    
    BOOL result = FALSE;
    
    if ([@"s" isEqualToString:[moreData_ lowercaseString]] || [@"si" isEqualToString:[moreData_ lowercaseString]]) {
        result = TRUE;
    }
    
    
    return result;
}

@end



#pragma mark -

@implementation ThirdAccountListResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearThirdAccountListResponseData {
    
    [thirdAccountList_ release];
    thirdAccountList_ = nil;
    
    [nextPage_ release];
    nextPage_ = nil;
    
    [backPage_ release];
    backPage_ = nil;
    
    [moreData_ release];
    moreData_ = nil;

    
}

@end