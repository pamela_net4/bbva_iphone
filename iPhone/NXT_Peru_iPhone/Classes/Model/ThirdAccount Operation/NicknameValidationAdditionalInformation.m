//
//  NicknameValidationResponse.m
//  NXT_Peru_iPad
//
//  Created by Jose Contreras Sanchez on 5/26/15.
//
//

#import "NicknameValidationAdditionalInformation.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    nvrxeas_AnalyzingIndicatorNickname = serxeas_ElementsCount, //!<Analyzing the account list
    nvrxeas_AnalyzingMessageValidation,

    
} NicknameValidationResponseXMLElementAnalyzerState;

/**
 * FOPInstitutionPaymentStepOneResponse private category
 */
@interface NicknameValidationAdditionalInformation(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearNicknameValidationResponseData;

@end

@implementation NicknameValidationAdditionalInformation

#pragma mark -
#pragma mark Properties
@synthesize indicatorNickname = indicatorNickname_;
@synthesize messageValidation = messageValidation_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearNicknameValidationResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a FOGeneralMenuStepOneResponse instance providing the associated notification
 *
 * @return The initialized FOGeneralMenuStepOneResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        // self.notificationToPost =  kNotificationThirdAccountListResponse;
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearNicknameValidationResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"indicador"]) {
            
            xmlAnalysisCurrentValue_ = nvrxeas_AnalyzingIndicatorNickname;
            
        } else if ([lname isEqualToString: @"mensajealias"]) {
            
            xmlAnalysisCurrentValue_ = nvrxeas_AnalyzingMessageValidation;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}
/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == nvrxeas_AnalyzingIndicatorNickname) {
            
            [indicatorNickname_ release];
            indicatorNickname_ = nil;
            indicatorNickname_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == nvrxeas_AnalyzingMessageValidation) {
            
            [messageValidation_ release];
            messageValidation_ = nil;
            messageValidation_ = [elementString copyWithZone:self.zone];
            
        }
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end

#pragma mark -

@implementation NicknameValidationAdditionalInformation(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearNicknameValidationResponseData {
    
    
    
    
}

@end
