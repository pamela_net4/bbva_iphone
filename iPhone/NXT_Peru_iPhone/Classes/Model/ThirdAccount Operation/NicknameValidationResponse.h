//
//  NicknameValidationResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/28/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
#import "NicknameValidationAdditionalInformation.h"

typedef enum {
    nvrxeas_AnalyzingNickNameResponse,
    nvrxeas_AnalyzingAdditionalInformation,
} nvrxeas_AnalyzingNicknameValidationResponseXMLElementAnalyzerState;

@interface NicknameValidationResponse : StatusEnabledResponse
{
@private
    NicknameValidationAdditionalInformation *additionalInformation_;
    NSError *parseError_;
}
@property (nonatomic, readonly, retain) NicknameValidationAdditionalInformation *additionalInformation;
@property (nonatomic, readonly, assign) NSError *parseError;
@end

