//
//  ThirdAccountList.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/14/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

//Forward declarations
@class ThirdAccount;

@interface ThirdAccountList : StatusEnabledResponse
{
    /**
     * Account  list
     */
    NSMutableArray *thirdAccountList_;
    
    /**
     * Auxiliary account
     */
    ThirdAccount *auxThirdAccount_;

}

/**
 * Provides read-only access to the account transaction list array
 */
@property (nonatomic, readonly, retain) NSArray *thirdAccountList;

/**
 * Provides read-only access to the account transactions count
 */
@property (nonatomic, readonly, assign) NSUInteger thirdAccountCount;


/**
 * Updates this instance with the given account transaction list
 *
 * @param anAccountTransactionList The account transaction list to update from
 */
- (void)updateFrom:(ThirdAccount *)aThirdAccount;

/**
 * Returns the AccountTransaction located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the AccountTransaction is located at
 * @return AccountTransaction located at the given position, or nil if position is not valid
 */
- (ThirdAccount *)thirdAccountAtPosition:(NSUInteger)aPosition;

/**
 * Remove the contained data
 */
- (void)removeData;
@end
