//
//  ThirdAccountDetailsAdditionalInformation.h
//  NXT_Peru_iPad
//
//  Created by Jose Contreras Sanchez on 5/26/15.
//
//

#import "StatusEnabledResponse.h"
#import "ThirdAccount.h"

typedef enum {
    tadaixeas_AnalyzingThirdAccountResponseDetail,
    tadaixeas_AnalyzingCoordinate,
    tadaixeas_AnalyzingSeal,
    tadaixeas_AnalyzingOperationTitle
} tadaixeas_AnalyzingThirdAccountRegistrationResponseXMLElementAnalyzerState;

@interface ThirdAccountDetailsAdditionalInformation : StatusEnabledResponse
{
@private
    NSString *operationTitle_;
    ThirdAccount *accountDetail_;
    NSString *coordinate_;
    NSString *seal_;
    NSError *parseError_;
}

@property (nonatomic, readonly, copy) NSString *operationTitle;
@property (nonatomic, readonly, copy) ThirdAccount *accountDetail;
@property (nonatomic, readonly, copy) NSString *coordinate;
@property (nonatomic, readonly, copy) NSString *seal;
@property (nonatomic, readonly, copy) NSError *parseError;

- (void) updateFrom :(ThirdAccountDetailsAdditionalInformation *) anAdditionalInformation ;
- (void) removeData;
@end
