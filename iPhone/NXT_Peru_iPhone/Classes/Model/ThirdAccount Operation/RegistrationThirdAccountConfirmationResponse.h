//
//  RegistrationThirdAccountConfirmationResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/14/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
#import "ThirdAccountDetailsAdditionalInformation.h"


typedef enum {
    rtacxeas_AnalyzingThirdAccountResponseDetail,
    rtacxeas_AnalyzingAdditionalInformation,
} rtacxeas_AnalyzingThirdAccountResponseXMLElementAnalyzerState;


@interface RegistrationThirdAccountConfirmationResponse : StatusEnabledResponse
{
@private
    ThirdAccountDetailsAdditionalInformation *additionalInformation_;
    NSError *parseError_;
}
@property (nonatomic, readonly, retain) ThirdAccountDetailsAdditionalInformation *additionalInformation;
@property (nonatomic, readonly, assign) NSError *parseError;
@end
