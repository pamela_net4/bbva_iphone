//
//  FastLoanAlternative.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"


@interface FastLoanAlternative : StatusEnabledResponse{
    @private

   /**
     * FastLoanAlternative number
     */
    NSString *number_;


   /**
     * FastLoanAlternative quotaValue
     */
    NSString *quotaValue_;


   /**
     * FastLoanAlternative quotaType
     */
    NSString *quotaType_;


   /**
     * FastLoanAlternative monthDobles
     */
    NSString *monthDobles_;


   /**
     * FastLoanAlternative term
     */
    NSString *term_;


   /**
     * FastLoanAlternative tcea
     */
    NSString *tcea_;


   /**
     * FastLoanAlternative quotaTotal
     */
    NSString *quotaTotal_;




}

/**
 * Provides read-only access to the FastLoanAlternative number
 */
@property (nonatomic, readonly, copy) NSString * number;


/**
 * Provides read-only access to the FastLoanAlternative quotaValue
 */
@property (nonatomic, readonly, copy) NSString * quotaValue;


/**
 * Provides read-only access to the FastLoanAlternative quotaType
 */
@property (nonatomic, readonly, copy) NSString * quotaType;


/**
 * Provides read-only access to the FastLoanAlternative monthDobles
 */
@property (nonatomic, readonly, copy) NSString * monthDobles;


/**
 * Provides read-only access to the FastLoanAlternative term
 */
@property (nonatomic, readonly, copy) NSString * term;


/**
 * Provides read-only access to the FastLoanAlternative tcea
 */
@property (nonatomic, readonly, copy) NSString * tcea;


/**
 * Provides read-only access to the FastLoanAlternative quotaTotal
 */
@property (nonatomic, readonly, copy) NSString * quotaTotal;




@end

