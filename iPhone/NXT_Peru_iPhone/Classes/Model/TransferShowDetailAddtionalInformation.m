//
//  TransferShowDetailAddtionalInformation.m
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 10/2/13.
//
//

#import "TransferShowDetailAddtionalInformation.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    tsaixeas_AnalyzingOperationNumber = serxeas_ElementsCount, //!<Analyzing the operation number
    tsaixeas_AnalyzingDate, //!<Analyzing the date
    tsaixeas_AnalyzingExpirationDate, //!<Analyzing the expiration date String
    tsaixeas_AnalyzingHour, //!<Analyzing the hour
    tsaixeas_AnalyzingExpirationHour, //!<Analyzing the expiration hour String
    tsaixeas_AnalyzingAmountToPay, //!<Analyzing the amount to pay
    tsaixeas_AnalyzingCurrencyAmountToPay, //!<Analyzing the currency amount to pay
    tsaixeas_AnalyzingChargedAmount, //!<Analyzing the amount to pay
    tsaixeas_AnalyzingCurrencyChargedAmount, //!<Analyzing the currency amount to pay
    tsaixeas_AnalyzingState, //!<Analyzing the state
    tsaixeas_AnalyzingBeneficiary, //!<Analyzing the state
    tsaixeas_AnalyzingOperation, //!<Analyzing the operation
    tsaixeas_AnalyzingSubject, //!<Analyzing the subject
    tsaixeas_AnalyzingHolder, //!<Analyzing the holder
    tsaixeas_AnalyzingCurrencyITF, //!<Analyzing the currency of the itf
    tsaixeas_AnalyzingCurrencyAccount, //!<Analyzing the currency of the account
    tsaixeas_AnalyzingItf, //!<Analyzing the itf
    tsaixeas_AnalyzingAccountType, //!<Analyzing the account type
    tsaixeas_AnalyzingMaxResendNumber //!<Analyzing the max number of times to resend
    
    
} TransferSuccessAdditionalInformationXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferSuccessAdditionalInformation private category
 */
@interface TransferShowDetailAddtionalInformation(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearTransferDetailAdditionalInformationData;

@end

#pragma mark -

@implementation TransferShowDetailAddtionalInformation

#pragma mark -
#pragma mark Properties
@synthesize dateString = dateString_;
@synthesize operationNumber = operationNumber_;
@synthesize beneficiary = beneficiary_;
@synthesize amountToPay = amountToPay_;
@synthesize currencyAmountToPay = currencyAmountToPay_;
@synthesize transferState = transferState_;
@synthesize accountType = accountType_;
@synthesize chargedAmount = chargedAmount_;
@synthesize currencyAccount = currencyAccount_;
@synthesize currencyChargedAmount = currencyChargedAmount_;
@synthesize currencyItf = currencyITF_;
@synthesize expirationDateString = expirationDateString_;
@synthesize expirationHourString = expirationHourString_;
@synthesize holder = holder_;
@synthesize hourString = hourString_;
@synthesize itf = itf_;
@synthesize operation = operation_;
@synthesize subject = subject_;
@synthesize maxResendString = maxResendString_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearTransferDetailAdditionalInformationData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearTransferDetailAdditionalInformationData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"numerooperacion"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingOperationNumber;
            
        } else if ([lname isEqualToString:@"operacion"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingOperation;
            
        } else if ([lname isEqualToString:@"tipocta"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingAccountType;
            
        } else if ([lname isEqualToString:@"moneda"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingCurrencyAccount;
            
        } else if ([lname isEqualToString:@"asunto"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingSubject;
            
        } else if ([lname isEqualToString:@"titular"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingHolder;
            
        } else if ([lname isEqualToString:@"monedaimporte"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingCurrencyAmountToPay;
            
        } else if ([lname isEqualToString:@"importe"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingAmountToPay;
            
        } else if ([lname isEqualToString:@"celularbeneficiario"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingBeneficiary;
            
        } else if ([lname isEqualToString:@"monedaitf"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingCurrencyITF;
            
        } else if ([lname isEqualToString:@"importeitf"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingItf;
            
        } else if ([lname isEqualToString:@"monedaimportecargados"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingCurrencyChargedAmount;
            
        } else if ([lname isEqualToString:@"importecargado"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingChargedAmount;
            
        } else if ([lname isEqualToString:@"estado"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingState;
            
        } else if ([lname isEqualToString:@"fechatransaccion"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingDate;
            
        } else if ([lname isEqualToString:@"horatransaccion"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingHour;
            
        } else if ([lname isEqualToString:@"fechavencimiento"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingExpirationDate;
            
        } else if ([lname isEqualToString:@"horavencimiento"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingExpirationHour;
            
        } else if ([lname isEqualToString:@"maxreenvio"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingMaxResendNumber;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingOperationNumber) {
            
            [operationNumber_ release];
            operationNumber_ = nil;
            operationNumber_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingOperation) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingAccountType) {
            
            [accountType_ release];
            accountType_ = nil;
            accountType_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingAmountToPay) {
            
            [amountToPay_ release];
            amountToPay_ = nil;
            amountToPay_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingCurrencyAccount) {
            
            [currencyAccount_ release];
            currencyAccount_ = nil;
            currencyAccount_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingSubject) {
            
            [subject_ release];
            subject_ = nil;
            subject_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingHolder) {
            
            [holder_ release];
            holder_ = nil;
            holder_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingCurrencyAmountToPay) {
            
            [currencyAmountToPay_ release];
            currencyAmountToPay_ = nil;
            currencyAmountToPay_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingAmountToPay) {
            
            [amountToPay_ release];
            amountToPay_ = nil;
            amountToPay_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingBeneficiary) {
            
            [beneficiary_ release];
            beneficiary_ = nil;
            beneficiary_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingCurrencyITF) {
            
            [currencyITF_ release];
            currencyITF_ = nil;
            currencyITF_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingItf) {
            
            [itf_ release];
            itf_ = nil;
            itf_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingCurrencyChargedAmount) {
            
            [currencyChargedAmount_ release];
            currencyChargedAmount_ = nil;
            currencyChargedAmount_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingChargedAmount) {
            
            [chargedAmount_ release];
            chargedAmount_ = nil;
            chargedAmount_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingState) {
            
            [transferState_ release];
            transferState_ = nil;
            transferState_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingDate) {
            
            [dateString_ release];
            dateString_ = nil;
            dateString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingHour) {
            
            [hourString_ release];
            hourString_ = nil;
            hourString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingExpirationDate) {
            
            [expirationDateString_ release];
            expirationDateString_ = nil;
            expirationDateString_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingExpirationHour) {
            
            [expirationHourString_ release];
            expirationHourString_ = nil;
            expirationHourString_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingMaxResendNumber) {
            
            [maxResendString_ release];
            maxResendString_ = nil;
            maxResendString_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end


#pragma mark -

@implementation TransferShowDetailAddtionalInformation(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearTransferDetailAdditionalInformationData {
    
    [operationNumber_ release];
    operationNumber_ = nil;
    
    [dateString_ release];
    dateString_ = nil;
	
	[amountToPay_ release];
	amountToPay_ = nil;
	
	[currencyAmountToPay_ release];
	currencyAmountToPay_ = nil;
	
	[beneficiary_ release];
	beneficiary_ = nil;
    
    [transferState_ release];
    transferState_ = nil;
	
}

@end
