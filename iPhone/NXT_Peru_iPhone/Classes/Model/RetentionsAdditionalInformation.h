/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


/**
 * Retention response additional information information
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface RetentionsAdditionalInformation : StatusEnabledResponse {

@private
    
    /**
     * Currency
     */
    NSString *currency_;
    
    /**
     * Total retention string
     */
    NSString *totalRetentionString_;
    
    /**
     * Total retention
     */
    NSDecimalNumber *totalRetention_;
    
}


/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the total retention string
 */
@property (nonatomic, readonly, copy) NSString *totalRetentionString;

/**
 * Provides read-only access to the total retention
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *totalRetention;

@end
