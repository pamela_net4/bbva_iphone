//
//  SalaryAdvanceSummary.m
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "SalaryAdvanceSummary.h"
#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     saseas_AnalyzingNumberOperation = serxeas_ElementsCount, //!<Analyzing the SalaryAdvanceSummary numberOperation
     saseas_AnalyzingAccount , //!<Analyzing the SalaryAdvanceSummary account
     saseas_AnalyzingCommission , //!<Analyzing the SalaryAdvanceSummary commission
     saseas_AnalyzingAmountAdvance , //!<Analyzing the SalaryAdvanceSummary amountAdvance
     saseas_AnalyzingDayOfPay , //!<Analyzing the SalaryAdvanceSummary dayOfPay
     saseas_AnalyzingDate , //!<Analyzing the SalaryAdvanceSummary date
     saseas_AnalyzingHour , //!<Analyzing the SalaryAdvanceSummary hour
     saseas_AnalyzingMessageNotification , //!<Analyzing the SalaryAdvanceSummary messageNotification
     saseas_AnalyzingCustomer , //!<Analyzing the SalaryAdvanceSummary customer

} SalaryAdvanceSummaryXMLElementAnalyzerState;

@implementation SalaryAdvanceSummary

#pragma mark -
#pragma mark Properties

@synthesize numberOperation = numberOperation_;
@synthesize account = account_;
@synthesize commission = commission_;
@synthesize amountAdvance = amountAdvance_;
@synthesize dayOfPay = dayOfPay_;
@synthesize date = date_;
@synthesize hour = hour_;
@synthesize messageNotification = messageNotification_;
@synthesize customer = customer_;
;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [numberOperation_ release];
    numberOperation_ = nil;

    [account_ release];
    account_ = nil;

    [commission_ release];
    commission_ = nil;

    [amountAdvance_ release];
    amountAdvance_ = nil;

    [dayOfPay_ release];
    dayOfPay_ = nil;

    [date_ release];
    date_ = nil;

    [hour_ release];
    hour_ = nil;

    [messageNotification_ release];
    messageNotification_ = nil;

    [customer_ release];
    customer_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if(0==1){
                   //delete fix
        }        else if ([lname isEqualToString: @"numerooperacion"]) {

            xmlAnalysisCurrentValue_ = saseas_AnalyzingNumberOperation;


        }
        else if ([lname isEqualToString: @"cuentaabono"]) {

            xmlAnalysisCurrentValue_ = saseas_AnalyzingAccount;


        }
        else if ([lname isEqualToString: @"comisionadelanto"]) {

            xmlAnalysisCurrentValue_ = saseas_AnalyzingCommission;


        }
        else if ([lname isEqualToString: @"montoadelanto"]) {

            xmlAnalysisCurrentValue_ = saseas_AnalyzingAmountAdvance;


        }
        else if ([lname isEqualToString: @"mensaje_diapago"]) {

            xmlAnalysisCurrentValue_ = saseas_AnalyzingDayOfPay;


        }
        else if ([lname isEqualToString: @"fecha"]) {

            xmlAnalysisCurrentValue_ = saseas_AnalyzingDate;


        }
        else if ([lname isEqualToString: @"hora"]) {

            xmlAnalysisCurrentValue_ = saseas_AnalyzingHour;


        }
        else if ([lname isEqualToString: @"disclaimer"]) {

            xmlAnalysisCurrentValue_ = saseas_AnalyzingMessageNotification;


        }
        else if ([lname isEqualToString: @"nombrecliente"]) {

            xmlAnalysisCurrentValue_ = saseas_AnalyzingCustomer;


        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(0==1){
           //delete fix
        }        else if (xmlAnalysisCurrentValue_ == saseas_AnalyzingNumberOperation) {

            [numberOperation_ release];
            numberOperation_ = nil;
            numberOperation_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == saseas_AnalyzingAccount) {

            [account_ release];
            account_ = nil;
            account_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == saseas_AnalyzingCommission) {

            [commission_ release];
            commission_ = nil;
            commission_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == saseas_AnalyzingAmountAdvance) {

            [amountAdvance_ release];
            amountAdvance_ = nil;
            amountAdvance_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == saseas_AnalyzingDayOfPay) {

            [dayOfPay_ release];
            dayOfPay_ = nil;
            dayOfPay_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == saseas_AnalyzingDate) {

            [date_ release];
            date_ = nil;
            date_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == saseas_AnalyzingHour) {

            [hour_ release];
            hour_ = nil;
            hour_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == saseas_AnalyzingMessageNotification) {

            [messageNotification_ release];
            messageNotification_ = nil;
            messageNotification_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == saseas_AnalyzingCustomer) {

            [customer_ release];
            customer_ = nil;
            customer_ = [elementString copyWithZone:self.zone];

        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

