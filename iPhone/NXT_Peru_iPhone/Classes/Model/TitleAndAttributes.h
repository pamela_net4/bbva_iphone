/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>

/**
 * Class to contain a title and a list of attributes associated to that title
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TitleAndAttributes : NSObject {
    
@private
    
    /**
     * Title string
     */
    NSString *titleString_;
    
    /**
     * Attributes array. All objects are NSString instances
     */
    NSMutableArray *attributesArray_;
    
    /**
     * Dictionary to save orientation of attributes.
     */
    NSMutableDictionary *orientationAttributesDictionary_;
    
}


/**
 * Provides read-write access to the title string
 */
@property (nonatomic, readwrite, copy) NSString *titleString;

/**
 * Provides read-only access to the ttributes array
 */
@property (nonatomic, readonly, retain) NSArray *attributesArray;

/**
 * Adds a string to the attributes array
 *
 * @param attribute The attribute to add to the attributes array
 */
- (void)addAttribute:(NSString *)attribute;

/**
 * Adds a string to the attributes array and orientation.
 *
 * @param attribute: The attribute to add to the attributes array.
 * @param align: A UITextAlignment orientation.
 */
- (void)addAttribute:(NSString *)attribute alignTo:(UITextAlignment)align;

/**
 * Get title and attribute align for attribute.
 *
 * @param attribute: The attribute to know orientation.
 */
- (UITextAlignment)getOrientationOfAttribute:(NSString *)attribute;

/**
 * Creates an autoreleased TitleAndAttributes instance
 *
 * @return The autoreleased TitleAndAttributes instance
 */
+ (TitleAndAttributes *)titleAndAttributes;

@end
