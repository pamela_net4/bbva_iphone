/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StockMarketAccount.h"

#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
    smaxeas_AnalyzingValueAccount = serxeas_ElementsCount, //!<Analyzing the stock market account value account
	smaxeas_AnalyzingCurrency, //!<Analyzing the stock market account currency
    smaxeas_AnalyzingValueBalance, //!<Analyzing the stock market account value balance
    smaxeas_AnalyzingCashBalance, //!<Analyzing the stock market account cash balance
    smaxeas_AnalyzingExpiredAccount, //!<Analyzing the stock market account expired account
    smaxeas_AnalyzingSubject, //!<Analyzing the stock market account subject
    smaxeas_AnalyzingBalanceInCountryCurrency //!<Analyzing the balance in country currency
    
} StockMarketAccountXMLElementAnalyzerState;


#pragma mark -

/**
 * StockMarketAccount private category
 */
@interface StockMarketAccount(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearStockMarketAccountData;

@end


#pragma mark -

@implementation StockMarketAccount

#pragma mark -
#pragma mark Properties

@synthesize valueAccount = valueAccount_;
@synthesize currency = currency_;
@synthesize valueBalanceString = valueBalanceString_;
@dynamic valueBalance;
@synthesize cashBalanceString = cashBalanceString_;
@dynamic cashBalance;
@synthesize expiredAccount = expiredAccount_;
@synthesize subject = subject_;
@synthesize balanceInCountryCurrencyString = balanceInCountryCurrencyString_;
@dynamic balanceInCountryCurrency;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {

    [self clearStockMarketAccountData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Title texts management

/*
 * Returns the stock market account list display name. It can be used whenever a stock marketaccount name must be displayed inside a list
 */
- (NSString *)stockMarketAccountListName {
    
    return [Tools obfuscateAccountNumber:valueAccount_];
    
}

/*
 * Returns the obfuscated account number
 */
- (NSString *)obfuscatedAccountNumber {
    
    return [Tools obfuscateAccountNumber:valueAccount_];
    
}


#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearStockMarketAccountData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"cuentavalor"]) {
            
            xmlAnalysisCurrentValue_ = smaxeas_AnalyzingValueAccount;
            
        } else if ([lname isEqualToString:@"moneda"]) {
            
            xmlAnalysisCurrentValue_ = smaxeas_AnalyzingCurrency;
            
        } else if ([lname isEqualToString:@"saldovaloridazo"]) {
            
            xmlAnalysisCurrentValue_ = smaxeas_AnalyzingValueBalance;
            
        } else if ([lname isEqualToString:@"saldoefectivo"]) {
            
            xmlAnalysisCurrentValue_ = smaxeas_AnalyzingCashBalance;
            
        } else if ([lname isEqualToString:@"cuentavencida"]) {
            
            xmlAnalysisCurrentValue_ = smaxeas_AnalyzingExpiredAccount;
            
        } else if ([lname isEqualToString:@"asunto"]) {
            
            xmlAnalysisCurrentValue_ = smaxeas_AnalyzingSubject;
            
        } else if ([lname isEqualToString:@"importeconvertido"]) {
            
            xmlAnalysisCurrentValue_ = smaxeas_AnalyzingBalanceInCountryCurrency;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSZone *zone = self.zone;

        if (xmlAnalysisCurrentValue_ == smaxeas_AnalyzingValueAccount) {
            
            [valueAccount_ release];
            valueAccount_ = nil;
            valueAccount_ = [elementString copyWithZone:zone];
            
        } else if (xmlAnalysisCurrentValue_ == smaxeas_AnalyzingCurrency) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:zone];
            
        } else if (xmlAnalysisCurrentValue_ == smaxeas_AnalyzingValueBalance) {
            
            [valueBalanceString_ release];
            valueBalanceString_ = nil;
            valueBalanceString_ = [elementString copyWithZone:zone];
            
        } else if (xmlAnalysisCurrentValue_ == smaxeas_AnalyzingCashBalance) {
            
            [cashBalanceString_ release];
            cashBalanceString_ = nil;
            cashBalanceString_ = [elementString copyWithZone:zone];
            
        } else if (xmlAnalysisCurrentValue_ == smaxeas_AnalyzingExpiredAccount) {
            
            [expiredAccount_ release];
            expiredAccount_ = nil;
            expiredAccount_ = [elementString copyWithZone:zone];
            
        } else if (xmlAnalysisCurrentValue_ == smaxeas_AnalyzingSubject) {
            
            [subject_ release];
            subject_ = nil;
            subject_ = [elementString copyWithZone:zone];
            
        } else if (xmlAnalysisCurrentValue_ == smaxeas_AnalyzingBalanceInCountryCurrency) {
            
            [balanceInCountryCurrencyString_ release];
            balanceInCountryCurrencyString_ = nil;
            balanceInCountryCurrencyString_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the value balance, calculating it from the string representation if necessary
 *
 * @return The value balance
 */
- (NSDecimalNumber *)valueBalance {
    
    NSDecimalNumber *result = valueBalance_;
    
    if (result == nil) {
        
        if ([valueBalanceString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:valueBalanceString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                valueBalance_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the cash balance, calculating it from the string representation if necessary
 *
 * @return The cash balance
 */
- (NSDecimalNumber *)cashBalance {
    
    NSDecimalNumber *result = cashBalance_;
    
    if (result == nil) {
        
        if ([cashBalanceString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:cashBalanceString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                cashBalance_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the balance converted into the country currency, calculating ir from string representation if needed
 *
 * @return The balance converted into country curency
 */
- (NSDecimalNumber *)balanceInCountryCurrency {
    
    NSDecimalNumber *result = balanceInCountryCurrency_;
    
    if (result == nil) {
        
        if ([balanceInCountryCurrencyString_ length] == 0) {
            
            if ([valueBalanceString_ length] > 0) {
            
                result = [Tools decimalFromServerString:valueBalanceString_];
                
            } else {
                
                result = [NSDecimalNumber notANumber];

            }
            
        } else {
            
            result = [Tools decimalFromServerString:balanceInCountryCurrencyString_];
            
        }
        
        if (![result compare:[NSDecimalNumber notANumber]]) {
            
            balanceInCountryCurrency_ = [result retain];
            
        }
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation StockMarketAccount(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearStockMarketAccountData {
    
    [valueAccount_ release];
    valueAccount_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [valueBalanceString_ release];
    valueBalanceString_ = nil;
    
    [valueBalance_ release];
    valueBalance_ = nil;
    
    [cashBalanceString_ release];
    cashBalanceString_ = nil;
    
    [cashBalance_ release];
    cashBalance_ = nil;
    
    [expiredAccount_ release];
    expiredAccount_ = nil;
    
    [subject_ release];
    subject_ = nil;
    
    [balanceInCountryCurrencyString_ release];
    balanceInCountryCurrencyString_ = nil;
    
    [balanceInCountryCurrency_ release];
    balanceInCountryCurrency_ = nil;
    
}

@end
