/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BankAccount.h"
#import "Tools.h"
#import "Constants.h"
#import "AccountTransaction.h"
#import "AccountTransactionList.h"
#import "AccountTransactionsAdditionalInformation.h"


/**
 * The term account code identifier.
 */
#define TERM_ACCOUNT_CODE   @"03"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
    axeas_AnalyzingNumber = serxeas_ElementsCount, //!<Analyzing the account number
	axeas_AnalyzingAccountType, //!<Analyzing the account type
	axeas_AnalyzingCurrentBalance, //!<Analyzing the current balance
	axeas_AnalyzingAvaliableBalance, //!<Analyzing the available valance
	axeas_AnalyzingCurrency, //!<Analyzing the currency
	axeas_AnalyzingBank, //!<Analyzing the account bank
	axeas_AnalyzingBranch, //!<Analyzing the account branch
	axeas_AnalyzingControlDigit, //!<Analyzing the control digit
	axeas_AnalyzingBranchAccount, //!<Analyzing the branch account
	axeas_AnalyzingType, //!<Analyzing the type
	axeas_AnalyzingState, //!<Analyzing the state
	axeas_AnalyzingPermission, //!<Analyzing the permission
	axeas_AnalyzingSubject, //!<Analyzing the subject
    axeas_AnalyzingParamCuenta,//!<Analyzing the paramCuenta
    axeas_AnalyzingBalanceInCountryCurrency //!<Analyzing the balance in country currency
    
} AccountXMLElementAnalyzerState;


#pragma mark -

/**
 * BankAccount private category
 */
@interface BankAccount(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearBankAccountData;

@end


#pragma mark -

@implementation BankAccount

#pragma mark -
#pragma mark Properties

@synthesize number = number_;
@synthesize accountType = accountType_;
@synthesize currentBalanceString = currentBalanceString_;
@dynamic currentBalance;
@synthesize availableBalanceString = availableBalanceString_;
@dynamic availableBalance;
@synthesize currency = currency_;
@synthesize bank = bank_;
@synthesize branch = branch_;
@synthesize controlDigit = controlDigit_;
@synthesize branchAccount = branchAccount_;
@synthesize type = type_;
@synthesize state = state_;
@synthesize permission = permission_;
@synthesize subject = subject_;
@synthesize paramCuenta = paramCuenta_;
@synthesize balanceInCountryCurrencyString = balanceInCountryCurrencyString_;
@dynamic balanceInCountryCurrency;
@synthesize isTermAccount = isTermAccount_;
@synthesize accountTransactionsList = accountTransactionsList_;
@synthesize transactionsAdditionalInformation = transactionsAdditionalInformation_;
@dynamic accountIdAndDescription;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearBankAccountData];
    
    [accountTransactionsList_ release];
    accountTransactionsList_ = nil;
    
    [transactionsAdditionalInformation_ release];
    transactionsAdditionalInformation_ = nil;
	
	[super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initialized. Initializes a BankAccount instance
 *
 * @return The initialized BankAccount instance
 */
- (id)init {
    
    if (self = [super init]) {
	
		self.notificationToPost = kNotificationAccountUpdated;

    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates this instance with the given account
 */
- (void)updateFrom:(BankAccount *)anAccount {
    
    NSZone *zone = self.zone;
    
    [number_ release];
    number_ = nil;
    number_ = [anAccount.number copyWithZone:zone];
    
    [accountType_ release];
    accountType_ = nil;
    accountType_ = [anAccount.accountType copyWithZone:zone];
    
    [currentBalanceString_ release];
    currentBalanceString_ = nil;
    currentBalanceString_ = [anAccount.currentBalanceString copyWithZone:zone];
    
    [currentBalance_ release];
    currentBalance_ = nil;
    
    [availableBalanceString_ release];
    availableBalanceString_ = nil;
    availableBalanceString_ = [anAccount.availableBalanceString copyWithZone:zone];
    
    [availableBalance_ release];
    availableBalance_ = nil;
    
    [currency_ release];
    currency_ = nil;
    currency_ = [anAccount.currency copyWithZone:zone];
    
    [bank_ release];
    bank_ = nil;
    bank_ = [anAccount.bank copyWithZone:zone];
    
    [branch_ release];
    branch_ = nil;
    branch_ = [anAccount.branch copyWithZone:zone];
    
    [controlDigit_ release];
    controlDigit_ = nil;
    controlDigit_ = [anAccount.controlDigit copyWithZone:zone];
    
    [branchAccount_ release];
    branchAccount_ = nil;
    branchAccount_ = [anAccount.branchAccount copyWithZone:zone];
    
    [type_ release];
    type_ = nil;
    type_ = [anAccount.type copyWithZone:zone];
    
    [state_ release];
    state_ = nil;
    state_ = [anAccount.state copyWithZone:zone];
    
    [permission_ release];
    permission_ = nil;
    permission_ = [anAccount.permission copyWithZone:zone];
    
    [subject_ release];
    subject_ = nil;
    subject_ = [anAccount.subject copyWithZone:zone];
    
    [paramCuenta_ release];
    paramCuenta_ = nil;
    paramCuenta_ = [anAccount.paramCuenta copyWithZone:zone];
    
    [balanceInCountryCurrencyString_ release];
    balanceInCountryCurrencyString_ = nil;
    balanceInCountryCurrencyString_ = [anAccount.balanceInCountryCurrencyString copyWithZone:zone];
    
    [balanceInCountryCurrency_ release];
    balanceInCountryCurrency_ = nil;
    
    [self updateAccountTransactionsList:anAccount.accountTransactionsList];
    [self updateAccountTransactionsAdditionalInformation:anAccount.transactionsAdditionalInformation];
    [self updateFromStatusEnabledResponse:anAccount];
	
	self.informationUpdated = anAccount.informationUpdated;
    
}

/*
 * Updates the transactions list
 */
- (void)updateAccountTransactionsList:(AccountTransactionList *)anAccountTransactionsList {
    
    if (accountTransactionsList_ == nil) {
        
        accountTransactionsList_ = [[AccountTransactionList alloc] init];
        
    }
    
    [accountTransactionsList_ updateFrom:anAccountTransactionsList];
    
}

/*
 * Updates the transactions additional information
 */
- (void)updateAccountTransactionsAdditionalInformation:(AccountTransactionsAdditionalInformation *)aTransactionsAdditionalInformation {
    
    if (transactionsAdditionalInformation_ == nil) {
        
        transactionsAdditionalInformation_ = [[AccountTransactionsAdditionalInformation alloc] init];
        
    }
    
    [transactionsAdditionalInformation_ updateFrom:aTransactionsAdditionalInformation];
    
}

#pragma mark -
#pragma mark Title texts management

/*
 * Returns the bank account list display name. It can be used whenever an account name must be displayed inside a list
 */
- (NSString *)bankAccountListName {
    
    NSString *result = [NSString stringWithFormat:@"%@ %@ - Disp. %@ %@", accountType_, [Tools obfuscateAccountNumber:number_], [Tools getCurrencySimbol:currency_], availableBalanceString_];
    
    return result;
    
}

/*
 * Returns the obfuscated account number
 */
- (NSString *)obfuscatedAccountNumber {
    
    return [Tools obfuscateAccountNumber:number_];
    
}

#pragma mark -
#pragma mark IAC generation

/*
 * Generates IAC from the account number.
 */
- (NSString *)IAC {
    
    if (bank_ == nil || !([bank_ length] > 2)) {
        return nil;
    }
    
    NSString *s1 = [bank_ substringFromIndex:1];
    
    if (branch_ == nil || !([branch_ length] > 2)) {
        return nil;
    }
    
    NSString *s2 = [branch_ substringFromIndex:1];
    
    if (type_ == nil || branchAccount_ == nil) {
        return nil;
    }
    
    NSString *s3 = [type_ stringByAppendingString:branchAccount_];
    
    if (controlDigit_ == nil) {
        return nil;
    }
    
    NSString *s4 = controlDigit_;
    
    return [NSString stringWithFormat:@"%@-%@-00%@-%@", s1, s2, s3, s4];
    
}


#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearBankAccountData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"numerocuenta"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingNumber;
            
        } else if ([lname isEqualToString: @"tipocta"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingAccountType;
            
        } else if ([lname isEqualToString: @"saldocontable"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingCurrentBalance;
            
        } else if ([lname isEqualToString: @"saldodisponible"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingAvaliableBalance;
            
        } else if ([lname isEqualToString: @"moneda"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingCurrency;
            
        } else if ([lname isEqualToString: @"banco"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingBank;
            
        } else if ([lname isEqualToString: @"oficina"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingBranch;
            
        } else if ([lname isEqualToString: @"dcontrol"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingControlDigit;
            
        } else if ([lname isEqualToString: @"cuenta"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingBranchAccount;
            
        } else if ([lname isEqualToString: @"tipo"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingType;
            
        } else if ([lname isEqualToString: @"estado"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingState;
            
        } else if ([lname isEqualToString: @"permiso"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingPermission;
            
        } else if ([lname isEqualToString: @"asunto"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingSubject;
            
        } else if ([lname isEqualToString:@"importeconvertido"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingBalanceInCountryCurrency;
            
        } else if ([lname isEqualToString:@"paramcuenta"]){
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingParamCuenta;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == axeas_AnalyzingNumber) {
            
            [number_ release];
            number_ = nil;
            number_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingAccountType) {
            
            [accountType_ release];
            accountType_ = nil;
            accountType_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingCurrentBalance) {
            
            [currentBalanceString_ release];
            currentBalanceString_ = nil;
            currentBalanceString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingAvaliableBalance) {
            
            [availableBalanceString_ release];
            availableBalanceString_ = nil;
            availableBalanceString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingCurrency) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingBank) {
            
            [bank_ release];
            bank_ = nil;
            bank_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingBranch) {
            
            [branch_ release];
            branch_ = nil;
            branch_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingControlDigit) {
            
            [controlDigit_ release];
            controlDigit_ = nil;
            controlDigit_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingBranchAccount) {
            
            [branchAccount_ release];
            branchAccount_ = nil;
            branchAccount_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingType) {
            
            [type_ release];
            type_ = nil;
            type_ = [elementString copyWithZone:self.zone];

            isTermAccount_ = [TERM_ACCOUNT_CODE isEqualToString:type_];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingState) {
            
            [state_ release];
            state_ = nil;
            state_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingPermission) {
            
            [permission_ release];
            permission_ = nil;
            permission_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingSubject) {
            
            [subject_ release];
            subject_ = nil;
            subject_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingBalanceInCountryCurrency) {
            
            [balanceInCountryCurrencyString_ release];
            balanceInCountryCurrencyString_ = nil;
            balanceInCountryCurrencyString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == axeas_AnalyzingParamCuenta){
            
            [paramCuenta_ release];
            paramCuenta_ = nil;
            paramCuenta_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;

}

#pragma mark -
#pragma mark Propertie selectors

/*
 * Returns the current balance, calculating ir from string representation if needed
 *
 * @return The current balance
 */
- (NSDecimalNumber *)currentBalance {
    
    NSDecimalNumber *result = currentBalance_;
    
    if (result == nil) {
        
        if ([currentBalanceString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:currentBalanceString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                currentBalance_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Return the available balance, calculating ir from string representation if needed
 *
 * @return The available balance
 */
- (NSDecimalNumber *)availableBalance {
    
    NSDecimalNumber *result = availableBalance_;
    
    if (result == nil) {
        
        if ([availableBalanceString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:availableBalanceString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                availableBalance_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the balance converted into the country currency, calculating ir from string representation if needed
 *
 * @return The balance converted into country curency
 */
- (NSDecimalNumber *)balanceInCountryCurrency {
    
    NSDecimalNumber *result = balanceInCountryCurrency_;
    
    if (result == nil) {
        
        if ([balanceInCountryCurrencyString_ length] == 0) {
            
            if ([availableBalanceString_ length] > 0) {
            
                result = [Tools decimalFromServerString:availableBalanceString_];

            } else {
                
                result = [NSDecimalNumber notANumber];

            }
            
        } else {
            
            result = [Tools decimalFromServerString:balanceInCountryCurrencyString_];
            
        }
        
        if (![result compare:[NSDecimalNumber notANumber]]) {
            
            balanceInCountryCurrency_ = [result retain];
            
        }
        
    }
    
    return result;

}

/*
 * Returns the account identification plus the description
 *
 * @return The account identification plus the description
 */
- (NSString *)accountIdAndDescription {
    
    NSString *result = @"";
    
    NSString *accountNumber = number_;
    
    if (accountNumber == nil) {
        accountNumber = subject_;
    }
    
    result = [NSString stringWithFormat:@"%@ %@ - Disp. %@ %@", accountType_, [Tools obfuscateAccountNumber:accountNumber], [Tools getCurrencySimbol:currency_], availableBalanceString_];
    
    return result;
    
}

@end


#pragma mark -

@implementation BankAccount(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearBankAccountData {
    
    [number_ release];
    number_ = nil;
    
    [accountType_ release];
    accountType_ = nil;
    
    [currentBalanceString_ release];
    currentBalanceString_ = nil;
    
    [currentBalance_ release];
    currentBalance_ = nil;
    
    [availableBalanceString_ release];
    availableBalanceString_ = nil;
    
    [availableBalance_ release];
    availableBalance_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [bank_ release];
    bank_ = nil;
    
    [branch_ release];
    branch_ = nil;
    
    [controlDigit_ release];
    controlDigit_ = nil;
    
    [branchAccount_ release];
    branchAccount_ = nil;
    
    [type_ release];
    type_ = nil;
    
    [state_ release];
    state_ = nil;
    
    [permission_ release];
    permission_ = nil;
    
    [subject_ release];
    subject_ = nil;
    
    [paramCuenta_ release];
    paramCuenta_ = nil;
    
    [balanceInCountryCurrencyString_ release];
    balanceInCountryCurrencyString_ = nil;
    
    [balanceInCountryCurrency_ release];
    balanceInCountryCurrency_ = nil;
    
    isTermAccount_ = NO;
    
}

@end
