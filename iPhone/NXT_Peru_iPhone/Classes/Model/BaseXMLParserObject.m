/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "BaseXMLParserObject.h"
#import "BaseXMLParserObject+protected.h"


#pragma mark -

@implementation BaseXMLParserObject

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
	
	parentXMLParserObject_ = nil;
	
	[openingTag_ release];
	openingTag_ = nil;
	
	[auxString_ release];
	auxString_ = nil;
	
	[super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initialiazer
 */
- (id)init {
    
	if (self = [super init]) {
        
		parentXMLParserObject_ = nil;
		openingTag_ = nil;
		auxString_ = nil;

	}
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. Resets the auxiliary string used to accumulate characters found while parsing
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	if (auxString_ == nil) {
	
		auxString_ = [[NSMutableString alloc] init];
		
	} else {
		
		[auxString_ setString:@""];
		
	}
    
    xmlAnalysisState_ = BXPOAnalyzingStateIdle;
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Auxiliary string is reseted
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {

	[auxString_ setString:@""];
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. If opening tag found, control is returned to the parent parseable object
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	NSString *lowercaseElementName = [elementName lowercaseString];
	
	if ((xmlAnalysisState_ == BXPOAnalyzingStateIdle) && ([lowercaseElementName isEqualToString:openingTag_])) {
		
		[parser setDelegate:parentXMLParserObject_];
		
	}
	
}

/**
 * Sent by a parser object to provide its delegate with a string representing all or part of the characters of the current element.
 * Characters found are appended to the auxiliary string
 *
 * @param parser A parser object
 * @param string A string representing the complete or partial textual content of the current element
 */
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {

	[auxString_ appendString:string];
	
}

@end


#pragma mark -

@implementation BaseXMLParserObject(protected)

#pragma mark -
#pragma mark Switching parsers

/*
 * Switch parsing from one object to another
 */
- (void)switchParser:(NSXMLParser *)aParser toParserObject:(BaseXMLParserObject *)anXMLParserObject withOpeningTag:(NSString *)anOpeningTag {
    
	if (anXMLParserObject != nil) {
		
		[aParser setDelegate:anXMLParserObject];
		
		anXMLParserObject->parentXMLParserObject_ = self;
		
		[anXMLParserObject->openingTag_ release];
		anXMLParserObject->openingTag_ = nil;
		anXMLParserObject->openingTag_ = [[anOpeningTag lowercaseString] copy];
		
		[anXMLParserObject parserDidStartDocument:aParser];
		
	}
    
}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the element string
 *
 * @return The element string
 * @protected
 */
- (NSString *)elementString {
    
    return auxString_;
    
}

@end
