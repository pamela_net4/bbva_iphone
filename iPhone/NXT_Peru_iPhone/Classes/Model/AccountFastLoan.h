//
//  AccountFastLoan.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"


@interface AccountFastLoan : StatusEnabledResponse{
    @private

   /**
     * AccountFastLoan id
     */
    NSString *id_;


   /**
     * AccountFastLoan type
     */
    NSString *type_;


   /**
     * AccountFastLoan number
     */
    NSString *number_;


   /**
     * AccountFastLoan currency
     */
    NSString *currency_;


   /**
     * AccountFastLoan account
     */
    NSString *account_;




}

/**
 * Provides read-only access to the AccountFastLoan id
 */
@property (nonatomic, readonly, copy) NSString * id;


/**
 * Provides read-only access to the AccountFastLoan type
 */
@property (nonatomic, readonly, copy) NSString * type;


/**
 * Provides read-only access to the AccountFastLoan number
 */
@property (nonatomic, readonly, copy) NSString * number;


/**
 * Provides read-only access to the AccountFastLoan currency
 */
@property (nonatomic, readonly, copy) NSString * currency;


/**
 * Provides read-only access to the AccountFastLoan account
 */
@property (nonatomic, readonly, copy) NSString * account;




@end

