/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StructuredXMLParser.h"

/**
 * Base class for every server response message that can contain an error code and message, or a successful response.
 * This base class analyzes the error section in the message, letting the successful response analysis to its child classes
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface StructuredStatusEnabledResponse : StructuredXMLParser {

@private
    
	/**
	 * Error code obtained from the server message
	 */
	NSString *errorCode_;
	
	/**
	 * Error message obtained from the server message
	 */
	NSString *errorMessage_;
	
    /**
     * Flag that indicates that the server returns an error
     */
    BOOL isError_;
    
    /**
     * Flag that indicates that the server returns an error
     */
    BOOL isLoginError_;

}

/**	
 * Provides read-write access to the error code
 */
@property (nonatomic, readwrite, copy) NSString *errorCode;

/**	
 * Provides read-write access to the error message
 */
@property (nonatomic, readwrite, copy) NSString *errorMessage;

/**	
 * Provides readwrite access to the error flag
 */
@property (nonatomic, readwrite, assign) BOOL isError;

/**	
 * Provides readwrite access to the login error flag
 */
@property (nonatomic, readwrite, assign) BOOL isLoginError;


@end
