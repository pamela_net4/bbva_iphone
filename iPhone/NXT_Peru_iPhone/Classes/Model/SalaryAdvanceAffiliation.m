//
//  SalaryAdvanceAffiliation.m
//  NXT_Peru_iPhone
//
//  Created by Flavio Franco Tunqui on 3/17/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "SalaryAdvanceAffiliation.h"
#import "Tools.h"
#import "EmailInfoList.h"

/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
    sareas_AnalyzingCustomer = serxeas_ElementsCount, //!<Analyzing the SalaryAdvanceReceive customer
    sareas_AnalyzingMessageAmount , //!<Analyzing the SalaryAdvanceReceive messageAmount
    sareas_AnalyzingMessageCommission , //!<Analyzing the SalaryAdvanceReceive messageCommission
    sareas_AnalyzingMessageDayPay , //!<Analyzing the SalaryAdvanceReceive messageDayPay
    sareas_AnalyzingEmailInfoList , //!<Analyzing the SalaryAdvanceReceive emailInfoList
    sareas_AnalyzingSeal , //!<Analyzing the SalaryAdvanceReceive seal
    sareas_AnalyzingDisclaimerContract , //!<Analyzing the SalaryAdvanceReceive disclaimerContract
    sareas_AnalyzingDisclaimerContractWeb , //!<Analyzing the SalaryAdvanceReceive disclaimerContractWeb
    sareas_AnalyzingCoordinate , //!<Analyzing the SalaryAdvanceReceive coordinate
    
} SalaryAdvanceAffiliationXMLElementAnalyzerState;

@implementation SalaryAdvanceAffiliation

#pragma mark -
#pragma mark Properties

@synthesize customer = customer_;
@synthesize messageAmount = messageAmount_;
@synthesize messageCommission = messageCommission_;
@synthesize messageDayPay = messageDayPay_;
@synthesize emailInfoList = emailInfoList_;
@synthesize seal = seal_;
@synthesize disclaimerContract = disclaimerContract_;
@synthesize disclaimerContractWeb = disclaimerContractWeb_;
@synthesize coordinate = coordinate_;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [customer_ release];
    customer_ = nil;
    
    [messageAmount_ release];
    messageAmount_ = nil;
    
    [messageCommission_ release];
    messageCommission_ = nil;
    
    [messageDayPay_ release];
    messageDayPay_ = nil;
    
    [emailInfoList_ release];
    emailInfoList_ = nil;
    
    [seal_ release];
    seal_ = nil;
    
    [disclaimerContract_ release];
    disclaimerContract_ = nil;
    
    [disclaimerContractWeb_ release];
    disclaimerContractWeb_ = nil;
    
    [coordinate_ release];
    coordinate_ = nil;
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if(0==1){
            //delete fix
        }        else if ([lname isEqualToString: @"nombrecliente"]) {
            
            xmlAnalysisCurrentValue_ = sareas_AnalyzingCustomer;
        }
        else if ([lname isEqualToString: @"mensaje_monto"]) {
            xmlAnalysisCurrentValue_ = sareas_AnalyzingMessageAmount;
        }
        else if ([lname isEqualToString: @"mensaje_comision"]) {
            xmlAnalysisCurrentValue_ = sareas_AnalyzingMessageCommission;
        }
        else if ([lname isEqualToString: @"mensaje_diapago"]) {
            xmlAnalysisCurrentValue_ = sareas_AnalyzingMessageDayPay;
        }
        else if ([lname isEqualToString: @"listacorreos"]) {
            
            xmlAnalysisCurrentValue_ = sareas_AnalyzingEmailInfoList;
            [emailInfoList_ release];
            emailInfoList_ = nil;
            emailInfoList_ = [[EmailInfoList alloc] init];
            emailInfoList_.openingTag = lname;
            [emailInfoList_ setParentParseableObject:self];
            [parser setDelegate:emailInfoList_];
            [emailInfoList_ parserDidStartDocument:parser];
        }
        else if ([lname isEqualToString: @"sello"]) {
            xmlAnalysisCurrentValue_ = sareas_AnalyzingSeal;
        }
        else if ([lname isEqualToString: @"disclaimercontrato"]) {
            xmlAnalysisCurrentValue_ = sareas_AnalyzingDisclaimerContract;
        }
        else if ([lname isEqualToString: @"disclaimercontratoweb"]) {
            xmlAnalysisCurrentValue_ = sareas_AnalyzingDisclaimerContractWeb;
        }
        else if ([lname isEqualToString: @"coordenada"]) {
            xmlAnalysisCurrentValue_ = sareas_AnalyzingCoordinate;
        }
        else {
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

        if(0==1){
            //delete fix
        }        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingCustomer) {
            
            [customer_ release];
            customer_ = nil;
            customer_ = [elementString copyWithZone:self.zone];
            
        }
        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingMessageAmount) {
            
            [messageAmount_ release];
            messageAmount_ = nil;
            messageAmount_ = [elementString copyWithZone:self.zone];
            
        }
        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingMessageCommission) {
            
            [messageCommission_ release];
            messageCommission_ = nil;
            messageCommission_ = [elementString copyWithZone:self.zone];
            
        }
        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingMessageDayPay) {
            
            [messageDayPay_ release];
            messageDayPay_ = nil;
            messageDayPay_ = [elementString copyWithZone:self.zone];
            
        }
        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingSeal) {
            
            [seal_ release];
            seal_ = nil;
            seal_ = [elementString copyWithZone:self.zone];
            
        }
        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingDisclaimerContract) {
            
            [disclaimerContract_ release];
            disclaimerContract_ = nil;
            disclaimerContract_ = [elementString copyWithZone:self.zone];
            
        }
        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingDisclaimerContractWeb) {
            
            [disclaimerContractWeb_ release];
            disclaimerContractWeb_ = nil;
            disclaimerContractWeb_ = [elementString copyWithZone:self.zone];
            
        }
        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingCoordinate) {
            
            [coordinate_ release];
            coordinate_ = nil;
            coordinate_ = [elementString copyWithZone:self.zone];
            
        }
        
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
}

@end