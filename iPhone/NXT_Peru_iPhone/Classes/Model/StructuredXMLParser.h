/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>


/**
 * Enumerates the type of element currently being parsed
 */
typedef enum {
    
    BXPOElementIdle = 0, //!<The parser is not parsing an element, or the element must be parsed by the same instance
    BXPOElementUnknown, //!<The element is an unknown type which must be scaped
    BXPOElementString, //!<The element is a string element
    BXPOElementCustom //!The element is a custom element parsed by a class derived from StructuredXMLParser
    
} BXPOElementType;


/**
 * Base object to parse XML documents in a structured way. That is, the XML structure is stored in objects that mimic that structure providing
 * attributes for the different types and instance types
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface StructuredXMLParser : NSObject <NSXMLParserDelegate, NSCopying> {
    
@private
	
    /**
     * Base parser initialized. YES when the parser has been initialized, NO otherwise. It allows a different behaviour when invoking the start element when parser is not initialized
     */
    BOOL initialized_;
    
    /**
     * Current element type
     */
    BXPOElementType elementType_;
    
    /**
     * Unknown or custom element depth counter. It allows the parser to end the element parsing once the depth is again zero
     */
    NSInteger depthCounter_;
    
	/**
	 * Auxiliary string to contruct element data
	 */
	NSMutableString *auxString_;
    
    /**
     * Custom parser for the current element when the element type is BXPOElementCustom
     */
    StructuredXMLParser *customParser_;
    
    /**
     * Dictionary containing the parsed information. Each key is a namespaceURI containg another dictionary with element name keys associated to a 
     * NSMutableArray. Each array contains either NSString instances or StructuredXMLParser subclases depending on the element type. When a StructuredXMLParser 
     * subclass is stored inside a NSMutableArray, every object in this array must be the same class,
     * or an exception can appear in some situations
     */
    NSMutableDictionary *parsedInformation_;
    
    /**
     * Attributes dictionary
     */
    NSMutableDictionary *attributesDictionary_;
    
    /**
     * Own namespaces prefixes dictionary. Keys in the dictionary are the prefixes, while values are their associated namespaces
     */
    NSMutableDictionary *ownNamespacesPrefixesDictionary_;
    
    /**
     * Parent namespaces prefixes dictionary. Keys in the dictionary are the prefixes, while values are their associated namespaces
     */
    NSDictionary *parentNamespacesPrefixesDictionary_;
    
    /**
     * Parsing namespaces prefixes dictionary which will become the next child own namespaces prefixes dictionary.
     * Keys in the dictionary are the prefixes, while values are their associated namespaces
     */
    NSMutableDictionary *parsingNamespacesPrefixesDictionary_;

}


/**
 * Provides read-only access to the own namespaces prefixes dictionary
 */
@property (nonatomic, readonly, retain) NSDictionary *ownNamespacesPrefixesDictionary;

/**
 * Provides read-only access to the whole namespaces prefixes dictionary (including the parent and own namespaces prefixes)
 */
@property (nonatomic, readonly, retain) NSDictionary *wholeNamespacesPrefixesDictionary;


/**
 * Designated intialized. It initializes a StructuredXMLParser instance with its parent namespace prefixes dictionary
 *
 * @param aParentNamespacesPrefixesDictionary The parent namespaces prefixes dictionary to store
 * @param ownNamespacesPrefixesDictionary The initial own namespaces prefixes dictionary to store
 * @return The initialized StructuredXMLParser instance
 */
- (id)initWithParentNamespacePrefixesDictionary:(NSDictionary *)aParentNamespacesPrefixesDictionary
                ownNamespacesPrefixesDictionary:(NSDictionary *)ownNamespacesPrefixesDictionary;


/**
 * Returns whether it is necesary to analyze the namespaces URI or not. When YES is returned, namespaces URIs are analyzed, when NO, namespaces URIs
 * are not analyzed. This information is only used while parsing. When parsing an XML document, the top parser value for this flag determines the
 * behaviour for the rest of parsers. Default implementation returns NO.
 *
 * @return The YES to analyze the namespaces URIs, NO to not analyze them
 */
- (BOOL)analyzeNamespacesURIs;

/**
 * Returns the attribute with the given name.
 *
 * @param anAttributeName The attribute name
 * @return The attribute with the given name, or nil if no attribute with the given name exists
 */
- (NSString *)attributeWithName:(NSString *)anAttributeName;

/**
 * Returns the array of parsed objects (either NSString or custom StructuredXMLParser) for a given element. If the element doesn't exist, the array returned is nil
 *
 * @param anElement The element to obtain the array of objects
 * @param namespaceURI The element name space
 * @return The array of parsed objects, or nil when the element doesn't exist
 */
- (NSArray *)arrayForElement:(NSString *)anElement
                namespaceURI:(NSString *)namespaceURI;

/**
 * Returns the parsed objet (either NSString or custom StructuredXMLParser) for the given element at the given array index.
 *
 * @param element The element to obtain the object from
 * @param namespaceURI The element name space
 * @param index The obnject index inside the element array
 * @return The parsed object (either NSString or custom StructuredXMLParser) or nil when the element doesn't exist or the index is out of bounds
 */
- (NSObject *)objectForElement:(NSString *)element
                  namespaceURI:(NSString *)namespaceURI
                       atIndex:(NSUInteger)index;

/**
 * Resets the parser, releasing the content.
 */
- (void)resetParser;

/**
 * Checks whether the string is a valid element name.
 *
 * @param anElementString The element name to check
 * @return YES when the provided element name is valid, NO otherwise
 */
+ (BOOL)isValidElementName:(NSString *)anElementString;

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Default implementation returns BXPOElementUnknown
 *
 * @param anElement The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)anElement
                  namespaceURI:(NSString *)namespaceURI;

/**
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Default implementation returns nil
 *
 * @param anElement The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element parser
 */
- (StructuredXMLParser *)parserForElement:(NSString *)anElement
                             namespaceURI:(NSString *)namespaceURI;

/**
 * Sets the string for an element with a given namespace at the given position. Previously existing element in that position is released.
 * When position is out of bounds, instances of the same class as the new object are inserted. Inserted element must have the same class as the previously stored ones
 *
 * @param elementObject The new element object to store
 * @param element The element to modify
 * @param namespaceURI The element name space
 * @param index The obnject index inside the element array
 */
- (void)setElementObject:(NSObject *)elementObject
              forElement:(NSString *)element
        withNamespaceURI:(NSString *)namespaceURI
                 atIndex:(NSUInteger)index;

/**
 * Sets an array of elements with a given namespace. Previously existing elements in the array are removed. All objects must be the same instance
 *
 * @param elementArray The new element array to store
 * @param element The element tag
 * @param namespaceURI The element name space
 */
- (void)setElementArray:(NSArray *)elementArray
              forElement:(NSString *)element
        withNamespaceURI:(NSString *)namespaceURI;

/**
 * Removes a given parser associated to the given element name
 *
 * @param anAssociatedParser The associated parser to remove
 * @param anElement The element name to remove the associated parser from
 * @param namespaceURI The element name space
 */
- (void)removeParser:(StructuredXMLParser *)anAssociatedParser
         fromElement:(NSString *)anElement
        namespaceURI:(NSString *)namespaceURI;

@end
