/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"

/**
 * Loan detail class contains information about a Loan
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Loan : StatusEnabledResponse {
    
@private 
    
    /**
     * Loan number
     */
    NSString *number_;
    
    /**
     * Loan type
     */
    NSString *type_;
    
    /**
     * Loan initial amount string
     */
    NSString *initialAmountString_;
    
    /**
     * Loan initial amount
     */
    NSDecimalNumber *initialAmount_;
    
    /**
     * Loan pending amount string
     */
    NSString *pendingAmountString_;
    
    /**
     * Loan pending amount
     */
    NSDecimalNumber *pendingAmount_;
    
    /**
     * Loan currency
     */
    NSString *currency_;

    /**
     * Loan past-due debt flag string
     */
    NSString *pastDueDebtString_;
    
    /**
     * Loan past-due debt flag
     */
    BOOL pastDueDebt_;
    
    /**
     * Loan subject
     */
    NSString *subject_;
    
    /**
     * Amount converted into country currency string
     */
    NSString *amountInCountryCurrencyString_;
    
    /**
     * Amount converted into country currency
     */
    NSDecimalNumber *amountInCountryCurrency_;
    
}

/**
 * Provides read-only access to the loan number
 */
@property (nonatomic, readonly, copy) NSString *number;

/**
 * Provides read-only access to the loan type
 */
@property (nonatomic, readonly, copy) NSString *type;

/**
 * Provides read-only access to the loan initial amount string
 */
@property (nonatomic, readonly, copy) NSString *initialAmountString;

/**
 * Provides read-only access to the loan initial amount
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *initialAmount;

/**
 * Provides read-only access to the loan pending amount string
 */
@property (nonatomic, readonly, copy) NSString *pendingAmountString;

/**
 * Provides read-only access to the loan pending amount
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *pendingAmount;

/**
 * Provides read-only access to the loan currency
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the loan past-due debt flag string
 */
@property (nonatomic, readonly, copy) NSString *pastDueDebtString;

/**
 * Provides read-only access to the loan past-due debt flag
 */
@property (nonatomic, readonly, assign) BOOL pastDueDebt;

/**
 * Provides read-only access to the loan subject
 */
@property (nonatomic, readonly, copy) NSString *subject;

/**
 * Provides read-only access to the amount converted into cuntry currency string
 */
@property (nonatomic, readonly, copy) NSString *amountInCountryCurrencyString;

/**
 * Provides read-only access to the amount converted into country currency
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *amountInCountryCurrency;

/**
 * Returns the loan list display name. It can be used whenever a loan name must be displayed inside a list
 *
 * @return The loan list display name
 */
- (NSString *)loanListName;

/**
 * Returns the obfuscated loan number
 *
 * @return The obfuscated loan number
 */
- (NSString *)obfuscatedLoanNumber;

@end
