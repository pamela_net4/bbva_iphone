/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class EmailInfo;


/**
 * Contains the list of EmailInfos
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface EmailInfoList : StatusEnabledResponse {

@private

	/**
	 * EmailInfos list array
	 */
	NSMutableArray *emailinfoList_;

	/**
	 * Auxiliar EmailInfo object for parsing
	 */
	EmailInfo *auxEmailInfo_;

}

/**
 * Provides read-only access to the emailinfos list array
 */
@property (nonatomic, readonly, retain) NSArray *emailinfoList;

/**
 * Provides read access to the number of EmailInfos stored
 */
@property (nonatomic, readonly, assign) NSUInteger emailinfoCount;

/**
 * Returns the EmailInfo located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the EmailInfo is located
 *
 * @return EmailInfo located at the given position, or nil if position is not valid
 */
- (EmailInfo *)emailinfoAtPosition:(NSUInteger)aPosition;

/**
 * Returns the EmailInfo position inside the list
 *
 * @param  aEmailInfo EmailInfo object which position we are looking for
 *
 * @return EmailInfo position inside the list
 */
- (NSUInteger)emailinfoPosition:(EmailInfo *)aEmailInfo;

/**
 * Returns the Laon with an emailinfo number, or nil if not valid
 *
 * @param  aEmailInfoId The emailinfo id
 * @return EmailInfo with this id, or nil if not valid
 */
//- (EmailInfo *)emailinfoFromEmailInfoId:(NSString *)aEmailInfoId;

/**
 * Updates the EmailInfo list from another EmailInfoList instance
 *
 * @param  aEmailInfoList The EmailInfoList list to update from
 */
- (void)updateFrom:(EmailInfoList *)aEmailInfoList;

/**
 * Remove the contained data
 */
- (void)removeData;

@end

