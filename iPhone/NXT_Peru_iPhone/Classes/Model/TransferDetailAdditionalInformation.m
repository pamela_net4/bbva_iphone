//
//  TransferDetailAdditionalInformation.m
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 10/2/13.
//
//

#import "TransferDetailAdditionalInformation.h"


/**
 * Enumerates the analysis states
 */
typedef enum {

    tsaixeas_AnalyzingOperationNumber = serxeas_ElementsCount, //!<Analyzing the operation number
    tsaixeas_AnalyzingDate, //!<Analyzing the date
    tsaixeas_AnalyzingAmountToPay, //!<Analyzing the amount to pay
    tsaixeas_AnalyzingState, //!<Analyzing the state
    tsaixeas_AnalyzingBeneficiary, //!<Analyzing the state
    tsaixeas_AnalyzingCurrencyAmountToPay //!<Analyzing the currency amount to pay
    
} TransferSuccessAdditionalInformationXMLElementAnalyzerState;

#pragma mark -

/**
 * TransferSuccessAdditionalInformation private category
 */
@interface TransferDetailAdditionalInformation(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearTransferDetailAdditionalInformationData;

@end

#pragma mark -

@implementation TransferDetailAdditionalInformation

#pragma mark -
#pragma mark Properties
@synthesize dateString = dateString_;
@synthesize operationNumber = operationNumber_;
@synthesize beneficiary = beneficiary_;
@synthesize amountToPay = amountToPay_;
@synthesize currencyAmountToPay = currencyAmountToPay_;
@synthesize transferState = transferState_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearTransferDetailAdditionalInformationData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearTransferDetailAdditionalInformationData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"numerooperacion"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingOperationNumber;
            
        } else if ([lname isEqualToString:@"fecha"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingDate;
            
        } else if ([lname isEqualToString:@"beneficiario"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingBeneficiary;
            
        } else if ([lname isEqualToString:@"moneda"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingCurrencyAmountToPay;
            
        } else if ([lname isEqualToString:@"importe"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingAmountToPay;
            
        } else if ([lname isEqualToString:@"estado"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingState;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingOperationNumber) {
            
            [operationNumber_ release];
            operationNumber_ = nil;
            operationNumber_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingDate) {
            
            [dateString_ release];
            dateString_ = nil;
            dateString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingAmountToPay) {
            
            [amountToPay_ release];
            amountToPay_ = nil;
            amountToPay_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingCurrencyAmountToPay) {
            
            [currencyAmountToPay_ release];
            currencyAmountToPay_ = nil;
            currencyAmountToPay_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingState) {
            
            [transferState_ release];
            transferState_ = nil;
            transferState_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingBeneficiary) {
            
            [beneficiary_ release];
            beneficiary_ = nil;
            beneficiary_ = [elementString copyWithZone:self.zone];
            
        } 
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end


#pragma mark -

@implementation TransferDetailAdditionalInformation(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearTransferDetailAdditionalInformationData {
    
    [operationNumber_ release];
    operationNumber_ = nil;
    
    [dateString_ release];
    dateString_ = nil;
	
	[amountToPay_ release];
	amountToPay_ = nil;
	
	[currencyAmountToPay_ release];
	currencyAmountToPay_ = nil;
	
	[beneficiary_ release];
	beneficiary_ = nil;
    
    [transferState_ release];
    transferState_ = nil;
	
}


@end
