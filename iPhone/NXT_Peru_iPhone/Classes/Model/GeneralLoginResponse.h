/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
    glxeas_AnalyzingAccessValidation = serxeas_ElementsCount, //!<Analyzing the access validation
    glxeas_AnalyzingStatus,  //!<Analyzing the status
	glxeas_AnalyzingErrorCode, //!<Analyzing the error code
    glxeas_AnalyzingCount //!<Subclasses must start their analysis elements at this position
    
} GeneralLoginXMLElementAnalyzerState;


/**
 * Contains information common to all login responsed: coordinate and error code. Those tags can appear or not
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GeneralLoginResponse : StatusEnabledResponse {
    
@private
    
    /**
     * Coordinate
     */
    NSString *status_;
    
    /**
     * Error code
     */
    NSString *loginErrorCode_;

}


/**
 * Provides read-write access to the coordinate
 */
@property (nonatomic, readwrite, copy) NSString *status;

/**
 * Provides read-write access to the error code
 */
@property (nonatomic, readwrite, copy) NSString *loginErrorCode;

@end
