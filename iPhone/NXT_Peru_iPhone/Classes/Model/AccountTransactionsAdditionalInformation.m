/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "AccountTransactionsAdditionalInformation.h"


/**
 * Enumerates the analysis state
 */
typedef enum {
    
    ataixeas_AnalyzingMoreData = serxeas_ElementsCount, //!<Analyzing the more data element
	ataixeas_AnalyzingIndex //!<Analyzing the index
    
} AccountTransactionsAdditionalInformationXMLElementAnalyzerState;


#pragma mark -

/**
 * AccountTransactionAdditionalnformation private category
 */
@interface AccountTransactionsAdditionalInformation(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearAccountTransactionsAdditionalInformationData;

@end


#pragma mark -

@implementation AccountTransactionsAdditionalInformation

#pragma mark -
#pragma mark Properties

@synthesize moreData = moreData_;
@synthesize index = index_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearAccountTransactionsAdditionalInformationData];
	
	[super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initialized. Initializes a AccountTransactionAdditionalnformation instance
 *
 * @return The initialized AccountTransactionAdditionalnformation instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationAccountTransactionsAdditionalnformationUpdated;
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates this instance with the given account transaction additional information
 */
- (void)updateFrom:(AccountTransactionsAdditionalInformation *)anAccountTransactionsAdditionalInformation {
    
    NSZone *zone = self.zone;
    
    [moreData_ release];
    moreData_ = nil;
    moreData_ = [anAccountTransactionsAdditionalInformation.moreData copyWithZone:zone];
    
    [index_ release];
    index_ = nil;
    index_ = [anAccountTransactionsAdditionalInformation.index copyWithZone:zone];
    
    [self updateFromStatusEnabledResponse:anAccountTransactionsAdditionalInformation];
	
	self.informationUpdated = anAccountTransactionsAdditionalInformation.informationUpdated;
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearAccountTransactionsAdditionalInformationData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"indmasdatos"]) {
            
            xmlAnalysisCurrentValue_ = ataixeas_AnalyzingMoreData;
            
        } else if ([lname isEqualToString: @"indindice"]) {
            
            xmlAnalysisCurrentValue_ = ataixeas_AnalyzingIndex;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == ataixeas_AnalyzingMoreData) {
            
            [moreData_ release];
            moreData_ = nil;
            moreData_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == ataixeas_AnalyzingIndex) {
            
            [index_ release];
            index_ = nil;
            index_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end


#pragma mark -

@implementation AccountTransactionsAdditionalInformation(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearAccountTransactionsAdditionalInformationData {
    
    [moreData_ release];
    moreData_ = nil;
    
    [index_ release];
    index_ = nil;
    
}

@end
