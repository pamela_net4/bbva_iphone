/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


/**
 * Deposit detail class contains information about a Deposit
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Deposit : StatusEnabledResponse {
    
@private
    
    /**
     * Deposit account number
     */
    NSString *accountNumber_;
    
    /**
     * Current balance string
     */
    NSString *currentBalanceString_;
    
    /**
     * Current balance
     */
    NSDecimalNumber *currentBalance_;
    
    /**
     * Available balance string
     */
    NSString *availableBalanceString_;
    
    /**
     * Available balance
     */
    NSDecimalNumber *availableBalance_;
    
    /**
     * Currency
     */
    NSString *currency_;

    /**
     * Subject
     */
    NSString *subject_;
    
    /**
     * Balance converted into country currency string
     */
    NSString *balanceInCountryCurrencyString_;
    
    /**
     * Balance converted into country currency
     */
    NSDecimalNumber *balanceInCountryCurrency_;
   
    /**
     * currency Available into current cts string
     */
    NSString *currencyAvailable_;
    
    /**
     * Cts indicator string
     */
    NSString *ctsIndicator_;
    
    /**
     * grouping code for cts
     */
    NSString *groupingCode_;
}


/**
 * Provides read-only access to the deposit account number
 */
@property (nonatomic, readonly, copy) NSString *accountNumber;

/**
 * Provides read-only access to the current balance string
 */
@property (nonatomic, readonly, copy) NSString *currentBalanceString;

/**
 * Provides read-only access to the current balance
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *currentBalance;

/**
 * Provides read-only access to the available balance string
 */
@property (nonatomic, readonly, copy) NSString *availableBalanceString;

/**
 * Provides read-only access to the available balance
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *availableBalance;

/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the subject
 */
@property (nonatomic, readonly, copy) NSString *subject;

/**
 * Provides read-only access to the balance converted into cuntry currency string
 */
@property (nonatomic, readonly, copy) NSString *balanceInCountryCurrencyString;

/**
 * Provides read-only access to the balance converted into country currency
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *balanceInCountryCurrency;

/**
 * Provides read-only access to currency Available string
 */
@property (nonatomic, readonly, copy) NSString *currencyAvailable;

/**
 * Provides read-only access to the cts Indicator string
 */
@property (nonatomic, readonly, copy) NSString *ctsIndicator;

/**
 * Provides read-only access to the grouping Code string
 */
@property (nonatomic, readonly, copy) NSString *groupingCode;

/**
 * Returns the deposit list display name. It can be used whenever a deposit name must be displayed inside a list
 *
 * @return The deposit list display name
 */
- (NSString *)depositListName;

/**
 * Returns the obfuscated deposit account number
 *
 * @return The obfuscated deposit account number
 */
- (NSString *)obfuscatedDepositAccountNumber;
//
//-(void) setAccountNumber:(NSString *)number;
//
//-(void) setGroupingCode:(NSString *)code;

@end

