//
//  CampaignList.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
#import "Campaign.h"

@interface CampaignList : StatusEnabledResponse{
@private
    NSMutableArray *campaignList_;
    Campaign *campaignAux_;
}

@property (nonatomic, readonly, retain) NSArray *campaignList;

- (void) updateFrom:(CampaignList *) campaignListOrigin;
- (Campaign *) campaignAtPosition: (NSUInteger) aPosition;
- (void) removeData;

@end
