//
//  SalaryAdvanceSummary.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"


@interface SalaryAdvanceSummary : StatusEnabledResponse{
    @private

   /**
     * SalaryAdvanceSummary numberOperation
     */
    NSString *numberOperation_;


   /**
     * SalaryAdvanceSummary account
     */
    NSString *account_;


   /**
     * SalaryAdvanceSummary commission
     */
    NSString *commission_;


   /**
     * SalaryAdvanceSummary amountAdvance
     */
    NSString *amountAdvance_;


   /**
     * SalaryAdvanceSummary dayOfPay
     */
    NSString *dayOfPay_;


   /**
     * SalaryAdvanceSummary date
     */
    NSString *date_;


   /**
     * SalaryAdvanceSummary hour
     */
    NSString *hour_;


   /**
     * SalaryAdvanceSummary messageNotification
     */
    NSString *messageNotification_;


   /**
     * SalaryAdvanceSummary customer
     */
    NSString *customer_;




}

/**
 * Provides read-only access to the SalaryAdvanceSummary numberOperation
 */
@property (nonatomic, readonly, copy) NSString * numberOperation;


/**
 * Provides read-only access to the SalaryAdvanceSummary account
 */
@property (nonatomic, readonly, copy) NSString * account;


/**
 * Provides read-only access to the SalaryAdvanceSummary commission
 */
@property (nonatomic, readonly, copy) NSString * commission;


/**
 * Provides read-only access to the SalaryAdvanceSummary amountAdvance
 */
@property (nonatomic, readonly, copy) NSString * amountAdvance;


/**
 * Provides read-only access to the SalaryAdvanceSummary dayOfPay
 */
@property (nonatomic, readonly, copy) NSString * dayOfPay;


/**
 * Provides read-only access to the SalaryAdvanceSummary date
 */
@property (nonatomic, readonly, copy) NSString * date;


/**
 * Provides read-only access to the SalaryAdvanceSummary hour
 */
@property (nonatomic, readonly, copy) NSString * hour;


/**
 * Provides read-only access to the SalaryAdvanceSummary messageNotification
 */
@property (nonatomic, readonly, copy) NSString * messageNotification;


/**
 * Provides read-only access to the SalaryAdvanceSummary customer
 */
@property (nonatomic, readonly, copy) NSString * customer;




@end

