/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class FastLoanQuota;


/**
 * Contains the list of FastLoanQuotas
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface FastLoanQuotaList : StatusEnabledResponse {

@private

	/**
	 * FastLoanQuotas list array
	 */
	NSMutableArray *fastloanquotaList_;

	/**
	 * Auxiliar FastLoanQuota object for parsing
	 */
	FastLoanQuota *auxFastLoanQuota_;

}

/**
 * Provides read-only access to the fastloanquotas list array
 */
@property (nonatomic, readonly, retain) NSArray *fastloanquotaList;

/**
 * Provides read access to the number of FastLoanQuotas stored
 */
@property (nonatomic, readonly, assign) NSUInteger fastloanquotaCount;

/**
 * Returns the FastLoanQuota located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the FastLoanQuota is located
 *
 * @return FastLoanQuota located at the given position, or nil if position is not valid
 */
- (FastLoanQuota *)fastloanquotaAtPosition:(NSUInteger)aPosition;

/**
 * Returns the FastLoanQuota position inside the list
 *
 * @param  aFastLoanQuota FastLoanQuota object which position we are looking for
 *
 * @return FastLoanQuota position inside the list
 */
- (NSUInteger)fastloanquotaPosition:(FastLoanQuota *)aFastLoanQuota;

/**
 * Returns the Laon with an fastloanquota number, or nil if not valid
 *
 * @param  aFastLoanQuotaId The fastloanquota id
 * @return FastLoanQuota with this id, or nil if not valid
 */
//- (FastLoanQuota *)fastloanquotaFromFastLoanQuotaId:(NSString *)aFastLoanQuotaId;

/**
 * Updates the FastLoanQuota list from another FastLoanQuotaList instance
 *
 * @param  aFastLoanQuotaList The FastLoanQuotaList list to update from
 */
- (void)updateFrom:(FastLoanQuotaList *)aFastLoanQuotaList;

/**
 * Remove the contained data
 */
- (void)removeData;

@end

