/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "StatusEnabledResponse.h"

/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    mrxeas_AnalyzingMethod = serxeas_ElementsCount,
	mrxeas_MATElementsCount //!<Number of elements in the enumerate. Not ment to be used as a state
} MATXMLElementAnalyzerState;


/**
 * Parses the MAT configuration XML
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MATConfigurationResponse : StatusEnabledResponse {
@private
    /**
	 * Array of methods
	 */
	NSMutableArray *methodsList_;
    
    /**
     * Aux method to parse with
     */
    NSMutableString *auxMethod_;
}

/**
 * Provides readwrite access to the methodsList
 */
@property (nonatomic, readonly, retain) NSArray *methodsList;

@end
