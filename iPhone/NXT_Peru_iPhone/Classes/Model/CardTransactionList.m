/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "CardTransactionList.h"
#import "CardTransaction.h"


#pragma mark -

/**
 * CardTransactionList private category
 */
@interface CardTransactionList(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearCardTransactionListData;

@end


#pragma mark -

@implementation CardTransactionList

#pragma mark -
#pragma mark Properties

@dynamic cardTransactionList;
@dynamic cardTransactionCount;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearCardTransactionListData];
    
    [cardTransactionList_ release];
    cardTransactionList_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Initialization

/**
 * Object initializer
 *
 * @return An initialized instance
 */
- (id) init {
    
	if (self = [super init]) {
        
        cardTransactionList_ = [[NSMutableArray alloc] init];
		informationUpdated_ = soie_NoInfo;
		self.notificationToPost = kNotificationCardTransactionListReceived;
        
	}
	
	return self;
    
}

#pragma mark -
#pragma mark Information management

/*	
 * Updates the card transaction list from another card transaction list
 */
- (void)updateFrom:(CardTransactionList *)aCardTransactionList {
	
    NSArray *cardTransactions = aCardTransactionList.cardTransactionList;
	
	[self clearCardTransactionListData];
	
	if (cardTransactions != nil) {
        
		for (CardTransaction *cardTransaction in cardTransactions) {
            
            [cardTransactionList_ addObject:cardTransaction];
            
		}
        
	}
	
    [self updateFromStatusEnabledResponse:aCardTransactionList];
	
	self.informationUpdated = aCardTransactionList.informationUpdated;
	
}

/*
 * Remove the contained data
 */
- (void)removeData {
    
	[self clearCardTransactionListData];
    
}

/*
 * Returns the CardTransaction located at the given position, or nil if position is not valid
 */
- (CardTransaction *)cardTransactionAtPosition:(NSUInteger)aPosition {
    
	CardTransaction *result = nil;
	
	if (aPosition < [cardTransactionList_ count]) {
        
		result = [cardTransactionList_ objectAtIndex:aPosition];
        
	}
	
	return result;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearCardTransactionListData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (auxCardTransaction_ != nil) {
        
        [cardTransactionList_ addObject:auxCardTransaction_];
		[auxCardTransaction_ release];
		auxCardTransaction_ = nil;
        
	}
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
            auxCardTransaction_ = [[CardTransaction alloc] init];
            [auxCardTransaction_ setParentParseableObject:self];
            auxCardTransaction_.openingTag = lname;
            [parser setDelegate:auxCardTransaction_];
            [auxCardTransaction_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
	if (auxCardTransaction_ != nil) {
        
        [cardTransactionList_ addObject:auxCardTransaction_];
		[auxCardTransaction_ release];
		auxCardTransaction_ = nil;
        
	}
    
    if ([cardTransactionList_ count] == 1) {
        
        CardTransaction *cardTransaction = [cardTransactionList_ objectAtIndex:0];
        
        if ([[cardTransaction amount] isEqualToNumber:[NSDecimalNumber zero]]) {
            
            [cardTransactionList_ removeAllObjects];
            
        }
        
    }
    
    [cardTransactionList_ sortUsingSelector:@selector(compareForDescendingDateOrdering:)];

    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the card transaction list
 *
 * @return The card transaction list
 */
- (NSArray *)cardTransactionList {
    
    return [NSArray arrayWithArray:cardTransactionList_];
    
}

/*
 * Returns the card transactions count
 *
 * @return The card transactions count
 */
- (NSUInteger)cardTransactionCount {
    
    return [cardTransactionList_ count];
    
}

@end


#pragma mark -

@implementation CardTransactionList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearCardTransactionListData {
    
    [cardTransactionList_ removeAllObjects];
    
    [auxCardTransaction_ release];
    auxCardTransaction_ = nil;
    
}

@end
