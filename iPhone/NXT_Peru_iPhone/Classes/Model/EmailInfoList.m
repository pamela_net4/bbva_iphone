/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "EmailInfoList.h"
#import "EmailInfo.h"


#pragma mark -

/**
 * EmailInfoList private category
 */
@interface EmailInfoList (private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearEmailInfoListData;

@end


#pragma mark -

@implementation EmailInfoList

@dynamic emailinfoList;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [self clearEmailInfoListData];

	[emailinfoList_ release];
	emailinfoList_ = nil;

	[super dealloc];

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a EmailInfoList instance creating the associated array
 *
 * @return The initialized EmailInfoList instance
 */
- (id)init {

    if (self = [super init]) {

        emailinfoList_ = [[NSMutableArray alloc] init];

    }

    return self;

}

#pragma mark -
#pragma mark Information management

/*
 * Updates the EmailInfo list from another EmailInfoList instance
 */
- (void)updateFrom:(EmailInfoList *)aEmailInfoList {

    NSArray *otherEmailInfoList = aEmailInfoList.emailinfoList;

	[self clearEmailInfoListData];

    for (EmailInfo *emailinfo in otherEmailInfoList) {

        [emailinfoList_ addObject:emailinfo];

    }

    [self updateFromStatusEnabledResponse:aEmailInfoList];

	self.informationUpdated = aEmailInfoList.informationUpdated;

}

/*
 * Remove object data
 */
- (void)removeData {

    [self clearEmailInfoListData];

}

#pragma mark -
#pragma mark Getters and setters

/**
 * Provides read access to the number of EmailInfo stored
 *
 * @return The count
 */
- (NSUInteger) emailinfoCount {
	return [emailinfoList_ count];
}

/*
 * Returns the EmailInfo located at the given position, or nil if position is not valid
 */
- (EmailInfo*) emailinfoAtPosition: (NSUInteger) aPosition {
	EmailInfo* result = nil;

	if (aPosition < [emailinfoList_ count]) {
		result = [emailinfoList_ objectAtIndex: aPosition];
	}

	return result;
}

/*
 * Returns the EmailInfo position inside the list
 */
- (NSUInteger) emailinfoPosition: (EmailInfo*) aEmailInfo {

	return [emailinfoList_ indexOfObject:aEmailInfo];

}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {

    [super parserDidStartDocument:parser];

	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearEmailInfoListData];

}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {

    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];

	if (auxEmailInfo_ != nil) {

		[emailinfoList_ addObject:auxEmailInfo_];
		[auxEmailInfo_ release];
		auxEmailInfo_ = nil;

	}

    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        NSString* lname = [elementName lowercaseString];

        if ([lname isEqualToString: @"correo"] || [lname isEqualToString: @"e"]) {

            auxEmailInfo_ = [[EmailInfo alloc] init];
            [auxEmailInfo_ setParentParseableObject:self];
            auxEmailInfo_.openingTag = lname;
            [parser setDelegate:auxEmailInfo_];
            [auxEmailInfo_ parserDidStartDocument: parser];

        } else {

            xmlAnalysisCurrentValue_ = serxeas_Nothing;

        }

    }

}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {

    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];

    if (auxEmailInfo_ != nil) {

		[emailinfoList_ addObject:auxEmailInfo_];
		[auxEmailInfo_ release];
		auxEmailInfo_ = nil;

	}

    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        informationUpdated_ = soie_InfoAvailable;

    }

    xmlAnalysisCurrentValue_ = serxeas_Nothing;

}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the emailinfo list array
 *
 * @return The emailinfo list array
 */
- (NSArray *)emailinfoList {

    return [NSArray arrayWithArray:emailinfoList_];

}

/**
 * Returns the EmailInfo with an EmailInfo id, or nil if not valid
 *//*
- (EmailInfo *)emailinfoFromEmailInfoId:(NSString *)aEmailInfoId {

	EmailInfo *emailinfo = nil;

	if (aEmailInfoId != nil) {
		EmailInfo *target;
		NSUInteger size = [emailinfoList_ count];
		BOOL found = NO;
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
			target = [emailinfoList_ objectAtIndex:i];
			if ([aEmailInfoId isEqualToString:target.id]) {
				emailinfo = target;
				found = YES;
			}
		}
	}

	return emailinfo;

}*/
@end


#pragma mark -

@implementation EmailInfoList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearEmailInfoListData {

    [emailinfoList_ removeAllObjects];

	informationUpdated_ = soie_NoInfo;

}

@end

