/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class Loan;


/**
 * Contains the list of Loans
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface LoanList : StatusEnabledResponse {
    
@private
	
	/**
	 * Loans list array
	 */
	NSMutableArray *loanList_;
		
	/**
	 * Auxiliar Loan object for parsing
	 */
	Loan *auxLoan_;
    
}

/**	
 * Provides read-only access to the loans list array
 */
@property (nonatomic, readonly, retain) NSArray *loanList;

/**	
 * Provides read access to the number of Loans stored
 */
@property (nonatomic, readonly, assign) NSUInteger loanCount;

/**
 * Returns the Loan located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the Loan is located
 *
 * @return Loan located at the given position, or nil if position is not valid
 */
- (Loan *)loanAtPosition:(NSUInteger)aPosition;

/**
 * Returns the Loan position inside the list
 *
 * @param  aLoan Loan object which position we are looking for
 *
 * @return Loan position inside the list
 */
- (NSUInteger)loanPosition:(Loan *)aLoan;

/**
 * Returns the Laon with an loan number, or nil if not valid
 *
 * @param  aLoanNumber The loan number
 * @return Loan with this number, or nil if not valid
 */
- (Loan *)loanFromLoanNumber:(NSString *)aLoanNumber;

/**
 * Updates the Loan list from another LoanList instance
 *
 * @param  aLoanList The LoanList list to update from
 */
- (void)updateFrom:(LoanList *)aLoanList;

/**
 * Remove the contained data
 */
- (void)removeData;

@end
