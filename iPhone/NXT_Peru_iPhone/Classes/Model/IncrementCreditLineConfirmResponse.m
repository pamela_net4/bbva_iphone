//
//  IncrementCreditLineConfirmResponse.m
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "IncrementCreditLineConfirmResponse.h"
#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     iclcreas_AnalyzingClientName = serxeas_ElementsCount, //!<Analyzing the IncrementCreditLineConfirmResponse clientName
     iclcreas_AnalyzingCardNumber , //!<Analyzing the IncrementCreditLineConfirmResponse cardNumber
     iclcreas_AnalyzingCardName , //!<Analyzing the IncrementCreditLineConfirmResponse cardName
     iclcreas_AnalyzingEmail , //!<Analyzing the IncrementCreditLineConfirmResponse email
     iclcreas_AnalyzingAmountIncrement , //!<Analyzing the IncrementCreditLineConfirmResponse amountIncrement
     iclcreas_AnalyzingCurrency , //!<Analyzing the IncrementCreditLineConfirmResponse currency
     iclcreas_AnalyzingOtpText , //!<Analyzing the IncrementCreditLineConfirmResponse otpText
     iclcreas_AnalyzingSeal , //!<Analyzing the IncrementCreditLineConfirmResponse seal
     iclcreas_AnalyzingCoordinate , //!<Analyzing the IncrementCreditLineConfirmResponse coordinate

} IncrementCreditLineConfirmResponseXMLElementAnalyzerState;

@implementation IncrementCreditLineConfirmResponse

#pragma mark -
#pragma mark Properties

@synthesize clientName = clientName_;
@synthesize cardNumber = cardNumber_;
@synthesize cardName = cardName_;
@synthesize email = email_;
@synthesize amountIncrement = amountIncrement_;
@synthesize currency = currency_;
@synthesize otpText = otpText_;
@synthesize seal = seal_;
@synthesize coordinate = coordinate_;
;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [clientName_ release];
    clientName_ = nil;

    [cardNumber_ release];
    cardNumber_ = nil;

    [cardName_ release];
    cardName_ = nil;

    [email_ release];
    email_ = nil;

    [amountIncrement_ release];
    amountIncrement_ = nil;

    [currency_ release];
    currency_ = nil;

    [otpText_ release];
    otpText_ = nil;

    [seal_ release];
    seal_ = nil;

    [coordinate_ release];
    coordinate_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if(0==1){
                   //delete fix
        }        else if ([lname isEqualToString: @"nombrecliente"]) {

            xmlAnalysisCurrentValue_ = iclcreas_AnalyzingClientName;


        }
        else if ([lname isEqualToString: @"numtarjeta"]) {

            xmlAnalysisCurrentValue_ = iclcreas_AnalyzingCardNumber;


        }
        else if ([lname isEqualToString: @"nombretarjeta"]) {

            xmlAnalysisCurrentValue_ = iclcreas_AnalyzingCardName;


        }
        else if ([lname isEqualToString: @"email"]) {

            xmlAnalysisCurrentValue_ = iclcreas_AnalyzingEmail;


        }
        else if ([lname isEqualToString: @"montoincremento"]) {

            xmlAnalysisCurrentValue_ = iclcreas_AnalyzingAmountIncrement;


        }
        else if ([lname isEqualToString: @"divisa"]) {

            xmlAnalysisCurrentValue_ = iclcreas_AnalyzingCurrency;


        }
        else if ([lname isEqualToString: @"textootp"]) {

            xmlAnalysisCurrentValue_ = iclcreas_AnalyzingOtpText;


        }
        else if ([lname isEqualToString: @"sello"]) {

            xmlAnalysisCurrentValue_ = iclcreas_AnalyzingSeal;


        }
        else if ([lname isEqualToString: @"coordenada"]) {

            xmlAnalysisCurrentValue_ = iclcreas_AnalyzingCoordinate;


        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(0==1){
           //delete fix
        }        else if (xmlAnalysisCurrentValue_ == iclcreas_AnalyzingClientName) {

            [clientName_ release];
            clientName_ = nil;
            clientName_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclcreas_AnalyzingCardNumber) {

            [cardNumber_ release];
            cardNumber_ = nil;
            cardNumber_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclcreas_AnalyzingCardName) {

            [cardName_ release];
            cardName_ = nil;
            cardName_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclcreas_AnalyzingEmail) {

            [email_ release];
            email_ = nil;
            email_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclcreas_AnalyzingAmountIncrement) {

            [amountIncrement_ release];
            amountIncrement_ = nil;
            amountIncrement_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclcreas_AnalyzingCurrency) {

            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclcreas_AnalyzingOtpText) {

            [otpText_ release];
            otpText_ = nil;
            otpText_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclcreas_AnalyzingSeal) {

            [seal_ release];
            seal_ = nil;
            seal_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclcreas_AnalyzingCoordinate) {

            [coordinate_ release];
            coordinate_ = nil;
            coordinate_ = [elementString copyWithZone:self.zone];

        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

