//
//  SafetyPayStatusAdditionalInformation.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "SafetyPayStatusAdditionalInformation.h"

/**
 * SafetyPayStatusAdditionalInformation private category
 */
@interface SafetyPayStatusAdditionalInformation (private)
/**
 * Clears the object data
 *
 * @private
 */
- (void) clearSafetyPayStatusAdditionalInformationData;

@end

#pragma mark -

@implementation SafetyPayStatusAdditionalInformation

#pragma mark -
#pragma mark Properties

@synthesize message = message_;
@synthesize activeStatus = activeStatus_;
@synthesize parseError = parseError_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearSafetyPayStatusAdditionalInformationData];
    
    [super dealloc];
    
}


/*
 * Remove the contained data
 */
- (void)removeData {
    
    [self clearSafetyPayStatusAdditionalInformationData];

}


- (void) updateFrom:(SafetyPayStatusAdditionalInformation *)anAdditionalInformation {
    
    NSZone *zone = self.zone;
    
    [activeStatus_ release];
    activeStatus_ = nil;
    activeStatus_ = [anAdditionalInformation.activeStatus copyWithZone:zone];
    
    [message_ release];
    message_ = nil;
    message_ = [anAdditionalInformation.message copyWithZone:zone];

}


#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
-(void) parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearSafetyPayStatusAdditionalInformationData];
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing){
        
        NSString *lname = [elementName lowercaseString];
        
        if([lname isEqualToString: @"estadoactivo"]){
            
            xmlAnalysisCurrentValue_ = stpaixeas_AnalyzingStatus;
            
        } else if([lname isEqualToString: @"mensajeinformativo"]){
            
            xmlAnalysisCurrentValue_ = stpaixeas_AnalyzingMessage;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
-(void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if(xmlAnalysisCurrentValue_ != serxeas_Nothing){
        
        NSString *lname = [elementName lowercaseString];
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if([lname isEqualToString: @"estadoactivo"]){
            
            [activeStatus_ release];
            activeStatus_ = nil;
            activeStatus_ = [elementString copyWithZone:self.zone];
            
        } else if([lname isEqualToString: @"mensajeinformativo"]){
            
            [message_ release];
            message_ = nil;
            message_ = [elementString copyWithZone:self.zone];
            
        }
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
    }
    
}

- (void) parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    [super parser:parser parseErrorOccurred:parseError];
    parseError_ = parseError;
    [self.parentParseableObject parser:parser parseErrorOccurred:parseError];
    
}

- (void) clearSafetyPayStatusAdditionalInformationData {
    [message_ release];
    message_ = nil;
    
    [activeStatus_ release];
    activeStatus_ = nil;
}

@end
