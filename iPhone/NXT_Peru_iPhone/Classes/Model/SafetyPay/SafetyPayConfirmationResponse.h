//
//  SafetyPayConfirmationResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"
#import "SafetyPayConfirmationAdditionalInformation.h"

typedef enum {
    stpcxeas_AnalyzingSafetyPayStatusResponseDetail,
    stpcxeas_AnalyzingAdditionalInformation,
    
} stpcxeas_AnalyzingSafetyPayStatusResponseXMLElementAnalyzerState;

@interface SafetyPayConfirmationResponse : StatusEnabledResponse
{
@private
    SafetyPayConfirmationAdditionalInformation *additionalInformation_;
    NSError *parseError_;
}
@property (nonatomic, readonly, retain) SafetyPayConfirmationAdditionalInformation *additionalInformation;
@property (nonatomic, readonly, assign) NSError *parseError;
@end
