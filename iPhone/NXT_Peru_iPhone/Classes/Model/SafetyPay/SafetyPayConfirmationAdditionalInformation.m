//
//  SafetyPayConfirmationAdditionalInformation.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "SafetyPayConfirmationAdditionalInformation.h"
#import "Tools.h"

@interface SafetyPayConfirmationAdditionalInformation(private)

- (void) clearSafetyPayConfirmationAdditionalInformationData;

@end

@implementation SafetyPayConfirmationAdditionalInformation

#pragma mark -
#pragma mark Properties

@synthesize transactionNumber = transactionNumber_;
@synthesize establishment = establishment_;
@synthesize currency = currency_;
@synthesize badge = badge_;
@synthesize amount = amount_;
@synthesize paidAmount = paidAmount_;
@synthesize company = company_;
@synthesize service = service_;
@dynamic operationDate;
@synthesize operationDateString = operationDateString_;
@synthesize operationNumber = operationNumber_;
@synthesize account = account_;
@synthesize parseError = parseError_;
@synthesize exchangeRate = exchangeRate_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearSafetyPayConfirmationAdditionalInformationData];
    
    [super dealloc];
    
}

- (void) removeData {
    
    [self clearSafetyPayConfirmationAdditionalInformationData];
    
}

- (void) updateFrom:(SafetyPayConfirmationAdditionalInformation *)anAdditionalInformation {
    
    [transactionNumber_ release];
    transactionNumber_ = nil;
    transactionNumber_ = [anAdditionalInformation.transactionNumber copyWithZone:self.zone];
    
    [establishment_ release];
    establishment_ = nil;
    establishment_ = [anAdditionalInformation.establishment copyWithZone:self.zone];
    
    [currency_ release];
    currency_ = nil;
    currency_ = [anAdditionalInformation.currency copyWithZone:self.zone];
    
    [badge_ release];
    badge_ = nil;
    badge_ = [anAdditionalInformation.badge copyWithZone:self.zone];
    
    [amount_ release];
    amount_ = nil;
    amount_ = [anAdditionalInformation.amount copyWithZone:self.zone];
    
    [paidAmount_ release];
    paidAmount_ = nil;
    paidAmount_ = [anAdditionalInformation.paidAmount copyWithZone:self.zone];
    
    [company_ release];
    company_ = nil;
    company_ = [anAdditionalInformation.company copyWithZone:self.zone];
    
    [service_ release];
    service_ = nil;
    service_ = [anAdditionalInformation.service copyWithZone:self.zone];
    
    [operationDateString_ release];
    operationDateString_ = nil;
    operationDateString_ = [anAdditionalInformation.operationDateString copyWithZone:self.zone];
    
    [operationDate_ release];
    operationDate_ = nil;
    operationDate_ = [anAdditionalInformation.operationDate copyWithZone:self.zone];
    
    [operationNumber_ release];
    operationNumber_ = nil;
    operationNumber_ = [anAdditionalInformation.operationNumber copyWithZone:self.zone];
    
    [account_ release];
    account_ = nil;
    account_ = [BankAccount alloc];
    [account_ updateFrom: [anAdditionalInformation account]];
    
    [exchangeRate_ release];
    exchangeRate_ = nil;
    exchangeRate_ = [anAdditionalInformation.exchangeRate copyWithZone:self.zone];
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
-(void) parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearSafetyPayConfirmationAdditionalInformationData];
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString *lname = [elementName lowercaseString];
        
        if([lname isEqualToString: @"identificador"]){
            
            xmlAnalysisCurrentValue_ = stpcaixeas_AnalyzingTransactionNumber;
            
        } else if([lname isEqualToString: @"establecimiento"]){
            
            xmlAnalysisCurrentValue_ = stpcaixeas_AnalyzingEstablishment;
            
        } else if([lname isEqualToString: @"moneda"]){
            
            xmlAnalysisCurrentValue_ = stpcaixeas_AnalyzingCurrency;
            
        } else if([lname isEqualToString: @"divimporte"]){
            
            xmlAnalysisCurrentValue_ = stpcaixeas_AnalyzingBadge;
            
        } else if([lname isEqualToString: @"importecargado"]){
            
            xmlAnalysisCurrentValue_ = stpcaixeas_AnalyzingAmount;
            
        } else if([lname isEqualToString: @"importepagado"]){
            
            xmlAnalysisCurrentValue_ = stpcaixeas_AnalyzingPaidAmount;
            
        } else if([lname isEqualToString: @"cuenta"]){
            
            xmlAnalysisCurrentValue_ = stpcaixeas_AnalyzingAccount;
            account_ = nil;
            account_ = [[BankAccount alloc] init];
            [account_ setParentParseableObject:self];
            account_.openingTag = lname;
            [parser setDelegate:account_];
            [account_ parserDidStartDocument:parser];
            
        } else if([lname isEqualToString: @"empresa"]){
            
            xmlAnalysisCurrentValue_ = stpcaixeas_AnalyzingCompany;
            
        } else if([lname isEqualToString: @"servicio"]){
            
            xmlAnalysisCurrentValue_ = stpcaixeas_AnalyzingService;
            
        } else if([lname isEqualToString: @"fechahora"]){
            
            xmlAnalysisCurrentValue_ = stpcaixeas_AnalyzingOperationDate;
            
        } else if([lname isEqualToString: @"numoperacion"]){
            
            xmlAnalysisCurrentValue_ = stpcaixeas_AnalyzingOperationNumber;
            
        } else if([lname isEqualToString: @"tipocambio"]){
            
            xmlAnalysisCurrentValue_ = stpcaixeas_AnalyzingExchangeRate;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
        
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    
    NSString* lname = [elementName lowercaseString];
    
    if(xmlAnalysisCurrentValue_ != serxeas_Nothing){
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if([lname isEqualToString: @"identificador"]){
            
            [transactionNumber_ release];
            transactionNumber_ = nil;
            transactionNumber_ = [elementString copyWithZone:self.zone];
            
        } else if ([lname isEqualToString: @"establecimiento"]) {
            
            [establishment_ release];
            establishment_ = nil;
            establishment_ = [elementString copyWithZone:self.zone];
            
        } else if ([lname isEqualToString: @"moneda"]){
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];
            
        } else if([lname isEqualToString: @"divimporte"]){
            
            [badge_ release];
            badge_ = nil;
            badge_ = [elementString copyWithZone:self.zone];
            
        } else if ([lname isEqualToString: @"importecargado"]){
            
            [amount_ release];
            amount_ = nil;
            amount_ = [elementString copyWithZone:self.zone];
            
        }  else if ([lname isEqualToString: @"importepagado"]){
            
            [paidAmount_ release];
            paidAmount_ = nil;
            paidAmount_ = [elementString copyWithZone: self.zone];
            
        } else if([lname isEqualToString: @"empresa"]){
            
            [company_ release];
            company_ = nil;
            company_ = [elementString copyWithZone:self.zone];
            
        } else if([lname isEqualToString: @"servicio"]){
            
            [service_ release];
            service_ = nil;
            service_ = [elementString copyWithZone:self.zone];
            
        } else if([lname isEqualToString: @"fechahora"]){
            
            [operationDateString_ release];
            operationDateString_ = nil;
            operationDateString_ = [elementString copyWithZone:self.zone];
            
        } else if([lname isEqualToString: @"numoperacion"]){
            
            [operationNumber_ release];
            operationNumber_ = nil;
            operationNumber_ = [elementString copyWithZone:self.zone];
            
        } else if([lname isEqualToString: @"tipocambio"]){
        
            [exchangeRate_ release];
            exchangeRate_ = nil;
            exchangeRate_ = [elementString copyWithZone:self.zone];
            
        }
        
    }
}


- (void) parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    [super parser:parser parseErrorOccurred:parseError];
    parseError = parseError_;
    [self.parentParseableObject parser:parser parseErrorOccurred:parseError];
}

- (NSDate *)operationDate {
    
    NSDate *result = operationDate_;
    
    if (result == nil) {
        
        if ([operationDateString_ length] > 0) {
            
            operationDate_ = [[Tools dateFromServerString:operationDateString_] retain];
            result = operationDate_;
            
        }
        
    }
    
    return result;
}

- (void) clearSafetyPayConfirmationAdditionalInformationData {

    [transactionNumber_ release];
    transactionNumber_ = nil;
    
    [establishment_ release];
    establishment_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [badge_ release];
    badge_ = nil;
    
    [amount_ release];
    amount_ = nil;
    
    [paidAmount_ release];
    paidAmount_ = nil;
    
    [company_ release];
    company_ = nil;
    
    [service_ release];
    service_ = nil;
    
    [operationDate_ release];
    operationDate_ = nil;
    
    [operationDateString_ release];
    operationDateString_ = nil;
    
    [operationNumber_ release];
    operationNumber_ = nil;
    
    [account_ release];
    account_ = nil;
    
    [exchangeRate_ release];
    exchangeRate_ = nil;
}

@end
