//
//  SafetyPayConfirmationAdditionalInformation.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BankAccount.h"

typedef enum {
    stpcaixeas_AnalyzingSafetyPayStepFourResponseDetail,
    stpcaixeas_AnalyzingTransactionNumber,
    stpcaixeas_AnalyzingEstablishment,
    stpcaixeas_AnalyzingCurrency,
    stpcaixeas_AnalyzingBadge,
    stpcaixeas_AnalyzingAmount,
    stpcaixeas_AnalyzingPaidAmount,
    stpcaixeas_AnalyzingAccount,
    stpcaixeas_AnalyzingCompany,
    stpcaixeas_AnalyzingService,
    stpcaixeas_AnalyzingOperationDate,
    stpcaixeas_AnalyzingOperationNumber,
    stpcaixeas_AnalyzingExchangeRate
} stpcaixeas_AnalyzingSafetyPayStepFourResponseXMLElementAnalyzerState;


@interface SafetyPayConfirmationAdditionalInformation : StatusEnabledResponse
{
@private
    NSString *transactionNumber_;
    NSString *establishment_;
    NSString *currency_;
    NSString *badge_;
    NSString *amount_;
    NSString *paidAmount_;
    BankAccount *account_;
    NSString *company_;
    NSString *service_;
    NSString *operationDateString_;
    NSDate *operationDate_;
    NSString *operationNumber_;
    NSString *exchangeRate_;
    NSError *parseError_;
}
@property (nonatomic, readonly, copy) NSString *transactionNumber;
@property (nonatomic, readonly, copy) NSString *establishment;
@property (nonatomic, readonly, copy) NSString *currency;
@property (nonatomic, readonly, copy) NSString *badge;
@property (nonatomic, readonly, copy) NSString *amount;
@property (nonatomic, readonly, copy) NSString *paidAmount;
@property (nonatomic, readonly, copy) BankAccount *account;
@property (nonatomic, readonly, copy) NSString *company;
@property (nonatomic, readonly, copy) NSString *service;
@property (nonatomic, readonly, copy) NSString *operationDateString;
@property (nonatomic, readonly, copy) NSDate *operationDate;
@property (nonatomic, readonly, copy) NSString *operationNumber;
@property (nonatomic, readonly, copy) NSString *exchangeRate;
@property (nonatomic, readonly, assign) NSError *parseError;
- (void) updateFrom : (SafetyPayConfirmationAdditionalInformation *) anAdditionalInformation;
- (void) removeData;
@end
