//
//  SafetyPayTransactionAdditionalInformation.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "SafetyPayTransactionAdditionalInformation.h"

/**
 * SafetyPayTransactionAdditionalInformation private category
 */
@interface SafetyPayTransactionAdditionalInformation(private)
/**
 * Clears the object data
 *
 * @private
 */
- (void) clearSafetyPayTransactionAdditionalInformationData;
@end

#pragma mark -

@implementation SafetyPayTransactionAdditionalInformation

#pragma mark -
#pragma mark Properties

@synthesize transactionNumber = transactionNumber_;
@synthesize establishment = establishment_;
@synthesize currency = currency_;
@synthesize amount = amount_;
@synthesize accountList = accountList_;
@synthesize company = company_;
@synthesize service = service_;
@synthesize alterCarrierList = alterCarrierList_;
@synthesize parseError = parseError_;
#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearSafetyPayTransactionAdditionalInformationData];
    
    [super dealloc];
    
}

/*
 * Remove the contained data
 */
- (void)removeData {
    
    [self clearSafetyPayTransactionAdditionalInformationData];
    
}

- (void) updateFrom:(SafetyPayTransactionAdditionalInformation *)anAdditionalInformation {
    
    NSZone *zone = self.zone;
    
    [transactionNumber_ release];
    transactionNumber_ = nil;
    transactionNumber_ = [anAdditionalInformation.transactionNumber copyWithZone:zone];
    
    [establishment_ release];
    establishment_ = nil;
    establishment_ = [anAdditionalInformation.establishment copyWithZone:zone];
    
    [currency_ release];
    currency_ = nil;
    currency_ = [anAdditionalInformation.currency copyWithZone:zone];
    
    [amount_ release];
    amount_ = nil;
    amount_ = [anAdditionalInformation.amount copyWithZone:zone];
    
    [accountList_ release];
    accountList_ = nil;
    accountList_ = [[AccountList alloc] init];
    [accountList_ updateFrom: [anAdditionalInformation accountList]];
    
    [company_ release];
    company_ = nil;
    company_ = [anAdditionalInformation.company copyWithZone:zone];
    
    [service_ release];
    service_ = nil;
    service_ = [anAdditionalInformation.service copyWithZone:zone];
    
    [alterCarrierList_ release];
    alterCarrierList_ = nil;
    alterCarrierList_ = [[AlterCarrierList alloc] init];
    alterCarrierList_ = [anAdditionalInformation alterCarrierList];
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
-(void) parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearSafetyPayTransactionAdditionalInformationData];
}


/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing){
        
        NSString *lname = [elementName lowercaseString];
        
        if([lname isEqualToString: @"identificador"]){
            xmlAnalysisCurrentValue_ = stptaixeas_AnalyzingTransactionNumber;
        }
        if([lname isEqualToString: @"establecimiento"]){
            xmlAnalysisCurrentValue_ = stptaixeas_AnalyzingEstablishment;
        }
        if([lname isEqualToString: @"moneda"]){
            xmlAnalysisCurrentValue_ = stptaixeas_AnalyzingCurrency;
        }
        if([lname isEqualToString: @"importe"]){
            xmlAnalysisCurrentValue_ = stptaixeas_AnalyzingAmount;
        }
        if([lname isEqualToString: @"cuentas"]){
            xmlAnalysisCurrentValue_ = stptaixeas_AnalyzingAccountList;
            [accountList_ release];
            accountList_ = nil;
            accountList_ = [[AccountList alloc] init];
            accountList_.openingTag = lname;
            [accountList_ setParentParseableObject:self];
            [parser setDelegate:accountList_];
            [accountList_ parserDidStartDocument:parser];
        }
        if([lname isEqualToString: @"empresa"]){
            xmlAnalysisCurrentValue_ = stptaixeas_AnalyzingCompany;
        }
        if([lname isEqualToString: @"servicio"]){
            xmlAnalysisCurrentValue_ = stptaixeas_AnalyzingService;
        }
        if([lname isEqualToString: @"operadoras"]){
            xmlAnalysisCurrentValue_ = stptaixeas_AnalyzingCarrierList;
            [alterCarrierList_ release];
            alterCarrierList_ = nil;
            alterCarrierList_ = [[AlterCarrierList alloc] init];
            alterCarrierList_.openingTag = lname;
            [alterCarrierList_ setParentParseableObject:self];
            [parser setDelegate: alterCarrierList_];
            [alterCarrierList_ parserDidStartDocument:parser];
            
        }
        
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if ([lname isEqualToString: @"identificador"]) {
            
            [transactionNumber_ release];
            transactionNumber_ = nil;
            transactionNumber_ = [elementString copyWithZone:self.zone];
            
        } else if ([lname isEqualToString: @"establecimiento"]){
            
            [establishment_ release];
            establishment_ =  nil;
            establishment_ = [elementString copyWithZone:self.zone];
            
        } else if ([lname isEqualToString: @"moneda"]){
        
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];
            
        } else if ([lname isEqualToString: @"importe"]){
            
            [amount_ release];
            amount_ = nil;
            amount_ = [elementString copyWithZone:self.zone];
            
        } else if ([lname isEqualToString: @"empresa"]){
        
            [company_ release];
            company_ = nil;
            company_ = [elementString copyWithZone:self.zone];
            
        } else if ([lname isEqualToString: @"servicio"]){
        
            [service_ release];
            service_ = nil;
            service_ = [elementString copyWithZone:self.zone];
            
        }
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
    }
}

-(void) parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    [super parser:parser parseErrorOccurred:parseError];
    parseError_ = parseError;
    [self.parentParseableObject parser:parser parseErrorOccurred:parseError];
    
}

#pragma mark -


#pragma mark Memory management

/*
 * Clears the object data
 */
- (void) clearSafetyPayTransactionAdditionalInformationData {
    [transactionNumber_ release];
    transactionNumber_ = nil;
    
    [establishment_ release];
    establishment_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [amount_ release];
    amount_ = nil;
    
    [accountList_ release];
    accountList_ = nil;
    
    [company_ release];
    company_ = nil;
    
    [service_ release];
    service_ = nil;
    
    [alterCarrierList_ release];
    alterCarrierList_ = nil;
}
@end
