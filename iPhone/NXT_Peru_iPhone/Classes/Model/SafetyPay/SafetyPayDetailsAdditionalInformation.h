//
//  SafetyPayDetailAdditionalInformation.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"
#import "BankAccount.h"

typedef enum {
    stpdaixeas_AnalyzingSafetyPayStepTwoResponseDetail,
    stpdaixeas_AnalyzingTransactionNumber,
    stpdaixeas_AnalyzingEstablishment,
    stpdaixeas_AnalyzingCurrency,
    stpdaixeas_AnalyzingBadge,
    stpdaixeas_AnalyzingAmount,
    stpdaixeas_AnalyzingAccount,
    stpdaixeas_AnalyzingCompany,
    stpdaixeas_AnalyzingService,
    stpdaixeas_AnalyzingCoordinate,
    stpdaixeas_AnalyzingSeal,
    stpdaixeas_AnalyzingExchangeRate
} stpdaixeas_AnalyzingSafetyPayStepTwoResponseXMLElementAnalyzerSate;

@interface SafetyPayDetailsAdditionalInformation : StatusEnabledResponse {
@private
    NSString *transactionNumber_;
    NSString *establishment_;
    NSString *currency_;
    NSString *badge_;
    NSString *amount_;
    BankAccount *account_;
    NSString *company_;
    NSString *service_;
    NSString *coordinate_;
    NSString *seal_;
    NSString *exchangeRate_;
    NSError *parseError_;
}
@property (nonatomic, readonly, copy) NSString *transactionNumber;
@property (nonatomic, readonly, copy) NSString *establishment;
@property (nonatomic, readonly, copy) NSString *currency;
@property (nonatomic, readonly, copy) NSString *badge;
@property (nonatomic, readonly, copy) NSString *amount;
@property (nonatomic, readonly, copy) BankAccount *account;
@property (nonatomic, readonly, copy) NSString *company;
@property (nonatomic, readonly, copy) NSString *service;
@property (nonatomic, readonly, copy) NSString *coordinate;
@property (nonatomic, readonly, copy) NSString *seal;
@property (nonatomic, readonly, copy) NSString *exchangeRate;
@property (nonatomic, readonly, assign) NSError *parseError;
- (void) updateFrom :(SafetyPayDetailsAdditionalInformation *) anAdditionalInformation ;
- (void) removeData;
@end
