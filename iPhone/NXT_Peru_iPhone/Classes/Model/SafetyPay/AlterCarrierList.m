//
//  AlterCarrierList.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 21/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "AlterCarrierList.h"
#import "AlterCarrier.h"

#pragma mark -

@interface AlterCarrierList(private)

- (void) clearCarrierListData;

@end

@implementation AlterCarrierList

@dynamic alterCarrierList;

- (void) dealloc {
    
    [self clearCarrierListData];
    
    [alterCarrierList_ release];
    alterCarrierList_ = nil;
    
    [super dealloc];
}

#pragma mark -

#pragma mark Initialization

- (id) init {
    
    if(self = [super init]) {
        alterCarrierList_ = [[NSMutableArray alloc] init];
        informationUpdated_ = soie_NoInfo;
        self.notificationToPost = kNotificationAlterCarrierListReceived;
    }
    return self;
}

#pragma mark -
#pragma mark Properties selectors

- (NSArray *) alterCarrierList {
    return [NSArray arrayWithArray: alterCarrierList_];
}

- (AlterCarrier *) carrierAtPosition:(NSUInteger)aPosition {
    AlterCarrier *result = nil;
    if(aPosition < [alterCarrierList_ count]){
        result = [alterCarrierList_ objectAtIndex:aPosition];
    }
    return result;
}

/**
 * Provides read access to the number of Carrier stored
 *
 * @return The count
 */
- (NSUInteger) alterCarrierCount {
	return [alterCarrierList_ count];
}


- (void) updateFrom:(AlterCarrierList *)anAlterCarrierList {
    NSArray *carrierList = anAlterCarrierList.alterCarrierList;
    [self clearCarrierListData];
    
    if(carrierList != nil) {
        for (AlterCarrier *carrier in carrierList) {
            [alterCarrierList_ addObject:carrier];
        }
    }
    [self updateFromStatusEnabledResponse: anAlterCarrierList];
}

- (void) parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearCarrierListData];
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if (alterCarrierAux_ != nil) {
        [alterCarrierList_ addObject: alterCarrierAux_];
        [alterCarrierAux_ release];
        alterCarrierAux_ = nil;
    }
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing){
        
        NSString *lname = [elementName lowercaseString];
        
        if([lname isEqualToString: @"e"]){
            [alterCarrierAux_ release];
            alterCarrierAux_ = [[AlterCarrier alloc] init];
            [alterCarrierAux_ setParentParseableObject:self];
            alterCarrierAux_.openingTag = lname;
            [parser setDelegate:alterCarrierAux_];
            [alterCarrierAux_ parserDidStartDocument:parser];
        } else {
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
    }
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if(alterCarrierAux_ != nil){
        [alterCarrierList_ addObject:alterCarrierAux_];
        [alterCarrierAux_ release];
        alterCarrierAux_ = nil;
    }
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        informationUpdated_ = soie_InfoAvailable;
    }

    xmlAnalysisCurrentValue_ = serxeas_Nothing;
}

- (NSMutableArray *)stringCarrierlist{
    
    NSMutableArray *stringCarrierList = [NSMutableArray array];
    
    for (AlterCarrier *carrier in alterCarrierList_) {
        [stringCarrierList addObject:carrier.description];
    }
    
    return stringCarrierList;
}

- (void) removeData {
    [self clearCarrierListData];
}

- (void) clearCarrierListData {
    [alterCarrierList_ removeAllObjects];
    [alterCarrierAux_ release];
    alterCarrierAux_ = nil;
}
@end
