//
//  AlterCarrierList.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 21/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
#import "AlterCarrier.h"

@interface AlterCarrierList : StatusEnabledResponse
{
@private
    NSMutableArray *alterCarrierList_;
    AlterCarrier *alterCarrierAux_;
}
/**
 * Provides read-only access to the carrier count
 */
@property (nonatomic, readonly, assign) NSUInteger alterCarrierCount;

@property (nonatomic, readonly, copy) NSArray *alterCarrierList;


- (NSMutableArray *)stringCarrierlist;

- (void) updateFrom: (AlterCarrierList *) anAlterCarrierList;
- (AlterCarrier *) carrierAtPosition: (NSUInteger) aPosition;

- (void) removeData;

@end
