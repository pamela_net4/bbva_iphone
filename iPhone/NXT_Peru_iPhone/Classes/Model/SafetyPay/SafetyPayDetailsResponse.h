//
//  SafetyPayDetailsResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"
#import "SafetyPayDetailsAdditionalInformation.h"

typedef enum {
    stpdxeas_AnalyzingSafetyPayStatusResponseDetail,
    stpdxeas_AnalyzingAdditionalInformation,
} stpdxeas_AnalyzingSafetyPayStatusResponseXMLElementAnalyzerState;

@interface SafetyPayDetailsResponse : StatusEnabledResponse
{
@private
    SafetyPayDetailsAdditionalInformation *additionalInformation_;
    NSError *parseError_;
}
@property (nonatomic, readonly, retain) SafetyPayDetailsAdditionalInformation *additionalInformation;
@property (nonatomic, readonly, assign) NSError *parseError;
@end
