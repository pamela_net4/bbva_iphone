//
//  AlterCarrier.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 21/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"

@interface AlterCarrier : StatusEnabledResponse
{
@private
    NSString *code_;
    NSString *description_;
}

@property (nonatomic, readonly, copy) NSString *code;
@property (nonatomic, readonly, copy) NSString *description;
- (void) clearAlterCarrierData;

@end
