//
//  SafetyPayStatusAdditionalInformation.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"

typedef enum {
    stpaixeas_AnalyzingSafetyPayStatusAdditionalInformationResponseDetail,
    stpaixeas_AnalyzingStatus,
    stpaixeas_AnalyzingMessage
}stpaixeas_AnalyzingSafetyPayStepOneResponseXMLElementAnalyzerState;


@interface SafetyPayStatusAdditionalInformation : StatusEnabledResponse
{
@private
    NSString *activeStatus_;
    NSString *message_;
    NSError *parseError_;
}
@property (nonatomic, readonly, assign) NSError *parseError;
@property (nonatomic, readonly, copy) NSString *activeStatus;
@property (nonatomic, readonly, copy) NSString *message;
- (void) updateFrom : (SafetyPayStatusAdditionalInformation *) aAdditionalInformation;
- (void) removeData;

@end
