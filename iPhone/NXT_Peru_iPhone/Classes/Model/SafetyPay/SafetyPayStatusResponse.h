//
//  SafetyPayStatusResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 24/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"
#import "SafetyPayStatusAdditionalInformation.h"

typedef enum {
    stpsxeas_AnalyzingSafetyPayStatusResponseDetail,
    stpsxeas_AnalyzingAdditionalInformation,
} stpsxeas_AnalyzingSafetyPayStatusResponseXMLElementAnalyzerState;

@interface SafetyPayStatusResponse : StatusEnabledResponse {
@private
    SafetyPayStatusAdditionalInformation *additionalInformation_;
    NSError *parseError_;
}
@property (nonatomic, readonly, retain) SafetyPayStatusAdditionalInformation *additionalInformation;
@property (nonatomic, readonly, retain) NSError *parseError;
@end
