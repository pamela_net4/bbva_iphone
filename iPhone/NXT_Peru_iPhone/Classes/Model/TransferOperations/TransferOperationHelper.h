/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import <Foundation/Foundation.h>
#import "TransfersConstants.h"

@class StatusEnabledResponse;

/**
 * Base class to represent a transfer operation to help view controllers to display the 
 * information and perform the operations.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferOperationHelper : NSObject {
@private
    
    /**
     * Selected amount string
     */
    NSString *amountString_;
    
    /**
     * Selected currency
     */
    NSString *currency_;
    
    /**
     * Selected reference
     */
    NSString *reference_;
    
    /**
     * Second factor key: user selected coordinates or OTP key received from sms
     */
    NSString *secondFactorKey_;

    /**
     * Send email flag
     */
    BOOL sendSMS_;
    
    /**
     * ITF selected
     */
    BOOL itfSelected_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationSMS1_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationSMS2_;
    
    /**
     * Show SMS 1 flag
     */
    BOOL showSMS1_;
    
    /**
     * Show SMS 2 flag
     */
    BOOL showSMS2_;
    
    /**
     * Send email flag
     */
    BOOL sendEmail_;
    
    /*
     *selected carrier one index
     */
    NSInteger selectedCarrier1Index_;
    
    /*
     *selected carrier two index
     */
    NSInteger selectedCarrier2Index_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationEmail1_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationEmail2_;
    
    /**
     * Show email 1 flag
     */
    BOOL showEmail1_;
    
    /**
     * Show email 2 flag
     */
    BOOL showEmail2_;
    
    /**
     * User selected email message
     */
    NSString *emailMessage_;
    
    /**
     * Legal terms accepted flag
     */
    BOOL legalTermsAccepted_;
    
    /*
     *the carrier list
     */
    NSArray *carrierList_;
    
}

/**
 * Provides read-only access to the transfer operation type
 */
@property (nonatomic, readonly, assign) TransferTypeEnum transferOperationType;

/**
 * Provides read-only access to the localized transfer operation type string. Default value is an empty string
 */
@property (nonatomic, readonly, copy) NSString *localizedTransferOperationTypeString;

/**
 * Provides read-only access to the localized terms and conditions string. Default value is an empty string
 */
@property (nonatomic, readonly, copy) NSString *localizedTermsAndConditionsString;

/**
 * Provides read-only access to the confirmation message. Default value is an empty string
 */
@property (nonatomic, readonly, copy) NSString *confirmationMessage;


/**
 * Provides read-only access to the confirmation message. Default value is an empty string
 */
@property (nonatomic, readonly, copy) NSString *notificationMessage;
/**
 * Provides read-only access to the coordinateKeyTitle string. Default value is an empty string
 */
@property (nonatomic, readonly, copy) NSString *coordinateKeyTitle;

/**
 * Provides read-only access to the sealText string. Default value is an empty string
 */
@property (nonatomic, readonly, copy) NSString *sealText;

/**
 * Provides read-only access to the legal Terms URL. Default value is an empty string
 */
@property (nonatomic, readonly, copy) NSString *legalTermsURL;

/**
 * Provides read-write access to the selected amount string
 */
@property (nonatomic, readwrite, copy) NSString *amountString;

/**
 * Provides read-write access to the selected currency
 */
@property (nonatomic, readwrite, copy) NSString *currency;

/**
 * Provides read-write access to the selected reference
 */
@property (nonatomic, readwrite, copy) NSString *reference;

/**
 * Provides read-only access to the second factor key. Default value is an empty string
 */
@property (nonatomic, readwrite, copy) NSString *secondFactorKey;

/**
 * Provides read-write access to the user selected destinationSMS1 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationSMS1;

/**
 * Provides read-write access to the user selected destinationSMSOperator1 address
 */
//@property (nonatomic, readwrite, assign) NSInteger destinationSMSOperator1;

/**
 * Provides read-write access to the user selected destinationSMS2 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationSMS2;

/**
 * Provides read-write access to the user selected destinationSMSOperator2 address
 */
//@property (nonatomic, readwrite, assign) NSInteger destinationSMSOperator2;

/**
 * Provides read-write access to the user selected showSMS1 
 */
@property (nonatomic, readwrite, assign) BOOL showSMS1;

/**
 * Provides read-write access to the user selected showSMS2 
 */
@property (nonatomic, readwrite, assign) BOOL showSMS2;

/**
 * Provides read-write access to the send email flag
 */
@property (nonatomic, readwrite, assign) BOOL sendEmail;

/**
 * Provides read-write access to the send sms flag
 */
@property (nonatomic, readwrite, assign) BOOL sendSMS;

/**
 * Provides read-write access to the selected carrier one index
 */
@property (nonatomic, readwrite, assign) NSInteger selectedCarrier1Index;

/**
 * Provides read-write access to the selected carrier two index
 */
@property (nonatomic, readwrite,assign) NSInteger selectedCarrier2Index;

/**
 * Provides read-write access to the user selected destinationEmail1 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationEmail1;

/**
 * Provides read-write access to the user selected destinationEmail2 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationEmail2;

/**
 * Provides read-write access to the user selected showEmail1 
 */
@property (nonatomic, readwrite, assign) BOOL showEmail1;

/**
 * Provides read-write access to the user selected showEmail2 
 */
@property (nonatomic, readwrite, assign) BOOL showEmail2;

/**
 * Provides read-write access to the user selected email message
 */
@property (nonatomic, readwrite, copy) NSString *emailMessage;

/**
 * Provides read-write access to the user selected email message
 */
@property (nonatomic, readonly, copy) NSString *itfMessageConfirm;


/**
 * Provides read-write access to the user selected email message
 */
@property (nonatomic, readonly, copy) NSString *itfMessageSuccess;


/**
 * Provides read-write access to the user selected legalTermsAccepted 
 */
@property (nonatomic, readwrite, assign) BOOL legalTermsAccepted;

/**
 * Provides read-only access to the can send email flag. Default value is NO
 */
@property (nonatomic, readonly, assign) BOOL canSendEmailandSMS;

/**
 * Provides read-only access to the can show legal terms flag. Default value is NO
 */
@property (nonatomic, readonly, assign) BOOL canShowLegalTerms;

/**
 * Provides read-only access to the can show itf flag. Default value is NO
 */
@property (nonatomic, readonly, assign) BOOL canShowITF;

/**
 * Provides readwrite access to the itfSelected
 */
@property (nonatomic, readwrite, assign) BOOL itfSelected;

/*
 *Provides readwrite access to the carrier list
 */
 @property (nonatomic,readwrite,retain) NSArray *carrierList;

/**
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 *
 * @return The array containing the transfer second step. All objects are TitleAndAttributes instances
 */
- (NSArray *)transferSecondStepInformation;

/**
 * Creates the title and attributes array to display in the transfer third step. Default implementation returns an empty array
 *
 * @return The array containing the transfer third step. All objects are TitleAndAttributes instances
 */
- (NSArray *)transferThirdStepInformation;

/**
 * Checks whether the user can start the operation. For instance, a user cannot make a transfer between his/her accounts when only one account is available.
 * Default implementation always return NO
 *
 * @return YES when the user can start the operation, NO otherwise
 */
- (BOOL)canStartOperation;

- (BOOL)canOmitOTP;
/**
 * Checks whether the information is valid to start the transfer confirmation request. When it is not valid, an error message, that must be displayed
 * to the user is returned, with the reason that made it not possible to start the operation. Othwewise, the returned value is empty or nil. Default
 * implementation returns nil
 *
 * @return The message to display to the user telling the reason for not starting the operation
 */
- (NSString *)transferConfirmationValidationErrorMessage;

/**
 * Performs the transfer confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 *
 * @return YES when the request can be started, NO otherwise
 */
- (BOOL)startTransferConfirmationRequest;

/**
 * Notifies the transfer operation helper the confirmation response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param confirmationResponse The confirmation response received from the server
 * @return YES when the confirmation response is correct, NO otherwise
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse;

/**
 * Checks whether the information is valid to start the transfer request. When it is not valid, an error message, that must be displayed
 * to the user is returned, with the reason that made it not possible to start the operation. Othwewise, the returned value is empty or nil.
 * Stored information is analyzed
 *
 * @return The message to display to the user telling the reason for not starting the operation
 */
- (NSString *)transferValidationErrorMessage;

/**
 * Performs the transfer request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 *
 * @return YES when the request can be started, NO otherwise
 */
- (BOOL)startTransferRequest;

/**
 * Performs the transfer request to the server. Returns the frequent operation status. Base class always returns NO, because no operation is
 * performed
 *
 * @return YES when the request can be started, NO otherwise
 */
- (BOOL)startFrequentOperationReactiveRequest;
/*
 * Returns the error message for the invalid data. If @"" the data is correct
 *
 * @return if the data is valid returns nil
 */
- (NSString *)startTransferRequestDataValidation;

/**
 * Notifies the transfer operation helper the transfer response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param transferResponse The transfer response received from the server
 * @return YES when the confirmation response is correct, NO otherwise
 */
- (BOOL)transferResponseReceived:(StatusEnabledResponse *)transferResponse;

/**
 * Tells if the user wants to send a notification message.
 *
 * @return YES, if any of the notification fields is active to send a message
 */
- (BOOL)isSendingNotification;

@end
