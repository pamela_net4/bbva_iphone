//
//  TransferToGiftCard.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/31/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "TransferOperationHelper.h"

//Forward declarations
@class FOCard;
@class Account;
@class BankAccount;
@class CheckComboCell;
@class GiftCardStepOneResponse;
@class GiftCardStepTwoResponse;
@class GiftCardStepThreeResponse;
@class TransferSuccessAdditionalInformation;
@class NXTCurrencyTextField;

/**
 * Base class to represents a transfer Using a gift card
 *
 * @author <a href="http://www.mdp.com.pe">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferToGiftCard : TransferOperationHelper{
@private

    /**
     * Array with the origin selectable accounts. All objects are PaymentElement instances
     */
    NSMutableArray *originAccountList_;
    
    /**
     * Array with the origin selectable cards. All objects are PaymentElement instances
     */
    NSMutableArray *originCardList_;

    /**
     * Selected origin account index. When out of bounds, no origin account is selected
     */
    NSInteger selectedOriginAccountIndex_;
    
    /**
     * Selected origin card index. When out of bounds, no origin account is selected
     */
    NSInteger selectedOriginCardIndex_;

    /**
     * First text field
     */
    NSString *firstText_;
    
    /**
     * Second text field
     */
    NSString *secondText_;
    
    /**
     * Third text field
     */
    NSString *thirdText_;
    
    /**
     * Fourth text field
     */
    NSString *fourthText_;

    /**
     * Array with the selectable currencies.
     */
    NSArray * currencyList_;

    /**
     * Server currencies list.
     */
    NSArray *serverCurrencyList_;

    /*
     *Selected currency index. When out of bounds, no currency is selected
     */
    NSInteger selectedCurrencyIndex_;

    /**
     * Coordinate reference
     */
    NSString *coordinateReference_;

    /**
     * Seal reference
     */
    NSString *sealReference_;

    /**
     * Transfer Confirmation Additional Information
     */
    GiftCardStepOneResponse *startUpResponseInformation_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    GiftCardStepTwoResponse *additionalInformation_;

    /**
     * Transfer Success Additional Information
     */
    GiftCardStepThreeResponse *transferSuccessAdditionalInfo_;

    /**
     * Amount text field
     */
    NXTCurrencyTextField *amountTextField_;
    
    /**
     * Indicator for the card list
     */
    BOOL hasCard_;
    
}

/**
 * Provides read-only access to the array with the origin selectable accounts
 */
@property (nonatomic, readonly, retain) NSArray *originAccountList;

/**
 * Provides read-only access to the array with the origin selectable cards
 */
@property (nonatomic, readonly, retain) NSArray *originCardList;

/**
 * Provides read-write access to the selected origin account index
 */
@property (nonatomic, readwrite, assign) NSInteger selectedOriginAccountIndex;

/**
 * Provides read-write access to the selected origin card index
 */
@property (nonatomic, readwrite, assign) NSInteger selectedOriginCardIndex;

/**
 * Provides read-only access to the selected origin account
 */
@property (nonatomic, readonly, retain) BankAccount *selectedOriginAccount;

/**
 * Provides read-write access to the firstTextField and exports it to the IB
 */
@property (nonatomic, readwrite, copy) NSString *firstText;

/**
 * Provides read-write access to the secondTextField and exports it to the IB
 */
@property (nonatomic, readwrite, copy) NSString *secondText;

/**
 * Provides read-write access to the thirdTextField and exports it to the IB
 */
@property (nonatomic, readwrite, copy) NSString *thirdText;

/**
 * Provides read-write access to the fourthTextField and exports it to the IB
 */
@property (nonatomic, readwrite, copy) NSString *fourthText;

/**
 * Provides read-only access to the array with the selectable currencies
 */
@property(nonatomic,readwrite,retain) NSArray *currencyList;

/**
 * Provides read-write access to the selected currency index
 */
@property(nonatomic,readwrite,assign) NSInteger selectedCurrencyIndex;

/**
 * Provides read-only access to the selected currency
 */
@property (nonatomic,readonly,copy) NSString *selectedCurrency;

/**
 * Provides readwrite access to the currencyTextField. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTCurrencyTextField *amountTextField;

/**
 * Provides readwrite access to the currencyTextField. Exported to IB
 */
@property (nonatomic, readwrite, retain) FOCard *selectedCard;

/**
 * Provides readwrite access to the currencyTextField. Exported to IB
 */
@property (nonatomic, readwrite, retain) Account *selectedAccount;

/**
 * Provides readwrite access to the currencyTextField. Exported to IB
 */
@property (nonatomic, readwrite) BOOL hasCard;

/**
 * Designated initialized. Initializes a TransferBetweenUserAccounts instance with the initial transfer accounts response
 *
 * @param transferStartupResponse The transfer accounts response
 * @return The initialized TransferBetweenUserAccounts instance;
 */
- (id)initWithTransferStartupResponse:(GiftCardStepOneResponse *)giftCardStepOneResponse;

/**
 * String array with the cards description
 */
-(NSMutableArray*) cardListString;
/**
 * String array with the accounts descriptions
 */
-(NSMutableArray *)accountListString;

@end
