//
//  ThirdAccountOperator.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/27/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "GlobalAdditionalInformation.h"
#import "ThirdAccountOperator.h"
#import "ThirdAccount.h"
#import "ThirdAccountConfirmationAdditionalInformation.h"
#import "ThirdAccountDetailsAdditionalInformation.h"
#import "RegistrationThirdAccountConfirmationResponse.h"
#import "RegistrationThirdAccountSuccessResponse.h"
#import "NicknameValidationResponse.h"

#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "Updater.h"

@implementation ThirdAccountOperator

@synthesize accountNumber = accountNumber_;
@synthesize operationType =operationType_;
@synthesize nickname = nickname_;
@synthesize cccNumber = cccNumber_;
@synthesize entityNumber = entityNumber_;
@synthesize officeNumber = officeNumber_;
@synthesize deleteFlag = deleteFlag_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    [accountNumber_ release];
    accountNumber_ = nil;
    
    [operationType_ release];
    operationType_ = nil;
    
    [nickname_ release];
    nickname_ = nil;
    
    [additionalInformation_ release];
    additionalInformation_ = nil;
    
    [thirdAccountSuccessAdditionalInfo_ release];
    thirdAccountSuccessAdditionalInfo_ = nil;
    
    [super dealloc];
}

#pragma mark -
#pragma mark Instance initializarion

/**
 * Superclass designated initializer. Initializes an empty TransferBetweenUserAccounts instance
 *
 * @return An empty TransferBetweenUserAccounts instance
 */
- (id)init {
    
    return [self initWithTransferStartupResponse:nil];
    
}


- (id)initWithTransferStartupResponse:(ThirdAccount *)transferStartupResponse{
    if ((self = [super init])) {
        
        accountNumber_ = transferStartupResponse.accountNumber;
        operationType_ = transferStartupResponse.operationTypeId;
        nickname_ = transferStartupResponse.nickName;
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Server operations management

-(BOOL)startNicknameValidation{
    BOOL result = NO;
    
    if([accountNumber_ length]>0 && [nickname_ length]>0 && [operationType_ length]>0){
        
        [[Updater getInstance] obtainValidationThirdAccountFromNickName:nickname_];
        
        result = YES;
    }
    
    return result;

}

/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startTransferRequest {
    BOOL result = NO;
    
    if ([operationType_ length]==0) {
        [Tools showAlertWithMessage:@"Seleccione una opción"];
        return  result;
    }
    
    if([accountNumber_ length]>0 && [nickname_ length]>0 && [operationType_ length]>0){
        
        if([operationType_ isEqualToString:THIRD_ACCOUNT_ID]){
            [[Updater  getInstance] obtainRegisterThirdAccountFromAccountNumber:accountNumber_ officeNumber:officeNumber_ accountType:operationType_ nickName:nickname_];
        }else{
            [[Updater  getInstance] obtainRegisterOtherBankFromAccountNumber:accountNumber_ entityNumber:entityNumber_ officeNumber:officeNumber_ ccNumber:cccNumber_ accountType:operationType_ nickName:nickname_];
             }
    
        result = YES;
    }
    
    return result;
}

/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startDeleteRequest {
    BOOL result = NO;
    
    if([accountNumber_ length]>0 && [nickname_ length]>0 && [operationType_ length]>0){
        
         NSArray *arrAccount = [accountNumber_ componentsSeparatedByString:@"-"];
        if([operationType_ isEqualToString:THIRD_ACCOUNT_ID]){
       
        [[Updater  getInstance] obtainRegisterThirdAccountFromAccountNumber:[arrAccount objectAtIndex:2] officeNumber:[arrAccount objectAtIndex:1] accountType:operationType_ nickName:nickname_ ];
        }
        else
        {
            [[Updater  getInstance] obtainRegisterOtherBankFromAccountNumber:[arrAccount objectAtIndex:2] entityNumber:[arrAccount objectAtIndex:0] officeNumber:[arrAccount objectAtIndex:1] ccNumber:[arrAccount objectAtIndex:3] accountType:operationType_ nickName:nickname_];

        }

        result = YES;
    }
    
    return result;
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startTransferRequestDataValidation{
    NSString *result =nil;
    if([nickname_ length] ==0)
    {
        result = NSLocalizedString(@"Ingrese un Alias.", nil);
        
    }
    else if([nickname_ length]<2 || [nickname_ length]>30){
        
        result = NSLocalizedString(@"Ingrese mínimo 2 y máximo 30 caracteres para el Alias.", nil);
        
    }
    else if([operationType_ length]==0)
    {
        result = NSLocalizedString(@"Seleccione un Tipo de Operación.", nil);
    }
    else if (([entityNumber_ length] == 0)  && [operationType_ isEqualToString:OTHER_BANK_ACCOUNT_ID]) {
        
        result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_ENTITY_TEXT_KEY, nil);
        
    }
    else if (([officeNumber_ length] == 0)) {
        
        result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_OFFICE_TEXT_KEY, nil);
        
    } else if (([accountNumber_ length] == 0)) {
        
        result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_ACCNUMBER_TEXT_KEY, nil);
        
    } else if (([officeNumber_ length] != 4) && [operationType_ isEqualToString:THIRD_ACCOUNT_ID]) {
        
        result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_OFFICE_TEXT_KEY, nil);
        
    } else if (([accountNumber_ length] != 10) && [operationType_ isEqualToString:THIRD_ACCOUNT_ID]) {
        
        result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_ACCNUMBER_TEXT_KEY, nil);
        
    }else if (([cccNumber_ length] == 0) && [operationType_ isEqualToString:OTHER_BANK_ACCOUNT_ID]) {
        
        result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_CC_TEXT_KEY, nil);
        
    }
    else if([accountNumber_ length]==0)
    {
         result = NSLocalizedString(@"Ingrese una cuenta de abono.", nil);
    }
    else{
        result = [super startTransferRequestDataValidation];
    }
    
    return result;
    
        
}


/*
 * Performs the payment confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (BOOL)startTransferConfirmationRequest {
    
    BOOL canStartProcess = YES;
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
    if ((otpUsage == otp_UsageOTP) && ([[self secondFactorKey] length] == 0)) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_OTP_KEY_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    } else if ((otpUsage == otp_UsageTC) && (([[self secondFactorKey] length] == 0) || ([[self secondFactorKey] length] < 3))) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_COORDINATES_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    }
    
    if (canStartProcess) {
        
        if(deleteFlag_){
            [[Updater getInstance] obtainDeleteThirdAccountFromAccountType:operationType_ nickName:nickname_ andSecondFactorKey:[self secondFactorKey]];
        
        }else{
            [[Updater getInstance] confirmOperationAccountFromSecondKeyFactor:[self secondFactorKey]];
        }
        
    }
    
    return canStartProcess;

}

/*
 * Notifies the transfer operation helper the confirmation response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        
        if ([confirmationResponse isKindOfClass:[RegistrationThirdAccountSuccessResponse class]]) {
            
            RegistrationThirdAccountSuccessResponse *response = (RegistrationThirdAccountSuccessResponse *)confirmationResponse;
            
            if (thirdAccountSuccessAdditionalInfo_ != nil) {
                [thirdAccountSuccessAdditionalInfo_ release];
                thirdAccountSuccessAdditionalInfo_ = nil;
            }
            thirdAccountSuccessAdditionalInfo_ = [response.additionalInformation retain];
            
            result = YES;
            
        }
        
    }
    
    return result;
    
}

/*
 * Notifies the transfer operation helper the tranfer response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)transferResponseReceived:(StatusEnabledResponse *)transferResponse {
    
    BOOL result = [super transferResponseReceived:transferResponse];
    
    if (!result) {
        
        if ([transferResponse isKindOfClass:[RegistrationThirdAccountConfirmationResponse class]]) {
            
            RegistrationThirdAccountConfirmationResponse *response = (RegistrationThirdAccountConfirmationResponse *)transferResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response.additionalInformation retain];
            
            result = YES;
            
        }
        
    }
    
    return result;
    
}
/*
 * Notifies the transfer operation helper the tranfer response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)validationNicknameResponseReceived:(StatusEnabledResponse *)transferResponse withDelegate:(id)delegate{
    
    BOOL result = [super transferResponseReceived:transferResponse];
    if (!result) {
        
        if ([transferResponse isKindOfClass:[NicknameValidationResponse class]]) {
            
            NicknameValidationResponse *response = (NicknameValidationResponse *)transferResponse;
            
            if([response.additionalInformation.indicatorNickname isEqualToString:@"S"])
            {
                //[Tools showInfoWithMessage:response.additionalInformation.messageValidation];
                [Tools showErrorWithMessage:response.additionalInformation.messageValidation andDelegate:delegate];
                result = NO;
            }
            else
            {
                result = YES;
            }
            
        }
    }
    return result;
}
#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)transferSecondStepInformation {
    
    
    NSMutableArray *mutableResult = [NSMutableArray array];
     TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];

    titleAndAttributes.titleString = NSLocalizedString(@"Operación",nil );
    [titleAndAttributes addAttribute:additionalInformation_.operationTitle];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(@"Alias",nil );
    [titleAndAttributes addAttribute:additionalInformation_.accountDetail.nickName];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(@"Tipo de operación",nil );
    [titleAndAttributes addAttribute:additionalInformation_.accountDetail.operationType];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(@"Cuenta de abono",nil );
    [titleAndAttributes addAttribute:additionalInformation_.accountDetail.accountNumber];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;

}

/*
 * Creates the title and attributes array to display in the transfer third step. Default implementation returns an empty array
 */
- (NSArray *)transferThirdStepInformation {
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    titleAndAttributes.titleString = NSLocalizedString(@"Operación",nil );
    [titleAndAttributes addAttribute:thirdAccountSuccessAdditionalInfo_.operationTitle];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(@"Alias",nil );
    [titleAndAttributes addAttribute:thirdAccountSuccessAdditionalInfo_.accountDetail.nickName];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(@"Tipo de operación",nil );
    [titleAndAttributes addAttribute:thirdAccountSuccessAdditionalInfo_.accountDetail.operationType];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(@"Cuenta de abono",nil );
    [titleAndAttributes addAttribute:thirdAccountSuccessAdditionalInfo_.accountDetail.accountNumber];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(@"Fecha y Hora",nil );
    [titleAndAttributes addAttribute:thirdAccountSuccessAdditionalInfo_.accountDetail.operationDate];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    return mutableResult;
}

/*
 * Returns the coordinateKeyTitle string. Base class returns an empry string
 *
 * @return The coordinateKeyTitle string
 */
- (NSString *)coordinateKeyTitle {
    
    return additionalInformation_.coordinate;
    
}
/*
 * Returns the seal string. Base class returns an empry string
 *
 * @return The seal string
 */
- (NSString *)sealText {
    
    return additionalInformation_.seal;
    
}
/**
 * Returns the transfer between user accounts operation type
 *
 * @return The transfer between user accounts operation type
 */
- (TransferTypeEnum)transferOperationType {
    
    return TTEThirdTransferAccount;
    
}
/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedTransferOperationTypeString {
    
    if([operationType_ isEqualToString:THIRD_ACCOUNT_ID])
    {
        return NSLocalizedString(@"Cuenta de terceros", nil);
    }
    else{
        return NSLocalizedString(@"Cuenta de otros bancos", nil);
    }
    
}

/*
 * Returns the confirmation message. The transfer between accounts localized string is returned
 *
 * @return The confirmation message
 */
- (NSString *)confirmationMessage {
    
    NSString *message = @"";
    
    if([thirdAccountSuccessAdditionalInfo_.waitNumber  length]>0)
    {
        
        message = [NSString stringWithFormat:@"%@",[thirdAccountSuccessAdditionalInfo_.waitNumber stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"]];
    
    }
    return message;
    
}

/*
 * Tells if the user wants to send a notification message.
 *
 * @return YES, if any of the notification fields is active to send a message
 */
- (BOOL)isSendingNotification {
    
    return [thirdAccountSuccessAdditionalInfo_.waitNumber  length]>0;
}
@end
