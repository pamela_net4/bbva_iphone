//
//  TransferDetailCashMobile.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 10/7/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "TransferOperationHelper.h"

@class TransferShowDetailAddtionalInformation;
@class TransferSuccessResponse;
@interface TransferDetailCashMobile : TransferOperationHelper{
    
    
    /**
     * Transfer Confirmation Additional Information
     */
    TransferShowDetailAddtionalInformation *transferSuccessAdditionalInfo_;
}

@property(nonatomic, retain) TransferShowDetailAddtionalInformation *transferSuccessAdditionalInfo;

/**
 * Designated initialized. Initializes a TransferBetweenUserAccounts instance with the initial transfer accounts response
 *
 * @param transferStartupResponse The transfer accounts response
 * @return The initialized TransferBetweenUserAccounts instance;
 */
- (id)initWithTransferStartupResponse:(TransferShowDetailAddtionalInformation *)transferStartupResponse;

/*
 * show the result of the resend operation
 */
- (void)showResultMessage:(TransferSuccessResponse *)response;

@end
