/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "TransferOperationHelper.h"



//Forward declarations
@class BankAccount;
@class TransferStartupResponse;
@class TransferTypePaymentResponse;
@class TransferConfirmationAdditionalInformation;
@class TransferSuccessAdditionalInformation;
@class NXTCurrencyTextField;

/**
 * Base class to represents a transfer between accounts operation
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferToOtherBankAccounts : TransferOperationHelper {
    
@private
    
    /**
     * Array with the origin selectable accounts. All objects are PaymentElement instances
     */
    NSMutableArray *originAccountList_;
    
    /**
     * Selected origin account index. When out of bounds, no origin account is selected
     */
    NSInteger selectedOriginAccountIndex_;

    /**
     * Selected destination account entity
     */
    NSString *selectedDestinationAccountEntity_;
    
    /**
     * Selected destination account office
     */
    NSString *selectedDestinationAccountOffice_;
    
    /**
     * Selected destination account accNumber
     */
    NSString *selectedDestinationAccountAccNumber_;
    
    /**
     * Selected destination account cc
     */
    NSString *selectedDestinationAccountCc_;
    
    /*
     * Array with the selectable currencies. 
     */
    NSArray *currencyList_;
    
    /**
     * Server currencies list. 
     */
    NSArray *serverCurrencyList_;
    
    /*
     *Selected currency index. When out of bounds, no currency is selected
     */
    NSInteger selectedCurrencyIndex_;
    
    /**
     * Array with the document type list
     */
    NSArray *documentTypeList_;
    
    /**
     * Selected document type index
     */
    NSInteger selectedDocumentTypeIndex_;
    
    /**
     * Selected document type index
     */
    NSInteger selectedTransferTypeIndex_;

    /**
     * Document number
     */
    NSString *documentNumber_;
    
    /**
     * Beneficiary
     */
    NSString *beneficiary_;

    TransferTypePaymentResponse *typePaymentInformation_;
    /**
     * Transfer Confirmation Additional Information
     */
    TransferConfirmationAdditionalInformation *additionalInformation_;
    
    /**
     * Transfer Success Additional Information
     */
    TransferSuccessAdditionalInformation *transferSuccessAdditionalInfo_;
    
    /**
     * Amount text field
     */
    NXTCurrencyTextField *amountTextField_;
    

}

/**
 * Provides read-only access to the array with the origin selectable accounts
 */
@property (nonatomic, readonly, retain) NSArray *originAccountList;

/**
 * Provides read-write access to the selected origin account index
 */
@property (nonatomic, readwrite, assign) NSInteger selectedOriginAccountIndex;

/**
 * Provides read-only access to the selected origin account
 */
@property (nonatomic, readonly, retain) BankAccount *selectedOriginAccount;

/**
 * Provides read-write access to the selected destination account entity
 */
@property (nonatomic, readwrite, copy) NSString *selectedDestinationAccountEntity;

/**
 * Provides read-write access to the selected destination account office
 */
@property (nonatomic, readwrite, copy) NSString *selectedDestinationAccountOffice;

/**
 * Provides read-write access to the selected destination account accNumber
 */
@property (nonatomic, readwrite, copy) NSString *selectedDestinationAccountAccNumber;

/**
 * Provides read-write access to the selected destination account cc
 */
@property (nonatomic, readwrite, copy) NSString *selectedDestinationAccountCc;

/**
 * Provides read-only access to the array with the selectable currencies
 */
@property(nonatomic,readwrite,retain) NSArray *currencyList;

/**
 * Provides read-write access to the selected currency index
 */
@property(nonatomic,readwrite,assign) NSInteger selectedCurrencyIndex;

/**
 * Provides read-only access to the selected currency
 */
@property (nonatomic,readonly,copy) NSString *selectedCurrency;

/**
 * Provides readwrite access to the amountTextField
 */
@property (nonatomic, readwrite, retain) NXTCurrencyTextField *amountTextField;

/**
 * Provides read-only access to the array of document types 
 */
@property (nonatomic, readonly, retain) NSArray *documentTypeList;

/**
 * Provides readwrite access to the selectedDocumentTypeIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedDocumentTypeIndex;

/**
 * Provides readwrite access to the selectedDocumentTypeIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedTransferTypeIndex;

/**
 * Provides readwrite access to the documentNumber
 */
@property (nonatomic, readwrite, copy) NSString *documentNumber;

/**
 * Provides readwrite access to the beneficiary
 */
@property (nonatomic, readwrite, copy) NSString *beneficiary;

/**
 * Provides readwrite access to the commissionN
 */
@property (nonatomic, readonly, copy) NSString *commissionN;

/**
 * Provides readwrite access to the commissionL
 */
@property (nonatomic, readonly, copy) NSString *commissionL;

/**
 * Provides readwrite access to the totalPaymentN
 */
@property (nonatomic, readonly, copy) NSString *totalPaymentN;

/**
 * Provides readwrite access to the totalPaymentL
 */
@property (nonatomic, readonly, copy) NSString *totalPaymentL;

/**
 * Provides readwrite access to the typeFlow
 */
@property (nonatomic, readonly, copy) NSString *typeFlow;

/**
 * Provides readwrite access to the typeFlow
 */
@property (nonatomic, readonly, copy) NSString *disclaimer;

@property(nonatomic,readonly,copy) NSString *itfMessage;

/**
 * Designated initialized. Initializes a TransferBetweenUserAccounts instance with the initial transfer accounts response
 *
 * @param transferStartupResponse The transfer accounts response
 * @return The initialized TransferBetweenUserAccounts instance;
 */
- (id)initWithTransferStartupResponse:(TransferStartupResponse *)transferStartupResponse;

-(BOOL) typeTransferStepReceived:(StatusEnabledResponse*) typeTransferResponse;

-(NSString*) startSecondTransferRequestDataValidation;

- (BOOL)startTypeTransferInmediateRequest;

- (BOOL)startTypeTransferToConfirmRequest;
@end
