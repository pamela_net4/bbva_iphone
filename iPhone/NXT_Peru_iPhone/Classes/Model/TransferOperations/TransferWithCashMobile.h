//
//  TransferWithCashMobile.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 9/25/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "TransferOperationHelper.h"

//Forward declarations
@class BankAccount;
@class TransferStartupResponse;
@class TransferConfirmationAdditionalInformation;
@class TransferSuccessAdditionalInformation;
@class NXTCurrencyTextField;

/**
 * Base class to represents a transfer between accounts operation
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferWithCashMobile : TransferOperationHelper {
    
@private
    
    /**
     * Array with the origin selectable accounts. All objects are PaymentElement instances
     */
    NSMutableArray *originAccountList_;
    
    /**
     * Selected origin account index. When out of bounds, no origin account is selected
     */
    NSInteger selectedOriginAccountIndex_;
    
    /**
     * Selected destination account office
     */
    NSString *selectedDestinationPhoneNumber_;
    
    
    /**
     * Coordinate reference
     */
    NSString *coordinateReference_;
    
    /**
     * Seal reference
     */
    NSString *sealReference_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    TransferConfirmationAdditionalInformation *additionalInformation_;
    
    /**
     * Transfer Success Additional Information
     */
    TransferSuccessAdditionalInformation *transferSuccessAdditionalInfo_;
    
    /**
     * Amount text field
     */
    NXTCurrencyTextField *amountTextField_;
    
}

/**
 * Provides read-only access to the array with the origin selectable accounts
 */
@property (nonatomic, readonly, retain) NSArray *originAccountList;

/**
 * Provides read-write access to the selected origin account index
 */
@property (nonatomic, readwrite, assign) NSInteger selectedOriginAccountIndex;

/**
 * Provides read-only access to the selected origin account
 */
@property (nonatomic, readonly, retain) BankAccount *selectedOriginAccount;

/**
 * Provides read-write access to the selected destination account office
 */
@property (nonatomic, readwrite, copy) NSString *selectedDestinationPhoneNumber;

/**
 * Provides readwrite access to the currencyTextField. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTCurrencyTextField *amountTextField;

/**
 * Designated initialized. Initializes a TransferBetweenUserAccounts instance with the initial transfer accounts response
 *
 * @param transferStartupResponse The transfer accounts response
 * @return The initialized TransferBetweenUserAccounts instance;
 */
- (id)initWithTransferStartupResponse:(TransferStartupResponse *)transferStartupResponse;
@end
