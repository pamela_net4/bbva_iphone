    //
//  TransferWithCashMobile.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 9/25/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "TransferWithCashMobile.h"

#import "AccountList.h"
#import "BankAccount.h"
#import "GlobalAdditionalInformation.h"
#import "NXTCurrencyTextField.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "TransferConfirmationAdditionalInformation.h"
#import "TransferConfirmationResponse.h"
#import "TransferStartupResponse.h"
#import "TransferSuccessResponse.h"
#import "TransferSuccessAdditionalInformation.h"
#import "Updater.h"

@interface TransferWithCashMobile()

/**
 * Checks if the destination account is in the account list
 *
 * @param destinationAccountNumber The destination account number
 */
- (BOOL)checkDestinationAccount:(NSString *)destinationAccountNumber;

@end

#pragma mark -

@implementation TransferWithCashMobile

#pragma mark -
#pragma mark Properties

@synthesize originAccountList = originAccountList_;
@synthesize selectedOriginAccountIndex = selectedOriginAccountIndex_;
@dynamic selectedOriginAccount;
@synthesize selectedDestinationPhoneNumber = selectedDestinationPhoneNumber_;
@synthesize amountTextField = amountTextField_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [originAccountList_ release];
    originAccountList_ = nil;
    
    [selectedDestinationPhoneNumber_ release];
    selectedDestinationPhoneNumber_ = nil;
    
    [additionalInformation_ release];
    additionalInformation_ = nil;
    
    [transferSuccessAdditionalInfo_ release];
    transferSuccessAdditionalInfo_ = nil;
    
    [amountTextField_ release];
    amountTextField_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initializarion

/**
 * Superclass designated initializer. Initializes an empty TransferBetweenUserAccounts instance
 *
 * @return An empty TransferBetweenUserAccounts instance
 */
- (id)init {
    
    return [self initWithTransferStartupResponse:nil];
    
}

/*
 * Designated initialized. Initializes a TransferBetweenUserAccounts instance with the initial transfer accounts response
 */
- (id)initWithTransferStartupResponse:(TransferStartupResponse *)transferStartupResponse {
    
    if ((self = [super init])) {
        
        // The initial operation in iPad returns elements no needed in iPhone
        originAccountList_ = [[NSMutableArray alloc] init];
        
        [self setCarrierList:transferStartupResponse.mobileCarriers];
    }
    
    
    
    return self;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the selected origin account index and updates the destination account list
 *
 * @param selectedOriginAccountIndex The new selected origin account index
 */
- (void)setSelectedOriginAccountIndex:(NSInteger)selectedOriginAccountIndex {
    
    Session *session = [Session getInstance];
    
    if (selectedOriginAccountIndex != selectedOriginAccountIndex_) {
        
        selectedOriginAccountIndex_ = selectedOriginAccountIndex;
        
        [originAccountList_ removeAllObjects];
        [originAccountList_ addObjectsFromArray:session.accounts];
    }
    
    if ((selectedOriginAccountIndex_ >= 0) && (selectedOriginAccountIndex_ < [originAccountList_ count])) {
        
        selectedOriginAccountIndex_ = selectedOriginAccountIndex;
        
    } else {
        
        selectedOriginAccountIndex_ = -1;
        
    }
    
}



/*
 * Returns the selected origin account
 *
 * @return The selected origin account
 */
- (BankAccount *)selectedOriginAccount {
    
    BankAccount *result = nil;
    
    if ((selectedOriginAccountIndex_ >= 0) && (selectedOriginAccountIndex_ < [originAccountList_ count])) {
        
        result = [originAccountList_ objectAtIndex:selectedOriginAccountIndex_];
        
    }
    
    return result;
    
}


#pragma mark -
#pragma mark Server operations management

/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startTransferRequest {
    
    BOOL result = NO;
    BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    NSString *originAccountNumber = [originAccount.subject stringByReplacingOccurrencesOfString:@"-" withString:@"$"];
    
    NSString *amount = [Tools formatAmountWithDotDecimalSeparator:self.amountString];
    
    NSString *currencySelected = @"SOLES";
    
    
    if (([originAccountNumber length] > 0) &&
        ([selectedDestinationPhoneNumber_ length] > 0) &&
        ([amount length] > 0) ) {
        
        [[Updater getInstance] transferWithCashMobileConfirmationFromAccountType:
                                                              originAccount.accountType
                                                                     fromCurrency:originAccount.currency
                                                                        fromIndex:originAccount.subject
                                                                        toPhone:
                                                            selectedDestinationPhoneNumber_
                                                                           amount:amount
                                                                         currency:currencySelected];
        
        result = YES;
        
    }
    
    return result;
    
}

- (BOOL)startFrequentOperationReactiveRequest
{
    [[Updater getInstance] obtainCashMobileFrequentOperationReactiveStepOne];
    return YES;
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startTransferRequestDataValidation {
	
	NSString *result = nil;
    
    NSString *amountOnlyNumbers = [self.amountString stringByReplacingOccurrencesOfString:@"." withString:@""];
    amountOnlyNumbers = [amountOnlyNumbers stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    long int amount = [amountOnlyNumbers intValue];
    
	if ([self selectedOriginAccountIndex] == NSNotFound || [self selectedOriginAccountIndex] < 0 || [self selectedOriginAccountIndex] > [[self originAccountList] count]) {
		
		result = NSLocalizedString(TRANSFER_ERROR_CASHMOBILE_ORIGIN_ACCOUNT_TEXT_KEY, nil);
		
	} else {
		
		BankAccount *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
		NSString *originAccountNumber = [originAccount subject];
        
        if (!([originAccountNumber length] > 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_CASHMOBILE_ORIGIN_ACCOUNT_TEXT_KEY, nil);
        } else{
                
                BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
                NSString *originAccountNumber = originAccount.subject;
                NSString *avaibleBalance = originAccount.availableBalanceString;
            
            avaibleBalance = [avaibleBalance stringByReplacingOccurrencesOfString:@"," withString:@""];
            
                NSString *currencySelected = @"SOLES";
                
                if (!([originAccountNumber length] > 0)) {
                    
                    result = NSLocalizedString(TRANSFER_ERROR_CASHMOBILE_ORIGIN_ACCOUNT_TEXT_KEY, nil);
                    
                } else if (!([currencySelected length] > 0)) {
                    
                    result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
                    
                } else if (([self.amountString isEqualToString:@"0.00"]) || ([self.amountString floatValue] == 0.0f)) {
                    
                    result = NSLocalizedString(TRANSFER_MUST_SELECT_AMOUNT_ERROR_TEXT_KEY, nil);
                    
                } else if (amount%20 != 0 ){
                    
                    result = NSLocalizedString(TRANSFER_MUST_BE_MULTIPLE_TWENTY_KEY, nil);
                    
                }else if (amount > [avaibleBalance floatValue]){
                    
                    result = NSLocalizedString(TRANSFER_AMOUNT_EXCEEDED_KEY, nil);
                    
                }else if ([selectedDestinationPhoneNumber_ length] == 0) {
                    
                    result = NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_EMPTY_KEY, nil);
                    
                } else if (![Tools isValidMobilePhoneNumberString:selectedDestinationPhoneNumber_]) {
                    
                    result = NSLocalizedString(TRANSFER_ERROR_PHONE_NUMBER_TEXT_KEY, nil);
                    
                } else   {
                    result = [super startTransferRequestDataValidation];
                }
            
        }
		
	}
    
    return result;
    
}


/*
 * Performs the payment confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (BOOL)startTransferConfirmationRequest {
    
    BOOL canStartProcess = YES;
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
    if (self.canShowLegalTerms && !self.legalTermsAccepted) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_LEGAL_TERMS_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    } else if ((otpUsage == otp_UsageOTP) && ([[self secondFactorKey] length] == 0)) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_OTP_KEY_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    } else if ((otpUsage == otp_UsageTC) && (([[self secondFactorKey] length] == 0) || ([[self secondFactorKey] length] < 3))) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_COORDINATES_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    }
    
    if (canStartProcess) {
        
        [[Updater getInstance] transferWithCashMobileResultFromSecondKeyFactor:[self secondFactorKey]];
        
    }
    
    return canStartProcess;
    
}

/*
 * Notifies the transfer operation helper the confirmation response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        
        if ([confirmationResponse isKindOfClass:[TransferSuccessResponse class]]) {
            
            TransferSuccessResponse *response = (TransferSuccessResponse *)confirmationResponse;
            
            if (transferSuccessAdditionalInfo_ != nil) {
                [transferSuccessAdditionalInfo_ release];
                transferSuccessAdditionalInfo_ = nil;
            }
            transferSuccessAdditionalInfo_ = [response.additionalInformation retain];
            
            result = YES;
            
        }
        
    }
    
    return result;
    
}

/*
 * Notifies the transfer operation helper the tranfer response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)transferResponseReceived:(StatusEnabledResponse *)transferResponse {
    
    BOOL result = [super transferResponseReceived:transferResponse];
    
    if (!result) {
        
        if ([transferResponse isKindOfClass:[TransferConfirmationResponse class]]) {
         
            
            TransferConfirmationResponse *response = (TransferConfirmationResponse *)transferResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response.additionalInformation retain];
            
            coordinateReference_ = [additionalInformation_ coordinate];
            sealReference_ = [additionalInformation_ seal];
            
            result = YES;
            
        }
        
    }
    
    return result;
    
}

/*
 * Checks if the destination account is in the account list
 */
- (BOOL)checkDestinationAccount:(NSString *)destinationAccountNumber {
    
    BOOL result = NO;
    
    BankAccount *account;
    NSInteger i = 0;
    NSInteger count = [self.originAccountList count];
    
    while (i < count && !result) {
        
        account = [self.originAccountList objectAtIndex:i];
        result = [[account number] isEqualToString:destinationAccountNumber];
        i++;
        
    }
    
    return result;
    
}


#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)transferSecondStepInformation {
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    BankAccount *selectedOriginAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    
    //origin
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CASH_MOBILE_ACCOUNT_CHARGED_TEXT_KEY, nil);
    
    NSString *originAccount = selectedOriginAccount.subject;
    
    if ([originAccount length] > 0) {
        
        if ([selectedOriginAccount.accountType length] > 0) {
            
            originAccount = [NSString stringWithFormat:@"%@ - %@ - %@", selectedOriginAccount.accountType, [Tools getCurrencyLiteral:selectedOriginAccount.currency] , originAccount];
            
        }
        
    } else {
        
        originAccount = selectedOriginAccount.accountType;
        
    }
    
    [titleAndAttributes addAttribute:originAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    // amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SEND_AMOUNT_TEXT_KEY, nil);
    
    NSString *currency = CURRENCY_SOLES_LITERAL;
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], self.amountString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    
    //destination
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_NUMBER_TITLE_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute:selectedDestinationPhoneNumber_];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //itf
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ACCOUNT_ITF_TEXT_KEY, nil);
    
    NSString *marketPlaceCommision = additionalInformation_.itfAmount;
    NSString *currencySymbol = additionalInformation_.itfSymbol;
    
    if (([marketPlaceCommision length] == 0) || ([marketPlaceCommision isEqualToString:@"0.00"])) {
        
        marketPlaceCommision = @"0.00";
        
    } else {
        
        if (([currencySymbol length] == 0) || ([currencySymbol isEqualToString:@"0.00"]))
            currencySymbol = [NSString stringWithFormat:@"%@", [Tools notNilString:[Tools getCurrencySimbol:selectedOriginAccount.currency]]];
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@", marketPlaceCommision]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    
    return [NSArray arrayWithArray:mutableResult];
    
}

/*
 * Creates the title and attributes array to display in the transfer third step. Default implementation returns an empty array
 */
- (NSArray *)transferThirdStepInformation {
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    BankAccount *selectedOriginAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    
    // Operation number
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CASH_MOBILE_NUMBER_OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operationNumber];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operation];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Origin
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CASH_MOBILE_ACCOUNT_CHARGED_TEXT_KEY, nil);
    
    NSString *originAccount = selectedOriginAccount.subject;
    
    if ([originAccount length] > 0) {
        
        if ([selectedOriginAccount.accountType length] > 0) {
            
            originAccount = [NSString stringWithFormat:@"%@ - %@ - %@", selectedOriginAccount.accountType, [Tools getCurrencyLiteral:selectedOriginAccount.currency], originAccount];
            
        }
        
    } else {
        
        originAccount = selectedOriginAccount.accountType;
        
    }
    
    [titleAndAttributes addAttribute:originAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFERED_SEND_IMPORT_TEXT_KEY, nil);
    
    NSString *currency = CURRENCY_SOLES_LITERAL;
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], self.amountString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Destination
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_NUMBER_TITLE_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute:selectedDestinationPhoneNumber_];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // itf
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ACCOUNT_ITF_TEXT_KEY, nil);
    
    NSString *marketPlaceCommision = additionalInformation_.itfAmount;
    NSString *currencySymbol = additionalInformation_.itfSymbol;
    
    if (([marketPlaceCommision length] == 0) || ([marketPlaceCommision isEqualToString:@"0.00"])) {
        
        marketPlaceCommision = @"0.00";
        
    } else {
        
        if (([currencySymbol length] == 0) || ([currencySymbol isEqualToString:@"0.00"]))
            currencySymbol = [NSString stringWithFormat:@"%@", [Tools notNilString:[Tools getCurrencySimbol:selectedOriginAccount.currency]]];
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@", marketPlaceCommision]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Amount charged
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CASH_MOBILE_AMOUNT_PAID_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], transferSuccessAdditionalInfo_.totalAmountToPay]];
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // State
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(STATE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.transferState];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Time
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_DATE_HOUR_SHORT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", transferSuccessAdditionalInfo_.dateString, transferSuccessAdditionalInfo_.hour]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Expiraion Time
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_EXPIRATION_DATE_HOUR_SHORT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", transferSuccessAdditionalInfo_.expirationDateString, transferSuccessAdditionalInfo_.expirationHourString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Notification Message
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = @"";
    [titleAndAttributes addAttribute: transferSuccessAdditionalInfo_.notificationMessage];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
    
}




/*
 * Returns the coordinateKeyTitle string. Base class returns an empry string
 *
 * @return The coordinateKeyTitle string
 */
- (NSString *)coordinateKeyTitle {
    
    return coordinateReference_;
    
}

/*
 * Returns the seal string. Base class returns an empry string
 *
 * @return The seal string
 */
- (NSString *)sealText {
    
    return sealReference_;
    
}

/**
 * Returns the transfer between user accounts operation type
 *
 * @return The transfer between user accounts operation type
 */
- (TransferTypeEnum)transferOperationType {
    
    return TTETransferWithCashMobile;
    
}

/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedTransferOperationTypeString {
    
    return NSLocalizedString(TRANSFER_WITH_CASH_MOBILE_SEND_TEXT_1_KEY, nil);
    
}

/*
 * Returns the localized terms and conditions string. The transfer between accounts legal terms localized string is returned
 *
 * @return The localized transfer legal terms operation type string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return NSLocalizedString(TRANSFER_LEGAL_TERMS_KEY, nil);
    
}

/*
 * Returns the confirmation message. The transfer between accounts localized string is returned
 *
 * @return The confirmation message
 */
- (NSString *)confirmationMessage {
    
    return transferSuccessAdditionalInfo_.confirmationMessage;
    
}

/*
 * Returns the can show legal terms flag
 */
- (BOOL)canShowLegalTerms {
    
    return YES;
    
}

/*
 * Returns the can show email and sms
 */
- (BOOL)canSendEmailandSMS {
    
    return YES;
    
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
#if defined(SIMULATE_HTTP_CONNECTION)
    return [Tools notNilString:@"http://www.google.es"];
#else
    return [Tools notNilString:additionalInformation_.disclaimer];
#endif
    
}

@end