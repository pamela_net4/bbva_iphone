//
//  ThirdAccountOperator.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 5/27/15.
//  Copyright (c) 2015 Movilok. All rights reserved.
//

#import "TransferOperationHelper.h"

#define THIRD_ACCOUNT_ID @"TRCTATER"
#define OTHER_BANK_ACCOUNT_ID @"TRCTAINT"

@class ThirdAccountConfirmationAdditionalInformation;
@class ThirdAccountDetailsAdditionalInformation;
@class ThirdAccount;

@interface ThirdAccountOperator : TransferOperationHelper
{

     ThirdAccountDetailsAdditionalInformation *additionalInformation_;
    
    ThirdAccountConfirmationAdditionalInformation *thirdAccountSuccessAdditionalInfo_;
    
    NSString *entityNumber_;
    
    NSString *officeNumber_;
    
    NSString *cccNumber_;
    
    NSString *accountNumber_;
    
    NSString *operationType_;
    
    NSString *nickname_;
    
    BOOL deleteFlag_;
   
}

/**
 * Provides read-only access to the account number
 */
@property (nonatomic, readwrite, retain) NSString *accountNumber;

/**
 * Provides read-only access to the office number
 */
@property (nonatomic, readwrite, retain) NSString *officeNumber;

/**
 * Provides read-only access to the entity number
 */
@property (nonatomic, readwrite, retain) NSString *entityNumber;


/**
 * Provides read-only access to the entity number
 */
@property (nonatomic, readwrite, retain) NSString *cccNumber;
/**
 * Provides read-only access to the account number
 */
@property (nonatomic, readwrite, retain) NSString *operationType;

/**
 * Provides read-only access to the account number
 */
@property (nonatomic, readwrite, retain) NSString *nickname;

/**
 * Provides read-only access to the account number
 */
@property (nonatomic, readwrite,assign) BOOL deleteFlag;

/**
 * Designated initialized. Initializes a TransferBetweenUserAccounts instance with the initial transfer accounts response
 *
 * @param transferStartupResponse The transfer accounts response
 * @return The initialized TransferBetweenUserAccounts instance;
 */
- (id)initWithTransferStartupResponse:(ThirdAccount *)transferStartupResponse;

- (BOOL)startDeleteRequest;

- (BOOL)startNicknameValidation;

- (BOOL)validationNicknameResponseReceived:(StatusEnabledResponse *)transferResponse withDelegate:(id)delegate;
@end
