/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "TransferBetweenAccounts.h"

#import "AccountList.h"
#import "BankAccount.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "TransferConfirmationAdditionalInformation.h"
#import "TransferConfirmationResponse.h"
#import "TransferStartupResponse.h"
#import "TransferSuccessResponse.h"
#import "TransferSuccessAdditionalInformation.h"
#import "Updater.h"

#pragma mark -

@implementation TransferBetweenAccounts

#pragma mark -
#pragma mark Properties

@synthesize originAccountList = originAccountList_;
@synthesize selectedOriginAccountIndex = selectedOriginAccountIndex_;
@dynamic selectedOriginAccount;
@synthesize destinationAccountList = destinationAccountList_;
@synthesize selectedDestinationAccountIndex = selectedDestinationAccountIndex_;
@dynamic selectedDestinationAccount;
@synthesize currencyList = currencyList_;
@synthesize selectedCurrencyIndex = selectedCurrencyIndex_;
@dynamic selectedCurrency;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [originAccountList_ release];
    originAccountList_ = nil;
    
    [destinationAccountList_ release];
    destinationAccountList_ = nil;
    
    [currencyList_ release];
    currencyList_ = nil;
    
    [serverCurrencyList_ release];
    serverCurrencyList_ = nil;
    
    [excludedDestinationAccount_ release];
    excludedDestinationAccount_ = nil;
    
    [additionalInformation_ release];
    additionalInformation_ = nil;
    
    [transferSuccessAdditionalInfo_ release];
    transferSuccessAdditionalInfo_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initializarion

/**
 * Superclass designated initializer. Initializes an empty TransferBetweenUserAccounts instance
 *
 * @return An empty TransferBetweenUserAccounts instance
 */
- (id)init {
    
    return [self initWithTransferStartupResponse:nil];
    
}

/*
 * Designated initialized. Initializes a TransferBetweenUserAccounts instance with the initial transfer accounts response
 */
- (id)initWithTransferStartupResponse:(TransferStartupResponse *)transferStartupResponse {
    
    if ((self = [super init])) {
        
        // The initial operation in iPad returns elements no needed in iPhone
        originAccountList_ = [[NSMutableArray alloc] init];
        destinationAccountList_ = [[NSMutableArray alloc] init];
        currencyList_ = [[NSArray alloc] initWithObjects:NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil),
                         NSLocalizedString(DOLLARS_CURRENCY_TEXT_KEY, nil),
                         nil];
        serverCurrencyList_ = [[NSArray alloc] initWithObjects:CURRENCY_SOLES_LITERAL,
                               CURRENCY_DOLARES_LITERAL,
                               nil];
        
    }
    


    return self;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the selected origin account index and updates the destination account list
 *
 * @param selectedOriginAccountIndex The new selected origin account index
 */
- (void)setSelectedOriginAccountIndex:(NSInteger)selectedOriginAccountIndex {
    
    Session *session = [Session getInstance];
    
    if (selectedOriginAccountIndex != selectedOriginAccountIndex_) {
        
        selectedOriginAccountIndex_ = selectedOriginAccountIndex;
        
        [originAccountList_ removeAllObjects];
        [originAccountList_ addObjectsFromArray:session.accountList.transferAccountList];
        
        [destinationAccountList_ removeAllObjects];
        [destinationAccountList_ addObjectsFromArray:session.accountList.transferAccountList];

        
        BankAccount *selectedDestinationAccount = self.selectedDestinationAccount;
        
        [excludedDestinationAccount_ release];
        excludedDestinationAccount_ = nil;

        if ((selectedOriginAccountIndex_ >= 0) && (selectedOriginAccountIndex_ < [originAccountList_ count])) {
            
            BankAccount *originAccount = [originAccountList_ objectAtIndex:selectedOriginAccountIndex_];
            BankAccount *destinationAccount = nil;
            BOOL excludeDestination = NO;
            
            for (destinationAccount in destinationAccountList_) {
                
                if ([destinationAccount.number isEqualToString:originAccount.number]) {
                    
                    excludeDestination = YES;
                    break;
                    
                }
                
            }
            
            if (excludeDestination) {
                
                excludedDestinationAccount_ = [destinationAccount retain];
                
            }
            
        } else {
            
            selectedOriginAccountIndex_ = -1;
            
        }
        
        NSUInteger selectedDestinationIndex = [[self destinationAccountList] indexOfObject:selectedDestinationAccount];
        
        if (selectedDestinationIndex != NSNotFound) {
            
            selectedDestinationAccountIndex_ = selectedDestinationIndex;
            
        } else {
            
            selectedDestinationAccountIndex_ = -1;
            
        }
        
    }
    
}

/*
 * Returns the selected origin account
 *
 * @return The selected origin account
 */
- (BankAccount *)selectedOriginAccount {
    
    BankAccount *result = nil;
    
    if ((selectedOriginAccountIndex_ >= 0) && (selectedOriginAccountIndex_ < [originAccountList_ count])) {
        
        result = [originAccountList_ objectAtIndex:selectedOriginAccountIndex_];
        
    }
    
    return result;
    
}

/*
 * Returns the destination accounts list
 *
 * @return The destination accounts list
 */
- (NSArray *)destinationAccountList {
    
    NSMutableArray *auxArray = [NSMutableArray arrayWithArray:destinationAccountList_];

    if (excludedDestinationAccount_ != nil) {
        
        [auxArray removeObject:excludedDestinationAccount_];
        
    }
    
    return [NSArray arrayWithArray:auxArray];
    
}

/*
 * Sets the selected detination account index
 *
 * @param selectedDestinationAccountIndex The new selected destination account index
 */
- (void)setSelectedDestinationAccountIndex:(NSInteger)selectedDestinationAccountIndex {
    
    if (selectedDestinationAccountIndex != selectedDestinationAccountIndex_) {
        
        selectedDestinationAccountIndex_ = selectedDestinationAccountIndex;
        NSArray *destinationAccountList = self.destinationAccountList;
        
        if ((selectedDestinationAccountIndex_ >= 0) && (selectedDestinationAccountIndex_ < [destinationAccountList count])) {
            
        } else {
            
            selectedDestinationAccountIndex_ = -1;
            
        }
        
    }
    
}

/*
 * Returns the selected destination account
 *
 * @return The selected destination account
 */
- (BankAccount *)selectedDestinationAccount {
    
    BankAccount *result = nil;
    
    NSArray *correctDestintionAccountList = self.destinationAccountList;
    
    if ((selectedDestinationAccountIndex_ >= 0) && (selectedDestinationAccountIndex_ < [correctDestintionAccountList count])) {
        
        result = [correctDestintionAccountList objectAtIndex:selectedDestinationAccountIndex_];
        
    }
    
    return result;
    
}

/*
 * Sets the selected currency index
 */
- (void)setSelectedCurrencyIndex:(NSInteger)selectedCurrencyIndex {
    
    if(selectedCurrencyIndex != selectedCurrencyIndex_) {
        
        selectedCurrencyIndex_ = selectedCurrencyIndex;
        NSArray *currencyList = serverCurrencyList_;
        
        if((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [currencyList count])) {
            
            self.currency = [currencyList objectAtIndex:selectedCurrencyIndex_];
            
        } else {
            selectedCurrencyIndex_ = -1;
        }
    
    }
}

/*
 * Returns the selected currency
 *
 * @return The selected currency
 */
- (NSString *)selectedCurrency {
    
    NSString *result = nil;
    
    NSArray *currencyList = serverCurrencyList_;
    
    if((selectedCurrencyIndex_ >=0) && (selectedCurrencyIndex_ < [currencyList count])) {
        
        result = [currencyList objectAtIndex:selectedCurrencyIndex_];
    }
    
    return result;
}

#pragma mark -
#pragma mark Server operations management

/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startTransferRequest {
    
    BOOL result = NO;
    BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    NSString *originAccountNumber = originAccount.number;
    BankAccount *destinationAccount = [self.destinationAccountList objectAtIndex:self.selectedDestinationAccountIndex];
    NSString *destinationAccountNumber = destinationAccount.number;
    NSString *amount = [Tools formatAmountWithDotDecimalSeparator:[self amountString]];
	NSString *currencySelected = @"";
    
    if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {

        currencySelected = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];
        
    }
    
    if (([originAccountNumber length] > 0) && ([destinationAccountNumber length] > 0) && ([amount length] != 0)) {
        
        [[Updater getInstance] transferBetweenAccountsConfirmationFromAccountNumber:originAccount.number 
                                                                    fromAccountType:originAccount.accountType 
                                                                       fromCurrency:originAccount.currency
                                                                           fromType:originAccount.type
                                                                          fromIndex:originAccount.subject
                                                                    toAccountNumber:destinationAccount.number
                                                                      toAccountType:destinationAccount.accountType
                                                                         toCurrency:destinationAccount.currency
                                                                             toType:destinationAccount.type
                                                                            toIndex:destinationAccount.subject
                                                                             amount:amount
                                                                           currency:currencySelected
                                                                         andSubject:self.reference];
        result = YES;
        
    }
    
    return result;
    
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startTransferRequestDataValidation {

    NSString *result = nil;
    
    if ([self selectedOriginAccountIndex] < 0) {
        
        result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
        
    } else if ([self selectedDestinationAccountIndex] < 0) {
        
        result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_TEXT_KEY, nil);
        
    } else if ([self selectedCurrencyIndex] < 0) {
        
        result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
        
    } else {
        
        BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
        NSString *originAccountNumber = originAccount.number;
        BankAccount *destinationAccount = [self.destinationAccountList objectAtIndex:self.selectedDestinationAccountIndex];
        NSString *destinationAccountNumber = destinationAccount.number;
		NSString *amount = [Tools formatAmountWithDotDecimalSeparator:[self amountString]]; 
        
        NSString *currencySelected = @"";
        
        if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {
            
            currencySelected = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];
            
        }
        
        if (!([originAccountNumber length] > 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
            
        } else if (!([destinationAccountNumber length] > 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_TEXT_KEY, nil);
            
        } else if (!([currencySelected length] > 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
            
        } else if ([amount length] == 0) {
            
            result = NSLocalizedString(TRANSFER_ERROR_AMOUNT_TEXT_KEY, nil);
            
        }
    }
    
    return result;

}


/*
 * Performs the payment confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (BOOL)startTransferConfirmationRequest {
            
    [[Updater getInstance] transferBetweenAccountsResult];
    
    return YES;

}

/*
 * Notifies the transfer operation helper the confirmation response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        
        if ([confirmationResponse isKindOfClass:[TransferSuccessResponse class]]) {
            
            TransferSuccessResponse *response = (TransferSuccessResponse *)confirmationResponse;
            
            if (transferSuccessAdditionalInfo_ != nil) {
                [transferSuccessAdditionalInfo_ release];
                transferSuccessAdditionalInfo_ = nil;
            }
            transferSuccessAdditionalInfo_ = [response.additionalInformation retain];

            result = YES;
            
        }
        
    }
    
    return result;
    
}

/*
 * Notifies the transfer operation helper the tranfer response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)transferResponseReceived:(StatusEnabledResponse *)transferResponse {
    
    BOOL result = [super transferResponseReceived:transferResponse];

    if (!result) {
        
        if ([transferResponse isKindOfClass:[TransferConfirmationResponse class]]) {
                        
            TransferConfirmationResponse *response = (TransferConfirmationResponse *)transferResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response.additionalInformation retain];
        
            result = YES;
            
        }
        
    }
    
    return result;
    
}


#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)transferSecondStepInformation {
    
    NSMutableArray *mutableResult = [NSMutableArray array];

    BankAccount *selectedOriginAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    BankAccount *selectedDestinationAccount = [self.destinationAccountList objectAtIndex:self.selectedDestinationAccountIndex];
    
    // amount
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_TO_MY_ACCOUNTS_AMOUNT_TEXT_KEY, nil);
    
    NSString *currency = @"";
    
    if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {
        
        currency = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];

    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], self.amountString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //origin
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORIGIN_TITLE_TEXT_KEY, nil);    

    NSString *originAccount = selectedOriginAccount.number;
    
    if ([originAccount length] > 0) {
        
        if ([selectedOriginAccount.accountType length] > 0) {
            
            originAccount = [NSString stringWithFormat:@"%@ - %@ - %@", selectedOriginAccount.accountType, [Tools getCurrencyLiteral:selectedOriginAccount.currency] , originAccount];
            
        }
        
    } else {
        
        originAccount = selectedOriginAccount.accountType;
        
    }    
    
    [titleAndAttributes addAttribute:originAccount];

        if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //destination
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_TITLE_TEXT_KEY, nil);  
    
    NSString *destinationAccount = selectedDestinationAccount.number;
    
    if ([destinationAccount length] > 0) {
        
        if ([selectedDestinationAccount.accountType length] > 0) {
            
            destinationAccount = [NSString stringWithFormat:@"%@ - %@ - %@", selectedDestinationAccount.accountType ,[Tools getCurrencyLiteral:selectedDestinationAccount.currency] ,destinationAccount];
            
        }
        
    } else {
        
        destinationAccount = selectedDestinationAccount.accountType;
        
    }
    
    [titleAndAttributes addAttribute:destinationAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //tax
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_TO_MY_ACCOUNTS_TAX_TEXT_KEY, nil);
    
    NSString *marketPlaceCommision = additionalInformation_.marketPlaceCommision;
    NSString *currencySymbol = @"";

    if (([marketPlaceCommision length] == 0) || ([marketPlaceCommision isEqualToString:@"0.00"])) {
        
        marketPlaceCommision = @"0.00";
        
    } else {
        
        currencySymbol = [NSString stringWithFormat:@"%@ ", [Tools notNilString:[Tools getCurrencySimbol:selectedOriginAccount.currency]]];
        
    }
    
    // iPad: accountOrigin_.currency
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@%@", currencySymbol, marketPlaceCommision]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //reference
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_REFERENCE_TEXT_KEY, nil);
    
    NSString *reference = @" ";
    
    if (![self.reference isEqualToString:@""]) {
        reference = self.reference;
    }
    
    [titleAndAttributes addAttribute:reference];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
    
}

/*
 * Creates the title and attributes array to display in the transfer third step. Default implementation returns an empty array
 */
- (NSArray *)transferThirdStepInformation {
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    BankAccount *selectedOriginAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    BankAccount *selectedDestinationAccount = [self.destinationAccountList objectAtIndex:self.selectedDestinationAccountIndex];
    
    // Operation number
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(NUMBER_OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operationNumber];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operation];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Origin
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORIGIN_TITLE_TEXT_KEY, nil);
    
    NSString *originAccount = selectedOriginAccount.number;
    
    if ([originAccount length] > 0) {
        
        if ([selectedOriginAccount.accountType length] > 0) {
            
            originAccount = [NSString stringWithFormat:@"%@ - %@ - %@", selectedOriginAccount.accountType, [Tools getCurrencyLiteral:selectedOriginAccount.currency], originAccount];
            
        }
        
    } else {
        
        originAccount = selectedOriginAccount.accountType;
        
    }
    
    [titleAndAttributes addAttribute:originAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_TEXT_KEY, nil);
    
    NSString *currency = @"";
    
    if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {
        
        currency = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], self.amountString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Destination
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_TITLE_TEXT_KEY, nil);
    
    NSString *destinationAccount = selectedDestinationAccount.number;
    
    if ([destinationAccount length] > 0) {
        
        if ([selectedDestinationAccount.accountType length] > 0) {
            
            destinationAccount = [NSString stringWithFormat:@"%@ - %@ - %@", selectedDestinationAccount.accountType ,[Tools getCurrencyLiteral:selectedDestinationAccount.currency], destinationAccount];
            
        }
        
    } else {
        
        destinationAccount = selectedDestinationAccount.accountType;
        
    }
    
    [titleAndAttributes addAttribute:destinationAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Owner
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORINGIN_ACCOUNT_OWNER_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.holder];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Reference
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_REFERENCE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:self.reference];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //tax
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_TO_MY_ACCOUNTS_TAX_TEXT_KEY, nil);
    
    NSString *marketPlaceCommision = additionalInformation_.marketPlaceCommision;
    NSString *currencySymbol = @"";
    
    if (([marketPlaceCommision length] == 0) || ([marketPlaceCommision isEqualToString:@"0.00"])) {
        
        marketPlaceCommision = @"0.00";
        
    } else {
        
        currencySymbol = [NSString stringWithFormat:@"%@ ", [Tools notNilString:[Tools getCurrencySimbol:selectedOriginAccount.currency]]];
        
    }
    
    // iPad: accountOrigin_.currency
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@%@", currencySymbol, marketPlaceCommision]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Change type
    if (![selectedOriginAccount.currency isEqualToString:selectedDestinationAccount.currency]) {
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(EXCHANGE_RATE_TEXT_KEY, nil);
        
        NSString *exchangeRate = transferSuccessAdditionalInfo_.exchangeRate;
        
        if ([exchangeRate length] == 0) {
            
            exchangeRate = @"0.00";
            
        }
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", 
                                          [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currencyExchangeRate], 
                                          exchangeRate]];
        
        if (titleAndAttributes != nil) {
            
            [mutableResult addObject:titleAndAttributes];
            
        }
    }
    
    // Amount paid
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_PAID_TITLE_TEXT_KEY, nil);
    
    NSString *amountPaidString = transferSuccessAdditionalInfo_.amountChargedString;
    
    if ([amountPaidString length] == 0) {
        
        amountPaidString = @"0.00";
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@",
                                      [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.amountChargedCurrencyString],
                                      amountPaidString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Amount charged
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_CHARGERD_TITLE_TEXT_KEY, nil);
    
    NSString *amountChargedString = transferSuccessAdditionalInfo_.amountPaidString;
    
    if ([amountChargedString length] == 0) {
        
        amountChargedString = @"0.00";
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@",
                                      [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.amountPaidCurrencyString],
                                      amountChargedString]];    

    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Time
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_DATE_HOUR_SHORT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", 
                                      [Tools notNilString:transferSuccessAdditionalInfo_.dateString], 
                                      [Tools notNilString:transferSuccessAdditionalInfo_.hour]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    return mutableResult;
    
}




/*
 * Returns the coordinateKeyTitle string. Base class returns an empry string
 *
 * @return The coordinateKeyTitle string
 */
- (NSString *)coordinateKeyTitle {
    
    return additionalInformation_.coordinate;
    
}

/*
 * Returns the seal string. Base class returns an empry string
 *
 * @return The seal string
 */
- (NSString *)sealText {
    
    return additionalInformation_.seal;
    
}

/**
 * Returns the transfer between user accounts operation type
 *
 * @return The transfer between user accounts operation type
 */
- (TransferTypeEnum)transferOperationType {
    
    return TTETransferBetweenUserAccounts;
    
}

/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedTransferOperationTypeString {
    
    return NSLocalizedString(TRANSFER_TO_MY_ACCOUNTS_CAPITAL_TITLE_KEY, nil);
    
}

/*
 * Returns the confirmation message. The transfer between accounts localized string is returned
 *
 * @return The confirmation message
 */
- (NSString *)confirmationMessage {
    
    return transferSuccessAdditionalInfo_.confirmationMessage;
    
}


@end
