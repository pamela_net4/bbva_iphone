//
//  TransferDetailCashMobile.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 10/7/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "TransferDetailCashMobile.h"
#import "Updater.h"
#import "TitleAndAttributes.h"
#import "Constants.h"
#import "StringKeys.h"
#import "TransferShowDetailAddtionalInformation.h"
#import "TransferShowDetailResponse.h"
#import "Tools.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "TransferSuccessResponse.h"
#import "TransferSuccessAdditionalInformation.h"


@implementation TransferDetailCashMobile
@synthesize transferSuccessAdditionalInfo = transferSuccessAdditionalInfo_;

/*
 * Performs the payment confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (BOOL)startTransferConfirmationRequest {
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    
    NSString *operationNumber = [user objectForKey:@"lastCashMobileConsult"];
    
    [[Updater getInstance] obtainCashMobileTransactionResend:operationNumber];
    
    
    return TRUE;
    
}

/*
 * show the result of the resend operation
 */
- (void)showResultMessage:(TransferSuccessResponse *)response
{
    
    [Tools showInfoWithMessage:response.additionalInformation.confirmationMessage];
}

/*
 * Designated initialized. Initializes a TransferBetweenUserAccounts instance with the initial transfer accounts response
 */
- (id)initWithTransferStartupResponse:(TransferShowDetailAddtionalInformation *)transferStartupResponse {
    
    if ((self = [super init])) {
        transferSuccessAdditionalInfo_ = transferStartupResponse;
    }
    
    
    
    return self;
    
}

#pragma mark -
#pragma mark Information distribution
/*
 * Creates the title and attributes array to display in the transfer third step. Default implementation returns an empty array
 */
- (NSArray *)transferThirdStepInformation {
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    
    // Operation number
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CASH_MOBILE_NUMBER_OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operationNumber];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operation];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Origin
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CASH_MOBILE_ACCOUNT_CHARGED_TEXT_KEY, nil);
    
    NSString *originAccount = transferSuccessAdditionalInfo_.subject;
    
    if ([originAccount length] > 0) {
        
        if ([transferSuccessAdditionalInfo_.currencyAccount length] > 0) {
            
            originAccount = [NSString stringWithFormat:@"%@ - %@ - %@", transferSuccessAdditionalInfo_.accountType, CURRENCY_SOLES_LITERAL, originAccount];
            
        }
        
    } else {
        
        originAccount = transferSuccessAdditionalInfo_.accountType;
        
    }
    
    [titleAndAttributes addAttribute:originAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // holder account charged
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CASH_MOBILE_ORINGIN_ACCOUNT_OWNER_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.holder];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFERED_SEND_IMPORT_TEXT_KEY, nil);
    
    NSString *currency = CURRENCY_SOLES_LITERAL;
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], transferSuccessAdditionalInfo_.amountToPay]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Destination
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_NUMBER_TITLE_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.beneficiary];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // itf
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ACCOUNT_ITF_TEXT_KEY, nil);
    
    NSString *marketPlaceCommision = transferSuccessAdditionalInfo_.itf;
    NSString *currencySymbol = transferSuccessAdditionalInfo_.currencyItf;
    
    if (([marketPlaceCommision length] == 0) || ([marketPlaceCommision isEqualToString:@"0.00"])) {
        
        marketPlaceCommision = @"0.00";
        
    } else {
        
        if (([currencySymbol length] == 0) || ([currencySymbol isEqualToString:@"0.00"]))
            currencySymbol = [NSString stringWithFormat:@"%@", [Tools notNilString:[Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currencyAccount]]];
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@", marketPlaceCommision]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Amount charged
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_PAID_DETAIL_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], transferSuccessAdditionalInfo_.chargedAmount]];
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // State
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(STATE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.transferState];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Time
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_DATE_HOUR_SHORT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", transferSuccessAdditionalInfo_.dateString, transferSuccessAdditionalInfo_.hourString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Expiraion Time
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_EXPIRATION_DATE_HOUR_SHORT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", transferSuccessAdditionalInfo_.expirationDateString, transferSuccessAdditionalInfo_.expirationHourString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
    
}

/*
 * Notifies the transfer operation helper the confirmation response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(TransferShowDetailResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        
        if ([confirmationResponse isKindOfClass:[TransferShowDetailResponse class]]) {
            
            TransferShowDetailResponse *response = (TransferShowDetailResponse *)confirmationResponse;
            
            if (transferSuccessAdditionalInfo_ != nil) {
                [transferSuccessAdditionalInfo_ release];
                transferSuccessAdditionalInfo_ = nil;
            }
            transferSuccessAdditionalInfo_ = [response.additionalInformation retain];
            
            result = YES;
            
        }
        
    }
    
    return result;
    
}


/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedTransferOperationTypeString {
    
    return NSLocalizedString(TRANSFER_WITH_CASH_MOBILE_TEXT_1_KEY, nil);
    
}

/**
 * Returns the transfer between user accounts operation type
 *
 * @return The transfer between user accounts operation type
 */
- (TransferTypeEnum)transferOperationType {
    
    return TTETransferConsultCashMobile;
    
}


@end
