/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import "TransferOperationHelper.h"

#import "GlobalAdditionalInformation.h"
#import "Session.h"
#import "StringKeys.h"
#import "Tools.h"

@implementation TransferOperationHelper

#pragma mark -
#pragma mark Properties

@synthesize amountString = amountString_;
@synthesize currency = currency_;
@synthesize reference = reference_;
@synthesize secondFactorKey = secondFactorKey_;
@synthesize sendSMS = sendSMS_;
@synthesize destinationSMS1 = destinationSMS1_;
@synthesize destinationSMS2 = destinationSMS2_;
@synthesize showSMS1 = showSMS1_;
@synthesize showSMS2 = showSMS2_;
@synthesize sendEmail = sendEmail_;
@synthesize destinationEmail1 = destinationEmail1_;
@synthesize destinationEmail2 = destinationEmail2_;
@synthesize showEmail1 = showEmail1_;
@synthesize showEmail2 = showEmail2_;

@synthesize selectedCarrier1Index = selectedCarrier1Index_;
@synthesize selectedCarrier2Index = selectedCarrier2Index_;
@synthesize emailMessage = emailMessage_;
@synthesize legalTermsAccepted = legalTermsAccepted_;
@synthesize itfSelected = itfSelected_;
@dynamic confirmationMessage;
@dynamic notificationMessage;
@dynamic transferOperationType;
@dynamic localizedTransferOperationTypeString;
@dynamic localizedTermsAndConditionsString;
@dynamic coordinateKeyTitle;
@dynamic sealText;
@dynamic legalTermsURL;
@dynamic canSendEmailandSMS;
@dynamic canShowITF;
@dynamic itfMessageSuccess;
@dynamic itfMessageConfirm;
@synthesize carrierList = carrierList_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [amountString_ release];
    amountString_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [reference_ release];
    reference_ = nil;
    
    [secondFactorKey_ release];
    secondFactorKey_ = nil;
    
    [destinationSMS1_ release];
    destinationSMS1_ = nil;
    
    [destinationSMS2_ release];
    destinationSMS2_ = nil;

    [destinationEmail1_ release];
    destinationEmail1_ = nil;
    
    [destinationEmail2_ release];
    destinationEmail2_ = nil;
    
    [emailMessage_ release];
    emailMessage_ = nil;
    
    [carrierList_ release];
    carrierList_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)transferSecondStepInformation {
    
    return [NSArray array];
    
}


/*
 * Creates the title and attributes array to display in the transfer third step. Default implementation returns an empty array
 */
- (NSArray *)transferThirdStepInformation {
    
    return [NSArray array];
    
}

#pragma mark -
#pragma mark Information validity

/*
 * Checks whether the user can start the operation. For instance, a user cannot make a transfer between his/her accounts when only one account is available.
 * Default implementation always return NO
 */
- (BOOL)canStartOperation {
    
    return NO;
    
}

-(BOOL)canOmitOTP{
    return NO;
}

/*
 * Checks whether the information is valid to start the transfer confirmation request. When it is not valid, an error message, that must be displayed
 * to the user is returned, with the reason that made it not possible to start the operation. Othwewise, the returned value is empty or nil. Default
 * implementation returns nil
 */
- (NSString *)transferConfirmationValidationErrorMessage {
    
    return nil;
    
}

/*
 * Checks whether the information is valid to start the transfer request. When it is not valid, an error message, that must be displayed
 * to the user is returned, with the reason that made it not possible to start the operation. Othwewise, the returned value is empty or nil. Stored
 * information is analyzed
 */
- (NSString *)transferValidationErrorMessage {
    
    NSString *result = @"";
    
    if ((self.canSendEmailandSMS) && (sendEmail_) && (sendSMS_)) {
        
        if ([destinationEmail1_ length] == 0) {

            result = @"Error_Send1";
//            result = NSLocalizedString(TRANSFER_EMAIL_DESTINATION_PLACEHOLDER_ERROR_TEXT_KEY, nil);
            
        } else if ([emailMessage_ length] == 0) {
            
            result = @"Error_Send_Message";  
//            result = NSLocalizedString(TRANSFER_EMAIL_MESSAGE_PLACEHOLDER_ERROR_TEXT_KEY, nil);
            
        }
        
    }
    
    
    if (([result length] == 0)) {
        
        OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
        
        if (([secondFactorKey_ length] == 0) && (otpUsage == otp_UsageTC)) {
            
            result = @"Error_User_Coordinates";
//            result = NSLocalizedString(TRANSFER_COORDINATE_ERROR_TEXT_KEY, nil);
            
        } else if (([secondFactorKey_ length] == 0) && (otpUsage == otp_UsageOTP)) {
            
            result = @"Error_User_OTPKey";
//            result = NSLocalizedString(TRANSFER_COORDINATE_ERROR_TEXT_KEY, nil);
            
        } 
        
    }
    
    return result;
    
}


#pragma mark -
#pragma mark Server operations management

/*
 * Performs the transfer confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (BOOL)startTransferConfirmationRequest {
    
    return NO;
    
}

/*
 * Notifies the transfer operation helper the confirmation response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    return NO;
    
}

- (BOOL)startFrequentOperationReactiveRequest
{
    return NO;
}

/*
 * Performs the transfer request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startTransferRequest {
    
    return NO;
    
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 *
 * @return if the data is valid returns nil
 */
- (NSString *)startTransferRequestDataValidation {
    
    NSString *result = nil;
    
    if (self.sendSMS) {
        
        if (self.showSMS1) {
            
            if (![Tools isValidMobilePhoneNumberString:[self destinationSMS1]]) {
                
                result = NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_ERROR_KEY, nil);
                
            } else if (self.selectedCarrier1Index < 0) {
                
                result = NSLocalizedString(TRANSFER_ERROR_CARRIER_TEXT_KEY, nil);
                
            }
        }
        
        if (([result length] == 0) && (self.showSMS2)) {
            
            if (![Tools isValidMobilePhoneNumberString:[self destinationSMS2]]) {
                
                result = NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_ERROR_KEY, nil);
                
            } else if (self.selectedCarrier2Index < 0) {
                
                result = NSLocalizedString(TRANSFER_ERROR_CARRIER_TEXT_KEY, nil);
                
            }
            
        }
        
    }

    if (([result length] == 0) && (self.sendEmail)) {
        
        if (self.showEmail1) {
        
            if ([self.destinationEmail1 length] <= 0)  {
            
                result = NSLocalizedString(TRANSFER_ERROR_EMAIL_TEXT_KEY, nil);
            
            }  else {
                
                NSArray *atSeparetedStrings = [self.destinationEmail1 componentsSeparatedByString:@"@"];
                NSUInteger atSeparetedStringsCount = [atSeparetedStrings count];
                
                if (atSeparetedStringsCount == 2) {
                    
                    NSString *firstString = [atSeparetedStrings objectAtIndex:0];
                    
                    if ([firstString length] > 0) {
                        
                        NSString *otherString = [atSeparetedStrings objectAtIndex:1];
                        NSRange dotRange = [otherString rangeOfString:@"."];
                        NSUInteger dotLocation = dotRange.location;
                        
                        if (!(((dotLocation > 0) && (dotLocation != NSNotFound)) && (dotLocation < ([otherString length] - 1))) ) {
                            
                            result = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                            
                        }   
                        
                    } else {
                        
                        result = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                        
                    }
                    
                } else {
                    
                    result = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                    
                }
                
            }
            
        }
        
        if (([result length] == 0) && (self.showEmail2)) {
            
            if ([self.destinationEmail2 length] <= 0) {
                
                result = NSLocalizedString(TRANSFER_ERROR_EMAIL_TEXT_KEY, nil);
                
            } else {
                
                NSArray *atSeparetedStrings = [self.destinationEmail2 componentsSeparatedByString:@"@"];
                NSUInteger atSeparetedStringsCount = [atSeparetedStrings count];
                
                if (atSeparetedStringsCount == 2) {
                    
                    NSString *firstString = [atSeparetedStrings objectAtIndex:0];
                    
                    if ([firstString length] > 0) {
                        
                        NSString *otherString = [atSeparetedStrings objectAtIndex:1];
                        NSRange dotRange = [otherString rangeOfString:@"."];
                        NSUInteger dotLocation = dotRange.location;
                        
                        if (!(((dotLocation > 0) && (dotLocation != NSNotFound)) && (dotLocation < ([otherString length] - 1))) ) {
                            
                            result = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                            
                        }
                        
                    } else {
                        
                        result = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                        
                    }
                    
                } else {
                    
                    result = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                    
                }
                
            }
            
        }
        
        if (([result length] == 0) && ([[self emailMessage] length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_EMAIL_MESSAGE_TEXT_KEY, nil);
            
        }
        
    }
    
    return result;

}

/*
 * Notifies the transfer operation helper the tranfer response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)transferResponseReceived:(StatusEnabledResponse *)transferResponse {
    
    return NO;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the transfer operation type. Base class returns a default value (transfer between user accounts)
 *
 * @return The transfer operation type
 */
- (TransferTypeEnum)trasnferOperationType {
    
    return TTETransferToOtherBankAccount;
    
}

/*
 * Returns the localized transfer operation type string. Base class returns an empty string
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedTransferOperationTypeString {
    
    return @"";
    
}

/*
 * Returns the localized terms and conditions string. Base class returns an empry string
 *
 * @return The localized terms and conditions string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return @"";
    
}

/*
 * Returns the confirmation message. Base class returns an empry string
 *
 * @return The confirmation message string
 */
- (NSString *)confirmationMessage {
    
    return @"";
    
}


/*
 * Returns the confirmation message. Base class returns an empry string
 *
 * @return The confirmation message string
 */
- (NSString *)notificationMessage {
    
    return @"";
    
}


/*
 * Returns the confirmation message. Base class returns an empry string
 *
 * @return The confirmation message string
 */
- (NSString *)itfMessageConfirm {
    
    return @"";
    
}
/*
 * Returns the confirmation message. Base class returns an empry string
 *
 * @return The confirmation message string
 */
- (NSString *)itfMessageSuccess {
    
    return @"";
    
}

/*
 * Returns the coordinateKeyTitle string. Base class returns an empty string
 *
 * @return The coordinateKeyTitle string
 */
- (NSString *)coordinateKeyTitle {
    
    return @"";
    
}

/*
 * Returns the seal string. Base class returns an empty string
 *
 * @return The seal string
 */
- (NSString *)sealText {
    
    return @"";
    
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
    return @"";
    
}

/*
 * Returns the needs coordinate key flag. Base class returns NO
 *
 * @return The need coordinate key flag
 */
- (BOOL)needsCoordinateKey {
    
    return NO;
    
}

/*
 * Returns the coordinate key. Base class returns an empty string
 *
 * @return The coordinate key
 */
- (NSString *)coordinateKey {
    
    return @"";
    
}

/*
 * Returns the can send email flag. Base class returns NO
 *
 * @return The can send email flag
 */
- (BOOL)canSendEmailandSMS {
    
    return NO;
    
}

/*
 * Returns the can send SMS flag. Base class returns NO
 *
 * @return The can send SMS flag
 */
//- (BOOL)canSendSMS {
//    
//    return NO;
//    
//}

/*
 * Returns the can show legal terms flag. Base class returns NO
 *
 * @return The can show legal terms flag
 */
- (BOOL)canShowLegalTerms {
    
    return NO;
    
}

/*
 * Returns the can show ITF flag
 */
- (BOOL)canShowITF {
    
    return NO;
    
}

/*
 * Tells if the user wants to send a notification message.
 *
 * @return YES, if any of the notification fields is active to send a message
 */
- (BOOL)isSendingNotification {

    return (showSMS1_ || showSMS2_ || showEmail1_ || showEmail2_);

}

@end
