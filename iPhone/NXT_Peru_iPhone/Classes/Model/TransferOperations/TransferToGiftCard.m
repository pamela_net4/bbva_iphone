//
//  TransferToGiftCard.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 1/31/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "TransferToGiftCard.h"

#import "AccountList.h"
#import "Account.h"
#import "AlterCarrierList.h"
#import "AlterCarrier.h"
#import "BankAccount.h"
#import "FOAccountList.h"
#import "FOCardList.h"
#import "FOCard.h"
#import "GiftCardStepOneResponse.h"
#import "GiftCardStepTwoResponse.h"
#import "GiftCardStepThreeResponse.h"
#import "GlobalAdditionalInformation.h"
#import "NXTCurrencyTextField.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "TransferConfirmationAdditionalInformation.h"
#import "TransferSuccessResponse.h"
#import "TransferSuccessAdditionalInformation.h"
#import "Updater.h"

@interface TransferToGiftCard()

/**
 * Checks if the destination account is in the account list
 *
 * @param destinationAccountNumber The destination account number
 */
- (BOOL)checkDestinationAccount:(NSString *)destinationAccountNumber;

@end

#pragma mark -

@implementation TransferToGiftCard

#pragma mark -
#pragma mark Properties

@synthesize originAccountList = originAccountList_;
@synthesize originCardList = originCardList_;
@synthesize selectedOriginAccountIndex = selectedOriginAccountIndex_;
@dynamic selectedOriginAccount;
@synthesize firstText = firstText_;
@synthesize secondText = secondText_;
@synthesize thirdText = thirdText_;
@synthesize fourthText = fourthText_;
@synthesize currencyList = currencyList_;
@synthesize selectedCurrencyIndex = selectedCurrencyIndex_;
@dynamic selectedCurrency;
@synthesize amountTextField = amountTextField_;
@synthesize hasCard = hasCard_;
@synthesize selectedAccount = selectedAccount_;
@synthesize selectedOriginCardIndex = selectedOriginCardIndex_;
@dynamic selectedCard;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [originAccountList_ release];
    originAccountList_ = nil;
    
    [originCardList_ release];
    originCardList_ = nil;
    
    [firstText_ release];
    firstText_ = nil;
    
    [secondText_ release];
    secondText_ = nil;
    
    [thirdText_ release];
    thirdText_ = nil;
    
    [fourthText_ release];
    fourthText_ = nil;
    
    [currencyList_ release];
    currencyList_ = nil;
    
    [serverCurrencyList_ release];
    serverCurrencyList_ = nil;
    
    [additionalInformation_ release];
    additionalInformation_ = nil;
    
    [transferSuccessAdditionalInfo_ release];
    transferSuccessAdditionalInfo_ = nil;
    
    [amountTextField_ release];
    amountTextField_ = nil;
    
    [selectedAccount_ release];
    selectedAccount_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initializarion

/**
 * Superclass designated initializer. Initializes an empty TransferBetweenUserAccounts instance
 *
 * @return An empty TransferBetweenUserAccounts instance
 */
- (id)init {
    
    return [self initWithTransferStartupResponse:nil];
    
}

/*
 * Designated initialized. Initializes a TransferBetweenUserAccounts instance with the initial transfer accounts response
 */
- (id)initWithTransferStartupResponse:(GiftCardStepOneResponse *)giftCardStepOneResponse {
    
    if ((self = [super init])) {
        
        // The initial operation in iPad returns elements no needed in iPhone
        originAccountList_ = [[NSMutableArray alloc] init];
        originCardList_ = [[NSMutableArray alloc] init];
        currencyList_ = [[NSArray alloc] initWithObjects:NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil),
                         NSLocalizedString(DOLLARS_CURRENCY_TEXT_KEY, nil),
                         nil];
        serverCurrencyList_ = [[NSArray alloc] initWithObjects:CURRENCY_SOLES_LITERAL,
                               CURRENCY_DOLARES_LITERAL,
                               nil];
        
        if (startUpResponseInformation_ != nil) {
            [startUpResponseInformation_ release];
            startUpResponseInformation_ = nil;
        }
        
        startUpResponseInformation_ = giftCardStepOneResponse;
        [startUpResponseInformation_ retain];
        
        [self setCarrierList:[self carrierListString]];
        
        hasCard_ = ([giftCardStepOneResponse.foCardList.foCardArray count] > 0);
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the selected origin account index and updates the destination account list
 *
 * @param selectedOriginAccountIndex The new selected origin account index
 */
- (void)setSelectedOriginAccountIndex:(NSInteger)selectedOriginAccountIndex {
    
    if (selectedOriginAccountIndex != selectedOriginAccountIndex_) {
        
        selectedOriginAccountIndex_ = selectedOriginAccountIndex;
        
        [originAccountList_ removeAllObjects];
        [originAccountList_ addObjectsFromArray:startUpResponseInformation_.foAccountList.accountList];
    }
    
    if ((selectedOriginAccountIndex_ >= 0) && (selectedOriginAccountIndex_ < [originAccountList_ count])) {
        
        selectedOriginAccountIndex_ = selectedOriginAccountIndex;
        
    } else {
        
        selectedOriginAccountIndex_ = -1;
        
    }
    
}

/*
 * Returns the selected origin account
 *
 * @return The selected origin account
 */
- (BankAccount *)selectedOriginAccount {
    
    BankAccount *result = nil;
    
    if ((selectedOriginAccountIndex_ >= 0) && (selectedOriginAccountIndex_ < [originAccountList_ count])) {
        
        result = [originAccountList_ objectAtIndex:selectedOriginAccountIndex_];
        
    }
    
    return result;
    
}

/*
 * Sets the selected origin account index and updates the destination account list
 *
 * @param selectedOriginAccountIndex The new selected origin account index
 */
- (void)setSelectedOriginCardIndex:(NSInteger)selectedOriginCardIndex{
    
    if (selectedOriginCardIndex != selectedOriginCardIndex_) {
        
        selectedOriginCardIndex_ = selectedOriginCardIndex;
        
        [originCardList_ removeAllObjects];
        [originCardList_ addObjectsFromArray:startUpResponseInformation_.foCardList.foCardArray];
    }
    
    if ((selectedOriginCardIndex_ >= 0) && (selectedOriginCardIndex_ < [originCardList_ count])) {
        
        selectedOriginCardIndex_ = selectedOriginCardIndex;
        
    } else {
        
        selectedOriginCardIndex_ = -1;
        
    }
    
}

/*
 * Returns the selected origin account
 *
 * @return The selected origin account
 */
- (FOCard *)selectedCard {
    
    FOCard *result = nil;
    
    if ((selectedOriginCardIndex_ >= 0) && (selectedOriginCardIndex_ < [originCardList_ count])) {
        
        result = [originCardList_ objectAtIndex:selectedOriginCardIndex_];
        
    }
    
    return result;
    
}

/*
 * Sets the selected currency index
 */
- (void)setSelectedCurrencyIndex:(NSInteger)selectedCurrencyIndex {
    
    if(selectedCurrencyIndex != selectedCurrencyIndex_) {
        
        selectedCurrencyIndex_ = selectedCurrencyIndex;
        NSArray *currencyList = serverCurrencyList_;
        
        if((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [currencyList count])) {
            
            self.currency = [currencyList objectAtIndex:selectedCurrencyIndex_];
            
        } else {
            
            selectedCurrencyIndex_ = -1;
        }
        
    }
}

/*
 * Returns the selected currency
 *
 * @return The selected currency
 */
- (NSString *)selectedCurrency {
    
    NSString *result = nil;
    
    NSArray *currencyList = serverCurrencyList_;
    
    if((selectedCurrencyIndex_ >=0) && (selectedCurrencyIndex_ < [currencyList count])) {
        
        result = [currencyList objectAtIndex:selectedCurrencyIndex_];
    }
    
    return result;
}
#pragma mark -
#pragma mark utility methods
-(NSArray*) carrierListString
{
    AlterCarrierList *carrierList = [startUpResponseInformation_ alterCarrierList];
    NSArray *carrierArray = [carrierList alterCarrierList];
    NSMutableArray *carrierStringArray  = [[[NSMutableArray alloc] init] autorelease];
    
    for (AlterCarrier *aCarrier in carrierArray)
    {
        if (aCarrier.description != nil) {
            [carrierStringArray addObject: aCarrier.description];
        }        
    }
    
    return  carrierStringArray;
}

-(NSMutableArray*) cardListString
{
    FOCardList *cardTypeList = [startUpResponseInformation_ foCardList];
    NSArray *cardTypeArray = [cardTypeList foCardArray];
    NSMutableArray *cardStringArray  = [[[NSMutableArray alloc]init] autorelease];
    
    for(int i=0;i<[cardTypeArray count];i++)
    {
        FOCard *card = [cardTypeArray objectAtIndex:i];
        NSString *cardType = card.cardType;
        [cardStringArray addObject: [NSString stringWithFormat:@"%@ | %@", [Tools obfuscateCardNumber:card.cardNumber],cardType]];
    }
    
    return  cardStringArray;
}
-(NSMutableArray *)accountListString
{
    FOAccountList *accountPayList = [startUpResponseInformation_ foAccountList];
    NSArray *accountPayArray = [accountPayList accountList];
    NSMutableArray *accountStringArray  = [[[NSMutableArray alloc]init] autorelease];
    
    for(int i=0 ;i<[accountPayArray count] ;i++)
    {
        Account *anAccount = [accountPayArray objectAtIndex:i];
        
        NSString *result = @"";
        
        NSString *accountNumber = [anAccount subject];
        
        result = [NSString stringWithFormat:@"%@ %@ - Disp. %@ %@", [anAccount accountType], [Tools obfuscateAccountNumber:accountNumber], [Tools getCurrencySimbol:[anAccount currency]], [anAccount balanceAvailable]];
        
        [accountStringArray addObject: result];
    }
    
    return accountStringArray;
}

#pragma mark -
#pragma mark Server operations management

- (BOOL)startFrequentOperationReactiveRequest
{
   /* NSString *accountCardNumber = @"";
    if (additionalInformation_.account != nil) {
        accountCardNumber = [NSString stringWithFormat:@"%@ %@ %@", additionalInformation_.account.subject,additionalInformation_.account.accountType, additionalInformation_.account.currency];
    }else if (additionalInformation_.foCard != nil){
        
        
        
        accountCardNumber = [NSString stringWithFormat:@"%@ - %@",additionalInformation_.foCard.cardNumber,additionalInformation_.foCard.cardType ];
    }*/
    [[Updater getInstance] obtainTranferToGiftCardFrequentOperationReactiveStepOneWithOperation:transferSuccessAdditionalInfo_.service andbeneficiaryAccount:additionalInformation_.giftCard andCurrency:additionalInformation_.currency andOriginAccount:(additionalInformation_.account)?additionalInformation_.account.subject:additionalInformation_.foCard.cardNumber andAmount:self.amountString];
    return NO;

}

/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startTransferRequest {
    
    BOOL result = NO;
    NSString *accountType = @"";
    NSString *originAccountNumber = @"";
    
    if (([self selectedOriginAccountIndex] != NSNotFound &&
         [self selectedOriginAccountIndex] >= 0 &&
         [self selectedOriginAccountIndex] < [[self originAccountList] count]) ){
        
        Account *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
        originAccountNumber = [originAccount param];
        accountType = @"C";
        
    }else if( ([self selectedOriginCardIndex] != NSNotFound &&
               [self selectedOriginCardIndex] >= 0 &&
               [self selectedOriginCardIndex] < [[self originCardList] count]) ){
        FOCard *originCard = [[self originCardList] objectAtIndex:[self selectedOriginCardIndex]];
        originAccountNumber = [originCard param];
        accountType = @"T";
    }
    
    
    NSString *amount = [Tools formatAmountWithDotDecimalSeparator:self.amountString];
    NSInteger selectedCurrencyIndex = selectedCurrencyIndex_;
    
    NSString *carrier1 = @"";
    NSString *carrier2 = @"";
    
    if (self.sendSMS) {
        
        AlterCarrier *carrierObj;
        
        if (self.showSMS1 && (self.selectedCarrier1Index >= 0)) {
            
            carrierObj = [[[startUpResponseInformation_ alterCarrierList] alterCarrierList] objectAtIndex:self.selectedCarrier1Index];
            
            carrier1 = carrierObj.code;
        }
        
        if (self.showSMS2 && (self.selectedCarrier2Index >= 0)) {
            
            carrierObj = [[[startUpResponseInformation_ alterCarrierList] alterCarrierList] objectAtIndex:self.selectedCarrier2Index];
            
            carrier2 = carrierObj.code;
            
        }
        
    }
    
    NSString *currencySelected = @"";
    
    if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {
        
        currencySelected = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];
        
    }
    
    NSString *destinationCard = @"";
    
    destinationCard = [NSString stringWithFormat:@"%@-%@-%@-%@", firstText_, secondText_, thirdText_, fourthText_];
    
    NSString *destinationPhone = @"";
    NSString *destinationCarrier = @"";
    
    if (self.sendSMS) {
        if (self.showSMS1) {
            destinationPhone = [self destinationSMS1];
            destinationCarrier = carrier1;
        }
        
        if (self.showSMS2) {
            destinationPhone = [destinationPhone stringByAppendingString:[NSString stringWithFormat:@",%@", [self destinationSMS2]]];
            destinationCarrier = [destinationCarrier stringByAppendingString:[NSString stringWithFormat:@",%@", carrier2]];
        }
    }
    
    NSString *destinationEmail = @"";
    
    if (self.sendEmail) {
        if (self.showEmail1) {
            destinationEmail = [self destinationEmail1];
        }
        
        if (self.showEmail2) {
            destinationEmail = [destinationEmail stringByAppendingString:[NSString stringWithFormat:@",%@",[self destinationEmail2]]];
        }
    }
    
    if (([originAccountNumber length] > 0) &&
        ([firstText_ length] > 0) &&
        ([secondText_ length] > 0) &&
        ([thirdText_ length] > 0) &&
        ([fourthText_ length] > 0) &&
        ([amount length] > 0) &&
        (selectedCurrencyIndex > -1)) {

        [[Updater getInstance] transferToGiftCardConfirmationFromAccountType:accountType
                                                                  fromNumber:originAccountNumber
                                                                    toNumber:destinationCard
                                                                      amount:amount
                                                                    currency:currencySelected
                                                         toConfirmationPhone:destinationPhone
                                                          toConfirmationMail:destinationEmail
                                                                     carrier:destinationCarrier
                                                            referenceMessage:[Tools notNilString:[self emailMessage]]];
        
        result = YES;
        
    }
    
    return result;
    
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startTransferRequestDataValidation {
	
	NSString *result = nil;
        
	if (!hasCard_ && ([self selectedOriginAccountIndex] == NSNotFound || [self selectedOriginAccountIndex] < 0 || [self selectedOriginAccountIndex] > [[self originAccountList] count] ) ) {
		
		result = NSLocalizedString(TRANSFER_SELECT_ORIGIN_ACCOUNT_TEXT_KEY, nil);
		
	} else if ( ([self selectedOriginAccountIndex] == NSNotFound ||
                [self selectedOriginAccountIndex] < 0 ||
                [self selectedOriginAccountIndex] > [[self originAccountList] count] )
                    &&
                ([self selectedOriginCardIndex] == NSNotFound ||
                [self selectedOriginCardIndex] < 0 ||
                [self selectedOriginCardIndex] > [[self originCardList] count] ) ){
        
        result = NSLocalizedString(PAYMENT_RECHARGE_ERROR_INVALID_NUMBER_TEXT_KEY, nil);
        
    }   else {
		
		NSString *originAccountNumber = @"";
        
        if (!hasCard_) {
            Account *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
            originAccountNumber = [originAccount subject];
        }else{
            if (([self selectedOriginAccountIndex] != NSNotFound &&
                [self selectedOriginAccountIndex] >= 0 &&
                [self selectedOriginAccountIndex] < [[self originAccountList] count]) ){
                
                Account *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
                originAccountNumber = [originAccount subject];
                
            }else if( ([self selectedOriginCardIndex] != NSNotFound &&
                      [self selectedOriginCardIndex] >= 0 &&
                      [self selectedOriginCardIndex] < [[self originCardList] count]) ){
                FOCard *originCard = [[self originCardList] objectAtIndex:[self selectedOriginCardIndex]];
                originAccountNumber = [originCard cardNumber];
            }
        }
        
        if (([firstText_ length] == 0) ||
            ([secondText_ length] == 0) ||
            ([thirdText_ length] == 0) ||
            ([fourthText_ length] == 0) ) {
            
            result = NSLocalizedString(TRANSFER_ERROR_CARD_NUMBER_TEXT_KEY, nil);
            
        } else if (([firstText_ length] != 4) ||
                   ([secondText_ length] != 4) ||
                   ([thirdText_ length] != 4) ||
                   ([fourthText_ length] != 4) ) {
            
            result = NSLocalizedString(TRANSFER_ERROR_VALID_GIFT_CARD_TEXT_KEY, nil);
            
        } else {
            
            NSString *destinationAccountNumber = [NSString stringWithFormat:@"%@-%@-%@-%@", firstText_,secondText_,thirdText_, fourthText_];
            
            if ([originAccountNumber isEqualToString:destinationAccountNumber]) {
                
                result = NSLocalizedString(TRANSFERS_TO_THIRD_ACCOUNTS_SAME_ACCOUNT_ERROR_KEY, nil);
                
            } else if ([self selectedCurrencyIndex] < 0) {
                
                result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
                
            } else {
                
                NSString *originAccountNumber = @"";
                
                if (([self selectedOriginAccountIndex] != NSNotFound &&
                     [self selectedOriginAccountIndex] >= 0 &&
                     [self selectedOriginAccountIndex] < [[self originAccountList] count]) ){
                    
                    Account *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
                    originAccountNumber = [originAccount subject];
                    
                }else if( ([self selectedOriginCardIndex] != NSNotFound &&
                           [self selectedOriginCardIndex] >= 0 &&
                           [self selectedOriginCardIndex] < [[self originCardList] count]) ){
                    FOCard *originCard = [[self originCardList] objectAtIndex:[self selectedOriginCardIndex]];
                    originAccountNumber = [originCard cardNumber];
                }
                
                NSString *currencySelected = @"";
                
                if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {
                    
                    currencySelected = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];
                    
                }
                
                if (!([originAccountNumber length] > 0)) {
                    
                    result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
                    
                } else if (!([currencySelected length] > 0)) {
                    
                    result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
                    
                } else if (([self.amountString isEqualToString:@"0.00"]) || ([self.amountString floatValue] == 0.0f)) {
                    
                    result = NSLocalizedString(TRANSFER_ERROR_TRANSFER_AMOUNT_TEXT_KEY, nil);
                    
                }
                else {
                    result = [super startTransferRequestDataValidation];
                }

                /* else if (([self.amountString floatValue] < 150.0f)) {
                    
                    result = NSLocalizedString(@"Ingrese un monto mayor o igual a 150", nil);
                    
                }*/
                
            }
        }
		
	}
    
    return result;
    
}


/*
 * Performs the payment confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (BOOL)startTransferConfirmationRequest {
    
    BOOL canStartProcess = YES;
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
    if (self.canShowLegalTerms && !self.legalTermsAccepted) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_LEGAL_TERMS_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    } else if ((otpUsage == otp_UsageOTP) && ([[self secondFactorKey] length] == 0)) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_OTP_KEY_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    } else if ((otpUsage == otp_UsageTC) && (([[self secondFactorKey] length] == 0) || ([[self secondFactorKey] length] < 3))) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_COORDINATES_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    }
    
    if (canStartProcess) {
        
        [[Updater getInstance] TransferToGiftCardResultFromSecondFactorKey:[self secondFactorKey]];
        
    }
    
    return canStartProcess;
    
}

/*
 * Notifies the transfer operation helper the confirmation response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        
        if ([confirmationResponse isKindOfClass:[GiftCardStepThreeResponse class]]) {
            
            GiftCardStepThreeResponse *response = (GiftCardStepThreeResponse *)confirmationResponse;
            
            if (transferSuccessAdditionalInfo_ != nil) {
                [transferSuccessAdditionalInfo_ release];
                transferSuccessAdditionalInfo_ = nil;
            }
            transferSuccessAdditionalInfo_ = [response retain];
            
            result = YES;
            
        }
        
    }
    
    return result;
    
}

/*
 * Notifies the transfer operation helper the tranfer response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)transferResponseReceived:(StatusEnabledResponse *)transferResponse {
    
    BOOL result = [super transferResponseReceived:transferResponse];
    
    if (!result) {
        
        if ([transferResponse isKindOfClass:[GiftCardStepTwoResponse class]]) {
            
            GiftCardStepTwoResponse *response = (GiftCardStepTwoResponse *)transferResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response retain];
            
            coordinateReference_ = [additionalInformation_ coordinate];
            sealReference_ = [[additionalInformation_ securitySeal] retain];
            
            result = YES;
            
        }
        
    }
    
    return result;
    
}

/*
 * Checks if the destination account is in the account list
 */
- (BOOL)checkDestinationAccount:(NSString *)destinationAccountNumber {
    
    BOOL result = NO;
    
    BankAccount *account;
    NSInteger i = 0;
    NSInteger count = [self.originAccountList count];
    
    while (i < count && !result) {
        
        account = [self.originAccountList objectAtIndex:i];
        result = [[account number] isEqualToString:destinationAccountNumber];
        i++;
        
    }
    
    return result;
    
}


#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)transferSecondStepInformation {
    
    NSMutableArray *mutableResult = [NSMutableArray array];

    // amount
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_GIFT_CARD_AMOUNT_TO_PAY_KEY, nil);
    
    NSString *currency = @"";
    
    if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {
        
        currency = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], self.amountString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    //Charged Account/Card
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_GIFT_CARD_CHARGED_ACCOUNT_CARD_KEY, nil);
    NSString *accountCardNumber = @"";
    NSString *accountCardNumberType = @"";
    NSString *currencyOperation = additionalInformation_.currency;
    
    if ([CURRENCY_SOLES_SYMBOL isEqualToString:additionalInformation_.currency]) {
        currencyOperation = CURRENCY_SOLES_LITERAL;
    }else{
        currencyOperation = CURRENCY_DOLARES_LITERAL;
    }
    
    if (additionalInformation_.account != nil) {
        accountCardNumberType = [NSString stringWithFormat:@"%@ | %@",additionalInformation_.account.accountType,[Tools getCurrencyLiteral: additionalInformation_.account.currency]];
        accountCardNumber=[Tools notNilString:additionalInformation_.account.subject];
        
    }else if (additionalInformation_.foCard != nil){
        
        
        
        accountCardNumberType = [NSString stringWithFormat:@"%@ | %@",additionalInformation_.foCard.cardType,[Tools getCurrencyLiteral: additionalInformation_.foCard.currency]];
        accountCardNumber=[Tools notNilString:additionalInformation_.foCard.cardNumber];
    }
    
    [titleAndAttributes addAttribute:accountCardNumberType];
    [titleAndAttributes addAttribute:accountCardNumber];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    //Gift Card
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_GIFT_CARD_TITLE_KEY, nil);
    
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.giftCard]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return [NSArray arrayWithArray:mutableResult];
    
}

/*
 * Creates the title and attributes array to display in the transfer third step. Default implementation returns an empty array
 */
- (NSArray *)transferThirdStepInformation {
    
    NSMutableArray *mutableResult = [NSMutableArray array];

    // Operation number
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(NUMBER_OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:transferSuccessAdditionalInfo_.operationNumber]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:transferSuccessAdditionalInfo_.service]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Origin
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORIGIN_TITLE_TEXT_KEY, nil);
    
    NSString *accountCardNumber = @"";
    NSString *accountCardNumberType = @"";
    NSString *currencyOperation = additionalInformation_.currency;
    
    if ([CURRENCY_SOLES_SYMBOL isEqualToString:additionalInformation_.currency]) {
        currencyOperation = CURRENCY_SOLES_LITERAL;
    }else{
        currencyOperation = CURRENCY_DOLARES_LITERAL;
    }

    if (additionalInformation_.account != nil) {
        accountCardNumberType = [NSString stringWithFormat:@"%@ | %@",additionalInformation_.account.accountType,[Tools getCurrencyLiteral: additionalInformation_.account.currency]];
         accountCardNumber=[Tools notNilString:additionalInformation_.account.subject];
        
    }else if (additionalInformation_.foCard != nil){
        
        accountCardNumberType = [NSString stringWithFormat:@"%@ | %@",additionalInformation_.foCard.cardType,[Tools getCurrencyLiteral: additionalInformation_.foCard.currency]];
        accountCardNumber=[Tools notNilString:additionalInformation_.foCard.cardNumber];
    }
    
    [titleAndAttributes addAttribute:accountCardNumberType];
    [titleAndAttributes addAttribute:accountCardNumber];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Gift card
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_GIFT_CARD_TITLE_KEY, nil);
    
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.giftCard]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }


    // Charged Amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CARD_PAYMENT_CHARGED_AMOUNT_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute: [Tools notNilString:transferSuccessAdditionalInfo_.amountCharge ]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    if([transferSuccessAdditionalInfo_.exchangeRate length]>0)
    {
        //Exchange rate
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(CHANGE_TYPE_TITLE_KEY, nil);
    
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@",  transferSuccessAdditionalInfo_.exchangeRate]];
    
        if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
        }
    }
    
    // Payed Amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_CHARGERD_TITLE_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currency], transferSuccessAdditionalInfo_.amountPaid]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Date Hour
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(PAYMENT_RECHARGE_DATE_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:transferSuccessAdditionalInfo_.dateHour]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
    
}




/*
 * Returns the coordinateKeyTitle string. Base class returns an empry string
 *
 * @return The coordinateKeyTitle string
 */
- (NSString *)coordinateKeyTitle {
    
    return coordinateReference_;
    
}

/*
 * Returns the seal string. Base class returns an empry string
 *
 * @return The seal string
 */
- (NSString *)sealText {
    
    return sealReference_;
    
}

/**
 * Returns the transfer between user accounts operation type
 *
 * @return The transfer between user accounts operation type
 */
- (TransferTypeEnum)transferOperationType {
    
    return TTETransferToGiftCard;
    
}

/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedTransferOperationTypeString {
    
    return NSLocalizedString(TRANSFER_GIFT_CARD_TEXT_1_KEY, nil);
    
}

/*
 * Returns the localized terms and conditions string. The transfer between accounts legal terms localized string is returned
 *
 * @return The localized transfer legal terms operation type string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return NSLocalizedString(TRANSFER_LEGAL_TERMS_KEY, nil);
    
}

/*
 * Returns the confirmation message. The transfer between accounts localized string is returned
 *
 * @return The confirmation message
 */
- (NSString *)confirmationMessage {
    
    return @"";//transferSuccessAdditionalInfo_.confirmationMessage;
    
}

/*
 * Returns the can show legal terms flag
 */
- (BOOL)canShowLegalTerms {
    
    return NO;
    
}

/*
 * Returns the can show email and sms
 */
- (BOOL)canSendEmailandSMS {
    
    return YES;
    
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
#if defined(SIMULATE_HTTP_CONNECTION)
    return [Tools notNilString:@"http://www.google.es"];
#else
    return [Tools notNilString:@"http://www.google.es"];//[Tools notNilString:additionalInformation_.disclaimer];
#endif
    
}

@end
