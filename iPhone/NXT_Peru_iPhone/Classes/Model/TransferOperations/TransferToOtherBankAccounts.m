/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "TransferToOtherBankAccounts.h"

#import "AccountList.h"
#import "BankAccount.h"
#import "GlobalAdditionalInformation.h"
#import "NXTCurrencyTextField.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "TransferConfirmationAdditionalInformation.h"
#import "TransferConfirmationResponse.h"
#import "TransferStartupResponse.h"
#import "TransferTypePaymentResponse.h"
#import "TransferSuccessResponse.h"
#import "TransferSuccessAdditionalInformation.h"
#import "Updater.h"

#pragma mark -

@interface TransferToOtherBankAccounts()

/**
 * Indicates document number correctness
 *
 * @param length The document length
 * @param code The document code
 * @private
 */
- (BOOL)isValidDocumentLength:(NSInteger)length forDocumentCode:(NSString *)code;

@end

#pragma mark -

@implementation TransferToOtherBankAccounts

#pragma mark -
#pragma mark Properties

@synthesize originAccountList = originAccountList_;
@synthesize selectedOriginAccountIndex = selectedOriginAccountIndex_;
@dynamic selectedOriginAccount;
@synthesize selectedDestinationAccountEntity = selectedDestinationAccountEntity_;
@synthesize selectedDestinationAccountOffice = selectedDestinationAccountOffice_;
@synthesize selectedDestinationAccountAccNumber = selectedDestinationAccountAccNumber_;
@synthesize selectedDestinationAccountCc = selectedDestinationAccountCc_;
@synthesize currencyList = currencyList_;
@synthesize selectedCurrencyIndex = selectedCurrencyIndex_;
@dynamic selectedCurrency;
@synthesize amountTextField = amountTextField_;
@synthesize documentTypeList = documentTypeList_;
@synthesize selectedTransferTypeIndex = selectedTransferTypeIndex_;
@synthesize selectedDocumentTypeIndex = selectedDocumentTypeIndex_;
@synthesize documentNumber = documentNumber_;
@synthesize beneficiary = beneficiary_;


#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [originAccountList_ release];
    originAccountList_ = nil;
    
    [selectedDestinationAccountEntity_ release];
    selectedDestinationAccountEntity_ = nil;
    
    [selectedDestinationAccountOffice_ release];
    selectedDestinationAccountOffice_ = nil;
    
    [selectedDestinationAccountAccNumber_ release];
    selectedDestinationAccountAccNumber_ = nil;
    
    [selectedDestinationAccountCc_ release];
    selectedDestinationAccountCc_ = nil;
    
    [currencyList_ release];
    currencyList_ = nil;
    
    [serverCurrencyList_ release];
    serverCurrencyList_ = nil;
    
    [documentTypeList_ release];
    documentTypeList_ = nil;
    
    [documentNumber_ release];
    documentNumber_ = nil;
    
    [beneficiary_ release];
    beneficiary_ = nil;
    
    [additionalInformation_ release];
    additionalInformation_ = nil;
    
    [transferSuccessAdditionalInfo_ release];
    transferSuccessAdditionalInfo_ = nil;
    
    [amountTextField_ release];
    amountTextField_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initializarion

/**
 * Superclass designated initializer. Initializes an empty TransferBetweenUserAccounts instance
 *
 * @return An empty TransferBetweenUserAccounts instance
 */
- (id)init {
    
    return [self initWithTransferStartupResponse:nil];
    
}

/*
 * Designated initialized. Initializes a TransferBetweenUserAccounts instance with the initial transfer accounts response
 */
- (id)initWithTransferStartupResponse:(TransferStartupResponse *)transferStartupResponse {
    
    if ((self = [super init])) {
        
        // The initial operation in iPad returns elements no needed in iPhone
        originAccountList_ = [[NSMutableArray alloc] init];
        currencyList_ = [[NSArray alloc] initWithObjects:NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil),
                         NSLocalizedString(DOLLARS_CURRENCY_TEXT_KEY, nil),
                         nil];
        serverCurrencyList_ = [[NSArray alloc] initWithObjects:CURRENCY_SOLES_LITERAL,
                               CURRENCY_DOLARES_LITERAL,
                               nil];
#warning Remove - Change way of request
        documentTypeList_= [[NSMutableArray alloc] initWithArray:transferStartupResponse.documents];
        [self setCarrierList:transferStartupResponse.mobileCarriers];
    }    

    return self;
    
}


#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the selected origin account index and updates the destination account list
 *
 * @param selectedOriginAccountIndex The new selected origin account index
 */
- (void)setSelectedOriginAccountIndex:(NSInteger)selectedOriginAccountIndex {
                
    Session *session = [Session getInstance];
    
    if (selectedOriginAccountIndex != selectedOriginAccountIndex_) {
        
        selectedOriginAccountIndex_ = selectedOriginAccountIndex;
        
        [originAccountList_ removeAllObjects];
        [originAccountList_ addObjectsFromArray:session.accountList.transferAccountList];
    }
    
    if ((selectedOriginAccountIndex_ >= 0) && (selectedOriginAccountIndex_ < [originAccountList_ count])) {
        
        selectedOriginAccountIndex_ = selectedOriginAccountIndex;
        
    } else {
        
        selectedOriginAccountIndex_ = -1;
        
    }
            
}

/*
 * Returns the selected origin account
 *
 * @return The selected origin account
 */
- (BankAccount *)selectedOriginAccount {
    
    BankAccount *result = nil;
    
    if ((selectedOriginAccountIndex_ >= 0) && (selectedOriginAccountIndex_ < [originAccountList_ count])) {
        
        result = [originAccountList_ objectAtIndex:selectedOriginAccountIndex_];
        
    }
    
    return result;
    
}

/*
 * Sets the selected currency index
 */
- (void)setSelectedCurrencyIndex:(NSInteger)selectedCurrencyIndex {
    
    if(selectedCurrencyIndex != selectedCurrencyIndex_) {
        
        selectedCurrencyIndex_ = selectedCurrencyIndex;
        NSArray *currencyList = serverCurrencyList_;
        
        if((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [currencyList count])) {
            
            self.currency = [currencyList objectAtIndex:selectedCurrencyIndex_];
            
        } else {
            selectedCurrencyIndex_ = -1;
        }
    
    }
}

/*
 * Returns the selected currency
 *
 * @return The selected currency
 */
- (NSString *)selectedCurrency {
    
    NSString *result = nil;
    
    NSArray *currencyList = serverCurrencyList_;
    
    if((selectedCurrencyIndex_ >=0) && (selectedCurrencyIndex_ < [currencyList count])) {
        
        result = [currencyList objectAtIndex:selectedCurrencyIndex_];
    }
    
    return result;
}

#pragma mark -
#pragma mark Server operations management

/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startTransferRequest {
    
    BOOL result = NO;
    BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    NSString *originAccountNumber = originAccount.number;
    NSString *amount = [Tools formatAmountWithDotDecimalSeparator:[self amountString]]; 
    
    NSString *carrier1 = @"";
    NSString *carrier2 = @"";

    if (self.sendSMS) {
        
        if (self.showSMS1 && (self.selectedCarrier1Index >= 0)) {
            
            MobileCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier1Index];
            carrier1 = [carrier code];
            
        }
        
        if (self.showSMS2 && (self.selectedCarrier2Index >= 0)) {
            
            MobileCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier2Index];
            carrier2 = [carrier code]; 
            
        }
        
    }
    
    NSString *currencySelected = @"";
    
    if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {
        
        currencySelected = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];
        
    }

    NSUInteger entityLength = [selectedDestinationAccountEntity_ length];
    NSUInteger officeLength = [selectedDestinationAccountOffice_ length];
    NSUInteger accountNumberLength = [selectedDestinationAccountAccNumber_ length];
    NSUInteger accountCCLength = [selectedDestinationAccountCc_ length];

    if (([originAccountNumber length] > 0) && 
        (entityLength > 0) &&
        (officeLength > 0) && 
        (accountNumberLength > 0) && 
        (accountCCLength > 0) &&
        ([amount length] > 0)) {
        
        NSString *correctEntity = selectedDestinationAccountEntity_;

        if (entityLength < MAX_OTHER_BANK_NUMBER_ENTITY) {
            
            NSUInteger neededZeros = MAX_OTHER_BANK_NUMBER_ENTITY - entityLength;
            correctEntity = @"";
            
            for (NSUInteger i = 0; i< neededZeros; i++) {
                
                correctEntity = [correctEntity stringByAppendingString:@"0"];
                
            }
            
            correctEntity = [correctEntity stringByAppendingString:selectedDestinationAccountEntity_];
            
        }
        
        NSString *correctOffice = selectedDestinationAccountOffice_;
        
        if (officeLength < MAX_OTHER_BANK_NUMBER_OFFICE) {
            
            NSUInteger neededZeros = MAX_OTHER_BANK_NUMBER_OFFICE - officeLength;
            correctOffice = @"";
            
            for (NSUInteger i = 0; i< neededZeros; i++) {
                
                correctOffice = [correctOffice stringByAppendingString:@"0"];
                
            }
            
            correctOffice = [correctOffice stringByAppendingString:selectedDestinationAccountOffice_];
            
        }
        
        NSString *correctAccountNumber = selectedDestinationAccountAccNumber_;
        
        if (accountNumberLength < MAX_OTHER_BANK_NUMBER_ACCOUNT) {
            
            NSUInteger neededZeros = MAX_OTHER_BANK_NUMBER_ACCOUNT - accountNumberLength;
            correctAccountNumber = @"";
            
            for (NSUInteger i = 0; i< neededZeros; i++) {
                
                correctAccountNumber = [correctAccountNumber stringByAppendingString:@"0"];
                
            }
            
            correctAccountNumber = [correctAccountNumber stringByAppendingString:selectedDestinationAccountAccNumber_];
            
        }
        
        NSString *correctAccountCC = selectedDestinationAccountCc_;
        
        if (accountCCLength < MAX_OTHER_BANK_NUMBER_CC) {
            
            NSUInteger neededZeros = MAX_OTHER_BANK_NUMBER_CC - accountCCLength;
            correctAccountCC = @"";
            
            for (NSUInteger i = 0; i< neededZeros; i++) {
                
                correctAccountCC = [correctAccountCC stringByAppendingString:@"0"];
                
            }
            
            correctAccountCC = [correctAccountCC stringByAppendingString:selectedDestinationAccountCc_];
            
        }

        [[Updater getInstance] transferToAccountsFromOtherBanksConfirmationFromAccountType:originAccount.type 
                                                                              fromCurrency:originAccount.currency
                                                                                 fromIndex:originAccount.subject 
                                                                                    toBank:correctEntity 
                                                                                  toBranch:correctOffice 
                                                                                 toAccount:correctAccountNumber 
                                                                                      toCc:correctAccountCC
                                                                                    amount:amount
                                                                                  currency:currencySelected
                                                                                 reference:@""
                                                                                    email1:[Tools notNilString:self.destinationEmail1]
                                                                                    email2:[Tools notNilString:self.destinationEmail1] 
                                                                                    phone1:[Tools notNilString:self.destinationSMS1] 
                                                                                  carrier1:carrier1
                                                                                    phone2:[Tools notNilString:self.destinationSMS2] 
                                                                                  carrier2:carrier2
                                                                                   message:[Tools notNilString:self.emailMessage]];
        

        
        
        result = YES;
        
    }
    
    return result;
    
}

- (BOOL)startFrequentOperationReactiveRequest
{
    BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    
    [[Updater getInstance] obtainTranferToOtherBankFrequentOperationReactiveStepOneWithOperation:transferSuccessAdditionalInfo_.operation
                                                                           andbeneficiaryAccount:additionalInformation_.beneficiaryAccount
                                                                              andDestinationBank:additionalInformation_.destinationBank
                                                                              andBeneficiaryName:additionalInformation_.beneficiary
                                                                                andOriginAccount:originAccount.number
                                                                                       andAmount:self.amountString
                                                                                     andCurrency:self.currency
                                                                                  andDescription:transferSuccessAdditionalInfo_.beneficiaryDocumentType
                                                                               andDocumentNumber:transferSuccessAdditionalInfo_.beneficiaryDocument
                                                                                 andDocumentCode:transferSuccessAdditionalInfo_.beneficiaryDocumentTypeId
                                                                                          andITF:self.itfSelected];

    return YES;
}
/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startTransferRequestDataValidation {

    NSString *result = nil;
    
    if (self.selectedOriginAccountIndex < 0) {
        
        result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
        
    } else if (self.selectedCurrencyIndex < 0) {
        
        result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
        
    } else {
        
        BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
        NSString *originAccountNumber = originAccount.number;
        
        NSString *currencySelected = @"";
        
        if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {
            
            currencySelected = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];
            
        }
        
        if (!([originAccountNumber length] > 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
        }
        else if (([selectedDestinationAccountEntity_ length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_ENTITY_TEXT_KEY, nil);
            
        } else if (([selectedDestinationAccountOffice_ length] == 0)) {
        
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_OFFICE_TEXT_KEY, nil);
            
        } else if (([selectedDestinationAccountAccNumber_ length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_ACCNUMBER_TEXT_KEY, nil);
            
        } else if (([selectedDestinationAccountCc_ length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_CC_TEXT_KEY, nil);
        
        } else if (!([currencySelected length] > 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
            
        }  else  if (([amountTextField_.text isEqualToString:@"0.00"]) || ([amountTextField_.text floatValue] == 0.0f)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_AMOUNT_TEXT_KEY, nil);
            
        } else {
            
            result = [super startTransferRequestDataValidation];
        
        }
        
    }
    
    return result;

}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startSecondTransferRequestDataValidation{
    
    NSString *result = nil;
    
    
    if(([typePaymentInformation_.typeFlow isEqualToString:@"A"] && selectedTransferTypeIndex_ == 1) ||
            [typePaymentInformation_.typeFlow isEqualToString:@"N"]){
    
        Document *doctype = [documentTypeList_ objectAtIndex:selectedDocumentTypeIndex_];
    
        if (beneficiary_ == nil || [beneficiary_ isEqualToString:@""]) {
            
            result = NSLocalizedString(TRANSFER_TO_ANOTHER_BANK_MUST_WRITE_A_BENEFICIARY_TEXT_KEY, nil);
            
        }else if(![self isValidDocumentLength:documentNumber_.length forDocumentCode:doctype.code]) {
        
            result = NSLocalizedString(TRANSFER_TO_ANOTHER_BANK_MUST_WRITE_A_DOCUMENT_NUMBER_TEXT_KEY, nil);
        
        }
        
    }else if(selectedTransferTypeIndex_<0){
        result = NSLocalizedString(@"Seleccione el tipo de transferencia que desea realizar",nil);
    }
    
    return result;
}


- (BOOL)startTypeTransferInmediateRequest{
    
    BOOL result = NO;
    
    [[Updater getInstance]  transferToAccountsFromOtherBanksWithInmediateConfirmation];
        result = YES;
    
    self.itfSelected = NO;
    
    return result;
}

-(BOOL)startTypeTransferToConfirmRequest{
    
    if(([typePaymentInformation_.typeFlow isEqualToString:@"A"] && selectedTransferTypeIndex_ == 1) ||
       [typePaymentInformation_.typeFlow isEqualToString:@"N"]){

    
    Document *document = [documentTypeList_ objectAtIndex:selectedDocumentTypeIndex_];
    
    [[Updater getInstance] transferToAccountsFromOtherBanksWithByScheduleConfirmationWithItf:self.itfSelected beneficiary:[Tools notNilString:beneficiary_] documentType:document.code andDocumentNumber:documentNumber_];
    }
    else {
        [self startTypeTransferInmediateRequest];
    }
    return YES;
}


/*
 * validate length document of beneficiary
 */
-(BOOL)isValidDocumentLength:(NSInteger)length forDocumentCode:(NSString *)code {
    
    BOOL result = YES;
    
    if ([code isEqualToString:DOC_TYPE_DNI]) {
        
        result = (length == 8);
        
    } else if ([code isEqualToString:DOC_TYPE_RUC]) {
        
        result = (length == 11);
        
    } else if ([code isEqualToString:DOC_TYPE_MILITAR_ID]) {
        
        result = (length == 8);
        
    } else if ([code isEqualToString:DOC_TYPE_PASAPORT]) {
        
        result = (length > 0) && (length <= 11);
        
    } else if ([code isEqualToString:DOC_TYPE_INMIGRATION_ID]) {
        
        result = (length > 0) && (length <= 12);
        
    }
    
    return result;
    
}


/*
 * Performs the payment confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (BOOL)startTransferConfirmationRequest {
    
    BOOL canStartProcess = YES;
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
    if (self.canShowLegalTerms && !self.legalTermsAccepted) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_LEGAL_TERMS_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    } else if ((otpUsage == otp_UsageOTP) && ([[self secondFactorKey] length] == 0) && [[additionalInformation_.moduleActive lowercaseString] isEqualToString:@"n"]) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_OTP_KEY_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    } else if ((otpUsage == otp_UsageTC) && (([[self secondFactorKey] length] == 0) || ([[self secondFactorKey] length] < 3)) && [[additionalInformation_.moduleActive lowercaseString] isEqualToString:@"n"]) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_COORDINATES_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    } 
    
    if (canStartProcess) {
        
        [[Updater getInstance] transferToAccountsFromOtherBanksResultFromSecondFactorKey:[self secondFactorKey]];
        
    }
    
    return canStartProcess;

}

-(BOOL) typeTransferStepReceived:(StatusEnabledResponse*) typeTransferResponse{
    
    BOOL result = NO;
    
    if ([typeTransferResponse isKindOfClass:[TransferTypePaymentResponse class]]) {
        
        TransferTypePaymentResponse *response = (TransferTypePaymentResponse*)typeTransferResponse;
        
        documentTypeList_= [[NSMutableArray alloc] initWithArray:response.documents];
        
        if (typePaymentInformation_ != nil) {
            [typePaymentInformation_ release];
            typePaymentInformation_ = nil;
        }
        
        
        if (additionalInformation_ != nil) {
            [additionalInformation_ release];
            additionalInformation_ = nil;
        }
        
        if(response.additionalInformation)
            additionalInformation_ = [response.additionalInformation retain];


        typePaymentInformation_ = [response retain];
        
        result = YES;
        
    }
    return result;
}

/*
 * Notifies the transfer operation helper the confirmation response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        
        if ([confirmationResponse isKindOfClass:[TransferSuccessResponse class]]) {
            
            TransferSuccessResponse *response = (TransferSuccessResponse *)confirmationResponse;
            
            if (transferSuccessAdditionalInfo_ != nil) {
                [transferSuccessAdditionalInfo_ release];
                transferSuccessAdditionalInfo_ = nil;
            }
            transferSuccessAdditionalInfo_ = [response.additionalInformation retain];

            result = YES;
            
        }
        
    }
    
    return result;
    
}

/*
 * Notifies the transfer operation helper the tranfer response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)transferResponseReceived:(StatusEnabledResponse *)transferResponse {
    
    BOOL result = [super transferResponseReceived:transferResponse];

    if (!result) {
        
        if ([transferResponse isKindOfClass:[TransferConfirmationResponse class]]) {
                        
            TransferConfirmationResponse *response = (TransferConfirmationResponse *)transferResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response.additionalInformation retain];
        
            result = YES;
            
        }
        
    }
    
    return result;
    
}


#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)transferSecondStepInformation {
    
    NSMutableArray *mutableResult = [NSMutableArray array];

    BankAccount *selectedOriginAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    
    // amount
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_TO_MY_ACCOUNTS_AMOUNT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:self.currency], self.amountString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Origin
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORIGIN_TITLE_TEXT_KEY, nil);    
    
    NSString *originAccount = selectedOriginAccount.number;
    
    if ([originAccount length] > 0) {
        
        if ([selectedOriginAccount.accountType length] > 0) {
            
            originAccount = [NSString stringWithFormat:@"%@ - %@ - %@", selectedOriginAccount.accountType, [Tools getCurrencyLiteral:selectedOriginAccount.currency] , originAccount];
            
        }
        
    } else {
        
        originAccount = selectedOriginAccount.accountType;
        
    }    
    
    [titleAndAttributes addAttribute:originAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Destination bank
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_BANK_TEXT_KEY, nil);  
    
    [titleAndAttributes addAttribute:additionalInformation_.destinationBank];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Destination
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_TITLE_TEXT_KEY, nil);  
    
    [titleAndAttributes addAttribute:additionalInformation_.beneficiaryAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    
    //Beneficiary
    if([additionalInformation_.beneficiary length] > 0) {
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(BENEFICIARY_TEXT_KEY, nil);  
    
        [titleAndAttributes addAttribute:additionalInformation_.beneficiary];
    
        if (titleAndAttributes != nil) {
        
            [mutableResult addObject:titleAndAttributes];
        
        }
    }
    
    //Beneficiary document
    if([additionalInformation_.beneficiaryDocument length]>0){
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(TITLE_DOCUMENT_BENEFICIARY_TEXT_KEY, nil);
    
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@",additionalInformation_.beneficiaryDocumentType,additionalInformation_.beneficiaryDocument]];
    
        if (titleAndAttributes != nil) {
        
            [mutableResult addObject:titleAndAttributes];
        
        }
        
    }
    
    //Tax
    NSString *commission = additionalInformation_.commission;
    NSString *currencyCommission = additionalInformation_.currencyCommission;
    NSString *currencySymbol = @"";

    if (([commission length] == 0) || ([commission isEqualToString:@"0.00"])) {
        
        commission = @"0.00";
        
    } else {
        
        currencySymbol = [NSString stringWithFormat:@"%@ ", [Tools notNilString:[Tools getCurrencySimbol:currencyCommission]]];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(COMISSION_TEXT_KEY, nil);  
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@%@", 
                                      currencySymbol, 
                                      commission]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    //Network use
    NSString *networkUse = additionalInformation_.networkUse;
    NSString *currencyNetworkUse = additionalInformation_.currencyNetworkUse;
    
    if ([networkUse length] == 0) {
        
        networkUse = @"0.00";
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(USE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", 
                                      [Tools getCurrencySimbol:currencyNetworkUse], 
                                      networkUse]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    //Amount to pay
    NSString *amountToPay = additionalInformation_.amountToPay;
    NSString *amountCurrencyToPay = additionalInformation_.amountCurrencyToPay;
    
    if ([amountToPay length] == 0) {
        
        amountToPay = @"0.00";
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FINAL_AMOUNT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", 
                                      [Tools getCurrencySimbol:amountCurrencyToPay], 
                                      amountToPay]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    // Exchange rate
    if (![Tools currency:selectedOriginAccount.currency isEqualToCurrency:self.currency]) {

        NSString *exchangeRateString = additionalInformation_.tipoCambio;
        
        if ([exchangeRateString length] > 0) {
            
            NSDecimalNumber *exchangeRate = [Tools decimalFromServerString:exchangeRateString];
            NSString *exchangeRateCurrency = [Tools getCurrencySimbol:additionalInformation_.monedaTipoCambio];
            
            if ([exchangeRateCurrency length] == 0) {
                
                exchangeRateCurrency = [Tools mainCurrencySymbol];
                
            }
            
            NSString *exchangeRateToDisplay = [Tools formatAmount:exchangeRate
                                                     withCurrency:exchangeRateCurrency
                                          currenctyPrecedesAmount:YES
                                                     decimalCount:4];
            
            if([exchangeRate floatValue]>0.0f){
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
                if (titleAndAttributes != nil) {
                
                titleAndAttributes.titleString = NSLocalizedString(TRANSFER_EXCHANGE_RATE_TEXT_KEY, nil);
                [titleAndAttributes addAttribute:exchangeRateToDisplay];
                [mutableResult addObject:titleAndAttributes];
                
                }
            }
            
        }
        
    }

    return mutableResult;
    
}

/*
 * Creates the title and attributes array to display in the transfer third step. Default implementation returns an empty array
 */
- (NSArray *)transferThirdStepInformation {
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    BankAccount *selectedOriginAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];    
    
    // Operation number
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    if([transferSuccessAdditionalInfo_.operationState length]>0){
        titleAndAttributes.titleString = NSLocalizedString(@"Estado de la operación", nil);
        [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operationState];
    
        if (titleAndAttributes != nil) {
        
            [mutableResult addObject:titleAndAttributes];
        
        }
    }
    
    if([transferSuccessAdditionalInfo_.operationNumber length]>0){
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(NUMBER_OPERATION_TITLE_TEXT_KEY, nil);
        [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operationNumber];
    
        if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
        }
    }

    
    // Operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operation];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Origin
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORIGIN_TITLE_TEXT_KEY, nil);
    
    NSString *originAccount = selectedOriginAccount.number;
    
    if ([originAccount length] > 0) {
        
        if ([selectedOriginAccount.accountType length] > 0) {
            
            originAccount = [NSString stringWithFormat:@"%@ - %@ - %@", selectedOriginAccount.accountType, [Tools getCurrencyLiteral:selectedOriginAccount.currency], originAccount];
            
        }
        
    } else {
        
        originAccount = selectedOriginAccount.accountType;
        
    }
    
    [titleAndAttributes addAttribute:originAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Owner
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORINGIN_ACCOUNT_OWNER_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Session getInstance].additionalInformation.customer];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Destination bank
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSACTION_DEST_BANK_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:additionalInformation_.destinationBank];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Destination
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_TITLE_TEXT_KEY, nil);  
    
    [titleAndAttributes addAttribute:additionalInformation_.beneficiaryAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    // bebeficiary
    if([Tools notNilString:additionalInformation_.beneficiary].length>0 ){
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(BENEFICIARY_TEXT_KEY, nil);
        [titleAndAttributes addAttribute:additionalInformation_.beneficiary];
    
        if (titleAndAttributes != nil) {
        
            [mutableResult addObject:titleAndAttributes];
        
        }
    }
    
    // bebeficiary document
    if([Tools notNilString:transferSuccessAdditionalInfo_.beneficiaryDocument].length>0){
    
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(TITLE_DOCUMENT_BENEFICIARY_TEXT_KEY, nil);
        [titleAndAttributes addAttribute: [NSString stringWithFormat:@"%@ %@",transferSuccessAdditionalInfo_.beneficiaryDocumentType,transferSuccessAdditionalInfo_.beneficiaryDocument]];
    
        if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
        }
    }
    
    // Amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_TEXT_KEY, nil);
    
    NSString *currency = @"";
    
    if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {
        
        currency = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];
        
    }

    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], transferSuccessAdditionalInfo_.amountToTransfer]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Tax
    NSString *commission = transferSuccessAdditionalInfo_.commission;
    NSString *currencySymbol = @"";
    
    if (([commission length] == 0) || ([commission isEqualToString:@"0.00"])) {
        
        commission = @"0.00";
        
    } else {
        
        currencySymbol = [NSString stringWithFormat:@"%@ ", [Tools notNilString:[Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currencyCommission]]];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(COMISSION_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@%@", 
                                      currencySymbol,
                                      commission]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Network use
    
    NSString *networkUse = transferSuccessAdditionalInfo_.networkUse;
    
    if ([networkUse length] == 0) {
        
        networkUse = @"0.00";
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(USE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", 
                                      [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currencyNetworkUse], 
                                      networkUse]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    // Amount to paid
    
    NSString *amountToPay = transferSuccessAdditionalInfo_.amountToPay;
    
    if ([amountToPay length] == 0) {
        
        amountToPay = @"0.00";
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FINAL_AMOUNT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", 
                                      [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currencyAmountToPay], 
                                      amountToPay]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Total amount
    
    NSString *totalAmountToPay = transferSuccessAdditionalInfo_.totalAmountToPay;
    
    if ([totalAmountToPay length] == 0) {
        
        totalAmountToPay = @"0.00";
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TOTAL_AMOUNT_CHARGED_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", 
                                      [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currencyTotalAmountToPay], 
                                      totalAmountToPay]];    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Exchange rate
    
    if (![Tools currency:selectedOriginAccount.currency isEqualToCurrency:self.currency]) {
        
        NSString *exchangeRateString = transferSuccessAdditionalInfo_.exchangeRate;
        
        if ([exchangeRateString length] > 0) {
            
            NSDecimalNumber *exchangeRate = [Tools decimalFromServerString:exchangeRateString];
            NSString *exchangeRateCurrency = [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currencyExchangeRate];
            
            if ([exchangeRateCurrency length] == 0) {
                
                exchangeRateCurrency = [Tools mainCurrencySymbol];
                
            }
            
            NSString *exchangeRateToDisplay = [Tools formatAmount:exchangeRate
                                                     withCurrency:exchangeRateCurrency
                                          currenctyPrecedesAmount:YES
                                                     decimalCount:4];
            
            if([exchangeRate floatValue]>0.0f){
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
                if (titleAndAttributes != nil) {
                
                titleAndAttributes.titleString = NSLocalizedString(TRANSFER_EXCHANGE_RATE_TEXT_KEY, nil);
                [titleAndAttributes addAttribute:exchangeRateToDisplay];
                [mutableResult addObject:titleAndAttributes];
                
                }
            }
            
        }
        
    }

    // Time
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_DATE_HOUR_SHORT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", transferSuccessAdditionalInfo_.dateString, transferSuccessAdditionalInfo_.hour]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    return mutableResult;
    
}


/*
 * Returns the coordinateKeyTitle string. Base class returns an empry string
 *
 * @return The coordinateKeyTitle string
 */
- (NSString *)coordinateKeyTitle {
    
    return additionalInformation_.coordinate;
    
}

/*
 * Returns the seal string. Base class returns an empry string
 *
 * @return The seal string
 */
- (NSString *)sealText {
    
    return additionalInformation_.seal;
    
}


-(BOOL)canOmitOTP{
    return [[additionalInformation_.moduleActive uppercaseString] isEqualToString:@"S"];
}

/**
 * Returns the transfer between user accounts operation type
 *
 * @return The transfer between user accounts operation type
 */
- (TransferTypeEnum)transferOperationType {
    
    return TTETransferToOtherBankAccount;
    
}

/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedTransferOperationTypeString {
    
    return NSLocalizedString(TRANSFER_TO_ANOTHER_BANK_TEXT_1_KEY, nil);
    
}

/*
 * Returns the localized terms and conditions string. The transfer between accounts legal terms localized string is returned
 *
 * @return The localized transfer legal terms operation type string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return NSLocalizedString(TRANSFER_LEGAL_TERMS_KEY, nil);
    
}

/*
 * Returns the confirmation message. The transfer between accounts localized string is returned
 *
 * @return The confirmation message
 */
- (NSString *)confirmationMessage {
    
    return transferSuccessAdditionalInfo_.confirmationMessage;
    
}


/*
 * Returns the confirmation message. The transfer between accounts localized string is returned
 *
 * @return The itf message
 */
- (NSString *)itfMessageSuccess {
    
    return transferSuccessAdditionalInfo_.itfMessage;
    
}


/*
 * Returns the confirmation message. The transfer between accounts localized string is returned
 *
 * @return The itf message
 */
- (NSString *)itfMessageConfirm{
    
    return additionalInformation_.itfMessage;
    
}


/*
 * Returns the confirmation message. The transfer between accounts localized string is returned
 *
 * @return The confirmation message
 */
- (NSString *)notificationMessage {
    
    return transferSuccessAdditionalInfo_.notificationMessage;
    
}

/*
 * Returns the can show legal terms flag
 */
- (BOOL)canShowLegalTerms {
    
    return YES;
    
}

/*
 * Returns the can show ITF flag
 */
- (BOOL)canShowITF {
    
    return YES;
    
}

/*
 * Returns the can show email and sms
 */
- (BOOL)canSendEmailandSMS {
    
    return YES;
    
}

-(NSString *)commissionN {
    return typePaymentInformation_.commissionN;
}

-(NSString *)commissionL{
    return typePaymentInformation_.commissionL;
}

-(NSString *)totalPaymentN{
    return typePaymentInformation_.totalPayN;
}

-(NSString *)totalPaymentL{
    return typePaymentInformation_.totalPayL;
}

-(NSString *)typeFlow{
    return typePaymentInformation_.typeFlow;
}
-(NSString *)disclaimer{
    return typePaymentInformation_.disclaimer;
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
#if defined(SIMULATE_HTTP_CONNECTION)
    return [Tools notNilString:@"http://www.google.es"];
#else
    return [Tools notNilString:additionalInformation_.disclaimer];
#endif
    
}

@end
