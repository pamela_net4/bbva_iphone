/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "TransferOperationHelper.h"


//Forward declarations
@class BankAccount;
@class TransferStartupResponse;
@class TransferConfirmationAdditionalInformation;
@class TransferSuccessAdditionalInformation;
@class NXTCurrencyTextField;

/**
 * Base class to represents a transfer between accounts operation
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferToThirdAccounts : TransferOperationHelper {
    
@private
    
    /**
     * Array with the origin selectable accounts. All objects are PaymentElement instances
     */
    NSMutableArray *originAccountList_;
    
    /**
     * Selected origin account index. When out of bounds, no origin account is selected
     */
    NSInteger selectedOriginAccountIndex_;
    
    /**
     * Selected destination account office
     */
    NSString *selectedDestinationAccountOffice_;
    
    /**
     * Selected destination account accNumber
     */
    NSString *selectedDestinationAccountAccNumber_;
    
    /**
     * Array with the selectable currencies. 
     */
    NSArray * currencyList_;
    
    /**
     * Server currencies list. 
     */
    NSArray *serverCurrencyList_;

    /*
     *Selected currency index. When out of bounds, no currency is selected
     */
    NSInteger selectedCurrencyIndex_;
    
    /**
     * Coordinate reference
     */
    NSString *coordinateReference_;
    
    /**
     * Seal reference
     */
    NSString *sealReference_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    TransferConfirmationAdditionalInformation *additionalInformation_;
    
    /**
     * Transfer Success Additional Information
     */
    TransferSuccessAdditionalInformation *transferSuccessAdditionalInfo_;
    
    /**
     * Amount text field
     */
    NXTCurrencyTextField *amountTextField_;

}

/**
 * Provides read-only access to the array with the origin selectable accounts
 */
@property (nonatomic, readonly, retain) NSArray *originAccountList;

/**
 * Provides read-write access to the selected origin account index
 */
@property (nonatomic, readwrite, assign) NSInteger selectedOriginAccountIndex;

/**
 * Provides read-only access to the selected origin account
 */
@property (nonatomic, readonly, retain) BankAccount *selectedOriginAccount;

/**
 * Provides read-write access to the selected destination account office
 */
@property (nonatomic, readwrite, copy) NSString *selectedDestinationAccountOffice;

/**
 * Provides read-write access to the selected destination account accNumber
 */
@property (nonatomic, readwrite, copy) NSString *selectedDestinationAccountAccNumber;

/**
 * Provides read-only access to the array with the selectable currencies
 */
@property(nonatomic,readwrite,retain) NSArray *currencyList;

/**
 * Provides read-write access to the selected currency index
 */
@property(nonatomic,readwrite,assign) NSInteger selectedCurrencyIndex;

/**
 * Provides read-only access to the selected currency
 */
@property (nonatomic,readonly,copy) NSString *selectedCurrency;

/**
 * Provides readwrite access to the currencyTextField. Exported to IB
 */
@property (nonatomic, readwrite, retain) IBOutlet NXTCurrencyTextField *amountTextField;

/**
 * Designated initialized. Initializes a TransferBetweenUserAccounts instance with the initial transfer accounts response
 *
 * @param transferStartupResponse The transfer accounts response
 * @return The initialized TransferBetweenUserAccounts instance;
 */
- (id)initWithTransferStartupResponse:(TransferStartupResponse *)transferStartupResponse;

@end
