/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "TransferToThirdAccounts.h"

#import "AccountList.h"
#import "BankAccount.h"
#import "GlobalAdditionalInformation.h"
#import "NXTCurrencyTextField.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "TransferConfirmationAdditionalInformation.h"
#import "TransferConfirmationResponse.h"
#import "TransferStartupResponse.h"
#import "TransferSuccessResponse.h"
#import "TransferSuccessAdditionalInformation.h"
#import "Updater.h"

@interface TransferToThirdAccounts()

/**
 * Checks if the destination account is in the account list
 *
 * @param destinationAccountNumber The destination account number
 */
- (BOOL)checkDestinationAccount:(NSString *)destinationAccountNumber;

@end
    
#pragma mark -

@implementation TransferToThirdAccounts

#pragma mark -
#pragma mark Properties

@synthesize originAccountList = originAccountList_;
@synthesize selectedOriginAccountIndex = selectedOriginAccountIndex_;
@dynamic selectedOriginAccount;
@synthesize selectedDestinationAccountOffice = selectedDestinationAccountOffice_;
@synthesize selectedDestinationAccountAccNumber = selectedDestinationAccountAccNumber_;
@synthesize currencyList = currencyList_;
@synthesize selectedCurrencyIndex = selectedCurrencyIndex_;
@dynamic selectedCurrency;
@synthesize amountTextField = amountTextField_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [originAccountList_ release];
    originAccountList_ = nil;
    
    [selectedDestinationAccountOffice_ release];
    selectedDestinationAccountOffice_ = nil;
    
    [selectedDestinationAccountAccNumber_ release];
    selectedDestinationAccountAccNumber_ = nil;
    
    [currencyList_ release];
    currencyList_ = nil;
    
    [serverCurrencyList_ release];
    serverCurrencyList_ = nil;
    
    [additionalInformation_ release];
    additionalInformation_ = nil;
    
    [transferSuccessAdditionalInfo_ release];
    transferSuccessAdditionalInfo_ = nil;
    
    [amountTextField_ release];
    amountTextField_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initializarion

/**
 * Superclass designated initializer. Initializes an empty TransferBetweenUserAccounts instance
 *
 * @return An empty TransferBetweenUserAccounts instance
 */
- (id)init {
    
    return [self initWithTransferStartupResponse:nil];
    
}

/*
 * Designated initialized. Initializes a TransferBetweenUserAccounts instance with the initial transfer accounts response
 */
- (id)initWithTransferStartupResponse:(TransferStartupResponse *)transferStartupResponse {
    
    if ((self = [super init])) {
        
        // The initial operation in iPad returns elements no needed in iPhone
        originAccountList_ = [[NSMutableArray alloc] init];
        currencyList_ = [[NSArray alloc] initWithObjects:NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil),
                         NSLocalizedString(DOLLARS_CURRENCY_TEXT_KEY, nil),
                         nil];
        serverCurrencyList_ = [[NSArray alloc] initWithObjects:CURRENCY_SOLES_LITERAL,
                               CURRENCY_DOLARES_LITERAL,
                               nil];
        
        [self setCarrierList:transferStartupResponse.mobileCarriers];
    }
    


    return self;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Sets the selected origin account index and updates the destination account list
 *
 * @param selectedOriginAccountIndex The new selected origin account index
 */
- (void)setSelectedOriginAccountIndex:(NSInteger)selectedOriginAccountIndex {
    
    Session *session = [Session getInstance];
    
    if (selectedOriginAccountIndex != selectedOriginAccountIndex_) {
        
        selectedOriginAccountIndex_ = selectedOriginAccountIndex;
        
        [originAccountList_ removeAllObjects];
        [originAccountList_ addObjectsFromArray:session.accountList.transferAccountList];
    }
    
    if ((selectedOriginAccountIndex_ >= 0) && (selectedOriginAccountIndex_ < [originAccountList_ count])) {
        
        selectedOriginAccountIndex_ = selectedOriginAccountIndex;
        
    } else {
        
        selectedOriginAccountIndex_ = -1;
        
    }
            
}

/*
 * Returns the selected origin account
 *
 * @return The selected origin account
 */
- (BankAccount *)selectedOriginAccount {
    
    BankAccount *result = nil;
    
    if ((selectedOriginAccountIndex_ >= 0) && (selectedOriginAccountIndex_ < [originAccountList_ count])) {
        
        result = [originAccountList_ objectAtIndex:selectedOriginAccountIndex_];
        
    }
    
    return result;
    
}

/*
 * Sets the selected currency index
 */
- (void)setSelectedCurrencyIndex:(NSInteger)selectedCurrencyIndex {
    
    if(selectedCurrencyIndex != selectedCurrencyIndex_) {
        
        selectedCurrencyIndex_ = selectedCurrencyIndex;
        NSArray *currencyList = serverCurrencyList_;
        
        if((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [currencyList count])) {
            
            self.currency = [currencyList objectAtIndex:selectedCurrencyIndex_];
            
        } else {
            
            selectedCurrencyIndex_ = -1;
        }
    
    }
}

/*
 * Returns the selected currency
 *
 * @return The selected currency
 */
- (NSString *)selectedCurrency {
    
    NSString *result = nil;
    
    NSArray *currencyList = serverCurrencyList_;
    
    if((selectedCurrencyIndex_ >=0) && (selectedCurrencyIndex_ < [currencyList count])) {
        
        result = [currencyList objectAtIndex:selectedCurrencyIndex_];
    }
    
    return result;
}

#pragma mark -
#pragma mark Server operations management

- (BOOL)startFrequentOperationReactiveRequest
{
    

    BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];

    [[Updater getInstance] obtainTranferToThirdBankFrequentOperationReactiveStepOneWithOperation:additionalInformation_.operation andbeneficiaryAccount:additionalInformation_.beneficiaryAccount andBeneficiaryName:additionalInformation_.beneficiary andOriginAccount:originAccount.number andAmount:self.amountString andBeneficiaryCurrency:additionalInformation_.beneficiaryAccountCurrency andOriginCurrency:originAccount.currency];;
    
    return YES;
}

/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startTransferRequest {
    
    BOOL result = NO;
    BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    NSString *originAccountNumber = originAccount.number;
    
    NSString *amount = [Tools formatAmountWithDotDecimalSeparator:self.amountString]; 
    NSInteger selectedCurrencyIndex = selectedCurrencyIndex_;
    
    NSString *carrier1 = @"";
    NSString *carrier2 = @"";
    
    if (self.sendSMS) {
        
        if (self.showSMS1 && (self.selectedCarrier1Index >= 0)) {
            
            MobileCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier1Index];
            carrier1 = [carrier code];
            
        }
        
        if (self.showSMS2 && (self.selectedCarrier2Index >= 0)) {
            
            MobileCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier2Index];
            carrier2 = [carrier code]; 
            
        }
        
    }
    
    NSString *currencySelected = @"";
    
    if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {
        
        currencySelected = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];
        
    }

    if (([originAccountNumber length] > 0) && 
        ([selectedDestinationAccountOffice_ length] > 0) && 
        ([selectedDestinationAccountAccNumber_ length] > 0) && 
        ([amount length] > 0) &&
        (selectedCurrencyIndex > -1)) {
        
        [[Updater getInstance] transferToThirdAccountsConfirmationFromAccountType:originAccount.type
                                                                     fromCurrency:originAccount.currency
                                                                        fromIndex:originAccount.subject
                                                                         toBranch:selectedDestinationAccountOffice_
                                                                        toAccount:selectedDestinationAccountAccNumber_
                                                                           amount:amount
                                                                         currency:currencySelected 
                                                                        reference:[Tools notNilString:[self reference]]
                                                                           email1:[Tools notNilString:[self destinationEmail1]]
                                                                           email2:[Tools notNilString:[self destinationEmail2]]
                                                                           phone1:[Tools notNilString:[self destinationSMS1]]
                                                                         carrier1:[Tools notNilString:carrier1]
                                                                           phone2:[Tools notNilString:[self destinationSMS2]]
                                                                         carrier2:[Tools notNilString:carrier2]
                                                                       andMessage:[Tools notNilString:[self emailMessage]]];
        
        result = YES;
        
    }
    
    return result;
    
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startTransferRequestDataValidation {
	
	NSString *result = nil;

	if ([self selectedOriginAccountIndex] == NSNotFound || [self selectedOriginAccountIndex] < 0 || [self selectedOriginAccountIndex] > [[self originAccountList] count]) {
		
		result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
		
	} else {
		
		BankAccount *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
		NSString *originAccountNumber = [originAccount number];
        
        if (([selectedDestinationAccountOffice_ length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_OFFICE_TEXT_KEY, nil);
        
        } else if (([selectedDestinationAccountAccNumber_ length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_ACCNUMBER_TEXT_KEY, nil);
            
        } else if (([selectedDestinationAccountOffice_ length] != 4)) {
                
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_OFFICE_TEXT_KEY, nil);
            
        } else if (([selectedDestinationAccountAccNumber_ length] != 10)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_ACCNUMBER_TEXT_KEY, nil);
            
        } else {
		
            NSString *destinationAccountNumber = [NSString stringWithFormat:@"0011-%@-%@-%@", selectedDestinationAccountOffice_, [selectedDestinationAccountAccNumber_ substringToIndex:2], [selectedDestinationAccountAccNumber_ substringFromIndex:2]];
		
            if ([originAccountNumber isEqualToString:destinationAccountNumber]) {
                
                result = NSLocalizedString(TRANSFERS_TO_THIRD_ACCOUNTS_SAME_ACCOUNT_ERROR_KEY, nil);
                
            } else if ([self selectedCurrencyIndex] < 0) {
                
                result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
                
            } else {
                
                BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
                NSString *originAccountNumber = originAccount.number;
                
                NSString *currencySelected = @"";
                
                if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {
                    
                    currencySelected = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];
                    
                }
                
                if (!([originAccountNumber length] > 0)) {
                    
                    result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
                    
                } else if (!([currencySelected length] > 0)) {
                    
                    result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
                    
                } else if (([self.amountString isEqualToString:@"0.00"]) || ([self.amountString floatValue] == 0.0f)) {
                    
                    result = NSLocalizedString(TRANSFER_ERROR_AMOUNT_TEXT_KEY, nil);
                    
                }
                else {
                    result = [super startTransferRequestDataValidation];
                }
            }
        }
		
	}
    
    return result;

}


/*
 * Performs the payment confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (BOOL)startTransferConfirmationRequest {
    
    BOOL canStartProcess = YES;
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];

    if (self.canShowLegalTerms && !self.legalTermsAccepted) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_LEGAL_TERMS_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    } else if ((otpUsage == otp_UsageOTP) && ([[self secondFactorKey] length] == 0) && [[additionalInformation_.moduleActive lowercaseString] isEqualToString:@"n"]) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_OTP_KEY_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    } else if ((otpUsage == otp_UsageTC) && (([[self secondFactorKey] length] == 0) || ([[self secondFactorKey] length] < 3)) && [[additionalInformation_.moduleActive lowercaseString] isEqualToString:@"n"]) {
        
        [Tools showInfoWithMessage:NSLocalizedString(TRANSFER_ERROR_COORDINATES_TEXT_KEY, nil)];
        canStartProcess = NO;
        
    }
    
    if (canStartProcess) {
        
        [[Updater getInstance] transferToThirdAccountsResultFromSecondFactorKey:[self secondFactorKey]];
        
    }
    
    return canStartProcess;
    
}

/*
 * Notifies the transfer operation helper the confirmation response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        
        if ([confirmationResponse isKindOfClass:[TransferSuccessResponse class]]) {
            
            TransferSuccessResponse *response = (TransferSuccessResponse *)confirmationResponse;
            
            if (transferSuccessAdditionalInfo_ != nil) {
                [transferSuccessAdditionalInfo_ release];
                transferSuccessAdditionalInfo_ = nil;
            }
            transferSuccessAdditionalInfo_ = [response.additionalInformation retain];

            result = YES;
            
        }
        
    }
    
    return result;
    
}

/*
 * Notifies the transfer operation helper the tranfer response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)transferResponseReceived:(StatusEnabledResponse *)transferResponse {
    
    BOOL result = [super transferResponseReceived:transferResponse];

    if (!result) {
        
        if ([transferResponse isKindOfClass:[TransferConfirmationResponse class]]) {
                        
            TransferConfirmationResponse *response = (TransferConfirmationResponse *)transferResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response.additionalInformation retain];
        
            coordinateReference_ = [additionalInformation_ coordinate];
            sealReference_ = [additionalInformation_ seal];
            
            result = YES;
            
        }
        
    }
    
    return result;
    
}

/*
 * Checks if the destination account is in the account list
 */
- (BOOL)checkDestinationAccount:(NSString *)destinationAccountNumber {

    BOOL result = NO;
    
    BankAccount *account;
    NSInteger i = 0;
    NSInteger count = [self.originAccountList count];
    
    while (i < count && !result) {
    
        account = [self.originAccountList objectAtIndex:i];
        result = [[account number] isEqualToString:destinationAccountNumber];
        i++;
        
    }
    
    return result;

}


#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)transferSecondStepInformation {
    
    NSMutableArray *mutableResult = [NSMutableArray array];

    BankAccount *selectedOriginAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    
    // amount
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_TO_MY_ACCOUNTS_AMOUNT_TEXT_KEY, nil);
    
    NSString *currency = @"";
    
    if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {
        
        currency = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], self.amountString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    //Operation
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:additionalInformation_.operation];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    
    //origin
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORIGIN_TITLE_TEXT_KEY, nil);    
    
    NSString *originAccount = selectedOriginAccount.number;
    
    if ([originAccount length] > 0) {
        
        if ([selectedOriginAccount.accountType length] > 0) {
            
            originAccount = [NSString stringWithFormat:@"%@ - %@ - %@", selectedOriginAccount.accountType, [Tools getCurrencyLiteral:selectedOriginAccount.currency] , originAccount];
            
        }
        
    } else {
        
        originAccount = selectedOriginAccount.accountType;
        
    }    
    
    [titleAndAttributes addAttribute:originAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
        
    //destination
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_TITLE_TEXT_KEY, nil);  
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ - %@", 
                                      additionalInformation_.beneficiaryAccountCurrency, 
                                      additionalInformation_.beneficiaryAccount]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //beneficiary
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_ACCOUNT_OWNER_TEXT_KEY, nil);  
    
    [titleAndAttributes addAttribute:additionalInformation_.beneficiary];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //tax
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_TO_MY_ACCOUNTS_TAX_TEXT_KEY, nil);  
    
    NSString *marketPlaceCommision = additionalInformation_.marketPlaceCommision;
    NSString *currencySymbol = @"";
    
    if (([marketPlaceCommision length] == 0) || ([marketPlaceCommision isEqualToString:@"0.00"])) {
        
        marketPlaceCommision = @"0.00";
        
    } else {
        
        currencySymbol = [NSString stringWithFormat:@"%@ ", [Tools notNilString:[Tools getCurrencySimbol:selectedOriginAccount.currency]]];
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@%@", currencySymbol, marketPlaceCommision]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //reference
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_REFERENCE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:self.reference];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Exchange rate
    
    if (![Tools currency:selectedOriginAccount.currency isEqualToCurrency:currency]) {
        
        NSString *exchangeRateString = additionalInformation_.tipoCambio;
        
        if ([exchangeRateString length] > 0) {
            
            NSDecimalNumber *exchangeRate = [Tools decimalFromServerString:exchangeRateString];
            NSString *exchangeRateCurrency = [Tools getCurrencySimbol:additionalInformation_.monedaTipoCambio];
            
            if ([exchangeRateCurrency length] == 0) {
                
                exchangeRateCurrency = [Tools mainCurrencySymbol];
                
            }
            
            NSString *exchangeRateToDisplay = [Tools formatAmount:exchangeRate
                                                     withCurrency:exchangeRateCurrency
                                          currenctyPrecedesAmount:YES
                                                     decimalCount:4];
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
            if (titleAndAttributes != nil) {
                
                titleAndAttributes.titleString = NSLocalizedString(TRANSFER_EXCHANGE_RATE_TEXT_KEY, nil);
                [titleAndAttributes addAttribute:exchangeRateToDisplay];
                [mutableResult addObject:titleAndAttributes];
                
            }
            
        }
        
    }

    return [NSArray arrayWithArray:mutableResult];
    
}

/*
 * Creates the title and attributes array to display in the transfer third step. Default implementation returns an empty array
 */
- (NSArray *)transferThirdStepInformation {
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    BankAccount *selectedOriginAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    
    // Operation number
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(NUMBER_OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operationNumber];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operation];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Origin
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORIGIN_TITLE_TEXT_KEY, nil);
    
    NSString *originAccount = selectedOriginAccount.number;
    
    if ([originAccount length] > 0) {
        
        if ([selectedOriginAccount.accountType length] > 0) {
            
            originAccount = [NSString stringWithFormat:@"%@ - %@ - %@", selectedOriginAccount.accountType, [Tools getCurrencyLiteral:selectedOriginAccount.currency], originAccount];
            
        }
        
    } else {
        
        originAccount = selectedOriginAccount.accountType;
        
    }
    
    [titleAndAttributes addAttribute:originAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_TEXT_KEY, nil);
    
    NSString *currency = @"";
    
    if ((selectedCurrencyIndex_ >= 0) && (selectedCurrencyIndex_ < [serverCurrencyList_ count])) {
        
        currency = [serverCurrencyList_ objectAtIndex:selectedCurrencyIndex_];
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], self.amountString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Destination
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_TITLE_TEXT_KEY, nil);  
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ - %@", 
                                      additionalInformation_.beneficiaryAccountCurrency, 
                                      additionalInformation_.beneficiaryAccount]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Owner
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_ACCOUNT_OWNER_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:additionalInformation_.beneficiary];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Reference
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_REFERENCE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:self.reference];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Tax
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_TO_MY_ACCOUNTS_TAX_TEXT_KEY, nil);  
    
    NSString *marketPlaceCommision = additionalInformation_.marketPlaceCommision;
    NSString *currencySymbol = @"";
    
    if (([marketPlaceCommision length] == 0) || ([marketPlaceCommision isEqualToString:@"0.00"])) {
        
        marketPlaceCommision = @"0.00";
        
    } else {
        
        currencySymbol = [NSString stringWithFormat:@"%@ ", [Tools notNilString:[Tools getCurrencySimbol:selectedOriginAccount.currency]]];
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@%@", currencySymbol, marketPlaceCommision]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    NSString *destinationAccountCurrency = [[additionalInformation_ beneficiaryAccountCurrency] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    // Change type
    if (![Tools currency:selectedOriginAccount.currency isEqualToCurrency:destinationAccountCurrency]) {
        
        NSString *exchangeRateString = transferSuccessAdditionalInfo_.exchangeRate;
        
        if ([exchangeRateString length] > 0) {
            
            NSDecimalNumber *exchangeRate = [Tools decimalFromServerString:exchangeRateString];
            NSString *exchangeRateCurrency = [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currencyExchangeRate];
            
            if ([exchangeRateCurrency length] == 0) {
                
                exchangeRateCurrency = [Tools mainCurrencySymbol];
                
            }
            
            NSString *exchangeRateToDisplay = [Tools formatAmount:exchangeRate
                                                     withCurrency:exchangeRateCurrency
                                          currenctyPrecedesAmount:YES
                                                     decimalCount:4];
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
            if (titleAndAttributes != nil) {
                
                titleAndAttributes.titleString = NSLocalizedString(TRANSFER_EXCHANGE_RATE_TEXT_KEY, nil);
                [titleAndAttributes addAttribute:exchangeRateToDisplay];
                [mutableResult addObject:titleAndAttributes];
                
            }
            
        }
        
    }
    
    // Amount paid
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_PAID_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.amountChargedCurrencyString], transferSuccessAdditionalInfo_.amountChargedString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Amount charged
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_CHARGERD_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.amountPaidCurrencyString], transferSuccessAdditionalInfo_.amountPaidString]];
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Time
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_DATE_HOUR_SHORT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", transferSuccessAdditionalInfo_.dateString, transferSuccessAdditionalInfo_.hour]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    return mutableResult;
    
}


-(BOOL)canOmitOTP{
    return [[additionalInformation_.moduleActive uppercaseString] isEqualToString:@"S"];
}

/*
 * Returns the coordinateKeyTitle string. Base class returns an empry string
 *
 * @return The coordinateKeyTitle string
 */
- (NSString *)coordinateKeyTitle {
    
    return coordinateReference_;
    
}

/*
 * Returns the seal string. Base class returns an empry string
 *
 * @return The seal string
 */
- (NSString *)sealText {
    
    return sealReference_;
    
}

/*
 * Returns the confirmation message. The transfer between accounts localized string is returned
 *
 * @return The itf message
 */
- (NSString *)itfMessageSuccess {
    
    return transferSuccessAdditionalInfo_.itfMessage;
    
}


/*
 * Returns the confirmation message. The transfer between accounts localized string is returned
 *
 * @return The itf message
 */
- (NSString *)itfMessageConfirm{
    
    return additionalInformation_.itfMessage;
    
}

/**
 * Returns the transfer between user accounts operation type
 *
 * @return The transfer between user accounts operation type
 */
- (TransferTypeEnum)transferOperationType {
    
    return TTETransferToOtherUserAccount;
    
}

/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedTransferOperationTypeString {
    
    return NSLocalizedString(TRANSFER_TO_THIRD_ACCOUNTS_TEXT_1_KEY, nil);
    
}

/*
 * Returns the localized terms and conditions string. The transfer between accounts legal terms localized string is returned
 *
 * @return The localized transfer legal terms operation type string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return NSLocalizedString(TRANSFER_LEGAL_TERMS_KEY, nil);
    
}

/*
 * Returns the confirmation message. The transfer between accounts localized string is returned
 *
 * @return The confirmation message
 */
- (NSString *)confirmationMessage {
    
    return transferSuccessAdditionalInfo_.confirmationMessage;
    
}

/*
 * Returns the can show legal terms flag
 */
- (BOOL)canShowLegalTerms {
    
    return YES;
    
}

/*
 * Returns the can show email and sms
 */
- (BOOL)canSendEmailandSMS {
    
    return YES;
    
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
#if defined(SIMULATE_HTTP_CONNECTION)
    return [Tools notNilString:@"http://www.google.es"];
#else
    return [Tools notNilString:additionalInformation_.disclaimer];
#endif
    
}

@end
