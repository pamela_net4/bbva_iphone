/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import "StatusEnabledResponse.h"


@interface DateLegalTermsResponse : StatusEnabledResponse {
    /**
     * current date recived from the server
     */
    NSString *dateLegalTerms_;
    
    /**
     * FAQ URL received from server
     */
    NSString *faqURL_;
    
}

/**
 * Provides read-only access to the date legal terms
 */
@property (nonatomic, readonly, copy) NSString *dateLegalTerms;


/**
 * Provides read-only access to the FAQ URL
 */
@property (nonatomic, readonly, copy) NSString *faqURL;

@end
