//
//  TransferTypePaymentResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 10/1/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "TransferTypePaymentResponse.h"
#import "TransferConfirmationAdditionalInformation.h"

typedef enum {
    
    ttprxeas_AnalyzingTypeFlow = serxeas_ElementsCount, //!<Analyzing the additional information
    ttprxeas_AnalyzingCommissionN,
    ttprxeas_AnalyzingCommissionL,
    ttprxeas_AnalyzingTotalPayN,
    ttprxeas_AnalyzingTotalPayL,
    ttprxeas_AnalyzingDocument,
    ttprxeas_AnalyzingAdditionalInformation,
    ttprxeas_AnalyzingDisclaimer
    
} TransferTypePaymentResponseXMLElementAnalyzerState;

@interface TransferTypePaymentResponse()

- (void)clearTransferTypePaymentResponseData;

@end

@implementation TransferTypePaymentResponse
@synthesize typeFlow = typeFlow_;
@synthesize commissionN = commissionN_;
@synthesize commissionL = commissionL_;
@synthesize totalPayL = totalPayL_;
@synthesize totalPayN = totalPayN_;
@synthesize documents = documents_;
@synthesize additionalInformation = additionalInformation_;
@synthesize disclaimer = disclaimer_;
#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearTransferTypePaymentResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a TransferSuccessResponse instance providing the associated notification
 *
 * @return The initialized TransferSuccessResponse instance
 */
- (id)init {
    
    if ((self = [super init])) {
    }
    
    return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    [auxDocument_ release];
    auxDocument_ = nil;
    
    [documents_ release];
    documents_ = [[NSMutableArray alloc] init];
    
    [typeFlow_ release];
    typeFlow_ = nil;
    
    [commissionN_ release];
    commissionN_ = nil;
    
    [commissionL_ release];
    commissionL_ = nil;
    
    [totalPayN_ release];
    totalPayN_ = nil;
    
    [totalPayL_ release];
    totalPayL_ = nil;
    
    [additionalInformation_ release];
    additionalInformation_ =nil;
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (auxDocument_ != nil) {
        [documents_ addObject:auxDocument_];
        [auxDocument_ release];
        auxDocument_ = nil;
    }
    
    NSString* lname = [elementName lowercaseString];
    if ([lname isEqualToString: @"tipoflujo"]) {
        xmlAnalysisCurrentValue_ = ttprxeas_AnalyzingTypeFlow;
      
    } else if ([lname isEqualToString: @"comisionn"]) {
        
        xmlAnalysisCurrentValue_ = ttprxeas_AnalyzingCommissionN;

    }else if ([lname isEqualToString: @"comisionl"]) {
     
        xmlAnalysisCurrentValue_ = ttprxeas_AnalyzingCommissionL;
        
    } else if ([lname isEqualToString: @"totalpagarl"]) {
          xmlAnalysisCurrentValue_ = ttprxeas_AnalyzingTotalPayL;
    } else if ([lname isEqualToString: @"totalpagarn"]) {
        xmlAnalysisCurrentValue_ = ttprxeas_AnalyzingTotalPayN;
    } else if ([lname isEqualToString: @"disclaimer"]) {
        xmlAnalysisCurrentValue_ = ttprxeas_AnalyzingDisclaimer;
    } else if ([lname isEqualToString: @"documento"]) {
      
        analyzingDocuments_ = YES;
    } else if ([lname isEqualToString: @"e"] && analyzingDocuments_) {
        
        xmlAnalysisCurrentValue_ = ttprxeas_AnalyzingDocument;
        
        [auxDocument_ release];
        auxDocument_ = [[Document alloc] init];
        auxDocument_.openingTag = lname;
        [auxDocument_ setParentParseableObject:self];
        [parser setDelegate:auxDocument_];
        [auxDocument_ parserDidStartDocument:parser];
        
        
    } else if ([lname isEqualToString: @"confirmaciontransferencia"]) {
        
        xmlAnalysisCurrentValue_ = ttprxeas_AnalyzingAdditionalInformation;
        [additionalInformation_ release];
        additionalInformation_ = nil;
        additionalInformation_ = [[TransferConfirmationAdditionalInformation alloc] init];
        additionalInformation_.openingTag = lname;
        [additionalInformation_ setParentParseableObject:self];
        [parser setDelegate:additionalInformation_];
        [additionalInformation_ parserDidStartDocument:parser];
        
    }else {
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        if (auxDocument_ != nil) {
            [documents_ addObject:auxDocument_];
            [auxDocument_ release];
            auxDocument_ = nil;
        }
        
        if(xmlAnalysisCurrentValue_== ttprxeas_AnalyzingTypeFlow){
            NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            [typeFlow_ release];
            typeFlow_ = nil;
            typeFlow_ = [elementString copyWithZone:self.zone];

        }
        
        if (xmlAnalysisCurrentValue_ == ttprxeas_AnalyzingCommissionN) {
            
            NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            [commissionN_ release];
            commissionN_ = nil;
            commissionN_ = [elementString copyWithZone:self.zone];
            
        }
        
        if (xmlAnalysisCurrentValue_ == ttprxeas_AnalyzingCommissionL) {
            
            NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            [commissionL_ release];
            commissionL_ = nil;
            commissionL_ = [elementString copyWithZone:self.zone];
            
        }
        
        if (xmlAnalysisCurrentValue_ == ttprxeas_AnalyzingTotalPayL) {
            
            NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            [totalPayL_ release];
            totalPayL_ = nil;
            totalPayL_ = [elementString copyWithZone:self.zone];
            
        }
        
        
        if (xmlAnalysisCurrentValue_ == ttprxeas_AnalyzingTotalPayN) {
            
            NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            [totalPayN_ release];
            totalPayN_ = nil;
            totalPayN_ = [elementString copyWithZone:self.zone];
            
        }
        
        if (xmlAnalysisCurrentValue_ == ttprxeas_AnalyzingDisclaimer) {
            
            NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            [disclaimer_ release];
            disclaimer_ = nil;
            disclaimer_ = [elementString copyWithZone:self.zone];
            
        }
        
    
    
    }
    else{
        informationUpdated_ = soie_InfoAvailable;
        
        
        if(xmlAnalysisCurrentValue_ == ttprxeas_AnalyzingAdditionalInformation){
            [parser setDelegate:parentParseableObject_];
            parentParseableObject_ = nil;
        }

    }
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
}
/**
 * Sent by a parser object to provide its delegate with a string representing all or part of the characters of the current element.
 * Characters found are appended to the current element being analyzed
 *
 * @param parser A parser object
 * @param string A string representing the complete or partial textual content of the current element
 */
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    [super parser:parser foundCharacters:string];
}
/*
 * Clears the object data
 */
- (void)clearTransferTypePaymentResponseData {
    
    
    [additionalInformation_ release];
    additionalInformation_ = nil;
    
    [commissionN_ release];
    commissionN_ = nil;
    
    [commissionL_ release];
    commissionL_ = nil;
    
    [auxDocument_ release];
    auxDocument_ = nil;
    
    [totalPayL_ release];
    totalPayL_ = nil;
    
    [totalPayN_ release];
    totalPayN_ = nil;
    
    [disclaimer_ release];
    disclaimer_ = nil;
    
    
    
}

@end
