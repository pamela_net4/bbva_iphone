/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class Deposit;


/**
 * Contains the list of Deposits
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface DepositList : StatusEnabledResponse {
    
@private
	
	/**
	 * Array containing the deposit list
	 */
	NSMutableArray *depositList_;
		
    /**
	 * Dictionary containing the deposit dictionary for graphic sector sort
	 */
    NSMutableDictionary *depositDictionary_;
    
	/**
	 * Auxiliar Deposit instance for parsing
	 */
	Deposit *auxDeposit_;
}


/**
 * Provides read-only access to the deposits list
 */
@property (nonatomic, readonly, retain) NSArray *depositList;

/**
 * Provides read-only access to the deposits dictionary
 */
@property (nonatomic, readonly, retain) NSMutableDictionary *depositDictionary;

/**	
 * Provides read access to the number of deposits stored
 */
@property (nonatomic, readonly, assign) NSUInteger depositCount;


- (NSUInteger)depositDictionaryCount;


/**
 * Returns the Deposit located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the Deposit is located
 *
 * @return Deposit located at the given position, or nil if position is not valid
 */
- (Deposit *)depositAtPosition:(NSUInteger)aPosition;

/**
 * Returns the Deposit position inside the list
 *
 * @param  aDeposit Deposit object which position we are looking for
 *
 * @return Deposit position inside the list
 */
- (NSUInteger)depositPosition:(Deposit *)aDeposit;

/**
 * Returns the Deposit with a deposit Number, or nil if not valid
 *
 * @param aDepositNumber The Deposit number
 *
 * @return Deposit with this number, or nil if not valid
 */
- (Deposit *)depositFromDepositNumber:(NSString *)aDepositNumber;

/**
 * Updates the Deposit list from another Deposit list
 *
 * @param  aDepositList The DepositList to update from
 */
- (void)updateFrom:(DepositList *)aDepositList;

/**
 * Remove the contained data
 */
- (void)removeData;

/**
 * Returns the Deposit group
 *
 * @param deposit The Deposit search in groups
 *
 * @return group of deposit 
 */
-(NSInteger) getGroup:(Deposit *)deposit;

/**
 * Returns array with the cell used to separete the groups
 *
 * @return list of elements to divide the list in groups
 */
-(NSArray *) changeGroups;

/**
 * Returns the group number of deposit removing the divide groups 
 *
 * @param row recived from table to search in the list and obtain the group
 *
 * @return group deleting the divide cell between the groups.
 */
-(NSInteger)getGroupWithPosition:(NSInteger)row;

@end
