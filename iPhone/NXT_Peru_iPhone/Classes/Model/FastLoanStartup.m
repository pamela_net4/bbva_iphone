//
//  FastLoanStartup.m
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "FastLoanStartup.h"
#import "Tools.h"
#import "FastLoanTermList.h"
#import "AccountFastLoanList.h"
#import "EmailInfoList.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     flseas_AnalyzingCurrency = serxeas_ElementsCount, //!<Analyzing the FastLoanStartup currency
     flseas_AnalyzingCurrencySymbol , //!<Analyzing the FastLoanStartup currencySymbol
     flseas_AnalyzingAmount , //!<Analyzing the FastLoanStartup amount
     flseas_AnalyzingAmountFormated , //!<Analyzing the FastLoanStartup amountFormated
     flseas_AnalyzingAmountMin , //!<Analyzing the FastLoanStartup amountMin
     flseas_AnalyzingAmountMinFormated , //!<Analyzing the FastLoanStartup amountMinFormated
     flseas_AnalyzingAmountMax , //!<Analyzing the FastLoanStartup amountMax
     flseas_AnalyzingAmountMaxFormated , //!<Analyzing the FastLoanStartup amountMaxFormated
     flseas_AnalyzingAmountCommission , //!<Analyzing the FastLoanStartup amountCommission
     flseas_AnalyzingTea , //!<Analyzing the FastLoanStartup tea
     flseas_AnalyzingTeaFormated , //!<Analyzing the FastLoanStartup teaFormated
     flseas_AnalyzingTceaFormated , //!<Analyzing the FastLoanStartup tceaFormated
     flseas_AnalyzingTerm , //!<Analyzing the FastLoanStartup term
     flseas_AnalyzingTermMax , //!<Analyzing the FastLoanStartup termMax
     flseas_AnalyzingTermMin , //!<Analyzing the FastLoanStartup termMin
     flseas_AnalyzingDayOfPay , //!<Analyzing the FastLoanStartup dayOfPay
     flseas_AnalyzingQuotaFormated , //!<Analyzing the FastLoanStartup quotaFormated
     flseas_AnalyzingQuotaType , //!<Analyzing the FastLoanStartup quotaType
     flseas_AnalyzingFastLoanTermList , //!<Analyzing the FastLoanStartup fastLoanTermList
     flseas_AnalyzingAccountFastLoanList , //!<Analyzing the FastLoanStartup accountFastLoanList
     flseas_AnalyzingEmailInfoList , //!<Analyzing the FastLoanStartup emailInfoList

} FastLoanStartupXMLElementAnalyzerState;

@implementation FastLoanStartup

#pragma mark -
#pragma mark Properties

@synthesize currency = currency_;
@synthesize currencySymbol = currencySymbol_;
@synthesize amount = amount_;
@synthesize amountFormated = amountFormated_;
@synthesize amountMin = amountMin_;
@synthesize amountMinFormated = amountMinFormated_;
@synthesize amountMax = amountMax_;
@synthesize amountMaxFormated = amountMaxFormated_;
@synthesize amountCommission = amountCommission_;
@synthesize tea = tea_;
@synthesize teaFormated = teaFormated_;
@synthesize tceaFormated = tceaFormated_;
@synthesize term = term_;
@synthesize termMax = termMax_;
@synthesize termMin = termMin_;
@synthesize dayOfPay = dayOfPay_;
@synthesize quotaFormated = quotaFormated_;
@synthesize quotaType = quotaType_;
@synthesize fastLoanTermList = fastLoanTermList_;
@synthesize accountFastLoanList = accountFastLoanList_;
@synthesize emailInfoList = emailInfoList_;
;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [currency_ release];
    currency_ = nil;

    [currencySymbol_ release];
    currencySymbol_ = nil;

    [amount_ release];
    amount_ = nil;

    [amountFormated_ release];
    amountFormated_ = nil;

    [amountMin_ release];
    amountMin_ = nil;

    [amountMinFormated_ release];
    amountMinFormated_ = nil;

    [amountMax_ release];
    amountMax_ = nil;

    [amountMaxFormated_ release];
    amountMaxFormated_ = nil;

    [amountCommission_ release];
    amountCommission_ = nil;

    [tea_ release];
    tea_ = nil;

    [teaFormated_ release];
    teaFormated_ = nil;

    [tceaFormated_ release];
    tceaFormated_ = nil;

    [term_ release];
    term_ = nil;

    [termMax_ release];
    termMax_ = nil;

    [termMin_ release];
    termMin_ = nil;

    [dayOfPay_ release];
    dayOfPay_ = nil;

    [quotaFormated_ release];
    quotaFormated_ = nil;

    [quotaType_ release];
    quotaType_ = nil;

    [fastLoanTermList_ release];
    fastLoanTermList_ = nil;

    [accountFastLoanList_ release];
    accountFastLoanList_ = nil;

    [emailInfoList_ release];
    emailInfoList_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if(0==1){
                   //delete fix
        }        else if ([lname isEqualToString: @"monedamonto"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingCurrency;


        }
        else if ([lname isEqualToString: @"monedasimbolo"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingCurrencySymbol;


        }
        else if ([lname isEqualToString: @"importemonto"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAmount;


        }
        else if ([lname isEqualToString: @"importemonto_dis"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAmountFormated;


        }
        else if ([lname isEqualToString: @"importemontomin"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAmountMin;


        }
        else if ([lname isEqualToString: @"importemontomin_dis"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAmountMinFormated;


        }
        else if ([lname isEqualToString: @"importemontomax"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAmountMax;


        }
        else if ([lname isEqualToString: @"importemontomax_dis"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAmountMaxFormated;


        }
        else if ([lname isEqualToString: @"importecomision"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAmountCommission;


        }
        else if ([lname isEqualToString: @"tea"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingTea;


        }
        else if ([lname isEqualToString: @"tea_dis"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingTeaFormated;


        }
        else if ([lname isEqualToString: @"tcea_dis"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingTceaFormated;


        }
        else if ([lname isEqualToString: @"plazo"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingTerm;


        }
        else if ([lname isEqualToString: @"plazomax"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingTermMax;


        }
        else if ([lname isEqualToString: @"plazomin"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingTermMin;


        }
        else if ([lname isEqualToString: @"diapago"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingDayOfPay;


        }
        else if ([lname isEqualToString: @"valor_cuota_dis"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingQuotaFormated;


        }
        else if ([lname isEqualToString: @"tipocuota"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingQuotaType;


        }
        else if ([lname isEqualToString: @"lista_plazos"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingFastLoanTermList;
            [fastLoanTermList_ release];
            fastLoanTermList_ = nil;
            fastLoanTermList_ = [[FastLoanTermList alloc] init];
            fastLoanTermList_.openingTag = lname;
            [fastLoanTermList_ setParentParseableObject:self];
            [parser setDelegate:fastLoanTermList_];
            [fastLoanTermList_ parserDidStartDocument:parser];


        }
        else if ([lname isEqualToString: @"cuentas"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAccountFastLoanList;
            [accountFastLoanList_ release];
            accountFastLoanList_ = nil;
            accountFastLoanList_ = [[AccountFastLoanList alloc] init];
            accountFastLoanList_.openingTag = lname;
            [accountFastLoanList_ setParentParseableObject:self];
            [parser setDelegate:accountFastLoanList_];
            [accountFastLoanList_ parserDidStartDocument:parser];


        }
        else if ([lname isEqualToString: @"correos"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingEmailInfoList;
            [emailInfoList_ release];
            emailInfoList_ = nil;
            emailInfoList_ = [[EmailInfoList alloc] init];
            emailInfoList_.openingTag = lname;
            [emailInfoList_ setParentParseableObject:self];
            [parser setDelegate:emailInfoList_];
            [emailInfoList_ parserDidStartDocument:parser];


        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(0==1){
           //delete fix
        }        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingCurrency) {

            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingCurrencySymbol) {

            [currencySymbol_ release];
            currencySymbol_ = nil;
            currencySymbol_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingAmount) {

            [amount_ release];
            amount_ = nil;
            amount_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingAmountFormated) {

            [amountFormated_ release];
            amountFormated_ = nil;
            amountFormated_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingAmountMin) {

            [amountMin_ release];
            amountMin_ = nil;
            amountMin_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingAmountMinFormated) {

            [amountMinFormated_ release];
            amountMinFormated_ = nil;
            amountMinFormated_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingAmountMax) {

            [amountMax_ release];
            amountMax_ = nil;
            amountMax_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingAmountMaxFormated) {

            [amountMaxFormated_ release];
            amountMaxFormated_ = nil;
            amountMaxFormated_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingAmountCommission) {

            [amountCommission_ release];
            amountCommission_ = nil;
            amountCommission_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingTea) {

            [tea_ release];
            tea_ = nil;
            tea_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingTeaFormated) {

            [teaFormated_ release];
            teaFormated_ = nil;
            teaFormated_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingTceaFormated) {

            [tceaFormated_ release];
            tceaFormated_ = nil;
            tceaFormated_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingTerm) {

            [term_ release];
            term_ = nil;
            term_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingTermMax) {

            [termMax_ release];
            termMax_ = nil;
            termMax_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingTermMin) {

            [termMin_ release];
            termMin_ = nil;
            termMin_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingDayOfPay) {

            [dayOfPay_ release];
            dayOfPay_ = nil;
            dayOfPay_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingQuotaFormated) {

            [quotaFormated_ release];
            quotaFormated_ = nil;
            quotaFormated_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingQuotaType) {

            [quotaType_ release];
            quotaType_ = nil;
            quotaType_ = [elementString copyWithZone:self.zone];

        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

