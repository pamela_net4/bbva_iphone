/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StructuredXMLParser.h"


#pragma mark -

/**
 * StructuredXMLParser privare extension
 */
@interface StructuredXMLParser()

/**
 * Sets the parent namespaces prefixes dictionary
 *
 * @param aParentNamespacesPrefixesDictionary The parent namespaces prefixes dictionary
 * @private
 */
- (void)setParentNamespacesPrefixesDictionary:(NSDictionary *)aParentNamespacesPrefixesDictionary;

/**
 * Sets the own namespaces prefixes dictionary
 *
 * @param ownNamespacesPrefixesDictionary The own namespaces prefixes dictionary
 * @private
 */
- (void)setOwnNamespacesPrefixesDictionary:(NSDictionary *)ownNamespacesPrefixesDictionary;

@end


#pragma mark -

@implementation StructuredXMLParser

#pragma mark -
#pragma mark Properties

@dynamic ownNamespacesPrefixesDictionary;
@dynamic wholeNamespacesPrefixesDictionary;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
	
    [customParser_ release];
    customParser_ = nil;
    
	[auxString_ release];
	auxString_ = nil;
    
    [parsedInformation_ release];
    parsedInformation_ = nil;
    
    [attributesDictionary_ release];
    attributesDictionary_ = nil;
    
    [ownNamespacesPrefixesDictionary_ release];
    ownNamespacesPrefixesDictionary_ = nil;
    
    [parentNamespacesPrefixesDictionary_ release];
    parentNamespacesPrefixesDictionary_ = nil;
    
    [parsingNamespacesPrefixesDictionary_ release];
    parsingNamespacesPrefixesDictionary_ = nil;

	[super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initialiazer. It initializes a SAXToDOMXMLParser without parent or own namespaces prefixes
 *
 * @return The initialized SAXToDOMXMLParser instance
 */
- (id)init {
    
    return [self initWithParentNamespacePrefixesDictionary:nil
                           ownNamespacesPrefixesDictionary:nil];
    
}

/*
 * Designated intialized. It initializes a StructuredXMLParser instance with its parent namespace prefixes dictionary
 */
- (id)initWithParentNamespacePrefixesDictionary:(NSDictionary *)aParentNamespacesPrefixesDictionary
                ownNamespacesPrefixesDictionary:(NSDictionary *)ownNamespacesPrefixesDictionary {
    
	if ((self = [super init])) {
        
        NSZone *zone = self.zone;
        initialized_ = NO;
        depthCounter_ = 0;
		auxString_ = [[NSMutableString allocWithZone:zone] init];
        parsedInformation_ = [[NSMutableDictionary allocWithZone:zone] init];
        attributesDictionary_ = [[NSMutableDictionary allocWithZone:zone] init];
        ownNamespacesPrefixesDictionary_ = [[NSMutableDictionary allocWithZone:zone] initWithDictionary:ownNamespacesPrefixesDictionary];
        parentNamespacesPrefixesDictionary_ = [[NSDictionary allocWithZone:zone] initWithDictionary:aParentNamespacesPrefixesDictionary];
        parsingNamespacesPrefixesDictionary_ = [[NSMutableDictionary allocWithZone:zone] init];
        
	}
	
	return self;
    
}

#pragma mark -
#pragma mark Driving the parsing

/*
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Default implementation returns BXPOElementUnknown
 */
- (BXPOElementType)elementType:(NSString *)anElement
                  namespaceURI:(NSString *)namespaceURI {
    
    return BXPOElementUnknown;
    
}

/*
 * Asks the subclasses for a custom element parser. The previous call to elementType: must have returned BXPOElementCustom. Subclasses must override this selector to
 * provide the base class with the addecuate information. Default implementation returns nil
 */
- (StructuredXMLParser *)parserForElement:(NSString *)anElement
                             namespaceURI:(NSString *)namespaceURI {
    
    return nil;
    
}


#pragma mark -
#pragma mark Reseting information

/*
 * Resets the parser, releasing the content
 */
- (void)resetParser {
    
    initialized_ = NO;
    
    [customParser_ release];
    customParser_ = nil;
    
    [parsedInformation_ removeAllObjects];
    [attributesDictionary_ removeAllObjects];
    
}

#pragma mark -
#pragma mark Utility selectors

/*
 * Checks whether the string is a valid element name.
 */
+ (BOOL)isValidElementName:(NSString *)anElementString {
    
    BOOL result = NO;
    
    if ([anElementString length] > 0) {
        
        //TODO:Implement
        result = YES;
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Accessing information

/*
 * Returns whether it is necesary to analyze the namespaces URI or not. When YES is returned, namespaces URIs are analyzed, when NO, namespaces URIs
 * are not analyzed. This information is only used while parsing. When parsing an XML document, the top parser value for this flag determines the
 * behaviour for the rest of parsers. Default implementation returns NO.
 */
- (BOOL)analyzeNamespacesURIs {
    
    return NO;
    
}

/*
 * Returns the attribute with the given name.attributesDictionary_
 */
- (NSString *)attributeWithName:(NSString *)anAttributeName {
    
    return [attributesDictionary_ objectForKey:anAttributeName];
    
}

/*
 * Returns the array of parsed objects (either NSString or custom StructuredXMLParser) for a given element. If the element doesn't exist, the array returned is nil
 */
- (NSArray *)arrayForElement:(NSString *)anElement
                namespaceURI:(NSString *)namespaceURI {
    
    NSArray *result = nil;
    
    NSMutableDictionary *mutableDictionary = [parsedInformation_ objectForKey:namespaceURI];
    NSMutableArray *mutableResult = [mutableDictionary objectForKey:anElement];
    
    if (mutableResult != nil) {
        
        result = [NSArray arrayWithArray:mutableResult];
        
    }
    
    return result;
    
}

/*
 * Returns the parsed objet (either NSString or custom StructuredXMLParser) for the given element at the given array index. If the element doesn't exist or the index is out of bounds, the
 * object returned is nil
 */
- (NSObject *)objectForElement:(NSString *)anElement
                  namespaceURI:(NSString *)namespaceURI
                       atIndex:(NSUInteger)anIndex {
    
    NSObject *result = nil;
    
    NSMutableDictionary *mutableDictionary = [parsedInformation_ objectForKey:namespaceURI];
    NSMutableArray *parsedArray = [mutableDictionary objectForKey:anElement];
    
    if ([parsedArray count] > anIndex) {
        
        result = [parsedArray objectAtIndex:anIndex];
        
    }
    
    return result;
    
}

/*
 * Sets the parent namespaces prefixes dictionary
 */
- (void)setParentNamespacesPrefixesDictionary:(NSDictionary *)aParentNamespacesPrefixesDictionary {
    
    if (parentNamespacesPrefixesDictionary_ != aParentNamespacesPrefixesDictionary) {
        
        [parentNamespacesPrefixesDictionary_ release];
        parentNamespacesPrefixesDictionary_ = nil;
        parentNamespacesPrefixesDictionary_ = [[NSDictionary allocWithZone:self.zone] initWithDictionary:aParentNamespacesPrefixesDictionary];
        
    }
    
}

/*
 * Sets the own namespaces prefixes dictionary
 */
- (void)setOwnNamespacesPrefixesDictionary:(NSDictionary *)ownNamespacesPrefixesDictionary {
    
    [ownNamespacesPrefixesDictionary_ removeAllObjects];
    [ownNamespacesPrefixesDictionary_ setDictionary:ownNamespacesPrefixesDictionary];

}

/*
 * Sets the string for an element with a given namespace at the given position. Previously existing element in that position is released.
 * When position is out of bounds, NSNull instances are inserted
 */
- (void)setElementObject:(NSString *)elementObject
              forElement:(NSString *)element
        withNamespaceURI:(NSString *)namespaceURI
                 atIndex:(NSUInteger)index {
    
    if ((elementObject != nil) && (([elementObject isKindOfClass:[NSString class]]) || ([elementObject isKindOfClass:[StructuredXMLParser class]])) &&
        (element != nil) && ([StructuredXMLParser isValidElementName:element]) && (namespaceURI != nil)) {
        
        NSMutableDictionary *elementsDictionary = [parsedInformation_ objectForKey:namespaceURI];
        
        if (elementsDictionary == nil) {
            
            elementsDictionary = [NSMutableDictionary dictionary];
            [parsedInformation_ setObject:elementsDictionary
                                   forKey:namespaceURI];
            
        }
        
        NSMutableArray *elementsArray = [elementsDictionary objectForKey:element];
        
        if (elementsArray == nil) {
            
            elementsArray = [NSMutableArray array];
            [elementsDictionary setObject:elementsArray
                                   forKey:element];
            
        }
        
        NSUInteger elementsCount = [elementsArray count];
        Class emptyInstanceClass = nil;
        
        if (elementsCount == 0) {
            
            emptyInstanceClass = [elementObject class];
            
        } else {
            
            NSObject *firstStoredObject = [elementsArray objectAtIndex:0];
            emptyInstanceClass = [firstStoredObject class];
            
        }
        
        if ([elementObject isMemberOfClass:emptyInstanceClass]) {
            
            if (index >= elementsCount) {
                
                NSObject *emptyObject = nil;
                
                if ([emptyInstanceClass isSubclassOfClass:[NSString class]]) {
                    
                    emptyObject = @"";

                } else {
                    
                    emptyObject = [[[emptyInstanceClass allocWithZone:self.zone] init] autorelease];
                    
                }
                
                for (NSInteger i = (index - elementsCount + 1); i > 0; i--) {
                    
                    [elementsArray addObject:emptyObject];
                    
                }
                
            }
            
            if (index < [elementsArray count]) {
                
                [elementsArray replaceObjectAtIndex:index
                                         withObject:elementObject];
                
            }
            
        }
        
    }
    
}

/*
 * Sets an array of elements with a given namespace. Previously existing elements in the array are removed. All objects must be the same instance
 */
- (void)setElementArray:(NSArray *)elementArray
             forElement:(NSString *)element
       withNamespaceURI:(NSString *)namespaceURI {
    
    if (([StructuredXMLParser isValidElementName:element]) && (elementArray != nil)) {
        
        NSMutableDictionary *namespaceDictionary = [parsedInformation_ objectForKey:namespaceURI];
        NSMutableArray *storedElementArray = [namespaceDictionary objectForKey:element];
        [storedElementArray removeAllObjects];
        
        NSUInteger index = 0;
        
        for (NSObject *elementObject in elementArray) {
            
            [self setElementObject:elementObject
                        forElement:element
                  withNamespaceURI:namespaceURI
                           atIndex:index];
            
            index++;
            
        }
        
    }
    
}

/*
 * Removes a given parser associated to the given element name
 */
- (void)removeParser:(StructuredXMLParser *)anAssociatedParser
         fromElement:(NSString *)anElement
        namespaceURI:(NSString *)namespaceURI {
    
    NSMutableDictionary *namespaceDictionary = [parsedInformation_ objectForKey:namespaceURI];
    
    if (namespaceDictionary != nil) {
        
        NSMutableArray *mutableArray = [namespaceDictionary objectForKey:anElement];
        [mutableArray removeObject:anAssociatedParser];
        
        if ([mutableArray count] == 0) {
            
            [namespaceDictionary removeObjectForKey:anElement];
            
            if ([[namespaceDictionary allKeys] count] == 0) {
                
                [parsedInformation_ removeObjectForKey:namespaceURI];
                
            }
            
        }
        
    }

}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it starts parsing a document. Resets the auxiliary string used to accumulate characters found while parsing
 *
 * @param parser The parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
	initialized_ = YES;
    
    [auxString_ setString:@""];
    
    [customParser_ release];
    customParser_ = nil;
    
    [parsedInformation_ removeAllObjects];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next parser is obtained and depth counter is updated
 *
 * @param parser The parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    if (initialized_) {
        
        if (elementType_ == BXPOElementIdle) {
            
            depthCounter_ = 1;
            elementType_ = [self elementType:elementName
                                namespaceURI:namespaceURI];
            
            if (elementType_ != BXPOElementString) {
                
                if (elementType_ == BXPOElementCustom) {
                    
                    [customParser_ release];
                    customParser_ = nil;
                    customParser_ = [[self parserForElement:elementName
                                               namespaceURI:namespaceURI] retain];
                    [customParser_ parser:parser
                          didStartElement:elementName
                             namespaceURI:namespaceURI
                            qualifiedName:qualifiedName
                               attributes:attributeDict];
                    
                    if ([customParser_ isKindOfClass:[StructuredXMLParser class]]) {
                        
                        [(StructuredXMLParser *)customParser_ setParentNamespacesPrefixesDictionary:self.wholeNamespacesPrefixesDictionary];
                        [(StructuredXMLParser *)customParser_ setOwnNamespacesPrefixesDictionary:parsingNamespacesPrefixesDictionary_];

                    }
                    
                }
                
            } else if (elementType_ == BXPOElementString) {
                
                [auxString_ setString:@""];
                
            }
            
        } else {
            
            depthCounter_ ++;
            
            if (elementType_ == BXPOElementCustom) {
                
                [customParser_ parser:parser
                      didStartElement:elementName
                         namespaceURI:namespaceURI
                        qualifiedName:qualifiedName
                           attributes:attributeDict];
                
            }
            
        }
        
        [parsingNamespacesPrefixesDictionary_ removeAllObjects];

    } else {
        
        [self resetParser];
        [attributesDictionary_ setDictionary:attributeDict];
        initialized_ = YES;
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. If opening tag found, control is returned to the parent parseable object
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    BOOL deleteCurrentParser = NO;
    
    NSObject *storingObject = nil;
    
    if (elementType_ == BXPOElementCustom) {
        
        [customParser_ parser:parser
                didEndElement:elementName
                 namespaceURI:namespaceURI
                qualifiedName:qName];
        
    }
    
    depthCounter_ --;
    
    if (depthCounter_ == 0) {
        
        if (elementType_ == BXPOElementString) {
            
            if (depthCounter_ == 0) {
                
                storingObject = [auxString_ stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
            }
            
        } else if (elementType_ == BXPOElementCustom) {
            
            storingObject = customParser_;
            deleteCurrentParser = YES;
            
        }
        
    }
    
    if (storingObject != nil) {
        
        NSString *namespace = namespaceURI;
        
        if (namespace == nil) {
            
            namespace = @"";
            
        }
        
        NSMutableDictionary *namespaceDictionary = [parsedInformation_ objectForKey:namespace];
        
        if (namespaceDictionary == nil) {
            
            namespaceDictionary = [NSMutableDictionary dictionary];
            
            if (namespaceDictionary != nil) {
                
                [parsedInformation_ setObject:namespaceDictionary
                                       forKey:namespace];
                
            }
            
        }
                
        NSMutableArray *currentArray = [namespaceDictionary objectForKey:elementName];
        
        if (currentArray == nil) {
            
            currentArray = [NSMutableArray array];
            
            if (currentArray != nil) {
                
                [namespaceDictionary setObject:currentArray
                                        forKey:elementName];
                
            }
            
        }
        
        [currentArray addObject:storingObject];
        
    }
    
    if (deleteCurrentParser) {
        
        [customParser_ release];
        customParser_ = nil;
        
    }
    
    if (depthCounter_ == 0) {
        
        elementType_ = BXPOElementIdle;
        
    }
	
}

/**
 * Sent by a parser object to its delegate the first time it encounters a given namespace prefix, which is mapped to a URI.
 * The parser namespace prefix is stored in the local namespaces
 *
 * @param parser A parser object
 * @param prefix A string that is a namespace prefix
 * @param namespaceURI A string that specifies a namespace URI
 */
- (void)parser:(NSXMLParser *)parser didStartMappingPrefix:(NSString *)prefix toURI:(NSString *)namespaceURI {
    
    if (([prefix length] > 0) && ([namespaceURI length] > 0)) {
        
        if (![[parsingNamespacesPrefixesDictionary_ allKeys] containsObject:prefix]) {
            
            [parsingNamespacesPrefixesDictionary_ setObject:namespaceURI
                                                     forKey:prefix];
            
        }
        
    }
    
}

/**
 * Sent by a parser object to provide its delegate with a string representing all or part of the characters of the current element.
 * Characters found are appended to the auxiliary string or propagated to the custom parser
 *
 * @param parser A parser object
 * @param string A string representing the complete or partial textual content of the current element
 */
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if (elementType_ == BXPOElementString) {
        
        [auxString_ appendString:string];
        
    } else if (elementType_ == BXPOElementCustom) {
        
        [customParser_ parser:parser
              foundCharacters:string];
        
    }
	
}

/**
 * Reported by a parser object to provide its delegate with a string representing all or part of the ignorable whitespace characters of
 * the current element. When analyzing with a custom parser, it is propagated to the custom parser
 *
 * @param parser A parser object
 * @param whitespaceString A string representing all or part of the ignorable whitespace characters of the current element
 */
- (void)parser:(NSXMLParser *)parser foundIgnorableWhitespace:(NSString *)whitespaceString {
    
    if (elementType_ == BXPOElementCustom) {
        
        [customParser_ parser:parser foundIgnorableWhitespace:whitespaceString];
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters a CDATA block. When analyzing with a custom parser, it is propagated to the
 * custom parser
 *
 * @param parser An NSXMLParser object parsing XML
 * @param CDATABlock A data object containing a block of CDATA
 */
- (void)parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock{
    
    if (elementType_ == BXPOElementCustom) {
        
        [customParser_ parser:parser
                   foundCDATA:CDATABlock];
        
    }
    
}

#pragma mark -
#pragma mark NSCopying protocol selectors

/**
 * Returns a new instance that’s a copy of the receiver. The correct subclass is created
 *
 * @param zone The zone identifies an area of memory from which to allocate for the new instance
 * @return The returned object is implicitly retained by the sender, who is responsible for releasing it
 */
- (id)copyWithZone:(NSZone *)zone {
    
    Class class = [self class];
    
    StructuredXMLParser *result = [[class allocWithZone:zone] init];
    
    [result setOwnNamespacesPrefixesDictionary:ownNamespacesPrefixesDictionary_];
    [result setParentNamespacesPrefixesDictionary:parentNamespacesPrefixesDictionary_];
    [result->attributesDictionary_ addEntriesFromDictionary:attributesDictionary_];
    [result->auxString_ appendString:auxString_];
    
    NSString *namespaceURI = nil;
    NSString *elementName = nil;
    
    NSArray *allNamespaces = [parsedInformation_ allKeys];
    NSMutableDictionary *elementsDictionary = nil;
    NSArray *allElementNames = nil;
    NSMutableArray *elementsArray = nil;
    NSObject* elementObject = nil;
    NSUInteger index;
    
    for (namespaceURI in allNamespaces) {
        
        elementsDictionary = [parsedInformation_ objectForKey:namespaceURI];
        
        allElementNames = [elementsDictionary allKeys];
        
        for (elementName in allElementNames) {
            
            elementsArray = [elementsDictionary objectForKey:elementName];
            
            index = 0;

            for (elementObject in elementsArray) {
                
                if ([elementObject conformsToProtocol:@protocol(NSCopying)]) {// [elementObject respondsToSelector:@selector(copyWithZone:)]) {
                    
//                    elementObject = [[elementObject performSelector:@selector(copyWithZone:)
//                                                         withObject:(id)zone] autorelease];
                    elementObject = [[(id<NSCopying>)elementObject copyWithZone:zone] autorelease];
                    
                }
                
                [result setElementObject:elementObject
                              forElement:elementName
                        withNamespaceURI:namespaceURI
                                 atIndex:index];
                
                index++;
                
            }
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the element string (if any)
 *
 * @return The element string (if any)
 */
- (NSString *)string {
    
    return [NSString stringWithString:auxString_];
    
}

/*
 * Returns the attributes dictionary
 *
 * @return The attributes dictionary
 */
- (NSDictionary *)attributesDictionary {
    
    return [NSDictionary dictionaryWithDictionary:attributesDictionary_];
    
}

/*
 * Returns the own namespaces prefixes dictionary
 *
 * @return The own namespaces prefixes dictionary
 */
- (NSDictionary *)ownNamespacesPrefixesDictionary {
    
    return [NSDictionary dictionaryWithDictionary:ownNamespacesPrefixesDictionary_];
    
}

/*
 * Returns the whole namespaces prefixes dictionary
 *
 * @return The whole namespaces prefixes dictionary
 */
- (NSDictionary *)wholeNamespacesPrefixesDictionary {
    
    NSMutableDictionary *mutableResult = [NSMutableDictionary dictionary];
    
    if (parentNamespacesPrefixesDictionary_ != nil) {
        
        [mutableResult addEntriesFromDictionary:parentNamespacesPrefixesDictionary_];
        
    }
    
    if (ownNamespacesPrefixesDictionary_ != nil) {
        
        [mutableResult addEntriesFromDictionary:ownNamespacesPrefixesDictionary_];
        
    }
    
    return [NSDictionary dictionaryWithDictionary:mutableResult];
    
}

@end
