//
//  FOInscriptionBaseProcess.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 14/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FOInscriptionBaseProcess.h"
#import "StringKeys.h"
#import "Carrier.h"
#import "Tools.h"
#import "Session.h"
#import "GlobalAdditionalInformation.h"
#import "TitleAndAttributes.h"
#import "FORCashMobileStepOneResponse.h"
#import "FOROtherBankTransferStepOneResponse.h"
#import "FORThirdAccountTransferStepOneResponse.h"
#import "FORRechargeGiftCardStepOneResponse.h"
#import "FORPaymentCOtherBankStepOneResponse.h"
#import "FORCashMobileStepTwoResponse.h"
#import "FOROtherBankTransferStepTwoResponse.h"
#import "FORThirdAccountTransferStepTwoResponse.h"
#import "FORRechargeGiftCardStepTwoResponse.h"
#import "FORPaymentCOtherBankStepTwoResponse.h"
#import "FORThirdCardPaymentStepTwoResponse.h"
#import "FORCashMobileStepThreeResponse.h"
#import "FOROtherBankTransferStepThreeResponse.h"
#import "FORThirdAccountTransferStepThreeResponse.h"
#import "FORRechargeGiftCardStepThreeResponse.h"
#import "FORPaymentCOtherBankStepThreeResponse.h"
#import "FORThirdCardPaymentStepThreeResponde.h"
#import "FORThirdCardPaymentStepOneResponse.h"
#import "FORRechargeGiftCardStepTwoResponse.h"
#import "FORRechargeGiftCardStepThreeResponse.h"
#import "FORInstitutionPaymentStepOneResponse.h"
#import "FORInstitutionPaymentStepTwoResponse.h"
#import "FORInstitutionPaymentStepThreeResponse.h"
#import "FORRechargeCellphoneStepOneResponse.h"
#import "FORRechargeCellphoneStepTwoResponse.h"
#import "FORRechargeCellphoneStepThreeResponse.h"
#import "FORServicePaymentStepOneResponse.h"
#import "FORServicePaymentStepThreeResponse.h"
#import "FORServicePaymentStepTwoResponse.h"
#import "FOEmailList.h"
#import "FOEmail.h"
#import "FOPhoneList.h"
#import "FOPhone.h"
#import "Channel.h"
#import "ChannelList.h"
#import "Updater.h"

@implementation FOInscriptionBaseProcess

@synthesize service = service_;
@synthesize rootViewController = rootViewController_;
@synthesize confirmationInfoArray = confirmationInfoArray_;
@synthesize successInfoArray = successInfoArray_;
@synthesize dataInfoArray = dataInfoArray_;
@synthesize coordHint = coordHint_;
@synthesize secondFactorKey = secondFactorKey_;
@synthesize seal = seal_;
@synthesize delegate = delegate_;
@synthesize sendEmail = sendEmail_;
@synthesize sendSMS = sendSMS_;
@synthesize selectedEmailIndex = selectedEmailIndex_;
@synthesize selectedPhoneNumberIndex = selectedPhoneNumberIndex_;
@synthesize emailMessage = emailMessage_;
@synthesize legalTermsAccepted = legalTermsAccepted_;
@synthesize legalTermsURL = legalTermsURL_;
@synthesize message = message_;
@synthesize largeAlias = largeAlias_;
@synthesize shortAlias = shortAlias_;
@synthesize emailArray = emailArray_;
@synthesize phoneNumberArray = phoneNumberArray_;
@synthesize channelArray = channelArray_;
@synthesize notificationEnabled = notificationsEnabled_;
@synthesize dayOfMonthNotification = dayOfMonthNotification_;
@synthesize selectedChannels = selectedChannels_;
@synthesize successMessage = successMessage_;
@synthesize phoneDisclaimer = phoneDisclaimer_;
@synthesize emailDisclaimer = emailDisclaimer_;

- (void)dealloc {
    
    [dataInfoArray_ release];
    dataInfoArray_ = nil;
    
    [confirmationInfoArray_ release];
    confirmationInfoArray_ = nil;
    
    [successInfoArray_ release];
    successInfoArray_ = nil;
    
    [coordHint_ release];
    coordHint_ = nil;
    
    [secondFactorKey_ release];
    secondFactorKey_ = nil;
    
    [seal_ release];
    seal_ = nil;
    
    
    [emailMessage_ release];
    emailMessage_ = nil;
    
    [legalTermsURL_ release];
    legalTermsURL_ = nil;
    
    [message_ release];
    message_ = nil;
    
    [shortAlias_ release];
    shortAlias_ = nil;
    
    [largeAlias_ release];
    largeAlias_ = nil;
    
    [channelArray_ release];
    channelArray_ = nil;
    
    [selectedChannels_ release];
    selectedChannels_ = nil;
    
    [emailArray_ release];
    emailArray_ = nil;
    
    [phoneNumberArray_ release];
    phoneNumberArray_ = nil;
    
    [successMessage_ release];
    successMessage_ = nil;
    
    dayOfMonthNotification_ = nil;
    
    notificationsEnabled_ = nil;
    
    [rootViewController_ release];
    rootViewController_ = nil;
    
    [super dealloc];
    
}

- (void)resetSMSAndEmailInformation {
    
    [self setSendEmail:NO];
    [self setSendSMS:NO];
    [self setNotificationEnabled:NO];
    
}

- (void)removeObservers{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORPaymentGiftCardStepOne object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORCashMobileStepOne object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFOROtherBankStepOne object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORThirdAccountStepOne object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORPaymentThirdCardStepOne object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORPaymentOtherBankStepOne object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORInstitutionAndCompaniesStepOne object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORRechargeMovistarStepOne object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORRechargeClaroStepOne object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORServiceClaroStepOne object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORServiceMovistarStepOne object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORServiceWaterStepOne object:nil];
    
    
}

- (id)init {
    
    if (self = [super init]) {
        
        dataInfoArray_ = [[NSMutableArray alloc] init];
        emailArray_ = [[NSMutableArray alloc] init];
        phoneNumberArray_ = [[NSMutableArray alloc] init];
        channelArray_ = [[NSMutableArray alloc] init];
        shortAlias_ = @"";
        largeAlias_ = @"";
		successInfoArray_ = [[NSMutableArray alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseFORPaymentGiftCardStepOneReceived:) name:kNotificationFORPaymentGiftCardStepOne object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseFORCashMobileStepOneReceived:) name:kNotificationFORCashMobileStepOne object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseFOROtherBankStepOneReceived:) name:kNotificationFOROtherBankStepOne object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseFORThirdAccountStepOneReceived:) name:kNotificationFORThirdAccountStepOne object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseFORPaymentThirdCardStepOneReceived:) name:kNotificationFORPaymentThirdCardStepOne object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseFORPaymentOtherBankStepOneReceived:) name:kNotificationFORPaymentOtherBankStepOne object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseFORInstitutionAndCompaniesStepOneReceived:) name:kNotificationFORInstitutionAndCompaniesStepOne object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseFORRechargeMovistarStepOneReceived:) name:kNotificationFORRechargeMovistarStepOne object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseFORRechargeClaroStepOneReceived:) name:kNotificationFORRechargeClaroStepOne object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseFORServiceClaroStepOneReceived:) name:kNotificationFORServiceClaroStepOne object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseFORServiceMovistarStepOneReceived:) name:kNotificationFORServiceMovistarStepOne object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseFORServiceWaterStepOneReceived:) name:kNotificationFORServiceWaterStepOne object:nil];
        
        
        
    }
    
    return self;
    
}



#pragma mark -

#pragma mark 1 - Recarga de Celulares

#pragma mark -

#pragma mark Movistar

- (void)responseFORRechargeMovistarStepOneReceived : (NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORRechargeClaroStepOne object:nil];
    [self removeObservers];
    
    FORRechargeCellphoneStepOneResponse *response = (FORRechargeCellphoneStepOneResponse *) [notification object];
    
    if (![response isError]) {
        
        
        [self setService: [response service]];
        [self setSuggestion: [response suggestion]];
        [self setPhoneDisclaimer: [response phoneDisclaimer]];
        [self setEmailDisclaimer: [response emailDisclaimer]];
        
        NSMutableArray *arrayEmail = response.emailList.foEmailArray;
        [emailArray_ removeAllObjects];
        [emailArray_ addObjectsFromArray:arrayEmail];
        
        NSMutableArray *phoneArray = [response.phoneList foPhoneArray];
        [phoneNumberArray_ removeAllObjects];
        [phoneNumberArray_ addObjectsFromArray:phoneArray];
        
        NSMutableArray *channels = [[NSMutableArray alloc] initWithArray: [response.channelList channelsList]];
        [channelArray_ removeAllObjects];
        [channelArray_ addObjectsFromArray:channels];
        
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
        [titleAndAttributes addAttribute:[response service]];
        [infoArray addObject: titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[self getCompanyName:[response carrier]]];
        [infoArray addObject: titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_PHONE_AND_CLIENT_CODE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response phoneNumber]];
        [infoArray addObject: titleAndAttributes];
        
        
        if ([response holderService] != nil && [[response holderService] length]>0) {
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response holderService]];
            [infoArray addObject: titleAndAttributes];
        }
        
        [self setDataInfoArray: infoArray];
        
        [[self delegate] dataAnalysisHasFinished];
        
        
        

    }
    [[self appDelegate] hideActivityIndicator];
}
-(NSString*)getCompanyName:(NSString*)name
{
    NSString *lowerCaseName  = [name lowercaseString];
    if([lowerCaseName isEqualToString:@"movi"])
    {
        return NSLocalizedString(MOVISTAR_CARRIER_TEXT_KEY, nil);
    }
    else if([lowerCaseName isEqualToString:@"clar"])
    {
        return NSLocalizedString(CLARO_CARRIER_TEXT_KEY, nil);
    }
    return [lowerCaseName capitalizedString];
}

#pragma mark Claro

- (void)responseFORRechargeClaroStepOneReceived : (NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORRechargeClaroStepOne object:nil];
    [self removeObservers];
    FORRechargeCellphoneStepOneResponse *response = (FORRechargeCellphoneStepOneResponse *) [notification object];
    
    if (![response isError]) {
        
        [self setService: [response service]];
        [self setSuggestion: [response suggestion]];
        [self setPhoneDisclaimer: [response phoneDisclaimer]];
        [self setEmailDisclaimer: [response emailDisclaimer]];
        
        NSMutableArray *arrayEmail = response.emailList.foEmailArray;
        [emailArray_ removeAllObjects];
        [emailArray_ addObjectsFromArray:arrayEmail];
        
        NSMutableArray *phoneArray = [response.phoneList foPhoneArray];
        [phoneNumberArray_ removeAllObjects];
        [phoneNumberArray_ addObjectsFromArray:phoneArray];
        
        NSMutableArray *channels = [[NSMutableArray alloc] initWithArray: [response.channelList channelsList]];
        [channelArray_ removeAllObjects];
        [channelArray_ addObjectsFromArray:channels];
        
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
        [titleAndAttributes addAttribute:[response service]];
        [infoArray addObject: titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response carrier]];
        [infoArray addObject: titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_PHONE_AND_CLIENT_CODE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response phoneNumber]];
        [infoArray addObject: titleAndAttributes];
        
        if ([response holderService] != nil && [[response holderService] length]>0) {
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response holderService]];
            [infoArray addObject: titleAndAttributes];
        }
        
        [self setDataInfoArray: infoArray];
        
        [[self delegate] dataAnalysisHasFinished];
        

    }
        [[self appDelegate] hideActivityIndicator];
}

#pragma mark -
#pragma mark 2 - Pago Servicios Publicos
#pragma mark -

#pragma mark Claro : fijo y celular
- (void)responseFORServiceClaroStepOneReceived : (NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORRechargeClaroStepOne object:nil];
   [self removeObservers];
    FORServicePaymentStepOneResponse *response = (FORServicePaymentStepOneResponse *) [notification object];
    
    if (![response isError]) {
        
        [self setService: [response service]];
        [self setSuggestion: [response suggestion]];
        [self setPhoneDisclaimer: [response phoneDisclaimer]];
        [self setEmailDisclaimer: [response emailDisclaimer]];
        
        NSMutableArray *arrayEmail = response.emailList.foEmailArray;
        [emailArray_ removeAllObjects];
        [emailArray_ addObjectsFromArray:arrayEmail];
        
        NSMutableArray *phoneArray = [response.phoneList foPhoneArray];
        [phoneNumberArray_ removeAllObjects];
        [phoneNumberArray_ addObjectsFromArray:phoneArray];
        
        NSMutableArray *channels = [[NSMutableArray alloc] initWithArray: [response.channelList channelsList]];
        [channelArray_ removeAllObjects];
        [channelArray_ addObjectsFromArray:channels];
        
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
        [titleAndAttributes addAttribute: [response service]];
        [infoArray addObject:titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response company]];
        [infoArray addObject: titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TYPE_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response serviceType]];
        [infoArray addObject: titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_PAYMENT_CODE_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response code]];
        [infoArray addObject: titleAndAttributes];
        
        [self setDataInfoArray: infoArray];
        
        [[self delegate] dataAnalysisHasFinished];
        

    }
        [[self appDelegate] hideActivityIndicator];
}


#pragma mark Movistar : fijo y celular
- (void)responseFORServiceMovistarStepOneReceived : (NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORRechargeClaroStepOne object:nil];
    [self removeObservers];
    FORServicePaymentStepOneResponse *response = (FORServicePaymentStepOneResponse *) [notification object];
    
    if (![response isError]) {
        
        [self setService: [response service]];
        [self setSuggestion: [response suggestion]];
        [self setPhoneDisclaimer: [response phoneDisclaimer]];
        [self setEmailDisclaimer: [response emailDisclaimer]];
        
        NSMutableArray *arrayEmail = response.emailList.foEmailArray;
        [emailArray_ removeAllObjects];
        [emailArray_ addObjectsFromArray:arrayEmail];
        
        NSMutableArray *phoneArray = [response.phoneList foPhoneArray];
        [phoneNumberArray_ removeAllObjects];
        [phoneNumberArray_ addObjectsFromArray:phoneArray];
        
        NSMutableArray *channels = [[NSMutableArray alloc] initWithArray: [response.channelList channelsList]];
        [channelArray_ removeAllObjects];
        [channelArray_ addObjectsFromArray:channels];
        
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
        [titleAndAttributes addAttribute: [response service]];
        [infoArray addObject:titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response company]];
        [infoArray addObject: titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TYPE_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response serviceType]];
        [infoArray addObject: titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_PHONE_AND_CLIENT_CODE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response code]];
        [infoArray addObject: titleAndAttributes];
        
        if ([response holder] != nil && [[response holder] length]>0) {
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response holder]];
            [infoArray addObject: titleAndAttributes];
        }
        
        
        [self setDataInfoArray: infoArray];
        [[self delegate] dataAnalysisHasFinished];

    }
        [[self appDelegate] hideActivityIndicator];
}


#pragma mark Agua
- (void)responseFORServiceWaterStepOneReceived : (NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORRechargeClaroStepOne object:nil];
    [self removeObservers];
    FORServicePaymentStepOneResponse *response = (FORServicePaymentStepOneResponse *) [notification object];
    
    if (![response isError]) {
        
        [self setService: [response service]];
        [self setSuggestion: [response suggestion]];
        [self setPhoneDisclaimer: [response phoneDisclaimer]];
        [self setEmailDisclaimer: [response emailDisclaimer]];
        
        NSMutableArray *arrayEmail = response.emailList.foEmailArray;
        [emailArray_ removeAllObjects];
        [emailArray_ addObjectsFromArray:arrayEmail];
        
        NSMutableArray *phoneArray = [response.phoneList foPhoneArray];
        [phoneNumberArray_ removeAllObjects];
        [phoneNumberArray_ addObjectsFromArray:phoneArray];
        
        NSMutableArray *channels = [[NSMutableArray alloc] initWithArray: [response.channelList channelsList]];
        [channelArray_ removeAllObjects];
        [channelArray_ addObjectsFromArray:channels];
        
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
        [titleAndAttributes addAttribute: [response service]];
        [infoArray addObject:titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response company]];
        [infoArray addObject: titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TYPE_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response serviceType]];
        [infoArray addObject: titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_NUMBER_OF_SUPPLY_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response code]];
        [infoArray addObject:titleAndAttributes];
        
        if ([response holder] != nil && [[response holder] length]>0) {
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response holder]];
            [infoArray addObject: titleAndAttributes];
        }
        
        [self setDataInfoArray: infoArray];
        [[self delegate] dataAnalysisHasFinished];

    }
        [[self appDelegate] hideActivityIndicator];
}


#pragma mark Empresas e Instituciones

- (void)responseFORInstitutionAndCompaniesStepOneReceived : (NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORInstitutionAndCompaniesStepOne object:nil];
    [self removeObservers];
    FORInstitutionPaymentStepOneResponse *response = (FORInstitutionPaymentStepOneResponse *) [notification object];
    
    if (![response isError]) {
        
        [self setService: [response service]];
        [self setSuggestion: [response suggestion]];
        [self setPhoneDisclaimer: [response phoneDisclaimer]];
        [self setEmailDisclaimer: [response emailDisclaimer]];
        
        NSMutableArray *arrayEmail = response.emailList.foEmailArray;
        [emailArray_ removeAllObjects];
        [emailArray_ addObjectsFromArray:arrayEmail];
        
        NSMutableArray *phoneArray = [response.phoneList foPhoneArray];
        [phoneNumberArray_ removeAllObjects];
        [phoneNumberArray_ addObjectsFromArray:phoneArray];
        
        NSMutableArray *channels = [[NSMutableArray alloc] initWithArray: [response.channelList channelsList]];
        [channelArray_ removeAllObjects];
        [channelArray_ addObjectsFromArray:channels];
        
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
        [titleAndAttributes addAttribute: [response service]];
        [infoArray addObject:titleAndAttributes];
        
        companyCode_ = [[response companyCode] retain];
        
        if ([[response companyCode] isEqualToString: COMPANY_LUZ_DEL_SUR] || [[response companyCode] isEqualToString: COMPANY_EDELNOR]) {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response agreement]];
            [infoArray addObject: titleAndAttributes];
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TYPE_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: NSLocalizedString(PUBLIC_SERVICE_ELECTRICITY_TEXT_KEY, nil)];
            [infoArray addObject: titleAndAttributes];
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_NUMBER_OF_SUPPLY_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response accessCode]];
            [infoArray addObject:titleAndAttributes];
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response description]];
            [infoArray addObject:titleAndAttributes];
            
        }
        else if([[response companyCode] isEqualToString: COMPANY_DIRECTTV] || [[response companyCode] isEqualToString:COMPANY_COD_DIRECTV]){
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_AGREEMENT_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response agreement]];
            [infoArray addObject: titleAndAttributes];
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TYPE_TITLE_TEXT_KEY, nil)];
            
            [titleAndAttributes addAttribute: NSLocalizedString(PUBLIC_SERVICE_CABLE_TEXT_KEY, nil)];
            [infoArray addObject: titleAndAttributes];
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_SDS_NUMBER_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response accessCode]];
            [infoArray addObject: titleAndAttributes];
            
        }
        else {
            
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_AGREEMENT_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response agreement]];
            [infoArray addObject:titleAndAttributes];
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_ACCESS_CODE_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response accessCode]];
            [infoArray addObject:titleAndAttributes];
            
            if ([response description] != nil || [@"" isEqualToString:[response description]]) {
                if ([[[response description] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_PAYMENT_DESCRIPTION_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response description]];
                    [infoArray addObject:titleAndAttributes];
                }
            }
            
        }
        
        
        [self setDataInfoArray: infoArray];
        
        [[self delegate] dataAnalysisHasFinished];
        
        
    }
        [[self appDelegate] hideActivityIndicator];
}

#pragma mark -
#pragma mark 3 - Transferencias
#pragma mark -

#pragma mark A cuentas de Terceros
- (void)responseFORThirdAccountStepOneReceived : (NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORThirdAccountStepOne object:nil];
    [self removeObservers];
    FORThirdAccountTransferStepOneResponse *response = (FORThirdAccountTransferStepOneResponse *) [notification object];
    
    if (![response isError]) {
        
        [self setService: [response service]];
        [self setSuggestion: [response suggestion]];
        [self setPhoneDisclaimer: [response phoneDisclaimer]];
        [self setEmailDisclaimer: [response emailDisclaimer]];
        
        NSMutableArray *arrayEmail = response.emailList.foEmailArray;
        [emailArray_ removeAllObjects];
        [emailArray_ addObjectsFromArray:arrayEmail];
        
        NSMutableArray *phoneArray = [response.phoneList foPhoneArray];
        [phoneNumberArray_ removeAllObjects];
        [phoneNumberArray_ addObjectsFromArray:phoneArray];
        
        NSMutableArray *channels = [[NSMutableArray alloc] initWithArray: [response.channelList channelsList]];
        [channelArray_ removeAllObjects];
        [channelArray_ addObjectsFromArray:channels];
        
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        /* service */
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
        [titleAndAttributes addAttribute:[response service]];
        [infoArray addObject: titleAndAttributes];
        
        
        /* payment account */
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_PAYMENT_ACCOUNT_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response paymentAccount]];
        [infoArray addObject: titleAndAttributes];
        
        /* holder name */
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response holderName]];
        [infoArray addObject: titleAndAttributes];
        
        [self setDataInfoArray: infoArray];
        
        [[self delegate] dataAnalysisHasFinished];
        

    }
        [[self appDelegate] hideActivityIndicator];
}

#pragma mark A Cuentas de otros bancos
- (void)responseFOROtherBankStepOneReceived : (NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFOROtherBankStepOne object:nil];
    [self removeObservers];
    FOROtherBankTransferStepOneResponse *response = (FOROtherBankTransferStepOneResponse *) [notification object];
    
    if (![response isError]) {
        
        [self setService: [response service]];
        [self setSuggestion: [response suggestion]];
        [self setPhoneDisclaimer: [response phoneDisclaimer]];
        [self setEmailDisclaimer: [response emailDisclaimer]];
        
        
        NSMutableArray *arrayEmail = response.emailList.foEmailArray;
        [emailArray_ removeAllObjects];
        [emailArray_ addObjectsFromArray:arrayEmail];
        
        NSMutableArray *phoneArray = [response.phoneList foPhoneArray];
        [phoneNumberArray_ removeAllObjects];
        [phoneNumberArray_ addObjectsFromArray:phoneArray];
        
        NSMutableArray *channels = [[NSMutableArray alloc] initWithArray: [response.channelList channelsList]];
        [channelArray_ removeAllObjects];
        [channelArray_ addObjectsFromArray:channels];
        
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        /* service */
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
        [titleAndAttributes addAttribute:[response service]];
        [infoArray addObject: titleAndAttributes];
        
        /* destination bank account */
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_TRANSFER_TO_ANOTHER_BANK_DESTINATION_TITLE, nil)];
        [titleAndAttributes addAttribute: [response paymentAccount]];
        [infoArray addObject: titleAndAttributes];
        
        /* destination bank */
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_DESTINATION_BANK_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response destionationBank]];
        [infoArray addObject: titleAndAttributes];
        
        /* is own account */
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_TRANSFER_TO_ANOTHER_BANK_DESTINATION_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response destinationBankAccount]];
        [infoArray addObject: titleAndAttributes];
        
        /* beneficiary name */
        if([[response beneficiaryName] length]>0){
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_BENEFICIARY_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response beneficiaryName]];
        [infoArray addObject: titleAndAttributes];
        }
        /* document type */
        if([[response docTypeDescription] length]>0){
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_DOCUMENT_TYPE_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response docTypeDescription]];
            [infoArray addObject: titleAndAttributes];
        }
        
        /* document number */
        if([[response docNumber] length]>0){
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_BENEFICIARY_DOCUMENT_NUMBER_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response docNumber]];
            [infoArray addObject: titleAndAttributes];
        }
        
        [self setDataInfoArray:infoArray];
        
        [[self delegate] dataAnalysisHasFinished];
        

    }
        [[self appDelegate] hideActivityIndicator];
}

#pragma mark -
#pragma mark 4 - Pago de Tarjetas
#pragma mark -

#pragma mark Tarjeta de Terceros
- (void)responseFORPaymentThirdCardStepOneReceived : (NSNotification *) notification {
    
    
    [[self appDelegate] hideActivityIndicator];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORPaymentThirdCardStepOne object:nil];
    [self removeObservers];
    FORThirdCardPaymentStepOneResponse *response = (FORThirdCardPaymentStepOneResponse *) [notification object];
    
    if (![response isError]) {
        
        [self setService: [response service]];
        [self setSuggestion: [response suggestion]];
        [self setPhoneDisclaimer: [response phoneDisclaimer]];
        [self setEmailDisclaimer: [response emailDisclaimer]];
        
        
        NSMutableArray *arrayEmail = response.emailList.foEmailArray;
        [emailArray_ removeAllObjects];
        [emailArray_ addObjectsFromArray:arrayEmail];
        
        NSMutableArray *phoneArray = [response.phoneList foPhoneArray];
        [phoneNumberArray_ removeAllObjects];
        [phoneNumberArray_ addObjectsFromArray:phoneArray];
        
        NSMutableArray *channels = [[NSMutableArray alloc] initWithArray: [response.channelList channelsList]];
        [channelArray_ removeAllObjects];
        [channelArray_ addObjectsFromArray:channels];
        
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        /* service */
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
        [titleAndAttributes addAttribute:[response service]];
        [infoArray addObject: titleAndAttributes];
        
        /* payment card number */
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[response card]];
        [infoArray addObject: titleAndAttributes];
        
        /* holder name */
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[response holderName]];
        [infoArray addObject: titleAndAttributes];
        
        [self setDataInfoArray:infoArray];
        
        [[self delegate] dataAnalysisHasFinished];
    }
    
}

#pragma mark Pago de tarjeta de otros bancos
- (void)responseFORPaymentOtherBankStepOneReceived : (NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORPaymentOtherBankStepOne object:nil];
    [self removeObservers];
    FORPaymentCOtherBankStepOneResponse *response = (FORPaymentCOtherBankStepOneResponse *) [notification object];
    
    if (![response isError]) {
        
        [self setService: [response service]];
        [self setSuggestion: [response suggestion]];
        [self setPhoneDisclaimer: [response phoneDisclaimer]];
        [self setEmailDisclaimer: [response emailDisclaimer]];
        
        NSMutableArray *arrayEmail = response.emailList.foEmailArray;
        [emailArray_ removeAllObjects];
        [emailArray_ addObjectsFromArray:arrayEmail];
        
        NSMutableArray *phoneArray = [response.phoneList foPhoneArray];
        [phoneNumberArray_ removeAllObjects];
        [phoneNumberArray_ addObjectsFromArray:phoneArray];
        
        NSMutableArray *channels = [[NSMutableArray alloc] initWithArray: [response.channelList channelsList]];
        [channelArray_ removeAllObjects];
        [channelArray_ addObjectsFromArray:channels];
        
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        /* service */
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
        [titleAndAttributes addAttribute:[response service]];
        [infoArray addObject: titleAndAttributes];
        
        /* payment card number */
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[response cardNumber]];
        [infoArray addObject: titleAndAttributes];
        
        /* card class */
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_PAYMENT_OTHER_BANK_CLASS_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[response cardClass]];
        [infoArray addObject: titleAndAttributes];
        
        /* destination bank */
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_DESTINATION_BANK_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[response destinationBank]];
        [infoArray addObject: titleAndAttributes];
        
        /* emition */
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_PAYMENT_OTHER_BANK_SITE_OF_EMISION_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[response emissionPlace]];
        [infoArray addObject: titleAndAttributes];
        
        /*holder name*/
        if([[response beneficiaryName] length]>0){
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute:[response beneficiaryName]];
            [infoArray addObject: titleAndAttributes];
        }
        
        
        [self setDataInfoArray: infoArray];
        
        [[self delegate] dataAnalysisHasFinished];
        

    }
        [[self appDelegate] hideActivityIndicator];
}

#pragma mark Tarjeta de regalo

- (void)responseFORPaymentGiftCardStepOneReceived : (NSNotification *) notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORPaymentGiftCardStepOne object:nil];
    [self removeObservers];
    FORRechargeGiftCardStepOneResponse *response = (FORRechargeGiftCardStepOneResponse *) [notification object];
    
    if (![response isError]) {
        
        [self setService: [response service]];
        [self setSuggestion: [response suggestion]];
        [self setPhoneDisclaimer: [response phoneDisclaimer]];
        [self setEmailDisclaimer: [response emailDisclaimer]];
        
        NSMutableArray *arrayEmail = response.emailList.foEmailArray;
        [emailArray_ removeAllObjects];
        [emailArray_ addObjectsFromArray:arrayEmail];
        
        NSMutableArray *phoneArray = [response.phoneList foPhoneArray];
        [phoneNumberArray_ removeAllObjects];
        [phoneNumberArray_ addObjectsFromArray:phoneArray];
        
        NSMutableArray *channels = [[NSMutableArray alloc] initWithArray: [response.channelList channelsList]];
        [channelArray_ removeAllObjects];
        [channelArray_ addObjectsFromArray:channels];
        
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        /* service */
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
        [titleAndAttributes addAttribute:[response service]];
        [infoArray addObject: titleAndAttributes];
        
        /* card number */
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_GIFT_CARD_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[response cardNumber]];
        [infoArray addObject: titleAndAttributes];
        
        /* destination account */
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_CHARGE_ACCOUNT_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[response chargeAccount]];
        [infoArray addObject: titleAndAttributes];
        
        [self setDataInfoArray: infoArray];
        
        
        //NSLog(@"%@", response.cardNumber);
        
        [[self delegate] dataAnalysisHasFinished];
        
        
    }
        [[self appDelegate] hideActivityIndicator];
}


#pragma mark -
#pragma mark 5 - Efectivo Movil
#pragma mark -


#pragma mark Efectivo Movil
- (void)responseFORCashMobileStepOneReceived : (NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORCashMobileStepOne object:nil];
    [self removeObservers];
    FORCashMobileStepOneResponse *response = (FORCashMobileStepOneResponse *) [notification object];
    
    if (![response isError]) {
        
        [self setService: [response service]];
        [self setSuggestion: [response suggestion]];
        [self setPhoneDisclaimer: [response phoneDisclaimer]];
        [self setEmailDisclaimer: [response emailDisclaimer]];
 
        NSMutableArray *arrayEmail = response.emailList.foEmailArray;
        [emailArray_ removeAllObjects];
        [emailArray_ addObjectsFromArray:arrayEmail];

        NSMutableArray *phoneArray = [response.phoneList foPhoneArray];
        [phoneNumberArray_ removeAllObjects];
        [phoneNumberArray_ addObjectsFromArray:phoneArray];
        
        NSMutableArray *channels = [[NSMutableArray alloc] initWithArray: [response.channelList channelsList]];
        [channelArray_ removeAllObjects];
        [channelArray_ addObjectsFromArray:channels];
        
        NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
        [titleAndAttributes addAttribute: [response service]];
        [infoArray addObject:titleAndAttributes];

        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_CHARGE_ACCOUNT_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response chargeAccount]];
        [infoArray addObject: titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_BENEFICIARY_PHONE_NUMBER_TITLE_KEY, nil)];
        [titleAndAttributes addAttribute: [response phone]];
        [infoArray addObject: titleAndAttributes];
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_AMOUNT_TO_CHARGE_TITLE_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute: [response amount]];
        [infoArray addObject: titleAndAttributes];
        
        
        [self setDataInfoArray: infoArray];
        
        [[self delegate] dataAnalysisHasFinished];
        
       
    }
        [[self appDelegate] hideActivityIndicator];
}
#pragma mark -

- (NSString *) inscriptionDataValidation {
    
    
    NSString *result = nil;
    
        if ([[self selectedChannels] count] == 0){
        
            result = NSLocalizedString(FO_ERROR_SELECT_AT_LEAST_ONE_CHANNEL_TEXT_KEY, nil);
        
        } else if ([[[self largeAlias] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
            
            result = NSLocalizedString(FO_ERROR_ENTER_OPERATION_DESCRIPTION_TEXT_KEY, nil);
            
        } else if ([[[self largeAlias] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
            
            result = NSLocalizedString(FO_ERROR_ENTER_OPERATION_DESCRIPTION_TEXT_KEY, nil);
            
        } else if ([self existsInSelectedChannelsWithValue: @"BSMS"]) {
            
            if ([[[self shortAlias] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
                
                result = NSLocalizedString(FO_ERROR_ENTER_SHORT_ALIAS_TEXT_KEY, nil);
                
            }else if ( [[[self shortAlias] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]] length] < 3) {
                
                result = NSLocalizedString(FO_ERROR_SHORT_ALIAS_MINLENGTH_TEXT_KEY, nil);
                
            } else if ( [[self shortAlias]  rangeOfString: @" "].location != NSNotFound){
                
                result = NSLocalizedString(FO_ERROR_SHORT_ALIAS_CONTAINS_WHITESPACE_TEXT_KEY, nil);
                
            }
            
        } else if ( [[[self shortAlias] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0 && [[[self shortAlias] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]] length] < 3) {
            
            result = NSLocalizedString(FO_ERROR_SHORT_ALIAS_MINLENGTH_TEXT_KEY, nil);
            
        } else if ( [[[self shortAlias] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0 && [[self shortAlias]  rangeOfString: @" "].location != NSNotFound){
            
            result = NSLocalizedString(FO_ERROR_SHORT_ALIAS_CONTAINS_WHITESPACE_TEXT_KEY, nil);
            
        }
    
    if (result == nil) {
        if ([self notificationEnabled]) {
            if([self.emailArray count]==0 &&[self.phoneNumberArray count]==0){
                
                result = NSLocalizedString(FO_ERROR_NOT_PHONE_AND_EMAIL_TEXT_KEY, nil);
                
            }
                else if (![self sendSMS] && ![self sendEmail]){
                
                result = NSLocalizedString(FO_ERROR_SELECT_NOTIFICATION_TYPE_TEXT_KEY, nil);
                
            }
            
        }
    }
    
    return result;
}

- (BOOL) existsInSelectedChannelsWithValue : (NSString *) value {
    
    for (Channel *channel in [self selectedChannels]) {
        if ([[value lowercaseString] isEqualToString: [[channel description] lowercaseString]]) {
            return YES;
        }
    }
    return NO;
}

- (void) startInscriptionDataRequest {
    
    NSString *message = [self inscriptionDataValidation];
    
    if (message == nil || [message isEqualToString: @""]) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FOdataResponseReceived:) name:kNotificationFORStepTwo object:nil];
        
        [self.appDelegate showActivityIndicator:poai_Both];
        
        BOOL isFirst = YES;
        NSMutableString *channelCode = [[NSMutableString alloc] init];
        for(NSString *channel in selectedChannels_)
        {
            if(isFirst)
            {
                [channelCode appendString: [NSString stringWithFormat:@"%@",channel]];
                isFirst=NO;
            }
            else
            {
                [channelCode appendString: [NSString stringWithFormat:@",%@",channel]];
            }
        }
        
        NSString *phone = nil;
        NSString *phoneCode = nil;
        NSString *email = nil;
        NSString *emailCode = nil;
        NSString *day = nil;
        
        
        int days= dayOfMonthNotification_;
        day = (days == 0) ? @"":[NSString stringWithFormat:@"%d",days];
        
        if ([self notificationEnabled]) {
            
            if ([self sendSMS]) {
                FOPhone *foPhone = [phoneNumberArray_ objectAtIndex:[self selectedPhoneNumberIndex]];
                phone = foPhone.phoneNumber;
                phoneCode = foPhone.code;
            }
            if ([self sendEmail]) {
                FOEmail *foEmail = [emailArray_ objectAtIndex:[self selectedEmailIndex]];
                email = foEmail.email;
                emailCode = foEmail.code;
            }
            
        }
        
        [[Updater getInstance] obtainFrequentOperationReactiveStepTwoWithOperation:service_
                                                                        andChannel:channelCode
                                                                     andShortAlias:shortAlias_
                                                                     andLargeAlias:largeAlias_
                                                                            andDay:day
                                                                        andCellSMS:phone
                                                                    andCellSMSCode:phoneCode
                                                                          andEmail:email
                                                                       anEmailCode:emailCode];
        
    } else {
    
        [Tools showInfoWithMessage: message];
        
    }
    
    
    
    
}


#pragma mark -
#pragma mark Respuesta para la segunda pantalla

- (void) FOdataResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORStepTwo object:nil];
    [self.appDelegate hideActivityIndicator];
    
    
    FOInitialResponse *FOStepTwoResponse = (FOInitialResponse *)[notification object];
    if ([FOStepTwoResponse isError] || FOStepTwoResponse == nil ) {
        return;
    }
    
    NSMutableArray *confirmationDataInfoArray = [[NSMutableArray alloc] init];
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    if([[[self service] lowercaseString] isEqualToString:[@"Recarga de Tarjeta Regalo" lowercaseString]])
    {
        
        FORRechargeGiftCardStepTwoResponse *response = (FORRechargeGiftCardStepTwoResponse *) FOStepTwoResponse;
        
        [self setCoordHint:[response coordinate]];
        [self setSeal:[response seal]];
        
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY,nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response chargeAccount] != nil) {
            if ([[[response chargeAccount] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_ORIGIN_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response chargeAccount]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
            
        }
        
        if ([response cardNumber] != nil) {
            if ([[[response cardNumber] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_NUMBER_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [response cardNumber]];
            [confirmationDataInfoArray addObject:titleAndAttributes];
            }
        }
        
        
    } else if([[[self service] lowercaseString] containsString:[@"Pago de tarjeta de otros bancos" lowercaseString]])
    {
        
        FORPaymentCOtherBankStepTwoResponse *response = (FORPaymentCOtherBankStepTwoResponse *) FOStepTwoResponse;
        
        [self setCoordHint:[response coordinate]];
        [self setSeal:[response seal]];
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY,nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
            }
        }
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response destinationBank] != nil) {
            if ([[[response destinationBank] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_TRANSFER_TO_ANOTHER_BANK_DESTINATION_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response destinationBank]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
            }
        }
        
        if ([response cardNumber] != nil) {
            if ([[[response cardNumber] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response cardNumber]];
                [confirmationDataInfoArray addObject: titleAndAttributes];
            }
        }
        
        if ([response cardType] != nil) {
            if ([[[response cardType] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_PAYMENT_OTHER_BANK_CLASS_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response cardType]];
                [confirmationDataInfoArray addObject: titleAndAttributes];
                
            }
        }
        
        if ([response placeEmission] != nil) {
            if ([[[response placeEmission] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_PAYMENT_OTHER_BANK_SITE_OF_EMISION_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response placeEmission]];
                [confirmationDataInfoArray addObject: titleAndAttributes];
                
                
            }
        }
        
        if ([response beneficiaryName] != nil) {
            if ([[[response beneficiaryName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
             
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_BENEFICIARY_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response beneficiaryName]];
                [confirmationDataInfoArray addObject: titleAndAttributes];
                
            }
        }
        
    } else if([[[self service] lowercaseString] isEqualToString:[@"Pago de Tarjeta de terceros" lowercaseString]])
    {
        
        FORThirdCardPaymentStepTwoResponse *response = (FORThirdCardPaymentStepTwoResponse *) FOStepTwoResponse;
        
        [self setCoordHint:[response coordinate]];
        [self setSeal:[response seal]];
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY,nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response thirdCard] != nil) {
            if ([[[response thirdCard] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response thirdCard]];
                [confirmationDataInfoArray addObject: titleAndAttributes];
            }
        }
        
        if ([response holderName] != nil) {
            if ([[[response holderName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_HOLDER_ACCOUNT_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response holderName]];
                [confirmationDataInfoArray addObject: titleAndAttributes];
            }
        }
        
        
        
    } else if([[[self service] lowercaseString] containsString:[@"Transferencia a cuentas de otros bancos" lowercaseString]])
    {
        
        FOROtherBankTransferStepTwoResponse *response = (FOROtherBankTransferStepTwoResponse *) FOStepTwoResponse;
        
        [self setCoordHint:[response coordinate]];
        [self setSeal: [response seal]];
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
        
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY,nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
            }
        }
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
            }
        }
        
        if ([response destinationBankAccount] != nil) {
            if ([[[response destinationBankAccount] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                
                [titleAndAttributes setTitleString: NSLocalizedString(FO_OWN_ACCOUNT_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response destinationBankAccount]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
            }
        }
        
        if ([response paymentAccount] != nil) {
            if ([[[response paymentAccount] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_INTERBANK_ACCOUNT_NUMBER_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response paymentAccount]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response beneficiaryName] != nil) {
            if ([[[response beneficiaryName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_BENEFICIARY_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response beneficiaryName]];
                [confirmationDataInfoArray addObject: titleAndAttributes];
                
            }
        }
        
        if ([response docTypeDescription] != nil) {
            if ([[[response docTypeDescription] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
               
                titleAndAttributes = [TitleAndAttributes  titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_DOCUMENT_TYPE_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response docTypeDescription]];
                [confirmationDataInfoArray addObject: titleAndAttributes];
            }
        }
        
        if ([response docNumber] != nil) {
            if ([[[response docNumber] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_BENEFICIARY_DOCUMENT_NUMBER_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response docNumber]];
                [confirmationDataInfoArray addObject: titleAndAttributes];
                
            }
        }
        
        
    } else if([[[self service] lowercaseString] isEqualToString:[@"Transferencia a cuentas de terceros" lowercaseString]])
    {
        
        FORThirdAccountTransferStepTwoResponse *response = (FORThirdAccountTransferStepTwoResponse *) FOStepTwoResponse;
        
        [self setCoordHint:[response coordinate]];
        [self setSeal: [response seal]];
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY,nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [confirmationDataInfoArray addObject:titleAndAttributes];

            }
        }
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
            }
        }
        
        if ([response paymentAccount] != nil) {
            if ([[[response paymentAccount] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_PAYMENT_ACCOUNT_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response paymentAccount]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
            }
        }
        
        if ([response holderName] != nil) {
            if ([[[response holderName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
           
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_HOLDER_ACCOUNT_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response holderName]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        
    } else if([[[self service] lowercaseString] isEqualToString:[@"Efectivo Móvil" lowercaseString]])
    {
        
        FORCashMobileStepTwoResponse *response = (FORCashMobileStepTwoResponse *) FOStepTwoResponse;
        
        [self setCoordHint:[response coordinate]];
        [self setSeal: [response seal]];
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
            }
        }
        
        if ([response chargeAccount] != nil) {
            if ([[[response chargeAccount] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_CHARGE_ACCOUNT_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response chargeAccount]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
            }
        }
        
        
        if ([response beneficiaryNumber] != nil) {
            if ([[[response beneficiaryNumber] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_BENEFICIARY_PHONE_NUMBER_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response beneficiaryNumber]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
            }
        }
        
        if ([response amount] != nil) {
            if ([[[response amount] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_AMOUNT_TO_CHARGE_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response amount]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY,nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
            }
        }
        
        
    }
    
    else if([[[self service] lowercaseString] isEqualToString:[@"Pago de Instituciones y Empresas" lowercaseString]]
            || [[[self service] lowercaseString] isEqualToString:[@"Pago de SERV" lowercaseString]]
            || [[[self service] lowercaseString] isEqualToString:[@"Pago de Servicios" lowercaseString]]
            || [[[self service] lowercaseString] isEqualToString:[@"Instituciones y empresas" lowercaseString]]
            || [[[self service] lowercaseString] isEqualToString:[@"Pago de servicios eléctricos" lowercaseString]])
    {
        FORInstitutionPaymentStepTwoResponse *response = (FORInstitutionPaymentStepTwoResponse *) FOStepTwoResponse;
        
        [self setCoordHint:[response coordinate]];
        [self setSeal: [response seal]];
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY,nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response agreement] != nil) {
            if ([[[response agreement] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                if([companyCode_ isEqualToString: COMPANY_LUZ_DEL_SUR] || [companyCode_ isEqualToString: COMPANY_EDELNOR])
                {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(@"Empresa", nil)];
                    [titleAndAttributes addAttribute: [response agreement]];
                    [confirmationDataInfoArray addObject:titleAndAttributes];
                    
                }
                
                else
                {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_AGREEMENT_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response agreement]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                }
            }
        }
        
        if ([response accessCode] != nil) {
            if ([[[response accessCode] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                
                if([companyCode_ isEqualToString: COMPANY_LUZ_DEL_SUR] || [companyCode_ isEqualToString: COMPANY_EDELNOR])
                {
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_NUMBER_OF_SUPPLY_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response accessCode]];
                    [confirmationDataInfoArray addObject: titleAndAttributes];
                }
                else
                {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_ACCESS_CODE_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response accessCode]];
                [confirmationDataInfoArray addObject: titleAndAttributes];
                }
            }
        }
        
        
        if ([response description] != nil) {
            if ([[[response description] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                if([companyCode_ isEqualToString: COMPANY_LUZ_DEL_SUR] || [companyCode_ isEqualToString: COMPANY_EDELNOR])
                {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response description]];
                    [confirmationDataInfoArray addObject:titleAndAttributes];
                }
                else
                {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_PAYMENT_DESCRIPTION_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response description]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                }
            }
        }
        
    } else if([[[self service] lowercaseString] isEqualToString:[@"Pago de servicios de agua" lowercaseString]])
    {
        
        FORServicePaymentStepTwoResponse *response = (FORServicePaymentStepTwoResponse *) FOStepTwoResponse;
        
        [self setCoordHint:[response coordinate]];
        [self setSeal:[response seal]];
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY,nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response company] != nil) {
            if ([[[response company] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response company]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response serviceType] != nil) {
            if ([[[response serviceType] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TYPE_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response serviceType]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response code] != nil) {
            if ([[[response code] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_NUMBER_OF_SUPPLY_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response code]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response holder] != nil) {
            if ([[[response holder] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response holder]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
    } else if([[[self service] lowercaseString] isEqualToString:[@"Pago de telefonía móvil Claro" lowercaseString]])
    {
        
        FORServicePaymentStepTwoResponse *response = (FORServicePaymentStepTwoResponse *) FOStepTwoResponse;
        
        [self setCoordHint:[response coordinate]];
        [self setSeal:[response seal]];
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY,nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response company] != nil) {
            if ([[[response company] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response company]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response serviceType] != nil) {
            if ([[[response serviceType] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TYPE_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response serviceType]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response code] != nil) {
            if ([[[response code] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_PAYMENT_CODE_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response code]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
    } else if([[[self service] lowercaseString] isEqualToString:[@"Pago de telefonía móvil" lowercaseString]] || [[[self service] lowercaseString] isEqualToString:[@"Pago de telefonía fija" lowercaseString]])
    {
        
        FORServicePaymentStepTwoResponse *response = (FORServicePaymentStepTwoResponse *) FOStepTwoResponse;
        
        [self setCoordHint:[response coordinate]];
        [self setSeal:[response seal]];
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY,nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response company] != nil) {
            if ([[[response company] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response company]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response serviceType] != nil) {
            if ([[[response serviceType] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TYPE_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response serviceType]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response code] != nil) {
            if ([[[response code] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_PAYMENT_CODE_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response code]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
    } else if([[[self service] lowercaseString] isEqualToString:[@"Recarga de celular - Claro" lowercaseString]] ||
              [[[self service] lowercaseString] isEqualToString:[@"Recarga de Teléfonos Móviles" lowercaseString]])
    {
        
        FORRechargeCellphoneStepTwoResponse *responseST = (FORRechargeCellphoneStepTwoResponse *) FOStepTwoResponse;
        
        [self setCoordHint:[responseST coordinate]];
        [self setSeal:[responseST seal]];
        
        if ([responseST operation] != nil) {
            if ([[[responseST operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY,nil)];
                [titleAndAttributes addAttribute: [responseST operation]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([responseST service] != nil) {
            if ([[[responseST service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [responseST service]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([responseST carrier] != nil) {
            if ([[[responseST carrier] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [self getCompanyName: [responseST carrier]]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([responseST phoneNumber] != nil) {
            if ([[[responseST phoneNumber] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_PHONE_AND_CLIENT_CODE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [responseST phoneNumber]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([responseST holderService] != nil) {
            if ([[[responseST holderService] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [responseST holderService]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
                
            }
        }
        
    }
    
    if ([FOStepTwoResponse nickname] != nil) {
        if ([[[FOStepTwoResponse nickname] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_LARGE_ALIAS_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [FOStepTwoResponse nickname]];
            [confirmationDataInfoArray addObject:titleAndAttributes];
        }
    }
    
    if ([FOStepTwoResponse shortNickname] != nil) {
        if ([[[FOStepTwoResponse shortNickname] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_SHORT_ALIAS_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [FOStepTwoResponse shortNickname]];
            [confirmationDataInfoArray addObject:titleAndAttributes];
        }
    }
    
    if ([FOStepTwoResponse dayNotice] != nil) {
        if ([[[FOStepTwoResponse dayNotice] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
            
            if (![[[FOStepTwoResponse dayNotice] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString: @"0"]) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_DAY_REMINDER_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [FOStepTwoResponse dayNotice]];
                [confirmationDataInfoArray addObject:titleAndAttributes];
            }
        }
    }
    
    if ([FOStepTwoResponse smsMobile] != nil) {
        if ([[[FOStepTwoResponse smsMobile] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_CELLPHONE_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [FOStepTwoResponse smsMobile]];
            [confirmationDataInfoArray addObject:titleAndAttributes];
            
        }
    }
    
    if ([FOStepTwoResponse smsEmail] != nil) {
        if ([[[FOStepTwoResponse smsEmail] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_EMAIL_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [FOStepTwoResponse smsEmail]];
            [confirmationDataInfoArray addObject:titleAndAttributes];
        }
    }
    
    if ([FOStepTwoResponse channelList] != nil) {
            if ([[FOStepTwoResponse channelList] channelCount] > 0 ) {
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_CHANNELS_TITLE_TEXT_KEY, nil)];
                
                NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[[FOStepTwoResponse channelList] channelsList]];
                
                for(Channel *channel in array){
                    [titleAndAttributes addAttribute: [channel description]];
                }
                
                [confirmationDataInfoArray addObject:titleAndAttributes];
            }
    }
    
    [self setConfirmationInfoArray:confirmationDataInfoArray];
    [self.delegate confirmationAnalysisHasFinished];
    
    
}

- (NSString *) inscriptionSuccessDataValidation {
    
    NSString *result = @"";
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
    if ((otpUsage == otp_UsageOTP) && ([secondFactorKey_ isEqualToString:@""])) {
        
        result = NSLocalizedString(MUST_INTRODUCE_OTP_KEY_TEXT_KEY, nil);
        
    } else if ((otpUsage == otp_UsageTC) && ([secondFactorKey_ isEqualToString:@""] || ([secondFactorKey_ length] < 3))) {
        
        result = NSLocalizedString(PAYMENT_ERROR_COORDINATES_TEXT_KEY, nil);
        
    }
    
    return result;
    
}

- (void) startInscriptionSuccessRequest {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FOInscriptionSuccessResponseReceived:) name:kNotificationFORStepThree object:nil];
    
    NSString *message = [self inscriptionSuccessDataValidation];
    
    if (message == nil || [message isEqualToString: @""]) {
    
        [[self appDelegate] showActivityIndicator: poai_Both];
        
        [[Updater getInstance] obtainFrequentOperationReactiveStepThreeFromSecondKeyFactor:secondFactorKey_ andOperation:[self service]];
    
    } else {
    
        [Tools showInfoWithMessage:message];
        
    }
    
}

- (void) FOInscriptionSuccessResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationFORStepThree object:nil];

    [[self appDelegate] hideActivityIndicator];
    
    
    FOInitialResponse *initialResponse = (FOInitialResponse *)[notification object];
    
    if ([initialResponse isError]) {
        return;
    }
    
    NSMutableArray *successInfoArray = [[NSMutableArray alloc] init];
    NSString *dateAndHourOperation = @"";
    NSMutableArray *channelsArray = [[NSMutableArray alloc] init];
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    if([[[self service] lowercaseString] isEqualToString:[@"Recarga de Tarjeta Regalo" lowercaseString]])
    {
        FORRechargeGiftCardStepThreeResponse *response = (FORRechargeGiftCardStepThreeResponse *) initialResponse;
        
        if(![response isError]){
            
            if ([response operation] != nil) {
                if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response operation]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response service] != nil) {
                if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                    [titleAndAttributes addAttribute: [response service]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response chargeAccount] != nil) {
                if ([[[response chargeAccount] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_CHARGE_ACCOUNT_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response chargeAccount]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response cardNumber] != nil) {
                if ([[[response cardNumber] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_NUMBER_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response cardNumber]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([[response channelList] channelsList] != nil) {
                
                if ([[response channelList] channelCount] > 0 ) {
                    
                    channelsArray = [[NSMutableArray alloc] initWithArray:[[response channelList] channelsList]];
                }
            }
            
            
            if ([response dateAndHourOperation] != nil) {
                if ([[[response dateAndHourOperation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    dateAndHourOperation = [response dateAndHourOperation];
                }
            }
            
            
            [self setSuccessMessage: [response disclaimer]];
            
        }
        
        
    }
    else if([[[self service] lowercaseString] containsString:[@"Pago de tarjeta de otros bancos" lowercaseString]])
    {
        FORPaymentCOtherBankStepThreeResponse *response = (FORPaymentCOtherBankStepThreeResponse *) initialResponse;
        
        if (![response isError]) {
            
            if ([response operation] != nil) {
                if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response operation]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response service] != nil) {
                if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                    [titleAndAttributes addAttribute: [response service]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response destinationBank_] != nil) {
                if ([[[response destinationBank_] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_DESTINATION_BANK_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response destinationBank_]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response cardNumber] != nil) {
                if ([[[response cardNumber] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response cardNumber]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response cardType] != nil) {
                if ([[[response cardType] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_PAYMENT_OTHER_BANK_CLASS_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response cardType]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response placeEmission] != nil) {
                if ([[[response placeEmission] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_PAYMENT_OTHER_BANK_SITE_OF_EMISION_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response placeEmission]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response beneficiaryName] != nil) {
                if ([[[response beneficiaryName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_BENEFICIARY_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response beneficiaryName]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response channelList] !=nil ) {
                if ([[response channelList] channelCount] > 0 ) {
                    
                    channelsArray = [[NSMutableArray alloc] initWithArray:[[response channelList] channelsList]];
                    
                }
            }
            
            if ([response dateAndHourOperation] != nil) {
                if ([[[response dateAndHourOperation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    dateAndHourOperation = [response dateAndHourOperation];
                }
            }
            
            [self setSuccessMessage: [response disclaimer]];
            
            
        }
        
        
    }
    else if([[[self service] lowercaseString] isEqualToString:[@"Pago de Tarjeta de terceros" lowercaseString]])
    {
        
        FORThirdCardPaymentStepThreeResponde *response = (FORThirdCardPaymentStepThreeResponde *) initialResponse;
        if (![response isError]) {
            
            if ([response operation] != nil) {
                if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response operation]];
                    [successInfoArray addObject: titleAndAttributes];
                    
                }
            }
            
            if ([response service] != nil) {
                if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                    [titleAndAttributes addAttribute: [response service]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
           
            if ([response thirdCard] != nil) {
                if ([[[response thirdCard] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response thirdCard]];
                    [successInfoArray addObject: titleAndAttributes];
                    
                }
            }
            
            if ([response holderName] != nil) {
                if ([[[response holderName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response holderName]];
                    [successInfoArray addObject: titleAndAttributes];
                    
                }
            }
            
            if ([response channelList] != nil) {
                if ([[response channelList] channelCount] > 0 ) {
                    
                    channelsArray = [[NSMutableArray alloc] initWithArray:[[response channelList] channelsList]];
                }
            }
           
            if ([response dateAndHourOperation] != nil) {
                if ([[[response holderName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    dateAndHourOperation = [response dateAndHourOperation];
                }
            }
            
            [self setSuccessMessage: [response disclaimer]];
            
        }
        
        //
    }
    else if([[[self service] lowercaseString] containsString:[@"Transferencia a cuentas de otros bancos" lowercaseString]])
    {
        
        FOROtherBankTransferStepThreeResponse *response = (FOROtherBankTransferStepThreeResponse *) initialResponse;
        if (![response isError]) {
            
            if ([response operation] != nil) {
                if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response operation]];
                    [successInfoArray addObject: titleAndAttributes];
                    
                }
            }
            
            if ([response service] != nil) {
                if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                    [titleAndAttributes addAttribute: [response service]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response destinationBankAccount] != nil) {
                if ([[[response destinationBankAccount] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_OWN_ACCOUNT_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response destinationBankAccount]];
                    [successInfoArray addObject: titleAndAttributes];
                    
                }
            }
            
            if ([response paymentAccount] != nil) {
                if ([[[response paymentAccount] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_INTERBANK_ACCOUNT_NUMBER_TITLE_TEXT_KEY, nil)];
                    
                    [titleAndAttributes addAttribute: [response paymentAccount]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response beneficiaryName] != nil) {
                if ([[[response beneficiaryName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_BENEFICIARY_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response beneficiaryName]];
                    [successInfoArray addObject: titleAndAttributes];
                    
                }
            }
            
            if ([response docTypeDescription] != nil) {
                if ([[[response docTypeDescription] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_DOCUMENT_TYPE_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response docTypeDescription]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response docTypeDescription] != nil) {
                if ([[[response docTypeDescription] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_BENEFICIARY_DOCUMENT_NUMBER_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response docNumber]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response channelList] != nil) {
                
                if ([[response channelList] channelCount] > 0 ) {
                    
                    channelsArray = [[NSMutableArray alloc] initWithArray:[[response channelList] channelsList]];
                    
                }
            }
            
            if ([response dateHour] != nil) {
                if ([[[response dateHour] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    dateAndHourOperation = [response dateHour];
                }
            }
            
            [self setSuccessMessage: [response informativeText]];
            
        }
        
    }
    else if([[[self service] lowercaseString] isEqualToString:[@"Transferencia a cuentas de terceros" lowercaseString]])
    {
        FORThirdAccountTransferStepThreeResponse *response = (FORThirdAccountTransferStepThreeResponse *) initialResponse;
        if (![response isError]) {
            
            if ([response operation] != nil) {
                if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response operation]];
                    [successInfoArray addObject: titleAndAttributes];
                    
                }
            }
            
            if ([response service] != nil) {
                if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                    [titleAndAttributes addAttribute: [response service]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response paymentAccount] != nil) {
                if ([[[response paymentAccount] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_PAYMENT_ACCOUNT_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response paymentAccount]];
                    [successInfoArray addObject: titleAndAttributes];
                    
                }
            }
            
            if ([response holderName] != nil) {
                if ([[[response holderName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response holderName]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response channelList] != nil) {
                if ([[response channelList] channelCount] > 0 ) {
                    
                    channelsArray = [[NSMutableArray alloc] initWithArray:[[response channelList] channelsList]];
                }
            }
            
            if ([response dateHour] != nil) {
                if ([[[response dateHour] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    dateAndHourOperation = [response dateHour];
                }
            }
            
            [self setSuccessMessage: [response informativeText]];
            

        }
        
    }
    else if([[[self service] lowercaseString] isEqualToString:[@"Efectivo Móvil" lowercaseString]])
    {
        
        FORCashMobileStepThreeResponse *response = (FORCashMobileStepThreeResponse *) initialResponse;
        if (![response isError]) {
            
            if ([response operation] != nil) {
                if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response operation]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response service] != nil) {
                if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                    [titleAndAttributes addAttribute: [response service]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response phone] != nil) {
                if ([[[response phone] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_BENEFICIARY_PHONE_NUMBER_TITLE_KEY, nil)];
                    [titleAndAttributes addAttribute: [response phone]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response amount] != nil) {
                if ([[[response amount] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_AMOUNT_TO_CHARGE_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response amount]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response channelList] != nil) {
                
                if ([[response channelList] channelCount] > 0 ) {
                    
                    channelsArray = [[NSMutableArray alloc] initWithArray:[[response channelList] channelsList]];
                }
            }
            
            if ([response dateAndHourOperation] != nil) {
                if ([[[response dateAndHourOperation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    dateAndHourOperation = [response dateAndHourOperation];
                }
            }
            
            [self setSuccessMessage: [response disclaimer]];
            
        }
        
    }
    else if([[[self service] lowercaseString] isEqualToString:[@"Pago de Instituciones y Empresas" lowercaseString]] || [[[self service] lowercaseString] isEqualToString:[@"Pago de SERV" lowercaseString]]
             || [[[self service] lowercaseString] isEqualToString:[@"Pago de Servicios" lowercaseString]]
             || [[[self service] lowercaseString] isEqualToString:[@"Instituciones y empresas" lowercaseString]]
            || [[[self service] lowercaseString] isEqualToString:[@"Pago de servicios eléctricos" lowercaseString]])
    {
        FORInstitutionPaymentStepThreeResponse *response = (FORInstitutionPaymentStepThreeResponse *) initialResponse;
        if (![response isError]) {
            
            if ([response operation] != nil) {
                if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY,nil)];
                    [titleAndAttributes addAttribute: [response operation]];
                    [successInfoArray addObject:titleAndAttributes];
                    
                }
            }
            
            if ([response service] != nil) {
                if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                   
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                    [titleAndAttributes addAttribute: [response service]];
                    [successInfoArray addObject: titleAndAttributes];
                }
            }
            
            if ([response agreement] != nil) {
                if ([[[response agreement] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    if([companyCode_ isEqualToString: COMPANY_LUZ_DEL_SUR] || [companyCode_ isEqualToString: COMPANY_EDELNOR])
                    {
                        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                        [titleAndAttributes setTitleString: NSLocalizedString(@"Empresa", nil)];
                        [titleAndAttributes addAttribute: [response agreement]];
                        [successInfoArray addObject: titleAndAttributes];
                    }
                    else
                    {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_AGREEMENT_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response agreement]];
                    [successInfoArray addObject: titleAndAttributes];
                    }
                }
            }
            
            if ([response accessCode] != nil) {
                if ([[[response accessCode] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    if([companyCode_ isEqualToString: COMPANY_LUZ_DEL_SUR] || [companyCode_ isEqualToString: COMPANY_EDELNOR])
                    {
                        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                        [titleAndAttributes setTitleString: NSLocalizedString(FO_NUMBER_OF_SUPPLY_TITLE_TEXT_KEY, nil)];
                        [titleAndAttributes addAttribute: [response accessCode]];
                        [successInfoArray addObject: titleAndAttributes];
                    }
                    else{
                        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                        [titleAndAttributes setTitleString: NSLocalizedString(FO_ACCESS_CODE_TITLE_TEXT_KEY, nil)];
                        [titleAndAttributes addAttribute: [response accessCode]];
                        [successInfoArray addObject: titleAndAttributes];
                    }
                    
                    
                }
            }
            
            if ([response description] != nil) {
                if ([[[response description] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    
                    
                    if([companyCode_ isEqualToString: COMPANY_LUZ_DEL_SUR] || [companyCode_ isEqualToString: COMPANY_EDELNOR])
                    {
                        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                        [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
                        [titleAndAttributes addAttribute: [response description]];
                        [successInfoArray addObject: titleAndAttributes];
                        
                    }
                    else
                    {
                    
                    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                    [titleAndAttributes setTitleString: NSLocalizedString(FO_PAYMENT_DESCRIPTION_TITLE_TEXT_KEY, nil)];
                    [titleAndAttributes addAttribute: [response description]];
                    [successInfoArray addObject: titleAndAttributes];
                    }
                    
                }
            }
            
            if ([response channelList] != nil) {
                if ([[response channelList] channelCount] > 0 ) {
                    
                    channelsArray = [[NSMutableArray alloc] initWithArray:[[response channelList] channelsList]];
                }
            }
            
            
            if ([response dateAndHourOperation] != nil) {
                if ([[[response dateAndHourOperation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                    dateAndHourOperation = [response dateAndHourOperation];
                }
            }
            [self setSuccessMessage: [response informativeText]];
            
            
        }
        
    } else if([[[self service] lowercaseString] isEqualToString:[@"Pago de servicios de agua" lowercaseString]])
    {
        
        FORServicePaymentStepThreeResponse *response = (FORServicePaymentStepThreeResponse *) initialResponse;
        
        if ([response isError]) {
            return;
        }
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response company] != nil) {
            if ([[[response company] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response company]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response serviceType] != nil) {
            if ([[[response serviceType] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TYPE_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response serviceType]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response code] != nil) {
            if ([[[response code] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_NUMBER_OF_SUPPLY_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response code]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response holder] != nil) {
            if ([[[response holder] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response holder]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response channelList] != nil) {
            if ([[response channelList] channelCount] > 0 ) {
                
                channelsArray = [[NSMutableArray alloc] initWithArray:[[response channelList] channelsList]];
            }
        }
        
        
       
        [self setSuccessMessage: [response informativeText]];
        
    } else if([[[self service] lowercaseString] isEqualToString:[@"Pago de telefonía móvil Claro" lowercaseString]])
    {
        
        FORServicePaymentStepThreeResponse *response = (FORServicePaymentStepThreeResponse *) initialResponse;
        
        if ([response isError]) {
            return;
        }
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response company] != nil) {
            if ([[[response company] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response company]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response serviceType] != nil) {
            if ([[[response serviceType] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TYPE_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response serviceType]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response code] != nil) {
            if ([[[response code] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_PAYMENT_CODE_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response code]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response channelList] != nil) {
            if ([[response channelList] channelCount] > 0 ) {
                
                channelsArray = [[NSMutableArray alloc] initWithArray:[[response channelList] channelsList]];
            }
        }
        
    
        [self setSuccessMessage: [response informativeText]];
        
    } else if([[[self service] lowercaseString] isEqualToString:[@"Pago de telefonía móvil" lowercaseString]] || [[[self service] lowercaseString] isEqualToString:[@"Pago de telefonía fija" lowercaseString]])
    {
        
        FORServicePaymentStepThreeResponse *response = (FORServicePaymentStepThreeResponse *) initialResponse;
        
        if ([response isError]) {
            return;
        }
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response company] != nil) {
            if ([[[response company] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response company]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response serviceType] != nil) {
            if ([[[response serviceType] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TYPE_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response serviceType]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response code] != nil) {
            if ([[[response code] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_PHONE_AND_CLIENT_CODE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response code]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        if ([response channelList] != nil) {
            if ([[response channelList] channelCount] > 0 ) {
                
                channelsArray = [[NSMutableArray alloc] initWithArray:[[response channelList] channelsList]];
            }
        }
        
        
      
        [self setSuccessMessage: [response informativeText]];
        
        
        
    } else if([[[self service] lowercaseString] isEqualToString:[@"Recarga de celular - Claro" lowercaseString]] ||
              [[[self service] lowercaseString] isEqualToString:[@"Recarga de Teléfonos Móviles" lowercaseString]])
    {
        
        FORRechargeCellphoneStepThreeResponse *response = (FORRechargeCellphoneStepThreeResponse *) initialResponse;
        
        if ([response isError]) {
            return;
        }
        
        if ([response operation] != nil) {
            if ([[[response operation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response operation]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response service] != nil) {
            if ([[[response service] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_TITLE_KEY, nil)];
                [titleAndAttributes addAttribute: [response service]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response carrier] != nil) {
            if ([[[response carrier] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_COMPANY_NAME_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [self getCompanyName: [response carrier]]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response phoneNumber] != nil) {
            if ([[[response phoneNumber] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_PHONE_AND_CLIENT_CODE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response phoneNumber]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }
        
        if ([response holderService] != nil) {
            if ([[[response holderService] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                [titleAndAttributes setTitleString: NSLocalizedString(FO_SERVICE_HOLDER_NAME_TITLE_TEXT_KEY, nil)];
                [titleAndAttributes addAttribute: [response holderService]];
                [successInfoArray addObject:titleAndAttributes];
                
            }
        }

        if ([response channelList] != nil) {
            if ([[response channelList] channelCount] > 0 ) {
                
                channelsArray = [[NSMutableArray alloc] initWithArray:[[response channelList] channelsList]];
            }
        }
        
        
        if ([response dateAndHourOperation] != nil) {
            if ([[[response dateAndHourOperation] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                dateAndHourOperation = [response dateAndHourOperation];
            }
        }
        
        if([response disclaimer]!= nil)
        {
            successMessage_  = [response disclaimer];
        }
        
    }
    
    if ([initialResponse nickname] != nil) {
        if ([[[initialResponse nickname] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_LARGE_ALIAS_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [initialResponse nickname]];
            [successInfoArray addObject: titleAndAttributes];
        }
    }
    
    
    if ([initialResponse shortNickname] != nil) {
        if ([[[initialResponse shortNickname] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_SHORT_ALIAS_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [initialResponse shortNickname]];
            [successInfoArray addObject: titleAndAttributes];
            
        }
    }
    
    if ([initialResponse dayNotice] != nil) {
        if ([[[initialResponse dayNotice] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
            
           if (![[[initialResponse dayNotice] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString: @"0"]) {
               titleAndAttributes = [TitleAndAttributes titleAndAttributes];
               [titleAndAttributes setTitleString: NSLocalizedString(FO_DAY_REMINDER_TITLE_TEXT_KEY, nil)];
               [titleAndAttributes addAttribute: [initialResponse dayNotice]];
               [successInfoArray addObject: titleAndAttributes];
            }
        }
    }
    
    if ([initialResponse smsMobile] != nil) {
        if ([[[initialResponse smsMobile] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_CELLPHONE_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [initialResponse smsMobile]];
            [successInfoArray addObject: titleAndAttributes];
            
        }
    }
    
    if ([initialResponse smsEmail] != nil) {
        if ([[[initialResponse smsEmail] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_EMAIL_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: [initialResponse smsEmail]];
            [successInfoArray addObject: titleAndAttributes];
        }
    }
    
    if ([initialResponse channelList] != nil) {
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString: NSLocalizedString(FO_CHANNELS_TITLE_TEXT_KEY, nil)];
        if (channelsArray != nil) {
            for(Channel *channel in channelsArray){
                [titleAndAttributes addAttribute: [channel description]];
            }
            [successInfoArray addObject:titleAndAttributes];
        }
    }
    
    if (dateAndHourOperation != nil) {
        if ([[dateAndHourOperation stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString: NSLocalizedString(FO_INSCRIPTION_DATE_TITLE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute: dateAndHourOperation];
            [successInfoArray addObject: titleAndAttributes];
            
        }
    }
    
    [self setSuccessInfoArray: successInfoArray];
    [self.delegate successAnalysisHasFinished];
}

- (FOTypeEnum) frequentOperationType {
    
    return FOTETransferToOtherBankAccount;
}

- (NXT_Peru_iPhone_AppDelegate *) appDelegate {
    
    return (NXT_Peru_iPhone_AppDelegate *) [UIApplication sharedApplication].delegate;
    
}

- (void)setDataInfoArray:(NSArray *)dataInfoArray {
    
    if (dataInfoArray_ != dataInfoArray) {
        
        [dataInfoArray_ removeAllObjects];
        
        for (NSObject *object in dataInfoArray) {
            
            if ([object isKindOfClass:[TitleAndAttributes class]]) {
                
                [dataInfoArray_ addObject:object];
                
            }
            
        }
        
    }
    
}

- (void)setSuccessInfoArray:(NSArray *)successInfoArray {
    
    if (successInfoArray_ != successInfoArray) {
        
        [successInfoArray_ removeAllObjects];
        
        for (NSObject *object in successInfoArray) {
            
            if ([object isKindOfClass:[TitleAndAttributes class]]) {
                
                [successInfoArray_ addObject:object];
                
            }
            
        }
        
    }
    
}

- (BOOL)typeOTPKey {
    
    BOOL result = NO;
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
    if ((otpUsage == otp_UsageOTP) || (otpUsage == otp_UsageUnknown)) {
        
        result = YES;
        
    } else if (otpUsage == otp_UsageTC) {
        
        result = NO;
        
    }
    
    return result;
    
}


@end
