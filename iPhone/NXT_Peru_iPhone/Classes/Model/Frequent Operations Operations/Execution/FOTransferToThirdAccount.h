//
//  FOTransferToThirdAccount.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/12/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOOperationHelper.h"

@class FOEThirdAccountTransferStepOneResponse;
@class TransferConfirmationAdditionalInformation;
@class TransferSuccessAdditionalInformation;
@interface FOTransferToThirdAccount : FOOperationHelper{
    
@private
    
    /**
     * Array with the origin selectable accounts. All objects are PaymentElement instances
     */
    NSMutableArray *originAccountList_;
    
    /**
     * Selected destination account office
     */
    NSString *selectedDestinationAccountOffice_;
    
    /**
     * Selected destination account accNumber
     */
    NSString *selectedDestinationAccountAccNumber_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    FOEThirdAccountTransferStepOneResponse *startUpResponseInformation_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    TransferConfirmationAdditionalInformation *additionalInformation_;
    
    /**
     * Transfer Success Additional Information
     */
    TransferSuccessAdditionalInformation *transferSuccessAdditionalInfo_;
    
}

/**
 * Provides read-only access to the array with the origin selectable accounts
 */
@property (nonatomic, readonly, retain) NSArray *originAccountList;
/**
 * Provides read-only access to the DestinationAccountOffice
 */
@property (nonatomic, readwrite, retain) NSString *selectedDestinationAccountOffice;
/**
 * Provides read-only access to the DestinationAccountAccNumber
 */
@property (nonatomic, readwrite, retain) NSString *selectedDestinationAccountAccNumber;
/**
 * Designated initialized. Initializes a FOEThirdAccountTransferStepOneResponse instance with the initial transfer accounts response
 *
 * @param transferStartupResponse The transfer accounts response
 * @return The initialized FOEThirdAccountTransferStepOneResponse instance;
 */
- (id)initWithTransferStartupResponse:(FOEThirdAccountTransferStepOneResponse *)fOEThirdAccountTransferStepOneResponse;

@end
