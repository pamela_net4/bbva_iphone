//
//  PaymentWaterService.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 3/3/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "PaymentWaterService.h"

#import "FOEServicePaymentStepOneResponse.h"
#import "BankAccount.h"
#import "accountPay.h"
#import "accountPayList.h"
#import "CardType.h"
#import "PendingDocument.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "PaymentConfirmationResponse.h"
#import "PaymentSuccessResponse.h"
#import "Tools.h"
#import "Updater.h"

@implementation PaymentWaterService
#pragma mark -
#pragma mark Properties

@synthesize originAccountList = originAccountList_;
@synthesize originCardList = originCardList_;
@synthesize isDB = isDB_;
@synthesize isPartial = isPartial_;
@synthesize brand = brand_;
@synthesize pendingDocumentsArray = pendingDocumentsArray_;
#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [originAccountList_ release];
    originAccountList_ = nil;
    
    
    [originCardList_ release];
    originCardList_ = nil;
    
    [brand_ release];
    brand_ = nil;
    
    [pendingDocumentsArray_ release];
    pendingDocumentsArray_ = nil;
    
    [super dealloc];
    
}
- (id)initWithPaymentStartupResponse:(FOEServicePaymentStepOneResponse *)fOEServicePaymentStepOneResponse{
    
    if ((self = [super init])) {
        
        // The initial operation in iPad returns elements no needed in iPhone
        
        [self setSelectedOriginAccountIndex:-1];
        [self setSelectedOriginCardIndex:-1];
        
        if (startUpResponseInformation_ != nil) {
            [startUpResponseInformation_ release];
            startUpResponseInformation_ = nil;
        }
        
        [self setSelectedCurrencyIndex:0];
        
        startUpResponseInformation_ = fOEServicePaymentStepOneResponse;
        [startUpResponseInformation_ retain];
        
        self.carrierList = [[NSArray alloc] initWithArray:[[fOEServicePaymentStepOneResponse foCarrierList] alterCarrierList]];
        
        newBrand = @"";
    }
    
    return self;
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the frequent operation type. Base class returns a default value (transfer between user accounts)
 *
 * @return The frequent operation type
 */
- (FOTypeEnum)foOperationType {
    
    return FOTEPaymentInstAndComp;
    
}

/*
 * Returns the List of currency for the operation. Base class returns an empty array
 *
 * @return The currency list
 */
- (NSArray *)currencyList {
    
    return [NSArray arrayWithObjects:NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil),
            NSLocalizedString(DOLLARS_CURRENCY_TEXT_KEY, nil),
            nil];
    
}

/*
 * Returns the can show legal terms flag.
 *
 */
- (BOOL)canShowLegalTerms {
    
    return TRUE;
    
}

/*
 * Returns the can show email and sms
 */
- (BOOL)canSendEmailandSMS {
    
    return YES;
    
}

/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedFOOperationTypeString {
    
    return NSLocalizedString(TRANSFER_TO_THIRD_ACCOUNTS_TEXT_1_KEY, nil);
    
}

/*
 * Returns the localized terms and conditions string. The transfer between accounts legal terms localized string is returned
 *
 * @return The localized transfer legal terms operation type string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return NSLocalizedString(TRANSFER_LEGAL_TERMS_KEY, nil);
    
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
    return @"";
    
}

#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foFirstStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Service
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SERVICE_TITLE_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.service]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Frequen operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_POSITION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.nick]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foSecondStepInformation{
    NSMutableArray *mutableResult = [NSMutableArray array];
    
  //Operation
    
   /* TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];

    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.operation]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    */
    //Service
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SERVICE_TITLE_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.service]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Frequen operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_POSITION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.nick]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    /*
    //institution and companie description
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_INSTITUTIONS_COMPANIES_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.agreement]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Agreement datas
    
    NSArray *titles = [startUpResponseInformation_.titlesArray componentsSeparatedByString:@"$"];
    NSArray *descriptions = [startUpResponseInformation_.array componentsSeparatedByString:@"$"];
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_AGREEMENTS_DATA_TITLE_TEXT_KEY, nil);
    
    for (int i = 0; i < [titles count]; i++) {
        
        NSString *value = [NSString stringWithFormat:@"%@ %@", [Tools notNilString:[titles objectAtIndex:i]], [Tools notNilString:[descriptions objectAtIndex:i]]];
        
        [titleAndAttributes addAttribute:value];
    }
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }*/
    
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foThirdStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Operation Number
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CASH_MOBILE_NUMBER_OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:paymentSuccessAdditionalInfo_.operationNumber]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    /*
    //Date/hour
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_OPERATION_DATE_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:[paymentSuccessAdditionalInfo_ date]], [Tools notNilString:[paymentSuccessAdditionalInfo_ time]] ]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Operation
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:paymentSuccessAdditionalInfo_.operation]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Charged account
    NSString *selectedAccountText = @"";
    
    if ([self selectedOriginAccountIndex] != -1) {
        accountPay *account = [originAccountList_ objectAtIndex:[self selectedOriginAccountIndex]];
        selectedAccountText = [NSString stringWithFormat:@"%@ %@", [account subject], [account currency]];
    }else{
        CardType *card = [originCardList_ objectAtIndex:[self selectedOriginCardIndex]];
        selectedAccountText = [NSString stringWithFormat:@"%@ | %@ %@", [card type], [card number], [card currency]];
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_ORIGIN_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:selectedAccountText];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //institution and companie description
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_INSTITUTIONS_COMPANIES_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.agreement]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Service code
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_SERVICE_CODE_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:paymentSuccessAdditionalInfo_.companieName]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    */
    
    return mutableResult;
}

/*
 * Creates the account array to display in the picker view. Default implementation returns an empty array
 */
- (NSArray *)accountStringList{
    
    NSArray *accountPayArray = originAccountList_;
    NSMutableArray *accountStringArray  = [[[NSMutableArray alloc]init] autorelease];
    for(int i=0;i<[accountPayArray count];i++)
    {
        [accountStringArray addObject: ((accountPay*)[accountPayArray objectAtIndex:i]).accountIdAndDescription];
    }
    
    return accountStringArray;
}

-(NSArray *)cardStringList
{
    NSArray *cardTypeArray = originCardList_;
    NSMutableArray *cardStringArray  = [[[NSMutableArray alloc]init] autorelease];
    for(int i=0;i<[cardTypeArray count];i++)
    {
        CardType *card = [cardTypeArray objectAtIndex:i];
        NSString *cardType = card.type;
        [cardStringArray addObject: [NSString stringWithFormat:@"%@ | %@", [Tools obfuscateCardNumber:card.num],cardType]];
    }
    
    return  cardStringArray;
}

/**
 * Returns the key name for the confirmation response
 *
 */
- (NSString *)notificationConfirmationKey{
    return kNotificationPaymentInstitutionsConfirmationPayResult;
}

/**
 * Returns the key name for the success response
 *
 */
- (NSString *)notificationSuccessKey{
    return kNotificationPaymentInstitutionsSuccessPayResult;
}

#pragma mark -
#pragma mark Operation methods
/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startFORequest {
    
    BOOL result = NO;
    NSString *originAccountNumber = @"";
    NSString *originCardNumber = @"";
    NSString *form = @"";
    
    if (([self selectedOriginAccountIndex] != NSNotFound &&
         [self selectedOriginAccountIndex] >= 0 &&
         [self selectedOriginAccountIndex] < [[self originAccountList] count]) ){
        
        accountPay *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
        originAccountNumber = [originAccount accountParam];
        form = @"CT";
        
    }else if( ([self selectedOriginCardIndex] != NSNotFound &&
               [self selectedOriginCardIndex] >= 0 &&
               [self selectedOriginCardIndex] < [[self originCardList] count]) ){
        CardType *originCard = [[self originCardList] objectAtIndex:[self selectedOriginCardIndex]];
        originCardNumber = [originCard param];
        form = @"TC";
    }
    NSString *amount = [Tools formatAmountWithDotDecimalSeparator:self.amountString];
    
    self.currency =  [self.currencyList objectAtIndex:[self selectedCurrencyIndex]];
    
    if (([self.currency length] > 0) ){
        
        NSString *carrier1 = @"";
        NSString *carrier2 = @"";
        
        if (self.selectedCarrier1Index > 0 && self.selectedCarrier1Index <  [self.carrierList count]) {
            AlterCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier1Index];
            
            carrier1 = carrier.code;
        }
        
        if (self.selectedCarrier2Index > 0 && self.selectedCarrier2Index <  [self.carrierList count]) {
            AlterCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier2Index];
            
            carrier2 = carrier.code;
        }
        
        [[Updater getInstance] obtainInstitutionsAndCompaniesPayConfirmationWithForm:form
                                                                   ownSubjectAccount:originAccountNumber
                                                                      ownSubjectCard:originCardNumber
                                                                           payImport:[Tools notNilString:amount]
                                                                        phonenumber1:[Tools notNilString:[self destinationSMS1]]
                                                                        phonenumber2:[Tools notNilString:[self destinationSMS2]]
                                                                              email1:[Tools notNilString:[self destinationEmail1]]
                                                                              email2:[Tools notNilString:[self destinationEmail2]]
                                                                            carrier1:[Tools notNilString:carrier1]
                                                                            carrier2:[Tools notNilString:carrier1]
                                                                         mailMessage:[Tools notNilString:[self emailMessage]]
                                                                               brand:newBrand
                                                                                type:0];
        
        result = YES;
        
    }
    
    return result;
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startFORequestDataValidation {
	
	NSString *result = nil;
    
	if (!self.hasCard && ([self selectedOriginAccountIndex] == NSNotFound || [self selectedOriginAccountIndex] < 0 || [self selectedOriginAccountIndex] > [[self originAccountList] count] ) ) {
		
		result = NSLocalizedString(TRANSFER_SELECT_ORIGIN_ACCOUNT_TEXT_KEY, nil);
		
	} else if ( ([self selectedOriginAccountIndex] == NSNotFound ||
                 [self selectedOriginAccountIndex] < 0 ||
                 [self selectedOriginAccountIndex] > [[self originAccountList] count] )
               &&
               ([self selectedOriginCardIndex] == NSNotFound ||
                [self selectedOriginCardIndex] < 0 ||
                [self selectedOriginCardIndex] > [[self originCardList] count] ) ){
                   
                   result = NSLocalizedString(TRANSFER_SELECT_ORIGIN_ACCOUNT_TEXT_KEY, nil);
                   
               }   else {
                   
                   NSString *originAccountNumber = @"";
                   
                   if (!self.hasCard) {
                       accountPay *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
                       originAccountNumber = [originAccount subject];
                   }else{
                       if (([self selectedOriginAccountIndex] != NSNotFound &&
                            [self selectedOriginAccountIndex] >= 0 &&
                            [self selectedOriginAccountIndex] < [[self originAccountList] count]) ){
                           
                           accountPay *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
                           originAccountNumber = [originAccount subject];
                           
                       }else if( ([self selectedOriginCardIndex] != NSNotFound &&
                                  [self selectedOriginCardIndex] >= 0 &&
                                  [self selectedOriginCardIndex] < [[self originCardList] count]) ){
                           CardType *originCard = [[self originCardList] objectAtIndex:[self selectedOriginCardIndex]];
                           originAccountNumber = [originCard number];
                       }
                   }
                   
                   
                   originAccountNumber = @"";
                   
                   if (([self selectedOriginAccountIndex] != NSNotFound &&
                        [self selectedOriginAccountIndex] >= 0 &&
                        [self selectedOriginAccountIndex] < [[self originAccountList] count]) ){
                       
                       accountPay *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
                       originAccountNumber = [originAccount subject];
                       
                   }else if( ([self selectedOriginCardIndex] != NSNotFound &&
                              [self selectedOriginCardIndex] >= 0 &&
                              [self selectedOriginCardIndex] < [[self originCardList] count]) ){
                       CardType *originCard = [[self originCardList] objectAtIndex:[self selectedOriginCardIndex]];
                       originAccountNumber = [originCard number];
                   }
                   
                   if (!([originAccountNumber length] > 0)) {
                       
                       result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
                       
                   } else if ((isPartial_) &&
                              (([self.amountString isEqualToString:@"0.00"]) || ([self.amountString floatValue] == 0.0f)) ) {
                       
                       result = NSLocalizedString(TRANSFER_ERROR_TRANSFER_AMOUNT_TEXT_KEY, nil);
                       
                   }
               }
    
    
    if (result == nil) {
        
        NSString *amount = @"";
        
        float total = 0.0f;
        int docIndex = -1;
        
        if (isDB_) {
            
            for (int i = 0; i < [brand_ count]; i++) {
                
                if ([@"1" isEqualToString:[brand_ objectAtIndex:i]])
                {
                    docIndex = i;
                    if (!isPartial_) {
                        PendingDocument *doc = [pendingDocumentsArray_ objectAtIndex:i];
                        NSString *aux = [doc totalAmount];
                        aux = [Tools formatAmountWithDotDecimalSeparator:aux];
                        total += [aux floatValue];
                    }
                }
                newBrand = [newBrand stringByAppendingString:[brand_ objectAtIndex:i]];
                
            }
            
            if (docIndex == -1) {
                result =  NSLocalizedString(PAYMENT_ERROR_SELECT_RECEIPT_TO_CANCEL_TEXT_KEY, nil);
                return result;
            }
            
            amount = [Tools formatAmountWithDotDecimalSeparator:self.amountString];
            
            if (isPartial_){
                
                PendingDocument *doc = [pendingDocumentsArray_ objectAtIndex:docIndex];
                
                total = [amount floatValue];
                
                minAmount = [Tools formatAmountWithDotDecimalSeparator:[doc minAmount]];
                maxAmount = [Tools formatAmountWithDotDecimalSeparator:[doc maxAmount]];
                
                if ([amount floatValue] < [minAmount floatValue] || [amount floatValue] > [maxAmount floatValue]) {
                    result = NSLocalizedString(PAYMENT_ERROR_AMOUNT_MUST_IN_RANGE_KEY, nil);
                    
                    return result;
                }
                
            }
            
        }
    }
    
    if (result == nil) {
        result = [super startFORequestDataValidation];
    }
    
    return result;
    
}

- (BOOL)startFoConfirmationRequest{
    BOOL result = [super startFoConfirmationRequest];
    
    [[Updater getInstance] institutionsAndcompaniesConfirmResultFromSecondKeyFactor:@""];
    
    return result;
}

/**
 * @param FOResponse The FO response received from the server
 * @return YES when the confirmation response is correct, NO otherwise
 */
- (BOOL)fOResponseReceived:(StatusEnabledResponse *)foResponse{
    
    BOOL result = [super fOResponseReceived:foResponse];
    
    if (!result) {
        
        if ([foResponse isKindOfClass:[PaymentConfirmationResponse class]]) {
            
            PaymentConfirmationResponse *response = (PaymentConfirmationResponse *)foResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response retain];
            
            self.seal = [additionalInformation_ seal];
            
            result = YES;
            
        }
    }
    
    return result;
}

/*
 * Notifies the fO operation helper the confirmation response received from the server. The fo operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        result = ![confirmationResponse isError];
        
        if (result) {
            if ([confirmationResponse isKindOfClass:[PaymentSuccessResponse class]]) {
                
                PaymentSuccessResponse *response = (PaymentSuccessResponse *)confirmationResponse;
                
                if (paymentSuccessAdditionalInfo_ != nil) {
                    [paymentSuccessAdditionalInfo_ release];
                    paymentSuccessAdditionalInfo_ = nil;
                }
                paymentSuccessAdditionalInfo_ = [response retain];
                
                result = YES;
                
            }
        }
        
    }
    
    return result;
    
    
}

@end
