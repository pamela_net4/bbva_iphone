//
//  FOOperationHelper.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/12/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FrequentOperationConstants.h"
#import "AlterCarrier.h"
#import "AlterCarrierList.h"

@class StatusEnabledResponse;
@interface FOOperationHelper : NSObject{
@private
    
    /**
     * list of currencies
     */
    NSArray *currencyList_;
    /**
     * Selected amount string
     */
    NSString *amountString_;
    
    /**
     * Selected currency
     */
    NSString *currency_;
    
    /**
     * Selected reference
     */
    NSString *reference_;
    /**
     * The seal for the operation
     */
    NSString *seal_;
    /**
     * Selected currency index
     */
    NSInteger selectedCurrencyIndex_;
    /**
     * Selected origin account index. When out of bounds, no origin account is selected
     */
    NSInteger selectedOriginAccountIndex_;
    /**
     * Selected origin card index. When out of bounds, no origin card is selected
     */
    NSInteger selectedOriginCardIndex_;
    /**
     * Flag to indicate if there is cards
     */
    BOOL hasCard_;
    
    /**
     * Send email flag
     */
    BOOL sendSMS_;
    /**
     * User selected destination email address
     */
    NSString *destinationSMS1_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationSMS2_;
    
    /**
     * Show SMS 1 flag
     */
    BOOL showSMS1_;
    
    /**
     * Show SMS 2 flag
     */
    BOOL showSMS2_;
    
    /**
     * Send email flag
     */
    BOOL sendEmail_;
    
    /*
     *selected carrier one index
     */
    NSInteger selectedCarrier1Index_;
    
    /*
     *selected carrier two index
     */
    NSInteger selectedCarrier2Index_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationEmail1_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationEmail2_;
    
    /**
     * Show email 1 flag
     */
    BOOL showEmail1_;
    
    /**
     * Show email 2 flag
     */
    BOOL showEmail2_;
    
    /**
     * User selected email message
     */
    NSString *emailMessage_;
    
    /*
     *the carrier list
     */
    NSArray *carrierList_;
    
    NSString *optionalDisclaimer_;
    
}

/**
 * Provides read-write access to the currency list
 */
@property (nonatomic, readwrite, retain) NSArray *currencyList;
/**
 * Provides read-only access to the transfer operation type
 */
@property (nonatomic, readonly, assign) FOTypeEnum foOperationType;
/**
 * Provides read-write access to the selected amount string
 */
@property (nonatomic, readwrite, copy) NSString *amountString;
/**
 * Provides read-write access to the selected currency
 */
@property (nonatomic, readwrite, copy) NSString *currency;
/**
 * Provides read-only access to the localized FO operation type string. Default value is an empty string
 */
@property (nonatomic, readonly, copy) NSString *localizedFOOperationTypeString;
/**
 * Provides read-only access to the legal Terms URL. Default value is an empty string
 */
@property (nonatomic, readonly, copy) NSString *legalTermsURL;
/**
 * Provides read-write access to the selected reference
 */
@property (nonatomic, readwrite, copy) NSString *reference;
/**
 * Provides read-write access to the secure seal
 */
@property (nonatomic, readwrite, copy) NSString *seal;
/**
 * Provides read-write access to the selected reference
 */
@property (nonatomic, readwrite, assign) NSInteger selectedCurrencyIndex;
/**
 * Provides read-write access to the selected reference
 */
@property (nonatomic, readwrite, assign) NSInteger selectedOriginAccountIndex;
/**
 * Provides read-write access to the selected reference
 */
@property (nonatomic, readwrite, assign) NSInteger selectedOriginCardIndex;
/**
 * Provides read-write access to the selected reference
 */
@property (nonatomic, readwrite, assign) BOOL hasCard;
/**
 * Provides read-only access to the can show legal terms flag. Default value is NO
 */
@property (nonatomic, readonly, assign) BOOL canShowLegalTerms;
/**
 * Provides read-write access to the user selected destinationSMS1 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationSMS1;

/**
 * Provides read-write access to the user selected destinationSMSOperator1 address
 */
//@property (nonatomic, readwrite, assign) NSInteger destinationSMSOperator1;

/**
 * Provides read-write access to the user selected destinationSMS2 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationSMS2;

/**
 * Provides read-write access to the user selected destinationSMSOperator2 address
 */
//@property (nonatomic, readwrite, assign) NSInteger destinationSMSOperator2;

/**
 * Provides read-write access to the user selected showSMS1
 */
@property (nonatomic, readwrite, assign) BOOL showSMS1;

/**
 * Provides read-write access to the user selected showSMS2
 */
@property (nonatomic, readwrite, assign) BOOL showSMS2;

/**
 * Provides read-write access to the send email flag
 */
@property (nonatomic, readwrite, assign) BOOL sendEmail;

/**
 * Provides read-write access to the send sms flag
 */
@property (nonatomic, readwrite, assign) BOOL sendSMS;

/**
 * Provides read-write access to the selected carrier one index
 */
@property (nonatomic, readwrite, assign) NSInteger selectedCarrier1Index;

/**
 * Provides read-write access to the selected carrier two index
 */
@property (nonatomic, readwrite,assign) NSInteger selectedCarrier2Index;

/**
 * Provides read-write access to the user selected destinationEmail1 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationEmail1;

/**
 * Provides read-write access to the user selected destinationEmail2 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationEmail2;

/**
 * Provides read-write access to the user selected showEmail1
 */
@property (nonatomic, readwrite, assign) BOOL showEmail1;

/**
 * Provides read-write access to the user selected showEmail2
 */
@property (nonatomic, readwrite, assign) BOOL showEmail2;

/**
 * Provides read-write access to the user selected email message
 */
@property (nonatomic, readwrite, copy) NSString *emailMessage;
/**
 * Provides read-only access to the can send email flag. Default value is NO
 */
@property (nonatomic, readonly, assign) BOOL canSendEmailandSMS;
/*
 *Provides readwrite access to the carrier list
 */
@property (nonatomic,readwrite,retain) NSArray *carrierList;


@property(nonatomic,readonly,copy) NSString *itfMessage;

@property(nonatomic,readonly,copy) NSString *confirmationMessage;

@property(nonatomic,readonly,copy) NSString *notificationMessage;

@property(nonatomic,readonly,copy) NSString *itfMessageConfirm;

/*
 *Provides readwrite access to the carrier list
 */
@property (nonatomic,readwrite,retain) NSString *optionalDisclaimer;


@property(nonatomic,readonly,copy) NSString *foHeaderTitle;

/**
 * Creates the title and attributes array to display in the frequent operation first step. Default implementation returns an empty array
 *
 * @return The array containing the frequent operation first step. All objects are TitleAndAttributes instances
 */
- (NSMutableArray *)foFirstStepInformation;

/**
 * Creates the title and attributes array to display in the frequent operation second step. Default implementation returns an empty array
 *
 * @return The array containing the frequent operation second step. All objects are TitleAndAttributes instances
 */
- (NSMutableArray *)foSecondStepInformation;

/**
 * Creates the title and attributes array to display in the frequent operation third step. Default implementation returns an empty array
 *
 * @return The array containing the frequent operation third step. All objects are TitleAndAttributes instances
 */
- (NSMutableArray *)foThirdStepInformation;

/**
 * Creates the account array to display in the picker view. Default implementation returns an empty array
 *
 * @return The array containing the list of array of accounts. All objects are NSString instances
 */
- (NSArray *)accountStringList;

/**
 * Creates the card array to display in the picker view. Default implementation returns an empty array
 *
 * @return The array containing the list of array of cards. All objects are NSString instances
 */
- (NSArray *)cardStringList;

/**
 * Returns the key name for the confirmation response
 *
 * @return The key name as a NSString. Base class always returns an empty String
 */
- (NSString *)notificationTINConfirmationKey;

/**
 * Returns the key name for the confirmation response
 *
 * @return The key name as a NSString. Base class always returns an empty String
 */
- (NSString *)notificationConfirmationKey;

/**
 * Returns the key name for the success response
 *
 * @return The key name as a NSString. Base class always returns an empty String
 */
- (NSString *)notificationSuccessKey;

/**
 * Performs the frequent operation request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 *
 * @return YES when the request can be started, NO otherwise
 */
- (BOOL)startFORequest;

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 *
 * @return if the data is valid returns nil
 */
- (NSString *)startFORequestDataValidation;

/**
 * Notifies the FO operation helper the FO response received from the server. The FO operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param FOResponse The FO response received from the server
 * @return YES when the confirmation response is correct, NO otherwise
 */
- (BOOL)fOResponseReceived:(StatusEnabledResponse *)foResponse;

/**
 * Performs the FO confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 *
 * @return YES when the request can be started, NO otherwise
 */
- (BOOL)startFoConfirmationRequest;

/**
 * Notifies the FO operation helper the confirmation response received from the server. The FO operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param confirmationResponse The confirmation response received from the server
 * @return YES when the confirmation response is correct, NO otherwise
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse;

@end
