//
//  FOPaymentCCThirdCard.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/20/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOOperationHelper.h"

@class FOEThirdCardPaymentStepOneResponse;
@class PaymentConfirmationResponse;
@class PaymentSuccessResponse;
@interface FOPaymentCCThirdCard : FOOperationHelper{
    
@private
    
    /**
     * Array with the origin selectable accounts. All objects are PaymentElement instances
     */
    NSMutableArray *originAccountList_;
    
    /**
     * Selected destination bank code
     */
    NSString *selectedDestinationBank_;
    
    /**
     * Selected destination account office
     */
    NSString *selectedDestinationAccountOffice_;
    
    /**
     * Selected destination account number
     */
    NSString *selectedDestinationAccountNumber_;
    
    /**
     * Selected destination account control number
     */
    NSString *selectedDestinationAccountControl_;
    
    /**
     * Flag to indicate if is owner of the account
     */
    BOOL isItf_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    FOEThirdCardPaymentStepOneResponse *startUpResponseInformation_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    PaymentConfirmationResponse *additionalInformation_;
    
    /**
     * Transfer Success Additional Information
     */
    PaymentSuccessResponse *paymentSuccessAdditionalInfo_;
    
}

/**
 * Provides read-only access to the array with the origin selectable accounts
 */
@property (nonatomic, readonly, retain) NSArray *originAccountList;
/**
 * Designated initialized. Initializes a FOEThirdAccountTransferStepOneResponse instance with the initial transfer accounts response
 *
 * @param transferStartupResponse The transfer accounts response
 * @return The initialized FOEThirdAccountTransferStepOneResponse instance;
 */
- (id)initWithPaymentStartupResponse:(FOEThirdCardPaymentStepOneResponse *)fOEThirdCardPaymentStepOneResponse;

@end

