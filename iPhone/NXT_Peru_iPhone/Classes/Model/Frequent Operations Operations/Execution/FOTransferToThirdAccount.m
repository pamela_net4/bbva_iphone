//
//  FOTransferToThirdAccount.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/12/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOTransferToThirdAccount.h"
#import "FOEThirdAccountTransferStepOneResponse.h"
#import "FOAccountList.h"
#import "BankAccount.h"
#import "AccountList.h"
#import "FOCardList.h"
#import "FOCard.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "TransferConfirmationResponse.h"
#import "TransferConfirmationAdditionalInformation.h"
#import "TransferSuccessAdditionalInformation.h"
#import "TransferSuccessResponse.h"
#import "Tools.h"
#import "Updater.h"

@implementation FOTransferToThirdAccount
#pragma mark -
#pragma mark Properties

@synthesize originAccountList = originAccountList_;
@synthesize selectedDestinationAccountAccNumber = selectedDestinationAccountAccNumber_;
@synthesize selectedDestinationAccountOffice = selectedDestinationAccountOffice_;
#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [originAccountList_ release];
    originAccountList_ = nil;
    
    [selectedDestinationAccountAccNumber_ release];
    selectedDestinationAccountAccNumber_ = nil;
    
    [selectedDestinationAccountOffice_ release];
    selectedDestinationAccountOffice_ = nil;
    
    [super dealloc];
    
}
- (id)initWithTransferStartupResponse:(FOEThirdAccountTransferStepOneResponse *)fOEThirdAccountTransferStepOneResponse{
    
    if ((self = [super init])) {
        
        // The initial operation in iPad returns elements no needed in iPhone
        Session *session = [Session getInstance];
        originAccountList_ = [[NSMutableArray alloc] initWithArray:session.accountList.transferAccountList];
        
        [self setSelectedOriginAccountIndex:-1];
        
        if (startUpResponseInformation_ != nil) {
            [startUpResponseInformation_ release];
            startUpResponseInformation_ = nil;
        }
        
        [self setSelectedCurrencyIndex:0];
        
        startUpResponseInformation_ = fOEThirdAccountTransferStepOneResponse;
        [startUpResponseInformation_ retain];
        
        NSArray *accountParts = [fOEThirdAccountTransferStepOneResponse.paymentAccount componentsSeparatedByString:@"-"];
        
        if ([accountParts count] > 1) {
            [self setSelectedDestinationAccountOffice:[accountParts objectAtIndex:1]];
        }
        
        if ([accountParts count] > 2) {
            [self setSelectedDestinationAccountAccNumber:[accountParts objectAtIndex:2]];
        }
        
        self.carrierList = [[NSArray alloc] initWithArray:[[fOEThirdAccountTransferStepOneResponse foCarrierList] alterCarrierList]];
    }
    
    return self;
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the frequent operation type. Base class returns a default value (transfer between user accounts)
 *
 * @return The frequent operation type
 */
- (FOTypeEnum)foOperationType {
    
    return FOTETransferToOtherUserAccount;
    
}

/*
 * Returns the List of currency for the operation. Base class returns an empty array
 *
 * @return The currency list
 */
- (NSArray *)currencyList {
    
    return [NSArray arrayWithObjects:NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil),
            NSLocalizedString(DOLLARS_CURRENCY_TEXT_KEY, nil),
            nil];
    
}

/*
 * Returns the can show legal terms flag.
 *
 */
- (BOOL)canShowLegalTerms {
    
    return TRUE;
    
}

/*
 * Returns the can show email and sms
 */
- (BOOL)canSendEmailandSMS {
    
    return YES;
    
}

/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedFOOperationTypeString {
    
    return NSLocalizedString(TRANSFER_TO_THIRD_ACCOUNTS_TEXT_1_KEY, nil);
    
}

/*
 * Returns the localized terms and conditions string. The transfer between accounts legal terms localized string is returned
 *
 * @return The localized transfer legal terms operation type string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return NSLocalizedString(TRANSFER_LEGAL_TERMS_KEY, nil);
    
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
#if defined(SIMULATE_HTTP_CONNECTION)
    return [Tools notNilString:@"http://www.google.es"];
#else
    return [Tools notNilString:additionalInformation_.disclaimer];
#endif
    
}

#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foFirstStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Service
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SERVICE_TITLE_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.service]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Frequen operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_POSITION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.nick]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //payment account
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_ACCOUNT_DESTINATION_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.paymentAccount]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Holder Payment account
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_ACCOUNT_OWNER_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.holderName]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foSecondStepInformation{
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Operation
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.operation]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Charged Account
    BankAccount *chargeAccount = [originAccountList_ objectAtIndex:self.selectedOriginAccountIndex];
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORIGIN_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ | %@", [Tools notNilString:chargeAccount.accountType], [Tools notNilString:[Tools getCurrencyLiteral:chargeAccount.currency]]]];
    [titleAndAttributes addAttribute:[Tools notNilString:chargeAccount.number]];
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_AMOUNT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:self.currency], [Tools notNilString:self.amountString]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Beneficiary account
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ - %@", [Tools getCurrencyLiteral:[Tools notNilString:additionalInformation_.beneficiaryAccountCurrency]],additionalInformation_.beneficiaryAccount]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    
    //Beneficiary holder account
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_HOLDER_NAME_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.beneficiary]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //market place comision
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_TO_MY_ACCOUNTS_TAX_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@",[Tools getCurrencySimbol:[Tools notNilString:[additionalInformation_.marketPlaceCommisionCurrency  uppercaseString]]], [Tools notNilString:additionalInformation_.marketPlaceCommision]
                                     ]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //reference
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_REFERENCE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:[self reference]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foThirdStepInformation{
 
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Operation Number
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(NUMBER_OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:transferSuccessAdditionalInfo_.operationNumber]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Operation type
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_OPERATION_TYPE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:transferSuccessAdditionalInfo_.operation]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Charged Account
    BankAccount *chargeAccount = [originAccountList_ objectAtIndex:self.selectedOriginAccountIndex];
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_HOLDER_BENEFICIARY_ACCOUNT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:chargeAccount.number], [Tools notNilString:[Tools getCurrencyLiteral:chargeAccount.currency]]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Transfer Amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.amountPaidCurrencyString], [Tools notNilString:transferSuccessAdditionalInfo_.amountPaidString]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //beneficiary account number
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ACCOUND_PAID_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.beneficiaryAccount]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Beneficiary holder name
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_HOLDER_NAME_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.beneficiary]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //reference
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_REFERENCE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:[self reference]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //market place comision
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_TO_MY_ACCOUNTS_TAX_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:[Tools notNilString:additionalInformation_.marketPlaceCommisionCurrency]], [Tools notNilString:additionalInformation_.marketPlaceCommision]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //exchange rate
    if(![transferSuccessAdditionalInfo_.amountChargedCurrencyString isEqualToString:transferSuccessAdditionalInfo_.amountPaidCurrencyString])
    {
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(CHANGE_TYPE_TITLE_KEY, nil);
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:[Tools notNilString:transferSuccessAdditionalInfo_.currencyExchangeRate]], [Tools notNilString:transferSuccessAdditionalInfo_.exchangeRate]]];
    
        if (titleAndAttributes != nil) {
        
            [mutableResult addObject:titleAndAttributes];
        
        }
    }
    
    
    //Transfer Amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFERED_IMPORT_CHARGE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.amountChargedCurrencyString], [Tools notNilString:transferSuccessAdditionalInfo_.amountChargedString]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Transfer abonado
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_CHARGERD_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.amountPaidCurrencyString], [Tools notNilString:transferSuccessAdditionalInfo_.amountPaidString]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_DATE_HOUR_SHORT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:transferSuccessAdditionalInfo_.dateString],[Tools notNilString:transferSuccessAdditionalInfo_.hour]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
   
    
    return mutableResult;
}

/*
 * Creates the account array to display in the picker view. Default implementation returns an empty array
 */
- (NSArray *)accountStringList{
    NSMutableArray *accountArray = [[[NSMutableArray alloc] init] autorelease];
        
    for (BankAccount *account in originAccountList_) {
        NSString *accountNumber = account.bankAccountListName;
        
        [accountArray addObject:accountNumber];
    }
    
    return accountArray;
}

/**
 * Returns the key name for the confirmation response
 *
 */
- (NSString *)notificationConfirmationKey{
    return kNotificationTransferConfirmationResponseReceived;
}

/**
 * Returns the key name for the success response
 *
 */
- (NSString *)notificationSuccessKey{
    return kNotificationTransferSuccessResponseReceived;
}

#pragma mark -
#pragma mark Operation methods
/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startFORequest {
    
    BOOL result = NO;
    BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    NSString *originAccountNumber = originAccount.number;
    
    self.amountString = [Tools formatAmountWithDotDecimalSeparator:self.amountString];

    self.currency =  [Tools getCurrencyServer:[self.currencyList objectAtIndex:[self selectedCurrencyIndex]]];
    
    if (([originAccountNumber length] > 0) && 
        ([selectedDestinationAccountOffice_ length] > 0) &&
        ([selectedDestinationAccountAccNumber_ length] > 0) &&
        ([self.currency length] > 0) ){
        
        NSString *carrier1 = @"";
        NSString *carrier2 = @"";
        
        if (self.selectedCarrier1Index > 0 && self.selectedCarrier1Index <  [self.carrierList count]) {
            AlterCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier1Index];
            
            carrier1 = carrier.code;
        }
        
        
        if (self.selectedCarrier2Index > 0 && self.selectedCarrier2Index <  [self.carrierList count]) {
            AlterCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier2Index];
            
            carrier2 = carrier.code;
        }
        
        [[Updater getInstance] transferToThirdAccountsConfirmationFromAccountType:originAccount.accountType
                                                                     fromCurrency:originAccount.currency
                                                                        fromIndex:originAccount.subject
                                                                         toBranch:selectedDestinationAccountOffice_
                                                                        toAccount:selectedDestinationAccountAccNumber_
                                                                           amount:self.amountString
                                                                         currency:[Tools notNilString:[self currency]]
                                                                        reference:[Tools notNilString:[self reference]]
                                                                           email1:[self destinationEmail1]
                                                                           email2:[self destinationEmail2]
                                                                           phone1:[self destinationSMS1]
                                                                         carrier1:carrier1
                                                                           phone2:[self destinationSMS2]
                                                                         carrier2:carrier2
                                                                       andMessage:[self emailMessage]];
        
        result = YES;
        
    }
    
    return result;
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startFORequestDataValidation {
	
	NSString *result = nil;
    
	if ([self selectedOriginAccountIndex] == NSNotFound || [self selectedOriginAccountIndex] < 0 || [self selectedOriginAccountIndex] > [[self originAccountList] count]) {
		
		result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
		
	} else {
		
		BankAccount *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
		NSString *originAccountNumber = [originAccount number];
        
        if (([selectedDestinationAccountOffice_ length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_OFFICE_TEXT_KEY, nil);
            
        } else if (([selectedDestinationAccountAccNumber_ length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_ACCNUMBER_TEXT_KEY, nil);
            
        } else if (([selectedDestinationAccountOffice_ length] != 4)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_OFFICE_TEXT_KEY, nil);
            
        } else if (([selectedDestinationAccountAccNumber_ length] != 10)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_ACCNUMBER_TEXT_KEY, nil);
            
        } else {
            
            NSString *destinationAccountNumber = [NSString stringWithFormat:@"0011-%@-%@-%@", selectedDestinationAccountOffice_, [selectedDestinationAccountAccNumber_ substringToIndex:2], [selectedDestinationAccountAccNumber_ substringFromIndex:2]];
            
            if ([originAccountNumber isEqualToString:destinationAccountNumber]) {
                
                result = NSLocalizedString(TRANSFERS_TO_THIRD_ACCOUNTS_SAME_ACCOUNT_ERROR_KEY, nil);
                
            } else if ([self selectedCurrencyIndex] < 0) {
                
                result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
                
            } else {
                
                BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
                NSString *originAccountNumber = originAccount.number;
                
                NSString *currencySelected = @"";
                
                if (([self selectedCurrencyIndex] >= 0) && ([self selectedCurrencyIndex] < [self.currencyList count])) {
                    
                    currencySelected = [self.currencyList objectAtIndex:self.selectedCurrencyIndex];
                    
                }
                
                if (!([originAccountNumber length] > 0)) {
                    
                    result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
                    
                } else if (!([currencySelected length] > 0)) {
                    
                    result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
                    
                } else if (([self.amountString isEqualToString:@"0.00"]) || ([self.amountString floatValue] == 0.0f)) {
                    
                    result = NSLocalizedString(TRANSFER_ERROR_AMOUNT_TEXT_KEY, nil);
                    
                }
            }
        }
		
	}
    
    if (result == nil) {
        result = [super startFORequestDataValidation];
    }
    
    return result;
    
}

- (BOOL)startFoConfirmationRequest{
    BOOL result = [super startFoConfirmationRequest];
    
    [[Updater getInstance] transferToThirdAccountsResultFromSecondFactorKey:@""];
    
    return result;
}

/**
 * @param FOResponse The FO response received from the server
 * @return YES when the confirmation response is correct, NO otherwise
 */
- (BOOL)fOResponseReceived:(StatusEnabledResponse *)foResponse{
   
    BOOL result = [super fOResponseReceived:foResponse];
    
    if (!result) {
        
        if (![foResponse isError] && [foResponse isKindOfClass:[TransferConfirmationResponse class]]) {
            
            TransferConfirmationResponse *response = (TransferConfirmationResponse *)foResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response.additionalInformation retain];
        
            self.seal = [additionalInformation_ seal];
            
            result = YES;
            
        }
    }
    
    return result;
}

/*
 * Notifies the fO operation helper the confirmation response received from the server. The fo operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        
        if ( ![confirmationResponse isError] && [confirmationResponse isKindOfClass:[TransferSuccessResponse class]]) {
            
            TransferSuccessResponse *response = (TransferSuccessResponse *)confirmationResponse;
            
            if (transferSuccessAdditionalInfo_ != nil) {
                [transferSuccessAdditionalInfo_ release];
                transferSuccessAdditionalInfo_ = nil;
            }
            transferSuccessAdditionalInfo_ = [response.additionalInformation retain];
            
            result = YES;
            
        }
        
    }
    
    return result;

    
}

@end
