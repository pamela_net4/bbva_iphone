//
//  FOTransferToOtherBankAccount.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/17/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOOperationHelper.h"

@class FOEOtherBankTransferStepOneResponse;
@class TransferConfirmationAdditionalInformation;
@class TransferSuccessAdditionalInformation;
@interface FOTransferToOtherBankAccount : FOOperationHelper{
    
@private
    
    /**
     * Array with the origin selectable accounts. All objects are PaymentElement instances
     */
    NSMutableArray *originAccountList_;
    
    /**
     * Selected destination bank code
     */
    NSString *selectedDestinationBank_;
    
    /**
     * Selected destination account office
     */
    NSString *selectedDestinationAccountOffice_;
    
    /**
     * Selected destination account number
     */
    NSString *selectedDestinationAccountNumber_;
    
    /**
     * Selected destination account control number
     */
    NSString *selectedDestinationAccountControl_;
    
    
    /**
     * Flag to indicate if is owner of the account
     */
    BOOL isItf_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    FOEOtherBankTransferStepOneResponse *startUpResponseInformation_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    TransferConfirmationAdditionalInformation *additionalInformation_;
    
    /**
     * Transfer Success Additional Information
     */
    TransferSuccessAdditionalInformation *transferSuccessAdditionalInfo_;
    
    
    /**
     * Provides readwrite access to the commissionN
     */
    NSString *commissionN_;
    
    /**
     * Provides readwrite access to the commissionL
     */
    NSString *commissionL_;
    
    /**
     * Provides readwrite access to the totalPaymentN
     */
    NSString *totalPaymentN_;
    
    /**
     * Provides readwrite access to the totalPaymentL
     */
    NSString *totalPaymentL_;
    
    /**
     * Provides readwrite access to the typeFlow
     */
    NSString *typeFlow_;
    
    /**
     * Array with the document type list
     */
    NSArray *documentTypeList_;
    
    /**
     * Selected document type index
     */
    NSInteger selectedDocumentTypeIndex_;
    
    /**
     * Selected document type index
     */
    NSInteger selectedTransferTypeIndex_;
    
    /**
     * Document number
     */
    NSString *documentNumber_;
    
    /**
     * Beneficiary
     */
    NSString *beneficiary_;
    
    NSString *beneficiaryDocumentTypeId;

    
}

/**
 * Provides read-only access to the array with the origin selectable accounts
 */
@property (nonatomic, readonly, retain) NSArray *originAccountList;


/**
 * Provides readwrite access to the itfSelected
 */
@property (nonatomic, readwrite, assign) BOOL isItf;


/**
 * Provides readwrite access to the commissionN
 */
@property (nonatomic, readwrite, copy) NSString *commissionN;

/**
 * Provides readwrite access to the commissionL
 */
@property (nonatomic, readwrite, copy) NSString *commissionL;

/**
 * Provides readwrite access to the totalPaymentN
 */
@property (nonatomic, readwrite, copy) NSString *totalPaymentN;

/**
 * Provides readwrite access to the totalPaymentL
 */
@property (nonatomic, readwrite, copy) NSString *totalPaymentL;

/**
 * Provides readwrite access to the typeFlow
 */
@property (nonatomic, readwrite, copy) NSString *typeFlow;


/**
 * Provides read-only access to the array of document types
 */
@property (nonatomic, readonly, retain) NSArray *documentTypeList;

/**
 * Provides readwrite access to the selectedDocumentTypeIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedDocumentTypeIndex;

/**
 * Provides readwrite access to the selectedDocumentTypeIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedTransferTypeIndex;

/**
 * Provides readwrite access to the documentNumber
 */
@property (nonatomic, readwrite, copy) NSString *documentNumber;

/**
 * Provides readwrite access to the beneficiary
 */
@property (nonatomic, readwrite, copy) NSString *beneficiary;

@property (nonatomic, readwrite, copy) NSString *beneficiaryDocumentTypeId;

/**
 * Designated initialized. Initializes a FOEThirdAccountTransferStepOneResponse instance with the initial transfer accounts response
 *
 * @param transferStartupResponse The transfer accounts response
 * @return The initialized FOEThirdAccountTransferStepOneResponse instance;
 */
- (id)initWithTransferStartupResponse:(FOEOtherBankTransferStepOneResponse *)fOEToOtherBankAccountTransferStepOneResponse;

- (NSString *)startSecondTransferRequestDataValidation;
- (BOOL)startTypeTransferInmediateRequest;
- (BOOL)startTypeTransferToConfirmRequest;
@end

