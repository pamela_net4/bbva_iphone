//
//  FORechargeCellPhone.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/25/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORechargeCellPhone.h"

#import "FOERechargeCellphoneStepOneResponse.h"
#import "FOAccountList.h"
#import "BankAccount.h"
#import "AccountList.h"
#import "FOCardList.h"
#import "FOCard.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "PaymentConfirmationResponse.h"
#import "PaymentSuccessResponse.h"
#import "Tools.h"
#import "Updater.h"
#import "FOECarrier.h"

@implementation FORechargeCellPhone
#pragma mark -
#pragma mark Properties

@synthesize originAccountList = originAccountList_;
#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [originAccountList_ release];
    originAccountList_ = nil;
    
    [super dealloc];
    
}
- (id)initWithPaymentStartupResponse:(FOERechargeCellphoneStepOneResponse *)fOERechargeCellphoneStepOneResponse{
    
    if ((self = [super init])) {
        
        // The initial operation in iPad returns elements no needed in iPhone
        Session *session = [Session getInstance];
        originAccountList_ = [[NSMutableArray alloc] initWithArray:session.accountList.transferAccountList];
        
        [self setSelectedOriginAccountIndex:-1];
        
        if (startUpResponseInformation_ != nil) {
            [startUpResponseInformation_ release];
            startUpResponseInformation_ = nil;
        }
        
        [self setSelectedCurrencyIndex:0];
        
        startUpResponseInformation_ = fOERechargeCellphoneStepOneResponse;
        [startUpResponseInformation_ retain];
        
        self.carrierList = [[NSArray alloc] initWithArray:[[fOERechargeCellphoneStepOneResponse foCarrierList] alterCarrierList]];
    }
    
    return self;
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the frequent operation type. Base class returns a default value (transfer between user accounts)
 *
 * @return The frequent operation type
 */
- (FOTypeEnum)foOperationType {
    
    return FOTEPaymentRechargeCellPhone;
    
}

/*
 * Returns the List of currency for the operation. Base class returns an empty array
 *
 * @return The currency list
 */
- (NSArray *)currencyList {
    
    return [NSArray arrayWithObjects:NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil),
            nil];
    
}

/*
 * Returns the can show legal terms flag.
 *
 */
- (BOOL)canShowLegalTerms {
    
    return FALSE;
    
}

/*
 * Returns the can show email and sms
 */
- (BOOL)canSendEmailandSMS {
    
    return YES;
    
}

/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedFOOperationTypeString {
    
    return NSLocalizedString(CARD_PAYMENT_THIRD_CARD_TITLE_TEXT_KEY, nil);
    
}

/*
 * Returns the localized terms and conditions string. The transfer between accounts legal terms localized string is returned
 *
 * @return The localized transfer legal terms operation type string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return NSLocalizedString(TRANSFER_LEGAL_TERMS_KEY, nil);
    
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
#if defined(SIMULATE_HTTP_CONNECTION)
    return [Tools notNilString:@"http://www.google.es"];
#else
    return [Tools notNilString:additionalInformation_.disclaimer];
#endif
    
}

#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foFirstStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Service
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SERVICE_TITLE_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.service]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Frequent operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_POSITION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.nick]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Company selected
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(PAYMENT_RECHARGE_BUSINESS_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.company]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Customer phone number
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_PAYMENT_RECHARGE_PHONE_AND_CLIENT_CODE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:(startUpResponseInformation_.phoneNumber != nil)?startUpResponseInformation_.phoneNumber:startUpResponseInformation_.cellPhone]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Service holder
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SERVICE_TITULAR_LOWERCASE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.holder]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foSecondStepInformation{
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Operation
    
    //Paid amount
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(PAYMENT_RECHARGE_AMOUNT_TO_RECHARGE_TEXT_KEY, nil);
    
    NSString *currencySymbol = [Tools getCurrencySimbol:[self.currency uppercaseString]];
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:currencySymbol], [Tools notNilString:self.amountString]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.operation]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(@"Empresa", nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.company]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Charged Account
    BankAccount *chargeAccount = [originAccountList_ objectAtIndex:self.selectedOriginAccountIndex];
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_ORIGIN_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ | %@ %@",[Tools notNilString:chargeAccount.accountType], [Tools notNilString:[Tools getCurrencyLiteral:chargeAccount.currency]], [Tools notNilString:chargeAccount.number]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
   
    
    //Customer phone number
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_PAYMENT_RECHARGE_PHONE_AND_CLIENT_CODE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.cellPhone]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Service holder
    /*
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SERVICE_TITULAR_LOWERCASE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.holder]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    */
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foThirdStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Operation number
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(NUMBER_OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:paymentSuccessAdditionalInfo_.operationNumber]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DATE_TRANSFER_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ | %@",[Tools notNilString:paymentSuccessAdditionalInfo_.transactionDate],[Tools notNilString:paymentSuccessAdditionalInfo_.transactionHour]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    //Operation type
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_OPERATION_TYPE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:paymentSuccessAdditionalInfo_.operation]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(@"Empresa", nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.company]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    //Charged Account
    BankAccount *chargeAccount = [originAccountList_ objectAtIndex:self.selectedOriginAccountIndex];
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_ORIGIN_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ | %@ %@", [Tools notNilString:chargeAccount.accountType],[Tools getCurrencyLiteral:[Tools notNilString:chargeAccount.currency]],[Tools notNilString:chargeAccount.number]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
 
    
    //Customer phone number
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_PAYMENT_RECHARGE_PHONE_AND_CLIENT_CODE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.cellPhone]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    /*
    //Service holder
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SERVICE_TITULAR_LOWERCASE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.holder]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }*/
    
    //charged amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_OF_RECHARGE_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:[Tools notNilString:paymentSuccessAdditionalInfo_.amountRechargeCurrency]], [Tools notNilString:paymentSuccessAdditionalInfo_.amountRecharge]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    if(([paymentSuccessAdditionalInfo_ changeType] != nil) &&
       (![[paymentSuccessAdditionalInfo_ changeType] isEqualToString:@""]) &&
       ([[paymentSuccessAdditionalInfo_ changeType] floatValue] != 0.0f))
    {
        //Exchange rate
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(CHANGE_TYPE_TITLE_KEY, nil);
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:paymentSuccessAdditionalInfo_.changeTypeCurrency] ,paymentSuccessAdditionalInfo_.changeType]];
        
        if (titleAndAttributes != nil) {
            
            [mutableResult addObject:titleAndAttributes];
            
        }
    }
    

    
    //charged amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(PAYMENT_RECHARGE_AMOUNT_CHARGED_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:[Tools notNilString:paymentSuccessAdditionalInfo_.totalChargedCurrency]], [Tools notNilString:paymentSuccessAdditionalInfo_.totalCharged]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
}

/*
 * Creates the account array to display in the picker view. Default implementation returns an empty array
 */
- (NSArray *)accountStringList{
    NSMutableArray *accountArray = [[[NSMutableArray alloc] init] autorelease];
    
    for (BankAccount *account in originAccountList_) {
        NSString *accountNumber = account.bankAccountListName;
        
        [accountArray addObject:accountNumber];
    }
    
    return accountArray;
}

/**
 * Returns the key name for the confirmation response
 *
 */
- (NSString *)notificationConfirmationKey{
    return kNotificationPaymentConfirmationResultEnds;
}

/**
 * Returns the key name for the success response
 *
 */
- (NSString *)notificationSuccessKey{
    return kNotificationPaymentSuccessResultEnds;
}

#pragma mark -
#pragma mark Operation methods
/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startFORequest {
    
    BOOL result = NO;
    BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    NSString *originAccountNumber = originAccount.number;
    
    self.amountString = [Tools formatAmountWithDotDecimalSeparator:self.amountString];
    
    self.currency =  [self.currencyList objectAtIndex:[self selectedCurrencyIndex]];
    
    if (([originAccountNumber length] > 0) &&
        ([self.currency length] > 0) ){
        
        NSString *carrier1 = @"";
        NSString *carrier2 = @"";
        
        if (self.selectedCarrier1Index > 0 && self.selectedCarrier1Index <  [self.carrierList count]) {
            AlterCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier1Index];
            
            carrier1 = carrier.code;
        }
        
        
        if (self.selectedCarrier2Index > 0 && self.selectedCarrier2Index <  [self.carrierList count]) {
            AlterCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier2Index];
            
            carrier2 = carrier.code;
        }
        
        NSString *destinationPhone = (startUpResponseInformation_.phoneNumber != nil)?startUpResponseInformation_.phoneNumber:startUpResponseInformation_.cellPhone;
        
        [[Updater getInstance] obtainPaymentRechargeConfirmationForPhoneNumber:destinationPhone
																		 issue:[originAccount branchAccount]
																		amount:self.amountString
																		email1:[Tools notNilString:[self destinationEmail1]]
                                                                        email2:[Tools notNilString:[self destinationEmail2]]
                                                                  phoneNumber1:[Tools notNilString:[self destinationSMS1]]
                                                                  phoneNumber2:[Tools notNilString:[self destinationSMS2]]
                                                                      carrier1:carrier1
                                                                      carrier2:carrier2
                                                                       message:[Tools notNilString:[self emailMessage]]];
        
        result = YES;
        
    }
    
    return result;
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startFORequestDataValidation {
	
	NSString *result = nil;
    
    NSDecimalNumber *ammount = [Tools decimalFromServerString: [self amountString]];
    
    if ([[[startUpResponseInformation_ company] lowercaseString] isEqualToString: [NSLocalizedString(MOVISTAR_CARRIER_TEXT_KEY, nil) lowercaseString]]) {
        
        for (FOECarrier *carrier in [[startUpResponseInformation_ foeCarrierList] carrierList]) {
            if ([[[carrier carrierCode] lowercaseString] isEqualToString: @"movi"]) {
                
                
                NSDecimalNumber *max = [Tools decimalFromServerString:[carrier max]];
                NSDecimalNumber *min = [Tools decimalFromServerString:[carrier min]];
                
                if ([max compare: ammount] == NSOrderedAscending) {
                    result = NSLocalizedString(PAYMENT_RECHARGE_ERROR_INVALID_MONTO_TEXT_KEY, nil);
                } else if([ammount compare: min] == NSOrderedAscending){
                    result = NSLocalizedString(PAYMENT_RECHARGE_ERROR_INVALID_MONTO_TEXT_KEY, nil);
                }
            }
        }
    } else if ([[[startUpResponseInformation_ company] lowercaseString] isEqualToString: [NSLocalizedString(CLARO_CARRIER_TEXT_KEY, nil) lowercaseString]]) {
            
            for (FOECarrier *carrier in [[startUpResponseInformation_ foeCarrierList] carrierList]) {
                
                NSDecimalNumber *max = [Tools decimalFromServerString:[carrier max]];
                NSDecimalNumber *min = [Tools decimalFromServerString:[carrier min]];
                
                if ([max compare: ammount] == NSOrderedAscending) {
                    result = NSLocalizedString(PAYMENT_RECHARGE_ERROR_INVALID_MONTO_TEXT_KEY, nil);
                } else if([ammount compare: min] == NSOrderedAscending){
                    result = NSLocalizedString(PAYMENT_RECHARGE_ERROR_INVALID_MONTO_TEXT_KEY, nil);
                }
            }
        }
    
    
	if ([self selectedOriginAccountIndex] == NSNotFound || [self selectedOriginAccountIndex] < 0 || [self selectedOriginAccountIndex] > [[self originAccountList] count]) {
		
		result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
		
	} else {
        
        if ([self selectedCurrencyIndex] < 0) {
            
            result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
            
        } else {
            
            BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
            NSString *originAccountNumber = originAccount.number;
            
            NSString *currencySelected = @"";
            
            if (([self selectedCurrencyIndex] >= 0) && ([self selectedCurrencyIndex] < [self.currencyList count])) {
                
                currencySelected = [self.currencyList objectAtIndex:self.selectedCurrencyIndex];
                
            }
            
            if (!([originAccountNumber length] > 0)) {
                
                result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
                
            } else if (!([currencySelected length] > 0)) {
                
                result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
                
            } else if (([self.amountString isEqualToString:@"0.00"]) || ([self.amountString floatValue] == 0.0f)) {
                
                result = NSLocalizedString(TRANSFER_ERROR_AMOUNT_TEXT_KEY, nil);
                
            }
            
        }
		
	}
    
    if (result == nil) {
        result = [super startFORequestDataValidation];
    }
    
    return result;
    
}

- (BOOL)startFoConfirmationRequest{
    BOOL result = [super startFoConfirmationRequest];
    
    
    
    [[Updater getInstance] obtainPaymentRechargeSuccessForSecondFactorKey:@""
                                                                  carrier:([startUpResponseInformation_.companyCode isEqualToString:FO_LANDLINE] && [startUpResponseInformation_.serviceType isEqualToString:FO_RECHARGE])?@"movi":[startUpResponseInformation_.companyCode lowercaseString]];
    
    return result;
}

/**
 * @param FOResponse The FO response received from the server
 * @return YES when the confirmation response is correct, NO otherwise
 */
- (BOOL)fOResponseReceived:(StatusEnabledResponse *)foResponse{
    
    BOOL result = [super fOResponseReceived:foResponse];
    
    if (!result) {
        
        if (![foResponse isError] && [foResponse isKindOfClass:[PaymentConfirmationResponse class]]) {
            
            PaymentConfirmationResponse *response = (PaymentConfirmationResponse *)foResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response retain];
            
            self.seal = [additionalInformation_ seal];
            
            result = YES;
            
        }
    }
    
    return result;
}

/*
 * Notifies the fO operation helper the confirmation response received from the server. The fo operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        
        if (![confirmationResponse isError] && [confirmationResponse isKindOfClass:[PaymentSuccessResponse class]]) {
            
            PaymentSuccessResponse *response = (PaymentSuccessResponse *)confirmationResponse;
            
            if (paymentSuccessAdditionalInfo_ != nil) {
                [paymentSuccessAdditionalInfo_ release];
                paymentSuccessAdditionalInfo_ = nil;
            }
            paymentSuccessAdditionalInfo_ = [response retain];
            
            result = YES;
            
        }
        
    }
    
    return result;
    
    
}

@end
