//
//  FOInstitutionsAndCompanies.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/24/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOInstitutionsAndCompanies.h"

#import "FOEInstitutionPaymentStepOneResponse.h"
#import "BankAccount.h"
#import "accountPay.h"
#import "accountPayList.h"
#import "CardType.h"
#import "PendingDocument.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "PaymentInstitutionAndCompaniesConfirmationInformationResponse.h"
#import "PaymentInstitutionAndCompaniesSuccessConfirmationResponse.h"
#import "PendingDocumentList.h"
#import "Tools.h"
#import "Updater.h"
#import "Ocurrency.h"
#import "OcurrencyList.h"
#import "PendingDocumentList.h"

@implementation FOInstitutionsAndCompanies
#pragma mark -
#pragma mark Properties

@synthesize originAccountList = originAccountList_;
@synthesize originCardList = originCardList_;
@synthesize isDB = isDB_;
@synthesize isPartial = isPartial_;
@synthesize brand = brand_;
@synthesize pendingDocumentsArray = pendingDocumentsArray_;
@synthesize startUpResponseInformation = startUpResponseInformation_;
@synthesize additionalInformation = additionalInformation_;
#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [originAccountList_ release];
    originAccountList_ = nil;
    
    
    [originCardList_ release];
    originCardList_ = nil;
    
    [brand_ release];
    brand_ = nil;
    
    [pendingDocumentsArray_ release];
    pendingDocumentsArray_ = nil;
    
    [super dealloc];
    
}
- (id)initWithPaymentStartupResponse:(FOEInstitutionPaymentStepOneResponse *)fOEInstitutionPaymentStepOneResponse{
    
    if ((self = [super init])) {
        
        // The initial operation in iPad returns elements no needed in iPhone
        
        [self setSelectedOriginAccountIndex:NSNotFound];
        [self setSelectedOriginCardIndex:NSNotFound];
        
        if (startUpResponseInformation_ != nil) {
            [startUpResponseInformation_ release];
            startUpResponseInformation_ = nil;
        }
        
        [self setSelectedCurrencyIndex:0];
        
        startUpResponseInformation_ = fOEInstitutionPaymentStepOneResponse;
        [startUpResponseInformation_ retain];
        
        self.carrierList = [[NSArray alloc] initWithArray:[[fOEInstitutionPaymentStepOneResponse foCarrierList] alterCarrierList]];
        
        newBrand = @"";
    }
    
    return self;
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the frequent operation type. Base class returns a default value (transfer between user accounts)
 *
 * @return The frequent operation type
 */
- (FOTypeEnum)foOperationType {
    
    return FOTEPaymentInstAndComp;
    
}

/*
 * Returns the List of currency for the operation. Base class returns an empty array
 *
 * @return The currency list
 */
- (NSArray *)currencyList {
    
    return [NSArray arrayWithObjects:NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil),
            NSLocalizedString(DOLLARS_CURRENCY_TEXT_KEY, nil),
            nil];
    
}

/*
 * Returns the can show legal terms flag.
 *
 */
- (BOOL)canShowLegalTerms {
    
    return NO;
    
}

/*
 * Returns the can show email and sms
 */
- (BOOL)canSendEmailandSMS {
    
    return YES;
    
}

/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedFOOperationTypeString {
    
    return NSLocalizedString(PAYMENT_INSTITUTIONS_AND_COMPANIES_TITLE_TEXT_KEY, nil);
    
}

/*
 * Returns the localized terms and conditions string. The transfer between accounts legal terms localized string is returned
 *
 * @return The localized transfer legal terms operation type string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return NSLocalizedString(TRANSFER_LEGAL_TERMS_KEY, nil);
    
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
#if defined(SIMULATE_HTTP_CONNECTION)
    return [Tools notNilString:@"http://www.google.es"];
#else
    return [Tools notNilString:additionalInformation_.disclaimer];
#endif
    
}

#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSMutableArray *)foFirstStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Service
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SERVICE_TITLE_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.service]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Frequen operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_POSITION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.nick]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //institution and companie description
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_INSTITUTIONS_COMPANIES_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.agreement]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
        
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foSecondStepInformation{
    NSMutableArray *mutableResult = [NSMutableArray array];
    

    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    
    
    titleAndAttributes.titleString = NSLocalizedString(INSTITUTIONS_COMPANIES_CONFIRMATION_AMOUNT_KEY, nil);
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@%@", additionalInformation_.payAmountCurrency,
                                      additionalInformation_.payAmount]];
    
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
  
        //Operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.operation]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
  
    
    //Service
    if([[startUpResponseInformation_.agreement lowercaseString] rangeOfString:@"directv"].location == NSNotFound)
    {
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(SERVICE_TITLE_KEY, nil);
        [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.service]];
    
        if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
        }
    }
    
    //Frequen operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_POSITION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.nick]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Charged account
    NSString *selectedAccountText = [NSString stringWithFormat:@"%@ | %@",additionalInformation_.accountType,additionalInformation_.currency];
    NSString *numberAccountText=[NSString stringWithFormat:@"%@",additionalInformation_.subject];
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_ORIGIN_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:selectedAccountText];
    [titleAndAttributes addAttribute:numberAccountText];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //institution and companie description
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_INSTITUTIONS_COMPANIES_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.agreement]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Agreement datas
    for (int i = 0; i < [[[additionalInformation_ ocurrencyList] ocurrencyList] count]; i++) {
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        Ocurrency *ocurrency = [[[additionalInformation_ ocurrencyList] ocurrencyList] objectAtIndex: i];
        [titleAndAttributes setTitleString: [Tools notNilString: [ocurrency description]]];
        if ([ocurrency valueCurrency] != nil) {
            [titleAndAttributes addAttribute: [NSString stringWithFormat: @"%@%@",[ocurrency valueCurrency],[ocurrency value]]];
        } else {
            [titleAndAttributes addAttribute: [ocurrency value]];
        }
        
        if(titleAndAttributes != nil){
            [mutableResult addObject: titleAndAttributes];
        }
    }
    

    if (isDB_) {

        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
         [titleAndAttributes setTitleString:NSLocalizedString(INSTITUTIONS_COMPANIES_HOLDER_NAME_KEY, nil)];
        [titleAndAttributes addAttribute:additionalInformation_.
         holderName ];
        
        if(titleAndAttributes != nil){
            [mutableResult addObject: titleAndAttributes];
        }

        
        if (isPartial_) {
            
            PendingDocument *doc = [[additionalInformation_.docList pendingDocumentList] objectAtIndex:0];
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
            [titleAndAttributes setTitleString: NSLocalizedString(DOCUMENT_TITLE_KEY, nil)];
            
            NSString *docDescription = @"";
            if([doc.description length]>0)
            {
                docDescription = [doc.description stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[doc.description substringToIndex:1] uppercaseString]];
            }
            
            [titleAndAttributes addAttribute:docDescription];
            
            if(titleAndAttributes != nil){
                [mutableResult addObject: titleAndAttributes];
            }

            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString:NSLocalizedString(PUBLIC_SERVICE_STEP_TWO_SELECTED_DUE_DATE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute:doc.expirationDate];
            
            if(titleAndAttributes != nil){
                [mutableResult addObject: titleAndAttributes];
            }

            titleAndAttributes = [TitleAndAttributes titleAndAttributes];

            NSString *minAmount= [NSString stringWithFormat:@"%@%@", doc.minAmountCurrency, doc.minAmount];
            
            [titleAndAttributes setTitleString:NSLocalizedString(INSTITUTIONS_COMPANIES_MIN_AMOUNT_KEY, nil)];
            [titleAndAttributes addAttribute:minAmount];
            
            
            if(titleAndAttributes != nil){
                [mutableResult addObject: titleAndAttributes];
            }
            

            titleAndAttributes = [TitleAndAttributes titleAndAttributes];

            NSString *maxAmount = [NSString stringWithFormat:@"%@%@", doc.maxAmountCurrency, doc.maxAmount];
            
            [titleAndAttributes setTitleString:NSLocalizedString(INSTITUTIONS_COMPANIES_MAX_AMOUNT_KEY, nil)];
            [titleAndAttributes addAttribute:maxAmount];
            
            if(titleAndAttributes != nil){
                [mutableResult addObject: titleAndAttributes];
            }

            
            
        }else{
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            

            
             [titleAndAttributes setTitleString: NSLocalizedString(INSTITUTIONS_COMPANIES_DOCUMENTS_NUMBER_KEY, nil)];
            [titleAndAttributes addAttribute:additionalInformation_.docNumber];
            
            if(titleAndAttributes != nil){
                [mutableResult addObject: titleAndAttributes];
            }

            
            NSArray *pendingDocs = [additionalInformation_.docList pendingDocumentList];
            
            for (PendingDocument *doc in pendingDocs) {
                
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                
                
                [titleAndAttributes setTitleString: [Tools notNilString:[NSString stringWithFormat:@"Recibo %@", doc.description]]];
                
                NSString *totalAmount = [NSString stringWithFormat:@"%@%@", doc.totalAmountCurrency, doc.totalAmount];
                
                 [titleAndAttributes addAttribute:totalAmount];
                
                if(titleAndAttributes != nil){
                    [mutableResult addObject: titleAndAttributes];
                }

            }
        }
    }
    
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foThirdStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Operation Number
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CASH_MOBILE_NUMBER_OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:paymentSuccessAdditionalInfo_.operationNumber]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Holder Name
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_HOLDER_NAME_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:paymentSuccessAdditionalInfo_.holderName]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
        
    //Date/hour
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_OPERATION_DATE_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:[paymentSuccessAdditionalInfo_ date]], [Tools notNilString:[paymentSuccessAdditionalInfo_ time]] ]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Operation
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:paymentSuccessAdditionalInfo_.operation]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Charged account
    NSString *selectedAccountText = [NSString stringWithFormat:@"%@ | %@",paymentSuccessAdditionalInfo_.accountType,paymentSuccessAdditionalInfo_.currency];
    NSString *numberAccountText=[NSString stringWithFormat:@"%@",paymentSuccessAdditionalInfo_.subject];
    /*
    if ([self selectedOriginAccountIndex] != NSNotFound) {
        accountPay *account = [originAccountList_ objectAtIndex:[self selectedOriginAccountIndex]];
        selectedAccountText = [NSString stringWithFormat:@"%@ - %@", [account accountType],[Tools getCurrencyLiteral:[[account currency] uppercaseString] ] ];
        numberAccountText=[NSString stringWithFormat:@"%@",[account subject]];
    }else{
        CardType *card = [originCardList_ objectAtIndex:[self selectedOriginCardIndex]];
        selectedAccountText = [NSString stringWithFormat:@"%@ | %@", [card type],[Tools getCurrencyLiteral:[[card currency] uppercaseString] ]];
        numberAccountText=[NSString stringWithFormat:@"%@",[card number]];
    }*/
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_ORIGIN_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:selectedAccountText];
    [titleAndAttributes addAttribute:numberAccountText];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //institution and companie description
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_INSTITUTIONS_COMPANIES_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.agreement]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    for (int i = 0; i < [[[paymentSuccessAdditionalInfo_ ocurrencyList] ocurrencyList] count]; i++) {
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        Ocurrency *ocurrency = [[[paymentSuccessAdditionalInfo_ ocurrencyList] ocurrencyList] objectAtIndex: i];
        [titleAndAttributes setTitleString: [Tools notNilString: [ocurrency description]]];
        if ([ocurrency valueCurrency] != nil) {
            [titleAndAttributes addAttribute: [NSString stringWithFormat: @"%@%@",[ocurrency valueCurrency],[ocurrency value]]];
        } else {
            [titleAndAttributes addAttribute: [ocurrency value]];
        }
        if(titleAndAttributes != nil){
            [mutableResult addObject: titleAndAttributes];
        }
    }
    
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(INSTITUTIONS_COMPANIES_CONFIRMATION_AMOUNT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat: @"%@%@",[Tools notNilString:paymentSuccessAdditionalInfo_.payedAmountCurrency],[Tools notNilString:paymentSuccessAdditionalInfo_.payedAmount]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    if(![paymentSuccessAdditionalInfo_.chargedAmountCurrency isEqualToString:paymentSuccessAdditionalInfo_.payedAmountCurrency] )
    {
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(CHANGE_TYPE_TITLE_KEY, nil);
        [titleAndAttributes addAttribute:[NSString stringWithFormat: @"%@%@",[Tools notNilString:paymentSuccessAdditionalInfo_.changeTypeCurrency],[Tools notNilString:paymentSuccessAdditionalInfo_.changeType]]];
    
        if (titleAndAttributes != nil) {
        
            [mutableResult addObject:titleAndAttributes];
        
        }
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(INSTITUTIONS_COMPANIES_CHARGED_AMOUNT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat: @"%@%@",[Tools notNilString:paymentSuccessAdditionalInfo_.chargedAmountCurrency],[Tools notNilString:paymentSuccessAdditionalInfo_.chargedAmount]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
}

/*
 * Creates the account array to display in the picker view. Default implementation returns an empty array
 */
- (NSArray *)accountStringList{
    
    NSArray *accountPayArray = originAccountList_;
    NSMutableArray *accountStringArray  = [[[NSMutableArray alloc]init] autorelease];
    for(int i=0;i<[accountPayArray count];i++)
    {
        [accountStringArray addObject: ((accountPay*)[accountPayArray objectAtIndex:i]).accountIdAndDescription];
    }
    
    return accountStringArray;
}

-(NSArray *)cardStringList
{
    NSArray *cardTypeArray = originCardList_;
    NSMutableArray *cardStringArray  = [[[NSMutableArray alloc]init] autorelease];
    for(int i=0;i<[cardTypeArray count];i++)
    {
        CardType *card = [cardTypeArray objectAtIndex:i];
        NSString *cardType = card.type;
        [cardStringArray addObject: [NSString stringWithFormat:@"%@ | %@", [Tools obfuscateCardNumber:card.num],cardType]];
    }
    
    return  cardStringArray;
}

/**
 * Returns the key name for the confirmation response
 *
 */
- (NSString *)notificationConfirmationKey{
    return kNotificationPaymentInstitutionsConfirmationPayResult;
}

/**
 * Returns the key name for the success response
 *
 */
- (NSString *)notificationSuccessKey{
    return kNotificationPaymentInstitutionsSuccessPayResult;
}

#pragma mark -
#pragma mark Operation methods
/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startFORequest {
    
    BOOL result = NO;
    NSString *originAccountNumber = @"";
    NSString *originCardNumber = @"";
    NSString *form = @"";
    
    if (([self selectedOriginAccountIndex] != NSNotFound &&
         [self selectedOriginAccountIndex] >= 0 &&
         [self selectedOriginAccountIndex] < [[self originAccountList] count]) ){
        
        accountPay *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
        originAccountNumber = [originAccount accountParam];
        form = @"CT";
        
    }else if( ([self selectedOriginCardIndex] != NSNotFound &&
               [self selectedOriginCardIndex] >= 0 &&
               [self selectedOriginCardIndex] < [[self originCardList] count]) ){
        CardType *originCard = [[self originCardList] objectAtIndex:[self selectedOriginCardIndex]];
        originCardNumber = [originCard param];
        form = @"TC";
    }
    NSString *amount = [Tools formatAmountWithDotDecimalSeparator:self.amountString];
    
    self.currency =  [self.currencyList objectAtIndex:[self selectedCurrencyIndex]];
    
    if (([self.currency length] > 0) ){
        
        NSString *carrier1 = @"";
        NSString *carrier2 = @"";
        
        if (self.selectedCarrier1Index > 0 && self.selectedCarrier1Index <  [self.carrierList count]) {
            AlterCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier1Index];
            
            carrier1 = carrier.code;
        }
        
        if (self.selectedCarrier2Index > 0 && self.selectedCarrier2Index <  [self.carrierList count]) {
            AlterCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier2Index];
            
            carrier2 = carrier.code;
        }
        
        [[Updater getInstance] obtainInstitutionsAndCompaniesPayConfirmationWithForm:form
                                                                   ownSubjectAccount:originAccountNumber
                                                                      ownSubjectCard:originCardNumber
                                                                           payImport:[Tools notNilString:amount]
                                                                        phonenumber1:[Tools notNilString:[self destinationSMS1]]
                                                                        phonenumber2:[Tools notNilString:[self destinationSMS2]]
                                                                              email1:[Tools notNilString:[self destinationEmail1]]
                                                                              email2:[Tools notNilString:[self destinationEmail2]]
                                                                            carrier1:[Tools notNilString:carrier1]
                                                                            carrier2:[Tools notNilString:carrier1]
                                                                         mailMessage:[Tools notNilString:[self emailMessage]]
                                                                               brand:newBrand
                                                                                type:0];
        
        result = YES;
        
    }
    
    return result;
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startFORequestDataValidation {
	
	NSString *result = nil;
    
    if (result == nil) {
       
        NSString *amount = @"";
        
        float total = 0.0f;
        int docIndex = -1;
        BOOL hasOlder = NO;
        if (isDB_) {
            
            newBrand = @"";
            
            for (int i = 0; i < [brand_ count]; i++) {
                
                if ([@"1" isEqualToString:[brand_ objectAtIndex:i]])
                {
                    docIndex = i;
                    if(docIndex ==0)
                    {
                        hasOlder =YES;
                    }

                    if (!isPartial_) {
                        PendingDocument *doc = [pendingDocumentsArray_ objectAtIndex:i];
                        NSString *aux = [doc totalAmount];
                        aux = [Tools formatAmountWithDotDecimalSeparator:aux];
                        total += [aux floatValue];
                    }
                }
                newBrand = [newBrand stringByAppendingString:[brand_ objectAtIndex:i]];
                
            }
            
            if (docIndex == -1) {
                result =  NSLocalizedString(PAYMENT_ERROR_SELECT_RECEIPT_TO_CANCEL_TEXT_KEY, nil);
                return result;
            }
            
            if(!hasOlder)
            {
                 return NSLocalizedString(PAYMENT_ERROR_RECEIPTS_NOT_OLDER_RECEPIPS_TEXT_KEY, nil);
            }
            
            amount = [Tools formatAmountWithDotDecimalSeparator:[Tools notNilString:self.amountString]];
            
            if (isPartial_){
                
                PendingDocument *doc = [pendingDocumentsArray_ objectAtIndex:docIndex];
                
                
                if(docIndex!=0)
                {
                    return  NSLocalizedString(PAYMENT_ERROR_RECEIPTS_NOT_OLDER_RECEPIPS_TEXT_KEY, nil);
                }

                
                total = [amount floatValue];
                
                minAmount = [[doc minAmount] stringByReplacingOccurrencesOfString:@"," withString:@""];
                maxAmount = [[doc maxAmount] stringByReplacingOccurrencesOfString:@"," withString:@""];
                
                if ([@"" isEqualToString:amount] ||
                    [amount isEqualToString:@"0.00"] ||
                    [amount floatValue] == 0.00f)
                {
                    
                    result = NSLocalizedString(INSTITUTIONS_COMPANIES_PARTIAL_AMOUNT_KEY, nil);
                    
                    return result;
                    
                } else if ([amount floatValue] < [minAmount floatValue] || [amount floatValue] > [maxAmount floatValue]) {
                    
                    result = NSLocalizedString(PAYMENT_ERROR_AMOUNT_MUST_IN_RANGE_KEY, nil);
                    
                    return result;
                    
                }
                
            }
            
        }
    }
    
    if (result == nil) {
        if (!self.hasCard && ([self selectedOriginAccountIndex] == NSNotFound || [self selectedOriginAccountIndex] < 0 || [self selectedOriginAccountIndex] > [[self originAccountList] count] ) ) {
            
            result = NSLocalizedString(PAYMENT_ERROR_ACCOUNT_PAYMENT_TEXT_KEY, nil);
            
        } else if ( ([self selectedOriginAccountIndex] == NSNotFound ||
                     [self selectedOriginAccountIndex] < 0 ||
                     [self selectedOriginAccountIndex] > [[self originAccountList] count] )
                   &&
                   ([self selectedOriginCardIndex] == NSNotFound ||
                    [self selectedOriginCardIndex] < 0 ||
                    [self selectedOriginCardIndex] > [[self originCardList] count] ) )
        {
                       
                       result = NSLocalizedString(PAYMENT_ERROR_ACCOUNT_PAYMENT_TEXT_KEY, nil);
                       
        }   else {
                       
                       NSString *originAccountNumber = @"";
                       
                       if (!self.hasCard) {
                           accountPay *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
                           originAccountNumber = [originAccount subject];
                       }else{
                           if (([self selectedOriginAccountIndex] != NSNotFound &&
                                [self selectedOriginAccountIndex] >= 0 &&
                                [self selectedOriginAccountIndex] < [[self originAccountList] count]) ){
                               
                               accountPay *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
                               originAccountNumber = [originAccount subject];
                               
                           }else if( ([self selectedOriginCardIndex] != NSNotFound &&
                                      [self selectedOriginCardIndex] >= 0 &&
                                      [self selectedOriginCardIndex] < [[self originCardList] count]) ){
                               CardType *originCard = [[self originCardList] objectAtIndex:[self selectedOriginCardIndex]];
                               originAccountNumber = [originCard number];
                           }
                       }
                       
                       
                       originAccountNumber = @"";
                       
                       if (([self selectedOriginAccountIndex] != NSNotFound &&
                            [self selectedOriginAccountIndex] >= 0 &&
                            [self selectedOriginAccountIndex] < [[self originAccountList] count]) ){
                           
                           accountPay *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
                           originAccountNumber = [originAccount subject];
                           
                       }else if( ([self selectedOriginCardIndex] != NSNotFound &&
                                  [self selectedOriginCardIndex] >= 0 &&
                                  [self selectedOriginCardIndex] < [[self originCardList] count]) ){
                           CardType *originCard = [[self originCardList] objectAtIndex:[self selectedOriginCardIndex]];
                           originAccountNumber = [originCard number];
                       }
                       
                       if (!([originAccountNumber length] > 0)) {
                           
                           result = NSLocalizedString(PAYMENT_ERROR_ACCOUNT_PAYMENT_TEXT_KEY, nil);
                           
                       }
                }
    }
    
    if (result == nil) {
        result = [super startFORequestDataValidation];
    }
    
    return result;
    
}

- (BOOL)startFoConfirmationRequest{
    BOOL result = [super startFoConfirmationRequest];
    
    [[Updater getInstance] institutionsAndcompaniesConfirmResultFromSecondKeyFactor:@""];
    
    return result;
}

/**
 * @param FOResponse The FO response received from the server
 * @return YES when the confirmation response is correct, NO otherwise
 */
- (BOOL)fOResponseReceived:(StatusEnabledResponse *)foResponse{
    
    BOOL result = [super fOResponseReceived:foResponse];
    
    if (!result) {
        
        if (![foResponse isError] && [foResponse isKindOfClass:[PaymentInstitutionAndCompaniesConfirmationInformationResponse class]]) {
            
            PaymentInstitutionAndCompaniesConfirmationInformationResponse *response = (PaymentInstitutionAndCompaniesConfirmationInformationResponse *)foResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response retain];
            
            self.seal = [additionalInformation_ seal];
            
            result = YES;
            
        }
    }
    
    return result;
}

/*
 * Notifies the fO operation helper the confirmation response received from the server. The fo operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        result = ![confirmationResponse isError];
        
        if (result) {
            if ([confirmationResponse isKindOfClass:[PaymentInstitutionAndCompaniesSuccessConfirmationResponse class]]) {
                
                PaymentInstitutionAndCompaniesSuccessConfirmationResponse *response = (PaymentInstitutionAndCompaniesSuccessConfirmationResponse *)confirmationResponse;
                
                if (paymentSuccessAdditionalInfo_ != nil) {
                    [paymentSuccessAdditionalInfo_ release];
                    paymentSuccessAdditionalInfo_ = nil;
                }
                paymentSuccessAdditionalInfo_ = [response retain];
                
                result = YES;
                
            }
        }
        
    }
    
    return result;
    
    
}

@end
