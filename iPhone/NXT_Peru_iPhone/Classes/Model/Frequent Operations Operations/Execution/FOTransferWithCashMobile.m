//
//  FOTransferWithCashMobile.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/24/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOTransferWithCashMobile.h"

#import "Account.h"
#import "FOECashMobileStepOneResponse.h"
#import "FOAccountList.h"
#import "BankAccount.h"
#import "AccountList.h"
#import "FOCardList.h"
#import "FOCard.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "TransferConfirmationResponse.h"
#import "TransferConfirmationAdditionalInformation.h"
#import "TransferSuccessAdditionalInformation.h"
#import "TransferSuccessResponse.h"
#import "Tools.h"
#import "Updater.h"

@implementation FOTransferWithCashMobile
#pragma mark -
#pragma mark Properties

@synthesize originAccountList = originAccountList_;
#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [originAccountList_ release];
    originAccountList_ = nil;
    
    [super dealloc];
    
}
- (id)initWithTransferStartupResponse:(FOECashMobileStepOneResponse *)fOECashMobileStepOneResponse{
    
    if ((self = [super init])) {
        
        // The initial operation in iPad returns elements no needed in iPhone
        originAccountList_ = [[NSMutableArray alloc] initWithArray:fOECashMobileStepOneResponse.foAccountList.accountList];
        
        [self setSelectedOriginAccountIndex:-1];
        
        if (startUpResponseInformation_ != nil) {
            [startUpResponseInformation_ release];
            startUpResponseInformation_ = nil;
        }
        
        [self setSelectedCurrencyIndex:0];
        
        startUpResponseInformation_ = fOECashMobileStepOneResponse;
        [startUpResponseInformation_ retain];
    }
    
    return self;
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the frequent operation type. Base class returns a default value (transfer between user accounts)
 *
 * @return The frequent operation type
 */
- (FOTypeEnum)foOperationType {
    
    return FOTETransferCashMobile;
    
}

/*
 * Returns the List of currency for the operation. Base class returns an empty array
 *
 * @return The currency list
 */
- (NSArray *)currencyList {
    
    return [NSArray arrayWithObjects:NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil),
            nil];
    
}

/*
 * Returns the can show legal terms flag.
 *
 */
- (BOOL)canShowLegalTerms {
    
    return TRUE;
    
}

/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedFOOperationTypeString {
    
    return NSLocalizedString(TRANSFER_TO_THIRD_ACCOUNTS_TEXT_1_KEY, nil);
    
}

/*
 * Returns the localized terms and conditions string. The transfer between accounts legal terms localized string is returned
 *
 * @return The localized transfer legal terms operation type string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return NSLocalizedString(TRANSFER_LEGAL_TERMS_KEY, nil);
    
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
#if defined(SIMULATE_HTTP_CONNECTION)
    return [Tools notNilString:@"http://www.google.es"];
#else
    return [Tools notNilString:additionalInformation_.disclaimer];
#endif
    
}

#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foFirstStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Service
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SERVICE_TITLE_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.service]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Frequen operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_POSITION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.nick]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(@"Número del beneficiario", nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.phoneNumber]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foSecondStepInformation{
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    BankAccount *selectedOriginAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    
    //origin
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CASH_MOBILE_ACCOUNT_CHARGED_TEXT_KEY, nil);
    
    NSString *originAccount = selectedOriginAccount.subject;
    
    if ([originAccount length] > 0) {
        
        if ([selectedOriginAccount.accountType length] > 0) {
            
            originAccount = [NSString stringWithFormat:@"%@ - %@ - %@", selectedOriginAccount.accountType, [Tools getCurrencyLiteral:selectedOriginAccount.currency] , originAccount];
            
        }
        
    } else {
        
        originAccount = selectedOriginAccount.accountType;
        
    }
    
    [titleAndAttributes addAttribute:originAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    // amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SEND_AMOUNT_TEXT_KEY, nil);
    
    NSString *currency = CURRENCY_SOLES_LITERAL;
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], self.amountString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    
    //destination
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_NUMBER_TITLE_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute:startUpResponseInformation_.phoneNumber];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //itf
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ACCOUNT_ITF_TEXT_KEY, nil);
    
    NSString *marketPlaceCommision = additionalInformation_.itfAmount;
    NSString *currencySymbol = additionalInformation_.itfSymbol;
    
    if (([marketPlaceCommision length] == 0) || ([marketPlaceCommision isEqualToString:@"0.00"])) {
        
        marketPlaceCommision = @"0.00";
        
    } else {
        
        if (([currencySymbol length] == 0) || ([currencySymbol isEqualToString:@"0.00"]))
            currencySymbol = [NSString stringWithFormat:@"%@", [Tools notNilString:[Tools getCurrencySimbol:selectedOriginAccount.currency]]];
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@", marketPlaceCommision]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foThirdStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    BankAccount *selectedOriginAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    
    // Operation number
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CASH_MOBILE_NUMBER_OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operationNumber];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operation];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Origin
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CASH_MOBILE_ACCOUNT_CHARGED_TEXT_KEY, nil);
    
    NSString *originAccount = selectedOriginAccount.subject;
    
    if ([originAccount length] > 0) {
        
        if ([selectedOriginAccount.accountType length] > 0) {
            
            originAccount = [NSString stringWithFormat:@"%@ - %@ - %@", selectedOriginAccount.accountType, [Tools getCurrencyLiteral:selectedOriginAccount.currency], originAccount];
            
        }
        
    } else {
        
        originAccount = selectedOriginAccount.accountType;
        
    }
    
    [titleAndAttributes addAttribute:originAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFERED_SEND_IMPORT_TEXT_KEY, nil);
    
    NSString *currency = CURRENCY_SOLES_LITERAL;
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], self.amountString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Destination
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_NUMBER_TITLE_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute:startUpResponseInformation_.phoneNumber];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // itf
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ACCOUNT_ITF_TEXT_KEY, nil);
    
    NSString *marketPlaceCommision = additionalInformation_.itfAmount;
    NSString *currencySymbol = additionalInformation_.itfSymbol;
    
    if (([marketPlaceCommision length] == 0) || ([marketPlaceCommision isEqualToString:@"0.00"])) {
        
        marketPlaceCommision = @"0.00";
        
    } else {
        
        if (([currencySymbol length] == 0) || ([currencySymbol isEqualToString:@"0.00"]))
            currencySymbol = [NSString stringWithFormat:@"%@", [Tools notNilString:[Tools getCurrencySimbol:selectedOriginAccount.currency]]];
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@", marketPlaceCommision]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Amount charged
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CASH_MOBILE_AMOUNT_PAID_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:currency], transferSuccessAdditionalInfo_.totalAmountToPay]];
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // State
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(STATE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.transferState];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Time
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_DATE_HOUR_SHORT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", transferSuccessAdditionalInfo_.dateString, transferSuccessAdditionalInfo_.hour]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Expiraion Time
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_EXPIRATION_DATE_HOUR_SHORT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", transferSuccessAdditionalInfo_.expirationDateString, transferSuccessAdditionalInfo_.expirationHourString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Notification Message
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = @"";
    [titleAndAttributes addAttribute: transferSuccessAdditionalInfo_.notificationMessage];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
}

/*
 * Creates the account array to display in the picker view. Default implementation returns an empty array
 */
- (NSArray *)accountStringList{
    NSMutableArray *accountArray = [[[NSMutableArray alloc] init] autorelease];
    
    for (BankAccount *account in originAccountList_) {
        NSString *accountNumber = account.accountIdAndDescription;
        
        [accountArray addObject:accountNumber];
    }
    
    return accountArray;
}

/**
 * Returns the key name for the confirmation response
 *
 */
- (NSString *)notificationConfirmationKey{
    return kNotificationTransferConfirmationResponseReceived;
}

/**
 * Returns the key name for the success response
 *
 */
- (NSString *)notificationSuccessKey{
    return kNotificationTransferSuccessResponseReceived;
}

#pragma mark -
#pragma mark Operation methods
/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startFORequest {
    
    BOOL result = NO;
    Account *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    NSString *originAccountNumber = originAccount.subject;
    
    self.amountString = [Tools formatAmountWithDotDecimalSeparator:self.amountString];
    
    self.currency =  [self.currencyList objectAtIndex:[self selectedCurrencyIndex]];
    
    if (([originAccountNumber length] > 0) &&
        ([self.currency length] > 0) ){
        
        [[Updater getInstance] transferWithCashMobileConfirmationFromAccountType:
         originAccount.accountType
                                                                    fromCurrency:originAccount.currency
                                                                       fromIndex:originAccount.subject
                                                                         toPhone:startUpResponseInformation_.phoneNumber
                                                                          amount:self.amountString
                                                                        currency:self.currency];
        
        result = YES;
        
    }
    
    return result;
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startFORequestDataValidation {
	
	NSString *result = nil;
    
    NSString *amountOnlyNumbers = [self.amountString stringByReplacingOccurrencesOfString:@"." withString:@""];
    amountOnlyNumbers = [amountOnlyNumbers stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    long int amount = [amountOnlyNumbers intValue];
    
	if ([self selectedOriginAccountIndex] == NSNotFound || [self selectedOriginAccountIndex] < 0 || [self selectedOriginAccountIndex] > [[self originAccountList] count]) {
		
		result = NSLocalizedString(TRANSFER_ERROR_CASHMOBILE_ORIGIN_ACCOUNT_TEXT_KEY, nil);
		
	} else {
		
		BankAccount *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
		NSString *originAccountNumber = [originAccount subject];
        
        if ([startUpResponseInformation_.phoneNumber length] == 0) {
            
            result = NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_EMPTY_KEY, nil);
            
        } else if (![Tools isValidMobilePhoneNumberString:startUpResponseInformation_.phoneNumber]) {
            
            result = NSLocalizedString(TRANSFER_ERROR_PHONE_NUMBER_TEXT_KEY, nil);
            
        } else if (!([originAccountNumber length] > 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_CASHMOBILE_ORIGIN_ACCOUNT_TEXT_KEY, nil);
        } else{
            
            BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
            NSString *originAccountNumber = originAccount.subject;
            NSString *avaibleBalance = originAccount.availableBalanceString;
            
            avaibleBalance = [avaibleBalance stringByReplacingOccurrencesOfString:@"," withString:@""];
            
            NSString *currencySelected = @"SOLES";
            
            if (!([originAccountNumber length] > 0)) {
                
                result = NSLocalizedString(TRANSFER_ERROR_CASHMOBILE_ORIGIN_ACCOUNT_TEXT_KEY, nil);
                
            } else if (!([currencySelected length] > 0)) {
                
                result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
                
            } else if (([self.amountString isEqualToString:@"0.00"]) || ([self.amountString floatValue] == 0.0f)) {
                
                result = NSLocalizedString(TRANSFER_MUST_SELECT_AMOUNT_ERROR_TEXT_KEY, nil);
                
            } else if (amount%20 != 0 ){
                
                result = NSLocalizedString(TRANSFER_MUST_BE_MULTIPLE_TWENTY_KEY, nil);
                
            }else if (amount > [avaibleBalance floatValue]){
                
                result = NSLocalizedString(TRANSFER_AMOUNT_EXCEEDED_KEY, nil);
                
            }
            
        }
		
	}
    return result;
    
}

- (BOOL)startFoConfirmationRequest{
    BOOL result = [super startFoConfirmationRequest];
    
    [[Updater getInstance] transferWithCashMobileResultFromSecondKeyFactor:@""];
    
    return result;
}

/**
 * @param FOResponse The FO response received from the server
 * @return YES when the confirmation response is correct, NO otherwise
 */
- (BOOL)fOResponseReceived:(StatusEnabledResponse *)foResponse{
    
    BOOL result = [super fOResponseReceived:foResponse];
    
    if (!result) {
        
        if (![foResponse isError] && [foResponse isKindOfClass:[TransferConfirmationResponse class]]) {
            
            TransferConfirmationResponse *response = (TransferConfirmationResponse *)foResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response.additionalInformation retain];
            
            self.seal = [additionalInformation_ seal];
            
            result = YES;
            
        }
    }
    
    return result;
}

/*
 * Notifies the fO operation helper the confirmation response received from the server. The fo operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        result = ![confirmationResponse isError];
        
        if (result) {
            if ([confirmationResponse isKindOfClass:[TransferSuccessResponse class]]) {
                
                TransferSuccessResponse *response = (TransferSuccessResponse *)confirmationResponse;
                
                if (transferSuccessAdditionalInfo_ != nil) {
                    [transferSuccessAdditionalInfo_ release];
                    transferSuccessAdditionalInfo_ = nil;
                }
                transferSuccessAdditionalInfo_ = [response.additionalInformation retain];
                
                result = YES;
                
            }
        }
        
    }
    
    return result;
    
    
}

@end
