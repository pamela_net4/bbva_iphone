//
//  FOOperationHelper.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/12/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOOperationHelper.h"
#import "StringKeys.h"
#import "Tools.h"

@implementation FOOperationHelper

#pragma mark -
#pragma mark Properties

@synthesize amountString = amountString_;
@synthesize currency = currency_;
@synthesize reference = reference_;
@synthesize hasCard = hasCard_;
@synthesize seal = seal_;
@synthesize selectedCurrencyIndex = selectedCurrencyIndex_;
@synthesize selectedOriginAccountIndex = selectedOriginAccountIndex_;
@synthesize selectedOriginCardIndex = selectedOriginCardIndex_;
@synthesize sendSMS = sendSMS_;
@synthesize destinationSMS1 = destinationSMS1_;
@synthesize destinationSMS2 = destinationSMS2_;
@synthesize showSMS1 = showSMS1_;
@synthesize showSMS2 = showSMS2_;
@synthesize sendEmail = sendEmail_;
@synthesize destinationEmail1 = destinationEmail1_;
@synthesize destinationEmail2 = destinationEmail2_;
@synthesize showEmail1 = showEmail1_;
@synthesize showEmail2 = showEmail2_;
@synthesize selectedCarrier1Index = selectedCarrier1Index_;
@synthesize selectedCarrier2Index = selectedCarrier2Index_;
@synthesize emailMessage = emailMessage_;
@synthesize optionalDisclaimer = optionalDisclaimer_;
@dynamic canSendEmailandSMS;
@dynamic foOperationType;
@dynamic currencyList;
@dynamic notificationMessage;
@dynamic confirmationMessage;
@dynamic foHeaderTitle;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [amountString_ release];
    amountString_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [reference_ release];
    reference_ = nil;
    
    [seal_ release];
    seal_ = nil;
    
    [destinationSMS1_ release];
    destinationSMS1_ = nil;
    
    [destinationSMS2_ release];
    destinationSMS2_ = nil;
    
    [destinationEmail1_ release];
    destinationEmail1_ = nil;
    
    [destinationEmail2_ release];
    destinationEmail2_ = nil;
    
    [emailMessage_ release];
    emailMessage_ = nil;
    
    [carrierList_ release];
    carrierList_ = nil;
    
    [super dealloc];
}
#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the transfer operation type. Base class returns a default value (transfer between user accounts)
 *
 * @return The transfer operation type
 */
- (FOTypeEnum)foOperationType {
    
    return FOTETransferToOtherBankAccount;
    
}

/*
 * Returns the List of currency for the operation. Base class returns an empty array
 *
 * @return The currency list
 */
- (NSArray *)currencyList {
    
    return [NSArray array];
    
}

-(NSString*)foHeaderTitle{
    return NSLocalizedString(FO_CONFIRM_HEADER_TITLE_KEY, nil);
}

#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the FO first step. Default implementation returns an empty array
 */
- (NSArray *)foFirstStepInformation {
    
    return [NSArray array];
    
}

/*
 * Creates the title and attributes array to display in the FO second step. Default implementation returns an empty array
 */
- (NSArray *)foSecondStepInformation {
    
    return [NSArray array];
    
}

/*
 * Creates the title and attributes array to display in the FO third step. Default implementation returns an empty array
 */
- (NSArray *)foThirdStepInformation {
    
    return [NSArray array];
    
}

/*
 * Creates the account array to display in the picker view. Default implementation returns an empty array
 */
- (NSArray *)accountStringList{
    return [NSArray array];
}

/**
 * Creates the card array to display in the picker view. Default implementation returns an empty array
 */
- (NSArray *)cardStringList{
    return [NSArray array];
}

/**
 * Returns the key name for the TIN confirmation response
 */
- (NSString *)notificationTINConfirmationKey
{
    return @"";
}

/**
 * Returns the key name for the confirmation response
 */
- (NSString *)notificationConfirmationKey{
    
    return @"";
}

/**
 * Returns the key name for the success response
 *
 */
- (NSString *)notificationSuccessKey{
    return @"";
}

/*
 * Returns the can show legal terms flag. Base class returns NO
 *
 * @return The can show legal terms flag
 */
- (BOOL)canShowLegalTerms {
    
    return NO;
    
}

/*
 * Returns the localized terms and conditions string. Base class returns an empry string
 *
 * @return The localized terms and conditions string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return @"";
    
}

/*
 * Returns the localized fo operation type string. Base class returns an empty string
 *
 * @return The localized fo operation type string
 */
- (NSString *)localizedFOOperationTypeString {
    
    return @"";
    
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
    return @"";
    
}

/*
 * Returns the can send email flag. Base class returns NO
 *
 * @return The can send email flag
 */
- (BOOL)canSendEmailandSMS {
    
    return NO;
    
}

#pragma mark -
#pragma mark Server operations management

/*
 * Performs the fO confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (BOOL)startFoConfirmationRequest {
    
    return YES;
    
}

/*
 * Notifies the fO operation helper the confirmation response received from the server. The fo operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    return NO;
    
}

/*
 * Performs the transfer request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startFORequest {
    
    return NO;
    
}
-(NSString*)itfMessage{
    return @"";
}

-(NSString*)confirmationMessage{
    return @"";
}

-(NSString*)notificationMessage{
    return  @"";
}
- (NSString *)itfMessageConfirm{
    
    return @"";
    
}
/*
 * Returns the error message for the invalid data. If @"" the data is correct
 *
 * @return if the data is valid returns nil
 */
- (NSString *)startFORequestDataValidation {

    NSString *result = nil;

    
    if (self.sendSMS) {
        
        if (self.showSMS1) {
            
            if ([@"" isEqualToString:[self destinationSMS1]]) {
                
                result = NSLocalizedString(FO_ERROR_FILL_PHONE_TEXT_KEY, nil);
                
            } else if (![Tools isValidMobilePhoneNumberString:[self destinationSMS1]]) {
                
                result = NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_ERROR_KEY, nil);
                
            } else if (self.selectedCarrier1Index < 0) {
                
                result = NSLocalizedString(TRANSFER_ERROR_CARRIER_TEXT_KEY, nil);
                
            }
        }
        
        if (([result length] == 0) && (self.showSMS2)) {
            
            if ([@"" isEqualToString:[self destinationSMS2]]) {
                
                result = NSLocalizedString(FO_ERROR_FILL_PHONE_TEXT_KEY, nil);
                
            } else if (![Tools isValidMobilePhoneNumberString:[self destinationSMS2]]) {
                
                result = NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_ERROR_KEY, nil);
                
            } else if (self.selectedCarrier2Index < 0) {
                
                result = NSLocalizedString(TRANSFER_ERROR_CARRIER_TEXT_KEY, nil);
                
            }
            
        }
        
    }
    
    if (([result length] == 0) && (self.sendEmail)) {
        
        if (self.showEmail1) {
            
            if ([self.destinationEmail1 length] <= 0)  {
                
                result = NSLocalizedString(TRANSFER_ERROR_EMAIL_TEXT_KEY, nil);
                
            }  else {
                
                NSArray *atSeparetedStrings = [self.destinationEmail1 componentsSeparatedByString:@"@"];
                NSUInteger atSeparetedStringsCount = [atSeparetedStrings count];
                
                if (atSeparetedStringsCount == 2) {
                    
                    NSString *firstString = [atSeparetedStrings objectAtIndex:0];
                    
                    if ([firstString length] > 0) {
                        
                        NSString *otherString = [atSeparetedStrings objectAtIndex:1];
                        NSRange dotRange = [otherString rangeOfString:@"."];
                        NSUInteger dotLocation = dotRange.location;
                        
                        if (!(((dotLocation > 0) && (dotLocation != NSNotFound)) && (dotLocation < ([otherString length] - 1))) ) {
                            
                            result = NSLocalizedString(FO_ERROR_VALID_EMAIL_TEXT_KEY, nil);
                            
                        }
                        
                    } else {
                        
                        result = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                        
                    }
                    
                } else {
                    
                    result = NSLocalizedString(FO_ERROR_VALID_EMAIL_TEXT_KEY, nil);
                    
                }
                
            }
            
        }
        
        if (([result length] == 0) && (self.showEmail2)) {
            
            if ([self.destinationEmail2 length] <= 0) {
                
                result = NSLocalizedString(TRANSFER_ERROR_EMAIL_TEXT_KEY, nil);
                
            } else {
                
                NSArray *atSeparetedStrings = [self.destinationEmail2 componentsSeparatedByString:@"@"];
                NSUInteger atSeparetedStringsCount = [atSeparetedStrings count];
                
                if (atSeparetedStringsCount == 2) {
                    
                    NSString *firstString = [atSeparetedStrings objectAtIndex:0];
                    
                    if ([firstString length] > 0) {
                        
                        NSString *otherString = [atSeparetedStrings objectAtIndex:1];
                        NSRange dotRange = [otherString rangeOfString:@"."];
                        NSUInteger dotLocation = dotRange.location;
                        
                        if (!(((dotLocation > 0) && (dotLocation != NSNotFound)) && (dotLocation < ([otherString length] - 1))) ) {
                            
                            result = NSLocalizedString(FO_ERROR_VALID_EMAIL_TEXT_KEY, nil);
                            
                        }
                        
                    } else {
                        
                        result = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                        
                    }
                    
                } else {
                    
                    result = NSLocalizedString(FO_ERROR_VALID_EMAIL_TEXT_KEY, nil);
                    
                }
                
            }
            
        }
        
        if (([result length] == 0) && ([[self emailMessage] length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_EMAIL_MESSAGE_TEXT_KEY, nil);
            
        }
        
    }
    
    return result;
    
    return result;
}

/*
 * Notifies the transfer operation helper the tranfer response received from the server. The transfer operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)fOResponseReceived:(StatusEnabledResponse *)foResponse {
    
    return NO;
    
}

@end
