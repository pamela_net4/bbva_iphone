//
//  FORechargeGiftCard.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/20/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOOperationHelper.h"

@class FOERechargeGiftCardStepOneResponse;
@class GiftCardStepTwoResponse;
@class GiftCardStepThreeResponse;
@interface FORechargeGiftCard : FOOperationHelper{
    
@private
    
    /**
     * Array with the origin selectable accounts. All objects are PaymentElement instances
     */
    NSMutableArray *originAccountList_;
    
    /**
     * Array with the origin selectable cards. All objects are PaymentElement instances
     */
    NSMutableArray *originCardList_;
    
    /**
     * Selected destination bank code
     */
    NSString *firstText_;
    
    /**
     * Selected destination account office
     */
    NSString *secondText_;
    
    /**
     * Selected destination account number
     */
    NSString *thirdText_;
    
    /**
     * Selected destination account control number
     */
    NSString *fourthText_;
    
    /**
     * Flag to indicate if is owner of the account
     */
    BOOL isItf_;
    
    /**
     * Gift Card Initial information
     */
    FOERechargeGiftCardStepOneResponse *startUpResponseInformation_;
    
    /**
     * Gift Card Confirmation Additional Information
     */
    GiftCardStepTwoResponse *additionalInformation_;
    
    /**
     * Gift Card Success Additional Information
     */
    GiftCardStepThreeResponse *transferSuccessAdditionalInfo_;
    
}

/**
 * Provides read-only access to the array with the origin selectable accounts
 */
@property (nonatomic, readonly, retain) NSArray *originAccountList;
/**
 * Provides read-only access to the array with the origin selectable cards
 */
@property (nonatomic, readonly, retain) NSArray *originCardList;
/**
 * Provides read-only access to the firstText
 */
@property (nonatomic, readwrite, retain) NSString *firstText;
/**
 * Provides read-only access to the secondText
 */
@property (nonatomic, readwrite, retain) NSString *secondText;
/**
 * Provides read-only access to the thirdText
 */
@property (nonatomic, readwrite, retain) NSString *thirdText;
/**
 * Provides read-only access to the fourthText
 */
@property (nonatomic, readwrite, retain) NSString *fourthText;
/**
 * Designated initialized. Initializes a FOERechargeGiftCardStepOneResponse instance with the initial transfer accounts response
 *
 * @param fOERechargeGiftCardStepOneResponse The Recharge Gift Card response
 * @return The initialized fOERechargeGiftCardStepOneResponse instance;
 */
- (id)initWithRechargeStartupResponse:(FOERechargeGiftCardStepOneResponse *)fOERechargeGiftCardStepOneResponse;

@end

