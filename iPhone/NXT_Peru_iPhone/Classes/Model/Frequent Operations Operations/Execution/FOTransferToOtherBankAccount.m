//
//  FOTransferToOtherBankAccount.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/17/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//
#import "FOTransferToOtherBankAccount.h"

#import "FOEOtherBankTransferStepOneResponse.h"
#import "FOAccountList.h"
#import "BankAccount.h"
#import "AccountList.h"
#import "FOCardList.h"
#import "FOCard.h"
#import "GlobalAdditionalInformation.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "TransferConfirmationResponse.h"
#import "TransferConfirmationAdditionalInformation.h"
#import "TransferSuccessAdditionalInformation.h"
#import "TransferSuccessResponse.h"
#import "TransferTypePaymentResponse.h"
#import "Tools.h"
#import "Updater.h"

@implementation FOTransferToOtherBankAccount
#pragma mark -
#pragma mark Properties

@synthesize originAccountList = originAccountList_;
@synthesize commissionN=  commissionN_;
@synthesize commissionL =commissionL_;
@synthesize totalPaymentN =totalPaymentN_;
@synthesize totalPaymentL = totalPaymentL_;
@synthesize typeFlow = typeFlow_;
@synthesize documentTypeList =documentTypeList_;
@synthesize selectedTransferTypeIndex = selectedTransferTypeIndex_;
@synthesize selectedDocumentTypeIndex = selectedDocumentTypeIndex_;
@synthesize documentNumber = documentNumber_;
@synthesize beneficiary = beneficiary_;
@synthesize isItf = isItf_;
@synthesize beneficiaryDocumentTypeId = beneficiaryDocumentTypeId;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [originAccountList_ release];
    originAccountList_ = nil;
    
    [selectedDestinationBank_ release];
    selectedDestinationBank_ = nil;
    
    [selectedDestinationAccountControl_ release];
    selectedDestinationAccountControl_ = nil;
    
    [selectedDestinationAccountNumber_ release];
    selectedDestinationAccountNumber_ = nil;
    
    [selectedDestinationAccountOffice_ release];
    selectedDestinationAccountOffice_ = nil;
    
    [super dealloc];
    
}
- (id)initWithTransferStartupResponse:(FOEOtherBankTransferStepOneResponse *)fOEToOtherBankAccountTransferStepOneResponse{
    
    if ((self = [super init])) {
        
        // The initial operation in iPad returns elements no needed in iPhone
        Session *session = [Session getInstance];
        originAccountList_ = [[NSMutableArray alloc] initWithArray:session.accountList.transferAccountList];
        
        [self setSelectedOriginAccountIndex:-1];
        
        if (startUpResponseInformation_ != nil) {
            [startUpResponseInformation_ release];
            startUpResponseInformation_ = nil;
        }
        
        [self setSelectedCurrencyIndex:0];
        
        startUpResponseInformation_ = fOEToOtherBankAccountTransferStepOneResponse;
        [startUpResponseInformation_ retain];
        
        NSArray *destinationAccount = [fOEToOtherBankAccountTransferStepOneResponse.paymentAccount componentsSeparatedByString:@"-"];
        
        if ([destinationAccount count] > 0) {
            selectedDestinationBank_ = [[destinationAccount objectAtIndex:0] retain];
        }
        if ([destinationAccount count] > 1) {
            selectedDestinationAccountOffice_ = [[destinationAccount objectAtIndex:1] retain];
        }
        if ([destinationAccount count] > 2) {
            selectedDestinationAccountNumber_ = [[destinationAccount objectAtIndex:2] retain];
        }
        if ([destinationAccount count] > 3) {
            selectedDestinationAccountControl_ = [[destinationAccount objectAtIndex:3] retain];
        }
        
        NSString *strCTABANCODESTINO ;
        
        if ([@"si" isEqualToString:[startUpResponseInformation_.destinationBankAccount lowercaseString]]) {
            isItf_ = TRUE;
            strCTABANCODESTINO = @"SI";
        }else{
            isItf_ = FALSE;
            strCTABANCODESTINO = @"NO";
        }
        
        //cambio 1 bruno
        //guardamos el valor del key CTABANCODESTINO  del xml para hacer switch en la pantalla De operacion frecuente segunda etapa
        [[NSUserDefaults standardUserDefaults] setObject:strCTABANCODESTINO forKey:@"strCTABANCODESTINO"];
        
        
        self.carrierList = [[NSArray alloc] initWithArray:[[fOEToOtherBankAccountTransferStepOneResponse foCarrierList] alterCarrierList]];
        self.beneficiary = startUpResponseInformation_.beneficiaryName;
        self.documentNumber = startUpResponseInformation_.docNumber;
        [[NSUserDefaults standardUserDefaults] setObject:startUpResponseInformation_.docNumber forKey:@"docNumber"];
        self.beneficiaryDocumentTypeId = startUpResponseInformation_.docType;

    }
    
    return self;
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the frequent operation type. Base class returns a default value (transfer between user accounts)
 *
 * @return The frequent operation type
 */
- (FOTypeEnum)foOperationType {
    
    return FOTETransferToOtherBankAccount;
    
}

/*
 * Returns the List of currency for the operation. Base class returns an empty array
 *
 * @return The currency list
 */
- (NSArray *)currencyList {
    
    return [NSArray arrayWithObjects:NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil),
            NSLocalizedString(DOLLARS_CURRENCY_TEXT_KEY, nil),
            nil];
    
}

/*
 * Returns the can show legal terms flag.
 *
 */
- (BOOL)canShowLegalTerms {
    
    return TRUE;
    
}

/*
 * Returns the can show email and sms
 */
- (BOOL)canSendEmailandSMS {
    
    return YES;
    
}

/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedFOOperationTypeString {
    
    return NSLocalizedString(TRANSFER_TO_ANOTHER_BANK_TEXT_1_KEY, nil);
    
}

/*
 * Returns the localized terms and conditions string. The transfer between accounts legal terms localized string is returned
 *
 * @return The localized transfer legal terms operation type string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return NSLocalizedString(TRANSFER_LEGAL_TERMS_KEY, nil);
    
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
#if defined(SIMULATE_HTTP_CONNECTION)
    return [Tools notNilString:@"http://www.google.es"];
#else
    return [Tools notNilString:additionalInformation_.disclaimer];
#endif
    
}

#pragma mark -
#pragma mark Information distribution
-(NSString*)foHeaderTitle{
    return @"Confirmar transferencia a otros bancos";
}
/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foFirstStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Service
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SERVICE_TITLE_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.service]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Frequen operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_POSITION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.nick]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //payment account
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_ACCOUNT_DESTINATION_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.paymentAccount]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //ITF account
    if([startUpResponseInformation_.destinationBankAccount length]>0){
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OWN_ACCOUNT_LABEL_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.destinationBankAccount]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    }
    
    //Holder Payment account
    if([startUpResponseInformation_.beneficiaryName length]>0){
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_ACCOUNT_OWNER_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.beneficiaryName]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    }
    
    //Document type
    if([startUpResponseInformation_.docTypeDescription length]>0){
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSACTION_DOCUMENT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.docTypeDescription]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    }
    //Beneficiary document number
    if([startUpResponseInformation_.docNumber length]>0){
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TITLE_DOCUMENT_BENEFICIARY_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.docNumber]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    }
    
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foSecondStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    BankAccount *selectedOriginAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
  
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    //Amount to pay
    NSString *amountToTrans = additionalInformation_.amountToTransfer;
    NSString *amountCurrencyToTrans = additionalInformation_.amountCurrencyToTransfer;
    
    if ([amountToTrans length] == 0) {
        
        amountToTrans = @"0.00";
        
    }
    
  
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_AMOUNT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@",
                                      [Tools getCurrencySimbol:amountCurrencyToTrans],
                                      amountToTrans]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
  
    
    // Origin
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORIGIN_TITLE_TEXT_KEY, nil);
    
    NSString *originAccount = [NSString stringWithFormat:@"%@ | %@", selectedOriginAccount.accountType, [Tools getCurrencyLiteral:selectedOriginAccount.currency]];

    
    [titleAndAttributes addAttribute:originAccount];
    [titleAndAttributes addAttribute:selectedOriginAccount.number];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    
    //Destination bank
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSACTION_DEST_BANK_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.destinationBank]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Payment account
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.beneficiaryAccount]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Operation
    
    
   /* titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.operation]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }*/
    
    //Beneficiary
    if([additionalInformation_.beneficiary length] > 0) {
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(BENEFICIARY_TEXT_KEY, nil);
        
        [titleAndAttributes addAttribute:additionalInformation_.beneficiary];
        
        if (titleAndAttributes != nil) {
            
            [mutableResult addObject:titleAndAttributes];
            
        }
    }
    
    //Beneficiary document
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TITLE_DOCUMENT_BENEFICIARY_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute:additionalInformation_.beneficiaryDocument];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    
    //Tax
    NSString *commission = additionalInformation_.commission;
    NSString *currencyCommission = additionalInformation_.currencyCommission;
    NSString *currencySymbol = @"";
    
    if (([commission length] == 0) || ([commission isEqualToString:@"0.00"])) {
        
        commission = @"0.00";
        
    } else {
        
        currencySymbol = [NSString stringWithFormat:@"%@ ", [Tools notNilString:[Tools getCurrencySimbol:currencyCommission]]];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(COMISSION_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@%@",
                                      currencySymbol,
                                      commission]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    //Network use
    NSString *networkUse = additionalInformation_.networkUse;
    NSString *currencyNetworkUse = additionalInformation_.currencyNetworkUse;
    
    if ([networkUse length] == 0) {
        
        networkUse = @"0.00";
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(USE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@",
                                      [Tools getCurrencySimbol:currencyNetworkUse],
                                      networkUse]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Amount to pay
    NSString *amountToPay = additionalInformation_.amountToPay;
    NSString *amountCurrencyToPay = additionalInformation_.amountCurrencyToPay;
    
    if ([amountToPay length] == 0) {
        
        amountToPay = @"0.00";
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FINAL_AMOUNT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@",
                                      [Tools getCurrencySimbol:amountCurrencyToPay],
                                      amountToPay]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    // Exchange rate
    if (![Tools currency:selectedOriginAccount.currency isEqualToCurrency:self.currency]) {
        
        NSString *exchangeRateString = additionalInformation_.tipoCambio;
        
        if ([exchangeRateString length] > 0) {
            
            NSDecimalNumber *exchangeRate = [Tools decimalFromServerString:exchangeRateString];
            NSString *exchangeRateCurrency = [Tools getCurrencySimbol:additionalInformation_.monedaTipoCambio];
            
            if ([exchangeRateCurrency length] == 0) {
                
                exchangeRateCurrency = [Tools mainCurrencySymbol];
                
            }
            
            NSString *exchangeRateToDisplay = [Tools formatAmount:exchangeRate
                                                     withCurrency:exchangeRateCurrency
                                          currenctyPrecedesAmount:YES
                                                     decimalCount:4];
            
            if([exchangeRate floatValue]>0.0f){
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
                if (titleAndAttributes != nil) {
                
                titleAndAttributes.titleString = NSLocalizedString(TRANSFER_EXCHANGE_RATE_TEXT_KEY, nil);
                [titleAndAttributes addAttribute:exchangeRateToDisplay];
                [mutableResult addObject:titleAndAttributes];
                
                }
            }
            
        }
        
    }
    
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foThirdStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    BankAccount *selectedOriginAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    
   
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    // Operation state
    if([transferSuccessAdditionalInfo_.operationState length]>0){
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(@"Estado de la operación", nil);
        [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operationState];
    
        if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
        }
    }
    
    // Operation number
    if([transferSuccessAdditionalInfo_.operationNumber length]>0){
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(NUMBER_OPERATION_TITLE_TEXT_KEY, nil);
        [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operationNumber];
    
        if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
        }
    }
    
     // Operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:transferSuccessAdditionalInfo_.operation];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    
    // Origin
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORIGIN_TITLE_TEXT_KEY, nil);
    
    NSString *originAccount = [NSString stringWithFormat:@"%@ | %@", selectedOriginAccount.accountType, [Tools getCurrencyLiteral:selectedOriginAccount.currency]];
    
    
    [titleAndAttributes addAttribute:originAccount];
    [titleAndAttributes addAttribute:selectedOriginAccount.number];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Owner
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORINGIN_ACCOUNT_OWNER_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Session getInstance].additionalInformation.customer];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Destination bank
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSACTION_DEST_BANK_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:additionalInformation_.destinationBank];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Destination
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_TITLE_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute:additionalInformation_.beneficiaryAccount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    // bebeficiary
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(BENEFICIARY_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:additionalInformation_.beneficiary];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // bebeficiary document
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TITLE_DOCUMENT_BENEFICIARY_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@",transferSuccessAdditionalInfo_.beneficiaryDocumentType, transferSuccessAdditionalInfo_.beneficiaryDocument]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_TEXT_KEY, nil);
    
    NSString *currency = @"";
    
    if (([self selectedCurrencyIndex] >= 0) && ([self selectedCurrencyIndex] < [[self currencyList] count])) {
        
        currency = [[self currencyList] objectAtIndex:[self selectedCurrencyIndex]];
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:[currency uppercaseString]], self.amountString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Tax
    NSString *commission = transferSuccessAdditionalInfo_.commission;
    NSString *currencySymbol = @"";
    
    if (([commission length] == 0) || ([commission isEqualToString:@"0.00"])) {
        
        commission = @"0.00";
        
    } else {
        
        currencySymbol = [NSString stringWithFormat:@"%@ ", [Tools notNilString:[Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currencyCommission]]];
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(COMISSION_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@%@",
                                      currencySymbol,
                                      commission]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Network use
    
    NSString *networkUse = transferSuccessAdditionalInfo_.networkUse;
    
    if ([networkUse length] == 0) {
        
        networkUse = @"0.00";
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(USE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@",
                                      [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currencyNetworkUse],
                                      networkUse]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    // Amount to paid
    
    NSString *amountToPay = transferSuccessAdditionalInfo_.amountToPay;
    
    if ([amountToPay length] == 0) {
        
        amountToPay = @"0.00";
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FINAL_AMOUNT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@",
                                      [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currencyAmountToPay],
                                      amountToPay]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Total amount
    
    NSString *totalAmountToPay = transferSuccessAdditionalInfo_.totalAmountToPay;
    
    if ([totalAmountToPay length] == 0) {
        
        totalAmountToPay = @"0.00";
        
    }
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TOTAL_AMOUNT_CHARGED_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@",
                                      [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currencyTotalAmountToPay],
                                      totalAmountToPay]];
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Exchange rate
    
    if (![Tools currency:selectedOriginAccount.currency isEqualToCurrency:self.currency]) {
        
        NSString *exchangeRateString = transferSuccessAdditionalInfo_.exchangeRate;
        
        if ([exchangeRateString length] > 0) {
            
            NSDecimalNumber *exchangeRate = [Tools decimalFromServerString:exchangeRateString];
            NSString *exchangeRateCurrency = [Tools getCurrencySimbol:transferSuccessAdditionalInfo_.currencyExchangeRate];
            
            if ([exchangeRateCurrency length] == 0) {
                
                exchangeRateCurrency = [Tools mainCurrencySymbol];
                
            }
            
            NSString *exchangeRateToDisplay = [Tools formatAmount:exchangeRate
                                                     withCurrency:exchangeRateCurrency
                                          currenctyPrecedesAmount:YES
                                                     decimalCount:4];
            
            if([exchangeRate floatValue]>0.0f){
                titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
                if (titleAndAttributes != nil) {
                
                titleAndAttributes.titleString = NSLocalizedString(TRANSFER_EXCHANGE_RATE_TEXT_KEY, nil);
                [titleAndAttributes addAttribute:exchangeRateToDisplay];
                [mutableResult addObject:titleAndAttributes];
                
                }
            }
        }
        
    }
    
    // Time
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_DATE_HOUR_SHORT_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:transferSuccessAdditionalInfo_.dateString], [Tools notNilString:transferSuccessAdditionalInfo_.hour]]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
}

-(NSString*)itfMessage{
    return transferSuccessAdditionalInfo_.itfMessage;
}

-(NSString*)confirmationMessage{
    return transferSuccessAdditionalInfo_.confirmationMessage;
}

-(NSString*)notificationMessage{
    return transferSuccessAdditionalInfo_.notificationMessage;
}
/*
 * Returns the confirmation message. The transfer between accounts localized string is returned
 *
 * @return The itf message
 */
- (NSString *)itfMessageConfirm{
    
    return additionalInformation_.itfMessage;
    
}

/*
 * Creates the account array to display in the picker view. Default implementation returns an empty array
 */
- (NSArray *)accountStringList{
    NSMutableArray *accountArray = [[[NSMutableArray alloc] init] autorelease];
    
    for (BankAccount *account in originAccountList_) {
        NSString *accountNumber = account.bankAccountListName;
        
        [accountArray addObject:accountNumber];
    }
    
    return accountArray;
}


/**
 * Returns the key name for the TIN confirmation response
 */
- (NSString *)notificationTINConfirmationKey{
     return kNotificationTransferConfirmationResponseReceived;
}

/**
 * Returns the key name for the confirmation response
 *
 */
- (NSString *)notificationConfirmationKey{
    return kNotificationTransferTypeCheckResponseReceived;
}

/**
 * Returns the key name for the success response
 *
 */
- (NSString *)notificationSuccessKey{
    return kNotificationTransferSuccessResponseReceived;
}

#pragma mark -
#pragma mark Operation methods
/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startFORequest {
    
    BOOL result = NO;
    BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    NSString *originAccountNumber = originAccount.number;
    
    self.amountString = [Tools formatAmountWithDotDecimalSeparator:self.amountString];
    
    self.currency =  [Tools getCurrencyServer:[self.currencyList objectAtIndex:[self selectedCurrencyIndex]]];
    
    if (([originAccountNumber length] > 0) &&
        ([selectedDestinationAccountOffice_ length] > 0) &&
        ([selectedDestinationAccountNumber_ length] > 0) &&
        ([selectedDestinationBank_ length] > 0) &&
        ([selectedDestinationAccountControl_ length] > 0) &&
        ([self.currency length] > 0) ){
        
        NSString *carrier1 = @"";
        NSString *carrier2 = @"";
        
        if (self.selectedCarrier1Index > 0 && self.selectedCarrier1Index <  [self.carrierList count]) {
            AlterCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier1Index];
            
            carrier1 = carrier.code;
        }
        
        
        if (self.selectedCarrier2Index > 0 && self.selectedCarrier2Index <  [self.carrierList count]) {
            AlterCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier2Index];
            
            carrier2 = carrier.code;
        }
        
        [[Updater getInstance] transferToAccountsFromOtherBanksConfirmationFromAccountType:originAccount.type
                                                                              fromCurrency:originAccount.currency
                                                                                 fromIndex:originAccount.subject
                                                                                    toBank:selectedDestinationBank_
                                                                                  toBranch:selectedDestinationAccountOffice_
                                                                                 toAccount:selectedDestinationAccountNumber_
                                                                                      toCc:selectedDestinationAccountControl_
                                                                                    amount:self.amountString
                                                                                  currency:self.currency reference:[Tools notNilString:[self reference]] email1:[self destinationEmail1] email2:[self destinationEmail2] phone1:[self destinationSMS1] carrier1:carrier1 phone2:[self destinationSMS2] carrier2:carrier2 message:[self emailMessage]];
         
        
        
        result = YES;
        
    }
    
    return result;
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startFORequestDataValidation {
	
	NSString *result = nil;
    
    if (self.selectedOriginAccountIndex < 0) {
        
        result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
        
    } else if (self.selectedCurrencyIndex < 0) {
        
        result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
        
    } else {
        
        BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
        NSString *originAccountNumber = originAccount.number;
        
        NSString *currencySelected = @"";
        
        if (([self selectedCurrencyIndex] >= 0) && ([self selectedCurrencyIndex] < [[self currencyList] count])) {
            
            currencySelected = [[self currencyList] objectAtIndex:[self selectedCurrencyIndex]];
            
        }
        
        if (!([originAccountNumber length] > 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
        }
        else if (([selectedDestinationBank_ length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_ENTITY_TEXT_KEY, nil);
            
        } else if (([selectedDestinationAccountOffice_ length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_OFFICE_TEXT_KEY, nil);
            
        } else if (([selectedDestinationAccountNumber_ length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_ACCNUMBER_TEXT_KEY, nil);
            
        } else if (([selectedDestinationAccountControl_ length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_DESTINATION_ACCOUNT_CC_TEXT_KEY, nil);
            
        } else if (!([currencySelected length] > 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
            
        } else if (startUpResponseInformation_.beneficiaryName == nil || [startUpResponseInformation_.beneficiaryName isEqualToString:@""]) {
            
            result = NSLocalizedString(TRANSFER_TO_ANOTHER_BANK_MUST_WRITE_A_BENEFICIARY_TEXT_KEY, nil);
            
        } else if (([self.amountString isEqualToString:@"0.00"]) || ([self.amountString floatValue] == 0.0f)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_AMOUNT_TEXT_KEY, nil);
            
        }
        
    }
    
    if (result == nil) {
        result = [super startFORequestDataValidation];
    }
    
    return result;
    
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startSecondTransferRequestDataValidation{
    
    NSString *result = nil;
    
    
    if(([self.typeFlow isEqualToString:@"A"] && selectedTransferTypeIndex_ == 1) ||
       [self.typeFlow isEqualToString:@"N"]){
        
        Document *doctype = [documentTypeList_ objectAtIndex:selectedDocumentTypeIndex_];
        
        if(![self isValidDocumentLength:documentNumber_.length forDocumentCode:doctype.code]) {
            
            result = NSLocalizedString(TRANSFER_TO_ANOTHER_BANK_MUST_WRITE_A_DOCUMENT_NUMBER_TEXT_KEY, nil);
            
        }else if (beneficiary_ == nil || [beneficiary_ isEqualToString:@""]) {
            
            result = NSLocalizedString(TRANSFER_TO_ANOTHER_BANK_MUST_WRITE_A_BENEFICIARY_TEXT_KEY, nil);
            
        }
        
    }else if(selectedTransferTypeIndex_<0){
        result = NSLocalizedString(@"Seleccione el tipo de transferencia que desea realizar",nil);
    }
    
    return result;
}


- (BOOL)startTypeTransferInmediateRequest{
    
    BOOL result = NO;
    
    [[Updater getInstance]  transferToAccountsFromOtherBanksWithInmediateConfirmation];
    result = YES;
    
    return result;
}

-(BOOL)startTypeTransferToConfirmRequest{
    
    if(([self.typeFlow isEqualToString:@"A"] && selectedTransferTypeIndex_ == 0) ||
       [self.typeFlow isEqualToString:@"L"]){
        
        [self startTypeTransferInmediateRequest];
       
    }
    else {
        
        
        Document *document = [documentTypeList_ objectAtIndex:selectedDocumentTypeIndex_];
        
        [[Updater getInstance] transferToAccountsFromOtherBanksWithByScheduleConfirmationWithItf:self.isItf beneficiary:[Tools notNilString:beneficiary_] documentType:document.code andDocumentNumber:documentNumber_];
    }
    return YES;
}

- (BOOL)startFoConfirmationRequest{
    BOOL result = [super startFoConfirmationRequest];
    
    [[Updater getInstance] transferToAccountsFromOtherBanksResultFromSecondFactorKey:@""];
    
    return result;
}

/**
 * @param FOResponse The FO response received from the server
 * @return YES when the confirmation response is correct, NO otherwise
 */
- (BOOL)fOResponseReceived:(StatusEnabledResponse *)foResponse{
    
    BOOL result = [super fOResponseReceived:foResponse];
    
    if (!result) {
        
        if (![foResponse isError] && [foResponse isKindOfClass:[TransferConfirmationResponse class]]) {
            
            TransferConfirmationResponse *response = (TransferConfirmationResponse *)foResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response.additionalInformation retain];
            
            self.seal = [additionalInformation_ seal];
            
            result = YES;
            
        }else if(![foResponse isError] && [foResponse isKindOfClass:[TransferTypePaymentResponse class]]){
            
            TransferTypePaymentResponse *response = (TransferTypePaymentResponse*)foResponse;
            
            self.commissionL = response.commissionL ;
            self.commissionN = response.commissionN;
            self.totalPaymentL = response.totalPayL;
            self.totalPaymentN = response.totalPayN;
            self.typeFlow = response.typeFlow;
            self.optionalDisclaimer = response.disclaimer;
            documentTypeList_= [[NSMutableArray alloc] initWithArray:response.documents];
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response.additionalInformation retain];
            result = YES;
            
        }
    }
    
    return result;
}

/*
 * Notifies the fO operation helper the confirmation response received from the server. The fo operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        
        if (![confirmationResponse isError] && [confirmationResponse isKindOfClass:[TransferSuccessResponse class]]) {
            
            TransferSuccessResponse *response = (TransferSuccessResponse *)confirmationResponse;
            
            if (transferSuccessAdditionalInfo_ != nil) {
                [transferSuccessAdditionalInfo_ release];
                transferSuccessAdditionalInfo_ = nil;
            }
            transferSuccessAdditionalInfo_ = [response.additionalInformation retain];
            
            result = YES;
            
        }
        
    }
    
    return result;
    
    
}

/*
 * validate length document of beneficiary
 */
-(BOOL)isValidDocumentLength:(NSInteger)length forDocumentCode:(NSString *)code {
    
    BOOL result = YES;
    
    if ([code isEqualToString:DOC_TYPE_DNI]) {
        
        result = (length == 8);
        
    } else if ([code isEqualToString:DOC_TYPE_RUC]) {
        
        result = (length == 11);
        
    } else if ([code isEqualToString:DOC_TYPE_MILITAR_ID]) {
        
        result = (length == 8);
        
    } else if ([code isEqualToString:DOC_TYPE_PASAPORT]) {
        
        result = (length > 0) && (length <= 11);
        
    } else if ([code isEqualToString:DOC_TYPE_INMIGRATION_ID]) {
        
        result = (length > 0) && (length <= 12);
        
    }
    
    return result;
    
}

@end
