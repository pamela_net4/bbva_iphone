//
//  FOPaymentCOtherBank.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/21/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOPaymentCOtherBank.h"

#import "FOEPaymentCOtherBankStepOneResponse.h"
#import "FOAccountList.h"
#import "BankAccount.h"
#import "AccountList.h"
#import "AlterCarrierList.h"
#import "FOCardList.h"
#import "FOCard.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "PaymentConfirmationResponse.h"
#import "PaymentTINOnlineResponse.h"
#import "PaymentSuccessResponse.h"
#import "Tools.h"
#import "Updater.h"


@implementation FOPaymentCOtherBank
#pragma mark -
#pragma mark Properties

@synthesize originAccountList = originAccountList_;
@synthesize commissionN=  commissionN_;
@synthesize commissionL =commissionL_;
@synthesize totalPaymentN =totalPaymentN_;
@synthesize totalPaymentL = totalPaymentL_;
@synthesize typeFlow = typeFlow_;
@synthesize beneficiary = beneficiary_;
@synthesize selectedTransferTypeIndex = selectedTransferTypeIndex_;
#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [originAccountList_ release];
    originAccountList_ = nil;
    
    [selectedDestinationAccountOffice_ release];
    selectedDestinationAccountOffice_ = nil;
    
    [super dealloc];
    
}
- (id)initWithPaymentStartupResponse:(FOEPaymentCOtherBankStepOneResponse *)FOEPaymentCOtherBankStepOneResponse{
    
    if ((self = [super init])) {
        
        // The initial operation in iPad returns elements no needed in iPhone
        Session *session = [Session getInstance];
        originAccountList_ = [[NSMutableArray alloc] initWithArray:session.accountList.transferAccountList];
        
        [self setSelectedOriginAccountIndex:-1];
        
        if (startUpResponseInformation_ != nil) {
            [startUpResponseInformation_ release];
            startUpResponseInformation_ = nil;
        }
        
        [self setSelectedCurrencyIndex:0];
        
        startUpResponseInformation_ = FOEPaymentCOtherBankStepOneResponse;
        [startUpResponseInformation_ retain];
        
        self.carrierList = [[NSArray alloc] initWithArray:[[FOEPaymentCOtherBankStepOneResponse foCarrierList] alterCarrierList]];
        
        self.beneficiary = startUpResponseInformation_.beneficiaryName ;
    }
    
    return self;
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the frequent operation type. Base class returns a default value (transfer between user accounts)
 *
 * @return The frequent operation type
 */
- (FOTypeEnum)foOperationType {
    
    return FOTEPaymentOtherBankCard;
    
}

/*
 * Returns the List of currency for the operation. Base class returns an empty array
 *
 * @return The currency list
 */
- (NSArray *)currencyList {
    
    return [NSArray arrayWithObjects:NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil),
            NSLocalizedString(DOLLARS_CURRENCY_TEXT_KEY, nil),
            nil];
    
}

/*
 * Returns the can show legal terms flag.
 *
 */
- (BOOL)canShowLegalTerms {
    
    return TRUE;
    
}

/*
 * Returns the can show email and sms
 */
- (BOOL)canSendEmailandSMS {
    
    return YES;
    
}

/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedFOOperationTypeString {
    
    return NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TITLE_TEXT_KEY, nil);
    
}

/*
 * Returns the localized terms and conditions string. The transfer between accounts legal terms localized string is returned
 *
 * @return The localized transfer legal terms operation type string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return NSLocalizedString(TRANSFER_LEGAL_TERMS_KEY, nil);
    
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
#if defined(SIMULATE_HTTP_CONNECTION)
    return [Tools notNilString:@"http://www.google.es"];
#else
    return [Tools notNilString:additionalInformation_.disclaimer];
#endif
    
}

#pragma mark -
#pragma mark Information distribution
-(NSString*)foHeaderTitle{
    return @"Confirmar pago de tarjeta de otros bancos";
}
/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foFirstStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Service
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SERVICE_TITLE_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.service]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Frequen operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_POSITION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.nick]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Third card number
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.cardNumber]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Card type
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_PAYMENT_THIRD_CARD_TYPE_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.cardType]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Destination bank
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_BANK_LOWER_CASE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.destinationBank]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Card's emission place
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_CARD_PAYMENT_OTHER_BANK_SITE_OF_EMISION_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.emissionPlace]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Beneficiary holder name
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_BENEFICIARY_NAME_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.beneficiaryName]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foSecondStepInformation{
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    // - Amount to pay
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_AMOUNT_TO_PAY_TEXT_KEY, nil)];
    
    
    NSString *amount = [NSString stringWithFormat:@"%@ %@",
                        [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:[Tools notNilString:[additionalInformation_ amountChargedCurrency]]],
                        [Tools notNilString:[additionalInformation_ amountCharged]]];
    
    [titleAndAttributes addAttribute:[Tools notNilString:amount]];
    
    if (titleAndAttributes != nil) {
        [mutableResult addObject:titleAndAttributes];
    }

    
    
    //Operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(OPERATION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.operation]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    //Charged Account
    
    BankAccount *chargeAccount = [originAccountList_ objectAtIndex:self.selectedOriginAccountIndex];
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_ORIGIN_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ | %@",[Tools notNilString: chargeAccount.accountType],[Tools notNilString:[Tools getCurrencyLiteral:chargeAccount.currency]]]];
    
    [titleAndAttributes addAttribute:[Tools notNilString:chargeAccount.number]];

    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    
    //Destination bank
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_BANK_LOWER_CASE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.bankName]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Destination card
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil);
    NSString *card = [NSString stringWithFormat:@"%@ %@",
                      [Tools notNilString:[startUpResponseInformation_ cardType]],
                      [Tools notNilString:[startUpResponseInformation_ cardNumber]]
                        ];
    
    [titleAndAttributes addAttribute:card];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Beneficiary holder name
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_BENEFICIARY_NAME_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.beneficiary]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Paid Amount
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_AMOUNT_TO_TRANSFER_TEXT_KEY, nil)];
    
    NSString *amountToTransfer = [NSString stringWithFormat:@"%@ %@",
                                  [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[additionalInformation_ amountPaidCurrency]]],
                                  [Tools notNilString:[additionalInformation_ amountPaid]]];
    
    [titleAndAttributes addAttribute:[Tools notNilString:amountToTransfer]];
    
    if (titleAndAttributes != nil) {
        [mutableResult addObject:titleAndAttributes];
    }
    
     
    // - Taxes
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TAX_TEXT_KEY, nil)];
    
    NSString *tax = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[additionalInformation_ taxCurrency]]], [Tools notNilString:[additionalInformation_ tax]]];
    
    [titleAndAttributes addAttribute:[Tools notNilString:tax]];
    
    if (titleAndAttributes != nil) {
        [mutableResult addObject:titleAndAttributes];
    }
    
    // - Network use
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_NETWORK_USE_TEXT_KEY, nil)];
    
    NSString *networkUse = [Tools notNilString:[additionalInformation_ networkUse]];
    
    NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:networkUse];
    
    if (![number isEqualToNumber:[NSDecimalNumber zero]]){
        
        networkUse = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[additionalInformation_ networkUseCurrency]]], [Tools notNilString:networkUse]];
        
    } else {
        
        networkUse = networkUse;
        
        
    }
    
    [titleAndAttributes addAttribute:[Tools notNilString:networkUse]];
    
    if (titleAndAttributes != nil) {
        [mutableResult addObject:titleAndAttributes];
    }
 
    
    
    
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foThirdStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    
    //Operation type
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];

    
    //Operation State
    if([paymentSuccessAdditionalInfo_.operationState length]>0){
    
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(@"Estado de la operación", nil);
        [titleAndAttributes addAttribute:[Tools notNilString:paymentSuccessAdditionalInfo_.operationState]];
    
        if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
        }
    }

    
    
    //Operation Number
    if([paymentSuccessAdditionalInfo_.operationNumber length]>0){
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(NUMBER_OPERATION_TITLE_TEXT_KEY, nil);
        [titleAndAttributes addAttribute:[Tools notNilString:paymentSuccessAdditionalInfo_.operationNumber]];
    
        if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
        }
    }
    
    // - Date Time
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_DATE_KEY, nil)];
    
    NSString *dateString = @"";
    
    if (([[paymentSuccessAdditionalInfo_ transactionDate] length] > 0) && ([[paymentSuccessAdditionalInfo_ transactionHour] length] > 0)) {
        
        dateString = [NSString stringWithFormat:@"%@ | %@", [paymentSuccessAdditionalInfo_ transactionDate], [paymentSuccessAdditionalInfo_ transactionHour]];
        
    } else {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat: @"dd/MM/yyyy | HH:mm"];
        dateString = [dateFormat stringFromDate:[NSDate date]];
        [dateFormat release];
        
    }
    
    [titleAndAttributes addAttribute:[Tools notNilString:dateString]];
    
    if (titleAndAttributes != nil) {
        [mutableResult addObject:titleAndAttributes];
    }
    
     // - Operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OPERATION_TEXT_KEY, nil)];
   // titleAndAttributes.titleString = NSLocalizedString(FO_OPERATION_TYPE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:paymentSuccessAdditionalInfo_.operation]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    //Charged Account
    BankAccount *chargeAccount = [originAccountList_ objectAtIndex:self.selectedOriginAccountIndex];
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_ORIGIN_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ | %@",[Tools notNilString: chargeAccount.accountType],[Tools notNilString:[Tools getCurrencyLiteral:chargeAccount.currency]]]];
    
    [titleAndAttributes addAttribute:[Tools notNilString:chargeAccount.number]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    
    //Charged account holder
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_CARD_PAYMENT_OTHER_BANK_OWNER_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:paymentSuccessAdditionalInfo_.accountOwner]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
     
    //Destination bank
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(DESTINATION_BANK_LOWER_CASE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:paymentSuccessAdditionalInfo_.destinationBank]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Destination card
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil);
    NSString *card = [NSString stringWithFormat:@"%@ %@",
                      [Tools notNilString:[startUpResponseInformation_ cardType]],
                      [Tools notNilString:[startUpResponseInformation_ cardNumber]] ];
    
    [titleAndAttributes addAttribute:card];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Destination card
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CARD_PAYMENT_OWNER_TEXT_KEY, nil);
    NSString *beneficiary = [Tools notNilString:paymentSuccessAdditionalInfo_.cardHolder];
    
    [titleAndAttributes addAttribute:beneficiary];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }

    
    
    //Transfer amount
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_CARD_PAYMENT_OTHER_BANK_AMOUNT_TEXT_KEY, nil);
    NSString *amount = [NSString stringWithFormat:@"%@ %@",
                        [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:[Tools notNilString:[paymentSuccessAdditionalInfo_ amountPayedCurrency]]],
                        [Tools notNilString:[paymentSuccessAdditionalInfo_ amountPayed]]];
    
    [titleAndAttributes addAttribute:amount];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // - Taxes
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TAX_TEXT_KEY, nil)];
    
    NSString *tax = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[paymentSuccessAdditionalInfo_ taxCurrency]]], [Tools notNilString:[paymentSuccessAdditionalInfo_ tax]]];
    
    [titleAndAttributes addAttribute:[Tools notNilString:tax]];
    
    if (titleAndAttributes != nil) {
        [mutableResult addObject:titleAndAttributes];
    }
    
    // - Network use
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_NETWORK_USE_TEXT_KEY, nil)];
    
    NSString *networkUse = [Tools notNilString:[paymentSuccessAdditionalInfo_ networkUse]];
    
    NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:networkUse];
    
    if (![number isEqualToNumber:[NSDecimalNumber zero]]){
        
        networkUse = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[paymentSuccessAdditionalInfo_ networkUseCurrency]]], [Tools notNilString:networkUse]];
        
    } else {
        
        networkUse = networkUse;
        
        
    }
    
    [titleAndAttributes addAttribute:[Tools notNilString:networkUse]];
    
    if (titleAndAttributes != nil) {
        [mutableResult addObject:titleAndAttributes];
    }
    
    //Charged Amount
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CARD_PAYMENT_OTHER_BANK_FINAL_AMOUNT_TEXT_KEY, nil);
    NSString *amountCharged = [NSString stringWithFormat:@"%@ %@",
                        [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:[Tools notNilString:[paymentSuccessAdditionalInfo_ amountChargedCurrency]]],
                        [Tools notNilString:[paymentSuccessAdditionalInfo_ amountToCharge]]];
    
    [titleAndAttributes addAttribute:amountCharged];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    //change type
    
    if (([paymentSuccessAdditionalInfo_ changeType] != nil) &&
        (![[paymentSuccessAdditionalInfo_ changeType] isEqualToString:@""]) &&
        ([[paymentSuccessAdditionalInfo_ changeType] floatValue] != 0.0f)) {
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(CHANGE_TYPE_TITLE_KEY, nil);
        NSString *changeType = [NSString stringWithFormat:@"%@ %@",
                                [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:[Tools notNilString:[paymentSuccessAdditionalInfo_ changeTypeCurrency]]],
                                [Tools formatAmountWithDotDecimalSeparator:[Tools notNilString:[paymentSuccessAdditionalInfo_ changeType]] ]];
        
        [titleAndAttributes addAttribute:changeType];
        
        if (titleAndAttributes != nil) {
            
            [mutableResult addObject:titleAndAttributes];
            
        }
        
    }

    
    //Total charged Amount
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_CARD_PAYMENT_OTHER_BANK_TOTAL_AMOUNT_TEXT_KEY, nil);
    NSString *totalAmountCharged = [NSString stringWithFormat:@"%@ %@",
                                    [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:[Tools notNilString:[paymentSuccessAdditionalInfo_ totalChargedCurrency]]],
                                    [Tools notNilString:[paymentSuccessAdditionalInfo_ totalCharged]]];
    
    [titleAndAttributes addAttribute:totalAmountCharged];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
}

-(NSString*)confirmationMessage{
    return paymentSuccessAdditionalInfo_.message;
}

-(NSString*)notificationMessage{
    return paymentSuccessAdditionalInfo_.notificationMessage;
}

/*
 * Creates the account array to display in the picker view. Default implementation returns an empty array
 */
- (NSArray *)accountStringList{
    NSMutableArray *accountArray = [[[NSMutableArray alloc] init] autorelease];
    
    for (BankAccount *account in originAccountList_) {
        NSString *accountNumber = account.bankAccountListName;
        
        [accountArray addObject:accountNumber];
    }
    
    return accountArray;
}

/**
 * Returns the key name for the TIN confirmation response
 */
- (NSString *)notificationTINConfirmationKey{
    return kNotificationPaymentConfirmationResultEnds;
}

/**
 * Returns the key name for the confirmation response
 *
 */
- (NSString *)notificationConfirmationKey{
    return kNotificationPaymentTINOnlineResultEnds;
}

/**
 * Returns the key name for the success response
 *
 */
- (NSString *)notificationSuccessKey{
    return kNotificationPaymentSuccessResultEnds;
}

#pragma mark -
#pragma mark Operation methods
/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startFORequest {
    
    BOOL result = NO;
    BankAccount *originAccount = [self.originAccountList objectAtIndex:self.selectedOriginAccountIndex];
    NSString *originAccountNumber = originAccount.number;
    
    self.amountString = [Tools formatAmountWithDotDecimalSeparator:self.amountString];
    
    self.currency = [Tools getCurrencyServer:[self.currencyList objectAtIndex:[self selectedCurrencyIndex]]];
    
    if (([originAccountNumber length] > 0) &&
        ([self.currency length] > 0) ){
        
        NSString *emissionPlaceCode = [NSString stringWithFormat:@"%@$%@", [startUpResponseInformation_ placeCode],[startUpResponseInformation_ emissionPlace]];
        
        NSString *carrier1 = @"";
        NSString *carrier2 = @"";
        
        if (self.selectedCarrier1Index > 0 && self.selectedCarrier1Index <  [self.carrierList count]) {
            AlterCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier1Index];
            
            carrier1 = carrier.code;
        }
        
        
        if (self.selectedCarrier2Index > 0 && self.selectedCarrier2Index <  [self.carrierList count]) {
            AlterCarrier *carrier = [self.carrierList objectAtIndex:self.selectedCarrier2Index];
            
            carrier2 = carrier.code;
        }
        
        [[Updater getInstance] obtainFOPaymentCardCardOtherBankConfirmationForIndicatorOP:@"S"
                                                                                  Service:startUpResponseInformation_.service
                                                                              Description:startUpResponseInformation_.description
                                                                               CardNumber:startUpResponseInformation_.cardNumber
                                                                                ClassCard:startUpResponseInformation_.cardType
                                                                          DestinationBank:startUpResponseInformation_.destinationBank
                                                                                 Locality:emissionPlaceCode
                                                                                    issue:[originAccount branchAccount] currency:[self currency] amount:[self amountString] 
                                                                              email1:[Tools notNilString:[self destinationEmail1]]
                                                                              email2:[Tools notNilString:[self destinationEmail2]]
                                                                        phoneNumber1:[Tools notNilString:[self destinationSMS1]]
                                                                        phoneNumber2:[Tools notNilString:[self destinationSMS2]]
                                                                            carrier1:[Tools notNilString:carrier1]
                                                                            carrier2:[Tools notNilString:carrier2]
                                                                             message:[Tools notNilString:[self emailMessage]]];
        
        result = YES;
        
    }
    
    return result;
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startFORequestDataValidation {
	
	NSString *result = nil;
    
	if ([self selectedOriginAccountIndex] == NSNotFound || [self selectedOriginAccountIndex] < 0 || [self selectedOriginAccountIndex] > [[self originAccountList] count]) {
		
		result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
		
	} else if (!([startUpResponseInformation_.emissionPlace length] > 0)) {
        
        result = NSLocalizedString(PAYMENT_ERROR_LOCALITY_TEXT_KEY, nil);
        
    } else if ([self selectedOriginAccountIndex] < 0) {
        
        result = NSLocalizedString(PAYMENT_ERROR_ACCOUNT_PAYMENT_TEXT_KEY, nil);
        
    } else if (([self amountString] == nil || [[self amountString] isEqualToString:@""])) {
        
        result = NSLocalizedString(CARD_PAYMENT_AMOUNT_TO_PAY_ERROR_KEY, nil);
        
    } else if ([self selectedCurrencyIndex] < 0) {
        
        result = NSLocalizedString(PAYMENT_ERROR_CURRENCY_TEXT_KEY, nil);
        
    } else if ([[startUpResponseInformation_ beneficiaryName] isEqualToString:@""]) {
        
        result = NSLocalizedString(PAYMENT_ERROR_BENEFICIARY_NAME_TEXT_KEY, nil);
        
    }
    
    if (result == nil) {
        result = [super startFORequestDataValidation];
    }
    
    return result;
    
}

- (BOOL)startTypePaymentInmediateRequest{
    
    BOOL result = NO;
    
    [[Updater getInstance]  obtainPaymentCardCardOtherBankTINConfirmationForFlag:@"IN" beneficiary:@""];
    result = YES;
    
    return result;
}

- (BOOL)startTypePaymentScheduledRequest{
    
    BOOL result = NO;
    
    [[Updater getInstance]  obtainPaymentCardCardOtherBankTINConfirmationForFlag:@"PH" beneficiary:beneficiary_];
    result = YES;
    
    return result;
}

-(BOOL)startTypePaymentRequest{
    
    if(([typeFlow_ isEqualToString:@"A"] && selectedTransferTypeIndex_ ==0) || [typeFlow_ isEqualToString:@"L"]){
        return  [self startTypePaymentInmediateRequest];
    }else{
        return [self startTypePaymentScheduledRequest];
    }
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startSecondTransferRequestDataValidation{
    NSString *result = nil;
    
    if(([self.typeFlow isEqualToString:@"A"] && selectedTransferTypeIndex_ == 1) ||
       [self.typeFlow isEqualToString:@"N"]){
        
        if (beneficiary_ == nil || [beneficiary_ isEqualToString:@""]) {
            
            result = NSLocalizedString(TRANSFER_TO_ANOTHER_BANK_MUST_WRITE_A_BENEFICIARY_TEXT_KEY, nil);
            
        }
    }
    else if(selectedTransferTypeIndex_<0){
        result = NSLocalizedString(@"Seleccione el tipo de transferencia que desea realizar",nil);
    }
    
    return result;
}

- (BOOL)startFoConfirmationRequest{
    BOOL result = [super startFoConfirmationRequest];
    
    [[Updater getInstance] obtainPaymentCardOtherBankSuccessForSecondFactorKey:@""];
    
    return result;
}

/**
 * @param FOResponse The FO response received from the server
 * @return YES when the confirmation response is correct, NO otherwise
 */
- (BOOL)fOResponseReceived:(StatusEnabledResponse *)foResponse{
    
    BOOL result = [super fOResponseReceived:foResponse];
    
    if (!result) {
        
        if ([foResponse isKindOfClass:[PaymentConfirmationResponse class]]) {
            
            PaymentConfirmationResponse *response = (PaymentConfirmationResponse *)foResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response retain];
            beneficiary_ = additionalInformation_.beneficiary;
            self.seal = [additionalInformation_ seal];
            
            result = YES;
            
        }else if(![foResponse isError] && [foResponse isKindOfClass:[PaymentTINOnlineResponse class]]){
            
            PaymentTINOnlineResponse *response = (PaymentTINOnlineResponse*)foResponse;
            
            self.commissionL = response.commissionL ;
            self.commissionN = response.commissionN;
            self.totalPaymentL = response.totalPayL;
            self.totalPaymentN = response.totalPayN;
            self.typeFlow = response.typeFlow;
            self.optionalDisclaimer = response.disclaimer;
            additionalInformation_ = [response.additionalInformation retain];
            if(additionalInformation_.beneficiary)
                self.beneficiary = additionalInformation_.beneficiary;
            self.seal = [additionalInformation_ seal];
            
            result = YES;
            
        }

    }
    
    return result;
}

/*
 * Notifies the fO operation helper the confirmation response received from the server. The fo operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        
        if (![confirmationResponse isError] && [confirmationResponse isKindOfClass:[PaymentSuccessResponse class]]) {
            
            PaymentSuccessResponse *response = (PaymentSuccessResponse *)confirmationResponse;
            
            if (paymentSuccessAdditionalInfo_ != nil) {
                [paymentSuccessAdditionalInfo_ release];
                paymentSuccessAdditionalInfo_ = nil;
            }
            paymentSuccessAdditionalInfo_ = [response retain];
            
            result = YES;
            
        }
        
    }
    
    return result;
    
    
}

@end
