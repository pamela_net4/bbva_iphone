//
//  FORechargeCellPhone.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/25/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOOperationHelper.h"

@class FOERechargeCellphoneStepOneResponse;
@class PaymentConfirmationResponse;
@class PaymentSuccessResponse;
@interface FORechargeCellPhone : FOOperationHelper{
    
@private
    
    /**
     * Array with the origin selectable accounts. All objects are PaymentElement instances
     */
    NSMutableArray *originAccountList_;
    
    /**
     * Flag to indicate if is owner of the account
     */
    BOOL isItf_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    FOERechargeCellphoneStepOneResponse *startUpResponseInformation_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    PaymentConfirmationResponse *additionalInformation_;
    
    /**
     * Transfer Success Additional Information
     */
    PaymentSuccessResponse *paymentSuccessAdditionalInfo_;
    
}

/**
 * Provides read-only access to the array with the origin selectable accounts
 */
@property (nonatomic, readonly, retain) NSArray *originAccountList;
/**
 * Designated initialized. Initializes a FOERechargeCellphoneStepOneResponse instance with the initial transfer accounts response
 *
 * @param fOERechargeCellphoneStepOneResponse The recharge response
 * @return The initialized FOERechargeCellphoneStepOneResponse instance;
 */
- (id)initWithPaymentStartupResponse:(FOERechargeCellphoneStepOneResponse *)fOERechargeCellphoneStepOneResponse;

@end

