//
//  FOInstitutionsAndCompanies.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/24/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOOperationHelper.h"

@class FOEInstitutionPaymentStepOneResponse;
@class PaymentInstitutionAndCompaniesConfirmationInformationResponse;
@class PaymentInstitutionAndCompaniesSuccessConfirmationResponse;
@interface FOInstitutionsAndCompanies : FOOperationHelper{
    
@private
    
    /**
     * Array with the origin selectable accounts. All objects are PaymentElement instances
     */
    NSMutableArray *originAccountList_;
    
    /**
     * Array with the origin selectable cards. All objects are PaymentElement instances
     */
    NSMutableArray *originCardList_;
    
    /**
     * Flag to indicate if is owner of the account
     */
    BOOL isItf_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    FOEInstitutionPaymentStepOneResponse *startUpResponseInformation_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    PaymentInstitutionAndCompaniesConfirmationInformationResponse *additionalInformation_;
    
    /**
     * Transfer Success Additional Information
     */
    PaymentInstitutionAndCompaniesSuccessConfirmationResponse *paymentSuccessAdditionalInfo_;
    
    /**
     * Flag to know if is with db
     */
    BOOL isDB_;
    
    /**
     * Flag to know if is partial o total
     */
    BOOL isPartial_;
    
    /**
     * array with the pending documents selected
     */
    NSArray *brand_;
    
    /**
     * array with the pending documents
     */
    NSArray *pendingDocumentsArray_;
    
    /**
     * min amount of the partial doc selected
     */
    NSString *minAmount;
    
    /**
     * max amount of the partial doc selected
     */
    NSString *maxAmount;
    
    /**
     * max amount of the partial doc selected
     */
    NSString *newBrand;
    
}

@property (nonatomic, readwrite, retain) PaymentInstitutionAndCompaniesConfirmationInformationResponse *additionalInformation;

@property (nonatomic, readwrite, retain) FOEInstitutionPaymentStepOneResponse *startUpResponseInformation;
/**
 * Provides read-only access to the array with the origin selectable accounts
 */
@property (nonatomic, readwrite, retain) NSArray *originAccountList;
/**
 * Provides read-only access to the array with the origin selectable cards
 */
@property (nonatomic, readwrite, retain) NSArray *originCardList;
/**
 * Provides read-only access to the array with the pending documents
 */
@property (nonatomic, readwrite, retain) NSArray *pendingDocumentsArray;
/**
 * Provides read-only access to the array with the pending documents selected
 */
@property (nonatomic, readwrite, retain) NSArray *brand;
/**
 * Provides read-only access to the Flag to know if is with db
 */
@property (nonatomic, readwrite, assign) BOOL isDB;
/**
 * Provides read-only access to the Flag to know if is partial o total
 */
@property (nonatomic, readwrite, assign) BOOL isPartial;


/**
 * Designated initialized. Initializes a FOEThirdAccountTransferStepOneResponse instance with the initial transfer accounts response
 *
 * @param transferStartupResponse The transfer accounts response
 * @return The initialized FOEThirdAccountTransferStepOneResponse instance;
 */
- (id)initWithPaymentStartupResponse:(FOEInstitutionPaymentStepOneResponse *)fOEInstitutionPaymentStepOneResponse;

@end

