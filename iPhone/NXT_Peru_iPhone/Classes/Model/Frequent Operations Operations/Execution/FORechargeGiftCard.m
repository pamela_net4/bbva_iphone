//
//  FORechargeGiftCard.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/20/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FORechargeGiftCard.h"

#import "FOERechargeGiftCardStepOneResponse.h"
#import "FOAccountList.h"
#import "FOCard.h"
#import "BankAccount.h"
#import "AccountList.h"
#import "FOCardList.h"
#import "GiftCardStepThreeResponse.h"
#import "GiftCardStepTwoResponse.h"
#import "GlobalAdditionalInformation.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "Updater.h"

@implementation FORechargeGiftCard
#pragma mark -
#pragma mark Properties

@synthesize originAccountList = originAccountList_;
@synthesize originCardList = originCardList_;
@synthesize firstText = firstText_;
@synthesize secondText = secondText_;
@synthesize thirdText = thirdText_;
@synthesize fourthText = fourthText_;
#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [originAccountList_ release];
    originAccountList_ = nil;
    
    [originCardList_ release];
    originCardList_ = nil;
    
    [firstText_ release];
    firstText_ = nil;
    
    [secondText_ release];
    secondText_ = nil;
    
    [thirdText_ release];
    thirdText_ = nil;
    
    [fourthText_ release];
    fourthText_ = nil;
    
    [super dealloc];
    
}
- (id)initWithRechargeStartupResponse:(FOERechargeGiftCardStepOneResponse *)fOERechargeGiftCardStepOneResponse{
    
    if ((self = [super init])) {
        
        // The initial operation in iPad returns elements no needed in iPhone

        originAccountList_ = [[NSMutableArray alloc] initWithArray:fOERechargeGiftCardStepOneResponse.foAccountList.accountList];
        
        [self setSelectedOriginAccountIndex:-1];
        
        if (startUpResponseInformation_ != nil) {
            [startUpResponseInformation_ release];
            startUpResponseInformation_ = nil;
        }
        
        startUpResponseInformation_ = [fOERechargeGiftCardStepOneResponse retain];
        
        [self setSelectedCurrencyIndex:0];
        
        if ([[[fOERechargeGiftCardStepOneResponse foCardList] foCardArray] count] > 0) {
            
            [self setHasCard:TRUE];
            
            originCardList_ = [[NSMutableArray alloc] initWithArray:[[fOERechargeGiftCardStepOneResponse foCardList] foCardArray]];
            
            [self setSelectedOriginCardIndex:-1];
            
        }
        
        NSArray *giftCardParts = [fOERechargeGiftCardStepOneResponse.giftCard componentsSeparatedByString:@"-"];
        
        if ([giftCardParts count] > 0) {
            [self setFirstText:[giftCardParts objectAtIndex:0]];
        }
        
        if ([giftCardParts count] > 1) {
            [self setSecondText:[giftCardParts objectAtIndex:1]];
        }
        
        if ([giftCardParts count] > 2) {
            [self setThirdText:[giftCardParts objectAtIndex:2]];
        }
        
        if ([giftCardParts count] > 3) {
            [self setFourthText:[giftCardParts objectAtIndex:3]];
        }
        self.carrierList = [[NSArray alloc] initWithArray:[[fOERechargeGiftCardStepOneResponse foCarrierList] alterCarrierList]];
    }
    
    return self;
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the frequent operation type. Base class returns a default value (transfer between user accounts)
 *
 * @return The frequent operation type
 */
- (FOTypeEnum)foOperationType {
    
    return FOTETransferToGiftCard;
    
}

/*
 * Returns the List of currency for the operation. Base class returns an empty array
 *
 * @return The currency list
 */
- (NSArray *)currencyList {
    
    return [NSArray arrayWithObjects:NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil),
            NSLocalizedString(DOLLARS_CURRENCY_TEXT_KEY, nil),
            nil];
    
}

/*
 * Returns the can show legal terms flag.
 *
 */
- (BOOL)canShowLegalTerms {
    
    return FALSE;
    
}

/*
 * Returns the can show email and sms
 */
- (BOOL)canSendEmailandSMS {
    
    return YES;
    
}

/*
 * Returns the localized transfer operation type string. The transfer between accounts localized string is returned
 *
 * @return The localized transfer operation type string
 */
- (NSString *)localizedFOOperationTypeString {
    
    return NSLocalizedString(TRANSFER_GIFT_CARD_TEXT_1_KEY, nil);
    
}

/*
 * Returns the localized terms and conditions string. The transfer between accounts legal terms localized string is returned
 *
 * @return The localized transfer legal terms operation type string
 */
- (NSString *)localizedTermsAndConditionsString {
    
    return NSLocalizedString(TRANSFER_LEGAL_TERMS_KEY, nil);
    
}

/*
 * Returns the legalTermsURL string. Base class returns an empty string
 *
 * @return The legalTermsURL string
 */
- (NSString *)legalTermsURL {
    
#if defined(SIMULATE_HTTP_CONNECTION)
    return [Tools notNilString:@"http://www.google.es"];
#else
    return [Tools notNilString:@"http://www.google.es"];
#endif
    
}

#pragma mark -
#pragma mark Information distribution

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foFirstStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Service
    
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(SERVICE_TITLE_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.service]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //Frequen operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_POSITION_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.nick]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    //gift Card number
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_GIFT_CARD_TITLE_TEXT_KEY, nil);
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.giftCard]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    return mutableResult;
}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foSecondStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Operation
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute:[Tools notNilString:startUpResponseInformation_.service]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    //Charged Account/Card
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_GIFT_CARD_CHARGED_ACCOUNT_CARD_KEY, nil);
    NSString *accountCardNumber = @"";
    NSString *currencyOperation = additionalInformation_.currency;
    
    if ([CURRENCY_SOLES_SYMBOL isEqualToString:additionalInformation_.currency]) {
        currencyOperation = CURRENCY_SOLES_LITERAL;
    }else{
        currencyOperation = CURRENCY_DOLARES_LITERAL;
    }
    
    if (additionalInformation_.account != nil) {
        accountCardNumber = [NSString stringWithFormat:@"%@ | %@ %@",additionalInformation_.account.accountType, [Tools getCurrencyLiteral:additionalInformation_.account.currency], additionalInformation_.account.subject];
    }else if (additionalInformation_.foCard != nil){
        
        accountCardNumber = [NSString stringWithFormat:@"%@ | %@ %@",additionalInformation_.foCard.cardType,[Tools getCurrencyLiteral: additionalInformation_.foCard.currency], additionalInformation_.foCard.cardNumber];
    }
    
    [titleAndAttributes addAttribute:accountCardNumber];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    
    //Gift Card
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_GIFT_CARD_TITLE_KEY, nil);
    
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.giftCard]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_GIFT_CARD_AMOUNT_TO_PAY_KEY, nil);
    
    NSString *currency = @"";
    
    if (([self selectedCurrencyIndex] >= 0) && ([self selectedCurrencyIndex] < [[self currencyList] count])) {
        
        currency = [[self currencyList] objectAtIndex:[self selectedCurrencyIndex]];
        
    }
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:[currency uppercaseString]], self.amountString]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return [NSArray arrayWithArray:mutableResult];

}

/*
 * Creates the title and attributes array to display in the transfer second step. Default implementation returns an empty array
 */
- (NSArray *)foThirdStepInformation{
    
    NSMutableArray *mutableResult = [NSMutableArray array];
    
    //Operation
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(FO_OPERATION_TITLE_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute:[Tools notNilString:transferSuccessAdditionalInfo_.service]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Origin
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(ORIGIN_TITLE_TEXT_KEY, nil);
    
    NSString *accountCardNumber = @"";
    NSString *currencyOperation = additionalInformation_.currency;
    
    if ([CURRENCY_SOLES_SYMBOL isEqualToString:additionalInformation_.currency]) {
        currencyOperation = CURRENCY_SOLES_LITERAL;
    }else{
        currencyOperation = CURRENCY_DOLARES_LITERAL;
    }
    
    if (additionalInformation_.account != nil) {
        accountCardNumber = [NSString stringWithFormat:@"%@ | %@ %@",additionalInformation_.account.accountType,[Tools getCurrencyLiteral: additionalInformation_.account.currency], additionalInformation_.account.subject];
    }else if (additionalInformation_.foCard != nil){
        
        accountCardNumber = [NSString stringWithFormat:@"%@ | %@ %@",additionalInformation_.foCard.cardType,[Tools getCurrencyLiteral: additionalInformation_.foCard.currency], additionalInformation_.foCard.cardNumber];
    }
    
    [titleAndAttributes addAttribute:accountCardNumber];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Gift card
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(TRANSFER_GIFT_CARD_TITLE_KEY, nil);
    
    [titleAndAttributes addAttribute:[Tools notNilString:additionalInformation_.giftCard]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    // Charged Amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(CARD_PAYMENT_CHARGED_AMOUNT_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute: [Tools notNilString:transferSuccessAdditionalInfo_.amountCharge ]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    if([transferSuccessAdditionalInfo_.exchangeRate length]>0)
    {
        //Exchange rate
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        titleAndAttributes.titleString = NSLocalizedString(CHANGE_TYPE_TITLE_KEY, nil);
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@",  transferSuccessAdditionalInfo_.exchangeRate]];
        
        if (titleAndAttributes != nil) {
            
            [mutableResult addObject:titleAndAttributes];
            
        }
    }

    
    // Payed Amount
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    titleAndAttributes.titleString = NSLocalizedString(AMOUNT_CHARGERD_TITLE_TEXT_KEY, nil);
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools getCurrencySimbol:[transferSuccessAdditionalInfo_.currency uppercaseString]], transferSuccessAdditionalInfo_.amountPaid]];
    
    if (titleAndAttributes != nil) {
        
        [mutableResult addObject:titleAndAttributes];
        
    }
    
    return mutableResult;
}

/*
 * Creates the account array to display in the picker view. Default implementation returns an empty array
 */
- (NSArray *)accountStringList{
    NSMutableArray *accountArray = [[[NSMutableArray alloc] init] autorelease];
    
    for (Account *account in originAccountList_) {
        NSString *accountNumber = account.accountIdAndDescription;
        
        [accountArray addObject:accountNumber];
    }
    
    return accountArray;
}
/*
 * Creates the account array to display in the picker view. Default implementation returns an empty array
 */
- (NSArray *)cardStringList{
    
    NSMutableArray *cardStringArray  = [[[NSMutableArray alloc]init] autorelease];
    for(int i=0; i<[self.originCardList count]; i++)
    {
        FOCard *card = [self.originCardList objectAtIndex:i];
        NSString *cardType = card.cardType;
        [cardStringArray addObject: [NSString stringWithFormat:@"%@ | %@", [Tools obfuscateCardNumber:card.cardNumber],cardType]];
    }
    
    return  cardStringArray;
}

/**
 * Returns the key name for the confirmation response
 *
 */
- (NSString *)notificationConfirmationKey{
    return kNotificationTransferConfirmationResponseReceived;
}

/**
 * Returns the key name for the success response
 *
 */
- (NSString *)notificationSuccessKey{
    return kNotificationTransferSuccessResponseReceived;
}

#pragma mark -
#pragma mark Operation methods
/*
 * Performs the payment request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (BOOL)startFORequest {
    
    BOOL result = NO;
    NSString *accountType = @"";
    NSString *originAccountNumber = @"";
    
    if (([self selectedOriginAccountIndex] != NSNotFound &&
         [self selectedOriginAccountIndex] >= 0 &&
         [self selectedOriginAccountIndex] < [[self originAccountList] count]) ){
        
        Account *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
        originAccountNumber = [originAccount param];
        accountType = @"C";
        
    }else if( ([self selectedOriginCardIndex] != NSNotFound &&
               [self selectedOriginCardIndex] >= 0 &&
               [self selectedOriginCardIndex] < [[self originCardList] count]) ){
        FOCard *originCard = [[self originCardList] objectAtIndex:[self selectedOriginCardIndex]];
        originAccountNumber = [originCard param];
        accountType = @"T";
    }
    NSString *amount = [Tools formatAmountWithDotDecimalSeparator:self.amountString];
    NSInteger selectedCurrencyIndex = [self selectedCurrencyIndex];
    NSString *currencySelected = @"";
    
    if ((selectedCurrencyIndex >= 0) && (selectedCurrencyIndex < [[self currencyList] count])) {
        
        currencySelected = [Tools getCurrencyServer:[self.currencyList objectAtIndex:[self selectedCurrencyIndex]]];
        
    }
    NSString *destinationCard = @"";
    
    destinationCard = [NSString stringWithFormat:@"%@-%@-%@-%@", firstText_, secondText_, thirdText_, fourthText_];
    
    if (([originAccountNumber length] > 0) &&
        ([firstText_ length] > 0) &&
        ([secondText_ length] > 0) &&
        ([thirdText_ length] > 0) &&
        ([fourthText_ length] > 0) &&
        ([amount length] > 0) &&
        (selectedCurrencyIndex > -1)) {
        
        [[Updater getInstance] transferToGiftCardConfirmationFromAccountType:accountType
                                                                  fromNumber:originAccountNumber
                                                                    toNumber:destinationCard
                                                                      amount:amount
                                                                    currency:currencySelected
                                                         toConfirmationPhone:@""
                                                          toConfirmationMail:@""
                                                                     carrier:@""
                                                            referenceMessage:@""];
        
        result = YES;
        
    }
    
    return result;
}

/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startFORequestDataValidation {
	
	NSString *result = nil;
    
	if (!self.hasCard && ([self selectedOriginAccountIndex] == NSNotFound || [self selectedOriginAccountIndex] < 0 || [self selectedOriginAccountIndex] > [[self originAccountList] count] ) ) {
		
		result = NSLocalizedString(TRANSFER_SELECT_ORIGIN_ACCOUNT_TEXT_KEY, nil);
		
	} else if ( ([self selectedOriginAccountIndex] == NSNotFound ||
                 [self selectedOriginAccountIndex] < 0 ||
                 [self selectedOriginAccountIndex] > [[self originAccountList] count] )
               &&
               ([self selectedOriginCardIndex] == NSNotFound ||
                [self selectedOriginCardIndex] < 0 ||
                [self selectedOriginCardIndex] > [[self originCardList] count] ) ){
                   
                   result = NSLocalizedString(PAYMENT_RECHARGE_ERROR_INVALID_NUMBER_TEXT_KEY, nil);
                   
               }   else {
                   
                   NSString *originAccountNumber = @"";
                   
                   if (!self.hasCard) {
                       Account *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
                       originAccountNumber = [originAccount subject];
                   }else{
                       if (([self selectedOriginAccountIndex] != NSNotFound &&
                            [self selectedOriginAccountIndex] >= 0 &&
                            [self selectedOriginAccountIndex] < [[self originAccountList] count]) ){
                           
                           Account *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
                           originAccountNumber = [originAccount subject];
                           
                       }else if( ([self selectedOriginCardIndex] != NSNotFound &&
                                  [self selectedOriginCardIndex] >= 0 &&
                                  [self selectedOriginCardIndex] < [[self originCardList] count]) ){
                           FOCard *originCard = [[self originCardList] objectAtIndex:[self selectedOriginCardIndex]];
                           originAccountNumber = [originCard cardNumber];
                       }
                   }
                   
                   if (([firstText_ length] == 0) ||
                       ([secondText_ length] == 0) ||
                       ([thirdText_ length] == 0) ||
                       ([fourthText_ length] == 0) ) {
                       
                       result = NSLocalizedString(TRANSFER_ERROR_CARD_NUMBER_TEXT_KEY, nil);
                       
                   } else if (([firstText_ length] != 4) ||
                              ([secondText_ length] != 4) ||
                              ([thirdText_ length] != 4) ||
                              ([fourthText_ length] != 4) ) {
                       
                       result = NSLocalizedString(TRANSFER_ERROR_VALID_GIFT_CARD_TEXT_KEY, nil);
                       
                   } else {
                       
                       NSString *destinationAccountNumber = [NSString stringWithFormat:@"%@-%@-%@-%@", firstText_,secondText_,thirdText_, fourthText_];
                       
                       if ([originAccountNumber isEqualToString:destinationAccountNumber]) {
                           
                           result = NSLocalizedString(TRANSFERS_TO_THIRD_ACCOUNTS_SAME_ACCOUNT_ERROR_KEY, nil);
                           
                       } else if ([self selectedCurrencyIndex] < 0) {
                           
                           result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
                           
                       } else {
                           
                           NSString *originAccountNumber = @"";
                           
                           if (([self selectedOriginAccountIndex] != NSNotFound &&
                                [self selectedOriginAccountIndex] >= 0 &&
                                [self selectedOriginAccountIndex] < [[self originAccountList] count]) ){
                               
                               Account *originAccount = [[self originAccountList] objectAtIndex:[self selectedOriginAccountIndex]];
                               originAccountNumber = [originAccount subject];
                               
                           }else if( ([self selectedOriginCardIndex] != NSNotFound &&
                                      [self selectedOriginCardIndex] >= 0 &&
                                      [self selectedOriginCardIndex] < [[self originCardList] count]) ){
                               FOCard *originCard = [[self originCardList] objectAtIndex:[self selectedOriginCardIndex]];
                               originAccountNumber = [originCard cardNumber];
                           }
                           
                           NSString *currencySelected = @"";
                           
                           if (([self selectedCurrencyIndex] >= 0) && ([self selectedCurrencyIndex] < [[self currencyList] count])) {
                               
                               currencySelected = [[self currencyList] objectAtIndex:[self selectedCurrencyIndex]];
                               
                           }
                           
                           if (!([originAccountNumber length] > 0)) {
                               
                               result = NSLocalizedString(TRANSFER_ERROR_ORIGIN_ACCOUNT_TEXT_KEY, nil);
                               
                           } else if (!([currencySelected length] > 0)) {
                               
                               result = NSLocalizedString(TRANSFER_ERROR_CURRENCY_TEXT_KEY, nil);
                               
                           } else if (([self.amountString isEqualToString:@"0.00"]) || ([self.amountString floatValue] == 0.0f)) {
                               
                               result = NSLocalizedString(TRANSFER_ERROR_TRANSFER_AMOUNT_TEXT_KEY, nil);
                               
                           } /*else if (([self.amountString floatValue] < 150.0f)) {
                               
                               result = NSLocalizedString(@"Ingrese un monto mayor o igual a 150", nil);
                               
                           }*/
                       }
                   }
                   
               }
    
    if (result == nil) {
        result = [super startFORequestDataValidation];
    }
    
    return result;
    
}

- (BOOL)startFoConfirmationRequest{
    BOOL result = [super startFoConfirmationRequest];
    
    [[Updater getInstance] TransferToGiftCardResultFromSecondFactorKey:@""];
    
    return result;
}

/**
 * @param FOResponse The FO response received from the server
 * @return YES when the confirmation response is correct, NO otherwise
 */
- (BOOL)fOResponseReceived:(StatusEnabledResponse *)foResponse{
    
    BOOL result = [super fOResponseReceived:foResponse];
    
    if (!result) {
        
        if (![foResponse isError] && [foResponse isKindOfClass:[GiftCardStepTwoResponse class]]) {
            
            GiftCardStepTwoResponse *response = (GiftCardStepTwoResponse *)foResponse;
            
            if (additionalInformation_ != nil) {
                [additionalInformation_ release];
                additionalInformation_ = nil;
            }
            
            additionalInformation_ = [response retain];
            
            self.seal = [additionalInformation_ securitySeal];
         
            result = YES;
            
        }
    }
    
    return result;
}

/*
 * Notifies the fO operation helper the confirmation response received from the server. The fo operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 */
- (BOOL)confirmationResponseReceived:(StatusEnabledResponse *)confirmationResponse {
    
    BOOL result = [super confirmationResponseReceived:confirmationResponse];
    
    if (!result) {
        
        if (![confirmationResponse isError] && [confirmationResponse isKindOfClass:[GiftCardStepThreeResponse class]]) {
            
            GiftCardStepThreeResponse *response = (GiftCardStepThreeResponse *)confirmationResponse;
            
            if (transferSuccessAdditionalInfo_ != nil) {
                [transferSuccessAdditionalInfo_ release];
                transferSuccessAdditionalInfo_ = nil;
            }
            transferSuccessAdditionalInfo_ = [response retain];
            
            result = YES;
            
        }
        
    }
    
    return result;
    
    
}

@end
