//
//  FOPaymentCOtherBank.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo on 2/21/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FOOperationHelper.h"

@class FOEPaymentCOtherBankStepOneResponse;
@class PaymentConfirmationResponse;
@class PaymentSuccessResponse;
@interface FOPaymentCOtherBank : FOOperationHelper{
    
@private
    
    /**
     * Array with the origin selectable accounts. All objects are PaymentElement instances
     */
    NSMutableArray *originAccountList_;
    
    /**
     * Selected destination bank code
     */
    NSString *selectedDestinationBank_;
    
    /**
     * Selected destination account office
     */
    NSString *selectedDestinationAccountOffice_;
    
    /**
     * Selected destination account number
     */
    NSString *selectedDestinationAccountNumber_;
    
    /**
     * Selected destination account control number
     */
    NSString *selectedDestinationAccountControl_;
    
    /**
     * Flag to indicate if is owner of the account
     */
    BOOL isItf_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    FOEPaymentCOtherBankStepOneResponse *startUpResponseInformation_;
    
    /**
     * Transfer Confirmation Additional Information
     */
    PaymentConfirmationResponse *additionalInformation_;
    
    /**
     * Transfer Success Additional Information
     */
    PaymentSuccessResponse *paymentSuccessAdditionalInfo_;
    
    /**
     * Provides readwrite access to the commissionN
     */
    NSString *commissionN_;
    
    /**
     * Provides readwrite access to the commissionL
     */
    NSString *commissionL_;
    
    /**
     * Provides readwrite access to the totalPaymentN
     */
    NSString *totalPaymentN_;
    
    /**
     * Provides readwrite access to the totalPaymentL
     */
    NSString *totalPaymentL_;
    
    /**
     * Provides readwrite access to the typeFlow
     */
    NSString *typeFlow_;
    
    
    /**
     * Selected document type index
     */
    NSInteger selectedTransferTypeIndex_;
    /**
     * Beneficiary
     */
    NSString *beneficiary_;

    
}

/**
 * Provides read-only access to the array with the origin selectable accounts
 */
@property (nonatomic, readonly, retain) NSArray *originAccountList;


/**
 * Provides readwrite access to the commissionN
 */
@property (nonatomic, readwrite, copy) NSString *commissionN;

/**
 * Provides readwrite access to the commissionL
 */
@property (nonatomic, readwrite, copy) NSString *commissionL;

/**
 * Provides readwrite access to the totalPaymentN
 */
@property (nonatomic, readwrite, copy) NSString *totalPaymentN;

/**
 * Provides readwrite access to the totalPaymentL
 */
@property (nonatomic, readwrite, copy) NSString *totalPaymentL;

/**
 * Provides readwrite access to the typeFlow
 */
@property (nonatomic, readwrite, copy) NSString *typeFlow;


/**
 * Provides readwrite access to the selectedDocumentTypeIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedTransferTypeIndex;

/**
 * Provides readwrite access to the beneficiary
 */
@property (nonatomic, readwrite, copy) NSString *beneficiary;

/**
 * Designated initialized. Initializes a FOEPaymentCOtherBankStepOneResponse instance with the initial transfer accounts response
 *
 * @param paymentStartupResponse The payment other bank card response
 * @return The initialized FOEPaymentCOtherBankStepOneResponse instance;
 */
- (id)initWithPaymentStartupResponse:(FOEPaymentCOtherBankStepOneResponse *)FOEPaymentCOtherBankStepOneResponse;


- (BOOL)startTypePaymentInmediateRequest;


- (BOOL)startTypePaymentScheduledRequest;

- (BOOL) startTypePaymentRequest;
/*
 * Returns the error message for the invalid data. If @"" the data is correct
 */
- (NSString *)startSecondTransferRequestDataValidation;
@end

