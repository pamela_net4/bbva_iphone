//
//  FOInscriptionBaseProcess.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 14/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "FrequentOperationConstants.h"

@protocol FOInscriptionBaseProcessDelegate

@optional

- (void) dataAnalysisHasFinished;

- (void) confirmationAnalysisHasFinished;

- (void) successAnalysisHasFinished;

- (void) successAnalysisHasFinishedWithError;

@end

@interface FOInscriptionBaseProcess : NSObject {

@private
    
    NSString *service_;
    
    NSMutableArray *confirmationInfoArray_;
    
    NSMutableArray *successInfoArray_;
    
    NSMutableArray *dataInfoArray_;
    
    NSMutableArray *emailArray_;
    
    NSMutableArray *phoneNumberArray_;
    
    NSMutableArray *channelArray_;
    
    NSString *largeAlias_;
    
    NSString *shortAlias_;
    
    NSInteger *dayOfMonthNotification_;
    
    NSString *coordHint_;
    
    NSString *seal_;
    
    NSString *secondFactorKey_;
    
    
    NSString *phoneDisclaimer_;
    
    NSString *emailDisclaimer_;
    
    NSMutableArray *selectedChannels_;
    
    BOOL notificationsEnabled_;
    
    id<FOInscriptionBaseProcessDelegate> delegate_;
    
    /**
     * Send email flag
     */
    BOOL sendSMS_;
    
    /**
     * Send email flag
     */
    BOOL sendEmail_;
    
    /*
     *selected carrier one index
     */
    NSInteger selectedPhoneNumberIndex_;
    
    /**
     * User selected destination email address
     */
    NSInteger selectedEmailIndex_;
    
    /**
     * User selected email message
     */
    NSString *emailMessage_;
    
    /**
     * Legal terms accepted flag
     */
    BOOL legalTermsAccepted_;
    
    /**
     * Legal terms URL
     */
    NSString *legalTermsURL_;
    
    /**
     * Confirmation message.
     */
    NSString *message_;
    
    BOOL *showComboEmailButton_;
    
    BOOL *showComboPhoneNumberButton_;
    
    NSString *successMessage_;
    
    NSString *suggestion_;
    
    UIViewController *rootViewController_;
    
    NSString *companyCode_;
    
}

@property (nonatomic, readwrite, assign) UIViewController *rootViewController;

@property (nonatomic, readwrite, assign) NSString *service;

@property (nonatomic, readwrite, assign) BOOL showEmailCombo;

@property (nonatomic, readwrite, assign) BOOL showPhoneNumberCombo;

@property (nonatomic, readwrite, assign) NSInteger *dayOfMonthNotification;

@property (nonatomic, readwrite, assign) BOOL notificationEnabled;

@property (nonatomic, readwrite, retain) NSString *largeAlias;

@property (nonatomic, readwrite, retain) NSString *shortAlias;

@property (nonatomic, readwrite, retain) NSMutableArray *selectedChannels;

@property (nonatomic, readwrite, retain) NSArray *emailArray;

@property (nonatomic, readwrite, retain) NSArray *phoneNumberArray;

@property (nonatomic, readwrite, retain) NSString *successMessage;

@property (nonatomic, readwrite, retain) NSString *phoneDisclaimer;

@property (nonatomic, readwrite, retain) NSString *emailDisclaimer;

@property (nonatomic, readwrite, retain) NSArray *confirmationInfoArray;

@property (nonatomic,readwrite, retain) NSMutableArray *channelArray;
/**
 * Provides read-write access to the successInfoArray
 */
@property (nonatomic, readwrite, retain) NSArray *successInfoArray;

/**
 * Provides read-write access to the dataInfoArray
 */
@property (nonatomic, readwrite, retain) NSArray *dataInfoArray;

/**
 * Provides read-write access to the coordHint
 */
@property (nonatomic, readwrite, copy) NSString *coordHint;

/**
 * Provides read-write access to the seal
 */
@property (nonatomic, readwrite, copy) NSString *seal;

/**
 * Provides read-write access to the second factor key
 */
@property (nonatomic, readwrite, copy) NSString *secondFactorKey;

/**
 * Provides read-write access to the NXT application delegate
 */
@property (nonatomic, readonly, retain) NXT_Peru_iPhone_AppDelegate *appDelegate;

/**
 * Provides read-write access to thedelegate
 */
@property (nonatomic, readwrite, assign) id<FOInscriptionBaseProcessDelegate> delegate;

/**
 * Provides read-write access to the user selected phone number
 */
@property (nonatomic, readwrite, assign) NSInteger selectedPhoneNumberIndex;

/**
 * Provides read-write access to the selected carrier one index
 */
@property (nonatomic, readwrite, assign) NSInteger selectedEmailIndex;

/**
 * Provides read-write access to the send email flag
 */
@property (nonatomic, readwrite, assign) BOOL sendEmail;

/**
 * Provides read-write access to the send sms flag
 */
@property (nonatomic, readwrite, assign) BOOL sendSMS;

/**
 * Provides read-write access to the user selected email message
 */
@property (nonatomic, readwrite, copy) NSString *emailMessage;

/**
 * Provides read-write access to the user selected legalTermsAccepted
 */
@property (nonatomic, readwrite, assign) BOOL legalTermsAccepted;

/**
 * Provides read-write access to the legalTermsURL
 */
@property (nonatomic, readwrite, copy) NSString *legalTermsURL;

/**
 * Provides read-write access to message.
 */
@property (nonatomic, readwrite, copy) NSString *message;

/**
 * Provides read-write access to suggestion.
 */
@property (nonatomic, readwrite, retain) NSString *suggestion;


- (NSString *) inscriptionDataValidation;

- (void) startInscriptionDataRequest; // 2nd request

- (void) FOdataResponseReceived : (NSNotification *) notification;

- (NSString *) inscriptionSuccessDataValidation;

- (void) startInscriptionSuccessRequest;

- (void) FOInscriptionSuccessResponseReceived:(NSNotification *) notificarion;

- (FOTypeEnum) frequentOperationType;

- (BOOL) typeOTPKey ;

- (void) resetSMSAndEmailInformation;

@end
