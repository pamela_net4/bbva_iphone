//
//  FastLoanTerm.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"


@interface FastLoanTerm : StatusEnabledResponse{
    @private

   /**
     * FastLoanTerm term
     */
    NSString *term_;




}

/**
 * Provides read-only access to the FastLoanTerm term
 */
@property (nonatomic, readonly, copy) NSString * term;




@end

