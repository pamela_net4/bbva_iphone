/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <CoreLocation/CoreLocation.h>


/**
 * Encapsulates a CLLocationCoordinate2D structure so it can be stored in an NSArray and similar storing elements
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GeographicCoordinate : NSObject {

@private
	
	/**
	 * Private coordinates structure
	 */
	CLLocationCoordinate2D coordinate_;
}

/**
 * Provides read-write access to the coordinate structure
 */
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;

@end
