/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


/**
 * Transfer confirmation response additional information information
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferConfirmationAdditionalInformation : StatusEnabledResponse {

@private
    
    /**
     * Market place commission
     */
    NSString *marketPlaceCommision_;
    
    /**
     * Market place commission currency
     */
    NSString *marketPlaceCommisionCurrency_;
    
    /**
     * Coordinate
     */
    NSString *coordinate_;
    
    /**
     * Seal
     */
    NSString *seal_;
    
	/**
	 * Operation
	 */
    NSString *operation_;
	
    /**
	 * Holder
	 */
    NSString *holder_;
    
	/**
	 * Beneficiary
	 */
    NSString *beneficiary_;
	
	/**
	 * Beneficiary account
	 */
    NSString *beneficiaryAccount_;
	
	/**
	 * Beneficiary account currency
	 */
    NSString *beneficiaryAccountCurrency_;
	
	/**
	 * Disclaimer
	 */
    NSString *disclaimer_;
	
	/**
	 * Detination bank
	 */
	NSString *destinationBank_;
	
	/**
	 * Amount to transfer
	 */
	NSString *amountToTransfer_;
	
	/**
	 * Amount currency to transfer
	 */
	NSString *amountCurrencyToTransfer_;
	
	/**
	 * Commission
	 */
	NSString *commission_;
	
	/**
	 * Currency commission
	 */
	NSString *currencyCommission_;
	
	/**
	 * Network use
	 */
	NSString *networkUse_;
	
	/**
	 * Currency network use
	 */
	NSString *currencyNetworkUse_;
	
	/**
	 * Amount to pay
	 */
	NSString *amountToPay_;
	
	/**
	 * Amount currency to pay
	 */
	NSString *amountCurrencyToPay_;
	
	/**
	 * ITF message
	 */
	NSString *itfMessage_;
    
    /**
	 * ITF amount
	 */
	NSString *itfAmount_;
    
    /**
	 * ITF symbol
	 */
	NSString *itfSymbol_;
	
	/**
	 * Beneficiary document
	 */
	NSString *beneficiaryDocument_;
    
    /**
     * Beneficiary document
     */
    NSString *beneficiaryDocumentType_;
    
    /**
     * Beneficiary document
     */
    NSString *beneficiaryDocumentTypeId_;

    /**
     * Tipo cambio
     */
    NSString *tipoCambio_;
    
    /**
     * Exchange rate currency
     */
    NSString *monedaTipoCambio_;
    
    NSString *moduleActive_;
	
}

/**
 * Provides read-only access to market place commission currency
 */
@property (nonatomic, readonly, copy) NSString *marketPlaceCommisionCurrency;

/**
 * Provides read-only access to market place commission
 */
@property (nonatomic, readonly, copy) NSString *marketPlaceCommision;

/**
 * Provides read-only access to operation
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to holder
 */
@property (nonatomic, readonly, copy) NSString *holder;

/**
 * Provides read-only access to beneficiary
 */
@property (nonatomic, readonly, copy) NSString *beneficiary;

/**
 * Provides read-only access tobeneficiaryAccount
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryAccount;

/**
 * Provides read-only access to beneficiaryAccountCurrency
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryAccountCurrency;

/**
 * Provides read-only access to disclaimer
 */
@property (nonatomic, readonly, copy) NSString *disclaimer;

/**
 * Provides read-only access to coordinate
 */
@property (nonatomic, readonly, copy) NSString *coordinate;

/**
 * Provides read-only access to seal
 */
@property (nonatomic, readonly, copy) NSString *seal;


@property (nonatomic, readonly, copy) NSString *moduleActive;

/**
 * Provides read-only access to destinationBank
 */
@property (nonatomic, readonly, copy) NSString *destinationBank;

/**
 * Provides read-only access to amountToTransfer
 */
@property (nonatomic, readonly, copy) NSString *amountToTransfer;

/**
 * Provides read-only access to amountCurrencyToTransfer
 */
@property (nonatomic, readonly, copy) NSString *amountCurrencyToTransfer;

/**
 * Provides read-only access to commission
 */
@property (nonatomic, readonly, copy) NSString *commission;

/**
 * Provides read-only access to currencyCommission
 */
@property (nonatomic, readonly, copy) NSString *currencyCommission;

/**
 * Provides read-only access to networkUse
 */
@property (nonatomic, readonly, copy) NSString *networkUse;

/**
 * Provides read-only access to currencyNetworkUse
 */
@property (nonatomic, readonly, copy) NSString *currencyNetworkUse;

/**
 * Provides read-only access to amountToPay
 */
@property (nonatomic, readonly, copy) NSString *amountToPay;

/**
 * Provides read-only access to amountCurrencyToPay
 */
@property (nonatomic, readonly, copy) NSString *amountCurrencyToPay;

/**
 * Provides read-only access to itfMessage
 */
@property (nonatomic, readonly, copy) NSString *itfMessage;

/**
 * Provides read-only access to itfMessage
 */
@property (nonatomic, readonly, copy) NSString *itfAmount;

/**
 * Provides read-only access to itfMessage
 */
@property (nonatomic, readonly, copy) NSString *itfSymbol;

/**
 * Provides read-only access to beneficiary document
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryDocument;

/**
 * Provides read-only access to beneficiary document
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryDocumentType;

/**
 * Provides read-only access to beneficiary document
 */
@property (nonatomic, readonly, copy) NSString *beneficiaryDocumentTypeId;

/**
 * Provides read-only access to tipo cambio
 */
@property (nonatomic, readonly, copy) NSString *tipoCambio;

/**
 * Provides read-only access to the exchange rate currency
 */
@property (nonatomic, readonly, copy) NSString *monedaTipoCambio;

@end
