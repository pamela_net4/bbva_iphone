//
//  FastLoanSummary.m
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "FastLoanSummary.h"
#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     flseas_AnalyzingCustomer = serxeas_ElementsCount, //!<Analyzing the FastLoanSummary customer
     flseas_AnalyzingAccount , //!<Analyzing the FastLoanSummary account
     flseas_AnalyzingCurrencyAccount , //!<Analyzing the FastLoanSummary currencyAccount
     flseas_AnalyzingDayOfPay , //!<Analyzing the FastLoanSummary dayOfPay
     flseas_AnalyzingNumberOperation , //!<Analyzing the FastLoanSummary numberOperation
     flseas_AnalyzingQuotaType , //!<Analyzing the FastLoanSummary quotaType
     flseas_AnalyzingContract , //!<Analyzing the FastLoanSummary contract
     flseas_AnalyzingTea , //!<Analyzing the FastLoanSummary tea
     flseas_AnalyzingTcea , //!<Analyzing the FastLoanSummary tcea
     flseas_AnalyzingCurrencyAmount , //!<Analyzing the FastLoanSummary currencyAmount
     flseas_AnalyzingAmount , //!<Analyzing the FastLoanSummary amount
     flseas_AnalyzingTerm , //!<Analyzing the FastLoanSummary term
     flseas_AnalyzingCurrencyQuota , //!<Analyzing the FastLoanSummary currencyQuota
     flseas_AnalyzingAmountQuota , //!<Analyzing the FastLoanSummary amountQuota
     flseas_AnalyzingMessageNotification , //!<Analyzing the FastLoanSummary messageNotification
     flseas_AnalyzingDate , //!<Analyzing the FastLoanSummary date
     flseas_AnalyzingHour , //!<Analyzing the FastLoanSummary hour

} FastLoanSummaryXMLElementAnalyzerState;

@implementation FastLoanSummary

#pragma mark -
#pragma mark Properties

@synthesize customer = customer_;
@synthesize account = account_;
@synthesize currencyAccount = currencyAccount_;
@synthesize dayOfPay = dayOfPay_;
@synthesize numberOperation = numberOperation_;
@synthesize quotaType = quotaType_;
@synthesize contract = contract_;
@synthesize tea = tea_;
@synthesize tcea = tcea_;
@synthesize currencyAmount = currencyAmount_;
@synthesize amount = amount_;
@synthesize term = term_;
@synthesize currencyQuota = currencyQuota_;
@synthesize amountQuota = amountQuota_;
@synthesize messageNotification = messageNotification_;
@synthesize date = date_;
@synthesize hour = hour_;
;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [customer_ release];
    customer_ = nil;

    [account_ release];
    account_ = nil;

    [currencyAccount_ release];
    currencyAccount_ = nil;

    [dayOfPay_ release];
    dayOfPay_ = nil;

    [numberOperation_ release];
    numberOperation_ = nil;

    [quotaType_ release];
    quotaType_ = nil;

    [contract_ release];
    contract_ = nil;

    [tea_ release];
    tea_ = nil;

    [tcea_ release];
    tcea_ = nil;

    [currencyAmount_ release];
    currencyAmount_ = nil;

    [amount_ release];
    amount_ = nil;

    [term_ release];
    term_ = nil;

    [currencyQuota_ release];
    currencyQuota_ = nil;

    [amountQuota_ release];
    amountQuota_ = nil;

    [messageNotification_ release];
    messageNotification_ = nil;

    [date_ release];
    date_ = nil;

    [hour_ release];
    hour_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if(0==1){
                   //delete fix
        }        else if ([lname isEqualToString: @"nombrecliente"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingCustomer;


        }
        else if ([lname isEqualToString: @"cuenta"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAccount;


        }
        else if ([lname isEqualToString: @"monedacuenta"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingCurrencyAccount;


        }
        else if ([lname isEqualToString: @"diapago"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingDayOfPay;


        }
        else if ([lname isEqualToString: @"numerooperacion"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingNumberOperation;


        }
        else if ([lname isEqualToString: @"tipocuota"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingQuotaType;


        }
        else if ([lname isEqualToString: @"contrato"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingContract;


        }
        else if ([lname isEqualToString: @"tea"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingTea;


        }
        else if ([lname isEqualToString: @"tcea"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingTcea;


        }
        else if ([lname isEqualToString: @"monedamonto"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingCurrencyAmount;


        }
        else if ([lname isEqualToString: @"importemonto"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAmount;


        }
        else if ([lname isEqualToString: @"plazo"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingTerm;


        }
        else if ([lname isEqualToString: @"monedacuota"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingCurrencyQuota;


        }
        else if ([lname isEqualToString: @"importecuota"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAmountQuota;


        }
        else if ([lname isEqualToString: @"mensajenotificacion"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingMessageNotification;


        }
        else if ([lname isEqualToString: @"fecha"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingDate;


        }
        else if ([lname isEqualToString: @"hora"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingHour;


        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(0==1){
           //delete fix
        }        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingCustomer) {

            [customer_ release];
            customer_ = nil;
            customer_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingAccount) {

            [account_ release];
            account_ = nil;
            account_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingCurrencyAccount) {

            [currencyAccount_ release];
            currencyAccount_ = nil;
            currencyAccount_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingDayOfPay) {

            [dayOfPay_ release];
            dayOfPay_ = nil;
            dayOfPay_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingNumberOperation) {

            [numberOperation_ release];
            numberOperation_ = nil;
            numberOperation_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingQuotaType) {

            [quotaType_ release];
            quotaType_ = nil;
            quotaType_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingContract) {

            [contract_ release];
            contract_ = nil;
            contract_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingTea) {

            [tea_ release];
            tea_ = nil;
            tea_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingTcea) {

            [tcea_ release];
            tcea_ = nil;
            tcea_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingCurrencyAmount) {

            [currencyAmount_ release];
            currencyAmount_ = nil;
            currencyAmount_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingAmount) {

            [amount_ release];
            amount_ = nil;
            amount_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingTerm) {

            [term_ release];
            term_ = nil;
            term_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingCurrencyQuota) {

            [currencyQuota_ release];
            currencyQuota_ = nil;
            currencyQuota_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingAmountQuota) {

            [amountQuota_ release];
            amountQuota_ = nil;
            amountQuota_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingMessageNotification) {

            [messageNotification_ release];
            messageNotification_ = nil;
            messageNotification_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingDate) {

            [date_ release];
            date_ = nil;
            date_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingHour) {

            [hour_ release];
            hour_ = nil;
            hour_ = [elementString copyWithZone:self.zone];

        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

