/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class CardTransactionList;
@class CardTransactionsAdditionalInformation;


/**
 * Contains the card transaction response obtained from the server
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardTransactionsResponse : StatusEnabledResponse {
    
@private
    
    /**
     * Card transaction list
     */
    CardTransactionList *cardTransactionList_;
    
    /**
     * Card transactions additional information
     */
    CardTransactionsAdditionalInformation *additionalInformation_;
    
}


/**
 * Provides read-only access to the card transaction list
 */
@property (nonatomic, readonly, retain) CardTransactionList *cardTransactionList;

/**
 * Provides read-only access to the card transactions additional information
 */
@property (nonatomic, readonly, retain) CardTransactionsAdditionalInformation *additionalInformation;

@end
