//
//  SalaryAdvanceAffiliation.h
//  NXT_Peru_iPhone
//
//  Created by Flavio Franco Tunqui on 3/17/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
@class EmailInfoList;

@interface SalaryAdvanceAffiliation : StatusEnabledResponse {
@private
    
    /**
     * SalaryAdvanceReceive customer
     */
    NSString *customer_;
    
    /**
     * SalaryAdvanceReceive messageAmount
     */
    NSString *messageAmount_;
    
    /**
     * SalaryAdvanceReceive messageCommission
     */
    NSString *messageCommission_;
    
    /**
     * SalaryAdvanceReceive messageDayPay
     */
    NSString *messageDayPay_;
    
    /**
     * SalaryAdvanceReceive emailInfoLis
     */
    EmailInfoList *emailInfoList;
    
    /**
     * SalaryAdvanceReceive seal
     */
    NSString *seal_;
    
    /**
     * SalaryAdvanceReceive disclaimerContract
     */
    NSString *disclaimerContract_;
    
    /**
     * SalaryAdvanceReceive disclaimerContractWeb
     */
    NSString *disclaimerContractWeb_;
    
    /**
     * SalaryAdvanceReceive coordinate
     */
    NSString *coordinate_;
}

/**
 * Provides read-only access to the SalaryAdvanceReceive customer
 */
@property (nonatomic, readonly, copy) NSString * customer;

/**
 * Provides read-only access to the SalaryAdvanceReceive messageAmount
 */
@property (nonatomic, readonly, copy) NSString * messageAmount;

/**
 * Provides read-only access to the SalaryAdvanceReceive messageCommission
 */
@property (nonatomic, readonly, copy) NSString * messageCommission;

/**
 * Provides read-only access to the SalaryAdvanceReceive messageDayPay
 */
@property (nonatomic, readonly, copy) NSString * messageDayPay;

/**
 * Provides read-only access to the SalaryAdvanceReceive emailInfoList
 */
@property (nonatomic, readonly, copy) EmailInfoList * emailInfoList;

/**
 * Provides read-only access to the SalaryAdvanceReceive seal
 */
@property (nonatomic, readonly, copy) NSString * seal;

/**
 * Provides read-only access to the SalaryAdvanceReceive disclaimerContract
 */
@property (nonatomic, readonly, copy) NSString * disclaimerContract;

/**
 * Provides read-only access to the SalaryAdvanceReceive disclaimerContractWeb
 */
@property (nonatomic, readonly, copy) NSString * disclaimerContractWeb;

/**
 * Provides read-only access to the SalaryAdvanceReceive coordinate
 */
@property (nonatomic, readonly, copy) NSString * coordinate;

@end