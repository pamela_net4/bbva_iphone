/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class Card;
@class Transaction;


/**
 * Contains the list of Cards
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardList : StatusEnabledResponse {
@private
	
	/**
	 * Array containing the Card list
	 */
	NSMutableArray *cardList_;
	
	/**
	 * Auxiliar Card object for parsing
	 */
	Card *auxCard_;
    
}

/**	
 * Provides read-only access to the number of Cards stored
 */
@property (nonatomic, readonly, retain) NSArray *cardList;

/**	
 * Provides read access to the number of Cards stored
 */
@property (nonatomic, readonly, assign) NSUInteger cardCount;

/**
 * Returns the Card located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the Card is located
 *
 * @return Card located at the given position, or nil if position is not valid
 */
- (Card *)cardAtPosition:(NSUInteger)aPosition;

/**
 * Returns the Card with a Card id, or nil if not valid
 *
 * @param aCardNumber The Card id
 *
 * @return Card with this number, or nil if not valid
 */
- (Card *)cardFromCardNumber:(NSString *)aCardNumber;

/**
 * Returns the card with an card 8 terminate number, or nil if not valid
 *
 * @param  anAccountNumber The account number
 * @return BankAccount with this number, or nil if not valid
 */
- (Card *)cardFromCardTerminateNumber:(NSString *)anAccountNumber;

/**
 * Returns the Card position inside the list
 *
 * @param  aCard Card object which position we are looking for
 *
 * @return Card position inside the list
 */
- (NSUInteger)cardPosition:(Card *)aCard;

/**
 * Updates the Card list from another Card list
 *
 * @param  aCardList The CardList to update from
 */
- (void)updateFrom:(CardList *)aCardList;

/**
 * Remove the contained data
 */
- (void)removeData;

@end
