/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "CardList.h"
#import "Card.h"

@interface CardList (privateCategory)

/**
 * Clears the object data
 */
- (void)clearCardListData;

/**
 * Adds an Card at the end of the list
 *
 * @param  aCard The Card to add
 */
- (void)addCard:(Card *)aCard;

@end

#pragma mark -

/**
 * Enumeration for the state of the analysis
 */
typedef enum {
	aocxeas_AnalyzingArrayOfCard = serxeas_ElementsCount
} ArrayOfCardXMLElementAnalyzerState;

@implementation CardList

@dynamic cardCount;
@dynamic cardList;

#pragma mark -
#pragma mark List methods

/*	
 * Adds an Card at the end of the list
 */
- (void)addCard:(Card *)aCard {
	if (aCard != nil) {
		[cardList_ addObject: aCard];
	}
}

/*	
 * Updates the Card list from another Card list
 */
- (void)updateFrom:(CardList *)aCardList {

    NSArray *cards = aCardList.cardList;

	[self clearCardListData];

	if (cards != nil) {
		for (Card *card in cards) {
			[self addCard:card];
		}
	}
	
    [self updateFromStatusEnabledResponse:aCardList];
	
	self.informationUpdated = aCardList.informationUpdated;
	
}

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
	
    [self clearCardListData];
    
	[cardList_ release];
	cardList_ = nil;
	
	[auxCard_ release];
	auxCard_ = nil;
	
	[super dealloc];

}

/*
 * Clears the data of the object
 */
- (void)clearCardListData {
	
	[cardList_ removeAllObjects];
	
	[auxCard_ release];
	auxCard_ = nil;
	
	informationUpdated_ = soie_NoInfo;
	
}

/*
 * Remove object data the data of the object
 */
- (void)removeData {
	
    [self clearCardListData];
	
}

#pragma mark -
#pragma mark Initialization

/**	
 * Object initializer
 *
 * @return An initialized instance
 */
- (id) init {
	if (self = [super init]) {

		informationUpdated_ = soie_NoInfo;
		cardList_ = [[NSMutableArray alloc] init];
		
	}
	
	return self;
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Provides read access to the number of Card stored
 *
 * @return The count
 */
- (NSUInteger) cardCount {
	return [cardList_ count];
}

/*
 * Returns the Card located at the given position, or nil if position is not valid
 */
- (Card*) cardAtPosition: (NSUInteger) aPosition {
	Card* result = nil;
	
	if (aPosition < [cardList_ count]) {
		result = [cardList_ objectAtIndex: aPosition];
	}
	
	return result;
}

/*
 * Returns the Card with an Card id, or nil if not valid
 */
- (Card *)cardFromCardNumber:(NSString *)aCardNumber {

	Card *card = nil;
	 
	if (aCardNumber != nil) {
		Card *target;
		NSUInteger size = [cardList_ count];
		BOOL found = NO;
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
			target = [cardList_ objectAtIndex:i];
			if ([aCardNumber isEqualToString:target.cardNumber]) {
				card = target;
				found = YES;
			}
		}
	}
	 
	return card;

}

/*
 * Returns the card with an card 8 terminate number, or nil if not valid
 */
- (Card *)cardFromCardTerminateNumber:(NSString *)anAccountNumber {
	
	Card *result = nil;
	
	if (anAccountNumber != nil) {
        
		Card *target;
		NSUInteger size = [cardList_ count];
		BOOL found = NO;
        
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
            
			target = [cardList_ objectAtIndex:i];
            
			if ([anAccountNumber isEqualToString:[[target cardNumber] substringFromIndex:[[target cardNumber] length] - 4]]) {
                
				result = target;
				found = YES;
                
			}
            
		}
        
	}
	
	return result;
	
}

/*
 * Returns the Card position inside the list
 */
- (NSUInteger) cardPosition: (Card*) aCard {

	return [cardList_ indexOfObject:aCard];
	
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void) parserDidStartDocument: (NSXMLParser*) parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearCardListData];

}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void) parser: (NSXMLParser*) parser didStartElement: (NSString*) elementName namespaceURI: (NSString*) namespaceURI 
  qualifiedName: (NSString*) qualifiedName attributes: (NSDictionary*) attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (auxCard_ != nil) {
		[self addCard:auxCard_];
		[auxCard_ release];
		auxCard_ = nil;
	}

	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            xmlAnalysisCurrentValue_ = aocxeas_AnalyzingArrayOfCard;
			[auxCard_ release];
            auxCard_ = [[Card alloc] init];
            auxCard_.openingTag = lname;
            [auxCard_ setParentParseableObject:self];
            [parser setDelegate:auxCard_];
            [auxCard_ parserDidStartDocument: parser];
        } else {
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void) parser: (NSXMLParser*) parser didEndElement: (NSString*) elementName namespaceURI: (NSString*) namespaceURI qualifiedName: (NSString*) qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"listtarj"] || [lname isEqualToString: @"listado"] || [lname isEqualToString: @"listadotarjs"] || [lname isEqualToString: @"listadotarj"]) {
            
            if (auxCard_ != nil) { // Insertion of the last Card
                [self addCard:auxCard_];
                [auxCard_ release];
                auxCard_ = nil;
            }
                
            informationUpdated_ = soie_InfoAvailable;
            
            [parser setDelegate:parentParseableObject_];
			//[parentParseableObject_ release];
            parentParseableObject_ = nil;
        }
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
    }
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the cards list
 *
 * @return The cards list
 */
- (NSArray *)cardList {
    
    return [NSArray arrayWithArray:cardList_];
    
}

@end