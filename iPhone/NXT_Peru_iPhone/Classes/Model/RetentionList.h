/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward decarations
@class Retention;


/**
 * Contains the list of retentions obtained from the server
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface RetentionList : StatusEnabledResponse {
    
@private
	
	/**
	 * Retentions array list
	 */
	NSMutableArray *retentionsList_;
    
	/**
	 * Auxiliar retention instance for parsing
	 */
	Retention *auxRetention_;
    
}

/**
 * Provides read-only access to the accounts count
 */
@property (nonatomic, readonly, assign) NSUInteger retentionsCount;

/**
 * Provides read-only access to the retentions array list
 */
@property (nonatomic, readonly, retain) NSArray *retentionsList;


/**
 * Updates the retention list from another retention list
 *
 * @param aRetentionList The RetentionList instance to update from
 */
- (void)updateFrom:(RetentionList *)aRetentionList;

/**
 * Remove the contained data
 */
- (void)removeData;


/**
 * Returns the Retention located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the Retention is located at
 * @return Retention located at the given position, or nil if position is not valid
 */
- (Retention *)retentionAtPosition:(NSUInteger)aPosition;
@end
