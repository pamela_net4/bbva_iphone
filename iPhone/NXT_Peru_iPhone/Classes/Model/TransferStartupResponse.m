/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "TransferStartupResponse.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    mcxeas_AnalyzingCode = serxeas_ElementsCount, //!<Analyzing the additional information
    mcxeas_AnalyzingDescription
    
} MobileCarrierXMLElementAnalyzerState;

@implementation MobileCarrier

@synthesize code = code_;
@synthesize description = description_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    [code_ release];
    code_ = nil;
    
    [description_ release];
    description_ = nil;
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a TransferSuccessResponse instance providing the associated notification
 *
 * @return The initialized TransferSuccessResponse instance
 */
- (id)init {
    
    if ((self = [super init])) {        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    [code_ release];
    code_ = [[NSMutableString alloc] initWithString:@""];
    
    [description_ release];
    description_ = [[NSMutableString alloc] initWithString:@""];
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    NSString* lname = [elementName lowercaseString];
    
    if ([lname isEqualToString: @"codigo"]) {
        xmlAnalysisCurrentValue_ = mcxeas_AnalyzingCode;
    } else if ([lname isEqualToString: @"descripcion"]) {
        xmlAnalysisCurrentValue_ = mcxeas_AnalyzingDescription;
    } else {
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if ([lname isEqualToString: @"e"]) {
        
        informationUpdated_ = soie_InfoAvailable;
        [parser setDelegate:parentParseableObject_];
        parentParseableObject_ = nil;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
}

/**
 * Sent by a parser object to provide its delegate with a string representing all or part of the characters of the current element.
 * Characters found are appended to the current element being analyzed
 *
 * @param parser A parser object
 * @param string A string representing the complete or partial textual content of the current element
 */
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    [super parser:parser foundCharacters:string];
    
    if ([string length] > 0) {
        
        switch (xmlAnalysisCurrentValue_) {
            case mcxeas_AnalyzingCode:                
                [code_ appendString:string];
                break;
            case mcxeas_AnalyzingDescription:                
                [description_ appendString:string];
                break;
            default:
                break;
        }
    }
}

@end

#pragma mark -

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    dxeas_AnalyzingCode = serxeas_ElementsCount, //!<Analyzing the additional information
    dxeas_AnalyzingDescription,
    dxeas_AnalyzingIsDefault
    
} DocumentXMLElementAnalyzerState;

@implementation Document

@synthesize code = code_;
@synthesize description = description_;
@synthesize isDefault = isDefault_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
	
    [code_ release];
    code_ = nil;
    
    [description_ release];
    description_ = nil;
	
    [isDefault_ release];
    isDefault_ = nil;
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a TransferSuccessResponse instance providing the associated notification
 *
 * @return The initialized TransferSuccessResponse instance
 */
- (id)init {
    
    if ((self = [super init])) {        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    [code_ release];
    code_ = [[NSMutableString alloc] initWithString:@""];
    
    [description_ release];
    description_ = [[NSMutableString alloc] initWithString:@""];
    
    [isDefault_ release];
    isDefault_ = [[NSMutableString alloc] initWithString:@""];

}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    NSString* lname = [elementName lowercaseString];
    
    if ([lname isEqualToString: @"codigodocumento"]) {
        xmlAnalysisCurrentValue_ = dxeas_AnalyzingCode;
    } else if ([lname isEqualToString: @"descripciondocumento"]) {
        xmlAnalysisCurrentValue_ = dxeas_AnalyzingDescription;
    } else if ([lname isEqualToString: @"valordefecto"]) {
        xmlAnalysisCurrentValue_ = dxeas_AnalyzingIsDefault;
    } else {
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if ([lname isEqualToString: @"e"]) {
        
        informationUpdated_ = soie_InfoAvailable;
        [parser setDelegate:parentParseableObject_];
        parentParseableObject_ = nil;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
}

/**
 * Sent by a parser object to provide its delegate with a string representing all or part of the characters of the current element.
 * Characters found are appended to the current element being analyzed
 *
 * @param parser A parser object
 * @param string A string representing the complete or partial textual content of the current element
 */
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    [super parser:parser foundCharacters:string];
    
    if ([string length] > 0) {
        
        switch (xmlAnalysisCurrentValue_) {
            case dxeas_AnalyzingCode:                
                [code_ appendString:string];
                break;
            case dxeas_AnalyzingDescription:                
                [description_ appendString:string];
                break;
            case dxeas_AnalyzingIsDefault:                
                [isDefault_ appendString:string];
                break;
            default:
                break;
        }
    }
}

@end

#pragma mark -

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    tttarrxeas_AnalyzingAmount = serxeas_ElementsCount, //!<Analyzing the additional information
    tttarrxeas_AnalyzingMobileCarrier,
    tttarrxeas_AnalyzingDocument,
    tttarrxeas_AnalyzingAccount,
    tttarrxeas_AnalyzingMessage
    
} TransferToThirdAccountsStartupResponseXMLElementAnalyzerState;

/**
 * TransferSuccessResponse private category
 */
@interface TransferStartupResponse()

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearTransferToThirdAccountsStartupResponseData;

@end


#pragma mark -

@implementation TransferStartupResponse

#pragma mark -
#pragma mark Properties

@synthesize amounts = amounts_;
@synthesize mobileCarriers = mobileCarriers_;
@synthesize documents = documents_;
@synthesize message = message_;
@synthesize accounts = accounts_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearTransferToThirdAccountsStartupResponseData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a TransferSuccessResponse instance providing the associated notification
 *
 * @return The initialized TransferSuccessResponse instance
 */
- (id)init {
    
    if ((self = [super init])) {
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    analyzingMobileCarriers_ = NO;
    
    [amounts_ release];
    amounts_ = [[NSMutableArray alloc] init];
    
    [auxAmount_ release];
    auxAmount_ = [[NSMutableString alloc] initWithString:@""];
    
    [mobileCarriers_ release];
    mobileCarriers_ = [[NSMutableArray alloc] init];
    
    [auxDocument_ release];
    auxDocument_ = nil;
    
    [documents_ release];
    documents_ = [[NSMutableArray alloc] init];
    
    [auxDocument_ release];
    auxDocument_ = nil;
    
    [message_ release];
    message_ = nil;
    
    [accounts_ release];
    accounts_ = [[NSMutableArray alloc] init];
    
    [auxAccount_ release];
    auxAccount_ = nil;

}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if ([auxAmount_ length] > 0) {
        [amounts_ addObject:auxAmount_];
        [auxAmount_ release];
        auxAmount_ = [[NSMutableString alloc] initWithString:@""];
    }
    
    if (auxMobileCarrier_ != nil) {
        [mobileCarriers_ addObject:auxMobileCarrier_];
        [auxMobileCarrier_ release];
        auxMobileCarrier_ = nil;
    }
    
    if (auxDocument_ != nil) {
        [documents_ addObject:auxDocument_];
        [auxDocument_ release];
        auxDocument_ = nil;
    }
    
    if (auxAccount_ != nil) {
        [accounts_ addObject:auxAccount_];
        [auxAccount_ release];
        auxAccount_ = nil;
    }
    
    NSString* lname = [elementName lowercaseString];
    
    if ([lname isEqualToString: @"importes"]) {
        xmlAnalysisCurrentValue_ = tttarrxeas_AnalyzingAmount;
        analyzingMobileCarriers_ = NO;
        analyzingDocuments_ = NO;
        analyzingAccounts_ = NO;
    } else if ([lname isEqualToString: @"operadoras"]) {
        analyzingMobileCarriers_ = YES;
        analyzingDocuments_ = NO;
        analyzingAccounts_ = NO;
    } else if ([lname isEqualToString: @"documento"]) {
        analyzingDocuments_ = YES;
        analyzingMobileCarriers_ = NO;
        analyzingAccounts_ = NO;
    } else if ([lname isEqualToString: @"mensaje"]) {
        xmlAnalysisCurrentValue_ = tttarrxeas_AnalyzingMessage;
    } else if ([lname isEqualToString: @"cuentas"]) {
        analyzingAccounts_ = YES;
        analyzingMobileCarriers_ = NO;
        analyzingDocuments_ = NO;
    } else if ([lname isEqualToString: @"e"] && analyzingMobileCarriers_) {
        
        xmlAnalysisCurrentValue_ = tttarrxeas_AnalyzingMobileCarrier;
        
        [auxMobileCarrier_ release];
        auxMobileCarrier_ = [[MobileCarrier alloc] init];
        auxMobileCarrier_.openingTag = lname;
        [auxMobileCarrier_ setParentParseableObject:self];
        [parser setDelegate:auxMobileCarrier_];
        [auxMobileCarrier_ parserDidStartDocument:parser];
        
    } else if ([lname isEqualToString: @"e"] && analyzingDocuments_) {
        
        xmlAnalysisCurrentValue_ = tttarrxeas_AnalyzingDocument;
        
        [auxDocument_ release];
        auxDocument_ = [[Document alloc] init];
        auxDocument_.openingTag = lname;
        [auxDocument_ setParentParseableObject:self];
        [parser setDelegate:auxDocument_];
        [auxDocument_ parserDidStartDocument:parser];
        
    } else if ([lname isEqualToString: @"e"] && analyzingAccounts_) {
        
        xmlAnalysisCurrentValue_ = tttarrxeas_AnalyzingAccount;
        
        [auxAccount_ release];
        auxAccount_ = [[BankAccount alloc] init];
        auxAccount_.openingTag = lname;
        [auxAccount_ setParentParseableObject:self];
        [parser setDelegate:auxAccount_];
        [auxAccount_ parserDidStartDocument:parser];
        
    } else {
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
    }

}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
            
    if ([lname isEqualToString: @"msg-s"]) {
        
        if ([auxAmount_ length] > 0) {
            [amounts_ addObject:auxAmount_];
            [auxAmount_ release];
            auxAmount_ = nil;
        }
        
        if (auxMobileCarrier_ != nil) {
            [mobileCarriers_ addObject:auxMobileCarrier_];
            [auxMobileCarrier_ release];
            auxMobileCarrier_ = nil;
        }
        
        if (auxDocument_ != nil) {
            [documents_ addObject:auxDocument_];
            [auxDocument_ release];
            auxDocument_ = nil;
        }
        
        if (auxAccount_ != nil) {
            [accounts_ addObject:auxAccount_];
            [auxAccount_ release];
            auxAccount_ = nil;
        }
        
        if (xmlAnalysisCurrentValue_ == tttarrxeas_AnalyzingMessage) {
            
            NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            [message_ release];
            message_ = nil;
            message_ = [elementString copyWithZone:self.zone];
            
        } 
        
        informationUpdated_ = soie_InfoAvailable;
        [parser setDelegate:parentParseableObject_];
        parentParseableObject_ = nil;
        
    }
        
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
}

/**
 * Sent by a parser object to provide its delegate with a string representing all or part of the characters of the current element.
 * Characters found are appended to the current element being analyzed
 *
 * @param parser A parser object
 * @param string A string representing the complete or partial textual content of the current element
 */
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    [super parser:parser foundCharacters:string];
    
    if ([string length] > 0) {
        
        switch (xmlAnalysisCurrentValue_) {
            case tttarrxeas_AnalyzingAmount:                
                [auxAmount_ appendString:string];
                break;
            default:
                break;
        }

    }

}

/*
 * Clears the object data
 */
- (void)clearTransferToThirdAccountsStartupResponseData {
    
    [amounts_ release];
    amounts_ = nil;
    
    [auxAmount_ release];
    auxAmount_ = nil;
    
    [mobileCarriers_ release];
    mobileCarriers_ = nil;
    
    [auxMobileCarrier_ release];
    auxMobileCarrier_ = nil;
    
    [documents_ release];
    documents_ = nil;
    
    [auxDocument_ release];
    auxDocument_ = nil;
    
    [message_ release];
    message_ = nil;

}

@end
