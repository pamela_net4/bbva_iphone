//
//  IncrementCreditLineStartUpResponse.m
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "IncrementCreditLineStartUpResponse.h"
#import "Tools.h"
#import "EmailInfoList.h"
#import "CardForIncrementList.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     iclsureas_AnalyzingAmountOffer = serxeas_ElementsCount, //!<Analyzing the IncrementCreditLineStartUpResponse amountOffer
     iclsureas_AnalyzingEmailInfoList , //!<Analyzing the IncrementCreditLineStartUpResponse emailInfoList
     iclsureas_AnalyzingCardForIncrementList , //!<Analyzing the IncrementCreditLineStartUpResponse cardForIncrementList

} IncrementCreditLineStartUpResponseXMLElementAnalyzerState;

@implementation IncrementCreditLineStartUpResponse

#pragma mark -
#pragma mark Properties

@synthesize amountOffer = amountOffer_;
@synthesize emailInfoList = emailInfoList_;
@synthesize cardForIncrementList = cardForIncrementList_;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [amountOffer_ release];
    amountOffer_ = nil;

    [emailInfoList_ release];
    emailInfoList_ = nil;

    [cardForIncrementList_ release];
    cardForIncrementList_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

         if ([lname isEqualToString: @"montooferta"]) {

            xmlAnalysisCurrentValue_ = iclsureas_AnalyzingAmountOffer;


        }
        else if ([lname isEqualToString: @"listacorreo"]) {

            xmlAnalysisCurrentValue_ = iclsureas_AnalyzingEmailInfoList;
            [emailInfoList_ release];
            emailInfoList_ = nil;
            emailInfoList_ = [[EmailInfoList alloc] init];
            emailInfoList_.openingTag = lname;
            [emailInfoList_ setParentParseableObject:self];
            [parser setDelegate:emailInfoList_];
            [emailInfoList_ parserDidStartDocument:parser];


        }
        else if ([lname isEqualToString: @"listatarjeta"]) {

            xmlAnalysisCurrentValue_ = iclsureas_AnalyzingCardForIncrementList;
            [cardForIncrementList_ release];
            cardForIncrementList_ = nil;
            cardForIncrementList_ = [[CardForIncrementList alloc] init];
            cardForIncrementList_.openingTag = lname;
            [cardForIncrementList_ setParentParseableObject:self];
            [parser setDelegate:cardForIncrementList_];
            [cardForIncrementList_ parserDidStartDocument:parser];


        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == iclsureas_AnalyzingAmountOffer) {

            [amountOffer_ release];
            amountOffer_ = nil;
            amountOffer_ = [elementString copyWithZone:self.zone];

        }
        
    }
    else if ([lname isEqualToString: @"msg-s"]) {
        
        informationUpdated_ = soie_InfoAvailable;
        [parser setDelegate:parentParseableObject_];
        parentParseableObject_ = nil;
        
    }
    else {
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

