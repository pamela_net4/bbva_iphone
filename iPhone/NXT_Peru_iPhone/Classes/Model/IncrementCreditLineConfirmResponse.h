//
//  IncrementCreditLineConfirmResponse.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"


@interface IncrementCreditLineConfirmResponse : StatusEnabledResponse{
    @private

   /**
     * IncrementCreditLineConfirmResponse clientName
     */
    NSString *clientName_;


   /**
     * IncrementCreditLineConfirmResponse cardNumber
     */
    NSString *cardNumber_;


   /**
     * IncrementCreditLineConfirmResponse cardName
     */
    NSString *cardName_;


   /**
     * IncrementCreditLineConfirmResponse email
     */
    NSString *email_;


   /**
     * IncrementCreditLineConfirmResponse amountIncrement
     */
    NSString *amountIncrement_;


   /**
     * IncrementCreditLineConfirmResponse currency
     */
    NSString *currency_;


   /**
     * IncrementCreditLineConfirmResponse otpText
     */
    NSString *otpText_;


   /**
     * IncrementCreditLineConfirmResponse seal
     */
    NSString *seal_;


   /**
     * IncrementCreditLineConfirmResponse coordinate
     */
    NSString *coordinate_;




}

/**
 * Provides read-only access to the IncrementCreditLineConfirmResponse clientName
 */
@property (nonatomic, readonly, copy) NSString * clientName;


/**
 * Provides read-only access to the IncrementCreditLineConfirmResponse cardNumber
 */
@property (nonatomic, readonly, copy) NSString * cardNumber;


/**
 * Provides read-only access to the IncrementCreditLineConfirmResponse cardName
 */
@property (nonatomic, readonly, copy) NSString * cardName;


/**
 * Provides read-only access to the IncrementCreditLineConfirmResponse email
 */
@property (nonatomic, readonly, copy) NSString * email;


/**
 * Provides read-only access to the IncrementCreditLineConfirmResponse amountIncrement
 */
@property (nonatomic, readonly, copy) NSString * amountIncrement;


/**
 * Provides read-only access to the IncrementCreditLineConfirmResponse currency
 */
@property (nonatomic, readonly, copy) NSString * currency;


/**
 * Provides read-only access to the IncrementCreditLineConfirmResponse otpText
 */
@property (nonatomic, readonly, copy) NSString * otpText;


/**
 * Provides read-only access to the IncrementCreditLineConfirmResponse seal
 */
@property (nonatomic, readonly, copy) NSString * seal;


/**
 * Provides read-only access to the IncrementCreditLineConfirmResponse coordinate
 */
@property (nonatomic, readonly, copy) NSString * coordinate;




@end

