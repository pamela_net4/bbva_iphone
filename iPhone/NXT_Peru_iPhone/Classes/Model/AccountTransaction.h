/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"

@class AccountTransactionDetail;

/**
 * Stores one account transaction obtained from the server
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountTransaction : StatusEnabledResponse {

@private
    
    /**
     * Operation date string
     */
    NSString *operationDateString_;
    
    /**
     * Operation date
     */
    NSDate *operationDate_;
    
    /**
     * Value date string
     */
    NSString *valueDateString_;
    
    /**
     * Value date
     */
    NSDate *valueDate_;
    
    /**
     * Description
     */
    NSString *description_;
    
    /**
     * Currency
     */
    NSString *currency_;
    
    /**
     * Amount string
     */
    NSString *amountString_;
    
    /**
     * Amount
     */
    NSDecimalNumber *amount_;
    
    /**
     * ITF
     */
    NSString *ITF_;
    
    /**
     * Transaction number
     */
    NSString *number_;
    
    /**
     * Subject
     */
    NSString *subject_;
    
    /**
     * Detail downloaded flag
     */
    BOOL detailDownloaded_;
    
    /**
     * Description
     */
    NSString *deatilDescription_;
    
    /**
     * Operation hour
     */
    NSString *operationHour_;    
    
    /**
     * Accounting date string
     */
    NSString *accountingDateString_;
    
    /**
     * Accounting date
     */
    NSDate *accountingDate_;
    
    /**
     * Check number
     */
    NSString *checkNumber_;
    
    /**
     * Type
     */
    NSString *type_;
    
    /**
     * Center
     */
    NSString *center_;
    
    /**
     * Literal
     */
    NSString *literal_;
    
}


/**
 * Provides read-only access to the operation date string
 */
@property (nonatomic, readonly, copy) NSString *operationDateString;

/**
 * Provides read-only access to the operation date
 */
@property (nonatomic, readonly, copy) NSDate *operationDate;

/**
 * Provides read-only access to the value date string
 */
@property (nonatomic, readonly, copy) NSString *valueDateString;

/**
 * Provides read-only access to the value date
 */
@property (nonatomic, readonly, copy) NSDate *valueDate;

/**
 * Provides read-only access to the description
 */
@property (nonatomic, readonly, copy) NSString *description;

/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the amount string
 */
@property (nonatomic, readonly, copy) NSString *amountString;

/**
 * Provides read-only access to the amount
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *amount;

/**
 * Provides read-only access to the ITF
 */
@property (nonatomic, readonly, copy) NSString *ITF;

/**
 * Provides read-only access to the transaction number
 */
@property (nonatomic, readonly, copy) NSString *number;

/**
 * Provides read-only access to the subject
 */
@property (nonatomic, readonly, copy) NSString *subject;

/**
 * Provides read-only access to the detail
 */
@property (nonatomic, readwrite, assign) BOOL detailDownloaded;

/**
 * Provides read-write access to the description
 */
@property (nonatomic, readwrite, copy) NSString *deatilDescription;

/**
 * Provides read-write access to the operationHour
 */
@property (nonatomic, readwrite, copy) NSString *operationHour;    

/**
 * Provides read-write access to the accountingDateString
 */
@property (nonatomic, readwrite, copy) NSString *accountingDateString;

/**
 * Provides read-write access to the accounting date
 */
@property (nonatomic, readwrite, copy) NSDate *accountingDate;

/**
 * Provides read-write access to the checkNumber
 */
@property (nonatomic, readwrite, copy) NSString *checkNumber;

/**
 * Provides read-write access to the type
 */
@property (nonatomic, readwrite, copy) NSString *type;

/**
 * Provides read-write access to the center
 */
@property (nonatomic, readwrite, copy) NSString *center;

/**
 * Provides read-write access to the literal
 */
@property (nonatomic, readwrite, copy) NSString *literal;

/**
 * Updates this instance with the given account transaction
 *
 * @param anAccountTransaction The account transaction to update from
 */
- (void)updateFrom:(AccountTransaction *)anAccountTransaction;

/**
 * Compares the account transaction with another to produce an ordered list where older transactions are the last items
 *
 * @param anAccountTransaction The account transaction to compare with
 * @return NSOrderedAscending when current account transaction is later than argument, NSOrderedDescending when current
 * account transaction is earlier than argument, NSOrderedSame otherwise
 */
- (NSComparisonResult)compareForDescendingDateOrdering:(AccountTransaction *)anAccountTransaction;

@end
