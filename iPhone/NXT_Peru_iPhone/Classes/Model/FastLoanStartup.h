//
//  FastLoanStartup.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
@class FastLoanTermList;
@class AccountFastLoanList;
@class EmailInfoList;


@interface FastLoanStartup : StatusEnabledResponse{
    @private

   /**
     * FastLoanStartup currency
     */
    NSString *currency_;


   /**
     * FastLoanStartup currencySymbol
     */
    NSString *currencySymbol_;


   /**
     * FastLoanStartup amount
     */
    NSString *amount_;


   /**
     * FastLoanStartup amountFormated
     */
    NSString *amountFormated_;


   /**
     * FastLoanStartup amountMin
     */
    NSString *amountMin_;


   /**
     * FastLoanStartup amountMinFormated
     */
    NSString *amountMinFormated_;


   /**
     * FastLoanStartup amountMax
     */
    NSString *amountMax_;


   /**
     * FastLoanStartup amountMaxFormated
     */
    NSString *amountMaxFormated_;


   /**
     * FastLoanStartup amountCommission
     */
    NSString *amountCommission_;


   /**
     * FastLoanStartup tea
     */
    NSString *tea_;


   /**
     * FastLoanStartup teaFormated
     */
    NSString *teaFormated_;


   /**
     * FastLoanStartup tceaFormated
     */
    NSString *tceaFormated_;


   /**
     * FastLoanStartup term
     */
    NSString *term_;


   /**
     * FastLoanStartup termMax
     */
    NSString *termMax_;


   /**
     * FastLoanStartup termMin
     */
    NSString *termMin_;


   /**
     * FastLoanStartup dayOfPay
     */
    NSString *dayOfPay_;


   /**
     * FastLoanStartup quotaFormated
     */
    NSString *quotaFormated_;


   /**
     * FastLoanStartup quotaType
     */
    NSString *quotaType_;


   /**
     * FastLoanStartup fastLoanTermList
     */
    FastLoanTermList *fastLoanTermList_;


   /**
     * FastLoanStartup accountFastLoanList
     */
    AccountFastLoanList *accountFastLoanList_;


   /**
     * FastLoanStartup emailInfoList
     */
    EmailInfoList *emailInfoList_;




}

/**
 * Provides read-only access to the FastLoanStartup currency
 */
@property (nonatomic, readonly, copy) NSString * currency;


/**
 * Provides read-only access to the FastLoanStartup currencySymbol
 */
@property (nonatomic, readonly, copy) NSString * currencySymbol;


/**
 * Provides read-only access to the FastLoanStartup amount
 */
@property (nonatomic, readonly, copy) NSString * amount;


/**
 * Provides read-only access to the FastLoanStartup amountFormated
 */
@property (nonatomic, readonly, copy) NSString * amountFormated;


/**
 * Provides read-only access to the FastLoanStartup amountMin
 */
@property (nonatomic, readonly, copy) NSString * amountMin;


/**
 * Provides read-only access to the FastLoanStartup amountMinFormated
 */
@property (nonatomic, readonly, copy) NSString * amountMinFormated;


/**
 * Provides read-only access to the FastLoanStartup amountMax
 */
@property (nonatomic, readonly, copy) NSString * amountMax;


/**
 * Provides read-only access to the FastLoanStartup amountMaxFormated
 */
@property (nonatomic, readonly, copy) NSString * amountMaxFormated;


/**
 * Provides read-only access to the FastLoanStartup amountCommission
 */
@property (nonatomic, readonly, copy) NSString * amountCommission;


/**
 * Provides read-only access to the FastLoanStartup tea
 */
@property (nonatomic, readonly, copy) NSString * tea;


/**
 * Provides read-only access to the FastLoanStartup teaFormated
 */
@property (nonatomic, readonly, copy) NSString * teaFormated;


/**
 * Provides read-only access to the FastLoanStartup tceaFormated
 */
@property (nonatomic, readonly, copy) NSString * tceaFormated;


/**
 * Provides read-only access to the FastLoanStartup term
 */
@property (nonatomic, readonly, copy) NSString * term;


/**
 * Provides read-only access to the FastLoanStartup termMax
 */
@property (nonatomic, readonly, copy) NSString * termMax;


/**
 * Provides read-only access to the FastLoanStartup termMin
 */
@property (nonatomic, readonly, copy) NSString * termMin;


/**
 * Provides read-only access to the FastLoanStartup dayOfPay
 */
@property (nonatomic, readonly, copy) NSString * dayOfPay;


/**
 * Provides read-only access to the FastLoanStartup quotaFormated
 */
@property (nonatomic, readonly, copy) NSString * quotaFormated;


/**
 * Provides read-only access to the FastLoanStartup quotaType
 */
@property (nonatomic, readonly, copy) NSString * quotaType;


/**
 * Provides read-only access to the FastLoanStartup fastLoanTermList
 */
@property (nonatomic, readwrite, retain) FastLoanTermList * fastLoanTermList;


/**
 * Provides read-only access to the FastLoanStartup accountFastLoanList
 */
@property (nonatomic, readonly, copy) AccountFastLoanList * accountFastLoanList;


/**
 * Provides read-only access to the FastLoanStartup emailInfoList
 */
@property (nonatomic, readonly, copy) EmailInfoList * emailInfoList;




@end

