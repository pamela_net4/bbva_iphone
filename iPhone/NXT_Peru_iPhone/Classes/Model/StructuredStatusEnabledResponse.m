/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StructuredStatusEnabledResponse.h"

#import "BaseXMLParserObject.h"


/*
 * Defines the error element tag
 */
NSString * const kStructuredStatusEnabledResponseErrorElement = @"ERROR";

/*
 * Defines the err-s element tag
 */
NSString * const kStructuredStatusEnabledResponseErrElement = @"ERR-S";

/*
 * Defines the cod element tag
 */
NSString * const kStructuredStatusEnabledResponseCodElement = @"COD";

/**
 * Defines the codigoerror element tag
 */
NSString * const kStructuredStatusEnabledResponseCodigoErrorElement = @"CODIGOERROR";

/*
 * Defines the message element tag
 */
NSString * const kStructuredStatusEnabledResponseMessageElement = @"MENSAJE";

/*
 * Defines the msg element tag
 */
NSString * const kStructuredStatusEnabledResponseMsgElement = @"MSG";

/*
 * Defines the cu element tag
 */
NSString * const kStructuredStatusEnabledResponseCuElement = @"CU";

@implementation StructuredStatusEnabledResponse


#pragma mark -
#pragma mark Properties

@synthesize errorCode = errorCode_;
@synthesize errorMessage = errorMessage_;
@synthesize isError = isError_;
@synthesize isLoginError = isLoginError_;


#pragma mark -
#pragma mark BaseXMLParserObject selectors

/**
 * Asks the subclasses for an element's type. The parser will perform different actions depending on the element type. Subclasses must override this selector
 * to provide the base class with the addecuate information. Returns BXPOElementIdle for the response element, BXPOElementCustom for the rest of known elements
 * and BXPOElementUnknown otherwise
 *
 * @param element The element to analyze
 * @param namespaceURI The element namespace URI
 * @return The element's type
 */
- (BXPOElementType)elementType:(NSString *)element
                    namespaceURI:(NSString *)namespaceURI {
    
    BXPOElementType result = [super elementType:element
                                   namespaceURI:namespaceURI];
    
    if (result == BXPOElementUnknown) {
        
        if (([element isEqualToString:kStructuredStatusEnabledResponseCodElement]) ||
            ([element isEqualToString:kStructuredStatusEnabledResponseMessageElement]) ||
            ([element isEqualToString:kStructuredStatusEnabledResponseMsgElement]) ||
            ([element isEqualToString:kStructuredStatusEnabledResponseCodigoErrorElement])) {
                
            result = BXPOElementString;
            
        } else if (([element isEqualToString:kStructuredStatusEnabledResponseErrorElement]) ||
                   ([element isEqualToString:kStructuredStatusEnabledResponseErrElement])) {
            
            result = BXPOElementIdle;
            isError_ = YES;

        }else if ([element isEqualToString:kStructuredStatusEnabledResponseCuElement]) {
            
            result = BXPOElementIdle;
            
        }
    }
 
    return result;
    
}

#pragma mark -
#pragma mark Properties selectors

/**
 * Returns the error code
 *
 * @return The errorCode
 */
- (NSString *)errorCode {
    
    NSString *result = nil;
    
    if (isError_) { 
        
        result = (NSString *)[self objectForElement:kStructuredStatusEnabledResponseCodElement
                                       namespaceURI:@""
                                            atIndex:0];
        
        if (result == nil || [result isEqualToString:@""]) {
        
            result = (NSString *)[self objectForElement:kStructuredStatusEnabledResponseCodigoErrorElement
                                           namespaceURI:@""
                                                atIndex:0];
        
        }
    
    }
    
    return result;
}

/**
 * Returns the error code
 *
 * @return The errorCode
 */
- (NSString *)errorMessage {
    
    NSString *result = nil;
    
    if (isError_) {
        
        result = (NSString *)[self objectForElement:kStructuredStatusEnabledResponseMsgElement
                                                 namespaceURI:@""
                                                      atIndex:0];
        
        if (result == nil || [result isEqualToString:@""]) {
            
            result = (NSString *)[self objectForElement:kStructuredStatusEnabledResponseMessageElement
                                           namespaceURI:@""
                                                atIndex:0];
            
        }
    
    }
    
    return result;
}

@end

