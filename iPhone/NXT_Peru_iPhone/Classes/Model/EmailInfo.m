//
//  EmailInfo.m
//  NXT_Peru_iPhone
//
//  Created by Estefany on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "EmailInfo.h"
#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     eieas_AnalyzingPosition = serxeas_ElementsCount, //!<Analyzing the EmailInfo position
     eieas_AnalyzingEmail , //!<Analyzing the EmailInfo email
     eieas_AnalyzingEmailType , //!<Analyzing the EmailInfo emailType

} EmailInfoXMLElementAnalyzerState;

@implementation EmailInfo

#pragma mark -
#pragma mark Properties

@synthesize position = position_;
@synthesize email = email_;
@synthesize emailType = emailType_;
;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [position_ release];
    position_ = nil;

    [email_ release];
    email_ = nil;

    [emailType_ release];
    emailType_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if(0==1){
                   //delete fix
        }        else if ([lname isEqualToString: @"num"]) {

            xmlAnalysisCurrentValue_ = eieas_AnalyzingPosition;

        }
        else if ([lname isEqualToString: @"email"]) {

            xmlAnalysisCurrentValue_ = eieas_AnalyzingEmail;

        }
        else if ([lname isEqualToString: @"tipoemail"]) {

            xmlAnalysisCurrentValue_ = eieas_AnalyzingEmailType;

        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(0==1){
           //delete fix
        }        else if (xmlAnalysisCurrentValue_ == eieas_AnalyzingPosition) {

            [position_ release];
            position_ = nil;
            position_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == eieas_AnalyzingEmail) {

            [email_ release];
            email_ = nil;
            email_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == eieas_AnalyzingEmailType) {

            [emailType_ release];
            emailType_ = nil;
            emailType_ = [elementString copyWithZone:self.zone];

        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

