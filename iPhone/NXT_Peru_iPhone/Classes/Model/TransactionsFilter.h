/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <Foundation/Foundation.h>


/**
 * Defines the available days to select
 */
typedef enum {
    
    lde_None = 0, //!<No day is selected
    lde_7 = 7, //!<Last 7 days selected
    lde_15 = 15, //!<Last 15 days selected
    lde_30 = 30, //!<Last 30 days selecter
    lde_All //!<Select all days
    
} LastDaysEnum;


/**
 * Defines the transactions filter
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransactionsFilter : NSObject <NSCopying> {
    
@private
    
    /**
     * Last days
     */
    LastDaysEnum lastDays_;
    
    /**
     * Show incomes
     */
    BOOL showIncomes_;
    
    /**
     * Show bills
     */
    BOOL showPayments_;
    
    /**
     * A unique NSDate to filter if we don't select nothing on the lastDaysSegment
     */
    NSDate *uniqueDay_;
    
    /**
     * Selected month to filter. In tis case each uniqueDay_ and lastDays_ must be null
     */
    NSDate *selectedMonth_;
    
}

/**
 * Provides readwrite access to the lastDays property
 */
@property (nonatomic, readwrite, assign) LastDaysEnum lastDays;

/**
 * Provides readwrite access to the showIncomes property
 */
@property (nonatomic, readwrite, assign) BOOL showIncomes;

/**
 * Provides readwrite access to the showBills property
 */
@property (nonatomic, readwrite, assign) BOOL showPayments;

/**
 * Provides readwrite access to the uniqueDay property
 */
@property (nonatomic, readwrite, retain) NSDate *uniqueDay;

/**
 * Provides readwrite access to the selectedMonth property
 */
@property (nonatomic, readwrite, retain) NSDate *selectedMonth;

/**
 * Provides read-only access to the is empry filter flag (YES when the filter produces no filtering, NO otherwise)
 */
@property (nonatomic, readonly, assign) BOOL isEmptyFilter;

/**
 * Designated initializer. Initializes a TransactionsFilter instance with another filter options
 *
 * @param filterOptions The assigned filter to the new filter
 * @return The initialized TransactionsFilter instance
 */
- (id)initWithFilter:(TransactionsFilter *)filterOptions;

@end
