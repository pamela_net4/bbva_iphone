//
//  FastLoanSchedule.m
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "FastLoanSchedule.h"
#import "Tools.h"
#import "FastLoanQuotaList.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     flseas_AnalyzingCurrencySymbol = serxeas_ElementsCount, //!<Analyzing the FastLoanSchedule currencySymbol
     flseas_AnalyzingTerm , //!<Analyzing the FastLoanSchedule term
     flseas_AnalyzingTceaFormated , //!<Analyzing the FastLoanSchedule tceaFormated
     flseas_AnalyzingAmortizationTotal , //!<Analyzing the FastLoanSchedule amortizationTotal
     flseas_AnalyzingInterestTotal , //!<Analyzing the FastLoanSchedule interestTotal
     flseas_AnalyzingCommissionTotal , //!<Analyzing the FastLoanSchedule commissionTotal
     flseas_AnalyzingQuotaTotal , //!<Analyzing the FastLoanSchedule quotaTotal
     flseas_AnalyzingComment , //!<Analyzing the FastLoanSchedule comment
     flseas_AnalyzingSolicitudeDate , //!<Analyzing the FastLoanSchedule solicitudeDate
     flseas_AnalyzingFastLoanQuotaList , //!<Analyzing the FastLoanSchedule fastLoanQuotaList

} FastLoanScheduleXMLElementAnalyzerState;

@implementation FastLoanSchedule

#pragma mark -
#pragma mark Properties

@synthesize currencySymbol = currencySymbol_;
@synthesize term = term_;
@synthesize tceaFormated = tceaFormated_;
@synthesize amortizationTotal = amortizationTotal_;
@synthesize interestTotal = interestTotal_;
@synthesize commissionTotal = commissionTotal_;
@synthesize quotaTotal = quotaTotal_;
@synthesize comment = comment_;
@synthesize solicitudeDate = solicitudeDate_;
@synthesize fastLoanQuotaList = fastLoanQuotaList_;
;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [currencySymbol_ release];
    currencySymbol_ = nil;

    [term_ release];
    term_ = nil;

    [tceaFormated_ release];
    tceaFormated_ = nil;

    [amortizationTotal_ release];
    amortizationTotal_ = nil;

    [interestTotal_ release];
    interestTotal_ = nil;

    [commissionTotal_ release];
    commissionTotal_ = nil;

    [quotaTotal_ release];
    quotaTotal_ = nil;

    [comment_ release];
    comment_ = nil;

    [solicitudeDate_ release];
    solicitudeDate_ = nil;

    [fastLoanQuotaList_ release];
    fastLoanQuotaList_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if(0==1){
                   //delete fix
        }        else if ([lname isEqualToString: @"monedasimbolo"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingCurrencySymbol;


        }
        else if ([lname isEqualToString: @"plazo"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingTerm;


        }
        else if ([lname isEqualToString: @"tcea_dis"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingTceaFormated;


        }
        else if ([lname isEqualToString: @"total_amortizacion"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAmortizationTotal;


        }
        else if ([lname isEqualToString: @"total_interes"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingInterestTotal;


        }
        else if ([lname isEqualToString: @"total_comision"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingCommissionTotal;


        }
        else if ([lname isEqualToString: @"total_cuota"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingQuotaTotal;


        }
        else if ([lname isEqualToString: @"glosa_desgravamen"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingComment;


        }
        else if ([lname isEqualToString: @"fecha_solicitud"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingSolicitudeDate;


        }
        else if ([lname isEqualToString: @"cuotas"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingFastLoanQuotaList;
            [fastLoanQuotaList_ release];
            fastLoanQuotaList_ = nil;
            fastLoanQuotaList_ = [[FastLoanQuotaList alloc] init];
            fastLoanQuotaList_.openingTag = lname;
            [fastLoanQuotaList_ setParentParseableObject:self];
            [parser setDelegate:fastLoanQuotaList_];
            [fastLoanQuotaList_ parserDidStartDocument:parser];


        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(0==1){
           //delete fix
        }        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingCurrencySymbol) {

            [currencySymbol_ release];
            currencySymbol_ = nil;
            currencySymbol_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingTerm) {

            [term_ release];
            term_ = nil;
            term_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingTceaFormated) {

            [tceaFormated_ release];
            tceaFormated_ = nil;
            tceaFormated_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingAmortizationTotal) {

            [amortizationTotal_ release];
            amortizationTotal_ = nil;
            amortizationTotal_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingInterestTotal) {

            [interestTotal_ release];
            interestTotal_ = nil;
            interestTotal_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingCommissionTotal) {

            [commissionTotal_ release];
            commissionTotal_ = nil;
            commissionTotal_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingQuotaTotal) {

            [quotaTotal_ release];
            quotaTotal_ = nil;
            quotaTotal_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingComment) {

            [comment_ release];
            comment_ = nil;
            comment_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingSolicitudeDate) {

            [solicitudeDate_ release];
            solicitudeDate_ = nil;
            solicitudeDate_ = [elementString copyWithZone:self.zone];

        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

