/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


/**
 * Stores one card transactions additional information element
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardTransactionsAdditionalInformation : StatusEnabledResponse {

@private
    
    /**
     * Closing date string
     */
    NSString *closingDateString_;
    
    /**
     * Closing date
     */
    NSDate *closingDate_;
    
    /**
     * Last payment day string
     */
    NSString *lastPaymentDayString_;
    
    /**
     * Last payment day
     */
    NSDate *lastPaymentDay_;
    
    /**
     * Minimum payment ML string
     */
    NSString *minimumPaymentMLString_;
    
    /**
     * Minimum payment ML
     */
    NSDecimalNumber *minimumPaymentML_;
    
    /**
     * Minimum payment ME string
     */
    NSString *minimumPaymentMEString_;
    
    /**
     * Minimum payment ME
     */
    NSDecimalNumber *minimumPaymentME_;
    
    /**
     * Total payment ML string
     */
    NSString *totalPaymentMLString_;
    
    /**
     * Total payment ML
     */
    NSDecimalNumber *totalPaymentML_;
    
    /**
     * Total payment ME string
     */
    NSString *totalPaymentMEString_;
    
    /**
     * Total payment ME
     */
    NSDecimalNumber *totalPaymentME_;
    
    /**
     * Delayed payment ML string
     */
    NSString *delayedPaymentMLString_;
    
    /**
     * Delayed payment ML
     */
    NSDecimalNumber *delayedPaymentML_;
    
    /**
     * Delayed payment ME string
     */
    NSString *delayedPaymentMEString_;
    
    /**
     * Delayed payment ME
     */
    NSDecimalNumber *delayedPaymentME_;
    
    /**
     * More date indicator
     */
    NSString *moreData_;
    
    /**
     * Index
     */
    NSString *index_;
    
}


/**
 * Provides read-only access to the closing date string
 */
@property (nonatomic, readonly, copy) NSString *closingDateString;

/**
 * Provides read-only access to the closing date
 */
@property (nonatomic, readonly, copy) NSDate *closingDate;

/**
 * Provides read-only access to the last payment day string
 */
@property (nonatomic, readonly, copy) NSString *lastPaymentDayString;

/**
 * Provides read-only access to the last payment day
 */
@property (nonatomic, readonly, copy) NSDate *lastPaymentDay;

/**
 * Provides read-only access to the minimum payment ML string
 */
@property (nonatomic, readonly, copy) NSString *minimumPaymentMLString;

/**
 * Provides read-only access to the mminimum payment ML
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *minimumPaymentML;

/**
 * Provides read-only access to the minimum payment ME string
 */
@property (nonatomic, readonly, copy) NSString *minimumPaymentMEString;

/**
 * Provides read-only access to the minimum payment ME
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *minimumPaymentME;

/**
 * Provides read-only access to the total payment ML string
 */
@property (nonatomic, readonly, copy) NSString *totalPaymentMLString;

/**
 * Provides read-only access to the total payment ML
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *totalPaymentML;

/**
 * Provides read-only access to the total payment ME string
 */
@property (nonatomic, readonly, copy) NSString *totalPaymentMEString;

/**
 * Provides read-only access to the total payment ME
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *totalPaymentME;

/**
 * Provides read-only access to the delayed payment ML string
 */
@property (nonatomic, readonly, copy) NSString *delayedPaymentMLString;

/**
 * Provides read-only access to the delayed payment ML
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *delayedPaymentML;

/**
 * Provides read-only access to the delayed payment ME string
 */
@property (nonatomic, readonly, copy) NSString *delayedPaymentMEString;

/**
 * Provides read-only access to the delayed payment ME
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *delayedPaymentME;

/**
 * Provides read-only access to the more date indicator
 */
@property (nonatomic, readonly, copy) NSString *moreData;

/**
 * Provides read-only access to the index
 */
@property (nonatomic, readonly, copy) NSString *index;


/**
 * Updates this instance with the given card transactions additional information
 *
 * @param aCardTransactionsAdditionalnformation The card transactions additional information to update from
 */
- (void)updateFrom:(CardTransactionsAdditionalInformation *)aCardTransactionsAdditionalnformation;

@end
