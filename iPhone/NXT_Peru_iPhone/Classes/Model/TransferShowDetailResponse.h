//
//  TransferShowDetailResponse.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 10/2/13.
//
//

#import "StatusEnabledResponse.h"

//Forward declarations
@class TransferShowDetailAddtionalInformation;

@interface TransferShowDetailResponse : StatusEnabledResponse{
@private
    /**
     * Additional information
     */
    TransferShowDetailAddtionalInformation *additionalInformation_;
    
    
}

/**
 * Provides read-only access to the array of aditional information
 */
@property (nonatomic, readonly, retain) TransferShowDetailAddtionalInformation *additionalInformation;
@end
