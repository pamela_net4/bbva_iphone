/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MutualFundList.h"
#import "MutualFund.h"


#pragma mark -

/**
 * MutualFundList private category
 */
@interface MutualFundList(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearMutualFundListData;

@end


#pragma mark -

@implementation MutualFundList

#pragma mark -
#pragma mark Properties

@dynamic mutualFundsList;
@dynamic mutualFundCount;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearMutualFundListData];
    
    [mutualFundsList_ release];
    mutualFundsList_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a MutualFundList instance creating the associated array
 *
 * @return The initialized MutualFundList instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        mutualFundsList_ = [[NSMutableArray alloc] init];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates the mutual funds list from another mutual funds list
 */
- (void)updateFrom:(MutualFundList *)aMutualFundList {
    
    NSArray *otherMutualFindListArray = aMutualFundList.mutualFundsList;
    
    [self clearMutualFundListData];
    
    for (MutualFund *mutualFund in otherMutualFindListArray) {
        
        [mutualFundsList_ addObject:mutualFund];
        
    }
    
    [self updateFromStatusEnabledResponse:aMutualFundList];
    
    self.informationUpdated = aMutualFundList.informationUpdated;
    
}

/*
 * Remove the contained data
 */
- (void)removeData {
    
    [self clearMutualFundListData];
    
}

#pragma mark -
#pragma mark Getters and setters

/*
 * Returns the Mutual fund located at the given position, or nil if position is not valid
 */
- (MutualFund *)mutualFundAtPosition:(NSUInteger)aPosition {
	MutualFund* result = nil;
	
	if (aPosition < [mutualFundsList_ count]) {
		result = [mutualFundsList_ objectAtIndex: aPosition];
	}
	
	return result;
}

/*
 * Returns the mutual fund with a mutual fund Number, or nil if not valid
 */
- (MutualFund *)mutualFundFromMutualFundNumber:(NSString *)aMutualFundNumber {
    
	MutualFund *result = nil;
    
	if (aMutualFundNumber != nil) {
        
		MutualFund *target;
		NSUInteger size = [mutualFundsList_ count];
		BOOL found = NO;
        
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
            
			target = [mutualFundsList_ objectAtIndex:i];
            
			if ([aMutualFundNumber isEqualToString:target.number]) {
                
				result = target;
				found = YES;
                
			}
            
		}
        
	}
    
	return result;
    
}

/*
 * Returns the Mutual fund position inside the list
 */
- (NSUInteger)mutualFundPosition:(MutualFund *)aMutualFund {
    
	return [mutualFundsList_ indexOfObject:aMutualFund];
	
}


#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearMutualFundListData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (auxMutualFund_ != nil) {
        
        [mutualFundsList_ addObject:auxMutualFund_];
        [auxMutualFund_ release];
        auxMutualFund_ = nil;
        
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
            auxMutualFund_ = [[MutualFund alloc] init];
            [auxMutualFund_ setParentParseableObject:self];
            auxMutualFund_.openingTag = lname;
            [parser setDelegate:auxMutualFund_];
            [auxMutualFund_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (auxMutualFund_ != nil) {
        
        [mutualFundsList_ addObject:auxMutualFund_];
        [auxMutualFund_ release];
        auxMutualFund_ = nil;
        
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the mutual funds list array
 *
 * @return The mutual funds list array
 */
- (NSArray *)mutualFundsList {

    return [NSArray arrayWithArray:mutualFundsList_];
    
}

/**
 * Provides read access to the number of mutual funds stored
 *
 * @return The count
 */
- (NSUInteger)mutualFundCount {
	return [mutualFundsList_ count];
}


@end


#pragma mark -

@implementation MutualFundList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearMutualFundListData {
    
    [mutualFundsList_ removeAllObjects];
    
    [auxMutualFund_ release];
    auxMutualFund_ = nil;
    
	informationUpdated_ = soie_NoInfo;

}

@end
