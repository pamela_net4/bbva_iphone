//
//  CardForIncrement.m
//  NXT_Peru_iPhone
//
//  Created by Estefany on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "CardForIncrement.h"
#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     cfieas_AnalyzingPosition = serxeas_ElementsCount, //!<Analyzing the CardForIncrement position
     cfieas_AnalyzingCardNumber , //!<Analyzing the CardForIncrement cardNumber
     cfieas_AnalyzingCardDescription , //!<Analyzing the CardForIncrement cardDescription
     cfieas_AnalyzingContractNumber , //!<Analyzing the CardForIncrement contractNumber
     cfieas_AnalyzingCreditLine , //!<Analyzing the CardForIncrement creditLine
     cfieas_AnalyzingMinIncrement , //!<Analyzing the CardForIncrement minIncrement
     cfieas_AnalyzingMaxIncrement , //!<Analyzing the CardForIncrement maxIncrement
     cfieas_AnalyzingCurrency , //!<Analyzing the CardForIncrement currency
     cfieas_AnalyzingCurrencySimbol , //!<Analyzing the CardForIncrement currencySimbol

} CardForIncrementXMLElementAnalyzerState;

@implementation CardForIncrement

#pragma mark -
#pragma mark Properties

@synthesize position = position_;
@synthesize cardNumber = cardNumber_;
@synthesize cardDescription = cardDescription_;
@synthesize contractNumber = contractNumber_;
@synthesize creditLine = creditLine_;
@synthesize minIncrement = minIncrement_;
@synthesize maxIncrement = maxIncrement_;
@synthesize currency = currency_;
@synthesize currencySimbol = currencySimbol_;
;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [position_ release];
    position_ = nil;

    [cardNumber_ release];
    cardNumber_ = nil;

    [cardDescription_ release];
    cardDescription_ = nil;

    [contractNumber_ release];
    contractNumber_ = nil;

    [creditLine_ release];
    creditLine_ = nil;

    [minIncrement_ release];
    minIncrement_ = nil;

    [maxIncrement_ release];
    maxIncrement_ = nil;

    [currency_ release];
    currency_ = nil;

    [currencySimbol_ release];
    currencySimbol_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if(0==1){
                   //delete fix
        }        else if ([lname isEqualToString: @"num"]) {

            xmlAnalysisCurrentValue_ = cfieas_AnalyzingPosition;

        }
        else if ([lname isEqualToString: @"numtarjeta"]) {

            xmlAnalysisCurrentValue_ = cfieas_AnalyzingCardNumber;

        }
        else if ([lname isEqualToString: @"nombretarjeta"]) {

            xmlAnalysisCurrentValue_ = cfieas_AnalyzingCardDescription;

        }
        else if ([lname isEqualToString: @"contrato"]) {

            xmlAnalysisCurrentValue_ = cfieas_AnalyzingContractNumber;

        }
        else if ([lname isEqualToString: @"lineacredito"]) {

            xmlAnalysisCurrentValue_ = cfieas_AnalyzingCreditLine;

        }
        else if ([lname isEqualToString: @"incmin"]) {

            xmlAnalysisCurrentValue_ = cfieas_AnalyzingMinIncrement;

        }
        else if ([lname isEqualToString: @"incmax"]) {

            xmlAnalysisCurrentValue_ = cfieas_AnalyzingMaxIncrement;

        }
        else if ([lname isEqualToString: @"moneda"]) {

            xmlAnalysisCurrentValue_ = cfieas_AnalyzingCurrency;

        }
        else if ([lname isEqualToString: @"divisadis"]) {

            xmlAnalysisCurrentValue_ = cfieas_AnalyzingCurrencySimbol;

        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(0==1){
           //delete fix
        }        else if (xmlAnalysisCurrentValue_ == cfieas_AnalyzingPosition) {

            [position_ release];
            position_ = nil;
            position_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == cfieas_AnalyzingCardNumber) {

            [cardNumber_ release];
            cardNumber_ = nil;
            cardNumber_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == cfieas_AnalyzingCardDescription) {

            [cardDescription_ release];
            cardDescription_ = nil;
            cardDescription_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == cfieas_AnalyzingContractNumber) {

            [contractNumber_ release];
            contractNumber_ = nil;
            contractNumber_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == cfieas_AnalyzingCreditLine) {

            [creditLine_ release];
            creditLine_ = nil;
            creditLine_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == cfieas_AnalyzingMinIncrement) {

            [minIncrement_ release];
            minIncrement_ = nil;
            minIncrement_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == cfieas_AnalyzingMaxIncrement) {

            [maxIncrement_ release];
            maxIncrement_ = nil;
            maxIncrement_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == cfieas_AnalyzingCurrency) {

            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == cfieas_AnalyzingCurrencySimbol) {

            [currencySimbol_ release];
            currencySimbol_ = nil;
            currencySimbol_ = [elementString copyWithZone:self.zone];

        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

