/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class FastLoanAlternative;


/**
 * Contains the list of FastLoanAlternatives
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface FastLoanAlternativeList : StatusEnabledResponse {

@private

	/**
	 * FastLoanAlternatives list array
	 */
	NSMutableArray *fastloanalternativeList_;

	/**
	 * Auxiliar FastLoanAlternative object for parsing
	 */
	FastLoanAlternative *auxFastLoanAlternative_;

}

/**
 * Provides read-only access to the fastloanalternatives list array
 */
@property (nonatomic, readonly, retain) NSArray *fastloanalternativeList;

/**
 * Provides read access to the number of FastLoanAlternatives stored
 */
@property (nonatomic, readonly, assign) NSUInteger fastloanalternativeCount;

/**
 * Returns the FastLoanAlternative located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the FastLoanAlternative is located
 *
 * @return FastLoanAlternative located at the given position, or nil if position is not valid
 */
- (FastLoanAlternative *)fastloanalternativeAtPosition:(NSUInteger)aPosition;

/**
 * Returns the FastLoanAlternative position inside the list
 *
 * @param  aFastLoanAlternative FastLoanAlternative object which position we are looking for
 *
 * @return FastLoanAlternative position inside the list
 */
- (NSUInteger)fastloanalternativePosition:(FastLoanAlternative *)aFastLoanAlternative;

/**
 * Returns the Laon with an fastloanalternative number, or nil if not valid
 *
 * @param  aFastLoanAlternativeId The fastloanalternative id
 * @return FastLoanAlternative with this id, or nil if not valid
 */
//- (FastLoanAlternative *)fastloanalternativeFromFastLoanAlternativeId:(NSString *)aFastLoanAlternativeId;

/**
 * Updates the FastLoanAlternative list from another FastLoanAlternativeList instance
 *
 * @param  aFastLoanAlternativeList The FastLoanAlternativeList list to update from
 */
- (void)updateFrom:(FastLoanAlternativeList *)aFastLoanAlternativeList;

/**
 * Remove the contained data
 */
- (void)removeData;

@end

