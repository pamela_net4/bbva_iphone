/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "Route.h"
#import "Location.h"


#pragma mark -

@implementation Route

#pragma mark -
#pragma mark Properties

@synthesize originLocation = originLocation_;
@synthesize destinationLocation = destinationLocation_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [originLocation_ release];
    originLocation_ = nil;
    
    [destinationLocation_ release];
    destinationLocation_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes an empy Route instance
 *
 * @return The empty initialized Route instance
 */
- (id)init {
    
    return [self initWithOriginLocation:nil
                    destinationLocation:nil];
    
}

/*
 * Designated initializer. Initializes a Route instance with the provided origin and destination locations
 */
- (id)initWithOriginLocation:(Location *)anOriginLocation
         destinationLocation:(Location *)aDestinationLocation {

    if ((self = [super init])) {
    
        originLocation_ = [anOriginLocation retain];
        destinationLocation_ = [aDestinationLocation retain];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Ordering and comparing selectors

/*
 * Compares one Route with another, using the origin and destination locations as element to compare. Elements are ordered alphabetically, first
 * by origin location, and the by destination location
 */
- (NSComparisonResult)compareAlphabetically:(Route *)aRoute {
    
    NSComparisonResult result = [originLocation_ compareAlphabetically:aRoute->originLocation_];
    
    if (result == NSOrderedSame) {
        
        result = [destinationLocation_ compareAlphabetically:aRoute->destinationLocation_];
        
    }
    
    return result;
    
}

/*
 * Compares to another Route instance to check whether both are equal. Two routes are considered to be equal if their origin
 * and destination locations are equal
 */
- (BOOL)isEqualToRoute:(Route *)aRoute {
    
    BOOL result = [originLocation_ isEqualToLocation:aRoute->originLocation_];
    
    if (result) {
        
        result = [destinationLocation_ isEqualToLocation:aRoute->destinationLocation_];
        
    }
    
    return result;
    
}

@end
