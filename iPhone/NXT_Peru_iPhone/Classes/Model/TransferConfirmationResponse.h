/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class TransferConfirmationAdditionalInformation;


/**
 * Contains the transfer confirmation response obtained from the server
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferConfirmationResponse : StatusEnabledResponse {

@private
    
    /**
     * Additional information
     */
    TransferConfirmationAdditionalInformation *additionalInformation_;
    
}


/**
 * Provides read-only access to the additional information
 */
@property (nonatomic, readonly, retain) TransferConfirmationAdditionalInformation *additionalInformation;

@end
