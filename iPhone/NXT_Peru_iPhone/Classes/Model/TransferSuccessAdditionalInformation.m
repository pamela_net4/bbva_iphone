/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "TransferSuccessAdditionalInformation.h"
#import "Tools.h"


/**
 * Enumerates the analysis states
 */
typedef enum {
    
    tsaixeas_AnalyzingOperationNumber = serxeas_ElementsCount, //!<Analyzing the operation number
	tsaixeas_AnalyzingOperation, //!<Analyzing the operation
	tsaixeas_AnalyzingHolder, //!<Analyzing the holder
	tsaixeas_AnalyzingAmountCharged, //!<Analyzing the amount charged
	tsaixeas_AnalyzingAmountChargedCurrency, //!<Analyzing the amount charged currency
	tsaixeas_AnalyzingAmountPaid, //!<Analyzing the amount paid
	tsaixeas_AnalyzingAmountPaidCurrency, //!<Analyzing the amount paid currency
	tsaixeas_AnalyzingExchangeRate, //!<Analyzing the exchange rate
	tsaixeas_AnalyzingDate, //!<Analyzing the date
    tsaixeas_AnalyzingHour, //!<Analyzing the hour
    tsaixeas_AnalyzingExpirationDate, //!<Analyzing the expiration date
    tsaixeas_AnalyzingExpirationHour, //!<Analyzing the expiration hour
	tsaixeas_AnalyzingCurrencyExchangeRate, //!<Analyzing the currency type change
	tsaixeas_AnalyzingCommision, //!<Analyzing the commission
	tsaixeas_AnalyzingCurrencyCommision, //!<Analyzing the currency commission
	tsaixeas_AnalyzingNetworkUse, //!<Analyzing the network use
	tsaixeas_AnalyzingCurrencyNetworkUse,  //!<Analyzing the currency network use
	tsaixeas_AnalyzingAmountToPay, //!<Analyzing the amount to pay
	tsaixeas_AnalyzingCurrencyAmountToPay, //!<Analyzing the currency amount to pay
	tsaixeas_AnalyzingTotalAmountToPay, //!<Analyzing the total amount to pay
	tsaixeas_AnalyzingCurrencyTotalAmountToPay, //!<Analyzing the currency total amount to paysion
    tsaixeas_AnalyzingITFCurrency,  //!<Analyzing the ITF message
    tsaixeas_AnalyzingITFAmount,  //!<Analyzing the ITF message
	tsaixeas_AnalyzingITFMessage,  //!<Analyzing the ITF message
	tsaixeas_AnalyzingConfirmationMessage,  //!<Analyzing the confirmation message
	tsaixeas_AnalyzingMarketPlaceCommision, //!<Analyzing the market place commision
    tsaixeas_AnalyzingMarketPlaceCommisionCurrency, //!<Analyzing the market place currency
    tsaixeas_AnalyzingBeneficiaryDocumentType,
    tsaixeas_AnalyzingBeneficiaryDocumentTypeInd,
	tsaixeas_AnalyzingBeneficiaryDocument, //!<Analyzing the beneficiary document
    tsaixeas_AnalyzingTransferState, //!<Analyzing the transfer state
    tsaixeas_AnalyzingNotificationMessage, //!<Analyzing the notificationMessage
    tsaixeas_AnalyzingOperationState, //!<Analyzing the operationState
    tsaixeas_AnalyzingAmountToTransfer
	
} TransferSuccessAdditionalInformationXMLElementAnalyzerState;


#pragma mark -

/**
 * TransferSuccessAdditionalInformation private category
 */
@interface TransferSuccessAdditionalInformation(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearTransferSuccessAdditionalInformationData;

@end


#pragma mark -

@implementation TransferSuccessAdditionalInformation

#pragma mark -
#pragma mark Properties
@synthesize operationState = operationState_;
@synthesize operationNumber = operationNumber_;
@synthesize operation = operation_;
@synthesize holder = holder_;
@synthesize amountChargedString = amountChargedString_;
@dynamic amountCharged;
@synthesize amountPaidString = amountPaidString_;
@dynamic amountPaid;
@synthesize exchangeRate = exchangeRate_;
@synthesize dateString = dateString_;
@synthesize expirationDateString = expirationDate_;
@dynamic date;
@synthesize expirationHourString = expirationHour_;
@synthesize hour = hour_;
@synthesize currencyExchangeRate = currencyExchangeRate_;
@synthesize commission = commission_;
@synthesize currencyCommission = currencyCommission_;
@synthesize networkUse = networkUse_;
@synthesize currencyNetworkUse = currencyNetworkUse_;
@synthesize amountToPay = amountToPay_;
@synthesize currencyAmountToPay = currencyAmountToPay_;
@synthesize totalAmountToPay = totalAmountToPay_;
@synthesize currencyTotalAmountToPay = currencyTotalAmountToPay_;
@synthesize currencyItf = currencyItf_;
@synthesize itfAmount = itfAmount_;
@synthesize itfMessage = itfMessage_;
@synthesize confirmationMessage = confirmationMessage_;
@synthesize amountChargedCurrencyString = amountChargedCurrencyString_;
@synthesize amountPaidCurrencyString = amountPaidCurrencyString_;
@synthesize marketPlaceCommisionString = marketPlaceCommisionString_;
@synthesize marketPlaceCommisionCurrencyString = marketPlaceCommisionCurrencyString_;
@dynamic marketPlaceCommision;
@synthesize beneficiaryDocument = beneficiaryDocument_;
@synthesize beneficiaryDocumentType = beneficiaryDocumentType_;
@synthesize beneficiaryDocumentTypeId = beneficiaryDocumentTypeId_;
@synthesize transferState = transferState_;
@synthesize notificationMessage = notificationMessage_;
@synthesize amountToTransfer = amountToTransfer_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearTransferSuccessAdditionalInformationData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearTransferSuccessAdditionalInformationData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"estadooperacion"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingOperationState;
            
        } else if ([lname isEqualToString:@"numerooperacion"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingOperationNumber;
            
        } else if ([lname isEqualToString:@"operacion"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingOperation;
            
        } else if ([lname isEqualToString:@"titular"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingHolder;
            
        } else if ([lname isEqualToString:@"importecargado"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingAmountCharged;
            
        } else if ([lname isEqualToString:@"monedaimportecargado"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingAmountChargedCurrency;
            
        } else if ([lname isEqualToString:@"importeabonado"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingAmountPaid;
            
        } else if ([lname isEqualToString:@"monedaimporteabonado"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingAmountPaidCurrency;
            
        } else if ([lname isEqualToString:@"tipocambio"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingExchangeRate;
            
        } else if ([lname isEqualToString:@"fecha"] || [lname isEqualToString:@"fechatransaccion"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingDate;
            
        } else if ([lname isEqualToString:@"hora"] || [lname isEqualToString:@"horatransaccion"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingHour;
            
        } else if ([lname isEqualToString:@"fechavencimiento"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingExpirationDate;
            
        }  else if ([lname isEqualToString:@"horavencimiento"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingExpirationHour;
            
        }  else if ([lname isEqualToString:@"monedatipocambio"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingCurrencyExchangeRate;
            
        } else if ([lname isEqualToString:@"comision"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingCommision;
            
        } else if ([lname isEqualToString:@"monedacomision"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingCurrencyCommision;
            
        } else if ([lname isEqualToString:@"usored"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingNetworkUse;
            
        } else if ([lname isEqualToString:@"monedausored"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingCurrencyNetworkUse;
            
        } else if ([lname isEqualToString:@"importecargar"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingAmountToPay;
            
        }else if ([lname isEqualToString:@"importetransferir"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingAmountToTransfer;
            
        } else if ([lname isEqualToString:@"monedaimportecargar"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingCurrencyAmountToPay;
            
        } else if ([lname isEqualToString:@"totalcargado"] || [lname isEqualToString:@"importetotal"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingTotalAmountToPay;
            
        } else if ([lname isEqualToString:@"monedatotalcargado"] || [lname isEqualToString:@"monedaimportetotal"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingCurrencyTotalAmountToPay;
            
        } else if ([lname isEqualToString:@"monedaitf"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingITFCurrency;
            
        } else if ([lname isEqualToString:@"importeitf"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingITFAmount;
            
        } else if ([lname isEqualToString:@"mensajeitf"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingITFMessage;
            
        } else if ([lname isEqualToString:@"mensajeconfimacion"] || [lname isEqualToString:@"mensajeconfirmacion"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingConfirmationMessage;
			
        } else if ([lname isEqualToString:@"comisionplaza"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingMarketPlaceCommision;
            
        } else if ([lname isEqualToString:@"monedacomisionplaza"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingMarketPlaceCommisionCurrency;
            
        } else if ([lname isEqualToString:@"documentobeneficiario"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingBeneficiaryDocument;
            
        }else if ([lname isEqualToString:@"indtipodocumento"]){
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingBeneficiaryDocumentTypeInd;
            
        }else if ([lname isEqualToString:@"tipodocumento"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingBeneficiaryDocumentType;
            
        } else if ([lname isEqualToString:@"estado"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingTransferState;
            
        } else if ([lname isEqualToString:@"mensajenotificacion"]) {
            
            xmlAnalysisCurrentValue_ = tsaixeas_AnalyzingNotificationMessage;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingOperationState) {
            
            [operationState_ release];
            operationState_ = nil;
            operationState_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingOperationNumber) {
            
            [operationNumber_ release];
            operationNumber_ = nil;
            operationNumber_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingOperation) {
            
            [operation_ release];
            operation_ = nil;
            operation_ = [elementString  copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingHolder) {
            
            [holder_ release];
            holder_ = nil;
            holder_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingAmountCharged) {
            
            [amountChargedString_ release];
            amountChargedString_ = nil;
            amountChargedString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingAmountChargedCurrency) {
            
            [amountChargedCurrencyString_ release];
            amountChargedCurrencyString_ = nil;
            amountChargedCurrencyString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingAmountPaid) {
            
            [amountPaidString_ release];
            amountPaidString_ = nil;
            amountPaidString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingAmountPaidCurrency) {
            
            [amountPaidCurrencyString_ release];
            amountPaidCurrencyString_ = nil;
            amountPaidCurrencyString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingExchangeRate) {
            
            [exchangeRate_ release];
            exchangeRate_ = nil;
            exchangeRate_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingDate) {
            
            [dateString_ release];
            dateString_ = nil;
            dateString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingHour) {
            
            [hour_ release];
            hour_ = nil;
            hour_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingExpirationDate) {
            
            [expirationDate_ release];
            expirationDate_ = nil;
            expirationDate_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingExpirationHour) {
            
            [expirationHour_ release];
            expirationHour_ = nil;
            expirationHour_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingCurrencyExchangeRate) {
            
            [currencyExchangeRate_ release];
            currencyExchangeRate_ = nil;
            currencyExchangeRate_ = [elementString copyWithZone:self.zone];
			
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingCommision) {
            
            [commission_ release];
            commission_ = nil;
            commission_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingCurrencyCommision) {
            
            [currencyCommission_ release];
            currencyCommission_ = nil;
            currencyCommission_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingNetworkUse) {
            
            [networkUse_ release];
            networkUse_ = nil;
            networkUse_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingCurrencyNetworkUse) {
            
            [currencyNetworkUse_ release];
            currencyNetworkUse_ = nil;
            currencyNetworkUse_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingAmountToPay) {
            
            [amountToPay_ release];
            amountToPay_ = nil;
            amountToPay_ = [elementString copyWithZone:self.zone];
            
        }else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingAmountToTransfer) {
            
            [amountToTransfer_ release];
            amountToTransfer_ = nil;
            amountToTransfer_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingCurrencyAmountToPay) {
            
            [currencyAmountToPay_ release];
            currencyAmountToPay_ = nil;
            currencyAmountToPay_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingTotalAmountToPay) {
            
            [totalAmountToPay_ release];
            totalAmountToPay_ = nil;
            totalAmountToPay_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingCurrencyTotalAmountToPay) {
            
            [currencyTotalAmountToPay_ release];
            currencyTotalAmountToPay_ = nil;
            currencyTotalAmountToPay_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingITFAmount) {
            
            [itfAmount_ release];
            itfAmount_ = nil;
            itfAmount_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingITFCurrency) {
            
            [currencyItf_ release];
            currencyItf_ = nil;
            currencyItf_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingITFMessage) {
            
            [itfMessage_ release];
            itfMessage_ = nil;
            itfMessage_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingConfirmationMessage) {
            
            [confirmationMessage_ release];
            confirmationMessage_ = nil;
            confirmationMessage_ = [[elementString stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"] copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingMarketPlaceCommision) {
            
            [marketPlaceCommisionString_ release];
            marketPlaceCommisionString_ = nil;
            marketPlaceCommisionString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingMarketPlaceCommisionCurrency) {
            
            [marketPlaceCommisionCurrencyString_ release];
            marketPlaceCommisionCurrencyString_ = nil;
            marketPlaceCommisionCurrencyString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingTransferState) {
            
            [transferState_ release];
            transferState_ = nil;
            transferState_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingNotificationMessage) {
            
            [notificationMessage_ release];
            notificationMessage_ = nil;
            notificationMessage_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingBeneficiaryDocument) {
            
            [beneficiaryDocument_ release];
            beneficiaryDocument_ = nil;
			beneficiaryDocument_ = [elementString copyWithZone:self.zone];
			
        }else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingBeneficiaryDocumentTypeInd) {
            
            [beneficiaryDocumentTypeId_ release];
            beneficiaryDocumentTypeId_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            beneficiaryDocumentTypeId_ = [aux retain];
            
        }else if (xmlAnalysisCurrentValue_ == tsaixeas_AnalyzingBeneficiaryDocumentType) {
            
            [beneficiaryDocumentType_ release];
            beneficiaryDocumentType_ = nil;
            
            NSString *aux = [[elementString copyWithZone:self.zone]autorelease];
            beneficiaryDocumentType_ = [aux retain];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the amount charged, calculating it from the string representation if necessary
 *
 * @return The amount charged
 */
- (NSDecimalNumber *)amountCharged {
    
    NSDecimalNumber *result = amountCharged_;
    
    if (result == nil) {
        
        if ([amountChargedString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:amountChargedString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                amountCharged_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the amount paid, calculating it from the string representation if necessary
 *
 * @return The amount paid
 */
- (NSDecimalNumber *)amountPaid {
    
    NSDecimalNumber *result = amountPaid_;
    
    if (result == nil) {
        
        if ([amountPaidString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:amountPaidString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                amountPaid_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the market place commision, calculating it from the string representation if necessary
 *
 * @return The amount paid
 */
- (NSDecimalNumber *)marketPlaceCommision {
    
    NSDecimalNumber *result = marketPlaceCommision_;
    
    if (result == nil) {
        
        if ([marketPlaceCommisionString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:marketPlaceCommisionString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                marketPlaceCommision_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the transaction date, calculating it from the string representation if necessary
 *
 * @return The transaction date
 */
- (NSDate *)date {
    
    NSDate *result = date_;
    
    if (result == nil) {
        
        date_ = [[Tools dateFromServerString:dateString_] retain];
        result = date_;
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation TransferSuccessAdditionalInformation(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearTransferSuccessAdditionalInformationData {
    
    [operationState_ release];
    operationState_ = nil;
    
    [operationNumber_ release];
    operationNumber_ = nil;
    
    [operation_ release];
    operation_ = nil;
    
    [holder_ release];
    holder_ = nil;
    
    [amountChargedString_ release];
    amountChargedString_ = nil;
    
    [amountCharged_ release];
    amountCharged_ = nil;
    
    [amountPaidString_ release];
    amountPaidString_ = nil;
    
    [amountPaid_ release];
    amountPaid_ = nil;
    
    [exchangeRate_ release];
    exchangeRate_ = nil;
    
    [dateString_ release];
    dateString_ = nil;
    
    [date_ release];
    date_ = nil;
	
	[hour_ release];
	hour_= nil;
    
	[currencyExchangeRate_ release];
	currencyExchangeRate_ = nil;
	
	[commission_ release];
	commission_ = nil;
	
	[currencyCommission_ release];
	currencyCommission_ = nil;
	
	[networkUse_ release];
	networkUse_ = nil;
	
	[currencyNetworkUse_ release];
	currencyNetworkUse_ = nil;
	
	[amountToPay_ release];
	amountToPay_ = nil;
	
	[currencyAmountToPay_ release];
	currencyAmountToPay_ = nil;
	
	[totalAmountToPay_ release];
	totalAmountToPay_ = nil;
	
	[currencyTotalAmountToPay_ release];
	currencyTotalAmountToPay_ = nil;
	
	[itfMessage_ release];
	itfMessage_ = nil;
	
	[confirmationMessage_ release];
	confirmationMessage_ = nil;
	
	[amountChargedCurrencyString_ release];
	amountChargedCurrencyString_ = nil;
    
	[amountPaidCurrencyString_ release];
	amountPaidCurrencyString_ = nil;
	
	[marketPlaceCommisionString_ release];
	marketPlaceCommisionString_= nil;
	
	[marketPlaceCommisionCurrencyString_ release];
	marketPlaceCommisionCurrencyString_ = nil;
	
	[beneficiaryDocument_ release];
	beneficiaryDocument_ = nil;
    
    [beneficiaryDocumentType_ release];
    beneficiaryDocumentType_ = nil;
    
    [beneficiaryDocumentTypeId_ release];
    beneficiaryDocumentTypeId_ = nil;
    
    [amountToTransfer_ release];
    amountToTransfer_ = nil;
	
}

@end
