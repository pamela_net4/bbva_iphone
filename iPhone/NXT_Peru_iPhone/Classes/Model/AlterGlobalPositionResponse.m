//
//  AlterGlobalPositionResponse.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 13/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "AlterGlobalPositionResponse.h"
#import "ListaCuentas.h"

typedef enum {
    
    gprxeas_AnalyzingListaCuentas = serxeas_ElementsCount
    
} GlobalPositionResponseXMLElementAnalyzerState;

@interface AlterGlobalPositionResponse(private)

- (void) clearAlterGlobalPositionResponseData;

@end

@implementation AlterGlobalPositionResponse

@synthesize listaCuentas = listaCuentas_;

- (void)dealloc
{
    [self clearAlterGlobalPositionResponseData];
    [super dealloc];
}

- (id) init {
    if (self = [super init]) {
        self.notificationToPost = kNotificationAlterGlobalPositionResponseReceived;
    }
    return self;
}

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearAlterGlobalPositionResponseData];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qName attributes:attributeDict];
    
    if(xmlAnalysisCurrentValue_ == serxeas_Nothing){
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"listcta"]) {
            xmlAnalysisCurrentValue_ = gprxeas_AnalyzingListaCuentas;
            [listaCuentas_ release];
            listaCuentas_ = nil;
            listaCuentas_ = [[ListaCuentas alloc] init];
            listaCuentas_.openingTag = lname;
            [listaCuentas_ setParentParseableObject:self];
            [parser setDelegate:listaCuentas_];
            [listaCuentas_ parserDidStartDocument:parser];
        } else {
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
        
    }
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if(xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        if([lname isEqualToString:@"msg-s"]){
            
            informationUpdated_ = soie_InfoAvailable;
            [parser setDelegate:parentParseableObject_];
            parentParseableObject_ = nil;
            
        }
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
    }
    
}

- (void) clearAlterGlobalPositionResponseData {
    
    [listaCuentas_ release];
    listaCuentas_ = nil;
    
}

@end
