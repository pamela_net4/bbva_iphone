/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "AccountTransactionList.h"
#import "AccountTransaction.h"


#pragma mark -

/**
 * AccountTransactionList private category
 */
@interface AccountTransactionList(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearAccountTransactionListData;

/**
 * Sorts the array of transaction by date descending
 */
- (void)sortTransanctionsByDateDescending;

/**
 * Groups the transactions by date
 */
- (void)groupByDate;

@end


#pragma mark -

@implementation AccountTransactionList

#pragma mark -
#pragma mark Properties

@dynamic accountTransactionList;
@dynamic accountTransactionCount;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {

    [self clearAccountTransactionListData];
    
    [accountTransactionList_ release];
    accountTransactionList_ = nil;
	
	[auxAccountTransaction_ release];
    auxAccountTransaction_ = nil;
    
    [groupedTransactions_ release];
    groupedTransactions_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Initialization

/**	
 * Object initializer
 *
 * @return An initialized instance
 */
- (id) init {
    
	if (self = [super init]) {
        
        accountTransactionList_ = [[NSMutableArray alloc] init];
		informationUpdated_ = soie_NoInfo;
		self.notificationToPost = kNotificationAccountTransactionListReceived;
        groupedTransactions_ = [[NSMutableDictionary alloc] init];
	}
	
	return self;
    
}

#pragma mark -
#pragma mark Information management

/*	
 * Updates the account transaction list from another account transaction list
 */
- (void)updateFrom:(AccountTransactionList *)anAccountTransactionList {
	
    NSArray *accountTransactions = anAccountTransactionList.accountTransactionList;
	
	[self clearAccountTransactionListData];
	
	if (accountTransactions != nil) {
        
		for (AccountTransaction *accountTransaction in accountTransactions) {
            
            [accountTransactionList_ addObject:accountTransaction];
            
		}
        
	}
	
    [self updateFromStatusEnabledResponse:anAccountTransactionList];
	
	self.informationUpdated = anAccountTransactionList.informationUpdated;
	
}

/*
 * Remove the contained data
 */
- (void)removeData {
    
	[self clearAccountTransactionListData];
    
}

/*
 * Returns the AccountTransaction located at the given position, or nil if position is not valid
 */
- (AccountTransaction *)accountTransactionAtPosition:(NSUInteger)aPosition {
    
	AccountTransaction *result = nil;
	
	if (aPosition < [accountTransactionList_ count]) {
        
		result = [accountTransactionList_ objectAtIndex:aPosition];
        
	}
	
	return result;
    
}

/*
 * Returns the AccountTransaction with an account transaction number, or nil if not valid
 */
- (AccountTransaction *)accountTransactionFromAccountTransactionNumber:(NSString *)anAccountTransactionNumber {
    
	AccountTransaction *result = nil;
    
	if (anAccountTransactionNumber != nil) {
        
		AccountTransaction *target;
		NSUInteger size = [accountTransactionList_ count];
		BOOL found = NO;
        
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
            
			target = [accountTransactionList_ objectAtIndex:i];
            
			if ([anAccountTransactionNumber isEqualToString:target.number]) {
                
				result = target;
				found = YES;
                
			}
            
		}
        
	}
    
	return result;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearAccountTransactionListData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (auxAccountTransaction_ != nil) {
        
        [accountTransactionList_ addObject:auxAccountTransaction_];
		[auxAccountTransaction_ release];
		auxAccountTransaction_ = nil;
        
	}
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
			[auxAccountTransaction_ release];
            auxAccountTransaction_ = [[AccountTransaction alloc] init];
            [auxAccountTransaction_ setParentParseableObject:self];
            auxAccountTransaction_.openingTag = lname;
            [parser setDelegate:auxAccountTransaction_];
            [auxAccountTransaction_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
	if (auxAccountTransaction_ != nil) {
        
        [accountTransactionList_ addObject:auxAccountTransaction_];
		[auxAccountTransaction_ release];
		auxAccountTransaction_ = nil;
        
	}
    
    [accountTransactionList_ sortUsingSelector:@selector(compareForDescendingDateOrdering:)];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    [self sortTransanctionsByDateDescending];
    [self groupByDate];
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the account transaction list
 *
 * @return The account transaction list
 */
- (NSArray *)accountTransactionList {
    
    return accountTransactionList_;
    
}

/*
 * Returns the account transactions count
 *
 * @return The account transactions count
 */
- (NSUInteger)accountTransactionCount {
    
    return [accountTransactionList_ count];
    
}

/*
 * Returns the list of transactions for the given date represented as seconds from Jan 1, 1970
 */
- (NSArray *)transactionListForDate:(NSTimeInterval)aDate {
    
    NSTimeInterval dateTimeInterval = floor(aDate / A_DAY_IN_SECONDS) * A_DAY_IN_SECONDS;
    NSNumber *dateNumber = [NSNumber numberWithDouble:dateTimeInterval];
    
    return [groupedTransactions_ objectForKey:dateNumber];
    
}



@end



#pragma mark -

@implementation AccountTransactionList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearAccountTransactionListData {
    
    [accountTransactionList_ removeAllObjects];
    
    [groupedTransactions_ release];
    groupedTransactions_ = [[NSMutableDictionary alloc] init];
    
}

#pragma mark -
#pragma mark Information management

/*
 * Groups the transactions by date
 */
- (void)groupByDate {
    
    NSMutableDictionary *newDatesDictionary = [[NSMutableDictionary alloc] init];
    NSDate *operationDate;
    NSTimeInterval operationDateTimeInterval;
    NSNumber *operationDateNumber;
    NSMutableArray *dateTransactionsArray = nil;
    
    for (AccountTransaction *transaction in accountTransactionList_) {
        
        operationDate = transaction.operationDate;
        operationDateTimeInterval = [operationDate timeIntervalSince1970];
        operationDateTimeInterval = floor(operationDateTimeInterval / A_DAY_IN_SECONDS) * A_DAY_IN_SECONDS;
        operationDateNumber = [NSNumber numberWithDouble:operationDateTimeInterval];
        
        //		DLog(@"groupByDate: %f %@ %@", operationDateTimeInterval, transaction.originalAmount, transaction.operationDate);
        
		
        dateTransactionsArray = [groupedTransactions_ objectForKey:operationDateNumber];
        
        if (dateTransactionsArray != nil) {
            
            if (![dateTransactionsArray containsObject:transaction]) {
                
                [dateTransactionsArray addObject:transaction];
                
            }
            
        } else {
            
            dateTransactionsArray = [newDatesDictionary objectForKey:operationDateNumber];
            
            if (dateTransactionsArray == nil) {
                
                dateTransactionsArray = [[NSMutableArray alloc] init];
                [newDatesDictionary setObject:dateTransactionsArray forKey:operationDateNumber];
                [dateTransactionsArray release];
                
            }
            
            if (![dateTransactionsArray containsObject:transaction]) {
                
                [dateTransactionsArray addObject:transaction];
                
            }
            
        }
        
    }
    
    for (operationDateNumber in [newDatesDictionary allKeys]) {
        
        dateTransactionsArray = [newDatesDictionary objectForKey:operationDateNumber];
        [groupedTransactions_ setObject:dateTransactionsArray forKey:operationDateNumber];
        
    }
    
    
    [newDatesDictionary release];
    
}

/*
 * Sorts the array of transaction by date descending
 */
- (void)sortTransanctionsByDateDescending {
    NSSortDescriptor *sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"operationDate" ascending:NO] autorelease];
    NSArray *sortedArray = [accountTransactionList_ sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    [accountTransactionList_ removeAllObjects];
    
    for (AccountTransaction *transaction in sortedArray) {
        [accountTransactionList_ addObject:transaction];
    }
    
    //    [transactionList_ release];
    //    transactionList_ = [sortedArray retain];
}


@end
