/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class StockMarketAccount;


/**
 * Contains the list of stock market accounts obtained from the server
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface StockMarketAccountList : StatusEnabledResponse {
    
@private
	
	/**
	 * Stock market account array list
	 */
	NSMutableArray *stockMarketAccountsList_;
    
	/**
	 * Auxiliar stock market account instance for parsing
	 */
	StockMarketAccount *auxStockMarketAccount_;
    
}


/**
 * Provides read-only access to the stock market account array list
 */
@property (nonatomic, readonly, retain) NSArray *stockMarketAccountList;

/**	
 * Provides read access to the number of StockAccounts stored
 */
@property (nonatomic, readonly, assign) NSUInteger stockMarketAccountCount;

/**
 * Returns the StockAccount located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the StockAccount is located
 *
 * @return StockAccount located at the given position, or nil if position is not valid
 */
- (StockMarketAccount *)stockMarketAccountAtPosition:(NSUInteger)aPosition;

/**
 * Returns the StockAccount position inside the list
 *
 * @param  aStockMarketAccount StockAccount object which position we are looking for
 *
 * @return StockAccount position inside the list
 */
- (NSUInteger)stockMarketAccountPosition:(StockMarketAccount *)aStockMarketAccount;

/**
 * Returns the stock market account with a value account number, or nil if not valid
 *
 * @param  aStockMarketAccountValueAccount The stock market account value account number to look for
 * @return The stock market account with this value account number, or nil if not valid
 */
- (StockMarketAccount *)stockMarketAccountFromValueAccount:(NSString *)aStockMarketAccountValueAccount;

/**
 * Updates the stock market account list from another stock market account list
 *
 * @param aStockMarketAccountList The StockMarketAccountList instance to update from
 */
- (void)updateFrom:(StockMarketAccountList *)aStockMarketAccountList;

/**
 * Remove the contained data
 */
- (void)removeData;

@end
