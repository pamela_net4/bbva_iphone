/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "DepositList.h"
#import "Deposit.h"


#pragma mark -

/**
 * DespositList private category
 */
@interface DepositList (private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearDepositListData;

@end


#pragma mark -

@implementation DepositList

@dynamic depositList;
@synthesize depositDictionary = depositDictionary_;
@dynamic depositCount;

#pragma mark -
#pragma mark List methods

-(NSArray *) changeGroups
{
    NSInteger numelements = [self depositCount] + [self depositDictionaryCount]-1;
    NSMutableArray *interDeposits = [NSMutableArray arrayWithCapacity:0];
    
    NSArray *depositList = [self depositList];
    
    for (NSUInteger j = 0;j<=[depositList count];j++)
    {                                                
        if(j>0 && j<[depositList count] )
        {
            Deposit *currentDeposit =[depositList objectAtIndex:j];
            Deposit *nextDeposit = [depositList objectAtIndex:j-1];
            if(![currentDeposit.groupingCode isEqualToString:nextDeposit.groupingCode])
            {
                [interDeposits addObject:[NSNumber numberWithInt:j+[interDeposits count]]];
            }
            
            
        }
        else if(j == [depositList count]){
            [interDeposits addObject:[NSNumber numberWithInt:numelements]];
        }
        
    }
    return [NSArray arrayWithArray:interDeposits];
}

-(NSInteger)getGroupWithPosition:(NSInteger)row
{   
    NSArray *arrayInterGroup = [self changeGroups];
    
    for (NSInteger i = [arrayInterGroup count]-1;i>=0; i--) {
        NSNumber *num = [arrayInterGroup objectAtIndex:i];
        if([num integerValue] < row)
        {
            row-=i+1;
            break;
        }

    }
    return row;
    
}

-(NSInteger) getGroup:(Deposit *)deposit
{
    NSArray *depositArray = [self depositList];
    NSUInteger group = 0;
    
    for (NSUInteger j = 0;j<[depositArray count];j++)
    {            
        Deposit *currentDeposit =[depositArray objectAtIndex:j];
//        Deposit *nextDeposit = [depositArray objectAtIndex:j];

        if(j>0 && j<[depositArray count] )
        {
            Deposit *nextDeposit = [depositArray objectAtIndex:j-1];
            
            if(![currentDeposit.groupingCode isEqualToString:nextDeposit.groupingCode])
            {
                group ++;
            }
        }
        
        if(currentDeposit == deposit)
        {
            break;
        }
    }    
    
    return group;
}

/*	
 * Adds an Deposit at the end of the list
 */
- (void)addDeposit:(Deposit *)aDeposit {
	if (aDeposit != nil) {
//		[depositList_ addObject: aDeposit];
        
        NSMutableArray *auxArray = [depositDictionary_ objectForKey:aDeposit.groupingCode];
        
        if(auxArray !=nil)
            [auxArray addObject:aDeposit];
        else {
            auxArray = [NSMutableArray arrayWithObject:aDeposit];
            [depositDictionary_ setObject:auxArray forKey:aDeposit.groupingCode];
            [depositList_ addObject:auxArray];
        }
		
	}
}

/*	
 * Updates the Deposit list from another Deposit list
 */
- (void)updateFrom:(DepositList *)aDepositList {

    NSArray *deposits = aDepositList.depositList;
	
	[self clearDepositListData];

	[depositDictionary_ release];
    depositDictionary_ = [[NSMutableDictionary alloc]initWithCapacity:0];

	if (deposits != nil) {
		for (Deposit *deposit in deposits) {
			[self addDeposit:deposit];
		}
	}
    
    [self updateFromStatusEnabledResponse:aDepositList];
	
	self.informationUpdated = aDepositList.informationUpdated;
	
}

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [self clearDepositListData];
    
    [depositDictionary_ release];
    depositDictionary_ = nil;
    
	[depositList_ release];
	depositList_ = nil;
	
	[auxDeposit_ release];
    auxDeposit_ = nil;
	
	[super dealloc];
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a DepositList instance creating the associated array
 *
 * @return The initialized DepositList instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        depositList_ = [[NSMutableArray alloc] init];
        depositDictionary_ = [[NSMutableDictionary alloc]init];
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Remove object data
 */
- (void)removeData {
	
	[depositList_ removeAllObjects];

	[depositDictionary_ removeAllObjects];
    
	[auxDeposit_ release];
	auxDeposit_ = nil;
	
	informationUpdated_ = soie_NoInfo;
	
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearDepositListData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (auxDeposit_ != nil) {
        
//		[depositList_ addObject:auxDeposit_];
//        
//		[auxDeposit_ release];
//		auxDeposit_ = nil;
        [self addDeposit:auxDeposit_];
        
	}

	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
			[auxDeposit_ release];
            auxDeposit_ = [[Deposit alloc] init];
            [auxDeposit_ setParentParseableObject:self];
            auxDeposit_.openingTag = lname;
            [parser setDelegate:auxDeposit_];
            [auxDeposit_ parserDidStartDocument: parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
	if (auxDeposit_ != nil) {
        
//		[depositList_ addObject:auxDeposit_];
//        
//		[auxDeposit_ release];
//		auxDeposit_ = nil;
        [self addDeposit:auxDeposit_];
        
	}
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the deposit list
 *
 * @return The deposit list
 */
- (NSArray *)depositList {
    
    NSMutableArray *mutableArray = [NSMutableArray array];
    
    for (NSArray *depositArray in depositList_) {
        
        [mutableArray addObjectsFromArray:depositArray];
        
    }
    
    return [NSArray arrayWithArray:mutableArray];
    
}

/*
 * Returns the deposit count
 *
 * @return The deposit count
 */
- (NSUInteger)depositCount {
    
    NSMutableArray *mutableArray = [NSMutableArray array];
    
    for (NSArray *depositArray in depositList_) {
        
        [mutableArray addObjectsFromArray:depositArray];
        
    }
    
	return [mutableArray count];
    
}

/*
 * Returns the deposit dictionary count
 *
 * @return The deposit dictionary count
 */
- (NSUInteger)depositDictionaryCount {
    
	return [depositDictionary_ count];
    
}

/*
 * Returns the Deposit located at the given position, or nil if position is not valid
 */
- (Deposit*) depositAtPosition: (NSUInteger) aPosition {
	Deposit* result = nil;
	
    NSMutableArray *mutableArray = [NSMutableArray array];
    
    for (NSArray *depositArray in depositList_) {
        
        [mutableArray addObjectsFromArray:depositArray];
        
    }
    
    
	if (aPosition < [mutableArray count]) {
		result = [mutableArray objectAtIndex: aPosition];
	}
	
	return result;
}

/*
 * Returns the Deposit position inside the list
 */
- (NSUInteger) depositPosition: (Deposit*) aDeposit {
    
    
    NSMutableArray *mutableArray = [NSMutableArray array];
    
    for (NSArray *depositArray in depositList_) {
        
        [mutableArray addObjectsFromArray:depositArray];
        
    }   
    
	return [mutableArray indexOfObject:aDeposit];
	
}

/**
 * Returns the Deposit with an Deposit number, or nil if not valid
 */
- (Deposit *)depositFromDepositNumber:(NSString *)aDepositNumber {
    
	Deposit *deposit = nil;
    
    NSMutableArray *mutableArray = [NSMutableArray array];
    
    for (NSArray *depositArray in depositList_) {
        
        [mutableArray addObjectsFromArray:depositArray];
        
    } 
    
	if (aDepositNumber != nil) {
		Deposit *target;
		NSUInteger size = [mutableArray count];
		BOOL found = NO;
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
			target = [mutableArray objectAtIndex:i];
			if ([aDepositNumber isEqualToString:target.accountNumber]) {
				deposit = target;
				found = YES;
			}
		}
	}
    
	return deposit;
    
}

@end


#pragma mark -

@implementation DepositList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearDepositListData {
    
    [depositList_ removeAllObjects];
        
    [depositDictionary_ removeAllObjects];
    
}

@end
