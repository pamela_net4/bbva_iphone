/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "AccountList.h"
#import "BankAccount.h"
#import "Tools.h"


#pragma mark -

/**
 * AccountList private category
 */
@interface AccountList(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearAccountListData;

@end


#pragma mark -

@implementation AccountList

@dynamic accountList;
@dynamic termAccountList;
@dynamic transferAccountList;
@dynamic accountCount;
@dynamic termAccountCount;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [self clearAccountListData];
    
	[accountList_ release];
	accountList_ = nil;
    
    [termAccountList_ release];
    termAccountList_ = nil;
	
    [transferAccountList_ release];
    transferAccountList_ = nil;
	
	[super dealloc];
    
}

#pragma mark -
#pragma mark Initialization

/**	
 * Object initializer
 *
 * @return An initialized instance
 */
- (id) init {
    
	if (self = [super init]) {
        
        accountList_ = [[NSMutableArray alloc] init];
        termAccountList_ = [[NSMutableArray alloc] init];
        transferAccountList_ = [[NSMutableArray alloc] init];
        informationUpdated_ = soie_NoInfo;
		self.notificationToPost = kNotificationAccountListReceived;
        
	}
	
	return self;
    
}

#pragma mark -
#pragma mark Information management

/*	
 * Updates the account list from another account list
 */
- (void)updateFrom:(AccountList *)anAccountList {
	
    NSArray *accounts = anAccountList.accountList;
    NSArray *termAccounts  = anAccountList.termAccountList;
    NSArray *transferAccounts  = anAccountList.transferAccountList;
	
	[self clearAccountListData];
	
	if (accounts != nil) {
		for (BankAccount *account in accounts) {
            [accountList_ addObject:account];
		}
	}
    
    if (termAccounts != nil) {
        for (BankAccount *account in termAccounts) {
            [termAccountList_ addObject:account];
        }
    }
	
    if (transferAccounts != nil) {
        for (BankAccount *account in transferAccounts) {
            [transferAccountList_ addObject:account];
        }
    }
	
    [self updateFromStatusEnabledResponse:anAccountList];
	
	self.informationUpdated = anAccountList.informationUpdated;
	
}

/*
 * Remove the contained data
 */
- (void)removeData {

	[self clearAccountListData];

}

/*
 * Returns the Account position inside the list
 */
- (NSUInteger)accountPosition:(BankAccount *)anAccount {
    
	return [accountList_ indexOfObject:anAccount];
	
}

/*
 * Returns the term account position inside the list
 */
- (NSUInteger)termAccountPosition:(BankAccount *)aTermAccount {
    
    return [termAccountList_ indexOfObject:aTermAccount];
    
}

/*
 * Returns the Account located at the given position, or nil if position is not valid
 */
- (BankAccount *)accountAtPosition:(NSUInteger)aPosition {
    
	BankAccount *result = nil;
	
	if (aPosition < [accountList_ count]) {
        
		result = [accountList_ objectAtIndex:aPosition];
        
	}
	
	return result;
    
}

/*
 * Returns the term account located at the given position, or nil if position is not valid
 */
- (BankAccount *)termAccountAtPosition:(NSUInteger)aPosition {
    
	BankAccount *result = nil;
	
	if (aPosition < [termAccountList_ count]) {
        
		result = [termAccountList_ objectAtIndex:aPosition];
        
	}
	
	return result;
    
}

/*
 * Returns the Account with an account number, or nil if not valid
 */
- (BankAccount *)accountFromAccountNumber:(NSString *)anAccountNumber {

	BankAccount *result = nil;
	 
	if (anAccountNumber != nil) {
        
		BankAccount *target;
		NSUInteger size = [accountList_ count];
		BOOL found = NO;
        
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
            
			target = [accountList_ objectAtIndex:i];
            
			if ([anAccountNumber isEqualToString:target.number]) {
                
				result = target;
				found = YES;
                
			}
            
		}
        
	}
	 
	return result;

}

/*
 * Returns the Account with an account 8 terminate number, or nil if not valid
 */
- (BankAccount *)accountFromAccountTerminateNumber:(NSString *)anAccountNumber {
	
	BankAccount *result = nil;
	
	if (anAccountNumber != nil) {
        
		BankAccount *target;
		NSUInteger size = [accountList_ count];
		BOOL found = NO;
        
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
            
			target = [accountList_ objectAtIndex:i];
            
			if ([anAccountNumber isEqualToString:[[target number] substringFromIndex:[[target number] length] - 8]]) {
                
				result = target;
				found = YES;
                
			}
            
		}
        
	}
	
	return result;
	
}

/*
 * Returns the term account with an account number, or nil if not valid
 */
- (BankAccount *)termAccountFromAccountNumber:(NSString *)aTermAccountNumber {
    
	BankAccount *result = nil;
    
	if (aTermAccountNumber != nil) {
        
		BankAccount *target;
		NSUInteger size = [termAccountList_ count];
		BOOL found = NO;
        
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
            
			target = [termAccountList_ objectAtIndex:i];
            
			if ([aTermAccountNumber isEqualToString:target.number]) {
                
				result = target;
				found = YES;
                
			}
            
		}
        
	}
    
	return result;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearAccountListData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (auxAccount_ != nil) {
        
        if (auxAccount_.isTermAccount) {
            
            [termAccountList_ addObject:auxAccount_];
            
        } else {
            
            if ([Tools isValidAccountForTransfers:auxAccount_]) {
                
                [transferAccountList_ addObject:auxAccount_];
                
            }
            
            [accountList_ addObject:auxAccount_];
            
        }
        
		[auxAccount_ release];
		auxAccount_ = nil;
        
	}

	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
			[auxAccount_ release];
            auxAccount_ = [[BankAccount alloc] init];
            [auxAccount_ setParentParseableObject:self];
            auxAccount_.openingTag = lname;
            [parser setDelegate:auxAccount_];
            [auxAccount_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
	if (auxAccount_ != nil) {
        
        if (auxAccount_.isTermAccount) {
            
            [termAccountList_ addObject:auxAccount_];

        } else {

            if ([Tools isValidAccountForTransfers:auxAccount_]) {
                
                [transferAccountList_ addObject:auxAccount_];
                
            }
            
            [accountList_ addObject:auxAccount_];

        }

		[auxAccount_ release];
		auxAccount_ = nil;
        
	}
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the account list
 *
 * @return The account list
 */
- (NSArray *)accountList {

    return [NSArray arrayWithArray:accountList_];
    
}

/*
 * Returns the term account list
 *
 * @return The term account list
 */
- (NSArray *)termAccountList {
    
    return [NSArray arrayWithArray:termAccountList_];
    
}

/*
 * Returns the transfer account list
 *
 * @return The transfer account list
 */
- (NSArray *)transferAccountList {
    
    return [NSArray arrayWithArray:transferAccountList_];
    
}

/*
 * Returns the accounts count
 *
 * @return The accounts count
 */
- (NSUInteger)accountCount {

    return [accountList_ count];
    
}

/*
 * Returns the term accounts count
 *
 * @return The term accounts count
 */
- (NSUInteger)termAccountCount {
    
    return [termAccountList_ count];
    
}

@end


#pragma mark -

@implementation AccountList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearAccountListData {
    
    [accountList_ removeAllObjects];
    [termAccountList_ removeAllObjects];
    [transferAccountList_ removeAllObjects];
    
    [auxAccount_ release];
    auxAccount_ = nil;
    
}

@end
