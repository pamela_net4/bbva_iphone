/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"
#import "BankAccount.h"

/**
 * Mobile carrier object
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MobileCarrier : StatusEnabledResponse {
@private
    /**
     * Code
     */
    NSMutableString *code_;
    
    /**
     * Description
     */
    NSMutableString *description_;
}

/**
 * Provides read-only access to the code
 */
@property (nonatomic, readonly, copy) NSString *code;

/**
 * Provides read-only access to the description
 */
@property (nonatomic, readonly, copy) NSString *description;

@end

/**
 * Document object
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Document : StatusEnabledResponse {
@private

    /**
     * Code
     */
    NSMutableString *code_;
    
    /**
     * Description
     */
    NSMutableString *description_;

    /**
     * Description
     */
    NSMutableString *isDefault_;

}

/**
 * Provides read-only access to the code
 */
@property (nonatomic, readonly, copy) NSString *code;

/**
 * Provides read-only access to the description
 */
@property (nonatomic, readonly, copy) NSString *description;

/**
 * Provides read-only access to the description
 */
@property (nonatomic, readonly, copy) NSString *isDefault;

@end


/**
 * Contains the transfer to third accounts startup response obtained from the server
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface TransferStartupResponse : StatusEnabledResponse {
@private
	
    /**
     * Array of amounts
     */
    NSMutableArray *amounts_;
    
    /**
     * Amount buffer
     */
    NSMutableString *auxAmount_;
    
    /**
     * Array of mobile carriers
     */
    NSMutableArray *mobileCarriers_;
    
    /**
     * Aux mobile carrier
     */
    MobileCarrier *auxMobileCarrier_;
    
    /**
     * Flag to indicate that we are analyzing mobile carriers
     */
    BOOL analyzingMobileCarriers_;
    
    /**
     * Array of documents
     */
    NSMutableArray *documents_;
    
    /**
     * Aux document
     */
    Document *auxDocument_;
    
    /**
     * Flag to indicate that we are analyzing documents
     */
    BOOL analyzingDocuments_;
    
    /**
     * Message
     */
    NSString *message_;
    
    /**
     * Aux account
     */
    BankAccount *auxAccount_;
    
    /**
     * Array of accounts
     */
    NSMutableArray *accounts_;
    
    /**
     * Flag to indicate that we are analyzing documents
     */
    BOOL analyzingAccounts_;

}

/**
 * Provides read-only access to the amounts
 */
@property (nonatomic, readonly, retain) NSMutableArray *amounts;

/**
 * Provides read-only access to the mobileCarriers
 */
@property (nonatomic, readonly, retain) NSMutableArray *mobileCarriers;

/**
 * Provides read-only access to the documents
 */
@property (nonatomic, readonly, retain) NSMutableArray *documents;

/**
 * Provides read-only access to the message
 */
@property (nonatomic, readonly, copy) NSString *message;

/**
 * Provides read-only access to the account list
 */
@property (nonatomic, readonly, retain) NSMutableArray *accounts;

@end