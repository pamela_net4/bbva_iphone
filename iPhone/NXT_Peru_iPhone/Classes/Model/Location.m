/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "Location.h"


/**
 * Defines the maximum distance allowed to determine two formatted address are the same
 */
#define MAXIMUM_DISTANCE_FOR_SAME_FORMATTED_ADDRESS                                 20.0f


#pragma mark -

@implementation Location

#pragma mark -
#pragma mark Properties

@synthesize geographicCoordinate = geographicCoordinate_;
@synthesize sensorCoordinate = sensorCoordinate_;
@synthesize formattedAddress = formattedAddress_;
@synthesize administrativeShortName = administrativeShortName_;
@synthesize country = country_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases the used memory
 */
- (void)dealloc {
    
    [formattedAddress_ release];
    formattedAddress_ = nil;
    
    [administrativeShortName_ release];
    administrativeShortName_ = nil;
    
    [country_ release];
    country_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initializer. Initializes an empy Location instance
 *
 * @return The empty initialized Location instance
 */
- (id)init {

    CLLocationCoordinate2D geographicCoordinate;
    geographicCoordinate.latitude = 0.0f;
    geographicCoordinate.longitude = 0.0f;
    return [self initWithGeographicCoordinate:geographicCoordinate
                             sensorCoordinate:NO
                             formattedAddress:nil
                      administrativeShortName:nil
                                      country:nil];
    
}

/*
 * Designated initializer. Initializes a Location instance with the provided information
 */
- (id)initWithGeographicCoordinate:(CLLocationCoordinate2D)aGeographicCoordinate
                  sensorCoordinate:(BOOL)aSensorCoordinate
                  formattedAddress:(NSString *)aFormattedAddress
           administrativeShortName:(NSString *)anAdministrativeShortName
                           country:(NSString *)aCountry {
    
    if (self = [super init]) {
        
        NSZone *zone = [self zone];
        geographicCoordinate_ = aGeographicCoordinate;
        sensorCoordinate_ = aSensorCoordinate;
        formattedAddress_ = [aFormattedAddress copyWithZone:zone];
        administrativeShortName_ = [anAdministrativeShortName copyWithZone:zone];
        country_ = [aCountry copyWithZone:zone];
        
    }
    
    return self;
    
}

/*
 * Initializes a Location instance with the provided information without neither the GoogleMaps API
 * second level administrative short name nor the GoogleMaps API country
 */
- (id)initWithGeographicCoordinate:(CLLocationCoordinate2D)aGeographicCoordinate
                  sensorCoordinate:(BOOL)aSensorCoordinate
                  formattedAddress:(NSString *)aFormattedAddress {
    
    return [self initWithGeographicCoordinate:aGeographicCoordinate
                             sensorCoordinate:aSensorCoordinate
                             formattedAddress:aFormattedAddress
                      administrativeShortName:nil
                                      country:nil];
    
}

#pragma mark -
#pragma mark Ordering and comparing selectors

/*
 * Compares one Location with another, using the formatted address as element to compare. Elements are ordered alphabetically
 */
- (NSComparisonResult)compareAlphabetically:(Location *)aLocation {
    
    return [formattedAddress_ compare:aLocation.formattedAddress];
    
}

/*
 * Compares to another Location instance to check whether both are equal. Two locations are considered to be equal if the have the
 * same formatted address and their distance is smaller to a given value
 */
- (BOOL)isEqualToLocation:(Location *)aLocation {
    
    BOOL result = NO;
    
    if (aLocation != nil) {
        
        if ([formattedAddress_ isEqualToString:aLocation.formattedAddress]) {
            
            CLLocation *curLocation = [[[CLLocation alloc] initWithLatitude:geographicCoordinate_.latitude
                                                                  longitude:geographicCoordinate_.longitude] autorelease];
            
            CLLocationCoordinate2D coordinate = aLocation.geographicCoordinate;
            CLLocation *otherLocation = [[[CLLocation alloc] initWithLatitude:coordinate.latitude
                                                      longitude:coordinate.longitude] autorelease];
            
            CLLocationDistance distanceToNewLocation = 1000.0f;
            
            if ((curLocation != nil) && (otherLocation != nil)) {
                
                if ([curLocation respondsToSelector:@selector(distanceFromLocation:)]) {
                    
                    distanceToNewLocation = [curLocation distanceFromLocation:otherLocation];
                    
                } else {
                    
                    distanceToNewLocation = [curLocation distanceFromLocation:otherLocation];
                    
                }
                
            }
            
            if (distanceToNewLocation <= MAXIMUM_DISTANCE_FOR_SAME_FORMATTED_ADDRESS) {
                
                result = YES;
                
            }
            
        }   
        
    }
    
    return result;
    
}

@end
