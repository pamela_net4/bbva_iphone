//
//  FastLoanQuota.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"


@interface FastLoanQuota : StatusEnabledResponse{
    @private

   /**
     * FastLoanQuota quotaNumber
     */
    NSString *quotaNumber_;


   /**
     * FastLoanQuota expirationDate
     */
    NSString *expirationDate_;


   /**
     * FastLoanQuota amortization
     */
    NSString *amortization_;


   /**
     * FastLoanQuota interest
     */
    NSString *interest_;


   /**
     * FastLoanQuota commissionSent
     */
    NSString *commissionSent_;


   /**
     * FastLoanQuota quotaTotal
     */
    NSString *quotaTotal_;


   /**
     * FastLoanQuota balance
     */
    NSString *balance_;




}

/**
 * Provides read-only access to the FastLoanQuota quotaNumber
 */
@property (nonatomic, readonly, copy) NSString * quotaNumber;


/**
 * Provides read-only access to the FastLoanQuota expirationDate
 */
@property (nonatomic, readonly, copy) NSString * expirationDate;


/**
 * Provides read-only access to the FastLoanQuota amortization
 */
@property (nonatomic, readonly, copy) NSString * amortization;


/**
 * Provides read-only access to the FastLoanQuota interest
 */
@property (nonatomic, readonly, copy) NSString * interest;


/**
 * Provides read-only access to the FastLoanQuota commissionSent
 */
@property (nonatomic, readonly, copy) NSString * commissionSent;


/**
 * Provides read-only access to the FastLoanQuota quotaTotal
 */
@property (nonatomic, readonly, copy) NSString * quotaTotal;


/**
 * Provides read-only access to the FastLoanQuota balance
 */
@property (nonatomic, readonly, copy) NSString * balance;




@end

