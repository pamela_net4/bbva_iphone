/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import <CoreLocation/CoreLocation.h>


/**
 * Contains a location information. It can be obtained via the GPS device or by an address search
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Location : NSObject {

@private
    
    /**
     * User geographic location
     */
    CLLocationCoordinate2D geographicCoordinate_;
    
    /**
     * Coordinate obtained via sensor flag. YES when the coordinate was obtained using a sensor such as a GPS device, NO otherwise
     */
    BOOL sensorCoordinate_;
    
    /**
     * Formatted address
     */
    NSString *formattedAddress_;
    
    /**
     * GoogleMaps API second level area administrative short name
     */
    NSString *administrativeShortName_;
    
    /**
     * GoogleMaps API contry short name
     */
    NSString *country_;
    
}

/**
 * Provides read-only access to the user geographic location
 */
@property (nonatomic, readonly, assign) CLLocationCoordinate2D geographicCoordinate;

/**
 * Provides read-write access to the coordinate obtained via sensor flag
 */
@property (nonatomic, readwrite, assign) BOOL sensorCoordinate;

/**
 * Provides read-only access to the formatted address
 */
@property (nonatomic, readonly, copy) NSString *formattedAddress;

/**
 * Provides read-only access to the GoogleMaps API second level area administrative short name
 */
@property (nonatomic, readonly, copy) NSString *administrativeShortName;

/**
 * Provides read-only access to the GoogleMaps API contry short name
 */
@property (nonatomic, readonly, copy) NSString *country;


/**
 * Designated initializer. Initializes a Location instance with the provided information
 *
 * @param aGeographicCoordinate The geographic location to store
 * @param aSensorCoordinate The coordinate obtained via sensor flag to store
 * @param aFormattedAddress The formatted address
 * @param anAdministrativeShortName The GoogleMaps API second level area administrative short name
 * @param aCountry The GoogleMaps API country short name
 * @return The initialized Location instance
 */
- (id)initWithGeographicCoordinate:(CLLocationCoordinate2D)aGeographicCoordinate
                  sensorCoordinate:(BOOL)aSensorCoordinate
                  formattedAddress:(NSString *)aFormattedAddress
           administrativeShortName:(NSString *)anAdministrativeShortName
                           country:(NSString *)aCountry;

/**
 * Initializes a Location instance with the provided information without neither the GoogleMaps API
 * second level administrative short name nor the GoogleMaps API country
 *
 * @param aGeographicCoordinate The geographic location to store
 * @param aSensorCoordinate The coordinate obtained via sensor flag to store
 * @param aFormattedAddress The formatted address
 * @return The initialized Location instance
 */
- (id)initWithGeographicCoordinate:(CLLocationCoordinate2D)aGeographicCoordinate
                  sensorCoordinate:(BOOL)aSensorCoordinate
                  formattedAddress:(NSString *)aFormattedAddress;


/**
 * Compares one Location with another, using the formatted address as element to compare. Elements are ordered alphabetically
 *
 * @param aLocation The Location instance to compare with
 * @return The result of comparing both formatted address strings
 */
- (NSComparisonResult)compareAlphabetically:(Location *)aLocation;

/**
 * Compares to another Location instance to check whether both are equal. Two locations are considered to be equal if the have the
 * same formatted address and their distance is smaller to a given value
 *
 * @param aLocation The Location instance to compare with
 * @return YES if both Location instances are considered to be equal, NO otherwise
 */
- (BOOL)isEqualToLocation:(Location *)aLocation;

@end
