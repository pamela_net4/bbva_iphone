//
//  TransferShowDetailAddtionalInformation.h
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 10/2/13.
//
//

#import "StatusEnabledResponse.h"

@interface TransferShowDetailAddtionalInformation : StatusEnabledResponse
{
@private
    /**
     * Operation number
     */
    NSString *operationNumber_;
    
    /**
     * Operation
     */
    NSString *operation_;

    /**
     * Account type
     */
    NSString *accountType_;

    /**
     * Currency Account
     */
    NSString *currencyAccount_;
    
    /**
     * Subject
     */
    NSString *subject_;
    
    /**
     * Holder of the account
     */
    NSString *holder_;
    
    /**
     * Transfer currency amount to pay
     */
    NSString *currencyAmountToPay_;
    
    /**
     * Transfer amount to pay
     */
    NSString *amountToPay_;
    
    /**
     * Transfer Beneficiary
     */
    NSString *beneficiary_;
    
    /**
     * ITF Currency
     */
    NSString *currencyITF_;
    
    /**
     * ITF
     */
    NSString *itf_;
    
    /**
     * Currency Charged Amount
     */
    NSString *currencyChargedAmount_;
    
    /**
     * Charged Amount
     */
    NSString *chargedAmount_;
    
    /**
     * Transfer state
     */
    NSString *transferState_;
    
    /**
     * Transfer date string
     */
    NSString *dateString_;
    
    /**
     * Transfer expiration date string
     */
    NSString *expirationDateString_;
    
    /**
     * Transfer hour string
     */
    NSString *hourString_;

    /**
     * Transfer expiration hour string
     */
    NSString *expirationHourString_;
    
    /**
     *  Max number of resends
     */
    NSString *maxResendString_;
}

/**
 * Provides read-only access to the operation number
 */
@property (nonatomic, readonly, copy) NSString *operationNumber;

/**
 * Provides read-only access to the operation
 */
@property (nonatomic, readonly, copy) NSString *operation;

/**
 * Provides read-only access to the account type
 */
@property (nonatomic, readonly, copy) NSString *accountType;

/**
 * Provides read-only access to the currency account
 */
@property (nonatomic, readonly, copy) NSString *currencyAccount;

/**
 * Provides read-only access to the Subject
 */
@property (nonatomic, readonly, copy) NSString *subject;

/**
 * Provides read-only access to the holder
 */
@property (nonatomic, readonly, copy) NSString *holder;

/**
 * Provides read-only access to the beneficiary
 */
@property (nonatomic, readonly, copy) NSString *beneficiary;

/**
 * Provides read-only access to the transfer currencyAmountToPay string
 */
@property (nonatomic, readonly, copy) NSString *currencyAmountToPay;

/**
 * Provides read-only access to the transfer amountToPay string
 */
@property (nonatomic, readonly, copy) NSString *amountToPay;

/**
 * Provides read-only access to the transfer currency itf
 */
@property (nonatomic, readonly, copy) NSString *currencyItf;

/**
 * Provides read-only access to the transfer itf
 */
@property (nonatomic, readonly, copy) NSString *itf;

/**
 * Provides read-only access to the transfer currency charged amount string
 */
@property (nonatomic, readonly, copy) NSString *currencyChargedAmount;

/**
 * Provides read-only access to the transfer charged amount string
 */
@property (nonatomic, readonly, copy) NSString *chargedAmount;

/**
 * Provides read-only access to the state of the transfer
 */
@property (nonatomic, readonly, copy) NSString *transferState;

/**
 * Provides read-only access to the transfer date string
 */
@property (nonatomic, readonly, copy) NSString *dateString;

/**
 * Provides read-only access to the transfer expiration date string
 */
@property (nonatomic, readonly, copy) NSString *expirationDateString;

/**
 * Provides read-only access to the transfer hour string
 */
@property (nonatomic, readonly, copy) NSString *hourString;

/**
 * Provides read-only access to the transfer expiration hour string
 */
@property (nonatomic, readonly, copy) NSString *expirationHourString;

/**
 * Provides read-only access to the max Resend number string
 */
@property (nonatomic, readonly, copy) NSString *maxResendString;

@end
