/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


/**
 * Contains a key string and its value string
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface KeyAndValueStrings : NSObject {
    
@private
    
    /**
     * Key string
     */
    NSString *key_;
    
    /**
     * Value string
     */
    NSString *value_;
    
}

/**
 * Provides read-only access to the key string
 */
@property (nonatomic, readonly, copy) NSString *key;

/**
 * Provides read-only access to the value string
 */
@property (nonatomic, readonly, copy) NSString *value;


/**
 * Designated initializer. Initializes a KeyAndValueStrings instance with the provided information
 *
 * @param aKey The key string to store
 * @param aValue The value string to store
 * @return The initialized KeyAndValueStrings instance
 */
- (id)initWithKey:(NSString *)aKey andValue:(NSString *)aValue;

@end
