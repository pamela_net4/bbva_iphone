/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class BankAccount;


/**
 * Contains the list of accounts
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountList : StatusEnabledResponse {
    
@private
	
	/**
	 * Accounts list array
	 */
	NSMutableArray *accountList_;
    
    /**
     * Term accounts list array
     */
    NSMutableArray *termAccountList_;
    
    /**
     * Transfer accounts list array
     */
    NSMutableArray *transferAccountList_;
    
	/**
	 * Auxiliar account object for parsing
	 */
	BankAccount *auxAccount_;
    
}

/**	
 * Provides read-only access to the accounts list array
 */
@property (nonatomic, readonly, retain) NSArray *accountList;

/**	
 * Provides read-only access to the term accounts list array
 */
@property (nonatomic, readonly, retain) NSArray *termAccountList;

/**	
 * Provides read-only access to the transfer accounts list array
 */
@property (nonatomic, readonly, retain) NSArray *transferAccountList;

/**
 * Provides read-only access to the accounts count
 */
@property (nonatomic, readonly, assign) NSUInteger accountCount;

/**
 * Provides read-only access to the term accounts count
 */
@property (nonatomic, readonly, assign) NSUInteger termAccountCount;

/**
 * Updates this instance with the given account list
 *
 * @param anAccountList The account list to update from
 */
- (void)updateFrom:(AccountList *)anAccountList;

/**
 * Returns the Account position inside the list
 *
 * @param  anAccount Account object which position we are looking for
 * @return Account position inside the list
 */
- (NSUInteger)accountPosition:(BankAccount *)anAccount;

/**
 * Returns the term account position inside the list
 *
 * @param  aTermAccount Term account object which position we are looking for
 * @return The term account position inside the list
 */
- (NSUInteger)termAccountPosition:(BankAccount *)aTermAccount;

/**
 * Returns the Account located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the Account is located
 * @return Account located at the given position, or nil if position is not valid
 */
- (BankAccount *)accountAtPosition:(NSUInteger)aPosition;

/**
 * Returns the term account located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the term account is located
 * @return Term account located at the given position, or nil if position is not valid
 */
- (BankAccount *)termAccountAtPosition:(NSUInteger)aPosition;

/**
 * Returns the BankAccount with an account number, or nil if not valid
 *
 * @param  anAccountNumber The account number
 * @return BankAccount with this number, or nil if not valid
 */
- (BankAccount *)accountFromAccountNumber:(NSString *)anAccountNumber;

/**
 * Returns the BankAccount with an account 8 terminate number, or nil if not valid
 *
 * @param  anAccountNumber The account number
 * @return BankAccount with this number, or nil if not valid
 */
- (BankAccount *)accountFromAccountTerminateNumber:(NSString *)anAccountNumber;

/**
 * Returns the term account with an account number, or nil if not valid
 *
 * @param  aTermAccountNumber The term account number
 * @return The term account with this number, or nil if not valid
 */
- (BankAccount *)termAccountFromAccountNumber:(NSString *)aTermAccountNumber;

/**
 * Remove the contained data
 */
- (void)removeData;
	
@end
