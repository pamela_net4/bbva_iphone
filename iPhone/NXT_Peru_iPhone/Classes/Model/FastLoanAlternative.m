//
//  FastLoanAlternative.m
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "FastLoanAlternative.h"
#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     flaeas_AnalyzingNumber = serxeas_ElementsCount, //!<Analyzing the FastLoanAlternative number
     flaeas_AnalyzingQuotaValue , //!<Analyzing the FastLoanAlternative quotaValue
     flaeas_AnalyzingQuotaType , //!<Analyzing the FastLoanAlternative quotaType
     flaeas_AnalyzingMonthDobles , //!<Analyzing the FastLoanAlternative monthDobles
     flaeas_AnalyzingTerm , //!<Analyzing the FastLoanAlternative term
     flaeas_AnalyzingTcea , //!<Analyzing the FastLoanAlternative tcea
     flaeas_AnalyzingQuotaTotal , //!<Analyzing the FastLoanAlternative quotaTotal

} FastLoanAlternativeXMLElementAnalyzerState;

@implementation FastLoanAlternative

#pragma mark -
#pragma mark Properties

@synthesize number = number_;
@synthesize quotaValue = quotaValue_;
@synthesize quotaType = quotaType_;
@synthesize monthDobles = monthDobles_;
@synthesize term = term_;
@synthesize tcea = tcea_;
@synthesize quotaTotal = quotaTotal_;
;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [number_ release];
    number_ = nil;

    [quotaValue_ release];
    quotaValue_ = nil;

    [quotaType_ release];
    quotaType_ = nil;

    [monthDobles_ release];
    monthDobles_ = nil;

    [term_ release];
    term_ = nil;

    [tcea_ release];
    tcea_ = nil;

    [quotaTotal_ release];
    quotaTotal_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if(0==1){
                   //delete fix
        }        else if ([lname isEqualToString: @"num_alternativa"]) {

            xmlAnalysisCurrentValue_ = flaeas_AnalyzingNumber;


        }
        else if ([lname isEqualToString: @"alt_valor_cuota"]) {

            xmlAnalysisCurrentValue_ = flaeas_AnalyzingQuotaValue;


        }
        else if ([lname isEqualToString: @"alt_tipo_cuota"]) {

            xmlAnalysisCurrentValue_ = flaeas_AnalyzingQuotaType;


        }
        else if ([lname isEqualToString: @"alt_meses_cuotas_dobles"]) {

            xmlAnalysisCurrentValue_ = flaeas_AnalyzingMonthDobles;


        }
        else if ([lname isEqualToString: @"alt_plazo"]) {

            xmlAnalysisCurrentValue_ = flaeas_AnalyzingTerm;


        }
        else if ([lname isEqualToString: @"alt_tcea"]) {

            xmlAnalysisCurrentValue_ = flaeas_AnalyzingTcea;


        }
        else if ([lname isEqualToString: @"alt_total_cuota"]) {

            xmlAnalysisCurrentValue_ = flaeas_AnalyzingQuotaTotal;


        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(0==1){
           //delete fix
        }        else if (xmlAnalysisCurrentValue_ == flaeas_AnalyzingNumber) {

            [number_ release];
            number_ = nil;
            number_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flaeas_AnalyzingQuotaValue) {

            [quotaValue_ release];
            quotaValue_ = nil;
            quotaValue_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flaeas_AnalyzingQuotaType) {

            [quotaType_ release];
            quotaType_ = nil;
            quotaType_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flaeas_AnalyzingMonthDobles) {

            [monthDobles_ release];
            monthDobles_ = nil;
            monthDobles_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flaeas_AnalyzingTerm) {

            [term_ release];
            term_ = nil;
            term_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flaeas_AnalyzingTcea) {

            [tcea_ release];
            tcea_ = nil;
            tcea_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flaeas_AnalyzingQuotaTotal) {

            [quotaTotal_ release];
            quotaTotal_ = nil;
            quotaTotal_ = [elementString copyWithZone:self.zone];

        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

