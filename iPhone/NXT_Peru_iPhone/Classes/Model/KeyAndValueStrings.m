/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "KeyAndValueStrings.h"


#pragma mark -

@implementation KeyAndValueStrings

#pragma mark -
#pragma mark Properties

@synthesize key = key_;
@synthesize value = value_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [key_ release];
    key_ = nil;
    
    [value_ release];
    value_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Superclass designated initialized. Releases the instance and returns a nil instance, as empty KeyAndValueStrings instances are not allowed
 *
 * @return Always nil
 */
- (id)init {
    
    [self autorelease];
    
    return nil;
    
}

/*
 * Designated initializer. Initializes a KeyAndValueStrings instance with the provided information
 */
- (id)initWithKey:(NSString *)aKey andValue:(NSString *)aValue {
    
    if (self = [super init]) {
        
        key_ = [aKey copy];
        value_ = [aValue copy];
        
    }
    
    return self;
    
}

@end
