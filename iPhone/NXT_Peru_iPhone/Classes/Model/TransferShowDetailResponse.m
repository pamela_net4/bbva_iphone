//
//  TransferShowDetailResponse.m
//  NXT_Peru_iPad
//
//  Created by Ricardo Herrera Valle on 10/2/13.
//
//

#import "TransferShowDetailResponse.h"
#import "TransferShowDetailAddtionalInformation.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    tsrxeas_AnalyzingAdditionalInformation = serxeas_ElementsCount, //!<Analyzing the additional information
    
} TransferSuccessResponseXMLElementAnalyzerState;

#pragma mark
/**
 * TransactionDetailResponse private category
 */
@interface TransferShowDetailResponse(private)
/**
 * Clears the object data
 *
 * @private
 */
- (void)clearTransferDetailResponseData;

@end

#pragma mark -

@implementation TransferShowDetailResponse

#pragma mark -
#pragma mark Properties

@synthesize additionalInformation = additionalInformation_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearTransferDetailResponseData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a TransferSuccessResponse instance providing the associated notification
 *
 * @return The initialized TransferDetailResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationTransferSuccessResponseReceived;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    [self clearTransferDetailResponseData];
    
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"informacionadicional"]) {
            
            xmlAnalysisCurrentValue_ = tsrxeas_AnalyzingAdditionalInformation;
			[additionalInformation_ release];
            additionalInformation_ = nil;
            additionalInformation_ = [[TransferShowDetailAddtionalInformation alloc] init];
            additionalInformation_.openingTag = lname;
            [additionalInformation_ setParentParseableObject:self];
            [parser setDelegate:additionalInformation_];
            [additionalInformation_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        if ([lname isEqualToString: @"msg-s"]) {
            
            informationUpdated_ = soie_InfoAvailable;
            [parser setDelegate:parentParseableObject_];
            parentParseableObject_ = nil;
            
        }
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
    }
    
}

@end


#pragma mark -

@implementation TransferShowDetailResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearTransferDetailResponseData {
    
    [additionalInformation_ release];
    additionalInformation_ = nil;
    
    [additionalInformation_ release];
    additionalInformation_ = nil;
    
}

@end
