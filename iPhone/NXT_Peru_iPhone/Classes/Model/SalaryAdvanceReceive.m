//
//  SalaryAdvanceReceive.m
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "SalaryAdvanceReceive.h"
#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     sareas_AnalyzingCustomer = serxeas_ElementsCount, //!<Analyzing the SalaryAdvanceReceive customer
     sareas_AnalyzingCommission , //!<Analyzing the SalaryAdvanceReceive commission
     sareas_AnalyzingAmountAdvance , //!<Analyzing the SalaryAdvanceReceive amountAdvance
     sareas_AnalyzingTotalToPay , //!<Analyzing the SalaryAdvanceReceive totalToPay
     sareas_AnalyzingDayOfPay , //!<Analyzing the SalaryAdvanceReceive dayOfPay
     sareas_AnalyzingSeal , //!<Analyzing the SalaryAdvanceReceive seal
     sareas_AnalyzingCoordinate , //!<Analyzing the SalaryAdvanceReceive coordinate

} SalaryAdvanceReceiveXMLElementAnalyzerState;

@implementation SalaryAdvanceReceive

#pragma mark -
#pragma mark Properties

@synthesize customer = customer_;
@synthesize commission = commission_;
@synthesize amountAdvance = amountAdvance_;
@synthesize totalToPay = totalToPay_;
@synthesize dayOfPay = dayOfPay_;
@synthesize seal = seal_;
@synthesize coordinate = coordinate_;
;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [customer_ release];
    customer_ = nil;

    [commission_ release];
    commission_ = nil;

    [amountAdvance_ release];
    amountAdvance_ = nil;

    [totalToPay_ release];
    totalToPay_ = nil;

    [dayOfPay_ release];
    dayOfPay_ = nil;

    [seal_ release];
    seal_ = nil;

    [coordinate_ release];
    coordinate_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if(0==1){
                   //delete fix
        }        else if ([lname isEqualToString: @"nombrecliente"]) {

            xmlAnalysisCurrentValue_ = sareas_AnalyzingCustomer;


        }
        else if ([lname isEqualToString: @"comisionadelanto"]) {

            xmlAnalysisCurrentValue_ = sareas_AnalyzingCommission;


        }
        else if ([lname isEqualToString: @"montoadelanto"]) {

            xmlAnalysisCurrentValue_ = sareas_AnalyzingAmountAdvance;


        }
        else if ([lname isEqualToString: @"montototal"]) {

            xmlAnalysisCurrentValue_ = sareas_AnalyzingTotalToPay;


        }
        else if ([lname isEqualToString: @"mensaje_diapago"]) {

            xmlAnalysisCurrentValue_ = sareas_AnalyzingDayOfPay;


        }
        else if ([lname isEqualToString: @"sello"]) {

            xmlAnalysisCurrentValue_ = sareas_AnalyzingSeal;


        }
        else if ([lname isEqualToString: @"coordenada"]) {

            xmlAnalysisCurrentValue_ = sareas_AnalyzingCoordinate;


        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(0==1){
           //delete fix
        }        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingCustomer) {

            [customer_ release];
            customer_ = nil;
            customer_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingCommission) {

            [commission_ release];
            commission_ = nil;
            commission_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingAmountAdvance) {

            [amountAdvance_ release];
            amountAdvance_ = nil;
            amountAdvance_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingTotalToPay) {

            [totalToPay_ release];
            totalToPay_ = nil;
            totalToPay_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingDayOfPay) {

            [dayOfPay_ release];
            dayOfPay_ = nil;
            dayOfPay_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingSeal) {

            [seal_ release];
            seal_ = nil;
            seal_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == sareas_AnalyzingCoordinate) {

            [coordinate_ release];
            coordinate_ = nil;
            coordinate_ = [elementString copyWithZone:self.zone];

        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

