//
//  FastLoanSimulation.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
@class FastLoanAlternativeList;


@interface FastLoanSimulation : StatusEnabledResponse{
    @private

   /**
     * FastLoanSimulation amount
     */
    NSString *amount_;


   /**
     * FastLoanSimulation amountFormated
     */
    NSString *amountFormated_;


   /**
     * FastLoanSimulation teaFormated
     */
    NSString *teaFormated_;


   /**
     * FastLoanSimulation dayOfPay
     */
    NSString *dayOfPay_;


   /**
     * FastLoanSimulation currencySymbol
     */
    NSString *currencySymbol_;


   /**
     * FastLoanSimulation fastLoanAlternativeList
     */
    FastLoanAlternativeList *fastLoanAlternativeList_;




}

/**
 * Provides read-only access to the FastLoanSimulation amount
 */
@property (nonatomic, readonly, copy) NSString * amount;


/**
 * Provides read-only access to the FastLoanSimulation amountFormated
 */
@property (nonatomic, readonly, copy) NSString * amountFormated;


/**
 * Provides read-only access to the FastLoanSimulation teaFormated
 */
@property (nonatomic, readonly, copy) NSString * teaFormated;


/**
 * Provides read-only access to the FastLoanSimulation dayOfPay
 */
@property (nonatomic, readonly, copy) NSString * dayOfPay;


/**
 * Provides read-only access to the FastLoanSimulation currencySymbol
 */
@property (nonatomic, readonly, copy) NSString * currencySymbol;


/**
 * Provides read-only access to the FastLoanSimulation fastLoanAlternativeList
 */
@property (nonatomic, readonly, copy) FastLoanAlternativeList * fastLoanAlternativeList;




@end

