/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


#pragma mark -

/**
 * StatusEnabledResponse private category
 */
@interface StatusEnabledResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearStatusEnabledResponseData;

@end


#pragma mark -

@implementation StatusEnabledResponse

#pragma mark -
#pragma mark Properties

@synthesize errorCode = errorCode_;
@synthesize errorMessage = errorMessage_;
@synthesize isError = isError_;
@synthesize isLoginError = isLoginError_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearStatusEnabledResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Updating information

/*
 * Updates the status enabled response from another status enabled response
 */
- (void)updateFromStatusEnabledResponse:(StatusEnabledResponse *)aStatusEnabledResponse {

	if (errorMessage_ != aStatusEnabledResponse.errorMessage) {
		
		[errorMessage_ release];
		errorMessage_ = nil;
		errorMessage_ = [aStatusEnabledResponse.errorMessage copyWithZone:self.zone];
		
	}
	
	if (errorCode_ != aStatusEnabledResponse.errorCode) {
		
		[errorCode_ release];
		errorCode_ = nil;
		errorCode_ = [aStatusEnabledResponse.errorCode copyWithZone:self.zone];
		
	}
	
	isError_ = aStatusEnabledResponse.isError;
	isLoginError_ = aStatusEnabledResponse.isLoginError;
		
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearStatusEnabledResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	NSString *lname = [elementName lowercaseString];
	
    if (([lname isEqualToString: @"error"]) || ([lname isEqualToString: @"err-s"])) {
        
        isError_ = YES;
        xmlAnalysisCurrentValue_ = serxeas_AnalyzingError;
        
    } else if (([lname isEqualToString:@"cod"] || [lname isEqualToString: @"codigoerror"]) && (isError_)) {
        
		xmlAnalysisCurrentValue_ = serxeas_AnalyzingErrorCode;
        
    } else if ((([lname isEqualToString: @"mensaje"]) || ([lname isEqualToString: @"msg"])) && (isError_)) {
        
		xmlAnalysisCurrentValue_ = serxeas_AnalyzingErrorMessage;
        
	} else {
        
		xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
//	if (xmlAnalysisCurrentValue_ == serxeas_AnalyzingErrorMessage) {
//        
//        [errorMessage_ release];
//        errorMessage_ = nil;
//        errorMessage_ = [elementString copyWithZone:self.zone];
//        informationUpdated_ = soie_Incomplete;
//		xmlAnalysisCurrentValue_ = serxeas_AnalyzingError;
//        
//	} else if (xmlAnalysisCurrentValue_ == serxeas_AnalyzingErrorCode) {
//        
//        [errorCode_ release];
//        errorCode_ = nil;
//        errorCode_ = [elementString copyWithZone:self.zone];
//        informationUpdated_ = soie_Incomplete;
//		xmlAnalysisCurrentValue_ = serxeas_AnalyzingError;
//        
//	} else if (xmlAnalysisCurrentValue_ == serxeas_AnalyzingError) {
//        
//        xmlAnalysisCurrentValue_ = serxeas_Nothing;
//        
//    }
    
	NSString *lname = [elementName lowercaseString];
    if (([lname isEqualToString:@"cod"] || [lname isEqualToString: @"codigoerror"]) && (isError_)) {
        
        [errorCode_ release];
        errorCode_ = nil;
        errorCode_ = [elementString copyWithZone:self.zone];
        informationUpdated_ = soie_Incomplete;
		xmlAnalysisCurrentValue_ = serxeas_AnalyzingError;
        
    } else if ((([lname isEqualToString: @"mensaje"]) || ([lname isEqualToString: @"msg"])) && (isError_)) {
        
        [errorMessage_ release];
        errorMessage_ = nil;
        errorMessage_ = [elementString copyWithZone:self.zone];
        informationUpdated_ = soie_Incomplete;
		xmlAnalysisCurrentValue_ = serxeas_AnalyzingError;
        
	}    
    
    
}

@end


#pragma mark -

@implementation StatusEnabledResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearStatusEnabledResponseData {
    
    [errorMessage_ release];
    errorMessage_ = nil;
    
    [errorCode_ release];
    errorCode_ = nil;
    
    isError_ = NO;
    isLoginError_ = NO;
    
}

@end
