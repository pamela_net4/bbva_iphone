/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class AccountFastLoan;


/**
 * Contains the list of AccountFastLoans
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountFastLoanList : StatusEnabledResponse {

@private

	/**
	 * AccountFastLoans list array
	 */
	NSMutableArray *accountfastloanList_;

	/**
	 * Auxiliar AccountFastLoan object for parsing
	 */
	AccountFastLoan *auxAccountFastLoan_;

}

/**
 * Provides read-only access to the accountfastloans list array
 */
@property (nonatomic, readonly, retain) NSArray *accountfastloanList;

/**
 * Provides read access to the number of AccountFastLoans stored
 */
@property (nonatomic, readonly, assign) NSUInteger accountfastloanCount;

/**
 * Returns the AccountFastLoan located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the AccountFastLoan is located
 *
 * @return AccountFastLoan located at the given position, or nil if position is not valid
 */
- (AccountFastLoan *)accountfastloanAtPosition:(NSUInteger)aPosition;

/**
 * Returns the AccountFastLoan position inside the list
 *
 * @param  aAccountFastLoan AccountFastLoan object which position we are looking for
 *
 * @return AccountFastLoan position inside the list
 */
- (NSUInteger)accountfastloanPosition:(AccountFastLoan *)aAccountFastLoan;

/**
 * Returns the Laon with an accountfastloan number, or nil if not valid
 *
 * @param  aAccountFastLoanId The accountfastloan id
 * @return AccountFastLoan with this id, or nil if not valid
 */
//- (AccountFastLoan *)accountfastloanFromAccountFastLoanId:(NSString *)aAccountFastLoanId;

/**
 * Updates the AccountFastLoan list from another AccountFastLoanList instance
 *
 * @param  aAccountFastLoanList The AccountFastLoanList list to update from
 */
- (void)updateFrom:(AccountFastLoanList *)aAccountFastLoanList;

/**
 * Remove the contained data
 */
- (void)removeData;

@end

