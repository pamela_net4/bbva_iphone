/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "TitleAndAttributes.h"


#pragma mark -

@implementation TitleAndAttributes

#pragma mark -
#pragma mark Properties

@synthesize titleString = titleString_;
@dynamic attributesArray;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [titleString_ release];
    titleString_ = nil;
    
    [attributesArray_ release];
    attributesArray_ = nil;
    
    [orientationAttributesDictionary_ release];
    orientationAttributesDictionary_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Creates an autoreleased TitleAndAttributes instance
 */
+ (TitleAndAttributes *)titleAndAttributes {
    
    return [[[TitleAndAttributes alloc] init] autorelease];
    
}

/**
 * Superclass designated initialization. Initializes a TitleAndAttributes instance by creating the necesary elements
 *
 * @return The initialized TitleAndAttributes instance
 */
- (id)init {
    
    if ((self = [super init])) {
        
        attributesArray_ = [[NSMutableArray alloc] init];
        
        orientationAttributesDictionary_ = [[NSMutableDictionary alloc] init];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Attributes list management

/*
 * Adds a string to the attributes array
 */
- (void)addAttribute:(NSString *)attribute {
    
    [self addAttribute:attribute alignTo:UITextAlignmentLeft];
    
}

/**
 * Adds a string to the attributes array and orientation.
 *
 * @param attribute: The attribute to add to the attributes array.
 * @param align: A TitleAndAttributesAlign orientation.
 */
- (void)addAttribute:(NSString *)attribute alignTo:(UITextAlignment)align {
    
    if ([attribute length] > 0) {
        
        [attributesArray_ addObject:attribute];
        
        [orientationAttributesDictionary_  setObject:[NSNumber numberWithInt:align] forKey:attribute];
        
    }
    
}

/*
 * Get title and attribute align for attribute.
 */
- (UITextAlignment)getOrientationOfAttribute:(NSString *)attribute {
    
    return [[orientationAttributesDictionary_ objectForKey:attribute] intValue];
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the title string
 *
 * @return The title string
 */
- (NSString *)titleString {
    
    return ([titleString_ length] > 0) ? titleString_ : @"";
    
}

/*
 * Returns the attributes array
 *
 * @return The attributes array
 */
- (NSArray *)attributesArray {
    
    return [NSArray arrayWithArray:attributesArray_];
    
}

@end
