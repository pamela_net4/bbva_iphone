/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class CardTransactionList;
@class CardTransactionsAdditionalInformation;


/**
 * Credit card class contains information about a credit card
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Card : StatusEnabledResponse {
    
@private
    
    /**
     * Card number
     */
    NSString *cardNumber_;
    
    /**
     * Card type
     */
    NSString *cardType_;
    
    /**
     * Available credit string
     */
    NSString *availableCreditString_;
    
    /**
     * Available credit
     */
    NSDecimalNumber *availableCredit_;
    
    /**
     * Credit limit string
     */
    NSString *creditLimitString_;
    
    /**
     * Credit limit
     */
    NSDecimalNumber *creditLimit_;
    
    /**
     * Currency
     */
    NSString *currency_;
    
    /**
     * Type
     */
    NSString *type_;
    
    /**
     * Contract
     */
    NSString *contract_;
    
    /**
     * Permission
     */
    NSString *permission_;
    
    /**
     * State
     */
    NSString *state_;
    
    /**
     * Subject
     */
    NSString *subject_;
    
    /**
     * Credit converted into country currency string
     */
    NSString *creditInCountryCurrencyString_;
    
    /**
     * Credit converted into country currency
     */
    NSDecimalNumber *creditInCountryCurrency_;
    
    /**
     * Card transactions list to display
     */
    CardTransactionList *cardTransactionsList_;
    
    /**
     * Contains transactions additional information
     */
    CardTransactionsAdditionalInformation *transactionsAdditionalInformation_;

}

/**
 * Provides read-only access to the card number
 */
@property (nonatomic, readonly, copy) NSString *cardNumber;

/**
 * Provides read-only access to the card type
 */
@property (nonatomic, readonly, copy) NSString *cardType;

/**
 * Provides read-only access to the available credit string
 */
@property (nonatomic, readonly, copy) NSString *availableCreditString;

/**
 * Provides read-only access to the available credit
 */
@property (nonatomic, readonly, retain) NSDecimalNumber *availableCredit;

/**
 * Provides read-only access to the credit limit string
 */
@property (nonatomic, readonly, copy) NSString *creditLimitString;

/**
 * Provides read-only access to the ccredit limit
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *creditLimit;

/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the type
 */
@property (nonatomic, readonly, copy) NSString *type;

/**
 * Provides read-only access to the contract
 */
@property (nonatomic, readonly, copy) NSString *contract;

/**
 * Provides read-only access to the permission
 */
@property (nonatomic, readonly, copy) NSString *permission;

/**
 * Provides read-only access to the state
 */
@property (nonatomic, readonly, copy) NSString *state;

/**
 * Provides read-only access to the subject
 */
@property (nonatomic, readonly, copy) NSString *subject;

/**
 * Provides read-only access to the credit converted into country currency string
 */
@property (nonatomic, readonly, copy) NSString *creditInCountryCurrencyString;

/**
 * Provides read-only access to the credit converted into country currency
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *creditInCountryCurrency;

/**
 * Provides read-only access to the card transactions list to display
 */
@property (nonatomic, readonly, retain) CardTransactionList *cardTransactionsList;

/**
 * Provides read-only access to the contains transactions additional information
 */
@property (nonatomic, readonly, retain) CardTransactionsAdditionalInformation *transactionsAdditionalInformation;


/**
 * Returns the card list display name. It can be used whenever a card name must be displayed inside a list
 *
 * @return The card list display name
 */
- (NSString *)cardListName;

/**
 * Returns the obfuscated card number
 *
 * @return The obfuscated card number
 */
- (NSString *)obfuscatedCardNumber;


/**
 * Updates this instance with the given card
 *
 * @param aCard The card to update from
 */
- (void)updateFrom:(Card *)aCard;

/**
 * Updates the transactions list
 *
 * @param aCardTransactionsList The card transactions list to update from
 */
- (void)updateCardTransactionsList:(CardTransactionList *)aCardTransactionsList;

/**
 * Updates the transactions additional information
 *
 * @param aTransactionsAdditionalInformation The card transactions additional information to update from
 */
- (void)updateCardTransactionsAdditionalInformation:(CardTransactionsAdditionalInformation *)aTransactionsAdditionalInformation;

/**
 * Returns the card identifier when displayed to the user. It consists of the obfuscated card number
 *
 * @return The card identifier when displayed to the user
 */
- (NSString *)cardIdentifierDisplayedToTheUser;

@end
