/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "LoanList.h"
#import "Loan.h"


#pragma mark -

/**
 * LoanList private category
 */
@interface LoanList (private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearLoanListData;

@end


#pragma mark -

@implementation LoanList

@dynamic loanList;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [self clearLoanListData];

	[loanList_ release];
	loanList_ = nil;
	
	[super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a LoanList instance creating the associated array
 *
 * @return The initialized LoanList instance
 */
- (id)init {

    if (self = [super init]) {
        
        loanList_ = [[NSMutableArray alloc] init];

    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*	
 * Updates the Loan list from another LoanList instance
 */
- (void)updateFrom:(LoanList *)aLoanList {
    
    NSArray *otherLoanList = aLoanList.loanList;
    
	[self clearLoanListData];
    
    for (Loan *loan in otherLoanList) {
        
        [loanList_ addObject:loan];
        
    }
	
    [self updateFromStatusEnabledResponse:aLoanList];
	
	self.informationUpdated = aLoanList.informationUpdated;
	
}

/*
 * Remove object data
 */
- (void)removeData {
	
    [self clearLoanListData];
	
}

#pragma mark -
#pragma mark Getters and setters

/**
 * Provides read access to the number of Loan stored
 *
 * @return The count
 */
- (NSUInteger) loanCount {
	return [loanList_ count];
}

/*
 * Returns the Loan located at the given position, or nil if position is not valid
 */
- (Loan*) loanAtPosition: (NSUInteger) aPosition {
	Loan* result = nil;
	
	if (aPosition < [loanList_ count]) {
		result = [loanList_ objectAtIndex: aPosition];
	}
	
	return result;
}

/*
 * Returns the Loan position inside the list
 */
- (NSUInteger) loanPosition: (Loan*) aLoan {
    
	return [loanList_ indexOfObject:aLoan];
	
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearLoanListData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (auxLoan_ != nil) {
        
		[loanList_ addObject:auxLoan_];
		[auxLoan_ release];
		auxLoan_ = nil;
        
	}

	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
            auxLoan_ = [[Loan alloc] init];
            [auxLoan_ setParentParseableObject:self];
            auxLoan_.openingTag = lname;
            [parser setDelegate:auxLoan_];
            [auxLoan_ parserDidStartDocument: parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (auxLoan_ != nil) {
        
		[loanList_ addObject:auxLoan_];
		[auxLoan_ release];
		auxLoan_ = nil;
        
	}
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the loan list array
 *
 * @return The loan list array
 */
- (NSArray *)loanList {

    return [NSArray arrayWithArray:loanList_];
    
}

/**
 * Returns the Loan with an Loan number, or nil if not valid
 */
- (Loan *)loanFromLoanNumber:(NSString *)aLoanNumber {
    
	Loan *loan = nil;
    
	if (aLoanNumber != nil) {
		Loan *target;
		NSUInteger size = [loanList_ count];
		BOOL found = NO;
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
			target = [loanList_ objectAtIndex:i];
			if ([aLoanNumber isEqualToString:target.number]) {
				loan = target;
				found = YES;
			}
		}
	}
    
	return loan;
    
}
@end


#pragma mark -

@implementation LoanList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearLoanListData {
    
    [loanList_ removeAllObjects];
    
	informationUpdated_ = soie_NoInfo;

}

@end
