/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */

#import "GlobalPositionResponse.h"
#import "AccountList.h"
#import "CardList.h"
#import "DepositList.h"
#import "LoanList.h"
#import "StockMarketAccountList.h"
#import "MutualFundList.h"
#import "GlobalAdditionalInformation.h"


/**
 * Enumerates the analysis states
 */
typedef enum {
    
    gprxeas_AnalyzingAccountList = serxeas_ElementsCount, //!<Analyzing the accounts list
    gprxeas_AnalyzingCardList, //!<Analyzing the cards list
	gprxeas_AnalyzingDepositList, //!<Analyzing the desposits list
    gprxeas_AnalyzingLoanList, //!<Analyzing the loan list
    gprxeas_AnalyzingStockMarketAccountList, //!<Analyzing the stock market accounts list
    gprxeas_AnalyzingMutualFundList, //!<Analyzing the mutual funds list
    gprxeas_AnalyzingAdditionalInformation //!<Analyzing the additional information
    
} GlobalPositionResponseXMLElementAnalyzerState;


#pragma mark -

/**
 * GlobalPositionResponse private category
 */
@interface GlobalPositionResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearGlobalPositionResponseData;

@end


#pragma mark -

@implementation GlobalPositionResponse

@synthesize accountList = accountList_;
@synthesize cardList = cardList_;
@synthesize depositsList = depositsList_;
@synthesize loansList = loansList_;
@synthesize stockMarketAccountsList = stockMarketAccountsList_;
@synthesize mutualFundsList = mutualFundsList_;
@synthesize additionalInformation = additionalInformation_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearGlobalPositionResponseData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a GlobalPositionResponse instance providing the associated notification
 *
 * @return The initialized GlobalPositionResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationGlobalPositionResponseReceived;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearGlobalPositionResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"listcta"]) {
            
            xmlAnalysisCurrentValue_ = gprxeas_AnalyzingAccountList;
			[accountList_ release];
            accountList_ = nil;
            accountList_ = [[AccountList alloc] init];
            accountList_.openingTag = lname;
            [accountList_ setParentParseableObject:self];
            [parser setDelegate:accountList_];
            [accountList_ parserDidStartDocument:parser];
            
            
        } else if ([lname isEqualToString: @"listtarj"]) {
            
            xmlAnalysisCurrentValue_ = gprxeas_AnalyzingCardList;
			[cardList_ release];
            cardList_ = nil;
            cardList_ = [[CardList alloc] init];
            cardList_.openingTag = lname;
            [cardList_ setParentParseableObject:self];
            [parser setDelegate:cardList_];
            [cardList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString: @"listcts"]) {
            
            xmlAnalysisCurrentValue_ = gprxeas_AnalyzingDepositList;
			[depositsList_ release];
            depositsList_ = nil;
            depositsList_ = [[DepositList alloc] init];
            depositsList_.openingTag = lname;
            [depositsList_ setParentParseableObject:self];
            [parser setDelegate:depositsList_];
            [depositsList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString: @"listprestamos"]) {
            
            xmlAnalysisCurrentValue_ = gprxeas_AnalyzingLoanList;
			[loansList_ release];
            loansList_ = nil;
            loansList_ = [[LoanList alloc] init];
            loansList_.openingTag = lname;
            [loansList_ setParentParseableObject:self];
            [parser setDelegate:loansList_];
            [loansList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString: @"listbolsa"]) {
            
            xmlAnalysisCurrentValue_ = gprxeas_AnalyzingStockMarketAccountList;
			[stockMarketAccountsList_ release];
            stockMarketAccountsList_ = [[StockMarketAccountList alloc] init];
            stockMarketAccountsList_.openingTag = lname;
            [stockMarketAccountsList_ setParentParseableObject:self];
            [parser setDelegate:stockMarketAccountsList_];
            [stockMarketAccountsList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString: @"listmutuos"]) {
            
            xmlAnalysisCurrentValue_ = gprxeas_AnalyzingMutualFundList;
			[mutualFundsList_ release];
            mutualFundsList_ = nil;
            mutualFundsList_ = [[MutualFundList alloc] init];
            mutualFundsList_.openingTag = lname;
            [mutualFundsList_ setParentParseableObject:self];
            [parser setDelegate:mutualFundsList_];
            [mutualFundsList_ parserDidStartDocument:parser];
            
        } else if ([lname isEqualToString: @"informacionadicional"]) {
            
            xmlAnalysisCurrentValue_ = gprxeas_AnalyzingAdditionalInformation;
			[additionalInformation_ release];
            additionalInformation_ = nil;
            additionalInformation_ = [[GlobalAdditionalInformation alloc] init];
            additionalInformation_.openingTag = lname;
            [additionalInformation_ setParentParseableObject:self];
            [parser setDelegate:additionalInformation_];
            [additionalInformation_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        if ([lname isEqualToString: @"msg-s"]) {
            
            informationUpdated_ = soie_InfoAvailable;
            [parser setDelegate:parentParseableObject_];
            parentParseableObject_ = nil;
            
        }
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    }
    
}

@end


#pragma mark -

@implementation GlobalPositionResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearGlobalPositionResponseData {
    
    [accountList_ release];
    accountList_ = nil;
    
    [cardList_ release];
    cardList_ = nil;
    
    [depositsList_ release];
    depositsList_ = nil;
    
    [loansList_ release];
    loansList_ = nil;
    
    [stockMarketAccountsList_ release];
    stockMarketAccountsList_ = nil;
	
    [mutualFundsList_ release];
    mutualFundsList_ = nil;
    
    [additionalInformation_ release];
    additionalInformation_ = nil;

}

@end
