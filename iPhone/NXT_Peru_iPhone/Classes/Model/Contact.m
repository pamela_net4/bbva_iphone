/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "Contact.h"

#pragma mark -

@implementation Contact

#pragma mark -
#pragma mark Properties

@synthesize name = name_;
@synthesize phoneNumber = phoneNumber_;
@synthesize carrier = carrier_;
@synthesize sendSMS = sendSMS_;
@synthesize sendMail = sendMail_;
@synthesize email = email_;
@synthesize photo = photo_;
@synthesize secondEmail = secondEmail_;
@synthesize telephoneList = telephoneList_;
@synthesize emailList = emailList_;


#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
	[name_ release];
    name_ = nil;
	
	[email_ release];
    email_ = nil;
	
	[photo_ release];
    photo_ = nil;
	
	[phoneNumber_ release];
    phoneNumber_ = nil;
	
	[carrier_ release];
    carrier_ = nil;
	
	[secondEmail_ release];
    secondEmail_ = nil;
	
	[telephoneList_ release];
    telephoneList_ = nil;
    
    [emailList_ release];
    emailList_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Initializes a contact instance given its name
 */
- (id)initWithName:(NSString *)aName {
    
    return [self initWithName:aName phoneNumber:nil];
    
}

/*
 * Designated initializer. Initializes a contact instance given its name and its phone number
 */
- (id)initWithName:(NSString *)aName phoneNumber:(NSString *)aPhoneNumber {
    
    if (self = [super init]) {
        
        name_ = [aName copy];
        phoneNumber_ = [aPhoneNumber copy];
        sendSMS_ = NO;
        sendMail_ = NO;
        email_ = nil;
        photo_ = nil;
        secondEmail_ = nil;
        telephoneList_ = [[NSMutableArray alloc] init];
        emailList_ = [[NSMutableArray alloc] init];
        
    }
    
    return self;
    
}

@end
