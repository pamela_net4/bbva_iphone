/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


/**
 * Stores a general server response, consisting in a status code
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface GeneralStatusResponse : StatusEnabledResponse {

@private
    
    /**
     * Status code
     */
    NSString *status_;
    
}


/**
 * Provides read-write access to the status code
 */
@property (nonatomic, readwrite, copy) NSString *status;

@end
