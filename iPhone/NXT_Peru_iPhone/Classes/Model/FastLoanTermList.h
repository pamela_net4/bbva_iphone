/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class FastLoanTerm;


/**
 * Contains the list of FastLoanTerms
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface FastLoanTermList : StatusEnabledResponse {

@private

	/**
	 * FastLoanTerms list array
	 */
	NSMutableArray *fastloantermList_;

	/**
	 * Auxiliar FastLoanTerm object for parsing
	 */
	FastLoanTerm *auxFastLoanTerm_;

}

/**
 * Provides read-only access to the fastloanterms list array
 */
@property (nonatomic, readonly, retain) NSArray *fastloantermList;

/**
 * Provides read access to the number of FastLoanTerms stored
 */
@property (nonatomic, readonly, assign) NSUInteger fastloantermCount;

/**
 * Returns the FastLoanTerm located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the FastLoanTerm is located
 *
 * @return FastLoanTerm located at the given position, or nil if position is not valid
 */
- (FastLoanTerm *)fastloantermAtPosition:(NSUInteger)aPosition;

/**
 * Returns the FastLoanTerm position inside the list
 *
 * @param  aFastLoanTerm FastLoanTerm object which position we are looking for
 *
 * @return FastLoanTerm position inside the list
 */
- (NSUInteger)fastloantermPosition:(FastLoanTerm *)aFastLoanTerm;

/**
 * Returns the Laon with an fastloanterm number, or nil if not valid
 *
 * @param  aFastLoanTermId The fastloanterm id
 * @return FastLoanTerm with this id, or nil if not valid
 */
//- (FastLoanTerm *)fastloantermFromFastLoanTermId:(NSString *)aFastLoanTermId;

/**
 * Updates the FastLoanTerm list from another FastLoanTermList instance
 *
 * @param  aFastLoanTermList The FastLoanTermList list to update from
 */
- (void)updateFrom:(FastLoanTermList *)aFastLoanTermList;

/**
 * Remove the contained data
 */
- (void)removeData;

@end

