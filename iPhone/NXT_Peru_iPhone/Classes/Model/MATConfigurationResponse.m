/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "MATConfigurationResponse.h"

@implementation MATConfigurationResponse

@synthesize methodsList = methodsList_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    [methodsList_ release];
    methodsList_ = nil;
    
    [auxMethod_ release];
    auxMethod_ = nil;
    
	[super dealloc];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    [super parserDidStartDocument:parser];
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    [methodsList_ release];
    methodsList_ = [[NSMutableArray alloc] init];
    
    [auxMethod_ release];
    auxMethod_ = [[NSMutableString alloc] initWithString:@""];
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
	
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	NSString *lname = [elementName lowercaseString];
	
	if ([auxMethod_ length] > 0) {
		[methodsList_ addObject:auxMethod_];
		[auxMethod_ release];
		auxMethod_ = [[NSMutableString alloc] initWithString:@""];
	}
    
    if ([lname isEqualToString: @"method"]) {
        xmlAnalysisCurrentValue_ = mrxeas_AnalyzingMethod;
	} else {
      	xmlAnalysisCurrentValue_ = serxeas_Nothing;  
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
	NSString *lname = [elementName lowercaseString];
    
    if ([lname isEqualToString:@"matconfiguration"]) { // Insertion of the last method
        
        if ([auxMethod_ length] > 0) {
            [methodsList_ addObject:auxMethod_];
            [auxMethod_ release];
            auxMethod_ = [[NSMutableString alloc] initWithString:@""];
        }
        
		informationUpdated_ = soie_InfoAvailable;
		[parser setDelegate:parentParseableObject_];
    }

    xmlAnalysisCurrentValue_ = serxeas_AnalyzingUnknown;
}

/**
 * Sent by a parser object to provide its delegate with a string representing all or part of the characters of the current element.
 * Characters found are appended to the current element being analyzed
 *
 * @param parser A parser object
 * @param string A string representing the complete or partial textual content of the current element
 */
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    [super parser:parser foundCharacters:string];
    
    if ([string length] > 0) {

        switch (xmlAnalysisCurrentValue_) {
            case mrxeas_AnalyzingMethod:                
                [auxMethod_ appendString:string];
                break;
            default:
                break;
        }
            
    }

}

@end
