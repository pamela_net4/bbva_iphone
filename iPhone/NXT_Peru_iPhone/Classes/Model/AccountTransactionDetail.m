/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "AccountTransactionDetail.h"
#import "Tools.h"

/**
 * Enumerates the analysis state
 */
typedef enum {
    
    atatdxeas_AnalyzingDescription = serxeas_ElementsCount, //!<Analyzing description
	atatdxeas_AnalyzingOperationHour, //!<Analyzing operation hour
    atatdxeas_AnalyzingAccountingDate, //!<Analyzing accounting date
    atatdxeas_AnalyzingCheckNumber, //!<Analyzing check number
    atatdxeas_AnalyzingType, //!<Analyzing type
    atatdxeas_AnalyzingCenter, //!<Analyzing center
    atatdxeas_AnalyzingLiteral //!<Analyzing literal
    
} AccountTransactionsDetailXMLElementAnalyzerState;


#pragma mark -

/**
 * AccountTransactionDetail private category
 */
@interface AccountTransactionDetail(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearAccountTransactionDetailData;

@end


#pragma mark -

@implementation AccountTransactionDetail

#pragma mark -
#pragma mark Properties

@synthesize description = description_;
@synthesize operationHour = operationHour_;
@synthesize accountingDateString = accountingDateString_;
@synthesize accountingDate = accountingDate_;
@synthesize checkNumber = checkNumber_;
@synthesize type = type_;
@synthesize center = center_;
@synthesize literal = literal_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearAccountTransactionDetailData];
	
	[super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initialized. Initializes a AccountTransactionAdditionalnformation instance
 *
 * @return The initialized AccountTransactionAdditionalnformation instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationAccountTransactionsAdditionalnformationUpdated;
        
    }
    
    return self;
    
}


#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearAccountTransactionDetailData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"descripcion"]) {
            
            xmlAnalysisCurrentValue_ = atatdxeas_AnalyzingDescription;
            
        } else if ([lname isEqualToString: @"horaoperacion"]) {
            
            xmlAnalysisCurrentValue_ = atatdxeas_AnalyzingOperationHour;
            
        } else if ([lname isEqualToString: @"fechacontable"]) {
            
            xmlAnalysisCurrentValue_ = atatdxeas_AnalyzingAccountingDate;
            
        } else if ([lname isEqualToString: @"numerocheque"]) {
            
            xmlAnalysisCurrentValue_ = atatdxeas_AnalyzingCheckNumber;
            
        } else if ([lname isEqualToString: @"tipo"]) {
            
            xmlAnalysisCurrentValue_ = atatdxeas_AnalyzingType;
            
        } else if ([lname isEqualToString: @"centro"]) {
            
            xmlAnalysisCurrentValue_ = atatdxeas_AnalyzingCenter;
            
        } else if ([lname isEqualToString: @"literal"]) {
            
            xmlAnalysisCurrentValue_ = atatdxeas_AnalyzingLiteral;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == atatdxeas_AnalyzingDescription) {
            
            [description_ release];
            description_ = nil;
            description_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == atatdxeas_AnalyzingOperationHour) {
            
            [operationHour_ release];
            operationHour_ = nil;
            operationHour_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == atatdxeas_AnalyzingAccountingDate) {
            
            [accountingDateString_ release];
            accountingDateString_ = nil;
            accountingDateString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == atatdxeas_AnalyzingCheckNumber) {
            
            [checkNumber_ release];
            checkNumber_ = nil;
            checkNumber_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == atatdxeas_AnalyzingType) {
            
            [type_ release];
            type_ = nil;
            type_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == atatdxeas_AnalyzingCenter) {
            
            [center_ release];
            center_ = nil;
            center_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == atatdxeas_AnalyzingLiteral) {
            
            [literal_ release];
            literal_ = nil;
            literal_ = [elementString copyWithZone:self.zone];
            
        } 
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

/*
 * Returns the value date
 *
 * @return The value date
 */
- (NSDate *)accountingDate {
    
    NSDate *result = accountingDate_;
    
    if (result == nil) {
        
        if ([accountingDateString_ length] > 0) {
            
            accountingDate_ = [[Tools dateFromServerString:accountingDateString_] retain];
            result = accountingDate_;
            
        }
        
    }
    
    return result;
    
}


@end


#pragma mark -

@implementation AccountTransactionDetail(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearAccountTransactionDetailData {
    
    [description_ release];
    description_ = nil;
    
    [operationHour_ release];
    operationHour_ = nil;
    
    [accountingDateString_ release];
    accountingDateString_ = nil;
    
    [accountingDate_ release];
    accountingDate_ = nil;
    
    [checkNumber_ release];
    checkNumber_ = nil;
    
    [type_ release];
    type_ = nil;
    
    [center_ release];
    center_ = nil;
    
    [literal_ release];
    literal_ = nil;
    
}

@end
