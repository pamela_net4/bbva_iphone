/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "FastLoanAlternativeList.h"
#import "FastLoanAlternative.h"


#pragma mark -

/**
 * FastLoanAlternativeList private category
 */
@interface FastLoanAlternativeList (private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFastLoanAlternativeListData;

@end


#pragma mark -

@implementation FastLoanAlternativeList

@dynamic fastloanalternativeList;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [self clearFastLoanAlternativeListData];

	[fastloanalternativeList_ release];
	fastloanalternativeList_ = nil;

	[super dealloc];

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a FastLoanAlternativeList instance creating the associated array
 *
 * @return The initialized FastLoanAlternativeList instance
 */
- (id)init {

    if (self = [super init]) {

        fastloanalternativeList_ = [[NSMutableArray alloc] init];

    }

    return self;

}

#pragma mark -
#pragma mark Information management

/*
 * Updates the FastLoanAlternative list from another FastLoanAlternativeList instance
 */
- (void)updateFrom:(FastLoanAlternativeList *)aFastLoanAlternativeList {

    NSArray *otherFastLoanAlternativeList = aFastLoanAlternativeList.fastloanalternativeList;

	[self clearFastLoanAlternativeListData];

    for (FastLoanAlternative *fastloanalternative in otherFastLoanAlternativeList) {

        [fastloanalternativeList_ addObject:fastloanalternative];

    }

    [self updateFromStatusEnabledResponse:aFastLoanAlternativeList];

	self.informationUpdated = aFastLoanAlternativeList.informationUpdated;

}

/*
 * Remove object data
 */
- (void)removeData {

    [self clearFastLoanAlternativeListData];

}

#pragma mark -
#pragma mark Getters and setters

/**
 * Provides read access to the number of FastLoanAlternative stored
 *
 * @return The count
 */
- (NSUInteger) fastloanalternativeCount {
	return [fastloanalternativeList_ count];
}

/*
 * Returns the FastLoanAlternative located at the given position, or nil if position is not valid
 */
- (FastLoanAlternative*) fastloanalternativeAtPosition: (NSUInteger) aPosition {
	FastLoanAlternative* result = nil;

	if (aPosition < [fastloanalternativeList_ count]) {
		result = [fastloanalternativeList_ objectAtIndex: aPosition];
	}

	return result;
}

/*
 * Returns the FastLoanAlternative position inside the list
 */
- (NSUInteger) fastloanalternativePosition: (FastLoanAlternative*) aFastLoanAlternative {

	return [fastloanalternativeList_ indexOfObject:aFastLoanAlternative];

}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {

    [super parserDidStartDocument:parser];

	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFastLoanAlternativeListData];

}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {

    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];

	if (auxFastLoanAlternative_ != nil) {

		[fastloanalternativeList_ addObject:auxFastLoanAlternative_];
		[auxFastLoanAlternative_ release];
		auxFastLoanAlternative_ = nil;

	}

	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        NSString* lname = [elementName lowercaseString];

        if ([lname isEqualToString: @"e"]) {

            auxFastLoanAlternative_ = [[FastLoanAlternative alloc] init];
            [auxFastLoanAlternative_ setParentParseableObject:self];
            auxFastLoanAlternative_.openingTag = lname;
            [parser setDelegate:auxFastLoanAlternative_];
            [auxFastLoanAlternative_ parserDidStartDocument: parser];

        } else {

            xmlAnalysisCurrentValue_ = serxeas_Nothing;

        }

    }

}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {

    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];

    if (auxFastLoanAlternative_ != nil) {

		[fastloanalternativeList_ addObject:auxFastLoanAlternative_];
		[auxFastLoanAlternative_ release];
		auxFastLoanAlternative_ = nil;

	}

    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        informationUpdated_ = soie_InfoAvailable;

    }

    xmlAnalysisCurrentValue_ = serxeas_Nothing;

}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the fastloanalternative list array
 *
 * @return The fastloanalternative list array
 */
- (NSArray *)fastloanalternativeList {

    return [NSArray arrayWithArray:fastloanalternativeList_];

}

/**
 * Returns the FastLoanAlternative with an FastLoanAlternative id, or nil if not valid
 *//*
- (FastLoanAlternative *)fastloanalternativeFromFastLoanAlternativeId:(NSString *)aFastLoanAlternativeId {

	FastLoanAlternative *fastloanalternative = nil;

	if (aFastLoanAlternativeId != nil) {
		FastLoanAlternative *target;
		NSUInteger size = [fastloanalternativeList_ count];
		BOOL found = NO;
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
			target = [fastloanalternativeList_ objectAtIndex:i];
			if ([aFastLoanAlternativeId isEqualToString:target.id]) {
				fastloanalternative = target;
				found = YES;
			}
		}
	}

	return fastloanalternative;

}*/
@end


#pragma mark -

@implementation FastLoanAlternativeList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFastLoanAlternativeListData {

    [fastloanalternativeList_ removeAllObjects];

	informationUpdated_ = soie_NoInfo;

}

@end

