/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "RetentionList.h"
#import "Retention.h"


#pragma mark -

/**
 * RetentionList private category
 */
@interface RetentionList(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearRetentionListData;

@end


#pragma mark -

@implementation RetentionList

#pragma mark -
#pragma mark Properties

@dynamic retentionsList;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {
    
    [self clearRetentionListData];
    
    [retentionsList_ release];
    retentionsList_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a RetentionList instance creating the associated array
 *
 * @return The initialized RetentionList instance
 */
- (id)init {
    
    if (self = [super init]) {
        
        retentionsList_ = [[NSMutableArray alloc] init];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates the retentions list from another retentions list
 */
- (void)updateFrom:(RetentionList *)aRetentionList {
    
    NSArray *otherRetentionListArray = aRetentionList.retentionsList;
    
    [self clearRetentionListData];
    
    for (Retention *retention in otherRetentionListArray) {
        
        [retentionsList_ addObject:retention];
        
    }
    
    [self updateFromStatusEnabledResponse:aRetentionList];
    
    self.informationUpdated = aRetentionList.informationUpdated;
    
}

/*
 * Remove the contained data
 */
- (void)removeData {
    
    [self clearRetentionListData];
    
}

#pragma mark -
#pragma mark Getters and setters

/*
 * Returns the Retention located at the given position, or nil if position is not valid
 */
- (Retention *)retentionAtPosition:(NSUInteger)aPosition {
    
	Retention *result = nil;
	
	if (aPosition < [retentionsList_ count]) {
        
		result = [retentionsList_ objectAtIndex:aPosition];
        
	}
	
	return result;
    
}

/**
 * Provides read access to the number of retentions stored
 *
 * @return The count
 */
- (NSUInteger) retentionsCount {
	return [retentionsList_ count];
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearRetentionListData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (auxRetention_ != nil) {
        
        [retentionsList_ addObject:auxRetention_];
        [auxRetention_ release];
        auxRetention_ = nil;
        
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
            auxRetention_ = [[Retention alloc] init];
            [auxRetention_ setParentParseableObject:self];
            auxRetention_.openingTag = lname;
            [parser setDelegate:auxRetention_];
            [auxRetention_ parserDidStartDocument:parser];
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (auxRetention_ != nil) {
        
        [retentionsList_ addObject:auxRetention_];
        [auxRetention_ release];
        auxRetention_ = nil;
        
    }
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the retentions list array
 *
 * @return The retentions list array
 */
- (NSArray *)retentionsList {
    
    return [NSArray arrayWithArray:retentionsList_];
    
}

@end


#pragma mark -

@implementation RetentionList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearRetentionListData {
    
    [retentionsList_ removeAllObjects];
    
    [auxRetention_ release];
    auxRetention_ = nil;
    
}

@end
