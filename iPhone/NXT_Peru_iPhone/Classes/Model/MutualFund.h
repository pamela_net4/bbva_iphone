/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


/**
 * Mutual fund class contains information about a mutual fund the user is investing in
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface MutualFund : StatusEnabledResponse {

@private
    
    /**
     * Mutual fund number
     */
    NSString *number_;
    
    /**
     * Mutual fund type
     */
    NSString *type_;
    
    /**
     * Mutual fund currency
     */
    NSString *currency_;
    
    /**
     * Mutual fund value string
     */
    NSString *valueString_;
    
    /**
     * Mutual fund value
     */
    NSDecimalNumber *value_;
    
    /**
     * Mutual fund share value string
     */
    NSString *shareValueString_;
    
    /**
     * Mutual fund share value
     */
    NSDecimalNumber *shareValue_;
    
    /**
     * Mutual fund total share value string
     */
    NSString *totalShareString_;
    
    /**
     * Mutual fund total share value
     */
    NSDecimalNumber *totalShare_;
    
    /**
     * Mutual fund subject
     */
    NSString *subject_;
    
    /**
     * Value converted into country currency string
     */
    NSString *valueInCountryCurrencyString_;
    
    /**
     * Value converted into country currency
     */
    NSDecimalNumber *valueInCountryCurrency_;
    
}

/**
 * Provides read-only access to the mutual fund number
 */
@property (nonatomic, readonly, copy) NSString *number;

/**
 * Provides read-only access to the mutual fund type
 */
@property (nonatomic, readonly, copy) NSString *type;

/**
 * Provides read-only access to the mutual fund currency
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the mutual fund value string
 */
@property (nonatomic, readonly, copy) NSString *valueString;

/**
 * Provides read-only access to the mutual fund value
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *value;

/**
 * Provides read-only access to the mutual fund share value string
 */
@property (nonatomic, readonly, copy) NSString *shareValueString;

/**
 * Provides read-only access to the mutual fund share value
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *shareValue;

/**
 * Provides read-only access to the mutual fund total share value string
 */
@property (nonatomic, readonly, copy) NSString *totalShareString;

/**
 * Provides read-only access to the mutual fund total share value
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *totalShare;

/**
 * Provides read-only access to the mutual fund subject
 */
@property (nonatomic, readonly, copy) NSString *subject;

/**
 * Provides read-only access to the value converted into cuntry currency string
 */
@property (nonatomic, readonly, copy) NSString *valueInCountryCurrencyString;

/**
 * Provides read-only access to the value converted into country currency
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *valueInCountryCurrency;


/**
 * Returns the mutual fund list display name. It can be used whenever a fund name must be displayed inside a list
 *
 * @return The mutual fund list display name
 */
- (NSString *)mutualFundListName;

/**
 * Returns the obfuscated mutual fund number
 *
 * @return The obfuscated mutual fund number
 */
- (NSString *)obfuscatedMutualFundNumber;

@end
