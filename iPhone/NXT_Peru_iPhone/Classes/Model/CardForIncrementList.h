/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class CardForIncrement;


/**
 * Contains the list of CardForIncrements
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardForIncrementList : StatusEnabledResponse {

@private

	/**
	 * CardForIncrements list array
	 */
	NSMutableArray *cardforincrementList_;

	/**
	 * Auxiliar CardForIncrement object for parsing
	 */
	CardForIncrement *auxCardForIncrement_;

}

/**
 * Provides read-only access to the cardforincrements list array
 */
@property (nonatomic, readonly, retain) NSArray *cardforincrementList;

/**
 * Provides read access to the number of CardForIncrements stored
 */
@property (nonatomic, readonly, assign) NSUInteger cardforincrementCount;

/**
 * Returns the CardForIncrement located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the CardForIncrement is located
 *
 * @return CardForIncrement located at the given position, or nil if position is not valid
 */
- (CardForIncrement *)cardforincrementAtPosition:(NSUInteger)aPosition;

/**
 * Returns the CardForIncrement position inside the list
 *
 * @param  aCardForIncrement CardForIncrement object which position we are looking for
 *
 * @return CardForIncrement position inside the list
 */
- (NSUInteger)cardforincrementPosition:(CardForIncrement *)aCardForIncrement;

/**
 * Returns the Laon with an cardforincrement number, or nil if not valid
 *
 * @param  aCardForIncrementId The cardforincrement id
 * @return CardForIncrement with this id, or nil if not valid
 */
//- (CardForIncrement *)cardforincrementFromCardForIncrementId:(NSString *)aCardForIncrementId;

/**
 * Updates the CardForIncrement list from another CardForIncrementList instance
 *
 * @param  aCardForIncrementList The CardForIncrementList list to update from
 */
- (void)updateFrom:(CardForIncrementList *)aCardForIncrementList;

/**
 * Remove the contained data
 */
- (void)removeData;

@end

