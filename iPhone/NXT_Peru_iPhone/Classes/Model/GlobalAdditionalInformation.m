/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GlobalAdditionalInformation.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
    gaixeas_AnalyzingCustomer = serxeas_ElementsCount, //!<Analyzing the customer name
	gaixeas_AnalyzingLastAccess, //!<Analyzing the holder last access
    gaixeas_AnalyzingAccumulatedPoints, //!<Analyzing the accumulated points
    gaixeas_AnalyzingExecutiveName, //!<Analyzing the executive name
    gaixeas_AnalyzingPurchaseExchangeRate, //!<Analyzing the purchase exchange rate
    gaixeas_AnalyzingSaleExchangeRate, //!<Analyzing the sale exchange rate
    gaixeas_AnalyzingEMail, //!<Analyzing the holder e-mail
    gaixeas_AnalyzingExecutiveEMail, //!<Analyzing the executive e-mail
    gaixeas_AnalyzingCustomerType, //!<Analyzing the customer type
    gaixeas_AnalyzingBranch, //!<Analyzing the customer branch
    gaixeas_AnalyzingMobilePhoneNumber, //!<Analyzing the customer mobile phone number
    gaixeas_AnalyzingCarrier, //!<Analyzing the customer mobile phone carrier
    gaixeas_AnalyzingMessageCTS1, //!<Analyzing the Message for group code type 1
    gaixeas_AnalyzingMessageCTS2, //!<Analyzing the Message for group code type 2
    gaixeas_AnalyzingOTPActivationService, //!<Analyzing the activation of otp service
    gaixeas_AnalyzingOTPUsage, //!<Analyzing the usage of otp
    gaixeas_AnalyzingTitularName //!<Analyzing the name of the titlar
    
} GlobalAdditionalInformationXMLElementAnalyzerState;


#pragma mark -

/**
 * GlobalAdditionalInformation private category
 */
@interface GlobalAdditionalInformation(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearGlobalAdditionalInformationData;

@end


#pragma mark -

@implementation GlobalAdditionalInformation

#pragma mark -
#pragma mark Properties

@synthesize customer = customer_;
@synthesize lastAccess = lastAccess_;
@synthesize accumulatedPoints = accumulatedPoints_;
@synthesize executiveName = executiveName_;
@synthesize purchaseExchangeRate = purchaseExchangeRate_;
@synthesize saleExchangeRate = saleExchangeRate_;
@synthesize eMail = eMail_;
@synthesize executiveEMail = executiveEMail_;
@synthesize customerType = customerType_;
@synthesize branch = branch_;
@synthesize mobilePhoneNumber = mobilePhoneNumber_;
@synthesize carrier = carrier_;
@synthesize messageCTS1 = messageCTS1_;
@synthesize messageCTS2 = messageCTS2_;
@synthesize otpActive = otpActive_;
@synthesize otpUsage = otpUsage_;
@synthesize titularName = titularName_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {

    [self clearGlobalAdditionalInformationData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates the global position additiona information from another global position additional information
 */
- (void)updateFrom:(GlobalAdditionalInformation *)anAdditionalInformation {
    
    NSZone *zone = self.zone;
    
    [customer_ release];
    customer_ = nil;
    customer_ = [anAdditionalInformation.customer copyWithZone:zone];
    
    [lastAccess_ release];
    lastAccess_ = nil;
    lastAccess_ = [anAdditionalInformation.lastAccess copyWithZone:zone];
    
    [accumulatedPoints_ release];
    accumulatedPoints_ = nil;
    accumulatedPoints_ = [anAdditionalInformation.accumulatedPoints copyWithZone:zone];
    
    [executiveName_ release];
    executiveName_ = nil;
    executiveName_ = [anAdditionalInformation.executiveName copyWithZone:zone];
    
    [purchaseExchangeRate_ release];
    purchaseExchangeRate_ = nil;
    purchaseExchangeRate_ = [anAdditionalInformation.purchaseExchangeRate copyWithZone:zone];
    
    [saleExchangeRate_ release];
    saleExchangeRate_ = nil;
    saleExchangeRate_ = [anAdditionalInformation.saleExchangeRate copyWithZone:zone];
    
    [eMail_ release];
    eMail_ = nil;
    eMail_ = [anAdditionalInformation.eMail copyWithZone:zone];
    
    [executiveEMail_ release];
    executiveEMail_ = nil;
    executiveEMail_ = [anAdditionalInformation.executiveEMail copyWithZone:zone];
    
    [customerType_ release];
    customerType_ = nil;
    customerType_ = [anAdditionalInformation.customerType copyWithZone:zone];
    
    [branch_ release];
    branch_ = nil;
    branch_ = [anAdditionalInformation.branch copyWithZone:zone];
    
    [mobilePhoneNumber_ release];
    mobilePhoneNumber_ = nil;
    mobilePhoneNumber_ = [anAdditionalInformation.mobilePhoneNumber copyWithZone:zone];
    
    [carrier_ release];
    carrier_ = nil;
    carrier_ = [anAdditionalInformation.carrier copyWithZone:zone];
    
    [messageCTS1_ release];
    messageCTS1_ = nil;
    messageCTS1_ = [anAdditionalInformation.messageCTS1 copyWithZone:zone];
    
    [messageCTS2_ release];
    messageCTS2_ = nil;
    messageCTS2_ = [anAdditionalInformation.messageCTS2 copyWithZone:zone];
    
    otpActive_ = [anAdditionalInformation otpActive];
    
    otpUsage_ = [anAdditionalInformation otpUsage];
    
    [titularName_ release];
    titularName_ = nil;
    titularName_ = [[anAdditionalInformation titularName] copyWithZone:zone];
    
}


/*
 * Remove the contained data
 */
- (void)removeData {
    
    [self clearGlobalAdditionalInformationData];
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearGlobalAdditionalInformationData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"titular"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingCustomer;
            
        } else if ([lname isEqualToString: @"ultimoacceso"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingLastAccess;
            
        } else if ([lname isEqualToString: @"puntosacumulados"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingAccumulatedPoints;
            
        } else if ([lname isEqualToString: @"nombreejecutivo"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingExecutiveName;
            
        } else if ([lname isEqualToString: @"tccompra"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingPurchaseExchangeRate;
            
        } else if ([lname isEqualToString: @"tcventa"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingSaleExchangeRate;
            
        } else if ([lname isEqualToString: @"email"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingEMail;
            
        } else if ([lname isEqualToString: @"emailejecutivo"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingExecutiveEMail;
            
        } else if ([lname isEqualToString: @"indcliente"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingCustomerType;
            
        } else if ([lname isEqualToString: @"oficina"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingBranch;
            
        } else if ([lname isEqualToString: @"celular"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingMobilePhoneNumber;
            
        } else if ([lname isEqualToString: @"operadora"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingCarrier;
            
        } else if ([lname isEqualToString: @"mensajects1"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingMessageCTS1;
            
        } else if ([lname isEqualToString: @"mensajects2"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingMessageCTS2;
            
        } else if ([lname isEqualToString:@"indicadorafiliacion"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingOTPActivationService;
            
        } else if ([lname isEqualToString:@"indicadoruso"]) {
            
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingOTPUsage;
            
        } else if ([lname isEqualToString:@"nombretitular"]) {
        
            xmlAnalysisCurrentValue_ = gaixeas_AnalyzingTitularName;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingCustomer) {
            
            [customer_ release];
            customer_ = nil;
            customer_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingLastAccess) {
            
            [lastAccess_ release];
            lastAccess_ = nil;
            lastAccess_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingAccumulatedPoints) {
            
            [accumulatedPoints_ release];
            accumulatedPoints_ = nil;
            accumulatedPoints_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingExecutiveName) {
            
            [executiveName_ release];
            executiveName_ = nil;
            executiveName_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingPurchaseExchangeRate) {
            
            [purchaseExchangeRate_ release];
            purchaseExchangeRate_ = nil;
            purchaseExchangeRate_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingSaleExchangeRate) {
            
            [saleExchangeRate_ release];
            saleExchangeRate_ = nil;
            saleExchangeRate_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingEMail) {
            
            [eMail_ release];
            eMail_ = nil;
            eMail_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingEMail) {
            
            [eMail_ release];
            eMail_ = nil;
            eMail_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingExecutiveEMail) {
            
            [executiveEMail_ release];
            executiveEMail_ = nil;
            executiveEMail_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingCustomerType) {
            
            [customerType_ release];
            customerType_ = nil;
            customerType_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingBranch) {
            
            [branch_ release];
            branch_ = nil;
            branch_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingMobilePhoneNumber) {
            
            [mobilePhoneNumber_ release];
            mobilePhoneNumber_ = nil;
            mobilePhoneNumber_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingCarrier) {
            
            [carrier_ release];
            carrier_ = nil;
            carrier_ = [elementString copyWithZone:self.zone];
            
        }  else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingMessageCTS1) {
            
            [messageCTS1_ release];
            messageCTS1_ = nil;
            messageCTS1_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingMessageCTS2) {
            
            [messageCTS2_ release];
            messageCTS2_ = nil;
            messageCTS2_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingOTPActivationService) {
            
            BOOL optActive = NO;
            
            if ([[elementString lowercaseString] isEqualToString:@"s"]) {
                
                optActive = YES;
                
            } 
                
            otpActive_ = optActive;
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingOTPUsage) {
            
            NSInteger otpUsage = [elementString intValue];
            
            if (otpUsage == 1) {
                
                otpUsage_ = otp_UsageOTP;
                
            } else if ((otpUsage == 2) || (otpUsage == 3)) {
                
                otpUsage_ = otp_UsageTC;
                
            } else {
                
                otpUsage_ = otp_UsageUnknown;
                
            }
            
            
        } else if (xmlAnalysisCurrentValue_ == gaixeas_AnalyzingTitularName) {
            
            [titularName_ release];
            titularName_ = nil;
            titularName_ = [elementString copyWithZone:[self zone]];
            
        }
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end


#pragma mark -

@implementation GlobalAdditionalInformation(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearGlobalAdditionalInformationData {
    
    [customer_ release];
    customer_ = nil;
    
    [lastAccess_ release];
    lastAccess_ = nil;
    
    [accumulatedPoints_ release];
    accumulatedPoints_ = nil;
    
    [executiveName_ release];
    executiveName_ = nil;
    
    [purchaseExchangeRate_ release];
    purchaseExchangeRate_ = nil;
    
    [saleExchangeRate_ release];
    saleExchangeRate_ = nil;
    
    [eMail_ release];
    eMail_ = nil;
    
    [executiveEMail_ release];
    executiveEMail_ = nil;
    
    [customerType_ release];
    customerType_ = nil;
    
    [branch_ release];
    branch_ = nil;
    
    [mobilePhoneNumber_ release];
    mobilePhoneNumber_ = nil;
    
    [carrier_ release];
    carrier_ = nil;
    
    [messageCTS1_ release];
    messageCTS1_ = nil;
    
    [messageCTS2_ release];
    messageCTS2_ = nil;
    
    [titularName_ release];
    titularName_ = nil;
    
    otpActive_ = NO;
    
    otpUsage_ = otp_UsageUnknown;
    
}

@end
