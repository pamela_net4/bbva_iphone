//
//  FastLoanConfirm.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"


@interface FastLoanConfirm : StatusEnabledResponse{
    @private

   /**
     * FastLoanConfirm customer
     */
    NSString *customer_;


   /**
     * FastLoanConfirm currencyAmount
     */
    NSString *currencyAmount_;


   /**
     * FastLoanConfirm amountFormated
     */
    NSString *amountFormated_;


   /**
     * FastLoanConfirm quotaFormated
     */
    NSString *quotaFormated_;


   /**
     * FastLoanConfirm term
     */
    NSString *term_;


   /**
     * FastLoanConfirm quotaType
     */
    NSString *quotaType_;


   /**
     * FastLoanConfirm monthDobles
     */
    NSString *monthDobles_;


   /**
     * FastLoanConfirm dayOfPay
     */
    NSString *dayOfPay_;


   /**
     * FastLoanConfirm tceaFormated
     */
    NSString *tceaFormated_;


   /**
     * FastLoanConfirm teaFormated
     */
    NSString *teaFormated_;


   /**
     * FastLoanConfirm account
     */
    NSString *account_;


   /**
     * FastLoanConfirm currencyAccount
     */
    NSString *currencyAccount_;


   /**
     * FastLoanConfirm email
     */
    NSString *email_;


   /**
     * FastLoanConfirm seal
     */
    NSString *seal_;


   /**
     * FastLoanConfirm coordinate
     */
    NSString *coordinate_;


   /**
     * FastLoanConfirm disclaimerContract
     */
    NSString *disclaimerContract_;


   /**
     * FastLoanConfirm disclaimerContractWeb
     */
    NSString *disclaimerContractWeb_;


   /**
     * FastLoanConfirm disclaimerASecured
     */
    NSString *disclaimerASecured_;


   /**
     * FastLoanConfirm disclaimerASecuredWeb
     */
    NSString *disclaimerASecuredWeb_;




}

/**
 * Provides read-only access to the FastLoanConfirm customer
 */
@property (nonatomic, readonly, copy) NSString * customer;


/**
 * Provides read-only access to the FastLoanConfirm currencyAmount
 */
@property (nonatomic, readonly, copy) NSString * currencyAmount;


/**
 * Provides read-only access to the FastLoanConfirm amountFormated
 */
@property (nonatomic, readonly, copy) NSString * amountFormated;


/**
 * Provides read-only access to the FastLoanConfirm quotaFormated
 */
@property (nonatomic, readonly, copy) NSString * quotaFormated;


/**
 * Provides read-only access to the FastLoanConfirm term
 */
@property (nonatomic, readonly, copy) NSString * term;


/**
 * Provides read-only access to the FastLoanConfirm quotaType
 */
@property (nonatomic, readonly, copy) NSString * quotaType;


/**
 * Provides read-only access to the FastLoanConfirm monthDobles
 */
@property (nonatomic, readonly, copy) NSString * monthDobles;


/**
 * Provides read-only access to the FastLoanConfirm dayOfPay
 */
@property (nonatomic, readonly, copy) NSString * dayOfPay;


/**
 * Provides read-only access to the FastLoanConfirm tceaFormated
 */
@property (nonatomic, readonly, copy) NSString * tceaFormated;


/**
 * Provides read-only access to the FastLoanConfirm teaFormated
 */
@property (nonatomic, readonly, copy) NSString * teaFormated;


/**
 * Provides read-only access to the FastLoanConfirm account
 */
@property (nonatomic, readonly, copy) NSString * account;


/**
 * Provides read-only access to the FastLoanConfirm currencyAccount
 */
@property (nonatomic, readonly, copy) NSString * currencyAccount;


/**
 * Provides read-only access to the FastLoanConfirm email
 */
@property (nonatomic, readonly, copy) NSString * email;


/**
 * Provides read-only access to the FastLoanConfirm seal
 */
@property (nonatomic, readonly, copy) NSString * seal;


/**
 * Provides read-only access to the FastLoanConfirm coordinate
 */
@property (nonatomic, readonly, copy) NSString * coordinate;


/**
 * Provides read-only access to the FastLoanConfirm disclaimerContract
 */
@property (nonatomic, readonly, copy) NSString * disclaimerContract;


/**
 * Provides read-only access to the FastLoanConfirm disclaimerContractWeb
 */
@property (nonatomic, readonly, copy) NSString * disclaimerContractWeb;


/**
 * Provides read-only access to the FastLoanConfirm disclaimerASecured
 */
@property (nonatomic, readonly, copy) NSString * disclaimerASecured;


/**
 * Provides read-only access to the FastLoanConfirm disclaimerASecuredWeb
 */
@property (nonatomic, readonly, copy) NSString * disclaimerASecuredWeb;




@end

