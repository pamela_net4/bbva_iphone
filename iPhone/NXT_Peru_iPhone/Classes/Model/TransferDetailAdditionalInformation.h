/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import "StatusEnabledResponse.h"

@interface TransferDetailAdditionalInformation : StatusEnabledResponse{
@private
    
    /**
     * Transfer date string
     */
    NSString *dateString_;
    
    /**
     * Operation number
     */
    NSString *operationNumber_;
    
    /**
     * Transfer Beneficiary
     */
    NSString *beneficiary_;
    
    /**
     * Transfer amount to pay
     */
    NSString *amountToPay_;
	
	/**
     * Transfer currency amount to pay
     */
    NSString *currencyAmountToPay_;
    
    /**
     * Transfer state
     */
    NSString *transferState_;
    
}

/**
 * Provides read-only access to the transfer date string
 */
@property (nonatomic, readonly, copy) NSString *dateString;

/**
 * Provides read-only access to the operation number
 */
@property (nonatomic, readonly, copy) NSString *operationNumber;

/**
 * Provides read-only access to the beneficiary
 */
@property (nonatomic, readonly, copy) NSString *beneficiary;

/**
 * Provides read-only access to the transfer amountToPay string
 */
@property (nonatomic, readonly, copy) NSString *amountToPay;

/**
 * Provides read-only access to the transfer currencyAmountToPay string
 */
@property (nonatomic, readonly, copy) NSString *currencyAmountToPay;

/**
 * Provides read-only access to the state of the transfer
 */
@property (nonatomic, readonly, copy) NSString *transferState;


@end
