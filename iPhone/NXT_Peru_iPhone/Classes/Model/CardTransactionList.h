/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class CardTransaction;


/**
 * Contains a list of card transaction
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardTransactionList : StatusEnabledResponse {
    
@private
    
    /**
     * Card transaction list
     */
    NSMutableArray *cardTransactionList_;
    
    /**
     * Auxiliary card transaction
     */
    CardTransaction *auxCardTransaction_;
    
}

/**	
 * Provides read-only access to the card transaction list array
 */
@property (nonatomic, readonly, retain) NSArray *cardTransactionList;

/**
 * Provides read-only access to the card transactions count
 */
@property (nonatomic, readonly, assign) NSUInteger cardTransactionCount;


/**
 * Updates this instance with the given card transaction list
 *
 * @param aCardTransactionList The card transaction list to update from
 */
- (void)updateFrom:(CardTransactionList *)aCardTransactionList;

/**
 * Returns the CardTransaction located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the CardTransaction is located at
 * @return CardTransaction located at the given position, or nil if position is not valid
 */
- (CardTransaction *)cardTransactionAtPosition:(NSUInteger)aPosition;

/**
 * Remove the contained data
 */
- (void)removeData;

@end
