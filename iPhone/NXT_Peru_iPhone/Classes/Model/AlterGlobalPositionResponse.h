//
//  AlterGlobalPositionResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 13/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"
#import "ListaCuentas.h"


@interface AlterGlobalPositionResponse : StatusEnabledResponse {
@private
    ListaCuentas *listaCuentas_;
}

@property (nonatomic, readonly, retain) ListaCuentas *listaCuentas;

@end
