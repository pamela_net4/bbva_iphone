/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


/**
 * Stores one card transaction obtained from the server
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface CardTransaction : StatusEnabledResponse {

@private
    
    /**
     * Operation date string
     */
    NSString *operationDateString_;
    
    /**
     * Operation date
     */
    NSDate *operationDate_;
    
    /**
     * Operation type
     */
    NSString *operationType_;
    
    /**
     * Use
     */
    NSString *use_;
    
    /**
     * Concept
     */
    NSString *concept_;
    
    /**
     * Operations ML string
     */
    NSString *operationsMLString_;
    
    /**
     * Operations ML
     */
    NSDecimalNumber *operationsML_;
    
    /**
     * Operations ME string
     */
    NSString *operationsMEString_;
    
    /**
     * Operations ME
     */
    NSDecimalNumber *operationsME_;
    
    /**
     * Currency
     */
    NSString *currency_;
    
    /**
     * Amount string
     */
    NSString *amountString_;
    
    /**
     * Amount
     */
    NSDecimalNumber *amount_;
    
    /**
     * Subject
     */
    NSString *subject_;
    
}


/**
 * Provides read-only access to the operation date string
 */
@property (nonatomic, readonly, copy) NSString *operationDateString;

/**
 * Provides read-only access to the operation date
 */
@property (nonatomic, readonly, copy) NSDate *operationDate;

/**
 * Provides read-only access to the operation type
 */
@property (nonatomic, readonly, copy) NSString *operationType;

/**
 * Provides read-only access to the use
 */
@property (nonatomic, readonly, copy) NSString *use;

/**
 * Provides read-only access to the concept
 */
@property (nonatomic, readonly, copy) NSString *concept;

/**
 * Provides read-only access to the operations ML string
 */
@property (nonatomic, readonly, copy) NSString *operationsMLString;

/**
 * Provides read-only access to the operations ML
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *operationsML;

/**
 * Provides read-only access to the operations ME string
 */
@property (nonatomic, readonly, copy) NSString *operationsMEString;

/**
 * Provides read-only access to the amount string
 */
@property (nonatomic, readonly, copy) NSString *amountString;

/**
 * Provides read-only access to the operations ME
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *operationsME;

/**
 * Provides read-only access to the amount
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *amount;

/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the subject
 */
@property (nonatomic, readonly, copy) NSString *subject;


/**
 * Updates this instance with the given card transaction
 *
 * @param aCardTransaction The card transaction to update from
 */
- (void)updateFrom:(CardTransaction *)aCardTransaction;

/**
 * Compares the card transaction with another to produce an ordered list where older transactions are the last items
 *
 * @param aCardTransaction The card transaction to compare with
 * @return NSOrderedAscending when current card transaction is later than argument, NSOrderedDescending when current
 * card transaction is earlier than argument, NSOrderedSame otherwise
 */
- (NSComparisonResult)compareForDescendingDateOrdering:(CardTransaction *)aCardTransaction;

@end
