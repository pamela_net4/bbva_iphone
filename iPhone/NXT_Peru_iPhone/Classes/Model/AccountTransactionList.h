/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class AccountTransaction;


/**
 * Contains a list of account transaction
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountTransactionList : StatusEnabledResponse {

@private
    
    /**
     * Account transaction list
     */
    NSMutableArray *accountTransactionList_;
    
    /**
     * Auxiliary account transaction
     */
    AccountTransaction *auxAccountTransaction_;
    
    /**
     * Grouped transactions
     */
    NSMutableDictionary *groupedTransactions_;
}

/**	
 * Provides read-only access to the account transaction list array
 */
@property (nonatomic, readonly, retain) NSArray *accountTransactionList;

/**
 * Provides read-only access to the account transactions count
 */
@property (nonatomic, readonly, assign) NSUInteger accountTransactionCount;


/**
 * Updates this instance with the given account transaction list
 *
 * @param anAccountTransactionList The account transaction list to update from
 */
- (void)updateFrom:(AccountTransactionList *)anAccountTransactionList;

/**
 * Returns the AccountTransaction located at the given position, or nil if position is not valid
 *
 * @param  aPosition Position the AccountTransaction is located at
 * @return AccountTransaction located at the given position, or nil if position is not valid
 */
- (AccountTransaction *)accountTransactionAtPosition:(NSUInteger)aPosition;

/**
 * Returns the account transaction with an account transaction number, or nil if not valid
 *
 * @param  anAccountNumber The account transaction number
 * @return AccountTransaction with this number, or nil if not valid
 */
- (AccountTransaction *)accountTransactionFromAccountTransactionNumber:(NSString *)anAccountNumber;

/**
 * Remove the contained data
 */
- (void)removeData;

/**
 * Returns the list of transactions for the given date represented as seconds from Jan 1, 1970
 *
 * @param aDate The date to obtain the transactions for
 * @return The date transactions array
 */
- (NSArray *)transactionListForDate:(NSTimeInterval)aDate;


@end
