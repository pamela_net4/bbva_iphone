/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


/**
 * Retention information
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Retention : StatusEnabledResponse {

@private
    
    /**
     * Discharge date string
     */
    NSString *dischargeDateString_;
    
    /**
     * Discharge date
     */
    NSDate *dischargeDate_;
    
    /**
     * Due date string
     */
    NSString *dueDateString_;
    
    /**
     * Due date
     */
    NSDate *dueDate_;
    
    /**
     * Number
     */
    NSString *number_;
    
    /**
     * Remarks
     */
    NSString *remarks_;
    
    /**
     * Current amount string
     */
    NSString *currentAmountString_;
    
    /**
     * Current amount
     */
    NSDecimalNumber *currentAmount_;
    
}


/**
 * Provides read-only access to the discharge date string
 */
@property (nonatomic, readonly, copy) NSString *dischargeDateString;

/**
 * Provides read-only access to the discharge date
 */
@property (nonatomic, readonly, copy) NSDate *dischargeDate;

/**
 * Provides read-only access to the due date string
 */
@property (nonatomic, readonly, copy) NSString *dueDateString;

/**
 * Provides read-only access to the ddue date
 */
@property (nonatomic, readonly, copy) NSDate *dueDate;

/**
 * Provides read-only access to the number
 */
@property (nonatomic, readonly, copy) NSString *number;

/**
 * Provides read-only access to the remarks
 */
@property (nonatomic, readonly, copy) NSString *remarks;

/**
 * Provides read-only access to the current amount string
 */
@property (nonatomic, readonly, copy) NSString *currentAmountString;

/**
 * Provides read-only access to the current amount
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *currentAmount;

@end
