//
//  SalaryAdvanceReceive.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"


@interface SalaryAdvanceReceive : StatusEnabledResponse{
    @private

   /**
     * SalaryAdvanceReceive customer
     */
    NSString *customer_;


   /**
     * SalaryAdvanceReceive commission
     */
    NSString *commission_;


   /**
     * SalaryAdvanceReceive amountAdvance
     */
    NSString *amountAdvance_;


   /**
     * SalaryAdvanceReceive totalToPay
     */
    NSString *totalToPay_;


   /**
     * SalaryAdvanceReceive dayOfPay
     */
    NSString *dayOfPay_;


   /**
     * SalaryAdvanceReceive seal
     */
    NSString *seal_;


   /**
     * SalaryAdvanceReceive coordinate
     */
    NSString *coordinate_;




}

/**
 * Provides read-only access to the SalaryAdvanceReceive customer
 */
@property (nonatomic, readonly, copy) NSString * customer;


/**
 * Provides read-only access to the SalaryAdvanceReceive commission
 */
@property (nonatomic, readonly, copy) NSString * commission;


/**
 * Provides read-only access to the SalaryAdvanceReceive amountAdvance
 */
@property (nonatomic, readonly, copy) NSString * amountAdvance;


/**
 * Provides read-only access to the SalaryAdvanceReceive totalToPay
 */
@property (nonatomic, readonly, copy) NSString * totalToPay;


/**
 * Provides read-only access to the SalaryAdvanceReceive dayOfPay
 */
@property (nonatomic, readonly, copy) NSString * dayOfPay;


/**
 * Provides read-only access to the SalaryAdvanceReceive seal
 */
@property (nonatomic, readonly, copy) NSString * seal;


/**
 * Provides read-only access to the SalaryAdvanceReceive coordinate
 */
@property (nonatomic, readonly, copy) NSString * coordinate;




@end

