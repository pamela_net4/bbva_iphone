//
//  TransferTypePaymentResponse.h
//  NXT_Peru_iPhone
//
//  Created by Jose Contreras Sanchez on 10/1/15.
//  Copyright © 2015 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
#import "TransferStartupResponse.h"
@class TransferConfirmationAdditionalInformation;

@interface TransferTypePaymentResponse : StatusEnabledResponse
{
    
    NSString *typeFlow_;
    
    NSString *commissionN_;
    
    NSString *commissionL_;
    
    NSString *totalPayN_;
    
    NSString *totalPayL_;
    
    NSString *disclaimer_;
    /**
     * Array of documents
     */
    NSMutableArray *documents_;
    
    /**
     * Aux document
     */
    Document *auxDocument_;
    
    /**
     * Flag to indicate that we are analyzing documents
     */
    
    
    /**
     * Additional information
     */
    TransferConfirmationAdditionalInformation *additionalInformation_;
    
    BOOL analyzingDocuments_;

}
/**
 * Provides read-only access to the type flow
 */
@property (nonatomic, readonly, copy) NSString *typeFlow;

@property (nonatomic, readonly, copy) NSString *commissionN;

@property (nonatomic, readonly, copy) NSString *commissionL;

@property (nonatomic, readonly, copy) NSString *totalPayN;

@property (nonatomic, readonly, copy) NSString *totalPayL;

@property (nonatomic, readonly, copy) NSString *disclaimer;

/**
 * Provides read-only access to the additional information
 */
@property (nonatomic, readonly, retain) TransferConfirmationAdditionalInformation *additionalInformation;
/**
 * Provides read-only access to the documents
 */
@property (nonatomic, readonly, retain) NSMutableArray *documents;
@end
