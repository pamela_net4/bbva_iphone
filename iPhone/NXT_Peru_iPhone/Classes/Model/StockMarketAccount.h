/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


/**
 * Stock market account class contains information about a stock market account containing the value account, currency, balances, expired account and subject
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface StockMarketAccount : StatusEnabledResponse {
    
@private
    
    /**
     * Value account
     */
    NSString *valueAccount_;
    
    /**
     * Currency
     */
    NSString *currency_;
    
    /**
     * Value balance string
     */
    NSString *valueBalanceString_;
    
    /**
     * Value balance
     */
    NSDecimalNumber *valueBalance_;
    
    /**
     * Cash balance string
     */
    NSString *cashBalanceString_;
    
    /**
     * Cash balance
     */
    NSDecimalNumber *cashBalance_;
    
    /**
     * Expired account
     */
    NSString *expiredAccount_;
    
    /**
     * Subject
     */
    NSString *subject_;
    
    /**
     * Balance converted into country currency string
     */
    NSString *balanceInCountryCurrencyString_;
    
    /**
     * Balance converted into country currency
     */
    NSDecimalNumber *balanceInCountryCurrency_;
    
}


/**
 * Provides read-only access to the value account
 */
@property (nonatomic, readonly, copy) NSString *valueAccount;

/**
 * Provides read-only access to the currency
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the value balance string
 */
@property (nonatomic, readonly, copy) NSString *valueBalanceString;

/**
 * Provides read-only access to the value balance
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *valueBalance;

/**
 * Provides read-only access to the cash balance string
 */
@property (nonatomic, readonly, copy) NSString *cashBalanceString;

/**
 * Provides read-only access to the cash balance
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *cashBalance;

/**
 * Provides read-write access to the expired account
 */
@property (nonatomic, readonly, copy) NSString *expiredAccount;

/**
 * Provides read-only access to the subject
 */
@property (nonatomic, readonly, copy) NSString *subject;

/**
 * Provides read-only access to the balance converted into cuntry currency string
 */
@property (nonatomic, readonly, copy) NSString *balanceInCountryCurrencyString;

/**
 * Provides read-only access to the balance converted into country currency
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *balanceInCountryCurrency;


/**
 * Returns the stock market account list display name. It can be used whenever a stock market account name must be displayed inside a list
 *
 * @return The stock market account list display name
 */
- (NSString *)stockMarketAccountListName;

/**
 * Returns the obfuscated account number
 *
 * @return The obfuscated account number
 */
- (NSString *)obfuscatedAccountNumber;


@end
