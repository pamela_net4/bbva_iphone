/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "GeneralLoginResponse.h"


#pragma mark -

/**
 * GeneralLoginResponse private category
 */
@interface GeneralLoginResponse(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearGeneralLoginResponseData;

@end


#pragma mark -

@implementation GeneralLoginResponse

#pragma mark -
#pragma mark Properties

@synthesize status = status_;
@synthesize loginErrorCode = loginErrorCode_;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {

    [self clearGeneralLoginResponseData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Designated initialized.
 */
- (id)init {
    
    if (self = [super init]) {

	}
    
    return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearGeneralLoginResponseData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"estado"]) {
            
            xmlAnalysisCurrentValue_ = glxeas_AnalyzingStatus;
            
        } else if ([lname isEqualToString: @"codigoerror"]) {
            
            xmlAnalysisCurrentValue_ = glxeas_AnalyzingErrorCode;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        if (xmlAnalysisCurrentValue_ == glxeas_AnalyzingStatus) {
            
            [status_ release];
            status_ = nil;
            status_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == glxeas_AnalyzingErrorCode) {
            
            [loginErrorCode_ release];
            loginErrorCode_ = nil;
            loginErrorCode_ = [elementString copyWithZone:self.zone];
            
            if ([loginErrorCode_ length] > 0) {
                
                self.errorMessage = loginErrorCode_;
                
            }
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

@end


#pragma mark -

@implementation GeneralLoginResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearGeneralLoginResponseData {
    
    [status_ release];
    status_ = nil;
    
    [loginErrorCode_ release];
    loginErrorCode_ = nil;
    
}

@end
