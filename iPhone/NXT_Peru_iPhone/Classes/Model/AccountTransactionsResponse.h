/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class AccountTransactionList;
@class AccountTransactionsAdditionalInformation;


/**
 * Contains the account transaction response obtained from the server
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountTransactionsResponse : StatusEnabledResponse {

@private
    
    /**
     * Account transaction list
     */
    AccountTransactionList *accountTransactionList_;
    
    /**
     * Account transactions additional information
     */
    AccountTransactionsAdditionalInformation *additionalInformation_;
    
}


/**
 * Provides read-only access to the account transaction list
 */
@property (nonatomic, readonly, retain) AccountTransactionList *accountTransactionList;

/**
 * Provides read-only access to the account transactions additional information
 */
@property (nonatomic, readonly, retain) AccountTransactionsAdditionalInformation *additionalInformation;

@end
