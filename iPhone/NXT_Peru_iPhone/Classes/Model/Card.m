/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "Card.h"
#import "StringKeys.h"
#import "Constants.h"
#import "Tools.h"
#import "CardTransactionList.h"
#import "CardTransactionsAdditionalInformation.h"


#pragma mark -

/**
 * Card private category
 */
@interface Card(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearCardData;

@end


#pragma mark -

/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
    cxeas_AnalyzingCardNumber = serxeas_ElementsCount, //!<Analyzing the card number
	cxeas_AnalyzingCardType, //!<Analyzing the card type
    cxeas_AnalyzingAvailableCredit, //!<Analying the available credit
	cxeas_AnalyzingCreditLimit, //!<Analyzing credit limit
    cxeas_AnalyzingCurrency, //!<Analyzing credit card currency
	cxeas_AnalyzingType, //!<Analyzing type
    cxeas_AnalyzingContract, //!<Analyzing contract
    cxeas_AnalyzingPermission, //!<Analyzing the permission
    cxeas_AnalyzingState, //!<Analyzing the state
    cxeas_AnalyzingSubject, //!<Analyzing the subject
    cxeas_AnayzingCreditInCountryCurrency //!<Analyzing the credit converted into country currency
    
} CardXMLElementAnalyzerState;

@implementation Card

#pragma mark -
#pragma mark Properties

@synthesize cardNumber = cardNumber_;
@synthesize cardType = cardType_;
@synthesize availableCreditString = availableCreditString_;
@dynamic availableCredit;
@synthesize creditLimitString = creditLimitString_;
@dynamic creditLimit;
@synthesize currency = currency_;
@synthesize type = type_;
@synthesize contract = contract_;
@synthesize permission = permission_;
@synthesize state = state_;
@synthesize subject = subject_;
@synthesize creditInCountryCurrencyString = creditInCountryCurrencyString_;
@dynamic creditInCountryCurrency;
@synthesize cardTransactionsList = cardTransactionsList_;
@synthesize transactionsAdditionalInformation = transactionsAdditionalInformation_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [self clearCardData];
    
    [cardTransactionsList_ release];
    cardTransactionsList_ = nil;
    
    [transactionsAdditionalInformation_ release];
    transactionsAdditionalInformation_ = nil;
	
	[super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes an empty CreditCard instance
 *
 * @return An empty CreditCard insntance
 */
- (id)init {
    
    if (self = [super init]) {

		self.notificationToPost = kNotificationCardUpdated;
		
	}
	
	return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates this instance with the given card
 */
- (void)updateFrom:(Card *)aCard {
	
    NSZone *zone = self.zone;
    
	[cardNumber_ release];
    cardNumber_ = nil;
	cardNumber_ = [aCard.cardNumber copyWithZone:zone];
    
	[cardType_ release];
    cardType_ = nil;
	cardType_ = [aCard.cardType copy];
    
	[availableCreditString_ release];
    availableCreditString_ = nil;
	availableCreditString_ = [aCard.availableCreditString copyWithZone:zone];
    
	[creditLimitString_ release];
    creditLimitString_ = nil;
	creditLimitString_ = [aCard.creditLimitString copyWithZone:zone];
    
	[currency_ release];
    currency_ = nil;
	currency_ = [aCard.currency copyWithZone:zone];
    
	[type_ release];
    type_ = nil;
	type_ = [aCard.type copyWithZone:zone];
    
	[contract_ release];
    contract_ = nil;
	contract_ = [aCard.contract copyWithZone:zone];
    
	[permission_ release];
    permission_ = nil;
	permission_ = [aCard.permission copyWithZone:zone];
    
	[state_ release];
    state_ = nil;
	state_ = [aCard.state copyWithZone:zone];
    
    [subject_ release];
    subject_ = nil;
    subject_ = [aCard.subject copyWithZone:zone];
    
    [creditInCountryCurrencyString_ release];
    creditInCountryCurrencyString_ = nil;
    creditInCountryCurrencyString_ = [aCard.creditInCountryCurrencyString copyWithZone:zone];
    
    [creditInCountryCurrency_ release];
    creditInCountryCurrency_ = nil;
    
    [self updateCardTransactionsList:aCard.cardTransactionsList];
    [self updateCardTransactionsAdditionalInformation:aCard.transactionsAdditionalInformation];
    [self updateFromStatusEnabledResponse:aCard];
	
	self.informationUpdated = aCard.informationUpdated;
    
}

/*
 * Updates the transactions list
 */
- (void)updateCardTransactionsList:(CardTransactionList *)aCardTransactionsList {
    
    if (cardTransactionsList_ == nil) {
        
        cardTransactionsList_ = [[CardTransactionList alloc] init];
        
    }
    
    [cardTransactionsList_ updateFrom:aCardTransactionsList];
    
}

/*
 * Updates the transactions additional information
 */
- (void)updateCardTransactionsAdditionalInformation:(CardTransactionsAdditionalInformation *)aTransactionsAdditionalInformation {
    
    if (transactionsAdditionalInformation_ == nil) {
        
        transactionsAdditionalInformation_ = [[CardTransactionsAdditionalInformation alloc] init];
        
    }
    
    [transactionsAdditionalInformation_ updateFrom:aTransactionsAdditionalInformation];
    
}

/*
 * Returns the card identifier when displayed to the user. It consists of the obfuscated card number
 */
- (NSString *)cardIdentifierDisplayedToTheUser {
    
    return [Tools obfuscateCardNumber:cardNumber_];
    
}


#pragma mark -
#pragma mark Title texts management

/*
 * Returns the card list display name. It can be used whenever a card name must be displayed inside a list
 */
- (NSString *)cardListName {
    
    //return [NSString stringWithFormat:@"%@ %@", cardType_, [Tools obfuscateCardNumber:cardNumber_]];
    return [NSString stringWithFormat:@"%@ | %@", [Tools obfuscateCardNumber:cardNumber_], cardType_];
    
}

/*
 * Returns the obfuscated card number
 */
- (NSString *)obfuscatedCardNumber {
    
    return [Tools obfuscateCardNumber:cardNumber_];
    
}


#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearCardData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"numerotarjeta"]) {
            
            xmlAnalysisCurrentValue_ = cxeas_AnalyzingCardNumber;
            
        } else if ([lname isEqualToString: @"tipotarjeta"]) {
            
            xmlAnalysisCurrentValue_ = cxeas_AnalyzingCardType;
            
        } else if ([lname isEqualToString: @"creditodisponible"]) {
            
            xmlAnalysisCurrentValue_ = cxeas_AnalyzingAvailableCredit;
            
        } else if ([lname isEqualToString: @"limitecredito"]) {
            
            xmlAnalysisCurrentValue_ = cxeas_AnalyzingCreditLimit;
            
        } else if ([lname isEqualToString: @"moneda"]) {
            
            xmlAnalysisCurrentValue_ = cxeas_AnalyzingCurrency;
            
        } else if ([lname isEqualToString: @"clase"]) {
            
            xmlAnalysisCurrentValue_ = cxeas_AnalyzingType;
            
        } else if ([lname isEqualToString: @"contrato"]) {
            
            xmlAnalysisCurrentValue_ = cxeas_AnalyzingContract;
            
        } else if ([lname isEqualToString: @"permiso"]) {
            
            xmlAnalysisCurrentValue_ = cxeas_AnalyzingPermission;
            
        } else if ([lname isEqualToString: @"estado"]) {
            
            xmlAnalysisCurrentValue_ = cxeas_AnalyzingState;
            
        } else if ([lname isEqualToString: @"asunto"]) {
            
            xmlAnalysisCurrentValue_ = cxeas_AnalyzingSubject;
            
        } else if ([lname isEqualToString: @"importeconvertido"]) {
            
            xmlAnalysisCurrentValue_ = cxeas_AnayzingCreditInCountryCurrency;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {

        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == cxeas_AnalyzingCardNumber) {
            
            [cardNumber_ release];
            cardNumber_ = nil;
            cardNumber_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == cxeas_AnalyzingCardType) {
            
            [cardType_ release];
            cardType_ = nil;
            cardType_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == cxeas_AnalyzingAvailableCredit) {
            
            [availableCreditString_ release];
            availableCreditString_ = nil;
            availableCreditString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == cxeas_AnalyzingCreditLimit) {
            
            [creditLimitString_ release];
            creditLimitString_ = nil;
            creditLimitString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == cxeas_AnalyzingCurrency) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == cxeas_AnalyzingType) {
            
            [type_ release];
            type_ = nil;
            type_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == cxeas_AnalyzingContract) {
            
            [contract_ release];
            contract_ = nil;
            contract_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == cxeas_AnalyzingPermission) {
            
            [permission_ release];
            permission_ = nil;
            permission_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == cxeas_AnalyzingState) {
            
            [state_ release];
            state_ = nil;
            state_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == cxeas_AnalyzingSubject) {
            
            [subject_ release];
            subject_ = nil;
            subject_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == cxeas_AnayzingCreditInCountryCurrency) {
            
            [creditInCountryCurrencyString_ release];
            creditInCountryCurrencyString_ = nil;
            creditInCountryCurrencyString_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the available credit calculating it from the string representation if necessary
 *
 * @return The available credit
 */
- (NSDecimalNumber *)availableCredit {
    
    NSDecimalNumber *result = availableCredit_;
    
    if (result == nil) {
        
        if ([availableCreditString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:availableCreditString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
				[availableCredit_ release];
                availableCredit_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the credit limit, calculating it from the string representation if necessary
 *
 * @return The credit limit
 */
- (NSDecimalNumber *)creditLimit {
    
    NSDecimalNumber *result = creditLimit_;
    
    if (result == nil) {
        
        if ([creditLimitString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:creditLimitString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                creditLimit_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the credit converted into country currency, calculating it from string representation if necessary
 *
 * @return The credit converted into country currency
 */
- (NSDecimalNumber *)creditInCountryCurrency {
    
    NSDecimalNumber *result = creditInCountryCurrency_;
    
    if (result == nil) {
        
        if ([creditInCountryCurrencyString_ length] == 0) {
            
            if ([availableCreditString_ length] > 0) {
                
                result = [Tools decimalFromServerString:availableCreditString_];
                
            } else {
                
                result = [NSDecimalNumber notANumber];

            }
            
        } else {
            
            result = [Tools decimalFromServerString:creditInCountryCurrencyString_];
            
        }
        
        if (![result compare:[NSDecimalNumber notANumber]]) {
            
            creditInCountryCurrency_ = [result retain];
            
        }
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation Card(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearCardData {
    
    [cardNumber_ release];
    cardNumber_ = nil;
    
    [cardType_ release];
    cardType_ = nil;
    
    [availableCreditString_ release];
    availableCreditString_ = nil;
    
    [availableCredit_ release];
    availableCredit_ = nil;
    
    [creditLimitString_ release];
    creditLimitString_ = nil;
    
    [creditLimit_ release];
    creditLimit_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [type_ release];
    type_ = nil;
    
    [contract_ release];
    contract_ = nil;
    
    [permission_ release];
    permission_ = nil;
    
    [state_ release];
    state_ = nil;
    
    [subject_ release];
    subject_ = nil;
    
    [creditInCountryCurrencyString_ release];
    creditInCountryCurrencyString_ = nil;
    
    [creditInCountryCurrency_ release];
    creditInCountryCurrency_ = nil;
    
}

@end
