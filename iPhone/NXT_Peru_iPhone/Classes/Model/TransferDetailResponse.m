/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import "TransferDetailResponse.h"
#import "TransferDetailAdditionalInformation.h"

/**
 * Enumerates the analysis states
 */
typedef enum {
    
    tsrxeas_AnalyzingAdditionalInformation = serxeas_ElementsCount, //!<Analyzing the additional information
    
} TransferSuccessResponseXMLElementAnalyzerState;

#pragma mark
/**
 * TransactionDetailResponse private category
 */
@interface TransferDetailResponse(private)
/**
 * Clears the object data
 *
 * @private
 */
- (void)clearTransferDetailResponseData;

@end

#pragma mark -

@implementation TransferDetailResponse

#pragma mark -
#pragma mark Properties

@synthesize additionalInformationArray = additionalInformationArray_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearTransferDetailResponseData];
	
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Super class designated initializer. It initalizes a TransferSuccessResponse instance providing the associated notification
 *
 * @return The initialized TransferDetailResponse instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationTransferSuccessResponseReceived;
        
    }
	
	return self;
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
    [additionalInformationArray_ release];
    additionalInformationArray_ = [[NSMutableArray alloc] init];

    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (auxAdditionalInformation_ != nil) {
        [additionalInformationArray_ addObject:auxAdditionalInformation_];
        [auxAdditionalInformation_ release];
        auxAdditionalInformation_ = nil;
    }
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"e"]) {
            
            xmlAnalysisCurrentValue_ = tsrxeas_AnalyzingAdditionalInformation;
			[auxAdditionalInformation_ release];
            auxAdditionalInformation_ = nil;
            auxAdditionalInformation_ = [[TransferDetailAdditionalInformation alloc] init];
            auxAdditionalInformation_.openingTag = lname;
            [auxAdditionalInformation_ setParentParseableObject:self];
            [parser setDelegate:auxAdditionalInformation_];
            [auxAdditionalInformation_ parserDidStartDocument:parser];
            
        }else if ([lname isEqualToString: @"movimientos"] || [lname isEqualToString: @"informacionadicional"]){
            xmlAnalysisCurrentValue_ = tsrxeas_AnalyzingAdditionalInformation;
            
        }else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    NSString* lname = [elementName lowercaseString];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        if ([lname isEqualToString: @"movimientos"]) {
            
            if (auxAdditionalInformation_ != nil) {
                [additionalInformationArray_ addObject:auxAdditionalInformation_];
                [auxAdditionalInformation_ release];
                auxAdditionalInformation_ = nil;
            }
            
            informationUpdated_ = soie_InfoAvailable;
            [parser setDelegate:parentParseableObject_];
            parentParseableObject_ = nil;
        }
        
        if ([lname isEqualToString: @"msg-s"]) {
            
            informationUpdated_ = soie_InfoAvailable;
            [parser setDelegate:parentParseableObject_];
            parentParseableObject_ = nil;
            
        }
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
        
    }
    
}

@end


#pragma mark -

@implementation TransferDetailResponse(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearTransferDetailResponseData {
    
    [auxAdditionalInformation_ release];
    auxAdditionalInformation_ = nil;
    
    [additionalInformationArray_ release];
    additionalInformationArray_ = nil;
    
}

@end
