//
//  GeneralLoginEncryptResponse.h
//  NXT_Peru_iPhone
//
//  Created by MDP on 24/12/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    glxeas_AnalyzingResponse = serxeas_ElementsCount //!<Analyzing the access validation
    
} GeneralLoginEncryptXMLElementAnalyzerState;

@interface GeneralLoginEncryptResponse : StatusEnabledResponse{

@private

    /**
     * Response
     */
     NSString *response_;
}
/**
 * Provides read-write access to the response
 */
@property (nonatomic, readwrite, copy) NSString *response;

@end
