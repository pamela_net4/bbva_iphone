//
//  CardForIncrement.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@interface CardForIncrement : StatusEnabledResponse{
    @private

   /**
     * CardForIncrement position
     */
    NSString *position_;


   /**
     * CardForIncrement cardNumber
     */
    NSString *cardNumber_;


   /**
     * CardForIncrement cardDescription
     */
    NSString *cardDescription_;


   /**
     * CardForIncrement contractNumber
     */
    NSString *contractNumber_;


   /**
     * CardForIncrement creditLine
     */
    NSString *creditLine_;


   /**
     * CardForIncrement minIncrement
     */
    NSString *minIncrement_;


   /**
     * CardForIncrement maxIncrement
     */
    NSString *maxIncrement_;


   /**
     * CardForIncrement currency
     */
    NSString *currency_;


   /**
     * CardForIncrement currencySimbol
     */
    NSString *currencySimbol_;




}

/**
 * Provides read-only access to the CardForIncrement position
 */
@property (nonatomic, readonly, copy) NSString * position;


/**
 * Provides read-only access to the CardForIncrement cardNumber
 */
@property (nonatomic, readonly, copy) NSString * cardNumber;


/**
 * Provides read-only access to the CardForIncrement cardDescription
 */
@property (nonatomic, readonly, copy) NSString * cardDescription;


/**
 * Provides read-only access to the CardForIncrement contractNumber
 */
@property (nonatomic, readonly, copy) NSString * contractNumber;


/**
 * Provides read-only access to the CardForIncrement creditLine
 */
@property (nonatomic, readonly, copy) NSString * creditLine;


/**
 * Provides read-only access to the CardForIncrement minIncrement
 */
@property (nonatomic, readonly, copy) NSString * minIncrement;


/**
 * Provides read-only access to the CardForIncrement maxIncrement
 */
@property (nonatomic, readonly, copy) NSString * maxIncrement;


/**
 * Provides read-only access to the CardForIncrement currency
 */
@property (nonatomic, readonly, copy) NSString * currency;


/**
 * Provides read-only access to the CardForIncrement currencySimbol
 */
@property (nonatomic, readonly, copy) NSString * currencySimbol;




@end

