/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "PaymentPSProcess.h"

#import "Card.h"
#import "Payment.h"
#import "PublicServiceResponse.h"
#import "PaymentInstitutionsAndCompaniesDetailResponse.h"
#import "StringKeys.h"
#import "Tools.h"




#pragma mark -

@implementation PaymentPSProcess

#pragma mark -
#pragma mark Properties

@synthesize selectedCompany = selectedCompany_;
@synthesize selectedCompanyIndex = selectedCompanyIndex_;
@synthesize selectedCompanyCode = selectedCompanyCode_;
@synthesize selectedCompanyParameter = selectedCompanyParameter_;
@synthesize supplies = supplies_;
@synthesize paymentsArray = paymentsArray_;
@synthesize selectedPaymentsArray = selectedPaymentsArray_;
@synthesize clientName = clientName_;
@synthesize amountToPay = amountToPay_;
@synthesize companyConfiguration = companyConfiguration_;
@synthesize psDelegate = psDelegate_;
@synthesize cardsArray = cardsArray_;
@synthesize hasCards = hasCards_;
@synthesize selectedCard = selectedCard_;
@synthesize selectingCard = selectingCard_;
@synthesize selectedLocalityIndex = selectedLocalityIndex_;
@synthesize selectedCardIndex = selectedCardIndex_;
@synthesize selectedMethodIndex = selectedMethodIndex_;
@synthesize currencyPS = currencyPS_;
@synthesize responseInformation = responseInformation_;
@synthesize responseInitialResponse = responseInitialResponse_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [selectedCompany_ release];
    selectedCompany_ = nil;
    
    [selectedCompanyCode_ release];
    selectedCompanyCode_ = nil;
    
    [selectedCompanyParameter_ release];
    selectedCompanyParameter_ = nil;

    [supplies_ release];
    supplies_ = nil;
    
    [paymentsArray_ release];
    paymentsArray_ = nil;
    
    [selectedPaymentsArray_ release];
    selectedPaymentsArray_ = nil;
    
    [clientName_ release];
    clientName_ = nil;
    
    [amountToPay_ release];
    amountToPay_ = nil;
    
    [companyConfiguration_ release];
    companyConfiguration_ = nil;
    
    [responseInformation_ release];
    responseInformation_ = nil;
    
    [responseInitialResponse_ release];
    responseInitialResponse_ = nil;
    
    psDelegate_ = nil;
    
    [cardsArray_ release];
    cardsArray_ = nil;
    
    [selectedCard_ release];
    selectedCard_ = nil;
    
    hasCards_ = NO;
    
    selectingCard_ = NO;
    
    [currencyPS_ release];
    currencyPS_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Information distribution

/**
 * Checks if there is enough information to start the startPaymentInitialRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentInitialValidation {

    NSString *result = nil;
    
    if (([self selectedCompany] == nil) || [[self selectedCompany] isEqualToString:@""]) {
        
        result = NSLocalizedString(PUBLIC_SERVICE_STEP_ONE_SELECT_BUSINESS_ERROR_KEY, nil);
        
    }
    
    hasCards_ = NO;
    selectingCard_ = NO;

    return result;

}

/**
 * Performs the payment initial request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentInitialRequest {
    
}

/**
 * Notifies the payment operation process the initial response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The initial response received from the server
 */
- (void)initialResponseReceived:(NSNotification *)notification {
    
}

#pragma mark -
#pragma mark Checking information

/*
 * Checks whether a receipt can be selected. Receipts must be selected from the futher past to the nearest past. One receipt with a date later that
 * an unselected receipt cannot be selected.
 */
- (BOOL)canSelectReceipt:(Payment *)receipt
        forSelectedArray:(NSArray *)selectedReceipts {
    
    BOOL result = YES;
    
    PaymentTypeEnum paymentType = [self paymentOperationType];

    BOOL obtainDueDate = (paymentType == PTEPaymentPSElectricServices);
    
    NSDate *receiptDate = nil;
    
    NSArray *paymentsArray = [self paymentsArray];
    NSArray *orderedPaymentsByDate = nil;

    if (obtainDueDate) {
        
        orderedPaymentsByDate = [paymentsArray sortedArrayUsingSelector:@selector(compareDueDateWithPayment:)];
        receiptDate = [receipt dueDateAsDate];
        
    } else {
        
        orderedPaymentsByDate = [paymentsArray sortedArrayUsingSelector:@selector(compareDateWithPayment:)];
        receiptDate = [receipt dateAsDate];
        
    }
    
    if (receiptDate != nil) {
        
        NSArray *correctedSelectedReceiptList = nil;
        
        if ((paymentType == PTEPaymentPSCellular) || (paymentType == PTEPaymentPSPhone)) {
            
            //Cellular and phone payments only accepts paying one receipt each time, and each selectio removes the previous one, so the selected array must be empty to perform the checking.
            correctedSelectedReceiptList = [NSArray array];
            
        } else {
            
            correctedSelectedReceiptList = [NSArray arrayWithArray:selectedReceipts];
            
        }

        NSDate *storedPaymentDate = nil;
        
        for (Payment *orderedPayment in orderedPaymentsByDate) {
            
            if (obtainDueDate) {
                
                storedPaymentDate = [orderedPayment dueDateAsDate];
                
            } else {
                
                storedPaymentDate = [orderedPayment dateAsDate];
                
            }
            
            if (storedPaymentDate == nil) {
                
                result = NO;
                break;
                
            } else if ([storedPaymentDate compare:receiptDate] == NSOrderedAscending) {
                
                if (![correctedSelectedReceiptList containsObject:orderedPayment]) {
                    
                    result = NO;
                    break;
                    
                }
                
            }
            
        }
        
    } else {
        
        result = NO;
        
    }
    
    return result;
    
}

/*
 * Corrects the selected receipts list, removing the receipts newer than th first not selected receipt.
 */
- (NSArray *)correctSelectedReceiptsList:(NSArray *)selectedReceiptList {
    
    NSMutableArray *result = [NSMutableArray array];
    
    PaymentTypeEnum paymentType = [self paymentOperationType];
    
    BOOL obtainDueDate = (paymentType== PTEPaymentPSElectricServices);
    
    NSArray *paymentsArray = [self paymentsArray];
    NSArray *orderedPaymentsByDate = nil;
    
    if (obtainDueDate) {
        
        orderedPaymentsByDate = [paymentsArray sortedArrayUsingSelector:@selector(compareDueDateWithPayment:)];
        
    } else {
        
        orderedPaymentsByDate = [paymentsArray sortedArrayUsingSelector:@selector(compareDateWithPayment:)];
        
    }
    
    Payment *firstNotSelectedReceipt = nil;
    
    for (Payment *storedReceipt in orderedPaymentsByDate) {
        
        if (![selectedReceiptList containsObject:storedReceipt]) {
            
            firstNotSelectedReceipt = storedReceipt;
            break;
            
        }
        
    }
    
    if (firstNotSelectedReceipt == nil) {
        
        [result addObjectsFromArray:selectedReceiptList];
        
    } else {
        
        NSDate *limitingDate = nil;
        
        if (obtainDueDate) {
            
            limitingDate = [firstNotSelectedReceipt dueDateAsDate];
            
        } else {
            
            limitingDate = [firstNotSelectedReceipt dateAsDate];
            
        }
        
        NSDate *selectedDate = nil;
        
        for (Payment *selectedReceipt in selectedReceiptList) {
            
            if (obtainDueDate) {
                
                selectedDate = [selectedReceipt dueDateAsDate];
                
            } else {
                
                selectedDate = [selectedReceipt dateAsDate];
                
            }
            
            if ([limitingDate compare:selectedDate] != NSOrderedAscending) {
                
                [result addObject:selectedReceipt];
                
            }
            
        }

    }
    
    return [NSArray arrayWithArray:result];
    
}

#pragma mark -
#pragma mark Setters and Getters

/*
 * Returns the BaseTransferProcess delegate.
 *
 * @return The BaseTransferProcess delegate.
 */
- (id<PaymentPSProcessDelegate>)psDelegate {
    
    id<PaymentPSProcessDelegate> result = nil;
    
    id<PaymentBaseProcessDelegate> superclassDelegate = self.delegate;
    
    if ([(NSObject *)superclassDelegate conformsToProtocol:@protocol(PaymentPSProcessDelegate)]) {
        
        result = (id<PaymentPSProcessDelegate>)superclassDelegate;
        
    }
    
    return result;
    
}

/*
 * Sets the public service Delegate.
 *
 * @param psDelegate The new process delegate to set.
 */
- (void)setPsDelegate:(id<PaymentPSProcessDelegate>)psDelegate {
    
    self.delegate = psDelegate;
    
}

/**
 * Sets the payments array
 */
- (void)setPaymentsArray:(NSArray *)paymentsArray {

    if (paymentsArray_ == nil) {
        
        paymentsArray_ = [[NSMutableArray alloc] init];
        
    }
    
    if (paymentsArray != paymentsArray_) {
        
        [paymentsArray_ removeAllObjects];
        
        for (NSObject *object in paymentsArray) {
            
            if ([object isKindOfClass:[Payment class]]) {
                
                [paymentsArray_ addObject:object];
                
            }
            
        }
        
    }

}


/**
 * Sets the selected payments array
 */
- (void)setSelectedPaymentsArray:(NSArray *)selectedPaymentsArray {
    
    if (selectedPaymentsArray_ == nil) {
        
        selectedPaymentsArray_ = [[NSMutableArray alloc] init];
        
    }
    
    if (selectedPaymentsArray != selectedPaymentsArray_) {
        
        [selectedPaymentsArray_ removeAllObjects];
        
        for (NSObject *object in selectedPaymentsArray) {
            
            if ([object isKindOfClass:[Payment class]]) {
                
                [selectedPaymentsArray_ addObject:object];
                
            }
            
        }
        
    }
    
}

/**
 * Sets the company configuration
 */
- (void)setCompanyConfiguration:(PublicServiceResponse *)companyConfiguration {

    if (companyConfiguration_ != companyConfiguration) {
    
        [companyConfiguration retain];
        [companyConfiguration_ release];
        companyConfiguration_ = companyConfiguration;
    
    }
    
}

/**
 * Sets the cards array
 */
- (void)setCardsArray:(NSArray *)cardsArray {

    if (cardsArray_ != cardsArray) {
        
        if (cardsArray_ == nil) {
            
            cardsArray_ = [[NSMutableArray alloc] init];
            
        } else {
            
            [cardsArray_ removeAllObjects];
            
        }
        
        [cardsArray_ removeAllObjects];
        
        for (NSObject *object in cardsArray) {
            
            if ([object isKindOfClass:[Card class]]) {
                
                [cardsArray_ addObject:object];
                
            }
            
        }
        
    }
    
    if ([cardsArray_ count] > 0) {
        
        [self setSelectedCardIndex:0];
        
    } else {
        
        [self setSelectedCardIndex:-1];
        
    }

}

@end
