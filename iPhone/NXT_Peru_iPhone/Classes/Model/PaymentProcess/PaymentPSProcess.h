/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import <Foundation/Foundation.h>

#import "PaymentBaseProcess.h"


//Forward declarations
@class Card;
@class Payment;
@class PublicServiceResponse;
@class StatusEnabledResponse;
@class PaymentInstitutionsAndCompaniesInitialResponse;
@class PaymentInstitutionsAndCompaniesDetailResponse;


/**
 * PaymentPSProcess delegate protocol. Delegate is notified when any of the server operations has finished
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol PaymentPSProcessDelegate <PaymentBaseProcessDelegate>

@required

/**
 * Delegate is notified when the initialization has finished 
 */
- (void)initilizationHasFinished:(BOOL)isNextDynamicView;

@end


/**
 * Base class to represent a payment public service operation to help view controllers to display the 
 * information and perform the operations.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentPSProcess : PaymentBaseProcess {
    
@private
    
    /**
     * Selected company
     */
    NSString *selectedCompany_;
    
    /**
     * Selected company index
     */
    NSInteger selectedCompanyIndex_;
    
    /**
     * Selected company code.
     */
    NSString *selectedCompanyCode_;
    
    /**
     *  SelectedParameter
     */
    NSString *selectedCompanyParameter_;
    
    /**
     * Supplies
     */
    NSString *supplies_;
    
    /**
     * Payments array
     */
    NSMutableArray *paymentsArray_;

    /**
     * Selected payments array
     */
    NSMutableArray *selectedPaymentsArray_;
    
    /**
     * Client Name
     */
    NSString *clientName_;
    
    /**
     * Amount to pay
     */
    NSString *amountToPay_;
    
    /**
     * Company configuration
     */
    PublicServiceResponse *companyConfiguration_;
    
    /**
     * Response information
     */
    PaymentInstitutionsAndCompaniesDetailResponse *responseInformation_;
    
    /**
     * Initial institutions response
     */
    PaymentInstitutionsAndCompaniesInitialResponse *responseInitialResponse_;
    
    /**
     * Cards array
     */
    NSMutableArray *cardsArray_;
    
    /**
     * Selected card
     */
    Card *selectedCard_;
    
    /**
     * Has cards flag
     */
    BOOL hasCards_;
    
    /**
     * Selecting card flag
     */
    BOOL selectingCard_;
    
    /**
     * Selected locality index
     */
    NSInteger selectedLocalityIndex_;
    
    /**
     * Selected card index
     */
    NSInteger selectedCardIndex_;

    /**
     * Selected Method Index
     */
    NSInteger selectedMethodIndex_;
   
    /**
     * Currency PS
     */
    NSString *currencyPS_;

}

/**
 * Provides read-write access to the selectedCompany
 */
@property (nonatomic, readwrite, copy) NSString *selectedCompany;

/**
 * Provides read-write access to the selectedCompanyIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedCompanyIndex;

/**
 * Provides read-write access to the selected company code
 */
@property (nonatomic, readwrite, copy) NSString *selectedCompanyCode;

/**
 * Provides read-write access to the selected company parameter
 */
@property (nonatomic, readwrite, copy) NSString *selectedCompanyParameter;

/**
 * Provides read-write access to the supplies
 */
@property (nonatomic, readwrite, copy) NSString *supplies;

/**
 * Provides read-write access to the selectedPaymentsArray
 */
@property (nonatomic, readwrite, copy) NSArray *selectedPaymentsArray;

/**
 * Provides read-write access to the paymentsArray
 */
@property (nonatomic, readwrite, copy) NSArray *paymentsArray;

/**
 * Provides read-write access to the clientName
 */
@property (nonatomic, readwrite, copy) NSString *clientName;

/**
 * Provides read-write access to the amountToPay
 */
@property (nonatomic, readwrite, copy) NSString *amountToPay;

/**
 * Provides read-write access to the companyConfiguration
 */
@property (nonatomic, readwrite, retain) PublicServiceResponse *companyConfiguration;

/**
 * Provides read-write access to the responseInformation
 */
@property (nonatomic, readwrite, retain) PaymentInstitutionsAndCompaniesDetailResponse *responseInformation;

/**
 * Provides read-write access to the responseInitialResponse
 */
@property (nonatomic, readwrite, retain) PaymentInstitutionsAndCompaniesInitialResponse *responseInitialResponse;

/**
 * Provides read-write access to the pdDelegate
 */
@property (nonatomic, readwrite, assign) id<PaymentPSProcessDelegate> psDelegate;

/**
 * Provides read-write access to the cardsArray
 */
@property (nonatomic, readwrite, retain) NSArray *cardsArray;

/**
 * Provides read-write access to the cardsArray
 */
@property (nonatomic, readwrite, retain) Card *selectedCard;

/**
 * Provides read-write access to the hasCards
 */
@property (nonatomic, readwrite, assign) BOOL hasCards;

/**
 * Provides read-write access to the selectingCard
 */
@property (nonatomic, readwrite, assign) BOOL selectingCard;

/**
 * Provides read-write access to the selectedLocalityIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedLocalityIndex;

/**
 * Provides read-write access to the selectedCardIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedCardIndex;

/**
 * Provides read-write access to the selectedMethodIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedMethodIndex;

/**
 * Provides read-write access to the currencyPS
 */
@property (nonatomic, readwrite, copy) NSString *currencyPS;

/**
 * Checks if there is enough information to start the startPaymentInitialRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentInitialValidation;

/**
 * Performs the payment initial request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentInitialRequest;

/**
 * Notifies the payment operation process the initial response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The initial response received from the server
 */
- (void)initialResponseReceived:(NSNotification *)notification;


/**
 * Checks whether a receipt can be selected. Receipts must be selected from the futher past to the nearest past. One receipt with a date later that
 * an unselected receipt cannot be selected.
 *
 * @param receipt The receipt to check.
 * @param selectedReceipts The receipts that where selected so far.
 * @return YES when the receipt can be selected, NO otherwise.
 */
- (BOOL)canSelectReceipt:(Payment *)receipt
        forSelectedArray:(NSArray *)selectedReceipts;

/**
 * Corrects the selected receipts list, removing the receipts newer than th first not selected receipt.
 *
 * @param selectedReceiptList The selected receipt list to correct.
 * @return The corrected selected receipt list.
 */
- (NSArray *)correctSelectedReceiptsList:(NSArray *)selectedReceiptList;

@end
