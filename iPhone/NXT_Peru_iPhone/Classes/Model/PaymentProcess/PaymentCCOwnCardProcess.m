/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import "PaymentCCOwnCardProcess.h"

#import "AccountList.h"
#import "BankAccount.h"
#import "Card.h"
#import "CardList.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentBaseProcess.h"
#import "PaymentDataResponse.h"
#import "PaymentConfirmationResponse.h"
#import "PaymentSuccessResponse.h"
#import "PaymentCardProcess.h"
#import "Payment.h"
#import "PaymentList.h"
#import "Session.h"
#import "StringKeys.h"
#import "StringList.h"
#import "TagAndValue.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "Updater.h"

@implementation PaymentCCOwnCardProcess

#pragma mark -
#pragma mark Properties

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Information distribution


/**
 * Checks if there is enough information to start the startPaymentDataRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentDataValidation {
    
    NSString *result = nil;
    
//    if ([[self supplies] length] < [[self companyConfiguration] minLong]) {
//    
//        result = NSLocalizedString(PAYMENT_ERROR_SUPPLY_NUMBER_TEXT_KEY, nil);
//        
//    }
        
    return result;
    
}

/**
 * Performs the payment data request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentDataRequest {
    
    NSString *errorMessage = [self paymentDataValidation];
    
    if ((errorMessage == nil) || ([errorMessage isEqualToString:@""])) {
     
		[[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(dataResponseReceived:) 
                                                     name:kNotificationPaymentDataResultEnds object:nil];
        
        NSString *cardNumber = [[[self ownCardSelected] cardNumber] substringFromIndex:([[[self ownCardSelected] cardNumber] length] - 4)];
        [[Updater getInstance] obtainPaymentCardContinentalOwnAccountDataForIssue:cardNumber];
        
    } else {
    
        [Tools showInfoWithMessage:errorMessage];
    
    }   
    
}

/**
 * Notifies the payment operation process the data response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The data response received from the server
 * @return YES when the data response is correct, NO otherwise
 */
- (void)dataResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentDataResultEnds 
                                                  object:nil];
    [[self appDelegate] hideActivityIndicator];

    PaymentDataResponse *response = (PaymentDataResponse *)[notification object];
	
	if (![response isError]) {

		// Accounts
		AccountList *bankAccounts = [[Session getInstance] accountList];
		NSMutableArray *accounts = [NSMutableArray array];
		NSArray *array = [NSArray arrayWithArray:[[response numberAccountList] stringList]];
		
		for (NSString *numberAccount in array) {
			
			BankAccount *result = [bankAccounts accountFromAccountTerminateNumber:numberAccount];
			
			if (result != nil) {
				
				[accounts addObject:result];
				
			}
            
		}
        
        [self setAccountsArray:accounts];
        
        // Data information
        
        NSMutableArray *informationArray = [[[NSMutableArray alloc] init] autorelease];
        
        // ACCOUNT STATUS
        
        // - Credit line
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_CREDIT_LINE_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response creditLimit]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Available credit
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_AVAILABLE_CREDIT_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response availableAmount]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }

        // - Currency
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_CURRENCY_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools getCurrencyLiteral:[response cardCurrency]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }

        // - Last payment date
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_LAST_PAYMENT_DAY_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response lastPaymentDate]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }

        // - Total debt Soles
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_TOTAL_DEBT_SOLES_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response totalDebtSoles]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Total debt Dolars
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_TOTAL_DEBT_DOLARS_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response totalDebtDolars]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }

        
        // SUMMARY
        
        // - Minimum payment soles
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_MIN_PAYMENT_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:CURRENCY_SOLES_LITERAL], [Tools notNilString:[response minPayment]]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Delay soles
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_DELAY_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:CURRENCY_SOLES_LITERAL], [Tools notNilString:[response monthInstalment]]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Month total payment soles
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_MONTH_TOTAL_PAYMENT_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:CURRENCY_SOLES_LITERAL], [Tools notNilString:[response monthTotal]]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Minimum payment dolars
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_MIN_PAYMENT_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:CURRENCY_DOLARES_LITERAL], [Tools notNilString:[response minPaymentDolar]]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Delay dolars
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_DELAY_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:CURRENCY_DOLARES_LITERAL], [Tools notNilString:[response monthInstalmentDolar]]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Month total payment dolars
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_MONTH_TOTAL_PAYMENT_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:CURRENCY_DOLARES_LITERAL], [Tools notNilString:[response monthTotalDolar]]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }

        [self setDataInfoArray:informationArray];
        
        [[self delegate] dataAnalysisHasFinished];

	}
        
}

/**
 * Checks if there is enough information to start the startPaymentConfirmationRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentConfirmationValidation {
    
    NSString *result = @"";
 
    if ([self selectedAccountIndex] < 0) {
    
        result = NSLocalizedString(PAYMENT_ERROR_ACCOUNT_PAYMENT_TEXT_KEY, nil);
    
    } else if (([self amount] == nil || [[self amount] isEqualToString:@""])) {
    
        result = NSLocalizedString(CARD_PAYMENT_SELECT_AMOUNT_TEXT_KEY, nil);
    
    } else if ([self selectedCurrencyIndex] < 0){
    
        result = NSLocalizedString(TRANSFER_MUST_SELECT_CURRENCY_ERROR_TEXT_KEY, nil);
    
    } else {
    
        result = [super paymentConfirmationValidation];
    
    }

    return result;
    
}

/**
 * Performs the payment confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentConfirmationRequest {
    
    NSString *errorMessage = [self paymentConfirmationValidation];
    
    if ((errorMessage == nil) || ([errorMessage isEqualToString:@""])) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(confirmationResponseReceived:) 
                                                     name:kNotificationPaymentConfirmationResultEnds object:nil];
        
        [self setSelectedAccount:[[self accountsArray] objectAtIndex:[self selectedAccountIndex]]];
        [self setSelectedCurrency:[[self currencyArray] objectAtIndex:[self selectedCurrencyIndex]]];
    
        [[Updater getInstance] obtainPaymentCardContinentalOwnAccountConfirmationForIssue:[[self selectedAccount] branchAccount] 
                                                                                 currency:[self selectedCurrency] 
                                                                                   amount:[Tools formatAmountWithDotDecimalSeparator:[self amount]]
                                                                                   email1:[Tools notNilString:[self destinationEmail1]]
                                                                                   email2:[Tools notNilString:[self destinationEmail2]]
                                                                             phoneNumber1:[Tools notNilString:[self destinationSMS1]]
                                                                             phoneNumber2:[Tools notNilString:[self destinationSMS2]] 
                                                                                 carrier1:[Tools notNilString:[self carrierLiteralForCarrier:[self selectedCarrier1Index]]]
                                                                                 carrier2:[Tools notNilString:[self carrierLiteralForCarrier:[self selectedCarrier2Index]]]
                                                                                  message:[Tools notNilString:[self emailMessage]]];
    
    } else {
    
        [Tools showInfoWithMessage:errorMessage];
    
    }
    
}

/**
 * Notifies the payment operation process the confirmation response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The confirmation response received from the server
 */
- (void)confirmationResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentConfirmationResultEnds 
                                                  object:nil];
    [[self appDelegate] hideActivityIndicator];
    
    PaymentConfirmationResponse *response = (PaymentConfirmationResponse *)[notification object];
	
	if (![response isError]) {
    
        
        NSString *amount = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response amountToChargeCurrency]]], [Tools notNilString:[response amountToCharge]]];
        
        NSString *account = [NSString stringWithFormat:@"%@ | %@\n%@", 
                             [Tools notNilString:[[self selectedAccount] accountType]], 
                             [Tools getCurrencyLiteral:[[self selectedAccount] currency]],
                             [Tools notNilString:[[self selectedAccount] number]]];
        
        NSString *card = [NSString stringWithFormat:@"%@\n%@", [Tools notNilString:[[self ownCardSelected] cardType]], [Tools notNilString:[[self ownCardSelected] cardNumber]]];

        
        self.coordHint = [response coordinate];
        self.seal = [response seal];
        
        // Confirmation information
        
        NSMutableArray *informationArray = [[[NSMutableArray alloc] init] autorelease];
        
        // - Amount to pay
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_AMOUNT_TO_PAY_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:amount]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Operation
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OPERATION_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(CARD_PAYMENT_OWN_CARD_TEXT_LOWER_KEY, nil)]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Account
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_ORIGIN_ACCOUNT_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:account]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Card
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:card]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        [self setConfirmationInfoArray:informationArray];
        
        [[self delegate] confirmationAnalysisHasFinished];
        
    }
    
}

/**
 * Checks if there is enough information to start the startPaymentSuccessRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentSuccessDataValidation {
    
    return [super paymentSuccessDataValidation];
    
}

/**
 * Performs the payment success request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (void)startPaymentSuccessRequest {
    
    [[self appDelegate] showActivityIndicator:poai_Both];
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(paymentSuccessResponseReceived:) 
                                                 name:kNotificationPaymentSuccessResultEnds object:nil];
    
    [[Updater getInstance] obtainPaymentCardContinentalOwnAccountSuccess];
    

}

/**
 * Notifies the payment operation process the success response received from the server. The payment operation process notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The success response received from the server
 */
- (void)paymentSuccessResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentSuccessResultEnds 
                                                  object:nil];
    [[self appDelegate] hideActivityIndicator];
    
    PaymentSuccessResponse *response = (PaymentSuccessResponse *)[notification object];
	
	if (![response isError]) {
        
        [self setMessage:[Tools notNilString:[response message]]];
        
        NSString *account = [NSString stringWithFormat:@"%@ | %@\n%@", 
                             [Tools notNilString:[[self selectedAccount] accountType]], 
                             [Tools getCurrencyLiteral:[[self selectedAccount] currency]],
                             [Tools notNilString:[[self selectedAccount] number]]];
        
        NSString *card = [NSString stringWithFormat:@"%@\n%@", [Tools notNilString:[[self ownCardSelected] cardType]], [Tools notNilString:[[self ownCardSelected] cardNumber]]];
        
        NSString *amountPaid = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response totalPaidCurrency]]], [Tools notNilString:[response totalPaid]]];
        
        NSString *amount = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response totalChargedCurrency]]], [Tools notNilString:[response totalCharged]]];
        
        // Success information
        
        NSMutableArray *informationArray = [[[NSMutableArray alloc] init] autorelease];
                
        // - Operation number
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OPERATION_NUMBER_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response operationNumber]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Operation date
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OPERATION_DATE_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response operationDate]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Value date
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_VALUE_DATE_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response valueDate]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
    
        // - Operation
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OPERATION_TEXT_KEY, nil)];
                    
        [titleAndAttributes addAttribute:[Tools notNilString:[NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(CARD_PAYMENT_OWN_CARD_TEXT_LOWER_KEY, nil)]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Account
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_ORIGIN_ACCOUNT_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:account]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Card
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:card]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
            
        // - Amount payed
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_CHARGED_AMOUNT_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:amount]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        if (([response changeType] != nil) && ([[response changeType] floatValue] != 0.0f)) {
            
            // - Change type
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
            [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_CHAGE_TYPE_TEXT_KEY, nil)];
            
            NSString *changeType = [NSString stringWithFormat:@"%@ %@",
                                    [Tools getCurrencySimbol:[Tools notNilString:[response changeTypeCurrency]]],
                                    [Tools notNilString:[response changeType]]];
                        
            [titleAndAttributes addAttribute:changeType];
            
            if (titleAndAttributes != nil) {
                [informationArray addObject:titleAndAttributes];
            }
            
        }
        
        // - Paid amount
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_PAYED_AMOUNT_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:amountPaid]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        [self setSuccessInfoArray:informationArray];
        
        [[self delegate] successAnalysisHasFinished];
        
    } else {
            
        [[self delegate] successAnalysisHasFinishedWithError];
            
    }

}

/**
 * Returns the payment operation type
 *
 * @return The payment operation type
 */
- (PaymentTypeEnum)paymentOperationType {
    
    return PTEPaymentContOwnCard;
    
}

@end
