//
//  PaymentISProcess.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 31/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentBaseProcess.h"
#import "SafetyPayStatusResponse.h"
#import "SafetyPayTransactionInfoResponse.h"

@protocol PaymentISProcessDelegate <PaymentBaseProcessDelegate>

@required

- (void)initilizationHasFinished;

@end

@interface PaymentISProcess : PaymentBaseProcess {

@private
    
    NSString *selectedCompany_;
    
    NSInteger *selectedCompanyIndex_;
    
    NSString *selectedCompanyCode_;
    
    NSString *selectedCompanyParameter_;
    
    NSString *transactionNumber_;
    
    NSString *amountToPay_;
    
    SafetyPayStatusResponse *internetShoppingPaymentStatus_;
    
    SafetyPayTransactionInfoResponse *internetShoppingTransactionInformation_;
    
}

@property (nonatomic, readwrite, copy) NSString *selectedCompany;

@property (nonatomic, readwrite, assign) NSInteger *selectedCompanyIndex;

@property (nonatomic, readwrite, copy) NSString *selectedCompanyCode;

@property (nonatomic, readwrite, copy) NSString *selectedCompanyParameter;

@property (nonatomic, readwrite, copy) NSString *transactionNumber;

@property (nonatomic, readwrite, copy) NSString *amountToPay;

@property (nonatomic, readwrite, retain) SafetyPayStatusResponse *internetShoppingPaymentStatus;

@property (nonatomic, readwrite, retain) SafetyPayTransactionInfoResponse *internetShoppingTransactionInformation;

@property (nonatomic, readwrite, assign) id<PaymentISProcessDelegate> internetShoppingDelegate;

@property (nonatomic, readwrite, assign) NSInteger *selectedLocalityIndex;

@property (nonatomic, readwrite, assign) NSInteger *selectedMethodIndex;

@property (nonatomic, readwrite, copy) NSString *currencyIS;

-(NSString *) paymentInitialValidation;

-(void) startPaymentInitialRequest;

-(void) initialResponseReceived:(NSNotification *) notification;



@end
