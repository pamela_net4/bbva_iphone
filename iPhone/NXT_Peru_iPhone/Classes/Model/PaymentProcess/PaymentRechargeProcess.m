/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "PaymentRechargeProcess.h"

#import "AccountList.h"
#import "BankAccount.h"
#import "Carrier.h"
#import "CarrierList.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "NXT_Peru_iPhoneStyler.h"
#import "PaymentRechargeInitialResponse.h"
#import "PaymentConfirmationResponse.h"
#import "PaymentSuccessResponse.h"
#import "Session.h"
#import "StringKeys.h"
#import "StringList.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "Updater.h"


#pragma mark -

@interface PaymentRechargeProcess()

/**
 * Provides read-write access to the recharge currency.
 */
@property (nonatomic, readwrite, copy) NSString *rechargeCurrency;

/**
 * Provides read-write access to the extra info.
 */
@property (nonatomic, readwrite, copy) NSString *extraInfo;

@end


#pragma mark -

@implementation PaymentRechargeProcess

#pragma mark -
#pragma mark Properties

@synthesize phoneNumber = phoneNumber_;
@synthesize amount = amount_;
@synthesize company = company_;
@synthesize selectedCarrierRechargeIndex = selectedCarrierRechargeIndex_;
@synthesize carrierRechargeList = carrierRechargeList_;
@synthesize longMaxSumi = longMaxSumi_;
@synthesize longMinSumi = longMinSumi_;
@synthesize rechargeCurrency = rechargeCurrency_;
@synthesize extraInfo = extraInfo_;

#pragma mark -
#pragma mark Initialization

/**	
 * Object initializer
 *
 * @return An initialized instance
 */
- (id) init {
    
	if (self = [super init]) {
        
        carrierRechargeList_ = [[NSMutableArray alloc] init];
        extraInfo_ = @"";
	}
	
	return self;
    
}

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [phoneNumber_ release];
    phoneNumber_ = nil;
    
    [amount_ release];
    amount_ = nil;
    
    [company_ release];
    company_ = nil;
    
    [carrierRechargeList_ release];
    carrierRechargeList_ = nil;
    
    [rechargeCurrency_ release];
    rechargeCurrency_ = nil;
    
    [extraInfo_ release];
    extraInfo_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Information distribution

- (BOOL)startFrequentOperationReactiveRequest
{
    
    if([[company_.carrier lowercaseString] isEqualToString:@"claro"] || [[company_.carrier lowercaseString] isEqualToString:@"clar"])
    {
        [[Updater getInstance] obtainRechargeClaroCellPhoneFrequentOperationReactiveStepOneWithOperation:@"Recarga de celular - Claro" andServiceType:@"15" andAccount:@"cuenta" andCellPhoneNumber:phoneNumber_];
        
    } else if ([[company_.carrier lowercaseString] isEqualToString:@"movistar"] || [[company_.carrier lowercaseString] isEqualToString:@"movi"])
    {
        
    [[Updater getInstance] obtainRechargeMovistarCellPhoneFrequentOperationReactiveStepOneWithOperation:@"Recarga de celular - Movistar" andCompany:company_.carrier andServiceType:@"14" andCellPhoneNumber:phoneNumber_ andAccount:@"cuenta" andAmount:amount_];
    
    }
    return YES;
}

/**
 * Checks if there is enough information to start the startPaymentDataRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentDataValidation {
    
    NSString *result = @"";
    
    return result;
    
}

/**
 * Performs the payment data request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentDataRequest {
    
    NSString *errorMessage = [self paymentDataValidation];
    
    if ((errorMessage == nil) || ([errorMessage isEqualToString:@""])) {
     
		[[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(dataResponseReceived:) 
                                                     name:kNotificationPaymentRechargesInitialResultEnds object:nil];
        
        if (company_ == nil) {
            
            [[Updater getInstance] obtainPaymentRechargeDataForCompany:kCarrierClaroCode];

        } else {
            
            [[Updater getInstance] obtainPaymentRechargeDataForCompany:[[company_ carrier] lowercaseString]];
        
        }
        
        
    } else {
    
        [Tools showInfoWithMessage:errorMessage];
    
    }   
    
}

/**
 * Notifies the payment operation process the data response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The data response received from the server
 * @return YES when the data response is correct, NO otherwise
 */
- (void)dataResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentRechargesInitialResultEnds 
                                                  object:nil];
    [[self appDelegate] hideActivityIndicator];

    PaymentRechargeInitialResponse *response = (PaymentRechargeInitialResponse *)[notification object];
	
	if (![response isError]) {
        
		// Accounts
		AccountList *bankAccounts = [[Session getInstance] accountList];
		NSMutableArray *accounts = [NSMutableArray array];
		NSArray *array = [NSArray arrayWithArray:[[response numberAccountList] stringList]];
		
		for (NSString *numberAccount in array) {
			
			BankAccount *result = [bankAccounts accountFromAccountTerminateNumber:numberAccount];
			
			if (result != nil) {
				
				[accounts addObject:result];
				
			}
            
		}
        
        [self setAccountsArray:accounts];

        [carrierRechargeList_ removeAllObjects];
        
        array = [NSArray arrayWithArray:[[response carrierList] carrierList]];
        
        for (NSObject *carrier in array) {
            
            if ([carrier isKindOfClass:[Carrier class]]) {
                
                [carrierRechargeList_ addObject:carrier];
                
            }
            
        }
                
        isAgreement_ = [response agreement];
        longMaxSumi_ = [response maxLong];
        longMinSumi_ = [response minLong];
        self.rechargeCurrency = [response currency];
        
        [[self delegate] dataAnalysisHasFinished];

	}
        
}

/**
 * Checks if there is enough information to start the startPaymentConfirmationRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentConfirmationValidation {
    
    NSString *result = @"";

    if (selectedCarrierRechargeIndex_ < 0) {
       
        result = NSLocalizedString(PAYMENT_ERROR_CARRIER_TEXT_KEY, nil);

    } else if ([phoneNumber_ length] < longMinSumi_) {
        
        result = NSLocalizedString(PAYMENT_RECHARGE_ERROR_NUMBER_OF_PHONE_TEXT_KEY, nil);
        
    } else if([[amount_ description] isEqualToString:@""]){
     
        result = NSLocalizedString(PAYMENT_RECHARGE_ERROR_EMPTY_MONTO_TEXT_KEY, nil);
        
    } else if ([self selectedAccountIndex] < 0) {
        
        result = NSLocalizedString(PAYMENT_RECHARGE_ERROR_INVALID_NUMBER_TEXT_KEY, nil);
        
    }  else {
    
        Carrier *carrier = [carrierRechargeList_ objectAtIndex:selectedCarrierRechargeIndex_];
        
        NSDecimalNumber *amount = [Tools decimalNumberFromLocalString:amount_];
        
        if ([[carrier max] compare:amount] == NSOrderedAscending) {
            result = NSLocalizedString(PAYMENT_RECHARGE_ERROR_INVALID_MONTO_TEXT_KEY, nil);
        } else if ([amount compare:[carrier min]] == NSOrderedAscending) {
            result = NSLocalizedString(PAYMENT_RECHARGE_ERROR_INVALID_MONTO_TEXT_KEY, nil);
        }
        
    }
    
    if ((result == nil) || [result isEqualToString:@""]) {
        
        result = [super paymentConfirmationValidation];
        
    }
    
    return result;
    
}

/**
 * Performs the payment confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentConfirmationRequest {
    
    NSString *errorMessage = [self paymentConfirmationValidation];
    
    if ((errorMessage == nil) || ([errorMessage isEqualToString:@""])) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(confirmationResponseReceived:) 
                                                     name:kNotificationPaymentConfirmationResultEnds object:nil];
			    	
        [self setSelectedAccount:[[self accountsArray] objectAtIndex:[self selectedAccountIndex]]];
        
		[[Updater getInstance] obtainPaymentRechargeConfirmationForPhoneNumber:phoneNumber_
																		 issue:[[self selectedAccount] branchAccount]
																		amount:[Tools formatAmountWithDotDecimalSeparator:amount_]
																		email1:[Tools notNilString:[self destinationEmail1]] 
                                                                        email2:[Tools notNilString:[self destinationEmail2]] 
                                                                  phoneNumber1:[Tools notNilString:[self destinationSMS1]] 
                                                                  phoneNumber2:[Tools notNilString:[self destinationSMS2]] 
                                                                      carrier1:[Tools notNilString:[self carrierLiteralForCarrier:[self selectedCarrier1Index]]]
                                                                      carrier2:[Tools notNilString:[self carrierLiteralForCarrier:[self selectedCarrier2Index]]]
                                                                       message:[self emailMessage]];
    
    } else {
    
        [Tools showInfoWithMessage:errorMessage];
    
    }   
    
}

/**
 * Notifies the payment operation process the confirmation response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The confirmation response received from the server
 */
- (void)confirmationResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentConfirmationResultEnds 
                                                  object:nil];
    [[self appDelegate] hideActivityIndicator];
    
    PaymentConfirmationResponse *response = (PaymentConfirmationResponse *)[notification object];
	
	if (![response isError]) {
        
        self.coordHint = [response coordinate];
        self.seal = [response seal];
        
        // Confirmation information
        
        NSMutableArray *informationArray = [[[NSMutableArray alloc] init] autorelease];
        
        // - amount to recharge
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_AMOUNT_TO_RECHARGE_TEXT_KEY, nil)];                
        
        [titleAndAttributes addAttribute:[Tools formatAmount:[Tools decimalNumberFromLocalString:amount_] withCurrency:[Tools mainCurrencyLiteral]]] ;
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Operation
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OPERATION_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:NSLocalizedString(PAYMENT_RECHARGE_TITLE_TEXT_KEY, nil)];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Company
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_BUSINESS_TEXT_KEY, nil)];
        
        NSString *nameToShow = [company_ carrierDisplayName];
        
        [titleAndAttributes addAttribute:[Tools notNilString:nameToShow]];
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Account
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_ORIGIN_ACCOUNT_TEXT_KEY, nil)];
        
        NSString *account = [NSString stringWithFormat:@"%@ | %@ %@", 
                             [Tools notNilString:[[self selectedAccount] accountType]],
                             [Tools getCurrencyLiteral:[[self selectedAccount] currency]],
                             [Tools notNilString:[[self selectedAccount] number]]];
        
        [titleAndAttributes addAttribute:[Tools notNilString:account]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Phone number
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PUBLIC_SERVICE_STEP_ONE_PHONE_NUMBER_OR_CLIENT_CODE_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:phoneNumber_]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
    
        
        // - Change type
        if (([response typeChange] != nil) && ![[response typeChange] isEqualToString:@""])  {
        
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
            [titleAndAttributes setTitleString:NSLocalizedString(EXCHANGE_RATE_TEXT_KEY, nil)];
            
            [titleAndAttributes addAttribute:[Tools notNilString:[response typeChange]]];
            
            if (titleAndAttributes != nil) {
                [informationArray addObject:titleAndAttributes];
            }
        
        }

        [self setConfirmationInfoArray:informationArray];
        
        [[self delegate] confirmationAnalysisHasFinished];
        
    } else {
        
        NSString *errorMessage = [response errorMessage];
        
        if ([errorMessage length] > 0) {
            
            [Tools showInfoWithMessage:errorMessage];
            
        } else {
            
            [Tools showInfoWithMessage:NSLocalizedString(GENERAL_ERROR2_KEY,nil)];
        }
        
    }
    
}

/**
 * Checks if there is enough information to start the startPaymentSuccessRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentSuccessDataValidation {
    
    return [super paymentSuccessDataValidation];
    
}

/**
 * Performs the payment success request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (void)startPaymentSuccessRequest {
    
    NSString *errorMessage = [self paymentSuccessDataValidation];
    
    if ((errorMessage == nil) || ([errorMessage isEqualToString:@""])) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(paymentSuccessResponseReceived:) 
                                                     name:kNotificationPaymentSuccessResultEnds object:nil];
        
        [[Updater getInstance] obtainPaymentRechargeSuccessForSecondFactorKey:[self secondFactorKey]
                                                                      carrier:[[company_ carrier] lowercaseString]];
        
    } else {
    
        [Tools showInfoWithMessage:errorMessage];
    
    }

    
}

/**
 * Notifies the payment operation process the success response received from the server. The payment operation process notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The success response received from the server
 */
- (void)paymentSuccessResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentSuccessResultEnds 
                                                  object:nil];
    [[self appDelegate] hideActivityIndicator];
    
    PaymentSuccessResponse *response = (PaymentSuccessResponse *)[notification object];
	
	if (![response isError]) {
        
        [self setExtraInfo:nil];
        
        if ([[[company_ carrier] lowercaseString] isEqualToString:kCarrierClaroCode]) {
            
            NSString *breakdown = [Tools notNilString:[response claroBreakdown]];
            NSString *customerService = [Tools notNilString:[response claroBalanceConsult]];
            NSString *daysUse = [Tools notNilString:[response claroDaysUse]];
            
            [self setExtraInfo:[[NSString stringWithFormat:NSLocalizedString(PAYMENT_RECHARGE_OPERATION_CLARO_TEXT_KEY, nil), daysUse, customerService, breakdown] retain]];
            
        } else if ([[[company_ carrier] lowercaseString] isEqualToString:kCarrierMovistarCode]) {
            
            NSString *breakdown = [Tools notNilString:[response breakdown]];
            NSString *customerService = [Tools notNilString:[response customerService]];
            
            [self setExtraInfo:[[NSString stringWithFormat:NSLocalizedString(PAYMENT_RECHARGE_OPERATION_MOVISTAR_TEXT_KEY, nil), customerService, breakdown] retain]];
            
        }
        
        [self setMessage:[Tools notNilString:[response message]]];
        
        // Success information
        
        NSMutableArray *informationArray = [[[NSMutableArray alloc] init] autorelease];
        
        // - Operation number
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OPERATION_NUMBER_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response operationNumber]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Date/Time
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(DATE_TRANSFER_TITLE_TEXT_KEY, nil)];

        NSString *dateString = @"";
        
        if (([[response transactionDate] length] > 0) && ([[response transactionHour] length] > 0)) {
            
            dateString = [NSString stringWithFormat:@"%@ | %@", [response transactionDate], [response transactionHour]];
            
        } else {
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat: @"dd/MM/yyyy | HH:mm"];
            dateString = [dateFormat stringFromDate:[NSDate date]];
            [dateFormat release];
            
        }
        
        [titleAndAttributes addAttribute:[Tools notNilString:dateString]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Operation
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OPERATION_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response operation]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Company
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_BUSINESS_TEXT_KEY, nil)];
        
        NSString *nameToShow = [company_ carrierDisplayName];
        
        [titleAndAttributes addAttribute:[Tools notNilString:nameToShow]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Account
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_ORIGIN_ACCOUNT_TEXT_KEY, nil)];
        
        NSString *account = [NSString stringWithFormat:@"%@ | %@ %@", 
                             [Tools notNilString:[[self selectedAccount] accountType]],
                             [Tools getCurrencyLiteral:[[self selectedAccount] currency]],
                             [Tools notNilString:[[self selectedAccount] number]]];
        
        [titleAndAttributes addAttribute:[Tools notNilString:account]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Phone number
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PUBLIC_SERVICE_STEP_ONE_PHONE_NUMBER_OR_CLIENT_CODE_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:phoneNumber_]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Recharge amount
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_AMOUNT_OF_RECHARGE_TEXT_KEY, nil)];
        
        NSString *rechargeTotal = [NSString stringWithFormat:@"%@ %@",
                                   [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response amountRechargeCurrency]]], 
                                   [Tools notNilString:[response amountRecharge]]];
        
        [titleAndAttributes addAttribute:[Tools notNilString:rechargeTotal]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        
        // - Change type
        if (([response changeType] != nil) && ([[response changeType] floatValue] != 0.0f))  {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
            [titleAndAttributes setTitleString:NSLocalizedString(EXCHANGE_RATE_TEXT_KEY, nil)];
            
            NSString *change = [NSString stringWithFormat:@"%@ %@",
                                [Tools clientCurrencySymbolForServerCurrency:[response changeTypeCurrency]],
                                [Tools notNilString:[response changeType]]];
            
            [titleAndAttributes addAttribute:[Tools notNilString:change]];
            
            if (titleAndAttributes != nil) {
                [informationArray addObject:titleAndAttributes];
            }
            
        }   
        
        // - Amount charged
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_CHARGED_AMOUNT_TEXT_KEY, nil)];
        
        NSString *totalCharged = [NSString stringWithFormat:@"%@ %@",
                                  [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response totalChargedCurrency]]], 
                                  [Tools notNilString:[response totalCharged]]];
        
        [titleAndAttributes addAttribute:[Tools notNilString:totalCharged]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Days of use
        
        if ([[[company_ carrier] lowercaseString] isEqualToString:@"movi"]) {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
            [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_DAYS_OF_USE_TEXT_KEY, nil)];
            
            NSString *useDays = [NSString stringWithFormat:@"%@ días", [Tools notNilString:[response daysUse]]];
            
            [titleAndAttributes addAttribute:[Tools notNilString:useDays]];
            
            if (titleAndAttributes != nil) {
                [informationArray addObject:titleAndAttributes];
            }
        }
        
        [self setSuccessInfoArray:informationArray];
        
        [[self delegate] successAnalysisHasFinished];
        
    } else {
        
        [[self delegate] successAnalysisHasFinishedWithError];
        
    }
    
}

/**
 * Returns the payment operation type
 *
 * @return The payment operation type
 */
- (PaymentTypeEnum)paymentOperationType {
    
    return PTEPaymentRecharge;
    
}

/**
 * Returns a success extra info text message
 */
- (NSString *)successExtraInfoText {
    
    if (extraInfo_ != nil) {
        
        return extraInfo_;
    
    } else {
        
        return @"";
    
    }
    
}


@end
