/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import <Foundation/Foundation.h>
#import "PaymentCardProcess.h"

@class StatusEnabledResponse;

/**
 * Base class to represent a payment public service operation to help view controllers to display the 
 * information and perform the operations.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentCCThirdCardProcess : PaymentCardProcess {
@private
    /**
     * Operation
     **/
    NSString *operation_;
    /**
     * Card owner
     */
    NSString *responseCardOwner_;
    
    /**
     * Card type
     */
    NSString *responseCardType_;
    
}
/**
 * Provides read-write access to the card owner.
 */
@property (nonatomic, readwrite, copy) NSString *responseCardOwner;

/**
 * Provides read-write access to the card type.
 */
@property (nonatomic, readwrite, copy) NSString *responseCardType;

/**
 * Provides read-write access to the operation.
 */
@property (nonatomic, readwrite, copy) NSString *operation;
@end
