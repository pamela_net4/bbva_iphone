/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import <Foundation/Foundation.h>
#import "PaymentCardProcess.h"

@class StatusEnabledResponse;

/**
 * Base class to represent a payment public service operation to help view controllers to display the 
 * information and perform the operations.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentCOtherBankProcess : PaymentCardProcess {
@private

    /**
     * Class
     */
    NSString *selectedClass_;
    
    /**
     * Destination bank
     */
    NSString *destinationBank_;
    
    /**
     * Destination bank name
     */
    NSString *destinationBankName_;
    
    /**
     * Tax
     */
    NSString *tax_;
    
    /**
     * Network use
     */
    NSString *networkUse_;
        
    /**
     * Amount to transfer
     */
    NSString *amountToTransfer_;
    
    NSString *commissionN_;
    
    NSString *commissionL_;
    
    NSString *totalPayN_;
    
    NSString *totalPayL_;
    
    NSString *typeFlow_;
    
    NSString *typeFlowChoosen_;

    NSString *disclaimer_;
    
    PaymentConfirmationResponse *responseConfirmation_;
}
/**
 * Provides read-write access to the selectedClass
 */
@property (nonatomic, readwrite,retain) PaymentConfirmationResponse *responseConfirmation;

/**
 * Provides read-write access to the selectedClass
 */
@property (nonatomic, readwrite, copy) NSString *selectedClass;

/**
 * Provides read-write access to the destinationBank
 */
@property (nonatomic, readwrite, copy) NSString *destinationBank;

/**
 * Provides read-write access to the destinationBankName
 */
@property (nonatomic, readwrite, copy) NSString *destinationBankName;

/**
 * Provides read-write access to the networkUse
 */
@property (nonatomic, readwrite, copy) NSString *networkUse;

/**
 * Provides read-write access to the commissionN
 */
@property (nonatomic, readwrite, copy) NSString *commissionN;

/**
 * Provides read-write access to the commissionL
 */
@property (nonatomic, readwrite, copy) NSString *commissionL;

/**
 * Provides read-write access to the totalPayN
 */
@property (nonatomic, readwrite, copy) NSString *totalPayN;

/**
 * Provides read-write access to the totalPayL
 */
@property (nonatomic, readwrite, copy) NSString *totalPayL;

/**
 * Provides read-write access to the typeFlow
 */
@property (nonatomic, readwrite, copy) NSString *typeFlow;

/**
 * Provides read-write access to the typeFlow
 */
@property (nonatomic, readwrite, copy) NSString *typeFlowChoosen;


/**
 * Provides read-write access to the typeFlow
 */
@property (nonatomic, readwrite, copy) NSString *disclaimer;
- (void)startTINPaymentConfirmationRequest;

@end
