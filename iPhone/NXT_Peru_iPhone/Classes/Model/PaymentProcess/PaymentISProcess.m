//
//  PaymentISProcess.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 31/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "PaymentISProcess.h"
#import "StringKeys.h"

@implementation PaymentISProcess

@synthesize selectedCompany = selectedCompany_;
@synthesize internetShoppingDelegate = internetShoppingDelegate_;
@synthesize internetShoppingPaymentStatus = internetShoppingPaymentStatus_;
@synthesize internetShoppingTransactionInformation = internetShoppingTransactionInformation_;
/*@synthesize transactionNumber = transactionNumber_;
@synthesize amountToPay = amountToPay_;*/

- (void) dealloc {

    [selectedCompany_ release];
    selectedCompany_ = nil;
    
    [super dealloc];
}

- (NSString *) paymentInitialValidation {
    
    NSString *result = nil;
    
    if (([self selectedCompany] == nil) || [[self selectedCompany] isEqualToString:@""]) {
        
        result = NSLocalizedString(INTERNET_SHOPPING_STEP_ONE_SELECT_BUSINESS_KEY, nil);
        
    }
    
    return result;

}

- (void) startPaymentInitialRequest{
    
}


- (void) initialResponseReceived:(NSNotification *)notification {}


- (id<PaymentISProcessDelegate>)internetShoppingDelegate {
    
    id<PaymentISProcessDelegate> result = nil;
    
    id<PaymentBaseProcessDelegate> superclassDelegate = self.delegate;
    
    if ([(NSObject *)superclassDelegate conformsToProtocol:@protocol(PaymentISProcessDelegate)]) {
        
        result = (id<PaymentISProcessDelegate>) superclassDelegate;
        
    }
    
    return result;
    
}

- (void) setInternetShoppingDelegate : (id<PaymentISProcessDelegate>) internetShoppingDelegate {
    
    self.delegate = internetShoppingDelegate;
    
}

@end
