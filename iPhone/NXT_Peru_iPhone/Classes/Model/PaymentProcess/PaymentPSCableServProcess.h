//
//  PaymentPSCableServProcess.h
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 11/28/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "PaymentPSProcess.h"

@class StatusEnabledResponse;

/**
 * Base class to represent a payment public service operation to help view controllers to display the
 * information and perform the operations.
 *
 * @author <a href="http://www.mdp.com">MDP Consulting</a>
 */
@interface PaymentPSCableServProcess : PaymentPSProcess{
@private

}

@end
