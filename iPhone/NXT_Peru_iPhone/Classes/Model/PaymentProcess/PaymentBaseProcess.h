/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import <Foundation/Foundation.h>
#import "PaymentsConstants.h"

#define PAYMENT_PASSWORD_OPERATION_KEY_ERROR                                                        @"-205"

@class BankAccount;
@class NXT_Peru_iPhone_AppDelegate;
@class PaymentDataResponse;
@class PaymentConfirmationResponse;
@class PaymentSuccessResponse;
@class StatusEnabledResponse;
@class FOEServicePaymentStepOneResponse;

/**
 * PaymentBaseProcess delegate protocol. Delegate is notified when any of the server operations has finished
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@protocol PaymentBaseProcessDelegate
@optional
/**
 * Delegate is notified when the data analysis has finished 
 */
- (void)dataAnalysisHasFinished;

/**
 * Delegate is notified when the confirmation analysis has finished
 */
- (void)confirmationTINAnalysisHasFinished;

/**
 * Delegate is notified when the confirmation analysis has finished 
 */
- (void)confirmationAnalysisHasFinished;

/**
 * Delegate is notified when the success analysis has finished 
 */
- (void)successAnalysisHasFinished;

/**
 * Delegate is notified when the success analysis has finished with error
 */
- (void)successAnalysisHasFinishedWithError;

@end


/**
 * Base class to represent a payment operation to help view controllers to display the 
 * information and perform the operations.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentBaseProcess : NSObject {
@private
    
    /**
     * Accounts array
     */
    NSMutableArray *accountsArray_;
    
    /**
     * Selected account
     */
    BankAccount *selectedAccount_;
    
    /**
     * Selected account index
     */
    NSInteger selectedAccountIndex_;
    
    /**
     * Data info array
     */
    NSMutableArray *dataInfoArray_;
    
    /**
     * Confirmation info array
     */
    NSMutableArray *confirmationInfoArray_;
    
    /**
     * Success info array
     */
    NSMutableArray *successInfoArray_;
    
    /**
     * Coordinate hint
     */
    NSString *coordHint_;
    
    /**
     * Seal
     */
    NSString *seal_;
    
    /**
     * Second factor key: user selected coordinates or OTP key received from sms
     */
    NSString *secondFactorKey_;
    
    /**
     * Delegate
     */
    id<PaymentBaseProcessDelegate> delegate_;
    
    /**
     * Send email flag
     */
    BOOL sendSMS_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationSMS1_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationSMS2_;
    
    /**
     * Show SMS 1 flag
     */
    BOOL showSMS1_;
    
    /**
     * Show SMS 2 flag
     */
    BOOL showSMS2_;
    
    /**
     * Send email flag
     */
    BOOL sendEmail_;
    
    /**
     * is frequent operation flag
     */
    BOOL isFO_;
    
    /*
     *selected carrier one index
     */
    NSInteger selectedCarrier1Index_;
    
    /*
     *selected carrier two index
     */
    NSInteger selectedCarrier2Index_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationEmail1_;
    
    /**
     * User selected destination email address
     */
    NSString *destinationEmail2_;
    
    /**
     * Show email 1 flag
     */
    BOOL showEmail1_;
    
    /**
     * Show email 2 flag
     */
    BOOL showEmail2_;
    
    /**
     * User selected email message
     */
    NSString *emailMessage_;
    
    /**
     * Legal terms accepted flag
     */
    BOOL legalTermsAccepted_;
    
    /**
     * The carrier list
     */
    NSArray *carrierList_;
    
    /**
     * Legal terms URL
     */
    NSString *legalTermsURL_;
    
    /**
     * Confirmation message.
     */
    NSString *message_;

    /**
     * Confirmation message.
     */
    NSString *notificationMessage_;
    
    /**
     * response for frequent operation execution
     */
    FOEServicePaymentStepOneResponse *foeResponse_;
    
}

/**
 * Provides read-write access to the accountsArray
 */
@property (nonatomic, readwrite, retain) NSArray *accountsArray;

/**
 * Provides read-write access to the selectedAccount
 */
@property (nonatomic, readwrite, retain) BankAccount *selectedAccount;

/**
 * Provides read-write access to selectedAccountIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedAccountIndex;

/**
 * Provides read-write access to the dataInfoArray
 */
@property (nonatomic, readwrite, retain) NSArray *dataInfoArray;

/**
 * Provides read-write access to the confirmationInfoArray
 */
@property (nonatomic, readwrite, retain) NSArray *confirmationInfoArray;

/**
 * Provides read-write access to the successInfoArray
 */
@property (nonatomic, readwrite, retain) NSArray *successInfoArray;

/**
 * Provides read-write access to the coordHint
 */
@property (nonatomic, readwrite, copy) NSString *coordHint;

/**
 * Provides read-write access to the seal
 */
@property (nonatomic, readwrite, copy) NSString *seal;

/**
 * Provides read-write access to the second factor key
 */
@property (nonatomic, readwrite, copy) NSString *secondFactorKey;

/**
 * Provides read-write access to the NXT application delegate
 */
@property (nonatomic, readonly, retain) NXT_Peru_iPhone_AppDelegate *appDelegate;

/**
 * Provides read-write access to thedelegate
 */
@property (nonatomic, readwrite, assign) id<PaymentBaseProcessDelegate> delegate;

/**
 * Provides read-write access to the user selected destinationSMS1 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationSMS1;

/**
 * Provides read-write access to the user selected destinationSMS2 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationSMS2;

/**
 * Provides read-write access to the user selected showSMS1 
 */
@property (nonatomic, readwrite, assign) BOOL showSMS1;

/**
 * Provides read-write access to the user selected showSMS2 
 */
@property (nonatomic, readwrite, assign) BOOL showSMS2;

/**
 * Provides read-write access to the send email flag
 */
@property (nonatomic, readwrite, assign) BOOL sendEmail;

/**
 * Provides read-write access to the send sms flag
 */
@property (nonatomic, readwrite, assign) BOOL sendSMS;

/**
 * Provides read-write access to the isFO
 */
@property (nonatomic, readwrite, assign) BOOL isFO;


/**
 * Provides read-write access to the selected carrier one index
 */
@property (nonatomic, readwrite, assign) NSInteger selectedCarrier1Index;

/**
 * Provides read-write access to the selected carrier two index
 */
@property (nonatomic, readwrite,assign) NSInteger selectedCarrier2Index;

/**
 * Provides read-write access to the user selected destinationEmail1 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationEmail1;

/**
 * Provides read-write access to the user selected destinationEmail2 address
 */
@property (nonatomic, readwrite, copy) NSString *destinationEmail2;

/**
 * Provides read-write access to the user selected showEmail1 
 */
@property (nonatomic, readwrite, assign) BOOL showEmail1;

/**
 * Provides read-write access to the user selected showEmail2 
 */
@property (nonatomic, readwrite, assign) BOOL showEmail2;

/**
 * Provides read-write access to the user selected email message
 */
@property (nonatomic, readwrite, copy) NSString *emailMessage;

/**
 * Provides read-write access to the user selected legalTermsAccepted 
 */
@property (nonatomic, readwrite, assign) BOOL legalTermsAccepted;

/**
 * Provides read-only access to the carrierList
 */
@property (nonatomic, readwrite, retain) NSArray *carrierList;

/**
 * Provides read-write access to the legalTermsURL
 */
@property (nonatomic, readwrite, copy) NSString *legalTermsURL;

/**
 * Provides read-write access to messag.
 */
@property (nonatomic, readwrite, copy) NSString *message;

/**
 * Provides read-write access to messag.
 */
@property (nonatomic, readwrite, copy) NSString *notificationMessage;

/**
 * Provides read-write access to the foeResponse
 */
@property (nonatomic, readwrite, retain) FOEServicePaymentStepOneResponse *foeResponse;

/**
 * Checks if there is enough information to start the startPaymentDataRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentDataValidation;

/**
 * Performs the payment data request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentDataRequest;

/**
 * Notifies the payment operation process the data response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The data response received from the server
 */
- (void)dataResponseReceived:(NSNotification *)notification;

/**
 * Checks if there is enough information to start the startPaymentConfirmationRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentConfirmationValidation;

/**
 * Performs the payment confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentConfirmationRequest;

/**
 * Notifies the payment operation process the confirmation response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The confirmation response received from the server
 */
- (void)confirmationResponseReceived:(NSNotification *)notification;

/**
 * Checks if there is enough information to start the startPaymentSuccessRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentSuccessDataValidation;

/**
 * Performs the payment success request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (void)startPaymentSuccessRequest;

/**
 * Notifies the payment operation process the success response received from the server. The payment operation process notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The success response received from the server
 */
- (void)paymentSuccessResponseReceived:(NSNotification *)notification;

/**
 * Returns the payment operation type
 *
 * @return The payment operation type
 */
- (PaymentTypeEnum)paymentOperationType;

/**
 * Returns a success extra info text message
 */
- (NSString *)successExtraInfoText;

/**
 * Performs the transfer request to the server. Returns the frequent operation status. Base class always returns NO, because no operation is
 * performed
 *
 * @return YES when the request can be started, NO otherwise
 */
- (BOOL)startFrequentOperationReactiveRequest;

/**
 * Return the literal for a carrier selected
 *
 * @param carrier The selected carrier
 * @private
 */
- (NSString *)carrierLiteralForCarrier:(NSInteger)carrier;

/**
 * Reset sms and email notifications information
 */
- (void)resetSMSAndEmailInformation;

/**
 * Checks if user must type the key received by sms or a coordinate key
 * 
 * @return YES if must type sms key. NO otherwise
 */
- (BOOL)typeOTPKey;

/**
 * Return the titles and attributes for the third view of fo execution
 *
 * @return the array with titles and attributes. Base clase always returns an empty array
 */
- (NSArray *)foThirdStepInformation;



@end
