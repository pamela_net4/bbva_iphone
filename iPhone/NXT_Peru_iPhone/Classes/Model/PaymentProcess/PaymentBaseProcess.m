/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import "PaymentBaseProcess.h"

#import "BankAccount.h"
#import "Carrier.h"
#import "GlobalAdditionalInformation.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Session.h"
#import "StringKeys.h"
#import "TitleAndAttributes.h"
#import "Tools.h"

#pragma mark -

@implementation PaymentBaseProcess

#pragma mark -
#pragma mark Properties

@synthesize accountsArray = accountsArray_;
@synthesize selectedAccount = selectedAccount_;
@synthesize selectedAccountIndex = selectedAccountIndex_;
@synthesize dataInfoArray = dataInfoArray_;
@synthesize confirmationInfoArray = confirmationInfoArray_;
@synthesize successInfoArray = successInfoArray_;
@synthesize coordHint = coordHint_;
@synthesize secondFactorKey = secondFactorKey_;
@synthesize seal = seal_;
@synthesize delegate = delegate_;
@synthesize destinationSMS1 = destinationSMS1_;
@synthesize destinationSMS2 = destinationSMS2_;
@synthesize showSMS1 = showSMS1_;
@synthesize showSMS2 = showSMS2_;
@synthesize sendEmail = sendEmail_;
@synthesize sendSMS = sendSMS_;
@synthesize isFO = isFO_;
@synthesize selectedCarrier1Index = selectedCarrier1Index_;
@synthesize selectedCarrier2Index = selectedCarrier2Index_;
@synthesize destinationEmail1 = destinationEmail1_;
@synthesize destinationEmail2 = destinationEmail2_;
@synthesize showEmail1 = showEmail1_;
@synthesize showEmail2 = showEmail2_;
@synthesize emailMessage = emailMessage_;
@synthesize legalTermsAccepted = legalTermsAccepted_;
@synthesize carrierList = carrierList_;
@synthesize legalTermsURL = legalTermsURL_;
@synthesize message = message_;
@synthesize notificationMessage = notificationMessage_;
@synthesize foeResponse = foeResponse_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [accountsArray_ release];
    accountsArray_ = nil;
    
    [selectedAccount_ release];
    selectedAccount_ = nil;
    
    [dataInfoArray_ release];
    dataInfoArray_ = nil;
    
    [confirmationInfoArray_ release];
    confirmationInfoArray_ = nil;
    
    [successInfoArray_ release];
    successInfoArray_ = nil;
    
    [coordHint_ release];
    coordHint_ = nil;
    
    [secondFactorKey_ release];
    secondFactorKey_ = nil;
    
    [seal_ release];
    seal_ = nil;
    
    delegate_ = nil;
    
    [destinationSMS1_ release];
    destinationSMS1_ = nil;
    
    [destinationSMS2_ release];
    destinationSMS2_ = nil;
    
    [destinationEmail1_ release];
    destinationEmail1_ = nil;
    
    [destinationEmail2_ release];
    destinationEmail2_ = nil;
    
    [emailMessage_ release];
    emailMessage_ = nil;
    
    [carrierList_ release];
    carrierList_ = nil;
    
    [legalTermsURL_ release];
    legalTermsURL_ = nil;
    
    [message_ release];
    message_ = nil;

    if (foeResponse_ != nil) {
        [foeResponse_ release];
        foeResponse_ = nil;
    }
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Utilities

/**
 * Return the literal for a carrier selected
 *
 * @param carrier The selected carrier
 * @private
 */
- (NSString *)carrierLiteralForCarrier:(NSInteger)carrier {
    
    NSString *literal = @"";
    
    NSString *carrierName = ((carrier == -1) || (carrier >= [carrierList_ count])) ? @"" : [carrierList_ objectAtIndex:carrier];
    
    if ([carrierName isEqualToString:NSLocalizedString(MOVISTAR_CARRIER_TEXT_KEY, nil)]) {
        
        literal = kCarrierMovistarCodeForNotification;
        
    } else if ([carrierName isEqualToString:NSLocalizedString(CLARO_CARRIER_TEXT_KEY, nil)]) {
    
        literal = kCarrierClaroCodeForNotification;
    
    } else if ([carrierName isEqualToString:NSLocalizedString(NEXTEL_CARRIER_TEXT, nil)]) {
        
        literal = kCarrierNextelCodeForNotification;
        
    }

    return literal;
    
}

/*
 * Reset sms and email notifications information
 */
- (void)resetSMSAndEmailInformation {
    
    [self setDestinationEmail1:@""];
    [self setDestinationEmail2:@""];
    [self setDestinationSMS1:@""];
    [self setDestinationSMS2:@""];
    [self setShowEmail1:NO];
    [self setShowEmail2:NO];
    [self setShowSMS1:NO];
    [self setShowSMS2:NO];
    [self setSendEmail:NO];
    [self setSendSMS:NO];
    
}


#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initialized. Initializes a PaymentBaseProcess instance
 *
 * @return The initialized PaymentBaseProcess instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		accountsArray_ = [[NSMutableArray alloc] init];
        dataInfoArray_ = [[NSMutableArray alloc] init];
		confirmationInfoArray_ = [[NSMutableArray alloc] init];
		successInfoArray_ = [[NSMutableArray alloc] init];
		carrierList_ = [[NSArray alloc] initWithObjects:NSLocalizedString(MOVISTAR_CARRIER_TEXT_KEY, nil),
						NSLocalizedString(CLARO_CARRIER_TEXT_KEY, nil),
                        NSLocalizedString(NEXTEL_CARRIER_TEXT, nil),
						nil];
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Server operations management

/**
 * Checks if there is enough information to start the startPaymentDataRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentDataValidation {

    return nil;

}

/**
 * Performs the payment data request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentDataRequest {

}

- (BOOL)startFrequentOperationReactiveRequest
{
    return NO;
    
    
    
}

/**
 * Notifies the payment operation process the data response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The data response received from the server
 * @return YES when the data response is correct, NO otherwise
 */
- (void)dataResponseReceived:(NSNotification *)notification {

}

/**
 * Checks if there is enough information to start the startPaymentConfirmationRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentConfirmationValidation {

    NSString *result = @"";
    
    if ([self sendSMS]) {
        
        if([self showSMS1]) {
            
            if ([@"" isEqualToString:[self destinationSMS1]]) {
                
                result = NSLocalizedString(FO_ERROR_FILL_PHONE_TEXT_KEY, nil);
                
            } else if (![Tools isValidMobilePhoneNumberString:[self destinationSMS1]])  {
                
                result = NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_ERROR_KEY, nil);
                
            } else if ([self selectedCarrier1Index] < 0)  {
                
                result = NSLocalizedString(TRANSFER_ERROR_CARRIER_TEXT_KEY, nil);
                
            }
            
        }
        
        if (([result length] == 0) && ([self showSMS2])) {
            
            if ([@"" isEqualToString:[self destinationSMS2]]) {
                
                result = NSLocalizedString(FO_ERROR_FILL_PHONE_TEXT_KEY, nil);
                
            } else if (![Tools isValidMobilePhoneNumberString:[self destinationSMS2]]) {
                
                result = NSLocalizedString(NOTIFICATIONS_TABLE_PHONE_ERROR_KEY, nil);
                
            } else if ([self selectedCarrier2Index] < 0) {
                
                result = NSLocalizedString(TRANSFER_ERROR_CARRIER_TEXT_KEY, nil);
                
            }
            
        }
        
    }

    if (([result length] == 0) && ([self sendEmail])) {
        
        if ([self showEmail1]) {
            
            if ([[self destinationEmail1] length] == 0)  {
                
                result = NSLocalizedString(PAYMENT_ERROR_EMAIL_TEXT_KEY, nil);
                
            } else {
                
                NSArray *atSeparetedStrings = [[self destinationEmail1] componentsSeparatedByString:@"@"];
                NSUInteger atSeparetedStringsCount = [atSeparetedStrings count];
                
                if (atSeparetedStringsCount == 2) {
                    
                    NSString *firstString = [atSeparetedStrings objectAtIndex:0];
                    
                    if ([firstString length] > 0) {
                        
                        NSString *otherString = [atSeparetedStrings objectAtIndex:1];
                        NSRange dotRange = [otherString rangeOfString:@"."];
                        NSUInteger dotLocation = dotRange.location;
                        
                        if( !(((dotLocation > 0) && (dotLocation != NSNotFound)) && (dotLocation < ([otherString length] - 1))) ) {
                            
                            result = NSLocalizedString(FO_ERROR_VALID_EMAIL_TEXT_KEY, nil);
                            
                        }                        
                    
                    } else {
                        
                        result = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                        
                    }
                    
                } else {
                    
                    result = NSLocalizedString(FO_ERROR_VALID_EMAIL_TEXT_KEY, nil);
                    
                }
                
            }
            
        }
        
        if (([result length] == 0) && ([self showEmail2])) {
            
            if ([[self destinationEmail2] length] == 0) {
                
                result = NSLocalizedString(PAYMENT_ERROR_EMAIL_TEXT_KEY, nil);
            
            } else {
                
                NSArray *atSeparetedStrings = [self.destinationEmail2 componentsSeparatedByString:@"@"];
                NSUInteger atSeparetedStringsCount = [atSeparetedStrings count];
                
                if (atSeparetedStringsCount == 2) {
                    
                    NSString *firstString = [atSeparetedStrings objectAtIndex:0];
                    
                    if ([firstString length] > 0) {
                        
                        NSString *otherString = [atSeparetedStrings objectAtIndex:1];
                        NSRange dotRange = [otherString rangeOfString:@"."];
                        NSUInteger dotLocation = dotRange.location;
                        
                        if( !(((dotLocation > 0) && (dotLocation != NSNotFound)) && (dotLocation < ([otherString length] - 1))) ) {
                            
                            result = NSLocalizedString(FO_ERROR_VALID_EMAIL_TEXT_KEY, nil);
                            
                        }    
                        
                    } else {
                       
                        result = NSLocalizedString(NOTIFICATIONS_TABLE_EMAIL_FILL_KEY, nil);
                        
                    }
                
                } else {
                
                    result = NSLocalizedString(FO_ERROR_VALID_EMAIL_TEXT_KEY, nil);
                
                }
            
            }
        
        }
        
        if (([result length] == 0) && ([[self emailMessage] length] == 0)) {
            
            result = NSLocalizedString(TRANSFER_ERROR_EMAIL_MESSAGE_TEXT_KEY, nil);
            
        }
    
    }
    
    return result;
    
}

/**
 * Performs the payment confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 *
 * @return YES when the request can be started, NO otherwise
 */
- (void)startPaymentConfirmationRequest {

}

/**
 * Notifies the payment operation process the confirmation response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The confirmation response received from the server
 */
- (void)confirmationResponseReceived:(NSNotification *)notification {

}

/**
 * Checks if there is enough information to start the startPaymentSuccessRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentSuccessDataValidation {

    NSString *result = @"";
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
    if (([self paymentOperationType] == PTEPaymentOtherBank) && !legalTermsAccepted_) {
        
        result = NSLocalizedString(MUST_ACCEPT_LEGAL_TERMS_TEXT_KEY, nil);
        
    } else if(![self isFO]){
        
        if([self paymentOperationType] == PTEPaymentISService && otpUsage == otp_UsageOTP && [secondFactorKey_ isEqualToString:@""] ){
            
            result = NSLocalizedString(MUST_INTRODUCE_IS_OTP_SMSKEY_TEXT_KEY, nil);
            
        } else if ((otpUsage == otp_UsageOTP) && ([secondFactorKey_ isEqualToString:@""])) {
            
            result = NSLocalizedString(MUST_INTRODUCE_OTP_KEY_TEXT_KEY, nil);
            
        } else if (([self paymentOperationType] == PTEPaymentISService) && (otpUsage == otp_UsageTC) && ([secondFactorKey_ isEqualToString:@""] || ([secondFactorKey_ length] < 3))) {
            
            result = NSLocalizedString(MUST_INTRODUCE_IS_OTP_COORDINATES_TEXT_KEY, nil);
            
        } else if ((otpUsage == otp_UsageTC) && ([secondFactorKey_ isEqualToString:@""] || ([secondFactorKey_ length] < 3))) {
            
            result = NSLocalizedString(PAYMENT_ERROR_COORDINATES_TEXT_KEY, nil);
            
        }
    }
    return result;

}

/**
 * Performs the payment success request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (void)startPaymentSuccessRequest {

}

/**
 * Notifies the payment operation process the success response received from the server. The payment operation process notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The success response received from the server
 */
- (void)paymentSuccessResponseReceived:(NSNotification *)notification {


}

/**
 * Returns the payment operation type
 *
 * @return The payment operation type. Default: PTEPaymentPSElectricServices
 */
- (PaymentTypeEnum)paymentOperationType {

    return PTEPaymentPSElectricServices;

}

/**
 * Returns a success extra info text message
 */
- (NSString *)successExtraInfoText {

    return @"";

}

#pragma mark -
#pragma mark Properties methods

/*
 * Returns the NXT application delegate
 */
- (NXT_Peru_iPhone_AppDelegate *)appDelegate {
    
    return (NXT_Peru_iPhone_AppDelegate *)[UIApplication sharedApplication].delegate;
}

/*
 * Sets the account array
 */
- (void)setAccountsArray:(NSArray *)accountsArray {

    if (accountsArray_ != accountsArray) {
    
        [accountsArray_ removeAllObjects];
        
        for (NSObject *object in accountsArray) {
            
            if ([object isKindOfClass:[BankAccount class]]) {
                
                [accountsArray_ addObject:object];
                
            }
            
        }
        
        if ([accountsArray_ count] > 0) {
            
            [self setSelectedAccountIndex:0];
            
        } else {
            
            [self setSelectedAccountIndex:-1];
            
        }
    
    }

}

/*
 * Sets the dataInfo array
 */
- (void)setDataInfoArray:(NSArray *)dataInfoArray {
    
    if (dataInfoArray_ != dataInfoArray) {
        
        [dataInfoArray_ removeAllObjects];
        
        for (NSObject *object in dataInfoArray) {
            
            if ([object isKindOfClass:[TitleAndAttributes class]]) {
                
                [dataInfoArray_ addObject:object];
                
            }
            
        }
        
    }
    
}

/*
 * Sets the confirmationInfoArray array
 */
- (void)setConfirmationInfoArray:(NSArray *)confirmationInfoArray {
    
    if (confirmationInfoArray_ != confirmationInfoArray) {
        
        [confirmationInfoArray_ removeAllObjects];
        
        for (NSObject *object in confirmationInfoArray) {
            
            if ([object isKindOfClass:[TitleAndAttributes class]]) {
                
                [confirmationInfoArray_ addObject:object];
                
            }
            
        }
        
    }
    
}

/*
 * Sets the successInfoArray array
 */
- (void)setSuccessInfoArray:(NSArray *)successInfoArray {
    
    if (successInfoArray_ != successInfoArray) {
        
        [successInfoArray_ removeAllObjects];
        
        for (NSObject *object in successInfoArray) {
            
            if ([object isKindOfClass:[TitleAndAttributes class]]) {
                
                [successInfoArray_ addObject:object];
                
            }
            
        }
        
    }
    
}

/*
 * Checks if user must type the key received by sms or a coordinate key
 */
- (BOOL)typeOTPKey {
    
    BOOL result = NO;
    
    OTP_Usage otpUsage = [[[Session getInstance] additionalInformation] otpUsage];
    
    if ((otpUsage == otp_UsageOTP) || (otpUsage == otp_UsageUnknown)) {
        
        result = YES;
        
    } else if (otpUsage == otp_UsageTC) {
        
        result = NO;
        
    }
    
    return result;
    
}

/**
 * Return the titles and attributes for the third view of fo execution
 *
 * @return the array with titles and attributes. Base clase always returns an empty array
 */
- (NSArray *)foThirdStepInformation{
    
    return [NSArray array];
}

@end
