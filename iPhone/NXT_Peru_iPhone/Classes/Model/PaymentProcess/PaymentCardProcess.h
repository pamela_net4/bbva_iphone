/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import <Foundation/Foundation.h>
#import "PaymentPSProcess.h"

@class StatusEnabledResponse;
@class Card;
/**
 * Base class to represent a Payment Continental Own Card operation to help view controllers to display the 
 * information and perform the operations.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentCardProcess : PaymentBaseProcess {
@private

    /**
     * Amount
     */
    NSString *amount_;
    
    /**
     * Currency array
     */
    NSMutableArray *currencyArray_;
    
    /**
     * Currency text array
     */
    NSMutableArray *currencyTextArray_;
    
    /**
     * Selected currency
     */
    NSString *selectedCurrency_;
    
    /**
     * Card Number
     */
    NSString *cardNumber_;
    
    /**
     * Locality array
     */
    NSMutableArray *localityArray_;
    
    /**
     * Selected locality
     */
    NSString *selectedLocality_;
    
    /**
     * Beneficiary
     */
    NSString *beneficiary_;
    
    /**
     * Own card selected
     */
    Card *ownCardSelected_;
    
    /**
     * Selected locality index
     */
    NSInteger selectedLocalityIndex_;
    
    /**
     * Selected currency index
     */
    NSInteger selectedCurrencyIndex_;
    
}

/**
 * Provides read-only access to the amount
 */
@property (nonatomic, readwrite, copy) NSString *amount;

/**
 * Provides read-only access to the currencyArray
 */
@property (nonatomic, readwrite, retain) NSArray *currencyArray;

/**
 * Provides read-only access to the currencyTextArray
 */
@property (nonatomic, readwrite, retain) NSArray *currencyTextArray;

/**
 * Provides read-only access to the amount
 */
@property (nonatomic, readwrite, copy) NSString *selectedCurrency;

/**
 * Provides read-only access to the cardNumber
 */
@property (nonatomic, readwrite, copy) NSString *cardNumber;

/**
 * Provides read-only access to the localityArray
 */
@property (nonatomic, readwrite, retain) NSArray *localityArray;

/**
 * Provides read-only access to the beneficiary
 */
@property (nonatomic, readwrite, copy) NSString *beneficiary;

/**
 * Provides read-only access to the ownCardSelected
 */
@property (nonatomic, readwrite, retain) Card *ownCardSelected;

/**
 * Provides read-only access to the selectedLocality
 */
@property (nonatomic, readwrite, copy) NSString *selectedLocality;

/**
 * Provides read-only access to the selectedLocalityIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedLocalityIndex;

/**
 * Provides read-only access to the selectedLocalityIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedCurrencyIndex;

@end
