/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import "PaymentPSElectServProcess.h"

#import "AccountList.h"
#import "BankAccount.h"
#import "Card.h"
#import "CardList.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentDataResponse.h"
#import "PaymentConfirmationResponse.h"
#import "PaymentSuccessResponse.h"
#import "Payment.h"
#import "PaymentList.h"
#import "PaymentInstitutionsAndCompaniesDetailResponse.h"
#import "PublicServiceResponse.h"
#import "Session.h"
#import "StringKeys.h"
#import "StringList.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "Updater.h"
#import "InstitutionList.h"
#import "Institution.h"
#import "PaymentInstitutionsAndCompaniesInitialResponse.h"

@implementation PaymentPSElectServProcess

#pragma mark -
#pragma mark Properties


#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Information distribution

- (BOOL)startFrequentOperationReactiveRequest
{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *numSum = [userDefault objectForKey:@"suministro"];
    NSString *codEntity = [userDefault objectForKey:@"codEntity"];
    NSString *codInstitution = [userDefault objectForKey:@"codInstitution"];
    NSString *codClass = [userDefault objectForKey:@"codClass"];
    NSString *descInst = [userDefault objectForKey:@"descInstitution"];
    
    
    NSString *operacion = [NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil),
                           NSLocalizedString(PUBLIC_SERVICE_ELECTRICITY_TEXT_LOWER_KEY, nil)];
    [[Updater getInstance] obtainPaymentToInstitutionAndCompaniesFrequentOperationReactiveStepOneWithOperation:operacion andCodMatri:numSum andsClase:codClass andsEntidad:codEntity andsCodEntidad:codInstitution andArreglo:numSum andsDescripcion:descInst andCuentaCargo:[self amountToPay] andHolderName:[self clientName]];
    
    return YES;
}

/**
 * Checks if there is enough information to start the startPaymentDataRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentInitialValidation {

    NSString *result = [super paymentInitialValidation];
    
    return result;
    
}

/**
 * Performs the payment initial request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentInitialRequest {
    
    NSString *errorMessage = [self paymentInitialValidation];
    
    if ((errorMessage == nil) || [errorMessage isEqualToString:@""]) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];

        if ([self selectedCompanyParameter]!=nil && ![@"" isEqualToString:[self selectedCompanyParameter]]) {
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(initialResponseReceived:)
                                                         name:kNotificationPaymentInstitutionsDetailResult
                                                       object:nil];
            
            [[Updater getInstance] obtainPaymentInstitutionsDetailForCod:[self selectedCompanyParameter] optionType:@"SERV"];
            
        }else{
            
            
             [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SettingFrequentOperation:) name:kNotificationPaymentInstitutionsSearchListResult object:nil];
            
            [[Updater getInstance] obtainInstitutionsAndCompaniesForEntity:[self selectedCompany] searchType:@"C" nextMovement:@"" indPag:@"" searchArg:[self selectedCompany] LastDescr:@"" button:@"siguiente" action:@"consultar"];
            
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(initialResponseReceived:)
                                                         name:kNotificationPaymentServicesListResultEnds
                                                       object:nil];
            [[Updater getInstance] obtainPaymentPSElectricServicesInitializationForCompany:[self selectedCompanyCode]];
        }
        
    } else {
        
        [Tools showInfoWithMessage:errorMessage];
        
    }
    
}


- (void)SettingFrequentOperation:(NSNotification *)notification
{
    
    
PaymentInstitutionsAndCompaniesInitialResponse *response = [notification object];
    

    
    
if (response != nil && [response isKindOfClass:[PaymentInstitutionsAndCompaniesInitialResponse class]] && !response.isError) {
        
InstitutionList *institutionList = [response institutionList];
        
NSArray  *institutionsArray_ = [[institutionList institutionList] retain];
        
        Institution *institution = [institutionsArray_ objectAtIndex:0];

    
    
    
    [[NSUserDefaults standardUserDefaults] setObject:institution.institutionEntity forKey:@"codEntity"];
    [[NSUserDefaults standardUserDefaults] setObject:institution.institutionCod forKey:@"codInstitution"];
    [[NSUserDefaults standardUserDefaults] setObject:institution.institutionClass forKey:@"codClass"];
    [[NSUserDefaults standardUserDefaults] setObject:institution.institutionDescription forKey:@"descInstitution"];
    
    
}
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationPaymentInstitutionsSearchListResult object:nil];
   
}


/**
 * Notifies the payment operation process the initial response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The initial response received from the server
 */
- (void)initialResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationPaymentServicesListResultEnds
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationPaymentInstitutionsDetailResult
                                                  object:nil];
    
    [[self appDelegate] hideActivityIndicator];
    
    id response;
    //![self.selectedCompanyCode isEqualToString:COMPANY_LUZ_DEL_SUR] &&  ![self.selectedCompanyCode isEqualToString:COMPANY_EDELNOR]
    if (![@"" isEqualToString:[self selectedCompanyParameter]]) {
        response = (PaymentInstitutionsAndCompaniesDetailResponse *)[notification object];
    }else{
        response = (PublicServiceResponse *)[notification object];
    }
    
    if (response != nil && ![response isError]) {
        //![self.selectedCompanyCode isEqualToString:COMPANY_LUZ_DEL_SUR] &&  ![self.selectedCompanyCode isEqualToString:COMPANY_EDELNOR]
        
        if (![@"" isEqualToString:[self selectedCompanyParameter]]) {
            [self setResponseInformation:response];
            [self.psDelegate initilizationHasFinished:TRUE];
        }else{
            [self setCompanyConfiguration:response];
            [self.psDelegate initilizationHasFinished:FALSE];
        }

    }
        
}



/**
 * Checks if there is enough information to start the startPaymentDataRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentDataValidation {
    
    NSString *result = nil;
    
    if ([[self supplies] length] < [[self companyConfiguration] minLong]) {
    
        result = NSLocalizedString(PAYMENT_ERROR_SUPPLY_NUMBER_TEXT_KEY, nil);
        
    }
        
    return result;
    
}

/**
 * Performs the payment data request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentDataRequest {
    
    NSString *errorMessage = [self paymentDataValidation];
    
    if ((errorMessage == nil) || ([errorMessage isEqualToString:@""])) {
     
		[[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(dataResponseReceived:) 
                                                     name:kNotificationPaymentDataResultEnds object:nil];
        
        [[Updater getInstance] obtainPaymentPSElectricServicesDataForCompany:[self selectedCompanyCode]
                                                                    supplies:[self supplies]];
        
    } else {
    
        [Tools showInfoWithMessage:errorMessage];
    
    }   
    
}

/**
 * Notifies the payment operation process the data response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The data response received from the server
 * @return YES when the data response is correct, NO otherwise
 */
- (void)dataResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentDataResultEnds 
                                                  object:nil];
    [[self appDelegate] hideActivityIndicator];

    PaymentDataResponse *response = (PaymentDataResponse *)[notification object];
	
	if (![response isError]) {

		// Accounts
		AccountList *bankAccounts = [[Session getInstance] accountList];
		NSMutableArray *accounts = [NSMutableArray array];
		NSArray *array = [NSArray arrayWithArray:[[response numberAccountList] stringList]];
		
		for (NSString *numberAccount in array) {
			
			BankAccount *result = [bankAccounts accountFromAccountTerminateNumber:numberAccount];
			
			if (result != nil) {
				
				[accounts addObject:result];
				
			}
            
		}
        
        [self setAccountsArray:accounts];
        [self setCurrencyPS:[response currency]];

        // Cards

		NSMutableArray *cards = [NSMutableArray array];
		
		if ([response cardsFlag]) {
			
			CardList *cardLists = [[Session getInstance] cardList];
			NSArray *array = [NSArray arrayWithArray:[[response numberCardList] stringList]];
			
			for (NSString *numberCard in array) {
				
				Card *result = [cardLists cardFromCardTerminateNumber:numberCard];
				
				if (result != nil) {
					
					[cards addObject:result];
					
				}
				
			}
			
		}
        
        [self setCardsArray:cards];
        
        // Payments
        
        PaymentList *payments = [response paymentList];
        
        [self setPaymentsArray:[payments paymentList]];
        
        // Other data to save
        
        self.clientName = [Tools notNilString:[response holder]];
        self.hasCards = [response cardsFlag];
        
        // Data information
        
        NSMutableArray *informationArray = [[[NSMutableArray alloc] init] autorelease];
        
        // - Company
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];

        [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_BUSINESS_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[self selectedCompany]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Supply
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PUBLIC_SERVICE_STEP_ONE_NUMBER_OF_SUPPLY_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response supply]]];
        [[NSUserDefaults standardUserDefaults] setObject:[response supply] forKey:@"suministro"];

        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Service owner
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(SERVICE_TITULAR_TEXT_KEY, nil)];
                
        [titleAndAttributes addAttribute:[Tools notNilString:[self clientName]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
    
        [self setDataInfoArray:informationArray];
        
        [[self delegate] dataAnalysisHasFinished];

	}
        
}

/**
 * Checks if there is enough information to start the startPaymentConfirmationRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentConfirmationValidation {
    
    NSString *result;
    
    if ([[self selectedPaymentsArray] count] == 0) {
        
        result = NSLocalizedString(PAYMENT_ERROR_RECEIPTS_MIN_TEXT_KEY, nil);
        
    } else if ([self hasCards] && ([self selectedMethodIndex] == -1)) {
        
        result = NSLocalizedString(PAYMENT_ERROR_PAYMENT_METHOD_TEXT_KEY, nil);
        
    } else if (([self hasCards] && ([self selectedMethodIndex] == 0)) && ([self selectedAccountIndex] < 0)) {
            
        result = NSLocalizedString(PAYMENT_ERROR_PAYMENT_METHOD_TEXT_KEY, nil);
            
    } else if (![self hasCards] && ([self selectedAccountIndex] < 0)) {
        
        result = NSLocalizedString(PAYMENT_ERROR_PAYMENT_METHOD_TEXT_KEY, nil);
        
    } else if ([self hasCards] && ([self selectedMethodIndex] == 1) && ([self selectedCardIndex] < 0)) {
            
        result = NSLocalizedString(PAYMENT_ERROR_PAYMENT_METHOD_TEXT_KEY, nil);
        
    } else if ([self selectedCompany] == nil) {
        
        result = NSLocalizedString(CARD_PAYMENT_SELECT_ACCOUNT_TEXT_KEY, nil);
        
    } else {
        
        result = [super paymentConfirmationValidation];
        
    }
    
    return result;
    
}

/**
 * Performs the payment confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentConfirmationRequest {
    
    NSString *errorMessage = [self paymentConfirmationValidation];
    
    if ((errorMessage == nil) || ([errorMessage isEqualToString:@""])) {
        
        if ([self hasCards]) {
            
            if ([self selectedMethodIndex] == 0) {
                
                [self setSelectedAccount:[[self accountsArray] objectAtIndex:[self selectedAccountIndex]]];
                
            } else if ([self selectedMethodIndex] == 1) {
                
                [self setSelectedCard:[[self cardsArray] objectAtIndex:[self selectedCardIndex]]];
                
            }
            
        } else {
            
            [self setSelectedAccount:[[self accountsArray] objectAtIndex:[self selectedAccountIndex]]];
            
        }
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(confirmationResponseReceived:) 
                                                     name:kNotificationPaymentConfirmationResultEnds object:nil];
        
        NSString *issue;
        
        if (self.hasCards && (self.selectedCard != nil)) {
			
			NSString *card = [self.selectedCard cardNumber];
			
			issue = [card substringFromIndex:[card length] - 4];
			
		} else {
			
			NSString *account = [[self selectedAccount] number];
			
			issue = [account substringFromIndex:[account length] - 8];
			
		}
        
        NSMutableString *payments = [NSMutableString string];
        NSString *paymentId = nil;
        
        for (Payment *payment in [self selectedPaymentsArray]) {
            
            paymentId = [payment idPayment];
            
            if ([paymentId length] > 0) {
                
                if ([payments length] > 0) {
                    
                    [payments appendString:@"$"];
                    
                }
                
                [payments appendString:paymentId];
                
            }
        
        }
    
        [[Updater getInstance] obtainPaymentPSElectricServicesConfirmationForCompany:[self selectedCompanyCode]
                                                                               issue:issue
                                                                           idPayment:payments
                                                                              email1:[Tools notNilString:[self destinationEmail1]] 
                                                                              email2:[Tools notNilString:[self destinationEmail2]] 
                                                                        phoneNumber1:[Tools notNilString:[self destinationSMS1]] 
                                                                        phoneNumber2:[Tools notNilString:[self destinationSMS2]] 
                                                                            carrier1:[Tools notNilString:[self carrierLiteralForCarrier:[self selectedCarrier1Index]]]
                                                                            carrier2:[Tools notNilString:[self carrierLiteralForCarrier:[self selectedCarrier2Index]]]
                                                                             message:[self emailMessage]];
    
    } else {
    
        [Tools showInfoWithMessage:errorMessage];
    
    }   
    
}

/**
 * Notifies the payment operation process the confirmation response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The confirmation response received from the server
 */
- (void)confirmationResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentConfirmationResultEnds 
                                                  object:nil];
    [[self appDelegate] hideActivityIndicator];
    
    PaymentConfirmationResponse *response = (PaymentConfirmationResponse *)[notification object];
	
	if (![response isError]) {
    
        self.amountToPay = [NSString stringWithFormat:@"%@ %@", 
                            [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response amountToChargeCurrency]]], 
                            [Tools notNilString:[response amountToCharge]]];
        
        
        self.coordHint = [response coordinate];
        self.seal = [response seal];
        
        // Confirmation information
        
        NSMutableArray *informationArray = [[[NSMutableArray alloc] init] autorelease];
        
        // - Amount to pay
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_AMOUNT_TO_PAY_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[self amountToPay]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Operation
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_OPERATION_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PUBLIC_SERVICE_ELECTRICITY_TEXT_LOWER_KEY, nil)]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Account
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        NSString *account = @"";
        
        if (self.hasCards && self.selectedCard != nil) {
            
            [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_RECHARGE_CARD_TEXT_KEY, nil)];

            account = [NSString stringWithFormat:@"%@ | %@ %@", [self.selectedCard cardType], [Tools getCurrencyLiteral:[self.selectedCard currency]], [self.selectedCard cardNumber]];
            
        } else if ([self selectedAccount]) {
            
            [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_ORIGIN_ACCOUNT_TEXT_KEY, nil)];

            account = [NSString stringWithFormat:@"%@ | %@ %@", [Tools notNilString:[[self selectedAccount] accountType]],
                       [Tools getCurrencyLiteral:[[self selectedAccount] currency]],
                       [Tools notNilString:[[self selectedAccount] number]]];
        
        }
        
        [titleAndAttributes addAttribute:[Tools notNilString:account]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Company
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_BUSINESS_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[self selectedCompany]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Supply number
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PUBLIC_SERVICE_STEP_ONE_NUMBER_OF_SUPPLY_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response supply]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Service owner
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(SERVICE_TITULAR_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[self clientName]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Payments count
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(NUMBER_OF_RECEIPTS_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%d", [[self selectedPaymentsArray] count]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        for (Payment *payment in [self selectedPaymentsArray]) {
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
            [titleAndAttributes setTitleString:[NSString stringWithFormat:NSLocalizedString(RECEIPT_VALUE_TEXT,nil), [Tools notNilString:[payment ticket]]]];
            
            [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", 
                                              [Tools clientCurrencySymbolForServerCurrency:CURRENCY_SOLES_LITERAL], 
                                              [Tools notNilString:[payment amount]]]];
            
            if (titleAndAttributes != nil) {
                [informationArray addObject:titleAndAttributes];
            }
        }
        
        [self setConfirmationInfoArray:informationArray];
        
        [[self delegate] confirmationAnalysisHasFinished];
        
    }
    
}

/**
 * Checks if there is enough information to start the startPaymentSuccessRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentSuccessDataValidation {
    
    return [super paymentSuccessDataValidation];
    
}

/**
 * Performs the payment success request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (void)startPaymentSuccessRequest {
    
    NSString *errorMessage = [self paymentSuccessDataValidation];
    
    if ((errorMessage == nil) || ([errorMessage isEqualToString:@""])) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(paymentSuccessResponseReceived:) 
                                                     name:kNotificationPaymentSuccessResultEnds object:nil];
        
        [[Updater getInstance] obtainPaymentPSElectricServicesSuccessForCompany:[self selectedCompanyCode]
                                                                secondFactorKey:[self secondFactorKey]];
        
    } else {
    
        [Tools showInfoWithMessage:errorMessage];
    
    }

    
}

/**
 * Notifies the payment operation process the success response received from the server. The payment operation process notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The success response received from the server
 */
- (void)paymentSuccessResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentSuccessResultEnds 
                                                  object:nil];
    [[self appDelegate] hideActivityIndicator];
    
    PaymentSuccessResponse *response = (PaymentSuccessResponse *)[notification object];
	
	if (![response isError]) {
        
        [self setMessage:[Tools notNilString:[response message]]];
        
        // Success information
        
        NSMutableArray *informationArray = [[[NSMutableArray alloc] init] autorelease];
        
        // - Operation number
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OPERATION_NUMBER_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response operationNumber]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Date/Time
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(DATE_TRANSFER_TITLE_TEXT_KEY, nil)];

        NSString *dateString = @"";
        
        if (([[response transactionDate] length] > 0) && ([[response transactionHour] length] > 0)) {
            
            dateString = [NSString stringWithFormat:@"%@ | %@", [response transactionDate], [response transactionHour]];
            
        } else {
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat: @"dd/MM/yyyy | HH:mm"];
            dateString = [dateFormat stringFromDate:[NSDate date]];
            [dateFormat release];
            
        }
        
        [titleAndAttributes addAttribute:[Tools notNilString:dateString]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Operation
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_OPERATION_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil), NSLocalizedString(PUBLIC_SERVICE_ELECTRICITY_TEXT_LOWER_KEY, nil)]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
    
        // - Account
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
                
        NSString *account = nil;
        
        if (self.hasCards && self.selectedCard != nil) {
            
            [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_RECHARGE_CARD_TEXT_KEY, nil)];
            
            account = [NSString stringWithFormat:@"%@ | %@ %@", [self.selectedCard cardType], [Tools getCurrencyLiteral:[self.selectedCard currency]], [self.selectedCard cardNumber]];
            
        } else if ([self selectedAccount]) {
            
            [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_ORIGIN_ACCOUNT_TEXT_KEY, nil)];
            
            account = [NSString stringWithFormat:@"%@ | %@ %@",
                       [Tools notNilString:[[self selectedAccount] accountType]],
                       [Tools getCurrencyLiteral:[[self selectedAccount] currency]],
                       [Tools notNilString:[[self selectedAccount] number]]];
            
        }
        
        [titleAndAttributes addAttribute:[Tools notNilString:account]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Company
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_BUSINESS_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[self selectedCompany]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Supply number
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PUBLIC_SERVICE_STEP_ONE_NUMBER_OF_SUPPLY_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response supply]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Service owner
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(SERVICE_TITULAR_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[self clientName]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Payments count
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(NUMBER_OF_RECEIPTS_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%d", [[self selectedPaymentsArray] count]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        for (Payment *payment in [self selectedPaymentsArray]) {
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
            [titleAndAttributes setTitleString:[NSString stringWithFormat:NSLocalizedString(RECEIPT_VALUE_TEXT,nil), [Tools notNilString:[payment ticket]]]];
            
            [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", 
                                              [Tools clientCurrencySymbolForServerCurrency:CURRENCY_SOLES_LITERAL], 
                                              [Tools notNilString:[payment amount]]]];
            
            if (titleAndAttributes != nil) {
                [informationArray addObject:titleAndAttributes];
            }
        }
        
        if (([response changeType] != nil) && ([[response changeType] floatValue] != 0.0f))  {
            
            // - Charged amount
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
            [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_CHARGED_AMOUNT_TEXT_KEY, nil)];
            
            NSString *amountCharged = [NSString stringWithFormat:@"%@ %@",
                                       [Tools notNilString:[Tools clientCurrencySymbolForServerCurrency:[response amountToPayCurrency]]],
                                       [Tools notNilString:[response amountToPay]]];
            
            [titleAndAttributes addAttribute:[Tools notNilString:amountCharged]];
            
            if (titleAndAttributes != nil) {
                [informationArray addObject:titleAndAttributes];
            }
            
            // - Change type
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
            [titleAndAttributes setTitleString:NSLocalizedString(EXCHANGE_RATE_TEXT_KEY, nil)];
            
            NSString *change = [NSString stringWithFormat:@"%@ %@",
                                [Tools clientCurrencySymbolForServerCurrency:[response changeTypeCurrency]],
                                [Tools notNilString:[response changeType]]];
            
            [titleAndAttributes addAttribute:[Tools notNilString:change]];
            
            if (titleAndAttributes != nil) {
                
                [informationArray addObject:titleAndAttributes];
                
            }
            
        }
        
        // - Payed amount
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_PAID_AMOUNT_TEXT_KEY, nil)];
        
        NSString *amount = [NSString stringWithFormat:@"%@ %@", 
                            [Tools notNilString:[Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response amountTurnedCurrency]]]],
                            [Tools notNilString:[response amountTurned]]];
        
        [titleAndAttributes addAttribute:[Tools notNilString:amount]];
        
        if (titleAndAttributes != nil) {
            
            [informationArray addObject:titleAndAttributes];
            
        }
        
        [self setSuccessInfoArray:informationArray];
        
        [[self delegate] successAnalysisHasFinished];
        
    } else {
        
        [[self delegate] successAnalysisHasFinishedWithError];
        
    }
    
}

/**
 * Returns the payment operation type
 *
 * @return The payment operation type
 */
- (PaymentTypeEnum)paymentOperationType {
    
    return PTEPaymentPSElectricServices;
    
}

@end
