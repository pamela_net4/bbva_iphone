//
//  PaymentISSafetyPayProcess.m
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 3/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//


#define ResponseTransactionInfo @""\
@"<MSG-S>"\
@"<INFORMACIONADICIONAL>"\
@"<IDENTIFICADOR>30690</IDENTIFICADOR>"\
@"<ESTABLECIMIENTO>M_TEST</ESTABLECIMIENTO>"\
@"<MONEDA>Soles</MONEDA>"\
@"<IMPORTE>185.36</IMPORTE>"\
@"<CUENTAS>"\
@"<E>"\
@"<MONEDA>SOLES</MONEDA>"\
@"<TIPOCTA>CUENTA VIP</TIPOCTA>"\
@"<ASUNTO>0011-0241-01-00976375</ASUNTO>"\
@"<SALDODISPONIBLE>1,925,702.44</SALDODISPONIBLE>"\
@"<DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
@"<PARAMCUENTA>CUENTA VIP$001102410100976375$SOLES</PARAMCUENTA>"\
@"</E>"\
@"<E>"\
@"<MONEDA>SOLES</MONEDA>"\
@"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
@"<ASUNTO>0011-0241-01-03003955</ASUNTO>"\
@"<SALDODISPONIBLE>5,876,524.15</SALDODISPONIBLE>"\
@"<DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
@"<PARAMCUENTA>CUENTA CORRIENTE$001102410103003955$SOLES</PARAMCUENTA>"\
@"</E>"\
@"<E>"\
@"<MONEDA>SOLES</MONEDA>"\
@"<TIPOCTA>CUENTA GANADORA</TIPOCTA>"\
@"<ASUNTO>0011-0486-02-00510821</ASUNTO>"\
@"<SALDODISPONIBLE>100,737,333.57</SALDODISPONIBLE>"\
@"<DIVSALDODISPONIBLE>S/.</DIVSALDODISPONIBLE>"\
@"<PARAMCUENTA>CUENTA GANADORA$001104860200510821$SOLES</PARAMCUENTA>"\
@"</E>"\
@"</CUENTAS>"\
@"<EMPRESA>Safetypay</EMPRESA>"\
@"<SERVICIO>Pago por compras por Internet</SERVICIO>"\
@"</INFORMACIONADICIONAL>"\
@"</MSG-S>"

#define ResponseDetailsInfo @""\
@"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
@"<MSG-S>"\
@"<INFORMACIONADICIONAL>"\
@"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
@"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
@"<MONEDA>SOLES</MONEDA>"\
@"<IMPORTE>456.90</IMPORTE>"\
@"<DIVIMPORTE>S/.</DIVIMPORTE>"\
@"<CUENTA>"\
@"<MONEDA>SOLES</MONEDA>"\
@"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
@"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
@"</CUENTA>"\
@"<EMPRESA>SafetyPay</EMPRESA>"\
@"<SERVICIO>Pago por compras por Internet</SERVICIO>"\
@"<COORDENADA>A-1</COORDENADA>"\
@"<PARAMCUENTA>CUENTA GANADORA$001104860200510821$SOLES</PARAMCUENTA>"\
@"<SELLOSEG>1010011010|-|--||-|-xoxooxxoxonynnyynyn</SELLOSEG>"\
@"</INFORMACIONADICIONAL>"\
@"</MSG-S>"

#define ResponseConfirmation @""\
@"<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"\
@"<MSG-S>"\
@"<INFORMACIONADICIONAL>"\
@"<IDENTIFICADOR>4263</IDENTIFICADOR>"\
@"<ESTABLECIMIENTO>Rosatel</ESTABLECIMIENTO>"\
@"<MONEDA>SOLES</MONEDA>"\
@"<IMPORTECARGADO>456.90</IMPORTECARGADO>"\
@"<DIVIMPORTE>S/.</DIVIMPORTE>"\
@"<CUENTA>"\
@"<MONEDA>SOLES</MONEDA>"\
@"<TIPOCTA>CUENTA CORRIENTE</TIPOCTA>"\
@"<ASUNTO>0011-0130-0119414501</ASUNTO>"\
@"</CUENTA>"\
@"<EMPRESA>SafetyPay</EMPRESA>"\
@"<SERVICIO>Pago por compras por Internet</SERVICIO>"\
@"<FECHAHORA>30/12/2012   15:30</FECHAHORA>"\
@"<NUMOPERACION>123451234566</NUMOPERACION>"\
@"</INFORMACIONADICIONAL>"\
@"</MSG-S>"


#import "PaymentISSafetyPayProcess.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Tools.h"
#import "Updater.h"
#import "StringKeys.h"
#import "SafetyPayTransactionInfoResponse.h"
#import "Session.h"
#import "BankAccount.h"
#import "TitleAndAttributes.h"
#import "SafetyPayDetailsResponse.h"
#import "SafetyPayConfirmationResponse.h"


@implementation PaymentISSafetyPayProcess

//@synthesize transactionNumber = transactionNumber_;
//@synthesize amountToPay = amountToPay_;

- (void) dealloc {
    
    /*[transactionNumber_ release];
    transactionNumber_ = nil;
    
    [amountToPay_ release];
    amountToPay_ = nil;*/
    
    [super dealloc];
    
}

- (NSString *) paymentInitialValidation {
    
    NSString *result = [super paymentInitialValidation];
    
    return result;
}

- (void) startPaymentInitialRequest {

    NSString *errorMessage = [self paymentInitialValidation];
    
    if((errorMessage == nil ) || [errorMessage isEqualToString: @""]){
    
        [[self appDelegate] showActivityIndicator:poai_Both];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initialISResponseReceived:) name:kNotificationSafetyPayStatusResponseReceived object:nil];
            
        [[Updater getInstance] obtainSafetyPayStatusInfo];
        
    } else {
        
        [Tools showInfoWithMessage: errorMessage];
        
    }
    
}


-(void) initialISResponseReceived:(NSNotification *)notification {

    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationSafetyPayStatusResponseReceived object:nil];
    
    [[self appDelegate] hideActivityIndicator];
    
    id response;
    
    if (![@"" isEqualToString:[self selectedCompanyParameter]]) {
        
        response = (SafetyPayStatusResponse *) [notification object];
        
    }
    
    if (response != nil && ![response isError]){
    
        if (![@"" isEqualToString: [self selectedCompanyParameter] ]) {
            
            [self setInternetShoppingPaymentStatus:response];
            [self.internetShoppingDelegate initilizationHasFinished];
            
        }
        
    }
    
}

- (NSString *) paymentDataValidation {

    NSString *result = nil;
    
    if([[self transactionNumber] length] == 0){
    
        result = NSLocalizedString(INTERNET_SHOPPING_ERROR_TRANS_NUMBER_TEXT_KEY, nil);
    
    } else if ([[self amountToPay] length] == 0) {
    
        result = NSLocalizedString(INTERNET_SHOPPING_ERROR_AMOUNT_TEXT_KEY, nil);
    
    }
    
    return result;
}

- (void) startPaymentDataRequest {

    NSString *errorMessage = [self paymentDataValidation];
    
    if ((errorMessage == nil) || [errorMessage isEqualToString:@""]) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataISResponseReceived:) name:kNotificationSafetyPayTransactionInfoResponseReceived object:nil];
        
        NSString *amount = [Tools formatAmountWithDotDecimalSeparator:[self amountToPay]];
        
        [[Updater getInstance] obtainSafetyPayTransactionInfoByTransactionNumber:[self transactionNumber] AndAmount: amount];
        
    } else {
    
        [Tools showInfoWithMessage: errorMessage];
        
    }
    
}

- (SafetyPayTransactionInfoResponse *) Mock_safetyPayTransactionInfoResponseDataWithXMLString: (NSString *) xmlString {

    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    SafetyPayTransactionInfoResponse *response = [SafetyPayTransactionInfoResponse alloc];
    response.openingTag = @"MSG-S";
    
    [parser setDelegate: response];
    
    [response parserDidStartDocument:parser];
    
    [parser parse];
    
    return response;
    
}

- (void) dataISResponseReceived:(NSNotification *)notification {

    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationSafetyPayTransactionInfoResponseReceived object:nil];
    
    [[self appDelegate] hideActivityIndicator];
    
    SafetyPayTransactionInfoResponse *response = (SafetyPayTransactionInfoResponse *) [notification object];
    
    if(![response isError]) {
    
        [self setInternetShoppingTransactionInformation:response];
        
        // Accounts
		AccountList *bankAccounts = [self.internetShoppingTransactionInformation.additionalInformation accountList];
        NSMutableArray *accounts = [[NSMutableArray alloc] initWithArray:[bankAccounts accountList]];
        
        [self setAccountsArray:accounts];
        
        NSMutableArray *informationArray = [[[NSMutableArray alloc] init]autorelease];
        
        // company
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_COMPANY_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[self selectedCompany]]];
         if(titleAndAttributes != nil)
             [informationArray addObject:titleAndAttributes];
        
        // transaction number
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_TRANS_NUMBER_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation transactionNumber]]];
        if(titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        // business
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_BUSINESS_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation establishment]]];
        if(titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        // currency
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_CURRENCY_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation currency]]];
        if(titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        // amount
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_AMOUNT_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[NSString stringWithFormat: @"%@" ,[response.additionalInformation amount]]]];
        if(titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        [self setDataInfoArray:informationArray];
        
        [[self delegate] dataAnalysisHasFinished];
    }

}

- (NSString *) paymentConfirmationValidation {

    NSString *result;
    
    if ([self selectedAccountIndex] < 0) {
        
        result = NSLocalizedString(INTERNET_SHOPPING_ERROR_ACCOUNT_PAYMENT_TEXT_KEY, nil);
        
    } else {
    
        result = [super paymentConfirmationValidation];
        
    }
    
    return result;

}



- (void) startPaymentConfirmationRequest {

    NSString *errorMessage = [self paymentConfirmationValidation];
    
    if ((errorMessage) == nil || [errorMessage isEqualToString: @""]) {
        
        [self setSelectedAccount: [[self accountsArray] objectAtIndex:[self selectedAccountIndex]]];
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(confirmationISResponseReceived:) name:kNotificationSafetyPayDetailsResponseReceived object:nil];
        
        NSString *account = [[self selectedAccount] paramCuenta];
        
        [[Updater getInstance] obtainSafetyPayPaymentDetailsByAccountNumber:account
                                                           cellphoneNumber1: [Tools notNilString:[self destinationSMS1]]
                                                           cellphoneNumber2: [Tools notNilString:[self destinationSMS2]]
                                                                   carrier1: [Tools notNilString: [self carrierLiteralForCarrier:[self selectedCarrier1Index]]]
                                                                   carrier2:[Tools notNilString:[self carrierLiteralForCarrier:[self selectedCarrier2Index]]]
                                                                     email1:[Tools notNilString:[self destinationEmail1]]
                                                                     email2:[Tools notNilString:[self destinationEmail2]]
                                                                    message:[self emailMessage]];
        
    } else {
    
        [Tools showInfoWithMessage: errorMessage];
    
    }
    
}

- (SafetyPayDetailsResponse *) Mock_safetyPayDetailsResponseDataWithXMLString: (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    SafetyPayDetailsResponse *response = [SafetyPayDetailsResponse alloc];
    response.openingTag = @"MSG-S";
    
    [parser setDelegate: response];
    
    [response parserDidStartDocument:parser];
    
    [parser parse];
    
    return response;
    
}

- (void) confirmationISResponseReceived:(NSNotification *)notification {

    [[NSNotificationCenter defaultCenter] removeObserver: self name: kNotificationSafetyPayDetailsResponseReceived object:nil];
    
    [[self appDelegate] hideActivityIndicator];
    
    SafetyPayDetailsResponse *response = (SafetyPayDetailsResponse *)[notification object];
    
    if(![response isError]){
    
        NSString *currency = [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response.additionalInformation currency]]];
        
        NSString *amountToPay = [Tools notNilString:[NSString stringWithFormat:@"%.2f",[[response.additionalInformation amount] floatValue]]];
        
        self.amountToPay = [NSString stringWithFormat:@"%@ %@", currency, amountToPay];
        
        
        self.coordHint = [response.additionalInformation coordinate];
        
        self.seal = [response.additionalInformation seal];
        
        NSMutableArray *informationArray = [[NSMutableArray alloc] init];
        
        // amount to pay
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_AMOUNT_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[self amountToPay]]];
        if (titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        // operation
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_OPERATION_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:NSLocalizedString(INTERNET_SHOPPING_STEP_ONE_TITLE_TEXT_KEY, nil)];
        if (titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        // account to charge
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_ACCOUNT_CHARGE_TEXT_KEY, nil)];
        
        NSString *account = [NSString stringWithFormat:@"%@ | %@",
                             [Tools notNilString:[[self selectedAccount] accountType]],
                             [Tools getCurrencyLiteral:[[self selectedAccount] currency]]];
        
        [titleAndAttributes addAttribute:[Tools notNilString: account]];
        [titleAndAttributes addAttribute:[Tools notNilString:[[self selectedAccount] subject]]];
        [titleAndAttributes addAttribute:@""];
        if (titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        // company
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_COMPANY_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[self selectedCompany]]];
        if (titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        // transaction number
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_TRANS_NUMBER_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation transactionNumber]]];
        if(titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        // business
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_BUSINESS_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation establishment]]];
        if(titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        // currency
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_CURRENCY_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation currency]]];
        if(titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        // exchange
        if ([response.additionalInformation exchangeRate] != nil) {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_EXCHANGE_RATE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation exchangeRate]]];
            if(titleAndAttributes != nil)
                [informationArray addObject:titleAndAttributes];
            
        }
        
        [self setConfirmationInfoArray:informationArray];
        [[self delegate] confirmationAnalysisHasFinished];
    
    }

}

- (NSString *) paymentSuccessDataValidation {

    return [super paymentSuccessDataValidation];
    
}

- (void) startPaymentSuccessRequest {

    NSString *errorMessage = [self paymentSuccessDataValidation];
    
    if (errorMessage == nil || [errorMessage isEqualToString:@""]) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paymentISSuccessResponseReceived:) name:kNotificationSafetyPayConfirmationResponseReceived object:nil];
        [[Updater getInstance] safetyPayResultFromSecondKeyFactor : [self secondFactorKey]];
        
    } else {
        
        [Tools showInfoWithMessage:errorMessage];
        
    }

}

- (SafetyPayConfirmationResponse *) Mock_safetyPayConfirmationResponseDataWithXMLString: (NSString *) xmlString {
    
    NSData *data = [xmlString dataUsingEncoding:NSUTF8StringEncoding];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    SafetyPayConfirmationResponse *response = [SafetyPayConfirmationResponse alloc];
    response.openingTag = @"MSG-S";
    
    [parser setDelegate: response];
    
    [response parserDidStartDocument:parser];
    
    [parser parse];
    
    return response;
    
}

- (void) paymentISSuccessResponseReceived : (NSNotification *) notification {
 
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationSafetyPayConfirmationResponseReceived object:nil];
    
    [[self appDelegate] hideActivityIndicator];
    
    SafetyPayConfirmationResponse *response = (SafetyPayConfirmationResponse *)[notification object];
    
    //SafetyPayConfirmationResponse *response = [self Mock_safetyPayConfirmationResponseDataWithXMLString:ResponseConfirmation];
    
    if (response != nil && ![response isError]){
    
        // success information
        NSMutableArray *informationArray = [[[NSMutableArray alloc] init] autorelease];
        
        // - Operation number
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_OPERATION_NUMBER_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation operationNumber]]];
        if (titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        
        // - Operation date
        if([response.additionalInformation operationDateString] !=nil && [[response.additionalInformation operationDateString] length]>0)
        {
        
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
            [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_OPERATION_DATE_TEXT_KEY, nil)];
        
            [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation operationDateString]]];
        
            if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
            }
        
        }
        // - Operation
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_OPERATION_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation service]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Account
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_ACCOUNT_CHARGE_TEXT_KEY, nil)];
        
        NSString *account = [NSString stringWithFormat:@"%@ | %@",
                             [Tools notNilString:[[self selectedAccount] accountType]],
                             [Tools getCurrencyLiteral:[[self selectedAccount] currency]]];
        
        [titleAndAttributes addAttribute:[Tools notNilString: account]];
        [titleAndAttributes addAttribute:[Tools notNilString:[[self selectedAccount] subject]]];
        [titleAndAttributes addAttribute:@""];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Company
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_COMPANY_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[self selectedCompany]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }

        // transaction number
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_TRANS_NUMBER_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation transactionNumber]]];
        if(titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        
        // business
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_BUSINESS_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation establishment]]];
        if(titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        
        // currency
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_CURRENCY_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation currency]]];
        if(titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
      
        
        NSString *amountToPay = [Tools notNilString:[NSString stringWithFormat:@"%@",[response.additionalInformation paidAmount]]];
        
        self.amountToPay = [NSString stringWithFormat:@"%@ %@", [response.additionalInformation badge], amountToPay];
        
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_AMOUNT_PAYED_TEXT_KEY, nil)];
        [titleAndAttributes addAttribute:self.amountToPay];
        if (titleAndAttributes != nil)
            [informationArray addObject:titleAndAttributes];
        
        // exchange
        if ([response.additionalInformation exchangeRate] != nil && [[response.additionalInformation exchangeRate] length]>0) {
            
            // - Exchange            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_EXCHANGE_RATE_TEXT_KEY, nil)];
            [titleAndAttributes addAttribute:[Tools notNilString:[response.additionalInformation exchangeRate]]];
            if(titleAndAttributes != nil)
                [informationArray addObject:titleAndAttributes];
            
            // - Charged amount
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
            [titleAndAttributes setTitleString:NSLocalizedString(INTERNET_SHOPPING_AMOUNT_CHARGED_TEXT_KEY, nil)];
            
            NSString *amountCharged = [NSString stringWithFormat:@"%@",
                                       [Tools notNilString:[NSString stringWithFormat:@"%@", [response.additionalInformation amount]]]];
            
            [titleAndAttributes addAttribute:[Tools notNilString:amountCharged]];
            if (titleAndAttributes != nil) {
                [informationArray addObject:titleAndAttributes];
            }
            
            
        }
        
        [self setSuccessInfoArray:informationArray];
        [[self delegate] successAnalysisHasFinished];
        
    } else {
    
        [[self delegate] successAnalysisHasFinishedWithError];
        
    }
    
}

- (PaymentTypeEnum) paymentOperationType {
    
    return PTEPaymentISService;
    
}

@end
