//
//  PaymentPSGasServProcess.m
//  NXT_Peru_iPhone
//
//  Created by Ricardo Herrera Valle on 11/28/13.
//  Copyright (c) 2013 Movilok. All rights reserved.
//

#import "PaymentPSGasServProcess.h"

#import "AccountList.h"
#import "BankAccount.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "PaymentDataResponse.h"
#import "PaymentConfirmationResponse.h"
#import "PaymentInstitutionsAndCompaniesDetailResponse.h"
#import "PaymentSuccessResponse.h"
#import "Payment.h"
#import "PaymentList.h"
#import "PublicServiceResponse.h"
#import "Session.h"
#import "StringKeys.h"
#import "StringList.h"
#import "TagAndValue.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "Updater.h"

@implementation PaymentPSGasServProcess
#pragma mark -
#pragma mark Properties

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Information distribution

/**
 * Checks if there is enough information to start the startPaymentDataRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentInitialValidation {
    
    NSString *result = [super paymentInitialValidation];
    
    return result;
    
}

/**
 * Performs the payment initial request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentInitialRequest {
    
    NSString *errorMessage = [self paymentInitialValidation];
    
    if ((errorMessage == nil) || [errorMessage isEqualToString:@""]) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(initialResponseReceived:)
                                                     name:kNotificationPaymentInstitutionsDetailResult
                                                   object:nil];
        
        [[Updater getInstance] obtainPaymentInstitutionsDetailForCod:[self selectedCompanyParameter] optionType:@"SERV"];
        
    } else {
        
        [Tools showInfoWithMessage:errorMessage];
        
    }
    
}

/**
 * Notifies the payment operation process the initial response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The initial response received from the server
 */
- (void)initialResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationPaymentInstitutionsDetailResult
                                                  object:nil];
    
    [[self appDelegate] hideActivityIndicator];
    
    PaymentInstitutionsAndCompaniesDetailResponse *response = (PaymentInstitutionsAndCompaniesDetailResponse *)[notification object];
    
    if (response != nil && ![response isError]) {
        
        [self setResponseInformation:response];
        [[self psDelegate] initilizationHasFinished:TRUE];
        
    }
    
}

/**
 * Checks if there is enough information to start the startPaymentSuccessRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentSuccessDataValidation {
    
    return [super paymentSuccessDataValidation];
    
}

/**
 * Performs the payment success request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (void)startPaymentSuccessRequest {
    
    NSString *errorMessage = [self paymentSuccessDataValidation];
    
    if ((errorMessage == nil) || ([errorMessage isEqualToString:@""])) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(paymentSuccessResponseReceived:)
                                                     name:kNotificationPaymentSuccessResultEnds object:nil];
        
        [[Updater getInstance] obtainPaymentPSWaterServicesSuccessForCompany:[self selectedCompanyCode]
                                                             secondFactorKey:[self secondFactorKey]];
        
    } else {
        
        [Tools showInfoWithMessage:errorMessage];
        
    }
    
}

/**
 * Notifies the payment operation process the success response received from the server. The payment operation process notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The success response received from the server
 */
- (void)paymentSuccessResponseReceived:(NSNotification *)notification {
    
}

/**
 * Returns the payment operation type
 *
 * @return The payment operation type
 */
- (PaymentTypeEnum)paymentOperationType {
    
    return PTEPaymentPSGas;
    
}

@end
