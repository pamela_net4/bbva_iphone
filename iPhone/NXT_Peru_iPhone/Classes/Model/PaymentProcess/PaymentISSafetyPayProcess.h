//
//  PaymentISSafetyPayProcess.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 3/02/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentISProcess.h"

@class StatusEnabledResponse;

@interface PaymentISSafetyPayProcess : PaymentISProcess{

    @private
    
    //NSString *transactionNumber_;
    
    //NSString *amountToPay_;
    
}

//@property (nonatomic, readwrite, retain) NSString *transactionNumber;

//@property (nonatomic, readwrite, retain) NSString *amountToPay;

@end
