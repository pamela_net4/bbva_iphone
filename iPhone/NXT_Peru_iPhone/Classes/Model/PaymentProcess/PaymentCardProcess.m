/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import "PaymentCardProcess.h"

#import "Constants.h"
#import "Locality.h"
#import "LocalityList.h"
#import "Session.h"
#import "StringKeys.h"
#import "StringList.h"
#import "Tools.h"

@implementation PaymentCardProcess

#pragma mark -
#pragma mark Properties

@synthesize amount = amount_;
@synthesize currencyArray = currencyArray_;
@synthesize currencyTextArray = currencyTextArray_;
@synthesize cardNumber = cardNumber_;
@synthesize localityArray = localityArray_;
@synthesize beneficiary = beneficiary_;
@synthesize selectedCurrency = selectedCurrency_;
@synthesize ownCardSelected = ownCardSelected_;
@synthesize selectedLocality = selectedLocality_;
@synthesize selectedLocalityIndex = selectedLocalityIndex_;
@synthesize selectedCurrencyIndex = selectedCurrencyIndex_;

#pragma mark -
#pragma mark Initialization

/**	
 * Object initializer
 *
 * @return An initialized instance
 */
- (id) init {
    
	if (self = [super init]) {
        
        currencyArray_ = [[NSMutableArray alloc] initWithObjects:
                          CURRENCY_SOLES_LITERAL,
                          CURRENCY_DOLARES_LITERAL,
                          nil];
        
        currencyTextArray_ = [[NSMutableArray alloc] initWithObjects:
                              NSLocalizedString(SOLES_CURRENCY_TEXT_KEY, nil),
                              NSLocalizedString(DOLLARS_CURRENCY_TEXT_KEY, nil),
                              nil];
        
        amount_ = [@"" copy];
        selectedLocalityIndex_ = -1;
        selectedCurrencyIndex_ = 0;
        
	}
	
	return self;
    
}

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [amount_ release];
    amount_ = nil;
    
    [currencyArray_ release];
    currencyArray_ = nil;

    [currencyTextArray_ release];
    currencyTextArray_ = nil;
    
    [selectedCurrency_ release];
    selectedCurrency_ = nil;
    
    [cardNumber_ release];
    cardNumber_ = nil;
    
    [localityArray_ release];
    localityArray_ = nil;
    
    [selectedLocality_ release];
    selectedLocality_ = nil;
    
    [beneficiary_ release];
    beneficiary_ = nil;
    
    [ownCardSelected_ release];
    ownCardSelected_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the locality array.
 *
 * @return The locality array.
 */
- (NSArray *)localityArray {
    
    return [NSArray arrayWithArray:localityArray_];
    
}

/*
 * Sets locality array
 *
 * @param localityArray The new locality array to set. Only Locality instances are stored.
 */
- (void)setLocalityArray:(NSArray *)localityArray {

    if (localityArray_ == nil) {
        
        localityArray_ = [[NSMutableArray alloc] init];
        
    }
    
    if (localityArray_ != localityArray) {
        
        [localityArray_ removeAllObjects];
        
        Class localityClass = [Locality class];
        
        for (NSObject *object in localityArray) {
            
            if ([object isKindOfClass:localityClass]) {
                
                [localityArray_ addObject:object];
                
            }
            
        }
        
    }
    
    if ([localityArray_ count] > 0) {
        
        [self setSelectedLocalityIndex:0];
        
    } else {
        
        [self setSelectedLocalityIndex:-1];
        
    }

}

/**
 * Sets the own card
 */
- (void)setOwnCardSelected:(Card *)ownCardSelected {

    if (ownCardSelected_ != ownCardSelected) {
        
        [ownCardSelected retain];
        [ownCardSelected_ release];
        ownCardSelected_ = ownCardSelected;
        
    }

}

@end
