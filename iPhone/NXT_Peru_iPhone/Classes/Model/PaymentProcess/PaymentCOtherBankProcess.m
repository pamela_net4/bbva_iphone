/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "PaymentCOtherBankProcess.h"

#import "AccountList.h"
#import "BankAccount.h"
#import "Card.h"
#import "CardList.h"
#import "CardType.h"
#import "GlobalAdditionalInformation.h"
#import "Locality.h"
#import "LocalityList.h"
#import "NXT_Peru_iPhone_AppDelegate.h"
#import "Payment.h"
#import "PaymentBaseProcess.h"
#import "PaymentCardProcess.h"
#import "PaymentTINOnlineResponse.h"
#import "PaymentConfirmationResponse.h"
#import "PaymentDataResponse.h"
#import "PaymentList.h"
#import "PaymentSuccessResponse.h"
#import "Session.h"
#import "StringKeys.h"
#import "StringList.h"
#import "TagAndValue.h"
#import "TitleAndAttributes.h"
#import "Tools.h"
#import "Updater.h"


#pragma mark -

/**
 * PaymentCOtherBankProcess private extension.
 */
@interface PaymentCOtherBankProcess()

/**
 * Provides read-write access to the amount to transfer.
 */
@property (nonatomic, readwrite, copy) NSString *amountToTransfer;

/**
 * Provides read-write access to the tax.
 */
@property (nonatomic, readwrite, copy) NSString *tax;

@end


#pragma mark -

@implementation PaymentCOtherBankProcess

#pragma mark -
#pragma mark Properties

@synthesize selectedClass = selectedClass_;
@synthesize destinationBank = destinationBank_;
@synthesize destinationBankName = destinationBankName_;
@synthesize networkUse = networkUse_;
@synthesize amountToTransfer = amountToTransfer_;
@synthesize tax = tax_;
@synthesize typeFlow = typeFlow_;
@synthesize typeFlowChoosen = typeFlowChoosen_;
@synthesize commissionL = commissionL_;
@synthesize commissionN = commissionN_;
@synthesize totalPayL = totalPayL_;
@synthesize totalPayN = totalPayN_;
@synthesize disclaimer = disclaimer_;
@synthesize responseConfirmation = responseConfirmation_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates the memory occupied by the receiver.
 */
- (void)dealloc {
    
    [selectedClass_ release];
    selectedClass_ = nil;
    
    [destinationBank_ release];
    destinationBank_ = nil;
    
    [destinationBankName_ release];
    destinationBankName_ = nil;
    
    [tax_ release];
    tax_ = nil;
    
    [networkUse_ release];
    networkUse_ = nil;
    
    [amountToTransfer_ release];
    amountToTransfer_ = nil;
    
    [typeFlow_ release];
    typeFlow_ = nil;
    
    [commissionN_ release];
    commissionN_ = nil;
    
    [commissionL_ release];
    commissionL_ = nil;
    
    [totalPayL_ release];
    totalPayL_ = nil;
    
    [totalPayN_ release];
    totalPayN_ = nil;
    
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Information distribution

- (BOOL)startFrequentOperationReactiveRequest
{

    NSArray *localityArray = [self localityArray];
    Locality *locality = [localityArray objectAtIndex:[self selectedLocalityIndex]];
    NSString *localityCode = [locality code];
    NSString *localityName = [locality localityName];
    
    
   
    
    
    
       [[Updater getInstance] obtainPaymentToOtherBankFrequentOperationReactiveStepOneWithOperation:@"Pago de tarjeta de otros bancos"
                                                                           andDestinationAccount:[Tools formatCardNumber:self.cardNumber]
                                                                       andDestinationAccountType:selectedClass_
                                                                          andNameDestinationBank:destinationBankName_
                                                                                     andBankCode:destinationBank_
                                                                                 andLocalityName:localityName
                                                                                 andLocalityCode:localityCode
                                                                                  andBeneficiary:self.beneficiary
                                                                                     andCurrency:self.selectedAccount.currency
                                                                                andOriginAccount:self.selectedAccount.number
                                                                                       andAmount:amountToTransfer_];
    
    
    return NO;
}

/**
 * Checks if there is enough information to start the startPaymentDataRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentDataValidation {
    
    NSString *result = @"";
        
    return result;
    
}

/**
 * Performs the payment data request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentDataRequest {
    
    NSString *errorMessage = [self paymentDataValidation];
    
    if ((errorMessage == nil) || ([errorMessage isEqualToString:@""])) {
        
		[[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(dataResponseReceived:) 
                                                     name:kNotificationPaymentDataResultEnds object:nil];
        
        [[Updater getInstance] obtainPaymentCardOtherBankDataForClass:selectedClass_ 
                                                             destBank:destinationBank_ 
                                                        accountNumber:[self cardNumber]];
        
    } else {
    
        [Tools showInfoWithMessage:errorMessage];
    
    }   
    
}

/**
 * Notifies the payment operation process the data response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The data response received from the server
 * @return YES when the data response is correct, NO otherwise
 */
- (void)dataResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentDataResultEnds 
                                                  object:nil];
    [[self appDelegate] hideActivityIndicator];

    PaymentDataResponse *response = (PaymentDataResponse *)[notification object];
	
	if (![response isError]) {

		// Accounts       
        AccountList *bankAccounts = [[Session getInstance] accountList];
        NSMutableArray *accounts = [NSMutableArray array];
        NSArray *array = [NSArray arrayWithArray:[[response numberAccountEnabledList] stringList]];
        
        for (NSString *numberAccount in array) {
            
            BankAccount *result = [bankAccounts accountFromAccountTerminateNumber:numberAccount];
            
            if (result != nil) {
                
                [accounts addObject:result];
                
            }
            
        }
        
        [self setAccountsArray:accounts];
        
        //Location
        
        LocalityList *localityList = [response emisionPlaces];
        
        [self setLocalityArray:[localityList localityList]];
        
        // Data information
        
        NSMutableArray *informationArray = [[[NSMutableArray alloc] init] autorelease];

                
        // - Card number
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_PAYMENT_TO_CARD_TEXT_KEY, nil)];
        
        NSString *string1 = [[self cardNumber] substringWithRange:NSMakeRange(0,4)];
        NSString *string2 = [[self cardNumber] substringWithRange:NSMakeRange(4,4)];
        NSString *string3 = [[self cardNumber] substringWithRange:NSMakeRange(8,4)];
        NSString *string4 = [[self cardNumber] substringWithRange:NSMakeRange(12,4)];
        
        NSString *cardStyled = [NSString stringWithFormat:@"%@-%@-%@-%@",
                                string1,
                                string2, 
                                string3, 
                                string4];
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:selectedClass_], [Tools notNilString:cardStyled]]];
        [titleAndAttributes addAttribute:[Tools notNilString:[response bankName]]];

        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        [self setDataInfoArray:informationArray];
        
        [[self delegate] dataAnalysisHasFinished];

	}
        
}

/**
 * Checks if there is enough information to start the startPaymentConfirmationRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentConfirmationValidation {
    
    NSString *result = @"";
    
    if ([self selectedLocalityIndex] < 0) {
        
        result = NSLocalizedString(PAYMENT_ERROR_LOCALITY_TEXT_KEY, nil);
        
    } else if ([self selectedAccountIndex] < 0) {
        
        result = NSLocalizedString(PAYMENT_ERROR_ACCOUNT_PAYMENT_TEXT_KEY, nil);
        
    } else if (([self amount] == nil || [[self amount] isEqualToString:@""])) {
        
        result = NSLocalizedString(CARD_PAYMENT_AMOUNT_TO_PAY_ERROR_KEY, nil);
        
    } else if ([self selectedCurrencyIndex] < 0) {
        
        result = NSLocalizedString(PAYMENT_ERROR_CURRENCY_TEXT_KEY, nil);
        
    } else {
        
        result = [super paymentConfirmationValidation];
        
    }
        
    return result;
    
}
- (NSString *)paymentTINConfirmationValidation {
    
    NSString *result = @"";

    if([typeFlowChoosen_ isEqualToString:@"PH"] || [typeFlow_ isEqualToString:@"N"]){
        if ([[self beneficiary] isEqualToString:@""]) {
     
        result = NSLocalizedString(PAYMENT_ERROR_BENEFICIARY_NAME_TEXT_KEY, nil);
        
        }
    }else if([typeFlowChoosen_ length]==0){
        
        result = NSLocalizedString(@"Seleccione el tipo de pago", nil);

    }
    return result;
}

/**
 * Performs the payment confirmation request to the server. Returns the operation status. Base class always returns NO, because no
 * operation is performed
 */
- (void)startPaymentConfirmationRequest {
    
    NSString *errorMessage = [self paymentConfirmationValidation];
    
    if ((errorMessage == nil) || ([errorMessage isEqualToString:@""])) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(confirmationToTINResponseReceived:)
                                                     name:kNotificationPaymentTINOnlineResultEnds object:nil];
        
        NSArray *localityArray = [self localityArray];
        Locality *locality = [localityArray objectAtIndex:[self selectedLocalityIndex]];
        NSString *localityCode = [locality code];
        
        [self setSelectedLocality:localityCode];
        [self setSelectedAccount:[[self accountsArray] objectAtIndex:[self selectedAccountIndex]]];
        [self setSelectedCurrency:[[self currencyArray] objectAtIndex:[self selectedCurrencyIndex]]];
        
    
        [[Updater getInstance] obtainPaymentCardCardOtherBankConfirmationForLocality:[self selectedLocality]
                                                                               issue:[[self selectedAccount] branchAccount] 
                                                                            currency:[self selectedCurrency] 
                                                                              amount:[Tools formatAmountWithDotDecimalSeparator:[self amount]]
                                                                              email1:[Tools notNilString:[self destinationEmail1]] 
                                                                              email2:[Tools notNilString:[self destinationEmail2]]
                                                                        phoneNumber1:[Tools notNilString:[self destinationSMS1]]
                                                                        phoneNumber2:[Tools notNilString:[self destinationSMS2]] 
                                                                            carrier1:[Tools notNilString:[self carrierLiteralForCarrier:[self selectedCarrier1Index]]]
                                                                            carrier2:[Tools notNilString:[self carrierLiteralForCarrier:[self selectedCarrier2Index]]]
                                                                             message:[self emailMessage]];
    
    } else {
    
        [Tools showInfoWithMessage:errorMessage];
    
    }   
    
}

- (void)startTINPaymentConfirmationRequest{
    
    NSString *errorMessage = [self paymentTINConfirmationValidation];
    
    if ((errorMessage == nil) || ([errorMessage isEqualToString:@""])) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(confirmationResponseReceived:)
                                                     name:kNotificationPaymentConfirmationResultEnds object:nil];
        
        [[Updater getInstance] obtainPaymentCardCardOtherBankTINConfirmationForFlag:[self typeFlowChoosen] beneficiary:[self beneficiary]];
        
    }else{
        
        [Tools showInfoWithMessage:errorMessage];
    }
    
}

/**
 * Notifies the payment operation process the confirmation response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The confirmation response received from the server
 */
- (void)confirmationToTINResponseReceived:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kNotificationPaymentTINOnlineResultEnds
                                                  object:nil];
    [[self appDelegate] hideActivityIndicator];
    
    PaymentTINOnlineResponse *response = (PaymentTINOnlineResponse *)[notification object];
    
    
    if (![response isError]) {
        
        [self setCommissionL:[response commissionL]];
        [self setCommissionN:[response commissionN]];
        [self setTotalPayL:[response totalPayL]];
        [self setTotalPayN:[response totalPayN]];
        [self setTypeFlow:[response typeFlow]];
        [self setDisclaimer:[response disclaimer]];
        
        if(response.additionalInformation){
            [self showConfirmationResult:response.additionalInformation];

        }else{
        [[self delegate]  confirmationTINAnalysisHasFinished];
        }
    }
    
}
/**
 * Notifies the payment operation process the confirmation response received from the server. The payment operation helper notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The confirmation response received from the server
 */
- (void)confirmationResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentConfirmationResultEnds
                                                  object:nil];
    [[self appDelegate] hideActivityIndicator];
    
    PaymentConfirmationResponse *response = (PaymentConfirmationResponse *)[notification object];
	
	if (![response isError]) {
    
        responseConfirmation_ = [response retain];
//        NSString *amount = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[response amountChargedCurrency]], [Tools notNilString:[response amountCharged]]];
        
        [self showConfirmationResult:response];
        
    }
    
}

-(void)showConfirmationResult:(PaymentConfirmationResponse*)response{
    NSString *account = [NSString stringWithFormat:@"%@ | %@ \n%@",
                         [Tools notNilString:[[self selectedAccount] accountType]],
                         [Tools getCurrencyLiteral:[[self selectedAccount] currency]],
                         [Tools notNilString:[[self selectedAccount] number]]];
    
    self.coordHint = [response coordinate];
    self.seal = [response seal];
    self.legalTermsURL = [response disclaimer];
    
    // Confirmation information
    
    NSMutableArray *informationArray = [[[NSMutableArray alloc] init] autorelease];
    
    // - Amount to pay
    TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_AMOUNT_TO_PAY_TEXT_KEY, nil)];
    
    NSString *amount = [NSString stringWithFormat:@"%@ %@",
                        [Tools translateServerCurrencySymbolIntoClientCurrencySymbol:[Tools notNilString:[response amountChargedCurrency]]],
                        [Tools notNilString:[response amountCharged]]];
    
    [titleAndAttributes addAttribute:[Tools notNilString:amount]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    // - Operation
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OPERATION_TEXT_KEY, nil)];
    [titleAndAttributes addAttribute:response.operation];
    /*[titleAndAttributes addAttribute:[NSString stringWithFormat:
     NSLocalizedString(PAYMENT_OF_TEXT_KEY, nil),
     NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TITLE_TEXT_KEY, nil)]];
     */
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    
    // - Account
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_ORIGIN_ACCOUNT_TEXT_KEY, nil)];
    
    [titleAndAttributes addAttribute:[Tools notNilString:account]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    // Destination bank
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_DEST_BANK_LONG_TEXT_KEY, nil)];
    
    [titleAndAttributes addAttribute:[Tools notNilString:[response bankName]]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    // - Card
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil)];
    
    NSString *string1 = [[self cardNumber] substringWithRange:NSMakeRange(0,4)];
    NSString *string2 = [[self cardNumber] substringWithRange:NSMakeRange(4,4)];
    NSString *string3 = [[self cardNumber] substringWithRange:NSMakeRange(8,4)];
    NSString *string4 = [[self cardNumber] substringWithRange:NSMakeRange(12,4)];
    
    NSString *cardStyled = [NSString stringWithFormat:@"%@-%@-%@-%@",
                            string1,
                            string2,
                            string3,
                            string4];
    
    [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:selectedClass_], [Tools notNilString:cardStyled]]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    
    if([response.beneficiary length]>0)
    {
    
        self.beneficiary = response.beneficiary;
        // - Beneficiary
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OWNER_TEXT_KEY, nil)];
    
        [titleAndAttributes addAttribute:[Tools notNilString:response.beneficiary]];
    
        if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
        }
    
    }
    
    // - Amount to transfer
    
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_AMOUNT_TO_TRANSFER_TEXT_KEY, nil)];
    
    self.amountToTransfer = [NSString stringWithFormat:@"%@ %@",
                             [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response amountPaidCurrency]]],
                             [Tools notNilString:[response amountPaid]]];
    
    [titleAndAttributes addAttribute:[Tools notNilString:amountToTransfer_]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    // - Taxes
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TAX_TEXT_KEY, nil)];
    
    self.tax = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response taxCurrency]]], [Tools notNilString:[response tax]]];
    
    [titleAndAttributes addAttribute:[Tools notNilString:tax_]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    // - Network use
    titleAndAttributes = [TitleAndAttributes titleAndAttributes];
    
    [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_NETWORK_USE_TEXT_KEY, nil)];
    
    NSString *networkUse = [Tools notNilString:[response networkUse]];
    
    NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:networkUse];
    
    if (![number isEqualToNumber:[NSDecimalNumber zero]]){
        
        self.networkUse = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response networkUseCurrency]]], [Tools notNilString:networkUse]];
        
    } else {
        
        self.networkUse = networkUse;
        
        
    }
    
    [titleAndAttributes addAttribute:[Tools notNilString:networkUse_]];
    
    if (titleAndAttributes != nil) {
        [informationArray addObject:titleAndAttributes];
    }
    
    
    [self setConfirmationInfoArray:informationArray];
    
    [[self delegate] confirmationAnalysisHasFinished];
}

/**
 * Checks if there is enough information to start the startPaymentSuccessRequest and if the data to send is valid.
 *
 * @return nil or @"" if there is not problem. IF NOT returns the message to show
 */
- (NSString *)paymentSuccessDataValidation {
    
    return [super paymentSuccessDataValidation];
    
}

/**
 * Performs the payment success request to the server. Returns the operation status. Base class always returns NO, because no operation is
 * performed
 */
- (void)startPaymentSuccessRequest {
    
    NSString *errorMessage = [self paymentSuccessDataValidation];
    
    if ((errorMessage == nil) || ([errorMessage isEqualToString:@""])) {
        
        [[self appDelegate] showActivityIndicator:poai_Both];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(paymentSuccessResponseReceived:) 
                                                     name:kNotificationPaymentSuccessResultEnds object:nil];
        
        [[Updater getInstance] obtainPaymentCardOtherBankSuccessForSecondFactorKey:[self secondFactorKey]];
        
    } else {
    
        [Tools showInfoWithMessage:errorMessage];
    
    }
    
}

/**
 * Notifies the payment operation process the success response received from the server. The payment operation process notifies
 * back whether the operation is correct or not. Default implementation always notifies an error back
 *
 * @param notification The success response received from the server
 */
- (void)paymentSuccessResponseReceived:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:kNotificationPaymentSuccessResultEnds 
                                                  object:nil];
    [[self appDelegate] hideActivityIndicator];
    
    PaymentSuccessResponse *response = (PaymentSuccessResponse *)[notification object];
	
	if (![response isError]) {
        
        [self setMessage:[Tools notNilString:[response message]]];
        [self setNotificationMessage:[Tools notNilString:[response notificationMessage]]];
        NSString *account = [NSString stringWithFormat:@"%@ | %@ \n%@",
                             [Tools notNilString:[[self selectedAccount] accountType]], 
                             [Tools getCurrencyLiteral:[[self selectedAccount] currency]],
                             [Tools notNilString:[[self selectedAccount] number]]];
        
        // Success information
        
        NSMutableArray *informationArray = [[[NSMutableArray alloc] init] autorelease];
                
        // - Operation state
        TitleAndAttributes *titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        if([[response operationState] length]>0){
            [titleAndAttributes setTitleString:NSLocalizedString(@"Estado de la operación", nil)];
        
            [titleAndAttributes addAttribute:[Tools notNilString:[response operationState]]];
        
            if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
            }
        }
        
        // - Operation number
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        if([[response operationNumber] length]>0){

            [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OPERATION_NUMBER_TEXT_KEY, nil)];
        
            [titleAndAttributes addAttribute:[Tools notNilString:[response operationNumber]]];
        
            if (titleAndAttributes != nil) {
                [informationArray addObject:titleAndAttributes];
            }
        }

        
        // - Date Time
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(PAYMENT_RECHARGE_DATE_KEY, nil)];
        
        NSString *dateString = @"";
        
        if (([[response transactionDate] length] > 0) && ([[response transactionHour] length] > 0)) {
            
            dateString = [NSString stringWithFormat:@"%@ | %@", [response transactionDate], [response transactionHour]];

        } else {
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat: @"dd/MM/yyyy | HH:mm"];
            dateString = [dateFormat stringFromDate:[NSDate date]];
            [dateFormat release];
            
        }
        
        [titleAndAttributes addAttribute:[Tools notNilString:dateString]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Operation
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OPERATION_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response operation]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Account
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_ORIGIN_ACCOUNT_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:account];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Account owner
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_OWNER_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response accountOwner]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Amount to transfer
        
//        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
//        
//        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_AMOUNT_TO_TRANSFER_TEXT_KEY, nil)];
//        
//        [titleAndAttributes addAttribute:amountToTransfer_];
//        
//        if (titleAndAttributes != nil) {
//            [informationArray addObject:titleAndAttributes];
//        }
        
        // Destination bank
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_DEST_BANK_LONG_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:[response destinationBank]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Card
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_CARD_NUMBER_TEXT_KEY, nil)];
        
        NSString *string1 = @"";
        NSString *string2 = @"";
        NSString *string3 = @"";
        NSString *string4 = @"";
        
        if ([[self cardNumber] length] >= 16) {
            
            string1 = [[self cardNumber] substringWithRange:NSMakeRange(0,4)];
            string2 = [[self cardNumber] substringWithRange:NSMakeRange(4,4)];
            string3 = [[self cardNumber] substringWithRange:NSMakeRange(8,4)];
            string4 = [[self cardNumber] substringWithRange:NSMakeRange(12,4)];
            
        }
        
        NSString *cardStyled = [NSString stringWithFormat:@"%@-%@-%@-%@",
                                string1,
                                string2, 
                                string3, 
                                string4];
        
        [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@", [Tools notNilString:selectedClass_], [Tools notNilString:cardStyled]]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Beneficiary
        if([[response cardHolder] length]>0){
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
            [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OWNER_TEXT_KEY, nil)];
        
            [titleAndAttributes addAttribute:[Tools notNilString:[response cardHolder]]];
            
            if (titleAndAttributes != nil) {
                [informationArray addObject:titleAndAttributes];
            }
        }

        // - Amount to transfer
        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_AMOUNT_TEXT_KEY, nil)];
        
        NSString *amountToTransfer = [NSString stringWithFormat:@"%@ %@",
                                      [Tools clientCurrencySymbolForServerCurrency:[response amountChargedCurrency]],
                                      [Tools notNilString:[response amountPayed]]];
        [titleAndAttributes addAttribute:[Tools notNilString:amountToTransfer]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Taxes
        self.tax = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response taxCurrency]]], [Tools notNilString:[response tax]]];

        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TAX_TEXT_KEY, nil)];
                
        [titleAndAttributes addAttribute:[Tools notNilString:tax_]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Network use
        NSString *networkUse = [Tools notNilString:[response networkUse]];
        
        NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:networkUse];
        
        if (![number isEqualToNumber:[NSDecimalNumber zero]]){
            
            self.networkUse = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response networkUseCurrency]]], [Tools notNilString:networkUse]];
            
        } else {
            
            self.networkUse = networkUse;
            
            
        }

        
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_NETWORK_USE_TEXT_KEY, nil)];
        
        [titleAndAttributes addAttribute:[Tools notNilString:networkUse_]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Amount to charge
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_FINAL_AMOUNT_TEXT_KEY, nil)];
        
        NSString *amountToCharge = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response amountChargedCurrency]]], [Tools notNilString:[response amountToCharge]]];
        
        [titleAndAttributes addAttribute:[Tools notNilString:amountToCharge]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        // - Change type
        if (([response changeType] != nil) && ([[response changeType] floatValue] != 0.0f)) {
            
            titleAndAttributes = [TitleAndAttributes titleAndAttributes];
            
            [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_CHAGE_TYPE_TEXT_KEY, nil)];
            
            [titleAndAttributes addAttribute:[NSString stringWithFormat:@"%@ %@",[Tools getCurrencySimbol:[Tools notNilString:[response changeTypeCurrency]]], [Tools notNilString:[response changeType]]]];
            
            if (titleAndAttributes != nil) {
                [informationArray addObject:titleAndAttributes];
            }
            
        }
        
        // - Total amount
        titleAndAttributes = [TitleAndAttributes titleAndAttributes];
        
        [titleAndAttributes setTitleString:NSLocalizedString(CARD_PAYMENT_OTHER_BANK_TOTAL_AMOUNT_TEXT_KEY, nil)];
        
        NSString *totalAmount = [NSString stringWithFormat:@"%@ %@", [Tools clientCurrencySymbolForServerCurrency:[Tools notNilString:[response totalChargedCurrency]]], [Tools notNilString:[response totalCharged]]];
        
        [titleAndAttributes addAttribute:[Tools notNilString:totalAmount]];
        
        if (titleAndAttributes != nil) {
            [informationArray addObject:titleAndAttributes];
        }
        
        [self setSuccessInfoArray:informationArray];
        
        [[self delegate] successAnalysisHasFinished];
        
    } else {
        
        [[self delegate] successAnalysisHasFinishedWithError];
        
    }
    
}

/**
 * Returns the payment operation type
 *
 * @return The payment operation type
 */
- (PaymentTypeEnum)paymentOperationType {
    
    return PTEPaymentOtherBank;
    
}


@end
