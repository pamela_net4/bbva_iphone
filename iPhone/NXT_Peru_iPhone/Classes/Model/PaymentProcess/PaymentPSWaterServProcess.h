/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import <Foundation/Foundation.h>
#import "PaymentPSProcess.h"

@class StatusEnabledResponse;

/**
 * Base class to represent a payment public service operation to help view controllers to display the 
 * information and perform the operations.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentPSWaterServProcess : PaymentPSProcess {
@private

    /**
     * The water amount.
     */
    NSString *waterAmount_;
    
    /**
     * The response of succes request.
     */
    PaymentSuccessResponse *successResponse;
}

/**
 * Provides read-write access to waterAmount.
 */
@property (nonatomic, readwrite, copy) NSString *waterAmount;

@end
