/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import <Foundation/Foundation.h>
#import "PaymentBaseProcess.h"

@class Carrier;
@class StatusEnabledResponse;

/**
 * Base class to represent a mobile recharge payment operation to help view controllers to display the 
 * information and perform the operations.
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface PaymentRechargeProcess : PaymentBaseProcess {
@private

    /**
     * Amount
     */
    NSString *amount_;
    
    /**
     * Phone number
     */
    NSString *phoneNumber_;

    /**
     * Company
     */
    Carrier *company_;
    
    /**
     * Carrier recharge list
     */
    NSMutableArray *carrierRechargeList_;

    /**
     * Carrier recharge index selected
     */
    NSInteger selectedCarrierRechargeIndex_;
    
    /**
     * Is agreement flag
     */
    BOOL isAgreement_;
    
    /**
     * Long max sumi
     */
    NSInteger longMaxSumi_;
    
    /**
     * Long min sumi
     */
    NSInteger longMinSumi_;
    
    /**
     * Recharge currency
     */
    NSString *rechargeCurrency_;
    
    /**
     * Extra info
     */
    NSString *extraInfo_;
    
}

/**
 * Provides read-write access to the amount
 */
@property (nonatomic, readwrite, copy) NSString *amount;

/**
 * Provides read-write access to the phoneNumber
 */
@property (nonatomic, readwrite, copy) NSString *phoneNumber;

/**
 * Provides read-write access to the company
 */
@property (nonatomic, readwrite, retain) Carrier *company;

/**
 * Provides read-only access to the company
 */
//@property (nonatomic, readonly, copy) NSArray *companiesArray;

/**
 * Provides read-only access to the carrierRechargeList
 */
@property (nonatomic, readonly, copy) NSArray *carrierRechargeList;

/**
 * Provides read-only access to the selectedCarrierRechargeIndex
 */
@property (nonatomic, readwrite, assign) NSInteger selectedCarrierRechargeIndex;

/**
 * Provides read-only access to the longMaxSumi
 */
@property (nonatomic, readonly, assign) NSInteger longMaxSumi;

/**
 * Provides read-only access to the longMaxSumi
 */
@property (nonatomic, readonly, assign) NSInteger longMinSumi;


@end
