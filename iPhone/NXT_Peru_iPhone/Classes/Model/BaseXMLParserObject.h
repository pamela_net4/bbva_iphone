/*
 * Copyright (c) 2010 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


/**
 * Enumerates the basic analyzing states
 */
typedef enum {
    
    BXPOAnalyzingStateIdle = 0, //!<The parser object is not analyzing any element
    BXPOAnalyzingStateCount //!<Child clases must start defining analyzing states at this position
    
} BXPOAnalyzingState;


/**
 * Base object to parse XML documents (or elements)
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BaseXMLParserObject : NSObject <NSXMLParserDelegate> {

@protected
    
	/**
	 * The current anaysis state
	 */
	NSInteger xmlAnalysisState_;
    
@private
	
	/**
	 * Parent XML parser object to return to once the current parsing is finished. Used to nest
     * parsing objects to parse different elements in a hierarchy
	 */
	BaseXMLParserObject* parentXMLParserObject_;
	
	/**
	 * Tag that opened the XML element to parse. The same tag must be found to close the element
	 */
	NSString *openingTag_;
	
	/**
	 * Auxiliary string to contruct element data
	 */
	NSMutableString *auxString_;

}

@end
