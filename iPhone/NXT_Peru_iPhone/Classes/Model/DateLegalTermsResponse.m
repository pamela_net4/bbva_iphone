/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

#import "DateLegalTermsResponse.h"
#import "Tools.h"


@interface DateLegalTermsResponse()

/**
 * Clears the object data
 */
- (void)clearData;

@end

#pragma mark -

/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    axeas_AnalyzingLegalTerms = 1,
    axeas_AnalyzingFAQURL
} AccountXMLElementAnalyzerState;

@implementation DateLegalTermsResponse

@synthesize dateLegalTerms = dateLegalTerms_;
@synthesize faqURL = faqURL_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [dateLegalTerms_ release];
    dateLegalTerms_ = nil;
    
    [faqURL_ release];
    faqURL_ = nil;
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/*
 * Clears the data of the object
 */
- (void) clearData {
	
    [dateLegalTerms_ release];
    dateLegalTerms_ = nil;
    
    [faqURL_ release];
    faqURL_ = nil;
	
}

/**
 * Super class designated initializer. It initalizes an empty FundDetail instance
 *
 * @return An empty FundDetail insntance
 */
- (id)init {	
    if (self = [super init]) {
		notificationToPost_ = [kNotificationDateLegalTermsReceived copy];
    }
	
	return self;
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void) parserDidStartDocument: (NSXMLParser*) parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void) parser: (NSXMLParser*) parser didStartElement: (NSString*) elementName namespaceURI: (NSString*) namespaceURI 
  qualifiedName: (NSString*) qualifiedName attributes: (NSDictionary*) attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"avisolegal"]) {
            xmlAnalysisCurrentValue_ = axeas_AnalyzingLegalTerms;
        } else if ([lname isEqualToString:@"urlfaq"]) {
            
            xmlAnalysisCurrentValue_ = axeas_AnalyzingFAQURL;
            
        } else {
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
        }
        
    }
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void) parser: (NSXMLParser*) parser didEndElement: (NSString*) elementName namespaceURI: (NSString*) namespaceURI qualifiedName: (NSString*) qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"appconfiguration"]) {
            informationUpdated_ = soie_InfoAvailable;
            [parser setDelegate:parentParseableObject_];
            //[parentParseableObject_ release];
            parentParseableObject_ = nil;
        } else if ([lname isEqualToString: @"avisolegal"]) {
			if (dateLegalTerms_ != nil) {
				NSString* auxString = [dateLegalTerms_ stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
				[dateLegalTerms_ release];
				dateLegalTerms_ = [auxString retain];
			}
        } else if ([lname isEqualToString:@"urlfaq"]) {
            
            NSString *auxString = [faqURL_ stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [faqURL_ release];
            faqURL_ = [auxString retain];
            
        }
        
        xmlAnalysisCurrentValue_ = serxeas_Nothing;
    }
    
}

/**
 * Sent by a parser object to provide its delegate with a string representing all or part of the characters of the current element.
 * Characters found are appended to the current element being analyzed
 *
 * @param parser A parser object
 * @param string A string representing the complete or partial textual content of the current element
 */
- (void) parser: (NSXMLParser*) parser foundCharacters: (NSString*) string {
    
    [super parser:parser foundCharacters:string];
    
	switch (xmlAnalysisCurrentValue_) {
		case axeas_AnalyzingLegalTerms:
			
			if (dateLegalTerms_ == nil) {
				dateLegalTerms_ = [[NSString alloc] initWithString:string];
			} else {
				NSString* auxString = [dateLegalTerms_ stringByAppendingString: string];
				[dateLegalTerms_ release];
				dateLegalTerms_ = [auxString retain];
			}
			
			break;
        case axeas_AnalyzingFAQURL: {
            
            if (faqURL_ == nil) {
                
                faqURL_ = [[NSString alloc] initWithString:string];
                
            } else {
                
                NSString *auxString = [faqURL_ stringByAppendingString:string];
                [faqURL_ release];
                faqURL_ = [auxString retain];
                
            }
            
            break;
            
        }
		default:
			break;
	}
}

@end