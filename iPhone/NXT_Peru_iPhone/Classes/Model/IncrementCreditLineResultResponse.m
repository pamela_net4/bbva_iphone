//
//  IncrementCreditLineResultResponse.m
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "IncrementCreditLineResultResponse.h"
#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     iclrreas_AnalyzingClientName = serxeas_ElementsCount, //!<Analyzing the IncrementCreditLineResultResponse clientName
     iclrreas_AnalyzingContract , //!<Analyzing the IncrementCreditLineResultResponse contract
     iclrreas_AnalyzingCardNumber , //!<Analyzing the IncrementCreditLineResultResponse cardNumber
     iclrreas_AnalyzingCardName , //!<Analyzing the IncrementCreditLineResultResponse cardName
     iclrreas_AnalyzingCurrency , //!<Analyzing the IncrementCreditLineResultResponse currency
     iclrreas_AnalyzingAmountCreditPrevious , //!<Analyzing the IncrementCreditLineResultResponse amountCreditPrevious
     iclrreas_AnalyzingAmountCredit , //!<Analyzing the IncrementCreditLineResultResponse amountCredit
     iclrreas_AnalyzingEmail , //!<Analyzing the IncrementCreditLineResultResponse email
     iclrreas_AnalyzingDate , //!<Analyzing the IncrementCreditLineResultResponse date
     iclrreas_AnalyzingHour , //!<Analyzing the IncrementCreditLineResultResponse hour
     iclrreas_AnalyzingConfirmationMessage , //!<Analyzing the IncrementCreditLineResultResponse confirmationMessage

} IncrementCreditLineResultResponseXMLElementAnalyzerState;

@implementation IncrementCreditLineResultResponse

#pragma mark -
#pragma mark Properties

@synthesize clientName = clientName_;
@synthesize contract = contract_;
@synthesize cardNumber = cardNumber_;
@synthesize cardName = cardName_;
@synthesize currency = currency_;
@synthesize amountCreditPrevious = amountCreditPrevious_;
@synthesize amountCredit = amountCredit_;
@synthesize email = email_;
@synthesize date = date_;
@synthesize hour = hour_;
@synthesize confirmationMessage = confirmationMessage_;
;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [clientName_ release];
    clientName_ = nil;

    [contract_ release];
    contract_ = nil;

    [cardNumber_ release];
    cardNumber_ = nil;

    [cardName_ release];
    cardName_ = nil;

    [currency_ release];
    currency_ = nil;

    [amountCreditPrevious_ release];
    amountCreditPrevious_ = nil;

    [amountCredit_ release];
    amountCredit_ = nil;

    [email_ release];
    email_ = nil;

    [date_ release];
    date_ = nil;

    [hour_ release];
    hour_ = nil;

    [confirmationMessage_ release];
    confirmationMessage_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if(0==1){
                   //delete fix
        }        else if ([lname isEqualToString: @"nombrecliente"]) {

            xmlAnalysisCurrentValue_ = iclrreas_AnalyzingClientName;


        }
        else if ([lname isEqualToString: @"contrato"]) {

            xmlAnalysisCurrentValue_ = iclrreas_AnalyzingContract;


        }
        else if ([lname isEqualToString: @"numtarjeta"]) {

            xmlAnalysisCurrentValue_ = iclrreas_AnalyzingCardNumber;


        }
        else if ([lname isEqualToString: @"nombretarjeta"]) {

            xmlAnalysisCurrentValue_ = iclrreas_AnalyzingCardName;


        }
        else if ([lname isEqualToString: @"divisa"]) {

            xmlAnalysisCurrentValue_ = iclrreas_AnalyzingCurrency;


        }
        else if ([lname isEqualToString: @"lineacreditoanterior"]) {

            xmlAnalysisCurrentValue_ = iclrreas_AnalyzingAmountCreditPrevious;


        }
        else if ([lname isEqualToString: @"lineacreditoactual"]) {

            xmlAnalysisCurrentValue_ = iclrreas_AnalyzingAmountCredit;


        }
        else if ([lname isEqualToString: @"email"]) {

            xmlAnalysisCurrentValue_ = iclrreas_AnalyzingEmail;


        }
        else if ([lname isEqualToString: @"fecha"]) {

            xmlAnalysisCurrentValue_ = iclrreas_AnalyzingDate;


        }
        else if ([lname isEqualToString: @"hora"]) {

            xmlAnalysisCurrentValue_ = iclrreas_AnalyzingHour;


        }
        else if ([lname isEqualToString: @"mensajeconfimacion"]) {

            xmlAnalysisCurrentValue_ = iclrreas_AnalyzingConfirmationMessage;


        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(0==1){
           //delete fix
        }        else if (xmlAnalysisCurrentValue_ == iclrreas_AnalyzingClientName) {

            [clientName_ release];
            clientName_ = nil;
            clientName_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclrreas_AnalyzingContract) {

            [contract_ release];
            contract_ = nil;
            contract_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclrreas_AnalyzingCardNumber) {

            [cardNumber_ release];
            cardNumber_ = nil;
            cardNumber_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclrreas_AnalyzingCardName) {

            [cardName_ release];
            cardName_ = nil;
            cardName_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclrreas_AnalyzingCurrency) {

            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclrreas_AnalyzingAmountCreditPrevious) {

            [amountCreditPrevious_ release];
            amountCreditPrevious_ = nil;
            amountCreditPrevious_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclrreas_AnalyzingAmountCredit) {

            [amountCredit_ release];
            amountCredit_ = nil;
            amountCredit_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclrreas_AnalyzingEmail) {

            [email_ release];
            email_ = nil;
            email_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclrreas_AnalyzingDate) {

            [date_ release];
            date_ = nil;
            date_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclrreas_AnalyzingHour) {

            [hour_ release];
            hour_ = nil;
            hour_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == iclrreas_AnalyzingConfirmationMessage) {

            [confirmationMessage_ release];
            confirmationMessage_ = nil;
            confirmationMessage_ = [elementString copyWithZone:self.zone];

        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

