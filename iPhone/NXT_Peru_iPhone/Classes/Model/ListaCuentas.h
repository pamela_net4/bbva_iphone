//
//  ListaCuentas.h
//  NXT_Peru_iPhone
//
//  Created by Jairo Garcia on 13/01/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatusEnabledResponse.h"
#import "CuentaBanco.h"

@interface ListaCuentas : StatusEnabledResponse {
@private
    NSMutableArray *listaCuentas_;
    CuentaBanco *cuentaBancoAux_;
}

@property (nonatomic, readonly, retain) NSArray *listaCuentas;

- (void) updateFrom:(ListaCuentas *) listaCuentasOrigen;
- (CuentaBanco *) cuentaAtPosition: (NSUInteger) aPosition;
- (void) removeData;
@end
