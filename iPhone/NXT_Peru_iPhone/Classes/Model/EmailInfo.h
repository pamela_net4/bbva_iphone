//
//  EmailInfo.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@interface EmailInfo : StatusEnabledResponse{
    @private

   /**
     * EmailInfo position
     */
    NSString *position_;


   /**
     * EmailInfo email
     */
    NSString *email_;


   /**
     * EmailInfo emailType
     */
    NSString *emailType_;




}

/**
 * Provides read-only access to the EmailInfo position
 */
@property (nonatomic, readonly, copy) NSString * position;


/**
 * Provides read-only access to the EmailInfo email
 */
@property (nonatomic, readonly, copy) NSString * email;


/**
 * Provides read-only access to the EmailInfo emailType
 */
@property (nonatomic, readonly, copy) NSString * emailType;




@end

