/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "AccountTransaction.h"
#import "Tools.h"


/**
 * Enumerates the analysis states
 */
typedef enum {
    
    atxeas_AnalyzingOperationDate = serxeas_ElementsCount, //!<Analyzing the transaction operation date
	atxeas_AnalyzingValueDate, //!<Analyzing the transaction value date
	atxeas_AnalyzingDescription, //!<Analyzing the transaction description
	atxeas_AnalyzingCurrency, //!<Analyzing the currency
	atxeas_AnalyzingAmount, //!<Analyzing the transaction amount
	atxeas_AnalyzingITF, //!<Analyzing the ITF
	atxeas_AnalyzingNumber, //!<Analyzing the transaction number
	atxeas_AnalyzingSubject //!<Analyzing the subject
    
} AccountTransactionXMLElementAnalyzerState;


#pragma mark -

/**
 * AccountTransaction private category
 */
@interface AccountTransaction(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearAccountTransactionData;

@end


#pragma mark -

@implementation AccountTransaction

#pragma mark -
#pragma mark Properties

@synthesize operationDateString = operationDateString_;
@dynamic operationDate;
@synthesize valueDateString = valueDateString_;
@synthesize valueDate = valueDate_;
@synthesize description = description_;
@synthesize currency = currency_;
@synthesize amountString = amountString_;
@dynamic amount;
@synthesize ITF = ITF_;
@synthesize number = number_;
@synthesize subject = subject_;
@synthesize detailDownloaded = detailDownloaded_;
@synthesize deatilDescription = deatilDescription_;
@synthesize operationHour = operationHour_;
@synthesize accountingDateString = accountingDateString_;
@synthesize accountingDate = accountingDate_;
@synthesize checkNumber = checkNumber_;
@synthesize type = type_;
@synthesize center = center_;
@synthesize literal = literal_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {
    
    [self clearAccountTransactionData];
	
	[super dealloc];
    
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initialized. Initializes an AccountTransaction instance
 *
 * @return The initialized AccountTransaction instance
 */
- (id)init {
    
    if (self = [super init]) {
        
		self.notificationToPost = kNotificationAccountTransactionUpdated;
        
    }
    
    return self;
    
}

#pragma mark -
#pragma mark Information management

/*
 * Updates this instance with the given account transaction
 */
- (void)updateFrom:(AccountTransaction *)anAccountTransaction {
    
    NSZone *zone = self.zone;
    
    [operationDateString_ release];
    operationDateString_ = nil;
    operationDateString_ = [anAccountTransaction.operationDateString copyWithZone:zone];
    
    [operationDate_ release];
    operationDate_ = nil;
    
    [valueDateString_ release];
    valueDateString_ = nil;
    valueDateString_ = [anAccountTransaction.valueDateString copyWithZone:zone];
    
    [valueDate_ release];
    valueDate_ = nil;
    
    [description_ release];
    description_ = nil;
    description_ = [anAccountTransaction.description copyWithZone:zone];
    
    [currency_ release];
    currency_ = nil;
    currency_ = [anAccountTransaction.currency copyWithZone:zone];
    
    [amountString_ release];
    amountString_ = nil;
    amountString_ = [anAccountTransaction.amountString copyWithZone:zone];
    
    [amount_ release];
    amount_ = nil;
    
    [ITF_ release];
    ITF_ = nil;
    ITF_ = [anAccountTransaction.ITF copyWithZone:zone];
    
    [number_ release];
    number_ = nil;
    number_ = [anAccountTransaction.number copyWithZone:zone];
    
    [subject_ release];
    subject_ = nil;
    subject_ = [anAccountTransaction.subject copyWithZone:zone];
    
    detailDownloaded_ = anAccountTransaction.detailDownloaded;
    
    [self updateFromStatusEnabledResponse:anAccountTransaction];
	
	self.informationUpdated = anAccountTransaction.informationUpdated;
    
}

#pragma mark -
#pragma mark Comparing selectors

/*
 * Compares the account transaction with another to produce an ordered list where older transactions are the last items
 */
- (NSComparisonResult)compareForDescendingDateOrdering:(AccountTransaction *)anAccountTransaction {
    
    NSComparisonResult result = NSOrderedAscending;
    
    if (anAccountTransaction != nil) {
        
        NSDate *otherDate = anAccountTransaction.operationDate;
        NSDate *date = self.operationDate;
        
        result = [date compare:otherDate];
        
        if (result == NSOrderedAscending) {
            
            result = NSOrderedDescending;
            
        } else if (result == NSOrderedDescending) {
            
            result = NSOrderedAscending;
            
        }
        
    }
    
    return result;
    
}

#pragma mark -
#pragma mark NSXMLParser delegate selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearAccountTransactionData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"fechaoperacion"]) {
            
            xmlAnalysisCurrentValue_ = atxeas_AnalyzingOperationDate;
            
        } else if ([lname isEqualToString: @"fechavalor"]) {
            
            xmlAnalysisCurrentValue_ = atxeas_AnalyzingValueDate;
            
        } else if ([lname isEqualToString: @"descripcion"]) {
            
            xmlAnalysisCurrentValue_ = atxeas_AnalyzingDescription;
            
        } else if ([lname isEqualToString: @"divisa"]) {
            
            xmlAnalysisCurrentValue_ = atxeas_AnalyzingCurrency;
            
        } else if ([lname isEqualToString: @"importe"]) {
            
            xmlAnalysisCurrentValue_ = atxeas_AnalyzingAmount;
            
        } else if ([lname isEqualToString: @"itf"]) {
            
            xmlAnalysisCurrentValue_ = atxeas_AnalyzingITF;
            
        } else if ([lname isEqualToString: @"nummovimiento"]) {
            
            xmlAnalysisCurrentValue_ = atxeas_AnalyzingNumber;
            
        } else if ([lname isEqualToString: @"asunto"]) {
            
            xmlAnalysisCurrentValue_ = atxeas_AnalyzingSubject;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == atxeas_AnalyzingOperationDate) {
            
            [operationDateString_ release];
            operationDateString_ = nil;
            operationDateString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == atxeas_AnalyzingValueDate) {
            
            [valueDateString_ release];
            valueDateString_ = nil;
            valueDateString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == atxeas_AnalyzingDescription) {
            
            [description_ release];
            description_ = nil;
            description_ = [[elementString capitalizedString] copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == atxeas_AnalyzingCurrency) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == atxeas_AnalyzingAmount) {
            
            [amountString_ release];
            amountString_ = nil;
            amountString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == atxeas_AnalyzingITF) {
            
            [ITF_ release];
            ITF_ = nil;
            ITF_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == atxeas_AnalyzingNumber) {
            
            [number_ release];
            number_ = nil;
            number_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == atxeas_AnalyzingSubject) {
            
            [subject_ release];
            subject_ = nil;
            subject_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the operation date
 *
 * @return The operation date
 */
- (NSDate *)operationDate {

    NSDate *result = operationDate_;
    
    if (result == nil) {
        
        if ([operationDateString_ length] > 0) {
            
            operationDate_ = [[Tools dateFromServerString:operationDateString_] retain];
            result = operationDate_;
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the value date
 *
 * @return The value date
 */
- (NSDate *)valueDate {
    
    NSDate *result = valueDate_;
    
    if (result == nil) {
        
        if ([valueDateString_ length] > 0) {
            
            valueDate_ = [[Tools dateFromServerString:valueDateString_] retain];
            result = valueDate_;
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the account transaction amount
 *
 * @return The account transaction amount
 */
- (NSDecimalNumber *)amount {

    NSDecimalNumber *result = amount_;
    
    if (result == nil) {
        
        if ([amountString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:amountString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                amount_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation AccountTransaction(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearAccountTransactionData {
    
    [operationDateString_ release];
    operationDateString_ = nil;
    
    [operationDate_ release];
    operationDate_ = nil;
    
    [valueDateString_ release];
    valueDateString_ = nil;
    
    [valueDate_ release];
    valueDate_ = nil;
    
    [description_ release];
    description_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [amountString_ release];
    amountString_ = nil;
    
    [amount_ release];
    amount_ = nil;
    
    [ITF_ release];
    ITF_ = nil;
    
    [number_ release];
    number_ = nil;
    
    [subject_ release];
    subject_ = nil;
    
    detailDownloaded_ = NO;
    
    [deatilDescription_ release];
    deatilDescription_ = nil;
    
    [operationHour_ release];
    operationHour_ = nil;
    
    [accountingDateString_ release];
    accountingDateString_ = nil;
    
    [accountingDate_ release];
    accountingDate_ = nil;
    
    [checkNumber_ release];
    checkNumber_ = nil;
    
    [type_ release];
    type_ = nil;
    
    [center_ release];
    center_ = nil;
    
    [literal_ release];
    literal_ = nil;
    
}

@end
