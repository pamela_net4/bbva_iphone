/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "AccountFastLoanList.h"
#import "AccountFastLoan.h"


#pragma mark -

/**
 * AccountFastLoanList private category
 */
@interface AccountFastLoanList (private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearAccountFastLoanListData;

@end


#pragma mark -

@implementation AccountFastLoanList

@dynamic accountfastloanList;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [self clearAccountFastLoanListData];

	[accountfastloanList_ release];
	accountfastloanList_ = nil;

	[super dealloc];

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a AccountFastLoanList instance creating the associated array
 *
 * @return The initialized AccountFastLoanList instance
 */
- (id)init {

    if (self = [super init]) {

        accountfastloanList_ = [[NSMutableArray alloc] init];

    }

    return self;

}

#pragma mark -
#pragma mark Information management

/*
 * Updates the AccountFastLoan list from another AccountFastLoanList instance
 */
- (void)updateFrom:(AccountFastLoanList *)aAccountFastLoanList {

    NSArray *otherAccountFastLoanList = aAccountFastLoanList.accountfastloanList;

	[self clearAccountFastLoanListData];

    for (AccountFastLoan *accountfastloan in otherAccountFastLoanList) {

        [accountfastloanList_ addObject:accountfastloan];

    }

    [self updateFromStatusEnabledResponse:aAccountFastLoanList];

	self.informationUpdated = aAccountFastLoanList.informationUpdated;

}

/*
 * Remove object data
 */
- (void)removeData {

    [self clearAccountFastLoanListData];

}

#pragma mark -
#pragma mark Getters and setters

/**
 * Provides read access to the number of AccountFastLoan stored
 *
 * @return The count
 */
- (NSUInteger) accountfastloanCount {
	return [accountfastloanList_ count];
}

/*
 * Returns the AccountFastLoan located at the given position, or nil if position is not valid
 */
- (AccountFastLoan*) accountfastloanAtPosition: (NSUInteger) aPosition {
	AccountFastLoan* result = nil;

	if (aPosition < [accountfastloanList_ count]) {
		result = [accountfastloanList_ objectAtIndex: aPosition];
	}

	return result;
}

/*
 * Returns the AccountFastLoan position inside the list
 */
- (NSUInteger) accountfastloanPosition: (AccountFastLoan*) aAccountFastLoan {

	return [accountfastloanList_ indexOfObject:aAccountFastLoan];

}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {

    [super parserDidStartDocument:parser];

	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearAccountFastLoanListData];

}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {

    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];

	if (auxAccountFastLoan_ != nil) {

		[accountfastloanList_ addObject:auxAccountFastLoan_];
		[auxAccountFastLoan_ release];
		auxAccountFastLoan_ = nil;

	}

	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        NSString* lname = [elementName lowercaseString];

        if ([lname isEqualToString: @"e"]) {

            auxAccountFastLoan_ = [[AccountFastLoan alloc] init];
            [auxAccountFastLoan_ setParentParseableObject:self];
            auxAccountFastLoan_.openingTag = lname;
            [parser setDelegate:auxAccountFastLoan_];
            [auxAccountFastLoan_ parserDidStartDocument: parser];

        } else {

            xmlAnalysisCurrentValue_ = serxeas_Nothing;

        }

    }

}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {

    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];

    if (auxAccountFastLoan_ != nil) {

		[accountfastloanList_ addObject:auxAccountFastLoan_];
		[auxAccountFastLoan_ release];
		auxAccountFastLoan_ = nil;

	}

    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        informationUpdated_ = soie_InfoAvailable;

    }

    xmlAnalysisCurrentValue_ = serxeas_Nothing;

}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the accountfastloan list array
 *
 * @return The accountfastloan list array
 */
- (NSArray *)accountfastloanList {

    return [NSArray arrayWithArray:accountfastloanList_];

}

/**
 * Returns the AccountFastLoan with an AccountFastLoan id, or nil if not valid
 *//*
- (AccountFastLoan *)accountfastloanFromAccountFastLoanId:(NSString *)aAccountFastLoanId {

	AccountFastLoan *accountfastloan = nil;

	if (aAccountFastLoanId != nil) {
		AccountFastLoan *target;
		NSUInteger size = [accountfastloanList_ count];
		BOOL found = NO;
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
			target = [accountfastloanList_ objectAtIndex:i];
			if ([aAccountFastLoanId isEqualToString:target.id]) {
				accountfastloan = target;
				found = YES;
			}
		}
	}

	return accountfastloan;

}*/
@end


#pragma mark -

@implementation AccountFastLoanList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearAccountFastLoanListData {

    [accountfastloanList_ removeAllObjects];

	informationUpdated_ = soie_NoInfo;

}

@end

