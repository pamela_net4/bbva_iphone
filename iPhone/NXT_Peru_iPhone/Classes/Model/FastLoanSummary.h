//
//  FastLoanSummary.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"


@interface FastLoanSummary : StatusEnabledResponse{
    @private

   /**
     * FastLoanSummary customer
     */
    NSString *customer_;


   /**
     * FastLoanSummary account
     */
    NSString *account_;


   /**
     * FastLoanSummary currencyAccount
     */
    NSString *currencyAccount_;


   /**
     * FastLoanSummary dayOfPay
     */
    NSString *dayOfPay_;


   /**
     * FastLoanSummary numberOperation
     */
    NSString *numberOperation_;


   /**
     * FastLoanSummary quotaType
     */
    NSString *quotaType_;


   /**
     * FastLoanSummary contract
     */
    NSString *contract_;


   /**
     * FastLoanSummary tea
     */
    NSString *tea_;


   /**
     * FastLoanSummary tcea
     */
    NSString *tcea_;


   /**
     * FastLoanSummary currencyAmount
     */
    NSString *currencyAmount_;


   /**
     * FastLoanSummary amount
     */
    NSString *amount_;


   /**
     * FastLoanSummary term
     */
    NSString *term_;


   /**
     * FastLoanSummary currencyQuota
     */
    NSString *currencyQuota_;


   /**
     * FastLoanSummary amountQuota
     */
    NSString *amountQuota_;


   /**
     * FastLoanSummary messageNotification
     */
    NSString *messageNotification_;


   /**
     * FastLoanSummary date
     */
    NSString *date_;


   /**
     * FastLoanSummary hour
     */
    NSString *hour_;




}

/**
 * Provides read-only access to the FastLoanSummary customer
 */
@property (nonatomic, readonly, copy) NSString * customer;


/**
 * Provides read-only access to the FastLoanSummary account
 */
@property (nonatomic, readonly, copy) NSString * account;


/**
 * Provides read-only access to the FastLoanSummary currencyAccount
 */
@property (nonatomic, readonly, copy) NSString * currencyAccount;


/**
 * Provides read-only access to the FastLoanSummary dayOfPay
 */
@property (nonatomic, readonly, copy) NSString * dayOfPay;


/**
 * Provides read-only access to the FastLoanSummary numberOperation
 */
@property (nonatomic, readonly, copy) NSString * numberOperation;


/**
 * Provides read-only access to the FastLoanSummary quotaType
 */
@property (nonatomic, readonly, copy) NSString * quotaType;


/**
 * Provides read-only access to the FastLoanSummary contract
 */
@property (nonatomic, readonly, copy) NSString * contract;


/**
 * Provides read-only access to the FastLoanSummary tea
 */
@property (nonatomic, readonly, copy) NSString * tea;


/**
 * Provides read-only access to the FastLoanSummary tcea
 */
@property (nonatomic, readonly, copy) NSString * tcea;


/**
 * Provides read-only access to the FastLoanSummary currencyAmount
 */
@property (nonatomic, readonly, copy) NSString * currencyAmount;


/**
 * Provides read-only access to the FastLoanSummary amount
 */
@property (nonatomic, readonly, copy) NSString * amount;


/**
 * Provides read-only access to the FastLoanSummary term
 */
@property (nonatomic, readonly, copy) NSString * term;


/**
 * Provides read-only access to the FastLoanSummary currencyQuota
 */
@property (nonatomic, readonly, copy) NSString * currencyQuota;


/**
 * Provides read-only access to the FastLoanSummary amountQuota
 */
@property (nonatomic, readonly, copy) NSString * amountQuota;


/**
 * Provides read-only access to the FastLoanSummary messageNotification
 */
@property (nonatomic, readonly, copy) NSString * messageNotification;


/**
 * Provides read-only access to the FastLoanSummary date
 */
@property (nonatomic, readonly, copy) NSString * date;


/**
 * Provides read-only access to the FastLoanSummary hour
 */
@property (nonatomic, readonly, copy) NSString * hour;




@end

