/*
 * Copyright (c) 2011 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


//Forward declarations
@class Location;


/**
 * Contains a route information, defined as an origin location and a destintion location
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface Route : NSObject {

@private
    
    /**
     * Route origin location
     */
    Location *originLocation_;
    
    /**
     * Route destination location
     */
    Location *destinationLocation_;
    
}

/**
 * Provides read-only access to the route origin location
 */
@property (nonatomic, readonly, retain) Location *originLocation;

/**
 * Provides read-only access to the route destination location
 */
@property (nonatomic, readonly, retain) Location *destinationLocation;


/**
 * Designated initializer. Initializes a Route instance with the provided origin and destination locations
 *
 * @param anOriginLocation The route origin location to store
 * @param aDestinationLocation The route destination location to store
 * @return The initialized Route instance
 */
- (id)initWithOriginLocation:(Location *)anOriginLocation
         destinationLocation:(Location *)aDestinationLocation;


/**
 * Compares one Route with another, using the origin and destination locations as element to compare. Elements are ordered alphabetically, first
 * by origin location, and the by destination location
 *
 * @param aRoute The Route instance to compare with
 * @return The result of comparing the origin locations, and if equal, the result of comparing the destination locations
 */
- (NSComparisonResult)compareAlphabetically:(Route *)aRoute;

/**
 * Compares to another Route instance to check whether both are equal. Two routes are considered to be equal if their origin
 * and destination locations are equal
 *
 * @param aRoute The Route instance to compare with
 * @return YES if both Routes instances are considered to be equal, NO otherwise
 */
- (BOOL)isEqualToRoute:(Route *)aRoute;

@end
