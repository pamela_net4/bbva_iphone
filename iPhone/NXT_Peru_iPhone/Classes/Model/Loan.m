/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "Loan.h"

#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
    lxeas_AnalyzingNumber = serxeas_ElementsCount, //!<Analyzing the loan number
	lxeas_AnalyzingType, //!<Analyzing the loan type
    lxeas_AnalyzingInitialAmount, //!<Analyzing the loan initial amount
    lxeas_AnalyzingPendingAmount, //!<Analyzing the loan pending amount
    lxeas_AnalyzingCurrency, //!<Analyzing the loan currency
    lxeas_AnalyzingPastDueDebt, //!<Analyzing the loan past due debt flag
    lxeas_AnalyzingSubject, //!<Analyzing the loan subject
    lxeas_AnalyzingAmountInCountryCurrency //!<Analyzing the amount in country currency

} LoanXMLElementAnalyzerState;


#pragma mark -

/**
 * Loan private category
 */
@interface Loan(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearLoanData;

@end


#pragma mark -

@implementation Loan

#pragma mark -
#pragma mark Properties

@synthesize number = number_;
@synthesize type = type_;
@synthesize initialAmountString = initialAmountString_;
@dynamic initialAmount;
@synthesize pendingAmountString = pendingAmountString_;
@dynamic pendingAmount;
@synthesize currency = currency_;
@synthesize pastDueDebtString = pastDueDebtString_;
@synthesize pastDueDebt = pastDueDebt_;
@synthesize subject = subject_;
@synthesize amountInCountryCurrencyString = amountInCountryCurrencyString_;
@dynamic amountInCountryCurrency;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [self clearLoanData];
	
	[super dealloc];
    
}


#pragma mark -
#pragma mark Title texts management

/*
 * Returns the loan list display name. It can be used whenever a loan name must be displayed inside a list
 */
- (NSString *)loanListName {
    
    return [NSString stringWithFormat:@"%@ %@", type_, [Tools obfuscateAccountNumber:number_]];
    
}

/*
 * Returns the obfuscated loan number
 */
- (NSString *)obfuscatedLoanNumber {
    
    return [Tools obfuscateAccountNumber:number_];
    
}


#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearLoanData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"numero"]) {
            
            xmlAnalysisCurrentValue_ = lxeas_AnalyzingNumber;
            
        } else if ([lname isEqualToString: @"tipo"]) {
            
            xmlAnalysisCurrentValue_ = lxeas_AnalyzingType;
            
        } else if ([lname isEqualToString: @"importeoriginal"]) {
            
            xmlAnalysisCurrentValue_ = lxeas_AnalyzingInitialAmount;
            
        } else if ([lname isEqualToString: @"capitalpendiente"]) {
            
            xmlAnalysisCurrentValue_ = lxeas_AnalyzingPendingAmount;
            
        } else if ([lname isEqualToString: @"moneda"]) {
            
            xmlAnalysisCurrentValue_ = lxeas_AnalyzingCurrency;
            
        } else if ([lname isEqualToString: @"deudavencida"]) {
            
            xmlAnalysisCurrentValue_ = lxeas_AnalyzingPastDueDebt;
            
        } else if ([lname isEqualToString: @"asunto"]) {
            
            xmlAnalysisCurrentValue_ = lxeas_AnalyzingSubject;
            
        } else if ([lname isEqualToString:@"importeconvertido"]) {
            
            xmlAnalysisCurrentValue_ = lxeas_AnalyzingAmountInCountryCurrency;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == lxeas_AnalyzingNumber) {
            
            [number_ release];
            number_ = nil;
            number_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == lxeas_AnalyzingType) {
            
            [type_ release];
            type_ = nil;
            type_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == lxeas_AnalyzingInitialAmount) {
            
            [initialAmountString_ release];
            initialAmountString_ = nil;
            initialAmountString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == lxeas_AnalyzingPendingAmount) {
            
            [pendingAmountString_ release];
            pendingAmountString_ = nil;
            pendingAmountString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == lxeas_AnalyzingCurrency) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == lxeas_AnalyzingPastDueDebt) {
            
            [pastDueDebtString_ release];
            pastDueDebtString_ = nil;
            pastDueDebtString_ = [elementString copyWithZone:self.zone];
            
            NSString *auxString = [pastDueDebtString_ lowercaseString];
            
            if ([auxString isEqualToString:@"si"]) {
                
                pastDueDebt_ = YES;
                
            } else {
                
                pastDueDebt_ = NO;
                
            }
            
        } else if (xmlAnalysisCurrentValue_ == lxeas_AnalyzingSubject) {
            
            [subject_ release];
            subject_ = nil;
            subject_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == lxeas_AnalyzingAmountInCountryCurrency) {
            
            [amountInCountryCurrencyString_ release];
            amountInCountryCurrencyString_ = nil;
            amountInCountryCurrencyString_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;

}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the initial amount, calculating it from the string representation if necessary
 *
 * @return The initial amount
 */
- (NSDecimalNumber *)initialAmount {
    
    NSDecimalNumber *result = initialAmount_;
    
    if (result == nil) {
        
        if ([initialAmountString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:initialAmountString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                initialAmount_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the pending amount, calculating it from the string representation if necessary
 *
 * @return The pending amount
 */
- (NSDecimalNumber *)pendingAmount {
    
    NSDecimalNumber *result = pendingAmount_;
    
    if (result == nil) {
        
        if ([pendingAmountString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:pendingAmountString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                pendingAmount_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the amount converted into the country currency, calculating ir from string representation if needed
 *
 * @return The amount converted into country curency
 */
- (NSDecimalNumber *)amountInCountryCurrency {
    
    NSDecimalNumber *result = amountInCountryCurrency_;
    
    if (result == nil) {
        
        if ([amountInCountryCurrencyString_ length] == 0) {
            
            if ([pendingAmountString_ length] > 0) {
                
                result = [Tools decimalFromServerString:pendingAmountString_];
                
            } else {
                
                result = [NSDecimalNumber notANumber];

            }
            
        } else {
            
            result = [Tools decimalFromServerString:amountInCountryCurrencyString_];
            
        }
        
        if (![result compare:[NSDecimalNumber notANumber]]) {
            
            amountInCountryCurrency_ = [result retain];
            
        }
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation Loan(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearLoanData {
    
    [number_ release];
    number_ = nil;
    
    [type_ release];
    type_ = nil;
    
    [initialAmountString_ release];
    initialAmountString_ = nil;
    
    [initialAmount_ release];
    initialAmount_ = nil;
    
    [pendingAmountString_ release];
    pendingAmountString_ = nil;
    
    [pendingAmount_ release];
    pendingAmount_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [pastDueDebtString_ release];
    pastDueDebtString_ = nil;
    
    pastDueDebt_ = NO;
    
    [subject_ release];
    subject_ = nil;
	
    [amountInCountryCurrencyString_ release];
    amountInCountryCurrencyString_ = nil;
    
    [amountInCountryCurrency_ release];
    amountInCountryCurrency_ = nil;
}

@end
