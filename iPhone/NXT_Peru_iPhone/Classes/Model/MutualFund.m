/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "MutualFund.h"

#import "Tools.h"


/**
 * Enumerates the analysis states
 */
typedef enum {
    
    mfxeas_AnalyzingNumber = serxeas_ElementsCount, //!<Analyzing the mutual fund number
	mfxeas_AnalyzingType, //!<Analyzing the mutual fund type
    mfxeas_AnalyzingCurrency, //!<Analyzing the mutual fund currency
    mfxeas_AnalyzingValue, //!<Analyzing the mutual fund value
    mfxeas_AnalyzingShareValue, //!<Analyzing the mutual fund share value
    mfxeas_AnalyzingTotalShare, //!<Analyzing the mutual fund total share
    mfxeas_AnalyzingSubject, //!<Analyzing the mutual fund subject
    mfxeas_AnalyzingValueInCountryCurrency //!<Analyzing the mutual fund value in country currency

} MutualFundXMLElementAnalyzerState;


#pragma mark -

/**
 * MutualFund private category
 */
@interface MutualFund(private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearMutualFundData;

@end


#pragma mark -

@implementation MutualFund

#pragma mark -
#pragma mark Properties

@synthesize number = number_;
@synthesize type = type_;
@synthesize currency = currency_;
@synthesize valueString = valueString_;
@dynamic value;
@synthesize shareValueString = shareValueString_;
@dynamic shareValue;
@synthesize totalShareString = totalShareString_;
@dynamic totalShare;
@synthesize subject = subject_;
@synthesize valueInCountryCurrencyString = valueInCountryCurrencyString_;
@dynamic valueInCountryCurrency;

#pragma mark -
#pragma mark Memory management

/**
 * Releases used memory
 */
- (void)dealloc {

    [self clearMutualFundData];
    
    [super dealloc];
    
}

#pragma mark -
#pragma mark Titles text management

/*
 * Returns the mutual fund list display name. It can be used whenever a fund name must be displayed inside a list
 */
- (NSString *)mutualFundListName {
    
    return [NSString stringWithFormat:@"%@ %@", type_, [Tools obfuscateAccountNumber:number_]];
    
}

/*
 * Returns the obfuscated mutual fund number
 */
- (NSString *)obfuscatedMutualFundNumber {

    return [Tools obfuscateAccountNumber:number_];
    
}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearMutualFundData];
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString:@"numero"]) {
            
            xmlAnalysisCurrentValue_ = mfxeas_AnalyzingNumber;
            
        } else if ([lname isEqualToString:@"tipo"]) {
            
            xmlAnalysisCurrentValue_ = mfxeas_AnalyzingType;
            
        } else if ([lname isEqualToString:@"moneda"]) {
            
            xmlAnalysisCurrentValue_ = mfxeas_AnalyzingCurrency;
            
        } else if ([lname isEqualToString:@"disponible"]) {
            
            xmlAnalysisCurrentValue_ = mfxeas_AnalyzingValue;
            
        } else if ([lname isEqualToString:@"valorcuota"]) {
            
            xmlAnalysisCurrentValue_ = mfxeas_AnalyzingShareValue;
            
        } else if ([lname isEqualToString:@"totalcuotas"]) {
            
            xmlAnalysisCurrentValue_ = mfxeas_AnalyzingTotalShare;
            
        } else if ([lname isEqualToString:@"asunto"]) {
            
            xmlAnalysisCurrentValue_ = mfxeas_AnalyzingSubject;
            
        } else if ([lname isEqualToString:@"importeconvertido"]) {
            
            xmlAnalysisCurrentValue_ = mfxeas_AnalyzingValueInCountryCurrency;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == mfxeas_AnalyzingNumber) {
            
            [number_ release];
            number_ = nil;
            number_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == mfxeas_AnalyzingType) {
            
            [type_ release];
            type_ = nil;
            type_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == mfxeas_AnalyzingCurrency) {
            
            [currency_ release];
            currency_ = nil;
            currency_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == mfxeas_AnalyzingValue) {
            
            [valueString_ release];
            valueString_ = nil;
            valueString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == mfxeas_AnalyzingShareValue) {
            
            [shareValueString_ release];
            shareValueString_ = nil;
            shareValueString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == mfxeas_AnalyzingTotalShare) {
            
            [totalShareString_ release];
            totalShareString_ = nil;
            totalShareString_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == mfxeas_AnalyzingSubject) {
            
            [subject_ release];
            subject_ = nil;
            subject_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == mfxeas_AnalyzingValueInCountryCurrency) {
            
            [valueInCountryCurrencyString_ release];
            valueInCountryCurrencyString_ = nil;
            valueInCountryCurrencyString_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the mutual fund value, calculating it from the string representation if necessary
 *
 * @return The mutual fund value
 */
- (NSDecimalNumber *)value {
    
    NSDecimalNumber *result = value_;
    
    if (result == nil) {
        
        if ([valueString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:valueString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                value_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the mutual fund share value, calculating it from the string representation if necessary
 *
 * @return The mutual fund share value
 */
- (NSDecimalNumber *)shareValue {
    
    NSDecimalNumber *result = shareValue_;
    
    if (result == nil) {
        
        if ([shareValueString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:shareValueString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                shareValue_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the mutual fund total share, calculating it from the string representation if necessary
 *
 * @return The mutual fund total share
 */
- (NSDecimalNumber *)totalShare {
    
    NSDecimalNumber *result = totalShare_;
    
    if (result == nil) {
        
        if ([totalShareString_ length] == 0) {
            
            result = [NSDecimalNumber notANumber];
            
        } else {
            
            result = [Tools decimalFromServerString:totalShareString_];
            
            if (![result compare:[NSDecimalNumber notANumber]]) {
                
                totalShare_ = [result retain];
                
            }
            
        }
        
    }
    
    return result;
    
}

/*
 * Returns the mutual fund value converted into the country currency, calculating ir from string representation if needed
 *
 * @return The mutual fund value converted into country curency
 */
- (NSDecimalNumber *)valueInCountryCurrency {
    
    NSDecimalNumber *result = valueInCountryCurrency_;
    
    if (result == nil) {
        
        if ([valueInCountryCurrencyString_ length] == 0) {
            
            if ([valueString_ length] > 0) {
            
                result = [Tools decimalFromServerString:valueString_];
                
            } else {
                
                result = [NSDecimalNumber notANumber];

            }
            
        } else {
            
            result = [Tools decimalFromServerString:valueInCountryCurrencyString_];
            
        }
        
        if (![result compare:[NSDecimalNumber notANumber]]) {
            
            valueInCountryCurrency_ = [result retain];
            
        }
        
    }
    
    return result;
    
}

@end


#pragma mark -

@implementation MutualFund(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearMutualFundData {
    
    [number_ release];
    number_ = nil;
    
    [type_ release];
    type_ = nil;
    
    [currency_ release];
    currency_ = nil;
    
    [valueString_ release];
    valueString_ = nil;
    
    [value_ release];
    value_ = nil;
    
    [shareValueString_ release];
    shareValueString_ = nil;
    
    [shareValue_ release];
    shareValue_ = nil;
    
    [totalShareString_ release];
    totalShareString_ = nil;
    
    [totalShare_ release];
    totalShare_ = nil;
    
    [subject_ release];
    subject_ = nil;
    
    [valueInCountryCurrencyString_ release];
    valueInCountryCurrencyString_ = nil;
    
    [valueInCountryCurrency_ release];
    valueInCountryCurrency_ = nil;
}

@end

