/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


//Forward declarations
@class AccountTransactionList;
@class AccountTransactionsAdditionalInformation;


/**
 * Account information. It contains an account number, an account alias, balance in euros, balance in other currency,
 * and the other currency
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface BankAccount : StatusEnabledResponse {
    
@private
    
    /**
     * Account number
     */
    NSString *number_;
    
    /**
     * Account type
     */
    NSString *accountType_;
    
    /**
     * Account balance string
     */
    NSString *currentBalanceString_;
    
    /**
     * Account balance
     */
    NSDecimalNumber *currentBalance_;
    
    /**
     * Available balance string
     */
    NSString *availableBalanceString_;
    
    /**
     * Available balance
     */
    NSDecimalNumber *availableBalance_;
    
    /**
     * Currency symbol
     */
    NSString *currency_;
    
    /**
     * Account bank
     */
    NSString *bank_;
    
    /**
     * Account branch
     */
    NSString *branch_;
    
    /**
     * Account control digit
     */
    NSString *controlDigit_;
    
    /**
     * Branch account
     */
    NSString *branchAccount_;
    
    /**
     * Type
     */
    NSString *type_;
    
    /**
     * Account state
     */
    NSString *state_;
    
    /**
     * Permission
     */
    NSString *permission_;
    
    /**
     * Subject
     */
    NSString *subject_;
    
    /**
     * Balance converted into country currency string
     */
    NSString *balanceInCountryCurrencyString_;
    
    /**
     * Balance converted into country currency
     */
    NSDecimalNumber *balanceInCountryCurrency_;
    
    
    NSString *paramCuenta_;
    
    /**
     * Account is a term account flag. YES when the account is a term account, NO otherwise
     */
    BOOL isTermAccount_;
    
    /**
     * Account transactions list
     */
    AccountTransactionList *accountTransactionsList_;
    
    /**
     * Account transactions additional information for the last performed transactions download
     */
    AccountTransactionsAdditionalInformation *transactionsAdditionalInformation_;
    

}

/**
 * Provides read-only access to the account number
 */
@property (nonatomic, readonly, copy) NSString *number;

/**
 * Provides read-only access to the account type
 */
@property (nonatomic, readonly, copy) NSString *accountType;

/**
 * Provides read-only access to the account balance string
 */
@property (nonatomic, readonly, copy) NSString *currentBalanceString;

/**
 * Provides read-only access to the aaccount balance
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *currentBalance;

/**
 * Provides read-only access to the available balance string
 */
@property (nonatomic, readonly, copy) NSString *availableBalanceString;

/**
 * Provides read-only access to the available balance
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *availableBalance;

/**
 * Provides read-only access to the currency symbol
 */
@property (nonatomic, readonly, copy) NSString *currency;

/**
 * Provides read-only access to the account bank
 */
@property (nonatomic, readonly, copy) NSString *bank;

/**
 * Provides read-only access to the account branch
 */
@property (nonatomic, readonly, copy) NSString *branch;

/**
 * Provides read-only access to the account control digit
 */
@property (nonatomic, readonly, copy) NSString *controlDigit;

/**
 * Provides read-only access to the branch account
 */
@property (nonatomic, readonly, copy) NSString *branchAccount;

/**
 * Provides read-only access to the type
 */
@property (nonatomic, readonly, copy) NSString *type;

/**
 * Provides read-only access to the account state
 */
@property (nonatomic, readonly, copy) NSString *state;

/**
 * Provides read-only access to the permission
 */
@property (nonatomic, readonly, copy) NSString *permission;

/**
 * Provides read-only access to the subject
 */
@property (nonatomic, readonly, copy) NSString *subject;


@property (nonatomic, readonly, copy) NSString *paramCuenta;

/**
 * Provides read-only access to the balance converted into cuntry currency string
 */
@property (nonatomic, readonly, copy) NSString *balanceInCountryCurrencyString;

/**
 * Provides read-only access to the balance converted into country currency
 */
@property (nonatomic, readonly, copy) NSDecimalNumber *balanceInCountryCurrency;

/**
 * Provides read-only access to the account is a term account flag
 */
@property (nonatomic, readonly, assign) BOOL isTermAccount;

/**
 * Provides read-only access to the movements list
 */
@property (nonatomic, readonly, retain) AccountTransactionList *accountTransactionsList;

/**
 * Provides read-only access to the account transactions additional information for the last performed transactions download
 */
@property (nonatomic, readonly, retain) AccountTransactionsAdditionalInformation *transactionsAdditionalInformation;

/**
 * Provides read-only access to the account identification plus the description
 */
@property (nonatomic, readonly, copy) NSString *accountIdAndDescription;

/**
 * Returns the bank account list display name. It can be used whenever an account name must be displayed inside a list
 *
 * @return The bank account list display name
 */
- (NSString *)bankAccountListName;

/**
 * Returns the obfuscated account number
 *
 * @return The obfuscated account number
 */
- (NSString *)obfuscatedAccountNumber;

/**
 * Generates IAC from the account number.
 *
 * @return Generated IAC.
 */
- (NSString *)IAC;

/**
 * Updates this instance with the given account
 *
 * @param anAccount The account to update from
 */
- (void)updateFrom:(BankAccount *)anAccount;

/**
 * Updates the transactions list
 *
 * @param anAccountTransactionsList The account transactions list to update from
 */
- (void)updateAccountTransactionsList:(AccountTransactionList *)anAccountTransactionsList;

/**
 * Updates the transactions additional information
 *
 * @param aTransactionsAdditionalInformation The account transactions additional information to update from
 */
- (void)updateAccountTransactionsAdditionalInformation:(AccountTransactionsAdditionalInformation *)aTransactionsAdditionalInformation;

@end
