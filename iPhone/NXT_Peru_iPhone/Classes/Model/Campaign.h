//
//  Campaign.h
//  NXT_Peru_iPhone
//
//  Created by Estefany on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"

@interface Campaign : StatusEnabledResponse{
    @private
    
    /**
     * Campaign text
     */
    NSString *text_;
    
    /**
     * Campaign button name
     */
    NSString *buttonName_;
    
    /**
     * Campaign url
     */
    NSString *url_;
    
    /**
     * Campaign type
     */
    NSString *type_;
}

/**
 * Provides read-only access to the campaign text
 */
@property (nonatomic, readonly, copy) NSString * text;

/**
 * Provides read-only access to the campaign button name
 */
@property (nonatomic, readonly, copy) NSString * buttonName;

/**
 * Provides read-only access to the campaign url
 */
@property (nonatomic, readonly, copy) NSString * url;

/**
 * Provides read-only access to the campaign  type
 */
@property (nonatomic, readonly, copy) NSString * type;

@end
