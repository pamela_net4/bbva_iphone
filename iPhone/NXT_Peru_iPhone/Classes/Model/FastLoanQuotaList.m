/*
 * Copyright (c) 2011 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */


#import "FastLoanQuotaList.h"
#import "FastLoanQuota.h"


#pragma mark -

/**
 * FastLoanQuotaList private category
 */
@interface FastLoanQuotaList (private)

/**
 * Clears the object data
 *
 * @private
 */
- (void)clearFastLoanQuotaListData;

@end


#pragma mark -

@implementation FastLoanQuotaList

@dynamic fastloanquotaList;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void)dealloc {

    [self clearFastLoanQuotaListData];

	[fastloanquotaList_ release];
	fastloanquotaList_ = nil;

	[super dealloc];

}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initializer. Initializes a FastLoanQuotaList instance creating the associated array
 *
 * @return The initialized FastLoanQuotaList instance
 */
- (id)init {

    if (self = [super init]) {

        fastloanquotaList_ = [[NSMutableArray alloc] init];

    }

    return self;

}

#pragma mark -
#pragma mark Information management

/*
 * Updates the FastLoanQuota list from another FastLoanQuotaList instance
 */
- (void)updateFrom:(FastLoanQuotaList *)aFastLoanQuotaList {

    NSArray *otherFastLoanQuotaList = aFastLoanQuotaList.fastloanquotaList;

	[self clearFastLoanQuotaListData];

    for (FastLoanQuota *fastloanquota in otherFastLoanQuotaList) {

        [fastloanquotaList_ addObject:fastloanquota];

    }

    [self updateFromStatusEnabledResponse:aFastLoanQuotaList];

	self.informationUpdated = aFastLoanQuotaList.informationUpdated;

}

/*
 * Remove object data
 */
- (void)removeData {

    [self clearFastLoanQuotaListData];

}

#pragma mark -
#pragma mark Getters and setters

/**
 * Provides read access to the number of FastLoanQuota stored
 *
 * @return The count
 */
- (NSUInteger) fastloanquotaCount {
	return [fastloanquotaList_ count];
}

/*
 * Returns the FastLoanQuota located at the given position, or nil if position is not valid
 */
- (FastLoanQuota*) fastloanquotaAtPosition: (NSUInteger) aPosition {
	FastLoanQuota* result = nil;

	if (aPosition < [fastloanquotaList_ count]) {
		result = [fastloanquotaList_ objectAtIndex: aPosition];
	}

	return result;
}

/*
 * Returns the FastLoanQuota position inside the list
 */
- (NSUInteger) fastloanquotaPosition: (FastLoanQuota*) aFastLoanQuota {

	return [fastloanquotaList_ indexOfObject:aFastLoanQuota];

}

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {

    [super parserDidStartDocument:parser];

	xmlAnalysisCurrentValue_ = serxeas_Nothing;
	[self clearFastLoanQuotaListData];

}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {

    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];

	if (auxFastLoanQuota_ != nil) {

		[fastloanquotaList_ addObject:auxFastLoanQuota_];
		[auxFastLoanQuota_ release];
		auxFastLoanQuota_ = nil;

	}

	if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        NSString* lname = [elementName lowercaseString];

        if ([lname isEqualToString: @"e"]) {

            auxFastLoanQuota_ = [[FastLoanQuota alloc] init];
            [auxFastLoanQuota_ setParentParseableObject:self];
            auxFastLoanQuota_.openingTag = lname;
            [parser setDelegate:auxFastLoanQuota_];
            [auxFastLoanQuota_ parserDidStartDocument: parser];

        } else {

            xmlAnalysisCurrentValue_ = serxeas_Nothing;

        }

    }

}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {

    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];

    if (auxFastLoanQuota_ != nil) {

		[fastloanquotaList_ addObject:auxFastLoanQuota_];
		[auxFastLoanQuota_ release];
		auxFastLoanQuota_ = nil;

	}

    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {

        informationUpdated_ = soie_InfoAvailable;

    }

    xmlAnalysisCurrentValue_ = serxeas_Nothing;

}

#pragma mark -
#pragma mark Properties selectors

/*
 * Returns the fastloanquota list array
 *
 * @return The fastloanquota list array
 */
- (NSArray *)fastloanquotaList {

    return [NSArray arrayWithArray:fastloanquotaList_];

}

/**
 * Returns the FastLoanQuota with an FastLoanQuota id, or nil if not valid
 *//*
- (FastLoanQuota *)fastloanquotaFromFastLoanQuotaId:(NSString *)aFastLoanQuotaId {

	FastLoanQuota *fastloanquota = nil;

	if (aFastLoanQuotaId != nil) {
		FastLoanQuota *target;
		NSUInteger size = [fastloanquotaList_ count];
		BOOL found = NO;
		for (NSUInteger i = 0; (i < size) && (!found); i++) {
			target = [fastloanquotaList_ objectAtIndex:i];
			if ([aFastLoanQuotaId isEqualToString:target.id]) {
				fastloanquota = target;
				found = YES;
			}
		}
	}

	return fastloanquota;

}*/
@end


#pragma mark -

@implementation FastLoanQuotaList(private)

#pragma mark -
#pragma mark Memory management

/*
 * Clears the object data
 */
- (void)clearFastLoanQuotaListData {

    [fastloanquotaList_ removeAllObjects];

	informationUpdated_ = soie_NoInfo;

}

@end

