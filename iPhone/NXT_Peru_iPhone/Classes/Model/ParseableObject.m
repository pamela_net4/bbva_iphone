/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "ParseableObject.h"


#pragma mark -

@implementation ParseableObject

#pragma mark -
#pragma mark Properties

@synthesize parentParseableObject = parentParseableObject_;
@synthesize downloadFile = downloadFile_;
@synthesize informationUpdated = informationUpdated_;
@synthesize openingTag = openingTag_;
@synthesize elementString = auxString_;
@synthesize notificationToPost = notificationToPost_;

#pragma mark -
#pragma mark Memory management

/**
 * Deallocates used memory
 */
- (void) dealloc {
	
	parentParseableObject_ = nil;
	
	[notificationToPost_ release];
	notificationToPost_ = nil;
	
	[downloadFile_ release];
	downloadFile_ = nil;
	
	[openingTag_ release];
	openingTag_ = nil;
	
	[auxString_ release];
	auxString_ = nil;
	
	[super dealloc];
}

#pragma mark -
#pragma mark Instance initialization

/**
 * Designated initialiazer
 */
- (id) init {
	if (self = [super init]) {
		parentParseableObject_ = nil;
		downloadFile_ = nil;
		openingTag_ = nil;
		auxString_ = nil;
		notificationToPost_ = nil;
	}
	
	return self;
}

#pragma mark -
#pragma mark Basic parsing

/**
 * Sent by the parser object to the delegate when it starts parsing a document. Resets the auxiliary string used to accumulate characters found while parsing
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
	
	if (auxString_ == nil) {
	
		auxString_ = [[NSMutableString alloc] init];
		
	} else {
		
		[auxString_ setString:@""];
		
	}
	
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Auxiliary string is reseted
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {

	[auxString_ setString:@""];
	
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. If opening tag found, control is returned to the parent parseable object
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	NSString* lname = [elementName lowercaseString];
	
	if ([lname isEqualToString:openingTag_]) {
		
		[parser setDelegate:parentParseableObject_];
		
	}
	
}

/**
 * Sent by a parser object to provide its delegate with a string representing all or part of the characters of the current element.
 * Characters found are appended to the auxiliary string
 *
 * @param parser A parser object
 * @param string A string representing the complete or partial textual content of the current element
 */
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {

	[auxString_ appendString:string];
	
}

#pragma mark -
#pragma mark Information access

/*
 * Returns YES if information is ready to be used
 */
- (BOOL) isInformationReady {
	return (informationUpdated_ == soie_InfoAvailable);
}


#pragma mark -
#pragma mark Properties methods

/*
 * Sets the new opening tag
 *
 * @param aValue The new opening tag to store
 */
- (void)setOpeningTag:(NSString *)aValue {

	if (![openingTag_ isEqualToString:aValue]) {
	
		[openingTag_ release];
		openingTag_ = nil;
		
		openingTag_ = [[aValue lowercaseString] copy];
		
	}
	
}

@end
