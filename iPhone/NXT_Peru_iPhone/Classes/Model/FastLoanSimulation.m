//
//  FastLoanSimulation.m
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "FastLoanSimulation.h"
#import "Tools.h"
#import "FastLoanAlternativeList.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     flseas_AnalyzingAmount = serxeas_ElementsCount, //!<Analyzing the FastLoanSimulation amount
     flseas_AnalyzingAmountFormated , //!<Analyzing the FastLoanSimulation amountFormated
     flseas_AnalyzingTeaFormated , //!<Analyzing the FastLoanSimulation teaFormated
     flseas_AnalyzingDayOfPay , //!<Analyzing the FastLoanSimulation dayOfPay
     flseas_AnalyzingCurrencySymbol , //!<Analyzing the FastLoanSimulation currencySymbol
     flseas_AnalyzingFastLoanAlternativeList , //!<Analyzing the FastLoanSimulation fastLoanAlternativeList

} FastLoanSimulationXMLElementAnalyzerState;

@implementation FastLoanSimulation

#pragma mark -
#pragma mark Properties

@synthesize amount = amount_;
@synthesize amountFormated = amountFormated_;
@synthesize teaFormated = teaFormated_;
@synthesize dayOfPay = dayOfPay_;
@synthesize currencySymbol = currencySymbol_;
@synthesize fastLoanAlternativeList = fastLoanAlternativeList_;
;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [amount_ release];
    amount_ = nil;

    [amountFormated_ release];
    amountFormated_ = nil;

    [teaFormated_ release];
    teaFormated_ = nil;

    [dayOfPay_ release];
    dayOfPay_ = nil;

    [currencySymbol_ release];
    currencySymbol_ = nil;

    [fastLoanAlternativeList_ release];
    fastLoanAlternativeList_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if(0==1){
                   //delete fix
        }        else if ([lname isEqualToString: @"monto_prestamo_pr"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAmount;


        }
        else if ([lname isEqualToString: @"monto_prestamo_dis_pr"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingAmountFormated;


        }
        else if ([lname isEqualToString: @"tea_prestamo_dis_pr"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingTeaFormated;


        }
        else if ([lname isEqualToString: @"dia_pago_prestamo_pr"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingDayOfPay;


        }
        else if ([lname isEqualToString: @"moneda_pr"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingCurrencySymbol;


        }
        else if ([lname isEqualToString: @"alternativas"]) {

            xmlAnalysisCurrentValue_ = flseas_AnalyzingFastLoanAlternativeList;
            [fastLoanAlternativeList_ release];
            fastLoanAlternativeList_ = nil;
            fastLoanAlternativeList_ = [[FastLoanAlternativeList alloc] init];
            fastLoanAlternativeList_.openingTag = lname;
            [fastLoanAlternativeList_ setParentParseableObject:self];
            [parser setDelegate:fastLoanAlternativeList_];
            [fastLoanAlternativeList_ parserDidStartDocument:parser];


        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(0==1){
           //delete fix
        }        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingAmount) {

            [amount_ release];
            amount_ = nil;
            amount_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingAmountFormated) {

            [amountFormated_ release];
            amountFormated_ = nil;
            amountFormated_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingTeaFormated) {

            [teaFormated_ release];
            teaFormated_ = nil;
            teaFormated_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingDayOfPay) {

            [dayOfPay_ release];
            dayOfPay_ = nil;
            dayOfPay_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flseas_AnalyzingCurrencySymbol) {

            [currencySymbol_ release];
            currencySymbol_ = nil;
            currencySymbol_ = [elementString copyWithZone:self.zone];

        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

