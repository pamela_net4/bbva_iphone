//
//  FrequentCard.m
//  NXT_Peru_iPhone
//
//  Created by MDP on 6/10/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import "FrequentCard.h"

@implementation FrequentCard

- (id) initWithDatos:(FMResultSet*)entidadResulSet
{
    
    self=[super init];
    
    if(self){
        
        self.code = [entidadResulSet objectForColumnName:@"code"];
        self.alias = [entidadResulSet objectForColumnName:@"alias"];
        self.value = [entidadResulSet objectForColumnName:@"value"];
        self.flag_last_user = [entidadResulSet objectForColumnName:@"flag_last_user"];
        self.masked = [entidadResulSet objectForColumnName:@"masked"];
        
    }
    return self;
}


@end
