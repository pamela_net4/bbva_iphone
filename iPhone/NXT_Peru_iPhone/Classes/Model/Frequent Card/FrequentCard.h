//
//  FrequentCard.h
//  NXT_Peru_iPhone
//
//  Created by MDP on 6/10/14.
//  Copyright (c) 2014 Movilok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMResultSet.h"

@interface FrequentCard : NSObject

@property (nonatomic,strong) NSNumber * code;
@property (nonatomic,strong) NSString * alias;
@property (nonatomic,strong) NSString * value;
@property (nonatomic,strong) NSString * masked;
@property (nonatomic,strong) NSNumber * flag_last_user;

- (id) initWithDatos:(FMResultSet*)entidadResulSet;

@end
