/*
 * Copyright (c) 2012 Movilok. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * Movilok ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with Movilok.
 */


#import "StatusEnabledResponse.h"


/**
 * Stores one account transactions additional information element
 *
 * @author <a href="mailto:info@movilok.com">Movilok Interactividad Movil S.L.</a>
 */
@interface AccountTransactionDetail : StatusEnabledResponse {

@private
    
    /**
     * Description
     */
    NSString *description_;
    
    /**
     * Operation hour
     */
    NSString *operationHour_;    
    
    /**
     * Accounting date string
     */
    NSString *accountingDateString_;
    
    /**
     * Accounting date
     */
    NSDate *accountingDate_;
    
    /**
     * Check number
     */
    NSString *checkNumber_;
    
    /**
     * Type
     */
    NSString *type_;
    
    /**
     * Center
     */
    NSString *center_;
    
    /**
     * Literal
     */
    NSString *literal_;
    
}


/**
 * Provides read-write access to the description
 */
@property (nonatomic, readwrite, copy) NSString *description;

/**
 * Provides read-write access to the operationHour
 */
@property (nonatomic, readwrite, copy) NSString *operationHour;    

/**
 * Provides read-write access to the accountingDateString
 */
@property (nonatomic, readwrite, copy) NSString *accountingDateString;

/**
 * Provides read-write access to the accounting date
 */
@property (nonatomic, readwrite, copy) NSDate *accountingDate;

/**
 * Provides read-write access to the checkNumber
 */
@property (nonatomic, readwrite, copy) NSString *checkNumber;

/**
 * Provides read-write access to the type
 */
@property (nonatomic, readwrite, copy) NSString *type;

/**
 * Provides read-write access to the center
 */
@property (nonatomic, readwrite, copy) NSString *center;

/**
 * Provides read-write access to the literal
 */
@property (nonatomic, readwrite, copy) NSString *literal;



@end
