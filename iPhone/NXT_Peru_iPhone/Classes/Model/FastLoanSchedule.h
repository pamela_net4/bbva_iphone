//
//  FastLoanSchedule.h
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "StatusEnabledResponse.h"
@class FastLoanQuotaList;


@interface FastLoanSchedule : StatusEnabledResponse{
    @private

   /**
     * FastLoanSchedule currencySymbol
     */
    NSString *currencySymbol_;


   /**
     * FastLoanSchedule term
     */
    NSString *term_;


   /**
     * FastLoanSchedule tceaFormated
     */
    NSString *tceaFormated_;


   /**
     * FastLoanSchedule amortizationTotal
     */
    NSString *amortizationTotal_;


   /**
     * FastLoanSchedule interestTotal
     */
    NSString *interestTotal_;


   /**
     * FastLoanSchedule commissionTotal
     */
    NSString *commissionTotal_;


   /**
     * FastLoanSchedule quotaTotal
     */
    NSString *quotaTotal_;


   /**
     * FastLoanSchedule comment
     */
    NSString *comment_;


   /**
     * FastLoanSchedule solicitudeDate
     */
    NSString *solicitudeDate_;


   /**
     * FastLoanSchedule fastLoanQuotaList
     */
    FastLoanQuotaList *fastLoanQuotaList_;




}

/**
 * Provides read-only access to the FastLoanSchedule currencySymbol
 */
@property (nonatomic, readonly, copy) NSString * currencySymbol;


/**
 * Provides read-only access to the FastLoanSchedule term
 */
@property (nonatomic, readonly, copy) NSString * term;


/**
 * Provides read-only access to the FastLoanSchedule tceaFormated
 */
@property (nonatomic, readonly, copy) NSString * tceaFormated;


/**
 * Provides read-only access to the FastLoanSchedule amortizationTotal
 */
@property (nonatomic, readonly, copy) NSString * amortizationTotal;


/**
 * Provides read-only access to the FastLoanSchedule interestTotal
 */
@property (nonatomic, readonly, copy) NSString * interestTotal;


/**
 * Provides read-only access to the FastLoanSchedule commissionTotal
 */
@property (nonatomic, readonly, copy) NSString * commissionTotal;


/**
 * Provides read-only access to the FastLoanSchedule quotaTotal
 */
@property (nonatomic, readonly, copy) NSString * quotaTotal;


/**
 * Provides read-only access to the FastLoanSchedule comment
 */
@property (nonatomic, readonly, copy) NSString * comment;


/**
 * Provides read-only access to the FastLoanSchedule solicitudeDate
 */
@property (nonatomic, readonly, copy) NSString * solicitudeDate;


/**
 * Provides read-only access to the FastLoanSchedule fastLoanQuotaList
 */
@property (nonatomic, readonly, copy) FastLoanQuotaList * fastLoanQuotaList;




@end

