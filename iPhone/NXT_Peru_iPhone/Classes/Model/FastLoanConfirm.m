//
//  FastLoanConfirm.m
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "FastLoanConfirm.h"
#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     flceas_AnalyzingCustomer = serxeas_ElementsCount, //!<Analyzing the FastLoanConfirm customer
     flceas_AnalyzingCurrencyAmount , //!<Analyzing the FastLoanConfirm currencyAmount
     flceas_AnalyzingAmountFormated , //!<Analyzing the FastLoanConfirm amountFormated
     flceas_AnalyzingQuotaFormated , //!<Analyzing the FastLoanConfirm quotaFormated
     flceas_AnalyzingTerm , //!<Analyzing the FastLoanConfirm term
     flceas_AnalyzingQuotaType , //!<Analyzing the FastLoanConfirm quotaType
     flceas_AnalyzingMonthDobles , //!<Analyzing the FastLoanConfirm monthDobles
     flceas_AnalyzingDayOfPay , //!<Analyzing the FastLoanConfirm dayOfPay
     flceas_AnalyzingTceaFormated , //!<Analyzing the FastLoanConfirm tceaFormated
     flceas_AnalyzingTeaFormated , //!<Analyzing the FastLoanConfirm teaFormated
     flceas_AnalyzingAccount , //!<Analyzing the FastLoanConfirm account
     flceas_AnalyzingCurrencyAccount , //!<Analyzing the FastLoanConfirm currencyAccount
     flceas_AnalyzingEmail , //!<Analyzing the FastLoanConfirm email
     flceas_AnalyzingSeal , //!<Analyzing the FastLoanConfirm seal
     flceas_AnalyzingCoordinate , //!<Analyzing the FastLoanConfirm coordinate
     flceas_AnalyzingDisclaimerContract , //!<Analyzing the FastLoanConfirm disclaimerContract
     flceas_AnalyzingDisclaimerContractWeb , //!<Analyzing the FastLoanConfirm disclaimerContractWeb
     flceas_AnalyzingDisclaimerASecured , //!<Analyzing the FastLoanConfirm disclaimerASecured
     flceas_AnalyzingDisclaimerASecuredWeb , //!<Analyzing the FastLoanConfirm disclaimerASecuredWeb

} FastLoanConfirmXMLElementAnalyzerState;

@implementation FastLoanConfirm

#pragma mark -
#pragma mark Properties

@synthesize customer = customer_;
@synthesize currencyAmount = currencyAmount_;
@synthesize amountFormated = amountFormated_;
@synthesize quotaFormated = quotaFormated_;
@synthesize term = term_;
@synthesize quotaType = quotaType_;
@synthesize monthDobles = monthDobles_;
@synthesize dayOfPay = dayOfPay_;
@synthesize tceaFormated = tceaFormated_;
@synthesize teaFormated = teaFormated_;
@synthesize account = account_;
@synthesize currencyAccount = currencyAccount_;
@synthesize email = email_;
@synthesize seal = seal_;
@synthesize coordinate = coordinate_;
@synthesize disclaimerContract = disclaimerContract_;
@synthesize disclaimerContractWeb = disclaimerContractWeb_;
@synthesize disclaimerASecured = disclaimerASecured_;
@synthesize disclaimerASecuredWeb = disclaimerASecuredWeb_;
;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [customer_ release];
    customer_ = nil;

    [currencyAmount_ release];
    currencyAmount_ = nil;

    [amountFormated_ release];
    amountFormated_ = nil;

    [quotaFormated_ release];
    quotaFormated_ = nil;

    [term_ release];
    term_ = nil;

    [quotaType_ release];
    quotaType_ = nil;

    [monthDobles_ release];
    monthDobles_ = nil;

    [dayOfPay_ release];
    dayOfPay_ = nil;

    [tceaFormated_ release];
    tceaFormated_ = nil;

    [teaFormated_ release];
    teaFormated_ = nil;

    [account_ release];
    account_ = nil;

    [currencyAccount_ release];
    currencyAccount_ = nil;

    [email_ release];
    email_ = nil;

    [seal_ release];
    seal_ = nil;

    [coordinate_ release];
    coordinate_ = nil;

    [disclaimerContract_ release];
    disclaimerContract_ = nil;

    [disclaimerContractWeb_ release];
    disclaimerContractWeb_ = nil;

    [disclaimerASecured_ release];
    disclaimerASecured_ = nil;

    [disclaimerASecuredWeb_ release];
    disclaimerASecuredWeb_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if(0==1){
                   //delete fix
        }        else if ([lname isEqualToString: @"nombrecliente"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingCustomer;


        }
        else if ([lname isEqualToString: @"monedamonto"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingCurrencyAmount;


        }
        else if ([lname isEqualToString: @"importemonto_dis"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingAmountFormated;


        }
        else if ([lname isEqualToString: @"valor_cuota_dis"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingQuotaFormated;


        }
        else if ([lname isEqualToString: @"plazo"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingTerm;


        }
        else if ([lname isEqualToString: @"tipocuota"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingQuotaType;


        }
        else if ([lname isEqualToString: @"mesescuotasdobles"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingMonthDobles;


        }
        else if ([lname isEqualToString: @"diapago"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingDayOfPay;


        }
        else if ([lname isEqualToString: @"tcea_dis"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingTceaFormated;


        }
        else if ([lname isEqualToString: @"tea_dis"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingTeaFormated;


        }
        else if ([lname isEqualToString: @"cuenta"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingAccount;


        }
        else if ([lname isEqualToString: @"monedacuenta"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingCurrencyAccount;


        }
        else if ([lname isEqualToString: @"correo"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingEmail;


        }
        else if ([lname isEqualToString: @"sello"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingSeal;


        }
        else if ([lname isEqualToString: @"coordenada"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingCoordinate;


        }
        else if ([lname isEqualToString: @"disclaimercontrato"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingDisclaimerContract;


        }
        else if ([lname isEqualToString: @"disclaimercontratoweb"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingDisclaimerContractWeb;


        }
        else if ([lname isEqualToString: @"disclaimerseguro"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingDisclaimerASecured;


        }
        else if ([lname isEqualToString: @"disclaimerseguroweb"]) {

            xmlAnalysisCurrentValue_ = flceas_AnalyzingDisclaimerASecuredWeb;


        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(0==1){
           //delete fix
        }        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingCustomer) {

            [customer_ release];
            customer_ = nil;
            customer_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingCurrencyAmount) {

            [currencyAmount_ release];
            currencyAmount_ = nil;
            currencyAmount_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingAmountFormated) {

            [amountFormated_ release];
            amountFormated_ = nil;
            amountFormated_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingQuotaFormated) {

            [quotaFormated_ release];
            quotaFormated_ = nil;
            quotaFormated_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingTerm) {

            [term_ release];
            term_ = nil;
            term_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingQuotaType) {

            [quotaType_ release];
            quotaType_ = nil;
            quotaType_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingMonthDobles) {

            [monthDobles_ release];
            monthDobles_ = nil;
            monthDobles_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingDayOfPay) {

            [dayOfPay_ release];
            dayOfPay_ = nil;
            dayOfPay_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingTceaFormated) {

            [tceaFormated_ release];
            tceaFormated_ = nil;
            tceaFormated_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingTeaFormated) {

            [teaFormated_ release];
            teaFormated_ = nil;
            teaFormated_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingAccount) {

            [account_ release];
            account_ = nil;
            account_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingCurrencyAccount) {

            [currencyAccount_ release];
            currencyAccount_ = nil;
            currencyAccount_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingEmail) {

            [email_ release];
            email_ = nil;
            email_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingSeal) {

            [seal_ release];
            seal_ = nil;
            seal_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingCoordinate) {

            [coordinate_ release];
            coordinate_ = nil;
            coordinate_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingDisclaimerContract) {

            [disclaimerContract_ release];
            disclaimerContract_ = nil;
            disclaimerContract_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingDisclaimerContractWeb) {

            [disclaimerContractWeb_ release];
            disclaimerContractWeb_ = nil;
            disclaimerContractWeb_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingDisclaimerASecured) {

            [disclaimerASecured_ release];
            disclaimerASecured_ = nil;
            disclaimerASecured_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flceas_AnalyzingDisclaimerASecuredWeb) {

            [disclaimerASecuredWeb_ release];
            disclaimerASecuredWeb_ = nil;
            disclaimerASecuredWeb_ = [elementString copyWithZone:self.zone];

        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

