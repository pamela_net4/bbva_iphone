//
//  Campaign.m
//  NXT_Peru_iPhone
//
//  Created by Estefany on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "Campaign.h"
#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
    lxeas_AnalyzingText = serxeas_ElementsCount, //!<Analyzing the campaign text
    lxeas_AnalyzingButtonName, //!<Analyzing the campaign button name
    lxeas_AnalyzingUrl, //!<Analyzing the campaign url
    lxeas_AnalyzingType //!<Analyzing the campaign type
} LoanXMLElementAnalyzerState;

@implementation Campaign

#pragma mark -
#pragma mark Properties

@synthesize text = text_;
@synthesize buttonName = buttonName_;
@synthesize url = url_;
@synthesize type = type_;


#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [text_ release];
    text_ = nil;
    
    [buttonName_ release];
    buttonName_ = nil;
    
    [url_ release];
    url_ = nil;
    
    [type_ release];
    type_ = nil;
    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];
        
        if ([lname isEqualToString: @"texto_campania"]) {
            
            xmlAnalysisCurrentValue_ = lxeas_AnalyzingText;
            
        } else if ([lname isEqualToString: @"nombre_boton"]) {
            
            xmlAnalysisCurrentValue_ = lxeas_AnalyzingButtonName;
            
        } else if ([lname isEqualToString: @"url"]) {
            
            xmlAnalysisCurrentValue_ = lxeas_AnalyzingUrl;
            
        }  else if ([lname isEqualToString: @"tipo"]) {
            
            xmlAnalysisCurrentValue_ = lxeas_AnalyzingType;
            
        } else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (xmlAnalysisCurrentValue_ == lxeas_AnalyzingText) {
            
            [text_ release];
            text_ = nil;
            text_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == lxeas_AnalyzingButtonName) {
            
            [buttonName_ release];
            buttonName_ = nil;
            buttonName_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == lxeas_AnalyzingUrl) {
            
            [url_ release];
            url_ = nil;
            url_ = [elementString copyWithZone:self.zone];
            
        } else if (xmlAnalysisCurrentValue_ == lxeas_AnalyzingType) {
            
            [type_ release];
            type_ = nil;
            type_ = [elementString copyWithZone:self.zone];
            
        }
        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end
