//
//  FastLoanQuota.m
//  NXT_Peru_iPhone
//
//  Created by Author on 13/01/16.
//  Copyright © 2016 Movilok. All rights reserved.
//

#import "FastLoanQuota.h"
#import "Tools.h"


/**
 * Enumeration for the state of the analysis
 */
typedef enum {
    
     flqeas_AnalyzingQuotaNumber = serxeas_ElementsCount, //!<Analyzing the FastLoanQuota quotaNumber
     flqeas_AnalyzingExpirationDate , //!<Analyzing the FastLoanQuota expirationDate
     flqeas_AnalyzingAmortization , //!<Analyzing the FastLoanQuota amortization
     flqeas_AnalyzingInterest , //!<Analyzing the FastLoanQuota interest
     flqeas_AnalyzingCommissionSent , //!<Analyzing the FastLoanQuota commissionSent
     flqeas_AnalyzingQuotaTotal , //!<Analyzing the FastLoanQuota quotaTotal
     flqeas_AnalyzingBalance , //!<Analyzing the FastLoanQuota balance

} FastLoanQuotaXMLElementAnalyzerState;

@implementation FastLoanQuota

#pragma mark -
#pragma mark Properties

@synthesize quotaNumber = quotaNumber_;
@synthesize expirationDate = expirationDate_;
@synthesize amortization = amortization_;
@synthesize interest = interest_;
@synthesize commissionSent = commissionSent_;
@synthesize quotaTotal = quotaTotal_;
@synthesize balance = balance_;
;

#pragma mark -
#pragma mark NSXMLParserDelegate protocol selectors

/**
 * Sent by the parser object to the delegate when it begins parsing a document.
 *
 * @param parser A parser object
 */
- (void)parserDidStartDocument:(NSXMLParser *)parser {
    
    [super parserDidStartDocument:parser];
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    [self clearData];
    
}

#pragma mark -
#pragma mark Memory management

/*
 * Clears the data of the object
 */
- (void) clearData {
    
    [quotaNumber_ release];
    quotaNumber_ = nil;

    [expirationDate_ release];
    expirationDate_ = nil;

    [amortization_ release];
    amortization_ = nil;

    [interest_ release];
    interest_ = nil;

    [commissionSent_ release];
    commissionSent_ = nil;

    [quotaTotal_ release];
    quotaTotal_ = nil;

    [balance_ release];
    balance_ = nil;


    
}

/**
 * Sent by a parser object to its delegate when it encounters a start tag for a given element. Next element is analyzed, or
 * its internal code is stored
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qualifiedName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 * @param attributeDict A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values
 */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    [super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributeDict];
    
    if (xmlAnalysisCurrentValue_ == serxeas_Nothing) {
        
        NSString* lname = [elementName lowercaseString];

        if(0==1){
                   //delete fix
        }        else if ([lname isEqualToString: @"numero_de_cuota"]) {

            xmlAnalysisCurrentValue_ = flqeas_AnalyzingQuotaNumber;


        }
        else if ([lname isEqualToString: @"fecha_vencimiento"]) {

            xmlAnalysisCurrentValue_ = flqeas_AnalyzingExpirationDate;


        }
        else if ([lname isEqualToString: @"amortizacion"]) {

            xmlAnalysisCurrentValue_ = flqeas_AnalyzingAmortization;


        }
        else if ([lname isEqualToString: @"interes"]) {

            xmlAnalysisCurrentValue_ = flqeas_AnalyzingInterest;


        }
        else if ([lname isEqualToString: @"comision_envio"]) {

            xmlAnalysisCurrentValue_ = flqeas_AnalyzingCommissionSent;


        }
        else if ([lname isEqualToString: @"cuota_total"]) {

            xmlAnalysisCurrentValue_ = flqeas_AnalyzingQuotaTotal;


        }
        else if ([lname isEqualToString: @"saldo"]) {

            xmlAnalysisCurrentValue_ = flqeas_AnalyzingBalance;


        }

        else {
            
            xmlAnalysisCurrentValue_ = serxeas_Nothing;
            
        }
        
    }
    
}

/**
 * Sent by a parser object to its delegate when it encounters an end tag for a specific element. Data formating is performed if necessary
 *
 * @param parser A parser object
 * @param elementName A string that is the name of an element (in its start tag)
 * @param namespaceURI If namespace processing is turned on, contains the URI for the current namespace as a string object
 * @param qName If namespace processing is turned on, contains the qualified name for the current namespace as a string object
 */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    [super parser:parser didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qName];
    
    if (xmlAnalysisCurrentValue_ != serxeas_Nothing) {
        
        NSString *elementString = [self.elementString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(0==1){
           //delete fix
        }        else if (xmlAnalysisCurrentValue_ == flqeas_AnalyzingQuotaNumber) {

            [quotaNumber_ release];
            quotaNumber_ = nil;
            quotaNumber_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flqeas_AnalyzingExpirationDate) {

            [expirationDate_ release];
            expirationDate_ = nil;
            expirationDate_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flqeas_AnalyzingAmortization) {

            [amortization_ release];
            amortization_ = nil;
            amortization_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flqeas_AnalyzingInterest) {

            [interest_ release];
            interest_ = nil;
            interest_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flqeas_AnalyzingCommissionSent) {

            [commissionSent_ release];
            commissionSent_ = nil;
            commissionSent_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flqeas_AnalyzingQuotaTotal) {

            [quotaTotal_ release];
            quotaTotal_ = nil;
            quotaTotal_ = [elementString copyWithZone:self.zone];

        }
        else if (xmlAnalysisCurrentValue_ == flqeas_AnalyzingBalance) {

            [balance_ release];
            balance_ = nil;
            balance_ = [elementString copyWithZone:self.zone];

        }

        
    } else {
        
        informationUpdated_ = soie_InfoAvailable;
        
    }
    
    xmlAnalysisCurrentValue_ = serxeas_Nothing;
    
}


@end

